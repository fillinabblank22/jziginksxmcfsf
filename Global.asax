<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="Castle.Core.Internal" %>
<%@ Import Namespace="Castle.MicroKernel.Registration" %>
<%@ Import Namespace="Castle.MicroKernel.Resolvers.SpecializedResolvers" %>
<%@ Import Namespace="Castle.Windsor" %>
<%@ Import Namespace="Castle.Windsor.Installer" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Indications" %>
<%@ Import Namespace="SolarWinds.Orion.Platform.Models" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.i18n" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.i18n.WebConsoleLanguage" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Session" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Settings" %>
<%@ Import Namespace="SolarWinds.Orion.Web.WebConsoleLanguage" %>

<%@ Application Language="C#" %>

<script runat="server">
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    void Application_Start(object sender, EventArgs e)
    {

        // Code that runs on application startup
#pragma warning disable 618
        //Currently there is no replacement for obsolete 'SetShadowCopyPath' method
        AppDomain.CurrentDomain.SetShadowCopyPath(
            string.Join(";",
                AppDomain.CurrentDomain.SetupInformation.ShadowCopyDirectories,
                SolarWinds.Orion.Core.Common.RegistrySettings.GetInstallLocation()));
#pragma warning restore 618

        SolarWinds.Logging.Log.Configure(Server.MapPath("/web.config"));

        log.Info("Application_Start");
        log.InfoFormat("ShadowCopyPath set to {0}", AppDomain.CurrentDomain.SetupInformation.ShadowCopyDirectories);

        // Redirect all calls to SHA256.Create() to be redirected to SHA256Cng, which is FIPS-compliant.
        CryptoConfig.AddAlgorithm(typeof(SHA256Cng), "System.Security.Cryptography.SHA256");

        SetupDependencies();

        // SignalR initialization
        RouteTable.Routes.MapHubs();

        GlobalConfiguration.Configure(WebApiConfig.Register);

        GlobalConfiguration.Configuration.Services.Replace(
            typeof(System.Web.Http.Dispatcher.IHttpControllerActivator),
            new WindsorControllerActivator((IWindsorContainer) this.Application[IOCConstants.ApplicationKey]));

        DeleteOldLogFiles();

        using (string.IsNullOrEmpty(LocaleConfiguration.CustomLocale) ? LocaleThreadState.EnsurePrimaryLocale() :
            LocaleThreadState.EnsureLocale(LocaleConfiguration.SuggestSupportedLocaleForThread(LocaleConfiguration.CustomLocale)))
        {
            OrionModuleManager.Initialize();

            SolarWinds.Orion.Core.Reporting.ReportingRepository.Begin(SolarWinds.Orion.Web.UI.StatusIcons.StatusIconPixelSizeCache.Instance);
            SolarWinds.Orion.Web.Map.MapEdgeTooltipRepositoryLoader.Initialize();
            SolarWinds.Orion.Web.Map.MapPointTooltipRepositoryLoader.Initialize();
        }


        AccountContext.GetContextAccountID += GetAccountIDFromHttpContext;
    }

    private AssemblyFilter RemoveApolloAssembliesFromFilter(AssemblyFilter filter)
    {
        return filter.FilterByName(p =>
            !p.Name.StartsWith("SolarWinds.Orion.Web.Platform") &&
            !p.Name.StartsWith("SolarWinds.Orion.Web.Services"));
    }

    private void SetupDependencies()
    {
        var orionAssemblies = new AssemblyFilter(HttpRuntime.BinDirectory, "SolarWinds.Orion*");
        var apiAssemblies = new AssemblyFilter(System.Web.HttpRuntime.BinDirectory, "SolarWinds*");

        orionAssemblies = RemoveApolloAssembliesFromFilter(orionAssemblies);

        var container = new WindsorContainer();
        container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));
        container
            .Register(
                Component.For<IWindsorContainer>()
                    .Instance(container)
                    .LifestyleSingleton(),
                Classes.FromAssemblyInDirectory(new AssemblyFilter(System.Web.HttpRuntime.BinDirectory, "OrionWeb.dll")).BasedOn(typeof(System.Web.Http.ApiController)).LifestylePerWebRequest()
            )
            .Install(
                FromAssembly.InDirectory(new AssemblyFilter(HttpRuntime.BinDirectory, "OrionWeb.dll")),
                FromAssembly.InDirectory(orionAssemblies));

        IAssemblyProvider ap = RemoveApolloAssembliesFromFilter(apiAssemblies);
        foreach (var assembly in ap.GetAssemblies())
        {
            try
            {
                container.Register(Classes.FromAssembly(assembly).BasedOn(typeof(System.Web.Http.ApiController))
                    .LifestylePerWebRequest());
            }
            catch (Exception) {}
        }

        (new LanguageDetectorsInstallerHelper())
            .Install<DefaultDetector>(container);

        this.Application[SolarWinds.Orion.Platform.Models.IOCConstants.ApplicationKey] = container;
        this.Application["WindsorServiceProvider"] = new SolarWinds.Orion.Web.WindsorServiceProvider(container);
    }

    static string GetAccountIDFromHttpContext()
    {
        var context = HttpContext.Current;
        var identity = Thread.CurrentPrincipal?.Identity;
        if (context == null && ( identity == null || identity.IsAuthenticated ))
        {
            if (log.IsTraceEnabled)
            {
                log.TraceFormat("null account for ({0})", identity?.Name ?? "null");
            }
            return null;
        }
        if (context != null && context.Profile != null && ! string.IsNullOrEmpty(context.Profile.UserName))
        {
            return context.Profile.UserName;
        }
        return string.Format("{0}_{1:N}", context?.User?.Identity?.Name ?? "", Guid.NewGuid());
    }

    void DeleteOldLogFiles()
    {
        try
        {
            Regex filenameMatcher = new Regex(@"OrionWeb\.log\.\d\d\d\d-\d\d-\d\d", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            string path = AppDomain.CurrentDomain.BaseDirectory;
            foreach (string file in System.IO.Directory.GetFiles(path, "OrionWeb.log.*"))
            {
                if (filenameMatcher.IsMatch(file))
                {
                    string fullName = System.IO.Path.Combine(path, file);
                    try
                    {
                        System.IO.File.Delete(fullName);
                        log.InfoFormat("Deleted {0}", fullName);
                    }
                    catch (Exception ex)
                    {
                        log.WarnFormat("Failed to delete old log file {0}: {1}", fullName, ex.Message);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Warn("Error deleting old log files.", ex);
        }
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
        SolarWinds.Orion.Web.Instrumentation.WebsitePerformanceMonitor.Shutdown();
        SessionManager.Shutdown();
        log.InfoFormat("Application_End");
    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        Exception ex = Server.GetLastError();

        try
        {
            if (ex != null && ex.InnerException is System.Web.UI.ViewStateException) {
                Server.ClearError();
                Response.Clear();
                Response.RedirectLocation = "/Orion/Login.aspx";
                Response.StatusCode = 302;
                Response.End();
                log.Warn("ViewStateException happened. To prevent this you can setup in web.config machineKey element attributes decryptionKey and validationKey to particular value instead of AutoGenerate.");
                log.Debug(ex);
                return;
            }

            // exceptions on login pages turn into endless redirects, which are bad.
            if (Profile != null && OrionMinReqsMaster.IsLoginPage == false && OrionMinReqsMaster.IsErrorPage == false)
            {
                //Check if this exception has been caused by current user being deleted by admin
                string currentUser = Profile.UserName;
                bool currentUserExists = !string.IsNullOrEmpty(currentUser) && SolarWinds.Orion.Web.DAL.AccountManagementDAL.AccountExists(currentUser);
                if (!currentUserExists)
                {
                    Server.ClearError();
                    Response.Clear();
                    Response.RedirectLocation = "/Orion/Logout.aspx";
                    Response.StatusCode = 302;
                    Response.End();
                    return;
                }
            }
        }
        catch (System.Exception unknownEx)
        {
            log.WarnFormat("Application_Error: Additional Exception checking current user account status: {0}", unknownEx.ToString());
        }

        // sometimes this method is called again after processing Error.aspx, 
        // which clears the server errors and GetLastError returns null
        // server.transfer is not working for updatepanels, skip them
        if (ex == null || String.Equals(this.Request.Headers["X-MicrosoftAjax"], "delta=true", StringComparison.InvariantCultureIgnoreCase))
        {
            this.Server.ClearError();
            return;
        }

        if (ex is HttpException && (((HttpException)ex).GetHttpCode() == 404))
        {
            // 404.  Check if the requested page is one we have a redirect for.
            if (Handle404Redirects())
                return;
        }

        // if the code below throws an exception, we'll see the 'yellow screen of death'. That's ok.

        string logappend = string.Empty;
        ErrorInspectorDetails details;

        Server.ClearError();

        try
        {
            details =
                ErrorInspector.Inspect(Context, ex, ErrorInspector.CoreUnhandledWebsiteInspector()) ??
                ErrorInspector.Inspect(Context, ex.InnerException, ErrorInspector.CoreUnhandledWebsiteInspector());

            if (details == null)
            {
                if (ex is HttpUnhandledException && ex.InnerException != null)
                    ex = ex.InnerException;

                details = new ErrorInspectorDetails { Error = ex };
            }

            logappend = "(" + details.Id + ")";
        }
        finally
        {
            log.Error("Application_Error" + logappend, ex);
            if (ex.InnerException != null)
            {
                log.Error("Application_Error: Inner exception", ex.InnerException);
            }
        }

        OrionErrorPageBase.TransferToErrorPage(Context, details);
    }

    private bool Handle404Redirects()
    {
        log.WarnFormat("404 File Not Found for {0}", Request.Path);

        var urlRedirects = OrionModuleManager.GetUrlRedirects();

        UriBuilder builder = new UriBuilder(Request.Url);

        // Handle Module UrlRedirects
        string redirectPath;
        if (urlRedirects.TryGetValue(builder.Path, out redirectPath))
        {
            builder.Path = redirectPath;
            log.WarnFormat("Redirecting 404 path to {0}", builder.ToString());

            Server.ClearError();
            Response.Clear();
            Response.RedirectLocation = builder.ToString();
            Response.StatusCode = 301;
            Response.End();
            return true;
        }

        // Handle Default.aspx redirects
        // Automatic Default.aspx redirecting doesn't happen on IIS 5.1 when wildcard app map is used
        // - we need to do this manually for when wildcard app mapping is enabled.
        if (builder.ToString().EndsWith("/"))
        {
            builder.Path = builder.Path + "Default.aspx";
            log.WarnFormat("Redirecting 404 path to {0}", builder.ToString());
            Server.ClearError();
            Response.Clear();
            Response.RedirectLocation = builder.ToString();
            Response.StatusCode = 301;
            Response.End();
            return true;
        }

        return false;
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
        log.InfoFormat($"Session_Start ({Session?.SessionID})");
        Session.Timeout = SolarWinds.Orion.Web.DAL.WebSettingsDAL.SessionTimeoutMinutes;
        SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.SessionStart(sender, e);
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        log.InfoFormat($"Session_End ({Session?.SessionID})");

        var userName = Session["-hack-username"]?.ToString();
        var userHostAddress = Session["-UserHostAddress"]?.ToString();

        if (string.IsNullOrEmpty(userName) == false)
        {
            if (!OrionMembershipProvider.GetDisableSessionTimeout(userName))
            {
                if (Session?.SessionID != null)
                    SessionManager.ExpireSession(Session.SessionID);

                if (WebSecuritySettings.Instance.EnableSessionEndAudit)
                {
                    var indication = new SessionEndIndication(IndicationType.Orion_SessionEnd, userName, userHostAddress);
                    IndicationPublisher.CreateV3().ReportIndication(indication);
                }
            }
        }

        SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.SessionEnd(sender, e);
    }

    private void RemoveDoubleSlashesFromUrl()
    {
        string requestUrl = Request.ServerVariables["REQUEST_URI"];
        string rewriteUrl = Request.ServerVariables["UNENCODED_URL"];
        if (rewriteUrl.Contains("//") && !requestUrl.Contains("//"))
            Response.RedirectPermanent(requestUrl);
    }

    private void EnsureThreadLocale()
    {
        var container = (IWindsorContainer)this.Application[SolarWinds.Orion.Platform.Models.IOCConstants.ApplicationKey];
        var provider = container.Resolve<WebConsoleLanguageProvider>();

        var lang = provider.SuggestLocaleForThread(Request);
        System.Threading.Thread.CurrentThread.CurrentUICulture = lang;

        log.VerboseFormat("EnsureThreadLocale: '{0}'", lang?.Name);
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        log.DebugFormat("BeginRequest: {0}", GetRequestUrl(Request));

        RemoveDoubleSlashesFromUrl();
        RedirectToLoginWithNoAutoLogin();
        EnsureThreadLocale();

        var isMobDev = Context.Items["IsMobileDeviceDetected"];
        if (isMobDev == null && CommonWebHelper.IsMobileDevicesAutodetectEnabled)
        {
            bool isMobile = !string.IsNullOrEmpty(Request.Browser["IsMobileDevice"]) && Request.Browser["IsMobileDevice"].Equals("true", StringComparison.OrdinalIgnoreCase);
            Context.Items["IsMobileDeviceDetected"] = isMobile || Request.Browser.IsMobileDevice;
        }

        Context.Items["RequestStartTime"] = DateTime.Now;

        var url = Request.CurrentExecutionFilePath;

        // for legacy pages that do not have an orion master page.
        SolarWinds.Orion.Web.UI.Localizer.HeadAdapterEnsureCorePageControls.HookRequest();
        // for backwards compatibility after change to .NET 4 (and ability to specify custom styles for validators)
        SolarWinds.Orion.Web.UI.ValidatorControlAdapter.HookRequest();

        var page = Context.Handler as Page;
        if (page != null && !(page.Master is OrionMinReqsMaster))
        {
            //for module pages that do not use OrionMinReqs masterpage, we still need to render as IE7 (until they support IE9)
            //OrionMinReqs still contains the same call because of incompatible resources that might trigger IE7 rendering when placed on any page supporting IE9
            ModulePatchHelper.InsertIECompatibilityHeader();
        }

        if (Request.RawUrl.Contains(CoreWebContent.UrlScanUrlFragment))
        {
            // Message: 'Rejected-By-UrlScan' was found in the URL.  The UrlScan tool from Microsoft may be intercepting requests for Orion resources.  Please check the configuration.  URL: {raw URL}
            log.WarnFormat(CoreWebContent.UrlScanInterceptionObservedMessage, CoreWebContent.UrlScanUrlFragment, Request.RawUrl);
        }

        ValidateXsrfToken();
    }

    protected void Application_AcquireRequestState(object sender, EventArgs e)
    {
        // Check user session validity against database
        var context = HttpContext.Current;
        if (context != null && context.Session != null)
        {
            var username = HttpContext.Current.User.Identity.Name;

            if (context.Request.Cookies.Get(FormsAuthentication.FormsCookieName) != null
                && !String.IsNullOrEmpty(username) 
                && !SessionManager.IsValidSession())
            {
                // Log out and redirect to login page in case the session is not valid
                LoggedInUser.LogOut(username);

                // Clear also session cookie 
                CookieHelper.ClearCookie("ASP.NET_SessionId", true);

                Response.Redirect("/Orion/Login.aspx?SuccessfulLogout=yes&ReturnUrl=" + HttpUtility.UrlEncode(context.Request.Url.PathAndQuery));
            }
        }
    }

    protected void Application_EndRequest(object sender, EventArgs e)
    {
        ////Check for 401 - Unauthorized error and redirect to forms gateway instead.
        //if (HttpContext.Current.Response.Status.Substring(0,3).Equals("401"))
        //{
        //    HttpContext.Current.Response.ClearContent();
        //    HttpContext.Current.Response.Redirect("~/Orion/Login.aspx?autologin=no");
        //    //HttpContext.Current.Response.Write("<script language='javascript'>self.location='Orion//Login.aspx?autologin=no';<//script>");
        //} 

        SolarWinds.Orion.Web.OrionMixedModeAuth.OnApplicationEndRequest(HttpContext.Current);

        var requestStartTime = Context.Items["RequestStartTime"] as DateTime?;
        var requestUrl = GetRequestUrl(Request);
        if (requestStartTime.HasValue)
        {
            var requestTime = DateTime.Now.Subtract(requestStartTime.Value);
            var requestTime_ms = requestTime.TotalMilliseconds;

            SolarWinds.Orion.Web.Instrumentation.WebsitePerformanceMonitor.ProcessEndRequest(requestUrl, requestTime);

            log.Debug($"EndRequest: {requestUrl} ({requestTime_ms}ms elapsed)");

            if (requestTime_ms >= GeneralSettings.Instance.RequestLoadingTimeWarningThreshold)
            {
                log.Warn($"Long request time: [url:{requestUrl}][time:{Math.Round(requestTime_ms)}]");
            }
        }
        else
        {
            log.Debug($"EndRequest: {requestUrl}");
        }
    }

    protected void Application_PostAuthorizeRequest()
    {
        if (HttpContext.Current.Request.FilePath.StartsWith("/api", StringComparison.InvariantCultureIgnoreCase)
            || HttpContext.Current.Request.FilePath.StartsWith("/sapi", StringComparison.InvariantCultureIgnoreCase))
        {
            HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }
    }

    public void FormsAuthentication_OnAuthenticate(object sender, FormsAuthenticationEventArgs args)
    {
        SolarWinds.Orion.Web.HttpModules.i18nRedirector.OnAuthenticate(sender, args);
        AllowAnonymousAspNetResources(args);
        /*
         * FB248420 - Unable to login to the website using IE 10 when "Enabled automatic login using Windows Authentication" is enabled
         * 
         * This is beacause the The HttpContext.User  is till having the Windows Principal even if we logged in using the Forms Authetication
         */
        if (FormsAuthentication.CookiesSupported && Request.Cookies[FormsAuthentication.FormsCookieName] != null)
        {
            try
            {
                var ticket = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                if (ticket != null)
                {
                    if (Context.User != null && (Context.User.Identity.Name != ticket.Name))
                    {
                        Context.User = new GenericPrincipal(new GenericIdentity(ticket.Name), new string[0]);
                    }

                }
                else
                {
                    log.Error("FormsAuthentication_OnAuthenticate: Invalid FormsAuthentication ticket.");
                }
            }
            catch (Exception ex)
            {
                /*
                 * Swalloing the exception as we dont want to pollute the log with this. Adding a machine key to web.config would help , 
                 * but at the moment since it doesn't cause any other usability issues we will leve it like this
                 * 
                 */
            }
        }

        else if (Context.User != null)
        {
            if (Request.Headers["Authorization"] == null)
            {
                Context.User = null;
            }
        }

    }
    /// <summary>
    /// This is a hack to prevent the Get request to Logout.aspx getting redirected to Login.aspx with return as Logout.aspx 
    /// and get logged in again automatically as a Windows principal User.
    /// It basically orginates from the way how we set up our Windows and Forms Authentication.
    /// </summary>
    private void RedirectToLoginWithNoAutoLogin()
    {

        if ((Request.Url.AbsolutePath.Contains("Login.aspx")) && (Request.QueryString["ReturnUrl"] != null)  && (Request.QueryString["ReturnUrl"].Contains("Logout.aspx")))
        {
            Response.Redirect("/Orion/Login.aspx?autologin=no");
        }
    }

    /// <summary>
    /// This is useful only when Anonymous auth is disabled in IIS but we still need to be able to access WebResource on login page
    /// </summary>
    private void AllowAnonymousAspNetResources(FormsAuthenticationEventArgs e)
    {
        if (Request.Url.AbsolutePath.Equals("/WebResource.axd", StringComparison.InvariantCultureIgnoreCase)
            || Request.Url.AbsolutePath.Equals("/ScriptResource.axd", StringComparison.InvariantCultureIgnoreCase))
        {
            e.Context.SkipAuthorization = true;
            e.User = new NullUser();
        }
    }

    private void ValidateXsrfToken()
    {
        if (GeneralSettings.Instance.EnableXsrfProtection && Request.HttpMethod != "GET" && Request.HttpMethod != "HEAD")
        {
            if(Request.Url.AbsolutePath.StartsWith("/api") ||
                Request.Url.AbsolutePath.StartsWith("/sapi") ||
                (Request.Url.AbsolutePath.Contains(".asmx") && !Request.Url.AbsolutePath.StartsWith("/orion/services/tabsmanager.asmx/GetTabs", StringComparison.InvariantCultureIgnoreCase)) )
            {
                var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (cookie != null)
                {
                    var ticket = FormsAuthentication.Decrypt(cookie.Value);
                    if (ticket != null)
                    {
                        var token = ticket.UserData;
                        log.DebugFormat("XSRF token value is {0}", token);
                        var header = Request.Headers["X-XSRF-TOKEN"];
                        if(header != token)
                        {
                            log.ErrorFormat("XSRF token in header does not match ({0}).", header);
                            Response.Clear();
                            Response.StatusCode = 400;
                            Response.End();
                        }
                        else
                        {
                            log.Debug("XSRF token match.");
                        }
                    }
                    else
                    {
                        log.ErrorFormat("Auth cookie is not valid for request {0}", Request.Url);
                    }
                }
                else
                {
                    log.ErrorFormat("Missing auth cookie for request {0}", Request.Url);
                }
            }
        }
    }

    private static string GetRequestUrl(HttpRequest request)
    {
        return request.Url.ToString().Replace("\n", "");
    }

</script>
