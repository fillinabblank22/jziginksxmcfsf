<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        // In case the user is authenticated using client certificate,
        // the IIS provides a identity already for the first request, but we have to redirect to login page,
        // because there is a lot of logic required for Orion to be working correctly
        if (Context.User?.Identity != null 
            && Context.Request.Cookies.Get(FormsAuthentication.FormsCookieName) == null)
        {
            Response.Redirect(string.Format("~/Orion/Login.aspx{0}",
                (Request.QueryString["IsMobileView"] != null) ? string.Format("?IsMobileView={0}", Request.QueryString["IsMobileView"]) : string.Empty));
        }
        else
        {
            // sync changes to login.aspx.cs::DoRedirect
            Response.Redirect(string.Format("~/Orion/default.aspx{0}",
                (Request.QueryString["IsMobileView"] != null) ? string.Format("?IsMobileView={0}", Request.QueryString["IsMobileView"]) : string.Empty));
        }
        base.OnLoad(e);
    }
</script>
