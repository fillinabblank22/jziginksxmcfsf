﻿using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Help_Default : System.Web.UI.Page
{
    private string serverName = SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer;
    private const string url = "/documentation2/api/url";
    private const string fallBackUrl = "https://support.solarwinds.com/success_center";
    protected void Page_Load(object sender, EventArgs e)
    {
        var topic = this.Request["topic"] ?? string.Empty;
        var module = this.Request["module"] ?? string.Empty;
        var language = this.Request["lang"] ?? "en";
        if (string.IsNullOrWhiteSpace(topic))
            Response.Redirect(fallBackUrl);

        var restClient = new RestClient();
        var respData = restClient.GetAsync(string.Concat(serverName, url), string.Concat("?module=", module, "&topic=", topic, "&language=", language));       

        try
        {
            var helpData = respData.Result;
            if(helpData == null) Response.Redirect(fallBackUrl);

            var currentVersion = GetModuleVersion(helpData.ProductName);
            var awailableVersion = GetAwailableVersion(helpData, currentVersion);
            Response.Redirect(string.Concat(serverName, helpData.Url.Replace("{{version}}", awailableVersion)));

        }
        catch (ThreadAbortException)
        {
            throw;
        }
        catch (Exception ex)
        {
            Response.Redirect(fallBackUrl);
        }
    }

    private string GetAwailableVersion(HelpData helpData, Version currentVersion)
    {
        var orderedVersion = helpData.Versions.OrderBy(v => new Version(v)).ToArray();
        for(var i = helpData.Versions.Length - 1; i >= 0;  i--)
        {
            if (new Version(orderedVersion[i]).CompareTo(currentVersion) <= 0)
                return orderedVersion[i];
        }
        return orderedVersion[orderedVersion.Length - 1];
    }

    private Version GetModuleVersion(string moduleName)
    {
        var modules = OrionModuleManager.GetInstalledModules();
        foreach(var module in modules)
        {
            if(string.Compare(module.ProductShortName, moduleName, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                return new Version(module.Version);
            }
        }
        return null;
    }

    private class RestClient {
        public async Task<HelpData> GetAsync(string url, string urlParameters) {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(2);
            client.BaseAddress = new Uri(url);            
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));            
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<HelpData>();
            }
            return null;
        } 
    }

    public class HelpData
    {
        public string Url { get; set; }
        public string ProductName { get; set; }
        public string[] Versions { get; set; }
    }
}