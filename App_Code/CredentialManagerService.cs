using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Actions.Utility;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class CredentialManagerService : System.Web.Services.WebService
{
    private const string CORE_CREDENTIAL_OWNER = CoreConstants.CoreCredentialOwner;
    private CredentialManager cMan = new CredentialManager();
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    protected const string CredentialsPasswordReplacement = "**********";

    public class WindowsCredential
    {
        public WindowsCredential()
        { }

        public WindowsCredential(UsernamePasswordCredential cred)
        {
            if (cred == null)
                throw new ArgumentNullException("cred");

            if (cred.ID.HasValue)
            {
                this.ID = cred.ID.Value;
            }

            this.Name = cred.Name;
            this.Username = cred.Username;
            this.Password = CredentialsPasswordReplacement;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    [WebMethod]
    public PageableDataTable GetCredentials()
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            int pageSize;
            int startRowNumber;

            string sortColumn = this.Context.Request.QueryString["sort"];
            string sortDirection = this.Context.Request.QueryString["dir"];

            Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
            Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

            if (pageSize < 10)
            {
                pageSize = 10;
            }

            DataTable table = new DataTable();
            table.Columns.Add("CredentialID", typeof(string));
            table.Columns.Add("CredentialName", typeof(string));
            table.Columns.Add("Username", typeof(string));
            table.Columns.Add("NodesAssigned", typeof(string));
            table.Columns.Add("ActionsAssigned", typeof(string));
            table.Columns.Add("Control", typeof(string));
            table.Columns.Add("IsBroken", typeof(bool));

            var upCredentials = cMan.GetCredentials<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, true);

            if (upCredentials.Count() > 0)
            {
                List<UsernamePasswordCredential> sortedCredentials = SortCredentials(upCredentials, sortColumn, sortDirection);
                var webDal = new WebDAL();
                Dictionary<int, int> nodeCountByCredentialId = webDal.GetNodesAssignedCountsByTypeCredentialIds(new List<string> { "WMICredential" });
                Dictionary<int, int> actionCountByCredentialId = webDal.GetActionsCountAssignedToCredentials();

                for (int i = startRowNumber; i < startRowNumber + pageSize; i++)
                {
                    if (i >= sortedCredentials.Count)
                        break;

                    var r = table.NewRow();
                    r["CredentialID"] = sortedCredentials[i].ID.Value;
                    r["CredentialName"] = sortedCredentials[i].Name;
                    r["Username"] = sortedCredentials[i].Username;
                    r["IsBroken"] = sortedCredentials[i].IsBroken;

                    if (nodeCountByCredentialId.ContainsKey(sortedCredentials[i].ID.Value))
                    {
                        r["NodesAssigned"] = nodeCountByCredentialId[sortedCredentials[i].ID.Value];
                    }
                    else
                    {
                        r["NodesAssigned"] = 0;
                    }

                    if (actionCountByCredentialId.ContainsKey(sortedCredentials[i].ID.Value))
                    {
                        r["ActionsAssigned"] = actionCountByCredentialId[sortedCredentials[i].ID.Value];
                    }
                    else
                    {
                        r["ActionsAssigned"] = 0;
                    }

                    r["Control"] = "UsernamePasswordCredential";

                    table.Rows.Add(r);
                }
            }

            return new PageableDataTable(table, upCredentials.Count());
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting credential table", ex);
            throw;
        }
    }


    [WebMethod]
    public List<object> GetCredentialsForDropDown()
    {
        AuthorizationChecker.AllowNodeManagement();

        var credList = new List<object>();

        try
        {
            var upCredentials = cMan.GetCredentials<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, true);

            foreach (var cred in upCredentials)
            {
                credList.Add(new { id = cred.ID, name = cred.Name });
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting credential table", ex);
            throw;
        }

        credList.Add(new { id = "-1", name = Resources.CoreWebContent.Add_New_Creds });
        credList.Add(new { id = "-2", name = Resources.CoreWebContent.Refresh_List });

        return credList;
    }

    [WebMethod]
    public PageableDataTable GetSNMPCredentials()
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            int pageSize;
            int startRowNumber;

            string sortColumn = this.Context.Request.QueryString["sort"];
            string sortDirection = this.Context.Request.QueryString["dir"];

            Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
            Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

            if (pageSize < 10)
            {
                pageSize = 10;
            }

            DataTable table = new DataTable();
            table.Columns.Add("CredentialID", typeof(string));
            table.Columns.Add("CredentialName", typeof(string));
            table.Columns.Add("Username", typeof(string));
            table.Columns.Add("NodesAssigned", typeof(string));
            table.Columns.Add("IsBroken", typeof(bool));

            var credentials = cMan.GetCredentials<SnmpCredentialsV3>();

            if (credentials == null || !credentials.Any())
                return new PageableDataTable(table, 0);

            Dictionary<int, int> nodeCountByCredentialId = new WebDAL().GetNodesAssignedCountsByTypeCredentialIds(new List<string> { "ROSNMPCredentialID", "RWSNMPCredentialID" });

            foreach (var cred in credentials)
            {
                var r = table.NewRow();

                r["CredentialID"] = cred.ID;
                r["CredentialName"] = cred.Name;
                r["Username"] = cred.UserName;
                r["IsBroken"] = cred.IsBroken;
                r["NodesAssigned"] = nodeCountByCredentialId.ContainsKey((int)cred.ID)
                    ? nodeCountByCredentialId[(int)cred.ID]
                    : 0;

                table.Rows.Add(r);
            }
            return new PageableDataTable(SortAndTakeSNMPCredentials(table, sortColumn, sortDirection, pageSize, startRowNumber), credentials.Count());
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting credential table", ex);
            throw;
        }
    }

    [WebMethod]
    public void DeleteSNMPCredentials(List<string> credentialIDs)
    {
        AuthorizationChecker.AllowNodeManagement();

        if (credentialIDs == null && credentialIDs.Count == 0)
            return;

        var nodeCountByCredentialId =
            new WebDAL().GetNodesAssignedCountsByTypeCredentialIds(new List<string> { "ROSNMPCredentialID", "RWSNMPCredentialID" });

        foreach (string id in credentialIDs)
        {
            try
            {
                var idNumber = Convert.ToInt32(id);
                if (nodeCountByCredentialId.ContainsKey(idNumber) && nodeCountByCredentialId[idNumber] > 0)
                {
                    log.ErrorFormat("SNMPv3 crdential {0} will not be deleted because there are {1} nodes using it", id, nodeCountByCredentialId[idNumber]);
                    continue;
                }
                cMan.DeleteCredential(CORE_CREDENTIAL_OWNER, int.Parse(id));
            }
            catch (Exception ex)
            {
                log.Error(String.Format("Exception occured when deleting SNMPv3 credential {0}", id), ex);
                throw;
            }
        }
    }

    private DataTable SortAndTakeSNMPCredentials(DataTable credSource, string sortColumn, string direction, int limit, int startRowNumber)
    {
        DataView dv = credSource.DefaultView;
        dv.Sort = sortColumn + " " + direction;
        DataTable sortedDT = dv.ToTable().Rows.Cast<DataRow>().Skip(startRowNumber).Take(limit).CopyToDataTable();

        return sortedDT;
    }

    private List<UsernamePasswordCredential> SortCredentials(IEnumerable<UsernamePasswordCredential> credSource, string sortColumn, string direction)
    {
        bool asc = direction == "ASC";

        if (sortColumn == "CredentialName")
        {
            if (asc)
                return credSource.OrderBy((c) => c.Name).ToList();
            else
                return credSource.OrderByDescending((c) => c.Name).ToList();
        }
        else if (sortColumn == "Username")
        {
            if (asc)
                return credSource.OrderBy((c) => c.Username).ToList();
            else
                return credSource.OrderByDescending((c) => c.Username).ToList();
        }
        else if (sortColumn == "NodesAssigned")
        {
            //if (asc)
            //    return credSource.OrderBy((c) => c.).ToList();
            //else
            //    return credSource.OrderByDescending((c) => c.Username).ToList();
            return credSource.ToList();
        }
        else
        {
            return credSource.OrderBy((c) => c.Name).ToList();
        }
    }

    public List<WindowsCredential> GetWindowsCredentials()
    {
        var creds = cMan.GetCredentials<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, true).Select(n => new WindowsCredential(n));

        return creds.ToList();
    }

    [WebMethod]
    public WindowsCredential GetWindowsCredential(string credentialId)
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            if (String.IsNullOrEmpty(credentialId))
                return null;

            var cred = this.cMan.GetCredential<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, Convert.ToInt32(credentialId), true);

            if (cred == null)
                return null;

            return new WindowsCredential(cred);
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when getting windows credential {0}", credentialId), ex);
            throw;
        }
    }

    [WebMethod]
    public SnmpCredentialsV3 GetSNMPCredential(string credentialId)
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            if (string.IsNullOrEmpty(credentialId))
                return null;

            var cred = this.cMan.GetCredential<SnmpCredentialsV3>(CORE_CREDENTIAL_OWNER, Convert.ToInt32(credentialId), true);

            cred.AuthenticationPassword = CredentialsPasswordReplacement;
            cred.PrivacyPassword = CredentialsPasswordReplacement;

            if (cred == null)
                return null;

            return cred;
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when getting windows credential {0}", credentialId), ex);
            throw;
        }
    }

    [WebMethod]
    public void DeleteCredentials(List<string> credentialIDs)
    {
        AuthorizationChecker.AllowNodeManagement();

        if (credentialIDs == null && credentialIDs.Count == 0)
            return;

        string currentId = null;

        try
        {
            Dictionary<int, int> nodeCountByCredentialId = new WebDAL().GetNodesAssignedCountsByTypeCredentialIds(new List<string> { "WMICredential" });

            foreach (string id in credentialIDs)
            {
                currentId = id;

                int idNumber = Convert.ToInt32(id);

                if (nodeCountByCredentialId.ContainsKey(idNumber) && nodeCountByCredentialId[idNumber] > 0)
                {
                    log.ErrorFormat("Crdential {0} will not be deleted because there are {1} node using it", id, nodeCountByCredentialId[idNumber]);
                    continue;
                }

                this.cMan.DeleteCredential(CORE_CREDENTIAL_OWNER, idNumber);
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when deleting windows credential {0}", currentId), ex);
            throw;
        }
    }

    [WebMethod]
    public int AddUpdateWindowsCredential(UsernamePasswordCredential credentials)
    {
        AuthorizationChecker.AllowNodeManagement();
        return ActionCredentialHelper.InsertUpdateUsernamePasswordCredential(credentials);
    }

    [WebMethod]
    public bool InsertUpdateSNMPCredential(string credentialId, string name, string username, string context,
        string authPassword, string encPassword, string authenticationType, string privacyType, bool authPassAsKey, bool encPassAsKey)
    {
        AuthorizationChecker.AllowNodeManagement();
        if (String.IsNullOrEmpty(name))
            return false;

        if (String.IsNullOrEmpty(username))
            return false;

        SnmpCredentialsV3 cred = null;

        if (String.IsNullOrEmpty(credentialId))
        {
            //check if such exists
            List<SnmpCredentialsV3> creds = new List<SnmpCredentialsV3>();
            creds.AddRange(this.cMan.GetCredentials<SnmpCredentialsV3>(CORE_CREDENTIAL_OWNER, true));

            if (creds.Any((c) => c.Name == name))
                return false; //exists - cannot add 

            // not exists - proceed with insert
            cred = new SnmpCredentialsV3()
            {
                Name = name,
                Context = context,
                UserName = username
            };

            UpdateSNMPCred(cred, authPassword, encPassword, authenticationType, privacyType, authPassAsKey, encPassAsKey);

            this.cMan.AddCredential<SnmpCredentialsV3>(CORE_CREDENTIAL_OWNER, cred);
            return true;
        }
        else
        {
            cred = this.cMan.GetCredential<SnmpCredentialsV3>(CORE_CREDENTIAL_OWNER, Convert.ToInt32(credentialId), true);
            SnmpCredentialsV3 oldCred = new SnmpCredentialsV3(cred); // storing old one for audit

            if (cred != null)
            {
                cred.UserName = username;
                cred.Context = context;

                UpdateSNMPCred(cred, authPassword, encPassword, authenticationType, privacyType, authPassAsKey, encPassAsKey);

                this.cMan.UpdateCredential<SnmpCredentialsV3>(CORE_CREDENTIAL_OWNER, cred);

                //now need to audit the changes
                //getting list of affected nodes
                DataTable details = new WebDAL().GetNumberOfNodesUsingCredentialsDetails(credentialId, new List<string> { "ROSNMPCredentialID", "RWSNMPCredentialID" });

                if (details != null && details.Rows.Count > 0)
                {
                    foreach (DataRow row in details.Rows)
                    {
                        //auditing
                        //N.NodeID, N.Caption, N.Uri, S.SettingName, N.IP_Address, N.Status
                        PublishSnmpIndication(oldCred, cred, row["NodeID"].ToString(), row["IP_Address"].ToString(), row["Caption"].ToString(), row["Status"].ToString(), row["Uri"].ToString(), row["SettingName"].ToString());
                        PublishSnmpV3Indication(oldCred, cred, row["NodeID"].ToString(), $@"{row["Uri"].ToString()}/SNMPv3Credentials", row["SettingName"].ToString());
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private void PublishSnmpIndication(SnmpCredentialsV3 oldCred, SnmpCredentialsV3 newCred, string nodeID, string ip, string caption, string status, string uri, string type)
    {
        var originalNodeProperties = CreateNodePropertyBag(nodeID, ip, caption, status, uri);
        var currentNodeProperties = CreateNodePropertyBag(nodeID, ip, caption, status, uri);

        switch (type)
        {
            case "ROSNMPCredentialID":
                originalNodeProperties.Add("SNMPV3UserName", oldCred.UserName);
                originalNodeProperties.Add("SNMPV3AuthPassword", oldCred.AuthenticationPassword);
                originalNodeProperties.Add("SNMPV3PrivacyPassword", oldCred.PrivacyPassword);
                originalNodeProperties.Add("SNMPV3AuthKeyIsPwd", oldCred.AuthenticationKeyIsPassword);
                originalNodeProperties.Add("SNMPV3PrivKeyIsPwd", oldCred.PrivacyKeyIsPassword);
                originalNodeProperties.Add("SNMPv3PrivacyType", oldCred.PrivacyType.ToString());
                originalNodeProperties.Add("SNMPv3AuthType", oldCred.AuthenticationType.ToString());
                originalNodeProperties.Add("SNMPv3Context", oldCred.Context);
                //-------------------------------
                currentNodeProperties.Add("SNMPV3UserName", newCred.UserName);
                currentNodeProperties.Add("SNMPV3AuthPassword", newCred.AuthenticationPassword);
                currentNodeProperties.Add("SNMPV3PrivacyPassword", newCred.PrivacyPassword);
                currentNodeProperties.Add("SNMPV3AuthKeyIsPwd", newCred.AuthenticationKeyIsPassword);
                currentNodeProperties.Add("SNMPV3PrivKeyIsPwd", newCred.PrivacyKeyIsPassword);
                currentNodeProperties.Add("SNMPv3PrivacyType", newCred.PrivacyType.ToString());
                currentNodeProperties.Add("SNMPv3AuthType", newCred.AuthenticationType.ToString());
                currentNodeProperties.Add("SNMPv3Context", newCred.Context);
                if (newCred.ID.HasValue)
                    currentNodeProperties.Add("SNMPv3CredentialID", newCred.ID.Value);

                break;
            case "RWSNMPCredentialID":
                originalNodeProperties.Add("RWSNMPV3UserName", oldCred.UserName);
                originalNodeProperties.Add("RWSNMPV3AuthPassword", oldCred.AuthenticationPassword);
                originalNodeProperties.Add("RWSNMPV3PrivacyPassword", oldCred.PrivacyPassword);
                originalNodeProperties.Add("RWSNMPV3AuthKeyIsPwd", oldCred.AuthenticationKeyIsPassword);
                originalNodeProperties.Add("RWSNMPV3PrivKeyIsPwd", oldCred.PrivacyKeyIsPassword);
                originalNodeProperties.Add("RWSNMPv3PrivacyType", oldCred.PrivacyType.ToString());
                originalNodeProperties.Add("RWSNMPv3AuthType", oldCred.AuthenticationType.ToString());
                originalNodeProperties.Add("RWSNMPv3Context", oldCred.Context);
                //--------------------------------------
                currentNodeProperties.Add("RWSNMPV3UserName", newCred.UserName);
                currentNodeProperties.Add("RWSNMPV3AuthPassword", newCred.AuthenticationPassword);
                currentNodeProperties.Add("RWSNMPV3PrivacyPassword", newCred.PrivacyPassword);
                currentNodeProperties.Add("RWSNMPV3AuthKeyIsPwd", newCred.AuthenticationKeyIsPassword);
                currentNodeProperties.Add("RWSNMPV3PrivKeyIsPwd", newCred.PrivacyKeyIsPassword);
                currentNodeProperties.Add("RWSNMPv3PrivacyType", newCred.PrivacyType.ToString());
                currentNodeProperties.Add("RWSNMPv3AuthType", newCred.AuthenticationType.ToString());
                currentNodeProperties.Add("RWSNMPv3Context", newCred.Context);
                if (newCred.ID.HasValue)
                    currentNodeProperties.Add("RWSNMPv3CredentialID", newCred.ID.Value);
                break;
        }

        PublishIndication(currentNodeProperties, originalNodeProperties);
    }

    private void PublishSnmpV3Indication(SnmpCredentialsV3 oldCred, SnmpCredentialsV3 newCred, string nodeID, string uri, string type)
    {
        var originalProps = new PropertyBag();
        var currentProps = new PropertyBag();

        switch (type)
        {
            case "ROSNMPCredentialID":
                originalProps.Add("Username", oldCred.UserName);
                originalProps.Add("AuthenticationKey", oldCred.AuthenticationPassword);
                originalProps.Add("PrivacyKey", oldCred.PrivacyPassword);
                originalProps.Add("AuthenticationKeyIsPassword", oldCred.AuthenticationKeyIsPassword);
                originalProps.Add("PrivacyKeyIsPassword", oldCred.PrivacyKeyIsPassword);
                originalProps.Add("PrivacyMethod", oldCred.PrivacyType.ToString());
                originalProps.Add("AuthenticationMethod", oldCred.AuthenticationType.ToString());
                originalProps.Add("Context", oldCred.Context);
                //-------------------------------
                currentProps.Add("Username", newCred.UserName);
                currentProps.Add("AuthenticationKey", newCred.AuthenticationPassword);
                currentProps.Add("PrivacyKey", newCred.PrivacyPassword);
                currentProps.Add("AuthenticationKeyIsPassword", newCred.AuthenticationKeyIsPassword);
                currentProps.Add("PrivacyKeyIsPassword", newCred.PrivacyKeyIsPassword);
                currentProps.Add("PrivacyMethod", newCred.PrivacyType.ToString());
                currentProps.Add("AuthenticationMethod", newCred.AuthenticationType.ToString());
                currentProps.Add("Context", newCred.Context);

                break;
            case "RWSNMPCredentialID":
                originalProps.Add("RWUsername", oldCred.UserName);
                originalProps.Add("RWAuthenticationKey", oldCred.AuthenticationPassword);
                originalProps.Add("RWPrivacyKey", oldCred.PrivacyPassword);
                originalProps.Add("RWAuthenticationKeyIsPassword", oldCred.AuthenticationKeyIsPassword);
                originalProps.Add("RWPrivacyKeyIsPassword", oldCred.PrivacyKeyIsPassword);
                originalProps.Add("RWPrivacyMethod", oldCred.PrivacyType.ToString());
                originalProps.Add("RWAuthenticationMethod", oldCred.AuthenticationType.ToString());
                originalProps.Add("RWContext", oldCred.Context);
                //-------------------------------
                currentProps.Add("RWUsername", newCred.UserName);
                currentProps.Add("RWAuthenticationKey", newCred.AuthenticationPassword);
                currentProps.Add("RWPrivacyKey", newCred.PrivacyPassword);
                currentProps.Add("RWAuthenticationKeyIsPassword", newCred.AuthenticationKeyIsPassword);
                currentProps.Add("RWPrivacyKeyIsPassword", newCred.PrivacyKeyIsPassword);
                currentProps.Add("RWPrivacyMethod", newCred.PrivacyType.ToString());
                currentProps.Add("RWAuthenticationMethod", newCred.AuthenticationType.ToString());
                currentProps.Add("RWContext", newCred.Context);
                break;
        }

        var indication = new SnmpV3Indication(IndicationType.System_InstanceModified, nodeID, uri, currentProps, originalProps);


        var publisher = IndicationPublisher.CreateV3();
        if (publisher != null)
        {
            publisher.ReportIndication(indication);
        }
    }

    private static PropertyBag CreateNodePropertyBag(string nodeID, string ip, string caption, string status, string uri)
    {
        return new PropertyBag
        {
            {"NodeId", nodeID},
            {"IP_Address", ip},
            {"Caption", caption},
            {"Status", status},
            {"Uri", uri}
        };
    }

    private void PublishWindowsCredentialIndication(UsernamePasswordCredential oldCred, UsernamePasswordCredential newCred, string nodeID, string ip,
        string caption, string status, string uri)
    {
        var originalNodeProperties = CreateNodePropertyBag(nodeID, ip, caption, status, uri);
        var currentNodeProperties = CreateNodePropertyBag(nodeID, ip, caption, status, uri);

        originalNodeProperties.Add("WMIUsername", oldCred.Username);
        originalNodeProperties.Add("WMIPassword", oldCred.Password);
        //--------------------------------------
        currentNodeProperties.Add("WMIUsername", newCred.Username);
        currentNodeProperties.Add("WMIPassword", newCred.Password);
        if (newCred.ID.HasValue)
            currentNodeProperties.Add("WMICredential", newCred.ID.Value);

        PublishIndication(currentNodeProperties, originalNodeProperties);
    }

    private void PublishIndication(PropertyBag currentNodeProperties, PropertyBag originalNodeProperties)
    {
        var indication = new NodeIndication(IndicationType.System_InstanceModified, currentNodeProperties,
            originalNodeProperties);


        var publisher = IndicationPublisher.CreateV3();
        if (publisher != null)
        {
            publisher.ReportIndication(indication);
        }
    }

    private void UpdateSNMPCred(SnmpCredentialsV3 cred, string authPassword, string encPassword, string authenticationType, string privacyType, bool authPassAsKey, bool encPassAsKey)
    {
        var auth = (SNMPv3AuthType)Enum.Parse(typeof(SNMPv3AuthType), authenticationType);
        cred.AuthenticationType = auth;

        if (auth != SNMPv3AuthType.None)
        {
            cred.AuthenticationKeyIsPassword = !authPassAsKey;
            cred.AuthenticationPassword = authPassword == CredentialsPasswordReplacement ? cred.AuthenticationPassword : authPassword;
        }
        else
        {
            cred.AuthenticationKeyIsPassword = true;
            cred.AuthenticationPassword = string.Empty;
        }

        var priv = (SNMPv3PrivacyType)Enum.Parse(typeof(SNMPv3PrivacyType), privacyType);
        cred.PrivacyType = priv;

        if (priv != SNMPv3PrivacyType.None)
        {
            cred.PrivacyKeyIsPassword = !encPassAsKey;
            cred.PrivacyPassword = encPassword == CredentialsPasswordReplacement ? cred.PrivacyPassword : encPassword;
        }
        else
        {
            cred.PrivacyType = SNMPv3PrivacyType.None;
            cred.PrivacyKeyIsPassword = true;
            cred.PrivacyPassword = string.Empty;
        }
    }

    [WebMethod]
    public bool InsertUpdateWindowsCredential(string credentialId, string name, string username, string password)
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            if (String.IsNullOrEmpty(name))
                return false;

            if (String.IsNullOrEmpty(username))
                return false;

            if (String.IsNullOrEmpty(credentialId))
            {
                List<UsernamePasswordCredential> creds = new List<UsernamePasswordCredential>();

                try
                {
                    creds.AddRange(this.cMan.GetCredentials<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, true));

                    if (creds.Any((c) => c.Name == name))
                        return false;
                }
                catch (CredentialNotFoundException)
                {
                    // no credentials found (why the exception ???), however, do nothing, proceed to insert
                }

                this.cMan.AddCredential<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, new UsernamePasswordCredential()
                {
                    Description = null,
                    Name = name,
                    Password = password,
                    Username = username
                });

                return true;
            }
            else
            {
                var cred = this.cMan.GetCredential<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, Convert.ToInt32(credentialId), true);

                if (cred != null)
                {
                    UsernamePasswordCredential oldCred = new UsernamePasswordCredential(cred.Username, cred.Password); // storing old one for audit
                    oldCred.ID = cred.ID;
                    cred.Username = username;
                    cred.Password = password == CredentialsPasswordReplacement ? oldCred.Password : password;

                    this.cMan.UpdateCredential<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, cred);

                    List<int> profilesUsingUpdatedCredential;
                    using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                    {
                        profilesUsingUpdatedCredential = businessProxy.GetProfileIDsUsingCredentials(new List<int>() { cred.ID.Value });
                    }

                    if (profilesUsingUpdatedCredential.Count > 0)
                    {
                        var rescheduler = new DiscoveryJobRescheduler();
                        rescheduler.RescheduleJobsForProfiles(profilesUsingUpdatedCredential);
                    }

                    //now need to audit the changes
                    //getting list of affected nodes
                    DataTable details = new WebDAL().GetNumberOfNodesUsingCredentialsDetails(credentialId,
                        new List<string> { "WMICredential" });

                    if (details != null && details.Rows.Count > 0)
                    {
                        foreach (DataRow row in details.Rows)
                        {
                            PublishWindowsCredentialIndication(oldCred, cred, row["NodeID"].ToString(), row["IP_Address"].ToString(),
                                row["Caption"].ToString(), row["Status"].ToString(), row["Uri"].ToString());
                        }
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when inserting/updating windows credential {0}", name), ex);
            throw;
        }
    }

    [WebMethod]
    public int InsertNewCredential(string credentialId, string name, string username, string password)
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {   
            if (String.IsNullOrEmpty(credentialId))
            {
                if (this.cMan
                    .GetCredentialNames<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER)
                    .Values.Contains(name))
                {
                    return -1;
                }

                var newCred = new UsernamePasswordCredential()
                {
                    Description = null,
                    Name = name,
                    Password = password,
                    Username = username
                };

                this.cMan.AddCredential<UsernamePasswordCredential>(CORE_CREDENTIAL_OWNER, newCred);

                return newCred.ID ?? -1;
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when inserting/updating windows credential {0}", name), ex);
            throw;
        }
        return -1;
    }

    [WebMethod]
    public int InsertUpdateApiCredentails(string token, string name, string credentialId = null)
    {
        AuthorizationChecker.AllowNodeManagement();
        try
        {
            if (String.IsNullOrEmpty(credentialId))
            {
                var newCred = new ApiKeyCredential()
                {
                    Name = name,
                    ApiKey = token
                };

                this.cMan.AddCredential<ApiKeyCredential>(CORE_CREDENTIAL_OWNER, newCred);

                return newCred.ID ?? -1;
            }
            else
            {
                var credToUpdate = this.cMan.GetCredential<ApiKeyCredential>(Int32.Parse(credentialId));
                credToUpdate.ApiKey = token;
                this.cMan.UpdateCredential<ApiKeyCredential>(CORE_CREDENTIAL_OWNER, credToUpdate);
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when inserting/updating api credential {0}", credentialId), ex);
            throw;
        }
        return -1;
    }

    [WebMethod]
    public ApiKeyCredential GetApiCredentails(string credentialId)
    {
        AuthorizationChecker.AllowNodeManagement();

        if (String.IsNullOrEmpty(credentialId))
            return null;

        return this.cMan.GetCredential<ApiKeyCredential>(Int32.Parse(credentialId));
    }

    [WebMethod]
    public int GetNumberOfDiscoveryProfilesUsingCredentials(string credentialId)
    {
        AuthorizationChecker.AllowNodeManagement();

        int credlId = 0;

        if (!Int32.TryParse(credentialId, out credlId))
        {
            return 0;
        }

        return GetNumberOfDiscoveryProfilesUsingCredentialsList(new[] { credentialId });
    }

    [WebMethod]
    public int GetNumberOfDiscoveryProfilesUsingCredentialsList(string[] credentialIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        if (credentialIds == null || credentialIds.Length == 0)
            return 0;

        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            return businessProxy.GetProfileIDsUsingCredentials(credentialIds.Select(int.Parse).ToList()).Count();
        }
    }

    [WebMethod]
    public int[] GetNumberOfNodesUsingCredentials(string credentialId, List<string> types)
    {
        AuthorizationChecker.AllowNodeManagement();

        int[] result = new[] { 0, 0 };

        if (string.IsNullOrEmpty(credentialId))
            return result;

        int credlId = 0;

        if (!Int32.TryParse(credentialId, out credlId))
        {
            return result;
        }

        result[0] = new WebDAL().GetNumberOfNodesUsingCredentials(credentialId, types);

        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            result[1] = businessProxy.GetProfileIDsUsingCredentials(new List<int>() { credlId }).Count();
        }

        return result;
    }
    [WebMethod]
    public int[] GetNumberOfEntitiesUsingCredentials(string credentialId, List<string> types)
    {
        AuthorizationChecker.AllowNodeManagement();

        int[] result = new[] { 0, 0, 0 };

        if (string.IsNullOrEmpty(credentialId))
            return result;

        int credlId = 0;

        if (!Int32.TryParse(credentialId, out credlId))
        {
            return result;
        }
		
		var webDal = new WebDAL();
        result[0] = webDal.GetNumberOfNodesUsingCredentials(credentialId, types);
		var actionsCounts = webDal.GetActionsCountAssignedToCredentials();
        result[1] = actionsCounts.ContainsKey(credlId) ? actionsCounts[credlId] : 0;

        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            result[2] = businessProxy.GetProfileIDsUsingCredentials(new List<int>() { credlId }).Count();
        }

        return result;
    }
}


