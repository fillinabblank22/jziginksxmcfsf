﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;

/// <summary>
/// Used as helper class for functions from SamlAccounts.aspx.cs and OrionAccounts.aspx.cs
/// </summary>
public static class AccountManagement
{
    /// <summary>
    /// Called when the user is created to set the default view rights
    /// </summary>
    /// <param name="thisProfile"></param>
    /// <param name="accountID"></param>
    public static void FillDefaultViewsForAccount(ProfileCommon thisProfile)
    {
        thisProfile.AllowEventClear = true; // should be on by default for all accounts - TT Case #2197
        // should be true by default for all accounts
        thisProfile.AllowOrionMapsManagement = true;
        thisProfile.AllowManageDashboards = true;

        foreach (string viewType in ViewManager.GetViewTypes())
        {
            string propName = ViewManager.GetUserPropertyNameByViewType(viewType);

            if (viewType.Equals("Summary") && ViewManager.GetDefaultSummaryViewId() != null)
            {
                thisProfile.SetPropertyValue(propName, ViewManager.GetDefaultSummaryViewId());
            }
            else
            {
                foreach (ViewInfo view in ViewManager.GetViewsByType(viewType))
                {
                    if (view.IsStaticSubview)
                        continue;
                    thisProfile.SetPropertyValue(propName, view.ViewID);
                    break;
                }
            }

            // TT #7375 Node Details view = By Device Type as default for new accounts
            if (propName == "NetPerfMon.NodeDetailsViewID")
                thisProfile.SetPropertyValue(propName, -1);
        }

        //var reportsFolderDir = ReportHelper.GetReportsRootFolderDir();
        //if (reportsFolderDir.Exists)

        thisProfile.SetPropertyValue("ReportFolder", CoreConstants.ReportNoReports);
        thisProfile.Save();

        //Assign menubars for new created user
        ITabManager manager = new TabManager();
        IList<Tab> tabs = manager.GetAllTabs();
        foreach (var tab in tabs)
        {
            if (IsHomeTab(tab.TabName))
            {
                //Assign Default menubar to Home tab for new created user
                manager.AssignBarToTab(thisProfile.UserName, tab.TabId, thisProfile.MenuName);
            }
            else
            {
                //Assign menubars for other modules
                string menuName = manager.GetCurrentBarName(thisProfile.UserName, tab.TabId);
                //assign the menu of the current user
                //if no current user then get default
                if (string.IsNullOrEmpty(menuName))
                {
                    menuName = manager.GetDefaultBarForTab(tab.TabName);
                }

                //Assign the menubar value
                if (!string.IsNullOrEmpty(menuName))
                {
                    manager.AssignBarToTab(thisProfile.UserName, tab.TabId, menuName);
                }
            }
        }
    }
    
    private static bool IsHomeTab(string tabName)
    {
        return tabName == "Home" || OrionModuleManager.GetCustomizations().Any(c => c.Action.Equals(Customization.CustomHomeTab) && c.Key.Equals(tabName));
    }
}
