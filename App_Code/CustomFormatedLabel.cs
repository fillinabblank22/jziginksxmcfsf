﻿using System;
using System.Web.UI;
using System.Drawing;

namespace SolarWinds.Orion.Web.Controls
{
    public class CustomFormatedLabel : System.Web.UI.WebControls.Label
    {
        #region Properties
        //private int warningLevel;

        [PersistenceMode(PersistenceMode.Attribute)]
        public int WarningLevel
        {
            get
            {
                object val = this.ViewState["warningLevel"];
                if (val != null && val is int)
                {
                    return (int)val;
                }
                return 0;
            }
            set
            {
                this.ViewState["warningLevel"] = value;
            }
        }

        //private int errorLevel;

        [PersistenceMode(PersistenceMode.Attribute)]
        public int ErrorLevel
        {
            get
            {
                object val = this.ViewState["errorLevel"];
                if (val != null && val is int)
                {
                    return (int)val;
                }
                return 0;
            }
            set
            {
                this.ViewState["errorLevel"] = value;
            }
        }

        [PersistenceMode(PersistenceMode.Attribute)]
        public int Status
        {
            get
            {
                object val = this.ViewState["status"];
                if (val != null && val is int)
                {
                    return (int)val;
                }
                return 0;
            }
            set
            {
                this.ViewState["status"] = value;
            }
        }

        private bool show = true;

        [PersistenceMode(PersistenceMode.Attribute)]
        public bool Show { get { return show; } set { show = value; } }

        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // warning means red text color
            if (Status > this.WarningLevel)
                this.ForeColor = Color.Red;
            else
                this.ForeColor = Color.Black;

            // error means red and bold
            if (Status > this.ErrorLevel)
                this.Font.Bold = true;
            else
                this.Font.Bold = false;
        }
    }
}