using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

namespace SolarWinds.Orion.Web.Controls
{
    public class CustomCheckBox : System.Web.UI.WebControls.CheckBox
    {
		/// <summary>
		/// Use this property to store any custom value to control's viewstate
		/// for later use in postback events
		/// </summary>
		public object CustomValue
		{
			get
			{
				return this.ViewState["CustomValue"];
			}
			set
			{
				this.ViewState["CustomValue"] = value;
			}
		}

		/// <summary>
		/// Use this property to store EventID to control's viewstate
		/// for later use in postback events
		/// </summary>
		[Obsolete("Use CustomValue property")]
        [PersistenceMode(PersistenceMode.Attribute)]
        public int EventID
        {
            get 
            {
                object val = this.ViewState["eventID"];
                if (val != null && val is int)
                {
                    return (int)val;
                }
                return -1;
            }
            set 
            {
                this.ViewState["eventID"] = value;
            }
        }
    }
}