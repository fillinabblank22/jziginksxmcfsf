﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace SolarWinds.Orion.Web.Extensions
{
    /// <summary>
    /// Summary description for IPAddressExtensions
    /// </summary>
    public static class IPAddressExtensions
    {

        public static bool IsFilledRange(this SolarWinds.Orion.Core.Models.Discovery.IPAddressRange target) { 
            
            return !string.IsNullOrEmpty(target.RangeBegin)
                && !string.IsNullOrEmpty(target.RangeEnd)
                && IsValidIPv4Format(target.RangeBegin)
                && IsValidIPv4Format(target.RangeEnd);
        }

        public static bool IsValidIPv4Format(this string input) {
            if (string.IsNullOrWhiteSpace(input)) {
                return false;
            }
            var quads = input.Trim().Split('.').Select(q => q.Trim()).ToArray();
            if (quads.Length != 4) {
                return false;
            }

            foreach (var quad in quads) { 
                int number = default(int);
                if (int.TryParse(quad, out number) && number >= 0 && number <= 255)
                {
                    continue;
                }
                else {
                    return false;
                }
            }
            return true;
        }


        public static bool IsValidRange(this SolarWinds.Orion.Core.Models.Discovery.IPAddressRange target)
        {
            if (target.RangeBegin == null || !target.RangeBegin.IsValidIPv4Format() || target.RangeEnd == null || !target.RangeEnd.IsValidIPv4Format())
            {
                return false;
            }

            try
            {
                var beginIp = SolarWinds.Common.Net.HostHelper.ConvertIPAddressToLong(target.RangeBegin);
                var endIp = SolarWinds.Common.Net.HostHelper.ConvertIPAddressToLong(target.RangeEnd);
                return beginIp <= endIp;
            }
            catch (FormatException)
            {

            }
            return false;
        }

        public static bool IsOverlapping(this SolarWinds.Orion.Core.Models.Discovery.IPAddressRange target, SolarWinds.Orion.Core.Models.Discovery.IPAddressRange other)
        {
            if (other == null)
            {
                throw new ArgumentNullException("other");
            }

            if (!other.IsValidRange())
            {
                return false;
            }
            var beginIp = SolarWinds.Common.Net.HostHelper.ConvertIPAddressToLong(target.RangeBegin);
            var endIp = SolarWinds.Common.Net.HostHelper.ConvertIPAddressToLong(target.RangeEnd);

            var otherBeginIp = SolarWinds.Common.Net.HostHelper.ConvertIPAddressToLong(other.RangeBegin);
            var otherEndIp = SolarWinds.Common.Net.HostHelper.ConvertIPAddressToLong(other.RangeEnd);
            return (beginIp <= otherBeginIp && otherEndIp <= endIp) // target range contains other range
                || (endIp >= otherBeginIp && endIp <= otherEndIp) // target range leftmost overlap 
                || (beginIp >= otherBeginIp && beginIp <= otherEndIp) // target range rightmost overlap
                || (beginIp >= otherBeginIp && endIp <= otherEndIp);// other range contains target range
        }

        public static string ToJSONString(this SolarWinds.Orion.Core.Models.Discovery.IPAddressRange target) {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(target.RangeBegin)) {
                sb.Append("\"begin\":\"").Append(target.RangeBegin).Append('"');
            }
            if (!String.IsNullOrEmpty(target.RangeEnd)) {
                if (sb.Length > 0) {
                    sb.Append(",");
                }

                sb.Append("\"end\":\"").Append(target.RangeEnd).Append('"');
            }
            sb.Insert(0, "{").Append("}");
            return sb.ToString();
        }
    }
}