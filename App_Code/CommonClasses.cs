﻿/// <summary>
/// Summary description for CommonClasses
/// </summary>
public class DataProps
{
    public string Key { get; set; }
    public string Value { get; set; }
    public string WarnMessage { get; set; }

    public DataProps(string key, string value, string message)
    {
        Key = key;
        Value = value;
        WarnMessage = message;
    }
}