using System;
using System.CodeDom;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using System.Web.UI;
using SolarWinds.Orion.Web.Helpers;

/// <summary>
/// HTML encoded variant of code expression wraps with HttpUtility.HtmlEncode function
/// in case of code returns a valid string.
/// </summary>
[ExpressionPrefix("HtmlEncodedCode")]
public class HtmlEncodedCodeExpressionBuilder : ExpressionBuilder
{
    public override CodeExpression GetCodeExpression(
        BoundPropertyEntry entry, 
        object parsedData, 
        ExpressionBuilderContext context)
            => new CodeSnippetExpression($"HtmlEncodedCodeExpressionBuilder.EncodeIfString({entry.Expression})");

    public static object EncodeIfString(object target)
    {
		var str = target as string;
		
        if (str != null)
        {
            return WebSecurityHelper.SanitizeHtmlV2(str);
        }

        return target;
    }
}

/// <summary>
/// HTML encoded variant of resource server expression.
///
/// E.g.
/// 
/// <%$ Resources: CoreWebContent, ScriptString %>
/// 
///     => <script>alert('aaa');</script>
/// 
/// <%$ HtmlEncodedResource: CoreWebContent, ScriptString %>
/// 
///     => &lt;script&gt;alert(&#39;aaa&#39;);&lt;/script&gt;
/// </summary>
[ExpressionPrefix("HtmlEncodedResources")]
public class HtmlEncodedResourcesExpressionBuilder : ExpressionBuilder
{
    public override CodeExpression GetCodeExpression(
        BoundPropertyEntry entry, 
        object parsedData,
        ExpressionBuilderContext context)
    {
        // Resources: CoreWebContent, WEBDATA_AY0_31 
        //  IN>  CoreWebContent, WEBDATA_AY0_31 
        // OUT>  CoreWebContent.WEBDATA_AY0_31 
        var rephrasedExpression = 
            String.Join(".", entry.Expression.Split(',').Select(p => p.Trim()));

        return new CodeSnippetExpression($"SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Resources.{rephrasedExpression})");
    }
}