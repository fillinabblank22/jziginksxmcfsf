using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SNMPModeEventArgs
/// </summary>
public class SNMPModeEventArgs:EventArgs
{
	private bool icmp;

	public  bool ICMP
	{
		get { return icmp; }
		set { icmp = value; }
	}

    public bool VMware { get; set; }

    public bool Windows { get; set; }

    public bool Agent { get; set; }
}
