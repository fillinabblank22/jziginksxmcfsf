﻿using System;
using Castle.Windsor;
using SolarWinds.Orion.Web;

namespace SolarWinds.Orion.Web
{
    public class WindsorServiceProvider : IServiceProvider, IReleasable
    {
        private readonly WindsorContainer _container;

        public WindsorServiceProvider(WindsorContainer container)
        {
            if (container == null)
                throw new ArgumentNullException(nameof(container));

            _container = container;
        }

        public object GetService(Type serviceType)
        {
            return _container.Resolve(serviceType);
        }

        public void Release(object instance)
        {
            if (instance == null)
                throw new ArgumentNullException(nameof(instance));

            _container.Release(instance);
        }
    }
}