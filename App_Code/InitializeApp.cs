// { 'module': 'InitializeApp', 'version': '1.0', 'requires': 'Microsoft.Web.Infrastructure.dll || .NET 4.5' }

using System;
using System.Linq;
using System.Web;
using System.Reflection;

namespace SolarWinds.AppCode
{
    public class InitializeApp
    {
        private static SolarWinds.Logging.Log Log = new SolarWinds.Logging.Log();

        public static void AppInitialize()
        {
            Log.Info("InitializeApp looking for hookups...");

            var registerModule = GetRegisterModuleDelegate();

            var assembly = Assembly.GetExecutingAssembly();
            var paramSignature = new[] { typeof(Func<Type, bool>) };

            foreach (var type in assembly.GetExportedTypes().Where(x => x.IsClass))
            {
                var methodInfo = type.GetMethod("OnApplicationInit", BindingFlags.Public | BindingFlags.Static, null,
                    paramSignature, null);

                if (methodInfo == null)
                    continue;

                try
                {
                    Log.InfoFormat("calling [{0}].OnApplicationInit()", type.FullName);
                    methodInfo.Invoke(null, new object[] {registerModule});
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("[{0}].OnApplicationInit() failed.", type.FullName), ex);
                }
            }

            Log.Info("InitializeApp finished hooking up.");
        }

        internal static Func<Type, bool> GetRegisterModuleDelegate()
        {
            var t1 = typeof (HttpApplication);

            MethodInfo methodInfo = t1.GetMethod("RegisterModule", BindingFlags.Public | BindingFlags.Static, null, new[] { typeof(Type) }, null);

            if (methodInfo == null)
                return null;

            return delegate(Type type)
            {
                try
                {
                    Log.InfoFormat("Attempting to register IHttpModule {0}.", type.FullName);
                    methodInfo.Invoke(null, new object[] {type});
                    return true;
                }
                catch (Exception ex)
                {
                    Log.Error("IHttpModule registration failed.", ex);
                    return false;
                }
            };
        }
    }
}