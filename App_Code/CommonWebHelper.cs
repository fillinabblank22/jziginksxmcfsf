﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Configuration;
using System.Web.Security.AntiXss;
using System.Collections.Specialized;
using SolarWinds.Logging;
using SolarWinds.MapEngine;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using CommonRegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;

public class CommonWebHelper
{
    protected static Log _log = new Log();

	private const int EngineHorizon = 120;

    static string _tagsToEncodeReg = "<(?!br|/br|p|/p|span|/span|div|/div|table|/table|tr|/tr|td|/td|a|/a|b|/b|i(?!nput)|/i(?!nput))(?:[^\">]|\"[^\"]*\")*>";
    static bool _isBreadcrumbsDisabled = GetBreadcrumbsSetting();

    static string EncodeText(Match m)
    {
        return HttpUtility.HtmlEncode(m.ToString());
    }
   
    public static string GetTitlePart(IList<string> messageTypes)
	{
		string separator = "";
		string title = "";
		for (int i = 0; i < messageTypes.Count - 1; i++)
		{
			title = title + separator + messageTypes[i];
			separator = Resources.CoreWebContent.String_Separator_1_VB0_59;
		}

        return String.Format(Resources.CoreWebContent.WEBCODE_VB0_275, title, messageTypes[messageTypes.Count - 1]);
	}

    public static string EncodeHTMLTags(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return string.Empty;
        }

        Regex regex = new Regex(_tagsToEncodeReg, RegexOptions.IgnoreCase);
        return regex.Replace(value, new MatchEvaluator(CommonWebHelper.EncodeText));
    }

    public static int TryToGetNodeIDFromRequest(string requestString)
    {
        int nodeID = -1;
        if (!String.IsNullOrEmpty(requestString))
        {
            string[] netObject = requestString.Split(':');
            if (netObject.Length == 2 && netObject[0].Trim().Equals("N", StringComparison.InvariantCultureIgnoreCase))
                Int32.TryParse(netObject[1].Trim(), out nodeID);
        }

        return nodeID;
    }

    public static string JQueryEncode(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return string.Empty;
        }

        value = value.Replace(@"'", "&apos;");
        value = HttpUtility.HtmlEncode(value);
        return value;
    }

    private static bool GetBreadcrumbsSetting()
    {
        bool disable = false;
        bool.TryParse(ConfigurationManager.AppSettings["DisableBreadcrumbs"], out disable);
        return disable;
    }

	public static bool IsMobileDevicesAutodetectEnabled
	{
		get
		{
			bool value = false;
			bool.TryParse(ConfigurationManager.AppSettings["EnableAutodetectMobileDevices"], out value);
			return value;
		}
	}

    private static bool GetBreadcrumbsPrintable()
    {
        // check if Printable=True exists in querystring, and if it does, disable the page breadcrumbs
        bool printable = false;
        if (HttpContext.Current.Request.QueryString["Printable"] != null)
        {
            bool.TryParse(HttpContext.Current.Request.QueryString["Printable"], out printable);
        }

        return printable;
    }

    public static bool IsBreadcrumbsDisabled
    {
        get
        {
            return (_isBreadcrumbsDisabled || GetBreadcrumbsPrintable());
        }
    }

    public static bool IsProductBlogDisabled
    {
        get
        {
            OrionSetting setting = SettingsDAL.GetSetting("ProductsBlog-Disable");
            if (setting != null && setting.SettingValue == 1)
                return true;
            else
                return false;
        }
    }

    public static bool IsMaintenanceRenewalsDisabled
    {
        get
        {
            OrionSetting setting = SettingsDAL.GetSetting("MaintenanceRenewals-Disable");
            if (setting != null && setting.SettingValue == 1)
                return true;
            else
                return false;
        }
    }

    public static double WarnRowsNumber
    {
        get
        {
            OrionSetting setting = SettingsDAL.GetSetting("DatabaseDetails-WarnRowsNumber");
            if (setting != null && setting.SettingValue > 0)
                return setting.SettingValue;
            else
                return 10000000;
        }
    }

    public static bool WarnRowsNumberEnabled
    {
        get
        {
            OrionSetting setting = SettingsDAL.GetSetting("DatabaseDetails-WarnRowsNumberEnabled");
            if (setting != null && setting.SettingValue == 0)
                return false;
            else
                return true;
        }
    }

    public static string FormatPercentStatusBar(object value, double errorLimit, double warningLimit, int width, bool show)
    {
        try
        {
            if (!show)
                return "&nbsp;";

            return FormatHelper.GetPercentBarFormat(Convert.ToDouble(value), errorLimit, warningLimit, width, false, true);
        }
        catch (InvalidCastException ex)
        {
            _log.ErrorFormat("Caught Exception while getting formatted percent status bar; Exception details: {0}", ex.ToString());
            return "&nbsp;";
        }
    }

    public static string FormatPercentValue(object value, double errorLimit, double warningLimit, bool show)
    {
        try
        {
            if (!show)
                return "&nbsp;";

            return FormatHelper.GetPercentFormat(Convert.ToDouble(value), errorLimit, warningLimit, false);
        }
        catch (InvalidCastException ex)
        {
            _log.ErrorFormat("Caught Exception while getting formatted percent value; Exception details: {0}", ex.ToString());
            return "&nbsp;";
        }
    }

    public static bool IsMobileBrowser()
    {
		return (HttpContext.Current.Items["IsMobileDeviceDetected"] != null) ? (bool)HttpContext.Current.Items["IsMobileDeviceDetected"] : false;;
    }

    public static bool IsMobileView(HttpRequest request)
    {
        string isMobileView = (request.QueryString["IsMobileView"] != null) ? request.QueryString["IsMobileView"] : string.Empty;
        string returnUrl = (request.QueryString["ReturnUrl"] != null) ? request.QueryString["ReturnUrl"] : string.Empty;

        return (returnUrl.ToLowerInvariant().Contains("ismobileview=true")
                    && !isMobileView.ToString().Equals("false", StringComparison.OrdinalIgnoreCase)
                    || isMobileView.ToString().Equals("true", StringComparison.OrdinalIgnoreCase)
                    || (IsMobileBrowser() && !isMobileView.ToString().Equals("false", StringComparison.OrdinalIgnoreCase)));
    }

    public static string FullSiteHref(HttpRequest request)
    {
        var queryStrings = HttpUtility.ParseQueryString(request.Url.Query.ToLowerInvariant());
        var encodedQueryStrings = HttpUtility.ParseQueryString(String.Empty);
        foreach (var key in queryStrings.AllKeys)
        {
            string validatedKey = HttpUtility.UrlDecode(AntiXssEncoder.UrlEncode(key));
            string validatedValue = HttpUtility.UrlDecode(AntiXssEncoder.UrlEncode(queryStrings[key]));
            encodedQueryStrings.Add(validatedKey, validatedValue);
        }
        encodedQueryStrings["ismobileview"] = "false";
        return $"{request.Url.LocalPath}?{encodedQueryStrings}";
    }

    public static string GetStatusIconPath(object uri, object status)
    {
        string path = string.Empty;
        if (uri != null && status != null)
        {
            string netObjectId = UriHelper.GetNetObjectId(uri.ToString());
            string swisType = NetObjectFactory.GetSWISTypeByNetObject(netObjectId);

            if (swisType.EndsWith("Orion.Nodes", StringComparison.OrdinalIgnoreCase))
            {
                NetObject netObj = NetObjectFactory.Create(netObjectId);
                var compStatus = (netObj as Node).Status;
                path = compStatus.ToString("smallimgpath", null);
            }
            else
            {
                path = string.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small", swisType, status.ToString());
            }
        }

        return path;
    }

    public static string RenderObjectName(object uri, object name)
    {
        string resLinkTemp = "<a href='/Orion/View.aspx?NetObject={0}'{2}>{1}</a>";

        string netObjectId = UriHelper.GetNetObjectId(uri.ToString());
        string swisType = NetObjectFactory.GetSWISTypeByNetObject(netObjectId);
        string addParams = string.Empty;

        if (swisType.EndsWith("Orion.Nodes", StringComparison.OrdinalIgnoreCase))
        {
            Node node = NetObjectFactory.Create(netObjectId) as Node;
            addParams = string.Format(" IP='{0}' NodeHostname='{1}' Community='{2}' ",
                node.IPAddress.ToString(),
                node.Hostname,
                (CommonRegistrySettings.AllowSecureDataOnWeb()) ? node.CommunityString : FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(node.CommunityString)));
        }

        return string.Format(resLinkTemp, netObjectId, FormatHelper.MakeBreakableString(name.ToString()), addParams);
    }

    public static string FilterXSS(string text)
    {
        // Encode the string input
        StringBuilder sb = new StringBuilder(HttpUtility.HtmlEncode(text));
        // Selectively allow  <b>, <br/> and <i>
        sb.Replace("&lt;b&gt;", "<b>");
        sb.Replace("&lt;/b&gt;", "</b>");
        sb.Replace("&lt;br/&gt;", "<br/>");
        sb.Replace("&lt;br&gt;", "<br>");
        sb.Replace("&lt;/br&gt;", "</br>");
        sb.Replace("&lt;i&gt;", "<i>");
        sb.Replace("&lt;/i&gt;", "</i>");
        return sb.ToString();
    }

	public static string GetEngineStatusImage(DateTime keepAlive)
	{
		var timeSpan = DateTime.UtcNow - keepAlive;
		if (timeSpan.TotalSeconds < EngineHorizon)
			return "Small-Up.gif";
		else if (timeSpan.TotalSeconds > EngineHorizon * 1.5)
			return "Small-Down.gif";
		else
			return "Small-Warning.gif";
	}
}
