﻿using System;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using SolarWinds.Orion.Web;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Logging;
using SolarWinds.ESI.Data.DAL.SWIS;
using SolarWinds.ESI.Common.Models;
using SolarWinds.Orion.Common;

/// <summary>
/// Summary description for ManageESIInstances
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ManageESIInstances : System.Web.Services.WebService
{
    public struct Response
    {
        public bool Success;
        public string Message;
    }

    private static Log log = new Log();


    [WebMethod]
    public Response CloseHint()
    {
        SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Set("Web-ManageESIInstances-ShowHint", false.ToString());

        return new Response { Success = true };
    }

    [WebMethod]
    public PageableDataTable GetESIInstances()
    {
        int startRowNumber;
        if (!Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber) || startRowNumber < 1)
            startRowNumber = 0;

        int pageSize;
        if (!Int32.TryParse(Context.Request.QueryString["limit"], out pageSize) || pageSize < 1)
            pageSize = 20;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        string sortOrder = "InstanceType";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}", sortColumn, sortDirection);
        }
        Dictionary<string, object> queryParams = new Dictionary<string, object>();
        string queryString =
            String.Format(
                @"SELECT s.InstanceID, s.Type as InstanceType, s.Name as InstanceName,s.Url as InstanceURL, s.CredentialID, s.OperationalState, s.Status
                FROM Orion.ESI.IncidentService s
                ORDER BY {0}
                WITH ROWS @startRowNumber TO @endRowNumber
                ", sortOrder);

        queryParams[@"startRowNumber"] = startRowNumber + 1;
        queryParams[@"endRowNumber"] = startRowNumber + pageSize;

        string countQuery =
            string.Format(
                @"SELECT Count(s.InstanceID) AS serversCount FROM Orion.ESI.IncidentService (nolock=true) s");

        DataTable ssDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countQuery);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new PageableDataTable(ssDataTable,
                                     (countTable.Rows == null || countTable.Rows.Count == 0
                                          ? 0
                                          : (int)((countTable.Rows[0][0] is System.DBNull) ? 0 : countTable.Rows[0][0])));
    }

    [WebMethod]
    public int GetESIInstanceNumber(string serverId, string direction, string sort)
    {
        string sortOrder = "s.InstanceName";
        if (!string.IsNullOrEmpty(sort))
        {
            sortOrder = string.Format("s.{0} {1}", sort, direction);
        }
        string queryString =
            String.Format(
                @"SELECT s.InstanceID FROM Orion.ESI.IncidentServices (nolock=true) s 
                                            ORDER BY {0}",
                sortOrder);

        DataTable ssDataTable;
        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(queryString);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        if (ssDataTable == null || ssDataTable.Rows == null || ssDataTable.Rows.Count == 0)
            return -1;

        int rowId = 0;
        foreach (DataRow row in ssDataTable.Rows)
        {
            if (Convert.ToString(row["SMTPServerID"]) == serverId)
                return rowId;
            rowId++;
        }
        return -1;
    }

    [WebMethod]
    public Response changeStateESIInstance(string[] ids, int state)
    {
        var response = new Response();
        
        try
        {
            if (OrionConfiguration.IsDemoServer)
                throw new InvalidOperationException("This operation is not allowed in demo mode.");

            if (!Enum.IsDefined(typeof(OperationalState), state))
                throw new ArgumentOutOfRangeException("state", 
                    String.Format("OperationalState enum has no definition for value '{0}'.", state));
            
            using (var instanceDAL = new InstanceDAL())
            {
                foreach (var id in ids)
                {
                    Guid guid;

                    if (!Guid.TryParse(id, out guid))
                        throw new ArgumentOutOfRangeException("ids", 
                            String.Format("Supplied id '{0}' is not a GUID.", id));

                    // we dont expect that many instances to implement bulk
                    instanceDAL.SetOperationalState(guid, (OperationalState)state);
                }
            }

            response.Success = true;
            return response;
        }
        catch (Exception ex)
        {
            log.Error("Couldn't update ESI Instance operational state.", ex);
            response.Success = false;
            response.Message = ex.Message;

            return response;
        }
    }

    [WebMethod]
    public Response deleteESIInstances(string[] ids)
    {
        var response = new Response();

        try
        {
            if (OrionConfiguration.IsDemoServer)
                throw new InvalidOperationException("This operation is not allowed in demo mode.");

            using (var instanceDAL = new InstanceDAL())
            {
                foreach (var id in ids)
                {
                    // Delete related actions
                    var relatedActionsQuery = @"SELECT DISTINCT A.Uri AS Uri FROM Orion.Actions A
                            JOIN Orion.ActionsProperties AP ON A.ActionID = AP.ActionID
                            WHERE AP.PropertyName = 'InstanceID' AND AP.PropertyValue = @InstanceID";

                    instanceDAL.WithProxy(p =>
                    {
                        var args = new Dictionary<string, object> { { "InstanceID", id } };
                        var relatedActions = p.Query(relatedActionsQuery, args);
                        p.BulkDelete(relatedActions.Rows.Cast<DataRow>().Select(d => d["Uri"].ToString()).ToArray());
                    });

                    // Delete the instance
                    instanceDAL.Delete(new Guid(id));
                }
            }

            response.Success = true;
            return response;
        }
        catch (Exception ex)
        {
            log.Error("Couldn't delete ESI Instance.", ex);
            response.Success = false;
            response.Message = ex.Message;

            return response;
        }
    }

    [WebMethod]
    public Response<object> GetUsedESIInstances(string[] ids)
    {
        string ESIInstancesIds = string.Join("','", ids);
        string sqlQuery = String.Format(@"SELECT I.InstanceID, I.Name, COUNT(A.ActionID) AS ActionCount from Orion.ESI.IncidentService (nolock=true)  I
JOIN Orion.ActionsProperties (nolock=true) A ON A.PropertyValue=I.InstanceID
WHERE A.PropertyValue IN ('{0}') and A.PropertyName='InstanceID' GROUP BY I.InstanceID, I.Name", ESIInstancesIds);
        DataTable ssDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(sqlQuery);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new Response<object>
        {
            result = ssDataTable.Rows.Cast<DataRow>().Select(r => new
            {
                InstanceId = r["InstanceID"],
                InstanceName = r["Name"],
                ActionCount = r["ActionCount"]
            }).ToArray()
        };
    }

    public class Response<T>
    {
        public T result { get; set; }
    }
}
