﻿using System;
using System.Resources;
using SolarWinds.Orion.Common;


namespace SolarWinds.Orion.Core.Common.i18n
{
    [Serializable]
    public class CoreValidationException : i18nReadyException
    {
        public enum Reason
        {
            MissingIpAddress
        }

        static string GetLoggingString(Reason reason, object[] args)
        {
            switch (reason)
            {
                case Reason.MissingIpAddress: return string.Format("IP address for host {0} is missing", GetArg(0, args));
            }

            return "i18nResourceException: " + reason;
        }

        public Reason Cause { get; private set; }
        protected override ResourceManager ResManager() { return Resources.CoreWebContent.ResourceManager; }
        protected override string KeyPrefix() { return GetType().Name; }
        protected override string KeyReason() { return Cause.ToString(); }

        public CoreValidationException(Reason reason) : base(GetLoggingString(reason, null), null) { Cause = reason; }
        public CoreValidationException(Reason reason, params object[] args) : base(GetLoggingString(reason, args), args) { Cause = reason; }
        public CoreValidationException(Reason reason, Exception e) : base(GetLoggingString(reason, null), e, null) { Cause = reason; }
        public CoreValidationException(Reason reason, Exception e, params object[] args) : base(GetLoggingString(reason, args), e, args) { Cause = reason; }

        //Duplicate of code
        //Make sense move to parent class? 
        protected static object GetArg(int pos, object[] args)
        {
            if (args == null || pos < 0 || pos >= args.Length) return null;
            return args[pos];
        }
    }
}