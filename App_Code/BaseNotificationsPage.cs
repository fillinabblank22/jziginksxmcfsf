﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Logging;

namespace SolarWinds.Orion.Web
{
    public class BaseNotificationsPage : Page
    {
        protected static Log log = new Log();

        protected void BusinessLayerExceptionHandler(Exception ex)
        {
            log.Error(ex);
        }

    }
}