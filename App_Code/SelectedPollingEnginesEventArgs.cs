﻿using SolarWinds.Orion.Models;
using System;
using System.Collections.Generic;

public class SelectedPollingEnginesEventArgs : EventArgs
{
    public IReadOnlyCollection<int> SelectedEngineIds
    {
        get;
    }

    public HashSet<EngineServerType> SelectedServerTypes
    {
        get;
    }

    public SelectedPollingEnginesEventArgs(int[] engineIds, HashSet<EngineServerType> engineServerTypes)
    {
        SelectedEngineIds = new List<int>(engineIds);
        SelectedServerTypes = engineServerTypes;
    }
}