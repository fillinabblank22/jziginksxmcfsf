﻿using SolarWinds.ESI.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.ESI.Data.DAL.SWIS;
using SolarWinds.ESI.Common.BL;
using System.Threading;
using SolarWinds.Orion.Common;
using System.Configuration;
using SolarWinds.SSDI.Strings;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Configuration;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.SSDI.Common.Models;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1), ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class EditServiceDeskInstance : System.Web.Services.WebService
{
	private static Log log = new Log();

	public EditServiceDeskInstance()
	{
	}

	[WebMethod(), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
	public Response<object> load(string instanceId)
	{
		using (var dal = new InstanceDAL())
		{
			if (String.IsNullOrEmpty(instanceId))
			{
				return GetNewInstanceResponse();
			}

			var instance = dal.Fetch(new Guid(instanceId));

			if (instance == null)
			{
				return GetNewInstanceResponse();
			}
			else
			{
				return GetInstanceResponse(dal, instance);
			}
		}
	}

	[WebMethod(EnableSession = true), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
	public Response<object> save(string id, string name, string url, string token)
	{
		if (OrionConfiguration.IsDemoServer)
			throw new InvalidOperationException("This operation is not allowed in demo mode.");

		using (var dal = new InstanceDAL())
		{
			var instance = CreateSsdInstanceObject(name, url);

			Guid idValue = Guid.Empty;

			if (!string.IsNullOrEmpty(id) && Guid.TryParse(id, out idValue))
			{
				instance.InstanceID = idValue;
			}

			bool isNewInstance = idValue == Guid.Empty;
			bool tokenChanged = !String.IsNullOrWhiteSpace(token);
			ApiKeyCredential credential = null;

			if (isNewInstance)
			{
				credential = GetNewCredential(token);
			}
			else
			{
				credential = dal.GetCredential<ApiKeyCredential>(instance.InstanceID);

				if (tokenChanged)
				{
					var result = new BLHelper().BLInvoke<InstanceResult>(instance.InstanceID, "UnregisterConnection");

					if (!result.Success)
					{
						return GetErrorResponse(result.Message, result.MessageDetail);
					}

					credential.ApiKey = token;
				}
			}

			return TryRegisterConnection(dal, instance, credential, tokenChanged, isNewInstance);
		}
	}

	[WebMethod(), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
	public Response<object> test(string id, string token)
	{
		if (OrionConfiguration.IsDemoServer)
			throw new InvalidOperationException("This operation is not allowed in demo mode.");

		using (var dal = new InstanceDAL())
		{
			Guid idValue;

			if (!Guid.TryParse(id, out idValue))
			{
				idValue = Guid.Empty;
			}

			bool isNewInstance = idValue == Guid.Empty;
			bool tokenChanged = !String.IsNullOrWhiteSpace(token);
			ApiKeyCredential credential = null;

			if (isNewInstance)
			{
				credential = GetNewCredential(token);
			}
			else
			{
				credential = dal.GetCredential<ApiKeyCredential>(idValue);

				if (tokenChanged)
				{
					credential.ApiKey = token;
				}
			}

			InstanceResult result = null;

			try
			{
				result = new BLHelper().BLInvokeStatic<InstanceResult>(SolarWinds.SSDI.Common.Constants.SsdiInstanceType,
																		"TestConnection",
																		new Dictionary<string, object>
																		{
																			["credential"] = credential,
																			["isNewInstance"] = isNewInstance
																		});
			}
			catch (Exception ex)
			{
				log.Error("The ServiceDesk test connection failed.", ex);
				return GetErrorResponse(Resources.ESIWebContent.Service_Desk_Test_Connection_Failed_Message, ex.ToString());
			}

			return GetTestConnectionResponse(result);
		}
	}

	private Instance CreateSsdInstanceObject(string name, string url)
	{
		return new Instance
		{
			Name = name,
			Url = url,
			InstanceType = SolarWinds.SSDI.Common.Constants.SsdiInstanceType,
			OperationalState = OperationalState.Enabled
		};
	}

	private ApiKeyCredential GetNewCredential(string token)
	{
		return new ApiKeyCredential(token)
		{
			Name = "SolarWinds Service Desk Credential"
		};
	}

	private Response<object> TryRegisterConnection(InstanceDAL instanceDal, Instance instance, ApiKeyCredential credential, bool tokenChanged, bool isNewInstance)
	{
		if (isNewInstance || tokenChanged)
		{
			InstanceResult result = null;

			try
			{
				result = new BLHelper().BLInvokeStatic<InstanceResult>(SolarWinds.SSDI.Common.Constants.SsdiInstanceType,
					"RegisterConnection",
					new Dictionary<string, object>
					{
						["credential"] = credential
					});

				if (!result.Success)
				{
					return GetErrorResponse(result.Message, result.MessageDetail);
				}

				instance.CustomData = result.CustomData;

			}
			catch (Exception ex)
			{
				log.Error("The ServiceDesk register connection failed.", ex);
				return GetErrorResponse("Register connection failed.", ex.ToString());
			}
		}

		var instanceId = instanceDal.InsertOrUpdate(instance, credential);

		if (instanceId != Guid.Empty && isNewInstance)
		{
			SetInstanceIdIntoSession(instanceId);
		}

		return GetSuccessSavedInstanceResponse(instanceId);
	}

	private Response<object> GetNewInstanceResponse()
	{
		return new Response<object>
		{
			result = new
			{
				id = Guid.Empty.ToString(),
				proxyEnabled = false,
				proxyAuthenticationEnabled = false
			}
		};
	}

	private Response<object> GetInstanceResponse(InstanceDAL dal, Instance instance)
	{
		ApiKeyCredential creds;

		if (OrionConfiguration.IsDemoServer)
		{
			creds = new ApiKeyCredential()
			{
				ID = 1234,
				ApiKey = "afdsdfdsfdsfsaa"
			};
		}
		else
		{
			creds = dal.GetCredential<ApiKeyCredential>(instance.CredentialID);
		}

		return new Response<object>
		{
			result = new
			{
				id = instance.InstanceID,
				name = instance.Name,
				url = instance.Url,
				token = creds.ApiKey
			}
		};
	}

	private Response<object> GetSuccessSavedInstanceResponse(Guid instanceId)
	{
		return new Response<object>
		{
			result = new
			{
				valid = true,
				instanceId = instanceId
			}
		};
	}

	private Response<object> GetTestConnectionResponse(InstanceResult result)
	{
		return new Response<object>
		{
			result = new
			{
				valid = result.Success,
				message = result.Message,
				detail = result.MessageDetail,
				instanceUrl = result.InstanceUrl
			}
		};
	}

	private Response<object> GetErrorResponse(string message, string detail)
	{
		return new Response<object>
		{
			result = new
			{
				valid = false,
				message = message,
				detail = detail
			}
		};
	}

	private void SetInstanceIdIntoSession(Guid instanceId)
	{
		var ctx = HttpContext.Current;
		if (ctx != null)
		{
			ctx.Session["NewIntegrationInstance"] = instanceId;
		}
	}

	[Serializable]
	public class Response<T>
	{
		public T result { get; set; }
	}
}