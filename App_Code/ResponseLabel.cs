﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolarWinds.Orion.Web.Controls
{
    /// <summary>
    /// Response label is customized label for warning or error status visualisation with using of text formating.
    /// </summary>
    public class ResponseLabel : SolarWinds.Orion.Web.Controls.CustomFormatedLabel
    {
        protected override void OnLoad(EventArgs e)
        {
            if (!this.Show)
                this.Text = "&nbsp;";
            else
                if (this.Status < 0)
                    this.Text = Resources.CoreWebContent.WEBCODE_AK0_57;
                else
                    this.Text = String.Format(Resources.CoreWebContent.WEBCODE_AK0_56, Status);

            base.OnLoad(e);
        }
    }
}
