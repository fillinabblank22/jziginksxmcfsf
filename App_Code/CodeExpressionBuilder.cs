using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Compilation;
using System.CodeDom;

[ExpressionPrefix("Code")]
public class CodeExpressionBuilder : ExpressionBuilder
{

    public override CodeExpression GetCodeExpression(BoundPropertyEntry entry,

       object parsedData, ExpressionBuilderContext context)
    {

        return new CodeSnippetExpression(entry.Expression);

    }

}