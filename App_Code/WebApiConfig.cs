﻿using System.Web.Http;
using SolarWinds.Orion.Web.Filters;


/// <summary>
/// Summary description for WebApiConfig
/// </summary>
public static class WebApiConfig
{
 
    public static void Register(HttpConfiguration config)
    {
        config.MapHttpAttributeRoutes();

        config.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = RouteParameter.Optional }
        );

        config.Routes.MapHttpRoute(
            name: "ApiWithSession",
            routeTemplate: "sapi/{controller}/{action}/{id}",
            defaults: new { id = RouteParameter.Optional }
         );

        config.Filters.Add(new CatchUnhandledExceptionFilterAttribute());
        config.Filters.Add(new ViewLimitationFilterAttribute());

        config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new SolarWinds.Orion.Web.PageableDataTableJsonConverter());
    }
}