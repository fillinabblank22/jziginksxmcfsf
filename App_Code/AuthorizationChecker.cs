﻿using System.Web;
using Resources;

/// <summary>
/// This class provides methods for authorization validation purposes.
/// </summary>
public static class AuthorizationChecker
{
    /// <summary>
    /// Checks whether the current user account has admin rights
    /// </summary>
    /// <returns>True if the current user account has admin rights, otherwise false</returns>
    public static bool IsAdmin => ((ProfileCommon)HttpContext.Current.Profile).AllowAdmin;

    /// <summary>
    /// Checks whether the current user account has admin rights, and throws an exception if it does not.
    /// </summary>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have admin rights.
    /// </exception>
    public static void AllowAdmin()
    {
        AllowAdmin(CoreWebContent.WEBCODE_AK0_182);
    }

    /// <summary>
    /// Checks whether the current user account has admin rights, and throws an exception if it does not.
    /// </summary>
    /// <param name="errorMessage">The error message.</param>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have admin rights.
    /// </exception>
    public static void AllowAdmin(string errorMessage)
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowAdmin)
        {
            throw new UnauthorizedAccessLocalizedException(() => errorMessage);
        }
    }
	
	/// <summary>
    /// Checks whether the current user account has view customization rights, and throws an exception if it does not.
    /// </summary>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have view customization rights.
    /// </exception>
    public static void AllowCustomize()
    {
        AllowCustomize(CoreWebContent.WEBCODE_VB0_317);
    }
	
	/// <summary>
    /// Checks whether the current user account has view customization rights, and throws an exception if it does not.
    /// </summary>
    /// <param name="errorMessage">The error message.</param>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have view customization rights.
    /// </exception>
    public static void AllowCustomize(string errorMessage)
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowCustomize)
        {
            throw new UnauthorizedAccessLocalizedException(() => errorMessage);
        }
    }
	
	/// <summary>
    /// Checks whether the current user account has report management rights, and throws an exception if it does not.
    /// </summary>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have report management rights.
    /// </exception>
    public static void AllowReportManagement()
    {
        AllowReportManagement(CoreWebContent.WEBCODE_SO0_41);
    }
	
	/// <summary>
    /// Checks whether the current user account has report management rights, and throws an exception if it does not.
    /// </summary>
    /// <param name="errorMessage">The error message.</param>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have report management rights.
    /// </exception>
    public static void AllowReportManagement(string errorMessage)
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowReportManagement)
        {
            throw new UnauthorizedAccessLocalizedException(() => errorMessage);
        }
    }
	
	/// <summary>
    /// Checks whether the current user account has node management rights, and throws an exception if it does not.
    /// </summary>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have node management rights.
    /// </exception>
    public static void AllowNodeManagement()
    {
        AllowNodeManagement(CoreWebContent.CredManagerService_UnauthorizedExceptionMessage);
    }
	
	/// <summary>
    /// Checks whether the current user account has node management rights, and throws an exception if it does not.
    /// </summary>
    /// <param name="errorMessage">The error message.</param>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have node management rights.
    /// </exception>
    public static void AllowNodeManagement(string errorMessage)
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowNodeManagement &&
		    !((ProfileCommon)HttpContext.Current.Profile).AllowAdmin)
        {
            throw new UnauthorizedAccessLocalizedException(() => errorMessage);
        }
    }
	
	/// <summary>
    /// Checks whether the current user account has unmanage rights, and throws an exception if it does not.
    /// </summary>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have unmanage rights.
    /// </exception>
    public static void AllowUnmanage()
    {
        AllowUnmanage(CoreWebContent.WEBDATA_YP0_23);
    }
	
	/// <summary>
    /// Checks whether the current user account has unmanage rights, and throws an exception if it does not.
    /// </summary>
    /// <param name="errorMessage">The error message.</param>
    /// <exception cref="UnauthorizedAccessLocalizedException">
    /// Will be thrown in case the current user account does not have unmanage rights.
    /// </exception>
    public static void AllowUnmanage(string errorMessage)
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowUnmanage)
        {
            throw new UnauthorizedAccessLocalizedException(() => errorMessage);
        }
    }
}