﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;


namespace SolarWinds.Orion.Web.Controls
{
    /// <summary>
    /// Percetnt label is customized label for warning or error status visualisation with using of text formating.
    /// </summary>
    public class PercentLabel : SolarWinds.Orion.Web.Controls.CustomFormatedLabel
    {
        [PersistenceMode(PersistenceMode.Attribute)]
        public bool NoLimit
        {
            get
            {
                return (ViewState["noLimit"] as bool?) ?? false;
            }
            set
            {
                ViewState["noLimit"] = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (Status < 0)
            {
                Status = 0;
            }

            if (!NoLimit && Status > 100)
            {
                Status = 100;
            }

            Text = Show ? string.Format("{0}&nbsp;%", Status) : "&nbsp;";
        }
    }
}