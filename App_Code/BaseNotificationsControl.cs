﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

namespace SolarWinds.Orion.Web.Controls
{
    public class BaseNotificationsControl : UserControl
    {
        protected static Log log = new Log();
        private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

        protected void BusinessLayerExceptionHandler(Exception ex)
        {
            log.Error(ex);
        }

        public delegate void ItemIgnoredEventHandler(Guid notificationId);
        public event ItemIgnoredEventHandler ItemIgnored;

        protected void FireItemIgnoredEvent(Guid notificationId)
        {
            if (ItemIgnored != null)
                ItemIgnored(notificationId);
        }

        protected void OnItemCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Ignore" && e.CommandArgument != null)
            {
                Guid notificationId = new Guid(e.CommandArgument.ToString());
                if (notificationId != Guid.Empty)
                {
                    using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                    {
                        proxy.IgnoreNotificationItem(notificationId);
                    }
                    FireItemIgnoredEvent(notificationId);
                }
            }
        }

        protected void IgnoreItemsBulk(List<Guid> notificationIds)
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                proxy.IgnoreNotificationItems(notificationIds);
            }
            FireItemIgnoredEvent(Guid.Empty);
        }

        protected void OnRefreshClick(object sender, EventArgs e)
        {
            RefreshContent();
        }

        public virtual void RefreshContent()
        {
            // have to be overriden by derived class
        }

    }
}