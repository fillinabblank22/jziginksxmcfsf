﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

/// <summary>
/// Class contains helper methods and implements protection against AngularJS template injection utilizing ng-non-bindable directive.
/// 
/// Directive ngNonBindable suppresses AngularJS processing for given and nested HTML elements. This directive can be used on vulnerable elements to suppress
/// evaluation of malicious AngularJS interpolation. We identified and allowed AngularJS processing only in places where it's necessary (megamenu, dashaboards..),
/// we suppressed AngularJS processing in the rest of website and therefore reduced attack surface for AngularJS template injection.
/// </summary>
public static class AngularJsHelper
{

    private static readonly string NgNonBindableAttributeName = "ng-non-bindable";

    /// <summary>
    /// Switch enables or disables global protection against AngularJS template injection by adding ng-non-bindable directive to legacy pages
    /// </summary>
    private static bool AngularJsTemplateInjectionProtectionEnabled => true;

    public static HashSet<string> AngularJsResourceFiles { get; } = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
    {
        "/Orion/NetPerfMon/Resources/Misc/XuiWrapper.ascx",
    };

    public static string RenderNgNonBindable()
    {
        return AngularJsTemplateInjectionProtectionEnabled ? NgNonBindableAttributeName : string.Empty;
    }

    public static string RenderNgNonBindable(bool renderNgNonBindable)
    {
        return (renderNgNonBindable && AngularJsTemplateInjectionProtectionEnabled ? NgNonBindableAttributeName : string.Empty);
    }

    public static void ApplyNgNonBindable(HtmlControl control)
    {
        if (AngularJsTemplateInjectionProtectionEnabled)
        {
            // add ng-non-bindable to element
            control.Attributes[NgNonBindableAttributeName] = "";
        }
    }

    private static string GetContextKeyForPageContent()
    {
        return "_angular_js_enabled_in_page_content_";
    }

    public static void EnableAngularJsForPageContent()
    {
        HttpContext.Current.Items[GetContextKeyForPageContent()] = true;
    }

    public static bool IsAngularJsEnabledForPageContent()
    {
        return (HttpContext.Current.Items[GetContextKeyForPageContent()] as bool?) ?? false;
    }
}