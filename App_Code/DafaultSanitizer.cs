using SolarWinds.Orion.Web.Helpers;

/// <summary>
/// Handles default sanitization
/// </summary>
public static class DefaultSanitizer
{
    /// <summary>
    /// Sanitizes HTML for displaying purposes
    /// </summary>
    /// <param name="target">Sanitized object will be sanitized if it is a string.
    /// No casting applied.</param>
    /// <returns>Sanitized string or original instance if not a string</returns>
    public static object SanitizeHtml(object target)
    {
        var str = target as string;

        if (str != null)
        {
            return WebSecurityHelper.SanitizeHtmlV2(str);
        }

        return target;
    }
}