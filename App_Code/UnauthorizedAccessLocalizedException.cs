﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.Internationalization.Exceptions;
using System.Security.Permissions;
using System.Runtime.Serialization;
using System.Globalization;

/// <summary>
/// Summary description for UnauthorizedAccessLocalizedException
/// </summary>
[Serializable]
public class UnauthorizedAccessLocalizedException : UnauthorizedAccessException, IHasLocalizedMessage
{
        public string LocalizedMessage { get; set; }

        public UnauthorizedAccessLocalizedException() : base()
        {
        }

        public UnauthorizedAccessLocalizedException(string message) : base(message)
        {
        }

        public UnauthorizedAccessLocalizedException(string message, Exception ex) : base(message, ex)
        {
        }

        public UnauthorizedAccessLocalizedException(Func<string> messageFn) : base(GetEnglishText(messageFn))
        {
            LocalizedMessage = messageFn();
        }

        public UnauthorizedAccessLocalizedException(Func<string> messageFn, Exception ex) : base(GetEnglishText(messageFn), ex)
        {
            LocalizedMessage = messageFn();
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected UnauthorizedAccessLocalizedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            LocalizedMessage = info.GetString("LocalizedMessage");
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("LocalizedMessage", LocalizedMessage);

            base.GetObjectData(info, context);
        }

        static CultureInfo EnglishCulture;

        static private string GetEnglishText( Func<string> fn )
        {
            CultureInfo restore = null;

            try
            {
                if (EnglishCulture == null)
                    EnglishCulture = new CultureInfo(1033); // 1033 is the English LCID

                restore = System.Threading.Thread.CurrentThread.CurrentUICulture;
                System.Threading.Thread.CurrentThread.CurrentUICulture = EnglishCulture;

                return fn();
            }
            finally
            {
                if (restore != null)
                    System.Threading.Thread.CurrentThread.CurrentUICulture = restore;
            }
        }
}