﻿
public class CommonConstants
{
	public const int MIN_ROWS_NUMBER = 20;
    public const string SNMPV3CredentialsChangedCommandName = "SNMPV3CredentialsChangedCommand";
    public const string WorkingDayOfWeekInterval = "1-5";
}
