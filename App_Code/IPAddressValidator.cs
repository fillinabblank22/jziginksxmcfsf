﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace SolarWinds.Orion.Web.Controls
{
	public class IPAddressValidator : RegularExpressionValidator
	{
		public IPAddressValidator()
		{
			this.ErrorMessage = "Invalid format. Must be an IP Address";
			this.ValidationExpression = @"^0*([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.0*([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
			this.Display = ValidatorDisplay.Dynamic;
		}
	}
}