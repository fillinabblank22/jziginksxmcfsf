﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using SolarWinds.Orion.Core.Web;

namespace SolarWinds.Orion.Web.Controls
{
    public class ToolsetImage : ToolsetLink
    {
        [PersistenceMode(PersistenceMode.Attribute)]
        public new string ImageUrl { get; set; }

        [PersistenceMode(PersistenceMode.Attribute)]
        public string ImageAlt { get; set; }

        [PersistenceMode(PersistenceMode.Attribute)]
        public string OnClientClick { get; set; }

        [PersistenceMode(PersistenceMode.Attribute)]
        public string OnClientMouseOver { get; set; }

        [PersistenceMode(PersistenceMode.Attribute)]
        public string OnClientMouseOut { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            var image = new Image();
            
            image.AlternateText = this.ImageAlt;
            image.ImageUrl = this.ImageUrl;
            
            image.Attributes.Add("IP", this.IPAddress);
            
            if (!String.IsNullOrEmpty(this.SysName.Trim()))
                image.Attributes.Add("NodeHostName", this.SysName.Trim());

            image.Attributes.Add("Community", FormatHelper.GetGuidParamString(this.CommunityGUID));

            if (!String.IsNullOrEmpty(this.InterfaceIndex))
            {
                image.Attributes.Add("IFName", this.InterfaceName);
                image.Attributes.Add("IFIndex", this.InterfaceIndex);
            }

            image.Attributes.Add("onclick", this.OnClientClick);
            image.Attributes.Add("onmouseover", this.OnClientMouseOver);
            image.Attributes.Add("onmouseout", this.OnClientMouseOut);
 
            this.Controls.Add(image);
        }
    }
}
