﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace SolarWinds.Orion.Web.Extensions
{
    /// <summary>
    /// Summary description for WebControlExtensions
    /// </summary>
    public static class WebControlExtensions
    {

        const string JASMINE_CSS_LINK_ID = "Jasmine";

        private static string [] libSources = new[] { 
                    "~/Orion/js/Tests/lib/jasmine-2.2.0/jasmine.js",
                    "~/Orion/js/Tests/lib/jasmine-2.2.0/jasmine-html.js",
                    "~/Orion/js/Tests/lib/jasmine-2.2.0/boot.js"
                };
        private static object locker = new object();

        private static HtmlGenericControl CreateScript(string src) {
            var script = new HtmlGenericControl("script");
            script.Attributes["src"] = VirtualPathUtility.ToAbsolute(src);
            script.Attributes["type"] = "text/javascript";
            return script;
        }
        public static void IncludeTests(this Control target, IEnumerable<string> sources, IEnumerable<string> specs)
        {
            if (!target.IsEmbeddedJavaScriptTestsEnabled()) {
                return;
            }

            var header = target.Page.Header;
            lock (locker)
            {
                
                var css = header.FindControl(JASMINE_CSS_LINK_ID) as HtmlGenericControl;

                if (css == null)
                {
                    //  Include Jasmine libraries

                    

                    css = new HtmlGenericControl("link");
                    css.Attributes["rel"] = "stylesheet";
                    css.Attributes["href"] = target.Page.ResolveUrl("~/Orion/js/Tests/lib/jasmine-2.2.0/jasmine.css");
                    css.Attributes["type"] = "text/css";
                    css.ID = JASMINE_CSS_LINK_ID;

                    header.Controls.Add(css);



                    foreach (var src in libSources)
                    {
                        var script = CreateScript(src);
                        header.Controls.Add(script);
                    }

                }
            }

            if (sources != null)
            {
                foreach (var src in sources)
                {
                    var script = CreateScript(src);
                    header.Controls.Add(script);
                }
            }

            if (specs != null) {
                foreach (var src in specs)
                {
                    var script = CreateScript(src);
                    header.Controls.Add(script);
                }
            }
        }

        public static void IncludeTests(this Control target, IEnumerable<string> sourcesAndSpecs) {
            target.IncludeTests(sourcesAndSpecs, null);
        }

        public static void IncludeTests(this Control target) {
            target.IncludeTests(null);
        }

        public static bool IsEmbeddedJavaScriptTestsEnabled(this Control target) {
            var setting = WebConfigurationManager.AppSettings["EmbeddedJavaScriptTestsEnabled"];
            var enabled = false;
            if (!string.IsNullOrEmpty(setting))
            {
                Boolean.TryParse(setting, out enabled);
            }
            return enabled;
        }


        public static void AddCssClass(this WebControl target, string cssClass) {
            if (!target.CssClass.Contains(cssClass))
            {
                target.CssClass = cssClass + " " + target.CssClass;
            }
        }

        public static void RemoveCssClass(this WebControl target, string cssClass) {

            if (target.CssClass.Contains(cssClass))
            {
                target.CssClass = target.CssClass.Replace(cssClass, "").Trim(); ;
            }

            
        }
    }

}