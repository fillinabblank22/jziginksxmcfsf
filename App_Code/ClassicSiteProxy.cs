//This file is a placeholder for modules that don't support changes made for 9.5

using System;
using System.Web;


/// <summary>
/// Summary description for ClassicSiteProxy
/// </summary>
public static class ClassicSiteProxy
{
    public static string FetchPage(HttpContext context, string pathAndQuery)
    {
        return FetchPage(context, pathAndQuery, false);
    }

    public static string FetchPage(HttpContext context, string pathAndQuery, bool allowPostThrough)
    {
        return FetchPage(context, pathAndQuery, allowPostThrough, false);
    }

    public static string FetchPage(HttpContext context, string pathAndQuery, bool allowPostThrough, bool followRedirects)
    {
	return "Unsupported Resource: " + pathAndQuery;       
    }
}
