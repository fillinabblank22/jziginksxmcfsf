﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SolarWinds.Orion.Core.Web;

namespace SolarWinds.Orion.Web.Controls
{
	public class ToolsetLink : HyperLink
	{
		private const string NodeDetailUrl = "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:";
		private const string InterfaceDetailUrl = "/Orion/NPM/InterfaceDetails.aspx?NetObject=I:";
		private const string VolumeDetailUrl = "/Orion/NetPerfMon/VolumeDetails.aspx?NetObject=V:";

		[PersistenceMode(PersistenceMode.Attribute)]
		public string IPAddress { get; set; }

		[PersistenceMode(PersistenceMode.Attribute)]
		public string DNS { get; set; }

		[PersistenceMode(PersistenceMode.Attribute)]
		public string SysName { get; set; }

		[PersistenceMode(PersistenceMode.Attribute)]
		public string CommunityGUID { get; set; }

		[PersistenceMode(PersistenceMode.Attribute)]
		public string InterfaceName { get; set; }

		[PersistenceMode(PersistenceMode.Attribute)]
		public string InterfaceIndex { get; set; }

		[PersistenceMode(PersistenceMode.Attribute)]
		public string NodeID { get; set; }

		[PersistenceMode(PersistenceMode.Attribute)]
		public string InterfaceID { get; set; }
	
		[PersistenceMode(PersistenceMode.Attribute)]
		public string VolumeID { get; set; }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			object showToolset = HttpContext.Current.Profile.GetPropertyValue("ToolsetIntegration");
			if (showToolset != null && (bool)showToolset && !String.IsNullOrEmpty(this.IPAddress))
			{
				this.Attributes["IP"] = this.IPAddress;
				string hostName = this.DNS.Trim();
				if (String.IsNullOrEmpty(hostName))
				{
					hostName = this.SysName.Trim();
				}
				if (!String.IsNullOrEmpty(hostName))
				{
					this.Attributes["NodeHostName"] = hostName;
				}
                this.Attributes["Community"] = FormatHelper.GetGuidParamString(this.CommunityGUID);

				if (!String.IsNullOrEmpty(this.InterfaceIndex))
				{
					this.Attributes["IFName"] = this.InterfaceName;
					this.Attributes["IFIndex"] = this.InterfaceIndex;
				}	   
			}

			if (!String.IsNullOrEmpty(this.NodeID))
			{
				this.NavigateUrl = NodeDetailUrl + this.NodeID;
			}
			else if (!String.IsNullOrEmpty(this.InterfaceID))
			{
				this.NavigateUrl = InterfaceDetailUrl + this.InterfaceID;
			}
			else if (!String.IsNullOrEmpty(this.VolumeID))
			{
				this.NavigateUrl = VolumeDetailUrl + this.VolumeID;
			}
		}
	}
}
