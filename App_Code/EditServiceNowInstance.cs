﻿using SolarWinds.ESI.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml;
using SolarWinds.ESI.Data.DAL.SWIS;
using SolarWinds.SNI.Client.Web;
using SolarWinds.ESI.Common.BL;
using System.Threading;
using SolarWinds.Orion.Common;
using System.Configuration;
using SolarWinds.Orion.Core.Common.Configuration;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1), ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class EditServiceNowInstance : System.Web.Services.WebService
{
    public EditServiceNowInstance()
    {
    }

    [WebMethod(), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public Response<object> load(string instanceId)
    {
        using (var dal = new InstanceDAL())
        {
            if (String.IsNullOrEmpty(instanceId))
            {
                return GetNewInstanceResponse();
            }

            var instance = dal.Fetch(new Guid(instanceId));

            if (instance == null)
            {
                return GetNewInstanceResponse();
            }
            else
            {
                return GetInstanceResponse(dal, instance);
            }
        }
    }

    [WebMethod(EnableSession = true), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public Response<Guid> save(string id,
                               string name,
                               string url,
                               string login,
                               string password,
                               bool passwordIsChanged)
    {
        if (OrionConfiguration.IsDemoServer)
            throw new InvalidOperationException("This operation is not allowed in demo mode.");

        using (var dal = new InstanceDAL())
        {
            var instance = new Instance
            {
                Name = name,
                Url = url,
                InstanceType = SolarWinds.SNI.Common.Constants.SniInstanceType,
                OperationalState = OperationalState.Enabled
            };

            Guid idValue = Guid.Empty;

            if (!string.IsNullOrEmpty(id) && Guid.TryParse(id, out idValue))
            {
                instance.InstanceID = idValue;
            }

            var credential = GetCredential(dal: dal,
                                           instanceId: idValue,
                                           login: login,
                                           password: password,
                                           passwordIsChanged: passwordIsChanged);

            var instanceId = dal.InsertOrUpdate(instance, credential);

            if (instanceId != Guid.Empty && idValue == Guid.Empty)
            {
                var ctx = HttpContext.Current;
                if (ctx != null)
                {
                    ctx.Session["NewIntegrationInstance"] = instanceId;
                }
            }

            return new Response<Guid>
            {
                result = instanceId
            };
        }
    }


    [WebMethod(), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public Response<object> test(string id,
                                 string url,
                                 string login,
                                 string password,
                                 bool passwordIsChanged)
    {
        if (OrionConfiguration.IsDemoServer)
            throw new InvalidOperationException("This operation is not allowed in demo mode.");

        using (var dal = new InstanceDAL())
        {
            Guid idValue;

            if (!Guid.TryParse(id, out idValue))
            {
                idValue = Guid.Empty;
            }

            var credential = GetCredential(dal: dal,
                                           instanceId: idValue,
                                           login: login,
                                           password: password,
                                           passwordIsChanged: passwordIsChanged);

            ConnectionHelper.ConnectionStateResponse stateResponse =
                new BLHelper().BLInvokeStatic<ConnectionHelper.ConnectionStateResponse>(SolarWinds.SNI.Common.Constants.SniInstanceType,
                                                                                        "TestConnection",
                                                                                        new Dictionary<string, object>()
                                                                                        {
                                                                                            ["url"] = url,
                                                                                            ["credential"] = credential,
                                                                                            ["culture"] = Thread.CurrentThread.CurrentUICulture.Name
                                                                                        });

            return new Response<object>
            {
                result = new
                {
                    valid = stateResponse.Success,
                    message = stateResponse.LocalizedMessage,
                    detail = stateResponse.ExceptionMessage
                }
            };
        }
    }

    [Serializable]
    public class Response<T>
    {
        public T result { get; set; }
    }

    private UsernamePasswordCredential GetCredential(InstanceDAL dal,
                                                   Guid instanceId,
                                                   string login,
                                                   string password,
                                                   bool passwordIsChanged)
    {
        UsernamePasswordCredential credential;

        if (instanceId != Guid.Empty)
        {
            credential = dal.GetCredential<UsernamePasswordCredential>(instanceId);
        }
        else
        {
            credential = new UsernamePasswordCredential(login, password);
            credential.Name = "ServiceNow Credential";
        }

        credential.Username = login;

        if (passwordIsChanged)
        {
            credential.Password = password;
        }

        return credential;
    }

    private Response<object> GetNewInstanceResponse()
    {
        return new Response<object>
        {
            result = new
            {
                id = Guid.Empty.ToString(),
                proxyEnabled = false,
                proxyAuthenticationEnabled = false
            }
        };
    }

    private Response<object> GetInstanceResponse(InstanceDAL dal, Instance instance)
    {
        UsernamePasswordCredential creds;

        if (OrionConfiguration.IsDemoServer)
        {
            creds = new UsernamePasswordCredential()
            {
                ID = 1234,
                Username = ConfigurationManager.AppSettings["ServiceNowDemoUser"],
                Password = "afdsdfdsfdsfsaa"
            };
        }
        else
        {
            creds = dal.GetCredential<UsernamePasswordCredential>(instance.CredentialID);
        }

        return new Response<object>
        {
            result = new
            {
                id = instance.InstanceID,
                name = instance.Name,
                url = instance.Url,
                login = creds.Username,
                passwordLength = string.IsNullOrEmpty(creds.Password) ? 0 : creds.Password.Length
            }
        };
    }
}

