/*!
 * @solarwinds/licensing-frontend 1.8.0-285
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(3);
var config_1 = __webpack_require__(4);
var index_1 = __webpack_require__(5);
var index_2 = __webpack_require__(12);
var index_3 = __webpack_require__(26);
index_3.default(app_1.default);
index_2.default(app_1.default);
config_1.default(app_1.default);
index_1.default(app_1.default);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsaUNBQWlDOztBQUVqQyw2QkFBOEI7QUFDOUIsbUNBQThCO0FBQzlCLDBDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMsd0NBQW9DO0FBRXBDLGVBQU0sQ0FBQyxhQUFTLENBQUMsQ0FBQztBQUNsQixlQUFLLENBQUMsYUFBUyxDQUFDLENBQUM7QUFDakIsZ0JBQU0sQ0FBQyxhQUFTLENBQUMsQ0FBQztBQUNsQixlQUFRLENBQUMsYUFBUyxDQUFDLENBQUMifQ==

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
angular.module("licensing.services", []);
angular.module("licensing.components", []);
angular.module("licensing.filters", []);
angular.module("licensing.providers", []);
angular.module("licensing", [
    "orion",
    "orion-ui-components",
    "licensing.services",
    "licensing.components",
    "licensing.filters",
    "licensing.providers"
]);
var licensing = Xui.registerModule("licensing");
exports.default = licensing;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxpQ0FBaUM7O0FBRWpDLE9BQU8sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUMsRUFBRSxDQUFDLENBQUM7QUFDeEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsRUFBQyxFQUFFLENBQUMsQ0FBQztBQUMxQyxPQUFPLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ3ZDLE9BQU8sQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUMsRUFBRSxDQUFDLENBQUM7QUFDekMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUU7SUFDcEIsT0FBTztJQUNQLHFCQUFxQjtJQUNyQixvQkFBb0I7SUFDcEIsc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixxQkFBcUI7Q0FDNUIsQ0FBQyxDQUFDO0FBRUgsSUFBTSxTQUFTLEdBQUcsR0FBRyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUNsRCxrQkFBZSxTQUFTLENBQUMifQ==

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
/**
 * Configuration file for repository module
 **/
run.$inject = ["$log", "$rootScope"];
config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log, $rootScope) {
    $log.info("Run module: Licensing");
}
/** @ngInject */
function config($stateProvider) {
    $stateProvider.state("licensing", {
        abstract: true,
        url: "/licensing",
        template: "<div ui-view></div>"
    });
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run)
        .config(config);
};
exports.default.$inject = ["module"];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxpQ0FBaUM7QUFDakM7O0lBRUk7O0FBS0osZ0JBQWdCO0FBQ2hCLGFBQWEsSUFBb0IsRUFBRSxVQUFnQztJQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7QUFDdkMsQ0FBQztBQUVELGdCQUFnQjtBQUNoQixnQkFBZ0IsY0FBeUM7SUFDckQsY0FBYyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUU7UUFDOUIsUUFBUSxFQUFFLElBQUk7UUFDZCxHQUFHLEVBQUUsWUFBWTtRQUNqQixRQUFRLEVBQUUscUJBQXFCO0tBQ2xDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFFRCxzRkFBc0Y7QUFDdEYsMEJBQTBCO0FBRTFCLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsR0FBRyxFQUFFO1NBQ1AsR0FBRyxDQUFDLEdBQUcsQ0FBQztTQUNSLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUN4QixDQUFDLENBQUMifQ==

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var licenseService_1 = __webpack_require__(6);
var navigationService_1 = __webpack_require__(10);
exports.default = function (module) {
    licenseService_1.default(module);
    navigationService_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1EQUE4QztBQUM5Qyx5REFBb0Q7QUFFcEQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLHdCQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkIsMkJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDOUIsQ0FBQyxDQUFDIn0=

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var license_service_1 = __webpack_require__(7);
exports.default = function (module) {
    module.service("licenseService", license_service_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFEQUErQztBQUUvQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSx5QkFBYyxDQUFDLENBQUM7QUFDckQsQ0FBQyxDQUFDIn0=

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var license_1 = __webpack_require__(8);
var orionServer_1 = __webpack_require__(9);
var LicenseService = /** @class */ (function () {
    function LicenseService(swApi, $timeout, $httpParamSerializer, $log, licensingConstants, swMegaMenuService) {
        this.swApi = swApi;
        this.$timeout = $timeout;
        this.$httpParamSerializer = $httpParamSerializer;
        this.$log = $log;
        this.licensingConstants = licensingConstants;
        this.swMegaMenuService = swMegaMenuService;
    }
    LicenseService.prototype.getLicenses = function () {
        var _this = this;
        this.licenses = [];
        return this.swApi.api(false).one("licensing/licenses").get()
            .then(function (response) {
            response.forEach(function (item, index) {
                _this.licenses.push(new license_1.default(item));
            });
            return _this.licenses;
        }).catch(function (e) {
            _this.licenses = null;
            return _this.licenses;
        });
    };
    LicenseService.prototype.getIsDemoServer = function () {
        return this.swApi.api(false).one("licensing/isDemoServer").get()
            .then(function (response) {
            return response;
        }).catch(function (e) {
            return false;
        });
    };
    LicenseService.prototype.syncLicenses = function () {
        var _this = this;
        return this.swApi.api(false).one("licensing/licenses").post("sync")
            .then(function () {
            return _this.getLicenses();
        }).catch(function (e) {
            return e.data;
        }).finally(function () {
            _this.swMegaMenuService.clearCache();
        });
    };
    LicenseService.prototype.deactivateLicenses = function (deactivation) {
        var _this = this;
        return this.swApi.api(false).one("licensing").post("deactivation", deactivation)
            .then(function (response) {
            return response;
        }).catch(function (e) {
            return e.data;
        }).finally(function () {
            _this.swMegaMenuService.clearCache();
        });
    };
    LicenseService.prototype.assignLicense = function (assignment) {
        return this.swApi.api(false).one("licensing").post("assignment", assignment)
            .then(function (response) {
            return response;
        }).catch(function (e) {
            return e.data;
        });
    };
    LicenseService.prototype.getValidAssignments = function (license) {
        var _this = this;
        var servers = [];
        var assignedTo = [];
        return this.swApi.api(false).one("licensing/validassignments?"
            + this.$httpParamSerializer(license.writer())).get()
            .then(function (response) {
            response.servers.forEach(function (item) {
                servers.push(new orionServer_1.default(item));
            });
            response.assignedTo.forEach(function (item) {
                assignedTo.push(new orionServer_1.default(item));
            });
            response.servers = servers;
            response.assignedTo = assignedTo;
            response.kind = response.kind.toLowerCase();
            if (response.kind === _this.licensingConstants.grid.apiSelectionMode.multi) {
                response.kind = _this.licensingConstants.grid.selectionMode.multi;
            }
            return response;
        }).catch(function (e) {
            return e.data;
        });
    };
    LicenseService.prototype.downloadDeactivationFile = function (deactivationReceipt) {
        var config = this.swApi.api(false).configuration;
        if (config.baseUrl) {
            var url = config.baseUrl + "/licensing/deactivation/receipt?receiptToken=" + deactivationReceipt;
            window.location.assign(url);
        }
    };
    LicenseService.prototype.checkOffline = function () {
        var _this = this;
        return this.swApi.api(false).one("licensing/onlinestate").get()
            .then(function (response) {
            response.message = response.message != null ? _this.replaceURLWithHTMLLinks(response.message) : null;
            return response;
        }).catch(function (e) {
            return e.data;
        });
    };
    LicenseService.prototype.replaceURLWithHTMLLinks = function (text) {
        // attach <a> tag to links, and if available, use text in brackets behind url as title
        // eg: "Blabla http://abc (title)" => "<a href='http://abc'>title</a>"
        var rxUrl = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])( \(([^)]+)\))?/i;
        return text.replace(rxUrl, function (x, url, y, z, title) { return "<a href=\"" + url + "\">" + (title || url) + "</a>"; });
    };
    LicenseService.$inject = [
        "swApi", "$timeout", "$httpParamSerializer", "$log", "licensingConstants", "swMegaMenuService"
    ];
    return LicenseService;
}());
exports.default = LicenseService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZS1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGljZW5zZS1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBSUEsd0RBQW1EO0FBS25ELCtEQUEwRDtBQUcxRDtJQU9JLHdCQUNZLEtBQW9CLEVBQ3BCLFFBQTRCLEVBQzVCLG9CQUF5QixFQUN6QixJQUFvQixFQUNwQixrQkFBNkIsRUFDN0IsaUJBQW1DO1FBTG5DLFVBQUssR0FBTCxLQUFLLENBQWU7UUFDcEIsYUFBUSxHQUFSLFFBQVEsQ0FBb0I7UUFDNUIseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFLO1FBQ3pCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBQ3BCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBVztRQUM3QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO0lBRS9DLENBQUM7SUFFTSxvQ0FBVyxHQUFsQjtRQUFBLGlCQWFDO1FBWkcsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEdBQUcsRUFBRTthQUN2RCxJQUFJLENBQUMsVUFBQyxRQUFtQjtZQUN0QixRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBYSxFQUFFLEtBQWE7Z0JBQzFDLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksaUJBQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzlDLENBQUMsQ0FBQyxDQUFDO1lBRUgsTUFBTSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBTTtZQUNaLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLHdDQUFlLEdBQXRCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLEdBQUcsRUFBRTthQUMvRCxJQUFJLENBQUMsVUFBQyxRQUFpQjtZQUNwQixNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQU07WUFDWixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLHFDQUFZLEdBQW5CO1FBQUEsaUJBU0M7UUFSRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUM5RCxJQUFJLENBQUM7WUFDRixNQUFNLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQU07WUFDWixNQUFNLENBQW9CLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDckMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1lBQ1AsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLDJDQUFrQixHQUF6QixVQUEwQixZQUEwQjtRQUFwRCxpQkFTQztRQVJHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxZQUFZLENBQUM7YUFDM0UsSUFBSSxDQUFDLFVBQUMsUUFBMkI7WUFDOUIsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxDQUFNO1lBQ1osTUFBTSxDQUFvQixDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNQLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSxzQ0FBYSxHQUFwQixVQUFxQixVQUE2QjtRQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDO2FBQ3ZFLElBQUksQ0FBQyxVQUFDLFFBQTJCO1lBQzlCLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBTTtZQUNaLE1BQU0sQ0FBb0IsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSw0Q0FBbUIsR0FBMUIsVUFBMkIsT0FBZ0I7UUFBM0MsaUJBd0JDO1FBdkJHLElBQU0sT0FBTyxHQUFrQixFQUFFLENBQUM7UUFDbEMsSUFBTSxVQUFVLEdBQWtCLEVBQUUsQ0FBQztRQUNyQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLDZCQUE2QjtjQUN4RCxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUU7YUFDL0MsSUFBSSxDQUFDLFVBQUMsUUFBK0I7WUFDbEMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFpQjtnQkFDdkMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLHFCQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztZQUNILFFBQVEsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBaUI7Z0JBQzFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxxQkFBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxDQUFDLENBQUM7WUFFSCxRQUFRLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUMzQixRQUFRLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztZQUNqQyxRQUFRLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFFNUMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksS0FBSyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLFFBQVEsQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDO1lBQ3JFLENBQUM7WUFDRCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQU07WUFDWixNQUFNLENBQW9CLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDckMsQ0FBQyxDQUFDLENBQUM7SUFDZixDQUFDO0lBRU0saURBQXdCLEdBQS9CLFVBQWdDLG1CQUEyQjtRQUN2RCxJQUFNLE1BQU0sR0FBUyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQVMsQ0FBQyxhQUFhLENBQUM7UUFDakUsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDakIsSUFBTSxHQUFHLEdBQWMsTUFBTSxDQUFDLE9BQU8scURBQWdELG1CQUFxQixDQUFDO1lBQzNHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLENBQUM7SUFDTCxDQUFDO0lBRU0scUNBQVksR0FBbkI7UUFBQSxpQkFRQztRQVBHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxHQUFHLEVBQUU7YUFDMUQsSUFBSSxDQUFDLFVBQUMsUUFBMkI7WUFDOUIsUUFBUSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3BHLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBTTtZQUNaLE1BQU0sQ0FBb0IsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTyxnREFBdUIsR0FBL0IsVUFBZ0MsSUFBWTtRQUN4QyxzRkFBc0Y7UUFDdEYsc0VBQXNFO1FBQ3RFLElBQU0sS0FBSyxHQUFXLDJGQUEyRixDQUFDO1FBQ2xILE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxVQUFDLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLElBQUssT0FBQSxlQUFZLEdBQUcsWUFBSyxLQUFLLElBQUksR0FBRyxVQUFNLEVBQXRDLENBQXNDLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBeEhhLHNCQUFPLEdBQUc7UUFDcEIsT0FBTyxFQUFFLFVBQVUsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLEVBQUUsb0JBQW9CLEVBQUUsbUJBQW1CO0tBQ2pHLENBQUM7SUF1SE4scUJBQUM7Q0FBQSxBQTFIRCxJQTBIQztrQkExSG9CLGNBQWMifQ==

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var License = /** @class */ (function () {
    function License(model) {
        var _this = this;
        this.assignedServers = null;
        this.assigned = true;
        /*
            * Method returns modified License object that is required by License service
            */
        this.writer = function () {
            if (!_this.licenseKey) {
                _this.licenseKey = "0000-0000-0000-0000-0000-0000-0000-0000";
            }
            return {
                licenseKey: _this.licenseKey,
                productName: _this.productName,
                licenseVersion: _this.licenseVersion
            };
        };
        this.getItemClass = function (days) {
            var itemClass = "bold";
            if (days === 0) {
                itemClass = "expired";
            }
            else if (days > 0 && days <= 15) {
                itemClass = "critical";
            }
            else if (days > 15 && days <= 30) {
                itemClass = "warning";
            }
            return itemClass;
        };
        if (model) {
            _.extend(this, model);
        }
        this.installed = this.productVersion !== null;
        if (this.isTempKey) {
            this.licenseType = "Temporary";
        }
        if (this.expiration) {
            this.licenseDaysClass = this.getItemClass(this.expiration.licenseDays);
            this.maintenanceDaysClass = this.getItemClass(this.expiration.maintenanceDays);
            this.isPerpetual = new Date(this.expiration.license).getFullYear() >= 9999;
        }
        if (this.assignedTo && this.assignedTo.length > 0) {
            this.assignedServers = _.map(this.assignedTo, function (o) { return o["hostName"]; }).join(",");
        }
    }
    return License;
}());
exports.default = License;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxpY2Vuc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFPdkM7SUF1QkksaUJBQVksS0FBZTtRQUEzQixpQkFvQkM7UUEzQk0sb0JBQWUsR0FBVyxJQUFJLENBQUM7UUFDL0IsYUFBUSxHQUFZLElBQUksQ0FBQztRQTRCaEM7O2NBRU07UUFDQyxXQUFNLEdBQUc7WUFDWixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixLQUFJLENBQUMsVUFBVSxHQUFHLHlDQUF5QyxDQUFDO1lBQ2hFLENBQUM7WUFFRCxNQUFNLENBQUM7Z0JBQ0gsVUFBVSxFQUFFLEtBQUksQ0FBQyxVQUFVO2dCQUMzQixXQUFXLEVBQUUsS0FBSSxDQUFDLFdBQVc7Z0JBQzdCLGNBQWMsRUFBRyxLQUFJLENBQUMsY0FBYzthQUN2QyxDQUFDO1FBQ04sQ0FBQyxDQUFDO1FBRU0saUJBQVksR0FBRyxVQUFDLElBQVk7WUFDaEMsSUFBSSxTQUFTLEdBQVUsTUFBTSxDQUFDO1lBQzlCLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNiLFNBQVMsR0FBRyxTQUFTLENBQUM7WUFDMUIsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxTQUFTLEdBQUcsVUFBVSxDQUFDO1lBQzNCLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsSUFBSSxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDakMsU0FBUyxHQUFHLFNBQVMsQ0FBQztZQUMxQixDQUFDO1lBQ0QsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUNyQixDQUFDLENBQUM7UUE5Q0UsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNSLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFCLENBQUM7UUFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDO1FBRTlDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQ25DLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUM7WUFFL0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFdBQVcsRUFBRSxJQUFJLElBQUksQ0FBQztRQUMvRSxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFVBQUMsQ0FBYyxJQUFPLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0csQ0FBQztJQUNMLENBQUM7SUE0QkwsY0FBQztBQUFELENBQUMsQUF2RUQsSUF1RUMifQ==

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionServer = /** @class */ (function () {
    function OrionServer(model) {
        this.iconMap = [
            "orion-sitemaster",
            "orion-sitemaster-backup",
            "orion-ape",
            "orion-ape-backup",
            "orion-webserver"
        ];
        this.statusIconMap = [
            "unknown",
            "up",
            "down"
        ];
        if (model) {
            _.extend(this, model);
        }
        this.icon = this.iconMap[this.status];
        this.statusIcon = this.statusIconMap[this.status];
        this.displayType = this.type.split(/(?=[A-Z])/).join(" ");
    }
    return OrionServer;
}());
exports.default = OrionServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25TZXJ2ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmlvblNlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QztJQXdCSSxxQkFBWSxLQUFtQjtRQXZCeEIsWUFBTyxHQUFHO1lBQ2Isa0JBQWtCO1lBQ2xCLHlCQUF5QjtZQUN6QixXQUFXO1lBQ1gsa0JBQWtCO1lBQ2xCLGlCQUFpQjtTQUNwQixDQUFDO1FBRUssa0JBQWEsR0FBRztZQUNuQixTQUFTO1lBQ1QsSUFBSTtZQUNKLE1BQU07U0FDVCxDQUFDO1FBWUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNSLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFCLENBQUM7UUFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUNMLGtCQUFDO0FBQUQsQ0FBQyxBQWpDRCxJQWlDQyJ9

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var navigation_service_1 = __webpack_require__(11);
exports.default = function (module) {
    module.service("navigationService", navigation_service_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJEQUFxRDtBQUVyRCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSw0QkFBaUIsQ0FBQyxDQUFDO0FBQzNELENBQUMsQ0FBQyJ9

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NavigationService = /** @class */ (function () {
    function NavigationService($state, $window, licensingConstants) {
        var _this = this;
        this.$state = $state;
        this.$window = $window;
        this.licensingConstants = licensingConstants;
        this.navigateToOrionProxySettingsPage = function () {
            _this.$window.open(_this.licensingConstants.links.httpProxySettings, "_blank");
        };
    }
    NavigationService.$inject = ["$state", "$window", "licensingConstants"];
    return NavigationService;
}());
exports.default = NavigationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibmF2aWdhdGlvbi1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFHSSwyQkFBb0IsTUFBMkIsRUFBVSxPQUEwQixFQUMvRCxrQkFBNkI7UUFEakQsaUJBRUM7UUFGbUIsV0FBTSxHQUFOLE1BQU0sQ0FBcUI7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFtQjtRQUMvRCx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQVc7UUFHMUMscUNBQWdDLEdBQUc7WUFDdEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNqRixDQUFDLENBQUM7SUFKRixDQUFDO0lBSmEseUJBQU8sR0FBRyxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztJQVN4RSx3QkFBQztDQUFBLEFBVkQsSUFVQztrQkFWb0IsaUJBQWlCIn0=

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var license_manager_1 = __webpack_require__(13);
exports.default = function (module) {
    license_manager_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFEQUErQztBQUUvQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IseUJBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUMzQixDQUFDLENBQUMifQ==

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var licenseManager_config_1 = __webpack_require__(14);
var offline_1 = __webpack_require__(21);
__webpack_require__(23);
__webpack_require__(24);
__webpack_require__(25);
exports.default = function (module) {
    module.config(licenseManager_config_1.default);
    offline_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlFQUEyRDtBQUMzRCxxQ0FBZ0M7QUFDaEMsaUNBQStCO0FBQy9CLG1EQUFpRDtBQUNqRCwrQ0FBNkM7QUFFN0Msa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQUMsK0JBQW9CLENBQUMsQ0FBQztJQUNwQyxpQkFBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3BCLENBQUMsQ0FBQyJ9

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var licenseManager_controller_1 = __webpack_require__(15);
function LicenseManagerConfig($stateProvider) {
    $stateProvider
        .state("license-manager", {
        title: "License Manager",
        url: "/licensing/license-manager?offline?fromHa",
        controller: licenseManager_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(20),
        resolve: {
            licenses: ["licenseService", function (licenseService) {
                    return licenseService.getLicenses();
                }],
            availabilityActionResult: ["licenseService", function (licenseService) {
                    return licenseService.checkOffline();
                }]
        }
    });
}
LicenseManagerConfig.$inject = ["$stateProvider"];
exports.default = LicenseManagerConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZU1hbmFnZXItY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGljZW5zZU1hbmFnZXItY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsdUNBQXVDO0FBQ3ZDLHlFQUFtRTtBQUduRSw4QkFBOEIsY0FBb0M7SUFDOUQsY0FBYztTQUNULEtBQUssQ0FBQyxpQkFBaUIsRUFDZjtRQUNHLEtBQUssRUFBRSxpQkFBaUI7UUFDeEIsR0FBRyxFQUFFLDJDQUEyQztRQUNoRCxVQUFVLEVBQUUsbUNBQXdCO1FBQ3BDLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQUMsdUJBQXVCLENBQUM7UUFDMUMsT0FBTyxFQUFFO1lBQ0wsUUFBUSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsVUFBQyxjQUE4QjtvQkFDdkQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDekMsQ0FBQyxDQUFDO1lBQ0Ysd0JBQXdCLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxVQUFDLGNBQThCO29CQUN2RSxNQUFNLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUMxQyxDQUFDLENBQUM7U0FDTDtLQUNKLENBQUMsQ0FBQztBQUNuQixDQUFDO0FBRUQsb0JBQW9CLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztBQUNsRCxrQkFBZSxvQkFBb0IsQ0FBQyJ9

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var availabilityState_1 = __webpack_require__(16);
var LicenseManagerController = /** @class */ (function () {
    function LicenseManagerController($scope, $state, xuiDialogService, xuiWizardDialogService, _t, licenses, availabilityActionResult, licenseService, xuiToastService, $timeout, $interval, sortingService, 
        //TODO remove after demo
        $stateParams, licensingConstants) {
        var _this = this;
        this.$scope = $scope;
        this.$state = $state;
        this.xuiDialogService = xuiDialogService;
        this.xuiWizardDialogService = xuiWizardDialogService;
        this._t = _t;
        this.licenses = licenses;
        this.availabilityActionResult = availabilityActionResult;
        this.licenseService = licenseService;
        this.xuiToastService = xuiToastService;
        this.$timeout = $timeout;
        this.$interval = $interval;
        this.sortingService = sortingService;
        this.$stateParams = $stateParams;
        this.licensingConstants = licensingConstants;
        this.gridPagination = {
            page: 1,
            pageSize: 10
        };
        this.sortingData = {
            sortBy: { id: "licenseName", label: this._t("Product Name") },
            sortableColumns: [
                { id: "licenseName", label: this._t("Product Name") },
                { id: "productVersion", label: this._t("Installed Version") },
                { id: "licenseType", label: this._t("License Type") },
                { id: "expiration.licenseDays", label: this._t("Expiration Date") }
            ]
        };
        this.selectionMode = "multi";
        this.smartMode = true;
        this.$onInit = function () {
            _this.$scope.licenses = _this.licenses;
            _this.$scope.selection = { items: [] };
            _this.smartItemsSourceLicenses = [];
            _this.availabilityState = availabilityState_1.default[_this.availabilityActionResult.status];
            //TODO remove after demo
            _this.params = _this.$stateParams;
            //TODO remove params after demo
            _this.$scope.isOnline = _this.params.offline ? false : _this.availabilityState === availabilityState_1.default.Online;
            _this.$scope.licenseActivationMode = _this.$scope.isOnline ? "online" : "offline";
            _this.$scope.anchorTarget = _this.params.fromHa ? "_self" : "_blank";
            _this.isBusy = false;
            if (angular.isDefined(_this.$scope.$watch)) {
                _this.$scope.$watch("selection", _this.launchGridRefreshTimer, true);
            }
            _this.licenseService.getIsDemoServer().then(function (result) {
                _this.$scope["demoMode"] = result;
            });
            _this.launchGridRefreshTimer();
            _this.checkStoreAvailability();
        };
        this.checkStoreAvailability = function () {
            if (!_this.isStoreAvailable()) {
                _this.xuiToastService.error(_this.availabilityActionResult.message, _this._t("License Server Error"), { stickyError: true });
            }
        };
        this.isStoreAvailable = function () {
            return _this.availabilityActionResult.status !== "PrimaryLicenseServerOffline" &&
                _this.availabilityActionResult.status !== "CorruptStore" &&
                _this.availabilityActionResult.status !== "SecurityFailure";
        };
        this.activateLicenseClick = function (license) {
            if (license === void 0) { license = null; }
            if (license) {
                _this.activateButtonTitle = _this._t("Activate");
                _this.activateDialogTitle = _this._t("Activate License");
                _this.$scope.activationLicense = _.clone(license);
            }
            else {
                //only one license can be activated at once
                if (_this.$scope.selection.items.length === 1) {
                    _this.$scope.activationLicense = _.clone(_this.$scope.selection.items[0]);
                }
                else {
                    _this.$scope.activationLicense = null;
                }
            }
        };
        this.deactivateLicenseClick = function () {
            _this.$scope.errorMessage = "";
            _this.$scope.deactivationLicenses = _.clone(_this.$scope.selection.items);
            _this.$scope.deactivationLicenses.forEach(function (item, index) {
                if (item.licenseKey === "") {
                    item.licenseKey = _this.licensingConstants.license.defaultKey;
                }
            });
            var title = _this.$scope.isOnline ? _this._t("Deactivate License") :
                _this._t("Deactivate License(No Internet Connection)");
            var actionFunction = _this.$scope.isOnline ? _this.deactivateLicenses : _this.deactivateLicensesOffline;
            _this.showDialog(title, _this._t("Deactivate"), _this._t("Deactivating license, please wait..."), __webpack_require__(17), actionFunction, "warning");
        };
        this.assignLicenseClick = function (license) {
            if (license === void 0) { license = null; }
            _this.$scope.errorMessage = "";
            if (_this.$scope.selection.items[0].licenseKey === "") {
                _this.$scope.selection.items[0].licenseKey = _this.licensingConstants.license.defaultKey;
            }
            return _this.licenseService.getValidAssignments(_this.$scope.selection.items[0]).then(function (response) {
                _this.$scope.validAssignment = response;
                if (!_.isEmpty(response.servers)) {
                    _this.$scope.orionServersSelection = response.assignedTo;
                }
                if (_this.orionServersSelectionWatch) {
                    _this.orionServersSelectionWatch(); //deregister watch
                }
                _this.orionServersSelectionWatch = _this.$scope.$watch("orionServersSelection", _this.validate, true);
                _this.setAssignmentsGridSortProperties();
                _this.showDialog(_this._t("Assign License"), _this._t("Assign"), _this._t("Assigning license, please wait..."), __webpack_require__(18), _this.assignLicense);
            });
        };
        this.setAssignmentsGridSortProperties = function () {
            _this.$scope.assignmentsGridSortingData = {
                sortableColumns: [{ name: "hostName", displayValue: _this._t("Host Name") },
                    { name: "status", displayValue: _this._t("Status") }],
                sortDirection: "asc",
                selectedItem: { name: "hostName", displayValue: _this._t("Host Name") }
            };
            _this.$scope.assignmentsGridSortingData.sortChanged = function () {
                _this.$scope.validAssignment.servers = _this.sortingService.sortBy(_this.$scope.validAssignment.servers, _this.$scope.assignmentsGridSortingData.selectedItem.name, _this.$scope.assignmentsGridSortingData.sortDirection);
            };
        };
        this.onLicenseActivate = function (result) {
            if (result.status.toLowerCase() === "success") {
                _this.refreshLicensesGrid().then(function (licenses) {
                    var activatedLicense = null;
                    if (result.activatedLicenses && result.activatedLicenses.length > 0) {
                        activatedLicense = result.activatedLicenses[0];
                    }
                    _this.showToastMessage(result, _this._t("Activation"), activatedLicense);
                    if (!_.isEmpty(result.additionalInfoMessage)) {
                        _this.xuiDialogService.showMessage({ title: _this._t("License activation"), message: result.additionalInfoMessage });
                    }
                });
            }
        };
        this.deactivateLicenses = function () {
            var deactivation = { licenses: _this.$scope.deactivationLicenses };
            return _this.licenseService.deactivateLicenses(deactivation).then(function (result) {
                _this.actionResult = result;
                if (result.status.toLowerCase() === "success") {
                    _this.refreshLicensesGrid();
                }
                return _this.showToastMessage(result, _this._t("Deactivation"));
            });
        };
        this.assignLicense = function () {
            var selectedServersIds = null;
            if (_this.$scope.orionServersSelection
                && _this.$scope.validAssignment.kind === _this.licensingConstants.grid.selectionMode.single) {
                var orionServerId = _this.$scope.orionServersSelection[0].id;
                selectedServersIds = [{ id: orionServerId }];
            }
            else if (_this.$scope.orionServersSelection.length > 0
                && _this.$scope.validAssignment.kind === _this.licensingConstants.grid.selectionMode.multi) {
                selectedServersIds = _this.$scope.orionServersSelection.map(function (prop) {
                    return { id: prop.id };
                });
            }
            if (selectedServersIds !== null) {
                var selectedLicense_1 = _.clone(_this.$scope.selection.items[0]);
                var assignment = { license: selectedLicense_1, assignTo: selectedServersIds };
                return _this.licenseService.assignLicense(assignment).then(function (result) {
                    if (_.isEmpty(result)) {
                        result = { status: "success" };
                    }
                    _this.actionResult = result;
                    if (result.status && result.status.toLowerCase() === "success") {
                        return _this.refreshLicensesGrid().then(function () {
                            return _this.showToastMessage(result, _this._t("Assignment"), selectedLicense_1);
                        });
                    }
                    else {
                        _this.showToastMessage(result, _this._t("Assignment"), selectedLicense_1);
                    }
                });
            }
        };
        this.deactivateLicensesOffline = function () {
            var deactivation = { licenses: _this.$scope.deactivationLicenses,
                provideReceipt: !_this.$scope.isOnline };
            return _this.licenseService.deactivateLicenses(deactivation).then(function (result) {
                _this.actionResult = result;
                if (!_.isEmpty(result.receiptToken)) {
                    _this.licenseService.downloadDeactivationFile(result.receiptToken);
                    _this.refreshLicensesGrid();
                }
                return _this.showToastMessage(result, _this._t("Offline Deactivation"));
            });
        };
        this.offlineLicenseClick = function () {
            //TODO remove after demo
            _this.availabilityState = _this.params.offline ? parseInt(_this.params.offline, 10) : _this.availabilityState;
            var dialogOptions = {
                headerText: null,
                title: _this._t("Cannot Resolve Licensing Server"),
                status: "warning",
                hideCancel: true,
                buttons: [{
                        name: "close",
                        isPrimary: true,
                        text: _this._t("Close"),
                        action: function (dialogResult) {
                            return true;
                        }
                    }]
            };
            _this.xuiDialogService.show({
                template: __webpack_require__(19)
            }, dialogOptions);
        };
        this.synchronizeLicenses = function () {
            _this.isBusy = true;
            _this.$scope.selection = { items: [] };
            return _this.licenseService.syncLicenses().then(function (licenses) {
                var isDemoMode = _this.$scope["demoMode"];
                if (!isDemoMode) {
                    _this.$scope.licenses = licenses;
                }
                _this.isBusy = false;
                if (_this.$scope.licenses !== null && !isDemoMode) {
                    return _this.showToastMessage(({ status: "Success" }), _this._t("Synchronization"));
                }
            }, function () {
                _this.isBusy = false;
            });
        };
        this.deactivateButtonDisabled = function () {
            if (_this.$scope.selection.items.length === 1) {
                if (_this.$scope.selection.items[0].licenseType === _this.licensingConstants.license.types.evaluation) {
                    return true;
                }
            }
            var selectionItems = _this.$scope.selection.items;
            if (selectionItems.length === 0) {
                return true;
            }
            return selectionItems.some(function (item) {
                return item.canDeactivate === false;
            });
        };
        this.assignButtonDisabled = function () {
            if (_this.$scope.selection.items.length !== 1) {
                return true;
            }
            return _this.$scope.selection.items[0].canAssign === false;
        };
        this.synchronizeButtonDisabled = function () {
            var isDemoMode = _this.$scope["demoMode"];
            return !_this.$scope.isOnline || isDemoMode;
        };
        this.activateButtonIsShown = function () {
            if (_this.$scope.selection.items.length === 0) {
                _this.activateButtonTitle = _this._t("Add License");
                _this.activateDialogTitle = _this._t("Add License");
                if (_this.$scope.isOnline) {
                    _this.activateButtonTitle = _this._t("Add/Upgrade License");
                    _this.activateDialogTitle = _this._t("Add/Upgrade License");
                }
            }
            else {
                if (_this.$scope.selection.items.length === 1) {
                    if (_this.$scope.selection.items[0].licenseType === _this.licensingConstants.license.types.evaluation) {
                        _this.activateButtonTitle = _this._t("Activate");
                        _this.activateDialogTitle = _this._t("Activate License");
                    }
                    else {
                        _this.activateButtonTitle = _this._t("Update");
                        _this.activateDialogTitle = _this._t("Update License");
                    }
                }
                if (_this.$scope.selection.items.length > 1) {
                    return false;
                }
            }
            return true;
        };
        this.deactivateButtonIsShown = function () {
            return _this.$scope.selection.items.length !== 0;
        };
        this.assignButtonIsShown = function () {
            if (_this.$scope.selection.items.length === 1) {
                if (_this.$scope.selection.items[0].licenseType !== _this.licensingConstants.license.types.evaluation) {
                    return true;
                }
            }
            return false;
        };
        this.synchronizeButtonIsShown = function () {
            return true;
        };
        this.launchGridRefreshTimer = function () {
            _this.$timeout(function () {
                if (!_.isEmpty(_this.licensesRefreshTimer)) {
                    _this.$interval.cancel(_this.licensesRefreshTimer);
                    _this.licensesRefreshTimer = null;
                }
                //do not refresh grid if any of licenses is selected
                if (_this.$scope.selection.items.length === 0) {
                    _this.licensesRefreshTimer = _this.$interval(function () { _this.refreshLicensesGrid(); }, _this.licensingConstants.grid.updateInterval);
                }
            }, 0);
        };
        this.refreshLicensesGrid = function () {
            _this.$scope.selection = { items: [] };
            return _this.licenseService.getLicenses().then(function (licenses) {
                if (angular.toJson(_this.$scope.licenses) !== angular.toJson(licenses)) {
                    _this.$scope.licenses = licenses;
                }
                return licenses;
            });
        };
        this.validate = function () {
            _this.$timeout(function () {
                if (_this.$scope.assignLicenseForm !== undefined) {
                    _this.actionButton.isDisabled = _.isEmpty(_this.$scope.orionServersSelection);
                }
            }, 0);
        };
        this.showToastMessage = function (response, serviceMethodName, activatedLicense) {
            if (serviceMethodName === void 0) { serviceMethodName = null; }
            if (activatedLicense === void 0) { activatedLicense = null; }
            var message = response.message;
            if (_.isEmpty(message)) {
                message = serviceMethodName + " " +
                    (response.status && response.status.toLowerCase() === "success" ?
                        _this._t("has been performed successfully") : _this._t("has not been performed successfully"));
            }
            if (response.status && response.status.toLowerCase() === "success") {
                if (activatedLicense) {
                    _this.$timeout(function () {
                        var highlightLicense = _.filter(_this.smartItemsSourceLicenses, ["licenseKey", activatedLicense.licenseKey]);
                        _this.xuiToastService.success(message, "", {}, highlightLicense);
                    }, 0);
                }
                else {
                    _this.xuiToastService.success(message);
                }
            }
            else {
                _this.$scope.errorMessage = message;
                return false;
            }
            return true;
        };
        this.showDialog = function (dialogTitle, actionButtonTitle, actionProgressMessage, templatePath, actionFunction, dialogStatus) {
            if (dialogStatus === void 0) { dialogStatus = null; }
            _this.actionButton = {
                name: "actionButton",
                text: actionButtonTitle,
                actionText: actionProgressMessage,
                isPrimary: true,
                action: actionFunction
            };
            var actionDialogOptions = {
                headerText: null,
                title: dialogTitle,
                hideCancel: false,
                viewModel: _this.$scope,
                buttons: [
                    _this.actionButton
                ]
            };
            if (dialogStatus) {
                actionDialogOptions.status = dialogStatus;
            }
            _this.xuiDialogService.show({
                template: templatePath
            }, actionDialogOptions);
        };
    }
    LicenseManagerController.$inject = ["$scope", "$state", "xuiDialogService", "xuiWizardDialogService",
        "getTextService", "licenses", "availabilityActionResult", "licenseService", "xuiToastService",
        "$timeout", "$interval", "xuiSortingService", "$stateParams", "licensingConstants"];
    return LicenseManagerController;
}());
exports.default = LicenseManagerController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZU1hbmFnZXItY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxpY2Vuc2VNYW5hZ2VyLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFXdkMsNEVBQXVFO0FBb0J2RTtJQWdDSSxrQ0FBbUIsTUFBcUIsRUFDNUIsTUFBMkIsRUFDM0IsZ0JBQXFCLEVBQ3JCLHNCQUEyQixFQUMzQixFQUFvQyxFQUNwQyxRQUFtQixFQUNwQix3QkFBMkMsRUFDMUMsY0FBOEIsRUFDOUIsZUFBa0MsRUFBVSxRQUE0QixFQUN4RSxTQUE4QixFQUFVLGNBQW1DO1FBQ25GLHdCQUF3QjtRQUNqQixZQUF1QyxFQUN2QyxrQkFBNkI7UUFaeEMsaUJBYUM7UUFia0IsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUM1QixXQUFNLEdBQU4sTUFBTSxDQUFxQjtRQUMzQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQUs7UUFDckIsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUFLO1FBQzNCLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBQ3BDLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDcEIsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUFtQjtRQUMxQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsb0JBQWUsR0FBZixlQUFlLENBQW1CO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBb0I7UUFDeEUsY0FBUyxHQUFULFNBQVMsQ0FBcUI7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBcUI7UUFFNUUsaUJBQVksR0FBWixZQUFZLENBQTJCO1FBQ3ZDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBVztRQWpDakMsbUJBQWMsR0FBUTtZQUN6QixJQUFJLEVBQUUsQ0FBQztZQUNQLFFBQVEsRUFBRSxFQUFFO1NBQ2YsQ0FBQztRQUVLLGdCQUFXLEdBQVE7WUFDdEIsTUFBTSxFQUFFLEVBQUUsRUFBRSxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsRUFBRTtZQUM3RCxlQUFlLEVBQUU7Z0JBQ2IsRUFBRSxFQUFFLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUNyRCxFQUFFLEVBQUUsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO2dCQUM3RCxFQUFFLEVBQUUsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQ3JELEVBQUUsRUFBRSxFQUFFLHdCQUF3QixFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLEVBQUU7YUFDdEU7U0FDSixDQUFDO1FBR0ssa0JBQWEsR0FBWSxPQUFPLENBQUM7UUFDakMsY0FBUyxHQUFhLElBQUksQ0FBQztRQW1CM0IsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQztZQUNyQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBc0IsRUFBQyxLQUFLLEVBQWEsRUFBRSxFQUFDLENBQUM7WUFDbEUsS0FBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztZQUVuQyxLQUFJLENBQUMsaUJBQWlCLEdBQVMsMkJBQWtCLENBQUMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3hGLHdCQUF3QjtZQUN4QixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUM7WUFDaEMsK0JBQStCO1lBQy9CLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsS0FBSywyQkFBaUIsQ0FBQyxNQUFNLENBQUM7WUFDekcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDaEYsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1lBQ25FLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBRXBCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDdkUsQ0FBQztZQUVELEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtnQkFDN0MsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsR0FBRyxNQUFNLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUU5QixLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUNsQyxDQUFDLENBQUM7UUFFTSwyQkFBc0IsR0FBRztZQUM3QixFQUFFLENBQUMsQ0FBRSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQ3RCLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLEVBQ3JDLEtBQUksQ0FBQyxFQUFFLENBQUMsc0JBQXNCLENBQUMsRUFDL0IsRUFBQyxXQUFXLEVBQUUsSUFBSSxFQUFDLENBQ3RCLENBQUM7WUFDTixDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUsscUJBQWdCLEdBQUc7WUFDdEIsTUFBTSxDQUFDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEtBQUssNkJBQTZCO2dCQUN6RSxLQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxLQUFLLGNBQWM7Z0JBQ3ZELEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEtBQUssaUJBQWlCLENBQzdEO1FBQ0wsQ0FBQyxDQUFDO1FBRUsseUJBQW9CLEdBQUcsVUFBQyxPQUF1QjtZQUF2Qix3QkFBQSxFQUFBLGNBQXVCO1lBQ2xELEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQy9DLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQ3ZELEtBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNyRCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osMkNBQTJDO2dCQUMzQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzNDLEtBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUUsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixLQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDekMsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFSywyQkFBc0IsR0FBRztZQUM1QixLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDOUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hFLEtBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBYSxFQUFFLEtBQWE7Z0JBQ2xFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztnQkFDakUsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO2dCQUNsRSxLQUFJLENBQUMsRUFBRSxDQUFDLDRDQUE0QyxDQUFDLENBQUM7WUFDdEQsSUFBSSxjQUFjLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLHlCQUF5QixDQUFDO1lBQ3JHLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUNqQixLQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUNyQixLQUFJLENBQUMsRUFBRSxDQUFDLHNDQUFzQyxDQUFDLEVBQy9DLE9BQU8sQ0FBQyx5Q0FBeUMsQ0FBQyxFQUFFLGNBQWMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN2RixDQUFDLENBQUM7UUFFSyx1QkFBa0IsR0FBRyxVQUFDLE9BQXVCO1lBQXZCLHdCQUFBLEVBQUEsY0FBdUI7WUFDaEQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBRTlCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDbkQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztZQUMzRixDQUFDO1lBRUQsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUMvRSxVQUFDLFFBQStCO2dCQUM1QixLQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUM7Z0JBQ3ZDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMvQixLQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUM7Z0JBQzVELENBQUM7Z0JBRUQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLDBCQUEwQixFQUFFLENBQUMsQ0FBQSxrQkFBa0I7Z0JBQ3hELENBQUM7Z0JBQ0QsS0FBSSxDQUFDLDBCQUEwQixHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLHVCQUF1QixFQUFFLEtBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBRW5HLEtBQUksQ0FBQyxnQ0FBZ0MsRUFBRSxDQUFDO2dCQUV4QyxLQUFJLENBQUMsVUFBVSxDQUNYLEtBQUksQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsRUFDekIsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFDakIsS0FBSSxDQUFDLEVBQUUsQ0FBQyxtQ0FBbUMsQ0FBQyxFQUM1QyxPQUFPLENBQUMscUNBQXFDLENBQUMsRUFDOUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRUsscUNBQWdDLEdBQUc7WUFDdEMsS0FBSSxDQUFDLE1BQU0sQ0FBQywwQkFBMEIsR0FBRztnQkFDckMsZUFBZSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFDO29CQUNyRCxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQztnQkFDeEUsYUFBYSxFQUFFLEtBQUs7Z0JBQ3BCLFlBQVksRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUM7YUFDeEUsQ0FBQztZQUVGLEtBQUksQ0FBQyxNQUFNLENBQUMsMEJBQTBCLENBQUMsV0FBVyxHQUFHO2dCQUM3QyxLQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUNoRyxLQUFJLENBQUMsTUFBTSxDQUFDLDBCQUEwQixDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQ3hELEtBQUksQ0FBQyxNQUFNLENBQUMsMEJBQTBCLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDbEUsQ0FBQyxDQUFDO1FBQ04sQ0FBQyxDQUFDO1FBRUssc0JBQWlCLEdBQUcsVUFBQyxNQUF5QjtZQUNqRCxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQW1CO29CQUNoRCxJQUFJLGdCQUFnQixHQUFZLElBQUksQ0FBQztvQkFDckMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLGlCQUFpQixJQUFJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbEUsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNuRCxDQUFDO29CQUVELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO29CQUV4RSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMzQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsb0JBQW9CLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQztvQkFDdkgsQ0FBQztnQkFDVCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFSyx1QkFBa0IsR0FBRztZQUN4QixJQUFJLFlBQVksR0FBaUIsRUFBRSxRQUFRLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQ2hGLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQXlCO2dCQUN2RixLQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQztnQkFDM0IsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUM1QyxLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDL0IsQ0FBQztnQkFDRCxNQUFNLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDbEUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyxrQkFBYSxHQUFHO1lBQ25CLElBQUksa0JBQWtCLEdBQTBCLElBQUksQ0FBQztZQUVyRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQjttQkFDOUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxLQUFLLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQzVGLElBQUksYUFBYSxHQUFpQixLQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBRSxDQUFDLEVBQUUsQ0FBQztnQkFDM0Usa0JBQWtCLEdBQTBCLENBQUMsRUFBRSxFQUFFLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztZQUV4RSxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLENBQUM7bUJBQ25ELEtBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksS0FBSyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUN4RixrQkFBa0IsR0FBbUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBc0IsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJO29CQUM3RSxNQUFNLENBQXNCLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDaEQsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsa0JBQWtCLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxpQkFBZSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlELElBQUksVUFBVSxHQUFzQixFQUFFLE9BQU8sRUFBRSxpQkFBZSxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxDQUFDO2dCQUUvRixNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBeUI7b0JBQ2hGLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNwQixNQUFNLEdBQXNCLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDO29CQUN0RCxDQUFDO29CQUNELEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO29CQUMzQixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQzt3QkFDN0QsTUFBTSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLElBQUksQ0FBQzs0QkFDbkMsTUFBTSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsRUFBRSxpQkFBZSxDQUFDLENBQUM7d0JBQ2pGLENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLGlCQUFlLENBQUMsQ0FBQztvQkFDMUUsQ0FBQztnQkFDTCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFSyw4QkFBeUIsR0FBRztZQUMvQixJQUFJLFlBQVksR0FBaUIsRUFBRSxRQUFRLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0I7Z0JBQ3pFLGNBQWMsRUFBRSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDNUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBeUI7Z0JBQ3ZGLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO2dCQUMzQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ2xFLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2dCQUMvQixDQUFDO2dCQUNELE1BQU0sQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1lBQzFFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUc7WUFDekIsd0JBQXdCO1lBQ3hCLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUM7WUFFMUcsSUFBSSxhQUFhLEdBQTJDO2dCQUN4RCxVQUFVLEVBQUUsSUFBSTtnQkFDaEIsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsaUNBQWlDLENBQUM7Z0JBQ2pELE1BQU0sRUFBRSxTQUFTO2dCQUNqQixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsT0FBTyxFQUFFLENBQUM7d0JBQ04sSUFBSSxFQUFFLE9BQU87d0JBQ2IsU0FBUyxFQUFFLElBQUk7d0JBQ2YsSUFBSSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3dCQUN0QixNQUFNLEVBQUUsVUFBVSxZQUFZOzRCQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNoQixDQUFDO3FCQUNKLENBQUM7YUFDTCxDQUFDO1lBQ0YsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQztnQkFDdkIsUUFBUSxFQUFFLE9BQU8sQ0FBQyxxQ0FBcUMsQ0FBQzthQUFFLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUc7WUFDekIsS0FBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQXNCLEVBQUUsS0FBSyxFQUFhLEVBQUUsRUFBRSxDQUFDO1lBQ3BFLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQW1CO2dCQUMvRCxJQUFJLFVBQVUsR0FBWSxLQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNsRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0JBQ2QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO2dCQUNwQyxDQUFDO2dCQUVELEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUVwQixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUMvQyxNQUFNLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUNMLENBQUMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLENBQUMsRUFDMUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLENBQUM7WUFDTCxDQUFDLEVBQUU7Z0JBQ0MsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyw2QkFBd0IsR0FBRztZQUM5QixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEtBQUssS0FBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDbEcsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDaEIsQ0FBQztZQUNMLENBQUM7WUFFRCxJQUFJLGNBQWMsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7WUFFakQsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUM7WUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFTLElBQWE7Z0JBQzdDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxLQUFLLEtBQUssQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztRQUVQLENBQUMsQ0FBQztRQUVLLHlCQUFvQixHQUFHO1lBQzFCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUQsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssS0FBSyxDQUFDO1FBQzlELENBQUMsQ0FBQztRQUVLLDhCQUF5QixHQUFHO1lBQy9CLElBQUksVUFBVSxHQUFZLEtBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDbEQsTUFBTSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksVUFBVSxDQUFDO1FBQy9DLENBQUMsQ0FBQztRQUVLLDBCQUFxQixHQUFHO1lBQzNCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0MsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ2xELEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUNsRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLHFCQUFxQixDQUFDLENBQUM7b0JBQzFELEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLHFCQUFxQixDQUFDLENBQUM7Z0JBQzlELENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxLQUFLLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7d0JBQ2xHLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUMvQyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUUzRCxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUM3QyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUN6RCxDQUFDO2dCQUNMLENBQUM7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN6QyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUNqQixDQUFDO1lBQ0wsQ0FBQztZQUVELE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDO1FBRUssNEJBQXVCLEdBQUc7WUFDN0IsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1FBQ3BELENBQUMsQ0FBQztRQUVLLHdCQUFtQixHQUFHO1lBQ3pCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0MsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsS0FBSyxLQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNsRyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNoQixDQUFDO1lBQ0wsQ0FBQztZQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQyxDQUFDO1FBRUssNkJBQXdCLEdBQUc7WUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDLENBQUM7UUFFSywyQkFBc0IsR0FBRztZQUM1QixLQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNWLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3hDLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUNqRCxLQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2dCQUNyQyxDQUFDO2dCQUVELG9EQUFvRDtnQkFDcEQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzQyxLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFRLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUNoRixLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNqRCxDQUFDO1lBRUwsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ1YsQ0FBQyxDQUFDO1FBRU0sd0JBQW1CLEdBQUc7WUFDMUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQXNCLEVBQUUsS0FBSyxFQUFhLEVBQUUsRUFBRSxDQUFDO1lBQ3BFLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQW1CO2dCQUM5RCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3BFLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztnQkFDcEMsQ0FBQztnQkFDRCxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3BCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRU0sYUFBUSxHQUFHO1lBQ2YsS0FBSSxDQUFDLFFBQVEsQ0FBQztnQkFDVixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQzlDLEtBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2dCQUNoRixDQUFDO1lBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ1YsQ0FBQyxDQUFDO1FBRU0scUJBQWdCLEdBQUcsVUFBQyxRQUEyQixFQUMzQixpQkFBZ0MsRUFBRSxnQkFBZ0M7WUFBbEUsa0NBQUEsRUFBQSx3QkFBZ0M7WUFBRSxpQ0FBQSxFQUFBLHVCQUFnQztZQUMxRixJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixPQUFPLEdBQUcsaUJBQWlCLEdBQUcsR0FBRztvQkFDakMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUM7d0JBQzdELEtBQUksQ0FBQyxFQUFFLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDLENBQUM7WUFDckcsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNqRSxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7b0JBQ25CLEtBQUksQ0FBQyxRQUFRLENBQUM7d0JBQ1YsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFNLEtBQUksQ0FBQyx3QkFBd0IsRUFDbEUsQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDN0MsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztvQkFDcEUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNWLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzFDLENBQUM7WUFFTCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDO2dCQUNuQyxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2pCLENBQUM7WUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQztRQUVNLGVBQVUsR0FBRyxVQUFDLFdBQW1CLEVBQUUsaUJBQXlCLEVBQ2hFLHFCQUE2QixFQUFFLFlBQW9CLEVBQUUsY0FBd0IsRUFDN0UsWUFBMkI7WUFBM0IsNkJBQUEsRUFBQSxtQkFBMkI7WUFDM0IsS0FBSSxDQUFDLFlBQVksR0FBRztnQkFDaEIsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLElBQUksRUFBRSxpQkFBaUI7Z0JBQ3ZCLFVBQVUsRUFBRSxxQkFBcUI7Z0JBQ2pDLFNBQVMsRUFBRSxJQUFJO2dCQUNmLE1BQU0sRUFBRSxjQUFjO2FBQ3pCLENBQUM7WUFFRixJQUFJLG1CQUFtQixHQUF1QjtnQkFDMUMsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLEtBQUssRUFBRSxXQUFXO2dCQUNsQixVQUFVLEVBQUUsS0FBSztnQkFDakIsU0FBUyxFQUFFLEtBQUksQ0FBQyxNQUFNO2dCQUN0QixPQUFPLEVBQUU7b0JBQ0wsS0FBSSxDQUFDLFlBQVk7aUJBQ3BCO2FBQ0osQ0FBQztZQUVGLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsbUJBQW1CLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQztZQUM5QyxDQUFDO1lBRUQsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQztnQkFDdkIsUUFBUSxFQUFFLFlBQVk7YUFBQyxFQUN0QixtQkFBbUIsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQztJQXhaRixDQUFDO0lBNUNhLGdDQUFPLEdBQUcsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFLHdCQUF3QjtRQUN6RixnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsMEJBQTBCLEVBQUUsZ0JBQWdCLEVBQUUsaUJBQWlCO1FBQzdGLFVBQVUsRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLG9CQUFvQixDQUFDLENBQUM7SUFtY3hGLCtCQUFDO0NBQUEsQUF0Y0QsSUFzY0M7a0JBdGNvQix3QkFBd0IifQ==

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AvailabilityState;
(function (AvailabilityState) {
    AvailabilityState[AvailabilityState["None"] = 0] = "None";
    AvailabilityState[AvailabilityState["Online"] = 1] = "Online";
    AvailabilityState[AvailabilityState["FipsFailure"] = 2] = "FipsFailure";
    AvailabilityState[AvailabilityState["HttpsClockFailure"] = 3] = "HttpsClockFailure";
    AvailabilityState[AvailabilityState["DnsFailure"] = 4] = "DnsFailure";
    AvailabilityState[AvailabilityState["ConnectionFailure"] = 5] = "ConnectionFailure";
    AvailabilityState[AvailabilityState["ConnectionTimeout"] = 6] = "ConnectionTimeout";
    AvailabilityState[AvailabilityState["ProxyDnsFailure"] = 7] = "ProxyDnsFailure";
    AvailabilityState[AvailabilityState["ProxyFailure"] = 8] = "ProxyFailure";
    AvailabilityState[AvailabilityState["GeneralNetworkFailure"] = 9] = "GeneralNetworkFailure";
    AvailabilityState[AvailabilityState["UnknownFailure"] = 10] = "UnknownFailure";
    AvailabilityState[AvailabilityState["Offline"] = 11] = "Offline";
})(AvailabilityState || (AvailabilityState = {}));
exports.default = AvailabilityState;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXZhaWxhYmlsaXR5U3RhdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhdmFpbGFiaWxpdHlTdGF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QyxJQUFLLGlCQWFKO0FBYkQsV0FBSyxpQkFBaUI7SUFDbEIseURBQVEsQ0FBQTtJQUNSLDZEQUFVLENBQUE7SUFDVix1RUFBZSxDQUFBO0lBQ2YsbUZBQXFCLENBQUE7SUFDckIscUVBQWMsQ0FBQTtJQUNkLG1GQUFxQixDQUFBO0lBQ3JCLG1GQUFxQixDQUFBO0lBQ3JCLCtFQUFtQixDQUFBO0lBQ25CLHlFQUFnQixDQUFBO0lBQ2hCLDJGQUF5QixDQUFBO0lBQ3pCLDhFQUFtQixDQUFBO0lBQ25CLGdFQUFZLENBQUE7QUFDaEIsQ0FBQyxFQWJJLGlCQUFpQixLQUFqQixpQkFBaUIsUUFhckI7QUFFRCxrQkFBZSxpQkFBaUIsQ0FBQyJ9

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=cl-deactivation-dialog> <xui-message type=error ng-show=vm.dialogOptions.viewModel.errorMessage>{{vm.dialogOptions.viewModel.errorMessage}}</xui-message> <div ng-show=vm.dialogOptions.viewModel.isOnline> <strong _t>Are you sure you want to deactivate the following licenses?</strong> <span _t>You will no longer be able to use the product when the licenses are deactivated.</span> </div> <div ng-show=!vm.dialogOptions.viewModel.isOnline> <div _t> We detected your computer is not connected to the Internet. To complete the license deactivation, you need access to a computer connected to the Internet and perform the following steps: </div> <ol> <li _t>Deactivate the license - your product can no longer be used once you do this</li> <li _t>Download a deactivation file</li> <li _t=\"['{{::vm.licensingConstants.links.customerPortal}}', '{{::vm.dialogOptions.viewModel.anchorTarget}}']\"> Upload the deactivation file to <a target={1} href={0}>Customer Portal</a> - if you skip this step, the license cannot be reactivated </li> </ol> <div> <strong _t>Are you sure you want to deactivate the following licenses?</strong> <span _t>You will no longer be able to use the product when the licenses are deactivated.</span> </div> </div> <ul class=deactivation-list> <li ng-repeat=\"license in vm.dialogOptions.viewModel.deactivationLicenses\"> <div><strong>{{ license.licenseName }}</strong></div> <div> <span class=text-muted _t>Installed Version</span> <span class=license-property-value>{{license.productVersion}}</span> <span class=text-muted _t>Type</span> <span class=license-property-value>{{license.licenseType}} </span> <span ng-if=\"license.licenseType === 'Evaluation' || license.licenseType === 'Temporary'\"><span class=text-muted _t>Expiration</span> <span class=license-property-value>{{license.expiration.license | date:'mediumDate'}}</span></span> <span ng-if=\"license.licenseType === 'Commercial'\"><span class=text-muted _t>Maintenance Expiration</span> <span class=license-property-value>{{license.expiration.maintenance | date:'mediumDate'}}</span></span> <span class=assigned-text> <span class=text-muted _t>Assigned to</span> <span class=license-property-value>{{license.assignedServers !== null ? license.assignedServers : 'Currently Not Assinged'}}</span> </span> <span ng-show=\"license.licenseType !== 'Evaluation'\"><span class=text-muted _t>License Key</span> <span class=license-property-value>{{license.licenseKey}}</span></span> </div> </li> </ul> </div>  </xui-dialog> ";

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=cl-assignment-dialog> <form name=vm.dialogOptions.viewModel.assignLicenseForm> <xui-message type=error ng-show=vm.dialogOptions.viewModel.errorMessage>{{vm.dialogOptions.viewModel.errorMessage}}</xui-message> <div> <div> <span _t>License</span> <span>: {{vm.dialogOptions.viewModel.selection.items[0].licenseName}}</span> <span class=pull-right>{{vm.dialogOptions.viewModel.selection.items[0].licenseKey}}</span> </div> <div class=\"col-sm-12 text-right text-break-line\"> <div class=row> <span ng-if=\"vm.dialogOptions.viewModel.selection.items[0].assignedServers !== null\"> <span class=text-muted _t> Assigned to </span>&nbsp; {{vm.dialogOptions.viewModel.selection.items[0].assignedServers}} </span> <span ng-if=\"vm.dialogOptions.viewModel.selection.items[0].assignedServers === null\" class=text-muted _t> Currently Not Assigned </span> </div> </div> </div> <div class=clearfix></div> <xui-divider></xui-divider> <h4 _t>AVAILABLE POLLERS:</h4> <xui-sorter items-source=vm.dialogOptions.viewModel.assignmentsGridSortingData.sortableColumns display-value=displayValue sort-direction=vm.dialogOptions.viewModel.assignmentsGridSortingData.sortDirection selected-item=vm.dialogOptions.viewModel.assignmentsGridSortingData.selectedItem on-change=vm.dialogOptions.viewModel.assignmentsGridSortingData.sortChanged()> </xui-sorter> <div class=row> <xui-listview items-source=vm.dialogOptions.viewModel.validAssignment.servers empty-text=\"_t(No valid assignments)\" template-url=assigments-grid-template selection-mode={{vm.dialogOptions.viewModel.validAssignment.kind}} selection=vm.dialogOptions.viewModel.orionServersSelection _ta> </xui-listview> <script type=text/ng-template id=assigments-grid-template> <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"node-icon\">\n                                <xui-icon icon=\"{{item.icon}}\" status=\"{{item.statusIcon}}\"></xui-icon>\n                                <strong class=\"cl-assignment-item-caption text-break-line\"> {{::item.hostName}}</strong>\n                            </div>\n                            <div class=\"cl-assignment-item-description text-muted\">{{::item.displayType}}</div>\n                        </div>\n                    </div> </script> </div> </form> </div> </xui-dialog> ";

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div ng-controller=\"OfflineServerDialogController as offlineCtrl\"> <p _t> We cannot resolve the SolarWinds licensing server (licenseserver.solarwinds.com). If your Orion server is behind a proxy server, the proxy server settings are needed before Orion can talk to the licensing server. Verify your Orion proxy settings. </p> <p> <xui-button size=small display-style=secondary ng-click=offlineCtrl.navigateToOrionProxySettingsPage() _t> CONFIGURE ORION PROXY SETTINGS </xui-button> </p> <p> <a href={{::offlineCtrl.licensingConstants.links.offlineTroubleshooting}} target={{::offlineCtrl.anchorTarget}}>&raquo; <span _t>Help me troubleshoot this issue</span></a> </p> </div> </xui-dialog> ";

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content is-busy=vm.isBusy _ta busy-message=_t(Loading...) page-layout=fill class=\"cl-license-manager-page xui-page-content--no-padding\"> <xui-page-header class=cl-license-manager-page__header> <div class=cl-license-manager-page__title-link ng-if=\"!vm.$scope.isOnline && vm.isStoreAvailable()\"> <a href ng-click=vm.offlineLicenseClick()> <xui-icon icon=severity_warning icon-size=small></xui-icon> <span _t>Offline</span> </a> </div> <div class=\"cl-license-manager-page__title-link customer-portal-link\"> <a ng-href={{::vm.licensingConstants.links.customerPortal}} target={{::vm.$scope.anchorTarget}} _t>CUSTOMER PORTAL</a> </div> <div class=\"cl-license-manager-page__title-link customer-portal-link\"> <a ng-href={{::vm.licensingConstants.links.licenseDetails}} target=_self _t>LICENSE DETAILS</a> </div> <h1 class=cl-license-manager-page__title _t>License Manager</h1> </xui-page-header> <div ng-if=vm.isStoreAvailable()> <xui-grid items-source=vm.$scope.licenses pagination-data=vm.gridPagination smart-mode=vm.smartMode selection-mode={{vm.selectionMode}} sorting-data=vm.sortingData smart-items-source=vm.smartItemsSourceLicenses template-url=grid-template selection=vm.$scope.selection controller=vm> <xui-grid-toolbar-container> <xui-toolbar> <xui-button ng-click=vm.activateLicenseClick() icon=enable display-style=tertiary ng-if=\"vm.activateButtonIsShown() && vm.$scope.selection.items.length > 0\" sw-license-activation-modal selected-license=vm.$scope.activationLicense mode=vm.$scope.licenseActivationMode dialog-title=vm.activateDialogTitle on-activate=vm.onLicenseActivate(result)> <span>{{vm.activateButtonTitle}}</span> </xui-button> <xui-button ng-click=vm.activateLicenseClick() icon=enable display-style=tertiary ng-if=\"!vm.$scope.selection.items.length > 0\" sw-license-activation-modal mode=vm.$scope.licenseActivationMode dialog-title=vm.activateDialogTitle on-activate=vm.onLicenseActivate(result)> <span>{{vm.activateButtonTitle}}</span> </xui-button> <xui-button icon=disable display-style=tertiary ng-click=vm.deactivateLicenseClick() is-disabled=vm.deactivateButtonDisabled() ng-if=vm.deactivateButtonIsShown()> <span _t>Deactivate</span> </xui-button> <xui-toolbar-divider></xui-toolbar-divider> <xui-button icon=reset _ta tool-tip=\"_t(Update all licenses which have outdated maintenance information)\" display-style=tertiary tooltip-append-to-body=true ng-click=vm.synchronizeLicenses() is-disabled=vm.synchronizeButtonDisabled() ng-if=vm.synchronizeButtonIsShown()> <span _t>Synchronize</span> </xui-button> <xui-toolbar-divider></xui-toolbar-divider> <xui-button icon=assign display-style=tertiary ng-click=vm.assignLicenseClick() is-disabled=vm.assignButtonDisabled() ng-if=vm.assignButtonIsShown()> <span _t>Assign</span> </xui-button> </xui-toolbar> </xui-grid-toolbar-container> </xui-grid> </div> </xui-page-content> <script type=text/ng-template id=grid-template> <div class=\"row flex-display flex-vertical-center\">\n        <div class=\"col-xs-12 col-md-6 left-container\">\n\n            <strong>{{::item.licenseName}}</strong> <span class=\"text-muted\">{{::item.productName}}</span>\n\n            <div>\n                <div class=\"details-item\" ng-show=\"::item.productVersion\">\n                    <span class=\"text-muted\" _t>Installed Version</span>\n                    &nbsp;{{::item.productVersion}}\n                </div>\n                <div class=\"details-item\">\n                    <span class=\"text-muted\" _t>Type</span>\n                    <span ng-class=\"{'bold': item.licenseType === 'Evaluation' || item.licenseType === 'Temporary' }\">&nbsp;{{::item.licenseType}}</span>\n                </div>\n                <div class=\"details-item\">\n                    <!--1. Is evaluation key or is temp key: display only expiration date (days remaining).-->\n                    <span ng-if=\"item.licenseType === 'Evaluation' || item.licenseType === 'Temporary'\">\n                        <span ng-if=\"::item.expiration.licenseDays > 0\">\n                            <span class=\" text-muted\" _t>Expiration</span>\n                            <span class=\"{{::item.licenseDaysClass}}\">\n                                <span ng-class=\"{'bold': item.licenseDaysClass}\">{{::item.expiration.license | date:'mediumDate'}}</span>\n                                <strong ng-show=\"item.licenseDaysClass\"\n                                        ng-class=\"{'expired': item.licenseDaysClass == 'critical'}\">\n                                    - {{::item.expiration.licenseDays}} <span _t>days left</span>\n                                </strong>\n                            </span>\n                        </span>\n\n                        <span ng-if=\"::item.expiration.licenseDays === 0\" class=\" expired\">\n                            <span _t>Expired on</span> {{::item.expiration.license | date:'mediumDate'}} - <a ng-href=\"{{::vm.licensingConstants.links.renewMaintainance}}\" target=\"{{::vm.$scope.anchorTarget}}\" _t>Renew Maintenance</a>\n                        </span>\n                    </span>\n                    <span ng-if=\"item.licenseType === 'Commercial'\">\n                        <!--2. is perpetual key (expires in year >= 9999): display only maintenance date.-->\n                        <span ng-if=\"::item.isPerpetual\">\n                            <span class=\"text-muted\" _t>Maintenance Expiration</span> {{::item.expiration.maintenance | date:'mediumDate'}}\n                        </span>\n\n                        <!--3. final: is non- perpetual key (expires in year < 9999): display maintenance date and days remaining.-->\n                        <span ng-if=\"::!item.isPerpetual\">\n                            <span class=\" text-muted\" _t>Maintenance Expiration</span>\n                            <span class=\"{{::item.maintenanceDaysClass}}\">\n                                <span ng-class=\"{'bold': item.maintenanceDaysClass}\">{{::item.expiration.maintenance | date:'mediumDate'}}</span>\n                                <strong ng-show=\"item.maintenanceDaysClass\"\n                                        ng-class=\"{'expired': item.maintenanceDaysClass == 'critical'}\">\n                                    - {{::item.expiration.maintenanceDays}} <span _t>days left</span>\n                                </strong>\n                            </span>\n                        </span>\n\n                        <span ng-if=\"::item.expiration.maintenanceDays === 0\" class=\"expired\">\n                            <span _t>Maintenance Expired on</span> {{::item.expiration.maintenance | date:'mediumDate'}} - <a ng-href=\"{{::vm.licensingConstants.links.renewMaintainance}}\" target=\"{{::vm.$scope.anchorTarget}}\" _t>Renew Maintenance</a>\n                        </span>\n                    </span>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"col-xs-12 col-md-6 right-container text-right flex-display\"\n             ng-class=\"{'activate-button-container flex-vertical-center': item.licenseType === 'Evaluation'}\">\n            <div ng-class=\"{'activation flex-display': item.licenseType === 'Evaluation'}\" class=\"flex-basis\">\n\n                <div class=\"activate-button pull-right\" ng-if=\"::item.licenseType === 'Evaluation'\">\n                    <xui-button ng-click=\"vm.activateLicenseClick(item)\"\n                     is-disabled=\"::!item.canActivate\"  sw-license-activation-modal\n                     selected-license=\"vm.$scope.activationLicense\"\n                     mode=\"vm.$scope.licenseActivationMode\"\n                     dialog-title=\"vm.activateDialogTitle\"\n                     on-activate=\"vm.onLicenseActivate(result)\" _t>\n                        Activate\n                    </xui-button>\n                </div>\n\n                <div ng-if=\"(item.installed && item.assigned) || (item.licenseType === 'Evaluation')\"\n                     class=\"assigned-text\"\n                     ng-class=\"{'pull-right evaluation' : item.licenseType === 'Evaluation'}\">\n                    <span ng-if=\"item.assignedServers !== null\">\n                        <span class=\"text-muted\" _t>\n                            Assigned to\n                        </span>\n                        {{::item.assignedServers}}\n                    </span>\n                    <span ng-if=\"item.assignedServers === null\" class=\"text-muted\" _t>\n                        Currently Not Assigned\n                    </span>\n                </div>\n\n                <div ng-show=\"::!item.installed\" class=\"not-installed\">\n                    <span class=\"warning\">\n                        <strong _t>Product is licensed but not installed</strong>\n                    </span>\n                </div>\n\n                <div ng-show=\"::!item.assigned\" class=\"not-assigned\">\n                    <span class=\"warning\">\n                        <strong _t>Not Assigned</strong> - <a href=\"#\" _t>Assign Now</a>\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"license-key flex-basis\" ng-show=\"item.licenseKey && item.licenseType !== 'Evaluation'\">\n                <span class=\"text-muted\" _t>License Key</span> &nbsp;{{::item.licenseKey}}\n            </div>\n        </div>\n    </div> </script> ";

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var offlineServerDialog_controller_1 = __webpack_require__(22);
exports.default = function (module) {
    module.controller("OfflineServerDialogController", offlineServerDialog_controller_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1GQUE2RTtBQUU3RSxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFVBQVUsQ0FBQywrQkFBK0IsRUFBRSx3Q0FBNkIsQ0FBQyxDQUFDO0FBQ3RGLENBQUMsQ0FBQyJ9

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var OfflineServerDialogController = /** @class */ (function () {
    function OfflineServerDialogController($stateParams, navigationService, licensingConstants) {
        var _this = this;
        this.$stateParams = $stateParams;
        this.navigationService = navigationService;
        this.licensingConstants = licensingConstants;
        this.$onInit = function () {
            _this.anchorTarget = _this.$stateParams.fromHa ? "_self" : "_blank";
        };
        this.navigateToOrionProxySettingsPage = function () {
            _this.navigationService.navigateToOrionProxySettingsPage();
        };
    }
    OfflineServerDialogController.$inject = ["$stateParams", "navigationService", "licensingConstants"];
    return OfflineServerDialogController;
}());
exports.default = OfflineServerDialogController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ZmbGluZVNlcnZlckRpYWxvZy1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib2ZmbGluZVNlcnZlckRpYWxvZy1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBSUE7SUFLSSx1Q0FBb0IsWUFBdUMsRUFDeEMsaUJBQW9DLEVBQ3BDLGtCQUE2QjtRQUZoRCxpQkFFb0Q7UUFGaEMsaUJBQVksR0FBWixZQUFZLENBQTJCO1FBQ3hDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFXO1FBRXpDLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxZQUFZLEdBQVMsS0FBSSxDQUFDLFlBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQSxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQzVFLENBQUMsQ0FBQztRQUVLLHFDQUFnQyxHQUFHO1lBQ3RDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQ0FBZ0MsRUFBRSxDQUFDO1FBQzlELENBQUMsQ0FBQztJQVJpRCxDQUFDO0lBTnRDLHFDQUFPLEdBQUcsQ0FBQyxjQUFjLEVBQUUsbUJBQW1CLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztJQWV4RixvQ0FBQztDQUFBLEFBaEJELElBZ0JDO2tCQWhCb0IsNkJBQTZCIn0=

/***/ }),
/* 23 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 24 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 25 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(27);
exports.default = function (module) {
    module.service("licensingConstants", constants_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlDQUFvQztBQUVwQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxtQkFBUyxDQUFDLENBQUM7QUFDcEQsQ0FBQyxDQUFDIn0=

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
        this.license = {
            defaultKey: "0000-0000-0000-0000-0000-0000-0000-0000",
            types: {
                commercial: "Commercial",
                evaluation: "Evaluation",
                temporary: "Temporary"
            }
        };
        this.links = {
            licenseDetails: "/Orion/Admin/Details/ModulesDetailsHost.aspx",
            customerPortal: "https://customerportal.solarwinds.com",
            renewMaintainance: "https://customerportal.solarwinds.com/renew-maintenance/renew",
            httpProxySettings: "/Orion/Admin/HttpProxySettings/Default.aspx",
            offlineTroubleshooting: "https://support.solarwinds.com/Success_Center/License_Manager\n            /It_is_not_possible_to_connect_to_the_licensing_server",
            privacy: "http://www.solarwinds.com/privacy.aspx"
        };
        this.grid = {
            selectionMode: {
                single: "single",
                multi: "multi"
            },
            apiSelectionMode: {
                single: "single",
                multi: "multiple"
            },
            updateInterval: 30000
        };
    }
    return Constants;
}());
exports.default = Constants;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBRXBDO0lBQUE7UUFFVyxZQUFPLEdBQUc7WUFDYixVQUFVLEVBQUUseUNBQXlDO1lBQ3JELEtBQUssRUFBRTtnQkFDSCxVQUFVLEVBQUUsWUFBWTtnQkFDeEIsVUFBVSxFQUFFLFlBQVk7Z0JBQ3hCLFNBQVMsRUFBRSxXQUFXO2FBQ3pCO1NBQ0osQ0FBQztRQUVLLFVBQUssR0FBRztZQUNYLGNBQWMsRUFBRSw4Q0FBOEM7WUFDOUQsY0FBYyxFQUFFLHVDQUF1QztZQUN2RCxpQkFBaUIsRUFBRSwrREFBK0Q7WUFDbEYsaUJBQWlCLEVBQUUsNkNBQTZDO1lBQ2hFLHNCQUFzQixFQUFFLG1JQUNtQztZQUMzRCxPQUFPLEVBQUUsd0NBQXdDO1NBQ3BELENBQUM7UUFFSyxTQUFJLEdBQUc7WUFDVixhQUFhLEVBQUU7Z0JBQ1gsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLEtBQUssRUFBRSxPQUFPO2FBQ2pCO1lBQ0QsZ0JBQWdCLEVBQUU7Z0JBQ2QsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLEtBQUssRUFBRSxVQUFVO2FBQ3BCO1lBRUQsY0FBYyxFQUFFLEtBQUs7U0FDeEIsQ0FBQztJQUNOLENBQUM7SUFBRCxnQkFBQztBQUFELENBQUMsQUFqQ0QsSUFpQ0MifQ==

/***/ })
/******/ ]);
//# sourceMappingURL=licensing.js.map