/*!
 * @solarwinds/interfaces-frontend 1.5.0-2520
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<sw-interfaces-list/> ";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(4);
module.exports = __webpack_require__(5);


/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(6);
var config_1 = __webpack_require__(7);
var index_1 = __webpack_require__(8);
var index_2 = __webpack_require__(10);
var index_3 = __webpack_require__(13);
var index_4 = __webpack_require__(20);
var templates_1 = __webpack_require__(23);
var index_5 = __webpack_require__(30);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);
index_5.default(app_1.default);


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("interfaces.services", []);
angular.module("interfaces.templates", []);
angular.module("interfaces.components", []);
angular.module("interfaces.filters", []);
angular.module("interfaces.providers", []);
angular.module("interfaces", [
    "orion",
    "interfaces.services",
    "interfaces.templates",
    "interfaces.components",
    "interfaces.filters",
    "interfaces.providers",
    "orion-ui-components"
]);
// create and register Xui (Orion) module wrapper
var interfaces = Xui.registerModule("interfaces");
exports.default = interfaces;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: interfaces");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = (function (module) {
    module.app()
        .run(run);
});
exports.default.$inject = ["module"];


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(9);
exports.default = (function (module) {
    module.service("interfacesConstants", constants_1.default);
});


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(11);
exports.default = (function (module) {
    // register views
    index_1.default(module);
});
var rootState = function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state("interfaces", {
        url: "/interfaces",
        controller: function ($state) {
            $state.go("home");
        }
    });
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var interfacesList_config_1 = __webpack_require__(12);
exports.default = (function (module) {
    module.config(interfacesList_config_1.default);
});


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state("interfacesList", {
        parent: "subview",
        i18Title: "_t(Interfaces List)",
        url: "/interfaces/interfaces-list",
        template: __webpack_require__(0)
    });
}
;
exports.default = config;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(14);
var index_2 = __webpack_require__(17);
exports.default = (function (module) {
    // register components
    index_1.default(module);
    index_2.default(module);
});


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var interfacesList_directive_1 = __webpack_require__(15);
var interfacesList_controller_1 = __webpack_require__(16);
__webpack_require__(1);
exports.default = (function (module) {
    module.controller("InterfacesListController", interfacesList_controller_1.default);
    module.component("swInterfacesList", interfacesList_directive_1.InterfacesListDirective);
});


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(1);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var InterfacesListDirective = /** @class */ (function () {
    function InterfacesListDirective() {
        this.restrict = "E";
        this.templateUrl = "interfaces/components/interfacesList/interfacesList-directive.html";
        this.replace = true;
        this.scope = {};
        this.controller = "InterfacesListController";
        this.controllerAs = "$interfacesCtrl";
    }
    return InterfacesListDirective;
}());
exports.InterfacesListDirective = InterfacesListDirective;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var InterfacesListController = /** @class */ (function () {
    /** @ngInject */
    InterfacesListController.$inject = ["interfacesListService", "interfacesViewService", "getTextService"];
    function InterfacesListController(interfacesListService, interfacesViewService, getTextService) {
        this.interfacesListService = interfacesListService;
        this.interfacesViewService = interfacesViewService;
        this.getTextService = getTextService;
    }
    InterfacesListController.prototype.$onInit = function () {
        this._t = this.getTextService;
        this.bpsUnit = "bitsPerSecond";
        this.options = {
            selectionProperty: "interfaceID",
            smartMode: false,
            showSelector: false,
            hideSearch: false,
            triggerSearchOnChange: true,
            hidePagination: false,
            showAddRemoveFilterProperty: false,
            templateUrl: "interfaces/components/interfacesList/interfacesListItem.html",
            pageSize: 20,
            emptyData: {
                image: "no-search-results",
                description: this._t("No data to show")
            }
        };
    };
    InterfacesListController.prototype.getInterfaces = function (filters, filterModels, pagination, sorting, search) {
        var filterParameters = {
            filterValues: filters,
            filterModels: filterModels,
            pagination: pagination,
            sorting: sorting,
            searching: search
        };
        // by observing 'filterModels' property we can distinguish the last request that filteredList makes
        // and get interface items
        if (filterModels) {
            return this.interfacesListService.getInterfaces(this.interfacesViewService.getNetObject().nodeId, filterParameters);
        }
        var queryFilters = this.interfacesViewService.getQueryFilters();
        if (Object.keys(queryFilters).length > 0) {
            this.addInitialFiltersToFilterParams(filterParameters, queryFilters);
        }
        return this.interfacesListService.getFilterProperties(this.interfacesViewService.getNetObject().nodeId, filterParameters);
    };
    InterfacesListController.prototype.addInitialFiltersToFilterParams = function (filterParameters, queryFilters) {
        if (Object.keys(filterParameters.filterValues).length !== 0) {
            // Adding initial filter values for existing filters in the 2nd request
            for (var _i = 0, _a = Object.keys(queryFilters); _i < _a.length; _i++) {
                var filter = _a[_i];
                var filterRefId = filter.toLowerCase();
                if (filterRefId in filterParameters.filterValues) {
                    filterParameters.filterValues[filterRefId] = [queryFilters[filter]];
                }
            }
            this.interfacesViewService.removeQueryFiltersFromUrl();
        }
    };
    InterfacesListController.prototype.hasInterfaceIssues = function (interfaceItem) {
        return interfaceItem.issues.length > 0;
    };
    InterfacesListController.prototype.formatForXuiPercentFilter = function (numberToFormat) {
        if (numberToFormat != null) {
            return numberToFormat / 100;
        }
        else {
            return NaN;
        }
    };
    return InterfacesListController;
}());
exports.default = InterfacesListController;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var interfaceIssues_directive_1 = __webpack_require__(18);
var interfaceIssues_controller_1 = __webpack_require__(19);
__webpack_require__(2);
exports.default = (function (module) {
    module.controller("InterfaceIssuesController", interfaceIssues_controller_1.default);
    module.component("swInterfaceIssues", interfaceIssues_directive_1.InterfaceIssuesDirective);
});


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(2);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var InterfaceIssuesDirective = /** @class */ (function () {
    function InterfaceIssuesDirective() {
        this.restrict = "E";
        this.templateUrl = "interfaces/components/interfaceIssues/interfaceIssues-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            issues: "="
        };
        this.controller = "InterfaceIssuesController";
        this.controllerAs = "vm";
    }
    return InterfaceIssuesDirective;
}());
exports.InterfaceIssuesDirective = InterfaceIssuesDirective;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var InterfaceIssuesController = /** @class */ (function () {
    function InterfaceIssuesController() {
        var _this = this;
        this.issuesCount = function () {
            return _this.issues.length;
        };
    }
    InterfaceIssuesController.prototype.$onInit = function () {
        this.popoverTemplate = { url: "interface-issues-list-template" };
    };
    InterfaceIssuesController.prototype.getWorstSeverity = function () {
        var severities = this.issues.map(function (i) { return i.severity; });
        return Math.max.apply(Math, severities.map(Number));
    };
    return InterfaceIssuesController;
}());
exports.default = InterfaceIssuesController;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var interfacesView_service_1 = __webpack_require__(21);
var interfacesList_service_1 = __webpack_require__(22);
exports.default = (function (module) {
    module.service("interfacesViewService", interfacesView_service_1.default);
    module.service("interfacesListService", interfacesList_service_1.default);
});


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
//Temporary solution until the final nature of the static subviews is known
var InterfacesViewService = /** @class */ (function () {
    /** @ngInject */
    InterfacesViewService.$inject = ["$log", "$location", "$q"];
    function InterfacesViewService($log, $location, $q) {
        this.$log = $log;
        this.$location = $location;
        this.$q = $q;
        this.netObject = null;
    }
    InterfacesViewService.prototype.getNetObject = function () {
        try {
            if (this.netObject) {
                return this.netObject;
            }
            var netObjectQueryString = this.$location.search().NetObject;
            var nodeId = parseInt((decodeURIComponent(netObjectQueryString)
                .match(/N\:([0-9]+)/)
                ? RegExp.$1
                : "0"), 10);
            this.netObject = {
                nodeId: nodeId
            };
            return this.netObject;
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    InterfacesViewService.prototype.getQueryFilters = function () {
        var filters = {};
        var ignoredFilters = ["NetObject", "ViewID"];
        var allFilters = this.$location.search();
        return _.omit(allFilters, ignoredFilters);
    };
    InterfacesViewService.prototype.removeQueryFiltersFromUrl = function () {
        var currentQueryString = this.$location.search();
        this.$location.search({ NetObject: currentQueryString.NetObject, ViewID: currentQueryString.ViewID });
    };
    return InterfacesViewService;
}());
exports.default = InterfacesViewService;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiGetInterfacesUrlFragment = "interfaces/nodes/{nodeId}/interfaces";
exports.swApiGetFilterPropertiesUrlFragment = exports.swApiGetInterfacesUrlFragment + "/noitems";
var InterfacesListService = /** @class */ (function () {
    /** @ngInject */
    InterfacesListService.$inject = ["swApi"];
    function InterfacesListService(swApi) {
        this.swApi = swApi;
    }
    InterfacesListService.prototype.getInterfaces = function (nodeId, filterParameters) {
        return this.getFilteredListQueryResults(exports.swApiGetInterfacesUrlFragment.replace("{nodeId}", nodeId.toString()), filterParameters);
    };
    InterfacesListService.prototype.getFilterProperties = function (nodeId, filterParameters) {
        return this.getFilteredListQueryResults(exports.swApiGetFilterPropertiesUrlFragment.replace("{nodeId}", nodeId.toString()), filterParameters);
    };
    InterfacesListService.prototype.getFilteredListQueryResults = function (route, filterParameters) {
        return this.swApi
            .api(false)
            .one(route)
            .customPOST(filterParameters);
    };
    return InterfacesListService;
}());
exports.default = InterfacesListService;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(24);
    req.keys().forEach(function (r) {
        var key = "interfaces" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = (function (module) {
    module.app().run(templates);
});


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/interfaceIssues/example/interfaceIssues.html": 25,
	"./components/interfaceIssues/interfaceIssues-directive.html": 26,
	"./components/interfacesList/example/interfacesList.html": 27,
	"./components/interfacesList/interfacesList-directive.html": 28,
	"./components/interfacesList/interfacesListItem.html": 29,
	"./views/interfacesList/interfacesList.html": 0
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 24;

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = " <xui-page-content page-title=\"Interface Issues - Test Example\" page-layout=form> <div id=E2EInterfaceIssues ng-controller=\"E2EInterfaceIssuesController as vm\"> <sw-interface-issues id=one-issue issues=vm.oneIssueArray></sw-interface-issues> <sw-interface-issues id=two-issues issues=vm.twoIssuesArray></sw-interface-issues> </div> </xui-page-content>";

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<span class=interface-issues> <span ng-if=\"vm.issuesCount() == 1\"> <xui-icon icon=\"{{vm.issues[0].severity | interfaceIssueSeverityToIcon}}\" icon-size=small></xui-icon> <span class=\"title xui-text-l\">{{vm.issues[0].title}}</span> </span> <span ng-if=\"vm.issuesCount() > 1\"> <xui-popover xui-popover-content=vm.popoverTemplate xui-popover-trigger=mouseenter> <xui-icon icon=\"{{vm.getWorstSeverity() | interfaceIssueSeverityToIcon}}\" icon-size=small></xui-icon> <span class=\"title xui-text-l\" _t=\"['{{vm.issuesCount()}}']\">{0} Issues</span> </xui-popover> </span> <script type=text/ng-template id=interface-issues-list-template> <div>\n            <xui-listview stripe=\"true\"\n                items-source=\"vm.issues\"\n                template-url=\"interface-issues-item-template\">\n            </xui-listview>\n        </div> </script> <script type=text/ng-template id=interface-issues-item-template> <div class=\"interface-issues-popup-item\">\n            <xui-icon icon=\"{{item.severity | interfaceIssueSeverityToIcon}}\"></xui-icon>\n            <span class=\"title xui-text-s\">{{item.title}}</span>\n        </div> </script> </span>";

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title=\"List of Interfaces - Test Example\" page-layout=form> <sw-interfaces-list></sw-interfaces-list> </xui-page-content>";

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = "<div class=interfaces-list ng-init=$interfacesCtrl.init()> <sw-server-side-filtered-list parent-controller=$interfacesCtrl options=$interfacesCtrl.options getdata=\"$interfacesCtrl.getInterfaces(filters, filterModels, pagination, sorting, searching)\"> </sw-server-side-filtered-list> </div> ";

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row interface-list-item\"> <div ng-class=\"vm.hasInterfaceIssues(item) ? 'col-md-3' : 'col-md-6'\" name=columnName> <div class=media> <div class=media-left> <xui-icon css-class=interface-icon icon=network-interface status=\"{{item.status | swStatus}}\"></xui-icon> </div> <div class=media-body> <a href={{item.detailsUrl}}> <span class=interface-name>{{::item.webConsoleDisplayName}}</span> <span class=interface-alias ng-if=item.interfaceAlias> <br/> <span class=interface-alias-text>{{::item.interfaceAlias}}</span> </span> </a> </div> </div> </div> <div class=col-md-3 name=columnIssue ng-if=vm.hasInterfaceIssues(item)> <sw-interface-issues issues=item.issues></sw-interface-issues> </div> <div class=\"col-md-2 interface-statisticsColumn\" name=columnInBps> <span _t=\"['{{ vm.formatForXuiPercentFilter(item.inUtilization) | xuiPercent:0 }}']\" sw-threshold-violation-highlight=\"{{ item.inPercentUtilizationThresholdStatus }}\"> in: <span class=interface-in-utilization>{0}</span> </span> <xui-help-hint _t=\"['{{item.inBps | xuiUnitConversion: 0 : false : vm.bpsUnit}}','{{item.inBandwidth | xuiUnitConversion: 0 : false : vm.bpsUnit}}']\"> <span class=interface-in-bits-per-second>{0}</span> of <span class=interface-in-bandwidth>{1}</span> </xui-help-hint> </div> <div class=\"col-md-2 interface-statisticsColumn\" name=columnOutBps> <span _t=\"['{{ vm.formatForXuiPercentFilter(item.outUtilization) | xuiPercent:0 }}']\" sw-threshold-violation-highlight=\"{{ item.outPercentUtilizationThresholdStatus }}\"> out: <span class=interface-out-utilization>{0}</span> </span> <xui-help-hint _t=\"['{{item.outBps | xuiUnitConversion: 0 : false : vm.bpsUnit}}','{{item.outBandwidth | xuiUnitConversion: 0 : false : vm.bpsUnit}}']\"> <span class=interface-out-bits-per-second>{0}</span> of <span class=interface-out-bandwidth>{1}</span> </xui-help-hint> </div> <div class=\"col-md-2 interface-statisticsColumn\" name=columnErrorRate ng-switch=\"item.errorsLastHour == null\"> <span class=interface-errors-in-last-hour ng-switch-when=true _t>---</span> <span class=interface-errors-in-last-hour ng-switch-default>{{item.errorsLastHour | number}}</span> <xui-help-hint _t>errors in last hour</xui-help-hint> </div> </div>";

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var shortTimeSpan_filter_1 = __webpack_require__(31);
var interfaceIssueSeverityToIcon_filter_1 = __webpack_require__(32);
exports.default = (function (module) {
    // register filters
    module.filter("shortTimeSpan", shortTimeSpan_filter_1.default);
    module.filter("interfaceIssueSeverityToIcon", interfaceIssueSeverityToIcon_filter_1.default);
});


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Filter converts TimeSpan string value ("00:00:00" or "000.00:00.00" ~ days.hours:minutes:seconds)
 * to a textual value e.g. "20 days" or "50 minutes", whichever highest unit is present in the input
 *
 * Result is localized
 * Result when empty defaults to "-"
 */
function shortTimeSpanFilter(_t) {
    var units = [_t("days"), _t("hours"), _t("minutes"), _t("seconds")];
    var unitsSingular = [_t("day"), _t("hour"), _t("minute"), _t("second")];
    var timeSpanRegex = /(?:(\d+)\.)?0*(\d+):0*(\d+):0*(\d+)/;
    var nbsp = String.fromCharCode(160);
    return function (timeSpanString, emptyResult) {
        if (emptyResult === void 0) { emptyResult = _t("now"); }
        var timeSpanMatches = timeSpanRegex.exec(timeSpanString);
        if (timeSpanMatches == null) {
            return null;
        }
        timeSpanMatches.shift(); // start with individual matches
        for (var index = 0; index < units.length; index++) {
            if (timeSpanMatches[index] && timeSpanMatches[index] !== "0") {
                var unitsArray = timeSpanMatches[index] !== "1" ? units : unitsSingular;
                return timeSpanMatches[index] + nbsp + unitsArray[index];
            }
        }
        return emptyResult;
    };
}
shortTimeSpanFilter.$inject = ["getTextService"];
exports.default = shortTimeSpanFilter;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var issueSeverity_1 = __webpack_require__(33);
/**
 * Filter that transforms interface issue severity to coresponding icon.
 */
function interfaceIssueSeverityToIconFilter() {
    return function (severity) {
        switch (severity) {
            case issueSeverity_1.IssueSeverity.Critical:
                return "status_critical";
            case issueSeverity_1.IssueSeverity.Warning:
                return "status_warning";
            default:
                return "";
        }
        ;
    };
}
exports.default = interfaceIssueSeverityToIconFilter;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
  * Interface issue severity enum
  *
  * @enum
  */
var IssueSeverity;
(function (IssueSeverity) {
    IssueSeverity[IssueSeverity["Warning"] = 0] = "Warning";
    IssueSeverity[IssueSeverity["Critical"] = 1] = "Critical";
})(IssueSeverity = exports.IssueSeverity || (exports.IssueSeverity = {}));


/***/ })
/******/ ]);
//# sourceMappingURL=interfaces.js.map