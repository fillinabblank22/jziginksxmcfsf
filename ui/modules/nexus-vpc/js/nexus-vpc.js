/*!
 * @solarwinds/nexus-vpc-frontend 1.6.0-2033
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<sw-vpc-list/>";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(6);
module.exports = __webpack_require__(7);


/***/ }),
/* 6 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(8);
var config_1 = __webpack_require__(9);
var index_1 = __webpack_require__(10);
var index_2 = __webpack_require__(12);
var index_3 = __webpack_require__(15);
var index_4 = __webpack_require__(34);
var templates_1 = __webpack_require__(40);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("nexus-vpc.services", []);
angular.module("nexus-vpc.templates", []);
angular.module("nexus-vpc.components", []);
angular.module("nexus-vpc.filters", []);
angular.module("nexus-vpc.providers", []);
angular.module("nexus-vpc", [
    "orion",
    "nexus-vpc.services",
    "nexus-vpc.templates",
    "nexus-vpc.components",
    "nexus-vpc.filters",
    "nexus-vpc.providers"
]);
// create and register Xui (Orion) module wrapper
var nexusVpc = Xui.registerModule("nexus-vpc");
exports.default = nexusVpc;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: nexus-vpc");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = (function (module) {
    module.app()
        .run(run);
});
exports.default.$inject = ["module"];


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(11);
exports.default = (function (module) {
    module.service("nexus-vpcConstants", constants_1.default);
});


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(13);
exports.default = (function (module) {
    // register views
    index_1.default(module);
});
var rootState = function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state("nexus-vpc", {
        url: "/nexus-vpc",
        controller: function ($state) {
            $state.go("home");
        }
    });
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcList_config_1 = __webpack_require__(14);
exports.default = (function (module) {
    module.config(vpcList_config_1.default);
});


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state("vpc-list", {
        parent: "subview",
        i18Title: "_t(vPCs List)",
        url: "/nexus-vpc/vpc-list",
        template: __webpack_require__(0)
    });
}
;
exports.default = config;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(16);
var index_2 = __webpack_require__(19);
var index_3 = __webpack_require__(22);
var index_4 = __webpack_require__(25);
var index_5 = __webpack_require__(28);
var index_6 = __webpack_require__(31);
exports.default = (function (module) {
    // register components
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
});


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcList_directive_1 = __webpack_require__(17);
var vpcList_controller_1 = __webpack_require__(18);
__webpack_require__(1);
exports.default = (function (module) {
    module.controller("VpcListController", vpcList_controller_1.default);
    module.component("swVpcList", vpcList_directive_1.VpcListDirective);
});


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(1);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var VpcListDirective = /** @class */ (function () {
    function VpcListDirective() {
        this.restrict = "E";
        this.templateUrl = "nexus-vpc/components/vpcList/vpcList-directive.html";
        this.replace = true;
        this.scope = {};
        this.controller = "VpcListController";
        this.controllerAs = "$vpcCtrl";
    }
    return VpcListDirective;
}());
exports.VpcListDirective = VpcListDirective;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcListController = /** @class */ (function () {
    /** @ngInject */
    VpcListController.$inject = ["vpcListService", "vpcViewService", "vpcHelperService"];
    function VpcListController(vpcListService, vpcViewService, vpcHelperService) {
        this.vpcListService = vpcListService;
        this.vpcViewService = vpcViewService;
        this.vpcHelperService = vpcHelperService;
        this.options = {
            selectionProperty: "vpcId",
            smartMode: false,
            showSelector: false,
            hideSearch: false,
            triggerSearchOnChange: true,
            hidePagination: false,
            showAddRemoveFilterProperty: false,
            templateUrl: "nexus-vpc/components/vpcList/vpcListItem.html",
            pageSize: 20,
            emptyData: {
                templateUrl: "nexus-vpc/components/vpcList/vpcListEmptyData.html"
            }
        };
        this.unknownPortHelpLinkFragment = "orionph-nexus-vpc-unknownport";
        this.unknownPortHelpLink = null;
        this.problematicLocalPortMemberInterfacesExists = false;
        this.problematicRemotePortMemberInterfacesExists = false;
        this.columnConfigWidthClass = "col-md-6";
    }
    VpcListController.prototype.$onInit = function () {
        this.initializeHelpLink();
    };
    VpcListController.prototype.initializeHelpLink = function () {
        var _this = this;
        this.vpcHelperService.getHelpUrl(this.unknownPortHelpLinkFragment).then(function (response) {
            _this.unknownPortHelpLink = response;
        }, function (error) {
            // Error is already logged but this is necessary to prevent warnings in console.
        });
    };
    VpcListController.prototype.getVpcs = function (filters, filterModels, pagination, sorting, search) {
        var filterParameters = {
            filterValues: filters,
            filterModels: filterModels,
            pagination: pagination,
            sorting: sorting,
            searching: search
        };
        var nodeId = this.vpcViewService.getNetObject().nodeId;
        var promiseResult = filterModels ?
            this.getAllData(nodeId, filterParameters) :
            this.getFilterProperties(nodeId, filterParameters);
        return promiseResult;
    };
    VpcListController.prototype.getAllData = function (nodeId, filterParameters) {
        var queryResult = this.vpcListService.getVpcs(nodeId, filterParameters);
        this.getVpcsFromQueryResult(queryResult);
        return queryResult;
    };
    VpcListController.prototype.getVpcsFromQueryResult = function (queryResult) {
        var _this = this;
        queryResult.then(function (response) {
            _this.items = response.items;
            _this.initializeProblematicInterfacesColumns();
            _this.initializecolumnConfigWidth();
        }, function (error) {
            // Error is already logged but this is necessary to prevent warnings in console.
        });
    };
    VpcListController.prototype.getFilterProperties = function (nodeId, filterParameters) {
        var promiseResult = this.vpcListService.getFilterProperties(nodeId, filterParameters);
        return promiseResult;
    };
    VpcListController.prototype.initializeProblematicInterfacesColumns = function () {
        var _this = this;
        if (!this.items) {
            return;
        }
        this.items.forEach(function (i) {
            if (!_this.problematicLocalPortMemberInterfacesExists) {
                _this.problematicLocalPortMemberInterfacesExists =
                    _this.vpcListService.problematicInterfacesExists(i.localPort);
            }
            if (!_this.problematicRemotePortMemberInterfacesExists) {
                _this.problematicRemotePortMemberInterfacesExists =
                    _this.vpcListService.problematicInterfacesExists(i.remotePort);
            }
        });
    };
    VpcListController.prototype.initializecolumnConfigWidth = function () {
        if (this.problematicLocalPortMemberInterfacesExists && this.problematicRemotePortMemberInterfacesExists) {
            this.columnConfigWidthClass = "col-md-2";
        }
        else if (this.problematicLocalPortMemberInterfacesExists || this.problematicRemotePortMemberInterfacesExists) {
            this.columnConfigWidthClass = "col-md-4";
        }
    };
    return VpcListController;
}());
exports.default = VpcListController;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcListEmptyData_directive_1 = __webpack_require__(20);
var vpcListEmptyData_controller_1 = __webpack_require__(21);
exports.default = (function (module) {
    module.controller("VpcListEmptyDataController", vpcListEmptyData_controller_1.default);
    module.component("nexusVpcListEmptyData", vpcListEmptyData_directive_1.VpcListEmptyDataDirective);
});


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcListEmptyDataDirective = /** @class */ (function () {
    function VpcListEmptyDataDirective() {
        this.restrict = "E";
        this.templateUrl = "nexus-vpc/components/vpcListEmptyData/vpcListEmptyData-directive.html";
        this.replace = true;
        this.scope = {};
        this.controller = "VpcListEmptyDataController";
        this.controllerAs = "$vpcListEmptyDataCtrl";
    }
    return VpcListEmptyDataDirective;
}());
exports.VpcListEmptyDataDirective = VpcListEmptyDataDirective;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcListEmptyDataController = /** @class */ (function () {
    /** @ngInject */
    VpcListEmptyDataController.$inject = ["vpcViewService", "cliCredentialsService", "vpcHelperService"];
    function VpcListEmptyDataController(vpcViewService, cliCredentialsService, vpcHelperService) {
        this.vpcViewService = vpcViewService;
        this.cliCredentialsService = cliCredentialsService;
        this.vpcHelperService = vpcHelperService;
        this.vpcConfigurationVpcHelpLinkFragment = "orionph-nexus-configure-vpcs";
        this.vpcConfigurationVpcHelpLink = null;
    }
    VpcListEmptyDataController.prototype.$onInit = function () {
        var _this = this;
        this.nodeId = this.vpcViewService.getNetObject().nodeId;
        this.cliCredentialsService.hasCliCredentials(this.nodeId).then(function (response) {
            _this.hasCliCredentials = response;
        }, function (error) {
            // Error is already logged but this is necessary to prevent warnings in console.
        });
        this.vpcHelperService.getHelpUrl(this.vpcConfigurationVpcHelpLinkFragment).then(function (response) {
            _this.vpcConfigurationVpcHelpLink = response;
        }, function (error) {
            // Error is already logged but this is necessary to prevent warnings in console.
        });
    };
    return VpcListEmptyDataController;
}());
exports.default = VpcListEmptyDataController;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcPort_directive_1 = __webpack_require__(23);
var vpcPort_controller_1 = __webpack_require__(24);
__webpack_require__(2);
exports.default = (function (module) {
    module.controller("VpcPortController", vpcPort_controller_1.default);
    module.component("nexusVpcPort", vpcPort_directive_1.VpcPortDirective);
});


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(2);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var VpcPortDirective = /** @class */ (function () {
    function VpcPortDirective() {
        this.restrict = "E";
        this.templateUrl = "nexus-vpc/components/vpcPort/vpcPort-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            port: "="
        };
        this.controller = "VpcPortController";
        this.controllerAs = "vm";
    }
    return VpcPortDirective;
}());
exports.VpcPortDirective = VpcPortDirective;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcPortController = /** @class */ (function () {
    /** @ngInject */
    VpcPortController.$inject = ["vpcListService"];
    function VpcPortController(vpcListService) {
        this.vpcListService = vpcListService;
        this.popoverTemplate = { url: "port-interfaces-list-template" };
        this.showInterfacesMonitoringButton = false;
    }
    VpcPortController.prototype.$onInit = function () {
        this.initializeInterfacesMonitoringButton();
    };
    VpcPortController.prototype.hasMemberInterface = function () {
        var result = this.port.memberInterfaces && this.port.memberInterfaces.length > 0;
        return result;
    };
    ;
    VpcPortController.prototype.initializeInterfacesMonitoringButton = function () {
        var _this = this;
        this.vpcListService.isInterfacesMonitoringSupported().then(function (isInterfacesMonitoringSupported) {
            _this.showInterfacesMonitoringButton = isInterfacesMonitoringSupported && !(_this.port.allMemberInterfacesMonitored && _this.port.detailsUrl);
        }, function (error) {
            // Error is already logged but this is necessary to prevent warnings in console.
        });
    };
    return VpcPortController;
}());
exports.default = VpcPortController;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcUnknownPort_directive_1 = __webpack_require__(26);
var vpcUnknownPort_controller_1 = __webpack_require__(27);
__webpack_require__(3);
exports.default = (function (module) {
    module.controller("VpcUnknownPortController", vpcUnknownPort_controller_1.default);
    module.component("nexusVpcUnknownPort", vpcUnknownPort_directive_1.VpcUnknownPortDirective);
});


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(3);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var VpcUnknownPortDirective = /** @class */ (function () {
    function VpcUnknownPortDirective() {
        this.restrict = "E";
        this.templateUrl = "nexus-vpc/components/vpcUnknownPort/vpcUnknownPort-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            helpLink: "="
        };
        this.controller = "VpcUnknownPortController";
        this.controllerAs = "vm";
    }
    return VpcUnknownPortDirective;
}());
exports.VpcUnknownPortDirective = VpcUnknownPortDirective;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcUnknownPortController = /** @class */ (function () {
    function VpcUnknownPortController() {
        this.popoverTemplate = { url: "unknown-port-template" };
    }
    return VpcUnknownPortController;
}());
exports.default = VpcUnknownPortController;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcPortHeader_directive_1 = __webpack_require__(29);
var vpcPortHeader_controller_1 = __webpack_require__(30);
exports.default = (function (module) {
    module.controller("VpcPortHeaderController", vpcPortHeader_controller_1.default);
    module.component("nexusVpcPortHeader", vpcPortHeader_directive_1.VpcPortHeaderDirective);
});


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var VpcPortHeaderDirective = /** @class */ (function () {
    function VpcPortHeaderDirective() {
        this.restrict = "E";
        this.templateUrl = "nexus-vpc/components/vpcPortHeader/vpcPortHeader-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            port: "="
        };
        this.controller = "VpcPortHeaderController";
        this.controllerAs = "vm";
    }
    return VpcPortHeaderDirective;
}());
exports.VpcPortHeaderDirective = VpcPortHeaderDirective;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcPortHeaderController = /** @class */ (function () {
    /** @ngInject */
    VpcPortHeaderController.$inject = ["$window"];
    function VpcPortHeaderController($window) {
        this.$window = $window;
    }
    VpcPortHeaderController.prototype.redirectToInterface = function () {
        this.$window.location.href = this.port.detailsUrl;
    };
    return VpcPortHeaderController;
}());
exports.default = VpcPortHeaderController;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcProblematicInterfaces_directive_1 = __webpack_require__(32);
var vpcProblematicInterfaces_controller_1 = __webpack_require__(33);
__webpack_require__(4);
exports.default = (function (module) {
    module.controller("VpcProblematicInterfacesController", vpcProblematicInterfaces_controller_1.default);
    module.component("nexusVpcProblematicInterfaces", vpcProblematicInterfaces_directive_1.VpcProblematicInterfacesDirective);
});


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(4);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var VpcProblematicInterfacesDirective = /** @class */ (function () {
    function VpcProblematicInterfacesDirective() {
        this.restrict = "E";
        this.templateUrl = "nexus-vpc/components/vpcProblematicInterfaces/vpcProblematicInterfaces-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            interfaces: "="
        };
        this.controller = "VpcProblematicInterfacesController";
        this.controllerAs = "vm";
    }
    return VpcProblematicInterfacesDirective;
}());
exports.VpcProblematicInterfacesDirective = VpcProblematicInterfacesDirective;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcProblematicInterfacesController = /** @class */ (function () {
    /** @ngInject */
    VpcProblematicInterfacesController.$inject = ["vpcProblematicInterfacesService"];
    function VpcProblematicInterfacesController(vpcProblematicInterfacesService) {
        this.vpcProblematicInterfacesService = vpcProblematicInterfacesService;
    }
    VpcProblematicInterfacesController.prototype.anyProblematicInterfaces = function () {
        return this.interfaces && this.getProblematicInterfacesCount() > 0;
    };
    ;
    VpcProblematicInterfacesController.prototype.getProblematicInterfacesCount = function () {
        return this.vpcProblematicInterfacesService.getProblematicInterfaces(this.interfaces).length;
    };
    ;
    VpcProblematicInterfacesController.prototype.getProblematicInterfacesWorstStatus = function () {
        return this.vpcProblematicInterfacesService.getProblematicInterfacesWorstStatus(this.interfaces);
    };
    return VpcProblematicInterfacesController;
}());
exports.default = VpcProblematicInterfacesController;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcView_service_1 = __webpack_require__(35);
var vpcList_service_1 = __webpack_require__(36);
var cliCredentials_service_1 = __webpack_require__(37);
var vpcHelper_service_1 = __webpack_require__(38);
var vpcProblematicInterfaces_service_1 = __webpack_require__(39);
exports.default = (function (module) {
    // register services
    module.service("vpcViewService", vpcView_service_1.default);
    module.service("vpcListService", vpcList_service_1.default);
    module.service("cliCredentialsService", cliCredentials_service_1.default);
    module.service("vpcHelperService", vpcHelper_service_1.default);
    module.service("vpcProblematicInterfacesService", vpcProblematicInterfaces_service_1.default);
});


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
//Temporary solution until the final nature of the static subviews is known
var VpcViewService = /** @class */ (function () {
    /** @ngInject */
    VpcViewService.$inject = ["$log", "$location"];
    function VpcViewService($log, $location) {
        this.$log = $log;
        this.$location = $location;
        this.netObject = null;
    }
    VpcViewService.prototype.getNetObject = function () {
        try {
            if (this.netObject) {
                return this.netObject;
            }
            var netObjectQueryString = this.$location.search().NetObject;
            var nodeId = parseInt((decodeURIComponent(netObjectQueryString)
                .match(/N\:([0-9]+)/)
                ? RegExp.$1
                : "0"), 10);
            this.netObject = {
                nodeId: nodeId
            };
            return this.netObject;
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    return VpcViewService;
}());
exports.default = VpcViewService;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiGetVpcsUrlFragment = "nexus-vpc/nodes/{nodeId}/vpcs";
exports.swApiGetFilterPropertiesUrlFragment = exports.swApiGetVpcsUrlFragment + "/noitems";
exports.swApiIsInterfacesMonitoringSupportedUrl = "nexus-vpc/interfaces/supported";
var VpcListService = /** @class */ (function () {
    /** @ngInject */
    VpcListService.$inject = ["swApi", "vpcProblematicInterfacesService"];
    function VpcListService(swApi, vpcProblematicInterfacesService) {
        this.swApi = swApi;
        this.vpcProblematicInterfacesService = vpcProblematicInterfacesService;
        this.interfacesMonitoringSupported = undefined;
    }
    VpcListService.prototype.getVpcs = function (nodeId, filterParameters) {
        return this.getFilteredListQueryResults(exports.swApiGetVpcsUrlFragment.replace("{nodeId}", nodeId.toString()), filterParameters);
    };
    VpcListService.prototype.getFilterProperties = function (nodeId, filterParameters) {
        return this.getFilteredListQueryResults(exports.swApiGetFilterPropertiesUrlFragment.replace("{nodeId}", nodeId.toString()), filterParameters);
    };
    VpcListService.prototype.getFilteredListQueryResults = function (route, filterParameters) {
        return this.swApi
            .api(false)
            .one(route)
            .customPOST(filterParameters);
    };
    VpcListService.prototype.problematicInterfacesExists = function (port) {
        var _this = this;
        if (!port || !port.memberInterfaces) {
            return false;
        }
        var result = port.memberInterfaces.some(function (i) {
            return _this.vpcProblematicInterfacesService.isInterfaceProblematic(i.status);
        }) === true;
        return result;
    };
    VpcListService.prototype.isInterfacesMonitoringSupported = function () {
        if (this.interfacesMonitoringSupported === undefined) {
            this.interfacesMonitoringSupported = this.swApi
                .api(false)
                .one(exports.swApiIsInterfacesMonitoringSupportedUrl)
                .get()
                .then(function (interfacesMonitoringSupported) {
                return interfacesMonitoringSupported === true;
            });
        }
        return this.interfacesMonitoringSupported;
    };
    return VpcListService;
}());
exports.default = VpcListService;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiHasCliCredentialsUrlFragment = "cli/credentials/nodes/{nodeId}/existence";
var CliCredentialsService = /** @class */ (function () {
    /** @ngInject */
    CliCredentialsService.$inject = ["swApi"];
    function CliCredentialsService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.hasCliCredentials = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiHasCliCredentialsUrlFragment.replace("{nodeId}", nodeId.toString()))
                .get()
                .then(function (hasCredentials) {
                return hasCredentials === true;
            });
        };
    }
    return CliCredentialsService;
}());
exports.default = CliCredentialsService;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiGetHelpUrlFragment = "nexus-vpc/help-url/{0}";
var VpcHelperService = /** @class */ (function () {
    /** @ngInject */
    VpcHelperService.$inject = ["swApi"];
    function VpcHelperService(swApi) {
        this.swApi = swApi;
    }
    VpcHelperService.prototype.getHelpUrl = function (helpUrlFragment) {
        return this.swApi
            .api(false)
            .one(exports.swApiGetHelpUrlFragment.replace("{0}", helpUrlFragment))
            .get();
    };
    ;
    return VpcHelperService;
}());
exports.default = VpcHelperService;


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcProblematicInterfacesService = /** @class */ (function () {
    function VpcProblematicInterfacesService() {
        this.problematicStatuses = [2 /* Down */, 3 /* Warning */];
    }
    VpcProblematicInterfacesService.prototype.isInterfaceProblematic = function (interfaceStatus) {
        return this.problematicStatuses.indexOf(interfaceStatus) >= 0;
    };
    VpcProblematicInterfacesService.prototype.getProblematicInterfacesWorstStatus = function (interfaces) {
        if (interfaces.some(function (i) { return i.status === 2 /* Down */; }) === true) {
            return 2 /* Down */;
        }
        return 3 /* Warning */;
    };
    VpcProblematicInterfacesService.prototype.getProblematicInterfaces = function (interfaces) {
        var _this = this;
        var result = [];
        interfaces.forEach(function (i) {
            if (_this.isInterfaceProblematic(i.status)) {
                result.push(i);
            }
        });
        return result;
    };
    return VpcProblematicInterfacesService;
}());
exports.default = VpcProblematicInterfacesService;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(41);
    req.keys().forEach(function (r) {
        var key = "nexus-vpc" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = (function (module) {
    module.app().run(templates);
});


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/vpcList/vpcList-directive.html": 42,
	"./components/vpcList/vpcListEmptyData.html": 43,
	"./components/vpcList/vpcListItem.html": 44,
	"./components/vpcListEmptyData/vpcListEmptyData-directive.html": 45,
	"./components/vpcPort/vpcPort-directive.html": 46,
	"./components/vpcPortHeader/vpcPortHeader-directive.html": 47,
	"./components/vpcProblematicInterfaces/vpcProblematicInterfaces-directive.html": 48,
	"./components/vpcUnknownPort/vpcUnknownPort-directive.html": 49,
	"./views/vpcList/vpcList.html": 0
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 41;

/***/ }),
/* 42 */
/***/ (function(module, exports) {

module.exports = "<div class=vpc-list ng-init=$vpcCtrl.init()> <sw-server-side-filtered-list parent-controller=$vpcCtrl options=$vpcCtrl.options getdata=\"$vpcCtrl.getVpcs(filters, filterModels, pagination, sorting, searching)\"> </sw-server-side-filtered-list> </div> ";

/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = "<div class=xui-empty> <nexus-vpc-list-empty-data></nexus-vpc-list-empty-data> </div>";

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row vpc-list-item\"> <div class=col-md-1> <div class=media> <div class=media-left> <xui-icon css-class=vpc-icon icon=nexus-vpc status=\"{{item.status | swStatus}}\"></xui-icon> </div> <div class=media-body> <span class=vpc-name>{{::item.name}}</span> </div> </div> </div> <div class=col-md-2> <nexus-vpc-port port=item.localPort name=localPortItem ng-if=item.localPort></nexus-vpc-port> </div> <div class=col-md-2 ng-if=vm.problematicLocalPortMemberInterfacesExists name=localProblematicInterfacesColumn> <nexus-vpc-problematic-interfaces interfaces=item.localPort.memberInterfaces name=localProblematicInterfaces ng-if=item.localPort></nexus-vpc-problematic-interfaces> </div> <div class=col-md-1> <xui-divider name=vpcPortDivider></xui-divider> </div> <div class=col-md-2> <nexus-vpc-port port=item.remotePort name=remotePortItem ng-if=item.remotePort></nexus-vpc-port> <nexus-vpc-unknown-port help-link=vm.unknownPortHelpLink name=unknownPortItem ng-if=!item.remotePort></nexus-vpc-unknown-port> </div> <div class=col-md-2 ng-if=vm.problematicRemotePortMemberInterfacesExists name=remoteProblematicInterfacesColumn> <nexus-vpc-problematic-interfaces interfaces=item.remotePort.memberInterfaces name=remoteProblematicInterfaces ng-if=item.remotePort></nexus-vpc-problematic-interfaces> </div> <div class={{vm.columnConfigWidthClass}} name=columnConfig> <div class=text-right ng-if=\"item.configDetails != null\"> <a ng-if=item.configDetails.isValid target=_self href={{item.configDetails.link}}> <span ng-repeat=\"label in item.configDetails.messages\"> {{label}} </span> </a> <div ng-if=\"item.configDetails.isValid === false\"> <span ng-repeat=\"label in item.configDetails.messages\"> {{label}} </span> </div> </div> </div> </div>";

/***/ }),
/* 45 */
/***/ (function(module, exports) {

module.exports = "<div class=vpc-list-empty-data> <xui-image size=100% image=no-data-to-show class=\"xui-empty_image empty-pic\"> </xui-image> <div ng-if=\"$vpcListEmptyDataCtrl.hasCliCredentials == false\" class=vpc-list-missing-cli-credentials> <p class=xui-text-l _t>CLI Credentials required.</p> <p class=xui-text-r _t>Please add your CLI Credentials to monitor vPCs.&nbsp;<sw-node-management-link node-management-page=NodeProperties.aspx node-id={{::$vpcListEmptyDataCtrl.nodeId}}>» Add CLI credentials</sw-node-management-link></p> </div> <div ng-if=$vpcListEmptyDataCtrl.hasCliCredentials class=vpc-list-configuration-help> <p class=xui-text-r _t>No vPC data available. <a ng-if=$vpcListEmptyDataCtrl.vpcConfigurationVpcHelpLink href={{::$vpcListEmptyDataCtrl.vpcConfigurationVpcHelpLink}} class=\"xui-text-a configuration-help-url\" target=_blank _t>» Learn more</a></p> </div> </div> ";

/***/ }),
/* 46 */
/***/ (function(module, exports) {

module.exports = "<span class=vpc-port> <div class=media> <div class=media-left> <xui-icon css-class=icon icon=\"{{vm.port.status | swOrionObjectStatusToIcon}}\" icon-size=small></xui-icon> </div> <div class=media-body> <xui-popover xui-popover-content=vm.popoverTemplate xui-popover-trigger=mouseenter xui-popover-title={{vm.port.name}} xui-popover-status=\"{{vm.port.status | swStatus}}\" ng-if=vm.hasMemberInterface()> <nexus-vpc-port-header name=portHeader port=vm.port></nexus-vpc-port-header> </xui-popover> <nexus-vpc-port-header name=portHeader port=vm.port ng-if=!vm.hasMemberInterface()></nexus-vpc-port-header> </div> </div> <div> <div class=xui-text-dscrn ng-if=vm.port.nodeCaption> <span _t>on </span> <a class=node-details-url href={{::vm.port.nodeDetailsUrl}} ng-if=vm.port.nodeDetailsUrl> <span class=\"node-caption-text xui-text-a\">{{::vm.port.nodeCaption}}</span> </a> <span class=node-caption-text ng-if=!vm.port.nodeDetailsUrl>{{::vm.port.nodeCaption}}</span> </div> </div> <script type=text/ng-template id=port-interfaces-list-template> <div class=\"vpc-start-monitoring-button\" ng-if=\"vm.showInterfacesMonitoringButton\">\n            <sw-node-management-link \n                node-management-page=\"ListResources.aspx\" \n                node-id=\"{{::vm.port.nodeId}}\">\n\n                <xui-icon icon=\"add\" icon-size=\"small\"></xui-icon>\n                <span _t>Start Monitoring Unknown Interfaces</span>\n            </sw-node-management-link>\n        </div>\n        <div class=\"xui-margin-md-neg\">\n            <xui-listview stripe=\"true\"\n                items-source=\"vm.port.memberInterfaces\"\n                template-url=\"port-interface-template\"\n                row-padding=\"narrow\">\n            </xui-listview>\n        </div> </script> <script type=text/ng-template id=port-interface-template> <div class=\"port-interface-popup-item\">\n            <xui-icon css-class=\"interface-icon\" icon=\"network-interface\" status=\"{{item.status | swStatus}}\"></xui-icon>\n            <a href={{item.detailsUrl}} class=\"interface-detail-url\" ng-if=\"item.detailsUrl\">\n                <span class=\"interface-name\">{{::item.webConsoleDisplayName}}</span>\n            </a>\n            <span class=\"interface-name\" ng-if=\"!item.detailsUrl\">{{::item.webConsoleDisplayName}}</span>\n        </div> </script> </span>";

/***/ }),
/* 47 */
/***/ (function(module, exports) {

module.exports = "<span class=vpc-port-header> <a class=details-url href=# ng-if=vm.port.detailsUrl ng-click=vm.redirectToInterface()> <span class=name>{{::vm.port.name}}</span> </a> <span class=name ng-if=!vm.port.detailsUrl>{{::vm.port.name}}</span> </span>";

/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = "<span class=vpc-problematic-interfaces> <div ng-if=vm.anyProblematicInterfaces()> <div class=media> <div class=media-left> <xui-icon css-class=icon icon=\"{{vm.getProblematicInterfacesWorstStatus() | swOrionObjectStatusToIcon}}\" icon-size=small></xui-icon> </div> <div class=media-body> <span class=count-text>{{vm.getProblematicInterfacesCount()}}</span> </div> </div> <div class=xui-text-dscrn _t> Interfaces with problems </div> </div> </span>";

/***/ }),
/* 49 */
/***/ (function(module, exports) {

module.exports = "<span class=unknown-vpc-port> <div class=media> <div class=media-left> <xui-icon css-class=icon icon=\"status_undefined\" icon-size=small></xui-icon> </div> <div class=media-body> <xui-popover xui-popover-content=vm.popoverTemplate xui-popover-trigger=mouseenter xui-popover-title=_t(Unknown) _ta xui-popover-status=unknown> <span class=\"name xui-text-a\" _t>Unknown</span> </xui-popover> </div> </div> <div class=xui-text-dscrn _t> on <span class=node-caption-text>Unknown</span> </div> <script type=text/ng-template id=unknown-port-template> <div class=\"xui-text-s\" _t>vPC peer was not found.</div>\n        <a ng-if=\"vm.helpLink\" href=\"{{::vm.helpLink}}\" class=\"xui-text-a help-url\" target=\"_blank\" _t>» Learn more</a> </script> </span>";

/***/ })
/******/ ]);
//# sourceMappingURL=nexus-vpc.js.map