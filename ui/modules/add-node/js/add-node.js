/*!
 * @solarwinds/add-node 2.0.0-541
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 19);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of services provided by Angular and related libraries
 */
exports.NgServiceNames = {
    ngWindowService: "$window",
    ngQService: "$q",
    ngScope: "$scope",
    ngState: "$state",
    ngStateParams: "$stateParams",
    ngProvideService: "$provide",
    ngControllerService: "$controller",
    ngRootScopeService: "$rootScope",
    ngHttpBackendService: "$httpBackend",
    ngInjectorService: "$injector",
    ngInterval: "$interval",
    ngTimeoutService: "$timeout"
};
/**
 * Names of services provided by Apollo
 */
exports.ApolloServiceNames = {
    toastService: "xuiToastService",
    getTextService: "getTextService",
    swApiService: "swApi",
    swUtilService: "swUtil",
    swisService: "swisService",
    dateTimeService: "swDateTimeService",
    dialogService: "xuiDialogService",
    wizardDialogService: "xuiWizardDialogService",
    swDemoService: "swDemoService",
    helpLinkService: "helpLinkService"
};
/**
 * Service names used by Add Node
 *
 * @class
 */
exports.ServiceNames = {
    routingService: "addNodeRoutingService",
    addNodeWizardService: "addNodeWizardService"
};


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Helper function for autogenerating of class's $inject property
 *
 * @param {string} dependency Name of a dependency under which injected component is registered (e.g. xuiToastService)
 * @example
 * import {Inject} from "decorators/di";
 * class MyClass { constructor(@Inject("xuiToastService") private svc: IXuiToastService){} };
 */
function Inject(dependency) {
    return function (prototype, method, argumentPosition) {
        prototype.$inject = prototype.$inject || [];
        prototype.$inject[argumentPosition] = dependency;
    };
}
exports.Inject = Inject;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(36));
__export(__webpack_require__(16));
__export(__webpack_require__(37));
__export(__webpack_require__(38));
__export(__webpack_require__(17));
__export(__webpack_require__(39));
__export(__webpack_require__(40));


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of all registered providers
 *
 * @class
 */
var ProviderNames = /** @class */ (function () {
    function ProviderNames() {
    }
    ProviderNames.ngStateProvider = "$stateProvider";
    ProviderNames.ngUrlRouterProvider = "$urlRouterProvider";
    ProviderNames.routingRegistrationProvider = "addNodeRoutingRegistrationProvider";
    return ProviderNames;
}());
exports.ProviderNames = ProviderNames;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(30));
__export(__webpack_require__(6));
__export(__webpack_require__(14));
__export(__webpack_require__(15));
__export(__webpack_require__(13));
__export(__webpack_require__(31));


/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * AddNode module names
 *
 * @const
 */
exports.ModuleNames = {
    main: "add-node",
    providers: "add-node.providers",
    services: "add-node.services",
    filters: "add-node.filters",
    components: "add-node.components"
};


/***/ }),
/* 7 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 8 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 9 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 10 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 11 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 12 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Possible orion user roles
 */
exports.Roles = {
    admin: "admin",
    demo: "demo"
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Route names used by capacity planning
 *
 * @const
 */
exports.RouteNames = {
    addNodeWizardStateName: "addNodeWizard"
};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Urls for routes used by Add Node
 *
 * @const
 */
exports.RouteUrls = {
    addNodeWizardUrl: "/add-node?serverAddress&pollingEngine"
};


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pollingMethod_1 = __webpack_require__(17);
;
/**
 * Default implementation for polling definition interface
 *
 * @class
 * @implements IPollingDefinition
 */
var PollingDefinition = /** @class */ (function () {
    function PollingDefinition() {
        this.pollingMethod = new pollingMethod_1.PollingMethod();
    }
    return PollingDefinition;
}());
exports.PollingDefinition = PollingDefinition;
;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Default implementation for polling method interface
 *
 * @class
 * @implements IPollingMethod
 */
var PollingMethod = /** @class */ (function () {
    function PollingMethod() {
    }
    return PollingMethod;
}());
exports.PollingMethod = PollingMethod;
;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    /**
     * constant for credential name
     */
    Constants.credentialName = "name";
    /**
     * constant for credential description
     */
    Constants.credentialDescription = "description";
    /**
     * constant for credential username
     */
    Constants.credentialUsername = "username";
    /**
     * constant for credential password
     */
    Constants.credentialPassword = "password";
    /**
     * constant for credential confirm password
     */
    Constants.credentialConfirmPassword = "confirmPassword";
    return Constants;
}());
exports.Constants = Constants;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(5);
module.exports = __webpack_require__(20);


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(21);
var index_1 = __webpack_require__(22);
var index_2 = __webpack_require__(28);
var index_3 = __webpack_require__(41);
var index_4 = __webpack_require__(45);
var index_5 = __webpack_require__(49);
var addNodeModule = app_1.default();
index_1.default(addNodeModule);
index_5.default(addNodeModule);
index_4.default(addNodeModule);
index_2.default(addNodeModule);
index_3.default(addNodeModule);
exports.default = addNodeModule;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var moduleNames_1 = __webpack_require__(6);
/**
 * Helper function indicating whether code is currently under test
 */
exports.isUnderTest = function () {
    return !!angular.mock;
};
/** @ngInject */
var run = function ($log) {
    $log.info("Run module: AddNode");
};
run.$inject = ["$log"];
exports.default = function () {
    angular.module(moduleNames_1.ModuleNames.providers, []);
    angular.module(moduleNames_1.ModuleNames.services, []);
    angular.module(moduleNames_1.ModuleNames.components, []);
    angular.module(moduleNames_1.ModuleNames.filters, []);
    angular.module(moduleNames_1.ModuleNames.main, [
        "ui.router",
        "orion",
        "orion-ui-components",
        moduleNames_1.ModuleNames.providers,
        moduleNames_1.ModuleNames.services,
        moduleNames_1.ModuleNames.components,
        moduleNames_1.ModuleNames.filters,
    ]);
    var module = Xui.registerModule(moduleNames_1.ModuleNames.main);
    module.app().run(run);
    return module;
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = __webpack_require__(23);
__webpack_require__(24);
var routes_1 = __webpack_require__(27);
exports.default = function (module) {
    http_1.default(module);
    routes_1.default(module);
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function configHttp($httpProvider) {
    // configure $http service to combine processing of multiple http responses received at around the same time 
    $httpProvider.useApplyAsync(true);
}
exports.configHttp = configHttp;
;
configHttp.$inject = ["$httpProvider"];
exports.default = function (module) {
    module.config(configHttp);
};


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */

/**
 * Requires dynamic context for .less files - i.e. loads all of them
 */
var req = __webpack_require__(25);


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./directives/addNodeWizard/addNodeWizard-directive.less": 7,
	"./directives/addNodeWizard/steps/metricsStep/metricsStep-directive.less": 8,
	"./directives/addNodeWizard/steps/objectTypeStep/objectTypeStep-directive.less": 9,
	"./directives/addNodeWizard/steps/pollingStep/credentialSelector/credentialSelector-directive.less": 10,
	"./directives/addNodeWizard/steps/pollingStep/pollingEngineSelector/pollingEngineSelector-directive.less": 11,
	"./directives/addNodeWizard/steps/pollingStep/pollingStep-directive.less": 12,
	"./styles.less": 5,
	"./styles/styles.less": 26
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 25;

/***/ }),
/* 26 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(3);
/**
 * Configures basic routing settings on config event
 *
 * @function
 */
var configureRouting = function (provider) {
    provider.registerRoutes();
};
exports.default = function (module) {
    module.app().config([index_1.ProviderNames.routingRegistrationProvider, configureRouting]);
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var routing_service_1 = __webpack_require__(29);
var addNodeWizard_service_1 = __webpack_require__(32);
var addNodeWizard_service_demo_1 = __webpack_require__(33);
exports.default = function (module) {
    module.service(index_1.ServiceNames.routingService, routing_service_1.RoutingService);
    module.service(index_1.ServiceNames.addNodeWizardService, addNodeWizard_service_1.AddNodeWizardService);
    module.app().decorator(index_1.ServiceNames.addNodeWizardService, addNodeWizard_service_demo_1.AddNodeWizardServiceDemoDecorator);
};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(4);
var index_2 = __webpack_require__(0);
/**
 * Routing service
 */
var RoutingService = /** @class */ (function () {
    function RoutingService($state, $window, swUtil) {
        var _this = this;
        this.$state = $state;
        this.$window = $window;
        this.swUtil = swUtil;
        this.goToAddNodeWizard = function () {
            _this.$state.go(index_1.RouteNames.addNodeWizardStateName, undefined, {
                reload: true
            });
        };
        this.goToVirtualizationSummary = function () {
            _this.$window.location.href = "/Orion/VIM/Summary.aspx?view=VIM%20Summary";
        };
        this.getAddNodeWizardHref = function () {
            return _this.$state.href(index_1.RouteNames.addNodeWizardStateName);
        };
        this.getNodeDetailsHref = function (nodeId) {
            return _this.swUtil.formatString("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", nodeId);
        };
    }
    RoutingService = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngState)),
        __param(1, di_1.Inject(index_2.NgServiceNames.ngWindowService)),
        __param(2, di_1.Inject(index_2.ApolloServiceNames.swUtilService)),
        __metadata("design:paramtypes", [Object, Object, Object])
    ], RoutingService);
    return RoutingService;
}());
exports.RoutingService = RoutingService;
;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var roles_1 = __webpack_require__(13);
/**
 * Auth profiles used by Add Node
 *
 * @const
 * @var
 */
exports.AuthProfiles = {
    guestProfile: {
        roles: [],
        permissions: []
    },
    adminProfile: {
        roles: [roles_1.Roles.admin],
        permissions: []
    },
    adminProfileDemoAllowed: {
        roles: [roles_1.Roles.admin, roles_1.Roles.demo],
        permissions: []
    }
};


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baseUrl = "add-node";
var getPollingInfoMethod = "polling-info";
var getPollingTypesMethod = "get-polling-types";
var getSettingsMethod = "get-settings";
var getResultMethod = "get-result";
var verifyCredentialsMethod = "credentials/verify-credentials";
var verifyNodeDuplicationMethod = "verify-node-duplication";
var createDiscoveryJobMethod = "create-discovery-job";
var getMetricsMethod = "get-metrics";
var addNodeMethod = "add-node";
var verifyServerAddressMethod = "verify-node-address";
var verifyNodeDuplicateMethod = "verify-node-duplicate";
var isAuthorized = "is-authorized";
var getCredentialsMethod = "credentials/get-credentials";
/**
 * Backend urls used by Add Node module
 *
 * @const
 */
exports.SwApiUrls = {
    baseUrl: baseUrl,
    getPollingInfo: baseUrl + "/" + getPollingInfoMethod,
    getPollingTypes: baseUrl + "/" + getPollingTypesMethod,
    getAddNodeSettings: baseUrl + "/" + getSettingsMethod,
    getMetrics: baseUrl + "/" + getMetricsMethod,
    verifyServerAddressMethod: baseUrl + "/" + verifyServerAddressMethod,
    getResult: baseUrl + "/" + getResultMethod,
    verifyCredentialsMethod: verifyCredentialsMethod,
    verifyNodeDuplicationMethod: verifyNodeDuplicationMethod,
    createDiscoveryJobMethod: createDiscoveryJobMethod,
    addNodeMethod: addNodeMethod,
    isAuthorized: baseUrl + "/" + isAuthorized,
    getCredentials: baseUrl + "/" + getCredentialsMethod
};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(4);
/**
* Service providing data for add node wizard
*/
var AddNodeWizardService = /** @class */ (function () {
    function AddNodeWizardService($q, swApi) {
        var _this = this;
        this.$q = $q;
        this.swApi = swApi;
        this.createDiscoveryJob = function (definition) {
            var request = {
                addNodeDefinition: definition
            };
            return _this.swApi.api(false).one(index_2.SwApiUrls.baseUrl).post(index_2.SwApiUrls.createDiscoveryJobMethod, request);
        };
        this.getMetrics = function (pluginType, discoveryJobId) {
            return _this.swApi.api(false).one(index_2.SwApiUrls.getMetrics).get({ pluginType: pluginType, jobId: discoveryJobId })
                .then(function (result) { return result.plain(); });
            ;
        };
        this.addNode = function (definition) {
            var request = {
                jobId: definition.discoveryJobId,
                addNodeDefinition: definition
            };
            return _this.swApi.api(false).one(index_2.SwApiUrls.baseUrl).post(index_2.SwApiUrls.addNodeMethod, request);
        };
        this.getSettings = function () {
            if (_this.addNodeSettings === undefined) {
                return _this.swApi.api(false).one(index_2.SwApiUrls.getAddNodeSettings).get().then(function (x) {
                    _this.addNodeSettings = x;
                    return _this.addNodeSettings;
                });
            }
            return _this.$q.resolve(_this.addNodeSettings);
        };
        this.verifyCredentials = function (ipAddress, engineId, pluginType, insertAfterValidation, credentialId, properties) {
            var request = {
                pluginType: pluginType,
                ipAddress: ipAddress,
                engineId: engineId,
                credentialId: credentialId,
                insertAfterValidation: insertAfterValidation,
                properties: properties
            };
            return _this.swApi.api(false).one(index_2.SwApiUrls.baseUrl).post(index_2.SwApiUrls.verifyCredentialsMethod, request);
        };
        this.verifyNodeDuplication = function (ipAddress, hostname, engineId) {
            var request = {
                ipAddress: ipAddress,
                hostname: hostname,
                engineId: engineId
            };
            return _this.swApi.api(false).one(index_2.SwApiUrls.baseUrl).post(index_2.SwApiUrls.verifyNodeDuplicationMethod, request);
        };
        this.getPollingInfo = function (pluginType) {
            return _this.swApi.api(false).one(index_2.SwApiUrls.getPollingInfo).get({ type: pluginType });
        };
        this.getCredentials = function (credentialRequest) {
            return _this.swApi.api(false).one(index_2.SwApiUrls.getCredentials).get({
                pluginType: credentialRequest.pluginType,
                pollingMethod: credentialRequest.pollingMethodType
            });
        };
        this.getPluginTiles = function () {
            return _this.swApi.api(false).one(index_2.SwApiUrls.getPollingTypes).get()
                .then(function (result) { return result.plain(); });
        };
        this.verifyServerAddress = function (address) {
            return _this.swApi.api(false).one(index_2.SwApiUrls.verifyServerAddressMethod).get({ serverAddress: address });
        };
        this.getDiscoveryResult = function (jobId) {
            return _this.swApi.api(false).one(index_2.SwApiUrls.getResult).get({ jobId: jobId });
        };
        this.isAuthorized = function () {
            return _this.swApi.api(false).one(index_2.SwApiUrls.isAuthorized).get()
                .then(function (result) { return result.plain(); });
        };
    }
    AddNodeWizardService = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.swApiService)),
        __metadata("design:paramtypes", [Function, Object])
    ], AddNodeWizardService);
    return AddNodeWizardService;
}());
exports.AddNodeWizardService = AddNodeWizardService;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(34);
var services_1 = __webpack_require__(0);
var addNodeWizard_1 = __webpack_require__(2);
var AddNodeWizardServiceDemoDecorator = /** @class */ (function () {
    function AddNodeWizardServiceDemoDecorator($delegate, $q, $timeout, _t, swDemoService) {
        if (!swDemoService.isDemoMode()) {
            return $delegate;
        }
        $delegate.verifyCredentials = function (ipAddress, engineId, pluginType, insertAfterValidation, credentialId, properties) {
            if (_.isUndefined(credentialId)) {
                var rejected = {
                    isValid: false,
                    failureReason: addNodeWizard_1.FailureReason.General,
                    messageLevel: addNodeWizard_1.MessageLevel.Error,
                    message: _t("THIS FEATURE IS NOT AVAILABLE IN THE ONLINE DEMO"),
                    messageMinor: undefined
                };
                return $q.resolve(rejected);
            }
            var resolved = {
                isValid: true,
                failureReason: addNodeWizard_1.FailureReason.None,
                messageLevel: undefined,
                message: undefined,
                messageMinor: undefined
            };
            return $q.resolve(resolved);
        };
        $delegate.verifyNodeDuplication = function (ipAddress, hostname, engineId) {
            return $q.resolve({
                isValid: true,
                failureReason: addNodeWizard_1.FailureReason.None,
                messageLevel: undefined,
                message: undefined,
                messageMinor: undefined
            });
        };
        $delegate.createDiscoveryJob = function (definition) {
            var def = $q.defer();
            $timeout(function () {
                def.resolve("00000000-0000-0000-0000-000000000000");
            }, 5000);
            return def.promise;
        };
        $delegate.getDiscoveryResult = function (jobId) {
            return $q.resolve({});
        };
        return $delegate;
    }
    AddNodeWizardServiceDemoDecorator = __decorate([
        __param(0, decorators_1.Inject("$delegate")),
        __param(1, decorators_1.Inject(services_1.NgServiceNames.ngQService)),
        __param(2, decorators_1.Inject(services_1.NgServiceNames.ngTimeoutService)),
        __param(3, decorators_1.Inject(services_1.ApolloServiceNames.getTextService)),
        __param(4, decorators_1.Inject(services_1.ApolloServiceNames.swDemoService)),
        __metadata("design:paramtypes", [Object, Function, Function, Function, Object])
    ], AddNodeWizardServiceDemoDecorator);
    return AddNodeWizardServiceDemoDecorator;
}());
exports.AddNodeWizardServiceDemoDecorator = AddNodeWizardServiceDemoDecorator;
;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(1));
__export(__webpack_require__(35));


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Generates get/set methods for a property
 * @param {string} [storageObject] Name of a backend property to store the real value
 */
function AutoProperty(storageObject) {
    return function (target, name) {
        var variableName = storageObject ? name : "_" + name;
        Object.defineProperty(target, name, {
            get: function () {
                if (storageObject) {
                    return this[storageObject][variableName];
                }
                return this[variableName];
            },
            set: function (value) {
                // on changing
                if (_.isFunction(this["onPropertyChanging"])) {
                    var handler = this["onPropertyChanging"];
                    value = handler(name, value);
                }
                if (storageObject) {
                    this[storageObject][variableName] = value;
                }
                else {
                    this[variableName] = value;
                }
                // on changed
                if (_.isFunction(this["onPropertyChanged"])) {
                    var handler = this["onPropertyChanged"];
                    handler(name, value);
                }
            },
            enumerable: true,
            configurable: true
        });
    };
}
exports.AutoProperty = AutoProperty;
;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pollingDefinition_1 = __webpack_require__(16);
;
/**
 * Default implementation for add node definition interface
 *
 * @class
 * @implements IAddNodeDefinition
 */
var AddNodeDefinition = /** @class */ (function () {
    function AddNodeDefinition() {
        this.pollingDefinition = new pollingDefinition_1.PollingDefinition();
        this.credentials = [];
    }
    return AddNodeDefinition;
}());
exports.AddNodeDefinition = AddNodeDefinition;
;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Specifies what caused validation to fail.
 *
 * @enum
 */
var FailureReason;
(function (FailureReason) {
    /**
     * None
     */
    FailureReason[FailureReason["None"] = 0] = "None";
    /**
     * Not specified
     */
    FailureReason[FailureReason["General"] = 1] = "General";
    /**
     * Ip address or hostname
     */
    FailureReason[FailureReason["IpAddress"] = 2] = "IpAddress";
    /**
     * Credentials in general
     */
    FailureReason[FailureReason["Credential"] = 3] = "Credential";
    /**
     * Username in credential
     */
    FailureReason[FailureReason["CredentialName"] = 4] = "CredentialName";
    /**
     * Password in credential
     */
    FailureReason[FailureReason["CredentialPassword"] = 5] = "CredentialPassword";
    /**
     * Type mismatch
     */
    FailureReason[FailureReason["EntityType"] = 6] = "EntityType";
    /**
     * Node already exists in Orion
     */
    FailureReason[FailureReason["NodeExists"] = 7] = "NodeExists";
})(FailureReason = exports.FailureReason || (exports.FailureReason = {}));


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Message level
 *
 * @enum
 */
var MessageLevel;
(function (MessageLevel) {
    /**
     * Info level
     */
    MessageLevel[MessageLevel["Info"] = 0] = "Info";
    /**
     * Warning level
     */
    MessageLevel[MessageLevel["Warning"] = 1] = "Warning";
    /**
     * Error level
     */
    MessageLevel[MessageLevel["Error"] = 2] = "Error";
})(MessageLevel = exports.MessageLevel || (exports.MessageLevel = {}));


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Extended visual representation of polling method
 *
 * @interface
 */
var ExtendedPollingMethodTile = /** @class */ (function () {
    function ExtendedPollingMethodTile() {
    }
    return ExtendedPollingMethodTile;
}());
exports.ExtendedPollingMethodTile = ExtendedPollingMethodTile;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Node subtype
 *
 * @enum
 */
var NodeSubType;
(function (NodeSubType) {
    /**
     * None
     */
    NodeSubType[NodeSubType["None"] = 0] = "None";
    /**
     * SNMP
     */
    NodeSubType[NodeSubType["SNMP"] = 1] = "SNMP";
    /**
    * ICMP
    */
    NodeSubType[NodeSubType["ICMP"] = 2] = "ICMP";
    /**
     * WMI
     */
    NodeSubType[NodeSubType["WMI"] = 3] = "WMI";
    /**
    * Agent
    */
    NodeSubType[NodeSubType["Agent"] = 4] = "Agent";
})(NodeSubType = exports.NodeSubType || (exports.NodeSubType = {}));


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(42);
var dateTime_filter_1 = __webpack_require__(43);
var numberFormat_filter_1 = __webpack_require__(44);
exports.default = function (module) {
    module.filter(index_1.FilterNames.dateTimeFilter, dateTime_filter_1.dateTimeFilterFactory);
    module.filter(index_1.FilterNames.numberFormatFilter, numberFormat_filter_1.numberFormatFilterFactory);
};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var filterRegistrationSuffix = "Filter";
var FilterNames = /** @class */ (function () {
    function FilterNames() {
    }
    FilterNames.dateTimeFilter = "addnodeDateFormat";
    FilterNames.numberFormatFilter = "addnodeNumberFormat";
    return FilterNames;
}());
exports.FilterNames = FilterNames;
;
var FilterFullNames = /** @class */ (function () {
    function FilterFullNames() {
    }
    FilterFullNames.dateTimeFilter = FilterNames.dateTimeFilter + filterRegistrationSuffix;
    FilterFullNames.numberFormatFilter = FilterNames.numberFormatFilter + filterRegistrationSuffix;
    return FilterFullNames;
}());
exports.FilterFullNames = FilterFullNames;
;


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
exports.dateTimeFilterFactory = function (dateTimeService, _t) {
    return function (valueUtc, format) {
        if (!valueUtc || !valueUtc.isValid()) {
            return _t("N/A");
        }
        // move to Orion timezone
        var valueOrion = dateTimeService.moveMomentToOrionTimezone(valueUtc);
        // check if today
        var orionToday = dateTimeService.getOrionNowMoment().startOf("day");
        var valueOrionDate = moment(valueOrion).startOf("day");
        if (valueOrionDate.toISOString() === orionToday.toISOString()) {
            return _t("Today");
        }
        return dateTimeService.formatMoment(valueOrion, format);
    };
};
exports.dateTimeFilterFactory.$inject = [index_1.ApolloServiceNames.dateTimeService, index_1.ApolloServiceNames.getTextService];


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Number format filter factory implemenatation
 *
 * @function
 */
exports.numberFormatFilterFactory = function () {
    return function (input, precision, suffix, singularSuffix, plusSign) {
        precision = angular.isNumber(precision) ? precision : 2;
        suffix = suffix || "%";
        var prefix = plusSign && input > 0 ? "+" : "";
        if (isNaN(input)) {
            return "---";
        }
        if (input === 1 && singularSuffix) {
            suffix = singularSuffix;
        }
        return prefix + Math.round(input * Math.pow(10, precision)) / Math.pow(10, precision) + " " + suffix;
    };
};


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(3);
var routingRegistration_provider_1 = __webpack_require__(46);
var replaceProviderKeyword = function (name) {
    return name.replace("Provider", "");
};
exports.default = function (module) {
    module.provider(replaceProviderKeyword(index_1.ProviderNames.routingRegistrationProvider), routingRegistration_provider_1.RoutingRegistrationProvider);
};


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(4);
var index_2 = __webpack_require__(3);
var index_3 = __webpack_require__(0);
var di_1 = __webpack_require__(1);
var routeUrls_1 = __webpack_require__(15);
var routeNames_1 = __webpack_require__(14);
var addNodeWizard_controller_1 = __webpack_require__(47);
var errorStateParams = {
    errorType: "auth",
    errorCode: "403",
    message: "Access Denied",
    location: null
};
/**
 * Function used as a resolve dependency to validate user permissions through "authProfile" route property
 *
 * @function
 */
var isAuthorized = function (addNodeWizardService, $q, $stateParams) {
    return addNodeWizardService.isAuthorized().then(function (result) {
        if (result.authorized) {
            return $q.resolve();
        }
        else {
            return $q.reject({ accessDenied: true });
        }
    });
};
isAuthorized.$inject = [index_3.ServiceNames.addNodeWizardService, index_3.NgServiceNames.ngQService, index_3.NgServiceNames.ngStateParams];
var isLicensedName = "isLicensed";
/**
 * Provider for routing-related registration
 *
 * @class
 */
var RoutingRegistrationProvider = /** @class */ (function () {
    function RoutingRegistrationProvider($stateProvider, $urlRouterProvider) {
        var _this = this;
        this.$stateProvider = $stateProvider;
        this.$urlRouterProvider = $urlRouterProvider;
        this.registerRoutes = function () {
            _this.registerAddNodeWizardRoute();
        };
        this.registerAddNodeWizardRoute = function () {
            _this.registerInternalRoute(routeNames_1.RouteNames.addNodeWizardStateName, routeUrls_1.RouteUrls.addNodeWizardUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Add Node Wizard)",
                template: __webpack_require__(48),
                controller: addNodeWizard_controller_1.AddNodeWizardController
            });
        };
        this.registerInternalRoute = function (stateName, stateUrl, authProfile, configuration, checkLicense) {
            if (checkLicense === void 0) { checkLicense = true; }
            var routerState = {
                i18Title: configuration.i18title,
                url: stateUrl,
                template: configuration.template,
                params: angular.extend({}, errorStateParams, { location: stateUrl, authProfile: authProfile }),
                authProfile: authProfile,
                resolve: {
                    isAuthorized: isAuthorized
                }
            };
            if (configuration.controller) {
                routerState.controller = configuration.controller;
                routerState.controllerAs = "vm";
            }
            _this.$stateProvider.state(stateName, routerState);
        };
        this.initGet = function () {
            _this.$get = [function () {
                    return {};
                }];
        };
        this.initGet();
    }
    RoutingRegistrationProvider = __decorate([
        __param(0, di_1.Inject(index_2.ProviderNames.ngStateProvider)),
        __param(1, di_1.Inject(index_2.ProviderNames.ngUrlRouterProvider)),
        __metadata("design:paramtypes", [Object, Object])
    ], RoutingRegistrationProvider);
    return RoutingRegistrationProvider;
}());
exports.RoutingRegistrationProvider = RoutingRegistrationProvider;
;


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(2);
/**
 * Controller for wizard view
 */
var AddNodeWizardController = /** @class */ (function () {
    function AddNodeWizardController($q, $stateParams, routingService) {
        var _this = this;
        this.$q = $q;
        this.$stateParams = $stateParams;
        this.routingService = routingService;
        this.init = function () {
            _this.definition = new index_2.AddNodeDefinition();
            return _this.$q.resolve();
        };
    }
    AddNodeWizardController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.NgServiceNames.ngStateParams)),
        __param(2, di_1.Inject(index_1.ServiceNames.routingService)),
        __metadata("design:paramtypes", [Function, Object, Object])
    ], AddNodeWizardController);
    return AddNodeWizardController;
}());
exports.AddNodeWizardController = AddNodeWizardController;
;


/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=vm.init()> <xui-page-content page-title=\"_t(Add a Virtual Object for Monitoring)\" page-layout=form class=xui-page-content--no-padding _ta> <add-node-add-node-wizard add-node-definition=vm.definition> </add-node-add-node-wizard> </xui-page-content> </div> ";

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


handleAuthorizationErrors.$inject = ["$rootScope", "$state"];
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(50);
var wizardStepToggle_1 = __webpack_require__(81);
/** @ngInject */
function handleAuthorizationErrors($rootScope, $state) {
    $rootScope.$on("$stateChangeError", function (e, toState, toParams, fromState, fromParams, error) {
        if (error && error.detail && error.detail.accessDenied === true) {
            $state.go("Error", {
                errorType: "auth",
                errorCode: "403",
                message: "Access Denied",
                location: null
            });
            e.preventDefault();
        }
    });
}
;
exports.default = function (module) {
    index_1.default(module);
    wizardStepToggle_1.default(module);
    module.app().run(handleAuthorizationErrors);
};


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var addNodeWizard_directive_1 = __webpack_require__(51);
var index_1 = __webpack_require__(54);
exports.default = function (module) {
    module.component("addNodeAddNodeWizard", addNodeWizard_directive_1.AddNodeWizardDirective);
    index_1.default(module);
};


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(7);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var addNodeWizard_controller_1 = __webpack_require__(52);
/**
 * Directive wrapper for add node wizard
 */
var AddNodeWizardDirective = /** @class */ (function () {
    function AddNodeWizardDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(53);
        this.controller = addNodeWizard_controller_1.AddNodeWizardController;
        this.controllerAs = "vm";
        this.bindToController = {
            addNodeDefinition: "<"
        };
        this.scope = {};
    }
    return AddNodeWizardDirective;
}());
exports.AddNodeWizardDirective = AddNodeWizardDirective;
;


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Controller for whole wizard directive
 */
var AddNodeWizardController = /** @class */ (function () {
    function AddNodeWizardController($q, $scope, dialogService, _t, addNodeWizardService, routingService, swDemoService) {
        var _this = this;
        this.$q = $q;
        this.$scope = $scope;
        this.dialogService = dialogService;
        this._t = _t;
        this.addNodeWizardService = addNodeWizardService;
        this.routingService = routingService;
        this.swDemoService = swDemoService;
        this.$onInit = function () {
            _this.initProperties();
            _this.resetRemoteControl();
            _this.steps = _this.createSteps();
        };
        this.createSteps = function () {
            var steps = [_this.objectTypeStep, _this.pollingStep, _this.metricsStep];
            return steps;
        };
        this.onFinish = function () {
            _this.finishAndAddAnotherButton.isDisabled = true;
            return _this.validatePage().then(function (valid) {
                if (!valid) {
                    return false;
                }
                return _this.onFinishInternal(_this.routingService.goToVirtualizationSummary, false);
            })
                .finally(function () {
                _this.finishAndAddAnotherButton.isDisabled = false;
            });
        };
        this.validatePage = function () {
            if (!angular.isFunction(_this.currentStepRemoteControl.isValid)) {
                return _this.$q.resolve(true);
            }
            return _this.currentStepRemoteControl.isValid();
        };
        this.onFinishInternal = function (onFinishRedirect, handleIsBusy) {
            if (handleIsBusy) {
                _this.isBusy = true;
            }
            return _this.validatePage().then(function (valid) {
                if (!valid) {
                    _this.isBusy = false;
                    return false;
                }
                return _this.addNodeWizardService.addNode(_this.addNodeDefinition)
                    .then(function (response) {
                    if (response.success) {
                        _this.dialogService.showMessage({ title: _this._t("Node Added"),
                            message: _this._t("Node was added successfully.") }).then(function () {
                            onFinishRedirect();
                        });
                    }
                    else {
                        _this.showErrorDialog();
                    }
                    return response.success;
                })
                    .catch(function () {
                    if (!_this.swDemoService.isDemoMode()) {
                        _this.showErrorDialog();
                    }
                    return false;
                })
                    .finally(function () {
                    if (handleIsBusy) {
                        _this.isBusy = false;
                    }
                });
            });
        };
        this.showErrorDialog = function () {
            _this.dialogService.showError({ title: _this._t("Node Not Added"),
                message: _this._t("Node was not added.") });
        };
        this.onCancel = function () {
            _this.routingService.goToVirtualizationSummary();
            return true;
        };
        this.onEnterStep = function (step) {
            if (step.label === _.last(_this.steps).label) {
                _this.additionalButtons.splice(0, _this.additionalButtons.length, _this.finishAndAddAnotherButton);
            }
            else {
                _this.additionalButtons.splice(0, _this.additionalButtons.length);
            }
        };
        this.onExitStep = function () {
            _this.resetRemoteControl();
        };
        this.onNextStep = function (from, to, cancellation) {
            var fromIndex = _.findIndex(_this.steps, { label: from.label });
            var toIndex = _.findIndex(_this.steps, { label: to.label });
            if (fromIndex < toIndex) {
                return _this.validatePage().then(function (result) {
                    if (result && _.isFunction(_this.currentStepRemoteControl.onNextStep)) {
                        return _this.currentStepRemoteControl.onNextStep(_this.addNodeDefinition);
                    }
                    return result;
                });
            }
            else {
                return _this.$q.resolve(true);
            }
        };
        this.resetRemoteControl = function () {
            _this.currentStepRemoteControl = {
                isValid: undefined,
                onNextStep: undefined,
                onPreviousStep: undefined
            };
        };
    }
    AddNodeWizardController.prototype.initProperties = function () {
        var _this = this;
        this.isBusy = false;
        this.steps = [];
        this.additionalButtons = [];
        this.currentStepIndex = 0;
        this.objectTypeStep = {
            label: "objectTypeStep",
            shortTitle: this._t("Type of Object"),
            templateUrl: "object-type-step"
        };
        this.pollingStep = {
            label: "pollingStep",
            shortTitle: this._t("Polling & Creds"),
            templateUrl: "polling-step"
        };
        this.pollingStep = {
            label: "pollingStep",
            shortTitle: this._t("Polling & Creds"),
            templateUrl: "polling-step"
        };
        this.metricsStep = {
            label: "metricsStep",
            shortTitle: this._t("Metrics"),
            templateUrl: "metrics-step"
        };
        this.finishAndAddAnotherButton = {
            name: "addAnother",
            text: this._t("Finish and Add Another"),
            action: function () {
                return _this.onFinishInternal(_this.routingService.goToAddNodeWizard, true);
            }
        };
    };
    ;
    AddNodeWizardController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.NgServiceNames.ngScope)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.dialogService)),
        __param(3, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(4, di_1.Inject(index_1.ServiceNames.addNodeWizardService)),
        __param(5, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(6, di_1.Inject(index_1.ApolloServiceNames.swDemoService)),
        __metadata("design:paramtypes", [Function, Object, Object, Function, Object, Object, Object])
    ], AddNodeWizardController);
    return AddNodeWizardController;
}());
exports.AddNodeWizardController = AddNodeWizardController;


/***/ }),
/* 53 */
/***/ (function(module, exports) {

module.exports = "<div class=add-node-wizard> <div xui-busy=vm.isBusy> <xui-wizard name=reportWizard ng-model=vm current-step-index=vm.currentStepIndex finish-text=_t(Finish) on-next-step=\"vm.onNextStep(from, to, cancellation)\" on-finish=vm.onFinish() on-cancel=vm.onCancel() on-enter-step=vm.onEnterStep(step) on-exit-step=vm.onExitStep() additional-buttons=vm.additionalButtons _ta> <xui-wizard-step ng-repeat=\"step in vm.steps track by step.label\" label={{::step.label}} short-title={{::step.shortTitle}} class=add-node-wizard-step> <ng-include src=::step.templateUrl></ng-include> </xui-wizard-step> <add-node-wizard-connector> </add-node-wizard-connector> </xui-wizard> <script type=text/ng-template id=object-type-step> <add-node-wizard-step-toggle>\n                <add-node-wizard-object-type-step\n                    add-node-definition=\"vm.addNodeDefinition\"\n                    remote-control=\"vm.currentStepRemoteControl\">\n                </add-node-wizard-object-type-step>\n            </add-node-wizard-step-toggle> </script> <script type=text/ng-template id=polling-step> <add-node-wizard-step-toggle>\n                <add-node-wizard-polling-step\n                    polling-definition=\"vm.addNodeDefinition.pollingDefinition\"\n                    plugin-type=\"vm.addNodeDefinition.pluginType\"\n                    credentials=\"vm.addNodeDefinition.credentials\"\n                    remote-control=\"vm.currentStepRemoteControl\">\n                </add-node-wizard-polling-step>\n            </add-node-wizard-step-toggle> </script> <script type=text/ng-template id=metrics-step> <add-node-wizard-step-toggle>\n                <add-node-wizard-metrics-step \n                    plugin-type=\"vm.addNodeDefinition.pluginType\"\n                    discovery-job-id=\"vm.addNodeDefinition.discoveryJobId\">\n                </add-node-wizard-metrics-step>\n            </add-node-wizard-step-toggle> </script> </div> </div> ";

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(55);
var index_2 = __webpack_require__(59);
var index_3 = __webpack_require__(77);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
};


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var objectTypeStep_directive_1 = __webpack_require__(56);
exports.default = function (module) {
    module.component("addNodeWizardObjectTypeStep", objectTypeStep_directive_1.ObjectTypeStepDirective);
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(9);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var objectTypeStep_controller_1 = __webpack_require__(57);
/**
 * Directive wrapper for add node wizard / object type step
 */
var ObjectTypeStepDirective = /** @class */ (function () {
    function ObjectTypeStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(58);
        this.controller = objectTypeStep_controller_1.ObjectTypeStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            addNodeDefinition: "<",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return ObjectTypeStepDirective;
}());
exports.ObjectTypeStepDirective = ObjectTypeStepDirective;
;


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(2);
var di_1 = __webpack_require__(1);
var index_2 = __webpack_require__(0);
/**
 * Controller for object type step
 */
var ObjectTypeStepController = /** @class */ (function () {
    function ObjectTypeStepController(xuiToastService, $q, $scope, addNodeWizardService) {
        var _this = this;
        this.xuiToastService = xuiToastService;
        this.$q = $q;
        this.$scope = $scope;
        this.addNodeWizardService = addNodeWizardService;
        this.$onInit = function () {
            _this.selectedObjectType = [];
            _this.remoteControl.isValid = _this.isValid;
            return _this.addNodeWizardService.getPluginTiles().then(function (result) {
                _this.objectTilesByCategories = _this.orderTilesInCategories(_this.groupTilesByCategoryId(_this.wrapTiles(result.pollingTypes)));
                _this.categoriesMetadata = result.categoriesMetadata;
                _this.setupSelectionWatch();
                _this.setDefaultSelection();
            });
        };
        this.isValid = function () {
            if (_.isEmpty(_this.selectedObjectType)) {
                return _this.$q.resolve(false);
            }
            var selectedCategory = _.findKey(_this.objectTilesByCategories, function (tiles) { return _.some(tiles, function (t) { return t.pluginType === _this.selectedObjectType[0]; }); });
            var selectedCategoryMetadata = _this.categoriesMetadata[selectedCategory];
            if (!!selectedCategoryMetadata.message && selectedCategoryMetadata.message.messageLevel === index_1.MessageLevel.Error) {
                _this.xuiToastService.error(selectedCategoryMetadata.message.highlightedText);
                return _this.$q.resolve(false);
            }
            return _this.$q.resolve(true);
        };
        this.getMessageLevel = function (level) {
            return index_1.MessageLevel[level].toLowerCase();
        };
        this.wrapTiles = function (objectTypes) {
            return _.map(objectTypes, function (value, key) {
                var extendedTile = new index_1.ExtendedPollingMethodTile();
                extendedTile.categoryId = value.categoryId;
                extendedTile.description = value.description;
                extendedTile.label = value.label;
                extendedTile.order = value.order;
                extendedTile.thumbnail = value.thumbnail;
                extendedTile.pluginType = key;
                return extendedTile;
            });
        };
        this.groupTilesByCategoryId = function (tiles) {
            return _.groupBy(tiles, function (t) { return t.categoryId; });
        };
        this.orderTilesInCategories = function (tiles) {
            return _.mapValues(tiles, function (t) { return _.sortBy(t, function (i) { return i.order; }); });
        };
        this.setDefaultSelection = function () {
            _this.selectedObjectType = [];
            if (_this.addNodeDefinition.pluginType) {
                _this.selectedObjectType.push(_this.addNodeDefinition.pluginType);
                return;
            }
            if (!_this.objectTilesByCategories || _.keys(_this.objectTilesByCategories).length < 1) {
                return;
            }
            var firstItem = _this.objectTilesByCategories[_.keys(_this.objectTilesByCategories)[0]][0];
            _this.selectedObjectType.push(firstItem.pluginType);
        };
        this.setupSelectionWatch = function () {
            _this.$scope.$watchCollection(function () { return _this.selectedObjectType; }, function (newSelected) {
                if (_.isEmpty(newSelected)) {
                    return;
                }
                if (newSelected[0] && newSelected[0] !== _this.addNodeDefinition.pluginType) {
                    _this.addNodeDefinition.credentials = [];
                    _this.addNodeDefinition.pollingDefinition.pollingMethod = new index_1.PollingMethod();
                }
                _this.addNodeDefinition.pluginType = newSelected[0];
            });
        };
    }
    ObjectTypeStepController = __decorate([
        __param(0, di_1.Inject(index_2.ApolloServiceNames.toastService)),
        __param(1, di_1.Inject(index_2.NgServiceNames.ngQService)),
        __param(2, di_1.Inject(index_2.NgServiceNames.ngScope)),
        __param(3, di_1.Inject(index_2.ServiceNames.addNodeWizardService)),
        __metadata("design:paramtypes", [Object, Function, Object, Object])
    ], ObjectTypeStepController);
    return ObjectTypeStepController;
}());
exports.ObjectTypeStepController = ObjectTypeStepController;


/***/ }),
/* 58 */
/***/ (function(module, exports) {

module.exports = "<div class=add-node-wizard-object-type-step> <div class=object-type-category ng-repeat=\"(key, categoryItems) in vm.objectTilesByCategories track by key\" ng-init=\"categoryMetadata = vm.categoriesMetadata[key]\" data-selector=pluginCategory:{{::key}}> <h2>{{::categoryMetadata.categoryTitle}}</h2> <xui-message type={{::vm.getMessageLevel(categoryMetadata.message.messageLevel)}} ng-if=::categoryMetadata.message allow-dismiss=true data-selector=categoryMessage:{{::key}}> <span ng-bind-html=::categoryMetadata.message.highlightedText></span> </xui-message> <xui-listview stripe=false items-source=::categoryItems template-url=object-tile-template selection=vm.selectedObjectType track-by=label selection-property=pluginType selection-mode=single> </xui-listview> </div> <script type=text/ng-template id=object-tile-template> <div class=\"flex-row-container\" data-selector=\"pluginType:{{::item.pluginType}}\">\n            <div class=\"flex-row-item\">\n                <xui-icon icon=\"{{::item.thumbnail}}\"></xui-icon>\n            </div>\n            <div class=\"flex-row-item flex-column-container\">\n                <h3 class=\"name-item\">{{::item.label}}</h3>\n                <div class=\"description-item xui-text-r\">{{::item.description}}</div>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(60);
var index_2 = __webpack_require__(64);
var pollingStep_directive_1 = __webpack_require__(68);
var index_3 = __webpack_require__(71);
var serverAddressValidator_1 = __webpack_require__(75);
exports.default = function (module) {
    module.component("addNodeWizardPollingStep", pollingStep_directive_1.PollingStepDirective);
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    serverAddressValidator_1.default(module);
};


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var credentialSelector_directive_1 = __webpack_require__(61);
exports.default = function (module) {
    module.component("addNodeWizardCredentialSelector", credentialSelector_directive_1.CredentialSelectorDirective);
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(10);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var credentialSelector_controller_1 = __webpack_require__(62);
/**
 * Directive which displays credentials selector
 */
var CredentialSelectorDirective = /** @class */ (function () {
    function CredentialSelectorDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(63);
        this.controller = credentialSelector_controller_1.CredentialSelectorController;
        this.controllerAs = "vm";
        this.bindToController = {
            pollingMethodType: "<",
            credentials: "=",
            pluginType: "<",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return CredentialSelectorDirective;
}());
exports.CredentialSelectorDirective = CredentialSelectorDirective;
;


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var constants_1 = __webpack_require__(18);
var index_1 = __webpack_require__(0);
var CredentialSelectorController = /** @class */ (function () {
    function CredentialSelectorController(addNodeWizardService, xuiToastService, _t, $q) {
        var _this = this;
        this.addNodeWizardService = addNodeWizardService;
        this.xuiToastService = xuiToastService;
        this._t = _t;
        this.$q = $q;
        this.$onInit = function () {
            _this.remoteControl.isValid = _this.isValid;
        };
        this.isNewCredentialSelected = function () {
            return _this.credential && !_.isNumber(_this.credential.id);
        };
        this.isValid = function () {
            if (!_this.isNewCredentialSelected()) {
                return _this.$q.resolve(true);
            }
            _this.credentialForm.confirmPassword.$setDirty();
            _this.credentialForm.password.$setDirty();
            _this.credentialForm.name.$setDirty();
            _this.credentialForm.username.$setDirty();
            return _this.$q.resolve(_this.credentialForm.$valid);
        };
        this.$onChanges = function (changes) {
            if (changes.pollingMethodType && _this.pollingMethodType) {
                _this.loadCredentials();
            }
        };
        this.validateLength = function (value) {
            if (!value) {
                return value;
            }
            var maxLength = 75;
            var returnValue = value;
            if (value.length > maxLength) {
                _this.xuiToastService.warning(_this._t("The text entered exceeds the maximum length."));
                returnValue = returnValue.substr(0, maxLength);
            }
            return returnValue;
        };
        this.initProperties = function () {
            if (!_.isEmpty(_this.credentials)) {
                _this.credential = _this.credentials[0];
            }
        };
        this.updatePasswordCompareValidity = function (filledProperty) {
            var isValid = true;
            if (filledProperty
                && _this.password !== _this.confirmPassword) {
                isValid = false;
            }
            _this.credentialForm.$setValidity("password-compare", isValid, undefined);
        };
        this.createAddNewCredentialOption = function () {
            if (!_this.loadedCredentials) {
                return;
            }
            var newCredentialOption = _.find(_this.loadedCredentials, function (cr) { return !_.isNumber(cr.id); });
            if (!newCredentialOption) {
                var newCredentialInfo = {
                    id: undefined,
                    name: _this._t("<New Credential>"),
                    properties: {}
                };
                _this.loadedCredentials = [newCredentialInfo].concat(_this.loadedCredentials);
                if (_this.credentials.length === 0) {
                    _this.credential = newCredentialInfo;
                }
            }
            else if (!_this.credentials) {
                _this.credential = newCredentialOption;
            }
        };
        this.loadCredentials = function () {
            return _this.addNodeWizardService.getCredentials({ pluginType: _this.pluginType, pollingMethodType: _this.pollingMethodType })
                .then(function (credentialResult) {
                _this.loadedCredentials = credentialResult.credentials;
                _this.createAddNewCredentialOption();
                _this.initProperties();
            });
        };
    }
    Object.defineProperty(CredentialSelectorController.prototype, "credential", {
        get: function () {
            return _.isEmpty(this.credentials) ? undefined : this.credentials[0];
        },
        set: function (value) {
            this.credentials = [value];
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(CredentialSelectorController.prototype, "credentialName", {
        get: function () {
            return this.credential.properties[constants_1.Constants.credentialName];
        },
        set: function (value) {
            var validatedValue = this.validateLength(value);
            this.credential.properties[constants_1.Constants.credentialName] = validatedValue;
            this.credential.properties[constants_1.Constants.credentialDescription] = validatedValue;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(CredentialSelectorController.prototype, "username", {
        get: function () {
            return this.credential.properties[constants_1.Constants.credentialUsername];
        },
        set: function (value) {
            var validatedValue = this.validateLength(value);
            this.credential.properties[constants_1.Constants.credentialUsername] = validatedValue;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(CredentialSelectorController.prototype, "password", {
        get: function () {
            return this.credential.properties[constants_1.Constants.credentialPassword];
        },
        set: function (value) {
            var validatedValue = this.validateLength(value);
            this.credential.properties[constants_1.Constants.credentialPassword] = validatedValue;
            this.updatePasswordCompareValidity(this.confirmPassword);
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(CredentialSelectorController.prototype, "confirmPassword", {
        get: function () {
            return this.credential.properties[constants_1.Constants.credentialConfirmPassword];
        },
        set: function (value) {
            var validatedValue = this.validateLength(value);
            this.credential.properties[constants_1.Constants.credentialConfirmPassword] = validatedValue;
            this.updatePasswordCompareValidity(this.password);
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    CredentialSelectorController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.addNodeWizardService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.toastService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(3, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __metadata("design:paramtypes", [Object, Object, Function, Function])
    ], CredentialSelectorController);
    return CredentialSelectorController;
}());
exports.CredentialSelectorController = CredentialSelectorController;


/***/ }),
/* 63 */
/***/ (function(module, exports) {

module.exports = "<div class=add-node-wizard-credential-selector> <div class=row> <div class=col-xs-4> <div class=xui-dropdown--justified> <xui-dropdown ng-if=vm.loadedCredentials caption=\"_t(Select Credential)\" items-source=vm.loadedCredentials selected-item=vm.credential display-value=name is-required=true _ta> </xui-dropdown> </div> </div> </div> <div ng-form=vm.credentialForm> <div ng-if=vm.isNewCredentialSelected()> <div class=row data-selector=credential-name> <div class=col-xs-4> <xui-textbox ng-if=vm.isNewCredentialSelected() name=name caption=\"_t(Credential Name)\" ng-model=vm.credentialName validators=required _ta> </xui-textbox> </div> </div> <div class=row data-selector=credential-username> <div class=col-xs-4> <xui-textbox ng-if=vm.isNewCredentialSelected() name=username caption=\"_t(User Name)\" ng-model=vm.username validators=required _ta> </xui-textbox> </div> </div> <div class=row data-selector=credential-password> <div class=col-xs-4> <xui-textbox ng-if=vm.isNewCredentialSelected() name=password caption=_t(Password) ng-model=vm.password type=password validators=required _ta> </xui-textbox> </div> </div> <div class=row data-selector=credential-confirm-password> <div class=col-xs-4> <xui-textbox ng-if=vm.isNewCredentialSelected() name=confirmPassword caption=\"_t(Confirm Password)\" ng-model=vm.confirmPassword type=password validators=required _ta> </xui-textbox> <div class=\"xui-validation has-error validation-message\"> <div ng-messages=vm.credentialForm.$error> <div ng-message=password-compare data-selector=validation-message _t>Password does not match the confirm password.</div> </div> </div> </div> </div> </div> </div> </div> ";

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pollingMethod_directive_1 = __webpack_require__(65);
exports.default = function (module) {
    module.component("addNodeWizardPollingMethod", pollingMethod_directive_1.PollingMethodDirective);
};


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pollingMethod_controller_1 = __webpack_require__(66);
/**
 * Directive for picking of the polling method
 */
var PollingMethodDirective = /** @class */ (function () {
    function PollingMethodDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(67);
        this.controller = pollingMethod_controller_1.PollingMethodController;
        this.controllerAs = "vm";
        this.bindToController = {
            pollingMethods: "<",
            pollingDefinition: "<",
            selectedCredentials: "="
        };
        this.scope = {};
    }
    return PollingMethodDirective;
}());
exports.PollingMethodDirective = PollingMethodDirective;
;


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Controller for polling method directive
 */
var PollingMethodController = /** @class */ (function () {
    function PollingMethodController(_t, swUtil) {
        var _this = this;
        this._t = _t;
        this.swUtil = swUtil;
        this.$onChanges = function (changes) {
            if (!changes.pollingMethods || !changes.pollingMethods.currentValue) {
                return;
            }
            _this.init();
        };
        this.init = function () {
            if (_this.pollingDefinition.pollingMethod) {
                var selectedMethod = _.find(_this.pollingMethods, function (pm) { return pm.label === _this.pollingDefinition.pollingMethod.label; });
                if (selectedMethod) {
                    _this.setSelectedMethod(_this.pollingDefinition.pollingMethod);
                    _this._selectedMethod = selectedMethod;
                    return;
                }
            }
            _this.selectedMethod = _this.pollingMethods[0];
        };
    }
    Object.defineProperty(PollingMethodController.prototype, "selectedMethod", {
        get: function () {
            return this._selectedMethod;
        },
        set: function (value) {
            this.setSelectedMethod(value);
            this.selectedCredentials = [];
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    PollingMethodController.prototype.setSelectedMethod = function (value) {
        this._selectedMethod = value;
        this.pollingDefinition.pollingMethod = __assign({}, value);
        if (value) {
            this.toolTipText = this.swUtil.formatString(this._t("This device will be polled using the {0} method."), value.label);
        }
        else {
            this.toolTipText = undefined;
        }
    };
    ;
    PollingMethodController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.swUtilService)),
        __metadata("design:paramtypes", [Function, Object])
    ], PollingMethodController);
    return PollingMethodController;
}());
exports.PollingMethodController = PollingMethodController;
;


/***/ }),
/* 67 */
/***/ (function(module, exports) {

module.exports = "<div class=add-node-wizard-polling-method> <h2><span _t>Polling method</span> <xui-icon icon=help tool-tip={{vm.toolTipText}} data-selector=polling-method-tooltip></xui-icon></h2> <div ng-if=vm.pollingMethods> <ng-form> <xui-radio ng-repeat=\"method in vm.pollingMethods track by method.type\" ng-model=vm.selectedMethod ng-value=method data-selector={{::method.type}}> {{::method.label}} </xui-radio> </ng-form> </div> </div> ";

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(12);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pollingStep_controller_1 = __webpack_require__(69);
/**
 * Directive wrapper for add node wizard / polling step
 */
var PollingStepDirective = /** @class */ (function () {
    function PollingStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(70);
        this.controller = pollingStep_controller_1.PollingStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            remoteControl: "<",
            pollingDefinition: "=",
            credentials: "=",
            pluginType: "="
        };
        this.scope = {};
    }
    return PollingStepDirective;
}());
exports.PollingStepDirective = PollingStepDirective;
;


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(2);
var constants_1 = __webpack_require__(18);
/**
 * Controller for polling step
 */
var PollingStepController = /** @class */ (function () {
    function PollingStepController(addNodeWizardService, routingService, $dialogService, _t, swUtil, helpLinkService, $q, $scope, $stateParams, $interval, $timeout) {
        var _this = this;
        this.addNodeWizardService = addNodeWizardService;
        this.routingService = routingService;
        this.$dialogService = $dialogService;
        this._t = _t;
        this.swUtil = swUtil;
        this.helpLinkService = helpLinkService;
        this.$q = $q;
        this.$scope = $scope;
        this.$stateParams = $stateParams;
        this.$interval = $interval;
        this.$timeout = $timeout;
        this.$onInit = function () {
            _this.resetRemoteControls();
            _this.remoteControl.isValid = _this.isValid;
            _this.remoteControl.onNextStep = _this.onNextStep;
            if (_.isEmpty(_this.pollingDefinition.serverIpAddress) && _.isEmpty(_this.pollingDefinition.serverHostname) && _this.$stateParams["serverAddress"]) {
                _this.serverAddress = _this.$stateParams["serverAddress"];
            }
            _this.createWatchers();
            _this.isBusy = false;
            _this.progressPercentage = 0;
            _this.progressMessageTitle = "";
            _this.progressMessageDescription = "";
            return _this.reloadPollingInfo();
        };
        this.initServerAddressValidator = function (model) {
            _this.serverAddressNgModel = model;
            model.$asyncValidators["addNodeServerAddress"] = _this.validateServerAddress;
        };
        this.initProperties = function () {
            _this._serverAddress = _this.pollingDefinition.serverHostname ? _this.pollingDefinition.serverHostname : _this.pollingDefinition.serverIpAddress;
        };
        this.reloadCredentials = function () {
            return _this.addNodeWizardService.getCredentials({ pluginType: _this.pluginType, pollingMethodType: _this.pollingDefinition.pollingMethod.type })
                .then(function (credentialResult) {
                _this.credentials = credentialResult.credentials;
            });
        };
        this.validateServerAddress = function (modelValue) {
            _this.resolvedIpAddress = undefined;
            _this.isIpResolvedFromHostname = false;
            if (_.isEmpty(modelValue)) {
                return _this.$q.resolve();
            }
            return _this.addNodeWizardService.verifyServerAddress(modelValue)
                .then(function (result) {
                // we always resolve IP address
                if (!_.isEmpty(result.resolvedIp)) {
                    _this.isIpResolvedFromHostname = !!result.isIpResolvedFromHostname;
                    _this.resolvedIpAddress = result.resolvedIp;
                    return;
                }
                return _this.$q.reject(result.message);
            });
        };
        this.createWatchers = function () {
            //If polling method changes, credential validation message won't be visible
            _this.$scope.$watchCollection(function () { return _this.credentials; }, function () {
                _this.isCredentialMessageVisible = false;
                var isNewCredentialOptionSelected = _this.credentials
                    && _this.credentials[0]
                    && !_.isNumber(_this.credentials[0].id);
                if (!_this.addNewCredentialListener && isNewCredentialOptionSelected) {
                    _this.addNewCredentialListener =
                        _this.$scope.$watch(function () {
                            if (_this.credentials[0]) {
                                return (_this.credentials[0].properties || {})[constants_1.Constants.credentialName];
                            }
                        }, function () {
                            _this.isCredentialMessageVisible = false;
                        });
                }
                else {
                    if (_.isFunction(_this.addNewCredentialListener)) {
                        _this.addNewCredentialListener();
                        _this.addNewCredentialListener = undefined;
                    }
                }
            });
        };
        this.isValid = function () {
            _this.serverAddressNgModel.$setDirty();
            return _this.waitForServerAddressValidation()
                .then(function () {
                if (_this.serverAddressNgModel.$invalid) {
                    return false;
                }
                _this.serverHostname = _this.serverAddress;
                _this.serverIpAddress = _this.resolvedIpAddress;
                var credentialWait;
                if (_.isFunction(_this.credentialSelectorRemoteControl.isValid)) {
                    credentialWait = _this.credentialSelectorRemoteControl.isValid();
                }
                else {
                    credentialWait = _this.$q.resolve(true);
                }
                return credentialWait.then(function (credentialValid) {
                    if (!credentialValid) {
                        return false;
                    }
                    if (_this.pollingDefinition.pollingMethod.nodeSubType === index_2.NodeSubType.None) {
                        return _this.verifyCredentials();
                    }
                    return _this.verifyNodeDuplication().then(function (result) {
                        if (!result) {
                            return false;
                        }
                        return _this.verifyCredentials();
                    });
                });
            });
        };
        this.verifyNodeDuplication = function () {
            return _this.addNodeWizardService.verifyNodeDuplication(_this.pollingDefinition.serverIpAddress, _this.pollingDefinition.serverHostname, _this.pollingDefinition.pollingEngineId).then(function (result) {
                if (result.isValid) {
                    return true;
                }
                if (result.failureReason === index_2.FailureReason.NodeExists) {
                    // Message cannot be split in two lines because of i18n typescript parser
                    // tslint:disable-next-line:max-line-length
                    result.message = _this.swUtil.formatString(_this._t("<strong>{0}</strong> is already monitored through polling engine <strong>{1}</strong>.<br /><a href=\"{2}\">View the node's details</a> where you can edit properties and change the polling IP address."), _this.pollingDefinition.serverIpAddress, result.existingNodeEngineName, _this.routingService.getNodeDetailsHref(result.existingNodeId));
                    if (result.messageLevel === index_2.MessageLevel.Error && _this.pollingInfo.engines && _this.pollingInfo.engines.length > 1) {
                        result.message += _this.swUtil.formatString(_this._t("<br />Or, monitor {0} as a separate node by assigning to a different polling engine."), _this.pollingDefinition.serverIpAddress);
                    }
                    // Just display warning popup when adding duplicates is allowed
                    if (result.messageLevel === index_2.MessageLevel.Warning) {
                        return _this.showNodeExistsDialog(result.message);
                    }
                }
                _this.showValidationMessage(result);
            });
        };
        this.verifyCredentials = function () {
            _this.progressMessageTitle = _this._t("Step 1 of 2: Verifying your credentials, please wait...");
            _this.progressMessageDescription = _this.swUtil.formatString(_this._t("Validating credentials via {0}"), _this.pollingDefinition.pollingMethod.label);
            _this.progressPercentage = 0;
            _this.isBusy = true;
            // multiple credentials are verified with discovery job
            if (_this.credentials && _this.credentials.length > 1) {
                _this.setProgressToFinish(true);
                return _this.$q.resolve(true);
            }
            return _this.addNodeWizardService.verifyCredentials(_this.pollingDefinition.serverIpAddress, _this.pollingDefinition.pollingEngineId, _this.pluginType, true, _this.credentials[0].id, _this.credentials[0].properties)
                .then(function (result) {
                if (result.isValid) {
                    if (!_.isNumber(_this.credentials[0].id)) {
                        _this.credentials = [result.credentialInfo];
                    }
                }
                else if (result.credentialInfo && !_.isNumber(_this.credentials[0].id)) {
                    _this.reloadCredentials();
                }
                //if it's second call with the same result of type "Info" and with the same failure reason, makes the result valid
                if (_this.messageLevel === result.messageLevel && _this.messageLevel === index_2.MessageLevel.Info && _this.failureReason === result.failureReason) {
                    result.isValid = true;
                }
                // in case of type mismatch get new type from result properties
                if (result.failureReason === index_2.FailureReason.EntityType &&
                    result.properties &&
                    result.properties.pluginType) {
                    _this.pluginType = result.properties.pluginType;
                    _this.pluginTypeChanged = true;
                }
                _this.showValidationMessage(result);
                if (_this.pluginTypeChanged) {
                    return _this.reloadPollingInfo().then(function () {
                        _this.setProgressToFinish(result.isValid);
                        return result.isValid;
                    });
                }
                else {
                    _this.setProgressToFinish(result.isValid);
                    return result.isValid;
                }
            });
        };
        this.reloadPollingInfo = function () {
            return _this.addNodeWizardService.getPollingInfo(_this.pluginType).then(function (pInfo) {
                _this.pollingInfo = pInfo;
                _this.pluginMessages = _this.pollingInfo.messages;
                _this.initProperties();
            });
        };
        this.waitForServerAddressValidation = function (delay) {
            if (delay === void 0) { delay = 0; }
            return _this.$timeout(function () {
                if (!_this.serverAddressNgModel.$pending) {
                    return;
                }
                return _this.waitForServerAddressValidation(500);
            }, delay);
        };
        this.onNextStep = function (addNodeDefinition) {
            addNodeDefinition.credentials = _this.credentials;
            if (_this.pluginTypeChanged) {
                var reloadedPollingMehod = _.find(_this.pollingInfo.pollingMethods, function (pm) { return pm.type === addNodeDefinition.pollingDefinition.pollingMethod.type; });
                addNodeDefinition.pollingDefinition.pollingMethod = reloadedPollingMehod;
            }
            return _this.addNodeWizardService.createDiscoveryJob(addNodeDefinition)
                .then(function (result) {
                addNodeDefinition.discoveryJobId = result;
                var def = _this.$q.defer();
                _this.waitForDiscoveryResult(addNodeDefinition, def);
                return def.promise.then(function () { return true; }).catch(function () { return false; });
            })
                .catch(function () {
                return false;
            }).finally(function () {
                _this.setProgressToFinish(false);
            });
        };
        this.waitForDiscoveryResult = function (addNodeDefinition, def) {
            _this.progressMessageTitle = _this._t("Step 2 of 2: Polling your environment, please wait...");
            _this.progressMessageDescription = "";
            _this.progressPercentage = 50;
            _this.isBusy = true;
            _this.$timeout(function () {
                _this.addNodeWizardService.getDiscoveryResult(addNodeDefinition.discoveryJobId)
                    .then(function (discoveryJobResult) {
                    if (_.isObject(discoveryJobResult)) {
                        def.resolve();
                    }
                    else {
                        _this.waitForDiscoveryResult(addNodeDefinition, def);
                    }
                }).catch(function () {
                    def.reject();
                });
            }, 3000);
        };
        this.showValidationMessage = function (result) {
            _this.messageLevel = result.messageLevel;
            _this.failureReason = result.failureReason;
            _this.isCredentialMessageVisible = false;
            _this.isGeneralMessageVisible = false;
            _this.isTypeMismatchMessageVisible = false;
            if (result.isValid) {
                return;
            }
            _this.messageText = result.message;
            _this.messageDescriptionText = result.messageMinor;
            switch (result.failureReason) {
                case index_2.FailureReason.General:
                case index_2.FailureReason.NodeExists:
                    _this.isGeneralMessageVisible = true;
                    break;
                case index_2.FailureReason.Credential:
                    _this.isCredentialMessageVisible = true;
                    break;
                case index_2.FailureReason.EntityType:
                    _this.isTypeMismatchMessageVisible = true;
                    break;
            }
            _this.messageType = _this.msgLevelToType(result.messageLevel);
        };
        this.msgLevelToType = function (msgLevel) {
            switch (msgLevel) {
                case index_2.MessageLevel.Info:
                    return "info";
                case index_2.MessageLevel.Warning:
                    return "warning";
                case index_2.MessageLevel.Error:
                    return "error";
                default:
                    return "info";
            }
        };
        this.showNodeExistsDialog = function (message) {
            return _this.$dialogService.showWarning({ title: _this._t("IP Address is already monitored"),
                message: message }, {
                name: "next",
                text: _this._t("Next"),
                action: function () { return true; }
            }).then(function (dialogResult) {
                if (dialogResult === "cancel") {
                    return _this.$q.resolve(false);
                }
                return _this.$q.resolve(true);
            });
        };
        this.resetRemoteControls = function () {
            _this.credentialSelectorRemoteControl = {
                isValid: undefined,
                onNextStep: undefined,
                onPreviousStep: undefined
            };
        };
        this.setProgressToFinish = function (stillBusy) {
            _this.progressPercentage = 100;
            _this.isBusy = stillBusy;
        };
    }
    Object.defineProperty(PollingStepController.prototype, "serverHostname", {
        set: function (hostname) {
            this.pollingDefinition.serverHostname = this.isIpResolvedFromHostname ? hostname : "";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollingStepController.prototype, "serverAddress", {
        get: function () {
            return this._serverAddress;
        },
        set: function (serverAddress) {
            this._serverAddress = serverAddress;
            this.pollingDefinition.serverHostname = serverAddress;
            this.pollingDefinition.serverIpAddress = undefined;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollingStepController.prototype, "serverIpAddress", {
        set: function (ipAddress) {
            this.pollingDefinition.serverIpAddress = ipAddress;
        },
        enumerable: true,
        configurable: true
    });
    PollingStepController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.addNodeWizardService)),
        __param(1, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.dialogService)),
        __param(3, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(4, di_1.Inject(index_1.ApolloServiceNames.swUtilService)),
        __param(5, di_1.Inject(index_1.ApolloServiceNames.helpLinkService)),
        __param(6, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(7, di_1.Inject(index_1.NgServiceNames.ngScope)),
        __param(8, di_1.Inject(index_1.NgServiceNames.ngStateParams)),
        __param(9, di_1.Inject(index_1.NgServiceNames.ngInterval)),
        __param(10, di_1.Inject(index_1.NgServiceNames.ngTimeoutService)),
        __metadata("design:paramtypes", [Object, Object, Object, Function, Object, Object, Function, Object, Object, Function, Function])
    ], PollingStepController);
    return PollingStepController;
}());
exports.PollingStepController = PollingStepController;
;


/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports = "<div class=add-node-wizard-polling-step> <div xui-busy=vm.isBusy xui-busy-message={{vm.progressMessageTitle}} xui-busy-show-progress={{vm.isBusy}} xui-busy-percent={{vm.progressPercentage}} xui-busy-help-text={{vm.progressMessageDescription}}> <xui-message ng-if=vm.pluginMessages ng-repeat=\"pluginMessage in vm.pluginMessages track by pluginMessage.highlightedText\" name=pluginMessage type={{::vm.msgLevelToType(pluginMessage.messageLevel)}} allow-dismiss=false> <strong>{{::pluginMessage.highlightedText}}</strong> <span>{{::pluginMessage.textMinor}}</span> <span ng-if=pluginMessage.knowledgeBaseInfo ng-repeat=\"kbInfo in pluginMessage.knowledgeBaseInfo track by kbInfo.id\"> <br/> <a ng-href={{::vm.helpLinkService.getKbLink(kbInfo.id)}} target=_blank rel=\"noopener noreferrer\"> » <span>{{::kbInfo.hyperLinkText}}</span> </a> </span> </xui-message> <xui-message ng-if=vm.isTypeMismatchMessageVisible name=typeMismatchMessage type=info allow-dismiss=true> <strong>{{::vm.messageText}}</strong> {{::vm.messageDescriptionText}} </xui-message> <div data-selector=general-message> <xui-message ng-if=vm.isGeneralMessageVisible name=generalMessage type={{::vm.messageType}} allow-dismiss=false> <span ng-bind-html=vm.messageText></span> </xui-message> </div> <div class=row> <div class=col-xs-4> <xui-textbox name=address data-selector=server-address caption=\"_t(IP address / Hostname)\" ng-model=vm.serverAddress ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" validators=required,addNodeServerAddressValidator _ta> <div ng-message=required _t>Missing hostname or IP address.</div> <div ng-message=addNodeServerAddress _t>Unable to resolve hostname</div> </xui-textbox> <div ng-if=vm.isIpResolvedFromHostname class=xui-text-dscrn _t=\"['{{::vm.resolvedIpAddress}}']\">Resolved IP: {0}</div> </div> <div class=col-xs-4> <add-node-wizard-polling-engine-selector engines=vm.pollingInfo.engines selected-engine-id=vm.pollingDefinition.pollingEngineId> </add-node-wizard-polling-engine-selector> </div> </div> <xui-divider></xui-divider> <add-node-wizard-polling-method polling-definition=vm.pollingDefinition selected-credentials=vm.credentials polling-methods=vm.pollingInfo.pollingMethods> </add-node-wizard-polling-method> <add-node-wizard-credential-selector polling-method-type=vm.pollingDefinition.pollingMethod.type credentials=vm.credentials plugin-type=vm.pluginType remote-control=vm.credentialSelectorRemoteControl> </add-node-wizard-credential-selector> <xui-message ng-if=vm.isCredentialMessageVisible name=credentialMessage type={{::vm.messageType}} allow-dismiss=false> {{::vm.messageText}} </xui-message> </div> </div> ";

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pollingEngineSelector_directive_1 = __webpack_require__(72);
exports.default = function (module) {
    module.component("addNodeWizardPollingEngineSelector", pollingEngineSelector_directive_1.PollingEngineSelectorDirective);
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(11);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pollingEngineSelector_controller_1 = __webpack_require__(73);
/**
 * Directive wrapper for polling engine selector
 */
var PollingEngineSelectorDirective = /** @class */ (function () {
    function PollingEngineSelectorDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(74);
        this.controller = pollingEngineSelector_controller_1.PollingEngineSelectorController;
        this.controllerAs = "vm";
        this.bindToController = {
            engines: "<",
            selectedEngineId: "="
        };
        this.scope = {};
    }
    return PollingEngineSelectorDirective;
}());
exports.PollingEngineSelectorDirective = PollingEngineSelectorDirective;


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Controller for polling engine
 */
var PollingEngineSelectorController = /** @class */ (function () {
    function PollingEngineSelectorController($stateParams) {
        var _this = this;
        this.$stateParams = $stateParams;
        this.$onInit = function () {
            _this.engines = [];
            if (!_this.selectedEngineId && _this.$stateParams["pollingEngine"]) {
                _this.selectedEngineId = _.parseInt(_this.$stateParams["pollingEngine"]);
            }
        };
        this.$onChanges = function (changes) {
            if (!changes.engines) {
                return;
            }
            if (_.isNumber(_this.selectedEngineId) && _this.engines) {
                var engine = _.find(_this.engines, function (e) { return e.id === _this.selectedEngineId; });
                if (!!engine) {
                    _this.selectedEngine = engine;
                }
                else {
                    _this.selectedEngine = _this.engines[0];
                }
            }
            else if (!_.isEmpty(_this.engines)) {
                _this.selectedEngine = _this.engines[0];
            }
        };
    }
    Object.defineProperty(PollingEngineSelectorController.prototype, "selectedEngine", {
        get: function () {
            return this._selectedEngine;
        },
        set: function (value) {
            this._selectedEngine = value;
            if (value) {
                this.selectedEngineId = value.id;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PollingEngineSelectorController.prototype, "isVisible", {
        get: function () {
            return this.engines && this.engines.length > 1;
        },
        enumerable: true,
        configurable: true
    });
    PollingEngineSelectorController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngStateParams)),
        __metadata("design:paramtypes", [Object])
    ], PollingEngineSelectorController);
    return PollingEngineSelectorController;
}());
exports.PollingEngineSelectorController = PollingEngineSelectorController;


/***/ }),
/* 74 */
/***/ (function(module, exports) {

module.exports = "<div ng-show=vm.isVisible class=\"add-node-wizard-polling-engine-selector xui-dropdown--justified\"> <xui-dropdown caption=\"_t(Polling Engine)\" items-source=vm.engines selected-item=vm.selectedEngine display-value=label _ta> </xui-dropdown> </div>";

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var serverAddressValidator_directive_1 = __webpack_require__(76);
exports.default = function (module) {
    module.component("addNodeServerAddressValidator", serverAddressValidator_directive_1.ServerAddressValidatorDirective);
    module.component("addnodeserveraddressvalidator", serverAddressValidator_directive_1.ServerAddressValidatorDirective);
};


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Directive wrapper for initialization of server address validator
 */
var ServerAddressValidatorDirective = /** @class */ (function () {
    function ServerAddressValidatorDirective() {
        this.restrict = "A";
        this.require = ["ngModel", "^^addNodeWizardPollingStep"];
        this.link = function (scope, instanceElement, instanceAttributes, controllers) {
            controllers[1].initServerAddressValidator(controllers[0]);
        };
    }
    return ServerAddressValidatorDirective;
}());
exports.ServerAddressValidatorDirective = ServerAddressValidatorDirective;


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var metricsStep_directive_1 = __webpack_require__(78);
exports.default = function (module) {
    module.component("addNodeWizardMetricsStep", metricsStep_directive_1.MetricsStepDirective);
};


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(8);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var metricsStep_controller_1 = __webpack_require__(79);
/**
 * Directive wrapper for add node wizard / metrics step
 */
var MetricsStepDirective = /** @class */ (function () {
    function MetricsStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(80);
        this.controller = metricsStep_controller_1.MetricsStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            pluginType: "<",
            discoveryJobId: "<"
        };
        this.scope = {};
    }
    return MetricsStepDirective;
}());
exports.MetricsStepDirective = MetricsStepDirective;
;


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Controller for metrics step
 */
var MetricsStepController = /** @class */ (function () {
    function MetricsStepController(_t, addNodeWizardService, helpLinkService) {
        var _this = this;
        this._t = _t;
        this.addNodeWizardService = addNodeWizardService;
        this.helpLinkService = helpLinkService;
        this.$onInit = function () {
            _this.isBusy = true;
            _this.pluginMessages = null;
            return _this.addNodeWizardService.getMetrics(_this.pluginType, _this.discoveryJobId).then(function (result) {
                _this.metrics = result.metrics;
                _this.pluginMessages = result.messages;
            }).finally(function () {
                _this.isBusy = false;
            });
        };
        this.infoText = this._t("Thresholds can be edited after this device is managed in Orion.");
    }
    MetricsStepController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(1, di_1.Inject(index_1.ServiceNames.addNodeWizardService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.helpLinkService)),
        __metadata("design:paramtypes", [Function, Object, Object])
    ], MetricsStepController);
    return MetricsStepController;
}());
exports.MetricsStepController = MetricsStepController;


/***/ }),
/* 80 */
/***/ (function(module, exports) {

module.exports = "<div class=add-node-wizard-metrics-step> <div xui-busy=vm.isBusy> <xui-message type=info allow-dismiss=true> {{vm.infoText}} </xui-message> <xui-message ng-if=vm.pluginMessages ng-repeat=\"pluginMessage in vm.pluginMessages track by pluginMessage.highlightedText\" name=pluginMessage type=info allow-dismiss=false> <strong>{{::pluginMessage.highlightedText}}</strong> <span>{{::pluginMessage.textMinor}}</span> <span ng-if=pluginMessage.knowledgeBaseInfo ng-repeat=\"kbInfo in pluginMessage.knowledgeBaseInfo track by kbInfo.id\"> <br/> <a ng-href={{::vm.helpLinkService.getKbLink(kbInfo.id)}} target=_blank rel=\"noopener noreferrer\"> » <span>{{::kbInfo.hyperLinkText}}</span> </a> </span> </xui-message> <xui-tabs ng-if=vm.metrics> <xui-tab ng-repeat=\"(key, categoryMetrics) in vm.metrics track by key\" data-selector=metricCategory:{{key}} tab-id={{key}} tab-title={{key}}> <xui-listview items-source=::categoryMetrics template-url=metrics-template row-padding=narrow> </xui-listview> </xui-tab> </xui-tabs> <script type=text/ng-template id=metrics-template> <div class=\"media\">\n                <div class=\"media-left\">\n                    <xui-icon\n                        icon-size=\"small\"\n                        icon=\"{{::item.icon}}\"\n                        icon-color=\"gray\">\n                    </xui-icon>\n                </div>\n                <div class=\"media-body\">\n                    <div class=\"row\">\n                        <div class=\"col-xs-7 xui-text-l\">\n                            <div data-selector=\"metric-display-name\">{{::item.displayName}}</div>\n                        </div>\n                        <div class=\"col-xs-5 text-right text-gray\" data-selector=\"metric-entity-name\">\n                            <strong>{{::item.entityName}}</strong>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-xs-3 xui-margin-smb-neg\">\n                            <p class=\"\" data-selector=\"metric-description\">\n                                <ng-bind-html class=\"text-gray\" ng-bind-html=\"::item.description\"></ng-bind-html>\n                            </p>\n                        </div>\n                        <div class=\"col-xs-2\">\n                            <span ng-if=\"!!item.warningValue\" class=\"xui-margin-lgr\">\n                                <xui-icon\n                                    icon-size=\"small\"\n                                    icon=\"status_warning\">\n                                </xui-icon>\n                                <strong data-selector=\"metric-warning-value\">&nbsp;{{::item.warningPrefix}} {{::item.warningValue}}{{::item.unit}}</strong>\n                            </span>\n                        </div>\n                        <div class=\"col-xs-2\">\n                            <span ng-if=\"!!item.criticalValue\">\n                                <xui-icon\n                                    icon-size=\"small\"\n                                    icon=\"status_critical\">\n                                </xui-icon>\n                                <strong data-selector=\"metric-critical-value\">&nbsp;{{::item.criticalPrefix}} {{::item.criticalValue}}{{::item.unit}}</strong>\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div> </script> </div> </div> ";

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var wizardStepToggle_directive_1 = __webpack_require__(82);
exports.default = function (module) {
    module.component("addNodeWizardStepToggle", wizardStepToggle_directive_1.WizardStepToggleDirective);
};


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Directive for toggling wizard step visibility
 */
var WizardStepToggleDirective = /** @class */ (function () {
    function WizardStepToggleDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(83);
        this.require = ["^^xuiWizardStep"];
        this.scope = {};
        this.link = function (scope, instanceElement, instanceAttributes, controllers) {
            // keep step instance within current scope
            scope.step = controllers[0];
        };
    }
    return WizardStepToggleDirective;
}());
exports.WizardStepToggleDirective = WizardStepToggleDirective;


/***/ }),
/* 83 */
/***/ (function(module, exports) {

module.exports = "<div> <div ng-if=step.active ng-transclude=\"\"></div> </div>";

/***/ })
/******/ ]);
//# sourceMappingURL=add-node.js.map