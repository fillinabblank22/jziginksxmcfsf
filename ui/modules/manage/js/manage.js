/*!
 * @solarwinds/entity-management 1.5.0-1414
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <p class=xui-text-p _t> We are sad to hear that you want or simply need to switch back to legacy page. </p> <p class=xui-text-p _t> Please help us understand why you want to switch back. Feedback helps us incorporate any missing functionality to address your needs. </p> <xui-textbox ng-model=vm.dialogOptions.viewModel.feedback caption=\"_t(Why do you need to switch back to legacy?)\" rows=5 _ta> </xui-textbox> </xui-dialog> ";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div> <xui-radio name=dialog-action ng-model=vm.dialogOptions.viewModel.dialogStatus value=mute _t> Mute alerts on this object </xui-radio> </div> <div> <xui-radio name=dialog-action ng-model=vm.dialogOptions.viewModel.dialogStatus value=unmanage _t> Stop polling this object (Unmanage) </xui-radio> </div> <div> <h6 _t>Beginning of the maintenance:</h6> <div> <xui-datetime-picker display-mode=inline ng-model=vm.dialogOptions.viewModel.timeFrom> </xui-datetime-picker> </div> </div> <div> <h6 _t>End of the maintenance:</h6> <div> <xui-datetime-picker display-mode=inline ng-model=vm.dialogOptions.viewModel.timeUntill> </xui-datetime-picker> </div> </div> </xui-dialog> ";

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=manage-nodes page-layout=fill> <xui-page-header class=manage-nodes__custom-header> <h1 class=\"xui-text-h1 xui-page-content__header-title\" _t>Manage Entities</h1> <xui-menu title=_t(Commands) display-style=tertiary _ta> <xui-menu-action action=$nodesCtrl.switchToLegacy() class=text-uppercase icon=redo _t> Switch back to legacy page </xui-menu-action> </xui-menu> </xui-page-header> <div class=manage-nodes__body ng-class=\"{'manage-nodes__intro-message--on': $nodesCtrl.showWelcomeMessage}\"> <div class=manage-nodes__intro-message--container ng-if=$nodesCtrl.showWelcomeMessage> <xui-message type=info allow-dismiss=true class=manage-nodes__intro-message on-dismiss=$nodesCtrl.onSwitchToLegacyDismiss()> <strong _t>Welcome to the Manage Entities page.</strong> <span _t> You can switch to the legacy page by using the Command menu in the page title section.</span> </xui-message> </div> <div class=manage-nodes__sidebar> <xui-sidebar-container layout=fill> <xui-filtered-list class=\"xui-edge-definer manage-nodes__filtered-list\" filter-properties-fn=$nodesCtrl.filterFunctions() filter-property-groups=$nodesCtrl.filterGroups filter-values=$nodesCtrl.filterValues filter-properties=$nodesCtrl.filterProperties filter-property-states=$nodesCtrl.filterPropertyStates on-filter-property-states-change=$nodesCtrl.updateUserSettings() items-source=$nodesCtrl.nodes on-refresh=\"$nodesCtrl.onRefresh(filters, pagination, sorting, searching)\" options=$nodesCtrl.filteredListOptions sorting=$nodesCtrl.sorting selection=$nodesCtrl.selection pagination-data=$nodesCtrl.paging remote-control=$nodesCtrl.remoteControl controller=$nodesCtrl layout=fill> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item ng-show=$nodesCtrl.actionAvailability.addNode()> <xui-button icon=add display-style=tertiary class=add-node--button ng-click=$nodesCtrl.navigationAction($nodesCtrl.actionLinks.addNode()) _t> Add Node </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-show=$nodesCtrl.actionAvailability.customPropertyEditor()> <xui-button icon=table icon-color=blue display-style=tertiary class=add-property--button ng-click=$nodesCtrl.navigationAction($nodesCtrl.actionLinks.edit()) _t> Custom Property Editor </xui-button> </xui-toolbar-item> <xui-toolbar-divider></xui-toolbar-divider> <xui-toolbar-item ng-show=$nodesCtrl.actionAvailability.moreMenuSingle() class=text-uppercase> <xui-menu display-style=tertiary menu-align=right title={{::$nodesCtrl.textConst.moreMenuTitle}}> <xui-menu-action action=$nodesCtrl.updateTopology() class=update-topology--button icon=reload _t> Update Topology </xui-menu-action> <xui-menu-action action=$nodesCtrl.editRowPropsAction() class=edit-row-properties--button icon=edit _t> Edit Row Properties </xui-menu-action> </xui-menu> </xui-toolbar-item> <xui-toolbar-item ng-show=$nodesCtrl.actionAvailability.editProperties()> <xui-button icon=edit display-style=tertiary class=edit--button ng-click=$nodesCtrl.editProperties() _t> Edit Properties </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-show=$nodesCtrl.actionAvailability.listResources()> <xui-button icon=list-all display-style=tertiary class=monitor-statistics--button ng-click=$nodesCtrl.navigationAction($nodesCtrl.actionLinks.listResources($nodesCtrl.getSelectedNodes()[0])) _t> List Resources </xui-button> </xui-toolbar-item> <xui-toolbar-divider ng-show=$nodesCtrl.actionAvailability.pollNow()></xui-toolbar-divider> <xui-toolbar-item ng-show=$nodesCtrl.selection.items.length> <xui-button icon=reload display-style=tertiary class=poll-now--button ng-click=$nodesCtrl.pollNow() _t> Poll Now </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-show=$nodesCtrl.actionAvailability.assignPollers()> <xui-button icon=assign display-style=tertiary class=assign-pollers-button ng-click=$nodesCtrl.assignPollers() _t> Assign Pollers </xui-button> </xui-toolbar-item> <xui-toolbar-divider ng-show=$nodesCtrl.selection.items.length></xui-toolbar-divider> <xui-toolbar-item ng-show=$nodesCtrl.actionAvailability.moreMenuMulti() class=text-uppercase> <xui-menu display-style=tertiary menu-align=right title={{::$nodesCtrl.textConst.moreMenuTitle}}> <xui-menu-header _t>Maintenance Mode</xui-menu-header> <xui-menu-action action=$nodesCtrl.suppressAlerts() class=mute-alerts--button icon=mute _t> Mute Alerts </xui-menu-action> <xui-menu-action action=$nodesCtrl.resumeAlerts() class=resume-alerts--button icon=sound-3 _t> Resume Alerts </xui-menu-action> <xui-menu-action action=$nodesCtrl.unmanageNodes() class=unmanage-again--button icon=un-manage _t> Un-manage Now </xui-menu-action> <xui-menu-action action=$nodesCtrl.manageAgainNodes() class=manage-again--button icon=performance _t> Manage Again </xui-menu-action> <xui-menu-action action=$nodesCtrl.createScheduleDialog() class=rediscover-now--button icon=schedule _t> Schedule Maintenance </xui-menu-action> <xui-menu-action action=$nodesCtrl.manageAgainNodes() class=cancel-unmanage--button icon=close _t> Cancel Planned Un-manage </xui-menu-action> <xui-menu-divider></xui-menu-divider> <xui-menu-header _t>Additional Actions</xui-menu-header> <xui-menu-action action=$nodesCtrl.usePolledStatus() class=use-polled-status--button icon=reload _t> Use Polled Status </xui-menu-action> <xui-menu-action action=$nodesCtrl.rediscoverNow() class=rediscover-now--button icon=scan _t> Rediscover </xui-menu-action> <xui-menu-divider></xui-menu-divider> <xui-menu-action action=$nodesCtrl.deleteNode() class=delete-node--button icon=delete _t> Delete </xui-menu-action> </xui-menu> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list> <xui-sidebar settings=$nodesCtrl.sidebar xui-if-show=vm.features.NPM> <xui-sidebar-header> <div class=entity-inspector__sidebar-header> <div class=entity-inspector__heading> <xui-button icon=close display-style=tertiary class=pull-right ng-click=$nodesCtrl.clearInspector()></xui-button> <h4 _t>Related Entities</h4> </div> <div class=entity-inspector__toolbar ng-show=$nodesCtrl.entityInspector.selection.length> <xui-toolbar class=\"entity-inspector-toolbar xui-toolbar--active\"> <xui-toolbar-item> <xui-button display-style=tertiary icon=delete ng-click=$nodesCtrl.deleteChildren() _t>Delete</xui-button> </xui-toolbar-item> <xui-toolbar-splitter></xui-toolbar-splitter> <xui-toolbar-item class=\"entity-inspector-toolbar__caption xui-text-l\"> <span>{{$nodesCtrl.entityInspector.selection.length}} </span> <span _t> item(s) selected.</span> </xui-toolbar-item> </xui-toolbar> </div> <div class=entity-inspector__group> <div class=row> <div class=\"col-xs-6 xui-dropdown--justified column-left\"> <xui-dropdown ng-model=$nodesCtrl.childEntityType items-source=$nodesCtrl.childEntityTypeOptions display-value=displayName is-disabled=true></xui-dropdown> </div> <div class=\"col-xs-6 xui-dropdown--justified column-right\"> <xui-dropdown ng-model=$nodesCtrl.childGroupType items-source=$nodesCtrl.childGroupTypeOptions display-value=displayName on-changed=$nodesCtrl.organizeChildren()></xui-dropdown> </div> </div> </div> <div class=entity-inspector__search> <xui-search placeholder={{::$nodesCtrl.textConst.searchPlaceholder}} value=$nodesCtrl.childSearch on-search=$nodesCtrl.updateRelatedEntites()> </xui-search> </div> </div> </xui-sidebar-header> <div class=entity-inspector__sidebar-body> <div class=entity-inspector__entity-container ng-repeat=\"entity in $nodesCtrl.entityInspector.populatedEntities track by entity.nodeID\"> <h4>{{::entity.caption}}</h4> <div ng-repeat=\"group in entity.childGroups track by $nodesCtrl.getGroupTrackingId($nodesCtrl.childGroupType, group)\"> <div class=entity-inspector__expander ng-if=group.children.length> <xui-expander> <xui-expander-heading> <div class=heading> <div class=heading__checkbox> <xui-checkbox class=xui-listitem__select ng-model=group.checked ng-change=$nodesCtrl.groupSelect(group) is-indeterminate=group.indeterminate> </xui-checkbox> </div> <div class=heading__caption> <div class=heading__icon><xui-icon icon={{::group.icon}}></xui-icon></div> <span class=\"heading__caption-text ellipsis\">{{::group.name}}</span> <span class=heading__counter>({{group.children.length}})</span> </div> </div> </xui-expander-heading> <div class=list> <xui-listview stripe=false row-padding=none items-source=group.children selection-mode=multi selection-property=netObjectId selection=$nodesCtrl.entityInspector.selection controller=$nodesCtrl track-by=netObjectId template-url=entityManagement/views/manage/nodes/childItem.html> </xui-listview> </div> </xui-expander> </div> </div> <div ng-if=\"entity.childrenCount == 0\" _t>There are no interfaces.</div> </div> </div> </xui-sidebar> </xui-sidebar-container> </div> </div> </xui-page-content> ";

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=sw-edit-row-props page-title=\"_t(Manage Entities)\" page-layout=fill _ta> <sw-header-overlay title=\"_t(Edit Row Properties & Layout)\" show-overlay=$editRowProps.showOverlay _ta> <xui-button ng-click=$editRowProps.saveChanges() display-style=primary _t> Save Changes </xui-button> <xui-button class=sw-edit-row-props__cancel-button ng-click=$editRowProps.cancel() display-style=tertiary _t> Cancel </xui-button> </sw-header-overlay> <xui-panel heading=\"_t(Available properties)\" panel-layout=fill _ta> <xui-panel-left-pane class=xui-dropdown--justified> <xui-dropdown ng-model=$editRowProps.propsToShow items-source=$editRowProps.listOfProps dropdown-append-to-body=true> </xui-dropdown> <xui-scroll-shadows ng-if=\"$editRowProps.listOfProps.indexOf($editRowProps.propsToShow) === 0\"> <div class=sw-edit-row-props__checkbox-container> <xui-checkbox-group ng-model=$editRowProps.nodePropsToShow.customProps> <xui-checkbox ng-repeat=\"customProp in $editRowProps.nodeProps.customProps\" value=customProp.navigationName> <span class=xui-tag> {{customProp.displayName}} </span> </xui-checkbox> </xui-checkbox-group> </div> </xui-scroll-shadows> <xui-scroll-shadows ng-if=\"$editRowProps.listOfProps.indexOf($editRowProps.propsToShow) === 1\"> <div class=sw-edit-row-props__checkbox-container> <div ng-repeat=\"systemPropGroup in $editRowProps.nodeProps.systemProps\"> <h4 class=\"xui-text-s text-uppercase\">{{systemPropGroup.displayName}}</h4> <xui-checkbox-group ng-model=$editRowProps.nodePropsToShow.systemProps> <xui-checkbox ng-repeat=\"systemProp in systemPropGroup.nodeProperties\" value=systemProp.navigationName> {{systemProp.displayName}} </xui-checkbox> </xui-checkbox-group> <xui-divider ng-if=!$last></xui-divider> </div> </div> </xui-scroll-shadows> </xui-panel-left-pane> <xui-panel-center-pane> <div class=sw-edit-row-props__center-pane> <div class=sw-edit-row-props__row-template> <h4 class=\"xui-text-s text-uppercase sw-edit-row-props__title\" _t>ROW TEMPLATE</h4> <div class=sw-edit-row-props__preview-top> <xui-icon icon=router status=up> </xui-icon> <span class=sw-edit-row-props__node-title _t> Entity Name </span> <xui-chiclets items-source=$editRowProps.nodePropsToShow.customProps item-label-fn=$editRowProps.getCustomPropLabel(itemId) is-draggable=true allow-clear-all=false class=sw-edit-row-props__chiclets> </xui-chiclets> </div> <div class=sw-edit-row-props__preview-bottom> <span class=sw-edit-row-props__ip-address _t> IP Address </span> <xui-icon icon=widget_chart> </xui-icon> <span _t> Machine Type </span> <xui-chiclets items-source=$editRowProps.nodePropsToShow.systemProps item-label-fn=$editRowProps.getSystemPropLabel(itemId) is-draggable=true allow-clear-all=false class=sw-edit-row-props__chiclets> </xui-chiclets> </div> </div> <xui-divider></xui-divider> </div> </xui-panel-center-pane> </xui-panel> </xui-page-content> ";

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(5);
module.exports = __webpack_require__(6);


/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(7);
var config_1 = __webpack_require__(8);
var index_1 = __webpack_require__(9);
var index_2 = __webpack_require__(11);
var index_3 = __webpack_require__(20);
var index_4 = __webpack_require__(21);
var templates_1 = __webpack_require__(24);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("manage.services", []);
angular.module("manage.templates", []);
angular.module("manage.components", ["orion-ui-components"]);
angular.module("manage.filters", []);
angular.module("manage.providers", []);
angular.module("manage", [
    "orion",
    "manage.services",
    "manage.templates",
    "manage.components",
    "manage.filters",
    "manage.providers"
]);
// create and register Xui (Orion) module wrapper
var entityManagement = Xui.registerModule("manage");
exports.default = entityManagement;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: entity-management");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(10);
exports.default = function (module) {
    module.service("entityManagementConstants", constants_1.default);
};


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var nodes_1 = __webpack_require__(12);
var editRowProperties_1 = __webpack_require__(16);
exports.default = function (module) {
    nodes_1.default(module);
    editRowProperties_1.default(module);
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var nodes_controller_1 = __webpack_require__(13);
var nodes_config_1 = __webpack_require__(14);
__webpack_require__(15);
exports.default = function (module) {
    module.controller("NodesController", nodes_controller_1.NodesController);
    module.app().config(nodes_config_1.config);
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NodesController = /** @class */ (function () {
    function NodesController($scope, _t, swApi, swManageService, webAdminService, toast, swSwisDataMappingService, $timeout, xuiDialogService, $q, $window, swAlertsManagementService, constants, entityPropsToDisplayService, swDemoService) {
        var _this = this;
        this.$scope = $scope;
        this._t = _t;
        this.swApi = swApi;
        this.swManageService = swManageService;
        this.webAdminService = webAdminService;
        this.toast = toast;
        this.swSwisDataMappingService = swSwisDataMappingService;
        this.$timeout = $timeout;
        this.xuiDialogService = xuiDialogService;
        this.$q = $q;
        this.$window = $window;
        this.swAlertsManagementService = swAlertsManagementService;
        this.constants = constants;
        this.entityPropsToDisplayService = entityPropsToDisplayService;
        this.swDemoService = swDemoService;
        this.nodes = [];
        this.actionLinks = {
            addNode: function () { return "/Orion/Nodes/Add/Default.aspx"; },
            edit: function () { return "/Orion/Admin/CPE/InlineEditor.aspx"; },
            import: function () { return "/Orion/Admin/CPE/ImportCP.aspx"; },
            listResources: function (node) {
                if (node) {
                    return "/Orion/Nodes/ListResources.aspx?Nodes=" + node.nodeID + "&SubType=" + node.objectSubType;
                }
            }
        };
        this.filterValues = {
            "Status": [],
            "Orion.Nodes|Vendor": [],
            "Orion.Nodes|MachineType": [],
            "Orion.Nodes|ObjectSubType": [],
            "Orion.Nodes|Engine.ServerName": []
        };
        //it seems like optional parameter for FilteredList
        this.filterGroups = ["Orion.Nodes", "general"];
        this.selection = {
            items: []
        };
        this.remoteControl = {
            refreshListData: function () { return _this.$q.resolve(); }
        };
        this.showWelcomeMessage = false;
        this.prefix = "WebNodeManagement_";
        this.actionAvailability = {
            addNode: function () { return !_this.selection.items.length; },
            customPropertyEditor: function () { return !_this.selection.items.length; },
            moreMenuSingle: function () { return !_this.selection.items.length; },
            editProperties: function () { return !!_this.selection.items.length; },
            listResources: function () {
                var selection = [];
                if (_this.selection.items.length === 1) {
                    selection = _this.nodes.filter(function (n) { return n.netObjectId === _this.selection.items[0]; });
                }
                return selection.length && selection[0].objectSubType !== "ICMP";
            },
            pollNow: function () { return !!_this.selection.items.length; },
            assignPollers: function () {
                return _this.features && _this.features.NPM && _this.selection.items.length &&
                    _this.nodes.filter(function (n) { return _this.selection.items.indexOf(n.netObjectId) > -1 &&
                        n.objectSubType === "SNMP"; }).length === _this.selection.items.length;
            },
            moreMenuMulti: function () { return !!_this.selection.items.length; }
        };
        this.vendorFilterTemplateUrl = "entityManagement/views/manage/nodes/nodesVendorFilter.html";
        this.isFirstLoad = true;
        this.$onInit = function () {
            _this.emptyText = _this._t("No results found for");
            _this.sorting = {
                sortableColumns: [
                    {
                        id: "Caption",
                        label: _this._t("Name"),
                    },
                    {
                        id: "Status",
                        label: _this._t("Status")
                    },
                    {
                        id: "IpAddress",
                        label: _this._t("IP Address"),
                    }
                ],
                sortBy: {
                    id: "Caption",
                    label: _this._t("Name")
                },
                direction: "asc"
            };
            _this.paging = {
                page: 1,
                pageSize: NodesController.defaultPageSize
            };
            _this.filteredListOptions = {
                allowSelectAllPages: false,
                pageSize: NodesController.defaultPageSize,
                rowPadding: "narrow",
                selectionMode: "multi",
                selectionProperty: "netObjectId",
                showAddRemoveFilterProperty: true,
                smartMode: false,
                showEmptyFilterProperties: true,
                showMorePropertyValuesThreshold: 10,
                sortableColumns: _this.sorting.sortableColumns,
                templateUrl: "entityManagement/views/manage/nodes/nodeItem.html",
                trackBy: "netObjectId"
            };
            _this.sidebar = {
                display: false,
                position: "right",
                shrink: true
            };
            _this.entityInspector = {
                entities: [],
                populatedEntities: [],
                children: [],
                search: "",
                groupBy: "",
                selection: [],
                updateParent: true
            };
            _this.childEntityTypeOptions = [
                { name: "Interfaces", displayName: _this._t("Interfaces") },
                { name: "Volumes", displayName: _this._t("Volumes") }
            ];
            _this.childEntityType = _this.childEntityTypeOptions[0];
            _this.childGroupTypeOptions = [
                { name: "typeId", displayName: _this._t("Type") },
                { name: "status", displayName: _this._t("Status") }
            ];
            _this.childGroupType = _this.childGroupTypeOptions[0];
            _this.$scope.$watch(function () { return _this.entityInspector.selection; }, _this.updateParentChecked, true);
            _this.textConst = {
                deleteCompleteMessage: _this._t("Deletion completed"),
                dialogClosedInfo: _this._t("Dialog closed"),
                dialogActionNotChoosed: _this._t("Choose an action"),
                manageAgainSuccess: _this._t("Manage Again successfully completed"),
                moreMenuTitle: _this._t("More"),
                pollNowStarted: _this._t("Poll will be completed in few second"),
                rediscoverNowStarted: _this._t("Rediscover will be completed in few seconds"),
                resumeAlertsSuccess: _this._t("Resuming alerts successfully completed"),
                suppressAlertsSuccess: _this._t("Muting alerts successfully completed"),
                unmanageAgainSuccess: _this._t("Un-manage Now successfully completed"),
                updateTopologySuccess: _this._t("Topology has been updated successfully"),
                usePolledStatusStarted: _this._t("Use polled status will be completed in few seconds"),
                searchPlaceholder: _this._t("Search...")
            };
            _this.nodePropsToShow = {
                customProps: [],
                systemProps: []
            };
            if (!_this.features) {
                _this.swApi.api(false).all("features").getList().then(function (availableFeatures) {
                    _this.features = {
                        NCM: _.includes(availableFeatures, "NCM.Nodes"),
                        NPM: _.includes(availableFeatures, "Interfaces")
                    };
                });
            }
            _this.applyUserSettings();
            _this.webAdminService.getUserSetting(_this.prefix + "WelcomeMessageDismissed").then(function (response) {
                response = response || "false";
                _this.showWelcomeMessage = response.toLowerCase() !== "true";
            });
        };
        this.onRefresh = function (filters, pagination, sorting, searching) {
            if (_this.isFirstLoad) {
                _this.isFirstLoad = false;
                return _this.$q.resolve();
            }
            var getNodes = _this.swManageService
                .getNodes(_this.nodePropsToShow, searching, filters, pagination, sorting)
                .then(function (nodes) {
                _this.nodes = nodes.items;
                _this.paging.total = nodes.totalCount;
                _this.updateAlertsSuppressionState();
                if (!_this.nodes.length) {
                    _this.updateEmptyText();
                }
            });
            var refreshFilters = _this.swManageService
                .getFiltersPropsAndValues(searching, _this.filterValues)
                .then(function (res) { return _this.filterProperties = _this.prepareFilters(res); });
            var refreshNodesAndFilters = function () {
                return _this.$q.all([getNodes, refreshFilters])
                    .then(function (result) { return result[0]; });
            };
            _this.searching = searching;
            return _this.updateUserSettings()
                .then(refreshNodesAndFilters);
        };
        this.navigationAction = function (location) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
            }
            else {
                _this.$window.location.assign(location);
            }
        };
        this.updateUserSettings = function () {
            var settingsPromise;
            var settingName = _this.prefix + "UserSettings";
            if (_this.swDemoService.isDemoMode()) {
                settingsPromise = _this.resolveUserSettings(settingName);
            }
            else {
                settingsPromise = _this.webAdminService.getUserSetting(settingName);
            }
            return settingsPromise
                .then(function (oldUserSettings) {
                var newUserSettings = {
                    actualPage: _this.paging.page,
                    filterByValues: _this.filterValues,
                    filtersState: _this.filterPropertyStates,
                    pageSize: _this.paging.pageSize,
                    sortDirection: _this.sorting.direction,
                    sortProp: _this.sorting.sortBy.id,
                    sortPropType: "Node"
                };
                var userSettings = angular.toJson(newUserSettings);
                if (oldUserSettings !== userSettings) {
                    return _this.updateUserSetting("UserSettings", userSettings);
                }
            });
        };
        /* Toolbar actions */
        this.updateTopology = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                return _this.swApi.ws.one("NodeManagement.asmx")
                    .post("CalculateSystemTopology", {})
                    .then(function () { return _this.toast.success(_this.textConst.updateTopologySuccess); });
                ;
            }
        };
        this.pollNow = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                var promises = _this.selection.items.map(function (item) {
                    return _this.swApi.ws.one("NodeManagement.asmx")
                        .post("PollNetObjNow", { netObjectIds: item });
                });
                return _this.$q.all(promises)
                    .then(function () { return _this.toast.success(_this.textConst.pollNowStarted); });
            }
        };
        this.editProperties = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return;
            }
            var ids = _this.selection.items;
            // To implement ReturnUrl we need to propagate it from here:
            // http://dev-opengrok.swdev.local/xref/Core/OrionCommon/OrionWebAssembly/ReferrerRedirector.cs#101
            var returnUrl = "";
            _this.prepareForm("Node", ids, returnUrl);
            _this.$window.document.forms.namedItem("selectedNetObjects").submit();
        };
        this.rediscoverNow = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                var promises = _.map(_this.selection.items, function (item) {
                    return _this.swApi.ws.one("NodeManagement.asmx")
                        .post("RediscoverNow", { netObjectIds: item });
                });
                return _this.$q.all(promises)
                    .then(function () { return _this.toast.success(_this.textConst.rediscoverNowStarted); });
            }
        };
        this.unmanageNodes = function (unmanageFrom, unmanageUntil) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                var bothDates = !!(unmanageFrom && unmanageUntil);
                return _this.swApi.ws.one("NodeManagement.asmx")
                    .post("UnmanageNodes", {
                    nodeIds: _this.selectionIDs(),
                    // OADP-714 fix - we need here Date object in UTC time for
                    // NodeManagement.asmx service without Z at the end
                    unmanageFrom: bothDates ? moment(unmanageFrom)
                        .utc()
                        .format("YYYY-MM-DD HH:mm")
                        : null,
                    unmanageUntil: bothDates ? moment(unmanageUntil)
                        .utc()
                        .format("YYYY-MM-DD HH:mm")
                        : null
                })
                    .then(function () {
                    _this.toast.success(_this.textConst.unmanageAgainSuccess);
                    _this.remoteControl.refreshListData();
                });
            }
        };
        this.manageAgainNodes = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                return _this.swApi.ws.one("NodeManagement.asmx")
                    .post("RemanageNodes", { nodeIds: _this.selectionIDs() })
                    .then(function () {
                    _this.toast.success(_this.textConst.manageAgainSuccess);
                    _this.remoteControl.refreshListData();
                });
            }
        };
        this.assignPollers = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            return _this.swApi.ws.one("NodeManagement.asmx")
                .post("StoreSelectedIDs", { ids: _this.selectionIDs(), netObjectType: "Nodes" })
                .then(function () { return _this.$window.location.assign("/Orion/NPM/NodeCustomPollers.aspx"); });
        };
        this.usePolledStatus = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                var promises = _.map(_this.selection.items, function (item) {
                    return _this.swApi.ws.one("NodeManagement.asmx")
                        .post("UsePolledStatus", { netObjectIds: item });
                });
                return _this.$q.all(promises)
                    .then(function () { return _this.toast.success(_this.textConst.usePolledStatusStarted); });
            }
        };
        this.suppressAlerts = function (suppressFrom, suppressUntil) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                var bothDates = !!(suppressFrom && suppressUntil);
                var uris = _this.getSelectedNodes().map(function (node) { return node.uri; });
                return _this.swAlertsManagementService
                    .suppressAlerts(uris, bothDates ? suppressFrom : undefined, bothDates ? suppressUntil : undefined)
                    .then(function () {
                    _this.toast.success(_this.textConst.suppressAlertsSuccess);
                    _this.updateAlertsSuppressionState();
                });
            }
        };
        //resume alerts on provided node or all selected nodes
        this.resumeAlerts = function (node) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                var uris = node
                    ? [node.uri]
                    : _this.getSelectedNodes().map(function (selectedNode) { return selectedNode.uri; });
                return _this.swAlertsManagementService
                    .resumeAlerts(uris)
                    .then(function () {
                    _this.toast.success(_this.textConst.resumeAlertsSuccess);
                    _this.updateAlertsSuppressionState();
                });
            }
        };
        this.deleteNode = function () {
            _this.deleteObjects(_this.selection.items, function () {
                _this.selection.items.length = 0;
                _this.remoteControl.refreshListData();
            });
        };
        this.deleteObjects = function (ids, callback) {
            var message;
            if (ids.length > 1) {
                message = String.format(_this._t("Are you sure you want to delete selected {0} entities?"), ids.length);
            }
            else {
                message = _this._t("Are you sure you want to delete selected entity?");
            }
            _this.xuiDialogService.showWarning({
                title: _this._t("Delete Entities"),
                message: message
            }, {
                name: "delete",
                text: _this._t("Delete"),
                action: function (dialogResult) {
                    if (_this.swDemoService.isDemoMode()) {
                        _this.swDemoService.showDemoErrorToast();
                    }
                    else {
                        _this.swApi.ws.one("NodeManagement.asmx")
                            .post("DeleteNetObjects", { netObjectIds: ids }).then(function () {
                            _this.toast.success(_this.textConst.deleteCompleteMessage);
                            callback();
                        });
                    }
                    return true;
                }
            });
        };
        this.editRowPropsAction = function () {
            _this.$window.location.assign("/ui/manage/nodes/edit-row");
        };
        /* End of Toolbar actions */
        /* Entity Inspector / Sidebar section */
        this.updateSidebar = function () {
            _this.sidebar.display = _this.entityInspector.entities.length > 0;
        };
        this.addToInspector = function (entity) {
            if (_this.entityInspector.entities.filter(function (e) { return e.nodeID === entity.nodeID; }).length === 0) {
                _this.entityInspector.entities.push(entity);
                _this.updateRelatedEntites();
                _this.updateSidebar();
            }
        };
        this.removeFromInspector = function (entity) {
            _this.entityInspector.entities = _this.entityInspector.entities.filter(function (e) { return e.nodeID !== entity.nodeID; });
            _this.updateRelatedEntites();
            _this.updateSidebar();
        };
        this.clearInspector = function () {
            _this.entityInspector.entities = [];
            _this.entityInspector.populatedEntities = [];
            _this.entityInspector.search = "";
            _this.childSearch = "";
            _this.entityInspector.selection = [];
            _this.updateSidebar();
        };
        this.isInspected = function (entityId) {
            return _this.entityInspector.entities.filter(function (e) { return e.nodeID === entityId; }).length > 0;
        };
        this.updateRelatedEntites = function () {
            _this.entityInspector.search = _this.childSearch;
            _this.entityInspector.selection = [];
            var getChildren;
            if (_this.childEntityType.name === "Interfaces") {
                getChildren = _this.getNodeInterfaces();
            }
            else if (_this.childEntityType.name === "Volumes") {
                getChildren = _this.getNodeVolumes();
            }
            return getChildren.then(_this.organizeChildren);
        };
        this.getNodeInterfaces = function () {
            var entityType = "orion.npm.interfaces";
            var properties = [
                "InterfaceID",
                "Caption",
                "DisplayName",
                "FullName",
                "InterfaceType",
                "InterfaceTypeDescription",
                "OrionIDPrefix",
                "DetailsUrl",
                "Status",
                "NodeId"
            ];
            var nodeIDs = _.map(_this.entityInspector.entities, function (e) { return e.nodeID; });
            return _this.swManageService.getNodeChildren(entityType, properties, nodeIDs, _this.entityInspector.search)
                .then(function (nodeInterfaces) {
                _this.entityInspector.children = _.map(nodeInterfaces.items, function (i) { return _this.interfaceAsChild(i); });
            });
        };
        // Stub
        this.getNodeVolumes = function () { return _this.$q.resolve(); };
        this.interfaceAsChild = function (i) {
            var map = {
                "id": "interfaceID",
                "name": "displayName",
                "typeId": "interfaceType",
                "typeName": "interfaceTypeDescription"
            };
            var child = _this.entityAsChild(i, map);
            child.icon = "network-interface";
            child.netObjectId = "" + (i.orionIDPrefix + i.interfaceID);
            return child;
        };
        this.entityAsChild = function (entity, map) {
            var child = entity;
            for (var key in map) {
                if (map.hasOwnProperty(key)) {
                    child[key] = child[map[key]];
                }
            }
            return child;
        };
        this.organizeChildren = function () {
            _this.entityInspector.selection = [];
            _this.entityInspector.populatedEntities = [];
            _.forEach(_this.entityInspector.entities, _this.populateEntity);
        };
        this.populateEntity = function (entity) {
            var entityChildren = _this.entityInspector.children.filter(function (c) { return c.nodeId === entity.nodeID; });
            entity.childrenCount = entityChildren.length;
            entity.childGroups = _this.groupChildren(entityChildren);
            _this.entityInspector.populatedEntities.push(entity);
        };
        this.groupChildren = function (children) {
            var childGroups = [];
            var grouper = {};
            var getGroupName = function (child) {
                if (_this.childGroupType.name === "typeId") {
                    var typeOption = _this.childEntityTypeOptions.find(function (c) { return c.name === child.typeName; });
                    return (typeOption) ? typeOption.displayName : child.typeName;
                }
                if (_this.childGroupType.name === "status") {
                    return _this.swSwisDataMappingService.getStatusTranslated(child[_this.childGroupType.name]);
                }
                return "";
            };
            var getGroupIcon = function (child) {
                if (_this.childGroupType.name === "status") {
                    return "status_" + _this.swSwisDataMappingService.getStatus(child[_this.childGroupType.name]);
                }
                if (_this.childEntityType.name === "Interfaces") {
                    return "network-interface";
                }
                if (_this.childEntityType.name === "Volumes") {
                    return "volume";
                }
                return "unknownnode";
            };
            var getGroup = function (child) {
                var groupId = child[_this.childGroupType.name].toString();
                grouper[groupId] = grouper[groupId] || {
                    id: groupId,
                    name: getGroupName(child),
                    icon: getGroupIcon(child),
                    checked: false,
                    indeterminate: false,
                    children: []
                };
                return grouper[groupId];
            };
            children.filter(function (child) {
                var group = getGroup(child);
                group.children.push(child);
            });
            _.forEach(grouper, function (group) { return childGroups.push(group); });
            return childGroups;
        };
        this.getGroupTrackingId = function (childGroupType, group) {
            return childGroupType.name + "-" + group.id;
        };
        this.groupSelect = function (group) {
            _this.entityInspector.updateParent = false;
            group.indeterminate = false;
            var childIds = group.children.map(function (child) { return child.netObjectId; });
            if (group.checked) {
                childIds.filter(function (id) {
                    if (_this.entityInspector.selection.indexOf(id) < 0) {
                        _this.entityInspector.selection.push(id);
                    }
                });
            }
            else {
                _this.entityInspector.selection = _this.entityInspector.selection.filter(function (id) { return childIds.indexOf(id) < 0; });
            }
        };
        this.updateParentChecked = function (newSelection, oldSelection) {
            if (!_this.entityInspector.updateParent || newSelection.length === oldSelection.length) {
                _this.entityInspector.updateParent = true;
                return;
            }
            var childModifiedNetObjectId;
            if (newSelection.length > oldSelection.length) {
                childModifiedNetObjectId = newSelection.filter(function (id) { return oldSelection.indexOf(id) < 0; })[0];
            }
            else {
                childModifiedNetObjectId = oldSelection.filter(function (id) { return newSelection.indexOf(id) < 0; })[0];
            }
            var childModifiedNodeId = _this.entityInspector.children
                .filter(function (c) { return c.netObjectId === childModifiedNetObjectId; })[0].nodeId;
            var entityModified = _this.entityInspector.entities.filter(function (e) { return e.nodeID === childModifiedNodeId; })[0];
            var groupModified = entityModified.childGroups
                .filter(function (g) { return g.children.filter(function (c) { return c.netObjectId === childModifiedNetObjectId; }).length > 0; })[0];
            var groupChildIds = groupModified.children.map(function (c) { return c.netObjectId; });
            var selectedChildIds = newSelection.filter(function (id) { return groupChildIds.indexOf(id) >= 0; });
            groupModified.checked = (selectedChildIds.length === groupChildIds.length) && !!groupChildIds.length;
            groupModified.indeterminate = !groupModified.checked && !!selectedChildIds.length && !!groupChildIds.length;
        };
        /* Child actions */
        this.deleteChildren = function () {
            _this.deleteObjects(_this.entityInspector.selection, function () {
                _this.entityInspector.selection.length = 0;
                _this.updateRelatedEntites();
            });
        };
        /* End of Child actions */
        /* End of Entity Inspector / Sidebar section */
        this.populateAvailableValues = function (prop) {
            var loadedProperty = _.filter(_this.filterProperties, function (p) { return p.refId === prop.refId; })[0];
            if (loadedProperty) {
                prop.values = loadedProperty.values || [];
            }
            ;
            return prop;
        };
        this.filterFunctions = function () {
            return _this.swManageService.getAvailableFilters()
                .then(function (af) { return _.map(af, _this.populateAvailableValues); });
        };
        this.getSelectedNodes = function () {
            return _this.nodes.filter(function (node) {
                return _this.selection.items.some(function (item) {
                    return item === node.netObjectId;
                });
            });
        };
        this.selectionIDs = function () {
            return _this.getSelectedNodes().map(function (item) { return item.nodeID; });
        };
        this.isNodeAlertsSuppressed = function (node) {
            if (_this.nodeAlertsState && node && _this.nodeAlertsState.length) {
                for (var i = 0; i < _this.nodeAlertsState.length; i++) {
                    var alertState = _this.nodeAlertsState[i];
                    if (alertState.EntityUri === node.uri) {
                        return alertState.SuppressionMode;
                    }
                }
            }
        };
        this.switchToLegacy = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return;
            }
            if (_this.constants.environment.oipEnabled) {
                _this.webAdminService.getUserSetting(_this.prefix + "NewPageFeedback").then(function (feedback) {
                    var viewModel = {
                        feedback: ""
                    };
                    var dialogSettings = {
                        title: _this._t("Switch back to legacy page"),
                        hideCancel: false,
                        hideTopCancel: false,
                        actionButtonText: _this._t("Switch to legacy"),
                        cancelButtonText: _this._t("Cancel"),
                        viewModel: viewModel
                    };
                    _this.xuiDialogService.showModal({
                        size: "sm",
                        template: __webpack_require__(0)
                    }, dialogSettings).then(function (res) {
                        if (res && res !== "cancel") {
                            var timeStamp = (new Date()).toUTCString();
                            var fullFeedback = feedback + timeStamp + ":\n" + viewModel.feedback + "\n\n";
                            var redirect = _this.updateUserSetting("UseLegacyPage", true), feedbackNew = _this.updateUserSetting("NewPageFeedback", fullFeedback), dismiss = _this.onSwitchToLegacyDismiss();
                            _this.$q.all([redirect, feedbackNew, dismiss]).then(function () {
                                _this.$window.location.assign("/orion/nodes/default.aspx");
                            });
                        }
                    });
                });
            }
            else {
                _this.updateUserSetting("UseLegacyPage", true).then(function () {
                    _this.$window.location.assign("/orion/nodes/default.aspx");
                });
            }
        };
        this.onSwitchToLegacyDismiss = function () {
            _this.showWelcomeMessage = false;
            return _this.updateUserSetting("WelcomeMessageDismissed", true);
        };
        this.applyUserSettings = function () {
            var settingsResolve;
            if (_this.swDemoService.isDemoMode()) {
                settingsResolve = [
                    _this.resolveUserSettings(_this.prefix + "UserSettings"),
                    _this.$q.resolve()
                ];
            }
            else {
                settingsResolve = [
                    _this.webAdminService.getUserSetting(_this.prefix + "UserSettings"),
                    _this.entityPropsToDisplayService.readEntitySetting(["Orion.Nodes"]) //1
                ];
            }
            return _this.$q.all(settingsResolve).then(function (response) {
                var userSettings = angular.fromJson(response[0]) || {};
                _this.nodePropsToShow = (response[1] && response[1]["Orion.Nodes"]) ? response[1]["Orion.Nodes"] :
                    _this.nodePropsToShow;
                _this.paging.page = userSettings.actualPage || 1;
                _this.paging.pageSize = userSettings.pageSize || NodesController.defaultPageSize;
                //Page size for pagination in Filtered List, Filtered List overrides paging.pageSize value using this option
                _this.filteredListOptions.pageSize = _this.paging.pageSize;
                _this.sorting.direction = userSettings.sortDirection || _this.sorting.direction;
                _this.sorting.sortBy = _.filter(_this.sorting.sortableColumns, function (a) { return a.id === userSettings.sortProp; })[0]
                    || _this.sorting.sortableColumns[0];
                if (userSettings.filterByValues) {
                    _this.filterValues = userSettings.filterByValues;
                }
                //timeout for dom refresh
                _this.$timeout(function () {
                    _this.filterPropertyStates = userSettings.filtersState || {};
                }, 0);
            });
        };
        this.defaultDemoSettings = angular.toJson({
            "actualPage": 1,
            "filterByValues": {
                "Status": [],
                "Orion.Nodes|Vendor": [],
                "Orion.Nodes|MachineType": [],
                "Orion.Nodes|ObjectSubType": [],
                "Orion.Nodes|Engine.ServerName": []
            },
            "filtersState": {
                "Orion.Nodes|Engine.ServerName": {
                    "isOpen": true
                }, "Orion.Nodes|Vendor": {
                    "isOpen": true
                }, "Orion.Nodes|MachineType": {
                    "isOpen": true
                }, "Status": {
                    "isOpen": true
                }
            },
            "pageSize": 50,
            "sortDirection": "asc",
            "sortProp": "Caption",
            "sortPropType": "Node"
        });
        this.resolveUserSettings = function (name) { return _this.$q.resolve(_this.getSessionSettings(name) || _this.defaultDemoSettings); };
        this.getSessionSettings = function (name) { return _this.$window.sessionStorage.getItem(name); };
        this.prepareFilters = function (filters) {
            var tuneCount = function (filter, filterValue) {
                var appliedFilter = _this.filterValues[filter.refId];
                if (appliedFilter && appliedFilter.length) {
                    var appliedPropIndex = appliedFilter.indexOf(filterValue.id);
                    filterValue.count = appliedPropIndex === -1 ? "+" + filterValue.count : filterValue.count;
                }
            };
            var localisedTFValues = {
                "True": _this._t("True"),
                "False": _this._t("False")
            };
            var localisedObjectSubTypeValues = {
                "Agent": _this._t("Agent"),
                "ICMP": _this._t("ICMP"),
                "SNMP": _this._t("SNMP"),
                "WMI": _this._t("WMI"),
            };
            var refStringsForLocalization = ["Orion.Nodes|IsOrionServer", "Orion.Nodes|IsServer", "Orion.Nodes|Agent.IsActiveAgent",
                "Orion.Nodes|DynamicIP", "Orion.Nodes|Agent.AutoUpdateEnabled"];
            filters.forEach(function (filter) {
                filter.parent = filter.parent || "general";
                filter.parentTitle = filter.parentTitle || _this._t("General");
                if (filter.refId === "Status") {
                    filter.values.forEach(function (filterValue) {
                        //translation of status filter values
                        filterValue.title = _.capitalize(_this.swSwisDataMappingService.getStatusTranslated(filterValue.id));
                        //icon for the filter
                        filterValue.icon = "status_" + _this.swSwisDataMappingService.getStatus(filterValue.id);
                        // tuneCount(filter, filterValue); TODO: return it after counters will be supported again
                    });
                }
                else if (filter.refId === "Orion.Nodes|Vendor") {
                    filter.values.forEach(function (filterValue) {
                        filterValue.title = filterValue.id;
                        filterValue.templateUrl = _this.vendorFilterTemplateUrl;
                        filterValue.vendorIcon = filterValue.icon;
                        delete filterValue.icon;
                        // tuneCount(filter, filterValue); TODO: return it after counters will be supported again
                    });
                }
                else if (filter.refId === "Orion.Nodes|ObjectSubType") {
                    filter.values.forEach(function (filterValue) {
                        filterValue.title = localisedObjectSubTypeValues[filterValue.id] || filterValue.id;
                    });
                }
                else if (refStringsForLocalization.some(function (x) { return x === filter.refId; })) {
                    filter.values.forEach(function (filterValue) {
                        filterValue.title = localisedTFValues[filterValue.id] || filterValue.id;
                    });
                }
                else {
                    filter.values.forEach(function (filterValue) {
                        filterValue.title = filterValue.id;
                        // tuneCount(filter, filterValue); TODO: return it after counters will be supported again
                    });
                }
            });
            return filters;
        };
        this.updateUserSetting = function (name, value) {
            return _this.swDemoService.isDemoMode()
                ? _this.$q.resolve()
                    .then(function () {
                    _this.$window.sessionStorage.setItem(_this.prefix + name, value);
                    return true;
                })
                : _this.webAdminService.saveUserSetting(_this.prefix + name, value);
        };
        this.newGuid = function () {
            function _p8(s) {
                var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
        };
        // We have to post HTML form, because there is no other way to make legacy page get params
        // http://stackoverflow.com/questions/8389646/send-post-data-on-redirect-with-javascript-jquery?rq=1
        this.prepareForm = function (netObjectType, ids, returnUrl) {
            var idsString = ids.join(",");
            var actionUrl = (netObjectType === "Interface")
                ? "/Orion/Interfaces/InterfaceProperties.aspx"
                : "/Orion/Nodes/NodeProperties.aspx";
            var guid = _this.newGuid();
            var form = "<form name=\"selectedNetObjects\" action=\"" + actionUrl + "\" method=\"POST\">\n                        <input type=\"hidden\" name=\"" + netObjectType + "s\" value=\"" + idsString + "\"/>\n                        <input type=\"hidden\" name=\"ReturnTo\" value=\"" + returnUrl + "\"/>\n                        <input type=\"hidden\" name=\"GuidID\" value=\"" + guid + "\"/>\n                    </form>";
            angular.element(document.body).append(form);
        };
        this.updateEmptyText = function () {
            if (_this.searching && _this.searching.trim().length) {
                _this.filteredListOptions.emptyText = _this.emptyText + " \"" + _this.searching + "\"";
            }
        };
        this.updateAlertsSuppressionState = function () {
            var uris = _.map(_this.nodes, function (node) { return node.uri; });
            if (uris && uris.length) {
                _this.swAlertsManagementService
                    .getAlertSuppressionState(uris)
                    .then(function (response) {
                    _this.nodeAlertsState = response;
                });
            }
            else {
                _this.nodeAlertsState = [];
            }
        };
    }
    NodesController.prototype.createScheduleDialog = function () {
        var _this = this;
        var dialogHeader = this._t("Scheduled Maintenance Of"), nodesStr = this._t("Nodes");
        if (this.selection.items.length === 1) {
            var selectedNodeCaption = this.getSelectedNodes()[0].caption;
            dialogHeader = dialogHeader + " " + selectedNodeCaption;
        }
        else {
            dialogHeader = dialogHeader + " " + this.selection.items.length + " " + nodesStr;
        }
        var viewModel = {
            timeFrom: moment().toDate(),
            timeUntill: moment().add(1, "days").toDate(),
            dialogStatus: "mute"
        };
        var dialogSettings = {
            title: dialogHeader,
            hideCancel: false,
            hideTopCancel: false,
            actionButtonText: this._t("Schedule"),
            cancelButtonText: this._t("Cancel"),
            viewModel: viewModel
        };
        var result = this.xuiDialogService.showModal({
            size: "lg",
            template: __webpack_require__(1),
        }, dialogSettings);
        result.then(function (res) {
            if (res && res !== "cancel") {
                switch (viewModel.dialogStatus) {
                    case "unmanage":
                        _this.unmanageNodes(viewModel.timeFrom, viewModel.timeUntill);
                        break;
                    case "mute":
                        _this.suppressAlerts(viewModel.timeFrom, viewModel.timeUntill);
                        break;
                }
            }
        });
    };
    NodesController.$inject = [
        "$scope",
        "getTextService",
        "swApi",
        "swManageService",
        "webAdminService",
        "xuiToastService",
        "swSwisDataMappingService",
        "$timeout",
        "xuiDialogService",
        "$q",
        "$window",
        "swAlertsManagementService",
        "constants",
        "swDisplayedEntityRowPropertiesService",
        "swDemoService"
    ];
    NodesController.defaultPageSize = 50;
    return NodesController;
}());
exports.NodesController = NodesController;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider) {
    var manageNodesState = "ManageNodes";
    $stateProvider
        .state(manageNodesState, {
        i18Title: "_t(Manage Nodes)",
        url: "/manage/nodes",
        controller: "NodesController",
        controllerAs: "$nodesCtrl",
        template: __webpack_require__(2),
        resolve: {
            ifUseLegacyPage: ["webAdminService", "$window", "$q",
                function (webAdminService, $window, $q) {
                    var deferred = $q.defer();
                    webAdminService.getUserSetting("WebNodeManagement_UseLegacyPage")
                        .then(function (settingValue) {
                        if (settingValue === "True") {
                            $window.location.assign("/orion/nodes/default.aspx");
                            deferred.reject("Router should stop while redirecting to legacy page...");
                        }
                        else {
                            deferred.resolve();
                        }
                    });
                    return deferred.promise;
                }]
        }
    });
}
exports.config = config;


/***/ }),
/* 15 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var editRowProperties_controller_1 = __webpack_require__(17);
var editRowProperties_config_1 = __webpack_require__(18);
__webpack_require__(19);
exports.default = function (module) {
    module.controller("EditRowPropsController", editRowProperties_controller_1.EditRowPropsController);
    module.app().config(editRowProperties_config_1.config);
};


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var EditRowPropsController = /** @class */ (function () {
    function EditRowPropsController($window, $q, swManageService, webAdminService, _t, nodePropsToShow, nodeProps, entityPropsToDisplayService, swDemoService) {
        var _this = this;
        this.$window = $window;
        this.$q = $q;
        this.swManageService = swManageService;
        this.webAdminService = webAdminService;
        this._t = _t;
        this.nodePropsToShow = nodePropsToShow;
        this.nodeProps = nodeProps;
        this.entityPropsToDisplayService = entityPropsToDisplayService;
        this.swDemoService = swDemoService;
        this.$onInit = function () {
            _this.showOverlay = true;
            _this.listOfProps = [
                _this._t("Custom Properties"),
                _this._t("System Properties")
            ];
            _this.propsToShow = _this.listOfProps[0];
            if (!_this.nodeProps) {
                _this.nodeProps = {
                    customProps: [],
                    systemProps: []
                };
            }
            if (!_this.nodePropsToShow) {
                _this.nodePropsToShow = {
                    customProps: [],
                    systemProps: []
                };
            }
        };
        this.saveChanges = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            else {
                return _this.entityPropsToDisplayService.writeEntitySetting("Orion.Nodes", _this.nodePropsToShow).then(function () {
                    return _this.$window.location.assign(EditRowPropsController.parent);
                });
            }
        };
        this.cancel = function () {
            _this.$window.location.assign(EditRowPropsController.parent);
        };
        this.getSystemPropLabel = function (navigationName) {
            if (navigationName) {
                for (var i = 0; i < _this.nodeProps.systemProps.length; i++) {
                    var nodeGroup = _this.nodeProps.systemProps[i];
                    var nodeProp = nodeGroup.nodeProperties
                        .filter(function (prop) { return prop.navigationName === navigationName; });
                    if (nodeProp[0]) {
                        return nodeProp[0].displayName;
                    }
                }
            }
            else {
                return "";
            }
        };
        this.getCustomPropLabel = function (navigationName) {
            if (navigationName) {
                var nodeProp = _this.nodeProps.customProps
                    .filter(function (prop) { return prop.navigationName === navigationName; });
                if (nodeProp[0]) {
                    return nodeProp[0].displayName;
                }
            }
            else {
                return "";
            }
        };
    }
    EditRowPropsController.$inject = [
        "$window",
        "$q",
        "swManageService",
        "webAdminService",
        "getTextService",
        "nodePropsToShow",
        "nodeProps",
        "swDisplayedEntityRowPropertiesService",
        "swDemoService"
    ];
    EditRowPropsController.parent = "/ui/manage/nodes";
    return EditRowPropsController;
}());
exports.EditRowPropsController = EditRowPropsController;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider) {
    var editRowPropsState = "EditRowProps";
    $stateProvider
        .state(editRowPropsState, {
        i18Title: "_t(Edit Row Properties & Layout)",
        url: "/manage/nodes/edit-row",
        controller: "EditRowPropsController",
        controllerAs: "$editRowProps",
        template: __webpack_require__(3),
        resolve: {
            nodePropsToShow: ["swDisplayedEntityRowPropertiesService",
                function (rowPropertyService) {
                    return rowPropertyService.readEntitySetting(["Orion.Nodes"]).then(function (entityPropsToShow) {
                        return entityPropsToShow["Orion.Nodes"];
                    });
                }],
            nodeProps: ["$q", "swManageService", function ($q, swManageService) {
                    return $q.all([swManageService.getNodeCustomProps(), swManageService.getNodeSystemProps()])
                        .then(function (response) {
                        return {
                            customProps: response[0],
                            systemProps: response[1]
                        };
                    });
                }]
        }
    });
}
exports.config = config;


/***/ }),
/* 19 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    // register components
};


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var manage_service_1 = __webpack_require__(22);
var swDisplayedEntityRowPropertiesService_1 = __webpack_require__(23);
exports.default = function (module) {
    // register services
    module.service("swManageService", manage_service_1.ManageService);
    module.service("swDisplayedEntityRowPropertiesService", swDisplayedEntityRowPropertiesService_1.default);
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ManageService = /** @class */ (function () {
    function ManageService(swApi, _t, webAdminService, $q, $httpParamSerializerJQLike) {
        var _this = this;
        this.swApi = swApi;
        this._t = _t;
        this.webAdminService = webAdminService;
        this.$q = $q;
        this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
        this.filterValues = {};
        this.defaultNodeProps = [
            "NodeID",
            "Caption",
            "Description",
            "DetailsUrl",
            "Dns",
            "EngineID",
            "Icon",
            "IpAddress",
            "IpAddressType",
            "InstanceType",
            "MachineType",
            "ObjectSubType",
            "OrionIDPrefix",
            "ServerName",
            "Status",
            "ChildStatus",
            "SysName",
            "Vendor",
            "VendorIcon",
            "Uri"
        ];
        this.entityToSearch = ["Orion.Nodes"];
        this.propertyToSearch = [
            "Caption",
            "IpAddress"
        ];
        this.generalFilter = {
            parent: "general",
            parentTitle: this._t("General")
        };
        this.nodeGeneralProps = {
            name: "generalProperties",
            displayName: this.generalFilter.parentTitle,
            nodeProperties: [{
                    "name": "Contact",
                    "navigationName": "Contact",
                    "displayName": this._t("Contact"),
                    "propertyType": "System.String"
                }, {
                    "name": "CustomStatus",
                    "navigationName": "CustomStatus",
                    "displayName": this._t("Custom Status"),
                    "propertyType": "System.String"
                }, {
                    "name": "IPAddressType",
                    "navigationName": "IpAddressType",
                    "displayName": this._t("IP Version"),
                    "propertyType": "System.String"
                }, {
                    "name": "Location",
                    "navigationName": "Location",
                    "displayName": this._t("Location"),
                    "propertyType": "System.String"
                }, {
                    "name": "Category",
                    "navigationName": "Category",
                    "displayName": this._t("Node Category"),
                    "propertyType": "System.String"
                }, {
                    "name": "UnManageFrom",
                    "navigationName": "UnManageFrom",
                    "displayName": this._t("Planned Outage"),
                    "propertyType": "System.String"
                }, {
                    "name": "ObjectSubType",
                    "navigationName": "ObjectSubType",
                    "displayName": this._t("Polling Method"),
                    "propertyType": "System.String"
                }, {
                    "name": "ServerName",
                    "navigationName": "Engine.ServerName",
                    "displayName": this._t("Server Name"),
                    "propertyType": "System.String"
                }]
        };
        this.customPOST = function (api, data) {
            return api.customPOST(_this.$httpParamSerializerJQLike(data), undefined, {}, { "Content-Type": "application/x-www-form-urlencoded" });
        };
        this.decorateNode = function (node) {
            // these properties should be present, otherwise repeater breaks - log this at the source to help investigation
            _.each(["orionIDPrefix", "nodeID", "engineID"], function (key) {
                if (!node.hasOwnProperty(key)) {
                    console.error(key + " missing on node data");
                }
            });
            node.netObjectId = "" + node.orionIDPrefix + node.nodeID + ":" + node.engineID;
            return node;
        };
        this.decorateGetNodesResponse = function (result) {
            result.items = _.map(result.items, _this.decorateNode);
            return result;
        };
        this.getNodes = function (propertiesToShow, search, filters, pagination, sorting) {
            return _this.customPOST(_this.searchApi, {
                q: search || "",
                entityToSearch: _this.entityToSearch,
                propertyToSearch: _this.propertyToSearch,
                propertyToSelect: _this.propertiesToSelect(propertiesToShow),
                skip: ((pagination.page - 1) * pagination.pageSize),
                top: pagination.pageSize,
                orderBy: _this.getOrderBy(sorting),
                filterBy: _this.getSearchFilters(filters)
            }).then(_this.decorateGetNodesResponse);
        };
        this.getFiltersPropsAndValues = function (search, filters) {
            return _this.customPOST(_this.filterApi, {
                q: search || "",
                filterBy: _this.getSearchFilters(filters),
                filters: _.keys(filters)
            }).then(function (res) { return _.filter(res.plain(), _this.isValidFilter); });
        };
        this.decorateFilter = function (filter) {
            if (!filter.parent) {
                filter.parent = _this.generalFilter.parent;
                filter.parentTitle = _this.generalFilter.parentTitle;
            }
            return filter;
        };
        this.getAvailableFilters = function () {
            return _this.availableFilters
                ? _this.$q.resolve(_this.availableFilters)
                : _this.availableFiltersApi.getList()
                    .then(function (res) { return res.plain(); })
                    .then(function (availableFilters) { return _.filter(availableFilters, _this.isValidFilter); })
                    .then(function (availableFilters) { return _.map(availableFilters, _this.decorateFilter); })
                    .then(function (availableFilters) { return _this.availableFilters = availableFilters; });
        };
        this.getNodeCustomProps = function () {
            return _this.nodesApi.all("customProps").getList()
                .then(function (res) { return res.plain(); });
        };
        this.getNodeSystemProps = function () {
            return _this.nodesApi.all("inheritedProps").getList()
                .then(function (res) { return [_this.nodeGeneralProps].concat(res.plain()); });
        };
        /**
         * @method getSearchFilters
         * @description Converts list of selected by user filters that filtered list returns into string, that basic search
         * api can process and use for node filtering
         * @param {xui.FilteredList.IFilterValues} filterValues selected by user filters that filtered list returns
         * @returns {string}
         */
        this.getSearchFilters = function (filterValues) {
            return _.concat(_.map(_this.entityToSearch, function (e) { return "InstanceType:" + e; }), _.filter(_.map(filterValues, function (values, filterId) {
                return values.length
                    ? filterId + ":" + _.join(values, "|")
                    : null;
            })));
        };
        this.formatNodeIds = function (nodeIds) {
            return "NodeID:" + _.join(nodeIds, "|");
        };
        this.getNodeChildren = function (entityToSearch, propertyToSelect, nodeIds, searchTerm) {
            return _this.searchApi.get({
                q: searchTerm || "",
                entityToSearch: entityToSearch,
                propertyToSearch: ["Caption"],
                propertyToSelect: propertyToSelect,
                skip: 0,
                top: 1000000,
                filterBy: _this.formatNodeIds(nodeIds)
            });
        };
        this.isValidFilter = function (filter) {
            var filterBlackList = ["InstanceType"];
            return (!filter.parent || _.includes(_this.entityToSearch, filter.parent))
                && !_.includes(filterBlackList, filter.refId);
        };
        this.isPropSpecial = function (name) {
            return name.indexOf(".") !== -1;
        };
        this.getPropsToShow = function (props) {
            return _.concat(props.systemProps && _.filter(props.systemProps, _this.isPropSpecial) || [], props.customProps || []);
        };
        /**
         * @method propertiesToSelect
         * @description Prepares list of properties that basic search api should select for every node. Combines custom,
         * general and inherited node properties
         * @param {INodePropsToShow} propertiesToShow list of node properties that user would like to see on node template
         * @returns {string[]}
         */
        this.propertiesToSelect = function (propertiesToShow) {
            return propertiesToShow
                ? _.uniq(_.concat(_this.defaultNodeProps, _this.getPropsToShow(propertiesToShow)))
                : _this.defaultNodeProps;
        };
        this.rxSortByIdPrefix = /^Orion.Nodes\|(\w+)$/;
        this.getSortById = function (id) {
            return _this.rxSortByIdPrefix.test(id)
                ? _this.rxSortByIdPrefix.exec(id)[1]
                : id;
        };
        this.getSortingDirection = function (direction) {
            return (direction.toLowerCase() === "asc") ? "+" : "-";
        };
        this.formatOrderBy = function (sorting) {
            return "" + _this.getSortingDirection(sorting.direction) + _this.getSortById(sorting.sortBy.id);
        };
        /**
         * @method getOrderBy
         * @description returns orderBy parameter value formatted in the way that basic search api will be able to parse
         * @param sorting {xui.IGridSorting} sorting from filtered-list
         * @returns {String}
         */
        this.getOrderBy = function (sorting) {
            return sorting ? _this.formatOrderBy(sorting) : "";
        };
        this.nodesApi = this.swApi.api(false).one("platform/nodes");
        this.searchApi = this.swApi.api(false).one("search/entities");
        this.filterApi = this.swApi.api(false).one("search/indexed-filters");
        this.availableFiltersApi = this.swApi.api(false).one("search/metadata/available-filters");
    }
    ManageService.$inject = ["swApi", "getTextService", "webAdminService", "$q", "$httpParamSerializerJQLike"];
    return ManageService;
}());
exports.ManageService = ManageService;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SwDisplayedEntityRowPropertiesService = /** @class */ (function () {
    function SwDisplayedEntityRowPropertiesService($log, webAdminService) {
        var _this = this;
        this.$log = $log;
        this.webAdminService = webAdminService;
        this.key = "swOrionDisplayedEntityRowProperties";
        this.parseRawSetting = function (rawData) {
            try {
                return rawData && angular.fromJson(rawData) || {};
            }
            catch (parsingError) {
                _this.$log.error("Failed to parse displayedEntityPropertySettings", parsingError);
                return {};
            }
        };
        /**
         *  @ngdoc method
         *  @name readEntitySetting
         *  @methodOf orion.services.swDisplayedEntityRowPropertiesService
         *  @description Reads the row display settings of the specified entity types
         *  @param {EntityTypes[] = null} entityTypes The required entity types.
         *  @returns {ng.IPromise<IBundledEntityPropsToShow>} returns the requested entity settings or all the entity
         *  settings if no EntityTypes is specified.
         **/
        this.readEntitySetting = function (entityTypes) {
            if (entityTypes === void 0) { entityTypes = null; }
            return _this.webAdminService
                .getUserSetting(_this.key)
                .then(_this.parseRawSetting)
                .then(function (allSettings) {
                return _.reduce(allSettings, function (selected, value, key) {
                    if (!entityTypes || _.includes(entityTypes, key)) {
                        selected[key] = value;
                    }
                    return selected;
                }, {});
            });
        };
        /**
         *  @ngdoc method
         *  @name writeEntitySetting
         *  @methodOf orion.services.swDisplayedEntityRowPropertiesService
         *  @description Write the display settings of the specified entity type
         *  @param {EntityTypes} entityType The entity type to overwritten.
         *  @param {IEntityPropsToShow} entitySetting The entity type to overwritten.
         *  @returns {ng.IPromise<boolean>} returns if the writing was successfull or not
         **/
        this.writeEntitySetting = function (type, value) {
            return _this.readEntitySetting()
                .then(function (entitySettings) {
                entitySettings[type] = value;
                return _this.webAdminService.saveUserSetting(_this.key, JSON.stringify(entitySettings));
            });
        };
    }
    SwDisplayedEntityRowPropertiesService.$inject = ["$log", "webAdminService"];
    return SwDisplayedEntityRowPropertiesService;
}());
exports.default = SwDisplayedEntityRowPropertiesService;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(25);
    req.keys().forEach(function (r) {
        var key = "entityManagement" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./views/editRowProperties/editRowProperties.html": 3,
	"./views/manage/nodes/childItem.html": 26,
	"./views/manage/nodes/dialogScheduleMaintenance.html": 1,
	"./views/manage/nodes/dialogSwitchToLegacy.html": 0,
	"./views/manage/nodes/nodeItem.html": 27,
	"./views/manage/nodes/nodes.html": 2,
	"./views/manage/nodes/nodesVendorFilter.html": 28
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 25;

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<div class=\"child-item ellipsis\"> <xui-icon icon=\"{{::item.icon || 'unknownnode'}}\" status=\"{{::item.status | swStatus}}\"></xui-icon> <a href={{::item.detailsUrl}}> <span class=child-item__caption ng-bind-html=\"item.name | xuiHighlight: vm.entityInspector.search\"></span> </a> </div> ";

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = "<div class=node-item> <div class=node-item__container> <sw-entity-list-item entity=item system-props=vm.nodePropsToShow.systemProps custom-props=vm.nodePropsToShow.customProps search-term=vm.searching> </sw-entity-list-item> </div> <div class=node-item__buttons> <div class=node-item__icon-mute> <xui-button icon=mute tool-tip=\"_t(Resume Alerts)\" _ta display-style=tertiary ng-click=vm.resumeAlerts(item) ng-if=vm.isNodeAlertsSuppressed(item)></xui-button> </div> <div class=node-item__btn-more ng-if=vm.features.NPM> <xui-button ng-click=vm.addToInspector(item) display-style=tertiary icon=caret-right is-empty=true ng-hide=vm.isInspected(item.nodeID)> </xui-button> <xui-button ng-click=vm.removeFromInspector(item) display-style=tertiary icon=close is-empty=true ng-show=vm.isInspected(item.nodeID)> </xui-button> </div> </div> </div> ";

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = "<div class=nodes-vendor-filter> <sw-vendor-icon class=nodes-vendor-filter__icon vendor={{::item.vendorIcon}}></sw-vendor-icon> <span class=nodes-vendor-filter__title ng-bind=::item.title></span> </div> ";

/***/ })
/******/ ]);
//# sourceMappingURL=manage.js.map