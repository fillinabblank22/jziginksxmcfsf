/*!
 * @solarwinds/netman-cisco-frontend 1.5.0-295
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=$ctrl.filteredListConfiguration> <netman-cisco-apic-members-list configuration=$ctrl.filteredListConfiguration></netman-cisco-apic-members-list> </div> ";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
  * APIC member type enum
  *
  * @enum
  */
var ApicMemberType;
(function (ApicMemberType) {
    ApicMemberType[ApicMemberType["Unknown"] = 0] = "Unknown";
    ApicMemberType[ApicMemberType["Tenant"] = 1] = "Tenant";
    ApicMemberType[ApicMemberType["ApplicationProfile"] = 2] = "ApplicationProfile";
    ApicMemberType[ApicMemberType["PhysicalEntitySpine"] = 3] = "PhysicalEntitySpine";
    ApicMemberType[ApicMemberType["PhysicalEntityLeaf"] = 4] = "PhysicalEntityLeaf";
    ApicMemberType[ApicMemberType["EndpointGroup"] = 5] = "EndpointGroup";
})(ApicMemberType = exports.ApicMemberType || (exports.ApicMemberType = {}));


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(4);
module.exports = __webpack_require__(5);


/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(6);
var config_1 = __webpack_require__(7);
var index_1 = __webpack_require__(8);
var index_2 = __webpack_require__(10);
var index_3 = __webpack_require__(14);
var index_4 = __webpack_require__(18);
var templates_1 = __webpack_require__(22);
var index_5 = __webpack_require__(26);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);
index_5.default(app_1.default);


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("netman-cisco.services", []);
angular.module("netman-cisco.templates", []);
angular.module("netman-cisco.components", []);
angular.module("netman-cisco.filters", []);
angular.module("netman-cisco.providers", []);
angular.module("netman-cisco", [
    "orion",
    "netman-cisco.services",
    "netman-cisco.templates",
    "netman-cisco.components",
    "netman-cisco.filters",
    "netman-cisco.providers"
]);
// create and register Xui (Orion) module wrapper
var netManCisco = Xui.registerModule("netman-cisco");
exports.default = netManCisco;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: netman-cisco");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = (function (module) {
    module.app()
        .run(run);
});
exports.default.$inject = ["module"];


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(9);
exports.default = (function (module) {
    module.service("netman-ciscoConstants", constants_1.default);
});


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(11);
exports.default = (function (module) {
    // register views
    index_1.default(module);
});
var rootState = function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state("netman-cisco", {
        url: "/netman-cisco",
        controller: function ($state) {
            $state.go("home");
        }
    });
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var apicMembersList_config_1 = __webpack_require__(12);
var apicMembersListView_controller_1 = __webpack_require__(13);
exports.default = (function (module) {
    module.config(apicMembersList_config_1.default);
    module.controller("ApicMembersListViewController", apicMembersListView_controller_1.default);
});


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state("apicMembersList", {
        parent: "subview",
        i18Title: "_t(APIC Members)",
        url: "/netman-cisco/apic-members-list",
        template: __webpack_require__(0),
        controller: "ApicMembersListViewController",
        controllerAs: "$ctrl"
    });
}
;
exports.default = config;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ApicMembersListViewController = /** @class */ (function () {
    /** @ngInject */
    ApicMembersListViewController.$inject = ["apicMembersListService"];
    function ApicMembersListViewController(apicMembersListService) {
        this.apicMembersListService = apicMembersListService;
    }
    ApicMembersListViewController.prototype.$onInit = function () {
        var _this = this;
        this.apicMembersListService.getFilteredListConfiguration()
            .then(function (configuration) {
            _this.filteredListConfiguration = configuration;
        });
    };
    return ApicMembersListViewController;
}());
exports.default = ApicMembersListViewController;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(15);
exports.default = (function (module) {
    // register components
    index_1.default(module);
});


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var apicMembersList_directive_1 = __webpack_require__(16);
var apicMembersList_controller_1 = __webpack_require__(17);
__webpack_require__(1);
exports.default = (function (module) {
    module.controller("ApicMembersListController", apicMembersList_controller_1.default);
    module.component("netmanCiscoApicMembersList", apicMembersList_directive_1.ApicMembersListDirective);
});


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(1);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ApicMembersListDirective = /** @class */ (function () {
    function ApicMembersListDirective() {
        this.restrict = "E";
        this.templateUrl = "netman-cisco/components/apicMembersList/apicMembersList-directive.html";
        this.replace = true;
        this.scope = {};
        this.controller = "ApicMembersListController";
        this.controllerAs = "$apicMembersListCtrl";
        this.bindToController = { configuration: "<" };
    }
    return ApicMembersListDirective;
}());
exports.ApicMembersListDirective = ApicMembersListDirective;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var filteringApiGetDataUrlFragment = "/filtering/apic-members/data";
var ApicMembersListController = /** @class */ (function () {
    /** @ngInject */
    ApicMembersListController.$inject = ["xuiFilteredListService", "swApi", "filteredListConfigurationService", "apicMembersListService"];
    function ApicMembersListController(xuiFilteredListService, swApi, filteredListConfigurationService, apicMembersListService) {
        this.xuiFilteredListService = xuiFilteredListService;
        this.swApi = swApi;
        this.filteredListConfigurationService = filteredListConfigurationService;
        this.apicMembersListService = apicMembersListService;
    }
    ApicMembersListController.prototype.$onInit = function () {
        var _this = this;
        this.nodesState = {
            options: {
                templateUrl: "apic-member-item-template",
                rowPadding: "narrow",
                hideSearch: false
            },
            pagination: {
                page: 1,
                pageSize: 10
            }
        };
        this.filteredListConfigurationService.configureFilteredListState(this.configuration, this.nodesState);
        var nodesStateModel = this.xuiFilteredListService.getModel(this, "nodesState");
        var dataSource = this.xuiFilteredListService.getDataSource(filteringApiGetDataUrlFragment, this.swApi.api(false), nodesStateModel, function (params) { return _this.filteredListConfigurationService.createCustomQueryParams(params); });
        // because filtering backend does not return string representation of enums for type property of filter item
        // (which causes filter items not to display), we have to fix that by deleting 'type' property from filter item objects
        var originalGetData = dataSource.getData.bind(dataSource);
        dataSource.getData = function (params) {
            return originalGetData(params)
                .then(_this.filteredListConfigurationService.deleteTypePropertyFromFilterItems);
        };
        this.nodesDispatcher = this.xuiFilteredListService.getDispatcherInstance(nodesStateModel, { dataSource: dataSource });
        this.apicMembersListService.getRedundancyWarningMessage()
            .then(function (result) {
            if (result) {
                _this.ipAddressForWarningMessage = result.ipAddresses;
                _this.kbLinkForWarningMessage = result.kbTroubleshootLink;
            }
        });
        this.apicMembersListService.getApicPollingState()
            .then(function (result) {
            if (result) {
                _this.pollingState = result;
            }
        });
    };
    ApicMembersListController.prototype.getCssClass = function (status) {
        switch (status) {
            case 14:
                return "apic-member-critical-status";
            case 3:
                return "apic-member-warning-status";
            case 1:
                return "apic-member-up-status";
            default:
                return "";
        }
    };
    return ApicMembersListController;
}());
exports.default = ApicMembersListController;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var netManView_service_1 = __webpack_require__(19);
var apicMembersList_service_1 = __webpack_require__(20);
var filteredListConfiguration_service_1 = __webpack_require__(21);
exports.default = (function (module) {
    // register services
    module.service("netManViewService", netManView_service_1.default);
    module.service("apicMembersListService", apicMembersList_service_1.default);
    module.service("filteredListConfigurationService", filteredListConfiguration_service_1.default);
});


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
//Temporary solution until the final nature of the static subviews is known
var NetManViewService = /** @class */ (function () {
    /** @ngInject */
    NetManViewService.$inject = ["$log", "$location"];
    function NetManViewService($log, $location) {
        this.$log = $log;
        this.$location = $location;
        this.netObject = null;
    }
    NetManViewService.prototype.getNetObject = function () {
        try {
            if (this.netObject) {
                return this.netObject;
            }
            var netObjectQueryString = this.$location.search().NetObject;
            var nodeId = parseInt((decodeURIComponent(netObjectQueryString)
                .match(/N\:([0-9]+)/)
                ? RegExp.$1
                : "0"), 10);
            this.netObject = {
                nodeId: nodeId
            };
            return this.netObject;
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    return NetManViewService;
}());
exports.default = NetManViewService;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var swApiGetWarningMessageUrlFragment = "netman-cisco/apic/nodes/{nodeId}/warningMessageData";
var swApiGetApicPollingStatusUrlFragment = "netman-cisco/apic/nodes/{nodeId}/apicPollingState";
var filteringApiGetConfigurationUrlFragment = "/filtering/apic-members/configuration";
var ApicMembersListService = /** @class */ (function () {
    /** @ngInject */
    ApicMembersListService.$inject = ["swApi", "netManViewService"];
    function ApicMembersListService(swApi, netManViewService) {
        this.swApi = swApi;
        this.netManViewService = netManViewService;
    }
    ApicMembersListService.prototype.getFilteredListConfiguration = function () {
        return this.swApi.api(false)
            .one(filteringApiGetConfigurationUrlFragment)
            .get();
    };
    ApicMembersListService.prototype.getRedundancyWarningMessage = function () {
        return this.getWarningMessageInformation(swApiGetWarningMessageUrlFragment);
    };
    /**
     * Gets APIC polling state from WebApi.
     * @returns Polling state result
     */
    ApicMembersListService.prototype.getApicPollingState = function () {
        return this.getWarningMessageInformation(swApiGetApicPollingStatusUrlFragment);
    };
    ApicMembersListService.prototype.getWarningMessageInformation = function (urlFragment) {
        var netObject = this.netManViewService.getNetObject();
        var nodeId = netObject.nodeId;
        var route = urlFragment.replace("{nodeId}", nodeId.toString());
        var legacyApi = false;
        return this.swApi
            .api(legacyApi)
            .one(route)
            .get();
    };
    return ApicMembersListService;
}());
exports.default = ApicMembersListService;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilteredListConfigurationService = /** @class */ (function () {
    // /** @ngInject */
    FilteredListConfigurationService.$inject = ["netManViewService"];
    function FilteredListConfigurationService(netManViewService) {
        this.netManViewService = netManViewService;
    }
    // this method is implemented as public in FilteredListService, but it's not exposed in interface
    FilteredListConfigurationService.prototype.configureFilteredListState = function (configuration, state) {
        if (configuration.filters) {
            state.filters = {
                filterProperties: configuration.filters
            };
        }
        if (configuration.list) {
            var sorter_1 = configuration.list.sorter;
            if (sorter_1 && sorter_1.defaultSort && sorter_1.sortableColumns) {
                var sortBy = _.find(sorter_1.sortableColumns, function (x) { return x.id === sorter_1.defaultSort.sortBy; });
                state.sorting = {
                    sortBy: sortBy,
                    direction: sorter_1.defaultSort.sortDirection,
                    sortableColumns: sorter_1.sortableColumns
                };
            }
        }
    };
    FilteredListConfigurationService.prototype.createCustomQueryParams = function (params) {
        var netObject = this.netManViewService.getNetObject();
        var nodeId = netObject.nodeId;
        if (!params.custom) {
            params.custom = {};
        }
        params.custom["nodeId"] = nodeId;
        return params;
    };
    FilteredListConfigurationService.prototype.deleteTypePropertyFromFilterItems = function (conf) {
        conf.filters.forEach(function (filter) {
            filter.items.forEach(function (filterItem) {
                delete filterItem.type;
            });
        });
        return conf;
    };
    return FilteredListConfigurationService;
}());
exports.default = FilteredListConfigurationService;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(23);
    req.keys().forEach(function (r) {
        var key = "netman-cisco" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = (function (module) {
    module.app().run(templates);
});


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/apicMembersList/apicMembersList-directive.html": 24,
	"./components/apicMembersList/example/apicMembersList.html": 25,
	"./views/apicMembersList/apicMembersList.html": 0
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 23;

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = "<div class=apic-members-list> <xui-message class=apic-members-list-warning-message ng-if=\"$apicMembersListCtrl.pollingState.pollingSuccess === false\" id=warningMessagePollingFail type=warning allow-dismiss=true _t> Cisco ACI polling <b>failed</b>. <span ng-if=$apicMembersListCtrl.pollingState.kbTroubleshootLink><a ng-href={{::$apicMembersListCtrl.pollingState.kbTroubleshootLink}} target=_blank _t>» Learn More</a></span> </xui-message> <xui-message class=apic-members-list-warning-message ng-if=$apicMembersListCtrl.ipAddressForWarningMessage id=warningMessage type=warning allow-dismiss=true> <span _t> Information about this ACI system is polled redundantly from multiple APIC nodes (<b>{{::$apicMembersListCtrl.ipAddressForWarningMessage}}</b>). SolarWinds recommends disabling ACI polling on all but one APIC. </span> <a ng-href={{::$apicMembersListCtrl.kbLinkForWarningMessage}} target=_blank rel=\"noopener noreferrer\">» <span _t>Learn More</span> </a> </xui-message> <xui-filtered-list-v2 state=$apicMembersListCtrl.nodesState dispatcher=$apicMembersListCtrl.nodesDispatcher controller=$apicMembersListCtrl> </xui-filtered-list-v2> <script type=text/ng-template id=apic-member-item-template> <div class=\"row apic-members-list-item\">\n            <div class=\"col-md-2\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon class=\"apic-member-status-icon\"\n                            icon=\"{{item.type | apicMemberTypeToIconFilter}}\" status=\"{{ item.status | swStatus }}\">\n                        </xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span class=\"apic-member-name\">{{::item.name}}</span>\n                        <xui-help-hint class=\"apic-member-type\">{{::item.type | apicMemberTypeToStringFilter}}</xui-help-hint>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4 apic-member-dn\">\n                {{::item.distinguishedName}}\n            </div>\n            <div class=\"col-md-6\">\n                <a ng-href={{::item.perfstackUrl}} target=\"_self\">\n                    <div class=\"apic-member-status-indicator {{vm.getCssClass(item.status)}} pull-right\" ng-if=\"item.healthScore <= 100\">{{::item.healthScore}}</div>\n                </a>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title=\"List of APIC members - Test Example\" page-layout=form> <div ng-controller=\"testCtrl as vm\"> <netman-cisco-apic-members-list configuration=vm.filteredListConfiguration></netman-cisco-apic-members-list> </div> </xui-page-content>";

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var apicMemberTypeToIcon_filter_1 = __webpack_require__(27);
var apicMemberTypeToString_filter_1 = __webpack_require__(28);
exports.default = (function (module) {
    // register filters
    module.filter("apicMemberTypeToIconFilter", apicMemberTypeToIcon_filter_1.default);
    module.filter("apicMemberTypeToStringFilter", apicMemberTypeToString_filter_1.default);
});


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var apicMemberType_1 = __webpack_require__(2);
/**
 * Filter that transforms APIC member type to corresponding xui icon name.
 */
function apicMemberTypeToIconFilter() {
    return function (apicMemberType) {
        switch (apicMemberType) {
            case apicMemberType_1.ApicMemberType.Tenant:
                return "unknownnode";
            case apicMemberType_1.ApicMemberType.ApplicationProfile:
                return "application-profile";
            case apicMemberType_1.ApicMemberType.PhysicalEntitySpine:
                return "spine";
            case apicMemberType_1.ApicMemberType.PhysicalEntityLeaf:
                return "leaf";
            case apicMemberType_1.ApicMemberType.EndpointGroup:
                return "virtual-cluster";
            default:
                return "";
        }
        ;
    };
}
exports.default = apicMemberTypeToIconFilter;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var apicMemberType_1 = __webpack_require__(2);
/**
 * Filter that transforms APIC member type to string representation.
 */
function apicMemberTypeToStringFilter(_t) {
    return function (apicMemberType) {
        switch (apicMemberType) {
            case apicMemberType_1.ApicMemberType.Tenant:
                return _t("Tenant");
            case apicMemberType_1.ApicMemberType.ApplicationProfile:
                return _t("Application Profile");
            case apicMemberType_1.ApicMemberType.PhysicalEntitySpine:
                return _t("Spine");
            case apicMemberType_1.ApicMemberType.PhysicalEntityLeaf:
                return _t("Leaf");
            case apicMemberType_1.ApicMemberType.EndpointGroup:
                return _t("Endpoint Group");
            default:
                return _t("Unknown");
        }
        ;
    };
}
apicMemberTypeToStringFilter.$inject = ["getTextService"];
exports.default = apicMemberTypeToStringFilter;


/***/ })
/******/ ]);
//# sourceMappingURL=netman-cisco.js.map