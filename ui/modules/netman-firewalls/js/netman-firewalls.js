/*!
 * @solarwinds/netman-firewalls-frontend 1.4.0-462
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function bpsStatusFilter() {
    return function (itemWithStatus, bpsPropertyName) {
        if (itemWithStatus.status === 0) {
            return null;
        }
        return itemWithStatus[bpsPropertyName];
    };
}
exports.default = bpsStatusFilter;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FirewallsDataListViewController = /** @class */ (function () {
    function FirewallsDataListViewController(firewallsDataListService) {
        this.firewallsDataListService = firewallsDataListService;
    }
    FirewallsDataListViewController.prototype.$onInit = function () {
        var _this = this;
        this.firewallsDataListService.getFilteredListConfiguration()
            .then(function (configuration) {
            _this.filteredListConfiguration = configuration;
        });
    };
    return FirewallsDataListViewController;
}());
exports.FirewallsDataListViewController = FirewallsDataListViewController;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=$ctrl.filteredListConfiguration> <site-to-site-tunnels-list configuration=$ctrl.filteredListConfiguration></site-to-site-tunnels-list> </div> ";

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=$ctrl.filteredListConfiguration> <remote-access-connections-list configuration=$ctrl.filteredListConfiguration></remote-access-connections-list> </div> ";

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FirewallsDataService = /** @class */ (function () {
    /** @ngInject */
    FirewallsDataService.$inject = ["swApi"];
    function FirewallsDataService(swApi) {
        this.swApi = swApi;
    }
    FirewallsDataService.prototype.getFilteredListConfiguration = function () {
        return this.swApi.api(false)
            .one(this.filteringApiGetConfigurationUrlFragment)
            .get();
    };
    return FirewallsDataService;
}());
exports.FirewallsDataService = FirewallsDataService;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(6);
module.exports = __webpack_require__(7);


/***/ }),
/* 6 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(8);
var config_1 = __webpack_require__(9);
var index_1 = __webpack_require__(10);
var index_2 = __webpack_require__(20);
var index_3 = __webpack_require__(22);
var index_4 = __webpack_require__(29);
var index_5 = __webpack_require__(36);
var templates_1 = __webpack_require__(45);
index_2.default(app_1.default);
config_1.default(app_1.default);
index_1.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
index_5.default(app_1.default);
templates_1.default(app_1.default);
index_1.default(app_1.default);


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("netman-firewalls.services", []);
angular.module("netman-firewalls.templates", []);
angular.module("netman-firewalls.components", []);
angular.module("netman-firewalls.filters", []);
angular.module("netman-firewalls.providers", []);
angular.module("netman-firewalls", [
    "orion",
    "netman-firewalls.services",
    "netman-firewalls.templates",
    "netman-firewalls.components",
    "netman-firewalls.filters",
    "netman-firewalls.providers"
]);
// create and register Xui (Orion) module wrapper
var netManFirewalls = Xui.registerModule("netman-firewalls");
exports.default = netManFirewalls;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: netman-firewalls");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = (function (module) {
    module.app()
        .run(run);
});
exports.default.$inject = ["module"];


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var remoteAccessConnectionDuration_filter_1 = __webpack_require__(11);
var remoteAccessConnectionIcon_filter_1 = __webpack_require__(12);
var nodeLink_filter_1 = __webpack_require__(14);
var tunnelStatusCode_filter_1 = __webpack_require__(15);
var localDateTimeFormat_filter_1 = __webpack_require__(16);
var bpsValue_filter_1 = __webpack_require__(17);
var encryptAlgorithm_filter_1 = __webpack_require__(18);
var hashAlgorithm_filter_1 = __webpack_require__(19);
var bpsStatus_filter_1 = __webpack_require__(0);
exports.default = (function (module) {
    module.filter("remoteAccessConnectionDuration", remoteAccessConnectionDuration_filter_1.default);
    module.filter("remoteAccessConnectionIcon", remoteAccessConnectionIcon_filter_1.default);
    module.filter("nodeLink", nodeLink_filter_1.default);
    module.filter("tunnelStatusCode", tunnelStatusCode_filter_1.default);
    module.filter("localDateTimeFormat", localDateTimeFormat_filter_1.default);
    module.filter("bpsValueFilter", bpsValue_filter_1.default);
    module.filter("encryptAlgorithmFilter", encryptAlgorithm_filter_1.default);
    module.filter("hashAlgorithmFilter", hashAlgorithm_filter_1.default);
    module.filter("bpsStatusFilter", bpsStatus_filter_1.default);
});


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Filter that returns humanized duration of the active connection.
 */
function connectionDurationFilter() {
    return function (connectTime) {
        var currentTime = new Date();
        var connectTimeUtcFormat = moment.utc(connectTime).toISOString();
        var durationInMilliseconds = currentTime.getTime() - new Date(connectTimeUtcFormat).getTime();
        var durationInMinutes = durationInMilliseconds / 60000;
        return moment.duration(durationInMinutes, "minutes").humanize();
    };
}
exports.default = connectionDurationFilter;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var remoteAccessStatus_1 = __webpack_require__(13);
/**
 * Filter that returns xui icon name to be used for Remote Access connection
 * based on the connection's status.
 */
function remoteAccessConnectionIconFilter() {
    return function (remoteAccessState) {
        return remoteAccessState === remoteAccessStatus_1.RemoteAccessStatus.Active ? "state_running" : "status_testing";
    };
}
exports.default = remoteAccessConnectionIconFilter;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
  * Remote access status
  * Corresponds to enum defined in Cortex plugin
 */
var RemoteAccessStatus;
(function (RemoteAccessStatus) {
    RemoteAccessStatus[RemoteAccessStatus["Active"] = 1] = "Active";
    RemoteAccessStatus[RemoteAccessStatus["Inactive"] = 2] = "Inactive";
})(RemoteAccessStatus = exports.RemoteAccessStatus || (exports.RemoteAccessStatus = {}));


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// Generates link based on neighborNodeId
function nodeLinkFilter(_t) {
    return function (neighborNodeId) {
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host;
        return baseUrl + "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + neighborNodeId;
    };
}
nodeLinkFilter.$inject = ["getTextService"];
exports.default = nodeLinkFilter;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// Transforms status specified as number into its textual representation.
function tunnelStatusCodeFilter(_t) {
    return function (statusValue) {
        switch (statusValue) {
            case 0:
                return _t("Unknown");
            case 1:
                return _t("Up");
            case 2:
                return _t("Down");
            case 24:
                return _t("Inactive");
        }
        ;
    };
}
tunnelStatusCodeFilter.$inject = ["getTextService"];
exports.default = tunnelStatusCodeFilter;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// Filter that returns date time in current locale format. 
function localDateTimeFormatFilter() {
    return function (dateTimeToFormat) {
        var dateTimeUtcFormat = moment.utc(dateTimeToFormat).toISOString();
        return moment(dateTimeToFormat).isValid() ? new Date(dateTimeUtcFormat).toLocaleString() : "N/A";
    };
}
exports.default = localDateTimeFormatFilter;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function bpsValueFilter() {
    return function (bps) {
        // We used 3rd party filter that produce three dashes,
        // but we want only one.
        if (bps === "---") {
            return "-";
        }
        return bps;
    };
}
exports.default = bpsValueFilter;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function encryptAlgorithmFilter(_t) {
    return function (encryptAlgorithmValue) {
        if (encryptAlgorithmValue === "not established") {
            return _t("N/A");
        }
        ;
        return encryptAlgorithmValue;
    };
}
encryptAlgorithmFilter.$inject = ["getTextService"];
exports.default = encryptAlgorithmFilter;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function hashAlgorithmFilter(_t) {
    return function (hashAlgorithmValue) {
        if (hashAlgorithmValue === "not established") {
            return _t("N/A");
        }
        ;
        return hashAlgorithmValue;
    };
}
hashAlgorithmFilter.$inject = ["getTextService"];
exports.default = hashAlgorithmFilter;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(21);
exports.default = (function (module) {
    module.service("netman-firewallsConstants", constants_1.default);
});


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(23);
var index_2 = __webpack_require__(26);
exports.default = (function (module) {
    // register views
    index_1.default(module);
    index_2.default(module);
});
var rootState = function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state("netman-firewalls", {
        url: "/netman-firewalls",
        controller: function ($state) {
            $state.go("home");
        }
    });
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var tunnelListView_controller_1 = __webpack_require__(24);
var tunnelList_config_1 = __webpack_require__(25);
exports.default = (function (module) {
    module.config(tunnelList_config_1.default);
    module.controller("TunnelListViewController", tunnelListView_controller_1.default);
});


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var firewallsDataListView_controller_1 = __webpack_require__(1);
var TunnelListViewController = /** @class */ (function (_super) {
    __extends(TunnelListViewController, _super);
    /** @ngInject */
    TunnelListViewController.$inject = ["tunnelListService"];
    function TunnelListViewController(tunnelListService) {
        return _super.call(this, tunnelListService) || this;
    }
    return TunnelListViewController;
}(firewallsDataListView_controller_1.FirewallsDataListViewController));
exports.default = TunnelListViewController;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'netman-firewalls/newPage' url
    $stateProvider.state("tunnelList", {
        parent: "subview",
        i18Title: "_t(Site To Site List)",
        url: "/netman-firewalls/site-to-site-list",
        template: __webpack_require__(2),
        controller: "TunnelListViewController",
        controllerAs: "$ctrl"
    });
}
;
exports.default = config;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var remoteAccessConnectionsList_config_1 = __webpack_require__(27);
var remoteAccessConnectionsListView_controller_1 = __webpack_require__(28);
exports.default = (function (module) {
    module.config(remoteAccessConnectionsList_config_1.default);
    module.controller("RemoteAccessConnectionsListViewController", remoteAccessConnectionsListView_controller_1.default);
});


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state("remoteAccessConnectionsList", {
        parent: "subview",
        i18Title: "_t(GlobalProtect Connections List)",
        url: "/netman-firewalls/remote-access-connections-list",
        template: __webpack_require__(3),
        controller: "RemoteAccessConnectionsListViewController",
        controllerAs: "$ctrl"
    });
}
;
exports.default = config;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var firewallsDataListView_controller_1 = __webpack_require__(1);
var RemoteAccessConnectionsListViewController = /** @class */ (function (_super) {
    __extends(RemoteAccessConnectionsListViewController, _super);
    /** @ngInject */
    RemoteAccessConnectionsListViewController.$inject = ["remoteAccessConnectionsListService"];
    function RemoteAccessConnectionsListViewController(remoteAccessConnectionsListService) {
        return _super.call(this, remoteAccessConnectionsListService) || this;
    }
    return RemoteAccessConnectionsListViewController;
}(firewallsDataListView_controller_1.FirewallsDataListViewController));
exports.default = RemoteAccessConnectionsListViewController;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var siteToSiteTunnelsList_1 = __webpack_require__(30);
var remoteAccessConnectionsList_1 = __webpack_require__(33);
exports.default = (function (module) {
    // register components
    siteToSiteTunnelsList_1.default(module);
    remoteAccessConnectionsList_1.default(module);
});


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var siteToSiteTunnelsList_controller_1 = __webpack_require__(31);
var siteToSiteTunnelsList_directive_1 = __webpack_require__(32);
exports.default = (function (module) {
    module.controller("SiteToSiteTunnelsListController", siteToSiteTunnelsList_controller_1.default);
    module.component("siteToSiteTunnelsList", siteToSiteTunnelsList_directive_1.SiteToSiteTunnelsListDirective);
});


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var filteringApiGetDataUrlFragment = "/filtering/site-to-site-tunnels/data";
var SiteToSiteTunnelsListController = /** @class */ (function () {
    /** @ngInject */
    SiteToSiteTunnelsListController.$inject = ["xuiFilteredListService", "swApi", "filteredListConfigurationService", "siteToSiteTunnelsServiceV2", "pollingStateService", "xuiDialogService", "filterSlidersService", "ellipsisOptionsService", "getTextService", "$window"];
    function SiteToSiteTunnelsListController(xuiFilteredListService, swApi, filteredListConfigurationService, siteToSiteTunnelsServiceV2, pollingStateService, xuiDialogService, filterSlidersService, ellipsisOptionsService, getTextService, $window) {
        this.xuiFilteredListService = xuiFilteredListService;
        this.swApi = swApi;
        this.filteredListConfigurationService = filteredListConfigurationService;
        this.siteToSiteTunnelsServiceV2 = siteToSiteTunnelsServiceV2;
        this.pollingStateService = pollingStateService;
        this.xuiDialogService = xuiDialogService;
        this.filterSlidersService = filterSlidersService;
        this.ellipsisOptionsService = ellipsisOptionsService;
        this.getTextService = getTextService;
        this.$window = $window;
        this.tunnelsSelectionLimit = 3;
    }
    SiteToSiteTunnelsListController.prototype.$onInit = function () {
        var _this = this;
        this._t = this.getTextService;
        this.nodesState = {
            options: {
                templateUrl: "tunnel-item-template",
                rowPadding: "narrow",
                hideSearch: false,
                selectionMode: "multi",
                selectionProperty: "id",
                preserveSelection: true,
                allowSelectAllPages: false,
                triggerSearchOnChange: true,
                emptyData: {
                    image: "no-search-results",
                    description: this._t("No data to show")
                }
            },
            pagination: {
                page: 1,
                pageSize: 10
            }
        };
        this.filteredListConfigurationService.configureFilteredListState(this.configuration, this.nodesState);
        var nodesStateModel = this.xuiFilteredListService.getModel(this, "nodesState");
        var dataSource = this.xuiFilteredListService.getDataSource(filteringApiGetDataUrlFragment, this.swApi.api(false), nodesStateModel, function (params) { return _this.filteredListConfigurationService.createCustomQueryParams(params); });
        var originalGetData = dataSource.getData.bind(dataSource);
        dataSource.getData = function (params) {
            return originalGetData(params)
                .then(function (_) { return _this.filterSlidersService.modifyFilters(_, _this); });
        };
        this.nodesDispatcher = this.xuiFilteredListService.getDispatcherInstance(nodesStateModel, { dataSource: dataSource });
        this.pollingStateService.getPollingState().then(function (nodesPollingStatus) {
            _this.kbLinkForWarningMessage = nodesPollingStatus.kbLink;
            _this.pollingFailed = !nodesPollingStatus.pollingSuccess;
        }).catch(function (reason) {
            console.warn(reason);
        });
    };
    SiteToSiteTunnelsListController.prototype.perfstackAnalysisClicked = function () {
        return __awaiter(this, void 0, void 0, function () {
            var selectedTunnelIds, message, perfstackLink;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        selectedTunnelIds = this.nodesState.selection.items;
                        if (!(selectedTunnelIds.length > this.tunnelsSelectionLimit)) return [3 /*break*/, 1];
                        message = "Maximum of " + this.tunnelsSelectionLimit + " tunnels selected is allowed for Perfstack analysis. \n                You have currently " + selectedTunnelIds.length + " tunnels selected.";
                        this.xuiDialogService.showError({ message: this._t(message) });
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.siteToSiteTunnelsServiceV2.getPerfstackLink(selectedTunnelIds)];
                    case 2:
                        perfstackLink = _a.sent();
                        this.$window.location.assign(perfstackLink);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SiteToSiteTunnelsListController.prototype.isPerfstackAnalysisDisabled = function () {
        var currentSelection = this.nodesState.selection.items;
        return currentSelection ? currentSelection.length === 0 : false;
    };
    SiteToSiteTunnelsListController.prototype.getEllipsisOptions = function (tooltipText) {
        return this.ellipsisOptionsService.getEllipsisOptions(tooltipText);
    };
    return SiteToSiteTunnelsListController;
}());
exports.default = SiteToSiteTunnelsListController;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SiteToSiteTunnelsListDirective = /** @class */ (function () {
    function SiteToSiteTunnelsListDirective() {
        this.restrict = "E";
        this.templateUrl = "netman-firewalls/components/siteToSiteTunnelsList/siteToSiteTunnelsList-directive.html";
        this.replace = true;
        this.scope = {};
        this.controller = "SiteToSiteTunnelsListController";
        this.controllerAs = "$siteToSiteTunnelsListCtrl";
        this.bindToController = { configuration: "<" };
    }
    return SiteToSiteTunnelsListDirective;
}());
exports.SiteToSiteTunnelsListDirective = SiteToSiteTunnelsListDirective;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var remoteAccessConnectionsList_controller_1 = __webpack_require__(34);
var remoteAccessConnectionsList_directive_1 = __webpack_require__(35);
exports.default = (function (module) {
    module.controller("RemoteAccessConnectionsListController", remoteAccessConnectionsList_controller_1.default);
    module.component("remoteAccessConnectionsList", remoteAccessConnectionsList_directive_1.RemoteAccessConnectionsListDirective);
});


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var filteringApiGetDataUrlFragment = "/filtering/remote-access-connections/data";
var RemoteAccessConnectionsListController = /** @class */ (function () {
    /** @ngInject */
    RemoteAccessConnectionsListController.$inject = ["xuiFilteredListService", "swApi", "filteredListConfigurationService", "pollingStateService", "filterSlidersService", "ellipsisOptionsService", "getTextService"];
    function RemoteAccessConnectionsListController(xuiFilteredListService, swApi, filteredListConfigurationService, pollingStateService, filterSlidersService, ellipsisOptionsService, getTextService) {
        this.xuiFilteredListService = xuiFilteredListService;
        this.swApi = swApi;
        this.filteredListConfigurationService = filteredListConfigurationService;
        this.pollingStateService = pollingStateService;
        this.filterSlidersService = filterSlidersService;
        this.ellipsisOptionsService = ellipsisOptionsService;
        this.getTextService = getTextService;
    }
    RemoteAccessConnectionsListController.prototype.$onInit = function () {
        var _this = this;
        this._t = this.getTextService;
        this.nodesState = {
            options: {
                templateUrl: "connection-item-template",
                rowPadding: "narrow",
                hideSearch: false,
                triggerSearchOnChange: true,
                emptyData: {
                    image: "no-search-results",
                    description: this._t("No data to show")
                }
            }
        };
        this.filteredListConfigurationService.configureFilteredListState(this.configuration, this.nodesState);
        var stateModel = this.xuiFilteredListService.getModel(this, "nodesState");
        var dataSource = this.xuiFilteredListService.getDataSource(filteringApiGetDataUrlFragment, this.swApi.api(false), stateModel, function (params) { return _this.filteredListConfigurationService.createCustomQueryParams(params); });
        var originalGetData = dataSource.getData.bind(dataSource);
        dataSource.getData = function (params) {
            return originalGetData(params)
                .then(function (_) { return _this.filterSlidersService.modifyFilters(_, _this); });
        };
        this.dispatcher = this.xuiFilteredListService.getDispatcherInstance(stateModel, { dataSource: dataSource });
        this.pollingStateService.getPollingState().then(function (nodesPollingStatus) {
            _this.kbLinkForWarningMessage = nodesPollingStatus.kbLink;
            _this.pollingFailed = !nodesPollingStatus.pollingSuccess;
        }).catch(function (reason) {
            console.warn(reason);
        });
    };
    RemoteAccessConnectionsListController.prototype.getEllipsisOptions = function (tooltipText) {
        return this.ellipsisOptionsService.getEllipsisOptions(tooltipText);
    };
    return RemoteAccessConnectionsListController;
}());
exports.default = RemoteAccessConnectionsListController;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var RemoteAccessConnectionsListDirective = /** @class */ (function () {
    function RemoteAccessConnectionsListDirective() {
        this.restrict = "E";
        this.templateUrl = "netman-firewalls/components/remoteAccessConnectionsList/remoteAccessConnectionsList-directive.html";
        this.replace = true;
        this.scope = {};
        this.controller = "RemoteAccessConnectionsListController";
        this.controllerAs = "$remoteAccessConnectionsListCtrl";
        this.bindToController = { configuration: "<" };
    }
    return RemoteAccessConnectionsListDirective;
}());
exports.RemoteAccessConnectionsListDirective = RemoteAccessConnectionsListDirective;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var netManView_service_1 = __webpack_require__(37);
var tunnelList_service_1 = __webpack_require__(38);
var siteToSiteTunnels_service_1 = __webpack_require__(39);
var filteredListConfiguration_service_1 = __webpack_require__(40);
var remoteAccessConnectionsList_service_1 = __webpack_require__(41);
var pollingState_service_1 = __webpack_require__(42);
var filterSliders_service_1 = __webpack_require__(43);
var ellipsisOptions_service_1 = __webpack_require__(44);
var bpsStatus_filter_1 = __webpack_require__(0);
exports.default = (function (module) {
    module.service("netManViewService", netManView_service_1.NetManViewService);
    module.service("tunnelListService", tunnelList_service_1.TunnelListService);
    module.service("remoteAccessConnectionsListService", remoteAccessConnectionsList_service_1.RemoteAccessConnectionsListService);
    module.service("siteToSiteTunnelsServiceV2", siteToSiteTunnels_service_1.SiteToSiteTunnelsService);
    module.service("filteredListConfigurationService", filteredListConfiguration_service_1.FilteredListConfigurationService);
    module.service("pollingStateService", pollingState_service_1.PollingStateService);
    module.service("filterSlidersService", filterSliders_service_1.FilterSlidersService);
    module.service("ellipsisOptionsService", ellipsisOptions_service_1.EllipsisOptionsService);
    module.service("bpsStatusFilter", bpsStatus_filter_1.default);
});


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NetManViewService = /** @class */ (function () {
    /** @ngInject */
    NetManViewService.$inject = ["$log", "$location"];
    function NetManViewService($log, $location) {
        this.$log = $log;
        this.$location = $location;
        this.netObject = null;
    }
    NetManViewService.prototype.getNetObject = function () {
        try {
            if (this.netObject) {
                return this.netObject;
            }
            var netObjectQueryString = this.$location.search().NetObject;
            var nodeId = parseInt((decodeURIComponent(netObjectQueryString)
                .match(/N\:([0-9]+)/)
                ? RegExp.$1
                : "0"), 10);
            this.netObject = {
                nodeId: nodeId
            };
            return this.netObject;
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    return NetManViewService;
}());
exports.NetManViewService = NetManViewService;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var firewallsData_service_1 = __webpack_require__(4);
var TunnelListService = /** @class */ (function (_super) {
    __extends(TunnelListService, _super);
    function TunnelListService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.filteringApiGetConfigurationUrlFragment = "/filtering/site-to-site-tunnels/configuration";
        return _this;
    }
    return TunnelListService;
}(firewallsData_service_1.FirewallsDataService));
exports.TunnelListService = TunnelListService;


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var swApiGetPerfstackLinkUrlFragment = "/vpn/l2ltunnels/getPerfstackLink";
var SiteToSiteTunnelsService = /** @class */ (function () {
    /** @ngInject */
    SiteToSiteTunnelsService.$inject = ["swApi", "$location"];
    function SiteToSiteTunnelsService(swApi, $location) {
        this.swApi = swApi;
        this.$location = $location;
    }
    SiteToSiteTunnelsService.prototype.getPerfstackLink = function (tunnelIds) {
        return this.swApi.api(false)
            .one(swApiGetPerfstackLinkUrlFragment)
            .customPOST(tunnelIds);
    };
    return SiteToSiteTunnelsService;
}());
exports.SiteToSiteTunnelsService = SiteToSiteTunnelsService;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilteredListConfigurationService = /** @class */ (function () {
    /** @ngInject */
    FilteredListConfigurationService.$inject = ["netManViewService"];
    function FilteredListConfigurationService(netManViewService) {
        this.netManViewService = netManViewService;
    }
    FilteredListConfigurationService.prototype.createCustomQueryParams = function (params) {
        var netObject = this.netManViewService.getNetObject();
        var nodeId = netObject.nodeId;
        if (!params.custom) {
            params.custom = {};
        }
        params.custom["nodeId"] = nodeId;
        return params;
    };
    // this method is implemented as public in FilteredListService, but it's not exposed in interface
    FilteredListConfigurationService.prototype.configureFilteredListState = function (configuration, state) {
        if (configuration && configuration.filters) {
            state.filters = {
                filterProperties: configuration.filters
            };
        }
        if (configuration && configuration.list) {
            var sorter_1 = configuration.list.sorter;
            if (sorter_1 && sorter_1.defaultSort && sorter_1.sortableColumns) {
                var sortBy = _.find(sorter_1.sortableColumns, function (x) { return x.id === sorter_1.defaultSort.sortBy; });
                state.sorting = {
                    sortBy: sortBy,
                    direction: sorter_1.defaultSort.sortDirection,
                    sortableColumns: sorter_1.sortableColumns
                };
            }
        }
    };
    return FilteredListConfigurationService;
}());
exports.FilteredListConfigurationService = FilteredListConfigurationService;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var firewallsData_service_1 = __webpack_require__(4);
var RemoteAccessConnectionsListService = /** @class */ (function (_super) {
    __extends(RemoteAccessConnectionsListService, _super);
    function RemoteAccessConnectionsListService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.filteringApiGetConfigurationUrlFragment = "/filtering/remote-access-connections/configuration";
        return _this;
    }
    return RemoteAccessConnectionsListService;
}(firewallsData_service_1.FirewallsDataService));
exports.RemoteAccessConnectionsListService = RemoteAccessConnectionsListService;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var swApiGetPollingStatusUrlFragment = "/netman-firewalls/{nodeId}/pollingstate";
var PollingStateService = /** @class */ (function () {
    /** @ngInject */
    PollingStateService.$inject = ["swApi", "$location"];
    function PollingStateService(swApi, $location) {
        this.swApi = swApi;
        this.$location = $location;
    }
    PollingStateService.prototype.getPollingState = function () {
        var nodeId = this.getNodeId();
        var route = swApiGetPollingStatusUrlFragment.replace("{nodeId}", nodeId.toString());
        var legacyApi = false;
        return this.swApi
            .api(legacyApi)
            .one(route)
            .get();
    };
    PollingStateService.prototype.getNodeId = function () {
        var netObjectQueryString = this.$location.search().NetObject;
        var nodeId = parseInt((decodeURIComponent(netObjectQueryString)
            .match(/N\:([0-9]+)/)
            ? RegExp.$1
            : "0"), 10);
        return nodeId;
    };
    return PollingStateService;
}());
exports.PollingStateService = PollingStateService;


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilterSlidersService = /** @class */ (function () {
    function FilterSlidersService() {
        // If true, slider will be checked.
        this.shouldCheckInSlider = false;
        this.shouldCheckOutSlider = false;
        // If true, the whole page will reload.
        this.shouldReload = false;
    }
    FilterSlidersService.prototype.modifyFilters = function (conf, controller) {
        var _this = this;
        var shouldRefresh = false;
        conf.filters.forEach(function (filter) {
            try {
                filter.items.forEach(function (filterItem) {
                    if (filterItem.type === 2) {
                        // Filtered list needs lower case string type representation in order to show slider.
                        filterItem.type = "range";
                        var filterSettings = filterItem.slider;
                        // We check if the slider value is consistent with the data. If not, should refresh will be set to true.
                        if (filterItem.id === "InSlider") {
                            shouldRefresh = shouldRefresh || _this.updateInSlider(controller, filterSettings);
                        }
                        if (filterItem.id === "OutSlider") {
                            shouldRefresh = shouldRefresh || _this.updateOutSlider(controller, filterSettings);
                        }
                    }
                    else {
                        // There is a bug in filtered list, it works only if the type property is deleted.
                        delete filterItem.type;
                    }
                });
            }
            catch (e) {
                console.warn(e);
            }
        });
        if (shouldRefresh && !this.shouldReload) {
            controller.remoteControl.refreshListData();
        }
        if (this.shouldReload) {
            this.shouldReload = false;
            location.reload(true);
        }
        return conf;
    };
    FilterSlidersService.prototype.updateOutSlider = function (controller, filterSettings) {
        var shouldRefresh = false;
        var outSlider = controller.nodesState.filters.filterValues.MbpsOut.checkboxes.OutSlider;
        var outSliderNeedsRefresh = this.editSlider(outSlider, filterSettings, "MbpsOut", controller);
        // Slider values are refreshed only if the checkbox of the filter is checked and unchecked.
        // So we uncheck, we request refresh and during the refresh we check again.
        // If you find out how to do it without this, please fix it.
        if (this.shouldCheckOutSlider) {
            outSlider.checked = true;
            this.shouldCheckOutSlider = false;
            shouldRefresh = true;
        }
        if (outSliderNeedsRefresh) {
            if (outSlider && outSlider.checked) {
                this.shouldCheckOutSlider = true;
                outSlider.checked = false;
                shouldRefresh = true;
            }
        }
        return shouldRefresh;
    };
    FilterSlidersService.prototype.updateInSlider = function (controller, filterSettings) {
        var shouldRefresh = false;
        var inSlider = controller.nodesState.filters.filterValues.MbpsIn.checkboxes.InSlider;
        var inSliderNeedsRefresh = this.editSlider(inSlider, filterSettings, "MbpsIn", controller);
        // Slider values are refreshed only if the checkbox of the filter is checked and unchecked.
        // So we uncheck, we request refresh and during the refresh we check again.
        // If you find out how to do it without this, please fix it.
        if (this.shouldCheckInSlider) {
            inSlider.checked = true;
            this.shouldCheckInSlider = false;
            shouldRefresh = true;
        }
        if (inSliderNeedsRefresh) {
            if (inSlider && inSlider.checked) {
                this.shouldCheckInSlider = true;
                inSlider.checked = false;
                return true;
            }
        }
        return shouldRefresh;
    };
    FilterSlidersService.prototype.editSlider = function (slider, filterSettings, filterName, controller) {
        var _this = this;
        if (!slider) {
            return false;
        }
        var needsRefresh = false;
        // We synchronize the actual state of the slider with the correct one from server.
        controller.nodesState.filters.visibleFilterProperties.forEach(function (filter) {
            if (filter.id === filterName) {
                var sliderRange = filter.items[0].slider;
                var sliderValues = slider.model;
                if (sliderRange.unit !== filterSettings.unit) {
                    // If you know how to change unit without reload, change it.
                    _this.shouldReload = true;
                    return true;
                }
                needsRefresh = _this.checkSelectedValues(filterSettings, sliderRange, sliderValues, needsRefresh);
                needsRefresh = _this.checkSliderRange(filterSettings, sliderRange, needsRefresh);
            }
        });
        return needsRefresh;
    };
    FilterSlidersService.prototype.checkSliderRange = function (filterSettings, sliderRange, needsRefresh) {
        // Checks if the actual range of the slider is the same as in the setting.
        // If not, values are corrected and refresh is requested.
        if (sliderRange.min !== filterSettings.min) {
            sliderRange.min = filterSettings.min;
            needsRefresh = true;
        }
        if (sliderRange.max !== filterSettings.max) {
            sliderRange.max = filterSettings.max;
            needsRefresh = true;
        }
        return needsRefresh;
    };
    FilterSlidersService.prototype.checkSelectedValues = function (filterSettings, sliderRange, sliderValues, needsRefresh) {
        // Checks selected values in filtered list. If the full range was selected before edit, the full range is selected too afterward.
        if (filterSettings.min > sliderValues.start ||
            (sliderValues.start === sliderRange.min && sliderValues.start !== filterSettings.min)) {
            sliderValues.start = filterSettings.min;
            needsRefresh = true;
        }
        if (filterSettings.max < sliderValues.end ||
            (sliderValues.end === sliderRange.max && sliderValues.end !== filterSettings.max)) {
            sliderValues.end = filterSettings.max;
            needsRefresh = true;
        }
        return needsRefresh;
    };
    return FilterSlidersService;
}());
exports.FilterSlidersService = FilterSlidersService;


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @description
 * Ellipsis options service provides default options for given tooltip text
 * and cached options object to avoid AngularJS thinking it's getting
 * new object every time.
 */
var EllipsisOptionsService = /** @class */ (function () {
    function EllipsisOptionsService() {
        this.ellipsisOptions = new Map();
    }
    EllipsisOptionsService.prototype.getEllipsisOptions = function (tooltipText) {
        var ellipsisOption = this.ellipsisOptions.get(tooltipText);
        if (ellipsisOption === undefined) {
            ellipsisOption = {
                tooltipText: tooltipText,
                tooltipOptions: {
                    "tooltip-placement": "bottom"
                }
            };
            this.ellipsisOptions.set(tooltipText, ellipsisOption);
        }
        return ellipsisOption;
    };
    return EllipsisOptionsService;
}());
exports.EllipsisOptionsService = EllipsisOptionsService;


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(46);
    req.keys().forEach(function (r) {
        var key = "netman-firewalls" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = (function (module) {
    module.app().run(templates);
});


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/remoteAccessConnectionsList/example/remoteAccessConnectionsList.html": 47,
	"./components/remoteAccessConnectionsList/remoteAccessConnectionsList-directive.html": 48,
	"./components/siteToSiteTunnelsList/example/siteToSiteTunnelsList.html": 49,
	"./components/siteToSiteTunnelsList/siteToSiteTunnelsList-directive.html": 50,
	"./views/remoteAccessConnectionsList/remoteAccessConnectionsList.html": 3,
	"./views/siteToSiteList/tunnelList.html": 2
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 46;

/***/ }),
/* 47 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title=\"GlobalProtect Connections - Test Example\" page-layout=form> <div ng-controller=\"testCtrl as vm\"> <remote-access-connections-list configuration=vm.filteredListConfiguration></remote-access-connections-list> </div> </xui-page-content>";

/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = "<div class=remote-access-connections-list> <xui-message ng-if=$remoteAccessConnectionsListCtrl.pollingFailed id=warningMessagePollingFail type=warning allow-dismiss=true> <span class=remote-access-tunnels-list-warning-message _t>Firewall REST API polling is not enabled or failed.</span> <span> <a ng-href={{::$remoteAccessConnectionsListCtrl.kbLinkForWarningMessage}} target=_blank rel=\"noopener noreferrer\"> » <span _t>Learn More</span> </a> </span> </xui-message> <xui-filtered-list-v2 state=$remoteAccessConnectionsListCtrl.nodesState dispatcher=$remoteAccessConnectionsListCtrl.dispatcher controller=$remoteAccessConnectionsListCtrl> </xui-filtered-list-v2> <script type=text/ng-template id=connection-item-template> <div class=\"row connections-list-item\">\n            <div class=\"col-md-3\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon class=\"connection-icon\" icon=\"{{ ::item.status | remoteAccessConnectionIcon }}\"></xui-icon>\n                    </div>\n                    <div class=\"media-body firewall-ellipsis-container\">\n                        <div\n                            xui-ellipsis\n                            append-to-body=\"true\"\n                            ellipsis-options=\"vm.getEllipsisOptions(item.username)\"\n                            class=\"connection-username\">\n                            {{ ::item.username }}\n                        </div>\n                        <xui-help-hint class=\"connection-duration\">\n                            <span ng-if=\"::item.disconnectTime\" _t>Connection Ended</span>\n                            <span ng-if=\"::!item.disconnectTime\" _t>\n                                Connected for {{ ::item.connectTime | remoteAccessConnectionDuration }}\n                            </span>\n                        </xui-help-hint>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-9\">\n                <div class=\"col-md-2\">\n                    <div class=\"connection-connect-time\">{{ ::item.connectTime | localDateTimeFormat }}</div>\n                    <xui-help-hint _t>connected</xui-help-hint>\n                </div>\n                <div class=\"col-md-2\">\n                    <div ng-if=\"::item.disconnectTime\">\n                        <div class=\"connection-disconnect-time\">{{ ::item.disconnectTime | localDateTimeFormat }}</div>\n                        <xui-help-hint _t>disconnected</xui-help-hint>\n                    </div>\n                </div>\n                <div class=\"col-md-2\">\n                    <div ng-if=\"::item.publicIp\">\n                        <div\n                            xui-ellipsis\n                            append-to-body=\"true\"\n                            ellipsis-options=\"vm.getEllipsisOptions(item.publicIp)\"\n                            class=\"connection-public-ip\">\n                            {{ ::item.publicIp }}\n                        </div>\n                        <xui-help-hint _t>public IP</xui-help-hint>\n                    </div>\n                </div>\n                <div class=\"col-md-2\">\n                    <div ng-if=\"::item.virtualIp\">\n                        <div\n                            xui-ellipsis\n                            append-to-body=\"true\"\n                            ellipsis-options=\"vm.getEllipsisOptions(item.virtualIp)\"\n                            class=\"connection-virtual-ip\">\n                            {{ ::item.virtualIp }}\n                        </div>\n                        <xui-help-hint _t>virtual IP</xui-help-hint>\n                    </div>\n                </div>\n                <div class=\"col-md-2\" ng-if=\"::item.bpsIn !== undefined\">\n                    <span class=\"connection-tunnel-bpsin\">\n                        {{ ::item  | bpsStatusFilter : 'bpsIn' | xuiUnitConversion: 2 : false : 'bitsPerSecond' | bpsValueFilter }}\n                    </span>\n                    <xui-help-hint>\n                        <span _t>in</span>\n                    </xui-help-hint>\n                </div>\n                <div class=\"col-md-2\" ng-if=\"::item.bpsOut !== undefined\">\n                    <span class=\"connection-tunnel-bpsout\">\n                        {{ ::item | bpsStatusFilter : 'bpsOut'  | xuiUnitConversion: 2 : false : 'bitsPerSecond' | bpsValueFilter}}\n                    </span>\n                    <xui-help-hint>\n                        <span _t>out</span>\n                    </xui-help-hint>\n                </div>\n            </div>\n        </div> </script> </div>";

/***/ }),
/* 49 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title=\"Site To Site List - Test Example\" page-layout=form> <div ng-controller=\"testCtrl as vm\"> <site-to-site-tunnels-list configuration=vm.filteredListConfiguration></site-to-site-tunnels-list> </div> </xui-page-content>";

/***/ }),
/* 50 */
/***/ (function(module, exports) {

module.exports = "<div class=site-to-site-tunnels-list> <xui-message ng-if=$siteToSiteTunnelsListCtrl.pollingFailed id=warningMessagePollingFail type=warning allow-dismiss=true> <span class=site-to-site-tunnels-list-warning-message _t>Firewall REST API polling is not enabled or failed.</span> <span> <a ng-href={{::$siteToSiteTunnelsListCtrl.kbLinkForWarningMessage}} target=_blank rel=\"noopener noreferrer\"> » <span _t>Learn More</span> </a> </span> </xui-message> <xui-filtered-list-v2 state=$siteToSiteTunnelsListCtrl.nodesState dispatcher=$siteToSiteTunnelsListCtrl.nodesDispatcher controller=$siteToSiteTunnelsListCtrl remote-control=$siteToSiteTunnelsListCtrl.remoteControl> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button class=perfstack-analysis icon=performance display-style=tertiary ng-click=$siteToSiteTunnelsListCtrl.perfstackAnalysisClicked() is-disabled=$siteToSiteTunnelsListCtrl.isPerfstackAnalysisDisabled() _t> Performance analyzer </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list-v2> <script type=text/ng-template id=tunnel-item-template> <div class=\"row tunnel-list-item\">\n            <div class=\"col-md-4\">\n                <div class=\"row\">\n                    <div class=\"col-md-7\">\n                        <div class=\"media\">\n                            <div class=\"media-left icon-holder\">\n                                <xui-icon icon=\"vpn-tunnel-sitetosite\" status=\"{{::item.status | swStatus}}\" />\n                            </div>\n                            <div class=\"media-body firewall-ellipsis-container\">\n                                <span\n                                    xui-ellipsis\n                                    append-to-body=\"true\"\n                                    ellipsis-options=\"vm.getEllipsisOptions(item.name)\"\n                                    class=\"s2s-tunnel-name\">\n                                    {{::item.name}}\n                                </span>\n                                <xui-help-hint>\n                                    <span class=\"s2s-tunnel-localip\">{{::item.localIp}}</span>\n                                    &rarr;\n                                    <a \n                                        class=\"s2s-tunnel-remotenode-link\" \n                                        ng-href=\"{{::item.remoteNodeId | nodeLink }}\" \n                                        ng-if=\"item.remoteNodeId !== 0\" \n                                        target=\"_blank\"> \n                                        <span class=\"s2s-tunnel-remoteip\">{{::item.remoteIp}}</span>\n                                    </a>\n                                    <span class=\"s2s-tunnel-remoteip\" ng-if=\"item.remoteNodeId === 0\">\n                                        {{::item.remoteIp}}\n                                    </span>\n                                </xui-help-hint>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-md-5\">\n                        <div class=\"media\">\n                            <span ng-class=\"['s2s-tunnel-status', {'xui-text-critical': item.issueMessage || item.status === 2}]\">\n                                {{::item.status | tunnelStatusCode}}\n                            </span>\n                            <span\n                                ng-if=\"item.issueMessage\"\n                                xui-ellipsis\n                                append-to-body=\"true\"\n                                ellipsis-options=\"vm.getEllipsisOptions(item.issueMessage)\"\n                                ng-class=\"['s2s-tunnel-issue', {'xui-text-critical': item.issueMessage}]\">\n                                , {{::item.issueMessage}}\n                            </span>\n                        </div>\n                        <xui-help-hint>\n                            <span class=\"s2s-tunnel-last-status-changed\" ng-if=\"item.lastStatusChanged != null\" _t>\n                                Since {{::item.lastStatusChanged | localDateTimeFormat}}\n                            </span>\n                        </xui-help-hint>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-8\">\n                <div class=\"row\">\n                    <div class=\"col-md-2\">\n                        <span class=\"s2s-tunnel-encryption-cipher\">\n                            {{::item.encryptionCipher | encryptAlgorithmFilter}}\n                        </span>\n                        <xui-help-hint>\n                            <span class=\"s2s-tunnel-encryption\" _t>encryption</span>\n                        </xui-help-hint>\n                    </div>\n                    <div class=\"col-md-2\">\n                        <span class=\"s2s-tunnel-hash-algorithm\">\n                            {{::item.hashAlgorithm | hashAlgorithmFilter}}\n                        </span>\n                        <xui-help-hint>\n                            <span class=\"s2s-tunnel-hashing\" _t>hashing</span>\n                        </xui-help-hint>\n                    </div>\n                    <div class=\"col-md-2\">\n                        <span\n                            xui-ellipsis\n                            append-to-body=\"true\"\n                            ellipsis-options=\"vm.getEllipsisOptions(item.virtualFirewall)\"\n                            class=\"s2s-tunnel-virtual-firewall\">\n                                {{::item.virtualFirewall}}\n                        </span>\n                        <xui-help-hint>\n                            <span class=\"s2s-tunnel-vsys\" _t>vsys</span>\n                        </xui-help-hint>\n                    </div>\n                    <div class=\"col-md-2\">\n                        <span\n                            xui-ellipsis\n                            append-to-body=\"true\"\n                            ellipsis-options=\"vm.getEllipsisOptions(item.securityZone)\"\n                            class=\"s2s-tunnel-security-zone\">\n                            {{::item.securityZone}}\n                        </span>\n                        <xui-help-hint>\n                            <span class=\"s2s-tunnel-securityzone\" _t>security zone</span>\n                        </xui-help-hint>\n                    </div>\n                    <div class=\"col-md-2\">\n                        <span class=\"s2s-tunnel-bpsin\">\n                            {{::item | bpsStatusFilter : 'bpsIn' | xuiUnitConversion: 2 : false : 'bitsPerSecond' | bpsValueFilter}}\n                        </span>\n                        <xui-help-hint>\n                            <span _t>in</span>\n                        </xui-help-hint>\n                    </div>\n                    <div class=\"col-md-2\">\n                        <span class=\"s2s-tunnel-bpsout\">\n                            {{::item | bpsStatusFilter : 'bpsOut' | xuiUnitConversion: 2 : false : 'bitsPerSecond' | bpsValueFilter}}\n                        </span>\n                        <xui-help-hint>\n                            <span _t>out</span>\n                        </xui-help-hint>\n                    </div>\n                </div>\n            </div>\n        </div> </script> </div>";

/***/ })
/******/ ]);
//# sourceMappingURL=netman-firewalls.js.map