/*!
 * @solarwinds/dpaim 11.4.1-271
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 27);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(37));
__export(__webpack_require__(45));


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(0);
var ConnectionTestResultToTextService = /** @class */ (function () {
    /** @ngInject */
    ConnectionTestResultToTextService.$inject = ["getTextService"];
    function ConnectionTestResultToTextService(getTextService) {
        this._t = getTextService;
    }
    ConnectionTestResultToTextService.prototype.toText = function (result) {
        switch (result) {
            case models_1.ConnectionTestResult.success:
                return this._t("Everything is OK and ready for integration.");
            case models_1.ConnectionTestResult.invalidCredentials:
                return this._t("The user or password you entered is incorrect.");
            case models_1.ConnectionTestResult.insufficientPrivileges:
                return this._t("This account does not have administrator privileges. Use an administrator account.");
            case models_1.ConnectionTestResult.notResponding:
                return this._t("Cannot find a DPA server at this address or port. Check your DPA server \
details and network connection.");
            case models_1.ConnectionTestResult.unsupportedVersion:
                return this._t("This version of SolarWinds Orion Platform is not compatible with your current version \
of SolarWinds DPA. Upgrade both products to their latest version from your SolarWinds Customer Portal.");
            case models_1.ConnectionTestResult.swisFailure:
                return this._t("Unable to connect to the remote instance data provider.");
            case models_1.ConnectionTestResult.unableToPingBack:
                return this._t("The DPA server failed to connect to the Orion Web Console by its hostname or \
IP address.");
            case models_1.ConnectionTestResult.alreadyIntegrated:
                return this._t("The DPA server at this address and port has already been integrated. To reintegrate, \
remove the existing integration and try again.");
            default:
                break;
        }
    };
    return ConnectionTestResultToTextService;
}());
exports.ConnectionTestResultToTextService = ConnectionTestResultToTextService;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.Constants = Constants;
;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Links = /** @class */ (function () {
    function Links() {
        this.relationshipManagement = "/Orion/DPA/Admin/ManageRelationships/DatabaseInstances.aspx";
        this.databasesSummary = "/Orion/DPA/Summary.aspx";
        this.dpaProduct = "http://www.solarwinds.com/embedded_in_products/productLink.aspx?Id=DPA_Orion_popup_download";
        this.learnAboutDpaIntegration = "http://www.solarwinds.com/documentation/helploader.aspx?lang=en&topic=OrionDPALearnMoreAboutOrionIntegration";
        this.dpaimReleaseNotesLink = "http://www.solarwinds.com/documentation/kbloader.aspx?kb=MT68880";
        this.dpaReleaseNotesLink = "https://support.solarwinds.com/Success_Center/Database_Performance_Analyzer_(DPA)/release_notes";
        this.dpaimProduct = "https://customerportal.solarwinds.com/Downloads";
    }
    Links.prototype.dpaServerDetails = function (dpaServerId) {
        return "/Orion/DPA/DpaServerDetails.aspx?NetObject=DBS:" + dpaServerId;
    };
    return Links;
}());
exports.Links = Links;
;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var LinksToDpa = /** @class */ (function () {
    function LinksToDpa() {
    }
    LinksToDpa.prototype.getSqlQueryDetailLink = function (globalDatabaseInstanceId, sqlHash) {
        return "/api/DpaLinksToDpaj/SqlQueryDetails?dbi=" + globalDatabaseInstanceId + "&hash=" + sqlHash;
    };
    return LinksToDpa;
}());
exports.LinksToDpa = LinksToDpa;
;


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content id=dpaServerManagementPage ng-init=vm.activate() page-layout=form is-busy=vm.isPageBusy> <xui-page-header> <div class=xui-page-content__header> <h1 class=xui-page-content__header-title _t>DPA Server Management</h1> <div class=xui-page-content__header-link ng-if=\"vm.dpaServers != null && vm.dpaServers.length > 0\"> <div class=xui-page-content__header-link-item> <xui-button size=small display-style=tertiary name=databasesSummary ng-click=vm.goToDatabasesSummary() _t>Databases Summary</xui-button> </div> <div class=xui-page-content__header-link-item> <xui-button size=small display-style=tertiary ng-click=vm.goToRelationshipManagement() _t>Relationship Management</xui-button> </div> </div> </div> </xui-page-header> <div class=dpa-server-management__content> <p _t>You can manage the integration between the Solarwinds Orion server and multiple Database Performance Analyzer (DPA) servers here.</p> <xui-message type=info ng-if=\"vm.dpaServers != null && vm.dpaServers.length == 0\"> <b _t>Where can I get Database Performance Analyzer (DPA)?</b> <span _t>If you have not installed DPA, you can learn how to install and configure DPA before integrating with Orion Platform.</span> <a ng-href={{::vm.dpaLinks.dpaProduct}} target=_blank>&raquo; <span _t>Get Database Performance Analyzer now</span></a> <span _t>or</span> <a ng-href={{::vm.dpaLinks.learnAboutDpaIntegration}} target=_blank>&raquo; <span _t>Learn more about DPA integration</span></a> </xui-message> <xui-divider class=xui-margin-lgh-neg></xui-divider> <div ng-if=\"vm.dpaServers != null\" class=xui-margin-lg-neg> <xui-grid template-url=dpa/views/admin/serverManagement/dpa-server-listview-item.html empty-data=vm.emptyData items-source=vm.dpaServers smart-mode=true selection-mode=multi selection-property=dpaServerId selection=vm.selection show-selector=true pagination-data=vm.pagination sorting-data=vm.sorting options=vm.options name=dpaServersGrid> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button ng-if=!vm.selection.items.length display-style=tertiary name=addDpaServer icon=add ng-click=vm.openAddServerDialog() _t> Add DPA Server </xui-button> <xui-button ng-if=vm.selection.items.length display-style=tertiary name=removeDpaServers icon=delete ng-click=vm.removeDpaServers(vm.selection.items) _t> Remove </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-grid> </div> </div> </xui-page-content>";

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content id=expertAdviceInfoPage ng-init=vm.activate() page-title={{vm.waitId}} page-layout=form is-busy=vm.isPageBusy> <div class=automationAdviceContent ng-ig=\"vm.htmlHelpContent != null\" ng-bind-html=vm.htmlHelpContent></div> </xui-page-content>";

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content id=sqlDetailPage class=xui-code--no-padding.hljs ng-init=vm.activate() page-layout=form is-busy=vm.isPageBusy> <div class=\"xui-page-content__header xui-margin-lgh-neg xui-margin-lgt-neg\"> <h1 class=\"xui-page-content__header-title automationCustomTitle\" _t>{{vm.sqlName}}</h1> <xui-button class=xui-page-content__header-link display-style=primary name=viewInDpa ng-click=vm.viewInDpa() _t>View Query History in DPA</xui-button> </div> <div class=\"xui-page-content__body xui-margin-lgh-neg xui-margin-lgb-neg\"> <div ng-if=\"vm.sqlText != null\"> <xui-code content=vm.sqlText lang=sql></xui-code> </div> </div> </xui-page-content> ";

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(9));
__export(__webpack_require__(25));
var ipHostnameValidator_1 = __webpack_require__(9);
var portValidator_1 = __webpack_require__(25);
exports.default = function (module) {
    // register components
    module.component("iphostname", ipHostnameValidator_1.default);
    module.component("port", portValidator_1.default);
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var services_1 = __webpack_require__(10);
var IpHostnameValidator = /** @class */ (function () {
    function IpHostnameValidator() {
        this.require = "ngModel";
        this.restrict = "A";
        this.link = function ($scope, element, attrs, controller) {
            if (!controller) {
                return;
            }
            var addressService = new services_1.AddressService();
            controller.$validators["iphostname"] = function (modelValue, viewValue) {
                var isValid = viewValue == null || viewValue === "" || addressService.isIpAddress(viewValue)
                    || addressService.isHostname(viewValue);
                controller.$setValidity("iphostname", isValid);
                return isValid;
            };
        };
    }
    return IpHostnameValidator;
}());
exports.default = IpHostnameValidator;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(11));
__export(__webpack_require__(16));
__export(__webpack_require__(19));
__export(__webpack_require__(20));
__export(__webpack_require__(21));
__export(__webpack_require__(22));
__export(__webpack_require__(23));
__export(__webpack_require__(24));
var dialogServices_1 = __webpack_require__(11);
var textServices_1 = __webpack_require__(16);
var dpaServerService_1 = __webpack_require__(20);
var addressService_1 = __webpack_require__(19);
var testConnectionService_1 = __webpack_require__(21);
var expertAdviceInfoService_1 = __webpack_require__(22);
var sqlQueryService_1 = __webpack_require__(23);
var redirectService_1 = __webpack_require__(24);
exports.default = function (module) {
    dialogServices_1.default(module);
    textServices_1.default(module);
    module.service("dpaServerService", dpaServerService_1.DpaServerService);
    module.service("dpaAddressService", addressService_1.AddressService);
    module.service("testConnectionService", testConnectionService_1.TestConnectionService);
    module.service("expertAdviceInfoService", expertAdviceInfoService_1.ExpertAdviceInfoService);
    module.service("sqlQueryService", sqlQueryService_1.SqlQueryService);
    module.service("redirectService", redirectService_1.RedirectService);
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(12));
__export(__webpack_require__(13));
__export(__webpack_require__(14));
__export(__webpack_require__(15));
var addServerDialogActionsService_1 = __webpack_require__(12);
var addServerDialogService_1 = __webpack_require__(13);
var compatibilityDialogService_1 = __webpack_require__(14);
var removeServersDialogService_1 = __webpack_require__(15);
exports.default = function (module) {
    // register components
    module.service("addServerDialogActionsService", addServerDialogActionsService_1.AddServerDialogActionsService);
    module.service("addServerDialogService", addServerDialogService_1.AddServerDialogService);
    module.service("compatibilityDialogService", compatibilityDialogService_1.CompatibilityDialogService);
    module.service("removeServersDialogService", removeServersDialogService_1.RemoveServersDialogService);
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(0);
var AddServerDialogActionsService = /** @class */ (function () {
    /** @ngInject */
    AddServerDialogActionsService.$inject = ["dpaServerService", "integrationPhaseToTextService", "compatibilityDialogService", "$q", "configureIntegrationResultToTextService"];
    function AddServerDialogActionsService(dpaServerService, integrationPhaseToTextService, compatibilityDialogService, $q, configureIntegrationResultToTextService) {
        var _this = this;
        this.dpaServerService = dpaServerService;
        this.integrationPhaseToTextService = integrationPhaseToTextService;
        this.compatibilityDialogService = compatibilityDialogService;
        this.$q = $q;
        this.configureIntegrationResultToTextService = configureIntegrationResultToTextService;
        this.taskFinishedCheckIntervalMs = 500;
        this.submitAction = function (dialogResult, addServerViewModel, forceIntegrationWhenPartiallyCompatible) {
            if (forceIntegrationWhenPartiallyCompatible === void 0) { forceIntegrationWhenPartiallyCompatible = false; }
            addServerViewModel.errorMessage = null;
            var deferred = _this.$q.defer();
            if (!addServerViewModel.isValid()) {
                deferred.resolve(false);
            }
            else {
                var dpaServerCreationResponse = _this.dpaServerService.createDpaServer({
                    address: addServerViewModel.address,
                    port: addServerViewModel.port,
                    name: addServerViewModel.name,
                    orionAddress: addServerViewModel.orionAddress,
                    username: addServerViewModel.username,
                    password: addServerViewModel.password,
                    forceIntegrationWhenPartiallyCompatible: forceIntegrationWhenPartiallyCompatible
                });
                dpaServerCreationResponse.then(function (response) {
                    _this.taskId = response.taskId;
                    var isIntegrationFinished = function () {
                        _this.dpaServerService.getTaskStatus(response.taskId)
                            .then(function (taskStatus) {
                            switch (taskStatus.result) {
                                case models_1.ConfigureIntegrationResult.Success:
                                    addServerViewModel.dpaServerId = taskStatus.dpaServerId;
                                    deferred.resolve(true);
                                    break;
                                case models_1.ConfigureIntegrationResult.CancelledByRequest:
                                    deferred.resolve(false);
                                    break;
                                case models_1.ConfigureIntegrationResult.PartiallyCompatible:
                                case models_1.ConfigureIntegrationResult.UnsupportedVersion:
                                    _this.compatibilityDialogService
                                        .openCompatibilityDialog(taskStatus.featuresCatalogDiff, taskStatus.result === models_1.ConfigureIntegrationResult.PartiallyCompatible)
                                        .then(function (continueAnyway) {
                                        if (continueAnyway) {
                                            _this.submitAction(dialogResult, addServerViewModel, true).then(function (result) {
                                                deferred.resolve(result);
                                            });
                                        }
                                        else {
                                            _this.taskId = null;
                                            deferred.resolve(false);
                                        }
                                    });
                                    break;
                                case models_1.ConfigureIntegrationResult.InProgress:
                                    setTimeout(function () { isIntegrationFinished(); }, _this.taskFinishedCheckIntervalMs);
                                    break;
                                default:
                                    addServerViewModel.errorMessage =
                                        _this.configureIntegrationResultToTextService.toText(taskStatus.result);
                                    deferred.resolve(false);
                                    break;
                            }
                        }).catch(function (error) {
                            _this.taskId = null;
                            addServerViewModel.errorMessage = _this.getErrorMessage(error);
                            deferred.resolve(false);
                        });
                    };
                    isIntegrationFinished();
                }).catch(function (error) {
                    _this.taskId = null;
                    addServerViewModel.errorMessage = _this.getErrorMessage(error);
                    deferred.resolve(false);
                });
            }
            return deferred.promise;
        };
        this.cancelAction = function (addServerViewModel) {
            if (_this.taskId !== undefined && _this.taskId !== null) {
                addServerViewModel.wasCanceled = true;
                return _this.dpaServerService.cancelIntegration(_this.taskId);
            }
            else {
                return _this.$q.resolve(false);
            }
        };
    }
    AddServerDialogActionsService.prototype.getErrorMessage = function (error) {
        if (error != null && error["data"] != null && error["data"]["message"] != null) {
            return error["data"]["message"];
        }
        else {
            return null;
        }
    };
    return AddServerDialogActionsService;
}());
exports.AddServerDialogActionsService = AddServerDialogActionsService;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AddServerDialogService = /** @class */ (function () {
    /** @ngInject */
    AddServerDialogService.$inject = ["xuiDialogService", "addServerDialogActionsService", "getTextService", "$q"];
    function AddServerDialogService(xuiDialogService, addServerDialogActionsService, getTextService, $q) {
        var _this = this;
        this.xuiDialogService = xuiDialogService;
        this.addServerDialogActionsService = addServerDialogActionsService;
        this.$q = $q;
        this.openAddServerDialog = function () {
            var deferred = _this.$q.defer();
            var addServerViewModel = {
                port: 8124,
                username: "dpa"
            };
            var integrateButton = {
                actionText: _this._t("Integrating ..."),
                name: "integrateButton",
                isPrimary: true,
                cssClass: "integrateButton",
                text: _this._t("Integrate"),
                action: function (dialogResult) {
                    return _this.addServerDialogActionsService.submitAction(dialogResult, addServerViewModel);
                },
                cancelAction: function () {
                    return _this.addServerDialogActionsService.cancelAction(addServerViewModel);
                }
            };
            var dialogPromise = _this.xuiDialogService.showModal({
                templateUrl: "dpa/views/admin/serverManagement/addServer/addServer.html",
                size: "lg"
            }, {
                title: _this._t("Add DPA Server"),
                viewModel: addServerViewModel,
                hideCancel: false,
                cancelButtonText: _this._t("Cancel"),
                buttons: [integrateButton]
            });
            dialogPromise.then(function () {
                deferred.resolve(addServerViewModel);
            });
            return deferred.promise;
        };
        this._t = getTextService;
    }
    return AddServerDialogService;
}());
exports.AddServerDialogService = AddServerDialogService;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var CompatibilityDialogService = /** @class */ (function () {
    /** @ngInject */
    CompatibilityDialogService.$inject = ["xuiDialogService", "getTextService", "$q"];
    function CompatibilityDialogService(xuiDialogService, getTextService, $q) {
        var _this = this;
        this.xuiDialogService = xuiDialogService;
        this.$q = $q;
        this.openCompatibilityDialog = function (featureCatalogDiff, isPartiallyCompatible, showContinueButton) {
            if (showContinueButton === void 0) { showContinueButton = true; }
            var buttons = [];
            var deferred = _this.$q.defer();
            if (isPartiallyCompatible && showContinueButton) {
                var continueButton = {
                    text: _this._t("Continue Anyway"),
                    name: "continueButton",
                    cssClass: "automationContinueButton",
                    displayStyle: "tertiary",
                    action: function () {
                        return true;
                    }
                };
                buttons = [continueButton];
            }
            var compatibilityDialogViewModel = {
                dpaVersionInstalled: featureCatalogDiff.dpaVersionInstalled,
                dpaimVersionInstalled: featureCatalogDiff.dpaimVersionInstalled,
                fullyCompatibleDpaVersion: featureCatalogDiff.fullyCompatibleDpaVersion,
                fullyCompatibleDpaimVersion: featureCatalogDiff.fullyCompatibleDpaimVersion,
                missingFeatures: featureCatalogDiff.missingFeatures,
                isPartiallyCompatible: isPartiallyCompatible
            };
            var dialogResult = _this.xuiDialogService.showModal({
                templateUrl: "dpa/views/admin/serverManagement/compatibility/compatibilityDialog.html",
                size: "lg"
            }, {
                title: compatibilityDialogViewModel.dpaVersionInstalled
                    !== compatibilityDialogViewModel.fullyCompatibleDpaVersion
                    ? _this._t("Newer Version of DPA Available") : _this._t("Newer Version of DPAIM Available"),
                hideTopCancel: true,
                hideCancel: false,
                cancelButtonText: showContinueButton ? _this._t("Cancel") : _this._t("Close"),
                viewModel: compatibilityDialogViewModel,
                buttons: buttons
            });
            dialogResult.then(function (result) {
                if (result === "cancel") {
                    deferred.resolve(false);
                }
                else {
                    deferred.resolve(true);
                }
            });
            return deferred.promise;
        };
        this._t = getTextService;
    }
    return CompatibilityDialogService;
}());
exports.CompatibilityDialogService = CompatibilityDialogService;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(0);
var RemoveServersDialogService = /** @class */ (function () {
    /** @ngInject */
    RemoveServersDialogService.$inject = ["xuiDialogService", "dpaServerService", "$q", "getTextService"];
    function RemoveServersDialogService(xuiDialogService, dpaServerService, $q, getTextService) {
        var _this = this;
        this.xuiDialogService = xuiDialogService;
        this.dpaServerService = dpaServerService;
        this.$q = $q;
        this.action = function (dialogResult, viewModel) {
            var deferred = _this.$q.defer();
            viewModel.isBusy = true;
            viewModel.errorMessage = null;
            var calls = [];
            angular.forEach(viewModel.dpaServerIds, function (serverId) {
                calls.push(_this.dpaServerService.removeDpaServer(serverId).then(function (result) {
                    if (result.disableIntegrationResult === models_1.DisableIntegrationResult.Success) {
                        viewModel.remainingServerIds.splice(viewModel.remainingServerIds.indexOf(serverId), 1);
                        viewModel.removedServerIds.push(serverId);
                    }
                }));
            });
            _this.$q.all(calls).then(function () {
                viewModel.isBusy = false;
                deferred.resolve(true);
            }).catch(function (error) {
                viewModel.errorMessage = _this.getErrorMessage(error);
                deferred.resolve(false);
            });
            return deferred.promise;
        };
        this._t = getTextService;
    }
    RemoveServersDialogService.prototype.getErrorMessage = function (error) {
        if (error != null && error["data"] != null && error["data"]["message"] != null) {
            return error["data"]["message"];
        }
        else {
            return null;
        }
    };
    RemoveServersDialogService.prototype.openRemoveDpaServersDialog = function (dpaServerIds) {
        var _this = this;
        var remainingServers = angular.copy(dpaServerIds);
        var deferred = this.$q.defer();
        var viewModel = {
            isBusy: false,
            dpaServerIds: dpaServerIds,
            remainingServerIds: remainingServers,
            removedServerIds: [],
            canceled: false
        };
        var buttons = [{
                name: "removeButton",
                isPrimary: true,
                cssClass: "removeButton",
                text: this._t("Remove"),
                actionText: this._t("Removing integration..."),
                action: function (dialogResult) {
                    return _this.action(dialogResult, viewModel);
                }
            }];
        var dialogPromise = this.xuiDialogService.showModal({
            size: "small",
            windowClass: "removeServersDialog"
        }, {
            hideTopCancel: true,
            title: this._t("Remove DPA Servers"),
            message: this._t("Are you sure you want to remove integration with selected DPA server(s)? \
You will lose all existing relationships between the Orion Web Console and your DPA server. \
Data on the DPA server will not be affected."),
            actionButtonText: this._t("Remove"),
            cancelButtonText: this._t("Cancel"),
            hideCancel: false,
            buttons: buttons,
            viewModel: viewModel
        });
        dialogPromise.then(function (res) {
            if (!res || res === "cancel") {
                viewModel.canceled = true;
            }
            deferred.resolve(viewModel);
        });
        return deferred.promise;
    };
    return RemoveServersDialogService;
}());
exports.RemoveServersDialogService = RemoveServersDialogService;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(17));
__export(__webpack_require__(1));
__export(__webpack_require__(1));
__export(__webpack_require__(18));
var configureIntegrationResultToTextService_1 = __webpack_require__(17);
var connectionTestResultToTextService_1 = __webpack_require__(1);
var integrationPhaseToTextService_1 = __webpack_require__(18);
exports.default = function (module) {
    // register components
    module.service("configureIntegrationResultToTextService", configureIntegrationResultToTextService_1.ConfigureIntegrationResultToTextService);
    module.service("connectionTestResultToTextService", connectionTestResultToTextService_1.ConnectionTestResultToTextService);
    module.service("integrationPhaseToTextService", integrationPhaseToTextService_1.IntegrationPhaseToTextService);
};


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(0);
var ConfigureIntegrationResultToTextService = /** @class */ (function () {
    /** @ngInject */
    ConfigureIntegrationResultToTextService.$inject = ["getTextService"];
    function ConfigureIntegrationResultToTextService(getTextService) {
        this._t = getTextService;
    }
    ConfigureIntegrationResultToTextService.prototype.toText = function (result) {
        switch (result) {
            case models_1.ConfigureIntegrationResult.GeneralError:
                return this._t("There was an error when trying to enable the integration with the DPA server.");
            case models_1.ConfigureIntegrationResult.InvalidCredentials:
                return this._t("The user or password you entered is incorrect.");
            case models_1.ConfigureIntegrationResult.InsufficientPrivileges:
                return this._t("This account does not have administrator privileges. Use an administrator account.");
            case models_1.ConfigureIntegrationResult.NotResponding:
                return this._t("Cannot find a DPA server at this address or port. Check your DPA server details and \
network connection.");
            case models_1.ConfigureIntegrationResult.SwisFailure:
                return this._t("Unable to connect to the remote instance data provider.");
            case models_1.ConfigureIntegrationResult.UnableToPingBack:
                return this._t("The DPA server failed to connect to the Orion Web Console by its hostname or \
IP address.");
            case models_1.ConfigureIntegrationResult.AlreadyIntegrated:
                return this._t("The DPA server at this address and port has already been integrated. \
To reintegrate, remove the existing integration and try again.");
            case models_1.ConfigureIntegrationResult.ErrorSavingSettings:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Saving jswisAddress, credential, or service account ID failed.");
            case models_1.ConfigureIntegrationResult.EnableFederationFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Establishing federation with jSWIS failed.");
            case models_1.ConfigureIntegrationResult.SubscriptionAlreadyExists:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Subscription already exists.");
            case models_1.ConfigureIntegrationResult.ConnectionToOrionFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Connection to Orion Web Console from DPA failed.");
            case models_1.ConfigureIntegrationResult.DiscoverNodesFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Discovering relationships between Orion Nodes and DPA Database Instances failed.");
            case models_1.ConfigureIntegrationResult.DiscoverServerApplicationsFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Discovering relationships between Orion Aplications (server role) and DPA Database Instances failed.");
            case models_1.ConfigureIntegrationResult.DiscoverClientApplicationsFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Discovering relationships between Orion Aplications (cleint role) and DPA Database Instances failed.");
            case models_1.ConfigureIntegrationResult.SaveMappingsFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Saving discovered relationships failed.");
            case models_1.ConfigureIntegrationResult.StartJSwisSubscriberFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Subscibing to jSwis failed.");
            case models_1.ConfigureIntegrationResult.PublishInstanceCreatedIndicationsFailed:
                return this._t("There was an error when trying to enable the integration with the DPA server: \
Publishing Database Instance indication failed.");
            default:
                return "";
        }
    };
    return ConfigureIntegrationResultToTextService;
}());
exports.ConfigureIntegrationResultToTextService = ConfigureIntegrationResultToTextService;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(0);
var IntegrationPhaseToTextService = /** @class */ (function () {
    /** @ngInject */
    IntegrationPhaseToTextService.$inject = ["getTextService"];
    function IntegrationPhaseToTextService(getTextService) {
        this._t = getTextService;
    }
    IntegrationPhaseToTextService.prototype.toText = function (phase) {
        switch (phase) {
            case models_1.IntegrationPhase.ConnectionTestCommand:
                return this._t("Testing connection...");
            case models_1.IntegrationPhase.CreateIntegrationCommand:
                return this._t("Creating integration in DPA...");
            case models_1.IntegrationPhase.SetJSwisObjectUriBaseCommand:
                return this._t("Setting jSwis object URI...");
            case models_1.IntegrationPhase.CreateServiceAccountInDpaCommnand:
                return this._t("Creating service account in DPA...");
            case models_1.IntegrationPhase.CreateServiceAccountInOrionCommand:
                return this._t("Creating service account in Orion...");
            case models_1.IntegrationPhase.DiscoverClientApplicationRelationshipsCommand:
                return this._t("Discovering client application relationships...");
            case models_1.IntegrationPhase.PublishInstanceCreatedIndicationsCommand:
                return this._t("Publishing indications...");
            case models_1.IntegrationPhase.DiscoverNodeRelationshipsCommand:
                return this._t("Discovering node relationships...");
            case models_1.IntegrationPhase.DiscoverServerApplicationRelationshipsCommand:
                return this._t("Discovering server application relationship...");
            case models_1.IntegrationPhase.EnableFederationCommand:
                return this._t("Enabling federation...");
            case models_1.IntegrationPhase.SaveIntegrationSettingsCommand:
                return this._t("Saving integration settings in Orion...");
            case models_1.IntegrationPhase.InvalidateCachesCommand:
                return this._t("Invalidating cache...");
            case models_1.IntegrationPhase.SaveRelationshipsCommand:
                return this._t("Saving relationships in Orion...");
            case models_1.IntegrationPhase.StartJSwisSubscriberCommand:
                return this._t("Subscribing to DPA server...");
            case models_1.IntegrationPhase.SsoEnableCommand:
                return this._t("Enabling SSO...");
            case models_1.IntegrationPhase.RollbackConnectionTestCommand:
                return this._t("Reverting...");
            case models_1.IntegrationPhase.RollbackCreateIntegrationCommand:
                return this._t("Removing integration from DPA...");
            case models_1.IntegrationPhase.RollbackCreateServiceAccountInDpaCommnand:
                return this._t("Removing service account from DPA...");
            case models_1.IntegrationPhase.RollbackCreateServiceAccountInOrionCommand:
                return this._t("Removing service account from Orion...");
            case models_1.IntegrationPhase.RollbackDiscoverClientApplicationRelationshipsCommand:
                return this._t("Reverting discovering client application relationships...");
            case models_1.IntegrationPhase.RollbackPublishInstanceCreatedIndicationsCommand:
                return this._t("Publishing indications...");
            case models_1.IntegrationPhase.RollbackDiscoverNodeRelationshipsCommand:
                return this._t("Reverting discovering node relationships...");
            case models_1.IntegrationPhase.RollbackDiscoverServerApplicationRelationshipsCommand:
                return this._t("Reverting discovering server application relationship...");
            case models_1.IntegrationPhase.RollbackEnableFederationCommand:
                return this._t("Disabling federation...");
            case models_1.IntegrationPhase.RollbackSaveIntegrationSettingsCommand:
                return this._t("Removing integration settings from Orion...");
            case models_1.IntegrationPhase.RollbackInvalidateCachesCommand:
                return this._t("Invalidating cache...");
            case models_1.IntegrationPhase.RollbackSaveRelationshipsCommand:
                return this._t("Removing relationships from Orion...");
            case models_1.IntegrationPhase.RollbackStartJSwisSubscriberCommand:
                return this._t("Removing subscriptions from DPA server...");
            case models_1.IntegrationPhase.RollbackSsoEnableCommand:
                return this._t("Disabling SSO...");
            case models_1.IntegrationPhase.RollbackSetJSwisObjectUriBaseCommand:
                return this._t("Reverting jSwis object URI...");
            default:
                break;
        }
    };
    return IntegrationPhaseToTextService;
}());
exports.IntegrationPhaseToTextService = IntegrationPhaseToTextService;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AddressService = /** @class */ (function () {
    function AddressService() {
    }
    AddressService.prototype.isIpv4Address = function (value) {
        var validIPv4AddressRegex = new RegExp("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.)"
            + "{3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
        return validIPv4AddressRegex.test(value);
    };
    AddressService.prototype.isIpv6Address = function (value) {
        var validIPv6AddressRegex = new RegExp("^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|"
            + "([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|"
            + "([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4})"
            + "{1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4})"
            + "{1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::"
            + "(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|"
            + "1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.)"
            + "{3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$");
        return validIPv6AddressRegex.test(value);
    };
    AddressService.prototype.isHostname = function (value) {
        var validHostnameRegex = new RegExp("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]"
            + "|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$");
        return validHostnameRegex.test(value);
    };
    AddressService.prototype.isIpAddress = function (value) {
        return this.isIpv4Address(value) || this.isIpv6Address(value);
    };
    return AddressService;
}());
exports.AddressService = AddressService;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var DpaServerService = /** @class */ (function () {
    /** @ngInject */
    DpaServerService.$inject = ["swApi"];
    function DpaServerService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.baseRoute = "dpa/dpaservers";
        this.getDpaServers = function () {
            return _this.swApi.api(false)
                .one(_this.baseRoute)
                .get();
        };
        this.getVersion = function (dpaServerId) {
            return _this.swApi.api(false)
                .one(_this.baseRoute + "/version", dpaServerId)
                .get();
        };
        this.createDpaServer = function (dpaServer) {
            return _this.swApi.api(false)
                .all(_this.baseRoute)
                .post(dpaServer);
        };
        this.getTaskStatus = function (taskId) {
            return _this.swApi.api(false)
                .one(_this.baseRoute + "/task", taskId)
                .get();
        };
        this.removeDpaServer = function (dpaServerId) {
            return _this.swApi.api(false)
                .one(_this.baseRoute, dpaServerId)
                .remove();
        };
        this.cancelIntegration = function (taskId) {
            return _this.swApi.api(false)
                .one(_this.baseRoute + "/task", taskId)
                .remove();
        };
    }
    return DpaServerService;
}());
exports.DpaServerService = DpaServerService;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var TestConnectionService = /** @class */ (function () {
    /** @ngInject */
    TestConnectionService.$inject = ["swApi"];
    function TestConnectionService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.baseRoute = "dpa/testconnection";
        this.testConnection = function (dpaServer) {
            return _this.swApi.api(false)
                .all(_this.baseRoute)
                .post(dpaServer);
        };
        this.getTaskStatus = function (taskId) {
            return _this.swApi.api(false)
                .one(_this.baseRoute + "/task", taskId)
                .get();
        };
        this.cancelTestConnection = function (taskId) {
            return _this.swApi.api(false)
                .one(_this.baseRoute, taskId)
                .remove();
        };
    }
    return TestConnectionService;
}());
exports.TestConnectionService = TestConnectionService;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ExpertAdviceInfoService = /** @class */ (function () {
    /** @ngInject */
    ExpertAdviceInfoService.$inject = ["swApi"];
    function ExpertAdviceInfoService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.baseRoute = "dpa/expertadviceinfo";
        this.getExpertAdviceInfo = function (globalDatabaseInstanceId, waitIdBase64) {
            return _this.swApi.api(false)
                .one(_this.baseRoute + "/" + globalDatabaseInstanceId + "/" + waitIdBase64)
                .get();
        };
    }
    return ExpertAdviceInfoService;
}());
exports.ExpertAdviceInfoService = ExpertAdviceInfoService;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SqlQueryService = /** @class */ (function () {
    /** @ngInject */
    SqlQueryService.$inject = ["swApi"];
    function SqlQueryService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.baseRoute = "dpa/sqlquery";
        this.getSqlQueryInfo = function (globalDatabaseInstanceId, sqlHash) {
            return _this.swApi.api(false)
                .one(_this.baseRoute + "/" + globalDatabaseInstanceId + "/" + sqlHash)
                .get();
        };
    }
    return SqlQueryService;
}());
exports.SqlQueryService = SqlQueryService;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var RedirectService = /** @class */ (function () {
    /** @ngInject */
    RedirectService.$inject = ["$window"];
    function RedirectService($window) {
        var _this = this;
        this.$window = $window;
        this.baseRoute = "dpa/sqlquery";
        this.redirectTo = function (absoluteUrlPath) {
            var targetUrl = _this.$window.location.origin + absoluteUrlPath;
            _this.$window.location.href = targetUrl;
        };
        this.openInNewTab = function (absoluteUrlPath) {
            var targetUrl = _this.$window.location.origin + absoluteUrlPath;
            _this.$window.open(targetUrl, "_blank");
        };
    }
    return RedirectService;
}());
exports.RedirectService = RedirectService;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var PortValidator = /** @class */ (function () {
    function PortValidator() {
        this.require = "ngModel";
        this.restrict = "A";
        this.link = function ($scope, element, attrs, controller) {
            if (!controller) {
                return;
            }
            controller.$validators["port"] = function (modelValue, viewValue) {
                var isValid = viewValue != null;
                if (isValid) {
                    var portNumber = +viewValue;
                    isValid = portNumber !== NaN && portNumber >= 1 && portNumber <= 65535;
                }
                controller.$setValidity("port", isValid);
                return isValid;
            };
        };
    }
    return PortValidator;
}());
exports.default = PortValidator;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


orionStatusIcon.$inject = ["$filter"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function orionStatusIcon($filter) {
    return function (orionStatus) {
        switch (orionStatus) {
            case 0:
                return "unknown";
            case 1:
                return "up";
            case 2:
                return "down";
            case 3:
                return "warning";
            case 14:
                return "critical";
            default:
                return "unknown";
        }
    };
}
exports.default = orionStatusIcon;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(28);
module.exports = __webpack_require__(29);


/***/ }),
/* 28 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(30);
var config_1 = __webpack_require__(31);
var index_1 = __webpack_require__(32);
var index_2 = __webpack_require__(33);
var index_3 = __webpack_require__(60);
var index_4 = __webpack_require__(10);
var templates_1 = __webpack_require__(61);
var index_5 = __webpack_require__(69);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);
index_5.default(app_1.default);


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("dpa.services", []);
angular.module("dpa.templates", []);
angular.module("dpa.components", []);
angular.module("dpa.filters", []);
angular.module("dpa.providers", []);
angular.module("dpa", [
    "orion",
    "dpa.services",
    "dpa.templates",
    "dpa.components",
    "dpa.filters",
    "dpa.providers"
]);
// create and register Xui (Orion) module wrapper
var dpa = Xui.registerModule("dpa");
exports.default = dpa;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: dpa");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(2));
__export(__webpack_require__(3));
__export(__webpack_require__(4));
var constants_1 = __webpack_require__(2);
var links_1 = __webpack_require__(3);
var linksToDpa_1 = __webpack_require__(4);
exports.default = function (module) {
    module.service("dpaConstants", constants_1.Constants);
    module.service("dpaLinks", links_1.Links);
    module.service("linksToDpa", linksToDpa_1.LinksToDpa);
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var serverManagement_1 = __webpack_require__(34);
var expertAdviceInfo_1 = __webpack_require__(54);
var sqlDetail_1 = __webpack_require__(57);
exports.default = function (module) {
    // register views
    serverManagement_1.default(module);
    expertAdviceInfo_1.default(module);
    sqlDetail_1.default(module);
};
var rootState = function ($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'dpa/newPage' url
    $stateProvider.state("dpa", {
        url: "/dpa",
        controller: function ($state) {
            $state.go("home");
        }
    });
};


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var addServer_1 = __webpack_require__(35);
var compatibility_1 = __webpack_require__(47);
var serverManagement_controller_1 = __webpack_require__(50);
var serverManagement_config_1 = __webpack_require__(51);
var serverListViewItem_controller_1 = __webpack_require__(52);
__webpack_require__(53);
exports.default = function (module) {
    addServer_1.default(module);
    compatibility_1.default(module);
    // use module prefix to avoid collisions in global namespace
    module.controller("dpaServerManagementController", serverManagement_controller_1.default);
    module.controller("dpaServerListViewItemController", serverListViewItem_controller_1.ServerListViewItemController);
    module.config(serverManagement_config_1.default);
};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var addServer_controller_1 = __webpack_require__(36);
exports.default = function (module) {
    // use module prefix to avoid collisions in global namespace
    module.controller("dpaAddServerController", addServer_controller_1.AddServerController);
};


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(0);
var AddServerController = /** @class */ (function () {
    /** @ngInject */
    AddServerController.$inject = ["$log", "$scope", "testConnectionService", "connectionTestResultToTextService", "getTextService", "$q"];
    function AddServerController($log, $scope, testConnectionService, connectionTestResultToTextService, getTextService, $q) {
        this.$log = $log;
        this.$scope = $scope;
        this.testConnectionService = testConnectionService;
        this.connectionTestResultToTextService = connectionTestResultToTextService;
        this.getTextService = getTextService;
        this.$q = $q;
    }
    AddServerController.prototype.$onInit = function () {
        this._t = this.getTextService;
        this.taskFinishedCheckIntervalMs = 500;
    };
    AddServerController.prototype.activate = function (vm) {
        var _this = this;
        this.viewModel = vm.dialogOptions.viewModel;
        if (vm["modalDialog"] != null) {
            this.form = vm["modalDialog"];
        }
        else {
            this.$log.error("vm.modalDialog is null");
            throw Error("vm does not contain modalDialog");
        }
        this.viewModel.isValid = function () {
            _this.viewModel.errorMessage = null;
            _this.testConnectionSuccessMessage = null;
            // workaround for UIF-4236
            angular.forEach(_this.form.$error.required, function (field) {
                field.$setDirty();
            });
            return _this.form.$valid;
        };
        this.$scope.$watch(function () {
            return _this.viewModel.address;
        }, function (newValue, oldValue, scope) {
            _this.formatDisplayName(newValue, _this.viewModel.port);
        });
        this.$scope.$watch(function () {
            return _this.viewModel.port;
        }, function (newValue, oldValue, scope) {
            _this.formatDisplayName(_this.viewModel.address, newValue);
        });
    };
    AddServerController.prototype.formatDisplayName = function (hostname, port) {
        if (!this.isCustomName) {
            if (hostname != null) {
                this.viewModel.name = hostname;
                if (port != null) {
                    if (this.viewModel.name != null && this.viewModel.name.length > 0 && hostname != null) {
                        this.viewModel.name += ":" + port;
                    }
                    else {
                        this.viewModel.name = port.toString();
                    }
                }
            }
        }
    };
    AddServerController.prototype.nameChanged = function () {
        this.isCustomName = true;
    };
    AddServerController.prototype.testConnection = function () {
        var _this = this;
        var deferVoid = this.$q.defer();
        this.viewModel.errorMessage = null;
        this.testConnectionSuccessMessage = null;
        if (this.viewModel.isValid()) {
            this.testingConnection = true;
            this.testConnectionService.testConnection({
                address: this.viewModel.address,
                port: this.viewModel.port,
                name: this.viewModel.name,
                orionAddress: this.viewModel.orionAddress,
                username: this.viewModel.username,
                password: this.viewModel.password
            }).then(function (result) {
                _this.testConnectionTaskId = result.taskId;
                _this.checkTestConnectionStatus().then(function () {
                    deferVoid.resolve();
                });
            }).catch(function (error) {
                _this.testingConnection = false;
                deferVoid.resolve();
            });
        }
        return deferVoid.promise;
    };
    AddServerController.prototype.checkTestConnectionStatus = function () {
        var _this = this;
        var deferVoid = this.$q.defer();
        this.testConnectionService.getTaskStatus(this.testConnectionTaskId)
            .then(function (response) {
            if (response.result === models_1.ConnectionTestResult.inProgress && _this.testingConnection) {
                setTimeout(function () { _this.checkTestConnectionStatus(); }, _this.taskFinishedCheckIntervalMs);
            }
            else {
                _this.testingConnection = false;
                if (response.result === models_1.ConnectionTestResult.partiallyCompatible
                    || response.result === models_1.ConnectionTestResult.unsupportedVersion) {
                    response.result = models_1.ConnectionTestResult.success;
                }
                var message = _this.connectionTestResultToTextService.toText(response.result);
                if (response.result === models_1.ConnectionTestResult.success) {
                    _this.testConnectionSuccessMessage = message;
                }
                else {
                    _this.viewModel.errorMessage = message;
                }
                deferVoid.resolve();
            }
        }).catch(function (error) {
            _this.testingConnection = false;
            deferVoid.resolve();
        });
        return deferVoid.promise;
    };
    ;
    AddServerController.prototype.cancelTestConnection = function () {
        this.testConnectionService.cancelTestConnection(this.testConnectionTaskId);
        this.testingConnection = false;
    };
    return AddServerController;
}());
exports.AddServerController = AddServerController;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(38));
__export(__webpack_require__(39));
__export(__webpack_require__(40));
__export(__webpack_require__(41));
__export(__webpack_require__(42));
__export(__webpack_require__(43));
__export(__webpack_require__(44));


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ConfigureIntegrationResult;
(function (ConfigureIntegrationResult) {
    ConfigureIntegrationResult[ConfigureIntegrationResult["Success"] = 0] = "Success";
    ConfigureIntegrationResult[ConfigureIntegrationResult["InProgress"] = 1] = "InProgress";
    ConfigureIntegrationResult[ConfigureIntegrationResult["CancelledByRequest"] = 2] = "CancelledByRequest";
    ConfigureIntegrationResult[ConfigureIntegrationResult["GeneralError"] = 3] = "GeneralError";
    ConfigureIntegrationResult[ConfigureIntegrationResult["InvalidCredentials"] = 4] = "InvalidCredentials";
    ConfigureIntegrationResult[ConfigureIntegrationResult["InsufficientPrivileges"] = 5] = "InsufficientPrivileges";
    ConfigureIntegrationResult[ConfigureIntegrationResult["NotResponding"] = 6] = "NotResponding";
    ConfigureIntegrationResult[ConfigureIntegrationResult["UnsupportedVersion"] = 7] = "UnsupportedVersion";
    ConfigureIntegrationResult[ConfigureIntegrationResult["SwisFailure"] = 8] = "SwisFailure";
    ConfigureIntegrationResult[ConfigureIntegrationResult["UnableToPingBack"] = 9] = "UnableToPingBack";
    ConfigureIntegrationResult[ConfigureIntegrationResult["AlreadyIntegrated"] = 10] = "AlreadyIntegrated";
    ConfigureIntegrationResult[ConfigureIntegrationResult["PartiallyCompatible"] = 101] = "PartiallyCompatible";
    ConfigureIntegrationResult[ConfigureIntegrationResult["ErrorSavingSettings"] = 11] = "ErrorSavingSettings";
    ConfigureIntegrationResult[ConfigureIntegrationResult["EnableFederationFailed"] = 12] = "EnableFederationFailed";
    ConfigureIntegrationResult[ConfigureIntegrationResult["SubscriptionAlreadyExists"] = 13] = "SubscriptionAlreadyExists";
    ConfigureIntegrationResult[ConfigureIntegrationResult["ConnectionToOrionFailed"] = 14] = "ConnectionToOrionFailed";
    ConfigureIntegrationResult[ConfigureIntegrationResult["DiscoverNodesFailed"] = 15] = "DiscoverNodesFailed";
    ConfigureIntegrationResult[ConfigureIntegrationResult["DiscoverServerApplicationsFailed"] = 16] = "DiscoverServerApplicationsFailed";
    ConfigureIntegrationResult[ConfigureIntegrationResult["DiscoverClientApplicationsFailed"] = 17] = "DiscoverClientApplicationsFailed";
    ConfigureIntegrationResult[ConfigureIntegrationResult["SaveMappingsFailed"] = 18] = "SaveMappingsFailed";
    ConfigureIntegrationResult[ConfigureIntegrationResult["StartJSwisSubscriberFailed"] = 19] = "StartJSwisSubscriberFailed";
    ConfigureIntegrationResult[ConfigureIntegrationResult["PublishInstanceCreatedIndicationsFailed"] = 20] = "PublishInstanceCreatedIndicationsFailed";
})(ConfigureIntegrationResult = exports.ConfigureIntegrationResult || (exports.ConfigureIntegrationResult = {}));


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var DisableIntegrationResult;
(function (DisableIntegrationResult) {
    DisableIntegrationResult[DisableIntegrationResult["Success"] = 0] = "Success";
    DisableIntegrationResult[DisableIntegrationResult["GeneralError"] = 1] = "GeneralError";
    DisableIntegrationResult[DisableIntegrationResult["JSwisFailure"] = 2] = "JSwisFailure";
    DisableIntegrationResult[DisableIntegrationResult["ErrorSavingSettings"] = 3] = "ErrorSavingSettings";
    DisableIntegrationResult[DisableIntegrationResult["IsNotIntegratedError"] = 4] = "IsNotIntegratedError";
    DisableIntegrationResult[DisableIntegrationResult["InvalidCredentials"] = 5] = "InvalidCredentials";
    DisableIntegrationResult[DisableIntegrationResult["NotResponding"] = 6] = "NotResponding";
    DisableIntegrationResult[DisableIntegrationResult["InsufficientPrivileges"] = 7] = "InsufficientPrivileges";
    DisableIntegrationResult[DisableIntegrationResult["ClearMappingsFailed"] = 8] = "ClearMappingsFailed";
    DisableIntegrationResult[DisableIntegrationResult["PublishInstanceDeletedIndicationsFailed"] = 9] = "PublishInstanceDeletedIndicationsFailed";
})(DisableIntegrationResult = exports.DisableIntegrationResult || (exports.DisableIntegrationResult = {}));


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var DpaServerCreationRequest = /** @class */ (function () {
    function DpaServerCreationRequest() {
    }
    return DpaServerCreationRequest;
}());
exports.DpaServerCreationRequest = DpaServerCreationRequest;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var DpaServerCreationResponse = /** @class */ (function () {
    function DpaServerCreationResponse() {
    }
    return DpaServerCreationResponse;
}());
exports.DpaServerCreationResponse = DpaServerCreationResponse;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var DpaServerRemovalResponse = /** @class */ (function () {
    function DpaServerRemovalResponse() {
    }
    return DpaServerRemovalResponse;
}());
exports.DpaServerRemovalResponse = DpaServerRemovalResponse;


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var IntegrationPhase;
(function (IntegrationPhase) {
    IntegrationPhase[IntegrationPhase["ConnectionTestCommand"] = 0] = "ConnectionTestCommand";
    IntegrationPhase[IntegrationPhase["CreateServiceAccountInDpaCommnand"] = 1] = "CreateServiceAccountInDpaCommnand";
    IntegrationPhase[IntegrationPhase["CreateServiceAccountInOrionCommand"] = 2] = "CreateServiceAccountInOrionCommand";
    IntegrationPhase[IntegrationPhase["CreateIntegrationCommand"] = 3] = "CreateIntegrationCommand";
    IntegrationPhase[IntegrationPhase["SetJSwisObjectUriBaseCommand"] = 4] = "SetJSwisObjectUriBaseCommand";
    IntegrationPhase[IntegrationPhase["SaveIntegrationSettingsCommand"] = 5] = "SaveIntegrationSettingsCommand";
    IntegrationPhase[IntegrationPhase["InvalidateCachesCommand"] = 6] = "InvalidateCachesCommand";
    IntegrationPhase[IntegrationPhase["EnableFederationCommand"] = 7] = "EnableFederationCommand";
    IntegrationPhase[IntegrationPhase["SsoEnableCommand"] = 8] = "SsoEnableCommand";
    IntegrationPhase[IntegrationPhase["PublishInstanceCreatedIndicationsCommand"] = 9] = "PublishInstanceCreatedIndicationsCommand";
    IntegrationPhase[IntegrationPhase["DiscoverNodeRelationshipsCommand"] = 10] = "DiscoverNodeRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["DiscoverServerApplicationRelationshipsCommand"] = 11] = "DiscoverServerApplicationRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["DiscoverClientApplicationRelationshipsCommand"] = 12] = "DiscoverClientApplicationRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["SaveRelationshipsCommand"] = 13] = "SaveRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["StartJSwisSubscriberCommand"] = 14] = "StartJSwisSubscriberCommand";
    // Rollback
    IntegrationPhase[IntegrationPhase["RollbackConnectionTestCommand"] = 15] = "RollbackConnectionTestCommand";
    IntegrationPhase[IntegrationPhase["RollbackCreateServiceAccountInDpaCommnand"] = 16] = "RollbackCreateServiceAccountInDpaCommnand";
    IntegrationPhase[IntegrationPhase["RollbackCreateServiceAccountInOrionCommand"] = 17] = "RollbackCreateServiceAccountInOrionCommand";
    IntegrationPhase[IntegrationPhase["RollbackCreateIntegrationCommand"] = 18] = "RollbackCreateIntegrationCommand";
    IntegrationPhase[IntegrationPhase["RollbackSetJSwisObjectUriBaseCommand"] = 19] = "RollbackSetJSwisObjectUriBaseCommand";
    IntegrationPhase[IntegrationPhase["RollbackSaveIntegrationSettingsCommand"] = 20] = "RollbackSaveIntegrationSettingsCommand";
    IntegrationPhase[IntegrationPhase["RollbackInvalidateCachesCommand"] = 21] = "RollbackInvalidateCachesCommand";
    IntegrationPhase[IntegrationPhase["RollbackEnableFederationCommand"] = 22] = "RollbackEnableFederationCommand";
    IntegrationPhase[IntegrationPhase["RollbackSsoEnableCommand"] = 23] = "RollbackSsoEnableCommand";
    IntegrationPhase[IntegrationPhase["RollbackPublishInstanceCreatedIndicationsCommand"] = 24] = "RollbackPublishInstanceCreatedIndicationsCommand";
    IntegrationPhase[IntegrationPhase["RollbackDiscoverNodeRelationshipsCommand"] = 25] = "RollbackDiscoverNodeRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["RollbackDiscoverServerApplicationRelationshipsCommand"] = 26] = "RollbackDiscoverServerApplicationRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["RollbackDiscoverClientApplicationRelationshipsCommand"] = 27] = "RollbackDiscoverClientApplicationRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["RollbackSaveRelationshipsCommand"] = 28] = "RollbackSaveRelationshipsCommand";
    IntegrationPhase[IntegrationPhase["RollbackStartJSwisSubscriberCommand"] = 29] = "RollbackStartJSwisSubscriberCommand";
})(IntegrationPhase = exports.IntegrationPhase || (exports.IntegrationPhase = {}));


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var IntegrationStatus;
(function (IntegrationStatus) {
    IntegrationStatus[IntegrationStatus["unknown"] = 0] = "unknown";
    // When DPA Server Status = Up
    IntegrationStatus[IntegrationStatus["success"] = 1] = "success";
    IntegrationStatus[IntegrationStatus["partiallyCompatible"] = 106] = "partiallyCompatible";
    // Down
    IntegrationStatus[IntegrationStatus["notResponding"] = 102] = "notResponding";
    // When DPA Server Status = Unknown
    IntegrationStatus[IntegrationStatus["invalidCredentials"] = 101] = "invalidCredentials";
    IntegrationStatus[IntegrationStatus["connectionToOrionFailed"] = 103] = "connectionToOrionFailed";
    IntegrationStatus[IntegrationStatus["unsupportedVersion"] = 104] = "unsupportedVersion";
    IntegrationStatus[IntegrationStatus["orionInvalidCredentials"] = 105] = "orionInvalidCredentials";
})(IntegrationStatus = exports.IntegrationStatus || (exports.IntegrationStatus = {}));


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(46));


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ConnectionTestResult;
(function (ConnectionTestResult) {
    ConnectionTestResult[ConnectionTestResult["success"] = 0] = "success";
    ConnectionTestResult[ConnectionTestResult["inProgress"] = 1] = "inProgress";
    ConnectionTestResult[ConnectionTestResult["cancelledByRequest"] = 2] = "cancelledByRequest";
    ConnectionTestResult[ConnectionTestResult["generalError"] = 3] = "generalError";
    ConnectionTestResult[ConnectionTestResult["invalidCredentials"] = 4] = "invalidCredentials";
    ConnectionTestResult[ConnectionTestResult["insufficientPrivileges"] = 5] = "insufficientPrivileges";
    ConnectionTestResult[ConnectionTestResult["notResponding"] = 6] = "notResponding";
    ConnectionTestResult[ConnectionTestResult["unsupportedVersion"] = 7] = "unsupportedVersion";
    ConnectionTestResult[ConnectionTestResult["swisFailure"] = 8] = "swisFailure";
    ConnectionTestResult[ConnectionTestResult["unableToPingBack"] = 9] = "unableToPingBack";
    ConnectionTestResult[ConnectionTestResult["alreadyIntegrated"] = 10] = "alreadyIntegrated";
    ConnectionTestResult[ConnectionTestResult["partiallyCompatible"] = 101] = "partiallyCompatible";
})(ConnectionTestResult = exports.ConnectionTestResult || (exports.ConnectionTestResult = {}));


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var compatibilityDialog_controller_1 = __webpack_require__(48);
__webpack_require__(49);
exports.default = function (module) {
    // use module prefix to avoid collisions in global namespace
    module.controller("dpaCompatibilityDialogController", compatibilityDialog_controller_1.CompatibilityDialogController);
};


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var CompatibilityDialogController = /** @class */ (function () {
    /** @ngInject */
    CompatibilityDialogController.$inject = ["$scope", "dpaLinks", "getTextService"];
    function CompatibilityDialogController($scope, dpaLinks, getTextService) {
        this.$scope = $scope;
        this.dpaLinks = dpaLinks;
        this.getTextService = getTextService;
    }
    CompatibilityDialogController.prototype.$onInit = function () {
        this._t = this.getTextService;
    };
    CompatibilityDialogController.prototype.activate = function (vm) {
        var compatibilityDialogViewModel = vm.dialogOptions.viewModel;
        var featureListText = this._t("New Features:");
        var featureListTemplate = "dpa/views/admin/serverManagement/compatibility/featureList.html";
        if (compatibilityDialogViewModel
            .dpaVersionInstalled !==
            compatibilityDialogViewModel.fullyCompatibleDpaVersion) {
            this.dpaToUpgrade = true;
            this.productToUpgrade = {
                serverName: this._t("DPA Server"),
                name: this._t("Database Performance Analyzer"),
                installedVersion: compatibilityDialogViewModel.dpaVersionInstalled,
                supportedVersion: compatibilityDialogViewModel.fullyCompatibleDpaVersion,
                featureList: compatibilityDialogViewModel.missingFeatures,
                featureListText: featureListText,
                featureListTemplate: featureListTemplate,
                releaseNotesLink: this.dpaLinks.dpaReleaseNotesLink,
                downloadLink: this.dpaLinks.dpaProduct,
                isPartiallyCompatible: compatibilityDialogViewModel.isPartiallyCompatible
            };
        }
        else {
            this.dpaToUpgrade = false;
            this.productToUpgrade = {
                serverName: this._t("Orion"),
                name: this._t("DPA Integration Module"),
                installedVersion: compatibilityDialogViewModel.dpaimVersionInstalled,
                supportedVersion: compatibilityDialogViewModel.fullyCompatibleDpaimVersion,
                featureList: compatibilityDialogViewModel.missingFeatures,
                featureListText: featureListText,
                featureListTemplate: featureListTemplate,
                releaseNotesLink: this.dpaLinks.dpaimReleaseNotesLink,
                downloadLink: this.dpaLinks.dpaimProduct,
                isPartiallyCompatible: compatibilityDialogViewModel.isPartiallyCompatible
            };
        }
    };
    return CompatibilityDialogController;
}());
exports.CompatibilityDialogController = CompatibilityDialogController;


/***/ }),
/* 49 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ServerManagementController = /** @class */ (function () {
    /** @ngInject */
    ServerManagementController.$inject = ["$scope", "$log", "dpaServerService", "$window", "dpaLinks", "getTextService", "addServerDialogService", "$q", "xuiToastService", "$timeout", "removeServersDialogService"];
    function ServerManagementController($scope, $log, dpaServerService, $window, dpaLinks, getTextService, addServerDialogService, $q, xuiToastService, $timeout, removeServersDialogService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.dpaServerService = dpaServerService;
        this.$window = $window;
        this.dpaLinks = dpaLinks;
        this.getTextService = getTextService;
        this.addServerDialogService = addServerDialogService;
        this.$q = $q;
        this.xuiToastService = xuiToastService;
        this.$timeout = $timeout;
        this.removeServersDialogService = removeServersDialogService;
        this.openAddServerDialog = function () {
            var dialogPromise = _this.addServerDialogService.openAddServerDialog();
            dialogPromise.then(function (addServerViewModel) {
                if (addServerViewModel.wasCanceled) {
                    _this.xuiToastService.info(_this._t("Integration with server") + " " +
                        addServerViewModel.name + " " +
                        _this._t("was cancelled."));
                }
                else if (addServerViewModel.dpaServerId != null) {
                    _this.loadDpaServers().then(function () {
                        var serverName = addServerViewModel.name;
                        var toastMessage = _this._t("Server") + " " + serverName + " " + _this._t("successfully added.");
                        var toastTitle = _this._t("DPA Server added.");
                        var dpaServerToHighlight = _.find(_this.dpaServers, function (dpaServer) {
                            return dpaServer.dpaServerId === addServerViewModel.dpaServerId;
                        });
                        // run it asynchronously as we need updated model in listview
                        _this.$timeout(function () {
                            _this.xuiToastService.success(toastMessage, toastTitle, {}, [dpaServerToHighlight]);
                        });
                    });
                }
            });
        };
    }
    ServerManagementController.prototype.$onInit = function () {
        this.isPageBusy = true;
        this._t = this.getTextService;
        this.emptyViewModel = {
            openAddServerDialog: this.openAddServerDialog
        };
        this.emptyData = {
            templateUrl: "dpa/views/admin/serverManagement/dpa-server-listview-empty.html",
            viewModel: this.emptyViewModel
        };
        this.options = {
            hideSearch: true,
            hidePagination: true
        };
        this.pagination = {
            page: 1,
            pageSize: 1000
        };
        this.sorting = {
            sortableColumns: [{
                    id: "name",
                    label: this._t("Name")
                }],
            sortBy: {
                id: "name",
                label: this._t("By name")
            },
            direction: "asc"
        };
    };
    ServerManagementController.prototype.activate = function () {
        this.loadDpaServers();
    };
    ServerManagementController.prototype.loadDpaServers = function () {
        var _this = this;
        var deferred = this.$q.defer();
        this.isPageBusy = true;
        this.dpaServerService.getDpaServers().then(function (result) {
            _this.isPageBusy = false;
            _this.dpaServers = result;
            var dpaServers = angular.copy(result);
            var loadVersionPromises = [];
            var _loop_1 = function (dpaServer) {
                if (dpaServer.featuresCatalogDiff != null
                    && dpaServer.featuresCatalogDiff.dpaVersionInstalled != null) {
                    dpaServer.version = dpaServer.featuresCatalogDiff.dpaVersionInstalled;
                }
                else {
                    dpaServer.version = _this._t("Loading ...");
                    var versionDeferred_1 = _this.$q.defer();
                    loadVersionPromises.push(versionDeferred_1.promise);
                    _this.dpaServerService.getVersion(dpaServer.dpaServerId)
                        .then(function (version) {
                        dpaServer.version = version ? version : _this._t("Cannot load data.");
                        _this.dpaServers = angular.copy(dpaServers);
                        versionDeferred_1.resolve();
                    });
                }
                _this.dpaServers = angular.copy(dpaServers);
            };
            for (var _i = 0, dpaServers_1 = dpaServers; _i < dpaServers_1.length; _i++) {
                var dpaServer = dpaServers_1[_i];
                _loop_1(dpaServer);
            }
            if (loadVersionPromises.length > 0) {
                _this.$q.all(loadVersionPromises).then(function () {
                    deferred.resolve();
                });
            }
            else {
                deferred.resolve();
            }
        });
        return deferred.promise;
    };
    ServerManagementController.prototype.goToRelationshipManagement = function () {
        var targetUrl = this.$window.location.origin + this.dpaLinks.relationshipManagement;
        this.$window.location.href = targetUrl;
    };
    ServerManagementController.prototype.goToDatabasesSummary = function () {
        var targetUrl = this.$window.location.origin + this.dpaLinks.databasesSummary;
        this.$window.location.href = targetUrl;
    };
    ServerManagementController.prototype.removeDpaServers = function (dpaServers) {
        var _this = this;
        var dialogPromise = this.removeServersDialogService.openRemoveDpaServersDialog(dpaServers);
        dialogPromise.then(function (viewModel) {
            if (viewModel.canceled) {
                return;
            }
            if (viewModel.errorMessage != null) {
                _this.xuiToastService.error(_this._t("Disabling integration failed"), viewModel.errorMessage);
            }
            else if (viewModel.remainingServerIds.length > 0) {
                if (viewModel.removedServerIds.length > 0) {
                    var remainingServerNames_1 = [];
                    angular.forEach(viewModel.remainingServerIds, function (serverId) {
                        remainingServerNames_1.push(_this.dpaServers
                            .filter(function (server) { return server.dpaServerId === serverId; })[0]
                            .name);
                    });
                    var removedServerNames_1 = [];
                    angular.forEach(viewModel.removedServerIds, function (serverId) {
                        removedServerNames_1.push(_this.dpaServers.filter(function (server) { return server.dpaServerId === serverId; })[0]
                            .name);
                    });
                    _this.xuiToastService.warning(_this._t("Integration with some DPA server(s) was removed"), _this._t("Successfully removed integration with server(s):") + " " +
                        removedServerNames_1.join(", ") + ". " +
                        _this._t("Failed to remove integration with server(s):") + " " +
                        remainingServerNames_1.join(", ") +
                        ".");
                }
                else {
                    _this.xuiToastService.error(_this._t("Integration with DPA server(s) was not removed"), _this._t("Disabling integration failed"));
                }
            }
            else {
                _this.xuiToastService.success(_this._t("Integration with DPA server(s) was removed"), _this._t("Disabling integration successful"));
            }
            _this.loadDpaServers();
            _this.selection = viewModel.remainingServerIds;
        });
    };
    return ServerManagementController;
}());
exports.default = ServerManagementController;


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
ServerManagementPageConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
// config is a function and not a class !!!
/** @ngInject */
function ServerManagementPageConfig($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'spark/entities' url
    // use controller name used in 'index.ts'
    $stateProvider.state("serverManagement", {
        url: "/dpa/admin/serverManagement",
        controller: "dpaServerManagementController",
        controllerAs: "vm",
        template: __webpack_require__(5)
    });
}
exports.default = ServerManagementPageConfig;


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ServerListViewItemController = /** @class */ (function () {
    /** @ngInject */
    ServerListViewItemController.$inject = ["dpaLinks", "dpaAddressService", "compatibilityDialogService"];
    function ServerListViewItemController(dpaLinks, dpaAddressService, compatibilityDialogService) {
        this.dpaLinks = dpaLinks;
        this.dpaAddressService = dpaAddressService;
        this.compatibilityDialogService = compatibilityDialogService;
    }
    ServerListViewItemController.prototype.openCompatibilityDialog = function (featuresCatalogDiff, isPartiallyCompatible) {
        this.compatibilityDialogService.openCompatibilityDialog(featuresCatalogDiff, isPartiallyCompatible, false);
        return false;
    };
    return ServerListViewItemController;
}());
exports.ServerListViewItemController = ServerListViewItemController;


/***/ }),
/* 53 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var expertAdviceInfo_config_1 = __webpack_require__(55);
var expertAdviceInfo_controller_1 = __webpack_require__(56);
exports.default = function (module) {
    // use module prefix to avoid collisions in global namespace
    module.controller("dpaExpertAdviceInfoController", expertAdviceInfo_controller_1.default);
    module.config(expertAdviceInfo_config_1.default);
};


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
ExpertAdviceInfoConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function ExpertAdviceInfoConfig($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'spark/entities' url
    // use controller name used in 'index.ts'
    $stateProvider.state("expertAdviceInfo", {
        url: "/dpa/expertAdviceInfo/:globalDatabaseInstanceId/:waitId",
        controller: "dpaExpertAdviceInfoController",
        controllerAs: "vm",
        template: __webpack_require__(6)
    });
}
exports.default = ExpertAdviceInfoConfig;


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ExpertAdviceInfoController = /** @class */ (function () {
    /** @ngInject */
    ExpertAdviceInfoController.$inject = ["$stateParams", "expertAdviceInfoService"];
    function ExpertAdviceInfoController($stateParams, expertAdviceInfoService) {
        this.$stateParams = $stateParams;
        this.expertAdviceInfoService = expertAdviceInfoService;
    }
    ExpertAdviceInfoController.prototype.$onInit = function () {
        this.isPageBusy = true;
        this.globalDatabaseInstanceId = +this.$stateParams["globalDatabaseInstanceId"];
        this.waitIdBase64Encoded = this.$stateParams["waitId"];
        this.waitId = atob(this.waitIdBase64Encoded);
    };
    ExpertAdviceInfoController.prototype.activate = function () {
        var _this = this;
        this.expertAdviceInfoService.getExpertAdviceInfo(this.globalDatabaseInstanceId, this.waitIdBase64Encoded)
            .then(function (text) {
            _this.isPageBusy = false;
            _this.htmlHelpContent = text;
        });
    };
    return ExpertAdviceInfoController;
}());
exports.default = ExpertAdviceInfoController;


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var sqlDetail_config_1 = __webpack_require__(58);
var sqlDetail_controller_1 = __webpack_require__(59);
exports.default = function (module) {
    // use module prefix to avoid collisions in global namespace
    module.controller("dpaSqlDetailController", sqlDetail_controller_1.default);
    module.config(sqlDetail_config_1.default);
};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
SqlDetailInfoConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function SqlDetailInfoConfig($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'spark/entities' url
    // use controller name used in 'index.ts'
    $stateProvider.state("sqlDetail", {
        url: "/dpa/sqlDetail/:globalDatabaseInstanceId/:sqlHash",
        controller: "dpaSqlDetailController",
        controllerAs: "vm",
        template: __webpack_require__(7)
    });
}
exports.default = SqlDetailInfoConfig;


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SqlDetailController = /** @class */ (function () {
    /** @ngInject */
    SqlDetailController.$inject = ["$stateParams", "sqlQueryService", "getTextService", "redirectService", "linksToDpa"];
    function SqlDetailController($stateParams, sqlQueryService, getTextService, redirectService, linksToDpa) {
        this.$stateParams = $stateParams;
        this.sqlQueryService = sqlQueryService;
        this.getTextService = getTextService;
        this.redirectService = redirectService;
        this.linksToDpa = linksToDpa;
    }
    SqlDetailController.prototype.$onInit = function () {
        this.isPageBusy = true;
        this._t = this.getTextService;
        this.globalDatabaseInstanceId = +this.$stateParams["globalDatabaseInstanceId"];
        this.sqlHash = this.$stateParams["sqlHash"];
    };
    SqlDetailController.prototype.activate = function () {
        var _this = this;
        this.sqlQueryService.getSqlQueryInfo(this.globalDatabaseInstanceId, this.sqlHash)
            .then(function (sqlQueryDetail) {
            _this.isPageBusy = false;
            _this.sqlName = _this._t("SQL Detail:") + " " + sqlQueryDetail.name;
            _this.sqlText = sqlQueryDetail.text;
        });
    };
    SqlDetailController.prototype.viewInDpa = function () {
        this.redirectService.openInNewTab(this.linksToDpa.getSqlQueryDetailLink(this.globalDatabaseInstanceId, this.sqlHash));
    };
    return SqlDetailController;
}());
exports.default = SqlDetailController;


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(8));
var validators_1 = __webpack_require__(8);
exports.default = function (module) {
    // register components
    validators_1.default(module);
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(62);
    req.keys().forEach(function (r) {
        var key = "dpa" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./views/admin/serverManagement/addServer/addServer.html": 63,
	"./views/admin/serverManagement/compatibility/compatibilityDialog.html": 64,
	"./views/admin/serverManagement/compatibility/featureList.html": 65,
	"./views/admin/serverManagement/compatibility/productToUpgrade-listItem.html": 66,
	"./views/admin/serverManagement/dpa-server-listview-empty.html": 67,
	"./views/admin/serverManagement/dpa-server-listview-item.html": 68,
	"./views/admin/serverManagement/serverManagement.html": 5,
	"./views/expertAdviceInfo/expertAdviceInfo.html": 6,
	"./views/sqlDetail/sqlDetail.html": 7
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 62;

/***/ }),
/* 63 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog id=addServerDialog> <div ng-controller=\"dpaAddServerController as ctl\" ng-init=ctl.activate(vm)> <xui-message type=info><span _t>Link your Database Performance Analyzer (DPA) server to your SolarWinds Orion server.</span><br> <span _t>Ports 17776, 17777 and 17778 must be open between DPA and Orion. The DPA server must be running.</span></xui-message> <div ng-if=\"ctl.viewModel.errorMessage != null\"> <xui-message type=error class=addServerErrorMessage>{{ctl.viewModel.errorMessage}}</xui-message> </div> <xui-textbox name=address ng-model=ctl.viewModel.address caption=\"_t(DPA Server IP Address or Hostname)\" validators=required,iphostname _ta> <div ng-message=required _t>DPA Server IP Address or Hostname is required.</div> <div ng-message=iphostname _t>The entered value is not a valid IP address or hostname.</div> </xui-textbox> <xui-textbox name=port ng-model=ctl.viewModel.port caption=_t(Port) validators=required,port custom-box-width=100px _ta> <div ng-message=required _t>A port number is required.</div> <div ng-message=port _t>The port must be a valid port number in the range 1 - 65535.</div> </xui-textbox> <xui-textbox name=name ng-model=ctl.viewModel.name caption=\"_t(Display Name)\" validators=required ng-change=ctl.nameChanged() help-text=\"_t(You can use a more familiar name to identify for your DPA server to be displayed in Orion.)\" _ta> <div ng-message=required _t>Display name is required.</div> </xui-textbox> <xui-textbox name=orionaddress ng-model=ctl.viewModel.orionAddress caption=\"_t(Orion Address)\" placeholder=\"_t(Default address is used)\" validators=iphostname help-text=\"_t(If left empty, the default Orion address is used automatically. Enter an address only if it cannot be resolved from the DPA side or you use a forwarding proxy.)\" _ta> <div ng-message=iphostname _t>The entered value is not a valid IP address or hostname.</div> </xui-textbox> <xui-textbox name=username ng-model=ctl.viewModel.username caption=\"_t(DPA Admin User Name)\" validators=required _ta> <div ng-message=required _t>DPA Admin User Name is required.</div> </xui-textbox> <xui-textbox name=password ng-model=ctl.viewModel.password type=password caption=_t(Password) validators=required _ta> <div ng-message=required _t>A password is required.</div> </xui-textbox> <div class=xui-padding-mdv> <xui-button ng-click=ctl.testConnection() is-busy=ctl.testingConnection name=testConnection _t> Test Connection </xui-button> <xui-button class=\"xui-margin-mdl cancelTestConnection\" ng-if=ctl.testingConnection display-style=tertiary ng-click=ctl.cancelTestConnection() _t> Cancel </xui-button> </div> <div ng-if=\"ctl.testConnectionSuccessMessage != null\"> <xui-message type=success class=testConnectionSuccessMessage>{{ctl.testConnectionSuccessMessage}}</xui-message> </div> </div> </xui-dialog> ";

/***/ }),
/* 64 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog id=compatibilityDialog> <div ng-controller=\"dpaCompatibilityDialogController as ctl\" ng-init=ctl.activate(vm)> <div class=automation-compatibility-description> <p ng-if=\"ctl.dpaToUpgrade && ctl.productToUpgrade.isPartiallyCompatible\" _t>Your version of DPA is compatible with the Orion Platform, but there is a newer version of DPA available that will enable additional features. We recommend upgrading DPA at your earliest convenience.</p> <p ng-if=\"ctl.dpaToUpgrade && !ctl.productToUpgrade.isPartiallyCompatible\" _t>Your version of DPA is not compatible with the Orion Platform. We recommend upgrading DPA at your earliest convenience.</p> <p ng-if=\"!ctl.dpaToUpgrade && ctl.productToUpgrade.isPartiallyCompatible\" _t>Your version of DPA Integration Module (DPAIM) is compatible with the DPA, but there is a newer version of DPAIM available that will enable additional features. We recommend upgrading DPAIM at your earliest convenience.</p> <p ng-if=\"!ctl.dpaToUpgrade && !ctl.productToUpgrade.isPartiallyCompatible\" _t>Your version of DPA Integration Module (DPAIM) is not compatible with the DPA. We recommend upgrading DPAIM at your earliest convenience.</p> </div> <xui-divider></xui-divider> <div class=xui-padding-mdv> <div class=xui-text-l _t>Product to upgrade</div> <xui-listview ng-if=ctl.productToUpgrade name=productToUpgrade items-source=[ctl.productToUpgrade] template-url=dpa/views/admin/serverManagement/compatibility/productToUpgrade-listItem.html> </xui-listview> </div> <div class=dpa-compatibility-dialog__nextStepsList> <div class=xui-text-l _t>Next Steps:</div> <ul class=xui-padding-lgl> <li _t>Download the recommended version of <span ng-if=ctl.dpaToUpgrade>DPA</span><span ng-if=!ctl.dpaToUpgrade>DPAIM</span> from <a class=automationDownloadLink ng-href={{ctl.productToUpgrade.downloadLink}} target=_blank class=.xui-text-a> the Customer Portal.</a></li> <li _t>Upgrade <span ng-if=ctl.dpaToUpgrade>DPA</span><span ng-if=!ctl.dpaToUpgrade>DPAIM</span> and enjoy the new functionality.</li> </ul> </div> </div> </xui-dialog> ";

/***/ }),
/* 65 */
/***/ (function(module, exports) {

module.exports = "<div ng-repeat=\"feature in item.featureList\">{{feature}}</div>";

/***/ }),
/* 66 */
/***/ (function(module, exports) {

module.exports = "<div class=row> <div class=col-md-4 _t>{{::item.name}}</div> <div class=col-md-3> <span class=\"xui-text-dscrn xui-margin-smh\" _t>Version Installed:</span> <span automation=version class=dpa-server-management__content__version__highlight ng-class=\"{'dpa-server-management__content__version__highlight__warning': item.isPartiallyCompatible,\n                'dpa-server-management__content__version__highlight__critical': !item.isPartiallyCompatible}\"> {{::item.installedVersion}} </span> </div> <div class=col-md-3><span class=\"xui-text-dscrn xui-margin-smh\" _t>Recommended Version:</span>{{::item.supportedVersion}}</div> <div class=col-md-2> <span xui-popover xui-popover-title={{::item.featureListText}} xui-popover-content=\"::{url: item.featureListTemplate}\" xui-popover-trigger=mouseenter> <a ng-href={{::item.releaseNotesLink}} target=_blank _t>What's new?</a> </span> </div> </div>";

/***/ }),
/* 67 */
/***/ (function(module, exports) {

module.exports = "<div class=dpa-server-management__no-integration-box> <div class=dpa-server-management__no-integration-box__content> <xui-image image=no-data-to-show margin=centered></xui-image> <p _t>No DPA server integrated yet.</p> <xui-button id=noIntegration-addServerButton display-style=primary ng-click=ctrl.viewModel.openAddServerDialog() name=addDpaServerEmptyGrid _t> Integrate with your first DPA server here </xui-button> </div> </div> ";

/***/ }),
/* 68 */
/***/ (function(module, exports) {

module.exports = "<div class=row ng-controller=\"dpaServerListViewItemController as ctlr\"> <div class=col-md-7> <div> <xui-icon icon-size=small icon=dpa-database status=\"{{::item.status | dpaOrionStatusIcon}}\"></xui-icon> <a class=automationDpaServerLink ng-href={{::ctlr.dpaLinks.dpaServerDetails(item.dpaServerId)}}>{{::item.name}}</a> </div> <div> <span class=dpa-server-management__content__label ng-if=ctlr.dpaAddressService.isIpAddress(item.hostname) _t>IP Address:</span> <span class=dpa-server-management__content__label ng-if=!ctlr.dpaAddressService.isIpAddress(item.hostname) _t>Hostname:</span> {{::item.hostname}} <span class=dpa-server-management__content__label _t>Port:</span> {{::item.port}} </div> </div> <div class=\"col-md-5 dpa-server-management__content__version\"> <div><span class=dpa-server-management__content__label _t>DPA version:</span> <span ng-if=\"item.featuresCatalogDiff.dpaVersionInstalled !== item.featuresCatalogDiff.fullyCompatibleDpaVersion\"> <span ng-class=\"{'dpa-server-management__content__version__highlight': item.integrationStatus == 104 || item.integrationStatus == 106,\n                'dpa-server-management__content__version__highlight__warning': item.integrationStatus == 106,\n            'dpa-server-management__content__version__highlight__critical': item.integrationStatus == 104}\">{{item.version}}</span> </span> <span ng-if=\"item.featuresCatalogDiff.dpaVersionInstalled === item.featuresCatalogDiff.fullyCompatibleDpaVersion\"> {{item.version}} </span> </div> <div ng-if=\"item.featuresCatalogDiff != null && (item.integrationStatus == 104 || item.integrationStatus == 106)\"> <a role=button class=automationCompatibilityDialogLink ng-click=\"ctlr.openCompatibilityDialog(item.featuresCatalogDiff, item.integrationStatus == 106)\"> <span _t>Compatibility issue detected. Click for details.</span> </a> </div> </div> </div>";

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(26));
var orionStatusIcon_1 = __webpack_require__(26);
exports.default = function (module) {
    module.filter("dpaOrionStatusIcon", orionStatusIcon_1.default);
};


/***/ })
/******/ ]);
//# sourceMappingURL=dpa.js.map