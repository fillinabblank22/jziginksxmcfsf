/*!
 * @solarwinds/apollo-website-frontend 6.1.0-4644
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 55);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(38));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDhCQUF5QiJ9

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = " <xui-dialog> <sw-license-activation> </sw-license-activation> </xui-dialog> ";

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var LicenseActivationConstants = /** @class */ (function () {
    function LicenseActivationConstants() {
        this.license = {
            defaultKey: "0000-0000-0000-0000-0000-0000-0000-0000"
        };
        this.links = {
            customerPortal: "https://customerportal.solarwinds.com",
            privacy: "https://www.solarwinds.com/privacy.aspx"
        };
    }
    return LicenseActivationConstants;
}());
exports.default = LicenseActivationConstants;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZUFjdGl2YXRpb24tY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGljZW5zZUFjdGl2YXRpb24tY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDO0lBQUE7UUFFVyxZQUFPLEdBQUc7WUFDYixVQUFVLEVBQUUseUNBQXlDO1NBQ3hELENBQUM7UUFFSyxVQUFLLEdBQUc7WUFDWCxjQUFjLEVBQUUsdUNBQXVDO1lBQ3ZELE9BQU8sRUFBRSx5Q0FBeUM7U0FDckQsQ0FBQztJQUNOLENBQUM7SUFBRCxpQ0FBQztBQUFELENBQUMsQUFWRCxJQVVDIn0=

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
function wrapInterceptor(config, callback) {
    function outerInterceptor(element, operation, path, url, headers, params, httpConfig) {
        var args = {
            element: element,
            operation: operation,
            path: path,
            url: url,
            headers: headers,
            params: params,
            httpConfig: httpConfig
        };
        return callback(config, args);
    }
    return outerInterceptor;
}
var SwApiService = /** @class */ (function () {
    function SwApiService($log, $window, _t, restangular, constants) {
        var _this = this;
        this.$log = $log;
        this.$window = $window;
        this._t = _t;
        this.restangular = restangular;
        this.constants = constants;
        this.legacyApi = null;
        this.newApi = null;
        this.ui = null;
        this.ws = null;
        this.configureServices = function () {
            _this.newApi = _this.restangular.withConfig(function (configurer) {
                // Configure restful access to the api service
                configurer.setBaseUrl(_this.apiEndpoint);
                configurer.setDefaultHttpFields({ cache: false });
                configurer.setRestangularFields({ id: "Id" });
                configurer.setResponseExtractor(_this.responseExtractor);
                configurer.addFullRequestInterceptor(wrapInterceptor(configurer, _this.apiRequestInterceptor));
            });
            _this.ui = _this.restangular.withConfig(function (configurer) {
                // Configure restful access to the ui service
                configurer.setBaseUrl(_this.uiEndpoint);
                configurer.setDefaultHttpFields({ cache: false });
                configurer.setRestangularFields({ id: "Id" });
                configurer.setResponseExtractor(_this.responseExtractor);
                configurer.addFullRequestInterceptor(wrapInterceptor(configurer, _this.uiRequestInterceptor));
            });
            _this.legacyApi = _this.restangular.withConfig(function (configurer) {
                // Configure restful access to the api service
                configurer.setBaseUrl(_this.legacyApiEndpoint);
                configurer.setDefaultHttpFields({ cache: false });
                configurer.setRestangularFields({ id: "Id" });
                configurer.setResponseExtractor(_this.responseExtractor);
                configurer.addFullRequestInterceptor(wrapInterceptor(configurer, _this.legacyApiRequestInterceptor));
            });
            _this.ws = _this.restangular.withConfig(function (configurer) {
                // Configure restful access to the api service
                configurer.setBaseUrl(_this.webServiceEndpoint);
                configurer.setDefaultHttpFields({ cache: false });
                configurer.setRestangularFields({ id: "Id" });
                configurer.setResponseExtractor(_this.responseExtractorLegacy);
                configurer.addFullRequestInterceptor(wrapInterceptor(configurer, _this.webServiceRequestInterceptor));
            });
        };
        this.api = function (legacy) {
            legacy = legacy || !angular.isDefined(legacy);
            return legacy ? _this.legacyApi : _this.newApi;
        };
        this.apiRequestInterceptor = function (config, args) {
            config.setBaseUrl(_this.apiEndpoint);
            // Copying the swAlertOnError flag is a workaround for httpConfig.params being stripped out of the response
            if (args.httpConfig.params) {
                args.params.swAlertOnError = args.httpConfig.params.swAlertOnError;
            }
            if (!args.params.lang) {
                args.params.lang = Xui.region;
            }
        };
        this.uiRequestInterceptor = function (config, args) {
            config.setBaseUrl(_this.uiEndpoint);
            // Copying the swAlertOnError flag is a workaround for httpConfig.params being stripped out of the response
            if (args.httpConfig.params) {
                args.params.swAlertOnError = args.httpConfig.params.swAlertOnError;
            }
            if (!args.params.lang) {
                args.params.lang = Xui.region;
            }
        };
        this.legacyApiRequestInterceptor = function (config /*, args: IInterceptorArgs*/) {
            config.setBaseUrl(_this.legacyApiEndpoint);
        };
        this.webServiceRequestInterceptor = function (config /*, args: IInterceptorArgs*/) {
            config.setBaseUrl(_this.webServiceEndpoint);
        };
        this.responseExtractor = function (response, operation /*, what, url*/) {
            var newResponse = response;
            if (operation === "getList") {
                if (typeof response.count !== "undefined" && typeof response.results !== "undefined") {
                    newResponse = response.results;
                    newResponse.count = response.count;
                }
            }
            return newResponse;
        };
        //same as previous, only with capitalized properties
        this.responseExtractorLegacy = function (response, operation /*, what, url*/) {
            var newResponse = response;
            if (operation === "getList") {
                if (typeof response.Count !== "undefined" && typeof response.Results !== "undefined") {
                    newResponse = response.Results;
                    newResponse.count = response.Count;
                }
            }
            return newResponse;
        };
        // Specifying endpoint defaults so it's able to run with mocked backend
        this.apiEndpoint = this.constants.environment.apiEndpoint || "http://localhost/api2";
        this.legacyApiEndpoint = this.constants.environment.legacyApiEndpoint || "http://localhost/api";
        this.webServiceEndpoint = this.constants.environment.webServiceEndpoint || "http://localhost/orion/services";
        this.uiEndpoint = this.apiEndpoint.replace("api2", "ui");
        this.configureServices();
    }
    SwApiService.prototype.transformDataTable = function (dataTable, totalRows) {
        var columns = dataTable.Columns;
        var entities = _.map(dataTable.Rows, function (row) {
            var entity = {};
            for (var index = 0; index < columns.length; index++) {
                entity[columns[index]] = row[index];
            }
            return entity;
        });
        return {
            rows: entities,
            total: totalRows
        };
    };
    SwApiService.$inject = ["$log", "$window", "getTextService", "Restangular", "constants"];
    return SwApiService;
}());
exports.SwApiService = SwApiService;
exports.default = SwApiService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dBcGktc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInN3QXBpLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9DQUFvQzs7QUFzQnBDLHlCQUF5QixNQUE2QixFQUFFLFFBQTJCO0lBQy9FLDBCQUNJLE9BQVksRUFBRSxTQUFpQixFQUFFLElBQVksRUFBRSxHQUFXLEVBQUUsT0FBWSxFQUFFLE1BQVcsRUFBRSxVQUFlO1FBRXRHLElBQU0sSUFBSSxHQUFHO1lBQ1QsT0FBTyxFQUFFLE9BQU87WUFDaEIsU0FBUyxFQUFFLFNBQVM7WUFDcEIsSUFBSSxFQUFFLElBQUk7WUFDVixHQUFHLEVBQUUsR0FBRztZQUNSLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsVUFBVSxFQUFFLFVBQVU7U0FDekIsQ0FBQztRQUNGLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFDRCxNQUFNLENBQUMsZ0JBQWdCLENBQUM7QUFDNUIsQ0FBQztBQUVEO0lBYUksc0JBQ1ksSUFBb0IsRUFDcEIsT0FBdUIsRUFDdkIsRUFBb0MsRUFDcEMsV0FBaUMsRUFDakMsU0FBYztRQUwxQixpQkFlQztRQWRXLFNBQUksR0FBSixJQUFJLENBQWdCO1FBQ3BCLFlBQU8sR0FBUCxPQUFPLENBQWdCO1FBQ3ZCLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBQ3BDLGdCQUFXLEdBQVgsV0FBVyxDQUFzQjtRQUNqQyxjQUFTLEdBQVQsU0FBUyxDQUFLO1FBVmxCLGNBQVMsR0FBeUIsSUFBSSxDQUFDO1FBQ3ZDLFdBQU0sR0FBeUIsSUFBSSxDQUFDO1FBQ3JDLE9BQUUsR0FBeUIsSUFBSSxDQUFDO1FBQ2hDLE9BQUUsR0FBeUIsSUFBSSxDQUFDO1FBbUIvQixzQkFBaUIsR0FBRztZQUN4QixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLFVBQUMsVUFBaUM7Z0JBQ3hFLDhDQUE4QztnQkFDOUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3hDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDOUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUN4RCxVQUFVLENBQUMseUJBQXlCLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO1lBQ2xHLENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLEVBQUUsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxVQUFDLFVBQWlDO2dCQUNwRSw2Q0FBNkM7Z0JBQzdDLFVBQVUsQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2QyxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDbEQsVUFBVSxDQUFDLG9CQUFvQixDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQzlDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDeEQsVUFBVSxDQUFDLHlCQUF5QixDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUNqRyxDQUFDLENBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsVUFBQyxVQUFpQztnQkFDM0UsOENBQThDO2dCQUM5QyxVQUFVLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUM5QyxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDbEQsVUFBVSxDQUFDLG9CQUFvQixDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQzlDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDeEQsVUFBVSxDQUFDLHlCQUF5QixDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQztZQUN4RyxDQUFDLENBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxFQUFFLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsVUFBQyxVQUFpQztnQkFDcEUsOENBQThDO2dCQUM5QyxVQUFVLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUMvQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDbEQsVUFBVSxDQUFDLG9CQUFvQixDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQzlDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztnQkFDOUQsVUFBVSxDQUFDLHlCQUF5QixDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztZQUN6RyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVLLFFBQUcsR0FBRyxVQUFDLE1BQWU7WUFDekIsTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFOUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQztRQUNqRCxDQUFDLENBQUM7UUFtQk0sMEJBQXFCLEdBQUcsVUFBQyxNQUE2QixFQUFFLElBQXNCO1lBQ2xGLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRXBDLDJHQUEyRztZQUMzRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQztZQUN2RSxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUM7WUFDbEMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLHlCQUFvQixHQUFHLFVBQUMsTUFBNkIsRUFBRSxJQUFzQjtZQUNqRixNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUVuQywyR0FBMkc7WUFDM0csRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUM7WUFDdkUsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO1lBQ2xDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFTSxnQ0FBMkIsR0FBRyxVQUFDLE1BQTZCLENBQUEsNEJBQTRCO1lBQzVGLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDOUMsQ0FBQyxDQUFDO1FBRU0saUNBQTRCLEdBQUcsVUFBQyxNQUE2QixDQUFBLDRCQUE0QjtZQUM3RixNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQztRQUVNLHNCQUFpQixHQUFHLFVBQUMsUUFBYSxFQUFFLFNBQWlCLENBQUMsZUFBZTtZQUN6RSxJQUFJLFdBQVcsR0FBRyxRQUFRLENBQUM7WUFDM0IsRUFBRSxDQUFDLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sUUFBUSxDQUFDLEtBQUssS0FBSyxXQUFXLElBQUksT0FBTyxRQUFRLENBQUMsT0FBTyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQ25GLFdBQVcsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO29CQUMvQixXQUFXLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7Z0JBQ3ZDLENBQUM7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUN2QixDQUFDLENBQUM7UUFFRixvREFBb0Q7UUFDNUMsNEJBQXVCLEdBQUcsVUFBQyxRQUFhLEVBQUUsU0FBaUIsQ0FBQyxlQUFlO1lBQy9FLElBQUksV0FBVyxHQUFHLFFBQVEsQ0FBQztZQUMzQixFQUFFLENBQUMsQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsRUFBRSxDQUFDLENBQUMsT0FBTyxRQUFRLENBQUMsS0FBSyxLQUFLLFdBQVcsSUFBSSxPQUFPLFFBQVEsQ0FBQyxPQUFPLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztvQkFDbkYsV0FBVyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7b0JBQy9CLFdBQVcsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztnQkFDdkMsQ0FBQztZQUNMLENBQUM7WUFDRCxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3ZCLENBQUMsQ0FBQztRQTdIRSx1RUFBdUU7UUFDdkUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksdUJBQXVCLENBQUM7UUFDckYsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGlCQUFpQixJQUFJLHNCQUFzQixDQUFDO1FBQ2hHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsSUFBSSxpQ0FBaUMsQ0FBQztRQUM3RyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV6RCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBOENNLHlDQUFrQixHQUF6QixVQUEwQixTQUFjLEVBQUUsU0FBYztRQUNwRCxJQUFNLE9BQU8sR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxVQUFVLEdBQVE7WUFDckQsSUFBTSxNQUFNLEdBQVEsRUFBRSxDQUFDO1lBQ3ZCLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDO2dCQUNsRCxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLENBQUM7WUFFRCxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDO1lBQ0gsSUFBSSxFQUFFLFFBQVE7WUFDZCxLQUFLLEVBQUUsU0FBUztTQUNuQixDQUFDO0lBQ04sQ0FBQztJQXZGYSxvQkFBTyxHQUFHLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxnQkFBZ0IsRUFBRSxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFpSjlGLG1CQUFDO0NBQUEsQUFuSkQsSUFtSkM7QUFuSlksb0NBQVk7QUFxSnpCLGtCQUFlLFlBQVksQ0FBQyJ9

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

// Use the fastest means possible to execute a task in its own turn, with
// priority over other events including IO, animation, reflow, and redraw
// events in browsers.
//
// An exception thrown by a task will permanently interrupt the processing of
// subsequent tasks. The higher level `asap` function ensures that if an
// exception is thrown by a task, that the task queue will continue flushing as
// soon as possible, but if you use `rawAsap` directly, you are responsible to
// either ensure that no exceptions are thrown from your task, or to manually
// call `rawAsap.requestFlush` if an exception is thrown.
module.exports = rawAsap;
function rawAsap(task) {
    if (!queue.length) {
        requestFlush();
        flushing = true;
    }
    // Equivalent to push, but avoids a function call.
    queue[queue.length] = task;
}

var queue = [];
// Once a flush has been requested, no further calls to `requestFlush` are
// necessary until the next `flush` completes.
var flushing = false;
// `requestFlush` is an implementation-specific method that attempts to kick
// off a `flush` event as quickly as possible. `flush` will attempt to exhaust
// the event queue before yielding to the browser's own event loop.
var requestFlush;
// The position of the next task to execute in the task queue. This is
// preserved between calls to `flush` so that it can be resumed if
// a task throws an exception.
var index = 0;
// If a task schedules additional tasks recursively, the task queue can grow
// unbounded. To prevent memory exhaustion, the task queue will periodically
// truncate already-completed tasks.
var capacity = 1024;

// The flush function processes all tasks that have been scheduled with
// `rawAsap` unless and until one of those tasks throws an exception.
// If a task throws an exception, `flush` ensures that its state will remain
// consistent and will resume where it left off when called again.
// However, `flush` does not make any arrangements to be called again if an
// exception is thrown.
function flush() {
    while (index < queue.length) {
        var currentIndex = index;
        // Advance the index before calling the task. This ensures that we will
        // begin flushing on the next task the task throws an error.
        index = index + 1;
        queue[currentIndex].call();
        // Prevent leaking memory for long chains of recursive calls to `asap`.
        // If we call `asap` within tasks scheduled by `asap`, the queue will
        // grow, but to avoid an O(n) walk for every task we execute, we don't
        // shift tasks off the queue after they have been executed.
        // Instead, we periodically shift 1024 tasks off the queue.
        if (index > capacity) {
            // Manually shift all values starting at the index back to the
            // beginning of the queue.
            for (var scan = 0, newLength = queue.length - index; scan < newLength; scan++) {
                queue[scan] = queue[scan + index];
            }
            queue.length -= index;
            index = 0;
        }
    }
    queue.length = 0;
    index = 0;
    flushing = false;
}

// `requestFlush` is implemented using a strategy based on data collected from
// every available SauceLabs Selenium web driver worker at time of writing.
// https://docs.google.com/spreadsheets/d/1mG-5UYGup5qxGdEMWkhP6BWCz053NUb2E1QoUTU16uA/edit#gid=783724593

// Safari 6 and 6.1 for desktop, iPad, and iPhone are the only browsers that
// have WebKitMutationObserver but not un-prefixed MutationObserver.
// Must use `global` or `self` instead of `window` to work in both frames and web
// workers. `global` is a provision of Browserify, Mr, Mrs, or Mop.

/* globals self */
var scope = typeof global !== "undefined" ? global : self;
var BrowserMutationObserver = scope.MutationObserver || scope.WebKitMutationObserver;

// MutationObservers are desirable because they have high priority and work
// reliably everywhere they are implemented.
// They are implemented in all modern browsers.
//
// - Android 4-4.3
// - Chrome 26-34
// - Firefox 14-29
// - Internet Explorer 11
// - iPad Safari 6-7.1
// - iPhone Safari 7-7.1
// - Safari 6-7
if (typeof BrowserMutationObserver === "function") {
    requestFlush = makeRequestCallFromMutationObserver(flush);

// MessageChannels are desirable because they give direct access to the HTML
// task queue, are implemented in Internet Explorer 10, Safari 5.0-1, and Opera
// 11-12, and in web workers in many engines.
// Although message channels yield to any queued rendering and IO tasks, they
// would be better than imposing the 4ms delay of timers.
// However, they do not work reliably in Internet Explorer or Safari.

// Internet Explorer 10 is the only browser that has setImmediate but does
// not have MutationObservers.
// Although setImmediate yields to the browser's renderer, it would be
// preferrable to falling back to setTimeout since it does not have
// the minimum 4ms penalty.
// Unfortunately there appears to be a bug in Internet Explorer 10 Mobile (and
// Desktop to a lesser extent) that renders both setImmediate and
// MessageChannel useless for the purposes of ASAP.
// https://github.com/kriskowal/q/issues/396

// Timers are implemented universally.
// We fall back to timers in workers in most engines, and in foreground
// contexts in the following browsers.
// However, note that even this simple case requires nuances to operate in a
// broad spectrum of browsers.
//
// - Firefox 3-13
// - Internet Explorer 6-9
// - iPad Safari 4.3
// - Lynx 2.8.7
} else {
    requestFlush = makeRequestCallFromTimer(flush);
}

// `requestFlush` requests that the high priority event queue be flushed as
// soon as possible.
// This is useful to prevent an error thrown in a task from stalling the event
// queue if the exception handled by Node.js’s
// `process.on("uncaughtException")` or by a domain.
rawAsap.requestFlush = requestFlush;

// To request a high priority event, we induce a mutation observer by toggling
// the text of a text node between "1" and "-1".
function makeRequestCallFromMutationObserver(callback) {
    var toggle = 1;
    var observer = new BrowserMutationObserver(callback);
    var node = document.createTextNode("");
    observer.observe(node, {characterData: true});
    return function requestCall() {
        toggle = -toggle;
        node.data = toggle;
    };
}

// The message channel technique was discovered by Malte Ubl and was the
// original foundation for this library.
// http://www.nonblocking.io/2011/06/windownexttick.html

// Safari 6.0.5 (at least) intermittently fails to create message ports on a
// page's first load. Thankfully, this version of Safari supports
// MutationObservers, so we don't need to fall back in that case.

// function makeRequestCallFromMessageChannel(callback) {
//     var channel = new MessageChannel();
//     channel.port1.onmessage = callback;
//     return function requestCall() {
//         channel.port2.postMessage(0);
//     };
// }

// For reasons explained above, we are also unable to use `setImmediate`
// under any circumstances.
// Even if we were, there is another bug in Internet Explorer 10.
// It is not sufficient to assign `setImmediate` to `requestFlush` because
// `setImmediate` must be called *by name* and therefore must be wrapped in a
// closure.
// Never forget.

// function makeRequestCallFromSetImmediate(callback) {
//     return function requestCall() {
//         setImmediate(callback);
//     };
// }

// Safari 6.0 has a problem where timers will get lost while the user is
// scrolling. This problem does not impact ASAP because Safari 6.0 supports
// mutation observers, so that implementation is used instead.
// However, if we ever elect to use timers in Safari, the prevalent work-around
// is to add a scroll event listener that calls for a flush.

// `setTimeout` does not call the passed callback if the delay is less than
// approximately 7 in web workers in Firefox 8 through 18, and sometimes not
// even then.

function makeRequestCallFromTimer(callback) {
    return function requestCall() {
        // We dispatch a timeout with a specified delay of 0 for engines that
        // can reliably accommodate that request. This will usually be snapped
        // to a 4 milisecond delay, but once we're flushing, there's no delay
        // between events.
        var timeoutHandle = setTimeout(handleTimer, 0);
        // However, since this timer gets frequently dropped in Firefox
        // workers, we enlist an interval handle that will try to fire
        // an event 20 times per second until it succeeds.
        var intervalHandle = setInterval(handleTimer, 50);

        function handleTimer() {
            // Whichever timer succeeds will cancel both timers and
            // execute the callback.
            clearTimeout(timeoutHandle);
            clearInterval(intervalHandle);
            callback();
        }
    };
}

// This is for `asap.js` only.
// Its name will be periodically randomized to break any code that depends on
// its existence.
rawAsap.makeRequestCallFromTimer = makeRequestCallFromTimer;

// ASAP was originally a nextTick shim included in Q. This was factored out
// into this ASAP package. It was later adapted to RSVP which made further
// amendments. These decisions, particularly to marginalize MessageChannel and
// to capture the MutationObserver implementation in a closure, were integrated
// back into ASAP proper.
// https://github.com/tildeio/rsvp.js/blob/cddf7232546a9cf858524b75cde6f9edf72620a7/lib/rsvp/asap.js

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(60)))

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var asap = __webpack_require__(4);

function noop() {}

// States:
//
// 0 - pending
// 1 - fulfilled with _value
// 2 - rejected with _value
// 3 - adopted the state of another promise, _value
//
// once the state is no longer pending (0) it is immutable

// All `_` prefixed properties will be reduced to `_{random number}`
// at build time to obfuscate them and discourage their use.
// We don't use symbols or Object.defineProperty to fully hide them
// because the performance isn't good enough.


// to avoid using try/catch inside critical functions, we
// extract them to here.
var LAST_ERROR = null;
var IS_ERROR = {};
function getThen(obj) {
  try {
    return obj.then;
  } catch (ex) {
    LAST_ERROR = ex;
    return IS_ERROR;
  }
}

function tryCallOne(fn, a) {
  try {
    return fn(a);
  } catch (ex) {
    LAST_ERROR = ex;
    return IS_ERROR;
  }
}
function tryCallTwo(fn, a, b) {
  try {
    fn(a, b);
  } catch (ex) {
    LAST_ERROR = ex;
    return IS_ERROR;
  }
}

module.exports = Promise;

function Promise(fn) {
  if (typeof this !== 'object') {
    throw new TypeError('Promises must be constructed via new');
  }
  if (typeof fn !== 'function') {
    throw new TypeError('Promise constructor\'s argument is not a function');
  }
  this._U = 0;
  this._V = 0;
  this._W = null;
  this._X = null;
  if (fn === noop) return;
  doResolve(fn, this);
}
Promise._Y = null;
Promise._Z = null;
Promise._0 = noop;

Promise.prototype.then = function(onFulfilled, onRejected) {
  if (this.constructor !== Promise) {
    return safeThen(this, onFulfilled, onRejected);
  }
  var res = new Promise(noop);
  handle(this, new Handler(onFulfilled, onRejected, res));
  return res;
};

function safeThen(self, onFulfilled, onRejected) {
  return new self.constructor(function (resolve, reject) {
    var res = new Promise(noop);
    res.then(resolve, reject);
    handle(self, new Handler(onFulfilled, onRejected, res));
  });
}
function handle(self, deferred) {
  while (self._V === 3) {
    self = self._W;
  }
  if (Promise._Y) {
    Promise._Y(self);
  }
  if (self._V === 0) {
    if (self._U === 0) {
      self._U = 1;
      self._X = deferred;
      return;
    }
    if (self._U === 1) {
      self._U = 2;
      self._X = [self._X, deferred];
      return;
    }
    self._X.push(deferred);
    return;
  }
  handleResolved(self, deferred);
}

function handleResolved(self, deferred) {
  asap(function() {
    var cb = self._V === 1 ? deferred.onFulfilled : deferred.onRejected;
    if (cb === null) {
      if (self._V === 1) {
        resolve(deferred.promise, self._W);
      } else {
        reject(deferred.promise, self._W);
      }
      return;
    }
    var ret = tryCallOne(cb, self._W);
    if (ret === IS_ERROR) {
      reject(deferred.promise, LAST_ERROR);
    } else {
      resolve(deferred.promise, ret);
    }
  });
}
function resolve(self, newValue) {
  // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
  if (newValue === self) {
    return reject(
      self,
      new TypeError('A promise cannot be resolved with itself.')
    );
  }
  if (
    newValue &&
    (typeof newValue === 'object' || typeof newValue === 'function')
  ) {
    var then = getThen(newValue);
    if (then === IS_ERROR) {
      return reject(self, LAST_ERROR);
    }
    if (
      then === self.then &&
      newValue instanceof Promise
    ) {
      self._V = 3;
      self._W = newValue;
      finale(self);
      return;
    } else if (typeof then === 'function') {
      doResolve(then.bind(newValue), self);
      return;
    }
  }
  self._V = 1;
  self._W = newValue;
  finale(self);
}

function reject(self, newValue) {
  self._V = 2;
  self._W = newValue;
  if (Promise._Z) {
    Promise._Z(self, newValue);
  }
  finale(self);
}
function finale(self) {
  if (self._U === 1) {
    handle(self, self._X);
    self._X = null;
  }
  if (self._U === 2) {
    for (var i = 0; i < self._X.length; i++) {
      handle(self, self._X[i]);
    }
    self._X = null;
  }
}

function Handler(onFulfilled, onRejected, promise){
  this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
  this.onRejected = typeof onRejected === 'function' ? onRejected : null;
  this.promise = promise;
}

/**
 * Take a potentially misbehaving resolver function and make sure
 * onFulfilled and onRejected are only called once.
 *
 * Makes no guarantees about asynchrony.
 */
function doResolve(fn, promise) {
  var done = false;
  var res = tryCallTwo(fn, function (value) {
    if (done) return;
    done = true;
    resolve(promise, value);
  }, function (reason) {
    if (done) return;
    done = true;
    reject(promise, reason);
  });
  if (!done && res === IS_ERROR) {
    done = true;
    reject(promise, LAST_ERROR);
  }
}


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("orion.decorators", []);
angular.module("orion.dialogs", []);
angular.module("orion.services", ["i18n-gettext-tools", "oc.lazyLoad"]);
angular.module("orion.filters", []);
angular.module("orion.views", ["oc.lazyLoad"]);
angular.module("orion.resources", []);
angular.module("orion.components", ["i18n-gettext-tools", "filtered-list", "oc.lazyLoad"]);
angular.module("orion", [
    "xui",
    "orion.decorators",
    "orion.dialogs",
    "orion.services",
    "orion.filters",
    "orion.views",
    "orion.resources",
    "orion.components"
]);
// create and register Xui (Orion) module wrapper
var orion = Xui.registerModule("orion");
exports.default = orion;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBRXBDLHlCQUF5QjtBQUN6QixPQUFPLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3ZDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3BDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO0FBQ3hFLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3BDLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztBQUMvQyxPQUFPLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3RDLE9BQU8sQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUUsQ0FBRSxvQkFBb0IsRUFBRSxlQUFlLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztBQUU1RixPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRTtJQUNwQixLQUFLO0lBQ0wsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsa0JBQWtCO0NBQ3JCLENBQUMsQ0FBQztBQUVILGlEQUFpRDtBQUNqRCxJQUFNLEtBQUssR0FBRyxHQUFHLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBRTFDLGtCQUFlLEtBQUssQ0FBQyJ9

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(6);
function resourceFactory(moduleName) {
    return function (name, require, fn) {
        var inject = ["$scope"];
        if (require) {
            inject.push.apply(inject, require);
        }
        fn.$inject = inject;
        angular.module(moduleName + ".resources").controller(name, fn);
        return this;
    };
}
var win = window;
win.SW = win.SW || {};
var sw = win.SW;
sw.apiKey = "892BE7A4298FC8D4";
sw.region = Xui.region;
sw.orion = app_1.default;
sw.orion.resource = resourceFactory("orion");
exports.default = sw;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3cuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDZCQUEwQjtBQUUxQix5QkFBeUIsVUFBa0I7SUFDdkMsTUFBTSxDQUFDLFVBQVUsSUFBWSxFQUFFLE9BQWlCLEVBQUUsRUFBWTtRQUMxRCxJQUFNLE1BQU0sR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTFCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDVixNQUFNLENBQUMsSUFBSSxPQUFYLE1BQU0sRUFBUyxPQUFPLEVBQUU7UUFDNUIsQ0FBQztRQUVELEVBQUUsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBRXBCLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLFlBQVksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDL0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDLENBQUM7QUFDTixDQUFDO0FBRUQsSUFBTSxHQUFHLEdBQUcsTUFBbUIsQ0FBQztBQUNoQyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBUyxDQUFDO0FBQzdCLElBQU0sRUFBRSxHQUFRLEdBQUcsQ0FBQyxFQUFFLENBQUM7QUFFdkIsRUFBRSxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQztBQUMvQixFQUFFLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUM7QUFDdkIsRUFBRSxDQUFDLEtBQUssR0FBRyxhQUFLLENBQUM7QUFDakIsRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBRTdDLGtCQUFlLEVBQUUsQ0FBQyJ9

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-error-inspector> <div class=sw-error-inspector__message>{{::$ctrl.message}}</div> <xui-message ng-if=::$ctrl.hasHints class=sw-error-inspector__hints type=info> <ol> <li ng-repeat=\"hintItem in ::$ctrl.hints\"><ng-bind-html ng-bind-html=::hintItem></ng-bind-html></li> </ol> </xui-message> <xui-expander ng-if=::$ctrl.hasError _ta heading=\"Error Details\"> <xui-textbox is-read-only=true ng-model=$ctrl.detail rows=20 class=sw-error-inspector__detail></xui-textbox> <xui-button icon=copy xui-clipboard=$ctrl.detail on-clipboard-success=$ctrl.copySuccess() _t>Copy</xui-button> </xui-expander> </div> ";

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = "<div class=\"xui sw-dashboard__editing-buttons\" ng-init=\"$root.dashboardTitle = ''\" ng-style=\"$root.editingDashboard && { 'top':'auto' }\"> <div class=sw-dashboard__editing-title ng-if=$root.drawerOpen _t>Add Widgets</div> <div class=sw-dashboard__editing-title ng-if=!$root.drawerOpen _t>Customize Page</div> <xui-button display-style=primary ng-click=\"$root.drawerOpen = true\" ng-show=!drawerOpen> <span _t>Add Widgets</span> </xui-button> <xui-button display-style=tertiary icon=check ng-click=\"$root.editingDashboard = $root.drawerOpen = false\" ng-show=!$root.drawerOpen> <span _t>Done Editing</span> </xui-button> <xui-button display-style=tertiary icon=check ng-click=\"$root.drawerOpen = false\" ng-show=$root.drawerOpen> <span _t>Done Adding Widgets</span> </xui-button> </div> ";

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = "<xui-button ng-if=$ctrl.isVisible() class=sw-evaluation-banner display-style=primary ng-click=$ctrl.showDetailsDialog()> <span ng-if=\"$ctrl.evaluatedModuleCount == 1\" _t> 1 product in evaluation. </span> <span ng-if=\"$ctrl.evaluatedModuleCount > 1\" _t=$ctrl.paramEval> {0} products in evaluation. </span> <span ng-if=\"$ctrl.expiredModuleCount == 1\" _t> 1 product evaluation expired. </span> <span ng-if=\"$ctrl.expiredModuleCount > 1\" _t=$ctrl.paramExpired> {0} products evaluations expired. </span> </xui-button> ";

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "<a class=sw-header-logo ng-if=::$ctrl.logoUri href=/Orion/View.aspx title=\"_t(Go To Orion Home)\" _ta><img ng-src={{::$ctrl.logoUri}} alt=\"_t(Go To Orion Home)\" class=sw-header-logo__img _ta/></a> ";

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<a href=https://www.solarwinds.com/documentation/helpLoader.aspx class=sw-mega-menu-help-link target=_blank rel=\"noopener noreferrer\"> <i class=\"sw-mega-menu-help-link__icon xui-icon\"></i> <span _t>Help</span> </a> ";

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = "<sw-evaluation-banner></sw-evaluation-banner> <div class=sw-mega-menu__menu> <sw-header-logo class=pull-left></sw-header-logo> <sw-navigation-menu class=pull-left></sw-navigation-menu> <div class=pull-right> <sw-notifications-component class=pull-left></sw-notifications-component> <sw-user-info class=pull-left></sw-user-info> <sw-orion-search-component class=pull-left></sw-orion-search-component> <sw-mega-menu-help-link class=pull-left></sw-mega-menu-help-link> </div> <sw-dashboard-edit-panel></sw-dashboard-edit-panel> </div> ";

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "<ul> <li ng-repeat=\"topMenuItem in ::$ctrl.menuItems\" class=\"animated fadeIn sw-navigation-menu__tab\" ng-show=!!topMenuItem> <a id=mega-menu-{{::topMenuItem.name}} href=# rel=\"noopener noreferrer\"> <span class=sw-navigation-menu__tab-title>{{::topMenuItem.displayName}}</span><span class=mega-menu-caret></span> </a> <div class=\"sw-navigation-menu__tab-content mega-submenu {{$ctrl.isDashboards(topMenuItem) ? 'full-menu ' + $ctrl.menuStyle : ''}}\"> <div class=mega-submenu-container> <ul ng-switch=$ctrl.isDashboards(topMenuItem)> <li class=sw-navigation-menu__sub-item ng-switch-when=false ng-repeat=\"menuItem in ::topMenuItem.items\"> <a class=sw-navigation-menu__sub-item-title rel=\"noopener noreferrer\" id={{::menuItem.name}}-item-{{$index}} href={{::menuItem.url}} target={{::$ctrl.itemTarget(menuItem)}}>{{::menuItem.displayName}}</a> </li> <li class=sw-navigation-menu__sub-header ng-switch-when=true ng-repeat=\"menuItem in ::topMenuItem.items\" ng-if=\"menuItem.items && menuItem.items.length\"> <h3> <button><span class=mega-menu-caret></span><span class=\"sw-navigation-menu__sub-header-title category-text\">{{::menuItem.displayName}}</span></button> </h3> <div class=sub-menu-container> <h3 id={{::menuItem.displayName}} class=sub-menu-title><a href={{::menuItem.items[0].url}}>{{::menuItem.displayName}}</a> </h3> <ul class=mega-menu-dash> <li class=sw-navigation-menu__sub-item ng-repeat=\"item in ::menuItem.items\"> <a class=sw-navigation-menu__sub-item-title rel=\"noopener noreferrer\" id=\"{{::menuItem.name | lowercase}}-item-{{$index}}\" href={{::item.url}} target={{::$ctrl.itemTarget(item)}}>{{::item.displayName}}</a> </li> </ul> </div> </li> </ul> </div> <div ng-if=$ctrl.showToggle(topMenuItem) class=sw-navigation-menu__config> <a href=/Orion/Admin/SelectMenuBar.aspx class=sw-navigation-menu__config-button rel=\"noopener noreferrer\" sw-auth sw-auth-roles=admin> <span _t>Configure</span> </a> <a href=# class=\"sw-navigation-menu__config__style-toggle menu-style-toggle\" rel=\"noopener noreferrer\" ng-click=$ctrl.toggleMenuStyle()> <span ng-hide=$ctrl.isMenuSecondary class=sw-navigation-menu__config__style-toggle__collapse _t>Collapse</span> <span ng-show=$ctrl.isMenuSecondary class=sw-navigation-menu__config__style-toggle__expand _t>Expand</span> </a> </div> </div> </li> </ul> ";

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-notifications ng-class=\"{'sw-notifications--opened': $ctrl.isOpen}\" ng-cloak uib-dropdown on-toggle=$ctrl.onToggle(open) sw-auth sw-auth-roles=admin,demo> <a class=\"sw-notifications__button dropdown-toggle\" href=# ng-class=\"{'sw-notifications__button--opened': $ctrl.isOpen}\" uib-dropdown-toggle type=button> <i class=\"sw-notifications__icon xui-icon\"></i> <span class=sw-notifications__count ng-hide=\"$ctrl.isEmpty || (!$ctrl.isError && $ctrl.isOpen)\">{{$ctrl.getCountText()}}</span> <span class=sw-notifications__caret></span> </a> <div class=\"sw-notifications__container dropdown-menu animated-fast fadeIn dropdown-menu-right\"> <div class=sw-notifications__empty ng-show=$ctrl.isEmpty _t> You have no notifications at this time. </div> <xui-button class=sw-notifications__dismiss-all display-style=link ng-click=$ctrl.dismissAllNotifications($event) ng-hide=\"$ctrl.notifications.length <= 1\" _t> Dismiss All </xui-button> <div ng-hide=$ctrl.isEmpty class=sw-notifications__summary> <span _t>Notifications</span>&nbsp;({{$ctrl.notifications.length}}) </div> <div class=sw-notifications__list> <div ng-repeat=\"$notification in $ctrl.notifications track by $index\" class=sw-notifications__list__item> <a ng-href={{::$notification.DetailsPageLink}} class=sw-notifications__list__item__link> <img class=sw-notifications__list__item__image sw-src={{::$notification.ImageLink}} ng-show=$notification.ImageLink> <span ng-bind-html=$notification.Message></span> </a> <div class=sw-notifications__list__item__delete> <xui-button ng-click=\"$ctrl.dismissNotification($event, $notification)\" tool-tip={{::$notification.CustomDismissButtonText}} display-style=link icon=close icon-color=white> </xui-button> </div> </div> </div> </div> </div> ";

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-orion-search-component__menu-item uib-dropdown ng-class=\"{'sw-orion-search-component--opened': vm.isOpen}\" auto-close=outsideClick on-toggle=vm.onToggle(open) sw-auth sw-auth-feature-toggle=SwSearch> <a href=# uib-dropdown-toggle class=\"dropdown-toggle sw-orion-search-component__menu-button\"> <xui-icon icon=search icon-color=\"{{vm.isOpen ? 'orange' : 'white'}}\" icon-hover-color=orange is-dynamic=true fill-container=true></xui-icon> </a> <div uib-dropdown-menu class=\"sw-orion-search-component__dropdown dropdown-menu animated-fast fadeIn dropdown-menu-right xui-padding-lg\"> <div class=\"sw-orion-search-component__dropdown-group xui-padding-lg\"> <xui-search on-search=vm.onSearch(value) placeholder=_t(Search...) value=vm.searchValue _ta></xui-search> <div class=\"sw-orion-search-component__search-history xui-padding-mdv\" ng-if=\"!vm.searchValue && vm.searchHistory.length\"> <div class=\"xui-text-s sw-orion-search-component__search-history-header xui-margin-smv\" _t>recent searches</div> <div class=\"sw-orion-search-component__search-history-item xui-text-r xui-padding-md\" ng-repeat=\"item in vm.searchHistory\" ng-click=vm.onSearch(item)> <span class=sw-orion-search-component__search-history-item-text>{{item}}</span> <xui-icon icon=caret-right></xui-icon> </div> </div> <div class=sw-orion-search-component__search-suggestions></div> </div> </div> </div> ";

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<a class=sw-user-info href=# ng-click=$ctrl.logout() ng-cloak> <span class=\"sw-user-info__icon xui-icon\"></span> <span class=sw-user-info__name ng-bind=::$ctrl.userLabel uib-tooltip={{::$ctrl.userLabel}} tooltip-placement=left></span> (<span _t>Logout</span>) </a> ";

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=sw-orion-api-error__dialog-message>{{::vm.dialogOptions.viewModel.header}}</div> <xui-textbox is-read-only=true ng-model=vm.dialogOptions.viewModel.errorString rows=20 class=sw-orion-api-error__dialog-details></xui-textbox> <xui-button xui-clipboard=vm.dialogOptions.viewModel.errorString icon=copy on-clipboard-success=vm.dialogOptions.viewModel.copySuccess() _t>Copy </xui-button> </xui-dialog> ";

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <h2 _t>Your Orion Web Console session is about to expire due to inactivity.</h2> <div class=sw-auth-expire-dialog__countdown> <div class=sw-auth-expire-dialog__count> {{ctrl.timeRemaining}} </div> <div class=sw-auth-expire-dialog__label> {{ctrl.timeType}} </div> </div> </xui-dialog>";

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=sw-fieldpicker-dialog> <div class=group-selection ng-if=false> <xui-dropdown items-source=modalOptions.viewModel.groups display-value=GroupCaption on-changed=modalOptions.viewModel.selectGroup(modalOptions.viewModel.selectedGroup) selected-item=modalOptions.viewModel.selectedGroup></xui-dropdown> </div> <div class=field-list> <div class=\"input-group search-input\"> <input class=form-control ng-model=modalOptions.viewModel.search.target.ItemCaption xui-set-focus=modalOptions.viewModel.search.focusInput type=search placeholder=_t(Search...) autofocus/> <span class=input-group-btn> <xui-button size=small icon-class=icon-cmd-search ng-click=modalOptions.viewModel.search.reset()></xui-button> </span> </div> <ul class=scroll-container> <li ng-repeat=\"field in modalOptions.viewModel.selectedGroup.Properties | filter:modalOptions.viewModel.search.target | orderBy: 'ItemCaption' \"> <xui-checkbox ng-model=field.IsSelected><span ng-bind-html=\"field.ItemCaption | highlight:modalOptions.viewModel.search.target.ItemCaption\"></span> </xui-checkbox> </li> </ul> </div> </div> </xui-dialog> ";

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <sw-error-inspector error=::vm.dialogOptions.viewModel.error></sw-error-inspector> </xui-dialog> ";

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div> <div> <xui-checkbox ng-model=vm.dialogOptions.viewModel.optIn _t> I would like to be part of the Orion Improvement Program and help by sending usage statistics of installed products. </xui-checkbox> <div> <div class=sw-link-dialog> <a target=_blank rel=\"noopener noreferrer\" href={{vm.dialogOptions.viewModel.learnMoreUrl}}>»&nbsp;<span _t>Learn more</span></a> </div> </div> </div></div></xui-dialog> ";

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div xui-busy=salesTriggerCtrl.isBusy class=sw-salestrigger-wrapper ng-class=::salesTriggerCtrl.imgClass> <div class=sw-salestrigger-content> <div class=caption>{{::salesTriggerCtrl.description}} <br/><ng-include src=::salesTriggerCtrl.descTemplate></ng-include> </div> <div> <ul class=sw-salestrigger__products> <li class=sw-salestrigger__products-header> <div class=small-gutter> <div ng-repeat=\"column in salesTriggerCtrl.columns\" ng-class=column.class>{{column.header}}</div> </div> </li> <div class=sw-salestrigger__products-item-container xui-scrollbar> <ng-include src=::salesTriggerCtrl.template></ng-include> </div> </ul> </div> </div> <div class=\"sw-salestrigger-links sw-salestrigger-adminonly\"> <div class=sw-salestrigger-linkwrapper> <a ng-if=::salesTriggerCtrl.showDetailsBtn class=\"xui-button btn btn-primary btn-xs\" href={{::salesTriggerCtrl.detailsLink}} target=_blank rel=\"noopener noreferrer\">{{::salesTriggerCtrl.detailsText}}</a> <a class=sw-link-dialog href=# ng-show=salesTriggerCtrl.showCancelReminder ng-click=salesTriggerCtrl.suppressReminder();>»&nbsp;<span _t>Don't remind me again</span></a> <div ng-if=::salesTriggerCtrl.showDetailsBtn class=sw-text-helpful _t>via Customer Portal</div> </div> </div> </div> <div ng-if=::salesTriggerCtrl.showFooter class=\"sw-salestrigger-footer sw-salestrigger-adminonly\"> <div class=\"sw-salestrigger-left col-sm-6\"> <div class=sw-salestrigger-header _t>Get a quote from sales</div> <ul> <ng-include src=::salesTriggerCtrl.footerEmailTemplate></ng-include> <li><span class=sw-salestrigger-label><span _t>North America</span>:&nbsp;</span><a href=tel:866-530-8100>866 530 8100</a></li> <li><span class=sw-salestrigger-label><span _t>Europe, Middle East, Africa</span>:&nbsp;</span><a href=\"tel:+353 21 5002900\">+353 21 5002900</a></li> <li><span class=sw-salestrigger-label><span _t>Asia Pacific</span>:&nbsp;</span><a href=\"tel:+61 2 8412 4900\">+61 2 8412 4900</a></li> </ul> </div> <div class=\"sw-salestrigger-right col-sm-6\"> <ng-include src=::salesTriggerCtrl.footerRightTemplate></ng-include> </div> </div> </xui-dialog> <script type=text/ng-template id=sales-trigger-eval.html> <li class=\"sw-salestrigger__products-item\" ng-repeat=\"license in ::salesTriggerCtrl.licenses\" ng-class=\"::license.isExpired ? 'sw-salestrigger-expired' : 'sw-salestrigger-expiring'\">\n        <div class=\"small-gutter\">\n            <div class=\"col-sm-7\">\n                <div title=\"{{::license.name}}\">{{::license.name}}</div>\n            </div>\n            <div class=\"col-sm-5\" ng-switch=\"license.isExpired\">\n                <div ng-switch-when=\"false\">\n                    <span class=\"sw-salestrigger-status\" >{{::license.daysRemaining}}&nbsp;\n                        <span _t ng-if=\"::license.daysRemaining == 1\">day</span>\n                        <span _t ng-if=\"::license.daysRemaining != 1\">days</span>\n                    </span>\n                    &nbsp;<span _t>on</span>\n                    &nbsp;<span>{{::license.expiration | swDate:'MMM d'}}</span>\n                </div>\n                <div ng-switch-when=\"true\">\n                    <span class=\"sw-salestrigger-status\" _t>Expired</span>\n                </div>\n            </div>\n        </div>\n    </li> </script> <script type=text/ng-template id=sales-trigger-license.html> <li class=\"sw-salestrigger__products-item sw-salestrigger-expiring\" ng-repeat=\"saturationInfo in ::salesTriggerCtrl.saturationInfo\">\n        <xui-expander heading=\"{{::saturationInfo.productName}}\">\n            <div class=\"expander__table-row\" ng-class=\"::saturationEntity.isLimitReached ? 'sw-salestrigger-expired' : 'sw-salestrigger-expiring'\" ng-repeat=\"saturationEntity in ::saturationInfo.saturationEntities\">\n                <div class=\"col-sm-4\">{{::saturationEntity.elementType}}</div>\n                <div class=\"col-sm-7\">\n                    <span class=\"sw-salestrigger-status\">{{::saturationEntity.saturation}}% used</span>\n                    monitoring {{::saturationEntity.count}}/{{::saturationEntity.maxCount}}\n                </div>\n            </div>\n        </xui-expander>\n    </li> </script> <script type=text/ng-template id=sales-trigger-maintenance.html> <li class=\"sw-salestrigger__products-item\" ng-repeat=\"maintenanceItem in ::salesTriggerCtrl.maintenanceItems\" ng-class=\"::maintenanceItem.isExpired ? 'sw-salestrigger-expired' : 'sw-salestrigger-expiring'\">\n        <div class=\"small-gutter\">\n            <div class=\"col-sm-7\">\n                <div>{{::maintenanceItem.displayName}}</div>\n            </div>\n            <div class=\"col-sm-5\" ng-switch=\"maintenanceItem.isExpired\">\n                <div ng-switch-when=\"false\">\n                    <span class=\"sw-salestrigger-status\" >{{::maintenanceItem.daysRemaining}}&nbsp;\n                        <span _t ng-if=\"::maintenanceItem.daysRemaining == 1\">day</span>\n                        <span _t ng-if=\"::maintenanceItem.daysRemaining != 1\">days</span>\n                    </span>\n                    &nbsp;<span _t>on</span>\n                    &nbsp;<span>{{::maintenanceItem.expiration}}</span>\n                </div>\n                <div ng-switch-when=\"true\">\n                    <span class=\"sw-salestrigger-status\" _t>Expired</span>\n                </div>\n            </div>\n        </div>\n    </li> </script> <script type=text/ng-template id=sales-trigger-poller-limit.html> <li class=\"sw-salestrigger__products-item\" ng-repeat=\"pollerLimitItem in ::salesTriggerCtrl.pollerLimitItems\"\n        ng-class=\"::pollerLimitItem.isLimitReached ? 'sw-salestrigger-expired' : 'sw-salestrigger-expiring'\">\n        <div class=\"small-gutter\">\n            <div class=\"col-sm-7\">\n                <span><div _t>Polling Engine on</div><div>&nbsp; {{::pollerLimitItem.engineName}}</div></span>\n            </div>\n            <div class=\"col-sm-5\">\n                <span class=\"sw-salestrigger-status\">{{::pollerLimitItem.currentUsage}}&nbsp;%</span>\n            </div>\n        </div>\n    </li> </script> <script type=text/ng-template id=sales-trigger-poller-limit-desc.html> <a class=\"sw-link-dialog sw-salestrigger-adminonly\" href=\"/Orion/Admin/Details/Engines.aspx\" target=\"_blank\">&#0187;&nbsp;<span _t>Polling details</span></a> </script> <script type=text/ng-template id=sales-trigger-upgrade.html> <li class=\"sw-salestrigger__products-item\" ng-repeat=\"upgradeItem in ::salesTriggerCtrl.upgradeSummary.items\">\n        <div class=\"small-gutter\"\n             ng-class=\"{'sw-salestrigger-product-blue-bold': upgradeItem.isVersionDifference, 'sw-salestrigger-product-blue': upgradeItem.isVersionDifference === false, 'sw-salestrigger-product-gray': upgradeItem.isMaintenanceExpired}\">\n            <div class=\"col-sm-7 sw-salestrigger-product-bold\">\n                <div>{{::upgradeItem.productName}}</div>\n            </div>\n            <div class=\"col-sm-5\">\n                <span ng-class=\"::upgradeItem.isMaintenanceExpired ? 'sw-salestrigger-status-gray' : 'sw-salestrigger-status-blue'\">{{::upgradeItem.version}}</span>\n            </div>\n        </div>\n        <div class=\"small-gutter\">\n            <a class=\"col-sm-7\" href=\"{{::upgradeItem.releaseNotes}}\" target=\"_blank\" rel=\"noopener noreferrer\"><span _t>RELEASE NOTES</span> &nbsp;&#0187;</a>\n        </div>\n        <div class=\"small-gutter sw-salestrigger__row-message\">\n            <div class=\"col-sm-12\">\n                <xui-message ng-if=\"upgradeItem.isMaintenanceExpired\" type=\"error\">\n                    <span _t>Maintenance has expired, renew now to access to this update.</span></br>\n                    <a href=\"http://localhost/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_renew&CMP=PRD-IPS-Settings-UpgrdPopupMaint-X-NOIP-X\"><span _t>View maintenance details</span></a>\n                </xui-message>\n            </div>\n        </div>\n    </li> </script> <script type=text/ng-template id=sales-trigger-footer-email-sales.html> <li><a href=\"mailto:customersales@solarwinds.com\" _t=\"[ 'customersales@solarwinds.com' ]\"><span _t>Email details to sales</span>&nbsp;({0})</a></li> </script> <script type=text/ng-template id=sales-trigger-footer-email-maintenance.html> <li><span class=\"sw-salestrigger-label\"><span _t>Email</span>:&nbsp;</span><a href=\"mailto:renewals@solarwinds.com\" _t=\"[ 'renewals@solarwinds.com' ]\">{0}</a></li> </script> <script type=text/ng-template id=sales-trigger-footer-right-default.html> <div class=\"sw-salestrigger-header\" _t>Already purchased?</div>\n    <div class=\"sw-salestrigger-description\"><span _t>You may need to apply your activation key(s) from </span>&nbsp;<a href=\"https://customerportal.solarwinds.com/home\" target=\"_blank\" rel=\"noopener noreferrer\"><span _t>Customer Portal</span></a>\n    </div>\n    <a class=\"xui-button btn btn-secondary\" href=\"/ui/licensing/license-manager\" target=\"_blank\" _t>MANAGE ORION LICENSES</a> </script> <script type=text/ng-template id=sales-trigger-footer-right-maintenance.html> <div class=\"sw-salestrigger-label\" _t>Why renew maintenance?</div>\n    <div>\n        <a class=\"sw-link-dialog\" target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=maintenance_card\">\n            &#0187; <span _t>Benefits of active maintenance</span>\n        </a>\n        <br />\n        <a class=\"sw-link-dialog\" target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=thwackproductblog\">\n            &#0187; <span _t>Subscribe to product blogs to see what we're working on</span>\n        </a>\n    </div>\n    <div class=\"sw-salestrigger-label\" _t>Already renewed?</div>\n    <div><span _t>You may need to apply your activation key(s) from</span>\n        <a href=\"https://customerportal.solarwinds.com/home\" target=\"_blank\" rel=\"noopener noreferrer\"> <span _t>Customer Portal</span></a>\n    </div> </script> ";

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-entity-popover> <div class=xui-margin-sm ng-if=::vm.menuItems.length> <xui-menu title=_t(Commands) display-style=tertiary items-source=::vm.menuItems _ta></xui-menu> </div> <div> <xui-listview ng-if=::vm.nonEmptyData.length items-source=::vm.nonEmptyData row-padding=compact template-fn=vm.templateFn> </xui-listview> </div> </div> ";

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = "<sw-entity-popover-property label={{::item.label}} value=::item.value unit={{::item.unit}} color={{::item.color}} icon={{::item.icon}} icon-status={{::item.iconStatus}}> </sw-entity-popover-property> ";

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-entity-popover-link> <xui-menu-link url={{::item.link}}>{{::item.buttonText}}</xui-menu-link> </div>";

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-entity-popover-property> <div class=sw-entity-popover-property__label ng-if=::!!vm.label> <div xui-ellipsis ellipsis-options=::vm.elipsisLabel> {{::vm.label}} </div> </div> <div class=sw-entity-popover-property__value> <span ng-if=::vm.icon> <xui-icon icon={{::vm.icon}} status={{::vm.iconStatus}} icon-size=small></xui-icon> </span> <div class=sw-entity-popover-property__value--value ng-class=::vm.getColorClass()> <div xui-ellipsis ellipsis-options=::vm.elipsisValue> {{::vm.getValue()}} {{::vm.unit}} </div> </div> </div> </div> ";

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-license-activation> <div ng-if=\"vm.dialogOptions.viewModel.licenseActivation.mode === 'online'\"> <div> <form name=vm.dialogOptions.viewModel.licenseActivation.activateLicenseOnlineForm> <xui-message type=error ng-show=vm.dialogOptions.viewModel.licenseActivation.errorMessage>{{vm.dialogOptions.viewModel.licenseActivation.errorMessage}}</xui-message> <div _t=\"['{{::vm.dialogOptions.viewModel.licenseActivation.swLicenseActivationConstants.links.customerPortal}}']\"> To activate a license, you will need an activation key. You can find your activation keys in the <a target=_blank rel=\"noopener noreferrer\" href={0} class=cl-customer-portal-link>Customer Portal</a>. </div> <xui-textbox name=activationKey caption=\"_t(Activation Key:)\" ng-model=vm.dialogOptions.viewModel.licenseActivation.selectedLicense.licenseKey help-text=\"_t(e.g. A1B2-C3D4-E5F6-G7H8-I9J1-K1LM-23NO-34PQ)\" validators=\"required,pattern=^([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})$\" _ta> <div ng-message=pattern><span _t>Activation key should be in XXXX-XXXX-XXXX-XXXX-XXXX-XXXX-XXXX-XXXX format.</span></div> <div ng-message=required><span _t>Enter an activation key.</span></div> </xui-textbox> <xui-divider></xui-divider> <div> <div class=row> <div class=col-xs-6><strong _t>REGISTRATION INFORMATION</strong> </div> <div class=col-xs-6><a class=pull-right href={{::vm.dialogOptions.viewModel.licenseActivation.swLicenseActivationConstants.links.privacy}} target=_blank rel=\"noopener noreferrer\" _t>Privacy Statement</a></div> </div> <div> <div> <label _t>Email</label> <xui-textbox name=email ng-model=vm.dialogOptions.viewModel.licenseActivation.selectedLicense.user.email type=email validators=required help-text=\"_t(A valid email address is required to activate your license.)\" _ta> <div ng-message=required><span _t>Enter an email address.</span></div> <div ng-message=email><span _t>Enter a valid email address.</span></div> </xui-textbox> </div> <div class=row> <div class=col-xs-6> <label _t>First Name</label> <xui-textbox name=firstName ng-model=vm.dialogOptions.viewModel.licenseActivation.selectedLicense.user.firstName validators=required> <div ng-message=required><span _t>Enter the first name.</span></div> </xui-textbox> </div> <div class=col-xs-6> <label _t>Last Name</label> <xui-textbox name=lastName ng-model=vm.dialogOptions.viewModel.licenseActivation.selectedLicense.user.lastName validators=required> <div ng-message=required><span _t>Enter the last name.</span></div> </xui-textbox> </div> </div> <div> <label _t>Phone Number</label><span _t>&nbsp; Optional</span> <xui-textbox name=phoneNumber ng-model=vm.dialogOptions.viewModel.licenseActivation.selectedLicense.user.phoneNumber> </xui-textbox> </div> </div> </div> </form> </div> </div> <div ng-if=\"vm.dialogOptions.viewModel.licenseActivation.mode !== 'online'\"> <div> <form name=vm.dialogOptions.viewModel.licenseActivation.activateLicenseOfflineForm> <div class=sw-license-activation__offline-message-wrapper ng-if=\"vm.dialogOptions.viewModel.licenseActivation.wizardWarningTextToken !== true\"> <xui-message type=info class=sw-license-activation__prewizard-text _t> We detected your computer is not connected to the Internet. You must activate your license manually. To complete the license activation, you need access to a computer connected to the Internet. </xui-message> </div> <xui-wizard class=sw-license-activation__offline-steps hide-header=false ng-model=vm.dialogOptions.viewModel.licenseActivation finish-text=_t(Activate) on-finish=vm.dialogOptions.viewModel.licenseActivation.activateLicenseOffline(cancellation) on-cancel=vm.dialogOptions.viewModel.licenseActivation.closeDialog() _ta> <xui-wizard-step label=first title=\"_t(Copy Machine ID)\" description=\"_t(Copy your machine's unique ID shown below.\n        You will need to get this ID onto a computer that has Internet access.\n        This ID is used to generate a license key file in the Customer Portal.)\" next-text=\"_t(Next >)\" on-enter=vm.dialogOptions.viewModel.licenseActivation.onOfflineFirstStepEnter() on-exit=vm.dialogOptions.viewModel.licenseActivation.preWizardTextOff() _ta> <div class=row> <span class=sw-license-activation__machine-id> <strong class=clearfix ng-model=vm.dialogOptions.viewModel.licenseActivation.activationToken>{{::vm.dialogOptions.viewModel.licenseActivation.activationToken}}</strong> </span> <span class=sw-license-activation__copy-to-clipboard> <xui-button size=medium display-style=tertiary icon=copy xui-clipboard=vm.dialogOptions.viewModel.licenseActivation.activationToken on-clipboard-success=vm.dialogOptions.viewModel.licenseActivation.onClipboardSuccess() _t> COPY TO CLIPBOARD</xui-button> </span> </div> </xui-wizard-step> <xui-wizard-step label=second title=\"_t(Download License File from Customer Portal)\" short-title=\"_t(Download License)\" description=\"_t(You must generate and download a license file from the Customer Portal.\n                            Take the machine ID you copied and move it onto a computer with Internet access.)\" next-text=\"_t(Next >)\" _ta> <ol> <li _t=\"['{{::vm.dialogOptions.viewModel.licenseActivation.swLicenseActivationConstants.links.customerPortal}}']\"> Login to the <a href={0} target=_blank rel=\"noopener noreferrer\" class=cl-customer-portal-link> Customer Portal (customerportal.solarwinds.com) </a> </li> <li _t>Navigate to your product and click Activate License Manually.</li> <li _t> In the Activate License form, enter the computer name of your Orion Server into the Computer Name field. </li> <li _t>Paste the unique machine ID you copied into the Unique Machine ID field.</li> <li _t>Press Generate License File to generate and download the License File.</li> <li _t>Move the downloaded License File back to the Orion server you're licensing.</li> </ol> </xui-wizard-step> <xui-wizard-step label=activate title=\"_t(Upload License Key to Orion Server)\" short-title=\"_t(Upload License Key)\" description=\"_t(On the machine you copied the Machine ID from, upload the license key file you downloaded from the Customer Portal.)\" on-enter=vm.dialogOptions.viewModel.licenseActivation.clearErrors() _ta> <xui-message type=error ng-show=vm.dialogOptions.viewModel.licenseActivation.errorMessage>{{vm.dialogOptions.viewModel.licenseActivation.errorMessage}}</xui-message> <xui-file-upload file=vm.dialogOptions.viewModel.licenseActivation.file accept=.lic on-change=vm.dialogOptions.viewModel.licenseActivation.clearErrors() is-required=true></xui-file-upload> </xui-wizard-step> </xui-wizard> </form> </div> </div> </div>";

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=sw-developer-page> <form name=developerForm novalidate> <h3>WebApi</h3> <xui-icon icon=networksegment status=\"{{viewModel.offline ? 'undefined' : 'up'}}\"></xui-icon> <div class=flex-end-row__container> <div class=\"flex-row__item url__input\"> <xui-textbox type=text id=apiEndpoint ng-model=viewModel.apiEndpoint caption=\"Test Endpoint\"></xui-textbox> </div> <div class=\"flex-row__item input-button\"> <xui-button ng-click=viewModel.testConnection()> <div _t>Ping</div> </xui-button> <xui-button ng-click=viewModel.testConnection(true)> <div _t>Ping (Administrators Only)</div> </xui-button> </div> </div> <br/> <xui-divider></xui-divider> <h3>Error handling</h3> <xui-checkbox ng-model=viewModel.suppressToastOnForceError>Supress Toast Message</xui-checkbox> <xui-button ng-click=viewModel.forceError() display-style=primary> <div _t>Force API error</div> </xui-button> <br/> <br/> <a href=developer/forceerror/ target=_self>500 error in rendering view</a> <br> <a href=random_page/ target=_self>view 404</a> <xui-divider></xui-divider> <h3>Feature Toggles</h3> <xui-listview stripe=true items-source=viewModel.featureToggles template-url=feature-toggle-template></xui-listview> <xui-divider></xui-divider> <h3>Demo scenarios</h3> <div class=flex-column__container> <div class=flex-column__item> <div class=flex-row__container> <div class=demo-header>REST</div> <code class=flex-row__item>/api2/ping</code></div> </div> <div class=flex-column__item> <xui-button ng-click=viewModel.testConnection() display-style=primary> <div _t>Get</div> </xui-button> </div> <div class=flex-column__item> <div class=flex-end-row__container> <xui-button ng-click=viewModel.postSuppressError() display-style=primary> <div _t>Post</div> </xui-button> <p class=flex-row__item> <xui-icon icon=severity_info></xui-icon> <span class=simple-label _t>suppresses demo error</span></p> </div> </div> <div class=flex-column__item> <div class=flex-end-row__container> <xui-button ng-click=viewModel.updateData() display-style=primary> <div _t>Put</div> </xui-button> <p class=flex-row__item> <xui-icon icon=severity_info></xui-icon> <span class=simple-label _t>returns mock data</span></p> </div> </div> <div class=flex-column__item> <xui-button ng-click=viewModel.deleteData() display-style=primary> <div _t>Delete</div> </xui-button> </div> </div> <br/> <div class=flex-column__container> <div class=\"demo-header flex-column__item\">Anchors & Links</div> <div class=flex-column__item> <a href=random_page/ target=_self>normal anchor</a> <br/> <a href=blocked_anchor_test_page/ target=_self>this should be blocked in demo (raw href)</a> <br/> <a href=blocked_anchor_test_page/ >this should also be blocked in demo (angular nav)</a> <br/> <a href=\"notorion/somepage.aspx?id=foo&name=bar\">blocked in demo based on url regex</a> </div> </div> <xui-divider></xui-divider> <h3>Auth</h3> <div class=flex-end-row__container> <div _t>the super-secret icon will appear for <em>admin</em> users:</div> <p class=flex-row__item sw-auth sw-auth-roles=admin> <xui-icon icon=status_unplugged></xui-icon> </p> </div> <div class=flex-column__container> <a href=developer/auth/admin target=_self>this link works for <em>admin</em> users</a> <a href=developer/auth/guest target=_self>this link works for <em>guest</em> users with <code>nodeManagement</code> or <code>alertManagement</code> permissions</a> </div> <xui-divider></xui-divider> <h3>SWIS/SWQL</h3> <sw-swql-query-editor query=\"'SELECT NodeID, InstanceType, Uri, InstanceSiteId FROM Orion.Nodes'\" show-records=true swql-validation=true title=\"SWQL Query\"> </sw-swql-query-editor> <br/> <xui-divider></xui-divider> <a href=developer/entity-popover> <h3>Entity popover</h3> </a> <xui-divider></xui-divider> <h3>Dialogs</h3> <h4>Sales Trigger</h4> <xui-radio ng-model=viewModel.salesTriggerType value=Eval>Eval</xui-radio> <xui-radio ng-model=viewModel.salesTriggerType value=Licensing>License</xui-radio> <xui-radio ng-model=viewModel.salesTriggerType value=Maintenance>Maintenance</xui-radio> <xui-radio ng-model=viewModel.salesTriggerType value=PollerLimit>Poller Limit</xui-radio> <xui-radio ng-model=viewModel.salesTriggerType value=Upgrade>Upgrade</xui-radio> <xui-button ng-click=viewModel.showSalesTriggerDialog() display-style=primary><div _t>Show Dialog</div></xui-button> <h4>Orion Improvement</h4> <xui-button ng-click=viewModel.showOrionImprovementDialog() display-style=primary><div _t>Show OIP Dialog</div></xui-button> <xui-divider></xui-divider> <h3>Activation Wizard</h3> <div class=flex-end-row__container> <p class=flex-row__item sw-auth sw-auth-roles=admin> <xui-message type=warning> For demo purposes you can find <b>test Activation Key <a ng-href={{::viewModel.demoFeaturesConfluencePage}} target=_blank>here</a></b><br/> Don't forget to <b>deactivate test license</b> (with infinite expiration date) at <a href=licensing/license-manager target=_self>License Manager's Page</a> </xui-message> </p> </div> <xui-radio ng-model=viewModel.connectionType value=online>Online</xui-radio> <xui-radio ng-model=viewModel.connectionType value=offline>Offline</xui-radio> <xui-radio ng-model=viewModel.connectionType value=\"\">Get view mode from \"licensing/onlinestate\" endpoint</xui-radio> <xui-button icon=add sw-license-activation-modal mode=viewModel.connectionType selected-license=viewModel.license dialog-title=viewModel.activationTitle on-activate=viewModel.onLicenseActivate(result)> <span>License Activation</span> </xui-button> </form> </xui-page-content> <script type=text/ng-template id=swql-default-template> <div>\n        {{item | json}}\n    </div> </script> <script type=text/ng-template id=feature-toggle-template> <div class=\"flex-row__container\">\n        <div class=\"flex-row__item\">\n            {{item.name}}\n        </div>\n        <div class=\"flex-row__item\">\n            <code>{{item.isEnabled ? 'on' : 'off'}}</code>\n        </div>\n    </span> </script> <script type=text/ng-template id=simple-result-template> <div class=\"flex-row__container\">\n        <xui-icon css-class=\"flex-row__item\" icon=\"unknownnode\"\n                  status=\"{{item.StatusIcon === 'Up.gif' ? 'up' : 'unknown'}}\"></xui-icon>\n        <div class=\"emphasis inline-hover flex-row__item\" xui-popover-title=\"IP Address\" xui-popover=\"{{item.DNS}}\"\n             xui-popover-trigger=\"mouseenter\">{{item.IPAddress}}\n        </div>\n        <div class=\"flex-row__item\">Last Sync <code>{{item.LastSync | date}}</code></div>\n    </div> </script> <script type=text/ng-template id=simple-result-template2> <div class=\"flex-row__container\">\n        <xui-icon css-class=\"flex-row__item\" icon=\"unknownnode\"\n                  status=\"{{item.StatusIcon === 'Up.gif' ? 'up' : 'unknown'}}\"></xui-icon>\n        <div class=\"emphasis inline-hover flex-row__item\" xui-popover-title=\"IP Address\" xui-popover=\"{{item.DNS}}\"\n             xui-popover-trigger=\"mouseenter\">{{item.IPAddress}}\n        </div>\n        <div class=\"flex-row__item\">Last Sync <code>{{item.LastSync | date}}</code></div>\n        <div class=\"flex-row__item\">Memory Used <code>{{item.PercentMemoryUsed ? item.PercentMemoryUsed : '?'}}</code>\n        </div>\n    </div> </script> ";

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <ul class=sw-discovered-resources> <li class=sw-discovered-resource ng-repeat=\"resource in modalOptions.resources\"> <div>{{ resource.Name }}</div> <div>{{ resource.Title }}</div> <div>{{ resource.Commands | json }}</div> <div>{{ resource.Settings | json }}</div> </li> </ul> </xui-dialog> ";

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=sw-entity-popover-view> <h2>Popovers available in Apollo</h2> <div class=row> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=N:1\">Node (NetObject style)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <span opid=0_Orion.Nodes_2 tipCtx=ownContext>Node (markers)</span> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Volumes\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=V:1\">Volume</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Groups\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=C:1\">Group (all ok)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Groups\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=C:2\">Group (some problems)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Groups\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=C:3\">Group (lots of problems)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.AlertActive\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=AAT:1\">Active Non-Global Alert</a> (should not show popover) </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.AlertActive\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=AAT:3\">Active Global Alert</a> </div> </div> <div class=row> <div class=col-md-1> <xui-icon icon='{{ \"Orion.NPM.Interfaces\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=I:1\">Interface</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.NPM.VSANs\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=NVS:1\">VSAN</a> </div> </div> <div class=row> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Test\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=TEST:1\">Custom entity</a> </div> </div> <div class=row> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=N:1000\">Not existing instance</a> </div> </div> <h2>Popovers to be implemented by modules</h2> <div class=row> <div class=col-md-1> <xui-icon icon='{{ \"Orion.APM.Application\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=AA:1\">Application</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.APM.Component\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=AM:1\">Component</a> </div> </div> <div class=row> <div class=col-md-1> <xui-icon icon='{{ \"orion.hardwarehealth.hardwareitem\" | swEntityIcon }}' status=small></xui-icon> <a href=\"#NetObject=HWHS:1\">Hardware</a> </div> </div> </xui-page-content> ";

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content> <header role=banner id=top> <h1>Demo page of common html elements with default styles</h1> <p>This is a test page filled with common HTML elements to be used to provide visual feedback whilst building CSS systems and frameworks.</p> </header> <nav role=navigation> <ul> <li> <a href=#text>Text</a> <ul> <li><a href=#text__headings>Headings</a></li> <li><a href=#text__paragraphs>Paragraphs</a></li> <li><a href=#text__blockquotes>Blockquotes</a></li> <li><a href=#text__lists>Lists</a></li> <li><a href=#text__hr>Horizontal rules</a></li> <li><a href=#text__tables>Tabular data</a></li> <li><a href=#text__code>Code</a></li> <li><a href=#text__inline>Inline elements</a></li> </ul> </li> <li> <a href=#embedded>Embedded content</a> <ul> <li><a href=#embedded__images>Images</a></li> <li><a href=#embedded__audio>Audio</a></li> <li><a href=#embedded__video>Video</a></li> <li><a href=#embedded__canvas>Canvas</a></li> <li><a href=#embedded__meter>Meter</a></li> <li><a href=#embedded__progress>Progress</a></li> <li><a href=#embedded__svg>Inline SVG</a></li> <li><a href=#embedded__iframe>IFrames</a></li> </ul> </li> <li> <a href=#forms>Form elements</a> <ul> <li><a href=#forms__input>Input fields</a></li> <li><a href=#forms__select>Select menus</a></li> <li><a href=#forms__checkbox>Checkboxes</a></li> <li><a href=#forms__radio>Radio buttons</a></li> <li><a href=#forms__textareas>Textareas</a></li> <li><a href=#forms__html5>HTML5 inputs</a></li> <li><a href=#forms__action>Action buttons</a></li> </ul> </li> </ul> </nav> <main role=main> <section id=text> <header> <h1>Text</h1> </header> <article id=text__headings> <header> <h1>Headings</h1> </header> <div> <h1>Heading 1</h1> <h2>Heading 2</h2> <h3>Heading 3</h3> <h4>Heading 4</h4> <h5>Heading 5</h5> <h6>Heading 6</h6> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=text__paragraphs> <header> <h1>Paragraphs</h1> </header> <div> <p>A paragraph (from the Greek paragraphos, �to write beside� or �written beside�) is a self-contained unit of a discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences. Though not required by the syntax of any language, paragraphs are usually an expected part of formal writing, used to organize longer prose.</p> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=text__blockquotes> <header> <h1>Blockquotes</h1> </header> <div> <blockquote> <p>A block quotation (also known as a long quotation or extract) is a quotation in a written document, that is set off from the main text as a paragraph, or block of text.</p> <p>It is typically distinguished visually using indentation and a different typeface or smaller size quotation. It may or may not include a citation, usually placed at the bottom.</p> <cite><a href=#!>Said no one, ever.</a></cite> </blockquote> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=text__lists> <header> <h1>Lists</h1> </header> <div> <h3>Definition list</h3> <dl> <dt>Definition List Title</dt> <dd>This is a definition list division.</dd> </dl> <h3>Ordered List</h3> <ol> <li>List Item 1</li> <li>List Item 2</li> <li>List Item 3</li> </ol> <h3>Unordered List</h3> <ul> <li>List Item 1</li> <li>List Item 2</li> <li>List Item 3</li> </ul> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=text__hr> <header> <h1>Horizontal rules</h1> </header> <div> <hr> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=text__tables> <header> <h1>Tabular data</h1> </header> <table> <caption>Table Caption</caption> <thead> <tr> <th>Table Heading 1</th> <th>Table Heading 2</th> <th>Table Heading 3</th> <th>Table Heading 4</th> <th>Table Heading 5</th> </tr> </thead> <tfoot> <tr> <th>Table Footer 1</th> <th>Table Footer 2</th> <th>Table Footer 3</th> <th>Table Footer 4</th> <th>Table Footer 5</th> </tr> </tfoot> <tbody> <tr> <td>Table Cell 1</td> <td>Table Cell 2</td> <td>Table Cell 3</td> <td>Table Cell 4</td> <td>Table Cell 5</td> </tr> <tr> <td>Table Cell 1</td> <td>Table Cell 2</td> <td>Table Cell 3</td> <td>Table Cell 4</td> <td>Table Cell 5</td> </tr> <tr> <td>Table Cell 1</td> <td>Table Cell 2</td> <td>Table Cell 3</td> <td>Table Cell 4</td> <td>Table Cell 5</td> </tr> <tr> <td>Table Cell 1</td> <td>Table Cell 2</td> <td>Table Cell 3</td> <td>Table Cell 4</td> <td>Table Cell 5</td> </tr> </tbody> </table> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=text__code> <header> <h1>Code</h1> </header> <div> <p><strong>Keyboard input:</strong> <kbd>Cmd</kbd></p> <p><strong>Inline code:</strong> <code>&lt;div&gt;code&lt;/div&gt;</code></p> <p><strong>Sample output:</strong> <samp>This is sample output from a computer program.</samp></p> <h2>Pre-formatted text</h2> <pre>P R E F O R M A T T E D T E X T ! \" # $ % &amp; ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; &lt; = &gt; ? @\n                        A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s\n                        t u v w x y z { | } ~ </pre> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=text__inline> <header> <h1>Inline elements</h1> </header> <div> <p><a href=#!>This is a text link</a>.</p> <p><strong>Strong is used to indicate strong importance.</strong></p> <p><em>This text has added emphasis.</em></p> <p>The <b>b element</b> is stylistically different text from normal text, without any special importance.</p> <p>The <i>i element</i> is text that is offset from the normal text.</p> <p>The <u>u element</u> is text with an unarticulated, though explicitly rendered, non-textual annotation.</p> <p> <del>This text is deleted</del> and <ins>This text is inserted</ins>.</p> <p> <s>This text has a strikethrough</s>.</p> <p>Superscript<sup>�</sup>.</p> <p>Subscript for things like H<sub>2</sub>O.</p> <p><small>This small text is small for for fine print, etc.</small></p> <p>Abbreviation: <abbr title=\"HyperText Markup Language\">HTML</abbr></p> <p><q cite=https://developer.mozilla.org/en-US/docs/HTML/Element/q>This text is a short inline quotation.</q></p> <p><cite>This is a citation.</cite></p> <p>The <dfn>dfn element</dfn> indicates a definition.</p> <p>The <mark>mark element</mark> indicates a highlight.</p> <p>The <var>variable element</var>, such as <var>x</var> = <var>y</var>.</p> <p>The time element: <time datetime=2013-04-06T12:32+00:00>2 weeks ago</time> </p> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> </section> <section id=embedded> <header> <h1>Embedded content</h1> </header> <article id=embedded__images> <header> <h2>Images</h2> </header> <div> <h3>No <code>&lt;figure&gt;</code> element</h3> <p><img src=https://placekitten.com/480/480 alt=\"Image alt text\"></p> <h3>Wrapped in a <code>&lt;figure&gt;</code> element, no <code>&lt;figcaption&gt;</code></h3> <figure><img src=https://placekitten.com/420/420 alt=\"Image alt text\"></figure> <h3>Wrapped in a <code>&lt;figure&gt;</code> element, with a <code>&lt;figcaption&gt;</code></h3> <figure> <img src=https://placekitten.com/420/420 alt=\"Image alt text\"> <figcaption>Here is a caption for this image.</figcaption> </figure> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=embedded__audio> <header> <h2>Audio</h2> </header> <div> <audio controls=\"\">audio</audio> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=embedded__video> <header> <h2>Video</h2> </header> <div> <video controls=\"\">video</video> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=embedded__canvas> <header> <h2>Canvas</h2> </header> <div> <canvas>canvas</canvas> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=embedded__meter> <header> <h2>Meter</h2> </header> <div> <meter value=2 min=0 max=10>2 out of 10</meter> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=embedded__progress> <header> <h2>Progress</h2> </header> <div> <progress>progress</progress> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=embedded__svg> <header> <h2>Inline SVG</h2> </header> <div> <svg width=100px height=100px> <circle cx=100 cy=100 r=100 fill=#1fa3ec></circle> </svg> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> <article id=embedded__iframe> <header> <h2>IFrame</h2> </header> <div> <iframe src=//placekitten.com/420/420 height=300></iframe> </div> <footer> <p><a href=#top>[Top]</a></p> </footer> </article> </section> <section id=forms> <header> <h1>Form elements</h1> </header> <form> <fieldset id=forms__input> <legend>Input fields</legend> <p> <label for=input__text>Text Input</label> <input id=input__text type=text placeholder=\"Text Input\"> </p> <p> <label for=input__password>Password</label> <input id=input__password type=password placeholder=\"Type your Password\"> </p> <p> <label for=input__webaddress>Web Address</label> <input id=input__webaddress type=url placeholder=http://yoursite.com> </p> <p> <label for=input__emailaddress>Email Address</label> <input id=input__emailaddress type=email placeholder=name@email.com> </p> <p> <label for=input__phone>Phone Number</label> <input id=input__phone type=tel placeholder=\"(999) 999-9999\"> </p> <p> <label for=input__search>Search</label> <input id=input__search type=search placeholder=\"Enter Search Term\"> </p> <p> <label for=input__text2>Number Input</label> <input id=input__text2 type=number placeholder=\"Enter a Number\"> </p> <p> <label for=input__text3 class=error>Error</label> <input id=input__text3 class=is-error type=text placeholder=\"Text Input\"> </p> <p> <label for=input__text4 class=valid>Valid</label> <input id=input__text4 class=is-valid type=text placeholder=\"Text Input\"> </p> </fieldset> <p><a href=#top>[Top]</a></p> <fieldset id=forms__select> <legend>Select menus</legend> <p> <label for=select>Select</label> <select id=select> <optgroup label=\"Option Group\"> <option>Option One</option> <option>Option Two</option> <option>Option Three</option> </optgroup> </select> </p> </fieldset> <p><a href=#top>[Top]</a></p> <fieldset id=forms__checkbox> <legend>Checkboxes</legend> <ul class=\"list list--bare\"> <li><label for=checkbox1><input id=checkbox1 name=checkbox type=checkbox checked=checked> Choice A</label></li> <li><label for=checkbox2><input id=checkbox2 name=checkbox type=checkbox> Choice B</label></li> <li><label for=checkbox3><input id=checkbox3 name=checkbox type=checkbox> Choice C</label></li> </ul> </fieldset> <p><a href=#top>[Top]</a></p> <fieldset id=forms__radio> <legend>Radio buttons</legend> <ul class=\"list list--bare\"> <li><label for=radio1><input id=radio1 name=radio type=radio class=radio checked=checked> Option 1</label></li> <li><label for=radio2><input id=radio2 name=radio type=radio class=radio> Option 2</label></li> <li><label for=radio3><input id=radio3 name=radio type=radio class=radio> Option 3</label></li> </ul> </fieldset> <p><a href=#top>[Top]</a></p> <fieldset id=forms__textareas> <legend>Textareas</legend> <p> <label for=textarea>Textarea</label> <textarea id=textarea rows=8 cols=48 placeholder=\"Enter your message here\"></textarea> </p> </fieldset> <p><a href=#top>[Top]</a></p> <fieldset id=forms__html5> <legend>HTML5 inputs</legend> <p> <label for=ic>Color input</label> <input type=color id=ic value=#000000> </p> <p> <label for=in>Number input</label> <input type=number id=in min=0 max=10 value=5> </p> <p> <label for=ir>Range input</label> <input type=range id=ir value=10> </p> <p> <label for=idd>Date input</label> <input type=date id=idd value=1970-01-01> </p> <p> <label for=idm>Month input</label> <input type=month id=idm value=1970-01> </p> <p> <label for=idw>Week input</label> <input type=week id=idw value=1970-W01> </p> <p> <label for=idt>Datetime input</label> <input type=datetime id=idt value=1970-01-01T00:00:00Z> </p> <p> <label for=idtl>Datetime-local input</label> <input type=datetime-local id=idtl value=1970-01-01T00:00> </p> </fieldset> <p><a href=#top>[Top]</a></p> <fieldset id=forms__action> <legend>Action buttons</legend> <p> <input type=submit value=\"<input type=submit>\"> <input type=button value=\"<input type=button>\"> <input type=reset value=\"<input type=reset>\"> <input type=submit value=\"<input disabled>\" disabled=disabled> </p> <p> <button type=submit>&lt;button type=submit&gt;</button> <button type=button>&lt;button type=button&gt;</button> <button type=reset>&lt;button type=reset&gt;</button> <button type=button disabled=disabled>&lt;button disabled&gt;</button> </p> </fieldset> <p><a href=#top>[Top]</a></p> </form> </section> </main> </xui-page-content> ";

/***/ }),
/* 33 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content> <style>input{width:100%}</style> <h1 _t>Hello world! This is a lovely day, isn't it?</h1> <p _t>This text is translated using '_t' directive - Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu, tanga a quickstepu.</p> <pre>&lt;p _t&gt;This text is translated using '_t' directive - Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu, tanga a quickstepu.&lt;/p&gt;</pre> <input value=\"_t(This input's value consists of translated text - Eble ĉiu kvazaŭ-deca fuŝĥoraĵo ĝojigos homtipon.)\" _ta/> <pre>&lt;input value=\"_t(This input's value consists of translated text - Eble ĉiu kvazaŭ-deca fuŝĥoraĵo ĝojigos homtipon.)\" _ta /&gt;</pre> <hr/> <p _t=\"[ 'something!' ]\">עטלף אבק נס דרך מזגן שהתפוצץ כי חם This text is translated using '_t' directive, with replacement: {0}</p> <pre>&lt;p _t=\"[ 'something!' ]\"&gt;עטלף אבק נס דרך מזגן שהתפוצץ כי חם This text is translated using '_t' directive, with replacement: {0}&lt;/p&gt;</pre> <input value=\"_t(This input's value consists of translated text, ऋषियों को सताने वाले दुष्ट राक्षसों के राजा रावण का सर्वनाश करने वाले विष्णुवतार भगवान श्रीराम, अयोध्या के महाराज दशरथ के बड़े सपुत्र थे। with replacement: {0})\" _ta=\"[ 'something!' ]\"/> <pre>&lt;input value=\"_t(This input's value consists of translated text, ऋषियों को सताने वाले दुष्ट राक्षसों के राजा रावण का सर्वनाश करने वाले विष्णुवतार भगवान श्रीराम, अयोध्या के महाराज दशरथ के बड़े सपुत्र थे। with replacement: {0})\" _ta=\"[ 'something!' ]\" /&gt;</pre> <hr/> <p>This paragraph contains text that is bound to the text from controller: {{ vm.something }}</p> <p>This paragraph contains text that is bound to the text from controller with one-time binding: {{ ::vm.something }}</p> <pre>vm.something = _t('いろはにほへと ちりぬるを わかよたれそ つねならむ うゐのおくやま けふこえて あさきゆめみし ゑひもせす')</pre> <hr/> <p>This paragraph contains text that is bound to the text that is taken from Api: {{ vm.takenFromApiText }}</p> <pre>return _t(\"příliš žluťoučký kůň úpěl ďábelské ódy\");</pre> <p _t>This paragraph contains translated text that is bound to the text that is taken from Api: {{ vm.takenFromApiText }}</p> <pre>&lt;p _t&gt;This paragraph contains translated text that is bound to the text that is taken from Api: {<wbr>{ vm.takenFromApiText }<wbr>}&lt;/p&gt;</pre> <p _t=\"[ '{{ vm.takenFromApiText }} {{ vm.takenFromApiText }}' ]\">This paragraph contains translated text with replacement from controller: {0}</p> <pre>&lt;p _t=\"[ '{<wbr>{ vm.takenFromApiText }<wbr>}' ]\"&gt;This paragraph contains translated text with replacement from controller: {0}&lt;/p&gt;</pre> <p>This paragraph contain text that is bound to the text that is loaded as Json: <code>{{ vm.takenFromApiJson }}</code> </p> <pre>{\n  \"Text\": \"_t(В чащах юга жил бы цитрус? Да, но фальшивый экземпляр!)\"\n}\n</pre> <pre>\n        var obj = i18nFile.DeserializeObject&lt;Deserialized&gt;(jsonPath)\n</pre> <span _t>This is text, that contains <b>some other</b> elements, <a href=https://www.solarwinds.com/ >including also link.</a></span> <pre>&lt;span _t&gt;This is text, that contains &lt;b&gt;some other&lt;/b&gt; elements, &lt;a href=\"https://www.solarwinds.com/\"&gt;including also link.&lt;/a&gt;&lt;/span&gt;</pre> <hr/> <p _t=\"[ '{{ ::vm.todayDate | swDate }}' ]\">This paragraph contains some date using replacement and filter: {0}</p> Next one will try to use XUI component with localized attribute: <button type=button class=\"btn btn-default\" xui-popover-placement=right xui-popover=\"_t(Open text on right)\" xui-popover-title=\"_t(i18n demo rocks)\" _t _ta>See? I'm localized! </button> <pre>&lt;button type=\"button\" class=\"btn btn-default\" xui-popover-placement=\"right\" xui-popover=\"_t(Open text on right)\" xui-popover-title=\"_t(i18n demo rocks)\" _t _ta&gt;See? I'm localized!&lt;/button&gt;</pre> <xui-dropdown size=medium caption=\"_t(Any text)\" items-source=vm.emptyArray _ta></xui-dropdown> <pre>&lt;xui-dropdown size=\"medium\" caption=\"_t(Any text)\" items-source=\"vm.emptyArray\" _ta&gt;&lt;/xui-dropdown&gt;</pre> </xui-page-content>";

/***/ }),
/* 34 */
/***/ (function(module, exports) {

module.exports = "<sw-orion-error error-report=vm.errorReport></sw-orion-error> ";

/***/ }),
/* 35 */
/***/ (function(module, exports) {

module.exports = "<xui-sidebar-container class=sw-subview-page layout=fill> <xui-sidebar settings=baseViewModel.settings class=sw-subview-page__sidebar> <script type=text/ng-template id=nav-item-template> <a class=\"nav-item-link NoTip\" target=\"_self\" ng-href=\"{{ ::item.pagePath }}\">\n                <div class=\"flex-row-container\" title=\"{{ ::item.shortTitle }}\">\n                    <div class=\"flex-row-item\">\n                        <img src=\"{{:: vm.getIconPath(item.viewIcon)}}\">\n                    </div>\n                    <div class=\"flex-row-item nav-item-title xui-ellipsis\">\n                        {{ ::item.shortTitle }}\n                    </div>\n                </div>\n            </a> </script> <xui-listview stripe=false items-source=baseViewModel.tabs template-url=nav-item-template selection=baseViewModel.navSelection selection-property=pagePath selection-mode=single row-padding=none controller=baseViewModel xui-if-show=baseViewModel.tabs.length> </xui-listview> </xui-sidebar> <div class=sw-subview-page__main-content> <ui-view> </ui-view> </div> </xui-sidebar-container> ";

/***/ }),
/* 36 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content is-busy=false page-layout=fill> <xui-page-header ng-show=viewModel.activeTab> <div class=\"sw-subview-page__header sw-status-{{viewModel.status}}__top\"> <div class=column-container> <div class=title-container-item> <xui-icon ng-if=viewModel.entityType icon=\"{{viewModel.entityType | swEntityIcon}}\" is-dynamic=true status=\"{{viewModel.status | swStatus:viewModel.childStatus}}\"></xui-icon> <span class=\"title xui-text-h2\">{{::viewModel.pageTitle}}</span> </div> </div> </div> </xui-page-header> <xui-page-breadcrumbs> <div class=\"flex-row-container pre-header\"> <xui-breadcrumb crumbs=viewModel.breadcrumbs></xui-breadcrumb> <div>{{viewModel.serverTime}}</div> </div> </xui-page-breadcrumbs> <ui-view> </ui-view> </xui-page-content> ";

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var AuthExpireDialogController = /** @class */ (function () {
    function AuthExpireDialogController($interval, $window, _t) {
        var _this = this;
        this.$interval = $interval;
        this.$window = $window;
        this._t = _t;
        this.$onInit = function () {
            _this.secondsRemaining = _.parseInt(_.toString(_this.secondsRemaining), 10);
            if (_this.secondsRemaining < 1) {
                throw new Error("Invalid argument: secondsRemaining=" + _this.secondsRemaining);
            }
            _this.decrement();
        };
        this.startCountdown = function () {
            _this.startTime = Date.now();
            _this._expireCountdown = _this.$interval(_this.decrement, 1000);
        };
        this.stopCountdown = function () {
            if (_this._expireCountdown) {
                _this.$interval.cancel(_this._expireCountdown);
                _this._expireCountdown = undefined;
            }
        };
        this.decrement = function () {
            if (_this.secondsRemaining > 1) {
                _this.secondsRemaining--;
                if (_this.secondsRemaining < 120) {
                    _this.timeRemaining = Math.floor(_this.secondsRemaining);
                    _this.timeType = _this.timeRemaining === 1 ? _this._t("second") : _this._t("seconds");
                }
                else {
                    _this.timeRemaining = Math.floor(_this.secondsRemaining / 60);
                    _this.timeType = _this._t("minutes");
                }
            }
            else {
                _this.stopCountdown();
                if (_this.isNoAction()) {
                    _this.goToLogoff();
                }
            }
        };
    }
    AuthExpireDialogController.prototype.goToLogoff = function () {
        var fullPath = encodeURIComponent(this.$window.location.pathname + this.$window.location.search);
        this.$window.location.href = "/Orion/Login.aspx?sessionTimeout=yes&ReturnUrl=" + fullPath;
    };
    AuthExpireDialogController.prototype.isNoAction = function () {
        return this.startTime > parseInt(localStorage.getItem("lastAction"), 10);
    };
    AuthExpireDialogController = __decorate([
        __param(0, decorators_1.Inject("$interval")),
        __param(1, decorators_1.Inject("$window")),
        __param(2, decorators_1.Inject("getTextService")),
        __metadata("design:paramtypes", [Function, Object, Function])
    ], AuthExpireDialogController);
    return AuthExpireDialogController;
}());
exports.default = AuthExpireDialogController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aEV4cGlyZURpYWxvZy1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aEV4cGlyZURpYWxvZy1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBTUEsK0NBRTBCO0FBRTFCO0lBRUksb0NBRVksU0FBMkIsRUFFM0IsT0FBdUIsRUFFdkIsRUFBb0M7UUFOaEQsaUJBUUM7UUFOVyxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUUzQixZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUV2QixPQUFFLEdBQUYsRUFBRSxDQUFrQztRQUl6QyxZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzFFLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixNQUFNLElBQUksS0FBSyxDQUFDLHdDQUFzQyxLQUFJLENBQUMsZ0JBQWtCLENBQUMsQ0FBQztZQUNuRixDQUFDO1lBQ0QsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLENBQUMsQ0FBQztRQU9LLG1CQUFjLEdBQUc7WUFDcEIsS0FBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDNUIsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqRSxDQUFDLENBQUM7UUFFSyxrQkFBYSxHQUFHO1lBQ25CLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM3QyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1lBQ3RDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFXTSxjQUFTLEdBQUc7WUFDaEIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN4QixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDOUIsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUN2RCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxhQUFhLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN0RixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDLENBQUM7b0JBQzVELEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDdkMsQ0FBQztZQUNMLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixLQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ3JCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDLENBQUM7SUFwREYsQ0FBQztJQTJCTywrQ0FBVSxHQUFsQjtRQUNJLElBQU0sUUFBUSxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsb0RBQWtELFFBQVUsQ0FBQztJQUM5RixDQUFDO0lBRU8sK0NBQVUsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBNUNnQiwwQkFBMEI7UUFHdEMsV0FBQSxtQkFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRW5CLFdBQUEsbUJBQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUVqQixXQUFBLG1CQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTs7T0FQWiwwQkFBMEIsQ0FpRTlDO0lBQUQsaUNBQUM7Q0FBQSxBQWpFRCxJQWlFQztrQkFqRW9CLDBCQUEwQiJ9

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function Inject(dependency) {
    return function (prototype, method, argumentPosition) {
        var inject = prototype.$inject || (prototype.$inject = []);
        inject[argumentPosition] = dependency;
    };
}
exports.Inject = Inject;
exports.default = Inject;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5qZWN0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaW5qZWN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsZ0JBQXVCLFVBQWtCO0lBQ3JDLE1BQU0sQ0FBQyxVQUFDLFNBQW1CLEVBQUUsTUFBdUIsRUFBRSxnQkFBd0I7UUFDMUUsSUFBTSxNQUFNLEdBQUcsU0FBUyxDQUFDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDN0QsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsVUFBVSxDQUFDO0lBQzFDLENBQUMsQ0FBQztBQUNOLENBQUM7QUFMRCx3QkFLQztBQUVELGtCQUFlLE1BQU0sQ0FBQyJ9

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AuthExpireDialogService = /** @class */ (function () {
    function AuthExpireDialogService(_t, dialogService, $log, $modal) {
        var _this = this;
        this._t = _t;
        this.dialogService = dialogService;
        this.$log = $log;
        this.$modal = $modal;
        this.settings = {
            backdrop: true,
            keyboard: true,
            windowClass: "xui-modal",
            windowTopClass: "xui-modal-top",
            templateUrl: "xui/components/dialog/dialog-service.html",
            backdropClass: "xui-modal-backdrop"
        };
        this.show = function (options) {
            _this.dialog = _this.showModal({
                template: "<sw-auth-expire-dialog seconds-remaining=\"" + options.secondsRemaining + "\">"
                    + "</sw-auth-expire-dialog>",
                windowClass: "sw-auth-expire-dialog"
            }, {
                title: _this._t("Hey! Anyone there?"),
                status: "warning",
                hideCancel: true,
                actionButtonText: _this._t("Keep me signed in!")
            });
            return _this.dialog.result;
        };
        this.showModal = function (customSettings, dialogOptions) {
            if (!customSettings) {
                customSettings = {};
            }
            customSettings.backdrop = "static";
            if (!dialogOptions) {
                _this.$log.error("xuiDialogService: No dialog options specified!");
                dialogOptions = {};
            }
            return _this.openModal(customSettings, dialogOptions);
        };
        this.openModal = function (customSettings, dialogOptions) {
            var modalSettings = {};
            if (dialogOptions.closeButtonText) {
                dialogOptions.cancelButtonText = dialogOptions.closeButtonText;
                _this.$log.warn([
                    "DialogService.show - IDialogOptions.closeButtonText is deprecated.",
                    " Please use IDialogOptions.cancelButtonText instead."
                ].join(""));
            }
            // Map angular-ui modal custom defaults to modal defaults defined in this service
            angular.extend(modalSettings, _this.settings, customSettings);
            if (!modalSettings.controller) {
                modalSettings.controller = "xuiDialogInstanceController";
                modalSettings.bindToController = true;
                modalSettings.controllerAs = "vm";
                modalSettings.resolve = {
                    dialogOptions: function () { return dialogOptions; }
                };
            }
            return _this.$modal.open(modalSettings);
        };
    }
    AuthExpireDialogService.prototype.close = function () {
        if (this.dialog) {
            this.dialog.close();
        }
    };
    AuthExpireDialogService.$inject = ["getTextService", "xuiDialogService", "$log", "$uibModal"];
    return AuthExpireDialogService;
}());
exports.default = AuthExpireDialogService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aEV4cGlyZURpYWxvZy1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aEV4cGlyZURpYWxvZy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBTXZDO0lBR0ksaUNBQW9CLEVBQW9DLEVBQ3BDLGFBQWlDLEVBQ2pDLElBQW1CLEVBQ25CLE1BQXlDO1FBSDdELGlCQUdrRTtRQUg5QyxPQUFFLEdBQUYsRUFBRSxDQUFrQztRQUNwQyxrQkFBYSxHQUFiLGFBQWEsQ0FBb0I7UUFDakMsU0FBSSxHQUFKLElBQUksQ0FBZTtRQUNuQixXQUFNLEdBQU4sTUFBTSxDQUFtQztRQUdyRCxhQUFRLEdBQXVDO1lBQ25ELFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxXQUFXLEVBQUUsV0FBVztZQUN4QixjQUFjLEVBQUUsZUFBZTtZQUMvQixXQUFXLEVBQUUsMkNBQTJDO1lBQ3hELGFBQWEsRUFBRSxvQkFBb0I7U0FDdEMsQ0FBQztRQUVLLFNBQUksR0FBRyxVQUFDLE9BQXdDO1lBQ25ELEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQztnQkFDckIsUUFBUSxFQUFFLGdEQUE2QyxPQUFPLENBQUMsZ0JBQWdCLFFBQUk7c0JBQ2pGLDBCQUEwQjtnQkFDNUIsV0FBVyxFQUFFLHVCQUF1QjthQUN2QyxFQUFFO2dCQUNDLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLG9CQUFvQixDQUFDO2dCQUNwQyxNQUFNLEVBQUUsU0FBUztnQkFDakIsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsb0JBQW9CLENBQUM7YUFDbEQsQ0FBQyxDQUFDO1lBRVAsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzlCLENBQUMsQ0FBQTtRQVFPLGNBQVMsR0FBRyxVQUFDLGNBQWtELEVBQ25ELGFBQTRCO1lBQzVDLEVBQUUsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDbEIsY0FBYyxHQUFHLEVBQUUsQ0FBQztZQUN4QixDQUFDO1lBQ0QsY0FBYyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7WUFDbkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO2dCQUNsRSxhQUFhLEdBQVEsRUFBRSxDQUFDO1lBQzVCLENBQUM7WUFDRCxNQUFNLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDekQsQ0FBQyxDQUFDO1FBRU0sY0FBUyxHQUFHLFVBQUMsY0FBa0QsRUFDdkQsYUFBNEI7WUFDeEMsSUFBSSxhQUFhLEdBQXVDLEVBQUUsQ0FBQztZQUMzRCxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDaEMsYUFBYSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQyxlQUFlLENBQUM7Z0JBQy9ELEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNYLG9FQUFvRTtvQkFDcEUsc0RBQXNEO2lCQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUUsQ0FBQztZQUNELGlGQUFpRjtZQUNqRixPQUFPLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxLQUFJLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1lBQzdELEVBQUUsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLGFBQWEsQ0FBQyxVQUFVLEdBQUcsNkJBQTZCLENBQUM7Z0JBQ3pELGFBQWEsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3RDLGFBQWEsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUNsQyxhQUFhLENBQUMsT0FBTyxHQUFHO29CQUNwQixhQUFhLEVBQUUsY0FBTSxPQUFBLGFBQWEsRUFBYixDQUFhO2lCQUNyQyxDQUFDO1lBQ04sQ0FBQztZQUNELE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMzQyxDQUFDLENBQUM7SUFsRStELENBQUM7SUEyQjNELHVDQUFLLEdBQVo7UUFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDeEIsQ0FBQztJQUNMLENBQUM7SUFwQ2EsK0JBQU8sR0FBRyxDQUFDLGdCQUFnQixFQUFFLGtCQUFrQixFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUMsQ0FBQztJQXdFeEYsOEJBQUM7Q0FBQSxBQXpFRCxJQXlFQztrQkF6RW9CLHVCQUF1QiJ9

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SalesTriggerDescriptorService = /** @class */ (function () {
    function SalesTriggerDescriptorService(_t, $log) {
        var _this = this;
        this._t = _t;
        this.$log = $log;
        this.getTitle = function (type) {
            return _this.typeDescriptor[type].title;
        };
        this.setTitle = function (type, title) {
            return _this.typeDescriptor[type].title = title;
        };
        this.getImgClass = function (type) {
            return _this.typeDescriptor[type].imgClass;
        };
        this.getDescriptionText = function (type) {
            return _this.typeDescriptor[type].desc.text;
        };
        this.getDescriptionTemplate = function (type) {
            return _this.typeDescriptor[type].desc.template;
        };
        this.showDetailsButton = function (type) {
            return _this.typeDescriptor[type].detailsButton.show;
        };
        this.getDetailsButtonLink = function (type) {
            return _this.typeDescriptor[type].detailsButton.link;
        };
        this.getDetailsButtonText = function (type) {
            return _this.typeDescriptor[type].detailsButton.text;
        };
        this.getColumns = function (type) {
            var columns = _this.typeDescriptor[type].columns;
            return _.transform(columns, function (result, column) {
                result.push({ header: column.header, class: column.class });
            }, []);
        };
        this.getTemplate = function (type) {
            return _this.typeDescriptor[type].template;
        };
        this.getFooter = function (type) {
            return _this.typeDescriptor[type].footer;
        };
        this.typeDescriptor = {
            "Eval": {
                title: this._t("Evaluation Is Ending!"),
                imgClass: "sw-salestrigger-evaluation",
                desc: { text: this._t("Purchase licenses to continue using key functionality.") },
                detailsButton: {
                    show: true, text: this._t("Estimate Purchase Cost"),
                    link: "https://www.solarwinds.com/embedded_in_products/productLink.aspx" +
                        "?id=online_quote&CMP=PRD-IPS-Settings-EvalPopup-X-NOIP-X"
                },
                columns: [
                    { header: this._t("License"), class: "col-sm-7" },
                    { header: this._t("Expiration Date"), class: "col-sm-5" }
                ],
                template: "sales-trigger-eval.html",
                footer: {
                    show: true,
                    emailTemplate: "sales-trigger-footer-email-sales.html",
                    rightTemplate: "sales-trigger-footer-right-default.html"
                }
            },
            "Licensing": {
                title: this._t("Reached License Limit!"),
                imgClass: "sw-salestrigger-license",
                desc: { text: this._t("Upgrade your license to the next tier to continue adding new objects.") },
                detailsButton: { show: false, text: "", link: "" },
                columns: [
                    { header: this._t("License"), class: "col-sm-4 gutter__expander-header" },
                    { header: this._t("Current usage"), class: "col-sm-7" }
                ],
                template: "sales-trigger-license.html",
                footer: {
                    show: true,
                    emailTemplate: "sales-trigger-footer-email-sales.html",
                    rightTemplate: "sales-trigger-footer-right-default.html"
                }
            },
            "Maintenance": {
                title: this._t("Maintenance Is Expiring!"),
                imgClass: "sw-salestrigger-maintenance",
                desc: {
                    text: this._t("Renew now to continue getting access to new product releases,\
                technical support, and much more.")
                },
                detailsButton: {
                    show: true,
                    text: this._t("View Maintenance Details"),
                    link: "https://customerportal.solarwinds.com/renew-maintenance/calculator"
                },
                columns: [
                    { header: this._t("License"), class: "col-sm-7" },
                    { header: this._t("Expiration Date"), class: "col-sm-5" }
                ],
                template: "sales-trigger-maintenance.html",
                footer: {
                    show: true,
                    emailTemplate: "sales-trigger-footer-email-maintenance.html",
                    rightTemplate: "sales-trigger-footer-right-maintenance.html"
                }
            },
            "PollerLimit": {
                title: this._t("Polling Rate Limit Exceeded!"),
                imgClass: "sw-salestrigger-polling",
                desc: {
                    text: this._t("Add an additional poller to continue at your current polling intervals."),
                    template: "sales-trigger-poller-limit-desc.html"
                },
                detailsButton: {
                    show: true, text: this._t("View Product Usage Report To Estimate Cost"),
                    link: "http://localhost/Orion/SummaryView.aspx?ViewID=1#"
                },
                columns: [
                    { header: this._t("Polling Engine"), class: "col-sm-7" },
                    { header: this._t("Current usage"), class: "col-sm-5" }
                ],
                template: "sales-trigger-poller-limit.html",
                footer: {
                    show: true,
                    emailTemplate: "sales-trigger-footer-email-sales.html",
                    rightTemplate: "sales-trigger-footer-right-default.html"
                }
            },
            "Upgrade": {
                title: "",
                imgClass: "sw-salestrigger-upgrade",
                desc: { text: this._t("The following updates are available for your Orion product(s).") },
                detailsButton: {
                    show: true,
                    text: this._t("Plan Your Upgrade"),
                    link: "https://customerportal.solarwinds.com/support/product-upgrade-advisor?CMP=PRD-IPS-UpdateNotification-ProductUpgradeAdvisor-X-X-X"
                },
                columns: [
                    { header: this._t("License"), class: "col-sm-7" },
                    { header: this._t("New version available"), class: "col-sm-5" }
                ],
                template: "sales-trigger-upgrade.html",
                footer: { show: false, emailTemplate: "", rightTemplate: "" }
            }
        };
    }
    SalesTriggerDescriptorService.$inject = ["getTextService", "$log"];
    return SalesTriggerDescriptorService;
}());
exports.default = SalesTriggerDescriptorService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FsZXNUcmlnZ2VyRGVzY3JpcHRvclNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzYWxlc1RyaWdnZXJEZXNjcmlwdG9yU2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQW9DdkM7SUFJSSx1Q0FBb0IsRUFBb0MsRUFBVSxJQUFvQjtRQUF0RixpQkFtR0M7UUFuR21CLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBZ0I7UUFxRy9FLGFBQVEsR0FBRyxVQUFDLElBQXNCO1lBQ3JDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMzQyxDQUFDLENBQUM7UUFFSyxhQUFRLEdBQUcsVUFBQyxJQUFzQixFQUFFLEtBQWE7WUFDcEQsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuRCxDQUFDLENBQUM7UUFFSyxnQkFBVyxHQUFHLFVBQUMsSUFBc0I7WUFDeEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQzlDLENBQUMsQ0FBQztRQUVLLHVCQUFrQixHQUFHLFVBQUMsSUFBc0I7WUFDL0MsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUMvQyxDQUFDLENBQUM7UUFFSywyQkFBc0IsR0FBRyxVQUFDLElBQXNCO1lBQ25ELE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDbkQsQ0FBQyxDQUFDO1FBRUssc0JBQWlCLEdBQUcsVUFBQyxJQUFzQjtZQUM5QyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1FBQ3hELENBQUMsQ0FBQztRQUVLLHlCQUFvQixHQUFHLFVBQUMsSUFBc0I7WUFDakQsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztRQUN4RCxDQUFDLENBQUM7UUFFSyx5QkFBb0IsR0FBRyxVQUFDLElBQXNCO1lBQ2pELE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7UUFDeEQsQ0FBQyxDQUFDO1FBRUssZUFBVSxHQUFHLFVBQUMsSUFBc0I7WUFDdkMsSUFBTSxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFDbEQsTUFBTSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLFVBQUMsTUFBaUIsRUFBRSxNQUFlO2dCQUN6RCxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO1lBQzVELENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNmLENBQUMsQ0FBQztRQUVLLGdCQUFXLEdBQUcsVUFBQyxJQUFzQjtZQUN4QyxNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUM7UUFDOUMsQ0FBQyxDQUFDO1FBRUssY0FBUyxHQUFHLFVBQUMsSUFBc0I7WUFDdEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzVDLENBQUMsQ0FBQztRQWpKRSxJQUFJLENBQUMsY0FBYyxHQUFHO1lBQ2xCLE1BQU0sRUFBRTtnQkFDSixLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztnQkFDdkMsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsd0RBQXdELENBQUMsRUFBRTtnQkFDakYsYUFBYSxFQUFFO29CQUNYLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsd0JBQXdCLENBQUM7b0JBQ25ELElBQUksRUFBRSxrRUFBa0U7d0JBQ3BFLDBEQUEwRDtpQkFDakU7Z0JBQ0QsT0FBTyxFQUFFO29CQUNMLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBQztvQkFDL0MsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUM7aUJBQzFEO2dCQUNELFFBQVEsRUFBRSx5QkFBeUI7Z0JBQ25DLE1BQU0sRUFBRTtvQkFDSixJQUFJLEVBQUUsSUFBSTtvQkFDVixhQUFhLEVBQUUsdUNBQXVDO29CQUN0RCxhQUFhLEVBQUUseUNBQXlDO2lCQUMzRDthQUNKO1lBQ0QsV0FBVyxFQUFFO2dCQUNULEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO2dCQUN4QyxRQUFRLEVBQUUseUJBQXlCO2dCQUNuQyxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyx1RUFBdUUsQ0FBQyxFQUFFO2dCQUNoRyxhQUFhLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRTtnQkFDbEQsT0FBTyxFQUFFO29CQUNMLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUUsS0FBSyxFQUFFLGtDQUFrQyxFQUFDO29CQUN2RSxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLGVBQWUsQ0FBQyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUM7aUJBQ3hEO2dCQUNELFFBQVEsRUFBRSw0QkFBNEI7Z0JBQ3RDLE1BQU0sRUFBRTtvQkFDSixJQUFJLEVBQUUsSUFBSTtvQkFDVixhQUFhLEVBQUUsdUNBQXVDO29CQUN0RCxhQUFhLEVBQUUseUNBQXlDO2lCQUMzRDthQUNKO1lBQ0QsYUFBYSxFQUFFO2dCQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO2dCQUMxQyxRQUFRLEVBQUUsNkJBQTZCO2dCQUN2QyxJQUFJLEVBQUU7b0JBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUM7a0RBQ29CLENBQUM7aUJBQ2xDO2dCQUNELGFBQWEsRUFBRTtvQkFDWCxJQUFJLEVBQUUsSUFBSTtvQkFDVixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztvQkFDekMsSUFBSSxFQUFFLG9FQUFvRTtpQkFDN0U7Z0JBQ0QsT0FBTyxFQUFFO29CQUNMLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBQztvQkFDL0MsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUM7aUJBQUM7Z0JBQzVELFFBQVEsRUFBRSxnQ0FBZ0M7Z0JBQzFDLE1BQU0sRUFBRTtvQkFDSixJQUFJLEVBQUUsSUFBSTtvQkFDVixhQUFhLEVBQUUsNkNBQTZDO29CQUM1RCxhQUFhLEVBQUUsNkNBQTZDO2lCQUMvRDthQUNKO1lBQ0QsYUFBYSxFQUFFO2dCQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO2dCQUM5QyxRQUFRLEVBQUUseUJBQXlCO2dCQUNuQyxJQUFJLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMseUVBQXlFLENBQUM7b0JBQ3hGLFFBQVEsRUFBRSxzQ0FBc0M7aUJBQ25EO2dCQUNELGFBQWEsRUFBRTtvQkFDWCxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLDRDQUE0QyxDQUFDO29CQUN2RSxJQUFJLEVBQUUsbURBQW1EO2lCQUM1RDtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUM7b0JBQ3RELEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsZUFBZSxDQUFDLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBQztpQkFDeEQ7Z0JBQ0QsUUFBUSxFQUFFLGlDQUFpQztnQkFDM0MsTUFBTSxFQUFFO29CQUNKLElBQUksRUFBRSxJQUFJO29CQUNWLGFBQWEsRUFBRSx1Q0FBdUM7b0JBQ3RELGFBQWEsRUFBRSx5Q0FBeUM7aUJBQzNEO2FBQ0o7WUFDRCxTQUFTLEVBQUU7Z0JBQ1AsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtnQkFDbkMsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsZ0VBQWdFLENBQUMsRUFBRTtnQkFDekYsYUFBYSxFQUFFO29CQUNYLElBQUksRUFBRSxJQUFJO29CQUNWLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO29CQUNsQyxJQUFJLEVBQUUsa0lBQWtJO2lCQUMzSTtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFDO29CQUMvQyxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBQztpQkFDaEU7Z0JBQ0QsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxhQUFhLEVBQUUsRUFBRSxFQUFFLGFBQWEsRUFBRSxFQUFFLEVBQUU7YUFDaEU7U0FDSixDQUFDO0lBQ04sQ0FBQztJQXBHYSxxQ0FBTyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFvSnZELG9DQUFDO0NBQUEsQUF2SkQsSUF1SkM7a0JBdkpvQiw2QkFBNkIifQ==

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var salesTriggerDescriptorService_1 = __webpack_require__(40);
var SalesTriggerDialogController = /** @class */ (function () {
    function SalesTriggerDialogController($scope, descriptorService, licenseService, notifyService) {
        var _this = this;
        this.$scope = $scope;
        this.descriptorService = descriptorService;
        this.licenseService = licenseService;
        this.notifyService = notifyService;
        this.isBusy = true;
        this.columns = [];
        this.showCancelReminder = true;
        this.$onInit = function () {
            _this.initDescriptorInfo();
            _this.initLicenseInfo();
        };
        this.suppressReminder = function () {
            _this.notifyService.suppressReminder(_this.type)
                .then(function () {
                _this.$scope.$close();
            });
        };
        this.initDescriptorInfo = function () {
            _this.template = _this.descriptorService.getTemplate(_this.type);
            _this.imgClass = _this.descriptorService.getImgClass(_this.type);
            _this.description = _this.descriptorService.getDescriptionText(_this.type);
            _this.descTemplate = _this.descriptorService.getDescriptionTemplate(_this.type);
            _this.columns = _this.descriptorService.getColumns(_this.type);
            _this.showDetailsBtn = _this.descriptorService.showDetailsButton(_this.type);
            _this.detailsText = _this.descriptorService.getDetailsButtonText(_this.type);
            _this.detailsLink = _this.descriptorService.getDetailsButtonLink(_this.type);
            _this.showFooter = _this.descriptorService.getFooter(_this.type).show;
            _this.footerEmailTemplate = _this.descriptorService.getFooter(_this.type).emailTemplate;
            _this.footerRightTemplate = _this.descriptorService.getFooter(_this.type).rightTemplate;
        };
        this.initLicenseInfo = function () {
            var promise;
            if (_this.type === "Licensing") {
                promise = _this.licenseService.getSaturationInfo();
                promise.then(function (saturationInfo) {
                    _this.saturationInfo = saturationInfo;
                });
            }
            else if (_this.type === "Maintenance") {
                promise = _this.licenseService.getMaintenanceItems();
                promise.then(function (items) {
                    _this.maintenanceItems = items;
                });
            }
            else if (_this.type === "PollerLimit") {
                promise = _this.licenseService.getPollerLimitItems();
                promise.then(function (items) {
                    _this.pollerLimitItems = items;
                });
            }
            else if (_this.type === "Upgrade") {
                promise = _this.licenseService.getUpgradeSummary();
                promise.then(function (summary) {
                    _this.upgradeSummary = summary;
                });
            }
            else {
                promise = _this.licenseService.getLicenses();
                promise.then(function (licenses) {
                    _this.licenses = licenses;
                });
            }
            promise.finally(function () { return _this.isBusy = false; });
        };
    }
    SalesTriggerDialogController = __decorate([
        __param(0, decorators_1.Inject("$scope")),
        __param(1, decorators_1.Inject("swSalesTriggerDescriptorService")),
        __param(2, decorators_1.Inject("swLicenseService")),
        __param(3, decorators_1.Inject("notifyService")),
        __metadata("design:paramtypes", [Object, salesTriggerDescriptorService_1.default, Object, Object])
    ], SalesTriggerDialogController);
    return SalesTriggerDialogController;
}());
exports.default = SalesTriggerDialogController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FsZXNUcmlnZ2VyRGlhbG9nQ29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNhbGVzVHJpZ2dlckRpYWxvZ0NvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7Ozs7Ozs7Ozs7Ozs7QUFPdkMsK0NBQTBDO0FBRzFDLGlGQUE0RTtBQUU1RTtJQXFCSSxzQ0FFWSxNQUFtQyxFQUVuQyxpQkFBZ0QsRUFFaEQsY0FBK0IsRUFFL0IsYUFBNkI7UUFSekMsaUJBVUM7UUFSVyxXQUFNLEdBQU4sTUFBTSxDQUE2QjtRQUVuQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQStCO1FBRWhELG1CQUFjLEdBQWQsY0FBYyxDQUFpQjtRQUUvQixrQkFBYSxHQUFiLGFBQWEsQ0FBZ0I7UUE1QmxDLFdBQU0sR0FBWSxJQUFJLENBQUM7UUFNdkIsWUFBTyxHQUFjLEVBQUUsQ0FBQztRQU94Qix1QkFBa0IsR0FBWSxJQUFJLENBQUM7UUFtQm5DLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQzFCLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUM7UUFFSyxxQkFBZ0IsR0FBRztZQUN0QixLQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ3pDLElBQUksQ0FBQztnQkFDRixLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRU0sdUJBQWtCLEdBQUc7WUFDekIsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5RCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlELEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4RSxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0UsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1RCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUUsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsb0JBQW9CLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFFLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxRSxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNuRSxLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsYUFBYSxDQUFDO1lBQ3JGLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDekYsQ0FBQyxDQUFDO1FBRU0sb0JBQWUsR0FBRztZQUN0QixJQUFJLE9BQXlCLENBQUM7WUFDOUIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2dCQUNsRCxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQUMsY0FBd0M7b0JBQ2xELEtBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO2dCQUN6QyxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2dCQUNwRCxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBeUI7b0JBQ25DLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsSUFBSSxLQUFLLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLE9BQU8sR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixFQUFFLENBQUM7Z0JBQ3BELE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUF5QjtvQkFDbkMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDbEMsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDakMsT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQkFDbEQsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFDLE9BQXdCO29CQUNsQyxLQUFJLENBQUMsY0FBYyxHQUFHLE9BQU8sQ0FBQztnQkFDbEMsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQzVDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFvQjtvQkFDOUIsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztZQUVELE9BQU8sQ0FBQyxPQUFPLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxFQUFuQixDQUFtQixDQUFDLENBQUM7UUFDL0MsQ0FBQyxDQUFDO0lBMURGLENBQUM7SUEvQmdCLDRCQUE0QjtRQXNCeEMsV0FBQSxtQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBRWhCLFdBQUEsbUJBQU0sQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFBO1FBRXpDLFdBQUEsbUJBQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1FBRTFCLFdBQUEsbUJBQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtpREFIRyx1Q0FBNkI7T0F6QjNDLDRCQUE0QixDQTBGaEQ7SUFBRCxtQ0FBQztDQUFBLEFBMUZELElBMEZDO2tCQTFGb0IsNEJBQTRCIn0=

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var mainNav_directive_1 = __webpack_require__(97);
var mainNavMenu_directive_1 = __webpack_require__(99);
exports.default = function (module) {
    module.component("swMainNav", mainNav_directive_1.default);
    module.component("swMainNavMenu", mainNavMenu_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHlEQUEwQztBQUMxQyxpRUFBa0Q7QUFHbEQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLDJCQUFPLENBQUMsQ0FBQztJQUN2QyxNQUFNLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSwrQkFBVyxDQUFDLENBQUM7QUFDbkQsQ0FBQyxDQUFDIn0=

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var authExpireDialog_service_1 = __webpack_require__(39);
var IdleSensorController = /** @class */ (function () {
    function IdleSensorController($log, authExpireDialog, $window, constants, $interval, $http) {
        var _this = this;
        this.$log = $log;
        this.authExpireDialog = authExpireDialog;
        this.$window = $window;
        this.constants = constants;
        this.$interval = $interval;
        this.$http = $http;
        this.actionStorageKey = "lastAction";
        this.$onInit = function () {
            _this.recordActivity();
            _this.renewSessionAndScheduleAlertCheck();
        };
        this.$onDestroy = function () {
            _this.$log.debug("destroying idle sensor");
            _this.cleanAlertCheck();
        };
        this.onActivity = _.debounce(function () { return _this.recordActivity(); }, 1000);
    }
    IdleSensorController.prototype.monitorElement = function (element) {
        this.$log.debug("idle-sensor monitoring", element[0]);
        element.on(IdleSensorController.EventsToMonitor, this.onActivity);
    };
    // Use browser`s local storage for having one lastActivity time for all tabs
    IdleSensorController.prototype.setLastActivity = function (lastAction) {
        return localStorage.setItem(this.actionStorageKey, lastAction.toString());
    };
    IdleSensorController.prototype.getLastActivity = function () {
        return Number(localStorage.getItem(this.actionStorageKey));
    };
    // Checks whether user made an action
    IdleSensorController.prototype.isNewAction = function () {
        return this.lastPing < this.getLastActivity();
    };
    IdleSensorController.prototype.fireRenewRequest = function () {
        var _this = this;
        var endpoint = this.constants.environment.contentApiEndpoint;
        var pingPath = "/orion/ping.aspx";
        var requestUrl = endpoint
            ? endpoint.replace(/\/ui\/api.*/, pingPath)
            : "" + this.$window.top.location.origin + pingPath;
        this.$http.post(requestUrl, {}, {
            cache: false,
        }).then(function (response) {
            _this.$log.debug("ping responded", response);
        }).catch(function (reason) {
            _this.$log.warn("ping failed", reason);
        });
    };
    IdleSensorController.prototype.renewSession = function () {
        this.$log.debug("renew session");
        if (this.isNocView()) {
            this.recordActivity();
        }
        this.lastPing = Date.now();
        this.fireRenewRequest();
    };
    IdleSensorController.prototype.recordActivity = function () {
        this.$log.debug("record activity");
        this.setLastActivity(Date.now());
    };
    IdleSensorController.prototype.getTimeSince = function (timestamp) {
        return (timestamp === undefined) ? 0 : Date.now() - timestamp;
    };
    IdleSensorController.prototype.getLogoffTimeout = function () {
        return this.constants.environment.authTimeoutMilliseconds;
    };
    IdleSensorController.prototype.getAlertTimeout = function () {
        return this.getLogoffTimeout() * 0.8;
    };
    IdleSensorController.prototype.getLogoffTimeoutSince = function (timestamp) {
        return this.getLogoffTimeout() - this.getTimeSince(timestamp) - 5000;
    };
    IdleSensorController.prototype.getAlertTimeoutSince = function (timestamp) {
        return this.getAlertTimeout() - this.getTimeSince(timestamp) - 5000;
    };
    IdleSensorController.prototype.isLogoffTimeoutSet = function () {
        return this.getLogoffTimeout() > 0;
    };
    IdleSensorController.prototype.isNocView = function () {
        var rxNocView = /isnocview/i;
        var search = this.$window.top.location.search.toLowerCase();
        return rxNocView.test(search);
    };
    IdleSensorController.prototype.cleanAlertCheck = function () {
        if (this.alertCheckTimerRef) {
            this.$interval.cancel(this.alertCheckTimerRef);
            this.alertCheckTimerRef = undefined;
        }
    };
    IdleSensorController.prototype.renewSessionAndScheduleAlertCheck = function () {
        var _this = this;
        this.$log.debug("scheduleAlertCheck");
        this.renewSession();
        if (!this.isLogoffTimeoutSet()) {
            return;
        }
        this.cleanAlertCheck();
        // $interval is used instead of $timeout to prevent e2e/protractor lock
        this.alertCheckTimerRef = this.$interval(function () {
            _this.checkForDueAlert();
        }, this.getAlertTimeoutSince(this.getLastActivity()), 1);
    };
    IdleSensorController.prototype.checkForDueAlert = function () {
        this.$log.debug("checkForDueAlert");
        if (this.isNocView() || this.isNewAction()) {
            this.renewSessionAndScheduleAlertCheck();
            return;
        }
        var secondsRemaining = this.getLogoffTimeoutSince(this.getLastActivity()) / 1000;
        this.alertUser(secondsRemaining);
    };
    IdleSensorController.prototype.alertUser = function (secondsRemaining) {
        var _this = this;
        this.$log.debug("alertUser");
        // Checks user`s activity every second(did he close the expire dialog? if so, then close it)
        var expireDialogCheckRef = this.$interval(function () {
            if (_this.isNewAction()) {
                _this.$log.debug("new action recorded, close dialog");
                _this.$interval.cancel(expireDialogCheckRef);
                _this.authExpireDialog.close();
            }
        }, 1000);
        this.authExpireDialog
            .show({
            secondsRemaining: Math.max(1, secondsRemaining - 1),
        })
            .finally(function () {
            _this.$interval.cancel(expireDialogCheckRef);
            _this.recordActivity();
            _this.renewSessionAndScheduleAlertCheck();
        });
    };
    IdleSensorController.EventsToMonitor = "mousemove keydown touch scroll";
    IdleSensorController = __decorate([
        __param(0, decorators_1.Inject("$log")),
        __param(1, decorators_1.Inject("swAuthExpireDialogService")),
        __param(2, decorators_1.Inject("$window")),
        __param(3, decorators_1.Inject("constants")),
        __param(4, decorators_1.Inject("$interval")),
        __param(5, decorators_1.Inject("$http")),
        __metadata("design:paramtypes", [Object, authExpireDialog_service_1.default, Object, Object, Function, Function])
    ], IdleSensorController);
    return IdleSensorController;
}());
exports.default = IdleSensorController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRsZVNlbnNvci1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaWRsZVNlbnNvci1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBR0EsK0NBQTBDO0FBQzFDLG9HQUErRjtBQUUvRjtJQUdJLDhCQUVZLElBQWlCLEVBRWpCLGdCQUEwQyxFQUUxQyxPQUF1QixFQUV2QixTQUFxQixFQUVyQixTQUEyQixFQUUzQixLQUFtQjtRQVovQixpQkFhSTtRQVhRLFNBQUksR0FBSixJQUFJLENBQWE7UUFFakIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUEwQjtRQUUxQyxZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUV2QixjQUFTLEdBQVQsU0FBUyxDQUFZO1FBRXJCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBRTNCLFVBQUssR0FBTCxLQUFLLENBQWM7UUFNZCxxQkFBZ0IsR0FBRyxZQUFZLENBQUM7UUFFMUMsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO1FBQzdDLENBQUMsQ0FBQztRQUVLLGVBQVUsR0FBRztZQUNoQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQzFDLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUM7UUFPTSxlQUFVLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGNBQWMsRUFBRSxFQUFyQixDQUFxQixFQUFFLElBQUksQ0FBQyxDQUFDO0lBdEJoRSxDQUFDO0lBaUJHLDZDQUFjLEdBQXJCLFVBQXNCLE9BQXlCO1FBQzNDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLHdCQUF3QixFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RELE9BQU8sQ0FBQyxFQUFFLENBQUMsb0JBQW9CLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBSUQsNEVBQTRFO0lBQ3BFLDhDQUFlLEdBQXZCLFVBQXdCLFVBQWtCO1FBQ3RDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRU8sOENBQWUsR0FBdkI7UUFDSSxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQscUNBQXFDO0lBQzdCLDBDQUFXLEdBQW5CO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFTywrQ0FBZ0IsR0FBeEI7UUFBQSxpQkFhQztRQVpHLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDO1FBQy9ELElBQU0sUUFBUSxHQUFHLGtCQUFrQixDQUFDO1FBQ3BDLElBQU0sVUFBVSxHQUFHLFFBQVE7WUFDdkIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQztZQUMzQyxDQUFDLENBQUMsS0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLFFBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBUyxVQUFVLEVBQUUsRUFBRSxFQUFFO1lBQ3BDLEtBQUssRUFBRSxLQUFLO1NBQ2YsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVE7WUFDYixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxNQUFNO1lBQ1osS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzFDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLDJDQUFZLEdBQXBCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDakMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDMUIsQ0FBQztRQUNELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFTyw2Q0FBYyxHQUF0QjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRU8sMkNBQVksR0FBcEIsVUFBcUIsU0FBaUI7UUFDbEMsTUFBTSxDQUFDLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxTQUFTLENBQUM7SUFDbEUsQ0FBQztJQUVPLCtDQUFnQixHQUF4QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQztJQUM5RCxDQUFDO0lBRU8sOENBQWUsR0FBdkI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEdBQUcsR0FBRyxDQUFDO0lBQ3pDLENBQUM7SUFFTyxvREFBcUIsR0FBN0IsVUFBOEIsU0FBaUI7UUFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDO0lBQ3pFLENBQUM7SUFFTyxtREFBb0IsR0FBNUIsVUFBNkIsU0FBa0I7UUFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQztJQUN4RSxDQUFDO0lBRU8saURBQWtCLEdBQTFCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRU8sd0NBQVMsR0FBakI7UUFDSSxJQUFNLFNBQVMsR0FBRyxZQUFZLENBQUM7UUFDL0IsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUM5RCxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRU8sOENBQWUsR0FBdkI7UUFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLENBQUM7UUFDeEMsQ0FBQztJQUNMLENBQUM7SUFFTyxnRUFBaUMsR0FBekM7UUFBQSxpQkFXQztRQVZHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsdUVBQXVFO1FBQ3ZFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3JDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVPLCtDQUFnQixHQUF4QjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGlDQUFpQyxFQUFFLENBQUM7WUFDekMsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUNELElBQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUNuRixJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVPLHdDQUFTLEdBQWpCLFVBQWtCLGdCQUF3QjtRQUExQyxpQkFxQkM7UUFwQkcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFN0IsNEZBQTRGO1FBQzVGLElBQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN4QyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO2dCQUNyRCxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dCQUM1QyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDbEMsQ0FBQztRQUNMLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxnQkFBZ0I7YUFDaEIsSUFBSSxDQUFDO1lBQ0YsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO1NBQ3RELENBQUM7YUFDRCxPQUFPLENBQUM7WUFDTCxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzVDLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixLQUFJLENBQUMsaUNBQWlDLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFuS2Msb0NBQWUsR0FBRyxnQ0FBZ0MsQ0FBQztJQURqRCxvQkFBb0I7UUFJaEMsV0FBQSxtQkFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBRWQsV0FBQSxtQkFBTSxDQUFDLDJCQUEyQixDQUFDLENBQUE7UUFFbkMsV0FBQSxtQkFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBRWpCLFdBQUEsbUJBQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUVuQixXQUFBLG1CQUFNLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFbkIsV0FBQSxtQkFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO2lEQVBVLGtDQUF3QjtPQVByQyxvQkFBb0IsQ0FxS3hDO0lBQUQsMkJBQUM7Q0FBQSxBQXJLRCxJQXFLQztrQkFyS29CLG9CQUFvQiJ9

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var license_model_1 = __webpack_require__(45);
var licenseActivation_service_1 = __webpack_require__(46);
var licenseActivation_constants_1 = __webpack_require__(2);
var user_model_1 = __webpack_require__(115);
var LicenseActivationModalController = /** @class */ (function () {
    function LicenseActivationModalController($scope, $log, $q, $timeout, constants, dialogService, wizardDialogService, _t, xuiToastService, swLicenseActivationService, swLicenseActivationConstants) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$q = $q;
        this.$timeout = $timeout;
        this.constants = constants;
        this.dialogService = dialogService;
        this.wizardDialogService = wizardDialogService;
        this._t = _t;
        this.xuiToastService = xuiToastService;
        this.swLicenseActivationService = swLicenseActivationService;
        this.swLicenseActivationConstants = swLicenseActivationConstants;
        this.wizardWarningTextToken = false;
        this.showActivationDialog = function () {
            _this.$timeout(function () {
                if (!_.isEmpty(_this.mode)) {
                    _this.createDialog();
                    return;
                }
                _this.swLicenseActivationService.getOnlineState()
                    .then(function (response) {
                    _this.mode = response.status.toLowerCase();
                    _this.createDialog();
                });
            });
        };
        this.isSuccess = function (result) { return _this.swLicenseActivationService.isSuccess(result); };
        this.createDialog = function () {
            var user = _this.constants.user;
            _this.clearActivationData(_this.selectedLicense);
            _this.selectedLicense = new license_model_1.default(_this.selectedLicense);
            _this.selectedLicense.user = user && user.AccountID ?
                new user_model_1.default(user.AccountID, _this.selectedLicense.user, true) : null;
            _this.$scope.$watch("licenseActivation.selectedLicense", _this.validate, true);
            if (_this.mode === "online") {
                var customizedDefaults = {
                    template: __webpack_require__(1),
                    size: "lg"
                };
                _this.actionButton = {
                    name: "actionButton",
                    text: _this._t("Activate"),
                    actionText: _this._t("Activating license, please wait..."),
                    isPrimary: true,
                    action: _this.activateLicenseOnline
                };
                var customizedOptions = {
                    cancelButtonText: _this._t("Cancel"),
                    title: _this.dialogTitle,
                    viewModel: _this.$scope,
                    buttons: [
                        _this.actionButton
                    ]
                };
                _this.dialogService.showModal(customizedDefaults, customizedOptions);
            }
            else {
                var wizardDialogOptions = {
                    template: __webpack_require__(1),
                    size: "lg",
                    title: _this.dialogTitle,
                    viewModel: _this.$scope
                };
                _this.wizardDialogService.showModal(wizardDialogOptions);
            }
        };
        this.activateLicenseOnline = function () {
            if (!!_this.selectedLicense && !!_this.selectedLicense.user && !!_this.selectedLicense.user.setLocalStorageData) {
                _this.selectedLicense.user.setLocalStorageData(_this.selectedLicense.user);
            }
            return _this.swLicenseActivationService.activateOnline(_this.selectedLicense)
                .then(function (response) {
                _this.actionResult = response;
                if (!_this.isSuccess(response)) {
                    _this.errorMessage = response.message;
                    _this.activateLicenseOnlineForm.activationKey.$setValidity("licenseKey", false);
                    _this.actionButton.isDisabled = true;
                }
                _this.onActivateInternal(response);
                return response;
            });
        };
        this.onOfflineFirstStepEnter = function () {
            _this.wizardWarningTextToken = false;
            _this.swLicenseActivationService.getActivationToken(_this.selectedLicense)
                .then(function (token) {
                _this.activationToken = token.replace(/[\[\]]/g, "");
            });
        };
        this.preWizardTextOff = function () {
            _this.wizardWarningTextToken = true;
        };
        this.activateLicenseOffline = function (cancellation) {
            if (_this.file && _this.file.size) {
                return _this.swLicenseActivationService.activateOffline(_this.selectedLicense, _this.file)
                    .then(function (response) {
                    _this.actionResult = response;
                    if (!_this.isSuccess(_this.actionResult)) {
                        _this.errorMessage = response.message;
                        // Return false to keep the wizard dialog open.
                        return false;
                    }
                    _this.onActivateInternal(response);
                    // Return true to let the wizard dialog close.
                    return true;
                });
            }
            _this.activateLicenseOfflineForm.$submitted = true;
            _this.errorMessage = _this._t("Select a valid license file.");
            // Keep the wizard dialog open.
            return _this.$q.when(false);
        };
        this.clearErrors = function () {
            _this.errorMessage = "";
        };
        this.onClipboardSuccess = function () {
            _this.xuiToastService.success(_this._t("Copied"));
        };
        this.validate = function () {
            _this.$timeout(function () {
                if (_this.activateLicenseOnlineForm) {
                    _this.actionButton.isDisabled = _this.activateLicenseOnlineForm.$invalid;
                }
                if (!_this.isSuccess(_this.actionResult)) {
                    _this.actionResult = null;
                    _this.errorMessage = "";
                    if (_this.activateLicenseOnlineForm) {
                        _this.activateLicenseOnlineForm.activationKey.$setValidity("licenseKey", true);
                        _this.actionButton.isDisabled = _this.activateLicenseOnlineForm.$invalid;
                    }
                }
            });
        };
        this.onActivateInternal = function (actionResult) {
            if (_this.isSuccess(actionResult)) {
                _this.closeDialog();
            }
            else {
                _this.errorMessage = _this.getSuccessMessage(actionResult);
            }
            _this.$timeout(function () {
                if (angular.isFunction(_this.onActivate)) {
                    _this.onActivate({ result: actionResult });
                }
            });
        };
        this.clearActivationData = function (license) {
            if (license) {
                license.licenseKey = "";
            }
            _this.errorMessage = "";
        };
        this.closeDialog = function () {
            _this.$timeout(function () {
                angular.element(".xui-modal .modal-header .xui-icon-close").click();
            });
        };
    }
    LicenseActivationModalController.prototype.getSuccessMessage = function (result) {
        if (result && !_.isEmpty(result.message)) {
            return result.message;
        }
        return this.isSuccess(result)
            ? this._t("Activation was successful.")
            : this._t("Activation could not be completed.");
    };
    LicenseActivationModalController = __decorate([
        __param(0, decorators_1.Inject("$scope")),
        __param(1, decorators_1.Inject("$log")),
        __param(2, decorators_1.Inject("$q")),
        __param(3, decorators_1.Inject("$timeout")),
        __param(4, decorators_1.Inject("constants")),
        __param(5, decorators_1.Inject("xuiDialogService")),
        __param(6, decorators_1.Inject("xuiWizardDialogService")),
        __param(7, decorators_1.Inject("getTextService")),
        __param(8, decorators_1.Inject("xuiToastService")),
        __param(9, decorators_1.Inject("swLicenseActivationService")),
        __param(10, decorators_1.Inject("swLicenseActivationConstants")),
        __metadata("design:paramtypes", [Object, Object, Function, Function, Object, Object, Object, Function, Object, licenseActivation_service_1.default,
            licenseActivation_constants_1.default])
    ], LicenseActivationModalController);
    return LicenseActivationModalController;
}());
exports.default = LicenseActivationModalController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZUFjdGl2YXRpb25Nb2RhbC1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGljZW5zZUFjdGl2YXRpb25Nb2RhbC1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBT0EsK0NBQTBDO0FBRTFDLGlEQUFzQztBQUN0Qyx5RUFBbUU7QUFDbkUsNkVBQXVFO0FBRXZFLDJDQUFnQztBQU1oQztJQWlCSSwwQ0FFWSxNQUFjLEVBRWQsSUFBaUIsRUFFakIsRUFBYSxFQUViLFFBQXlCLEVBRXpCLFNBQXFCLEVBRXJCLGFBQTZCLEVBRTdCLG1CQUF5QyxFQUV6QyxFQUFvQyxFQUVwQyxlQUE4QixFQUU5QiwwQkFBb0QsRUFFckQsNEJBQXdEO1FBdEJuRSxpQkF1Qkk7UUFyQlEsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUVkLFNBQUksR0FBSixJQUFJLENBQWE7UUFFakIsT0FBRSxHQUFGLEVBQUUsQ0FBVztRQUViLGFBQVEsR0FBUixRQUFRLENBQWlCO1FBRXpCLGNBQVMsR0FBVCxTQUFTLENBQVk7UUFFckIsa0JBQWEsR0FBYixhQUFhLENBQWdCO1FBRTdCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBc0I7UUFFekMsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFFcEMsb0JBQWUsR0FBZixlQUFlLENBQWU7UUFFOUIsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUEwQjtRQUVyRCxpQ0FBNEIsR0FBNUIsNEJBQTRCLENBQTRCO1FBN0I1RCwyQkFBc0IsR0FBWSxLQUFLLENBQUM7UUFnQ3hDLHlCQUFvQixHQUFHO1lBQzFCLEtBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1YsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3hCLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFDcEIsTUFBTSxDQUFDO2dCQUNYLENBQUM7Z0JBQ0QsS0FBSSxDQUFDLDBCQUEwQixDQUFDLGNBQWMsRUFBRTtxQkFDM0MsSUFBSSxDQUFDLFVBQUMsUUFBc0I7b0JBQ3pCLEtBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDMUMsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRU0sY0FBUyxHQUFHLFVBQUMsTUFBb0IsSUFBSyxPQUFBLEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQWpELENBQWlELENBQUM7UUFFeEYsaUJBQVksR0FBRztZQUNuQixJQUFJLElBQUksR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztZQUMvQixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQy9DLEtBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSx1QkFBTyxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RCxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNoRCxJQUFJLG9CQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBRXJFLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLG1DQUFtQyxFQUFFLEtBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFN0UsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixJQUFNLGtCQUFrQixHQUFnQztvQkFDcEQsUUFBUSxFQUFFLE9BQU8sQ0FBUyx3Q0FBd0MsQ0FBQztvQkFDbkUsSUFBSSxFQUFFLElBQUk7aUJBQ2IsQ0FBQztnQkFFRixLQUFJLENBQUMsWUFBWSxHQUFHO29CQUNoQixJQUFJLEVBQUUsY0FBYztvQkFDcEIsSUFBSSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDO29CQUN6QixVQUFVLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxvQ0FBb0MsQ0FBQztvQkFDekQsU0FBUyxFQUFFLElBQUk7b0JBQ2YsTUFBTSxFQUFFLEtBQUksQ0FBQyxxQkFBcUI7aUJBQ3BCLENBQUM7Z0JBRW5CLElBQU0saUJBQWlCLEdBQXVCO29CQUMxQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQztvQkFDbkMsS0FBSyxFQUFFLEtBQUksQ0FBQyxXQUFXO29CQUN2QixTQUFTLEVBQUUsS0FBSSxDQUFDLE1BQU07b0JBQ3RCLE9BQU8sRUFBRTt3QkFDTCxLQUFJLENBQUMsWUFBWTtxQkFDcEI7aUJBQ0osQ0FBQztnQkFDRixLQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1lBQ3hFLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixJQUFNLG1CQUFtQixHQUE2QjtvQkFDbEQsUUFBUSxFQUFFLE9BQU8sQ0FBUyx3Q0FBd0MsQ0FBQztvQkFDbkUsSUFBSSxFQUFFLElBQUk7b0JBQ1YsS0FBSyxFQUFFLEtBQUksQ0FBQyxXQUFXO29CQUN2QixTQUFTLEVBQUUsS0FBSSxDQUFDLE1BQU07aUJBQ3pCLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzVELENBQUM7UUFDTCxDQUFDLENBQUE7UUFFTSwwQkFBcUIsR0FBRztZQUMzQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGVBQWUsSUFBSSxDQUFDLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztnQkFDM0csS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3RSxDQUFDO1lBQ0QsTUFBTSxDQUFDLEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQztpQkFDdEUsSUFBSSxDQUFDLFVBQUMsUUFBc0I7Z0JBQ3pCLEtBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO2dCQUM3QixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM1QixLQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7b0JBQ3JDLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDL0UsS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN4QyxDQUFDO2dCQUNELEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbEMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUNwQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVLLDRCQUF1QixHQUFHO1lBQzdCLEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7WUFDcEMsS0FBSSxDQUFDLDBCQUEwQixDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUM7aUJBQ25FLElBQUksQ0FBQyxVQUFDLEtBQWE7Z0JBQ2hCLEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFFSyxxQkFBZ0IsR0FBRztZQUN0QixLQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztRQUVLLDJCQUFzQixHQUFHLFVBQUMsWUFBK0I7WUFDNUQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sQ0FBQyxLQUFJLENBQUMsMEJBQTBCLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxlQUFlLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQztxQkFDbEYsSUFBSSxDQUFDLFVBQUMsUUFBc0I7b0JBQ3pCLEtBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO29CQUM3QixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDckMsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO3dCQUNyQywrQ0FBK0M7d0JBQy9DLE1BQU0sQ0FBQyxLQUFLLENBQUM7b0JBQ2pCLENBQUM7b0JBQ0QsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNsQyw4Q0FBOEM7b0JBQzlDLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQztZQUNELEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ2xELEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBQzVELCtCQUErQjtZQUMvQixNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUssZ0JBQVcsR0FBRztZQUNqQixLQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUM7UUFFSyx1QkFBa0IsR0FBRztZQUN4QixLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDcEQsQ0FBQyxDQUFDO1FBRUssYUFBUSxHQUFHO1lBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQztnQkFDVixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMseUJBQXlCLENBQUMsUUFBUSxDQUFDO2dCQUMzRSxDQUFDO2dCQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNyQyxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztvQkFDekIsS0FBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7b0JBQ3ZCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7d0JBQ2pDLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDOUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQztvQkFDM0UsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFXSyx1QkFBa0IsR0FBRyxVQUFDLFlBQTBCO1lBQ25ELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixLQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDdkIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzdELENBQUM7WUFFRCxLQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNWLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsQ0FBQyxDQUFDO2dCQUM5QyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFTSx3QkFBbUIsR0FBRyxVQUFDLE9BQWdCO1lBQzNDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDNUIsQ0FBQztZQUNELEtBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQzNCLENBQUMsQ0FBQztRQUVNLGdCQUFXLEdBQUc7WUFDbEIsS0FBSSxDQUFDLFFBQVEsQ0FBQztnQkFDVixPQUFPLENBQUMsT0FBTyxDQUFDLDBDQUEwQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDeEUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUE7SUF6S0UsQ0FBQztJQXVJSSw0REFBaUIsR0FBekIsVUFBMEIsTUFBb0I7UUFDMUMsRUFBRSxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQzFCLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7WUFDekIsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsNEJBQTRCLENBQUM7WUFDdkMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsb0NBQW9DLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBdExnQixnQ0FBZ0M7UUFrQjVDLFdBQUEsbUJBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUVoQixXQUFBLG1CQUFNLENBQUMsTUFBTSxDQUFDLENBQUE7UUFFZCxXQUFBLG1CQUFNLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFWixXQUFBLG1CQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7UUFFbEIsV0FBQSxtQkFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRW5CLFdBQUEsbUJBQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1FBRTFCLFdBQUEsbUJBQU0sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBO1FBRWhDLFdBQUEsbUJBQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1FBRXhCLFdBQUEsbUJBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBRXpCLFdBQUEsbUJBQU0sQ0FBQyw0QkFBNEIsQ0FBQyxDQUFBO1FBRXBDLFlBQUEsbUJBQU0sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFBO3VIQURILG1DQUF3QjtZQUV2QixxQ0FBMEI7T0F2Q2xELGdDQUFnQyxDQWtOcEQ7SUFBRCx1Q0FBQztDQUFBLEFBbE5ELElBa05DO2tCQWxOb0IsZ0NBQWdDIn0=

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var License = /** @class */ (function () {
    function License(model) {
        var _this = this;
        /*
         * Returns modified License object that is required by License service
         */
        this.writer = function (constants) {
            if (!_this.licenseKey) {
                _this.licenseKey = constants.license.defaultKey;
            }
            return {
                licenseKey: _this.licenseKey,
                productName: _this.productName,
                licenseVersion: _this.licenseVersion
            };
        };
        if (model) {
            _.extend(this, model);
        }
    }
    return License;
}());
exports.default = License;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZS1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxpY2Vuc2UtbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFLdkM7SUFTSSxpQkFBWSxLQUFlO1FBQTNCLGlCQUlDO1FBRUQ7O1dBRUc7UUFDSSxXQUFNLEdBQUcsVUFBQyxTQUFxQztZQUNsRCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixLQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDO1lBQ25ELENBQUM7WUFFRCxNQUFNLENBQUM7Z0JBQ0gsVUFBVSxFQUFFLEtBQUksQ0FBQyxVQUFVO2dCQUMzQixXQUFXLEVBQUUsS0FBSSxDQUFDLFdBQVc7Z0JBQzdCLGNBQWMsRUFBRSxLQUFJLENBQUMsY0FBYzthQUN0QyxDQUFDO1FBQ04sQ0FBQyxDQUFDO1FBbEJFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDUixDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMxQixDQUFDO0lBQ0wsQ0FBQztJQWdCTCxjQUFDO0FBQUQsQ0FBQyxBQTdCRCxJQTZCQyJ9

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var license_model_1 = __webpack_require__(45);
var licenseActivation_constants_1 = __webpack_require__(2);
var LicenseActivationService = /** @class */ (function () {
    function LicenseActivationService($httpParamSerializer, $log, swApi, swLicenseActivationConstants, swMegaMenuService) {
        this.$httpParamSerializer = $httpParamSerializer;
        this.$log = $log;
        this.swApi = swApi;
        this.swLicenseActivationConstants = swLicenseActivationConstants;
        this.swMegaMenuService = swMegaMenuService;
    }
    Object.defineProperty(LicenseActivationService.prototype, "licenseApi", {
        get: function () {
            return this.swApi.api(false).one("licensing");
        },
        enumerable: true,
        configurable: true
    });
    LicenseActivationService.prototype.isSuccess = function (result) {
        return !!(result && result.status && result.status.toLowerCase() === "success");
    };
    LicenseActivationService.prototype.getOnlineState = function () {
        return this.licenseApi.one("onlinestate").get()
            .then(function (result) { return result.plain(); });
    };
    LicenseActivationService.prototype.activateOnline = function (license) {
        var _this = this;
        return this.licenseApi.post("activation", license)
            .then(function (response) { return response && response.plain(); })
            .then(function (response) {
            return _this.swMegaMenuService.clearCache()
                .then(function () { return response; }, function (err) {
                _this.clearCacheError(err);
                return response;
            });
        });
    };
    LicenseActivationService.prototype.getActivationToken = function (license) {
        license = license || new license_model_1.default();
        var values = this.$httpParamSerializer(license.writer(this.swLicenseActivationConstants));
        var url = "/activationtoken?" + values;
        return this.licenseApi.one(url).get()
            .then(function (response) { return response.token; });
    };
    LicenseActivationService.prototype.activateOffline = function (license, file) {
        var _this = this;
        license = license || new license_model_1.default();
        var formData = new FormData();
        formData.append("file", file, file.name);
        return this.licenseApi.one("activation")
            .post("file?" + this.$httpParamSerializer(license.writer(this.swLicenseActivationConstants)), formData, { "Content-Type": "application/json" }, { "Content-Type": undefined })
            .then(function (result) { return result && result.plain(); })
            .then(function (result) {
            return _this.swMegaMenuService.clearCache()
                .then(function () { return result; }, function (err) {
                _this.clearCacheError(err);
                return result;
            });
        });
    };
    LicenseActivationService.prototype.clearCacheError = function (err) {
        this.$log.error(err);
    };
    LicenseActivationService = __decorate([
        __param(0, decorators_1.Inject("$httpParamSerializer")),
        __param(1, decorators_1.Inject("$log")),
        __param(2, decorators_1.Inject("swApi")),
        __param(3, decorators_1.Inject("swLicenseActivationConstants")),
        __param(4, decorators_1.Inject("swMegaMenuService")),
        __metadata("design:paramtypes", [Function, Object, Object, licenseActivation_constants_1.default, Object])
    ], LicenseActivationService);
    return LicenseActivationService;
}());
exports.LicenseActivationService = LicenseActivationService;
exports.default = LicenseActivationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZUFjdGl2YXRpb24tc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxpY2Vuc2VBY3RpdmF0aW9uLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFJQSwrQ0FBMEM7QUFFMUMsaURBQXNDO0FBRXRDLDZFQUF1RTtBQUV2RTtJQUNJLGtDQUVZLG9CQUEwQyxFQUUxQyxJQUFpQixFQUVqQixLQUFvQixFQUVwQiw0QkFBd0QsRUFFeEQsaUJBQW1DO1FBUm5DLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFFMUMsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUVqQixVQUFLLEdBQUwsS0FBSyxDQUFlO1FBRXBCLGlDQUE0QixHQUE1Qiw0QkFBNEIsQ0FBNEI7UUFFeEQsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtJQUM1QyxDQUFDO0lBRUosc0JBQVksZ0RBQVU7YUFBdEI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2xELENBQUM7OztPQUFBO0lBRU0sNENBQVMsR0FBaEIsVUFBaUIsTUFBb0I7UUFDakMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUM7SUFDcEYsQ0FBQztJQUVNLGlEQUFjLEdBQXJCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsRUFBRTthQUMxQyxJQUFJLENBQUMsVUFBQyxNQUFnQixJQUFLLE9BQUEsTUFBTSxDQUFDLEtBQUssRUFBZ0IsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFTSxpREFBYyxHQUFyQixVQUFzQixPQUFnQjtRQUF0QyxpQkFhQztRQVpHLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDO2FBQzdDLElBQUksQ0FBQyxVQUFDLFFBQWtCLElBQUssT0FBQSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssRUFBZ0IsRUFBMUMsQ0FBMEMsQ0FBQzthQUN4RSxJQUFJLENBQUMsVUFBQyxRQUFzQjtZQUN6QixNQUFNLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsRUFBRTtpQkFDckMsSUFBSSxDQUNELGNBQU0sT0FBQSxRQUFRLEVBQVIsQ0FBUSxFQUNkLFVBQUMsR0FBRztnQkFDQSxLQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMxQixNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3BCLENBQUMsQ0FDSixDQUFDO1FBQ1YsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0scURBQWtCLEdBQXpCLFVBQTBCLE9BQWdCO1FBQ3RDLE9BQU8sR0FBRyxPQUFPLElBQUksSUFBSSx1QkFBTyxFQUFFLENBQUM7UUFDbkMsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztRQUM1RixJQUFNLEdBQUcsR0FBRyxzQkFBb0IsTUFBUSxDQUFDO1FBQ3pDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUU7YUFDaEMsSUFBSSxDQUFDLFVBQUMsUUFBc0IsSUFBSyxPQUFBLFFBQVEsQ0FBQyxLQUFLLEVBQWQsQ0FBYyxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVNLGtEQUFlLEdBQXRCLFVBQXVCLE9BQWdCLEVBQUUsSUFBUztRQUFsRCxpQkFzQkM7UUFyQkcsT0FBTyxHQUFHLE9BQU8sSUFBSSxJQUFJLHVCQUFPLEVBQUUsQ0FBQztRQUNuQyxJQUFNLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQ2hDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQzthQUNuQyxJQUFJLENBQ0QsVUFBUSxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBRyxFQUN0RixRQUFRLEVBQ1IsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUUsRUFDdEMsRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLENBQ2hDO2FBQ0EsSUFBSSxDQUFDLFVBQUMsTUFBZ0IsSUFBSyxPQUFBLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFnQixFQUF0QyxDQUFzQyxDQUFDO2FBQ2xFLElBQUksQ0FBQyxVQUFDLE1BQW9CO1lBQ3ZCLE1BQU0sQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxFQUFFO2lCQUNyQyxJQUFJLENBQ0QsY0FBTSxPQUFBLE1BQU0sRUFBTixDQUFNLEVBQ1osVUFBQyxHQUFHO2dCQUNBLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDbEIsQ0FBQyxDQUNKLENBQUM7UUFDVixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTyxrREFBZSxHQUF2QixVQUF3QixHQUFRO1FBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUE1RVEsd0JBQXdCO1FBRTVCLFdBQUEsbUJBQU0sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFBO1FBRTlCLFdBQUEsbUJBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUVkLFdBQUEsbUJBQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUVmLFdBQUEsbUJBQU0sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFBO1FBRXRDLFdBQUEsbUJBQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO21FQURVLHFDQUEwQjtPQVQzRCx3QkFBd0IsQ0E2RXBDO0lBQUQsK0JBQUM7Q0FBQSxBQTdFRCxJQTZFQztBQTdFWSw0REFBd0I7QUErRXJDLGtCQUFlLHdCQUF3QixDQUFDIn0=

/***/ }),
/* 47 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var tooltip_service_1 = __webpack_require__(49);
var entityPopoverLoader_directive_1 = __webpack_require__(50);
var TooltipLoaderController = /** @class */ (function () {
    function TooltipLoaderController($log, tooltipService, $compile, $scope, $q, $timeout, $templateCache, $location, _t) {
        var _this = this;
        this.$log = $log;
        this.tooltipService = tooltipService;
        this.$compile = $compile;
        this.$scope = $scope;
        this.$q = $q;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.$location = $location;
        this._t = _t;
        this.$onInit = function () {
            _this.createdTooltips = _this.createdTooltips || [];
        };
        this.getTemplate = function (templateId) {
            return _this.$templateCache.get(templateId);
        };
    }
    /**
     * There are two supported ways to provide data for tooltips - href of a link or orion-type attribute on any element
     * @param element
     * @returns {ITooltipIds}
     */
    TooltipLoaderController.prototype.getIdsFromElement = function (element, type) {
        var _this = this;
        var ids = null;
        var warn = function (text, value) {
            _this.$log.warn(text + ": " + value);
            return null;
        };
        if (element.is("[" + entityPopoverLoader_directive_1.default.ENTITY_OPID_ATTR + "]")) {
            var opid = element.attr(entityPopoverLoader_directive_1.default.ENTITY_OPID_ATTR);
            ids = this.getIdsFromMarker(element) || warn("failed to parse marker id", opid);
        }
        if (ids == null && element.is("[" + entityPopoverLoader_directive_1.default.NETOBJECT_ATTR + "]")) {
            var netObject = element.attr(entityPopoverLoader_directive_1.default.NETOBJECT_ATTR);
            if (type === tooltip_service_1.DataIdentifierType.notSpecified) {
                ids = this.getIdsFromNetObject("NetObject=" + netObject);
            }
            if (ids == null || type === tooltip_service_1.DataIdentifierType.netObjectGeneric) {
                ids = this.getIdsFromnetObjectGeneric("NetObject=" + netObject);
            }
            ids = ids || warn("failed to parse netobject id", netObject);
        }
        if (ids == null && element.is("[href]")) {
            var uri = element.attr("href");
            ids = this.getIdsFromUri(uri, type) || warn("failed to parse uri id", uri);
        }
        return ids;
    };
    TooltipLoaderController.prototype.getIdsFromUri = function (uri, type) {
        var uriParsed = decodeURIComponent(uri);
        var ids = null;
        if (type === tooltip_service_1.DataIdentifierType.notSpecified) {
            ids = this.getIdsFromOpid(uriParsed, true) || this.getIdsFromNetObject(uriParsed);
        }
        if (ids == null || type === tooltip_service_1.DataIdentifierType.netObjectGeneric) {
            ids = this.getIdsFromnetObjectGeneric(uriParsed);
        }
        return ids;
    };
    TooltipLoaderController.prototype.getIdsFromMarker = function (element) {
        var opid = element.attr(entityPopoverLoader_directive_1.default.ENTITY_OPID_ATTR);
        var ids = this.getIdsFromOpid(opid, false);
        if (ids != null && element.is("[tipCtx]")) {
            ids.context = element.attr("tipCtx");
        }
        ids.idType = tooltip_service_1.DataIdentifierType.notSpecified;
        return ids;
    };
    TooltipLoaderController.prototype.decorateNetObjectIds = function (ids, uri) {
        if (ids) {
            var contextRegex = /\bTipCtx=([a-zA-Z0-9]+)\b/g;
            var ctxMatch = contextRegex.exec(uri);
            if (ctxMatch) {
                ids.context = ctxMatch[1];
            }
        }
        return ids;
    };
    TooltipLoaderController.prototype.getIdsFromNetObject = function (netObject) {
        var netObjectRegex = /\bNetObject=([A-Z]+):([A-Z0-9:]+)\b/g;
        var match = netObjectRegex.exec(netObject);
        if (!match) {
            return null;
        }
        return this.decorateNetObjectIds({
            siteid: this.getSiteIdFromServer(netObject),
            type: match[1],
            id: match[2],
            context: "default",
            idType: tooltip_service_1.DataIdentifierType.notSpecified
        }, netObject);
    };
    TooltipLoaderController.prototype.getIdsFromnetObjectGeneric = function (netObject) {
        var netObjectRegex = /\bNetObject=([A-Z]+):(.*)\b/g;
        var match = netObjectRegex.exec(netObject);
        if (!match) {
            return null;
        }
        return this.decorateNetObjectIds({
            siteid: this.getSiteIdFromServer(netObject),
            type: match[1],
            id: match[2],
            context: "default",
            idType: tooltip_service_1.DataIdentifierType.netObjectGeneric
        }, netObject);
    };
    TooltipLoaderController.prototype.getIdsFromOpid = function (opid, fromUrl) {
        var opidRegex = fromUrl
            ? /\bOpid=([0-9]*)([_]{0,1})([a-zA-Z0-9.]+)_([0-9_]+)\b/g
            : /([0-9]*)([_]{0,1})([a-zA-Z0-9.]+)_([0-9_]+)/g;
        var match = opidRegex.exec(opid);
        if (!match) {
            return null;
        }
        var siteId = (match[1] === "") ? null : match[1];
        if (!siteId && fromUrl) {
            siteId = this.getSiteIdFromServer(opid);
        }
        return {
            siteid: siteId,
            type: match[3],
            id: match[4],
            context: "default",
            idType: tooltip_service_1.DataIdentifierType.notSpecified
        };
    };
    TooltipLoaderController.prototype.getSiteIdFromServer = function (url) {
        var siteIdRegExp = /(?:\/Server\/)([0-9]+)/g;
        var siteIdMatch = siteIdRegExp.exec(url);
        return siteIdMatch && siteIdMatch[1] ? siteIdMatch[1] : null;
    };
    TooltipLoaderController.prototype.idsEqual = function (id1, id2) {
        return id1 && id2 && (Number(id1.siteid) === Number(id2.siteid))
            && (id1.type === id2.type && Number(id1.id) === Number(id2.id));
    };
    TooltipLoaderController.prototype.matchesLocation = function (ids, type) {
        var locationIds = this.getIdsFromUri(this.$location.absUrl(), type);
        return this.idsEqual(locationIds, ids);
    };
    TooltipLoaderController.prototype.load = function (container, element, hover, master) {
        var _this = this;
        var popoverContext = {
            container: container,
            element: element,
            hover: hover,
        };
        var idsInit = this.ids = this.getIdsFromElement(element, tooltip_service_1.DataIdentifierType.notSpecified);
        if (idsInit == null || this.matchesLocation(idsInit, tooltip_service_1.DataIdentifierType.notSpecified)) {
            this.$log.info(idsInit ? "target id matches location, skipping" : "no target id found, skipping");
            return this.$q.reject();
        }
        return this.tooltipService.getTooltipConfiguration(idsInit.type)
            .then(function (conf) {
            if (conf === null) {
                _this.$log.info("New entity popover is not enabled or does not exist for this type: " + idsInit.type);
                return _this.$q.reject();
            }
            if (conf.dataIdentifierType !== idsInit.idType) {
                idsInit = _this.getIdsFromElement(element, conf.dataIdentifierType);
            }
            if (idsInit == null || _this.matchesLocation(idsInit, conf.dataIdentifierType)) {
                _this.$log.info(idsInit ? "target id matches location, skipping" : "no target id found, skipping");
                return _this.$q.reject();
            }
            return idsInit;
        })
            .then(function (ids) {
            _this.ids = ids;
            if (_this.createdTooltips.indexOf(_this.createKeyFromIds(ids)) > -1) {
                _this.$log.debug("This element already has a popover, skipping its creation");
                return _this.$q.reject();
            }
            _this.createdTooltips.push(_this.createKeyFromIds(ids));
            var data = _this.getTooltipData(ids.siteid, ids.type, ids.context, ids.id);
            return _this.$q.all([
                _this.tooltipService.loadScript(ids.type, ids.context),
                _this.tooltipService.loadStylesheet(ids.type, ids.context),
                _this.getTooltipTemplate(ids.type, ids.context)
            ]).then(function (_a) {
                var template = _a[2];
                var hoveredElement = element[0];
                var tooltip;
                var shouldShow = master
                    ? function () { return master.popover.is(":visible"); }
                    : function () { return hoveredElement === hover.element; };
                var dontShow = function () {
                    _this.$log.debug("Popover should not be shown anymore due to hover change");
                    return _this.$q.reject();
                };
                // there might be a delay between fetching the data and mouse movement,
                // so recheck whether the original element is still active
                if (!shouldShow()) {
                    return dontShow();
                }
                return _this.createAndCompileTooltip(popoverContext, template, data)
                    .then(function (newTooltip) {
                    tooltip = newTooltip;
                    if (master) {
                        master.slave = tooltip;
                        // delay as long as possible but still short enough so that linkage is removed
                        // before the old popup is removed after the new one fades in
                        _this.$timeout(function () {
                            master.slave = null;
                        }, TooltipLoaderController.PopoverRefreshUnlinkTimeout);
                    }
                    if (!shouldShow()) {
                        tooltip.removePopover();
                        return dontShow();
                    }
                    container.after(tooltip.popover);
                    return tooltip;
                });
            });
        }).catch(function (reason) {
            if (reason) {
                _this.$log.warn("Error while loading popover", reason);
            }
            return _this.$q.reject(reason);
        });
    };
    TooltipLoaderController.prototype.getTooltipTemplate = function (type, ctx) {
        var _this = this;
        return this.tooltipService
            .loadTemplate(type, ctx)
            .then(function (template) {
            template.template = template.template && _this.getTemplate(template.template);
            if (template.extensions) {
                template.extensions = _.mapValues(template.extensions, _this.getTemplate);
            }
            return template;
        });
    };
    TooltipLoaderController.prototype.getTooltipData = function (siteId, type, ctx, id) {
        return this.tooltipService
            .getData(siteId, type, ctx, id)
            .then(function (data) { return data; });
    };
    TooltipLoaderController.prototype.updatePopover = function (context, master) {
        this.load(context.container, context.element, context.hover, master)
            .catch(angular.noop);
    };
    TooltipLoaderController.prototype.createAndCompileTooltip = function (context, template, data) {
        var _this = this;
        // we create a new isolated scope for the tooltip - otherwise all tooltips would share the settings
        var popoverScope = this.popoverScope = this.$scope.$new(true);
        // it has to be nested like this because angular doesn't allow passing dom elements directly
        var flagNoTipReject = {};
        return data
            .then(function (popoverData) {
            if (!popoverData) {
                return _this.$q.reject();
            }
            if (popoverData.noTip) {
                return _this.$q.reject(flagNoTipReject);
            }
            var content = _.join([
                TooltipLoaderController.GENERIC_POPOVER_TEMPLATE,
                template.template
            ].concat(_.values(template.extensions)), "");
            var tooltipPopover;
            popoverScope.title = popoverData.title;
            popoverScope.status = popoverData.status;
            popoverScope.childStatus = popoverData.childStatus;
            popoverScope.entityType = (popoverData.identifiers && popoverData.identifiers.entityType);
            popoverScope.popoverData = popoverData;
            popoverScope.popoverData.actionApplied = function () { return _this.updatePopover(context, tooltipPopover); };
            popoverScope.popoverTemplate = "<div class=\"xui-margin-md-neg\">" + content + "</div>";
            tooltipPopover = _this.compilePopover(context.element, popoverScope, _this.ids);
            return tooltipPopover;
        })
            .catch(function (error) {
            if (error === flagNoTipReject) {
                return _this.$q.reject();
            }
            popoverScope.title = _this._t("Not found");
            // TODO: special missing data template?
            popoverScope.popoverTemplate = _this._t("We have no data for this item.");
            return _this.compilePopover(context.element, popoverScope, _this.ids);
        });
    };
    TooltipLoaderController.prototype.compilePopover = function (element, tooltipScope, ids) {
        var _this = this;
        var popoverElement;
        var popoverData;
        var removePopover = function () {
            var keyIndex = _this.createdTooltips.indexOf(_this.createKeyFromIds(ids), 0);
            if (keyIndex > -1) {
                _this.createdTooltips.splice(keyIndex, 1);
            }
            _this.$timeout(function () {
                tooltipScope.$destroy();
            });
            if (popoverData.slave) {
                popoverData.slave.removePopover();
            }
            popoverElement.remove();
        };
        tooltipScope.removePopover = removePopover;
        tooltipScope.container = { nestedElement: element };
        tooltipScope.initialShow = false;
        var xuiPopover = "<xui-popover\n    xui-popover-element='container'\n    xui-popover-content=\"popoverTemplate\"\n    xui-popover-title=\"{{::title}}\"\n    xui-popover-status=\"{{::(status | swStatus:childStatus)}}\"\n    xui-popover-icon=\"{{::(entityType ? (entityType | swEntityIcon) : undefined)}}\"\n    xui-popover-on-hide=\"removePopover()\"\n    xui-popover-is-displayed=\"initialShow\">\n</xui-popover>";
        popoverElement = this.$compile(xuiPopover)(tooltipScope);
        tooltipScope.initialShow = true;
        popoverData = {
            popover: popoverElement,
            removePopover: removePopover
        };
        return popoverData;
    };
    TooltipLoaderController.prototype.createKeyFromIds = function (ids) {
        return ids.context + ":" + ids.id + ":" + ids.type + ":" + (ids.siteid || 0);
    };
    TooltipLoaderController.GENERIC_POPOVER_TEMPLATE = "<sw-entity-popover content=\"::popoverData\"></sw-entity-popover>";
    TooltipLoaderController.PopoverRefreshUnlinkTimeout = 500;
    TooltipLoaderController.$inject = [
        "$log", "swTooltipService", "$compile",
        "$scope", "$q", "$timeout",
        "$templateCache", "$location", "getTextService"
    ];
    return TooltipLoaderController;
}());
exports.default = TooltipLoaderController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5UG9wb3ZlckxvYWRlci1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZW50aXR5UG9wb3ZlckxvYWRlci1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBV3ZDLDZEQUErRDtBQUkvRCxpRkFBMEY7QUFrQzFGO0lBWUksaUNBQ1ksSUFBaUIsRUFDakIsY0FBK0IsRUFDL0IsUUFBeUIsRUFDekIsTUFBYyxFQUNkLEVBQWEsRUFDYixRQUF5QixFQUN6QixjQUFxQyxFQUNyQyxTQUEyQixFQUMzQixFQUFvQztRQVRoRCxpQkFXQztRQVZXLFNBQUksR0FBSixJQUFJLENBQWE7UUFDakIsbUJBQWMsR0FBZCxjQUFjLENBQWlCO1FBQy9CLGFBQVEsR0FBUixRQUFRLENBQWlCO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxPQUFFLEdBQUYsRUFBRSxDQUFXO1FBQ2IsYUFBUSxHQUFSLFFBQVEsQ0FBaUI7UUFDekIsbUJBQWMsR0FBZCxjQUFjLENBQXVCO1FBQ3JDLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBSXpDLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSSxDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUM7UUFDdEQsQ0FBQyxDQUFDO1FBMlBNLGdCQUFXLEdBQUcsVUFBQyxVQUFrQjtZQUNyQyxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBVztRQUE3QyxDQUE2QyxDQUFDO0lBaFFsRCxDQUFDO0lBTUQ7Ozs7T0FJRztJQUNJLG1EQUFpQixHQUF4QixVQUF5QixPQUF5QixFQUFFLElBQXdCO1FBQTVFLGlCQWdDQztRQS9CRyxJQUFJLEdBQUcsR0FBZ0IsSUFBSSxDQUFDO1FBRTVCLElBQU0sSUFBSSxHQUFHLFVBQUMsSUFBWSxFQUFFLEtBQWE7WUFDckMsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUksSUFBSSxVQUFLLEtBQU8sQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDO1FBRUYsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxNQUFJLHVDQUFhLENBQUMsZ0JBQWdCLE1BQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwRCxJQUFNLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLHVDQUFhLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUMxRCxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNwRixDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLElBQUksSUFBSSxPQUFPLENBQUMsRUFBRSxDQUFDLE1BQUksdUNBQWEsQ0FBQyxjQUFjLE1BQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRSxJQUFNLFNBQVMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLHVDQUFhLENBQUMsY0FBYyxDQUFDLENBQUM7WUFFN0QsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLG9DQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLEdBQUcsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsZUFBYSxTQUFXLENBQUMsQ0FBQztZQUM3RCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUUsR0FBRyxJQUFJLElBQUksSUFBSSxJQUFJLEtBQUssb0NBQWtCLENBQUMsZ0JBQWlCLENBQUMsQ0FBQyxDQUFDO2dCQUNoRSxHQUFHLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGVBQWEsU0FBVyxDQUFDLENBQUM7WUFDcEUsQ0FBQztZQUVELEdBQUcsR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLDhCQUE4QixFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ2pFLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksSUFBSSxJQUFJLE9BQU8sQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQU0sR0FBRyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDakMsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMvRSxDQUFDO1FBRUQsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFTywrQ0FBYSxHQUFyQixVQUFzQixHQUFXLEVBQUUsSUFBd0I7UUFDdkQsSUFBTSxTQUFTLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFMUMsSUFBSSxHQUFHLEdBQWdCLElBQUksQ0FBQztRQUU1QixFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssb0NBQWtCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUMzQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3RGLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksSUFBSSxJQUFJLElBQUksS0FBSyxvQ0FBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDOUQsR0FBRyxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyRCxDQUFDO1FBRUQsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFTyxrREFBZ0IsR0FBeEIsVUFBeUIsT0FBeUI7UUFDOUMsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyx1Q0FBYSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDMUQsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFN0MsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLElBQUksSUFBSSxPQUFPLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QyxHQUFHLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQztRQUVELEdBQUcsQ0FBQyxNQUFNLEdBQUcsb0NBQWtCLENBQUMsWUFBWSxDQUFDO1FBQzdDLE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRU8sc0RBQW9CLEdBQTVCLFVBQTZCLEdBQWdCLEVBQUUsR0FBVztRQUN0RCxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ04sSUFBTSxZQUFZLEdBQUcsNEJBQTRCLENBQUM7WUFDbEQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN4QyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNYLEdBQUcsQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFTyxxREFBbUIsR0FBM0IsVUFBNEIsU0FBaUI7UUFDekMsSUFBTSxjQUFjLEdBQUcsc0NBQXNDLENBQUM7UUFDOUQsSUFBTSxLQUFLLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUU3QyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1lBQzdCLE1BQU0sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDO1lBQzNDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2QsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDWixPQUFPLEVBQUUsU0FBUztZQUNsQixNQUFNLEVBQUcsb0NBQWtCLENBQUMsWUFBWTtTQUMzQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ2xCLENBQUM7SUFFTyw0REFBMEIsR0FBbEMsVUFBbUMsU0FBaUI7UUFDaEQsSUFBTSxjQUFjLEdBQUcsOEJBQThCLENBQUM7UUFDdEQsSUFBTSxLQUFLLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUU3QyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1lBQzdCLE1BQU0sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDO1lBQzNDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2QsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDWixPQUFPLEVBQUUsU0FBUztZQUNsQixNQUFNLEVBQUUsb0NBQWtCLENBQUMsZ0JBQWdCO1NBQzlDLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbEIsQ0FBQztJQUVPLGdEQUFjLEdBQXRCLFVBQXVCLElBQVksRUFBRSxPQUFnQjtRQUNqRCxJQUFNLFNBQVMsR0FBRyxPQUFPO1lBQ3JCLENBQUMsQ0FBQyx1REFBdUQ7WUFDekQsQ0FBQyxDQUFDLDhDQUE4QyxDQUFDO1FBRXJELElBQU0sS0FBSyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFbkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1QsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBRUQsSUFBSSxNQUFNLEdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3pELEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDckIsTUFBTSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxDQUFDO1FBRUQsTUFBTSxDQUFDO1lBQ0gsTUFBTSxFQUFFLE1BQU07WUFDZCxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNkLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1osT0FBTyxFQUFFLFNBQVM7WUFDbEIsTUFBTSxFQUFFLG9DQUFrQixDQUFDLFlBQVk7U0FDMUMsQ0FBQztJQUNOLENBQUM7SUFFTyxxREFBbUIsR0FBM0IsVUFBNEIsR0FBVztRQUNuQyxJQUFNLFlBQVksR0FBRyx5QkFBeUIsQ0FBQztRQUMvQyxJQUFNLFdBQVcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLE1BQU0sQ0FBQyxXQUFXLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNqRSxDQUFDO0lBRU8sMENBQVEsR0FBaEIsVUFBaUIsR0FBZ0IsRUFBRSxHQUFnQjtRQUMvQyxNQUFNLENBQUMsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztlQUN6RCxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRU8saURBQWUsR0FBdkIsVUFBd0IsR0FBZ0IsRUFBRSxJQUF3QjtRQUM5RCxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEUsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFLTSxzQ0FBSSxHQUFYLFVBQ0ksU0FBMkIsRUFDM0IsT0FBeUIsRUFDekIsS0FBb0IsRUFDcEIsTUFBd0I7UUFKNUIsaUJBMkZDO1FBckZHLElBQU0sY0FBYyxHQUFvQjtZQUNwQyxTQUFTLEVBQUUsU0FBUztZQUNwQixPQUFPLEVBQUUsT0FBTztZQUNoQixLQUFLLEVBQUUsS0FBSztTQUNmLENBQUM7UUFDRixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsb0NBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFMUYsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxvQ0FBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEYsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUMsOEJBQThCLENBQUMsQ0FBQztZQUNsRyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUM1QixDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzthQUMzRCxJQUFJLENBQUUsVUFBQyxJQUFJO1lBQ1IsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2hCLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHdFQUFzRSxPQUFPLENBQUMsSUFBTSxDQUFDLENBQUM7Z0JBQ3JHLE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzVCLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLE9BQU8sR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3ZFLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksSUFBSSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUUsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUMsOEJBQThCLENBQUMsQ0FBQztnQkFDbEcsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDNUIsQ0FBQztZQUVELE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDbkIsQ0FBQyxDQUFDO2FBQ0QsSUFBSSxDQUFFLFVBQUMsR0FBRztZQUVQLEtBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBRWYsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoRSxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQywyREFBMkQsQ0FBQyxDQUFDO2dCQUM3RSxNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM1QixDQUFDO1lBQ0QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFFdEQsSUFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDNUUsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDO2dCQUNmLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQztnQkFDckQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDO2dCQUN6RCxLQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDO2FBQ2pELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxFQUFjO29CQUFULGdCQUFRO2dCQUNsQixJQUFNLGNBQWMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLElBQUksT0FBd0IsQ0FBQztnQkFDN0IsSUFBTSxVQUFVLEdBQUcsTUFBTTtvQkFDckIsQ0FBQyxDQUFDLGNBQU0sT0FBQSxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBN0IsQ0FBNkI7b0JBQ3JDLENBQUMsQ0FBQyxjQUFNLE9BQUEsY0FBYyxLQUFLLEtBQUssQ0FBQyxPQUFPLEVBQWhDLENBQWdDLENBQUM7Z0JBQzdDLElBQU0sUUFBUSxHQUFHO29CQUNiLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUM7b0JBQzNFLE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUM1QixDQUFDLENBQUM7Z0JBQ0YsdUVBQXVFO2dCQUN2RSwwREFBMEQ7Z0JBQzFELEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNoQixNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUM7Z0JBQ0QsTUFBTSxDQUFDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQztxQkFDOUQsSUFBSSxDQUFDLFVBQUMsVUFBVTtvQkFDYixPQUFPLEdBQUcsVUFBVSxDQUFDO29CQUNyQixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO3dCQUNULE1BQU0sQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO3dCQUN2Qiw4RUFBOEU7d0JBQzlFLDZEQUE2RDt3QkFDN0QsS0FBSSxDQUFDLFFBQVEsQ0FBQzs0QkFDVixNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQzt3QkFDeEIsQ0FBQyxFQUFFLHVCQUF1QixDQUFDLDJCQUEyQixDQUFDLENBQUM7b0JBQzVELENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQ2hCLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQzt3QkFDeEIsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN0QixDQUFDO29CQUNELFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNqQyxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUNuQixDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsTUFBTTtZQUNaLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsNkJBQTZCLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDMUQsQ0FBQztZQUNELE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFLTyxvREFBa0IsR0FBMUIsVUFBMkIsSUFBWSxFQUFFLEdBQVc7UUFBcEQsaUJBVUM7UUFURyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWM7YUFDckIsWUFBWSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7YUFDdkIsSUFBSSxDQUFDLFVBQUMsUUFBUTtZQUNYLFFBQVEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFFBQVEsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM3RSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDdEIsUUFBUSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzdFLENBQUM7WUFDRCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVPLGdEQUFjLEdBQXRCLFVBQXVCLE1BQWMsRUFBRSxJQUFZLEVBQUUsR0FBVyxFQUFFLEVBQVU7UUFDeEUsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjO2FBQ3JCLE9BQU8sQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUM7YUFDOUIsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQWMsSUFBSSxFQUFsQixDQUFrQixDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVPLCtDQUFhLEdBQXJCLFVBQXNCLE9BQXdCLEVBQUUsTUFBd0I7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUM7YUFDL0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRU8seURBQXVCLEdBQS9CLFVBQ0ksT0FBd0IsRUFDeEIsUUFBMEIsRUFDMUIsSUFBNEI7UUFIaEMsaUJBOENDO1FBekNHLG1HQUFtRztRQUNuRyxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFrQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvRSw0RkFBNEY7UUFDNUYsSUFBTSxlQUFlLEdBQUcsRUFBRSxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxJQUFJO2FBQ04sSUFBSSxDQUFDLFVBQUMsV0FBVztZQUNkLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDZixNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM1QixDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUMzQyxDQUFDO1lBRUQsSUFBTSxPQUFPLEdBQUcsQ0FBQyxDQUFDLElBQUk7Z0JBQ2xCLHVCQUF1QixDQUFDLHdCQUF3QjtnQkFDaEQsUUFBUSxDQUFDLFFBQVE7cUJBQ2QsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEdBQ2pDLEVBQUUsQ0FBQyxDQUFDO1lBRVAsSUFBSSxjQUErQixDQUFDO1lBRXBDLFlBQVksQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUN2QyxZQUFZLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFDekMsWUFBWSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO1lBQ25ELFlBQVksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUFJLFdBQVcsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDMUYsWUFBWSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7WUFDdkMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEdBQUcsY0FBTSxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLGNBQWMsQ0FBQyxFQUEzQyxDQUEyQyxDQUFDO1lBQzNGLFlBQVksQ0FBQyxlQUFlLEdBQUcsc0NBQWtDLE9BQU8sV0FBUSxDQUFDO1lBRWpGLGNBQWMsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsWUFBWSxFQUFFLEtBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM5RSxNQUFNLENBQUMsY0FBYyxDQUFDO1FBQzFCLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFDLEtBQUs7WUFDVCxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDNUIsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDNUIsQ0FBQztZQUNELFlBQVksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMxQyx1Q0FBdUM7WUFDdkMsWUFBWSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDLENBQUM7WUFDekUsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxZQUFZLEVBQUUsS0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hFLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVPLGdEQUFjLEdBQXRCLFVBQXVCLE9BQWlDLEVBQUUsWUFBb0IsRUFBRSxHQUFnQjtRQUFoRyxpQkEwQ0M7UUF6Q0csSUFBSSxjQUFnQyxDQUFDO1FBQ3JDLElBQUksV0FBNEIsQ0FBQztRQUVqQyxJQUFNLGFBQWEsR0FBRztZQUNsQixJQUFJLFFBQVEsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDM0UsRUFBRSxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdDLENBQUM7WUFFRCxLQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNWLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUM1QixDQUFDLENBQUMsQ0FBQztZQUNILEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixXQUFXLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3RDLENBQUM7WUFDRCxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRUYsWUFBWSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDM0MsWUFBWSxDQUFDLFNBQVMsR0FBRyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsQ0FBQztRQUNwRCxZQUFZLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUVqQyxJQUFNLFVBQVUsR0FBRyw0WUFRWixDQUFDO1FBRVIsY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekQsWUFBWSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFFaEMsV0FBVyxHQUFHO1lBQ1YsT0FBTyxFQUFFLGNBQWM7WUFDdkIsYUFBYSxFQUFFLGFBQWE7U0FDL0IsQ0FBQztRQUVGLE1BQU0sQ0FBQyxXQUFXLENBQUM7SUFDdkIsQ0FBQztJQUVPLGtEQUFnQixHQUF4QixVQUF5QixHQUFnQjtRQUNyQyxNQUFNLENBQUksR0FBRyxDQUFDLE9BQU8sU0FBSSxHQUFHLENBQUMsRUFBRSxTQUFJLEdBQUcsQ0FBQyxJQUFJLFVBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUUsQ0FBQztJQUNyRSxDQUFDO0lBN1lhLGdEQUF3QixHQUFHLG1FQUFpRSxDQUFDO0lBQzdGLG1EQUEyQixHQUFXLEdBQUcsQ0FBQztJQUUxQywrQkFBTyxHQUFHO1FBQ3BCLE1BQU0sRUFBRSxrQkFBa0IsRUFBRSxVQUFVO1FBQ3RDLFFBQVEsRUFBRSxJQUFJLEVBQUUsVUFBVTtRQUMxQixnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsZ0JBQWdCO0tBQ2xELENBQUM7SUF1WU4sOEJBQUM7Q0FBQSxBQS9ZRCxJQStZQztrQkEvWW9CLHVCQUF1QiJ9

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var PluginRetrievalContext = /** @class */ (function () {
    function PluginRetrievalContext() {
    }
    return PluginRetrievalContext;
}());
var DataIdentifierType;
(function (DataIdentifierType) {
    DataIdentifierType[DataIdentifierType["notSpecified"] = 0] = "notSpecified";
    DataIdentifierType[DataIdentifierType["netObjectGeneric"] = 1] = "netObjectGeneric";
})(DataIdentifierType = exports.DataIdentifierType || (exports.DataIdentifierType = {}));
var TooltipService = /** @class */ (function () {
    function TooltipService(swApi, $q, $templateCache, $ocLazyLoad, revision) {
        var _this = this;
        this.swApi = swApi;
        this.$q = $q;
        this.$templateCache = $templateCache;
        this.$ocLazyLoad = $ocLazyLoad;
        this.revision = revision;
        this.typeRequirementsCache = {};
        this.resourcesCache = {};
        this.getResource = function (url, queryParams) {
            return _this.swApi.api(false).one(url).get(queryParams);
        };
        this.setRequirementforContext = function (reqPerCtx) {
            return _this.typeRequirementsCache[reqPerCtx.context] = reqPerCtx;
        };
        this.loadRequirementForContext = function (ctx) {
            return _this.getResource("tooltip/types/requirements/" + ctx)
                .then(function (requirements) { return _this.setRequirementforContext({
                context: ctx,
                requirements: requirements
            }); });
        };
        this.getResourceRefId = function (ref) {
            return ref.contentType + ":" + ref.context + ":" + ref.type;
        };
        this.getResourceRef = function (resourceRef) {
            return _this.resourcesCache[_this.getResourceRefId(resourceRef)];
        };
        this.setResourceRef = function (resourceRef) {
            return _this.resourcesCache[_this.getResourceRefId(resourceRef)] = resourceRef;
        };
        /**
         * If a custom javascript is available for this type of resource,
         * load and inject it via ocLazyLoad
         */
        this.loadScript = function (type, context) {
            return _this.getResourceForCtx({ type: type, context: context, contentType: "scripts" }, function (url) {
                return _this.$q
                    .when(_this.$ocLazyLoad.load({
                    type: "js",
                    path: _this.revision.getPathWithRevision("/api2/" + url)
                }))
                    .then(function () { return _this.$q.when(url); });
            });
        };
        /**
         * If a custom stylesheet is available for this type of resource,
         * create a <link /> tag referencing the stylesheet url.
         */
        this.loadStylesheet = function (type, context) {
            return _this.getResourceForCtx({ type: type, context: context, contentType: "styles" }, function (url) {
                _this.appendStylesToDom(url);
                return _this.$q.when(url);
            });
        };
        this.generateTemplateId = function (type, context, extension) {
            return "orion-tooltip/" + context + "/" + type + "/" + extension;
        };
        this.prepareTemplate = function (template, type, context, extension) {
            if (!template) {
                return template;
            }
            if (_.startsWith(template, "T:")) {
                return template.substr(2);
            }
            var templateId = _this.generateTemplateId(type, context, extension);
            _this.$templateCache.put(templateId, template);
            return templateId;
        };
        /**
         * If a customized template is available for this tooltip, then retrieve the template, put it into template
         * cache and return the template cache key.
         */
        this.loadTemplate = function (type, context) {
            return _this.getResourceForCtx({ type: type, context: context, contentType: "templates" }, function (url) { return _this.getResource(url)
                .then(function (template) { return _this.processTemplate(template, type, context); }); }).then(function (template) { return (angular.copy(template) || {}); });
        };
        this.getData = function (siteId, type, context, id) {
            var plugin = new PluginRetrievalContext;
            plugin.Id = encodeURIComponent(id);
            if (siteId !== null) {
                plugin.SiteId = Number(siteId);
            }
            plugin.Context = encodeURIComponent(context);
            plugin.Type = type;
            return _this.getResource("tooltip/data", plugin);
        };
        this.getEnabledTooltipConfigurations = function () {
            return _this.enabledTooltipsCache
                ? _this.$q.resolve(_this.enabledTooltipsCache)
                : _this.getResource("tooltip/types/configuration")
                    .then(function (cache) {
                    _this.enabledTooltipsCache = {};
                    cache.forEach(function (n) { return _this.enabledTooltipsCache[n.key] = n; });
                    return _this.enabledTooltipsCache;
                });
        };
        this.isTooltipEnabled = function (type) {
            // empty cache means not loaded cache because the generic plugin should always be there
            return _this.getEnabledTooltipConfigurations()
                .then(function (cache) { return _this.enabledTooltipsCache[type] != null; });
        };
        this.getTooltipConfiguration = function (type) {
            return _this.getEnabledTooltipConfigurations()
                .then(function (cache) { return _this.enabledTooltipsCache[type]; });
        };
        this.init();
    }
    /**
     * Initializes the information about what types of tooltips require custom scripts, styles and templates.
     * This information is used later to request custom resources when a tooltip creation is requested.
     */
    TooltipService.prototype.init = function () {
        this.getRequirementForContext("default");
    };
    TooltipService.prototype.getRequirementForContext = function (ctx) {
        var req = this.typeRequirementsCache[ctx];
        return req
            ? this.$q.resolve(req)
            : this.loadRequirementForContext(ctx);
    };
    TooltipService.prototype.getResourceForCtx = function (resourceRef, processResourceValue) {
        var _this = this;
        var loadedResourceRef = this.getResourceRef(resourceRef);
        if (loadedResourceRef) {
            return this.$q.resolve(loadedResourceRef.value);
        }
        return this.getRequirementForContext(resourceRef.context)
            .then(function (reqPerCtx) {
            if (_.includes(reqPerCtx.requirements[resourceRef.contentType], resourceRef.type)) {
                var resourceUrl = "tooltip/" + resourceRef.contentType + "/" + resourceRef.type + "/" + resourceRef.context;
                return processResourceValue(resourceUrl)
                    .then(function (value) {
                    resourceRef.value = value;
                    _this.setResourceRef(resourceRef);
                    return value;
                });
            }
            _this.setResourceRef(resourceRef);
            return null;
        });
    };
    TooltipService.prototype.appendStylesToDom = function (url) {
        var head = document.getElementsByTagName("head")[0];
        var link = document.createElement("link");
        link.rel = "stylesheet";
        link.type = "text/css";
        link.href = "/api2/" + url;
        link.media = "all";
        head.appendChild(link);
    };
    TooltipService.prototype.processTemplate = function (tooltipTemplate, type, context) {
        var _this = this;
        tooltipTemplate.template = this.prepareTemplate(tooltipTemplate.template, type, context);
        if (tooltipTemplate.extensions) {
            tooltipTemplate.extensions = _.mapValues(tooltipTemplate.extensions, function (extTemplate, extName) { return _this.prepareTemplate(extTemplate, type, context, extName); });
        }
        return tooltipTemplate;
    };
    TooltipService.$inject = ["swApi", "$q", "$templateCache", "$ocLazyLoad", "revisionService"];
    return TooltipService;
}());
exports.TooltipService = TooltipService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidG9vbHRpcC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBNkJwQztJQUFBO0lBS0EsQ0FBQztJQUFELDZCQUFDO0FBQUQsQ0FBQyxBQUxELElBS0M7QUFFRCxJQUFZLGtCQUdYO0FBSEQsV0FBWSxrQkFBa0I7SUFDMUIsMkVBQWdCLENBQUE7SUFDaEIsbUZBQW9CLENBQUE7QUFDeEIsQ0FBQyxFQUhXLGtCQUFrQixHQUFsQiwwQkFBa0IsS0FBbEIsMEJBQWtCLFFBRzdCO0FBRUQ7SUFRSSx3QkFDWSxLQUFvQixFQUNwQixFQUFhLEVBQ2IsY0FBcUMsRUFDckMsV0FBc0IsRUFDdEIsUUFBMEI7UUFMdEMsaUJBUUM7UUFQVyxVQUFLLEdBQUwsS0FBSyxDQUFlO1FBQ3BCLE9BQUUsR0FBRixFQUFFLENBQVc7UUFDYixtQkFBYyxHQUFkLGNBQWMsQ0FBdUI7UUFDckMsZ0JBQVcsR0FBWCxXQUFXLENBQVc7UUFDdEIsYUFBUSxHQUFSLFFBQVEsQ0FBa0I7UUFUOUIsMEJBQXFCLEdBQXFELEVBQUUsQ0FBQztRQUM3RSxtQkFBYyxHQUFtRCxFQUFFLENBQUM7UUFxQnBFLGdCQUFXLEdBQUcsVUFBSSxHQUFXLEVBQUUsV0FBaUI7WUFDcEQsT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFJLFdBQVcsQ0FBQztRQUFsRCxDQUFrRCxDQUFDO1FBRS9DLDZCQUF3QixHQUFHLFVBQUMsU0FBc0M7WUFDMUUsT0FBQSxLQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLFNBQVM7UUFBekQsQ0FBeUQsQ0FBQztRQUVsRCw4QkFBeUIsR0FBRyxVQUFDLEdBQVc7WUFDNUMsT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFvQixnQ0FBOEIsR0FBSyxDQUFDO2lCQUNuRSxJQUFJLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxLQUFJLENBQUMsd0JBQXdCLENBQUM7Z0JBQ2hELE9BQU8sRUFBRSxHQUFHO2dCQUNaLFlBQVksRUFBRSxZQUFZO2FBQzdCLENBQUMsRUFIb0IsQ0FHcEIsQ0FBQztRQUpQLENBSU8sQ0FBQztRQVNKLHFCQUFnQixHQUFHLFVBQUMsR0FBd0I7WUFDaEQsT0FBRyxHQUFHLENBQUMsV0FBVyxTQUFJLEdBQUcsQ0FBQyxPQUFPLFNBQUksR0FBRyxDQUFDLElBQU07UUFBL0MsQ0FBK0MsQ0FBQztRQUU1QyxtQkFBYyxHQUFHLFVBQUMsV0FBZ0M7WUFDdEQsT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUF2RCxDQUF1RCxDQUFDO1FBRXBELG1CQUFjLEdBQUcsVUFBQyxXQUFnQztZQUN0RCxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsV0FBVztRQUFyRSxDQUFxRSxDQUFDO1FBMEIxRTs7O1dBR0c7UUFDSSxlQUFVLEdBQUcsVUFBQyxJQUFZLEVBQUUsT0FBZTtZQUM5QyxPQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FDbEIsRUFBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBQyxFQUN0RCxVQUFDLEdBQVc7Z0JBQ1IsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFO3FCQUNULElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztvQkFDeEIsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLEtBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztpQkFDMUQsQ0FBQyxDQUFDO3FCQUNGLElBQUksQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQWpCLENBQWlCLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQ2dCO1FBVnJCLENBVXFCLENBQUM7UUFFMUI7OztXQUdHO1FBQ0ksbUJBQWMsR0FBRyxVQUFDLElBQVksRUFBRSxPQUFlO1lBQ2xELE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUMsRUFDeEUsVUFBQyxHQUFXO2dCQUNSLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUIsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdCLENBQUMsQ0FBcUI7UUFKMUIsQ0FJMEIsQ0FBQztRQVl2Qix1QkFBa0IsR0FBRyxVQUFDLElBQVksRUFBRSxPQUFlLEVBQUUsU0FBa0I7WUFDM0UsT0FBQSxtQkFBaUIsT0FBTyxTQUFJLElBQUksU0FBSSxTQUFXO1FBQS9DLENBQStDLENBQUM7UUFFNUMsb0JBQWUsR0FBRyxVQUFDLFFBQWdCLEVBQUUsSUFBWSxFQUFFLE9BQWUsRUFBRSxTQUFrQjtZQUMxRixFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ1osTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUNwQixDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5QixDQUFDO1lBQ0QsSUFBTSxVQUFVLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDckUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzlDLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDdEIsQ0FBQyxDQUFBO1FBYUQ7OztXQUdHO1FBQ0ksaUJBQVksR0FBRyxVQUFDLElBQVksRUFBRSxPQUFlO1lBQ2hELE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUNsQixFQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFDLEVBQ3hELFVBQUMsR0FBVyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBbUIsR0FBRyxDQUFDO2lCQUNuRCxJQUFJLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLEVBQTdDLENBQTZDLENBQUMsRUFEbkQsQ0FDbUQsQ0FDdkUsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFxQixFQUFsRCxDQUFrRCxDQUFDO1FBSnRFLENBSXNFLENBQUM7UUFFcEUsWUFBTyxHQUFHLFVBQUMsTUFBYyxFQUFFLElBQVksRUFBRSxPQUFlLEVBQUUsRUFBVTtZQUV2RSxJQUFJLE1BQU0sR0FBRyxJQUFJLHNCQUFzQixDQUFDO1lBQ3hDLE1BQU0sQ0FBQyxFQUFFLEdBQUcsa0JBQWtCLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbkMsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLENBQUM7WUFDRCxNQUFNLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzdDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRW5CLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUF3QixjQUFjLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDM0UsQ0FBQyxDQUFDO1FBRU0sb0NBQStCLEdBQUc7WUFDdEMsT0FBQSxLQUFJLENBQUMsb0JBQW9CO2dCQUNyQixDQUFDLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDO2dCQUM1QyxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBMEIsNkJBQTZCLENBQUM7cUJBQ3JFLElBQUksQ0FBQyxVQUFBLEtBQUs7b0JBQ1AsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztvQkFDL0IsS0FBSyxDQUFDLE9BQU8sQ0FBRSxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFwQyxDQUFvQyxDQUFDLENBQUM7b0JBQzFELE1BQU0sQ0FBQyxLQUFJLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQztRQVBWLENBT1UsQ0FBQztRQUVSLHFCQUFnQixHQUFHLFVBQUMsSUFBWTtZQUNuQyx1RkFBdUY7WUFDdkYsT0FBQSxLQUFJLENBQUMsK0JBQStCLEVBQUU7aUJBQ2pDLElBQUksQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQXZDLENBQXVDLENBQUM7UUFEM0QsQ0FDMkQsQ0FBQztRQUUxRCw0QkFBdUIsR0FBSSxVQUFDLElBQVc7WUFDekMsT0FBQSxLQUFJLENBQUMsK0JBQStCLEVBQUU7aUJBQ2pDLElBQUksQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsRUFBL0IsQ0FBK0IsQ0FBRTtRQURwRCxDQUNvRCxDQUFDO1FBektyRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7T0FHRztJQUNLLDZCQUFJLEdBQVo7UUFDSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQWVPLGlEQUF3QixHQUFoQyxVQUFpQyxHQUFXO1FBQ3hDLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QyxNQUFNLENBQUMsR0FBRztZQUNOLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7WUFDdEIsQ0FBQyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBV08sMENBQWlCLEdBQXpCLFVBQ0ksV0FBZ0MsRUFDaEMsb0JBQWdFO1FBRnBFLGlCQXNCQztRQWxCRyxJQUFNLGlCQUFpQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0QsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO2FBQ3BELElBQUksQ0FBQyxVQUFBLFNBQVM7WUFDWCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hGLElBQU0sV0FBVyxHQUFHLGFBQVcsV0FBVyxDQUFDLFdBQVcsU0FBSSxXQUFXLENBQUMsSUFBSSxTQUFJLFdBQVcsQ0FBQyxPQUFTLENBQUM7Z0JBQ3BHLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUM7cUJBQ25DLElBQUksQ0FBQyxVQUFDLEtBQWE7b0JBQ2hCLFdBQVcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO29CQUMxQixLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNqQyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUNqQixDQUFDLENBQUMsQ0FBQztZQUNYLENBQUM7WUFDRCxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBOEJPLDBDQUFpQixHQUF6QixVQUEyQixHQUFXO1FBQ2xDLElBQU0sSUFBSSxHQUFJLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2RCxJQUFNLElBQUksR0FBSSxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxHQUFHLEdBQUksWUFBWSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxHQUFHLEdBQUcsQ0FBQztRQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFpQk8sd0NBQWUsR0FBdkIsVUFBd0IsZUFBaUMsRUFBRSxJQUFZLEVBQUUsT0FBZTtRQUF4RixpQkFTQztRQVJHLGVBQWUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN6RixFQUFFLENBQUMsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUM3QixlQUFlLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQ3BDLGVBQWUsQ0FBQyxVQUFVLEVBQzFCLFVBQUMsV0FBVyxFQUFFLE9BQU8sSUFBSyxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLEVBQXpELENBQXlELENBQ3RGLENBQUM7UUFDTixDQUFDO1FBQ0QsTUFBTSxDQUFDLGVBQWUsQ0FBQztJQUMzQixDQUFDO0lBM0lhLHNCQUFPLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBdUxoRyxxQkFBQztDQUFBLEFBekxELElBeUxDO0FBekxZLHdDQUFjIn0=

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(51);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var entityPopoverLoader_controller_1 = __webpack_require__(48);
function processSelectors(selectors) {
    return (selectors || []).map(function (sel) { return sel + ":not(.NoTip):not(.forceLegacyTooltips *)"; });
}
/**
 * @ngdoc directive
 * @name orion.directive:swEntityPopoverLoader
 * @restrict A
 *
 * @description
 * This directive is used on containers to enable Orion popovers.
 *
 * @example
 * <example module="orion">
 *     <file src="src/directives/entityPopoverLoader/docs/entityPopoverLoader-examples.html" name="index.html"></file>
 *     <file src="src/directives/entityPopoverLoader/docs/entityPopoverLoader-examples.js" name="app.js"></file>
 * </example>
 */
var TooltipLoader = /** @class */ (function () {
    function TooltipLoader($timeout) {
        var _this = this;
        this.$timeout = $timeout;
        this.restrict = "A";
        this.controller = entityPopoverLoader_controller_1.default;
        this.controllerAs = "vm";
        this.bindToController = {};
        this.link = function (scope, element, attrs, ctrl) {
            var selector = _.join(TooltipLoader.SELECTORS, ",");
            var selectorArea = _.join(TooltipLoader.AREA_SELECTORS, ",");
            var tooltipPopover;
            function parseCoords($area) {
                var text = $area.attr("coords");
                var coords = _.map((text || ",,,").split(","), Number);
                return {
                    left: coords[0],
                    top: coords[1],
                    right: coords[2],
                    bottom: coords[3]
                };
            }
            function createAreaWrapper($area) {
                var $wrapper = angular.element("<a>");
                var mapName = $area.closest("map").attr("name");
                var $image = angular.element("img[usemap=\"#" + mapName + "\"]");
                var $posParent = $image.parent();
                var offset = $image.offset();
                var coords = parseCoords($area);
                offset.left += coords.left;
                offset.top += coords.top;
                $wrapper.appendTo($posParent);
                $wrapper.attr("href", $area.attr("href"));
                $wrapper.css({
                    "width": coords.right - coords.left,
                    "height": coords.bottom - coords.top,
                    "position": "absolute",
                    "visibility": "hidden",
                    "display": "inline-block",
                    "z-index": -1
                });
                $wrapper.offset(offset);
                return $wrapper.get(0);
            }
            function getOrSetData($el, key, create) {
                var value = $el.data(key);
                if (value) {
                    return value;
                }
                var newValue = create($el);
                $el.data(key, newValue);
                return newValue;
            }
            function getAreaWrapper(area, create) {
                if (create === void 0) { create = true; }
                return getOrSetData(area, "SWI_TOOLTIP_WRAPPER", create ? createAreaWrapper : function () { return null; });
            }
            var hoverContext = {
                element: null,
            };
            var handleHover = function (target) {
                _this.$timeout(function () {
                    if (hoverContext.element === target[0]) {
                        ctrl.load(element, target, hoverContext)
                            .then(function (popover) { return tooltipPopover = popover; })
                            .catch(angular.noop); // UIF-7010 Needed block to avoid error in console when cancelled request
                    }
                }, TooltipLoader.TOOLTIP_DELAY);
            };
            element.on("mouseover", selectorArea, function (event) {
                var target = angular.element(event.currentTarget);
                var wrapper = getAreaWrapper(target);
                angular.element(wrapper).trigger("mouseover");
            });
            element.on("mouseleave", selectorArea, function (event) {
                var target = angular.element(event.currentTarget);
                var wrapper = getAreaWrapper(target, false);
                if (wrapper) {
                    angular.element(wrapper).trigger("mouseleave");
                }
            });
            // We don't want the mouseover to trigger immediately, so we postpone the tooltip creation
            // after `tooltipDelay` milliseconds. We store the last active element to know if the user still hovers the
            // mouse over the same element after the delay expires.
            element.on("mouseover", selector, function (event) {
                var target = angular.element(event.currentTarget);
                hoverContext.element = target[0];
                handleHover(target);
            });
            element.on("mouseleave", selector, function () {
                hoverContext.element = null;
            });
            element.on("dragstart", selector, function () {
                if (tooltipPopover) {
                    tooltipPopover.removePopover();
                }
            });
        };
    }
    TooltipLoader.ENTITY_OPID_ATTR = "opid";
    TooltipLoader.NETOBJECT_ATTR = "netobject";
    TooltipLoader.TOOLTIP_DELAY = 200; // milliseconds
    TooltipLoader.NET_OBJECT_HREF_SELECTOR = "[href*='NetObject=']:not([href*='NoTip'])";
    TooltipLoader.OPID_HREF_SELECTOR = "[href*='Opid=']:not([href*='NoTip'])";
    TooltipLoader.SELECTORS = processSelectors([
        "a" + TooltipLoader.NET_OBJECT_HREF_SELECTOR,
        "a" + TooltipLoader.OPID_HREF_SELECTOR,
        "[" + TooltipLoader.NETOBJECT_ATTR + "]",
        "[" + TooltipLoader.ENTITY_OPID_ATTR + "]"
    ]);
    TooltipLoader.AREA_SELECTORS = processSelectors([
        "area" + TooltipLoader.NET_OBJECT_HREF_SELECTOR,
        "area" + TooltipLoader.OPID_HREF_SELECTOR
    ]);
    TooltipLoader.$inject = ["$timeout"];
    return TooltipLoader;
}());
exports.default = TooltipLoader;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5UG9wb3ZlckxvYWRlci1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnRpdHlQb3BvdmVyTG9hZGVyLWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHVDQUF1QztBQUN2QyxtRkFBdUc7QUFPdkcsMEJBQTBCLFNBQW1CO0lBQ3pDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBRyxHQUFHLDZDQUEwQyxFQUFoRCxDQUFnRCxDQUFDLENBQUM7QUFDMUYsQ0FBQztBQUVEOzs7Ozs7Ozs7Ozs7O0dBYUc7QUFDSDtJQTJCSSx1QkFBb0IsUUFBNEI7UUFBaEQsaUJBQ0M7UUFEbUIsYUFBUSxHQUFSLFFBQVEsQ0FBb0I7UUFQekMsYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNmLGVBQVUsR0FBRyx3Q0FBdUIsQ0FBQztRQUNyQyxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFPdEIsU0FBSSxHQUFHLFVBQ1YsS0FBZ0IsRUFDaEIsT0FBNEIsRUFDNUIsS0FBcUIsRUFDckIsSUFBNkI7WUFFN0IsSUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3RELElBQU0sWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUMvRCxJQUFJLGNBQStCLENBQUM7WUFhcEMscUJBQXFCLEtBQTBCO2dCQUMzQyxJQUFNLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNsQyxJQUFNLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDekQsTUFBTSxDQUFDO29CQUNILElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNmLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNkLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNoQixNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztpQkFDcEIsQ0FBQztZQUNOLENBQUM7WUFFRCwyQkFBMkIsS0FBMEI7Z0JBQ2pELElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3hDLElBQU0sT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNsRCxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFnQixPQUFPLFFBQUksQ0FBQyxDQUFDO2dCQUM1RCxJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ25DLElBQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDL0IsSUFBTSxNQUFNLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsQyxNQUFNLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQzNCLE1BQU0sQ0FBQyxHQUFHLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDekIsUUFBUSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDOUIsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxRQUFRLENBQUMsR0FBRyxDQUFDO29CQUNULE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJO29CQUNuQyxRQUFRLEVBQUUsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsR0FBRztvQkFDcEMsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLFlBQVksRUFBRSxRQUFRO29CQUN0QixTQUFTLEVBQUUsY0FBYztvQkFDekIsU0FBUyxFQUFFLENBQUMsQ0FBQztpQkFDaEIsQ0FBQyxDQUFDO2dCQUNILFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hCLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNCLENBQUM7WUFFRCxzQkFDSSxHQUF3QixFQUN4QixHQUFXLEVBQ1gsTUFBMkM7Z0JBRTNDLElBQU0sS0FBSyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzVCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDakIsQ0FBQztnQkFDRCxJQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzdCLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3BCLENBQUM7WUFFRCx3QkFBd0IsSUFBc0IsRUFBRSxNQUFzQjtnQkFBdEIsdUJBQUEsRUFBQSxhQUFzQjtnQkFDbEUsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsY0FBTSxPQUFBLElBQUksRUFBSixDQUFJLENBQUMsQ0FBQztZQUM5RixDQUFDO1lBRUQsSUFBTSxZQUFZLEdBQWtCO2dCQUNoQyxPQUFPLEVBQUUsSUFBSTthQUNoQixDQUFDO1lBRUYsSUFBTSxXQUFXLEdBQUcsVUFBQyxNQUF3QjtnQkFDekMsS0FBSSxDQUFDLFFBQVEsQ0FBQztvQkFDVixFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxZQUFZLENBQUM7NkJBQ25DLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLGNBQWMsR0FBRyxPQUFPLEVBQXhCLENBQXdCLENBQUM7NkJBQ3pDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyx5RUFBeUU7b0JBQ3ZHLENBQUM7Z0JBQ0wsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNwQyxDQUFDLENBQUM7WUFFRixPQUFPLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsVUFBQyxLQUF3QjtnQkFDM0QsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3BELElBQU0sT0FBTyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDdkMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxZQUFZLEVBQUUsVUFBQyxLQUF3QjtnQkFDNUQsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3BELElBQU0sT0FBTyxHQUFHLGNBQWMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQzlDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ1YsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ25ELENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUVILDBGQUEwRjtZQUMxRiwyR0FBMkc7WUFDM0csdURBQXVEO1lBQ3ZELE9BQU8sQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSxVQUFDLEtBQXdCO2dCQUN2RCxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDcEQsWUFBWSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztZQUVILE9BQU8sQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBRTtnQkFDL0IsWUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxRQUFRLEVBQUU7Z0JBQzlCLEVBQUUsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQ2pCLGNBQWMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDbkMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO0lBekhGLENBQUM7SUEzQmEsOEJBQWdCLEdBQUcsTUFBTSxDQUFDO0lBQzFCLDRCQUFjLEdBQUcsV0FBVyxDQUFDO0lBQzdCLDJCQUFhLEdBQUcsR0FBRyxDQUFDLENBQUMsZUFBZTtJQUVwQyxzQ0FBd0IsR0FBRywyQ0FBMkMsQ0FBQztJQUN2RSxnQ0FBa0IsR0FBRyxzQ0FBc0MsQ0FBQztJQUU1RCx1QkFBUyxHQUFHLGdCQUFnQixDQUFDO1FBQ3ZDLE1BQUksYUFBYSxDQUFDLHdCQUEwQjtRQUM1QyxNQUFJLGFBQWEsQ0FBQyxrQkFBb0I7UUFDdEMsTUFBSSxhQUFhLENBQUMsY0FBYyxNQUFHO1FBQ25DLE1BQUksYUFBYSxDQUFDLGdCQUFnQixNQUFHO0tBQ3hDLENBQUMsQ0FBQztJQUVXLDRCQUFjLEdBQUcsZ0JBQWdCLENBQUM7UUFDNUMsU0FBTyxhQUFhLENBQUMsd0JBQTBCO1FBQy9DLFNBQU8sYUFBYSxDQUFDLGtCQUFvQjtLQUM1QyxDQUFDLENBQUM7SUFPVyxxQkFBTyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7SUE2SHpDLG9CQUFDO0NBQUEsQUF0SkQsSUFzSkM7a0JBdEpvQixhQUFhIn0=

/***/ }),
/* 51 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 52 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionErrorPageController = /** @class */ (function () {
    function OrionErrorPageController($stateParams) {
        var _this = this;
        this.$stateParams = $stateParams;
        this.$onInit = function () {
            _this.errorReport = _this.$stateParams;
        };
    }
    OrionErrorPageController.$inject = ["$stateParams"];
    return OrionErrorPageController;
}());
exports.default = OrionErrorPageController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25FcnJvci1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib3Jpb25FcnJvci1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBSXZDO0lBR0ksa0NBQW9CLFlBQW9DO1FBQXhELGlCQUE0RDtRQUF4QyxpQkFBWSxHQUFaLFlBQVksQ0FBd0I7UUFFakQsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDO1FBQ3pDLENBQUMsQ0FBQztJQUp5RCxDQUFDO0lBRjlDLGdDQUFPLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQVM3QywrQkFBQztDQUFBLEFBVkQsSUFVQztBQUVELGtCQUFlLHdCQUF3QixDQUFDIn0=

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    var defaultParameters = {
        errorType: "",
        errorCode: "",
        message: "",
        location: ""
    };
    var errorState = "Error";
    $stateProvider
        .state(errorState, {
        i18Title: "_t(Error)",
        controller: "OrionErrorPageController",
        controllerAs: "vm",
        template: __webpack_require__(34),
        params: defaultParameters
    });
    $urlRouterProvider.otherwise(function ($injector, $location) {
        var $state = $injector.get("$state");
        var _t = $injector.get("getTextService");
        var swUtil = $injector.get("swUtil");
        var path = $location.path();
        var parameters = {
            errorType: "navigation",
            errorCode: "404",
            message: swUtil.formatString(_t("The file '{0}' does not exist"), path),
            location: path
        };
        $state.go(errorState, parameters);
    });
}
exports.default = config;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25FcnJvci1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmlvbkVycm9yLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUt2QyxnQkFBZ0I7QUFDaEIsZ0JBQ0ksY0FBb0MsRUFDcEMsa0JBQTRDO0lBRTVDLElBQU0saUJBQWlCLEdBQTJCO1FBQzlDLFNBQVMsRUFBRSxFQUFFO1FBQ2IsU0FBUyxFQUFFLEVBQUU7UUFDYixPQUFPLEVBQUUsRUFBRTtRQUNYLFFBQVEsRUFBRSxFQUFFO0tBQ2YsQ0FBQztJQUNGLElBQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQztJQUUzQixjQUFjO1NBQ1QsS0FBSyxDQUFDLFVBQVUsRUFBYztRQUMzQixRQUFRLEVBQUUsV0FBVztRQUNyQixVQUFVLEVBQUUsMEJBQTBCO1FBQ3RDLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQVMsbUJBQW1CLENBQUM7UUFDOUMsTUFBTSxFQUFFLGlCQUFpQjtLQUM1QixDQUFDLENBQUM7SUFFUCxrQkFBa0IsQ0FBQyxTQUFTLENBQ3hCLFVBQUMsU0FBbUMsRUFBRSxTQUE4QjtRQUNoRSxJQUFNLE1BQU0sR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFzQixRQUFRLENBQUMsQ0FBQztRQUM1RCxJQUFNLEVBQUUsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFtQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzdFLElBQU0sTUFBTSxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQVcsUUFBUSxDQUFDLENBQUM7UUFDakQsSUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzlCLElBQU0sVUFBVSxHQUEyQjtZQUN2QyxTQUFTLEVBQUUsWUFBWTtZQUN2QixTQUFTLEVBQUUsS0FBSztZQUNoQixPQUFPLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsK0JBQStCLENBQUMsRUFBRSxJQUFJLENBQUM7WUFDdkUsUUFBUSxFQUFFLElBQUk7U0FDakIsQ0FBQztRQUVGLE1BQU0sQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3RDLENBQUMsQ0FDSixDQUFDO0FBQ04sQ0FBQztBQUVELGtCQUFlLE1BQU0sQ0FBQyJ9

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(56);
module.exports = __webpack_require__(57);


/***/ }),
/* 56 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
__webpack_require__(58);
var app_1 = __webpack_require__(6);
var config_1 = __webpack_require__(63);
var templates_1 = __webpack_require__(64);
var index_1 = __webpack_require__(80);
var index_2 = __webpack_require__(96);
var http_1 = __webpack_require__(154);
var index_3 = __webpack_require__(159);
var views_1 = __webpack_require__(185);
var filters_1 = __webpack_require__(213);
var components_1 = __webpack_require__(216);
config_1.default(app_1.default);
index_1.default(app_1.default);
index_2.default(app_1.default);
http_1.default(app_1.default);
index_3.default(app_1.default);
templates_1.default(app_1.default);
views_1.default(app_1.default);
filters_1.default(app_1.default);
components_1.default(app_1.default);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsaUNBQWlDOztBQUVqQzs7SUFFSTtBQUVILDRCQUEwQjtBQUUzQixvQ0FBaUM7QUFDakMsMENBQXFDO0FBQ3JDLGdEQUEyQztBQUMzQyx5Q0FBc0M7QUFDdEMsNENBQTRDO0FBQzVDLCtCQUEwQjtBQUMxQiwwQ0FBd0M7QUFDeEMsaUNBQTRCO0FBQzVCLHFDQUFnQztBQUNoQywyQ0FBc0M7QUFFdEMsZ0JBQU0sQ0FBQyxhQUFLLENBQUMsQ0FBQztBQUNkLGVBQU8sQ0FBQyxhQUFLLENBQUMsQ0FBQztBQUNmLGVBQVUsQ0FBQyxhQUFLLENBQUMsQ0FBQztBQUNsQixjQUFJLENBQUMsYUFBSyxDQUFDLENBQUM7QUFDWixlQUFRLENBQUMsYUFBSyxDQUFDLENBQUM7QUFDaEIsbUJBQVMsQ0FBQyxhQUFLLENBQUMsQ0FBQztBQUNqQixlQUFLLENBQUMsYUFBSyxDQUFDLENBQUM7QUFDYixpQkFBTyxDQUFDLGFBQUssQ0FBQyxDQUFDO0FBQ2Ysb0JBQVUsQ0FBQyxhQUFLLENBQUMsQ0FBQyJ9

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

// not "use strict" so we can declare global "Promise"

var asap = __webpack_require__(59);

if (typeof Promise === 'undefined') {
  Promise = __webpack_require__(5)
  __webpack_require__(61)
}

__webpack_require__(62);


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// rawAsap provides everything we need except exception management.
var rawAsap = __webpack_require__(4);
// RawTasks are recycled to reduce GC churn.
var freeTasks = [];
// We queue errors to ensure they are thrown in right order (FIFO).
// Array-as-queue is good enough here, since we are just dealing with exceptions.
var pendingErrors = [];
var requestErrorThrow = rawAsap.makeRequestCallFromTimer(throwFirstError);

function throwFirstError() {
    if (pendingErrors.length) {
        throw pendingErrors.shift();
    }
}

/**
 * Calls a task as soon as possible after returning, in its own event, with priority
 * over other events like animation, reflow, and repaint. An error thrown from an
 * event will not interrupt, nor even substantially slow down the processing of
 * other events, but will be rather postponed to a lower priority event.
 * @param {{call}} task A callable object, typically a function that takes no
 * arguments.
 */
module.exports = asap;
function asap(task) {
    var rawTask;
    if (freeTasks.length) {
        rawTask = freeTasks.pop();
    } else {
        rawTask = new RawTask();
    }
    rawTask.task = task;
    rawAsap(rawTask);
}

// We wrap tasks with recyclable task objects.  A task object implements
// `call`, just like a function.
function RawTask() {
    this.task = null;
}

// The sole purpose of wrapping the task is to catch the exception and recycle
// the task object after its single use.
RawTask.prototype.call = function () {
    try {
        this.task.call();
    } catch (error) {
        if (asap.onerror) {
            // This hook exists purely for testing purposes.
            // Its name will be periodically randomized to break any code that
            // depends on its existence.
            asap.onerror(error);
        } else {
            // In a web browser, exceptions are not fatal. However, to avoid
            // slowing down the queue of pending tasks, we rethrow the error in a
            // lower priority turn.
            pendingErrors.push(error);
            requestErrorThrow();
        }
    } finally {
        this.task = null;
        freeTasks[freeTasks.length] = this;
    }
};


/***/ }),
/* 60 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//This file contains the ES6 extensions to the core Promises/A+ API

var Promise = __webpack_require__(5);

module.exports = Promise;

/* Static Functions */

var TRUE = valuePromise(true);
var FALSE = valuePromise(false);
var NULL = valuePromise(null);
var UNDEFINED = valuePromise(undefined);
var ZERO = valuePromise(0);
var EMPTYSTRING = valuePromise('');

function valuePromise(value) {
  var p = new Promise(Promise._0);
  p._V = 1;
  p._W = value;
  return p;
}
Promise.resolve = function (value) {
  if (value instanceof Promise) return value;

  if (value === null) return NULL;
  if (value === undefined) return UNDEFINED;
  if (value === true) return TRUE;
  if (value === false) return FALSE;
  if (value === 0) return ZERO;
  if (value === '') return EMPTYSTRING;

  if (typeof value === 'object' || typeof value === 'function') {
    try {
      var then = value.then;
      if (typeof then === 'function') {
        return new Promise(then.bind(value));
      }
    } catch (ex) {
      return new Promise(function (resolve, reject) {
        reject(ex);
      });
    }
  }
  return valuePromise(value);
};

var iterableToArray = function (iterable) {
  if (typeof Array.from === 'function') {
    // ES2015+, iterables exist
    iterableToArray = Array.from;
    return Array.from(iterable);
  }

  // ES5, only arrays and array-likes exist
  iterableToArray = function (x) { return Array.prototype.slice.call(x); };
  return Array.prototype.slice.call(iterable);
}

Promise.all = function (arr) {
  var args = iterableToArray(arr);

  return new Promise(function (resolve, reject) {
    if (args.length === 0) return resolve([]);
    var remaining = args.length;
    function res(i, val) {
      if (val && (typeof val === 'object' || typeof val === 'function')) {
        if (val instanceof Promise && val.then === Promise.prototype.then) {
          while (val._V === 3) {
            val = val._W;
          }
          if (val._V === 1) return res(i, val._W);
          if (val._V === 2) reject(val._W);
          val.then(function (val) {
            res(i, val);
          }, reject);
          return;
        } else {
          var then = val.then;
          if (typeof then === 'function') {
            var p = new Promise(then.bind(val));
            p.then(function (val) {
              res(i, val);
            }, reject);
            return;
          }
        }
      }
      args[i] = val;
      if (--remaining === 0) {
        resolve(args);
      }
    }
    for (var i = 0; i < args.length; i++) {
      res(i, args[i]);
    }
  });
};

Promise.reject = function (value) {
  return new Promise(function (resolve, reject) {
    reject(value);
  });
};

Promise.race = function (values) {
  return new Promise(function (resolve, reject) {
    iterableToArray(values).forEach(function(value){
      Promise.resolve(value).then(resolve, reject);
    });
  });
};

/* Prototype Methods */

Promise.prototype['catch'] = function (onRejected) {
  return this.then(null, onRejected);
};


/***/ }),
/* 62 */
/***/ (function(module, exports) {

// should work in any browser without browserify

if (typeof Promise.prototype.done !== 'function') {
  Promise.prototype.done = function (onFulfilled, onRejected) {
    var self = arguments.length ? this.then.apply(this, arguments) : this
    self.then(null, function (err) {
      setTimeout(function () {
        throw err
      }, 0)
    })
  }
}

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
config.$inject = ["$controllerProvider", "$compileProvider", "$httpProvider", "constants", "swOrionDemoModeConstants", "$templateRequestProvider"];
run.$inject = ["$rootScope", "$log", "$state", "$window", "swOrionDemoModeConstants", "swAuthService", "swSalesTriggerDialogService", "swOrionImprovementDialogService"];
Object.defineProperty(exports, "__esModule", { value: true });
var sw_1 = __webpack_require__(7);
var demoConstants = {
    routes: {
        "/api2/ping": {
            post: { alertUser: false, data: {} }
        },
        "/api2/badrequest": {}
    },
    blockedAnchors: []
};
/** @ngInject */
function config($controllerProvider, $compileProvider, $httpProvider, constants, swOrionDemoModeConstants, $templateRequestProvider) {
    var env = sw_1.default.environment = (sw_1.default.environment || {});
    env.isProduction = (env.name === "production");
    $compileProvider.debugInfoEnabled(!env.isProduction);
    configureSW();
    configureHttp();
    var orionResources = angular.module("orion.resources");
    orionResources.controller = $controllerProvider.register;
    orionResources.widget = $compileProvider.directive;
    swOrionDemoModeConstants.blockedAnchors.push("blocked_anchor_test_page/");
    swOrionDemoModeConstants.blockedAnchors.push("notorion/somepage.aspx*");
    //#region Internal Methods
    function configureSW() {
        constants.environment = sw_1.default.environment;
        constants.locale = sw_1.default.region;
        constants.user = sw_1.default.user;
        constants.license = sw_1.default.license ? sw_1.default.license : {};
        constants.dashboards = { maxColumns: 6 };
        constants.featureToggles = sw_1.default.featureToggles;
    }
    function configureHttp() {
        $httpProvider.interceptors.push("swErrorInterceptor");
        $httpProvider.interceptors.push("swViewIdInterceptor");
        if (sw_1.default.environment.demoMode === true) {
            $httpProvider.interceptors.push("swDemoInterceptor");
        }
        if (constants.useResponseFormatter) {
            $httpProvider.interceptors.push("swFormatInterceptor");
        }
        // WebApi currently only supports JQuery formatting for URL parameters.
        $httpProvider.defaults.paramSerializer = "$httpParamSerializerJQLike";
        $templateRequestProvider.httpOptions({ _isTemplate: true });
    }
    //#endregion
}
/** @ngInject */
function run($rootScope, $log, $state, $window, swOrionDemoModeConstants, swAuthService, swSalesTriggerDialogService, swOrionImprovementDialogService) {
    $log.info("module-run: " + sw_1.default.orion.app().name);
    function auth(event, toState, toParams, fromState, fromParams) {
        if (toState.authProfile && !swAuthService.isUserAuthorized(toState.authProfile)) {
            $state.transitionTo("Error", toState.params);
            event.preventDefault();
        }
    }
    function configureCoreShims() {
        var salesTriggerShim = {
            ShowEvalPopupAsync: function () { swSalesTriggerDialogService.show("Eval"); },
            ShowEvalPopup: function () { swSalesTriggerDialogService.show("Eval"); },
            ShowLicensePopupAsync: function () { swSalesTriggerDialogService.show("Licensing"); },
            ShowLicensePopup: function () { swSalesTriggerDialogService.show("Licensing"); },
            ShowMaintenancePopupAsync: function () { swSalesTriggerDialogService.show("Maintenance"); },
            ShowMaintenancePopup: function () { swSalesTriggerDialogService.show("Maintenance"); },
            ShowPollerLimitPopupAsync: function () { swSalesTriggerDialogService.show("PollerLimit"); },
            ShowPollerLimitPopup: function () { swSalesTriggerDialogService.show("PollerLimit"); },
            ShowUpgradePopupAsync: function () { swSalesTriggerDialogService.show("Upgrade"); },
            ShowUpgradePopup: function () { swSalesTriggerDialogService.show("Upgrade"); }
        };
        $rootScope.SW.Core = $rootScope.SW.Core || {};
        if (!$rootScope.SW.Core.SalesTrigger) {
            $rootScope.SW.Core.SalesTrigger = salesTriggerShim;
        }
        var oipShim = {
            showDialog: function (isChecked) { swOrionImprovementDialogService.show(isChecked); }
        };
        $rootScope.SW.Orion = $rootScope.SW.Orion || {};
        if (!$rootScope.SW.Orion.OIP) {
            $rootScope.SW.Orion.OIP = oipShim;
        }
    }
    function configureMomentLocale() {
        // check if navigator is even accessible
        if (!$window.navigator) {
            return;
        }
        // get current locale - IE has 'userLanguage' and others 'language'
        var currentLocale = ($window.navigator.userLanguage || $window.navigator.language)
            .toLowerCase().trim();
        // find two-letter locale description
        var foundLocale = /^[a-z]+/.exec(currentLocale);
        if (!foundLocale || foundLocale.length !== 1) {
            return;
        }
        var definedLocales = moment.locales();
        // use locale definition if loaded
        if (definedLocales.indexOf(foundLocale[0]) >= 0) {
            $log.info("setting moment locale to: " + foundLocale[0]);
            moment.locale(foundLocale[0]);
        }
    }
    $rootScope.SW = sw_1.default;
    $rootScope.demoMode = sw_1.default.environment.demoMode;
    $rootScope.$demoConfig = swOrionDemoModeConstants;
    $rootScope.$on("$stateChangeStart", auth);
    configureCoreShims();
    configureMomentLocale();
}
exports.default = function (module) {
    var constants = {
        useResponseFormatter: false
    };
    module.app()
        .constant("constants", constants)
        .constant("swOrionDemoModeConstants", demoConstants)
        .config(config)
        .run(run); // this is run when the injector is done loading all modules
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBZ0JwQywyQkFBc0I7QUFHdEIsSUFBTSxhQUFhLEdBQUc7SUFDbEIsTUFBTSxFQUFFO1FBQ0osWUFBWSxFQUFFO1lBQ1YsSUFBSSxFQUFFLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFDO1NBQ3JDO1FBQ0Qsa0JBQWtCLEVBQUUsRUFBRTtLQUN6QjtJQUNELGNBQWMsRUFBYSxFQUFFO0NBQ2hDLENBQUM7QUFTRixnQkFBZ0I7QUFDaEIsZ0JBQ0ksbUJBQXdDLEVBQ3hDLGdCQUFrQyxFQUNsQyxhQUE0QixFQUM1QixTQUFxQixFQUNyQix3QkFBaUQsRUFDakQsd0JBQTZCO0lBRTdCLElBQU0sR0FBRyxHQUFtQixZQUFFLENBQUMsV0FBVyxHQUFHLENBQUMsWUFBRSxDQUFDLFdBQVcsSUFBSSxFQUFvQixDQUFDLENBQUM7SUFDdEYsR0FBRyxDQUFDLFlBQVksR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssWUFBWSxDQUFDLENBQUM7SUFDL0MsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7SUFFckQsV0FBVyxFQUFFLENBQUM7SUFDZCxhQUFhLEVBQUUsQ0FBQztJQUVoQixJQUFNLGNBQWMsR0FBOEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3BHLGNBQWMsQ0FBQyxVQUFVLEdBQUcsbUJBQW1CLENBQUMsUUFBUSxDQUFDO0lBQ3pELGNBQWMsQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsU0FBdUIsQ0FBQztJQUVqRSx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7SUFDMUUsd0JBQXdCLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBRXhFLDBCQUEwQjtJQUUxQjtRQUNJLFNBQVMsQ0FBQyxXQUFXLEdBQUcsWUFBRSxDQUFDLFdBQVcsQ0FBQztRQUN2QyxTQUFTLENBQUMsTUFBTSxHQUFHLFlBQUUsQ0FBQyxNQUFNLENBQUM7UUFDN0IsU0FBUyxDQUFDLElBQUksR0FBRyxZQUFFLENBQUMsSUFBSSxDQUFDO1FBQ3pCLFNBQVMsQ0FBQyxPQUFPLEdBQUcsWUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsWUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ2pELFNBQVMsQ0FBQyxVQUFVLEdBQUcsRUFBQyxVQUFVLEVBQUUsQ0FBQyxFQUFDLENBQUM7UUFDdkMsU0FBUyxDQUFDLGNBQWMsR0FBRyxZQUFFLENBQUMsY0FBYyxDQUFDO0lBQ2pELENBQUM7SUFFRDtRQUNJLGFBQWEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDdEQsYUFBYSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUV2RCxFQUFFLENBQUMsQ0FBQyxZQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ25DLGFBQWEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDekQsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFDakMsYUFBYSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUMzRCxDQUFDO1FBRUQsdUVBQXVFO1FBQ3ZFLGFBQWEsQ0FBQyxRQUFRLENBQUMsZUFBZSxHQUFHLDRCQUE0QixDQUFDO1FBRXRFLHdCQUF3QixDQUFDLFdBQVcsQ0FBQyxFQUFDLFdBQVcsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxZQUFZO0FBQ2hCLENBQUM7QUFFRCxnQkFBZ0I7QUFDaEIsYUFDSSxVQUEwQixFQUMxQixJQUFpQixFQUNqQixNQUFnQyxFQUNoQyxPQUF1QixFQUN2Qix3QkFBNkIsRUFDN0IsYUFBMkIsRUFDM0IsMkJBQXNELEVBQ3RELCtCQUErRDtJQUUvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxZQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRWhELGNBQ0ksS0FBb0IsRUFDcEIsT0FBNEIsRUFDNUIsUUFBYSxFQUNiLFNBQThCLEVBQzlCLFVBQWU7UUFFZixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzdDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUMzQixDQUFDO0lBQ0wsQ0FBQztJQUVEO1FBQ0ksSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixrQkFBa0IsRUFBRSxjQUFRLDJCQUEyQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkUsYUFBYSxFQUFFLGNBQVEsMkJBQTJCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRSxxQkFBcUIsRUFBRSxjQUFRLDJCQUEyQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0UsZ0JBQWdCLEVBQUUsY0FBUSwyQkFBMkIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFFLHlCQUF5QixFQUFFLGNBQVEsMkJBQTJCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyRixvQkFBb0IsRUFBRSxjQUFRLDJCQUEyQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEYseUJBQXlCLEVBQUUsY0FBUSwyQkFBMkIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JGLG9CQUFvQixFQUFFLGNBQVEsMkJBQTJCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoRixxQkFBcUIsRUFBRSxjQUFRLDJCQUEyQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0UsZ0JBQWdCLEVBQUUsY0FBUSwyQkFBMkIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzNFLENBQUM7UUFDRixVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxFQUFnQixDQUFDO1FBQzVELEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNuQyxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsZ0JBQWdCLENBQUM7UUFDdkQsQ0FBQztRQUNELElBQU0sT0FBTyxHQUFHO1lBQ1osVUFBVSxFQUFFLFVBQUMsU0FBa0IsSUFBTywrQkFBK0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzNGLENBQUM7UUFDRixVQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssSUFBSSxFQUFZLENBQUM7UUFDMUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzNCLFVBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFRDtRQUNJLHdDQUF3QztRQUN4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxtRUFBbUU7UUFDbkUsSUFBTSxhQUFhLEdBQUcsQ0FBUSxPQUFPLENBQUMsU0FBVSxDQUFDLFlBQVksSUFBSSxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQzthQUN2RixXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUUxQixxQ0FBcUM7UUFDckMsSUFBTSxXQUFXLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUVsRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUVELElBQU0sY0FBYyxHQUFVLE1BQU8sQ0FBQyxPQUFPLEVBQWMsQ0FBQztRQUU1RCxrQ0FBa0M7UUFDbEMsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxJQUFJLENBQUMsK0JBQTZCLFdBQVcsQ0FBQyxDQUFDLENBQUcsQ0FBQyxDQUFDO1lBQ3pELE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsQ0FBQztJQUNMLENBQUM7SUFFRCxVQUFVLENBQUMsRUFBRSxHQUFHLFlBQUUsQ0FBQztJQUNuQixVQUFVLENBQUMsUUFBUSxHQUFHLFlBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO0lBQzlDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsd0JBQXdCLENBQUM7SUFDbEQsVUFBVSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUUxQyxrQkFBa0IsRUFBRSxDQUFDO0lBQ3JCLHFCQUFxQixFQUFFLENBQUM7QUFDNUIsQ0FBQztBQUVELGtCQUFlLFVBQUMsTUFBa0I7SUFDOUIsSUFBTSxTQUFTLEdBQUc7UUFDZCxvQkFBb0IsRUFBRSxLQUFLO0tBQzlCLENBQUM7SUFFRixNQUFNLENBQUMsR0FBRyxFQUFFO1NBQ1AsUUFBUSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUM7U0FDaEMsUUFBUSxDQUFDLDBCQUEwQixFQUFFLGFBQWEsQ0FBQztTQUNuRCxNQUFNLENBQUMsTUFBTSxDQUFDO1NBQ2QsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsNERBQTREO0FBQy9FLENBQUMsQ0FBQyJ9

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(65);
    req.keys().forEach(function (r) {
        var key = "orion" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidGVtcGxhdGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxrQ0FBa0M7O0FBSWxDOzs7O0dBSUc7QUFDSCxtQkFBbUIsY0FBd0M7SUFDdkQsSUFBTSxHQUFHLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLDJCQUEyQixDQUFDLENBQUM7SUFDckUsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQVM7UUFDekIsSUFBTSxHQUFHLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLGNBQWMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xDLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUVELGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQ2hDLENBQUMsQ0FBQyJ9

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/errorInspector/errorInspector-component.html": 8,
	"./components/megaMenu/dashboardEditPanel/dashboardEditPanel-component.html": 9,
	"./components/megaMenu/evaluationBanner/evaluationBanner-component.html": 10,
	"./components/megaMenu/headerLogo/headerLogo-component.html": 11,
	"./components/megaMenu/helpLink/helpLink-component.html": 12,
	"./components/megaMenu/megaMenu-component.html": 13,
	"./components/megaMenu/navigationMenu/navigationMenu-component.html": 14,
	"./components/megaMenu/notifications/notifications-component.html": 15,
	"./components/megaMenu/orionSearch/orionSearch-component.html": 16,
	"./components/megaMenu/userInfo/userInfo-component.html": 17,
	"./dialogs/apiError/apiError.html": 18,
	"./dialogs/authExpireDialog/authExpireDialog.html": 19,
	"./dialogs/fieldPickerDialog/fieldPicker-dialog.html": 20,
	"./dialogs/menuError/menuError.html": 21,
	"./dialogs/orionImprovement/orionImprovementDialog.html": 22,
	"./dialogs/salesTrigger/salesTriggerDialog.html": 23,
	"./directives/dashboardMenu/dashboardMenu.html": 66,
	"./directives/entityListItem/entityListItem-directive.html": 67,
	"./directives/entityManagementNewPageMessage/entityManagementNewPageMessage-directive.html": 68,
	"./directives/entityManagementNewPageSwitcher/entityManagementNewPageSwitcher-directive.html": 69,
	"./directives/entityPopover/entityPopover-directive.html": 24,
	"./directives/entityPopover/entityPopover-item.html": 25,
	"./directives/entityPopover/entityPopover-linkItem-template.html": 26,
	"./directives/entityPopover/property/entityPopoverProperty-directive.html": 27,
	"./directives/entityPopoverLoader/example/entityPopoverLoader-directive.html": 70,
	"./directives/error/error-directive.html": 71,
	"./directives/licenseActivation/example/licenseActivation-directive.html": 72,
	"./directives/licenseActivation/licenseActivation-directive.html": 28,
	"./directives/licenseActivation/licenseActivationModal-template.html": 1,
	"./directives/mainNav/mainNav-directive.html": 73,
	"./directives/mainNav/mainNavMenu-directive.html": 74,
	"./directives/newPageMessage/newPageMessage-directive.html": 75,
	"./directives/newPageSwitcher/newPageSwitcher-directive.html": 76,
	"./directives/orionWebsite/orionWebsite-directive.html": 77,
	"./directives/search/search-directive.html": 78,
	"./views/dashboard/dashboard.html": 79,
	"./views/developer/developer.html": 29,
	"./views/developer/dialogs/resources-dialog.html": 30,
	"./views/developer/entityPopover/entityPopover.html": 31,
	"./views/developer/htmlElements/html-elements.html": 32,
	"./views/i18nDemo/i18nDemo.html": 33,
	"./views/orionError/orionError.html": 34,
	"./views/subview/baseSubview.html": 35,
	"./views/subview/subview.html": 36
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 65;

/***/ }),
/* 66 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-dashboard__menu-container> <div class=sw-dashboard__menu> <ul class=sw-dashboard__menu-items ng-transclude></ul> </div> </div> ";

/***/ }),
/* 67 */
/***/ (function(module, exports) {

module.exports = "<div class=entity-list-item> <div class=entity-list-item__container> <div class=entity-list-item__top-container> <div> <a ng-href={{vm.entity.detailsUrl}}> <span class=entity-list-item__icon> <xui-icon icon=\"{{::vm.entity.instanceType | swEntityIcon}}\" status=\"{{::vm.entity.status | swStatus: vm.entity.childStatus}}\"> </xui-icon> </span> <span ng-if=vm.getEntityTitle() class=entity-list-item__title ng-bind-html=\"vm.getEntityTitle() | xuiHighlight: vm.searchTerm\"> </span> </a> </div> <div class=entity-list-item__marks> <div ng-if=value class=\"entity-list-item__custom-property xui-tag\" ng-repeat=\"value in vm.getPropsValues(vm.customProps) track by $index\"> <span>{{value}}</span> </div> </div> </div> <div class=entity-list-item__bottom-container> <div class=entity-list-item__parent-node ng-if=\"vm.entity['node.displayName'] && vm.entity['node.detailsUrl']\"> <span _t>on</span> <a ng-href=\"{{vm.entity['node.detailsUrl']}}\">{{vm.entity['node.displayName']}}</a> </div> <div class=entity-list-item__ip-address ng-if=vm.entity.ipAddress> <div ng-bind-html=\"vm.entity.ipAddress | xuiHighlight: vm.searchTerm\"></div> </div> <div class=entity-list-item__vendor> <sw-vendor-icon ng-if=vm.entity.vendorIcon vendor={{vm.entity.vendorIcon}} title={{vm.entity.vendor}}></sw-vendor-icon> <div class=entity-list-item__vendor-name ng-bind-html=vm.entity.machineType></div> </div> <div ng-if=value class=entity-list-item__system-property ng-repeat=\"value in vm.getPropsValues(vm.systemProps) track by $index\"> {{value}} </div> </div> </div> </div> ";

/***/ }),
/* 68 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-new-page-message> <xui-message type=info allow-dismiss=true on-dismiss=vm.onDismissInternal()> {{::vm.msgText}} &nbsp; <sw-entity-management-new-page-switcher link-icon=double-caret-right link-text={{::vm.linkText}}></sw-entity-management-new-page-switcher> </xui-message> </div> ";

/***/ }),
/* 69 */
/***/ (function(module, exports) {

module.exports = "<span class=sw-new-page-switcher> <xui-button size=small display-style=link icon={{::vm.linkIcon}} ng-click=vm.switch()>{{ ::(vm.linkText || vm.linkDefaultText) }}</xui-button> </span> ";

/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports = "<div class=row sw-entity-popover-loader> <div class=col-md-1 id=noPopover> <xui-icon icon='{{ \"Orion.Interfaces\" | swEntityIcon }}' status=small></xui-icon> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <a id=netObjectPopover href=\"#NetObject=N:1\">Node (NetObject href)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <a id=netObjectPopoverNoTipUrl href=\"#NetObject=N:2&amp;NoTip\">Node (NetObject href, url NoTip)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <a id=netObjectPopoverNoTip href=\"#NetObject=N:3\" class=NoTip>Node (NetObject href, class NoTip)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <a id=netObjectPopoverMatchingLocation href=\"#NetObject=N:100\">Node (NetObject href, matching location)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <span id=netObjectAttrPopover netobject=N:1>Node (NetObject attr)</span> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <span id=netObjectAttrPopoverNoTip netobject=N:2 class=NoTip>Node (NetObject attr, class NoTip)</span> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <a id=netObjectAttrPopoverMatchingLocation netobject=N:100>Node (NetObject href, matching location)</a> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <span id=markerPopover opid=0_Orion.Nodes_2>Node (markers)</span> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> <span id=markerPopoverNoTip opid=0_Orion.Nodes_2 class=NoTip>Node (markers, class NoTip)</span> </div> <div class=col-md-1> <xui-icon icon='{{ \"Orion.Volumes\" | swEntityIcon }}' status=small></xui-icon> <a id=netObjectDisabledPopover href=\"#NetObject=V:1\">Volume</a> </div> <div class=col-md-1> <map name=map1> <area href=\"#NetObject=N:4\" shape=rect coords=0,0,400,400> </map> <img id=netObjectPopowerArea usemap=#map1 src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZEAAAGRAQMAAACe7xwUAAAAA1BMVEXAwMBmS75kAAAAKklEQVR4Xu3AAQ0AAADCIPuntscHCwIAAAAAAAAAAAAAAAAAAAAAAAAAOFF0AAGuMoA8AAAAAElFTkSuQmCC /> <xui-icon icon='{{ \"Orion.Nodes\" | swEntityIcon }}' status=small></xui-icon> </div> </div> ";

/***/ }),
/* 71 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-orion-error-page> <xui-page-content page-title=\"\" ng-cloak> <h1 class=sw-orion-error-page__summary-header>{{vm.headerText}}</h1> <div class=sw-orion-error-page__summary-message>{{vm.errorReport.message}}</div> <div class=sw-orion-error-page__image-backdrop> <div class=sw-orion-error-page__image ng-class=vm.imageClass></div> </div> <div class=sw-orion-error-page__button> <xui-button display-style=primary ng-click=vm.returnToHome() _t>Back To Orion Summary</xui-button> <span class=sw-orion-error-page__button-spacer></span> <xui-button display-style=secondary ng-click=vm.showHideDetails()>{{vm.detailsButtonText}}</xui-button> <span class=sw-orion-error-page__button-spacer></span> <xui-button display-style=secondary xui-clipboard=vm.errorStringified icon=copy _t>Copy Error Report </xui-button> </div> <div uib-collapse=!vm.isDetailsOpen class=sw-orion-error-page__details> <form> <xui-textbox rows=6 is-read-only=true ng-model=vm.errorStringified></xui-textbox> </form> </div> <div ng-include=vm.footerTemplateUrl></div> <div class=sw-orion-error-page__footer> <span _t>For more troubleshooting help see</span> <a target=_blank rel=\"noopener noreferrer\" href=https://knowledgebase.solarwinds.com/kb/ _t>SolarWinds Knowledge Base</a> <span _t>or</span> <a target=_blank rel=\"noopener noreferrer\" href=https://www.solarwinds.com/support/ _t>Contact Support</a> </div> </xui-page-content> </div> ";

/***/ }),
/* 72 */
/***/ (function(module, exports) {

module.exports = "<div id=license-activation-e2e> <h3>Activation Wizard</h3> <form name=license-activation-form> <div class=flex-end-row__container> <p class=flex-row__item> <xui-message type=warning> For demo purposes you can find <b>test Activation Key <a ng-href={{::viewModel.demoFeaturesConfluencePage}} target=_blank>here</a></b><br/> Don't forget to <b>deactivate test license</b> (with infinite expiration date) at <a href=licensing/license-manager target=_self>License Manager's Page</a> </xui-message> </p> </div> <xui-radio ng-model=viewModel.connectionType value=online>Online</xui-radio> <xui-radio name=offline ng-model=viewModel.connectionType value=offline>Offline</xui-radio> <xui-button icon=add sw-license-activation-modal mode=viewModel.connectionType selected-license=viewModel.license dialog-title=viewModel.activationTitle on-activate=viewModel.onLicenseActivate(result)> <span>License Activation</span> </xui-button> </form> </div> ";

/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = "<div id=mega-tabs> <ul> <sw-main-nav-menu ng-repeat=\"viewGroup in nav.viewGroups\" menu=viewGroup></sw-main-nav-menu> </ul> </div>";

/***/ }),
/* 74 */
/***/ (function(module, exports) {

module.exports = "<li class=\"animated fadeIn\" ng-show=vm.menu> <a id=mega-menu-{{::vm.menu.name}} href=#><span>{{::vm.menu.title}}</span><span class=mega-menu-caret></span></a> <div class=\"mega-submenu {{::vm.menu.isDashboards ? 'full-menu' : ''}}\"> <div class=mega-submenu-container> <ul ng-switch=vm.menu.isDashboards> <li ng-switch-when=false ng-repeat=\"menuItem in ::vm.menu.views\"> <a id={{::vm.menu.name}}-item-{{$index}} href={{::menuItem.url}}>{{::menuItem.title}}</a> </li> <li ng-switch-when=true ng-repeat=\"group in ::vm.parentNavController.dashboardGroups\" ng-if=\"group.views && group.views.length\"> <h3> <button><span class=mega-menu-caret></span><span class=category-text>{{::group.title}}</span></button> </h3> <div class=sub-menu-container> <h3 id=mega-menu-{{::group.name}} class=sub-menu-title><a href={{::group.views[0].url}}>{{::group.title}}</a> </h3> <ul class=mega-menu-dash> <li ng-repeat=\"view in ::group.views\"> <a id=\"{{::group.name | lowercase}}-item-{{$index}}\" href={{::view.url}} target=_self>{{::view.title}}</a> </li> </ul> </div> </li> </ul> </div> <div id=mega-menu-config class=mega-menu-config sw-auth sw-auth-roles=admin> <a href=/Orion/Admin/SelectMenuBar.aspx><span _t>Configure</span></a> <button class=menu-style-toggle> <i class=icon></i><span class=primary-text _t>Collapse</span><span class=secondary-text _t>Expand</span> </button> </div> </div> </li> ";

/***/ }),
/* 75 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-new-page-message> <xui-message type=info allow-dismiss=true on-dismiss=vm.onDismissInternal()> {{::vm.messageText}} &nbsp; <sw-new-page-switcher link-icon=double-caret-right link-text={{::vm.linkText}} link-address={{::vm.linkAddress}} message-text={{::vm.messageText}} dismiss-message-setting-name={{::vm.dismissMessageSettingName}} dismiss-message-setting-value={{::vm.dismissMessageSettingValue}} use-legacy-page-setting-name={{::vm.useLegacyPageSettingName}} use-legacy-page-setting-value={{::vm.useLegacyPageSettingValue}}> </sw-new-page-switcher> </xui-message> </div> ";

/***/ }),
/* 76 */
/***/ (function(module, exports) {

module.exports = "<span class=sw-new-page-switcher> <xui-button size=small display-style=link icon={{::vm.linkIcon}} ng-click=vm.switch()>{{ ::(vm.linkText || vm.linkDefaultText) }}</xui-button> </span> ";

/***/ }),
/* 77 */
/***/ (function(module, exports) {

module.exports = "<div id=page-wrapper class=xui-page-wrapper sw-entity-popover-loader> <sw-mega-menu ng-if=::shell.showNewMegamenu()></sw-mega-menu> <div id=content class=\"sw-content-area xui-content-area\" ng-class=\"{'sw-eval-mode': shell.websiteOptions.licenseSummary.evalModuleCount > 0 || shell.websiteOptions.licenseSummary.expiredModuleCount > 0}\"> <div class=\"sw-view xui-view\" ui-view> <div class=sw-mainview-loader> <div class=sw-mainview-loader__content> <i class=\"xui-icon xui-icon-name-busy-cube xui-spinner xui-spinner-64\"></i> <div class=sw-mainview-loader__title _t>Loading Orion Web Console...</div> </div> </div> </div> </div> <xui-footer class=sw-footer ng-if=::shell.websiteOptions.displayHeaderAndFooter> <div class=sw-footer__content> <a href=https://www.solarwinds.com target=_blank rel=\"noopener noreferrer\" class=sw-footer__logo> <img alt=SolarWinds height=19 width=77 sw-src=/orion/images/SolarWinds.Logo.Footer.svg /> </a> <span class=sw-footer__copyright>{{ ::shell.websiteOptions.footerOptions.copyright }}</span> </div> </xui-footer> </div> ";

/***/ }),
/* 78 */
/***/ (function(module, exports) {

module.exports = "<div class=\"sw-orion-search xui\" ng-class=\"{'sw-orion-search--opened': vm.isOpen}\"> <div class=sw-orion-search__menu-item uib-dropdown auto-close=outsideClick on-toggle=vm.onToggle(open)> <a href=# uib-dropdown-toggle class=\"dropdown-toggle sw-orion-search__menu-button\"> <xui-icon icon=search icon-color=\"{{vm.isOpen ? 'orange' : 'white'}}\" icon-hover-color=orange is-dynamic=true fill-container=true></xui-icon> </a> <div uib-dropdown-menu class=\"sw-orion-search__dropdown dropdown-menu animated-fast fadeIn dropdown-menu-right xui-padding-lg\"> <div class=\"sw-orion-search__dropdown-group xui-padding-lg\"> <xui-search on-search=vm.onSearch(value) placeholder=_t(Search...) value=vm.searchValue _ta></xui-search> <div class=\"sw-orion-search__search-history xui-padding-mdv\" ng-if=\"!vm.searchValue && vm.searchHistory.length\"> <div class=\"xui-text-s sw-orion-search__search-history-header xui-margin-smv\" _t>recent searches</div> <div class=\"sw-orion-search__search-history-item xui-text-r xui-padding-md\" ng-repeat=\"item in vm.searchHistory\" ng-click=vm.onSearch(item) tabindex=0> <span class=sw-orion-search__search-history-item-text>{{item}}</span> <xui-icon icon=caret-right></xui-icon> </div> </div> <div class=sw-orion-search__search-suggestions></div> </div> </div> </div> </div> ";

/***/ }),
/* 79 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title={{$dashboard.title}} page-layout=fullWidth class=sw-dashboard-page is-busy=$dashboard.isBusy> <sw-dashboard widgets=$dashboard.widgets></sw-dashboard> </xui-page-content> ";

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(81);
var index_2 = __webpack_require__(84);
var index_3 = __webpack_require__(86);
var fieldPickerDialog_1 = __webpack_require__(91);
__webpack_require__(94);
__webpack_require__(95);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    fieldPickerDialog_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGtEQUF3RDtBQUN4RCxrREFBOEQ7QUFDOUQsOENBQXNEO0FBQ3RELHlEQUFvRDtBQUVwRCxvQ0FBa0M7QUFDbEMsc0NBQW9DO0FBRXBDLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixlQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3pCLGVBQXNCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDL0IsZUFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMzQiwyQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM5QixDQUFDLENBQUMifQ==

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var authExpireDialog_controller_1 = __webpack_require__(37);
var authExpireDialog_1 = __webpack_require__(82);
var authExpireDialog_service_1 = __webpack_require__(39);
exports.default = function (module) {
    module.controller("swAuthExpireDialogController", authExpireDialog_controller_1.default);
    module.component("swAuthExpireDialog", authExpireDialog_1.default);
    module.service("swAuthExpireDialogService", authExpireDialog_service_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDZFQUF1RTtBQUN2RSx1REFBMkQ7QUFDM0QsdUVBQWlFO0FBRWpFLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsVUFBVSxDQUFDLDhCQUE4QixFQUFFLHFDQUEwQixDQUFDLENBQUM7SUFDOUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsRUFBRSwwQkFBeUIsQ0FBQyxDQUFDO0lBQ2xFLE1BQU0sQ0FBQyxPQUFPLENBQUMsMkJBQTJCLEVBQUUsa0NBQXVCLENBQUMsQ0FBQztBQUN6RSxDQUFDLENBQUMifQ==

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(83);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var authExpireDialog_controller_1 = __webpack_require__(37);
var AuthExpireDialogComponent = /** @class */ (function () {
    function AuthExpireDialogComponent() {
        this.template = __webpack_require__(19);
        this.controller = authExpireDialog_controller_1.default;
        this.controllerAs = "ctrl";
        this.bindToController = {
            secondsRemaining: "@"
        };
        this.link = function (scope, element, attrs, controller) {
            controller.startCountdown();
            scope.$on("$destroy", controller.stopCountdown);
        };
    }
    return AuthExpireDialogComponent;
}());
exports.default = AuthExpireDialogComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aEV4cGlyZURpYWxvZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImF1dGhFeHBpcmVEaWFsb2cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw2RUFBdUU7QUFFdkU7SUFBQTtRQUVXLGFBQVEsR0FBRyxPQUFPLENBQVMseUJBQXlCLENBQUMsQ0FBQztRQUN0RCxlQUFVLEdBQUcscUNBQTBCLENBQUM7UUFDeEMsaUJBQVksR0FBRyxNQUFNLENBQUM7UUFDdEIscUJBQWdCLEdBQUc7WUFDdEIsZ0JBQWdCLEVBQUUsR0FBRztTQUN4QixDQUFDO1FBQ0ssU0FBSSxHQUFHLFVBQ1YsS0FBZ0IsRUFDaEIsT0FBNEIsRUFDNUIsS0FBcUIsRUFDckIsVUFBc0M7WUFFdEMsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzVCLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUM7SUFDTixDQUFDO0lBQUQsZ0NBQUM7QUFBRCxDQUFDLEFBakJELElBaUJDIn0=

/***/ }),
/* 83 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionImprovementDialogService_1 = __webpack_require__(85);
exports.default = function (module) {
    module.service("swOrionImprovementDialogService", orionImprovementDialogService_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlGQUE0RTtBQUU1RSxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQ0FBaUMsRUFBRSx1Q0FBNkIsQ0FBQyxDQUFDO0FBQ3JGLENBQUMsQ0FBQyJ9

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionImprovementDialogService = /** @class */ (function () {
    function OrionImprovementDialogService(dialogService, _t, swApi, constants, $log) {
        var _this = this;
        this.dialogService = dialogService;
        this._t = _t;
        this.swApi = swApi;
        this.constants = constants;
        this.$log = $log;
        this.title = "";
        this.show = function (isOptInChecked) {
            var viewModel = {
                optIn: isOptInChecked,
                learnMoreUrl: "https://solarwinds.com/documentation/kbLoader.aspx?kb=4678&lang=" + _this.constants.locale,
                route: "../../Orion/OrionImprovement/Services/Oip.asmx",
                post: "ChangeOptIn"
            };
            return _this.dialogService.showModal({
                template: __webpack_require__(22),
            }, {
                title: _this._t("Orion Improvement Program"),
                hideCancel: false,
                cancelButtonText: _this._t("Cancel"),
                buttons: [{
                        name: "ok",
                        isPrimary: true,
                        text: _this._t("OK"),
                        action: function () {
                            _this.swApi.ws.one(viewModel.route).post(viewModel.post, { enabled: viewModel.optIn });
                            return true;
                        }
                    }],
                viewModel: viewModel
            });
        };
    }
    OrionImprovementDialogService.$inject = ["xuiDialogService",
        "getTextService",
        "swApi",
        "constants",
        "$log"];
    return OrionImprovementDialogService;
}());
exports.default = OrionImprovementDialogService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25JbXByb3ZlbWVudERpYWxvZ1NlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmlvbkltcHJvdmVtZW50RGlhbG9nU2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUl2QztJQVFJLHVDQUFvQixhQUFpQyxFQUN6QyxFQUFvQyxFQUNwQyxLQUFvQixFQUNwQixTQUFxQixFQUNyQixJQUFvQjtRQUpoQyxpQkFLQztRQUxtQixrQkFBYSxHQUFiLGFBQWEsQ0FBb0I7UUFDekMsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDcEMsVUFBSyxHQUFMLEtBQUssQ0FBZTtRQUNwQixjQUFTLEdBQVQsU0FBUyxDQUFZO1FBQ3JCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBWHhCLFVBQUssR0FBVyxFQUFFLENBQUM7UUFjcEIsU0FBSSxHQUFHLFVBQUMsY0FBdUI7WUFDbEMsSUFBSSxTQUFTLEdBQUc7Z0JBQ1osS0FBSyxFQUFFLGNBQWM7Z0JBQ3JCLFlBQVksRUFBRSxrRUFBa0UsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU07Z0JBQ3hHLEtBQUssRUFBRSxnREFBZ0Q7Z0JBQ3ZELElBQUksRUFBRSxhQUFhO2FBQ3RCLENBQUM7WUFDRixNQUFNLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7Z0JBQ2hDLFFBQVEsRUFBRSxPQUFPLENBQVMsK0JBQStCLENBQUM7YUFDN0QsRUFBRTtnQkFDQyxLQUFLLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztnQkFDM0MsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNuQyxPQUFPLEVBQUUsQ0FBQzt3QkFDTixJQUFJLEVBQUUsSUFBSTt3QkFDVixTQUFTLEVBQUUsSUFBSTt3QkFDZixJQUFJLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0JBQ25CLE1BQU0sRUFBRTs0QkFDSixLQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEVBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDOzRCQUNwRixNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNoQixDQUFDO3FCQUNKLENBQUM7Z0JBQ0YsU0FBUyxFQUFFLFNBQVM7YUFDdkIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFBO0lBMUJELENBQUM7SUFWYSxxQ0FBTyxHQUFHLENBQUMsa0JBQWtCO1FBQzNDLGdCQUFnQjtRQUNoQixPQUFPO1FBQ1AsV0FBVztRQUNYLE1BQU0sQ0FBQyxDQUFDO0lBaUNaLG9DQUFDO0NBQUEsQUF4Q0QsSUF3Q0M7a0JBeENvQiw2QkFBNkIifQ==

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var salesTriggerDescriptorService_1 = __webpack_require__(40);
var salesTriggerDialogController_1 = __webpack_require__(41);
var salesTriggerDialog_1 = __webpack_require__(87);
var salesTriggerDialogService_1 = __webpack_require__(89);
var EvalController_1 = __webpack_require__(90);
exports.default = function (module) {
    module.service("swSalesTriggerDescriptorService", salesTriggerDescriptorService_1.default);
    module.controller("swSalesTriggerDialogController", salesTriggerDialogController_1.default);
    module.controller("EvalController", EvalController_1.default);
    module.component("swSalesTriggerDialog", salesTriggerDialog_1.default);
    module.service("swSalesTriggerDialogService", salesTriggerDialogService_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlGQUE0RTtBQUM1RSwrRUFBMEU7QUFDMUUsMkRBQStEO0FBQy9ELHlFQUFvRTtBQUNwRSxtREFBOEM7QUFFOUMsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsaUNBQWlDLEVBQUUsdUNBQTZCLENBQUMsQ0FBQztJQUNqRixNQUFNLENBQUMsVUFBVSxDQUFDLGdDQUFnQyxFQUFFLHNDQUE0QixDQUFDLENBQUM7SUFDbEYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRSx3QkFBYyxDQUFDLENBQUM7SUFDcEQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsRUFBRSw0QkFBMkIsQ0FBQyxDQUFDO0lBQ3RFLE1BQU0sQ0FBQyxPQUFPLENBQUMsNkJBQTZCLEVBQUUsbUNBQXlCLENBQUMsQ0FBQztBQUM3RSxDQUFDLENBQUMifQ==

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(88);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var salesTriggerDialogController_1 = __webpack_require__(41);
var SalesTriggerDialogComponent = /** @class */ (function () {
    function SalesTriggerDialogComponent() {
        this.restrict = "E";
        this.template = __webpack_require__(23);
        this.controller = salesTriggerDialogController_1.default;
        this.controllerAs = "salesTriggerCtrl";
        this.bindToController = {
            type: "@"
        };
    }
    return SalesTriggerDialogComponent;
}());
exports.default = SalesTriggerDialogComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FsZXNUcmlnZ2VyRGlhbG9nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2FsZXNUcmlnZ2VyRGlhbG9nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDLCtFQUEwRTtBQUUxRTtJQUFBO1FBQ1csYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNmLGFBQVEsR0FBRyxPQUFPLENBQVMsMkJBQTJCLENBQUMsQ0FBQztRQUN4RCxlQUFVLEdBQUcsc0NBQTRCLENBQUM7UUFDMUMsaUJBQVksR0FBRyxrQkFBa0IsQ0FBQztRQUNsQyxxQkFBZ0IsR0FBRztZQUN0QixJQUFJLEVBQUUsR0FBRztTQUNaLENBQUM7SUFDTixDQUFDO0lBQUQsa0NBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQyJ9

/***/ }),
/* 88 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SalesTriggerDialogService = /** @class */ (function () {
    function SalesTriggerDialogService(dialogService, descriptorService, licenseService, $q, $log, _t) {
        var _this = this;
        this.dialogService = dialogService;
        this.descriptorService = descriptorService;
        this.licenseService = licenseService;
        this.$q = $q;
        this.$log = $log;
        this._t = _t;
        this.title = "";
        this.show = function (type) {
            return _this.resolveTitle(type).then(function (title) {
                return _this.dialogService.showModal({
                    template: "<sw-sales-trigger-dialog type='" + type + "'>\n                            </sw-sales-trigger-dialog>",
                    windowClass: "sw-salestrigger-dialog"
                }, {
                    title: title,
                    hideCancel: true
                });
            });
        };
        this.resolveTitle = function (type) {
            return _this.$q(function ($resolve) {
                var resolve = function () { return $resolve(_this.descriptorService.getTitle(type)); };
                if (type === "Maintenance") {
                    _this.licenseService.hasMaintenanceExpired()
                        .then(function (hasExpired) {
                        if (hasExpired) {
                            _this.descriptorService.setTitle("Maintenance", _this._t("Maintenance Has Expired!"));
                        }
                        resolve();
                    });
                }
                else if (type === "Upgrade") {
                    var title = _this.licenseService.getUpgradeSummary()
                        .then(function (summary) {
                        _this.descriptorService.setTitle(type, summary.title);
                        resolve();
                    });
                }
                else {
                    resolve();
                }
            });
        };
    }
    SalesTriggerDialogService.$inject = [
        "xuiDialogService",
        "swSalesTriggerDescriptorService",
        "swLicenseService",
        "$q",
        "$log",
        "getTextService"
    ];
    return SalesTriggerDialogService;
}());
exports.default = SalesTriggerDialogService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FsZXNUcmlnZ2VyRGlhbG9nU2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNhbGVzVHJpZ2dlckRpYWxvZ1NlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFRdkM7SUFZSSxtQ0FBb0IsYUFBaUMsRUFDekMsaUJBQWdELEVBQ2hELGNBQStCLEVBQy9CLEVBQWdCLEVBQ2hCLElBQW9CLEVBQ3BCLEVBQW9DO1FBTGhELGlCQU1DO1FBTm1CLGtCQUFhLEdBQWIsYUFBYSxDQUFvQjtRQUN6QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQStCO1FBQ2hELG1CQUFjLEdBQWQsY0FBYyxDQUFpQjtRQUMvQixPQUFFLEdBQUYsRUFBRSxDQUFjO1FBQ2hCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBQ3BCLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBaEJ4QyxVQUFLLEdBQVcsRUFBRSxDQUFDO1FBbUJwQixTQUFJLEdBQUcsVUFBQyxJQUFzQjtZQUNqQyxNQUFNLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUFhO2dCQUM5QyxNQUFNLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7b0JBQ2hDLFFBQVEsRUFBRSxvQ0FBa0MsSUFBSSwrREFDVDtvQkFDdkMsV0FBVyxFQUFFLHdCQUF3QjtpQkFDeEMsRUFBRTtvQkFDSyxLQUFLLEVBQUUsS0FBSztvQkFDWixVQUFVLEVBQUUsSUFBSTtpQkFDbkIsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUE7UUFFTyxpQkFBWSxHQUFHLFVBQUMsSUFBc0I7WUFDMUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQVMsVUFBQyxRQUFvQztnQkFDeEQsSUFBTSxPQUFPLEdBQUcsY0FBTSxPQUFBLFFBQVEsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQS9DLENBQStDLENBQUM7Z0JBRXRFLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUN6QixLQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFO3lCQUN0QyxJQUFJLENBQUMsVUFBQyxVQUFtQjt3QkFDdEIsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzs0QkFDYixLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLDBCQUEwQixDQUFDLENBQUUsQ0FBQzt3QkFDekYsQ0FBQzt3QkFDRCxPQUFPLEVBQUUsQ0FBQztvQkFDZCxDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDO2dCQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDNUIsSUFBTSxLQUFLLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsRUFBRTt5QkFDaEQsSUFBSSxDQUFDLFVBQUMsT0FBd0I7d0JBQzNCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckQsT0FBTyxFQUFFLENBQUM7b0JBQ2QsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixPQUFPLEVBQUUsQ0FBQztnQkFDZCxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7SUFyQ0YsQ0FBQztJQWZhLGlDQUFPLEdBQUc7UUFDcEIsa0JBQWtCO1FBQ2xCLGlDQUFpQztRQUNqQyxrQkFBa0I7UUFDbEIsSUFBSTtRQUNKLE1BQU07UUFDTixnQkFBZ0I7S0FDbkIsQ0FBQztJQThDTixnQ0FBQztDQUFBLEFBeERELElBd0RDO2tCQXhEb0IseUJBQXlCIn0=

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var EvalController = /** @class */ (function () {
    function EvalController(salesTriggerDialogService) {
        var _this = this;
        this.salesTriggerDialogService = salesTriggerDialogService;
        this.showEvalDetails = function () {
            _this.salesTriggerDialogService.show("Eval");
        };
    }
    EvalController.$inject = ["swSalesTriggerDialogService"];
    return EvalController;
}());
exports.default = EvalController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRXZhbENvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJFdmFsQ29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QztJQUdJLHdCQUFxQix5QkFBOEQ7UUFBbkYsaUJBQ0M7UUFEb0IsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUFxQztRQUc1RSxvQkFBZSxHQUFHO1lBQ3JCLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFBO0lBSkQsQ0FBQztJQUhhLHNCQUFPLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO0lBUTVELHFCQUFDO0NBQUEsQUFURCxJQVNDO2tCQVRvQixjQUFjIn0=

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(92);
var fieldPicker_service_1 = __webpack_require__(93);
exports.default = function (module) {
    module.service("swFieldPickerService", fieldPicker_service_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFDQUFtQztBQUNuQyw2REFBdUQ7QUFFdkQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsNkJBQWtCLENBQUMsQ0FBQztBQUMvRCxDQUFDLENBQUMifQ==

/***/ }),
/* 92 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var FieldPickerService = /** @class */ (function () {
    function FieldPickerService($q, getTextService, dialogService, 
        //Interfaces for TileStackService, groups, fields will be created in scope of UIF-2804
        tileStackService) {
        var _this = this;
        this.$q = $q;
        this.getTextService = getTextService;
        this.dialogService = dialogService;
        this.tileStackService = tileStackService;
        this.events = {};
        this.version = "1.0";
        this.defaultOptions = {
            title: this.getTextService("Select Fields"),
            selectedGroup: "Orion.Nodes",
            selectedFields: []
        };
        this.showDialog = function (options) {
            options = angular.extend(_this.defaultOptions, options);
            _this.selectedFields = options.selectedFields;
            var vm = {
                isBusy: true,
                groups: [],
                selectedGroup: null,
                selectGroup: _this.selectGroup,
                search: {
                    focusInput: true,
                    target: {
                        ItemCaption: ""
                    },
                    reset: function () {
                        this.focusInput = true;
                        this.target.ItemCaption = "";
                    }
                }
            };
            var deferred = _this.$q.defer();
            _this.tileStackService.getGroups().then(function (result) {
                vm.groups = result.rows;
                var startGroup = _.find(result.rows, { GroupID: options.selectedGroup });
                if (!startGroup) {
                    startGroup = result.rows[0];
                }
                _this.selectGroup(startGroup).then(function (group) {
                    vm.selectedGroup = group;
                    _this.dialogService
                        .showModal({
                        template: __webpack_require__(20)
                    }, {
                        headerText: options.title,
                        viewModel: vm,
                        showClose: true
                    }).then(function () {
                        deferred.resolve(_.filter(this.selectedGroup.Properties, "IsSelected"));
                    }).catch(function () {
                        deferred.reject();
                    });
                });
            });
            return deferred.promise;
        };
        this.selectGroup = function (group) {
            if (!group) {
                return _this.$q.when(null);
            }
            return _this.tileStackService.getGroupProperties(group).then(function (properties) {
                _.each(group.Properties, function (p) {
                    p.IsSelected = false;
                });
                if (_this.selectedFields) {
                    _.each(_this.selectedFields, function (field) {
                        var gf = _.find(group.Properties, { ItemID: field });
                        if (gf) {
                            gf.IsSelected = true;
                        }
                    });
                }
                _this.selectedGroup = group;
                return _this.selectedGroup;
            });
        };
    }
    FieldPickerService.$inject = ["$q", "getTextService", "xuiDialogService", "tileStackService"];
    return FieldPickerService;
}());
exports.default = FieldPickerService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGRQaWNrZXItc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZpZWxkUGlja2VyLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFPdkM7SUFjSSw0QkFDWSxFQUFhLEVBQ2IsY0FBd0MsRUFDeEMsYUFBNkI7UUFDckMsc0ZBQXNGO1FBQzlFLGdCQUFtQztRQUwvQyxpQkFNSTtRQUxRLE9BQUUsR0FBRixFQUFFLENBQVc7UUFDYixtQkFBYyxHQUFkLGNBQWMsQ0FBMEI7UUFDeEMsa0JBQWEsR0FBYixhQUFhLENBQWdCO1FBRTdCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBbUI7UUFoQnZDLFdBQU0sR0FBUSxFQUFFLENBQUM7UUFDakIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUloQixtQkFBYyxHQUFRO1lBQzFCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQztZQUMzQyxhQUFhLEVBQUUsYUFBYTtZQUM1QixjQUFjLEVBQUUsRUFBRTtTQUNyQixDQUFDO1FBVUssZUFBVSxHQUFHLFVBQUMsT0FBWTtZQUM3QixPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELEtBQUksQ0FBQyxjQUFjLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQztZQUU3QyxJQUFNLEVBQUUsR0FBUTtnQkFDWixNQUFNLEVBQUUsSUFBSTtnQkFDWixNQUFNLEVBQUUsRUFBRTtnQkFDVixhQUFhLEVBQUUsSUFBSTtnQkFDbkIsV0FBVyxFQUFFLEtBQUksQ0FBQyxXQUFXO2dCQUM3QixNQUFNLEVBQUU7b0JBQ0osVUFBVSxFQUFFLElBQUk7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixXQUFXLEVBQUUsRUFBRTtxQkFDbEI7b0JBQ0QsS0FBSyxFQUFFO3dCQUNILElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO3dCQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7b0JBQ2pDLENBQUM7aUJBQ0o7YUFDSixDQUFDO1lBRUYsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUVqQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBVztnQkFDL0MsRUFBRSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUN4QixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLGFBQWEsRUFBQyxDQUFDLENBQUM7Z0JBQ3ZFLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDZCxVQUFVLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaEMsQ0FBQztnQkFFRCxLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQVU7b0JBQ3pDLEVBQUUsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO29CQUV6QixLQUFJLENBQUMsYUFBYTt5QkFDYixTQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLE9BQU8sQ0FBUywyQkFBMkIsQ0FBQztxQkFDekQsRUFBUTt3QkFDTCxVQUFVLEVBQUUsT0FBTyxDQUFDLEtBQUs7d0JBQ3pCLFNBQVMsRUFBRSxFQUFFO3dCQUNiLFNBQVMsRUFBRSxJQUFJO3FCQUNsQixDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUNSLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7d0JBQ0wsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUN0QixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1lBRUgsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRUssZ0JBQVcsR0FBRyxVQUFDLEtBQVU7WUFDNUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNULE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QixDQUFDO1lBRUQsTUFBTSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxVQUFlO2dCQUN4RSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDO29CQUNoQyxDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDekIsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQ3RCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxVQUFDLEtBQVU7d0JBQ25DLElBQUksRUFBRSxHQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUMsQ0FBQyxDQUFDO3dCQUN4RCxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDOzRCQUNMLEVBQUUsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO3dCQUN6QixDQUFDO29CQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNQLENBQUM7Z0JBRUQsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBRTNCLE1BQU0sQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDO1lBQzlCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO0lBNUVDLENBQUM7SUFuQlUsMEJBQU8sR0FBa0IsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsa0JBQWtCLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztJQWdHNUcseUJBQUM7Q0FBQSxBQWpHRCxJQWlHQztrQkFqR29CLGtCQUFrQiJ9

/***/ }),
/* 94 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 95 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(42);
var index_1 = __webpack_require__(101);
var index_2 = __webpack_require__(103);
var index_3 = __webpack_require__(108);
var index_4 = __webpack_require__(110);
var index_5 = __webpack_require__(114);
var index_6 = __webpack_require__(42);
var index_7 = __webpack_require__(118);
var index_8 = __webpack_require__(122);
var index_9 = __webpack_require__(123);
var index_10 = __webpack_require__(128);
var index_11 = __webpack_require__(132);
var index_12 = __webpack_require__(134);
var index_13 = __webpack_require__(136);
var index_14 = __webpack_require__(139);
var index_15 = __webpack_require__(142);
var index_16 = __webpack_require__(145);
var index_17 = __webpack_require__(148);
var index_18 = __webpack_require__(151);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
    index_7.default(module);
    index_8.default(module);
    index_9.entityPopover(module);
    index_10.entityListItem(module);
    index_11.default(module);
    index_12.dashboardFunctions(module);
    index_13.default(module);
    index_14.default(module);
    index_15.default(module);
    index_16.default(module);
    index_17.default(module);
    index_18.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFCQUFtQjtBQUNuQiw0Q0FBNEM7QUFDNUMsK0NBQWtEO0FBQ2xELGlEQUFzRDtBQUN0RCx1Q0FBa0M7QUFDbEMsbURBQTBEO0FBQzFELHlDQUFzQztBQUN0Qyx3Q0FBb0M7QUFDcEMscURBQXdEO0FBQ3hELCtDQUFzRDtBQUN0RCxpREFBd0Q7QUFDeEQsMkNBQXdDO0FBQ3hDLHFEQUFnRTtBQUNoRSxpRUFBb0Y7QUFDcEYsa0VBQXNGO0FBQ3RGLGlEQUFvRDtBQUNwRCwrQ0FBZ0Q7QUFDaEQsaURBQW9EO0FBQ3BELGtEQUFzRDtBQUV0RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsZUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25CLGVBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN0QixlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEIsZUFBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2QsZUFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxQixlQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEIsZUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2YsZUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RCLHFCQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEIsdUJBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2QixnQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pCLDJCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLGdCQUE4QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLGdCQUErQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3hDLGdCQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkIsZ0JBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNyQixnQkFBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZCLGdCQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDNUIsQ0FBQyxDQUFDIn0=

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var mainNav_controller_1 = __webpack_require__(98);
var MainNav = /** @class */ (function () {
    function MainNav() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/mainNav/mainNav-directive.html";
        this.controller = mainNav_controller_1.default;
        this.transclude = false;
        this.replace = true;
        this.controllerAs = "nav";
        this.bindToController = {
            dashboardGroups: "<",
            viewGroups: "<"
        };
    }
    return MainNav;
}());
exports.default = MainNav;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbk5hdi1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtYWluTmF2LWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJEQUFxRDtBQUVyRDtJQUFBO1FBQ1csYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNmLGdCQUFXLEdBQUcsaURBQWlELENBQUM7UUFDaEUsZUFBVSxHQUFHLDRCQUFpQixDQUFDO1FBQy9CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLHFCQUFnQixHQUFHO1lBQ3RCLGVBQWUsRUFBRSxHQUFHO1lBQ3BCLFVBQVUsRUFBRSxHQUFHO1NBQ2xCLENBQUM7SUFDTixDQUFDO0lBQUQsY0FBQztBQUFELENBQUMsQUFYRCxJQVdDIn0=

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var MainNavController = /** @class */ (function () {
    /** @ngInject */
    MainNavController.$inject = ["$timeout", "swApi"];
    function MainNavController($timeout, swApi) {
        this.$timeout = $timeout;
        this.swApi = swApi;
        this.hasDashboards = false;
        //disable the linter, because this huge object is mirrored in legacy code, and we don't want them diverging
        /* tslint:disable */
        this.MegaMenu = {
            init: function (swApi) {
                var $header = jQuery('.sw-page-header'), $menu = $header.find('#mega-tabs .mega-submenu.full-menu'), $menuList = $menu.find('.mega-submenu-container > ul'), $menuItems = $menuList.find('> li'), $menuToggle = $menu.find('.mega-menu-config > .menu-style-toggle'), $evalBar = jQuery('#evalMsg'), $form = jQuery('#content'), $navScrollContainer = jQuery('#swNavScroll'), $userSection = jQuery('#userName'), minWidth = 1024, // minimum supported width
                forceScroll = false, maxScroll = 0, menus = { primary: 'menu-primary', secondary: 'menu-secondary' }, menuPinned = false, timeout, openTime, resizeThrottle, scrollThrottle;
                // setup a smartresize function that debounces the resize events
                function smartResizeHandler() {
                    if (window.innerWidth < 1024) {
                        maxScroll = Math.abs(window.innerWidth - minWidth);
                        $userSection.css('right', (-1 * maxScroll) + 'px');
                        forceScroll = true;
                    }
                    else {
                        $userSection.css('right', '0');
                        forceScroll = false;
                    }
                }
                // anonymous function handles throttled resize events
                resizeThrottle = _.throttle(function (e) {
                    smartResizeHandler(); // call the actual handler
                }, 100);
                // bind the resize event handler
                window.addEventListener('resize', resizeThrottle, false);
                smartResizeHandler(); // trigger this once on page load
                // same thing for scrolling
                function smartScrollHandler() {
                    if (!forceScroll) {
                        return;
                    }
                    if (window.pageXOffset > 0) {
                        var offset = -1 * Math.min(maxScroll, window.pageXOffset);
                        $navScrollContainer.css('left', offset + 'px');
                    }
                    else {
                        $navScrollContainer.css('left', '0');
                    }
                }
                scrollThrottle = _.throttle(function (e) {
                    smartScrollHandler();
                }, 25);
                window.addEventListener('scroll', scrollThrottle, false);
                smartScrollHandler();
                // append eval mode class to $form if eval message is visible
                if ($evalBar.children().length > 0) {
                    $form.addClass('sw-eval-mode');
                }
                // jQuery menu-aim plugin for handling mouse movement over the compact menu.
                // https://github.com/kamens/jQuery-menu-aim
                $menuList.menuAim({
                    activate: function (row) {
                        jQuery(row).addClass('menu-active');
                    },
                    deactivate: function (row) {
                        jQuery(row).removeClass('menu-active');
                    },
                    exitMenu: function () {
                        return false;
                    }
                });
                // Helper function to handle user interaction outside a given element.
                function clickOutside($element) {
                    var deferred = jQuery.Deferred();
                    var $body = jQuery('body');
                    $body
                        .on('click', function (e) {
                        if (jQuery(e.target).closest($element).length === 0) {
                            $body.off('click');
                            deferred.resolve();
                            return false;
                        }
                    })
                        .on('keydown', function (e) {
                        // Allow menus to be closed with the escape key.
                        if (e.which === 27) {
                            $body.off('keydown');
                            deferred.resolve();
                            return false;
                        }
                    });
                    return deferred.promise();
                }
                $menu.addClass(SW.user.menuStyle);
                function setHeights() {
                    // Set the maximum height of the menu based on the window height.
                    $menuList = $menu.find('.mega-submenu-container > ul');
                    $menuItems = $menuList.find('> li');
                    var windowHeight = jQuery(window).height();
                    var menuHeight = Math.round(windowHeight * 0.7);
                    $menuList.css('max-height', menuHeight);
                    if (MenuSelector.isMenuStyleSelected(menus.secondary)) {
                        $menuItems.css('height', 'auto');
                    }
                    else {
                        // Align the heights of the rows to the list with the largest height.
                        var columns = Math.floor($menuList.width() / $menuItems.width());
                        if (columns == null || columns < 2)
                            return;
                        for (var i = 0, j = $menuItems.length; i < j; i += columns) {
                            var maxHeight = 0, $rows = $menuItems.slice(i, i + columns);
                            $rows.each(function () {
                                var itemHeight = jQuery(this).outerHeight();
                                if (itemHeight > maxHeight)
                                    maxHeight = itemHeight;
                            });
                            $rows.css('height', maxHeight);
                        }
                    }
                }
                var MenuSelector = {
                    init: function () {
                        $menuToggle.click(function (e) {
                            e.preventDefault();
                            MenuSelector.toggleMenu();
                        });
                        // If the module count is 1 then use the expanded version and hide the toggle,
                        // greater than 4 then we want to make the secondary menu the default.
                        var moduleCount = $menuItems.length;
                        if (moduleCount === 1 && !this.isMenuStyleSelected(menus.secondary)) {
                            // Hide the toggle if there is 1 module and the seondary menu is not specified.
                            $menuToggle.hide();
                        }
                        if (moduleCount > 4) {
                            // During init if the primary menu class is specified then it means the user has
                            // a stored setting for this account so leave it as it is. Otherwise, toggle the
                            // secondary menu.
                            if (!this.isMenuStyleSelected(menus.primary)) {
                                $menu.addClass(menus.secondary);
                            }
                        }
                    },
                    isMenuStyleSelected: function (style) {
                        return $menu.hasClass(style);
                    },
                    toggleMenu: function () {
                        // We need to use opacity rather than display so the size of the menu is available for setHeights().
                        $menu.animate({ opacity: 0.0 }, 200, function () {
                            pinMenu(jQuery('#mega-tabs > ul > li.menu-open'));
                            $menu.toggleClass(menus.secondary);
                            setHeights();
                            var onComplete = function () {
                                $menu.animate({ opacity: 1.0 }, 600);
                            };
                            var selectedMenu = $menu.hasClass(menus.secondary) ?
                                menus.secondary : menus.primary;
                            // Persist the user's setting.
                            swApi.ws.one("NodeManagement.asmx")
                                .post("SaveUserSetting", { name: "MegaMenu-Style", value: selectedMenu })
                                .finally(onComplete);
                        });
                    }
                };
                MenuSelector.init();
                function openMenu($menuItem) {
                    if (!$menuItem.length)
                        return;
                    openTime = Date.now();
                    $menuItem
                        .addClass('menu-open')
                        .find('> .mega-submenu')
                        .fadeIn(300);
                }
                function closeMenu($menuItem) {
                    if (!$menuItem.length)
                        return;
                    $menuItem
                        .removeClass('menu-open')
                        .find('> .mega-submenu')
                        .fadeOut(100);
                }
                function unpinMenu($menuItem) {
                    menuPinned = false;
                    closeMenu($menuItem);
                }
                function pinMenu($menuItem) {
                    menuPinned = true;
                    closeMenu(jQuery('#mega-tabs > ul > li.menu-open'));
                    openMenu($menuItem);
                    clickOutside($menuItem).then(function () {
                        unpinMenu($menuItem);
                    });
                }
                // Close the menu when a menu item is clicked.
                jQuery('.sub-menu-container > ul > li > a').click(function () {
                    closeMenu(jQuery('#mega-tabs > ul > li.menu-open'));
                });
                // Handle mouseenter/mouseleave/click events on the top level menus.
                jQuery('#mega-tabs > ul > li')
                    .mouseenter(function () {
                    var $menuItem = jQuery(this);
                    if ($menuItem.is('.menu-open'))
                        return;
                    timeout = setTimeout(function () {
                        // Close any menus that might be open.
                        unpinMenu(jQuery('#mega-tabs > ul > li.menu-open'));
                        openMenu($menuItem);
                        setHeights();
                    }, 300);
                })
                    .mouseleave(function () {
                    if (menuPinned)
                        return;
                    clearTimeout(timeout);
                    var $menuItem = jQuery(this);
                    setTimeout(function () {
                        if (!$menuItem.is(':hover')) {
                            closeMenu($menuItem);
                        }
                    }, 200);
                })
                    .find('> a').click(function (e) {
                    clearTimeout(timeout);
                    var $menuItem = jQuery(this).parent();
                    var isOpen = $menuItem.is(".menu-open");
                    if (isOpen && (Math.abs(Date.now() - openTime) < 400)) {
                        return;
                    }
                    if (isOpen) {
                        unpinMenu($menuItem);
                    }
                    else {
                        pinMenu($menuItem);
                    }
                    return false;
                });
            }
        };
    }
    Object.defineProperty(MainNavController.prototype, "dashboardGroups", {
        get: function () {
            return this._dashboardGroups;
        },
        set: function (groups) {
            var _this = this;
            this._dashboardGroups = groups;
            this.hasDashboards = _.some(groups, function (group) {
                return group.views && group.views.length > 0;
            });
            if (groups) {
                this.$timeout(function () {
                    _this.MegaMenu.init(_this.swApi);
                }, 0);
            }
        },
        enumerable: true,
        configurable: true
    });
    return MainNavController;
}());
exports.default = MainNavController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbk5hdi1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWFpbk5hdi1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBSUE7SUFDSSxnQkFBZ0I7SUFDaEIsMkJBQ1ksUUFBNEIsRUFDNUIsS0FBb0I7UUFEcEIsYUFBUSxHQUFSLFFBQVEsQ0FBb0I7UUFDNUIsVUFBSyxHQUFMLEtBQUssQ0FBZTtRQUd6QixrQkFBYSxHQUFZLEtBQUssQ0FBQztRQW9CdEMsMkdBQTJHO1FBQzNHLG9CQUFvQjtRQUNaLGFBQVEsR0FBRztZQUNmLElBQUksRUFBRSxVQUFVLEtBQW9CO2dCQUNoQyxJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsaUJBQWlCLENBQUMsRUFDbkMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsb0NBQW9DLENBQUMsRUFDMUQsU0FBUyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsRUFDdEQsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ25DLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLHdDQUF3QyxDQUFDLEVBQ2xFLFFBQVEsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQzdCLEtBQUssR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQzFCLG1CQUFtQixHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFDNUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFDbEMsUUFBUSxHQUFHLElBQUksRUFBRSwwQkFBMEI7Z0JBQzNDLFdBQVcsR0FBRyxLQUFLLEVBQ25CLFNBQVMsR0FBRyxDQUFDLEVBQ2IsS0FBSyxHQUFHLEVBQUMsT0FBTyxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUMsRUFDOUQsVUFBVSxHQUFHLEtBQUssRUFDbEIsT0FBZSxFQUNmLFFBQWdCLEVBQ2hCLGNBQWlELEVBQ2pELGNBQWlELENBQUM7Z0JBRXRELGdFQUFnRTtnQkFDaEU7b0JBQ0ksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUMzQixTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxDQUFDO3dCQUNuRCxZQUFZLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO3dCQUNuRCxXQUFXLEdBQUcsSUFBSSxDQUFDO29CQUN2QixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLFlBQVksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO3dCQUMvQixXQUFXLEdBQUcsS0FBSyxDQUFDO29CQUN4QixDQUFDO2dCQUNMLENBQUM7Z0JBRUQscURBQXFEO2dCQUNyRCxjQUFjLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQU07b0JBQ3hDLGtCQUFrQixFQUFFLENBQUMsQ0FBQywwQkFBMEI7Z0JBQ3BELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDUixnQ0FBZ0M7Z0JBQ2hDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN6RCxrQkFBa0IsRUFBRSxDQUFDLENBQUMsaUNBQWlDO2dCQUV2RCwyQkFBMkI7Z0JBQzNCO29CQUNJLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQzt3QkFDZixNQUFNLENBQUM7b0JBQ1gsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3pCLElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDMUQsbUJBQW1CLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUM7b0JBQ25ELENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osbUJBQW1CLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDekMsQ0FBQztnQkFDTCxDQUFDO2dCQUVELGNBQWMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBTTtvQkFDeEMsa0JBQWtCLEVBQUUsQ0FBQztnQkFDekIsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNQLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN6RCxrQkFBa0IsRUFBRSxDQUFDO2dCQUVyQiw2REFBNkQ7Z0JBQzdELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDbkMsQ0FBQztnQkFFRCw0RUFBNEU7Z0JBQzVFLDRDQUE0QztnQkFDdEMsU0FBVSxDQUFDLE9BQU8sQ0FBQztvQkFDckIsUUFBUSxFQUFFLFVBQVUsR0FBd0I7d0JBQ3hDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ3hDLENBQUM7b0JBQ0QsVUFBVSxFQUFFLFVBQVUsR0FBd0I7d0JBQzFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQzNDLENBQUM7b0JBQ0QsUUFBUSxFQUFFO3dCQUNOLE1BQU0sQ0FBQyxLQUFLLENBQUM7b0JBQ2pCLENBQUM7aUJBQ0osQ0FBQyxDQUFDO2dCQUVILHNFQUFzRTtnQkFDdEUsc0JBQXNCLFFBQWdCO29CQUNsQyxJQUFJLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ2pDLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFFM0IsS0FBSzt5QkFDQSxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQzt3QkFDcEIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ2xELEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQ25CLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQzs0QkFDbkIsTUFBTSxDQUFDLEtBQUssQ0FBQzt3QkFDakIsQ0FBQztvQkFDTCxDQUFDLENBQUM7eUJBQ0QsRUFBRSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7d0JBQ3RCLGdEQUFnRDt3QkFDaEQsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDOzRCQUNqQixLQUFLLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDOzRCQUNyQixRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7NEJBQ25CLE1BQU0sQ0FBQyxLQUFLLENBQUM7d0JBQ2pCLENBQUM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBR1AsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDOUIsQ0FBQztnQkFFRCxLQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBRWxDO29CQUNJLGlFQUFpRTtvQkFDakUsU0FBUyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQztvQkFDdkQsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BDLElBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDM0MsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDLENBQUM7b0JBQ2hELFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxDQUFDO29CQUV4QyxFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDcEQsVUFBVSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7b0JBQ3JDLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0oscUVBQXFFO3dCQUNyRSxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsR0FBRyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQzt3QkFDakUsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLElBQUksSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDOzRCQUFDLE1BQU0sQ0FBQzt3QkFFM0MsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLE9BQU8sRUFBRSxDQUFDOzRCQUN6RCxJQUFJLFNBQVMsR0FBRyxDQUFDLEVBQ2IsS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQzs0QkFFN0MsS0FBSyxDQUFDLElBQUksQ0FBQztnQ0FDUCxJQUFJLFVBQVUsR0FBVyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7Z0NBQ3BELEVBQUUsQ0FBQyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7b0NBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQzs0QkFDdkQsQ0FBQyxDQUFDLENBQUM7NEJBQ0gsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLENBQUM7d0JBQ25DLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO2dCQUVELElBQUksWUFBWSxHQUFHO29CQUNmLElBQUksRUFBRTt3QkFDRixXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBbUI7NEJBQzNDLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQzs0QkFDbkIsWUFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDO3dCQUM5QixDQUFDLENBQUMsQ0FBQzt3QkFFSCw4RUFBOEU7d0JBQzlFLHNFQUFzRTt3QkFDdEUsSUFBSSxXQUFXLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQzt3QkFDcEMsRUFBRSxDQUFDLENBQUMsV0FBVyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNsRSwrRUFBK0U7NEJBQy9FLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDdkIsQ0FBQzt3QkFFRCxFQUFFLENBQUMsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbEIsZ0ZBQWdGOzRCQUNoRixnRkFBZ0Y7NEJBQ2hGLGtCQUFrQjs0QkFDbEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDM0MsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7NEJBQ3BDLENBQUM7d0JBQ0wsQ0FBQztvQkFDTCxDQUFDO29CQUNELG1CQUFtQixFQUFFLFVBQVUsS0FBYTt3QkFDeEMsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ2pDLENBQUM7b0JBQ0QsVUFBVSxFQUFFO3dCQUNSLG9HQUFvRzt3QkFDcEcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUMsRUFBRSxHQUFHLEVBQUU7NEJBQy9CLE9BQU8sQ0FBQyxNQUFNLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDOzRCQUVsRCxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQzs0QkFFbkMsVUFBVSxFQUFFLENBQUM7NEJBRWIsSUFBSSxVQUFVLEdBQUc7Z0NBQ2IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzs0QkFDdkMsQ0FBQyxDQUFDOzRCQUVGLElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0NBQ2hELEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7NEJBRXBDLDhCQUE4Qjs0QkFDOUIsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUM7aUNBQzlCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFDLENBQUM7aUNBQ3RFLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQzt3QkFDN0IsQ0FBQyxDQUFDLENBQUM7b0JBQ1AsQ0FBQztpQkFDSixDQUFDO2dCQUNGLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFFcEIsa0JBQWtCLFNBQWlCO29CQUMvQixFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7d0JBQUMsTUFBTSxDQUFDO29CQUM5QixRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUV0QixTQUFTO3lCQUNKLFFBQVEsQ0FBQyxXQUFXLENBQUM7eUJBQ3JCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQzt5QkFDdkIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyQixDQUFDO2dCQUVELG1CQUFtQixTQUFnQjtvQkFDL0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO3dCQUFDLE1BQU0sQ0FBQztvQkFFOUIsU0FBUzt5QkFDSixXQUFXLENBQUMsV0FBVyxDQUFDO3lCQUN4QixJQUFJLENBQUMsaUJBQWlCLENBQUM7eUJBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDdEIsQ0FBQztnQkFFRCxtQkFBbUIsU0FBaUI7b0JBQ2hDLFVBQVUsR0FBRyxLQUFLLENBQUM7b0JBQ25CLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekIsQ0FBQztnQkFFRCxpQkFBaUIsU0FBaUI7b0JBQzlCLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ2xCLFNBQVMsQ0FBQyxNQUFNLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDO29CQUNwRCxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBRXBCLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQ3pCLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekIsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQztnQkFFRCw4Q0FBOEM7Z0JBQzlDLE1BQU0sQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDOUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hELENBQUMsQ0FBQyxDQUFDO2dCQUVILG9FQUFvRTtnQkFDcEUsTUFBTSxDQUFDLHNCQUFzQixDQUFDO3FCQUN6QixVQUFVLENBQUM7b0JBQ1IsSUFBSSxTQUFTLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM3QixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUFDLE1BQU0sQ0FBQztvQkFFdkMsT0FBTyxHQUFHLFVBQVUsQ0FBQzt3QkFDakIsc0NBQXNDO3dCQUN0QyxTQUFTLENBQUMsTUFBTSxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQzt3QkFFcEQsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUVwQixVQUFVLEVBQUUsQ0FBQztvQkFDakIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLENBQUMsQ0FBQztxQkFDRCxVQUFVLENBQUM7b0JBQ1IsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDO3dCQUFDLE1BQU0sQ0FBQztvQkFFdkIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUN0QixJQUFJLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRTdCLFVBQVUsQ0FBQzt3QkFDUCxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUMxQixTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ3pCLENBQUM7b0JBQ0wsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLENBQUMsQ0FBQztxQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztvQkFDOUIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUV0QixJQUFJLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBRXRDLElBQUksTUFBTSxHQUFHLFNBQVMsQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3hDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDcEQsTUFBTSxDQUFDO29CQUNYLENBQUM7b0JBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt3QkFDVCxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pCLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN2QixDQUFDO29CQUVELE1BQU0sQ0FBQyxLQUFLLENBQUM7Z0JBQ2pCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztTQUNKLENBQUM7SUF4U0MsQ0FBQztJQU1KLHNCQUFXLDhDQUFlO2FBQTFCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUNqQyxDQUFDO2FBQ0QsVUFBMkIsTUFBVztZQUF0QyxpQkFXQztZQVZHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7WUFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFDLEtBQVU7Z0JBQzNDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNqRCxDQUFDLENBQUMsQ0FBQztZQUVILEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsSUFBSSxDQUFDLFFBQVEsQ0FBQztvQkFDVixLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25DLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNWLENBQUM7UUFDTCxDQUFDOzs7T0FaQTtJQWlTTCx3QkFBQztBQUFELENBQUMsQUE5U0QsSUE4U0MifQ==

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var mainNavMenu_controller_1 = __webpack_require__(100);
var MainNavMenu = /** @class */ (function () {
    function MainNavMenu() {
        this.restrict = "E";
        this.require = ["^swMainNav", "swMainNavMenu"];
        this.templateUrl = "orion/directives/mainNav/mainNavMenu-directive.html";
        this.transclude = false;
        this.replace = true;
        this.controller = mainNavMenu_controller_1.default;
        this.controllerAs = "vm";
        this.bindToController = {
            menu: "<"
        };
        this.link = function ($scope, element, attrs, controllers) {
            controllers[1].parentNavController = controllers[0];
        };
    }
    return MainNavMenu;
}());
exports.default = MainNavMenu;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbk5hdk1lbnUtZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWFpbk5hdk1lbnUtZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbUVBQTZEO0FBRzdEO0lBQUE7UUFDVyxhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsWUFBTyxHQUFHLENBQUMsWUFBWSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQzFDLGdCQUFXLEdBQUcscURBQXFELENBQUM7UUFDcEUsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsZUFBVSxHQUFHLGdDQUFxQixDQUFDO1FBQ25DLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLHFCQUFnQixHQUFHO1lBQ3RCLElBQUksRUFBRSxHQUFHO1NBQ1osQ0FBQztRQUVLLFNBQUksR0FBRyxVQUNWLE1BQWlCLEVBQ2pCLE9BQTRCLEVBQzVCLEtBQXFCLEVBQ3JCLFdBQXVEO1lBRXZELFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQUFELGtCQUFDO0FBQUQsQ0FBQyxBQXBCRCxJQW9CQyJ9

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var MainNavMenuController = /** @class */ (function () {
    function MainNavMenuController() {
    }
    return MainNavMenuController;
}());
exports.default = MainNavMenuController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbk5hdk1lbnUtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW5OYXZNZW51LWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUFBO0lBR0EsQ0FBQztJQUFELDRCQUFDO0FBQUQsQ0FBQyxBQUhELElBR0MifQ==

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var idleSensor_controller_1 = __webpack_require__(43);
var idleSensor_component_1 = __webpack_require__(102);
exports.default = function (module) {
    module.controller("swIdleSensorController", idleSensor_controller_1.default);
    module.component("swIdleSensor", idleSensor_component_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlFQUEyRDtBQUMzRCwrREFBZ0Q7QUFFaEQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsd0JBQXdCLEVBQUUsK0JBQW9CLENBQUMsQ0FBQztJQUNsRSxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSw4QkFBVSxDQUFDLENBQUM7QUFDakQsQ0FBQyxDQUFDIn0=

/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var idleSensor_controller_1 = __webpack_require__(43);
var IdleSensor = /** @class */ (function () {
    function IdleSensor() {
        this.restrict = "A";
        this.controller = idleSensor_controller_1.default;
        this.controllerAs = "idleSensor";
        this.bindToController = {};
        this.link = function (scope, element, attrs, ctrl) {
            ctrl.monitorElement(element);
        };
    }
    return IdleSensor;
}());
exports.default = IdleSensor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRsZVNlbnNvci1jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpZGxlU2Vuc29yLWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QyxpRUFBMkQ7QUFFM0Q7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixlQUFVLEdBQUcsK0JBQW9CLENBQUM7UUFDbEMsaUJBQVksR0FBRyxZQUFZLENBQUM7UUFDNUIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRXRCLFNBQUksR0FBRyxVQUNWLEtBQWdCLEVBQUUsT0FBNEIsRUFDOUMsS0FBcUIsRUFBRSxJQUEwQjtZQUVqRCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQztJQUNOLENBQUM7SUFBRCxpQkFBQztBQUFELENBQUMsQUFaRCxJQVlDIn0=

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dashboardMenu_1 = __webpack_require__(104);
var dashboardMenuItem_1 = __webpack_require__(107);
exports.default = function (module) {
    module.component("swDashboardMenu", dashboardMenu_1.default);
    module.component("swDashboardMenuItem", dashboardMenuItem_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlEQUE0QztBQUM1Qyx5REFBb0Q7QUFFcEQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsdUJBQWEsQ0FBQyxDQUFDO0lBQ25ELE1BQU0sQ0FBQyxTQUFTLENBQUMscUJBQXFCLEVBQUUsMkJBQWlCLENBQUMsQ0FBQztBQUMvRCxDQUFDLENBQUMifQ==

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(105);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dashboardMenu_controller_1 = __webpack_require__(106);
var DashboardMenu = /** @class */ (function () {
    function DashboardMenu($log, $window, _t) {
        this.$log = $log;
        this.$window = $window;
        this._t = _t;
        this.transclude = true;
        this.restrict = "E";
        this.scope = {};
        this.templateUrl = "orion/directives/dashboardMenu/dashboardMenu.html";
        this.controller = dashboardMenu_controller_1.default;
        this.controllerAs = "$dashboardMenu";
        this.bindToController = {};
    }
    DashboardMenu.$inject = ["$log", "$window", "$rootScope", "getTextService"];
    return DashboardMenu;
}());
exports.default = DashboardMenu;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkTWVudS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhc2hib2FyZE1lbnUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx1RUFBaUU7QUFFakU7SUFFSSx1QkFBb0IsSUFBb0IsRUFDNUIsT0FBMEIsRUFDMUIsRUFBb0M7UUFGNUIsU0FBSSxHQUFKLElBQUksQ0FBZ0I7UUFDNUIsWUFBTyxHQUFQLE9BQU8sQ0FBbUI7UUFDMUIsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFFekMsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLGdCQUFXLEdBQUcsbURBQW1ELENBQUM7UUFDbEUsZUFBVSxHQUFHLGtDQUF1QixDQUFDO1FBQ3JDLGlCQUFZLEdBQUcsZ0JBQWdCLENBQUM7UUFDaEMscUJBQWdCLEdBQUcsRUFBRSxDQUFDO0lBUnNCLENBQUM7SUFIdEMscUJBQU8sR0FBRyxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixDQUFDLENBQUM7SUFZaEYsb0JBQUM7Q0FBQSxBQWJELElBYUM7a0JBYm9CLGFBQWEifQ==

/***/ }),
/* 105 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DashboardMenuController = /** @class */ (function () {
    function DashboardMenuController($log) {
        this.$log = $log;
    }
    DashboardMenuController.$inject = ["$log"];
    return DashboardMenuController;
}());
exports.default = DashboardMenuController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkTWVudS1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGFzaGJvYXJkTWVudS1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0E7SUFFSSxpQ0FBb0IsSUFBb0I7UUFBcEIsU0FBSSxHQUFKLElBQUksQ0FBZ0I7SUFBRyxDQUFDO0lBRDlCLCtCQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUVyQyw4QkFBQztDQUFBLEFBSEQsSUFHQztrQkFIb0IsdUJBQXVCIn0=

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DashboardMenuItem = /** @class */ (function () {
    function DashboardMenuItem($log) {
        this.$log = $log;
        this.transclude = true;
        this.restrict = "E";
        this.scope = {};
        this.template = "<li class=\"sw-dashboard__menu-item\" ng-transclude />";
    }
    DashboardMenuItem.$inject = ["$log"];
    return DashboardMenuItem;
}());
exports.default = DashboardMenuItem;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkTWVudUl0ZW0uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkYXNoYm9hcmRNZW51SXRlbS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBO0lBRUksMkJBQW9CLElBQW9CO1FBQXBCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBRWpDLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNmLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxhQUFRLEdBQUcsd0RBQXNELENBQUM7SUFMOUIsQ0FBQztJQUQ5Qix5QkFBTyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7SUFPckMsd0JBQUM7Q0FBQSxBQVJELElBUUM7a0JBUm9CLGlCQUFpQiJ9

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var demoAnchorScrub_directive_1 = __webpack_require__(109);
function isDemo(module) {
    return module && module.environment && module.environment.demoMode;
}
exports.default = function (module) {
    if (isDemo(module)) {
        module.component("swDemoAnchorScrub", demoAnchorScrub_directive_1.default);
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHlFQUE0RDtBQUU1RCxnQkFBZ0IsTUFBVztJQUN2QixNQUFNLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUM7QUFDdkUsQ0FBQztBQUVELGtCQUFlLFVBQUMsTUFBbUI7SUFDL0IsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixNQUFNLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLG1DQUFpQixDQUFDLENBQUM7SUFDN0QsQ0FBQztBQUNMLENBQUMsQ0FBQyJ9

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SwDemoAnchorScrub = /** @class */ (function () {
    function SwDemoAnchorScrub(swDemoService) {
        var _this = this;
        this.swDemoService = swDemoService;
        this.restrict = "E";
        this.link = function (scope, element, attrs) {
            if (_this.swDemoService.isBlockedHrefInDemo(attrs["href"])) {
                element.on("click", function (eventArgs) {
                    eventArgs.preventDefault();
                    _this.swDemoService.showDemoErrorToast();
                });
            }
        };
    }
    SwDemoAnchorScrub.$inject = ["swDemoService"];
    return SwDemoAnchorScrub;
}());
exports.default = SwDemoAnchorScrub;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVtb0FuY2hvclNjcnViLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRlbW9BbmNob3JTY3J1Yi1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFJdkM7SUFHSSwyQkFBb0IsYUFBMkI7UUFBL0MsaUJBQ0M7UUFEbUIsa0JBQWEsR0FBYixhQUFhLENBQWM7UUFHeEMsYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUVmLFNBQUksR0FBRyxVQUFDLEtBQWdCLEVBQUUsT0FBNEIsRUFBRSxLQUFxQjtZQUNoRixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEQsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBQyxTQUFjO29CQUMvQixTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQzNCLEtBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztnQkFDNUMsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO0lBWEYsQ0FBQztJQUhhLHlCQUFPLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQWU5Qyx3QkFBQztDQUFBLEFBaEJELElBZ0JDO2tCQWhCb0IsaUJBQWlCIn0=

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var error_controller_1 = __webpack_require__(111);
var error_directive_1 = __webpack_require__(112);
exports.default = function (module) {
    module.controller("swOrionErrorController", error_controller_1.default);
    module.component("swOrionError", error_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVEQUFzRDtBQUN0RCxxREFBb0Q7QUFFcEQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsd0JBQXdCLEVBQUUsMEJBQW9CLENBQUMsQ0FBQztJQUNsRSxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSx5QkFBbUIsQ0FBQyxDQUFDO0FBQzFELENBQUMsQ0FBQyJ9

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionErrorController = /** @class */ (function () {
    function OrionErrorController($window, $log, _t, swUtil) {
        var _this = this;
        this.$window = $window;
        this.$log = $log;
        this._t = _t;
        this.swUtil = swUtil;
        this.$onInit = function () {
            //this should really be static, but I need the _t to translate
            _this.codeToTextMap = {
                "403": _this._t("Forbidden"),
                "404": _this._t("File Not Found"),
                "500": _this._t("Internal Server Error")
            };
            //this could be done by simple concatenation; however, this map also reflects known images
            _this.codeToClassMap = {
                "403": "sw-orion-error-page__image--404",
                "404": "sw-orion-error-page__image--404",
                "500": "sw-orion-error-page__image--500"
            };
            if (!_this.errorReport) {
                _this.errorReport = {
                    errorType: "",
                    errorCode: "",
                    message: _this._t("An unknown error occurred"),
                    location: ""
                };
                _this.$log.error("No parameters passed to error page!");
                return;
            }
            _this.$log.error("Error Page Parameters", _this.errorReport);
            //try to get specific image for code- if it fails, fall back to 404
            var image = _this.codeToClassMap[_this.errorReport.errorCode];
            if (image) {
                _this.imageClass = image;
            }
            else {
                _this.imageClass = _this.codeToClassMap[404];
            }
            //if we have specific text for a code, use it.  Otherwise fall back to generic error
            if (_this.errorReport.errorCode && _this.codeToTextMap[_this.errorReport.errorCode]) {
                _this.headerText = _this.swUtil.formatString(_this._t("{0} - {1}, Ooops..."), _this.errorReport.errorCode, _this.codeToTextMap[_this.errorReport.errorCode]);
            }
            else {
                _this.headerText = _this._t("Website Error");
            }
            if (_this.footerTemplateUrl) {
                _this.$log.debug("Custom footer template detected");
            }
            _this.errorStringified = JSON.stringify(_this.errorReport, null, "\t");
            _this.updateDetailButtonText();
        };
        this.returnToHome = function () {
            _this.$window.location.href = "/Orion/View.aspx";
        };
        this.showHideDetails = function () {
            _this.isDetailsOpen = !_this.isDetailsOpen;
            _this.updateDetailButtonText();
        };
        this.updateDetailButtonText = function () {
            if (_this.isDetailsOpen) {
                _this.detailsButtonText = _this._t("Hide Error Report");
            }
            else {
                _this.detailsButtonText = _this._t("Show Error Report");
            }
        };
        this.isDetailsOpen = false;
    }
    OrionErrorController.$inject = ["$window", "$log", "getTextService", "swUtil"];
    return OrionErrorController;
}());
exports.default = OrionErrorController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3ItY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVycm9yLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFLdkM7SUFNSSw4QkFDWSxPQUEwQixFQUMxQixJQUFvQixFQUNwQixFQUFvQyxFQUNwQyxNQUFnQjtRQUo1QixpQkFNQztRQUxXLFlBQU8sR0FBUCxPQUFPLENBQW1CO1FBQzFCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBQ3BCLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBQ3BDLFdBQU0sR0FBTixNQUFNLENBQVU7UUFJckIsWUFBTyxHQUFHO1lBQ2IsOERBQThEO1lBQzlELEtBQUksQ0FBQyxhQUFhLEdBQUc7Z0JBQ2pCLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDM0IsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ2hDLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLHVCQUF1QixDQUFDO2FBQzFDLENBQUM7WUFFRiwwRkFBMEY7WUFDMUYsS0FBSSxDQUFDLGNBQWMsR0FBRztnQkFDbEIsS0FBSyxFQUFFLGlDQUFpQztnQkFDeEMsS0FBSyxFQUFFLGlDQUFpQztnQkFDeEMsS0FBSyxFQUFFLGlDQUFpQzthQUMzQyxDQUFDO1lBRUYsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDcEIsS0FBSSxDQUFDLFdBQVcsR0FBRztvQkFDZixTQUFTLEVBQUUsRUFBRTtvQkFDYixTQUFTLEVBQUUsRUFBRTtvQkFDYixPQUFPLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztvQkFDN0MsUUFBUSxFQUFFLEVBQUU7aUJBQ2YsQ0FBQztnQkFDRixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO2dCQUN2RCxNQUFNLENBQUM7WUFDWCxDQUFDO1lBQ0QsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRTNELG1FQUFtRTtZQUNuRSxJQUFNLEtBQUssR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDUixLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUM1QixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9DLENBQUM7WUFFRCxvRkFBb0Y7WUFDcEYsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0UsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FDdEMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUM5QixLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUNqRCxDQUFDO1lBQ04sQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUMvQyxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDekIsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQztZQUN2RCxDQUFDO1lBRUQsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDckUsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDbEMsQ0FBQyxDQUFDO1FBRUssaUJBQVksR0FBRztZQUNsQixLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsa0JBQWtCLENBQUM7UUFDcEQsQ0FBQyxDQUFDO1FBRUssb0JBQWUsR0FBRztZQUNyQixLQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQztZQUN6QyxLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUNsQyxDQUFDLENBQUM7UUFFTSwyQkFBc0IsR0FBRztZQUM3QixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDckIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUMxRCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUMxRCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBSUssa0JBQWEsR0FBRyxLQUFLLENBQUM7SUEzRTdCLENBQUM7SUFYYSw0QkFBTyxHQUFHLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsQ0FBQztJQTRGNUUsMkJBQUM7Q0FBQSxBQTdGRCxJQTZGQztrQkE3Rm9CLG9CQUFvQiJ9

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(113);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionErrorDirective = /** @class */ (function () {
    function OrionErrorDirective() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/error/error-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            errorReport: "=",
            footerTemplateUrl: "@?"
        };
        this.controller = "swOrionErrorController";
        this.controllerAs = "vm";
    }
    return OrionErrorDirective;
}());
exports.default = OrionErrorDirective;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3ItZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZXJyb3ItZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDO0lBQUE7UUFDVyxhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsZ0JBQVcsR0FBRyw2Q0FBNkMsQ0FBQztRQUM1RCxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLHFCQUFnQixHQUFHO1lBQ3RCLFdBQVcsRUFBRSxHQUFHO1lBQ2hCLGlCQUFpQixFQUFFLElBQUk7U0FDMUIsQ0FBQztRQUNLLGVBQVUsR0FBRyx3QkFBd0IsQ0FBQztRQUN0QyxpQkFBWSxHQUFHLElBQUksQ0FBQztJQUMvQixDQUFDO0lBQUQsMEJBQUM7QUFBRCxDQUFDLEFBWEQsSUFXQyJ9

/***/ }),
/* 113 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var licenseActivationModal_controller_1 = __webpack_require__(44);
var licenseActivation_directive_1 = __webpack_require__(116);
var licenseActivationModal_directive_1 = __webpack_require__(117);
var licenseActivation_service_1 = __webpack_require__(46);
var licenseActivation_constants_1 = __webpack_require__(2);
__webpack_require__(47);
exports.default = function (module) {
    module.controller("swLicenseActivationModalController", licenseActivationModal_controller_1.default);
    module.component("swLicenseActivation", licenseActivation_directive_1.default);
    module.component("swLicenseActivationModal", licenseActivationModal_directive_1.default);
    module.service("swLicenseActivationService", licenseActivation_service_1.default);
    module.service("swLicenseActivationConstants", licenseActivation_constants_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLHlGQUFtRjtBQUNuRiw2RUFBOEQ7QUFDOUQsdUZBQXdFO0FBQ3hFLHlFQUFtRTtBQUNuRSw2RUFBdUU7QUFFdkUsOENBQTRDO0FBRTVDLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsVUFBVSxDQUFDLG9DQUFvQyxFQUFFLDJDQUFnQyxDQUFDLENBQUM7SUFFMUYsTUFBTSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRSxxQ0FBaUIsQ0FBQyxDQUFDO0lBQzNELE1BQU0sQ0FBQyxTQUFTLENBQUMsMEJBQTBCLEVBQUUsMENBQXNCLENBQUMsQ0FBQztJQUVyRSxNQUFNLENBQUMsT0FBTyxDQUFDLDRCQUE0QixFQUFFLG1DQUF3QixDQUFDLENBQUM7SUFDdkUsTUFBTSxDQUFDLE9BQU8sQ0FBQyw4QkFBOEIsRUFBRSxxQ0FBMEIsQ0FBQyxDQUFDO0FBQy9FLENBQUMsQ0FBQyJ9

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User(id, model, fromLocalStorage) {
        if (fromLocalStorage === void 0) { fromLocalStorage = false; }
        var _this = this;
        this.id = id;
        this.setLocalStorageData = function (user) {
            localStorage.setItem("activateLicenseFormUser", JSON.stringify(user));
            localStorage.setItem("OrionCurrentUser", _this.id);
        };
        this.getLocalStorageData = function () {
            return JSON.parse(localStorage.getItem("activateLicenseFormUser"));
        };
        if (fromLocalStorage && localStorage.length > 0 && localStorage.getItem("OrionCurrentUser") === this.id) {
            model = this.getLocalStorageData();
        }
        if (model) {
            _.extend(this, model);
        }
    }
    return User;
}());
exports.default = User;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInVzZXItbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFFdkM7SUFPSSxjQUFvQixFQUFVLEVBQUUsS0FBWSxFQUFFLGdCQUFpQztRQUFqQyxpQ0FBQSxFQUFBLHdCQUFpQztRQUEvRSxpQkFRQztRQVJtQixPQUFFLEdBQUYsRUFBRSxDQUFRO1FBVXZCLHdCQUFtQixHQUFHLFVBQUMsSUFBVTtZQUNwQyxZQUFZLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN0RSxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUM7UUFFTSx3QkFBbUIsR0FBRztZQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUM7UUFmRSxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdEcsS0FBSyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ3ZDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUIsQ0FBQztJQUNMLENBQUM7SUFVTCxXQUFDO0FBQUQsQ0FBQyxBQXpCRCxJQXlCQyJ9

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(47);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var LicenseActivation = /** @class */ (function () {
    function LicenseActivation() {
        this.restrict = "E";
        this.template = __webpack_require__(28);
        this.replace = true;
        this.transclude = false;
    }
    return LicenseActivation;
}());
exports.default = LicenseActivation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZUFjdGl2YXRpb24tZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGljZW5zZUFjdGl2YXRpb24tZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDO0lBQUE7UUFDVyxhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsYUFBUSxHQUFHLE9BQU8sQ0FBUyxvQ0FBb0MsQ0FBQyxDQUFDO1FBQ2pFLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixlQUFVLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7SUFBRCx3QkFBQztBQUFELENBQUMsQUFMRCxJQUtDIn0=

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var licenseActivationModal_controller_1 = __webpack_require__(44);
/**
 * @ngdoc directive
 * @name orion.directive:swLicenseActivationModal
 * @restrict A
 *
 * @description
 * License Activation Modal is a dialog component providing an UI for activation of SolarWinds products licenses.
 *
 * @parameters
 * @param {License} selected-license Selected license that should be activated
 * @param {string} mode Offline or Online mode of component UI.
 * @param {string} dialog-title Name for dialog's header title.
 * @param {Function} on-activate The callback function what is called when the License is activated
 *
 * @example
 *   <example module="orion">
 *       <file name="config.js">
 *            Xui.translations = {
 *              xui_wizard_back: "< Back",
 *              xui_wizard_next: "Next >",
 *              xui_wizard_finish: "Close",
 *              xui_wizard_cancel: "Cancel",
 *              xui_fileupload_browse: "Browse",
 *              xui_fileupload_change: "Change the file",
 *              xui_fileupload_remove: "Remove",
 *              xui_fileupload_no_file: "No file selected yet.",
 *              xui_fileupload_require_error: "Selecting a file is required"
 *            };
 *        </file>
 *       <file src="src/directives/licenseActivation/docs/licenseActivationModal-example.html" name="index.html"></file>
 *       <file src="src/directives/licenseActivation/docs/licenseActivationModal-example.js" name="script.js"></file>
 *   </example>
 **/
var LicenseActivationModal = /** @class */ (function () {
    function LicenseActivationModal() {
        this.restrict = "A";
        this.bindToController = {
            selectedLicense: "=?",
            mode: "<",
            dialogTitle: "<",
            onActivate: "&?"
        };
        this.controller = licenseActivationModal_controller_1.default;
        this.controllerAs = "licenseActivation";
        this.link = function (scope, element, attrs, ctrl) {
            element.bind("click", function () {
                ctrl.showActivationDialog();
            });
        };
    }
    return LicenseActivationModal;
}());
exports.default = LicenseActivationModal;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZUFjdGl2YXRpb25Nb2RhbC1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsaWNlbnNlQWN0aXZhdGlvbk1vZGFsLWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2Qyx5RkFBbUY7QUFFbkY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBZ0NJO0FBQ0o7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFFZixxQkFBZ0IsR0FBRztZQUN0QixlQUFlLEVBQUUsSUFBSTtZQUNyQixJQUFJLEVBQUUsR0FBRztZQUNULFdBQVcsRUFBRSxHQUFHO1lBQ2hCLFVBQVUsRUFBRSxJQUFJO1NBQ25CLENBQUM7UUFFSyxlQUFVLEdBQUcsMkNBQWdDLENBQUM7UUFDOUMsaUJBQVksR0FBRyxtQkFBbUIsQ0FBQztRQUVuQyxTQUFJLEdBQXdCLFVBQy9CLEtBQWdCLEVBQ2hCLE9BQTRCLEVBQzVCLEtBQXFCLEVBQ3JCLElBQXNDO1lBRXRDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNsQixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztJQUNOLENBQUM7SUFBRCw2QkFBQztBQUFELENBQUMsQUF2QkQsSUF1QkMifQ==

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var search_controller_1 = __webpack_require__(119);
var search_directive_1 = __webpack_require__(120);
exports.default = function (module) {
    module.controller("swOrionSearchController", search_controller_1.default);
    module.component("swOrionSearch", search_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlEQUF3RDtBQUN4RCx1REFBc0Q7QUFFdEQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMseUJBQXlCLEVBQUUsMkJBQXFCLENBQUMsQ0FBQztJQUNwRSxNQUFNLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSwwQkFBb0IsQ0FBQyxDQUFDO0FBQzVELENBQUMsQ0FBQyJ9

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var searchPageUrl = "/ui/search";
var OrionSearchController = /** @class */ (function () {
    function OrionSearchController($window, $q, $element, searchHistoryService) {
        this.$window = $window;
        this.$q = $q;
        this.$element = $element;
        this.searchHistoryService = searchHistoryService;
        this.isOpen = false;
    }
    OrionSearchController.prototype.onSearch = function (value) {
        this.searchValue = value;
        if (!this.searchValue) {
            return;
        }
        this.searchValue = this.searchValue.trim();
        this.searchHistoryService.addToHistory(this.searchValue);
        this.redirectToSearch(this.searchValue);
        return this.$q.defer();
    };
    OrionSearchController.prototype.onToggle = function (isOpen) {
        var _this = this;
        if (isOpen) {
            this.searchHistoryService.getHistory(5).then(function (history) {
                _this.searchHistory = history;
                var toBeFocused = _this.$element.find(".xui-search__input-control");
                toBeFocused.focus();
                _this.isOpen = isOpen;
            });
        }
    };
    OrionSearchController.prototype.redirectToSearch = function (searchValue) {
        var url = this.$window.location.origin + searchPageUrl + "?q=" + searchValue;
        this.$window.open(url, "_self");
    };
    OrionSearchController.$inject = ["$window", "$q", "$element", "swSearchHistoryService"];
    return OrionSearchController;
}());
exports.OrionSearchController = OrionSearchController;
exports.default = OrionSearchController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZWFyY2gtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQVFBLElBQU0sYUFBYSxHQUFXLFlBQVksQ0FBQztBQUUzQztJQU1JLCtCQUNZLE9BQTBCLEVBQzFCLEVBQWdCLEVBQ2hCLFFBQTZCLEVBQzdCLG9CQUEyQztRQUgzQyxZQUFPLEdBQVAsT0FBTyxDQUFtQjtRQUMxQixPQUFFLEdBQUYsRUFBRSxDQUFjO1FBQ2hCLGFBQVEsR0FBUixRQUFRLENBQXFCO1FBQzdCLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBdUI7UUFSaEQsV0FBTSxHQUFZLEtBQUssQ0FBQztJQVMzQixDQUFDO0lBRUUsd0NBQVEsR0FBZixVQUFnQixLQUFhO1FBQ3pCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUUzQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFTSx3Q0FBUSxHQUFmLFVBQWdCLE1BQWU7UUFBL0IsaUJBU0M7UUFSRyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ1QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFzQjtnQkFDaEUsS0FBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7Z0JBQzdCLElBQU0sV0FBVyxHQUF3QixLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO2dCQUMxRixXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3BCLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztJQUNMLENBQUM7SUFFTyxnREFBZ0IsR0FBeEIsVUFBeUIsV0FBbUI7UUFDeEMsSUFBTSxHQUFHLEdBQVcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLGFBQWEsR0FBRyxLQUFLLEdBQUcsV0FBVyxDQUFDO1FBQ3ZGLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBdENhLDZCQUFPLEdBQUcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO0lBdUNwRiw0QkFBQztDQUFBLEFBeENELElBd0NDO0FBeENZLHNEQUFxQjtBQTBDbEMsa0JBQWUscUJBQXFCLENBQUMifQ==

/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(121);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionBasicSearchDirective = /** @class */ (function () {
    function OrionBasicSearchDirective() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/search/search-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            something: "="
        };
        this.controller = "swOrionSearchController";
        this.controllerAs = "vm";
    }
    return OrionBasicSearchDirective;
}());
exports.default = OrionBasicSearchDirective;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNlYXJjaC1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFFdkM7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixnQkFBVyxHQUFHLCtDQUErQyxDQUFDO1FBQzlELFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gscUJBQWdCLEdBQUc7WUFDdEIsU0FBUyxFQUFFLEdBQUc7U0FDakIsQ0FBQztRQUNLLGVBQVUsR0FBRyx5QkFBeUIsQ0FBQztRQUN2QyxpQkFBWSxHQUFHLElBQUksQ0FBQztJQUMvQixDQUFDO0lBQUQsZ0NBQUM7QUFBRCxDQUFDLEFBVkQsSUFVQyJ9

/***/ }),
/* 121 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var entityPopoverLoader_controller_1 = __webpack_require__(48);
var entityPopoverLoader_directive_1 = __webpack_require__(50);
__webpack_require__(51);
exports.default = function (module) {
    module.controller("swEntityPopoverLoaderController", entityPopoverLoader_controller_1.default);
    module.component("swEntityPopoverLoader", entityPopoverLoader_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1GQUF1RTtBQUN2RSxpRkFBNEQ7QUFDNUQsZ0RBQThDO0FBRTlDLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsVUFBVSxDQUFDLGlDQUFpQyxFQUFFLHdDQUF1QixDQUFDLENBQUM7SUFDOUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsRUFBRSx1Q0FBYSxDQUFDLENBQUM7QUFDN0QsQ0FBQyxDQUFDIn0=

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../../ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
var entityPopover_directive_1 = __webpack_require__(124);
var entityPopoverProperty_directive_1 = __webpack_require__(126);
exports.entityPopover = function (module) {
    templates.$inject = ["$templateCache"];
    module.component("swEntityPopover", entityPopover_directive_1.EntityPopover);
    module.component("swEntityPopoverProperty", entityPopoverProperty_directive_1.EntityPopoverProperty);
    /** @ngInject */
    function templates($templateCache) {
        $templateCache.put("sw-entity-popover-item", __webpack_require__(25));
        $templateCache.put("sw-entity-popover-link-item-template", __webpack_require__(26));
    }
    module.app().run(templates);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEscUNBQXFDOztBQUdyQyxxRUFBMEQ7QUFDMUQsOEZBQW1GO0FBRXRFLFFBQUEsYUFBYSxHQUFHLFVBQUMsTUFBZTtJQUN6QyxNQUFNLENBQUMsU0FBUyxDQUFDLGlCQUFpQixFQUFFLHVDQUFhLENBQUMsQ0FBQztJQUNuRCxNQUFNLENBQUMsU0FBUyxDQUFDLHlCQUF5QixFQUFFLHVEQUFxQixDQUFDLENBQUM7SUFFbkUsZ0JBQWdCO0lBQ2hCLG1CQUFtQixjQUF3QztRQUN2RCxjQUFjLENBQUMsR0FBRyxDQUNkLHdCQUF3QixFQUN4QixPQUFPLENBQVMsMkJBQTJCLENBQUMsQ0FDL0MsQ0FBQztRQUNGLGNBQWMsQ0FBQyxHQUFHLENBQ2Qsc0NBQXNDLEVBQ3RDLE9BQU8sQ0FBUyx3Q0FBd0MsQ0FBQyxDQUM1RCxDQUFDO0lBQ04sQ0FBQztJQUVELE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDaEMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(52);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(52);
var entityPopover_controller_1 = __webpack_require__(125);
/**
 * @ngdoc directive
 * @name orion.directive:swEntityPopover
 * @restrict E
 *
 * @description
 * This component is used as a content for popover for Orion entities. It is a basic template that lists provided
 * properties and shows a 'Commands' menu with provided links. Properties with empty value are not displayed.
 *
 * @example
 * <example module="orion">
 *     <file src="src/directives/entityPopover/docs/entityPopover-examples.html" name="index.html"></file>
 *     <file src="src/directives/entityPopover/docs/entityPopover-examples.js" name="app.js"></file>
 * </example>
 */
var EntityPopover = /** @class */ (function () {
    function EntityPopover() {
        this.restrict = "E";
        this.controller = entityPopover_controller_1.EntityPopoverController;
        this.controllerAs = "vm";
        this.scope = {};
        this.bindToController = {
            content: "<"
        };
        this.template = __webpack_require__(24);
    }
    return EntityPopover;
}());
exports.EntityPopover = EntityPopover;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5UG9wb3Zlci1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnRpdHlQb3BvdmVyLWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QywwQ0FBd0M7QUFDeEMsdUVBQXFFO0FBRXJFOzs7Ozs7Ozs7Ozs7OztHQWNHO0FBQ0g7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixlQUFVLEdBQUcsa0RBQXVCLENBQUM7UUFDckMsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLHFCQUFnQixHQUFHO1lBQ3RCLE9BQU8sRUFBRSxHQUFHO1NBQ2YsQ0FBQztRQUNLLGFBQVEsR0FBRyxPQUFPLENBQVMsZ0NBQWdDLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBQUQsb0JBQUM7QUFBRCxDQUFDLEFBVEQsSUFTQztBQVRZLHNDQUFhIn0=

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var EntityPopoverController = /** @class */ (function () {
    /** @ngInject */
    EntityPopoverController.$inject = ["$injector", "$log", "$q", "getTextService"];
    function EntityPopoverController($injector, $log, $q, getTextService) {
        this.$injector = $injector;
        this.$log = $log;
        this.$q = $q;
        this.getTextService = getTextService;
        this.templateFn = function (item) {
            if (item.cp && item.itemTemplate) {
                return item.itemTemplate;
            }
            return "sw-entity-popover-item";
        };
    }
    EntityPopoverController.prototype.$onInit = function () {
        this.menuItems = this.getMenuItems();
        this.nonEmptyData = this.getData();
    };
    EntityPopoverController.prototype.getData = function () {
        var data = this.addShowMoreButton(this.sortData(this.content.data));
        return _.filter(data || [], function (value) { return (value.value && !value.type) // in case there is no type, we will include that data
            || (value.value && value.type === "System.String") // text not-empty values
            || (value.type !== "System.String" && value.value >= 0) // numeric positive values
            || (!value.value && value.cp); } // all custom properties
        );
    };
    EntityPopoverController.prototype.getMenuItems = function () {
        var _this = this;
        if (!this.content.menu) {
            return [];
        }
        // we need to convert our menu item definitions to those accepted by <xui-menu items-source="..." />
        return this.content.menu.map(function (m) {
            // we support either "url" links
            if (m.url) {
                return _.assign({}, m, { itemType: "link" });
            }
            // or actions leading to defined injectable (or it's method)
            if (m.action) {
                var actionDefinition_1 = m.action;
                var action_1 = function () {
                    var injectable = _this.$injector.get(actionDefinition_1.name);
                    if (angular.isUndefined(injectable)) {
                        _this.$log.error("Injectable '" + actionDefinition_1.name +
                            "' requested by menu item definition " + JSON.stringify(action_1) + " not found.");
                        return;
                    }
                    // determine the target depending on whether the method was defined or not
                    var target = actionDefinition_1.method ?
                        injectable[actionDefinition_1.method].bind(injectable) :
                        injectable;
                    if (!angular.isFunction(target)) {
                        _this.$log.error("Target of menu item " + JSON.stringify(action_1) +
                            " is not a function: " + target);
                        return;
                    }
                    _this.$q.when(target(actionDefinition_1.params)).then(function () {
                        if (angular.isFunction(_this.content.actionApplied)) {
                            _this.content.actionApplied();
                        }
                    });
                };
                return _.assign({}, m, { itemType: "action", action: action_1 });
            }
            throw new Error("Unsupported type of menu item: " + JSON.stringify(m));
        });
    };
    //this sorts custom properties so they appear after all the other data, including Extension Data
    EntityPopoverController.prototype.sortData = function (data) {
        return data && data.filter(function (e) { return !e.cp; }).concat(data.filter(function (e) { return e.cp; })) || [];
    };
    EntityPopoverController.prototype.addShowMoreButton = function (data) {
        var _this = this;
        var customPropertiesDisplayed = 5;
        var customPropertiesCounter = 0;
        var result = [];
        data.forEach(function (element) {
            if (element.cp) {
                customPropertiesCounter++;
                if (customPropertiesCounter === customPropertiesDisplayed + 1) {
                    element.buttonText = _this.getTextService("Show more custom properties");
                    element.itemTemplate = "sw-entity-popover-link-item-template";
                    element.link = "/Orion/Admin/CPE/InlineEditor.aspx";
                }
            }
        });
        return data;
    };
    return EntityPopoverController;
}());
exports.EntityPopoverController = EntityPopoverController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5UG9wb3Zlci1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZW50aXR5UG9wb3Zlci1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBNEN2QztJQVdJLGdCQUFnQjtJQUNoQixpQ0FBb0IsU0FBbUMsRUFBVSxJQUFvQixFQUFVLEVBQWdCLEVBQ25HLGNBQWdEO1FBRHhDLGNBQVMsR0FBVCxTQUFTLENBQTBCO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBZ0I7UUFBVSxPQUFFLEdBQUYsRUFBRSxDQUFjO1FBQ25HLG1CQUFjLEdBQWQsY0FBYyxDQUFrQztRQVRwRCxlQUFVLEdBQUcsVUFBQyxJQUEyQjtZQUM3QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUM3QixDQUFDO1lBQ0QsTUFBTSxDQUFDLHdCQUF3QixDQUFDO1FBQ3BDLENBQUMsQ0FBQztJQUtGLENBQUM7SUFFTSx5Q0FBTyxHQUFkO1FBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVNLHlDQUFPLEdBQWQ7UUFDSSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFFcEUsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQ1gsSUFBSSxJQUFJLEVBQUUsRUFDVixVQUFBLEtBQUssSUFBSSxPQUFBLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxzREFBc0Q7ZUFDekYsQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLENBQUMsd0JBQXdCO2VBQ3hFLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxlQUFlLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBRSwwQkFBMEI7ZUFDaEYsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUhwQixDQUdvQixDQUFDLHdCQUF3QjtTQUN6RCxDQUFDO0lBQ04sQ0FBQztJQUVNLDhDQUFZLEdBQW5CO1FBQUEsaUJBOENDO1FBN0NHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxFQUFFLENBQUM7UUFDZCxDQUFDO1FBRUQsb0dBQW9HO1FBQ3BHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDO1lBQzFCLGdDQUFnQztZQUNoQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDUixNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUMsUUFBUSxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7WUFDL0MsQ0FBQztZQUVELDREQUE0RDtZQUM1RCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDWCxJQUFNLGtCQUFnQixHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7Z0JBRWxDLElBQU0sUUFBTSxHQUFHO29CQUNYLElBQU0sVUFBVSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFNLGtCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUVsRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbEMsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxHQUFHLGtCQUFnQixDQUFDLElBQUk7NEJBQ2xELHNDQUFzQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBTSxDQUFDLEdBQUcsYUFBYSxDQUFDLENBQUM7d0JBQ3JGLE1BQU0sQ0FBQztvQkFDWCxDQUFDO29CQUNELDBFQUEwRTtvQkFDMUUsSUFBTSxNQUFNLEdBQUcsa0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3BDLFVBQVUsQ0FBQyxrQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDdEQsVUFBVSxDQUFDO29CQUVmLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzlCLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBTSxDQUFDOzRCQUMzRCxzQkFBc0IsR0FBRyxNQUFNLENBQUMsQ0FBQzt3QkFDckMsTUFBTSxDQUFDO29CQUNYLENBQUM7b0JBQ0QsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUMvQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNqRCxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxDQUFDO3dCQUNqQyxDQUFDO29CQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNQLENBQUMsQ0FBQztnQkFFRixNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsUUFBTSxFQUFDLENBQUMsQ0FBQztZQUNqRSxDQUFDO1lBRUQsTUFBTSxJQUFJLEtBQUssQ0FBQyxpQ0FBaUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0UsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZ0dBQWdHO0lBQ3hGLDBDQUFRLEdBQWhCLFVBQWlCLElBQThCO1FBQzNDLE1BQU0sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBTCxDQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxFQUFFLEVBQUosQ0FBSSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEYsQ0FBQztJQUVPLG1EQUFpQixHQUF6QixVQUEwQixJQUE4QjtRQUF4RCxpQkFpQkM7UUFoQkcsSUFBTSx5QkFBeUIsR0FBRyxDQUFDLENBQUM7UUFDcEMsSUFBSSx1QkFBdUIsR0FBRyxDQUFDLENBQUM7UUFDaEMsSUFBSSxNQUFNLEdBQTRCLEVBQUUsQ0FBQztRQUV6QyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUNoQixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDYix1QkFBdUIsRUFBRSxDQUFDO2dCQUMxQixFQUFFLENBQUMsQ0FBQyx1QkFBdUIsS0FBSyx5QkFBeUIsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM1RCxPQUFPLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsNkJBQTZCLENBQUMsQ0FBQztvQkFDeEUsT0FBTyxDQUFDLFlBQVksR0FBRyxzQ0FBc0MsQ0FBQztvQkFDOUQsT0FBTyxDQUFDLElBQUksR0FBRyxvQ0FBb0MsQ0FBQztnQkFDeEQsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUNMLDhCQUFDO0FBQUQsQ0FBQyxBQXhHRCxJQXdHQztBQXhHWSwwREFBdUIifQ==

/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(127);

"use strict";
///<reference path="../../../ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
var EntityPopoverProperty = /** @class */ (function () {
    function EntityPopoverProperty($timeout) {
        var _this = this;
        this.$timeout = $timeout;
        this.controller = EntityPopoverPropertyController;
        this.controllerAs = "vm";
        this.scope = {};
        this.bindToController = {
            label: "@",
            value: "<",
            unit: "@",
            color: "@",
            icon: "@",
            iconStatus: "@",
            vendorIcon: "@"
        };
        this.template = __webpack_require__(27);
        this.link = function (scope, element, attrs, ctrl) {
            // Will run after rendering dom is finished
            _this.$timeout(function () {
                // Manually trigger resize event because xui is binding tooltips in on-resize event callback
                element.find(".xui-ellipsis").trigger("resize");
            }, 0);
        };
    }
    EntityPopoverProperty.$inject = ["$timeout"];
    return EntityPopoverProperty;
}());
exports.EntityPopoverProperty = EntityPopoverProperty;
var EntityPopoverPropertyController = /** @class */ (function () {
    /** @ngInject */
    EntityPopoverPropertyController.$inject = ["$filter"];
    function EntityPopoverPropertyController($filter) {
        var _this = this;
        this.$filter = $filter;
        this.$onInit = function () {
            _this.elipsisLabel = {
                tooltipText: _this.label,
                tooltipOptions: { "tooltip-placement": "bottom" }
            };
            _this.elipsisValue = {
                tooltipText: _this.unit ? _this.value + " " + _this.unit : _this.value,
                tooltipOptions: { "tooltip-placement": "bottom" }
            };
        };
    }
    EntityPopoverPropertyController.prototype.getColorClass = function () {
        return this.color ? "xui-text-" + this.color + (" sw-entity-popover-property__value--" + this.color) : "xui-text-normal";
    };
    EntityPopoverPropertyController.prototype.getValue = function () {
        if (angular.isNumber(this.value)) {
            return this.$filter("number")(this.value);
        }
        return this.value;
    };
    return EntityPopoverPropertyController;
}());
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5UG9wb3ZlclByb3BlcnR5LWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVudGl0eVBvcG92ZXJQcm9wZXJ0eS1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHdDQUF3Qzs7QUFJeEM7SUFpQkksK0JBQW9CLFFBQTRCO1FBQWhELGlCQUNDO1FBRG1CLGFBQVEsR0FBUixRQUFRLENBQW9CO1FBaEJ6QyxlQUFVLEdBQUcsK0JBQStCLENBQUM7UUFDN0MsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLHFCQUFnQixHQUFHO1lBQ3RCLEtBQUssRUFBRSxHQUFHO1lBQ1YsS0FBSyxFQUFFLEdBQUc7WUFDVixJQUFJLEVBQUUsR0FBRztZQUNULEtBQUssRUFBRSxHQUFHO1lBQ1YsSUFBSSxFQUFFLEdBQUc7WUFDVCxVQUFVLEVBQUUsR0FBRztZQUNmLFVBQVUsRUFBRSxHQUFHO1NBQ2xCLENBQUM7UUFDSyxhQUFRLEdBQUcsT0FBTyxDQUFTLHdDQUF3QyxDQUFDLENBQUM7UUFPckUsU0FBSSxHQUFHLFVBQUMsS0FBZ0IsRUFBRSxPQUE0QixFQUN6RCxLQUFxQixFQUFFLElBQXFDO1lBRXhELDJDQUEyQztZQUMzQyxLQUFJLENBQUMsUUFBUSxDQUFDO2dCQUVWLDRGQUE0RjtnQkFDNUYsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDcEQsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ1YsQ0FBQyxDQUFBO0lBWEwsQ0FBQztJQUhhLDZCQUFPLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQWV6Qyw0QkFBQztDQUFBLEFBOUJELElBOEJDO0FBOUJZLHNEQUFxQjtBQWdDbEM7SUFXSSxnQkFBZ0I7SUFDaEIseUNBQW9CLE9BQTBCO1FBQTlDLGlCQUFrRDtRQUE5QixZQUFPLEdBQVAsT0FBTyxDQUFtQjtRQUV2QyxZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsWUFBWSxHQUFHO2dCQUNoQixXQUFXLEVBQUUsS0FBSSxDQUFDLEtBQUs7Z0JBQ3ZCLGNBQWMsRUFBRSxFQUFFLG1CQUFtQixFQUFFLFFBQVEsRUFBRTthQUNwRCxDQUFDO1lBRUQsS0FBSSxDQUFDLFlBQVksR0FBRztnQkFDakIsV0FBVyxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLO2dCQUNsRSxjQUFjLEVBQUUsRUFBRSxtQkFBbUIsRUFBRSxRQUFRLEVBQUU7YUFDcEQsQ0FBQztRQUNOLENBQUMsQ0FBQztJQVorQyxDQUFDO0lBYzNDLHVEQUFhLEdBQXBCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLGNBQVksSUFBSSxDQUFDLEtBQU8sSUFBRyx5Q0FBdUMsSUFBSSxDQUFDLEtBQU8sQ0FBQSxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQztJQUMzSCxDQUFDO0lBRU0sa0RBQVEsR0FBZjtRQUNJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUMsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFDTCxzQ0FBQztBQUFELENBQUMsQUFwQ0QsSUFvQ0MifQ==

/***/ }),
/* 127 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../../ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
var entityListItem_directive_1 = __webpack_require__(129);
var entityListItem_controller_1 = __webpack_require__(131);
exports.entityListItem = function (module) {
    module.component("swEntityListItem", entityListItem_directive_1.EntityListItem);
    module.controller("swEntityListItemCtrl", entityListItem_controller_1.EntityListItemController);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEscUNBQXFDOztBQUdyQyx1RUFBNEQ7QUFDNUQseUVBQXVFO0FBRTFELFFBQUEsY0FBYyxHQUFHLFVBQUMsTUFBZTtJQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLHlDQUFjLENBQUMsQ0FBQztJQUNyRCxNQUFNLENBQUMsVUFBVSxDQUFDLHNCQUFzQixFQUFFLG9EQUF3QixDQUFDLENBQUM7QUFDeEUsQ0FBQyxDQUFDIn0=

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(130);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @ngdoc directive
 * @name orion.directive:swEntityListItem
 * @restrict E
 *
 * @description
 * This directive implements node item template.
 *
 * @parameters
 * @param {SW.orion.INode} entity Entity object.
 * @param {Array<string>=} systemProps Array of names of system properties.
 * @param {Array<string>=} customProps Array of names of custom properties.
 * @param {string=} searchTerm Searching string to be highlighted.
 *
 * @example
 *   <example module="orion">
 *       <file src="src/directives/entityListItem/docs/swEntityListItem-example.html" name="index.html"></file>
 *       <file src="src/directives/entityListItem/docs/swEntityListItem-example.js" name="script.js"></file>
 *   </example>
 **/
var EntityListItem = /** @class */ (function () {
    function EntityListItem() {
        this.bindToController = {
            entity: "<",
            systemProps: "<?",
            customProps: "<?",
            searchTerm: "=?"
        };
        this.controller = "swEntityListItemCtrl";
        this.controllerAs = "vm";
        this.replace = true;
        this.restrict = "E";
        this.scope = {};
        this.templateUrl = "orion/directives/entityListItem/entityListItem-directive.html";
    }
    return EntityListItem;
}());
exports.EntityListItem = EntityListItem;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5TGlzdEl0ZW0tZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZW50aXR5TGlzdEl0ZW0tZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBbUJJO0FBRUo7SUFBQTtRQUNXLHFCQUFnQixHQUFHO1lBQ3RCLE1BQU0sRUFBRSxHQUFHO1lBQ1gsV0FBVyxFQUFFLElBQUk7WUFDakIsV0FBVyxFQUFFLElBQUk7WUFDakIsVUFBVSxFQUFFLElBQUk7U0FDbkIsQ0FBQztRQUNLLGVBQVUsR0FBRyxzQkFBc0IsQ0FBQztRQUNwQyxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNmLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxnQkFBVyxHQUFHLCtEQUErRCxDQUFDO0lBQ3pGLENBQUM7SUFBRCxxQkFBQztBQUFELENBQUMsQUFiRCxJQWFDO0FBYlksd0NBQWMifQ==

/***/ }),
/* 130 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var EntityListItemController = /** @class */ (function () {
    function EntityListItemController() {
        var _this = this;
        this.getPropsValues = function (props) {
            if (props) {
                var propsValues = [];
                for (var i = 0; i <= props.length; i++) {
                    //Since names of properties can be in camelCase or PascalCase this will check for both cases here
                    var value = _this.entity[props[i]] || _this.entity[_.lowerFirst(props[i])];
                    if (value) {
                        propsValues.push(value);
                    }
                }
                return propsValues;
            }
        };
    }
    EntityListItemController.prototype.getEntityTitle = function () {
        if (angular.isString(this.entity.caption)) {
            return this.entity.caption;
        }
        if (angular.isString(this.entity.displayName)) {
            return this.entity.displayName;
        }
        return null;
    };
    return EntityListItemController;
}());
exports.EntityListItemController = EntityListItemController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5TGlzdEl0ZW0tY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVudGl0eUxpc3RJdGVtLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUFBO1FBQUEsaUJBZ0NDO1FBMUJVLG1CQUFjLEdBQUcsVUFBQyxLQUFlO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ1IsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUVyQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztvQkFDckMsaUdBQWlHO29CQUNqRyxJQUFJLEtBQUssR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUUxRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUNSLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzVCLENBQUM7Z0JBQ0wsQ0FBQztnQkFFRCxNQUFNLENBQUMsV0FBVyxDQUFDO1lBQ3ZCLENBQUM7UUFDTCxDQUFDLENBQUM7SUFXTixDQUFDO0lBVFUsaURBQWMsR0FBckI7UUFDSSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUMvQixDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1QyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDbkMsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUNMLCtCQUFDO0FBQUQsQ0FBQyxBQWhDRCxJQWdDQztBQWhDWSw0REFBd0IifQ==

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var viewInfo_1 = __webpack_require__(133);
exports.default = function (module) {
    module.component("swViewInfo", viewInfo_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVDQUFrQztBQUVsQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsa0JBQVEsQ0FBQyxDQUFDO0FBQzdDLENBQUMsQ0FBQyJ9

/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ViewInfo = /** @class */ (function () {
    /** @ngInject */
    ViewInfo.$inject = ["swOrionViewService", "$log"];
    function ViewInfo(swOrionViewService, $log) {
        var _this = this;
        this.swOrionViewService = swOrionViewService;
        this.$log = $log;
        this.restrict = "A";
        this.compile = function (el, attr) {
            if (attr["swViewInfo"]) {
                try {
                    _this.swOrionViewService.activeViewInfo = angular.fromJson(attr["swViewInfo"]);
                    if (_this.swOrionViewService.activeViewInfo && _this.swOrionViewService.activeViewInfo.readOnly) {
                        angular.element("body").addClass("sw-dashboard--read-only");
                    }
                }
                catch (ex) {
                    _this.$log.error("Error encountered parsing orion view information:", attr["swViewInfo"], ex);
                }
            }
            return {};
        };
    }
    return ViewInfo;
}());
exports.default = ViewInfo;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlld0luZm8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aWV3SW5mby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGtCQUFvQixrQkFBeUMsRUFBVSxJQUFpQjtRQUF4RixpQkFBNEY7UUFBeEUsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUF1QjtRQUFVLFNBQUksR0FBSixJQUFJLENBQWE7UUFFakYsYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUVmLFlBQU8sR0FBRyxVQUFDLEVBQXVCLEVBQUUsSUFBb0I7WUFDM0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckIsSUFBSSxDQUFDO29CQUNELEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDOUUsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsSUFBSSxLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7d0JBQzVGLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUM7b0JBQ2hFLENBQUM7Z0JBRUwsQ0FBQztnQkFBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNWLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDakcsQ0FBQztZQUNMLENBQUM7WUFDRCxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ2QsQ0FBQyxDQUFBO0lBakIwRixDQUFDO0lBa0JoRyxlQUFDO0FBQUQsQ0FBQyxBQXBCRCxJQW9CQyJ9

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dashboardFunctions_directive_1 = __webpack_require__(135);
function dashboardFunctions(module) {
    module.component("swDashboardFunctions", dashboardFunctions_directive_1.DashboardFunctions);
}
exports.dashboardFunctions = dashboardFunctions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtFQUFvRTtBQUVwRSw0QkFBbUMsTUFBZTtJQUM5QyxNQUFNLENBQUMsU0FBUyxDQUFDLHNCQUFzQixFQUFFLGlEQUFrQixDQUFDLENBQUM7QUFDakUsQ0FBQztBQUZELGdEQUVDIn0=

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DashboardFunctions = /** @class */ (function () {
    /** @ngInject */
    DashboardFunctions.$inject = ["$rootScope", "$window", "swDemoService"];
    function DashboardFunctions($rootScope, $window, swDemoService) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$window = $window;
        this.swDemoService = swDemoService;
        this.restrict = "EA";
        this.scope = false;
        this.link = function (scope, element, attrs) {
            _this.$rootScope.editDashboard = function () {
                _this.$rootScope.editingDashboard = true;
                _this.$rootScope.dashboardMenuOpen = false;
            };
            _this.$rootScope.legacyEditDashboard = function (url) {
                if (_this.swDemoService.isDemoMode()) {
                    _this.swDemoService.showDemoErrorToast();
                    return;
                }
                _this.$window.location.href = url;
            };
            _this.$rootScope.navToSubview = function (url, isAddTab) {
                if (isAddTab && _this.swDemoService.isDemoMode()) {
                    _this.swDemoService.showDemoErrorToast();
                    return;
                }
                _this.$window.location.href = url;
            };
        };
    }
    return DashboardFunctions;
}());
exports.DashboardFunctions = DashboardFunctions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkRnVuY3Rpb25zLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhc2hib2FyZEZ1bmN0aW9ucy1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFVQTtJQUVJLGdCQUFnQjtJQUNoQiw0QkFBb0IsVUFBd0MsRUFDeEMsT0FBMEIsRUFDMUIsYUFBMkI7UUFGL0MsaUJBR0M7UUFIbUIsZUFBVSxHQUFWLFVBQVUsQ0FBOEI7UUFDeEMsWUFBTyxHQUFQLE9BQU8sQ0FBbUI7UUFDMUIsa0JBQWEsR0FBYixhQUFhLENBQWM7UUFHeEMsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBRWQsU0FBSSxHQUFHLFVBQUMsS0FBZ0IsRUFBRSxPQUE0QixFQUFFLEtBQXFCO1lBQ2hGLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxHQUFHO2dCQUM1QixLQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDeEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDOUMsQ0FBQyxDQUFDO1lBRUYsS0FBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsR0FBRyxVQUFDLEdBQVc7Z0JBQzlDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNsQyxLQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixFQUFFLENBQUM7b0JBQ3hDLE1BQU0sQ0FBQztnQkFDWCxDQUFDO2dCQUNELEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7WUFDckMsQ0FBQyxDQUFDO1lBRUYsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEdBQUcsVUFBQyxHQUFXLEVBQUUsUUFBaUI7Z0JBQzFELEVBQUUsQ0FBQyxDQUFDLFFBQVEsSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDOUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO29CQUN4QyxNQUFNLENBQUM7Z0JBQ1gsQ0FBQztnQkFDRCxLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1lBQ3JDLENBQUMsQ0FBQztRQUNOLENBQUMsQ0FBQztJQTFCRixDQUFDO0lBMkJMLHlCQUFDO0FBQUQsQ0FBQyxBQWpDRCxJQWlDQztBQWpDWSxnREFBa0IifQ==

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var entityManagementNewPageMessage_directive_1 = __webpack_require__(137);
var entityManagementNewPageMessage_controller_1 = __webpack_require__(138);
exports.default = function (module) {
    module.controller("swEntityManagementNewPageMessageController", entityManagementNewPageMessage_controller_1.default);
    module.component("swEntityManagementNewPageMessage", entityManagementNewPageMessage_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVHQUF3RjtBQUN4Rix5R0FBbUc7QUFFbkcsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsNENBQTRDLEVBQUUsbURBQXdDLENBQUMsQ0FBQztJQUMxRyxNQUFNLENBQUMsU0FBUyxDQUFDLGtDQUFrQyxFQUFDLGtEQUE4QixDQUFDLENBQUM7QUFDeEYsQ0FBQyxDQUFDIn0=

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var EntityManagementNewPageMessage = /** @class */ (function () {
    function EntityManagementNewPageMessage() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/entityManagementNewPageMessage/entityManagementNewPageMessage-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            onDismiss: "&?"
        };
        this.controller = "swEntityManagementNewPageMessageController";
        this.controllerAs = "vm";
    }
    return EntityManagementNewPageMessage;
}());
exports.default = EntityManagementNewPageMessage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5TWFuYWdlbWVudE5ld1BhZ2VNZXNzYWdlLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVudGl0eU1hbmFnZW1lbnROZXdQYWdlTWVzc2FnZS1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFFdkM7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixnQkFBVyxHQUNsQiwrRkFBK0YsQ0FBQztRQUN6RixZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLHFCQUFnQixHQUFHO1lBQ3RCLFNBQVMsRUFBRSxJQUFJO1NBQ2xCLENBQUM7UUFDSyxlQUFVLEdBQUcsNENBQTRDLENBQUM7UUFDMUQsaUJBQVksR0FBRyxJQUFJLENBQUM7SUFDL0IsQ0FBQztJQUFELHFDQUFDO0FBQUQsQ0FBQyxBQVhELElBV0MifQ==

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var userSettingName = "WebNodeManagement_NewPageNotificationDismissed";
var userSettingValue = true;
var EntityManagementNewPageMessageController = /** @class */ (function () {
    function EntityManagementNewPageMessageController(_t, webAdminService) {
        var _this = this;
        this._t = _t;
        this.webAdminService = webAdminService;
        this.$onInit = function () {
            _this.msgText = _this._t("You can switch to the Manage Entities page.");
            _this.linkText = _this._t("Show me");
        };
    }
    EntityManagementNewPageMessageController.prototype.onDismissInternal = function () {
        if (angular.isFunction(this.onDismiss)) {
            this.onDismiss();
        }
        this.webAdminService.saveUserSetting(userSettingName, userSettingValue);
    };
    EntityManagementNewPageMessageController.$inject = [
        "getTextService",
        "webAdminService"
    ];
    return EntityManagementNewPageMessageController;
}());
exports.default = EntityManagementNewPageMessageController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5TWFuYWdlbWVudE5ld1BhZ2VNZXNzYWdlLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnRpdHlNYW5hZ2VtZW50TmV3UGFnZU1lc3NhZ2UtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUl2QyxJQUFNLGVBQWUsR0FBRyxnREFBZ0QsQ0FBQztBQUN6RSxJQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQztBQUU5QjtJQVVJLGtEQUFvQixFQUFvQyxFQUM1QyxlQUFpQztRQUQ3QyxpQkFFSTtRQUZnQixPQUFFLEdBQUYsRUFBRSxDQUFrQztRQUM1QyxvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFHdEMsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLDZDQUE2QyxDQUFDLENBQUM7WUFDdEUsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztJQUxDLENBQUM7SUFPRyxvRUFBaUIsR0FBeEI7UUFDSSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLENBQUM7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBeEJhLGdEQUFPLEdBQUc7UUFDaEIsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtLQUNwQixDQUFDO0lBc0JWLCtDQUFDO0NBQUEsQUExQkQsSUEwQkM7a0JBMUJvQix3Q0FBd0MifQ==

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var entityManagementNewPageSwitcher_directive_1 = __webpack_require__(140);
var entityManagementNewPageSwitcher_controller_1 = __webpack_require__(141);
exports.default = function (module) {
    module.controller("swEntityManagementNewPageSwitcherController", entityManagementNewPageSwitcher_controller_1.default);
    module.component("swEntityManagementNewPageSwitcher", entityManagementNewPageSwitcher_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlHQUEwRjtBQUMxRiwyR0FBcUc7QUFFckcsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsNkNBQTZDLEVBQUUsb0RBQXlDLENBQUMsQ0FBQztJQUM1RyxNQUFNLENBQUMsU0FBUyxDQUFDLG1DQUFtQyxFQUFFLG1EQUErQixDQUFDLENBQUM7QUFDM0YsQ0FBQyxDQUFDIn0=

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var EntityManagementNewPageSwitcher = /** @class */ (function () {
    function EntityManagementNewPageSwitcher() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/entityManagementNewPageSwitcher/entityManagementNewPageSwitcher-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            linkIcon: "@?",
            linkText: "@?"
        };
        this.controller = "swEntityManagementNewPageSwitcherController";
        this.controllerAs = "vm";
    }
    return EntityManagementNewPageSwitcher;
}());
exports.default = EntityManagementNewPageSwitcher;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5TWFuYWdlbWVudE5ld1BhZ2VTd2l0Y2hlci1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnRpdHlNYW5hZ2VtZW50TmV3UGFnZVN3aXRjaGVyLWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QztJQUFBO1FBQ1csYUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNmLGdCQUFXLEdBQ2xCLGlHQUFpRyxDQUFDO1FBQzNGLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gscUJBQWdCLEdBQUc7WUFDdEIsUUFBUSxFQUFFLElBQUk7WUFDZCxRQUFRLEVBQUUsSUFBSTtTQUNqQixDQUFDO1FBQ0ssZUFBVSxHQUFHLDZDQUE2QyxDQUFDO1FBQzNELGlCQUFZLEdBQUcsSUFBSSxDQUFDO0lBQy9CLENBQUM7SUFBRCxzQ0FBQztBQUFELENBQUMsQUFaRCxJQVlDIn0=

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var usPageSwitchName = "WebNodeManagement_UseLegacyPage";
var usPageSwitchValue = false;
var usHideMessageName = "WebNodeManagement_NewPageNotificationDismissed";
var usHideMessageValue = true;
var EntityManagementNewPageSwitcherController = /** @class */ (function () {
    function EntityManagementNewPageSwitcherController(_t, $window, webAdminService) {
        var _this = this;
        this._t = _t;
        this.$window = $window;
        this.webAdminService = webAdminService;
        this.$onInit = function () {
            _this.pageLink = "/ui/manage/nodes";
            _this.linkDefaultText = _this._t("Manage Entities");
        };
    }
    EntityManagementNewPageSwitcherController.prototype.switch = function () {
        var _this = this;
        this.webAdminService.saveUserSetting(usPageSwitchName, usPageSwitchValue)
            .then(function () { _this.webAdminService.saveUserSetting(usHideMessageName, usHideMessageValue); })
            .then(function () { _this.$window.open(_this.pageLink, "_self"); });
    };
    EntityManagementNewPageSwitcherController.$inject = [
        "getTextService",
        "$window",
        "webAdminService"
    ];
    return EntityManagementNewPageSwitcherController;
}());
exports.default = EntityManagementNewPageSwitcherController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5TWFuYWdlbWVudE5ld1BhZ2VTd2l0Y2hlci1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZW50aXR5TWFuYWdlbWVudE5ld1BhZ2VTd2l0Y2hlci1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBSXZDLElBQU0sZ0JBQWdCLEdBQUcsaUNBQWlDLENBQUM7QUFDM0QsSUFBTSxpQkFBaUIsR0FBRyxLQUFLLENBQUM7QUFFaEMsSUFBTSxpQkFBaUIsR0FBRyxnREFBZ0QsQ0FBQztBQUMzRSxJQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQztBQUVoQztJQVlJLG1EQUFvQixFQUFvQyxFQUM1QyxPQUEwQixFQUMxQixlQUFpQztRQUY3QyxpQkFHSTtRQUhnQixPQUFFLEdBQUYsRUFBRSxDQUFrQztRQUM1QyxZQUFPLEdBQVAsT0FBTyxDQUFtQjtRQUMxQixvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFHdEMsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQztZQUNuQyxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUM7SUFMQyxDQUFDO0lBT0csMERBQU0sR0FBYjtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLEVBQUUsaUJBQWlCLENBQUM7YUFDeEUsSUFBSSxDQUFDLGNBQVEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM1RixJQUFJLENBQUMsY0FBUSxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQXpCYSxpREFBTyxHQUFHO1FBQ2hCLGdCQUFnQjtRQUNoQixTQUFTO1FBQ1QsaUJBQWlCO0tBQ3BCLENBQUM7SUFzQlYsZ0RBQUM7Q0FBQSxBQTNCRCxJQTJCQztrQkEzQm9CLHlDQUF5QyJ9

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var lazyLoadModule_controller_1 = __webpack_require__(143);
var lazyLoadModule_directive_1 = __webpack_require__(144);
exports.default = function (module) {
    module.controller("swLazyLoadModuleController", lazyLoadModule_controller_1.default);
    module.component("swLazyLoadModule", lazyLoadModule_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlFQUFtRTtBQUNuRSx1RUFBaUU7QUFFakUsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsNEJBQTRCLEVBQUUsbUNBQXdCLENBQUMsQ0FBQztJQUMxRSxNQUFNLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLGtDQUF1QixDQUFDLENBQUM7QUFDbEUsQ0FBQyxDQUFDIn0=

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var LazyLoadModuleController = /** @class */ (function () {
    function LazyLoadModuleController(swApi, $element, $scope, $compile, $log) {
        var _this = this;
        this.swApi = swApi;
        this.$element = $element;
        this.$scope = $scope;
        this.$compile = $compile;
        this.$log = $log;
        this.$onInit = function () {
            _this.swApi.ui
                .one("scripts/list/" + _this.swLazyLoadModule)
                .get()
                .then(function (value) {
                _this.$scope.lazyLoadParams = value;
                var el = _this.$compile("<div oc-lazy-load='lazyLoadParams'>" + _this.$element.html() + "</div>")(_this.$scope);
                _this.$element.html("");
                _this.$element.append(el);
            }, function (error) {
                _this.$log.error("Module '" + _this.swLazyLoadModule + "' couldn't be loaded because of: " + error);
            });
        };
    }
    LazyLoadModuleController.$inject = ["swApi", "$element", "$scope", "$compile", "$log"];
    return LazyLoadModuleController;
}());
exports.default = LazyLoadModuleController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF6eUxvYWRNb2R1bGUtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxhenlMb2FkTW9kdWxlLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFJdkM7SUFLSSxrQ0FDWSxLQUFvQixFQUNwQixRQUE2QixFQUM3QixNQUFpQixFQUNqQixRQUE0QixFQUM1QixJQUFvQjtRQUxoQyxpQkFNQztRQUxXLFVBQUssR0FBTCxLQUFLLENBQWU7UUFDcEIsYUFBUSxHQUFSLFFBQVEsQ0FBcUI7UUFDN0IsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixhQUFRLEdBQVIsUUFBUSxDQUFvQjtRQUM1QixTQUFJLEdBQUosSUFBSSxDQUFnQjtRQUd6QixZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7aUJBQ1IsR0FBRyxDQUFDLGtCQUFnQixLQUFJLENBQUMsZ0JBQWtCLENBQUM7aUJBQzVDLEdBQUcsRUFBRTtpQkFDTCxJQUFJLENBQUMsVUFBQyxLQUFlO2dCQUNsQixLQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7Z0JBQ25DLElBQU0sRUFBRSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsd0NBQXNDLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFdBQVEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdCLENBQUMsRUFBRSxVQUFDLEtBQUs7Z0JBQ0wsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBVyxLQUFJLENBQUMsZ0JBQWdCLHlDQUFvQyxLQUFPLENBQUMsQ0FBQztZQUNqRyxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztJQWRGLENBQUM7SUFSYSxnQ0FBTyxHQUFHLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBdUJoRiwrQkFBQztDQUFBLEFBMUJELElBMEJDO2tCQTFCb0Isd0JBQXdCIn0=

/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var LazyLoadModuleDirective = /** @class */ (function () {
    function LazyLoadModuleDirective() {
        this.restrict = "A";
        this.transclude = false;
        this.scope = {};
        this.bindToController = {
            swLazyLoadModule: "@"
        };
        this.controller = "swLazyLoadModuleController";
        this.controllerAs = "vm";
    }
    return LazyLoadModuleDirective;
}());
exports.default = LazyLoadModuleDirective;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF6eUxvYWRNb2R1bGUtZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGF6eUxvYWRNb2R1bGUtZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDO0lBQUE7UUFDVyxhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gscUJBQWdCLEdBQUc7WUFDdEIsZ0JBQWdCLEVBQUUsR0FBRztTQUN4QixDQUFDO1FBQ0ssZUFBVSxHQUFHLDRCQUE0QixDQUFDO1FBQzFDLGlCQUFZLEdBQUcsSUFBSSxDQUFDO0lBQy9CLENBQUM7SUFBRCw4QkFBQztBQUFELENBQUMsQUFURCxJQVNDIn0=

/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionWebsite_directive_1 = __webpack_require__(146);
var orionWebsite_controller_1 = __webpack_require__(147);
exports.default = function (module) {
    module.controller("swOrionWebsiteController", orionWebsite_controller_1.default);
    module.component("swOrionWebsite", orionWebsite_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1FQUFzRDtBQUN0RCxxRUFBaUU7QUFFakUsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsMEJBQTBCLEVBQUUsaUNBQXdCLENBQUMsQ0FBQztJQUN4RSxNQUFNLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFDLGdDQUFjLENBQUMsQ0FBQztBQUN0RCxDQUFDLENBQUMifQ==

/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionWebsite = /** @class */ (function () {
    function OrionWebsite() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/orionWebsite/orionWebsite-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            options: "@"
        };
        this.controller = "swOrionWebsiteController";
        this.controllerAs = "shell";
    }
    return OrionWebsite;
}());
exports.default = OrionWebsite;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25XZWJzaXRlLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9yaW9uV2Vic2l0ZS1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFFdkM7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixnQkFBVyxHQUNsQiwyREFBMkQsQ0FBQztRQUNyRCxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLHFCQUFnQixHQUFHO1lBQ3RCLE9BQU8sRUFBRSxHQUFHO1NBQ2YsQ0FBQztRQUNLLGVBQVUsR0FBRywwQkFBMEIsQ0FBQztRQUN4QyxpQkFBWSxHQUFHLE9BQU8sQ0FBQztJQUNsQyxDQUFDO0lBQUQsbUJBQUM7QUFBRCxDQUFDLEFBWEQsSUFXQyJ9

/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionWebsiteController = /** @class */ (function () {
    function OrionWebsiteController($rootScope, $state, $window, constants, getTextServiceEx, viewService, webAdmin, swDemoService, swAuthService) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$window = $window;
        this.constants = constants;
        this.getTextServiceEx = getTextServiceEx;
        this.viewService = viewService;
        this.webAdmin = webAdmin;
        this.swDemoService = swDemoService;
        this.swAuthService = swAuthService;
        this.isBusy = true;
        this.isLoadingMenus = true;
        this.totalAlerts = 0;
        this.user = this.constants.user;
        this.now = Date.now();
        this.$onInit = function () {
            _this.websiteOptions = JSON.parse(_this.options);
            _this.$rootScope.isBusy = true;
            _this.activate();
        };
        this.isCurrentPage = function (page) {
            return _this.$state.is("views." + page.type, { viewName: page.name });
        };
        this.logout = function () {
            _this.$window.location.href = "/Orion/Logout.aspx";
        };
        this.shouldDisplayEval = function () {
            if (_this.websiteOptions.licenseSummary) {
                if (_this.websiteOptions.licenseSummary.evalModuleCount > 0 ||
                    _this.websiteOptions.licenseSummary.expiredModuleCount > 0) {
                    return true;
                }
            }
            return false;
        };
        this.activate = function () {
            _this.$rootScope.$on("$stateChangeSuccess", _this.onStateChangeSuccess);
            _this.$rootScope.$watch("isBusy", function (newValue, oldValue) {
                _this.isBusy = newValue;
            });
            //TODO add real interfaces when ViewService in converted to ts
            _this.user = _this.webAdmin.getUser();
            if (_this.showNewMegamenu() === true) {
                _this.isBusy = _this.isLoadingMenus = false;
                return;
            }
            _this.viewService.getViews()
                .then(function (viewGroups) {
                _this.viewGroups = viewGroups;
                var dashboardGroup = _.find(viewGroups, { name: "dashboards" });
                if (!!dashboardGroup) {
                    _this.dashboardGroups = dashboardGroup.viewGroups;
                }
                if (!_this.user.AllowAdmin && !_this.swDemoService.isDemoMode()) {
                    _.remove(_this.viewGroups, { name: "settings" });
                }
            }).finally(function () {
                _this.isBusy = _this.isLoadingMenus = false;
            });
        };
        this.onStateChangeSuccess = function (event, toState) {
            // it is not actually possible to use services in state.configure
            // so, let's fill that gap here
            if (!!toState && !!toState.i18Title) {
                _this.$rootScope.title =
                    toState.title = _this.getTextServiceEx.translateIfNeeded(toState.i18Title);
                delete toState.i18Title; // unset i18Title, so Xui does not see it
            }
            // update page title based on current state
            if (!!toState) {
                _this.title = toState.title || "!title not specified!";
                _this.commands = toState.data ? toState.data.commands : null;
                _this.alerts = function () { return toState.data ? toState.data.alerts : 0; };
            }
        };
        this.getUserLabel = function () {
            var ADGroupAccountMember = 4;
            var label = _this.user.AccountID;
            if (_this.user.AccountType === ADGroupAccountMember && _this.user.GroupInfo) {
                label = label + " - " + _this.user.GroupInfo;
            }
            return label;
        };
        this.showNewMegamenu = function () {
            return _this.websiteOptions.displayHeaderAndFooter;
        };
    }
    OrionWebsiteController.$inject = [
        "$rootScope", "$state", "$window",
        "constants", "getTextServiceEx", "viewService", "webAdminService", "swDemoService", "swAuthService"
    ];
    return OrionWebsiteController;
}());
exports.default = OrionWebsiteController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25XZWJzaXRlLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmlvbldlYnNpdGUtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQWF2QztJQW1CSSxnQ0FBb0IsVUFBc0IsRUFDdEIsTUFBcUIsRUFDckIsT0FBdUIsRUFDdkIsU0FBcUIsRUFDckIsZ0JBQTRDLEVBQzVDLFdBQXlCLEVBQ3pCLFFBQTBCLEVBQzFCLGFBQTJCLEVBQzNCLGFBQTJCO1FBUi9DLGlCQVNDO1FBVG1CLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUN2QixjQUFTLEdBQVQsU0FBUyxDQUFZO1FBQ3JCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBNEI7UUFDNUMsZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFDekIsYUFBUSxHQUFSLFFBQVEsQ0FBa0I7UUFDMUIsa0JBQWEsR0FBYixhQUFhLENBQWM7UUFDM0Isa0JBQWEsR0FBYixhQUFhLENBQWM7UUFuQnZDLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFDZCxtQkFBYyxHQUFHLElBQUksQ0FBQztRQUd0QixnQkFBVyxHQUFHLENBQUMsQ0FBQztRQUNoQixTQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFHM0IsUUFBRyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQWNsQixZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pDLEtBQUksQ0FBQyxVQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNyQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDcEIsQ0FBQyxDQUFDO1FBRUssa0JBQWEsR0FBRyxVQUFDLElBQW9DO1lBQ3hELE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUN6RSxDQUFDLENBQUM7UUFFSyxXQUFNLEdBQUc7WUFDWixLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsb0JBQW9CLENBQUM7UUFDdEQsQ0FBQyxDQUFDO1FBRUssc0JBQWlCLEdBQUc7WUFDdkIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsQ0FBQztvQkFDdEQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEQsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDaEIsQ0FBQztZQUNULENBQUM7WUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUMsQ0FBQTtRQUVPLGFBQVEsR0FBRztZQUNmLEtBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3RFLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxVQUFDLFFBQWlCLEVBQUUsUUFBaUI7Z0JBQ2xFLEtBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDO1lBQzNCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsOERBQThEO1lBQzlELEtBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUVwQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxFQUFFLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDbEMsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztnQkFDMUMsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUVELEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFO2lCQUN0QixJQUFJLENBQUMsVUFBQyxVQUFlO2dCQUNsQixLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFDN0IsSUFBTSxjQUFjLEdBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztnQkFDdkUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQ25CLEtBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQztnQkFDckQsQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzVELENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDO2dCQUNwRCxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2dCQUNQLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDOUMsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFFTSx5QkFBb0IsR0FBRyxVQUFDLEtBQW9CLEVBQUUsT0FBWTtZQUM5RCxpRUFBaUU7WUFDakUsK0JBQStCO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUs7b0JBQ2pCLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDOUUsT0FBTyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMseUNBQXlDO1lBQ3RFLENBQUM7WUFDRCwyQ0FBMkM7WUFDM0MsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ1osS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxJQUFJLHVCQUF1QixDQUFDO2dCQUN0RCxLQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQzVELEtBQUksQ0FBQyxNQUFNLEdBQUcsY0FBTSxPQUFBLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQXRDLENBQXNDLENBQUM7WUFDL0QsQ0FBQztRQUNMLENBQUMsQ0FBQTtRQUVNLGlCQUFZLEdBQUc7WUFDbEIsSUFBTSxvQkFBb0IsR0FBVyxDQUFDLENBQUM7WUFDdkMsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFFaEMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssb0JBQW9CLElBQUksS0FBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxLQUFLLEdBQU0sS0FBSyxXQUFNLEtBQUksQ0FBQyxJQUFJLENBQUMsU0FBVyxDQUFDO1lBQ2hELENBQUM7WUFFRCxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUMsQ0FBQTtRQUVNLG9CQUFlLEdBQUc7WUFDckIsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUM7UUFDdEQsQ0FBQyxDQUFBO0lBbkZELENBQUM7SUEzQmEsOEJBQU8sR0FBRztRQUNwQixZQUFZLEVBQUUsUUFBUSxFQUFFLFNBQVM7UUFDakMsV0FBVyxFQUFFLGtCQUFrQixFQUFFLGFBQWEsRUFBRSxpQkFBaUIsRUFBRSxlQUFlLEVBQUUsZUFBZTtLQUN0RyxDQUFDO0lBNEdOLDZCQUFDO0NBQUEsQUFoSEQsSUFnSEM7a0JBaEhvQixzQkFBc0IifQ==

/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var newPageMessage_directive_1 = __webpack_require__(149);
var newPageMessage_controller_1 = __webpack_require__(150);
exports.default = function (module) {
    module.controller("swNewPageMessageController", newPageMessage_controller_1.default);
    module.component("swNewPageMessage", newPageMessage_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVFQUF3RDtBQUN4RCx5RUFBbUU7QUFFbkUsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsNEJBQTRCLEVBQUUsbUNBQXdCLENBQUMsQ0FBQztJQUMxRSxNQUFNLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLGtDQUFjLENBQUMsQ0FBQztBQUN6RCxDQUFDLENBQUMifQ==

/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NewPageMessage = /** @class */ (function () {
    function NewPageMessage() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/newPageMessage/newPageMessage-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            onDismiss: "&?",
            dismissMessageSettingName: "@?",
            dismissMessageSettingValue: "@?",
            useLegacyPageSettingName: "@?",
            useLegacyPageSettingValue: "@?",
            linkText: "@?",
            linkAddress: "@?",
            messageText: "@?"
        };
        this.controller = "swNewPageMessageController";
        this.controllerAs = "vm";
    }
    return NewPageMessage;
}());
exports.default = NewPageMessage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3UGFnZU1lc3NhZ2UtZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibmV3UGFnZU1lc3NhZ2UtZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDO0lBQUE7UUFDVyxhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsZ0JBQVcsR0FDbEIsK0RBQStELENBQUM7UUFDekQsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxxQkFBZ0IsR0FBRztZQUN0QixTQUFTLEVBQUUsSUFBSTtZQUNmLHlCQUF5QixFQUFFLElBQUk7WUFDL0IsMEJBQTBCLEVBQUUsSUFBSTtZQUNoQyx3QkFBd0IsRUFBRSxJQUFJO1lBQzlCLHlCQUF5QixFQUFFLElBQUk7WUFDL0IsUUFBUSxFQUFFLElBQUk7WUFDZCxXQUFXLEVBQUUsSUFBSTtZQUNqQixXQUFXLEVBQUUsSUFBSTtTQUNwQixDQUFDO1FBQ0ssZUFBVSxHQUFHLDRCQUE0QixDQUFDO1FBQzFDLGlCQUFZLEdBQUcsSUFBSSxDQUFDO0lBQy9CLENBQUM7SUFBRCxxQkFBQztBQUFELENBQUMsQUFsQkQsSUFrQkMifQ==

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NewPageMessageController = /** @class */ (function () {
    function NewPageMessageController(_t, webAdminService) {
        var _this = this;
        this._t = _t;
        this.webAdminService = webAdminService;
        this.$onInit = function () {
            _this.linkText = "default link text";
        };
    }
    NewPageMessageController.prototype.onDismissInternal = function () {
        if (angular.isFunction(this.onDismiss)) {
            this.onDismiss();
        }
        this.webAdminService.saveUserSetting(this.dismissMessageSettingName, this.dismissMessageSettingValue);
    };
    NewPageMessageController.$inject = [
        "getTextService",
        "webAdminService"
    ];
    return NewPageMessageController;
}());
exports.default = NewPageMessageController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3UGFnZU1lc3NhZ2UtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5ld1BhZ2VNZXNzYWdlLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFJdkM7SUFlSSxrQ0FBb0IsRUFBb0MsRUFDNUMsZUFBaUM7UUFEN0MsaUJBRUk7UUFGZ0IsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDNUMsb0JBQWUsR0FBZixlQUFlLENBQWtCO1FBR3RDLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxRQUFRLEdBQUcsbUJBQW1CLENBQUM7UUFDeEMsQ0FBQyxDQUFDO0lBSkMsQ0FBQztJQU1HLG9EQUFpQixHQUF4QjtRQUNJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQztRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQztJQUMxRyxDQUFDO0lBNUJhLGdDQUFPLEdBQUc7UUFDaEIsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtLQUNwQixDQUFDO0lBMEJWLCtCQUFDO0NBQUEsQUE5QkQsSUE4QkM7a0JBOUJvQix3QkFBd0IifQ==

/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var newPageSwitcher_directive_1 = __webpack_require__(152);
var newPageSwitcher_controller_1 = __webpack_require__(153);
exports.default = function (module) {
    module.controller("swNewPageSwitcherController", newPageSwitcher_controller_1.default);
    module.component("swNewPageSwitcher", newPageSwitcher_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlFQUEwRDtBQUMxRCwyRUFBcUU7QUFFckUsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsNkJBQTZCLEVBQUUsb0NBQXlCLENBQUMsQ0FBQztJQUM1RSxNQUFNLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLG1DQUFlLENBQUMsQ0FBQztBQUMzRCxDQUFDLENBQUMifQ==

/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NewPageSwitcher = /** @class */ (function () {
    function NewPageSwitcher() {
        this.restrict = "E";
        this.templateUrl = "orion/directives/newPageSwitcher/newPageSwitcher-directive.html";
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            linkIcon: "@?",
            linkText: "@?",
            linkAddress: "@?",
            pageLink: "@?",
            dismissMessageSettingName: "@?",
            dismissMessageSettingValue: "@?",
            useLegacyPageSettingName: "@?",
            useLegacyPageSettingValue: "@?"
        };
        this.controller = "swNewPageSwitcherController";
        this.controllerAs = "vm";
    }
    return NewPageSwitcher;
}());
exports.default = NewPageSwitcher;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3UGFnZVN3aXRjaGVyLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5ld1BhZ2VTd2l0Y2hlci1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFFdkM7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixnQkFBVyxHQUNsQixpRUFBaUUsQ0FBQztRQUMzRCxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLHFCQUFnQixHQUFHO1lBQ3RCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxXQUFXLEVBQUUsSUFBSTtZQUNqQixRQUFRLEVBQUUsSUFBSTtZQUNkLHlCQUF5QixFQUFFLElBQUk7WUFDL0IsMEJBQTBCLEVBQUUsSUFBSTtZQUNoQyx3QkFBd0IsRUFBRSxJQUFJO1lBQzlCLHlCQUF5QixFQUFFLElBQUk7U0FDbEMsQ0FBQztRQUNLLGVBQVUsR0FBRyw2QkFBNkIsQ0FBQztRQUMzQyxpQkFBWSxHQUFHLElBQUksQ0FBQztJQUMvQixDQUFDO0lBQUQsc0JBQUM7QUFBRCxDQUFDLEFBbEJELElBa0JDIn0=

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NewPageSwitcherController = /** @class */ (function () {
    function NewPageSwitcherController(_t, $window, webAdminService) {
        var _this = this;
        this._t = _t;
        this.$window = $window;
        this.webAdminService = webAdminService;
        this.$onInit = function () {
            _this.linkText = "Default Link Text";
        };
    }
    NewPageSwitcherController.prototype.switch = function () {
        var _this = this;
        this.webAdminService.saveUserSetting(this.useLegacyPageSettingName, this.useLegacyPageSettingValue)
            .then(function () { _this.webAdminService.saveUserSetting(_this.dismissMessageSettingName, _this.dismissMessageSettingValue); })
            .then(function () { _this.$window.open(_this.linkAddress, "_self"); });
    };
    NewPageSwitcherController.$inject = [
        "getTextService",
        "$window",
        "webAdminService"
    ];
    return NewPageSwitcherController;
}());
exports.default = NewPageSwitcherController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3UGFnZVN3aXRjaGVyLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuZXdQYWdlU3dpdGNoZXItY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUl2QztJQWdCSSxtQ0FBb0IsRUFBb0MsRUFDNUMsT0FBMEIsRUFDMUIsZUFBaUM7UUFGN0MsaUJBR0k7UUFIZ0IsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDNUMsWUFBTyxHQUFQLE9BQU8sQ0FBbUI7UUFDMUIsb0JBQWUsR0FBZixlQUFlLENBQWtCO1FBR3RDLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxRQUFRLEdBQUcsbUJBQW1CLENBQUM7UUFDeEMsQ0FBQyxDQUFDO0lBSkMsQ0FBQztJQU1HLDBDQUFNLEdBQWI7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMseUJBQXlCLENBQUM7YUFDbEcsSUFBSSxDQUFDLGNBQVEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLHlCQUF5QixFQUFFLEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3RILElBQUksQ0FBQyxjQUFRLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBNUJhLGlDQUFPLEdBQUc7UUFDaEIsZ0JBQWdCO1FBQ2hCLFNBQVM7UUFDVCxpQkFBaUI7S0FDcEIsQ0FBQztJQXlCVixnQ0FBQztDQUFBLEFBOUJELElBOEJDO2tCQTlCb0IseUJBQXlCIn0=

/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var error_interceptor_1 = __webpack_require__(155);
var demo_interceptor_1 = __webpack_require__(156);
var format_interceptor_1 = __webpack_require__(157);
var view_id_interceptor_1 = __webpack_require__(158);
exports.default = function (module) {
    module.factory("swErrorInterceptor", error_interceptor_1.default);
    module.factory("swDemoInterceptor", demo_interceptor_1.default);
    module.factory("swFormatInterceptor", format_interceptor_1.default);
    module.factory("swViewIdInterceptor", view_id_interceptor_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlEQUF1RDtBQUN2RCx1REFBcUQ7QUFDckQsMkRBQXlEO0FBQ3pELDZEQUFzRDtBQUV0RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSwyQkFBb0IsQ0FBQyxDQUFDO0lBQzNELE1BQU0sQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEVBQUUsMEJBQW1CLENBQUMsQ0FBQztJQUN6RCxNQUFNLENBQUMsT0FBTyxDQUFDLHFCQUFxQixFQUFFLDRCQUFxQixDQUFDLENBQUM7SUFDN0QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRSw2QkFBaUIsQ0FBQyxDQUFDO0FBQzdELENBQUMsQ0FBQyJ9

/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var defaultPlatformParams = {
    swAlertOnError: true,
    swLogOutOnAuthError: false,
    swLogOnError: [401, 403, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510],
    swToastOnError: [401, 403, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510]
};
var ErrorHttpInterceptor = /** @class */ (function () {
    function ErrorHttpInterceptor($q, $log, $injector, $rootScope, toast, _t, $window) {
        var _this = this;
        this.$q = $q;
        this.$log = $log;
        this.$injector = $injector;
        this.$rootScope = $rootScope;
        this.toast = toast;
        this._t = _t;
        this.$window = $window;
        this.offline = false;
        this.response = function (response) {
            _this.handleReconnect();
            return response;
        };
        this.responseError = function (response) {
            if (response.status <= 0) {
                _this.handleConnectError(response);
            }
            else {
                _this.handleServerError(response);
            }
            return _this.$q.reject(response);
        };
        this.reportAuthError = function () {
            _this.toast.error(_this._t("Authorization has been denied for this request."), _this._t("Authorization Failure"));
        };
        this.getConfigParameters = function (response) {
            var config = response && response.config;
            var params = (config && config.params || {});
            if (params.swAlertOnError === undefined) {
                params.swAlertOnError = defaultPlatformParams.swAlertOnError;
            }
            if (params.swLogOutOnAuthError === undefined) {
                params.swLogOutOnAuthError = defaultPlatformParams.swLogOutOnAuthError;
            }
            params.swLogOnError = _this.initializeCodeArray(params.swLogOnError, defaultPlatformParams.swLogOnError);
            params.swToastOnError = _this.initializeCodeArray(params.swToastOnError, defaultPlatformParams.swToastOnError);
            return params;
        };
        this.initializeCodeArray = function (errorCodes, defaults) {
            if (errorCodes === undefined) {
                return defaults;
            }
            if (errorCodes === false) {
                return [];
            }
            if (errorCodes.constructor === Array) {
                return errorCodes.concat(defaults);
            }
            return errorCodes;
        };
        this.isAuthError = function (status) {
            return [401, 403].indexOf(status) !== -1;
        };
        this.redirectOnSessionTimeout = function () {
            var location = _this.$window.location || {};
            var fullPath = encodeURIComponent("" + location.pathname + location.search);
            location.href = "/Orion/Login.aspx?sessionTimeout=yes&ReturnUrl=" + fullPath;
        };
        this.toastOnServerError = function (response) {
            _this.toast.error(_this._t("We are having difficulty communicating with the Orion server.<br />") +
                _this._t("<span class='sw-orion-api-error__link'>Click here for details.</span>"), _this._t("Server Error"), {
                toastClass: "xui-toast sw-orion-api-error__toast",
                onclick: function () {
                    var dialog = _this.$injector.get("xuiDialogService");
                    var vm = {
                        errorString: JSON.stringify(response, null, "\t"),
                        header: response.data.exceptionMessage || response.data.message,
                        copySuccess: function () {
                            _this.toast.info(_this._t("Error details have been copied to the clipboard."), _this._t("Copied"));
                        }
                    };
                    _this._dialogPromise = dialog
                        .showModal({
                        template: __webpack_require__(18),
                        size: "lg",
                        windowClass: "sw-orion-api-error__dialog"
                    }, {
                        title: _this._t("Error Details"),
                        viewModel: vm,
                        hideCancel: true,
                        status: "error"
                    })
                        .finally(function () {
                        _this._dialogPromise = null;
                    });
                }
            });
        };
        this.handleServerError = function (response) {
            _this.handleReconnect();
            var params = _this.getConfigParameters(response);
            if (_this.isAuthError(response.status)) {
                if (_this.isToastMessageRequired(response, params)) {
                    _this.reportAuthError();
                }
                // handle session timeout
                if (response.status === 401 && params.swLogOutOnAuthError) {
                    _this.redirectOnSessionTimeout();
                }
                return;
            }
            if (_this.isLogMessageRequired(response, params)) {
                _this.$log.error("$http response error: " + angular.toJson(response));
            }
            if (_this.isToastMessageRequired(response, params)) {
                _this.toastOnServerError(response);
            }
        };
        this.handleConnectError = function (response) {
            if (!_this.offline) {
                _this.offline = true;
                _this.$rootScope.$emit("sw-connection-lost");
            }
        };
        this.handleReconnect = function () {
            if (_this.offline) {
                _this.offline = false;
                _this.$rootScope.$emit("sw-connection-restored");
            }
        };
        this.shouldErrorCodeBeProcessed = function (codesToProcess, currentStatusCode) {
            var invertedCurrentStatusCode = currentStatusCode * -1;
            return codesToProcess.indexOf(currentStatusCode) > -1 &&
                codesToProcess.indexOf(invertedCurrentStatusCode) < 0;
        };
        this.isLogMessageRequired = function (response, params) {
            return params.swAlertOnError &&
                (params.swLogOnError.constructor === Array ?
                    _this.shouldErrorCodeBeProcessed(params.swLogOnError, response.status) :
                    true);
        };
        this.isToastMessageRequired = function (response, params) {
            return params.swAlertOnError &&
                (params.swToastOnError.constructor === Array ?
                    _this.shouldErrorCodeBeProcessed(params.swToastOnError, response.status) :
                    true);
        };
        return this;
    }
    Object.defineProperty(ErrorHttpInterceptor, "defaultPlatformParams", {
        get: function () {
            return defaultPlatformParams;
        },
        enumerable: true,
        configurable: true
    });
    ErrorHttpInterceptor = __decorate([
        __param(0, decorators_1.Inject("$q")),
        __param(1, decorators_1.Inject("$log")),
        __param(2, decorators_1.Inject("$injector")),
        __param(3, decorators_1.Inject("$rootScope")),
        __param(4, decorators_1.Inject("xuiToastService")),
        __param(5, decorators_1.Inject("getTextService")),
        __param(6, decorators_1.Inject("$window")),
        __metadata("design:paramtypes", [Function, Object, Object, Object, Object, Function, Object])
    ], ErrorHttpInterceptor);
    return ErrorHttpInterceptor;
}());
exports.ErrorHttpInterceptor = ErrorHttpInterceptor;
exports.default = ErrorHttpInterceptor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3ItaW50ZXJjZXB0b3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlcnJvci1pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0NBQW9DOzs7Ozs7Ozs7Ozs7OztBQVdwQyw0Q0FBdUM7QUFTdkMsSUFBTSxxQkFBcUIsR0FBeUI7SUFDaEQsY0FBYyxFQUFFLElBQUk7SUFDcEIsbUJBQW1CLEVBQUUsS0FBSztJQUMxQixZQUFZLEVBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7SUFDaEYsY0FBYyxFQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDO0NBQ3JGLENBQUM7QUFFRjtJQUdJLDhCQUVZLEVBQWEsRUFFYixJQUFpQixFQUVqQixTQUFnQyxFQUVoQyxVQUE2QixFQUU3QixLQUF1QixFQUV2QixFQUFvQyxFQUVwQyxPQUF1QjtRQWRuQyxpQkFpQkM7UUFmVyxPQUFFLEdBQUYsRUFBRSxDQUFXO1FBRWIsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUVqQixjQUFTLEdBQVQsU0FBUyxDQUF1QjtRQUVoQyxlQUFVLEdBQVYsVUFBVSxDQUFtQjtRQUU3QixVQUFLLEdBQUwsS0FBSyxDQUFrQjtRQUV2QixPQUFFLEdBQUYsRUFBRSxDQUFrQztRQUVwQyxZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQWhCM0IsWUFBTyxHQUFHLEtBQUssQ0FBQztRQXlCakIsYUFBUSxHQUFHLFVBQUMsUUFBcUM7WUFDcEQsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDO1FBRUssa0JBQWEsR0FBRyxVQUFDLFFBQXFDO1lBQ3pELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDckMsQ0FBQztZQUVELE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwQyxDQUFDLENBQUM7UUFFTSxvQkFBZSxHQUFHO1lBQ3RCLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUNaLEtBQUksQ0FBQyxFQUFFLENBQUMsaURBQWlELENBQUMsRUFDMUQsS0FBSSxDQUFDLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQyxDQUNuQyxDQUFDO1FBQ04sQ0FBQyxDQUFDO1FBRU0sd0JBQW1CLEdBQUcsVUFBQyxRQUFzQztZQUNqRSxJQUFNLE1BQU0sR0FBbUIsUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDM0QsSUFBTSxNQUFNLEdBQXlCLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUM7WUFFckUsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLENBQUMsY0FBYyxHQUFHLHFCQUFxQixDQUFDLGNBQWMsQ0FBQztZQUNqRSxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLG1CQUFtQixLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLE1BQU0sQ0FBQyxtQkFBbUIsR0FBRyxxQkFBcUIsQ0FBQyxtQkFBbUIsQ0FBQztZQUMzRSxDQUFDO1lBRUQsTUFBTSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN4RyxNQUFNLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLHFCQUFxQixDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBRTlHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDbEIsQ0FBQyxDQUFBO1FBRU8sd0JBQW1CLEdBQUcsVUFBQyxVQUE4QixFQUFFLFFBQTRCO1lBQ3ZGLEVBQUUsQ0FBQyxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3BCLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxVQUFVLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDdkIsTUFBTSxDQUFZLEVBQUUsQ0FBQztZQUN6QixDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLFdBQVcsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxNQUFNLENBQWEsVUFBVyxDQUFDLE1BQU0sQ0FBWSxRQUFRLENBQUMsQ0FBQztZQUMvRCxDQUFDO1lBQ0QsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUN0QixDQUFDLENBQUE7UUFFTyxnQkFBVyxHQUFHLFVBQUMsTUFBYztZQUNqQyxNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQTtRQUVPLDZCQUF3QixHQUFHO1lBQy9CLElBQU0sUUFBUSxHQUFzQixLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUM7WUFDaEUsSUFBTSxRQUFRLEdBQUcsa0JBQWtCLENBQUMsS0FBRyxRQUFRLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFRLENBQUMsQ0FBQztZQUM5RSxRQUFRLENBQUMsSUFBSSxHQUFHLG9EQUFrRCxRQUFVLENBQUM7UUFDakYsQ0FBQyxDQUFBO1FBRU8sdUJBQWtCLEdBQUcsVUFBQyxRQUFzQztZQUNoRSxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FDWixLQUFJLENBQUMsRUFBRSxDQUFDLHFFQUFxRSxDQUFDO2dCQUM5RSxLQUFJLENBQUMsRUFBRSxDQUFDLHVFQUF1RSxDQUFDLEVBQ2hGLEtBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLEVBQ3ZCO2dCQUNJLFVBQVUsRUFBRSxxQ0FBcUM7Z0JBQ2pELE9BQU8sRUFBRTtvQkFDTCxJQUFNLE1BQU0sR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBb0Isa0JBQWtCLENBQUMsQ0FBQztvQkFDekUsSUFBTSxFQUFFLEdBQUc7d0JBQ1AsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUM7d0JBQ2pELE1BQU0sRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTzt3QkFDL0QsV0FBVyxFQUFFOzRCQUNULEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsa0RBQWtELENBQUMsRUFDdkUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3dCQUMzQixDQUFDO3FCQUNKLENBQUM7b0JBQ0YsS0FBSSxDQUFDLGNBQWMsR0FBRyxNQUFNO3lCQUN2QixTQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLE9BQU8sQ0FBUyxtQ0FBbUMsQ0FBQzt3QkFDOUQsSUFBSSxFQUFFLElBQUk7d0JBQ1YsV0FBVyxFQUFFLDRCQUE0QjtxQkFDNUMsRUFBRTt3QkFDQyxLQUFLLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxlQUFlLENBQUM7d0JBQy9CLFNBQVMsRUFBRSxFQUFFO3dCQUNiLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixNQUFNLEVBQUUsT0FBTztxQkFDbEIsQ0FBQzt5QkFDRCxPQUFPLENBQUM7d0JBQ0wsS0FBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0JBQy9CLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUM7YUFDSixDQUFDLENBQUM7UUFDWCxDQUFDLENBQUE7UUFFTyxzQkFBaUIsR0FBRyxVQUFDLFFBQXNDO1lBQy9ELEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUV2QixJQUFJLE1BQU0sR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFaEQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDaEQsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUMzQixDQUFDO2dCQUVELHlCQUF5QjtnQkFDekIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztvQkFDeEQsS0FBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7Z0JBQ3BDLENBQUM7Z0JBQ0QsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5QyxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDekUsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLHVCQUFrQixHQUFHLFVBQUMsUUFBcUM7WUFDL0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ3BCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDaEQsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLG9CQUFlLEdBQUc7WUFDdEIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDcEQsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLCtCQUEwQixHQUFHLFVBQUMsY0FBd0IsRUFBRSxpQkFBeUI7WUFDckYsSUFBTSx5QkFBeUIsR0FBVyxpQkFBaUIsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNqRSxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDOUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqRSxDQUFDLENBQUE7UUFFTyx5QkFBb0IsR0FBRyxVQUFDLFFBQXFDLEVBQUUsTUFBNEI7WUFDL0YsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjO2dCQUNyQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsV0FBVyxLQUFLLEtBQUssQ0FBQyxDQUFDO29CQUMzQyxLQUFJLENBQUMsMEJBQTBCLENBQVksTUFBTSxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDbEYsSUFBSSxDQUFDLENBQUM7UUFDbEIsQ0FBQyxDQUFDO1FBRU0sMkJBQXNCLEdBQUcsVUFBQyxRQUFxQyxFQUFFLE1BQTRCO1lBQ2pHLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYztnQkFDckIsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxLQUFLLENBQUMsQ0FBQztvQkFDN0MsS0FBSSxDQUFDLDBCQUEwQixDQUFZLE1BQU0sQ0FBQyxjQUFjLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ3BGLElBQUksQ0FBQyxDQUFDO1FBQ2xCLENBQUMsQ0FBQztRQW5LRSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxzQkFBa0IsNkNBQXFCO2FBQXZDO1lBQ0ksTUFBTSxDQUFDLHFCQUFxQixDQUFDO1FBQ2pDLENBQUM7OztPQUFBO0lBeEJRLG9CQUFvQjtRQUl4QixXQUFBLG1CQUFNLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFWixXQUFBLG1CQUFNLENBQUMsTUFBTSxDQUFDLENBQUE7UUFFZCxXQUFBLG1CQUFNLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFbkIsV0FBQSxtQkFBTSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBRXBCLFdBQUEsbUJBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBRXpCLFdBQUEsbUJBQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1FBRXhCLFdBQUEsbUJBQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQTs7T0FoQmIsb0JBQW9CLENBeUxoQztJQUFELDJCQUFDO0NBQUEsQUF6TEQsSUF5TEM7QUF6TFksb0RBQW9CO0FBMkxqQyxrQkFBZSxvQkFBb0IsQ0FBQyJ9

/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @ngdoc service
 * @name orion.factory.swDemoInterceptor
 *
 * @description
 * swDemoInterceptor Service factory that can be registered with the
 * $httpProvider and can be added to the $httpProvider.interceptors array.
 * For example:
 * <pre>
 *       if (SW.environment.demoMode === true) {
 *            $httpProvider.interceptors.push("swDemoInterceptor");
 *        }
 * </pre>
 * Used for changing of services response in demo mode.
 **/
var DemoHttpInterceptor = /** @class */ (function () {
    function DemoHttpInterceptor($rootScope, $log, demo) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$log = $log;
        this.demo = demo;
        /**
         *  @ngdoc method
         *  @name response
         *  @methodOf orion.factory.swDemoInterceptor
         *  @description Modifies response when demo mode equals true
         *  @param {any} response from services that will be modified if we are in demo mode
        **/
        this.response = function (response) {
            if (_this.$rootScope.demoMode && _this.isDemoResponse(response)) {
                var config = _this.$rootScope.$demoConfig;
                var notSupported = function () { return new Error("Operation not supported in Demo Mode."); };
                var alertUser = _this.demo.showDemoErrorToast;
                var parser = document.createElement("a");
                parser.href = response.config.url;
                // Handle leading slash in IE.
                // https://connect.microsoft.com/IE/feedbackdetail/view/...
                //  ... 1002846/pathname-incorrect-for-out-of-document-elements#tabs
                var routePath_1 = parser.pathname.toLowerCase().replace(/^\//, "");
                var methodType = response.config.method.toLowerCase();
                var route = _.find(config.routes, function (r, url) {
                    var regExp = new RegExp(url, "i");
                    return regExp.test(routePath_1) || regExp.test("/" + routePath_1);
                });
                var demoMock = _.get(route, methodType);
                if (demoMock) {
                    if (demoMock.alertUser !== false) {
                        alertUser();
                    }
                    if (demoMock.data) {
                        response.data = _.cloneDeep(demoMock.data);
                    }
                    else {
                        throw notSupported();
                    }
                }
                else {
                    alertUser();
                    throw notSupported();
                }
            }
            return response;
        };
        /**
         *  @ngdoc method
         *  @name isDemoResponse
         *  @methodOf orion.factory.swDemoInterceptor
         *  @description Checks if demo response was received from services
         *  @param {any} response from services that can contain demo data
        **/
        this.isDemoResponse = function (response) {
            return response.status === 202 &&
                response.data &&
                response.data.error &&
                response.data.error.code === 1001;
        };
        return {
            response: this.response
        };
    }
    DemoHttpInterceptor.$inject = ["$rootScope", "$log", "swDemoService"];
    return DemoHttpInterceptor;
}());
exports.default = DemoHttpInterceptor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVtby1pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRlbW8taW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9DQUFvQzs7QUFNcEM7Ozs7Ozs7Ozs7Ozs7O0lBY0k7QUFDSjtJQUdJLDZCQUFvQixVQUEwQixFQUFVLElBQW9CLEVBQ3hELElBQVM7UUFEN0IsaUJBS0M7UUFMbUIsZUFBVSxHQUFWLFVBQVUsQ0FBZ0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFnQjtRQUN4RCxTQUFJLEdBQUosSUFBSSxDQUFLO1FBSzdCOzs7Ozs7V0FNRztRQUNJLGFBQVEsR0FBRyxVQUFDLFFBQXdDO1lBQ3ZELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM1RCxJQUFNLE1BQU0sR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztnQkFDM0MsSUFBTSxZQUFZLEdBQUcsY0FBTSxPQUFBLElBQUksS0FBSyxDQUFDLHVDQUF1QyxDQUFDLEVBQWxELENBQWtELENBQUM7Z0JBQzlFLElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7Z0JBQy9DLElBQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBRWxDLDhCQUE4QjtnQkFDOUIsMkRBQTJEO2dCQUMzRCxvRUFBb0U7Z0JBQ3BFLElBQU0sV0FBUyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDbkUsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3hELElBQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxVQUFDLENBQWEsRUFBRSxHQUFXO29CQUMzRCxJQUFNLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7b0JBQ3BDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLFdBQVMsQ0FBQyxDQUFDO2dCQUNsRSxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFNLFFBQVEsR0FBUSxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDL0MsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDWCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQy9CLFNBQVMsRUFBRSxDQUFDO29CQUNoQixDQUFDO29CQUVELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNoQixRQUFRLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMvQyxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLE1BQU0sWUFBWSxFQUFFLENBQUM7b0JBQ3pCLENBQUM7Z0JBQ0wsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixTQUFTLEVBQUUsQ0FBQztvQkFDWixNQUFNLFlBQVksRUFBRSxDQUFDO2dCQUN6QixDQUFDO1lBQ0wsQ0FBQztZQUVELE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDO1FBRUY7Ozs7OztXQU1HO1FBQ0ssbUJBQWMsR0FBRyxVQUFDLFFBQXlDO1lBQy9ELE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUc7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJO2dCQUNiLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSztnQkFDbkIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQztRQUMxQyxDQUFDLENBQUM7UUE3REUsTUFBTSxDQUFDO1lBQ0gsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQ0gsQ0FBQztJQUM3QixDQUFDO0lBUGEsMkJBQU8sR0FBRyxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFrRXBFLDBCQUFDO0NBQUEsQUFuRUQsSUFtRUM7QUFFRCxrQkFBZSxtQkFBbUIsQ0FBQyJ9

/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var FormatHttpInterceptor = /** @class */ (function () {
    function FormatHttpInterceptor($log) {
        var _this = this;
        this.$log = $log;
        this.response = function (response) {
            response.data = _this.formatResponse(response.data);
            return response;
        };
        this.formatResponse = function (obj) {
            if (_.isArray(obj)) {
                for (var i = 0; i < obj.length; i++) {
                    obj[i] = _this.formatResponse(obj[i]);
                }
                return obj;
            }
            if (_.isObject(obj)) {
                return _.transform(obj, function (result, value, key) {
                    result[key] = _this.formatResponse(value);
                }, obj);
            }
            return _this.formatAtomicValue(obj);
        };
        this.formatAtomicValue = function (val) {
            if (_.isString(val)) {
                return _this.convertMsStringToDate(val);
            }
            return val;
        };
        this.convertMsStringToDate = function (val) {
            if (FormatHttpInterceptor.MSJsonDateRegEx.test(val)) {
                try {
                    return new Date(parseInt(_.trimStart(val, "/Date("), 10));
                }
                catch (ex) {
                    _this.$log.error("failed to parse microsoft-formatted date string: " + val);
                }
            }
            return val;
        };
        return this;
    }
    FormatHttpInterceptor.MSJsonDateRegEx = new RegExp("^\/Date\((d|-|.*)\)[\/]");
    FormatHttpInterceptor.$inject = ["$log"];
    return FormatHttpInterceptor;
}());
exports.default = FormatHttpInterceptor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LWludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZm9ybWF0LWludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBRXBDO0lBS0ksK0JBQW9CLElBQW9CO1FBQXhDLGlCQUVDO1FBRm1CLFNBQUksR0FBSixJQUFJLENBQWdCO1FBSWpDLGFBQVEsR0FBRyxVQUFDLFFBQXdDO1lBQ3ZELFFBQVEsQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkQsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUM7UUFFSyxtQkFBYyxHQUFHLFVBQUMsR0FBUTtZQUM3QixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakIsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7b0JBQ2xDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6QyxDQUFDO2dCQUNELE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDZixDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxVQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsR0FBRztvQkFDdkMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNaLENBQUM7WUFFRCxNQUFNLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztRQUVNLHNCQUFpQixHQUFHLFVBQUMsR0FBUTtZQUNqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEIsTUFBTSxDQUFDLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMzQyxDQUFDO1lBQ0QsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNmLENBQUMsQ0FBQztRQUVNLDBCQUFxQixHQUFHLFVBQUMsR0FBVztZQUN4QyxFQUFFLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxDQUFDO29CQUNELE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDOUQsQ0FBQztnQkFBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNWLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxHQUFHLEdBQUcsQ0FBQyxDQUFDO2dCQUMvRSxDQUFDO1lBQ0wsQ0FBQztZQUNELE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDZixDQUFDLENBQUM7UUF4Q0UsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBTmMscUNBQWUsR0FBRyxJQUFJLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBRXpELDZCQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQTRDckMsNEJBQUM7Q0FBQSxBQS9DRCxJQStDQztBQUVELGtCQUFlLHFCQUFxQixDQUFDIn0=

/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ViewIdInterceptor = /** @class */ (function () {
    /** @ngInject */
    ViewIdInterceptor.$inject = ["swOrionViewService"];
    function ViewIdInterceptor(swOrionViewService) {
        var _this = this;
        this.swOrionViewService = swOrionViewService;
        this.request = function (config) {
            if (config._isTemplate) {
                return config;
            }
            if (_this.swOrionViewService &&
                _this.swOrionViewService.activeViewInfo &&
                _this.swOrionViewService.activeViewInfo.viewId) {
                if (!config.params) {
                    config.params = {};
                }
                if (!config.params.viewId) {
                    config.params.viewId = _this.swOrionViewService.activeViewInfo.viewId;
                }
            }
            return config;
        };
        return this;
    }
    return ViewIdInterceptor;
}());
exports.ViewIdInterceptor = ViewIdInterceptor;
exports.default = ViewIdInterceptor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1pZC1pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpZXctaWQtaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQTtJQUNJLGdCQUFnQjtJQUNoQiwyQkFBb0Isa0JBQXlDO1FBQTdELGlCQUVDO1FBRm1CLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBdUI7UUFJdEQsWUFBTyxHQUFHLFVBQUMsTUFBc0I7WUFDcEMsRUFBRSxDQUFDLENBQUUsTUFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDbEIsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxrQkFBa0I7Z0JBQ3ZCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjO2dCQUN0QyxLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO2dCQUN2QixDQUFDO2dCQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUN4QixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztnQkFDekUsQ0FBQztZQUVMLENBQUM7WUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQ2xCLENBQUMsQ0FBQztRQXBCRSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFxQkwsd0JBQUM7QUFBRCxDQUFDLEFBekJELElBeUJDO0FBekJZLDhDQUFpQjtBQTJCOUIsa0JBQWUsaUJBQWlCLENBQUMifQ==

/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var auth_service_1 = __webpack_require__(160);
var connection_service_1 = __webpack_require__(161);
var content_service_1 = __webpack_require__(162);
var dateTime_service_1 = __webpack_require__(163);
var hub_service_1 = __webpack_require__(164);
var lazyLoadModule_service_1 = __webpack_require__(165);
var license_service_1 = __webpack_require__(166);
var megaMenu_service_1 = __webpack_require__(167);
var netObjectOpidConverter_service_1 = __webpack_require__(168);
var notify_service_1 = __webpack_require__(169);
var orionNavigation_service_1 = __webpack_require__(170);
var orionViewInfo_service_1 = __webpack_require__(171);
var ping_service_1 = __webpack_require__(172);
var polling_service_1 = __webpack_require__(173);
var resourceLoader_service_1 = __webpack_require__(174);
var revision_service_1 = __webpack_require__(175);
var searchHistory_service_1 = __webpack_require__(176);
var swApi_service_1 = __webpack_require__(3);
var swDashboardView_service_1 = __webpack_require__(177);
var swDemo_service_1 = __webpack_require__(178);
var swisDataMapping_service_1 = __webpack_require__(179);
var swis_service_1 = __webpack_require__(180);
var tileStack_service_1 = __webpack_require__(181);
var tooltip_service_1 = __webpack_require__(49);
var view_service_1 = __webpack_require__(182);
var webAdmin_service_1 = __webpack_require__(183);
var globalFilteringQueryStringBuilder_service_1 = __webpack_require__(184);
exports.default = function (module) {
    module.service("contentService", content_service_1.ContentService);
    module.service("notifyService", notify_service_1.NotifyService);
    module.service("pollingService", polling_service_1.PollingService);
    module.service("resourceLoader", resourceLoader_service_1.ResourceLoaderService);
    module.service("revisionService", revision_service_1.RevisionService);
    module.service("swApi", swApi_service_1.SwApiService);
    module.service("swAuthService", auth_service_1.AuthService);
    module.service("swConnectionService", connection_service_1.ConnectionService);
    module.service("swDashboardViewService", swDashboardView_service_1.SwDashboardViewService);
    module.service("swDateTimeService", dateTime_service_1.DateTimeService);
    module.service("swDemoService", swDemo_service_1.SwDemoService);
    module.service("swHubService", hub_service_1.HubService);
    module.service("swisService", swis_service_1.SwisService);
    module.service("swLazyLoadModuleService", lazyLoadModule_service_1.LazyLoadModuleService);
    module.service("swLicenseService", license_service_1.LicenseService);
    module.service("swMegaMenuService", megaMenu_service_1.MegaMenuService);
    module.service("swNetObjectOpidConverterService", netObjectOpidConverter_service_1.NetObjectOpidConverterService);
    module.service("swOrionNavigationService", orionNavigation_service_1.OrionNavigationService);
    module.service("swOrionViewService", orionViewInfo_service_1.OrionViewInfoService);
    module.service("swPingService", ping_service_1.PingService);
    module.service("swSearchHistoryService", searchHistory_service_1.SearchHistoryService);
    module.service("swSwisDataMappingService", swisDataMapping_service_1.SwisDataMappingService);
    module.service("swTooltipService", tooltip_service_1.TooltipService);
    module.service("tileStackService", tileStack_service_1.TileStackService);
    module.service("tooltipService", tooltip_service_1.TooltipService);
    module.service("viewService", view_service_1.ViewService);
    module.service("webAdminService", webAdmin_service_1.WebAdminService);
    module.service("globalFilteringQueryStringBuilderService", globalFilteringQueryStringBuilder_service_1.GlobalFilteringQueryStringBuilderService);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLCtDQUE2QztBQUM3QywyREFBeUQ7QUFDekQscURBQW1EO0FBQ25ELHVEQUFxRDtBQUNyRCw2Q0FBMkM7QUFDM0MsbUVBQWlFO0FBQ2pFLHFEQUFtRDtBQUNuRCx1REFBcUQ7QUFDckQsbUZBQWlGO0FBQ2pGLG1EQUFpRDtBQUNqRCxxRUFBbUU7QUFDbkUsaUVBQStEO0FBQy9ELCtDQUE2QztBQUM3QyxxREFBbUQ7QUFDbkQsbUVBQWlFO0FBQ2pFLHVEQUFxRDtBQUNyRCxpRUFBK0Q7QUFDL0QsaURBQStDO0FBQy9DLHFFQUFtRTtBQUNuRSxtREFBaUQ7QUFDakQscUVBQW1FO0FBQ25FLCtDQUE2QztBQUM3Qyx5REFBdUQ7QUFDdkQscURBQW1EO0FBQ25ELCtDQUE2QztBQUM3Qyx1REFBcUQ7QUFDckQseUdBQXVHO0FBRXZHLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLGdDQUFjLENBQUMsQ0FBQztJQUNqRCxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSw4QkFBYSxDQUFDLENBQUM7SUFDL0MsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxnQ0FBYyxDQUFDLENBQUM7SUFDakQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSw4Q0FBcUIsQ0FBQyxDQUFDO0lBQ3hELE1BQU0sQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsa0NBQWUsQ0FBQyxDQUFDO0lBQ25ELE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLDRCQUFZLENBQUMsQ0FBQztJQUN0QyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSwwQkFBVyxDQUFDLENBQUM7SUFDN0MsTUFBTSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRSxzQ0FBaUIsQ0FBQyxDQUFDO0lBQ3pELE1BQU0sQ0FBQyxPQUFPLENBQUMsd0JBQXdCLEVBQUUsZ0RBQXNCLENBQUMsQ0FBQztJQUNqRSxNQUFNLENBQUMsT0FBTyxDQUFDLG1CQUFtQixFQUFFLGtDQUFlLENBQUMsQ0FBQztJQUNyRCxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSw4QkFBYSxDQUFDLENBQUM7SUFDL0MsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsd0JBQVUsQ0FBQyxDQUFDO0lBQzNDLE1BQU0sQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLDBCQUFXLENBQUMsQ0FBQztJQUMzQyxNQUFNLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLDhDQUFxQixDQUFDLENBQUM7SUFDakUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxnQ0FBYyxDQUFDLENBQUM7SUFDbkQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxrQ0FBZSxDQUFDLENBQUM7SUFDckQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQ0FBaUMsRUFBRSw4REFBNkIsQ0FBQyxDQUFDO0lBQ2pGLE1BQU0sQ0FBQyxPQUFPLENBQUMsMEJBQTBCLEVBQUUsZ0RBQXNCLENBQUMsQ0FBQztJQUNuRSxNQUFNLENBQUMsT0FBTyxDQUFDLG9CQUFvQixFQUFFLDRDQUFvQixDQUFDLENBQUM7SUFDM0QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsMEJBQVcsQ0FBQyxDQUFDO0lBQzdDLE1BQU0sQ0FBQyxPQUFPLENBQUMsd0JBQXdCLEVBQUUsNENBQW9CLENBQUMsQ0FBQztJQUMvRCxNQUFNLENBQUMsT0FBTyxDQUFDLDBCQUEwQixFQUFFLGdEQUFzQixDQUFDLENBQUM7SUFDbkUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxnQ0FBYyxDQUFDLENBQUM7SUFDbkQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxvQ0FBZ0IsQ0FBQyxDQUFDO0lBQ3JELE1BQU0sQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsZ0NBQWMsQ0FBQyxDQUFDO0lBQ2pELE1BQU0sQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLDBCQUFXLENBQUMsQ0FBQztJQUMzQyxNQUFNLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFLGtDQUFlLENBQUMsQ0FBQztJQUNuRCxNQUFNLENBQUMsT0FBTyxDQUFDLDBDQUEwQyxFQUFFLG9GQUF3QyxDQUFDLENBQUM7QUFDekcsQ0FBQyxDQUFDIn0=

/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AuthService = /** @class */ (function () {
    function AuthService(constants, $log, swDemoService) {
        var _this = this;
        this.constants = constants;
        this.$log = $log;
        this.swDemoService = swDemoService;
        this.permissions = {
            alertManagement: "alertManagement",
            customize: "customize",
            manageDashboards: "manageDashboards",
            disableAction: "disableAction",
            disableAlert: "disableAlert",
            disableAllActions: "disableAllActions",
            eventClear: "eventClear",
            mapManagement: "mapManagement",
            nodeManagement: "nodeManagement",
            reportManagement: "reportManagement",
            unmanage: "unmanage",
            viewCopCheck: "viewCopCheck",
            orionMapsManagement: "orionMapsManagement",
            uploadImagesToOrionMaps: "uploadImagesToOrionMaps",
        };
        this.roles = {
            admin: "admin"
        };
        this.demoModeClaim = "demo";
        this.getUser = function () {
            return _this.convertToUser(_this.constants.user);
        };
        this.isUserAdmin = function () {
            return _.find(_this.getUser().roles, function (role) {
                return role === _this.roles.admin;
            }) !== undefined;
        };
        this.isUserAuthorized = function (authProfile) {
            if (angular.isUndefined(authProfile)) {
                return false;
            }
            return _this.userPermissionsMatch(authProfile.permissions) &&
                _this.userRolesMatch(authProfile.roles);
        };
        this.isFeatureEnabled = function (featureToggle) {
            return undefined !== _.find(_this.constants.featureToggles, { name: featureToggle, isEnabled: true });
        };
        this.userPermissionsMatch = function (permissions) {
            return permissions.length === 0 ||
                _.intersection(permissions, _this.getUser().permissions).length !== 0;
        };
        this.userRolesMatch = function (roles) {
            return roles.length === 0 ||
                _.intersection(roles, _this.getUser().roles).length !== 0;
        };
        this.convertToUser = function (orionUser) {
            var roles = [];
            var permissions = _this.getPermissions(orionUser);
            if (orionUser.AllowAdmin) {
                roles.push(_this.roles.admin);
            }
            if (_this.swDemoService.isDemoMode()) {
                roles.push(_this.demoModeClaim);
                permissions.push(_this.demoModeClaim);
            }
            return {
                id: orionUser.AccountID,
                accountSid: orionUser.AccountSid,
                accountType: orionUser.AccountType,
                accountEnabled: orionUser.AccountEnabled,
                expires: orionUser.Expires,
                lastLogin: orionUser.LastLogin,
                isSessionTimeoutEnabled: !orionUser.DisableSessionTimeout,
                roles: roles,
                permissions: permissions,
                userTabs: orionUser.UserTabs,
                userSettings: orionUser.UserSettings
            };
        };
        this.getPermissions = function (orionUser) {
            var permissions = [];
            _.forIn(_this.permissionsMap, function (value, key) {
                var hasPermission = _.get(orionUser, key);
                if (hasPermission) {
                    permissions.push(value);
                }
            });
            return permissions;
        };
        this.permissionsMap = {
            AllowAlertManagement: this.permissions.alertManagement,
            AllowCustomize: this.permissions.customize,
            AllowManageDashboards: this.permissions.manageDashboards,
            AllowDisableAction: this.permissions.disableAction,
            AllowDisableAlert: this.permissions.disableAlert,
            AllowDisableAllActions: this.permissions.disableAllActions,
            AllowEventClear: this.permissions.eventClear,
            AllowMapManagement: this.permissions.mapManagement,
            AllowNodeManagement: this.permissions.nodeManagement,
            AllowReportManagement: this.permissions.reportManagement,
            AllowUnmanage: this.permissions.unmanage,
            AllowViewCopCheck: this.permissions.viewCopCheck,
            AllowOrionMapsManagement: this.permissions.orionMapsManagement,
            AllowUploadImagesToOrionMaps: this.permissions.uploadImagesToOrionMaps
        };
    }
    AuthService.$inject = ["constants", "$log", "swDemoService"];
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBTXBDO0lBSUkscUJBQW9CLFNBQXFCLEVBQVUsSUFBb0IsRUFBVSxhQUEyQjtRQUE1RyxpQkFDQztRQURtQixjQUFTLEdBQVQsU0FBUyxDQUFZO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBZ0I7UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBYztRQUdyRyxnQkFBVyxHQUFHO1lBQ2pCLGVBQWUsRUFBRSxpQkFBaUI7WUFDbEMsU0FBUyxFQUFFLFdBQVc7WUFDdEIsZ0JBQWdCLEVBQUUsa0JBQWtCO1lBQ3BDLGFBQWEsRUFBRSxlQUFlO1lBQzlCLFlBQVksRUFBRSxjQUFjO1lBQzVCLGlCQUFpQixFQUFFLG1CQUFtQjtZQUN0QyxVQUFVLEVBQUUsWUFBWTtZQUN4QixhQUFhLEVBQUUsZUFBZTtZQUM5QixjQUFjLEVBQUUsZ0JBQWdCO1lBQ2hDLGdCQUFnQixFQUFFLGtCQUFrQjtZQUNwQyxRQUFRLEVBQUUsVUFBVTtZQUNwQixZQUFZLEVBQUUsY0FBYztZQUM1QixtQkFBbUIsRUFBRSxxQkFBcUI7WUFDMUMsdUJBQXVCLEVBQUUseUJBQXlCO1NBQ3JELENBQUM7UUFFSyxVQUFLLEdBQUc7WUFDWCxLQUFLLEVBQUUsT0FBTztTQUNqQixDQUFDO1FBRUssa0JBQWEsR0FBRyxNQUFNLENBQUM7UUFFdkIsWUFBTyxHQUFHO1lBQ2IsTUFBTSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuRCxDQUFDLENBQUM7UUFFSyxnQkFBVyxHQUFHO1lBQ2pCLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsVUFBQyxJQUFZO2dCQUN6QyxNQUFNLENBQUMsSUFBSSxLQUFLLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxLQUFLLFNBQVMsQ0FBQztRQUN6QixDQUFDLENBQUM7UUFFSyxxQkFBZ0IsR0FBRyxVQUFDLFdBQXlCO1lBQ2hELEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2pCLENBQUM7WUFFRCxNQUFNLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7Z0JBQ3JELEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQztRQUVLLHFCQUFnQixHQUFHLFVBQUMsYUFBcUI7WUFDNUMsTUFBTSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLEVBQUMsSUFBSSxFQUFFLGFBQWEsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztRQUN2RyxDQUFDLENBQUM7UUFFTSx5QkFBb0IsR0FBRyxVQUFDLFdBQXFCO1lBQ2pELE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUM7Z0JBQzNCLENBQUMsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1FBQzdFLENBQUMsQ0FBQztRQUVNLG1CQUFjLEdBQUcsVUFBQyxLQUFlO1lBQ3JDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUM7Z0JBQ3JCLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1FBQ2pFLENBQUMsQ0FBQztRQUVNLGtCQUFhLEdBQUcsVUFBQyxTQUFxQjtZQUMxQyxJQUFNLEtBQUssR0FBYSxFQUFFLENBQUM7WUFDM0IsSUFBTSxXQUFXLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNuRCxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pDLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDbEMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQy9CLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3pDLENBQUM7WUFFRCxNQUFNLENBQUM7Z0JBQ0gsRUFBRSxFQUFFLFNBQVMsQ0FBQyxTQUFTO2dCQUN2QixVQUFVLEVBQUUsU0FBUyxDQUFDLFVBQVU7Z0JBQ2hDLFdBQVcsRUFBRSxTQUFTLENBQUMsV0FBVztnQkFDbEMsY0FBYyxFQUFFLFNBQVMsQ0FBQyxjQUFjO2dCQUN4QyxPQUFPLEVBQUUsU0FBUyxDQUFDLE9BQU87Z0JBQzFCLFNBQVMsRUFBRSxTQUFTLENBQUMsU0FBUztnQkFDOUIsdUJBQXVCLEVBQUUsQ0FBQyxTQUFTLENBQUMscUJBQXFCO2dCQUN6RCxLQUFLLEVBQUUsS0FBSztnQkFDWixXQUFXLEVBQUUsV0FBVztnQkFDeEIsUUFBUSxFQUFFLFNBQVMsQ0FBQyxRQUFRO2dCQUM1QixZQUFZLEVBQUUsU0FBUyxDQUFDLFlBQVk7YUFDdkMsQ0FBQztRQUNOLENBQUMsQ0FBQztRQUVNLG1CQUFjLEdBQUcsVUFBQyxTQUFxQjtZQUMzQyxJQUFNLFdBQVcsR0FBYSxFQUFFLENBQUM7WUFDakMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLFVBQUMsS0FBYSxFQUFFLEdBQVc7Z0JBQ3BELElBQU0sYUFBYSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QyxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUNoQixXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM1QixDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3ZCLENBQUMsQ0FBQztRQUVNLG1CQUFjLEdBQUc7WUFDckIsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlO1lBQ3RELGNBQWMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVM7WUFDMUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0I7WUFDeEQsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhO1lBQ2xELGlCQUFpQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWTtZQUNoRCxzQkFBc0IsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQjtZQUMxRCxlQUFlLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVO1lBQzVDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTtZQUNsRCxtQkFBbUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWM7WUFDcEQscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0I7WUFDeEQsYUFBYSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtZQUN4QyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVk7WUFDaEQsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7WUFDOUQsNEJBQTRCLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUI7U0FDekUsQ0FBQztJQS9HRixDQUFDO0lBSGEsbUJBQU8sR0FBRyxDQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFtSG5FLGtCQUFDO0NBQUEsQUFySEQsSUFxSEM7QUFySFksa0NBQVcifQ==

/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ConnectionService = /** @class */ (function () {
    function ConnectionService($rootScope, $interval, pingService, toastService, _t, $log) {
        var _this = this;
        this.$interval = $interval;
        this.pingService = pingService;
        this.toastService = toastService;
        this._t = _t;
        this.$log = $log;
        this.isOffline = false;
        this.lostHandlers = [];
        this.restoredHandlers = [];
        this.registerConnectionLost = function (statusHandler) {
            _this.lostHandlers.push(statusHandler);
        };
        this.unregisterConnectionLost = function (statusHandler) {
            _this.unregister(statusHandler, _this.lostHandlers);
        };
        this.registerConnectionRestored = function (statusHandler) {
            _this.restoredHandlers.push(statusHandler);
        };
        this.unregisterConnectionRestored = function (statusHandler) {
            _this.unregister(statusHandler, _this.restoredHandlers);
        };
        this.unregister = function (statusHandler, delegates) {
            _.remove(delegates, function (delegate) {
                return statusHandler === delegate;
            });
        };
        this.handleConnectionLost = function () {
            _this.$log.error("connection lost");
            if (_this.isOffline === false) {
                _this.isOffline = true;
                _this.toastService.error(_this._t("We are having difficulty connecting to the Orion server."), _this._t("Connection Error"), { stickyError: true });
                _.forEach(_this.lostHandlers, function (delegate) { return delegate(); });
                _this.pingPromise = _this.$interval(function () { return _this.pingService.post(); }, 5000);
            }
        };
        this.handleConnectionRestored = function () {
            _this.$log.info("connection restored");
            if (_this.isOffline) {
                _this.isOffline = false;
                _this.cancelInterval();
                _this.toastService.clear();
                _.forEach(_this.restoredHandlers, function (delegate) { return delegate(); });
            }
        };
        this.cancelInterval = function () {
            if (_this.pingPromise) {
                _this.$interval.cancel(_this.pingPromise);
                _this.pingPromise = null;
            }
        };
        $rootScope.$on("sw-connection-lost", this.handleConnectionLost);
        $rootScope.$on("sw-connection-restored", this.handleConnectionRestored);
        $rootScope.$on("$destroy", this.cancelInterval);
    }
    ConnectionService.$inject = ["$rootScope", "$interval", "swPingService", "xuiToastService", "getTextService", "$log"];
    return ConnectionService;
}());
exports.ConnectionService = ConnectionService;
exports.default = ConnectionService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdGlvbi1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29ubmVjdGlvbi1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBS3BDO0lBU0ksMkJBQ0ksVUFBNkIsRUFDckIsU0FBMkIsRUFDM0IsV0FBeUIsRUFDekIsWUFBK0IsRUFDL0IsRUFBb0MsRUFDcEMsSUFBaUI7UUFON0IsaUJBV0M7UUFUVyxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQixnQkFBVyxHQUFYLFdBQVcsQ0FBYztRQUN6QixpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0IsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDcEMsU0FBSSxHQUFKLElBQUksQ0FBYTtRQWJyQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGlCQUFZLEdBQStCLEVBQUUsQ0FBQztRQUM5QyxxQkFBZ0IsR0FBK0IsRUFBRSxDQUFDO1FBa0JuRCwyQkFBc0IsR0FBRyxVQUFDLGFBQXVDO1lBQ3BFLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzFDLENBQUMsQ0FBQztRQUVLLDZCQUF3QixHQUFHLFVBQUMsYUFBdUM7WUFDdEUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQztRQUVLLCtCQUEwQixHQUFHLFVBQUMsYUFBdUM7WUFDeEUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5QyxDQUFDLENBQUM7UUFFSyxpQ0FBNEIsR0FBRyxVQUFDLGFBQXVDO1lBQzFFLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzFELENBQUMsQ0FBQztRQUVLLGVBQVUsR0FBRyxVQUFDLGFBQXVDLEVBQUUsU0FBcUM7WUFDL0YsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsVUFBQyxRQUFrQztnQkFDbkQsTUFBTSxDQUFDLGFBQWEsS0FBSyxRQUFRLENBQUM7WUFDdEMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFTSx5QkFBb0IsR0FBRztZQUMzQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ25DLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsS0FBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUNuQixLQUFJLENBQUMsRUFBRSxDQUFDLDBEQUEwRCxDQUFDLEVBQ25FLEtBQUksQ0FBQyxFQUFFLENBQUMsa0JBQWtCLENBQUMsRUFDM0IsRUFBQyxXQUFXLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztnQkFFekIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFLFVBQUMsUUFBa0MsSUFBSyxPQUFBLFFBQVEsRUFBRSxFQUFWLENBQVUsQ0FBQyxDQUFDO2dCQUNqRixLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEVBQXZCLENBQXVCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDM0UsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLDZCQUF3QixHQUFHO1lBQy9CLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDdEMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pCLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUN2QixLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLFVBQUMsUUFBa0MsSUFBSyxPQUFBLFFBQVEsRUFBRSxFQUFWLENBQVUsQ0FBQyxDQUFDO1lBQ3pGLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFTSxtQkFBYyxHQUFHO1lBQ3JCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3hDLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQzVCLENBQUM7UUFDTCxDQUFDLENBQUM7UUF4REUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNoRSxVQUFVLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3hFLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBYmEseUJBQU8sR0FBRyxDQUFDLFlBQVksRUFBRSxXQUFXLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBbUV0SCx3QkFBQztDQUFBLEFBMUVELElBMEVDO0FBMUVZLDhDQUFpQjtBQTRFOUIsa0JBQWUsaUJBQWlCLENBQUMifQ==

/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var sw_1 = __webpack_require__(7);
function ContentService(restangular, constants) {
    return restangular.withConfig(function (configurer) {
        // Configure restful access to the api service
        configurer.setBaseUrl(constants.environment.contentApiEndpoint);
        configurer.setDefaultHttpFields({ cache: false });
        configurer.setRestangularFields({ id: "Id" });
        configurer.setResponseExtractor(responseExtractor);
        configurer.addFullRequestInterceptor(fullRequestInterceptor);
    });
    function fullRequestInterceptor(element, operation, what, url, headers, params, httpConfig) {
        params.lang = sw_1.default.region;
    }
    function responseExtractor(response, operation) {
        var newResponse = response;
        if (operation === "getList") {
            if (typeof response.count !== "undefined" && typeof response.results !== "undefined") {
                newResponse = response.results;
                newResponse.count = response.count;
            }
        }
        return newResponse;
    }
}
exports.ContentService = ContentService;
ContentService.$inject = ["Restangular", "constants"];
exports.default = ContentService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGVudC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBS3BDLG1DQUE4QjtBQUU5Qix3QkFBZ0MsV0FBcUIsRUFBRSxTQUFxQjtJQUV4RSxNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxVQUFDLFVBQXFCO1FBQ2hELDhDQUE4QztRQUM5QyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNoRSxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBQyxLQUFLLEVBQUUsS0FBSyxFQUFDLENBQUMsQ0FBQztRQUNoRCxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBQyxFQUFFLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztRQUM1QyxVQUFVLENBQUMsb0JBQW9CLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNuRCxVQUFVLENBQUMseUJBQXlCLENBQUMsc0JBQXNCLENBQUMsQ0FBQztJQUNqRSxDQUFDLENBQUMsQ0FBQztJQUVILGdDQUNJLE9BQVksRUFDWixTQUFpQixFQUNqQixJQUFZLEVBQ1osR0FBVyxFQUNYLE9BQVksRUFDWixNQUFXLEVBQ1gsVUFBMEM7UUFFMUMsTUFBTSxDQUFDLElBQUksR0FBRyxZQUFFLENBQUMsTUFBTSxDQUFDO0lBQzVCLENBQUM7SUFFRCwyQkFBNEIsUUFBYSxFQUFFLFNBQWlCO1FBQ3hELElBQUksV0FBVyxHQUFHLFFBQVEsQ0FBQztRQUMzQixFQUFFLENBQUMsQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMxQixFQUFFLENBQUMsQ0FBQyxPQUFPLFFBQVEsQ0FBQyxLQUFLLEtBQUssV0FBVyxJQUFJLE9BQU8sUUFBUSxDQUFDLE9BQU8sS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNuRixXQUFXLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztnQkFDL0IsV0FBVyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1lBQ3ZDLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUN2QixDQUFDO0FBQ0wsQ0FBQztBQWpDRCx3Q0FpQ0M7QUFFRCxjQUFjLENBQUMsT0FBTyxHQUFHLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0FBRXRELGtCQUFlLGNBQWMsQ0FBQyJ9

/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @ngdoc service
 * @name orion.services.swDateTimeService
 *
 * @description
 * Service which purpose is to provide datetime-related functionality
 *
 **/
var DateTimeService = /** @class */ (function () {
    /** @ngInject */
    DateTimeService.$inject = ["dateFilter"];
    function DateTimeService(dateFilter) {
        var _this = this;
        this.dateFilter = dateFilter;
        /**
         * @ngdoc method
         * @name isValueDateInstance
         * @methodOf orion.services.swDateTimeService
         * @description Gets boolean indicating whether given value is Date instance
         **/
        this.isValueDateInstance = function (value) {
            return !!value && value instanceof Date;
        };
        /**
         * @ngdoc method
         * @name isValueMomentInstance
         * @methodOf orion.services.swDateTimeService
         * @description Gets boolean indicating whether given value is moment instance
         **/
        this.isValueMomentInstance = function (value) {
            return !!value && value instanceof moment && value.isValid();
        };
        /**
         * @ngdoc method
         * @name getOrionOffset
         * @methodOf orion.services.swDateTimeService
         * @description Gets Orion timezone offset in minutes
         **/
        this.getOrionOffset = function () {
            // note: we presume that init method has been called as soon as possible so offset is already loaded here
            return _this.orionOffset;
        };
        /**
         * @ngdoc method
         * @name getOrionNowDate
         * @methodOf orion.services.swDateTimeService
         * @description Gets current date and time in Orion as Date object
         **/
        this.getOrionNowDate = function () {
            return _this.convertMomentToOrionDateInternal(moment());
        };
        /**
         * @ngdoc method
         * @name getOrionNowMoment
         * @methodOf orion.services.swDateTimeService
         * @description Gets current date and time in Orion as Moment object
         **/
        this.getOrionNowMoment = function () {
            return moment().utcOffset(_this.orionOffset);
        };
        /**
         * @ngdoc method
         * @name getOrionTodayMoment
         * @methodOf orion.services.swDateTimeService
         * @description Gets current date in Orion as Moment object
         **/
        this.getOrionTodayMoment = function () {
            return _this.getOrionNowMoment().startOf("day");
        };
        /**
         * @ngdoc method
         * @name isOrionTodayMoment
         * @methodOf orion.services.swDateTimeService
         * @description Gets flag indicating whether a given moment instance refers to Orion current date
         **/
        this.isOrionTodayMoment = function (value) {
            var orionToday = _this.getOrionTodayMoment();
            var valueDateOnly = moment(value).startOf("day");
            return orionToday.valueOf() === valueDateOnly.valueOf();
        };
        /**
         * @ngdoc method
         * @name convertMomentToOrionDate
         * @methodOf orion.services.swDateTimeService
         * @description Converts Moment value to Date object in Orion timezone
         * @returns {Date} Date instance where date and time parts refer to Orion
         **/
        this.convertMomentToOrionDate = function (value) {
            return _this.convertMomentToOrionDateInternal(value);
        };
        /**
         * @ngdoc method
         * @name convertIsoStringToOrionDate
         * @methodOf orion.services.swDateTimeService
         * @description Converts ISO string value to Date object in Orion timezone
         * @returns {Date} Date instance where date and time parts refer to Orion
         **/
        this.convertIsoStringToOrionDate = function (value, isUtc) {
            if (isUtc === void 0) { isUtc = true; }
            if (!value) {
                return null;
            }
            var m;
            if (isUtc) {
                m = moment.utc(value);
            }
            else {
                m = moment(value);
            }
            return _this.convertMomentToOrionDateInternal(m);
        };
        /**
         * @ngdoc method
         * @name convertOrionDateToMoment
         * @methodOf orion.services.swDateTimeService
         * @description Converts Orion datetime to Moment object in UTC
         **/
        this.convertOrionDateToMoment = function (value) {
            return _this.convertOrionDateToMomentInternal(value);
        };
        /**
         * @ngdoc method
         * @name moveUtcDateToOrionTimezone
         * @methodOf orion.services.swDateTimeService
         * @description Moves Moment value to Orion timezone
         **/
        this.moveMomentToOrionTimezone = function (value) {
            return value.utcOffset(_this.orionOffset);
        };
        /**
         * @ngdoc method
         * @name formatDate
         * @methodOf orion.services.swDateTimeService
         * @description Formats given Date or Moment object to requested format
         **/
        this.format = function (value, format) {
            // check if Date
            if (_this.isValueDateInstance(value)) {
                return _this.formatDate(value, format);
            }
            // check if Moment
            if (_this.isValueMomentInstance(value)) {
                return _this.formatMoment(value, format);
            }
            throw Error("Datetime value not cannot be recognized.");
        };
        /**
         * @ngdoc method
         * @name formatDate
         * @methodOf orion.services.swDateTimeService
         * @description Formats given Date object to requested format
         **/
        this.formatDate = function (value, format) {
            switch (format) {
                case "iso": return value.toISOString();
                default: return _this.dateFilter(value, format);
            }
            ;
        };
        /**
         * @ngdoc method
         * @name formatMoment
         * @methodOf orion.services.swDateTimeService
         * @description Formats given Moment object to requested format
         **/
        this.formatMoment = function (value, format) {
            switch (format) {
                case "short": return value.format("l LT");
                case "medium": return value.format("ll LTS");
                case "shortDate": return value.format("l");
                case "mediumDate": return value.format("ll");
                case "longDate": return value.format("LL");
                case "shortTime": return value.format("LT");
                case "mediumTime": return value.format("LTS");
                case "iso": return value.toISOString();
                // otherwise return ISO
                default: return value.format();
            }
            ;
        };
        this.convertMomentToOrionDateInternal = function (value) {
            // note: it subtracts local offset because when converted by "toDate" it is moved by local offset
            return moment(value)
                .utc()
                .add(-_this.localOffset, "minutes")
                .add(_this.orionOffset, "minutes")
                .toDate();
        };
        this.convertOrionDateToMomentInternal = function (value) {
            // note: it is presumed that given Date was already shifted by local offset to look like Orion datetime
            return moment
                .utc(value)
                .add(_this.localOffset, "minutes")
                .add(-_this.orionOffset, "minutes");
        };
        this.init = function () {
            _this._localOffset = moment().utcOffset();
            _this.orionOffset = SW.environment.timezoneOffsetMinutes;
        };
        this.init();
    }
    Object.defineProperty(DateTimeService.prototype, "localOffset", {
        /**
         * @ngdoc method
         * @name getLocalOffset
         * @methodOf orion.services.swDateTimeService
         * @description Gets browser timezone offset in minutes
         **/
        get: function () {
            return this._localOffset;
        },
        enumerable: true,
        configurable: true
    });
    ;
    return DateTimeService;
}());
exports.DateTimeService = DateTimeService;
exports.default = DateTimeService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZVRpbWUtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhdGVUaW1lLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9DQUFvQzs7QUFlcEM7Ozs7Ozs7SUFPSTtBQUNKO0lBSUksZ0JBQWdCO0lBQ2hCLHlCQUFvQixVQUF1QjtRQUEzQyxpQkFFQztRQUZtQixlQUFVLEdBQVYsVUFBVSxDQUFhO1FBYzNDOzs7OztZQUtJO1FBQ0csd0JBQW1CLEdBQUcsVUFBQyxLQUFVO1lBQ3BDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLEtBQUssWUFBWSxJQUFJLENBQUM7UUFDNUMsQ0FBQyxDQUFDO1FBRUY7Ozs7O1lBS0k7UUFDRywwQkFBcUIsR0FBRyxVQUFDLEtBQVU7WUFDdEMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksS0FBSyxZQUFZLE1BQU0sSUFBb0IsS0FBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2xGLENBQUMsQ0FBQztRQUVGOzs7OztZQUtJO1FBQ0csbUJBQWMsR0FBRztZQUNwQix5R0FBeUc7WUFDekcsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRUY7Ozs7O1lBS0k7UUFDRyxvQkFBZSxHQUFHO1lBQ3JCLE1BQU0sQ0FBQyxLQUFJLENBQUMsZ0NBQWdDLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUMzRCxDQUFDLENBQUM7UUFFRjs7Ozs7WUFLSTtRQUNHLHNCQUFpQixHQUFHO1lBQ3ZCLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQztRQUVGOzs7OztZQUtJO1FBQ0csd0JBQW1CLEdBQUc7WUFDekIsTUFBTSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuRCxDQUFDLENBQUM7UUFFRjs7Ozs7WUFLSTtRQUNHLHVCQUFrQixHQUFHLFVBQUMsS0FBb0I7WUFDN0MsSUFBTSxVQUFVLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFDOUMsSUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUVuRCxNQUFNLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxLQUFLLGFBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM1RCxDQUFDLENBQUM7UUFFRjs7Ozs7O1lBTUk7UUFDRyw2QkFBd0IsR0FBRyxVQUFDLEtBQW9CO1lBQ25ELE1BQU0sQ0FBQyxLQUFJLENBQUMsZ0NBQWdDLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDO1FBRUY7Ozs7OztZQU1JO1FBQ0csZ0NBQTJCLEdBQUcsVUFBQyxLQUFhLEVBQUUsS0FBcUI7WUFBckIsc0JBQUEsRUFBQSxZQUFxQjtZQUN0RSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsTUFBTSxDQUFDLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUQsSUFBSSxDQUFnQixDQUFDO1lBRXJCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ1IsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEIsQ0FBQztZQUVELE1BQU0sQ0FBQyxLQUFJLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEQsQ0FBQyxDQUFDO1FBRUY7Ozs7O1lBS0k7UUFDRyw2QkFBd0IsR0FBRyxVQUFDLEtBQVc7WUFDMUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4RCxDQUFDLENBQUM7UUFFRjs7Ozs7WUFLSTtRQUNHLDhCQUF5QixHQUFHLFVBQUMsS0FBb0I7WUFDcEQsTUFBTSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQztRQUVGOzs7OztZQUtJO1FBQ0csV0FBTSxHQUFHLFVBQUMsS0FBMkIsRUFBRSxNQUFnQztZQUMxRSxnQkFBZ0I7WUFDaEIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQzFDLENBQUM7WUFFRCxrQkFBa0I7WUFDbEIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLENBQUM7WUFFRCxNQUFNLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1FBQzVELENBQUMsQ0FBQztRQUVGOzs7OztZQUtJO1FBQ0csZUFBVSxHQUFHLFVBQUMsS0FBVyxFQUFFLE1BQWdDO1lBQzlELE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsS0FBSyxLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDdkMsU0FBUyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDbkQsQ0FBQztZQUFBLENBQUM7UUFDTixDQUFDLENBQUM7UUFFRjs7Ozs7WUFLSTtRQUNHLGlCQUFZLEdBQUcsVUFBQyxLQUFvQixFQUFFLE1BQWdDO1lBQ3pFLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsS0FBSyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFDLEtBQUssUUFBUSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM3QyxLQUFLLFdBQVcsRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDM0MsS0FBSyxZQUFZLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzdDLEtBQUssVUFBVSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQyxLQUFLLFdBQVcsRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUMsS0FBSyxZQUFZLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzlDLEtBQUssS0FBSyxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3ZDLHVCQUF1QjtnQkFDdkIsU0FBUyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ25DLENBQUM7WUFBQSxDQUFDO1FBQ04sQ0FBQyxDQUFDO1FBRU0scUNBQWdDLEdBQUcsVUFBQyxLQUFvQjtZQUM1RCxpR0FBaUc7WUFDakcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7aUJBQ2YsR0FBRyxFQUFFO2lCQUNMLEdBQUcsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDO2lCQUNqQyxHQUFHLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUM7aUJBQ2hDLE1BQU0sRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FBQztRQUVNLHFDQUFnQyxHQUFHLFVBQUMsS0FBVztZQUNuRCx1R0FBdUc7WUFDdkcsTUFBTSxDQUFDLE1BQU07aUJBQ1IsR0FBRyxDQUFDLEtBQUssQ0FBQztpQkFDVixHQUFHLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUM7aUJBQ2hDLEdBQUcsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFDO1FBRU0sU0FBSSxHQUFHO1lBQ1gsS0FBSSxDQUFDLFlBQVksR0FBRyxNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN6QyxLQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUM7UUFDNUQsQ0FBQyxDQUFDO1FBdk5FLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQixDQUFDO0lBUUQsc0JBQUksd0NBQVc7UUFOZjs7Ozs7WUFLSTthQUNKO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7SUFBQSxDQUFDO0lBNk1OLHNCQUFDO0FBQUQsQ0FBQyxBQTlORCxJQThOQztBQTlOWSwwQ0FBZTtBQWdPNUIsa0JBQWUsZUFBZSxDQUFDIn0=

/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var HubService = /** @class */ (function () {
    function HubService() {
        var _this = this;
        //dashboard property is not yet implemented on SignalR interface
        //but is globally available in the $.connection though
        this.hub = $.connection.dashboard;
        this.initialize = function () {
            // this.hub.connect();
        };
        this.on = function (name, callback) {
            _this.hub.client[name] = callback;
        };
    }
    HubService.$inject = [];
    return HubService;
}());
exports.HubService = HubService;
exports.default = HubService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHViLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJodWItc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0NBQW9DOztBQUVwQztJQUFBO1FBQUEsaUJBY0M7UUFYRyxnRUFBZ0U7UUFDaEUsc0RBQXNEO1FBQzlDLFFBQUcsR0FBVSxDQUFDLENBQUMsVUFBVyxDQUFDLFNBQVMsQ0FBQztRQUV0QyxlQUFVLEdBQUc7WUFDaEIsc0JBQXNCO1FBQzFCLENBQUMsQ0FBQztRQUVLLE9BQUUsR0FBRyxVQUFDLElBQVksRUFBRSxRQUFrQjtZQUMzQyxLQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUM7UUFDbkMsQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQWJpQixrQkFBTyxHQUFrQixFQUFFLENBQUM7SUFhOUMsaUJBQUM7Q0FBQSxBQWRELElBY0M7QUFkWSxnQ0FBVTtBQWdCdkIsa0JBQWUsVUFBVSxDQUFDIn0=

/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var LazyLoadModuleService = /** @class */ (function () {
    function LazyLoadModuleService(swApi, $ocLazyLoad, $log, revision) {
        this.swApi = swApi;
        this.$ocLazyLoad = $ocLazyLoad;
        this.$log = $log;
        this.revision = revision;
    }
    LazyLoadModuleService.prototype.load = function (module) {
        var _this = this;
        return this.swApi.ui.one("scripts/list/" + module).get()
            .then(function (value) {
            var withRevision = value.map(function (path) { return _this.revision.getPathWithRevision(path); });
            return _this.$ocLazyLoad.load(withRevision);
        }, function (error) {
            _this.$log.error("Module '" + module + "' couldn't be loaded because of: " + error);
        });
    };
    LazyLoadModuleService.$inject = ["swApi", "$ocLazyLoad", "$log", "revisionService"];
    return LazyLoadModuleService;
}());
exports.LazyLoadModuleService = LazyLoadModuleService;
exports.default = LazyLoadModuleService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF6eUxvYWRNb2R1bGUtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxhenlMb2FkTW9kdWxlLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFLQTtJQUlJLCtCQUNZLEtBQW1CLEVBQ25CLFdBQXNCLEVBQ3RCLElBQWlCLEVBQ2pCLFFBQTBCO1FBSDFCLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsZ0JBQVcsR0FBWCxXQUFXLENBQVc7UUFDdEIsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNqQixhQUFRLEdBQVIsUUFBUSxDQUFrQjtJQUNuQyxDQUFDO0lBRUcsb0NBQUksR0FBWCxVQUFZLE1BQWM7UUFBMUIsaUJBUUM7UUFQRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLGtCQUFnQixNQUFRLENBQUMsQ0FBQyxHQUFHLEVBQUU7YUFDbkQsSUFBSSxDQUFDLFVBQUMsS0FBZTtZQUNsQixJQUFNLFlBQVksR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsRUFBdkMsQ0FBdUMsQ0FBQyxDQUFDO1lBQ2hGLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUMvQyxDQUFDLEVBQUUsVUFBQyxLQUFLO1lBQ0wsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBVyxNQUFNLHlDQUFvQyxLQUFPLENBQUMsQ0FBQztRQUNsRixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFqQmEsNkJBQU8sR0FBRyxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixDQUFDLENBQUM7SUFrQmhGLDRCQUFDO0NBQUEsQUFwQkQsSUFvQkM7QUFwQlksc0RBQXFCO0FBc0JsQyxrQkFBZSxxQkFBcUIsQ0FBQyJ9

/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var LicenseService = /** @class */ (function () {
    function LicenseService(constants, swApi, $log) {
        var _this = this;
        this.constants = constants;
        this.swApi = swApi;
        this.$log = $log;
        this.getLicenseSummary = function () {
            return _this.loadSaturationInfo().then(function () {
                return _this.licensesSummary;
            });
        };
        this.getLicenses = function () {
            return _this.getLicenseSummary().then(function (summary) {
                return summary.licenses;
            });
        };
        this.getMaintenanceItems = function () {
            return _this.loadMaintenanceItems();
        };
        this.hasMaintenanceExpired = function () {
            return _this.getMaintenanceItems().then(function (items) {
                return items.length === 0 || _.find(items, { "isExpired": true }) !== undefined;
            });
        };
        this.getSaturationInfo = function () {
            return _this.loadSaturationInfo();
        };
        this.getPollerLimitItems = function () {
            return _this.loadPollerLimitItems();
        };
        this.getUpgradeSummary = function () {
            return _this.loadUpgradeSummary();
        };
        this.transformLicenseSummary = function () {
            _this.licensesSummary = {
                evalModuleCount: _this.constants.license.EvalModuleCount,
                expiredModuleCount: _this.constants.license.ExpiredModuleCount,
                rcModuleCount: _this.constants.license.RcModuleCount,
                licenses: _this.getTransformedLicenses(),
                saturationInfo: []
            };
        };
        this.getTransformedLicenses = function () {
            return _.transform(_this.constants.license.Licenses, function (result, value) {
                result.push({
                    name: value.LicenseName,
                    productName: value.ProductName,
                    daysRemaining: value.DaysRemaining,
                    expiration: value.Expiration,
                    maintenanceExpiration: value.MaintenanceExpiration,
                    isEvaluation: value.IsEvaluation,
                    isExpired: value.IsExpired,
                    saturationEntities: []
                });
            }, []);
        };
        this.loadSaturationInfo = function () {
            _this.licensesSummary.saturationInfo = [];
            return _this.salesApi
                .post("GetLicenseModuleInfo", {})
                .then(function (response) {
                var saturationEntities = response.d;
                if (saturationEntities) {
                    _.forEach(saturationEntities, function (saturationEntity) {
                        var entities = _this.transformSaturationElements(saturationEntity.ElementList);
                        _this.licensesSummary.saturationInfo.push({
                            licenseName: saturationEntity.ModuleName,
                            productName: saturationEntity.ProductDisplayName,
                            saturationEntities: entities
                        });
                    });
                }
                return _this.licensesSummary.saturationInfo;
            });
        };
        this.transformSaturationElements = function (saturationElements) {
            return _.transform(saturationElements, function (result, value) {
                result.push({
                    elementType: value.ElementType,
                    count: value.Count,
                    maxCount: value.MaxCount,
                    saturation: value.Saturation,
                    isLimitReached: value.IsLimitReached
                });
            }, []);
        };
        this.loadMaintenanceItems = function () {
            var maintenanceItems = [];
            return _this.salesApi.post("GetMaintenanceModuleInfo", {})
                .then(function (response) {
                var items = response.d;
                if (items) {
                    _.transform(items, function (result, item) {
                        result.push({
                            moduleName: item.ModuleName,
                            displayName: item.DisplayName,
                            isExpired: item.IsExpired,
                            daysRemaining: item.DaysRemaining,
                            expiration: item.Expiration
                        });
                    }, maintenanceItems);
                }
                return maintenanceItems;
            });
        };
        this.loadPollerLimitItems = function () {
            var pollerLimitItems = [];
            return _this.salesApi.post("GetPollerLimitInfo", {})
                .then(function (response) {
                var items = response.d;
                if (items) {
                    _.transform(items, function (result, item) {
                        result.push({
                            isLimitReached: item.IsPollerLimitReached,
                            engineName: item.EngineName,
                            currentUsage: item.CurrentUsage
                        });
                    }, pollerLimitItems);
                }
                return pollerLimitItems;
            });
        };
        this.loadUpgradeSummary = function () {
            var upgradeSummary = {
                title: "",
                items: []
            };
            return _this.salesApi.post("GetUpgradeModuleInfo", {})
                .then(function (response) {
                var summary = response.d;
                if (summary) {
                    upgradeSummary.title = summary.title;
                    _.transform(summary.ugradeItems, function (result, item) {
                        result.push({
                            productName: item.ProductDisplayName,
                            isVersionDifference: item.IsVersionDifference,
                            isMaintenanceExpired: item.IsMaintenanceExpired,
                            releaseNotes: item.ReleaseNotes,
                            version: item.Version
                        });
                    }, upgradeSummary.items);
                }
                return upgradeSummary;
            });
        };
        this.transformLicenseSummary();
    }
    Object.defineProperty(LicenseService.prototype, "salesApi", {
        get: function () {
            return this.swApi.ws.one("SalesTrigger.asmx");
        },
        enumerable: true,
        configurable: true
    });
    LicenseService = __decorate([
        __param(0, decorators_1.Inject("constants")),
        __param(1, decorators_1.Inject("swApi")),
        __param(2, decorators_1.Inject("$log")),
        __metadata("design:paramtypes", [Object, Object, Object])
    ], LicenseService);
    return LicenseService;
}());
exports.LicenseService = LicenseService;
exports.default = LicenseService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZS1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGljZW5zZS1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7Ozs7Ozs7Ozs7Ozs7O0FBU3BDLDRDQUF1QztBQUV2QztJQUdJLHdCQUVZLFNBQXFCLEVBRXJCLEtBQW9CLEVBRXBCLElBQWlCO1FBTjdCLGlCQVNDO1FBUFcsY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUVyQixVQUFLLEdBQUwsS0FBSyxDQUFlO1FBRXBCLFNBQUksR0FBSixJQUFJLENBQWE7UUFTdEIsc0JBQWlCLEdBQUc7WUFDdkIsTUFBTSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLElBQUksQ0FBQztnQkFDbEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyxnQkFBVyxHQUFHO1lBQ2pCLE1BQU0sQ0FBQyxLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUF3QjtnQkFDMUQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyx3QkFBbUIsR0FBRztZQUN6QixNQUFNLENBQUMsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDdkMsQ0FBQyxDQUFDO1FBRUssMEJBQXFCLEdBQUc7WUFDM0IsTUFBTSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQXlCO2dCQUM3RCxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBQyxXQUFXLEVBQUUsSUFBSSxFQUFDLENBQUMsS0FBSyxTQUFTLENBQUM7WUFDbEYsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyxzQkFBaUIsR0FBRztZQUN2QixNQUFNLENBQUMsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDckMsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUc7WUFDekIsTUFBTSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztRQUVLLHNCQUFpQixHQUFHO1lBQ3ZCLE1BQU0sQ0FBQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUNyQyxDQUFDLENBQUM7UUFFTSw0QkFBdUIsR0FBRztZQUM5QixLQUFJLENBQUMsZUFBZSxHQUFHO2dCQUNuQixlQUFlLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsZUFBZTtnQkFDdkQsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsa0JBQWtCO2dCQUM3RCxhQUFhLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsYUFBYTtnQkFDbkQsUUFBUSxFQUFFLEtBQUksQ0FBQyxzQkFBc0IsRUFBRTtnQkFDdkMsY0FBYyxFQUFFLEVBQUU7YUFDckIsQ0FBQztRQUNOLENBQUMsQ0FBQztRQUVNLDJCQUFzQixHQUFHO1lBQzdCLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxVQUFDLE1BQWtCLEVBQUUsS0FBVTtnQkFDL0UsTUFBTSxDQUFDLElBQUksQ0FBQztvQkFDUixJQUFJLEVBQUUsS0FBSyxDQUFDLFdBQVc7b0JBQ3ZCLFdBQVcsRUFBRSxLQUFLLENBQUMsV0FBVztvQkFDOUIsYUFBYSxFQUFFLEtBQUssQ0FBQyxhQUFhO29CQUNsQyxVQUFVLEVBQUUsS0FBSyxDQUFDLFVBQVU7b0JBQzVCLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxxQkFBcUI7b0JBQ2xELFlBQVksRUFBRSxLQUFLLENBQUMsWUFBWTtvQkFDaEMsU0FBUyxFQUFFLEtBQUssQ0FBQyxTQUFTO29CQUMxQixrQkFBa0IsRUFBRSxFQUFFO2lCQUN6QixDQUFDLENBQUM7WUFDUCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFFTSx1QkFBa0IsR0FBRztZQUN6QixLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7WUFDekMsTUFBTSxDQUFDLEtBQUksQ0FBQyxRQUFRO2lCQUNmLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxFQUFFLENBQUM7aUJBQ2hDLElBQUksQ0FBQyxVQUFDLFFBQWE7Z0JBQ2hCLElBQU0sa0JBQWtCLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDdEMsRUFBRSxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO29CQUNyQixDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLFVBQUMsZ0JBQXFCO3dCQUNoRCxJQUFNLFFBQVEsR0FBRyxLQUFJLENBQUMsMkJBQTJCLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQ2hGLEtBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQzs0QkFDckMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLFVBQVU7NEJBQ3hDLFdBQVcsRUFBRSxnQkFBZ0IsQ0FBQyxrQkFBa0I7NEJBQ2hELGtCQUFrQixFQUFFLFFBQVE7eUJBQy9CLENBQUMsQ0FBQztvQkFDUCxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2dCQUNELE1BQU0sQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQztZQUMvQyxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVNLGdDQUEyQixHQUFHLFVBQUMsa0JBQXlCO1lBQzVELE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLFVBQUMsTUFBa0MsRUFBRSxLQUFVO2dCQUNsRixNQUFNLENBQUMsSUFBSSxDQUFDO29CQUNSLFdBQVcsRUFBRSxLQUFLLENBQUMsV0FBVztvQkFDOUIsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO29CQUNsQixRQUFRLEVBQUUsS0FBSyxDQUFDLFFBQVE7b0JBQ3hCLFVBQVUsRUFBRSxLQUFLLENBQUMsVUFBVTtvQkFDNUIsY0FBYyxFQUFFLEtBQUssQ0FBQyxjQUFjO2lCQUN2QyxDQUFDLENBQUM7WUFDUCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFFWCxDQUFDLENBQUM7UUFFTSx5QkFBb0IsR0FBRztZQUMzQixJQUFJLGdCQUFnQixHQUF1QixFQUFFLENBQUM7WUFDOUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFLEVBQUUsQ0FBQztpQkFDcEQsSUFBSSxDQUFDLFVBQUMsUUFBYTtnQkFDaEIsSUFBTSxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDekIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDUixDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxVQUFDLE1BQTBCLEVBQUUsSUFBUzt3QkFDckQsTUFBTSxDQUFDLElBQUksQ0FBQzs0QkFDUixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7NEJBQzNCLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVzs0QkFDN0IsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTOzRCQUN6QixhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWE7NEJBQ2pDLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTt5QkFDOUIsQ0FBQyxDQUFDO29CQUVQLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUN6QixDQUFDO2dCQUNELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztZQUM1QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVNLHlCQUFvQixHQUFHO1lBQzNCLElBQUksZ0JBQWdCLEdBQXVCLEVBQUUsQ0FBQztZQUM5QyxNQUFNLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxDQUFDO2lCQUM5QyxJQUFJLENBQUMsVUFBQyxRQUFhO2dCQUNoQixJQUFNLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNSLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLFVBQUMsTUFBMEIsRUFBRSxJQUFTO3dCQUNyRCxNQUFNLENBQUMsSUFBSSxDQUFDOzRCQUNSLGNBQWMsRUFBRSxJQUFJLENBQUMsb0JBQW9COzRCQUN6QyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7NEJBQzNCLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTt5QkFDbEMsQ0FBQyxDQUFDO29CQUVQLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUN6QixDQUFDO2dCQUNELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztZQUM1QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVNLHVCQUFrQixHQUFHO1lBQ3pCLElBQUksY0FBYyxHQUFvQjtnQkFDbEMsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsS0FBSyxFQUFFLEVBQUU7YUFDWixDQUFDO1lBQ0YsTUFBTSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLEVBQUUsQ0FBQztpQkFDaEQsSUFBSSxDQUFDLFVBQUMsUUFBYTtnQkFDaEIsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDM0IsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDVixjQUFjLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7b0JBQ3JDLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxVQUFDLE1BQXNCLEVBQUUsSUFBUzt3QkFDL0QsTUFBTSxDQUFDLElBQUksQ0FBQzs0QkFDUixXQUFXLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjs0QkFDcEMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLG1CQUFtQjs0QkFDN0Msb0JBQW9CLEVBQUUsSUFBSSxDQUFDLG9CQUFvQjs0QkFDL0MsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZOzRCQUMvQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87eUJBQ3hCLENBQUMsQ0FBQztvQkFFUCxDQUFDLEVBQUUsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixDQUFDO2dCQUNELE1BQU0sQ0FBQyxjQUFjLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFsS0UsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVELHNCQUFZLG9DQUFRO2FBQXBCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2xELENBQUM7OztPQUFBO0lBaEJRLGNBQWM7UUFJbEIsV0FBQSxtQkFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRW5CLFdBQUEsbUJBQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUVmLFdBQUEsbUJBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQTs7T0FSVixjQUFjLENBOEsxQjtJQUFELHFCQUFDO0NBQUEsQUE5S0QsSUE4S0M7QUE5S1ksd0NBQWM7QUFnTDNCLGtCQUFlLGNBQWMsQ0FBQyJ9

/***/ }),
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var MenuEntity = "Orion.Web.Menu";
var MegaMenuService = /** @class */ (function () {
    function MegaMenuService(swisService, xuiToastService, xuiDialogService, _t, inspectorService) {
        var _this = this;
        this.swisService = swisService;
        this.xuiToastService = xuiToastService;
        this.xuiDialogService = xuiDialogService;
        this._t = _t;
        this.inspectorService = inspectorService;
        this.clearCache = function () {
            return _this.swisService.invoke(MenuEntity, "ClearCache");
        };
        this.showError = function (response) {
            var error = _this.inspectorService.formatResponse(response);
            _this.xuiToastService.error(_this._t("We are having difficulty communicating with the Orion server. "
                + "Some of the backend services are not working properly. "
                + "<span class=\"sw-orion-menu-error__link\">Learn more</span>"), _this._t("Menu Failure"), {
                closeButton: false,
                stickyError: true,
                toastClass: "xui-toast sw-orion-menu-error__toast",
                onclick: function () {
                    var vm = { error: error, };
                    _this.xuiDialogService
                        .showModal({
                        template: __webpack_require__(21),
                        size: "lg",
                        windowClass: "sw-orion-menu-error__dialog"
                    }, {
                        title: _this._t("Error Details"),
                        viewModel: vm,
                        hideCancel: true,
                        status: "error",
                        actionButtonText: _this._t("Close"),
                    })
                        .finally(function () {
                        _this.showError(response);
                    });
                }
            });
        };
        this.buildTree = function (items) {
            var groupedByParentId = _.groupBy(items, "parentId");
            var sorter = function (menuItems) { return _.sortBy(menuItems, "sortOrder"); };
            var sorterWithFallback = function (menuItems) {
                if (_.uniqBy(menuItems, "sortOrder").length < 2) {
                    var numberAfterUnderscore_1 = function (id) {
                        var regex = /_([0-9]+)$/;
                        var match = regex.exec(id);
                        return match && match.length === 2
                            ? parseInt(match[1], 10)
                            : undefined;
                    };
                    if (_.every(menuItems, function (item) { return !!numberAfterUnderscore_1(item.id); })) {
                        return _.sortBy(menuItems, function (item) { return numberAfterUnderscore_1(item.id); });
                    }
                    return _.sortBy(menuItems, "id");
                }
                return sorter(menuItems);
            };
            _.forEach(items, function (item) {
                if (_.has(groupedByParentId, item.id)) {
                    item.items = sorterWithFallback(groupedByParentId[item.id]);
                }
            });
            var topLevelItems = _.filter(items, function (item) { return _.isNull(item.parentId); });
            return sorter(topLevelItems);
        };
        this.mapItem = function (record) {
            return {
                id: record.ID,
                parentId: record.ParentID,
                type: record.Type,
                name: record.Name,
                displayName: record.DisplayName,
                description: record.Description,
                url: record.Url,
                openInNewWindow: record.OpenInNewWindow,
                sortOrder: record.SortOrder
            };
        };
    }
    MegaMenuService.prototype.getItems = function () {
        var _this = this;
        var query = "\n            SELECT\n                ID\n                , ParentID\n                , Type\n                , Name\n                , DisplayName\n                , Description\n                , Url\n                , OpenInNewWindow\n                , SortOrder\n            FROM " + MenuEntity + "\n            ORDER BY SortOrder";
        return this.swisService
            .runQuery(query, null, null, null, {
            params: { swAlertOnError: false }
        })
            .then(function (result) { return _.map(result.rows, _this.mapItem); })
            .then(function (result) { return _this.buildTree(result); });
    };
    MegaMenuService = __decorate([
        __param(0, decorators_1.Inject("swisService")),
        __param(1, decorators_1.Inject("xuiToastService")),
        __param(2, decorators_1.Inject("xuiDialogService")),
        __param(3, decorators_1.Inject("getTextService")),
        __param(4, decorators_1.Inject("swErrorInspectorService")),
        __metadata("design:paramtypes", [Object, Object, Object, Function, Object])
    ], MegaMenuService);
    return MegaMenuService;
}());
exports.MegaMenuService = MegaMenuService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVnYU1lbnUtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1lZ2FNZW51LXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFJQSw0Q0FBdUM7QUFHdkMsSUFBTSxVQUFVLEdBQUcsZ0JBQWdCLENBQUM7QUFFcEM7SUFDSSx5QkFFWSxXQUF5QixFQUV6QixlQUFpQyxFQUVqQyxnQkFBZ0MsRUFFaEMsRUFBb0MsRUFFcEMsZ0JBQXdDO1FBVnBELGlCQVdLO1FBVE8sZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFFekIsb0JBQWUsR0FBZixlQUFlLENBQWtCO1FBRWpDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBZ0I7UUFFaEMsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFFcEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUF3QjtRQTBCN0MsZUFBVSxHQUFHO1lBQ2hCLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQU8sVUFBVSxFQUFFLFlBQVksQ0FBQztRQUF2RCxDQUF1RCxDQUFDO1FBRXJELGNBQVMsR0FBRyxVQUFDLFFBQW1CO1lBQ25DLElBQU0sS0FBSyxHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDN0QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQ3RCLEtBQUksQ0FBQyxFQUFFLENBQ0gsZ0VBQWdFO2tCQUM5RCx5REFBeUQ7a0JBQ3pELDZEQUEyRCxDQUNoRSxFQUNELEtBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLEVBQ3ZCO2dCQUNJLFdBQVcsRUFBRSxLQUFLO2dCQUNsQixXQUFXLEVBQUUsSUFBSTtnQkFDakIsVUFBVSxFQUFFLHNDQUFzQztnQkFDbEQsT0FBTyxFQUFFO29CQUNMLElBQU0sRUFBRSxHQUFHLEVBQUUsS0FBSyxPQUFBLEdBQUcsQ0FBQztvQkFDdEIsS0FBSSxDQUFDLGdCQUFnQjt5QkFDaEIsU0FBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxPQUFPLENBQVMscUNBQXFDLENBQUM7d0JBQ2hFLElBQUksRUFBRSxJQUFJO3dCQUNWLFdBQVcsRUFBRSw2QkFBNkI7cUJBQzdDLEVBQUU7d0JBQ0MsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsZUFBZSxDQUFDO3dCQUMvQixTQUFTLEVBQUUsRUFBRTt3QkFDYixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsTUFBTSxFQUFFLE9BQU87d0JBQ2YsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7cUJBQ3JDLENBQUM7eUJBQ0QsT0FBTyxDQUFDO3dCQUNMLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQzdCLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUM7YUFDSixDQUNKLENBQUM7UUFDTixDQUFDLENBQUM7UUFFTSxjQUFTLEdBQUcsVUFBQyxLQUFzQjtZQUN2QyxJQUFNLGlCQUFpQixHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZELElBQU0sTUFBTSxHQUFHLFVBQUMsU0FBMEIsSUFBSyxPQUFBLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQyxFQUFoQyxDQUFnQyxDQUFDO1lBQ2hGLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxTQUEwQjtnQkFDbEQsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlDLElBQU0sdUJBQXFCLEdBQUcsVUFBQyxFQUFVO3dCQUNyQyxJQUFNLEtBQUssR0FBRyxZQUFZLENBQUM7d0JBQzNCLElBQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBRTdCLE1BQU0sQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDOzRCQUM5QixDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7NEJBQ3hCLENBQUMsQ0FBQyxTQUFTLENBQUM7b0JBQ3BCLENBQUMsQ0FBQztvQkFFRixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxVQUFBLElBQUksSUFBSSxPQUFBLENBQUMsQ0FBQyx1QkFBcUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQy9ELE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxVQUFBLElBQUksSUFBSSxPQUFBLHVCQUFxQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO29CQUN2RSxDQUFDO29CQUVELE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDckMsQ0FBQztnQkFFRCxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdCLENBQUMsQ0FBQztZQUVGLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQUEsSUFBSTtnQkFDakIsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwQyxJQUFJLENBQUMsS0FBSyxHQUFHLGtCQUFrQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNoRSxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFNLGFBQWEsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxVQUFBLElBQUksSUFBSSxPQUFBLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUF2QixDQUF1QixDQUFDLENBQUM7WUFFdkUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUE7UUFFTyxZQUFPLEdBQUcsVUFBQyxNQUF1QjtZQUN0QyxNQUFNLENBQUM7Z0JBQ0gsRUFBRSxFQUFFLE1BQU0sQ0FBQyxFQUFFO2dCQUNiLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTtnQkFDekIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJO2dCQUNqQixJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7Z0JBQ2pCLFdBQVcsRUFBRSxNQUFNLENBQUMsV0FBVztnQkFDL0IsV0FBVyxFQUFFLE1BQU0sQ0FBQyxXQUFXO2dCQUMvQixHQUFHLEVBQUUsTUFBTSxDQUFDLEdBQUc7Z0JBQ2YsZUFBZSxFQUFFLE1BQU0sQ0FBQyxlQUFlO2dCQUN2QyxTQUFTLEVBQUUsTUFBTSxDQUFDLFNBQVM7YUFDOUIsQ0FBQztRQUNOLENBQUMsQ0FBQTtJQTlHRyxDQUFDO0lBRUUsa0NBQVEsR0FBZjtRQUFBLGlCQXFCQztRQXBCRyxJQUFNLEtBQUssR0FBRyxpU0FXSCxVQUFVLHFDQUNFLENBQUM7UUFFeEIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXO2FBQ2xCLFFBQVEsQ0FBa0IsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFO1lBQ2hELE1BQU0sRUFBRSxFQUFFLGNBQWMsRUFBRSxLQUFLLEVBQUU7U0FDcEMsQ0FBQzthQUNELElBQUksQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxLQUFJLENBQUMsT0FBTyxDQUFDLEVBQWhDLENBQWdDLENBQUM7YUFDaEQsSUFBSSxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFuQ1EsZUFBZTtRQUVuQixXQUFBLG1CQUFNLENBQUMsYUFBYSxDQUFDLENBQUE7UUFFckIsV0FBQSxtQkFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUE7UUFFekIsV0FBQSxtQkFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7UUFFMUIsV0FBQSxtQkFBTSxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFFeEIsV0FBQSxtQkFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUE7O09BVjdCLGVBQWUsQ0EySDNCO0lBQUQsc0JBQUM7Q0FBQSxBQTNIRCxJQTJIQztBQTNIWSwwQ0FBZSJ9

/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
* @ngdoc service
*
* @name orion.services.swNetObjectOpidConverterService
*
* @description
* Service provides conversion between NetObjects and Opids.
* Service is a wrapper for appropriate REST API.
*
*/
var NetObjectOpidConverterService = /** @class */ (function () {
    /** @ngInject */
    NetObjectOpidConverterService.$inject = ["swApi"];
    function NetObjectOpidConverterService(swApi) {
        this.swApi = swApi;
    }
    /**
     *  @ngdoc method
     *  @name netObjectsToOpids
     *  @methodOf orion.services.swNetObjectOpidConverterService
     *  @description Converts array of NetObjects to Opids.
     *  @param {INetObject[]} netObjects Array of NetObjects for conversion
     *  @returns {IPromise<string[]>} Array of converted Opids.
     *  Can contain `null` elements for netObjects that could not be converted.
     **/
    NetObjectOpidConverterService.prototype.netObjectsToOpids = function (netObjects) {
        return this.api("netobjects-to-opids")
            .get({
            netObjects: _.map(netObjects, function (n) { return n.netObject; }),
            instanceSiteIds: _.map(netObjects, function (n) { return n.instanceSiteId || 0; })
        })
            .then(function (result) { return result.plain(); });
    };
    /**
     *  @ngdoc method
     *  @name opidsToNetObjects
     *  @methodOf orion.services.swNetObjectOpidConverterService
     *  @description Converts array of Opids to NetObjects.
     *  @param {string[]} opids Array of Opids for conversion
     *  @returns {IPromise<INetObject[]>} Array of converted NetObjects.
     *  Can contain `null` elements for Opids that could not be converted.
     **/
    NetObjectOpidConverterService.prototype.opidsToNetObjects = function (opids) {
        return this.api("opids-to-netobjects")
            .get({ opids: opids })
            .then(function (result) { return result.plain(); });
    };
    /**
     *  @ngdoc method
     *  @name formatOpid
     *  @methodOf orion.services.swNetObjectOpidConverterService
     *  @description Composes opid string from given IOpid data.
     *  @param {IOpid} data IOpid structure.
     *  @returns {string} Opid.
     */
    NetObjectOpidConverterService.prototype.formatOpid = function (data) {
        return (data && data.entityType && data.entityIds && data.entityIds.length)
            ? "" + (data.instanceSiteId ? data.instanceSiteId + "_" : "") + data.entityType + "_" + data.entityIds.join("_")
            : null;
    };
    /**
     *  @ngdoc method
     *  @name parseOpid
     *  @methodOf orion.services.swNetObjectOpidConverterService
     *  @description Parses opid string back into IOpid data.
     *  @param {string} data Opid string.
     *  @returns {IOpid} Parsed IOpid data.
     */
    NetObjectOpidConverterService.prototype.parseOpid = function (opid) {
        var rxOpid = /^(?:(\d*)_)?([a-zA-Z0-9.]+)_([\d_]+)$/;
        var matches = rxOpid.exec(opid);
        if (!matches) {
            return null;
        }
        return {
            instanceSiteId: Number(matches[1] || 0),
            entityType: matches[2],
            entityIds: matches[3].split("_").map(Number),
        };
    };
    NetObjectOpidConverterService.prototype.api = function (method) {
        return this.swApi.api(false).one("converters").one(method);
    };
    return NetObjectOpidConverterService;
}());
exports.NetObjectOpidConverterService = NetObjectOpidConverterService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV0T2JqZWN0T3BpZENvbnZlcnRlci1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibmV0T2JqZWN0T3BpZENvbnZlcnRlci1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBSUM7Ozs7Ozs7OztFQVNFO0FBQ0g7SUFDSSxnQkFBZ0I7SUFDaEIsdUNBQW9CLEtBQW9CO1FBQXBCLFVBQUssR0FBTCxLQUFLLENBQWU7SUFBSSxDQUFDO0lBRTdDOzs7Ozs7OztRQVFJO0lBQ0cseURBQWlCLEdBQXhCLFVBQXlCLFVBQXdCO1FBQzdDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDO2FBQ2pDLEdBQUcsQ0FBQztZQUNELFVBQVUsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxTQUFTLEVBQVgsQ0FBVyxDQUFDO1lBQy9DLGVBQWUsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxFQUFyQixDQUFxQixDQUFDO1NBQ2pFLENBQUM7YUFDRCxJQUFJLENBQUMsVUFBQyxNQUFtQixJQUFLLE9BQUEsTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFkLENBQWMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7Ozs7Ozs7UUFRSTtJQUNHLHlEQUFpQixHQUF4QixVQUF5QixLQUFlO1FBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDO2FBQ2pDLEdBQUcsQ0FBQyxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUM7YUFDZCxJQUFJLENBQUMsVUFBQyxNQUFtQixJQUFLLE9BQUEsTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFkLENBQWMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ0ksa0RBQVUsR0FBakIsVUFBa0IsSUFBVztRQUN6QixNQUFNLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQ3ZFLENBQUMsQ0FBQyxNQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFJLElBQUksQ0FBQyxjQUFjLE1BQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFHLElBQUksQ0FBQyxVQUFVLFNBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFHO1lBQ3pHLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDZixDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNJLGlEQUFTLEdBQWhCLFVBQWlCLElBQVk7UUFDekIsSUFBTSxNQUFNLEdBQUcsdUNBQXVDLENBQUM7UUFDdkQsSUFBTSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDWCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxNQUFNLENBQUM7WUFDSCxjQUFjLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdkMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDdEIsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztTQUMvQyxDQUFDO0lBQ04sQ0FBQztJQUVPLDJDQUFHLEdBQVgsVUFBWSxNQUFjO1FBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFDTCxvQ0FBQztBQUFELENBQUMsQUEzRUQsSUEyRUM7QUEzRVksc0VBQTZCIn0=

/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/*
using Local(Timestamp) because serialization to javascript thinks it's Local Time
(but SWIS provides UTC)
*/
var swqlNotifications = "\nSELECT\n    [NotificationID],\n    [TypeID],\n    ToLocal([Timestamp]) AS Timestamp,\n    [ImageLink],\n    [DisplayAs],\n    [CustomDismissButtonText],\n    [HideDismissButton],\n    [AcknowledgeByItemId],\n    [DetailsPageLink],\n    [DetailsPageLinkCaption],\n    [Message]\nFROM Orion.NotificationItemGrouped\nORDER BY [Timestamp] DESC\n";
var swqlTypePermissions = "\nSELECT\n    NotificationTypeID,\n    RequiredRoleID\nFROM Orion.NotificationTypePermission\n";
var notificationEntities = {
    item: "Orion.NotificationItemGrouped",
    permission: "Orion.NotificationTypePermission",
};
var notificationGuids = {
    Maintenance: "561BE782-187F-4977-B5C4-B8666E73E582",
    PollerLimit: "C7070869-B2B8-42ED-8472-7F24056435D9",
};
var typeGuids = {
    Eval: "6EE3D05F-7555-4E3E-9338-AA338834FE36",
    Licensing: "3604B78F-77F9-429B-B5E1-03462A563E66",
    Upgrade: "60FB0695-15BC-4F16-9DCA-A844AE17E714",
};
var NotifyService = /** @class */ (function () {
    /** @ngInject */
    NotifyService.$inject = ["$q", "$log", "pollingService", "swDemoService", "swisService", "constants", "webAdminService", "swMegaMenuService"];
    function NotifyService($q, $log, pollingService, swDemoService, swisService, constants, webAdminService, swMegaMenuService) {
        var _this = this;
        this.$q = $q;
        this.$log = $log;
        this.pollingService = pollingService;
        this.swDemoService = swDemoService;
        this.swisService = swisService;
        this.constants = constants;
        this.webAdminService = webAdminService;
        this.swMegaMenuService = swMegaMenuService;
        this.notifications = [];
        this.getNotifications = function () { return _this.notifications; };
        this.hasError = function () {
            return !!_this.error;
        };
        this.dismissNotification = function (notification) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            if (notification.AcknowledgeByItemId) {
                return _this.swisService.invoke(notificationEntities.item, "AcknowledgeById", [
                    notification.NotificationID,
                    _this.constants.user.AccountID,
                    moment().toISOString()
                ]);
            }
            return _this.swisService.invoke(notificationEntities.item, "AcknowledgeByType", [
                notification.TypeID,
                true,
                _this.constants.user.AccountID,
                moment().toISOString()
            ]);
        };
        this.dismissAllNotifications = function () {
            if (!_this.lastPoll) {
                return;
            }
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            return _this.swisService.invoke(notificationEntities.item, "AcknowledgeAll", [
                _this.constants.user.AccountID,
                _this.lastPoll
            ]).then(function () {
                _this.notifications = [];
            });
        };
        this.queryTypePermissions = function () {
            return _this.swisService
                .runQuery(swqlTypePermissions, null, null, { swLogOutOnAuthError: true }, {
                params: { swAlertOnError: false }
            })
                .then(function (data) { return _.reduce(data.rows, function (result, row) {
                var roles = (result[row.NotificationTypeID] || (result[row.NotificationTypeID] = []));
                roles.push(row.RequiredRoleID);
                return result;
            }, {}); });
        };
        this.queryNotifications = function () {
            return _this.swisService
                .runQuery(swqlNotifications, null, null, { swLogOutOnAuthError: true }, {
                params: { swAlertOnError: false }
            })
                .then(function (result) { return result.rows; });
        };
        this.checkForUpdates = function () {
            return _this.$q.all([
                _this.queryTypePermissions(),
                _this.queryNotifications()
            ]).then(function (_a) {
                var typeRoles = _a[0], notifications = _a[1];
                _this.error = null;
                _this.notifications = _this.swDemoService.isDemoMode() ? notifications : _this.filterNotifications(notifications, typeRoles);
                _this.lastPoll = (_this.notifications[0] || { Timestamp: null }).Timestamp;
            }, function (error) {
                _this.$log.debug(error);
                _this.error = error;
                _this.notifications = [];
                _this.swMegaMenuService.showError(error);
            });
        };
        this.activate = function () {
            _this.pollingService.register(_this.checkForUpdates, true);
        };
        this.activate();
    }
    // AllowAdmin:              1
    // AllowNodeManagement:     2
    // AllowCustomize:          3
    // AllowEventClear:         4
    NotifyService.prototype.getUserRoles = function (user) {
        var roles = [];
        if (user) {
            if (user.AllowAdmin) {
                roles.push(1);
            }
            if (user.AllowNodeManagement) {
                roles.push(2);
            }
            if (user.AllowCustomize) {
                roles.push(3);
            }
            if (user.AllowEventClear) {
                roles.push(4);
            }
        }
        return roles;
    };
    NotifyService.prototype.suppressReminder = function (notificationType) {
        var _this = this;
        var actions = {
            Eval: function () {
                return _this.webAdminService.saveWebSetting("EvaluationExpiration-Check", 0)
                    .then(function () { return ({
                    typeGuid: typeGuids.Eval,
                }); });
            },
            Maintenance: function () {
                return _this.webAdminService.saveWebSetting("MaintenanceExpiration-Check", 0)
                    .then(function () { return ({
                    notificationGuid: notificationGuids.Maintenance,
                }); });
            },
            Licensing: function () {
                return _this.webAdminService.saveWebSetting("LicenseSaturation-Disable", 1)
                    .then(function () { return ({
                    typeGuid: typeGuids.Licensing,
                }); });
            },
            PollerLimit: function () {
                return _this.webAdminService.saveWebSetting("PollerLimit-Check", 0)
                    .then(function () { return ({
                    notificationGuid: notificationGuids.PollerLimit,
                }); });
            },
            Upgrade: function () {
                return _this.webAdminService.saveWebSetting("MaintenanceRenewals-Check", 1)
                    .then(function () { return ({
                    typeGuid: typeGuids.Upgrade,
                }); });
            },
        };
        var action = actions[notificationType];
        if (!action) {
            throw new Error("Unknown notificationType [" + notificationType + "]");
        }
        return action().then(function (data) {
            if (data.notificationGuid) {
                return _this.swisService.invoke(notificationEntities.item, "AcknowledgeById", [
                    data.notificationGuid,
                    _this.constants.user.AccountID,
                    moment().toISOString(),
                ]);
            }
            if (data.typeGuid) {
                return _this.swisService.invoke(notificationEntities.item, "AcknowledgeByType", [
                    data.typeGuid,
                    true,
                    _this.constants.user.AccountID,
                    moment().toISOString(),
                ]);
            }
        });
    };
    NotifyService.prototype.hasRoles = function (userRoles, requiredRoles) {
        return _.isEqual(_.intersection(userRoles, requiredRoles), requiredRoles);
    };
    NotifyService.prototype.filterNotifications = function (notifications, typeRoles) {
        var _this = this;
        var userRoles = this.getUserRoles(this.constants.user);
        return _.filter(notifications, function (n) { return _this.hasRoles(userRoles, typeRoles[n.TypeID]); });
    };
    return NotifyService;
}());
exports.NotifyService = NotifyService;
exports.default = NotifyService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZ5LXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJub3RpZnktc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0NBQW9DOztBQXdCcEM7OztFQUdFO0FBQ0YsSUFBTSxpQkFBaUIsR0FBRyx5VkFlekIsQ0FBQztBQUVGLElBQU0sbUJBQW1CLEdBQUcsZ0dBSzNCLENBQUM7QUFFRixJQUFNLG9CQUFvQixHQUFHO0lBQ3pCLElBQUksRUFBRSwrQkFBK0I7SUFDckMsVUFBVSxFQUFFLGtDQUFrQztDQUNqRCxDQUFDO0FBRUYsSUFBTSxpQkFBaUIsR0FBRztJQUN0QixXQUFXLEVBQUUsc0NBQXNDO0lBQ25ELFdBQVcsRUFBRSxzQ0FBc0M7Q0FDdEQsQ0FBQztBQUVGLElBQU0sU0FBUyxHQUFHO0lBQ2QsSUFBSSxFQUFFLHNDQUFzQztJQUM1QyxTQUFTLEVBQUUsc0NBQXNDO0lBQ2pELE9BQU8sRUFBRSxzQ0FBc0M7Q0FDbEQsQ0FBQztBQUVGO0lBTUksZ0JBQWdCO0lBQ2hCLHVCQUNZLEVBQWEsRUFDYixJQUFpQixFQUNqQixjQUErQixFQUMvQixhQUEyQixFQUMzQixXQUF5QixFQUN6QixTQUFxQixFQUNyQixlQUFpQyxFQUNqQyxpQkFBbUM7UUFSL0MsaUJBV0M7UUFWVyxPQUFFLEdBQUYsRUFBRSxDQUFXO1FBQ2IsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNqQixtQkFBYyxHQUFkLGNBQWMsQ0FBaUI7UUFDL0Isa0JBQWEsR0FBYixhQUFhLENBQWM7UUFDM0IsZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFDekIsY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUNyQixvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFDakMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQWJ2QyxrQkFBYSxHQUFvQixFQUFFLENBQUM7UUFrQnJDLHFCQUFnQixHQUFHLGNBQXVCLE9BQUEsS0FBSSxDQUFDLGFBQWEsRUFBbEIsQ0FBa0IsQ0FBQztRQUU3RCxhQUFRLEdBQUc7WUFDZCxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUM7UUFDeEIsQ0FBQyxDQUFBO1FBeUJNLHdCQUFtQixHQUFHLFVBQUMsWUFBMkI7WUFDckQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLEtBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztnQkFDeEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDN0IsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLEVBQUU7b0JBQ3pFLFlBQVksQ0FBQyxjQUFjO29CQUMzQixLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTO29CQUM3QixNQUFNLEVBQUUsQ0FBQyxXQUFXLEVBQUU7aUJBQ3pCLENBQUMsQ0FBQztZQUNQLENBQUM7WUFDRCxNQUFNLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLG1CQUFtQixFQUFFO2dCQUMzRSxZQUFZLENBQUMsTUFBTTtnQkFDbkIsSUFBSTtnQkFDSixLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUM3QixNQUFNLEVBQUUsQ0FBQyxXQUFXLEVBQUU7YUFDekIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRUssNEJBQXVCLEdBQUc7WUFDN0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDakIsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxLQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQ3hDLE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzdCLENBQUM7WUFDRCxNQUFNLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLGdCQUFnQixFQUFFO2dCQUN4RSxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUM3QixLQUFJLENBQUMsUUFBUTthQUNoQixDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBdURNLHlCQUFvQixHQUFHO1lBQzNCLE9BQUEsS0FBSSxDQUFDLFdBQVc7aUJBQ1gsUUFBUSxDQUFzQixtQkFBbUIsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUUsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQzNGLE1BQU0sRUFBRSxFQUFFLGNBQWMsRUFBRSxLQUFLLEVBQUU7YUFDcEMsQ0FBQztpQkFDRCxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxDQUFDLENBQUMsTUFBTSxDQUNsQixJQUFJLENBQUMsSUFBSSxFQUNULFVBQUMsTUFBTSxFQUFFLEdBQUc7Z0JBQ1IsSUFBTSxLQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEYsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDbEIsQ0FBQyxFQUNELEVBQXNCLENBQ3pCLEVBUmEsQ0FRYixDQUFDO1FBWk4sQ0FZTSxDQUFDO1FBRUgsdUJBQWtCLEdBQUc7WUFDekIsT0FBQSxLQUFJLENBQUMsV0FBVztpQkFDWCxRQUFRLENBQWdCLGlCQUFpQixFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRSxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsRUFBRTtnQkFDbkYsTUFBTSxFQUFFLEVBQUUsY0FBYyxFQUFFLEtBQUssRUFBRTthQUNwQyxDQUFDO2lCQUNELElBQUksQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxJQUFJLEVBQVgsQ0FBVyxDQUFDO1FBSmhDLENBSWdDLENBQUM7UUFpQjlCLG9CQUFlLEdBQUc7WUFDckIsT0FBQSxLQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQztnQkFDUixLQUFJLENBQUMsb0JBQW9CLEVBQUU7Z0JBQzNCLEtBQUksQ0FBQyxrQkFBa0IsRUFBRTthQUM1QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsRUFBMEI7b0JBQXpCLGlCQUFTLEVBQUUscUJBQWE7Z0JBQzlCLEtBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUNsQixLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDMUgsS0FBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDN0UsQ0FBQyxFQUFFLFVBQUMsS0FBSztnQkFDTCxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdkIsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Z0JBQ25CLEtBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixLQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVDLENBQUMsQ0FBQztRQVpGLENBWUUsQ0FBQztRQUVDLGFBQVEsR0FBRztZQUNmLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDN0QsQ0FBQyxDQUFDO1FBL0tFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBUUQsNkJBQTZCO0lBQzdCLDZCQUE2QjtJQUM3Qiw2QkFBNkI7SUFDN0IsNkJBQTZCO0lBQ3JCLG9DQUFZLEdBQXBCLFVBQXFCLElBQWdCO1FBQ2pDLElBQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNqQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1AsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEIsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEIsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQixDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQXNDTSx3Q0FBZ0IsR0FBdkIsVUFBd0IsZ0JBQXdCO1FBQWhELGlCQW1EQztRQWxERyxJQUFNLE9BQU8sR0FBbUQ7WUFDNUQsSUFBSSxFQUFFO2dCQUNGLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsNEJBQTRCLEVBQUUsQ0FBQyxDQUFDO3FCQUMvRCxJQUFJLENBQUMsY0FBTSxPQUFBLENBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVMsQ0FBQyxJQUFJO2lCQUMzQixDQUFDLEVBRlUsQ0FFVixDQUFDO1lBSFAsQ0FHTztZQUNYLFdBQVcsRUFBRTtnQkFDVCxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLDZCQUE2QixFQUFFLENBQUMsQ0FBQztxQkFDaEUsSUFBSSxDQUFDLGNBQU0sT0FBQSxDQUFDO29CQUNULGdCQUFnQixFQUFFLGlCQUFpQixDQUFDLFdBQVc7aUJBQ2xELENBQUMsRUFGVSxDQUVWLENBQUM7WUFIUCxDQUdPO1lBQ1gsU0FBUyxFQUFFO2dCQUNQLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsMkJBQTJCLEVBQUUsQ0FBQyxDQUFDO3FCQUM5RCxJQUFJLENBQUMsY0FBTSxPQUFBLENBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVMsQ0FBQyxTQUFTO2lCQUNoQyxDQUFDLEVBRlUsQ0FFVixDQUFDO1lBSFAsQ0FHTztZQUNYLFdBQVcsRUFBRTtnQkFDVCxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQztxQkFDdEQsSUFBSSxDQUFDLGNBQU0sT0FBQSxDQUFDO29CQUNULGdCQUFnQixFQUFFLGlCQUFpQixDQUFDLFdBQVc7aUJBQ2xELENBQUMsRUFGVSxDQUVWLENBQUM7WUFIUCxDQUdPO1lBQ1gsT0FBTyxFQUFFO2dCQUNMLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsMkJBQTJCLEVBQUUsQ0FBQyxDQUFDO3FCQUM5RCxJQUFJLENBQUMsY0FBTSxPQUFBLENBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVMsQ0FBQyxPQUFPO2lCQUM5QixDQUFDLEVBRlUsQ0FFVixDQUFDO1lBSFAsQ0FHTztTQUNkLENBQUM7UUFFRixJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN6QyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDVixNQUFNLElBQUksS0FBSyxDQUFDLCtCQUE2QixnQkFBZ0IsTUFBRyxDQUFDLENBQUM7UUFDdEUsQ0FBQztRQUVELE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ3JCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLEVBQUU7b0JBQ3pFLElBQUksQ0FBQyxnQkFBZ0I7b0JBQ3JCLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVM7b0JBQzdCLE1BQU0sRUFBRSxDQUFDLFdBQVcsRUFBRTtpQkFDekIsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixNQUFNLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLG1CQUFtQixFQUFFO29CQUMzRSxJQUFJLENBQUMsUUFBUTtvQkFDYixJQUFJO29CQUNKLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVM7b0JBQzdCLE1BQU0sRUFBRSxDQUFDLFdBQVcsRUFBRTtpQkFDekIsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQXdCTyxnQ0FBUSxHQUFoQixVQUFpQixTQUFtQixFQUFFLGFBQXVCO1FBQ3pELE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUNaLENBQUMsQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGFBQWEsQ0FBQyxFQUN4QyxhQUFhLENBQ2hCLENBQUM7SUFDTixDQUFDO0lBRU8sMkNBQW1CLEdBQTNCLFVBQTRCLGFBQThCLEVBQUUsU0FBMkI7UUFBdkYsaUJBTUM7UUFMRyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekQsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQ1gsYUFBYSxFQUNiLFVBQUEsQ0FBQyxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUE3QyxDQUE2QyxDQUNyRCxDQUFDO0lBQ04sQ0FBQztJQW9CTCxvQkFBQztBQUFELENBQUMsQUFqTUQsSUFpTUM7QUFqTVksc0NBQWE7QUFtTTFCLGtCQUFlLGFBQWEsQ0FBQyJ9

/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
* @ngdoc service
*
* @name orion.services.swOrionNavigationService
*
* @description
* Service provides basic navigation for common Orion pages.
*
*/
var OrionNavigationService = /** @class */ (function () {
    /** @ngInject */
    OrionNavigationService.$inject = ["$window", "$state", "swNetObjectOpidConverterService"];
    function OrionNavigationService($window, $state, swNetObjectOpidConverterService) {
        var _this = this;
        this.$window = $window;
        this.$state = $state;
        this.swNetObjectOpidConverterService = swNetObjectOpidConverterService;
        /**
         *  @ngdoc method
         *  @name goToLogout
         *  @methodOf orion.services.swOrionNavigationService
         *  @description Redirect to login page. Redirect will go through logout page making user logged out.
         **/
        this.goToLogout = function () {
            var url = "/Orion/Logout.aspx";
            _this.goToUrl(url);
        };
    }
    /**
     *  @ngdoc method
     *  @name goToView
     *  @methodOf orion.services.swOrionNavigationService
     *  @description Redirect to detail view page for appropriate Opid.
     *  Current implementation always redirect to legacy view. Future implementation will check if there is new Apollo
     *  view for provided Opid and redirect to it otherwise redirect to legacy.
     *  @param {string} opid Opid of entity to be displayed on detail view
     **/
    OrionNavigationService.prototype.goToView = function (opid) {
        this.goToLegacyView(opid);
    };
    /**
     *  @ngdoc method
     *  @name goToLegacyView
     *  @methodOf orion.services.swOrionNavigationService
     *  @description Redirect to legacy detail view page for appropriate Opid.
     *  @param {string} opid Opid of entity to be displayed on detail view
     **/
    OrionNavigationService.prototype.goToLegacyView = function (opid) {
        var _this = this;
        this.swNetObjectOpidConverterService.opidsToNetObjects([opid])
            .then(function (opids) { return _this.goToLegacyViewByNetObject(opids[0].netObject, opids[0].instanceSiteId); });
    };
    /**
     *  @ngdoc method
     *  @name goToLegacyViewByNetObject
     *  @methodOf orion.services.swOrionNavigationService
     *  @description Redirect to legacy detail view page for appropriate NetObject.
     *  @param {string} netObject NetObject of entity to be displayed on detail view
     *  @param {number} instanceSiteId InstanceSiteId associated with `netObject`
     **/
    OrionNavigationService.prototype.goToLegacyViewByNetObject = function (netObject, instanceSiteId) {
        var viewUrl = this.buildLegacyViewUrl(netObject, instanceSiteId);
        this.goToUrl(viewUrl);
    };
    /**
     *  @ngdoc method
     *  @name goToErrorPage
     *  @methodOf orion.services.swOrionNavigationService
     *  @description Displays Apollo error page.
     *  Works only on full Apollo pages.
     *  @param {Partial<IOrionErrorStateParams>} params Params containing error details
     **/
    OrionNavigationService.prototype.goToErrorPage = function (params) {
        this.$state.go("Error", params);
    };
    OrionNavigationService.prototype.goToAllActiveAlerts = function () {
        var url = "/Orion/Netperfmon/Alerts.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToManageAlerts = function () {
        var url = "/Orion/Alerts/Default.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToManageReports = function () {
        var url = "/Orion/Reports/Default.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToManageViews = function () {
        var url = "/Orion/Admin/ListViews.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToManageCustomProperties = function () {
        var url = "/Orion/Admin/CPE/Default.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToAddNode = function () {
        var url = "/Orion/Nodes/Add/Default.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToManageAccounts = function () {
        var url = "/Orion/Admin/Accounts/Accounts.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToMessageCenter = function () {
        var url = "/Orion/Netperfmon/OrionMessages.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToEvents = function () {
        var url = "/Orion/Netperfmon/Events.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToAllReports = function () {
        var url = "/Orion/Reports/ViewReports.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToDiscovery = function () {
        var url = "/Discovery/Default.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToLegacyManageNodes = function () {
        var url = "/Orion/Nodes/Default.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToSettings = function () {
        var url = "/Orion/Admin/Default_Orion.aspx";
        this.goToUrl(url);
    };
    OrionNavigationService.prototype.goToUrl = function (url) {
        this.$window.location.href = url;
    };
    OrionNavigationService.prototype.buildLegacyViewUrl = function (netObject, instanceSiteId) {
        var viewUrl = "/Orion/View.aspx?NetObject=" + netObject;
        if (angular.isNumber(instanceSiteId)) {
            viewUrl = "/Server/" + instanceSiteId + "/" + viewUrl;
        }
        return viewUrl;
    };
    return OrionNavigationService;
}());
exports.OrionNavigationService = OrionNavigationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25OYXZpZ2F0aW9uLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmlvbk5hdmlnYXRpb24tc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUlDOzs7Ozs7OztFQVFFO0FBQ0g7SUFDSSxnQkFBZ0I7SUFDaEIsZ0NBQW9CLE9BQXVCLEVBQ3ZCLE1BQXFCLEVBQ3JCLCtCQUErRDtRQUZuRixpQkFFdUY7UUFGbkUsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFDdkIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixvQ0FBK0IsR0FBL0IsK0JBQStCLENBQWdDO1FBRW5GOzs7OztZQUtJO1FBQ0csZUFBVSxHQUFHO1lBQ2hCLElBQU0sR0FBRyxHQUFHLG9CQUFvQixDQUFDO1lBRWpDLEtBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEIsQ0FBQyxDQUFDO0lBWm9GLENBQUM7SUFjdkY7Ozs7Ozs7O1FBUUk7SUFDRyx5Q0FBUSxHQUFmLFVBQWdCLElBQVk7UUFDeEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQ7Ozs7OztRQU1JO0lBQ0csK0NBQWMsR0FBckIsVUFBc0IsSUFBWTtRQUFsQyxpQkFHQztRQUZHLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3pELElBQUksQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsRUFBM0UsQ0FBMkUsQ0FBQyxDQUFDO0lBQ3BHLENBQUM7SUFFRDs7Ozs7OztRQU9JO0lBQ0csMERBQXlCLEdBQWhDLFVBQWlDLFNBQWlCLEVBQUUsY0FBdUI7UUFDdkUsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUVuRSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7Ozs7OztRQU9JO0lBQ0csOENBQWEsR0FBcEIsVUFBcUIsTUFBd0M7UUFDekQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFTSxvREFBbUIsR0FBMUI7UUFDSSxJQUFNLEdBQUcsR0FBRywrQkFBK0IsQ0FBQztRQUU1QyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFTSxpREFBZ0IsR0FBdkI7UUFDSSxJQUFNLEdBQUcsR0FBRyw0QkFBNEIsQ0FBQztRQUV6QyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFTSxrREFBaUIsR0FBeEI7UUFDSSxJQUFNLEdBQUcsR0FBRyw2QkFBNkIsQ0FBQztRQUUxQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFTSxnREFBZSxHQUF0QjtRQUNJLElBQU0sR0FBRyxHQUFHLDZCQUE2QixDQUFDO1FBRTFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVNLDJEQUEwQixHQUFqQztRQUNJLElBQU0sR0FBRyxHQUFHLCtCQUErQixDQUFDO1FBRTVDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVNLDRDQUFXLEdBQWxCO1FBQ0ksSUFBTSxHQUFHLEdBQUcsK0JBQStCLENBQUM7UUFFNUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRU0sbURBQWtCLEdBQXpCO1FBQ0ksSUFBTSxHQUFHLEdBQUcscUNBQXFDLENBQUM7UUFFbEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRU0sa0RBQWlCLEdBQXhCO1FBQ0ksSUFBTSxHQUFHLEdBQUcsc0NBQXNDLENBQUM7UUFFbkQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRU0sMkNBQVUsR0FBakI7UUFDSSxJQUFNLEdBQUcsR0FBRywrQkFBK0IsQ0FBQztRQUU1QyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFTSwrQ0FBYyxHQUFyQjtRQUNJLElBQU0sR0FBRyxHQUFHLGlDQUFpQyxDQUFDO1FBRTlDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVNLDhDQUFhLEdBQXBCO1FBQ0ksSUFBTSxHQUFHLEdBQUcseUJBQXlCLENBQUM7UUFFdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRU0sc0RBQXFCLEdBQTVCO1FBQ0ksSUFBTSxHQUFHLEdBQUcsMkJBQTJCLENBQUM7UUFFeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRU0sNkNBQVksR0FBbkI7UUFDSSxJQUFNLEdBQUcsR0FBRyxpQ0FBaUMsQ0FBQztRQUU5QyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFTyx3Q0FBTyxHQUFmLFVBQWdCLEdBQVc7UUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztJQUNyQyxDQUFDO0lBRU8sbURBQWtCLEdBQTFCLFVBQTJCLFNBQWlCLEVBQUUsY0FBdUI7UUFDakUsSUFBSSxPQUFPLEdBQUcsZ0NBQThCLFNBQVcsQ0FBQztRQUV4RCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQyxPQUFPLEdBQUcsYUFBVyxjQUFjLFNBQUksT0FBUyxDQUFDO1FBQ3JELENBQUM7UUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFDTCw2QkFBQztBQUFELENBQUMsQUFoS0QsSUFnS0M7QUFoS1ksd0RBQXNCIn0=

/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var OrionViewInfoService = /** @class */ (function () {
    function OrionViewInfoService() {
    }
    return OrionViewInfoService;
}());
exports.OrionViewInfoService = OrionViewInfoService;
exports.default = OrionViewInfoService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25WaWV3SW5mby1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib3Jpb25WaWV3SW5mby1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFBQTtJQUVBLENBQUM7SUFBRCwyQkFBQztBQUFELENBQUMsQUFGRCxJQUVDO0FBRlksb0RBQW9CO0FBSWpDLGtCQUFlLG9CQUFvQixDQUFDIn0=

/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var PingService = /** @class */ (function () {
    function PingService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.get = function (requiresAdmin) {
            return _this.prepRequest(requiresAdmin).get();
        };
        this.post = function (requiresAdmin) {
            return _this.prepRequest(requiresAdmin).post();
        };
        this.put = function (requiresAdmin) {
            return _this.prepRequest(requiresAdmin).put();
        };
        this.delete = function (requiresAdmin) {
            return _this.prepRequest(requiresAdmin).remove();
        };
        this.prepRequest = function (requiresAdmin) {
            return _this.swApi.api(false).one(requiresAdmin ? _this.adminUrl : _this.url);
        };
    }
    Object.defineProperty(PingService.prototype, "url", {
        get: function () {
            return "ping";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PingService.prototype, "adminUrl", {
        get: function () {
            return "ping/admin";
        },
        enumerable: true,
        configurable: true
    });
    PingService.$inject = ["swApi", "xuiToastService", "$log"];
    return PingService;
}());
exports.PingService = PingService;
exports.default = PingService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGluZy1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGluZy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBS3BDO0lBSUkscUJBQ1ksS0FBb0I7UUFEaEMsaUJBRUk7UUFEUSxVQUFLLEdBQUwsS0FBSyxDQUFlO1FBR3pCLFFBQUcsR0FBRyxVQUFDLGFBQXVCO1lBQ2pDLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ2pELENBQUMsQ0FBQztRQUVLLFNBQUksR0FBRyxVQUFDLGFBQXVCO1lBQ2xDLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2xELENBQUMsQ0FBQztRQUVLLFFBQUcsR0FBRyxVQUFDLGFBQXVCO1lBQ2pDLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ2pELENBQUMsQ0FBQztRQUVLLFdBQU0sR0FBRyxVQUFDLGFBQXVCO1lBQ3BDLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3BELENBQUMsQ0FBQztRQUVNLGdCQUFXLEdBQUcsVUFBQyxhQUF1QjtZQUMxQyxNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9FLENBQUMsQ0FBQztJQXBCQyxDQUFDO0lBc0JKLHNCQUFZLDRCQUFHO2FBQWY7WUFDSSxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQ2xCLENBQUM7OztPQUFBO0lBRUQsc0JBQVksaUNBQVE7YUFBcEI7WUFDSSxNQUFNLENBQUMsWUFBWSxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBaENhLG1CQUFPLEdBQUcsQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFpQ2pFLGtCQUFDO0NBQUEsQUFuQ0QsSUFtQ0M7QUFuQ1ksa0NBQVc7QUFxQ3hCLGtCQUFlLFdBQVcsQ0FBQyJ9

/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var PollingService = /** @class */ (function () {
    function PollingService($interval, $log, constants) {
        var _this = this;
        this.$interval = $interval;
        this.$log = $log;
        this.constants = constants;
        this.callbacks = [];
        this.version = "1.0";
        this.register = function (callback, runImmediate) {
            var existing = _this.findCallback(callback);
            if (!existing) {
                _this.callbacks.push(callback);
                if (runImmediate) {
                    callback();
                }
            }
        };
        this.unregister = function (callback) {
            for (var index = 0; index < _this.callbacks.length; index++) {
                if (angular.equals(_this.callbacks[index], callback)) {
                    _this.callbacks.splice(index, 1);
                }
            }
        };
        this.findCallback = function (callback) {
            return _.find(_this.callbacks, function (cb) {
                return angular.equals(cb, callback);
            });
        };
        this.runCallbacks = function () {
            _.each(_this.callbacks, function (cb) {
                try {
                    cb();
                }
                catch (ex) {
                    _this.$log.error(ex);
                }
            });
        };
        this.startPoller = function () {
            _this.$interval(_this.runCallbacks, _this.constants.environment.autoRefreshSeconds * 1000);
        };
        this.startPoller();
    }
    PollingService = __decorate([
        __param(0, decorators_1.Inject("$interval")),
        __param(1, decorators_1.Inject("$log")),
        __param(2, decorators_1.Inject("constants")),
        __metadata("design:paramtypes", [Function, Object, Object])
    ], PollingService);
    return PollingService;
}());
exports.PollingService = PollingService;
exports.default = PollingService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9sbGluZy1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9sbGluZy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBR0EsNENBQXVDO0FBTXZDO0lBSUksd0JBRVksU0FBMkIsRUFFM0IsSUFBaUIsRUFFakIsU0FBcUI7UUFOakMsaUJBU0M7UUFQVyxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUUzQixTQUFJLEdBQUosSUFBSSxDQUFhO1FBRWpCLGNBQVMsR0FBVCxTQUFTLENBQVk7UUFUekIsY0FBUyxHQUFnQixFQUFFLENBQUM7UUFDcEIsWUFBTyxHQUFXLEtBQUssQ0FBQztRQWFqQyxhQUFRLEdBQUcsVUFBQyxRQUFtQixFQUFFLFlBQXFCO1lBQ3pELElBQUksUUFBUSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0MsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNaLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM5QixFQUFFLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUNmLFFBQVEsRUFBRSxDQUFDO2dCQUNmLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUssZUFBVSxHQUFHLFVBQUMsUUFBbUI7WUFDcEMsR0FBRyxDQUFDLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDO2dCQUN6RCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNsRCxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRU0saUJBQVksR0FBRyxVQUFDLFFBQW1CO1lBQ3ZDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUUsVUFBQyxFQUFFO2dCQUM3QixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFTSxpQkFBWSxHQUFHO1lBQ25CLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVMsRUFBRSxVQUFDLEVBQWE7Z0JBQ2pDLElBQUksQ0FBQztvQkFDRCxFQUFFLEVBQUUsQ0FBQztnQkFDVCxDQUFDO2dCQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ1YsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3hCLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVNLGdCQUFXLEdBQUc7WUFDbEIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxDQUFDO1FBQzVGLENBQUMsQ0FBQztRQXZDRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQWJRLGNBQWM7UUFLbEIsV0FBQSxtQkFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRW5CLFdBQUEsbUJBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUVkLFdBQUEsbUJBQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTs7T0FUZixjQUFjLENBb0QxQjtJQUFELHFCQUFDO0NBQUEsQUFwREQsSUFvREM7QUFwRFksd0NBQWM7QUFzRDNCLGtCQUFlLGNBQWMsQ0FBQyJ9

/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ResourceLoaderService = /** @class */ (function () {
    function ResourceLoaderService($templateRequest, $q, constants) {
        var _this = this;
        this.$templateRequest = $templateRequest;
        this.$q = $q;
        this.constants = constants;
        this.loadResource = function (resource) {
            try {
                if (resource.name) {
                    var contentApiEndpoint = _this.constants.environment.contentApiEndpoint;
                    var module_1 = resource.module, name_1 = resource.name;
                    var templateUrl = contentApiEndpoint + "/res/" + module_1 + "/" + name_1 + "/package";
                    return _this.$templateRequest(templateUrl, true)
                        .then(function (response) {
                        return angular.fromJson(response);
                    });
                }
                else {
                    throw new Error("Resource name not specified.");
                }
            }
            catch (ex) {
                return _this.$q.when(function () {
                    throw ex;
                });
            }
        };
    }
    ResourceLoaderService.$inject = ["$templateRequest", "$q", "constants"];
    return ResourceLoaderService;
}());
exports.ResourceLoaderService = ResourceLoaderService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2VMb2FkZXItc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJlc291cmNlTG9hZGVyLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFNQTtJQUlJLCtCQUFxQixnQkFBeUMsRUFDekMsRUFBYSxFQUNiLFNBQXFCO1FBRjFDLGlCQUdJO1FBSGlCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBeUI7UUFDekMsT0FBRSxHQUFGLEVBQUUsQ0FBVztRQUNiLGNBQVMsR0FBVCxTQUFTLENBQVk7UUFHbkMsaUJBQVksR0FBRyxVQUFDLFFBQW1CO1lBQ3RDLElBQUksQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixJQUFBLG1FQUFrQixDQUFnQztvQkFDbEQsSUFBQSwwQkFBTSxFQUFFLHNCQUFJLENBQWM7b0JBRWxDLElBQU0sV0FBVyxHQUFNLGtCQUFrQixhQUFRLFFBQU0sU0FBSSxNQUFJLGFBQVUsQ0FBQztvQkFFMUUsTUFBTSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDO3lCQUMxQyxJQUFJLENBQUMsVUFBQyxRQUFhO3dCQUNoQixNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDdEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixNQUFNLElBQUksS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUM7Z0JBQ3BELENBQUM7WUFDTCxDQUFDO1lBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDVixNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0JBQ2hCLE1BQU0sRUFBRSxDQUFDO2dCQUNiLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztRQUNMLENBQUMsQ0FBQTtJQXRCRSxDQUFDO0lBTFUsNkJBQU8sR0FBRyxDQUFDLGtCQUFrQixFQUFFLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztJQTRCcEUsNEJBQUM7Q0FBQSxBQTlCRCxJQThCQztBQTlCWSxzREFBcUIifQ==

/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var RevisionService = /** @class */ (function () {
    function RevisionService(constants) {
        this.constants = constants;
    }
    RevisionService.prototype.getRevision = function () {
        return this.constants && this.constants.environment && this.constants.environment.webRevision || "";
    };
    RevisionService.prototype.getPathWithRevision = function (path) {
        var rxHasParams = /[?]/;
        var revision = this.getRevision();
        return "" + path + (rxHasParams.test(path) ? "&" : "?") + revision;
    };
    RevisionService.$inject = ["constants"];
    return RevisionService;
}());
exports.RevisionService = RevisionService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmV2aXNpb24tc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJldmlzaW9uLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9DQUFvQzs7QUFJcEM7SUFJSSx5QkFDWSxTQUFxQjtRQUFyQixjQUFTLEdBQVQsU0FBUyxDQUFZO0lBQzlCLENBQUM7SUFFSSxxQ0FBVyxHQUFuQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7SUFDeEcsQ0FBQztJQUVNLDZDQUFtQixHQUExQixVQUEyQixJQUFZO1FBQ25DLElBQU0sV0FBVyxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDcEMsTUFBTSxDQUFDLEtBQUcsSUFBSSxJQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFHLFFBQVUsQ0FBQztJQUNyRSxDQUFDO0lBZGEsdUJBQU8sR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBZTFDLHNCQUFDO0NBQUEsQUFqQkQsSUFpQkM7QUFqQlksMENBQWUifQ==

/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var SearchHistory = /** @class */ (function (_super) {
    __extends(SearchHistory, _super);
    function SearchHistory() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SearchHistory;
}(Array));
exports.SearchHistory = SearchHistory;
/**
 * @ngdoc service
 * @name orion.services.swSearchHistoryService
 *
 * @description Service for storing and reading the global search history within orion
 *
 **/
var SearchHistoryService = /** @class */ (function () {
    function SearchHistoryService($log, webAdminService) {
        this.$log = $log;
        this.webAdminService = webAdminService;
        this.key = "swOrionSearchHistory";
        this.maxHistory = 100;
    }
    /**
     *  @ngdoc method
     *  @name addToHistory
     *  @methodOf orion.services.swSearchHistoryService
     *  @description Add item to the history, if the item is already part of history, then won't duplicate,
     *  but move to the first place. There is a maximum number of history items, what is set to 100.
     *  @param {string} value The item to be added to the history
     *  @returns {IPromise<boolean>} returns if the save was successfull
     **/
    SearchHistoryService.prototype.addToHistory = function (value) {
        var _this = this;
        return this.getHistory().then(function (history) {
            history = history.slice(0, _this.maxHistory);
            var positionIfExists = history.indexOf(value);
            if (positionIfExists !== -1) {
                history.splice(positionIfExists, 1);
            }
            history.unshift(value);
            return _this.webAdminService.saveUserSetting(_this.key, JSON.stringify(history.reverse()));
        });
    };
    /**
     *  @ngdoc method
     *  @name getHistory
     *  @methodOf orion.services.swSearchHistoryService
     *  @description Read the last specified items of the history
     *  @param {number=undefined} count Optional value, what sets that how many items will be returned, default is
     *  undefined what will return all the items.
     *  @returns {IPromise<SearchHistory>} returns history items
     **/
    SearchHistoryService.prototype.getHistory = function (count) {
        var _this = this;
        /*
        let history: SearchHistory = this.$cookies.getObject(this.key) || [];
        */
        return this.webAdminService.getUserSetting(this.key).then(function (history) {
            try {
                return JSON.parse(history).reverse().slice(0, count);
            }
            catch (error) {
                _this.$log.warn("The stored search history data is wrong.");
                return [];
            }
        });
    };
    /**
     *  @ngdoc method
     *  @name clearHistory
     *  @methodOf orion.services.swSearchHistoryService
     *  @description Resets the search history.
     *  @returns {IPromise<boolean>} returns if the remove was successfull
     **/
    SearchHistoryService.prototype.clearHistory = function () {
        return this.webAdminService.saveUserSetting(this.key, JSON.stringify(""));
    };
    SearchHistoryService.$inject = ["$log", "webAdminService"];
    return SearchHistoryService;
}());
exports.SearchHistoryService = SearchHistoryService;
exports.default = SearchHistoryService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoSGlzdG9yeS1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2VhcmNoSGlzdG9yeS1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQVVBO0lBQW1DLGlDQUFhO0lBQWhEOztJQUFrRCxDQUFDO0lBQUQsb0JBQUM7QUFBRCxDQUFDLEFBQW5ELENBQW1DLEtBQUssR0FBVztBQUF0QyxzQ0FBYTtBQUUxQjs7Ozs7O0lBTUk7QUFDSjtJQU1JLDhCQUFvQixJQUFpQixFQUFVLGVBQWlDO1FBQTVELFNBQUksR0FBSixJQUFJLENBQWE7UUFBVSxvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFIeEUsUUFBRyxHQUFXLHNCQUFzQixDQUFDO1FBQ3JDLGVBQVUsR0FBVyxHQUFHLENBQUM7SUFFa0QsQ0FBQztJQUVwRjs7Ozs7Ozs7UUFRSTtJQUNHLDJDQUFZLEdBQW5CLFVBQW9CLEtBQWE7UUFBakMsaUJBVUM7UUFURyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLE9BQXNCO1lBQ2pELE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDNUMsSUFBTSxnQkFBZ0IsR0FBVyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hELEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN4QyxDQUFDO1lBQ0QsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2QixNQUFNLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDN0YsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7Ozs7O1FBUUk7SUFDRyx5Q0FBVSxHQUFqQixVQUFrQixLQUFjO1FBQWhDLGlCQWFDO1FBWkc7O1VBRUU7UUFDRixNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE9BQWU7WUFFdEUsSUFBSSxDQUFDO2dCQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsMENBQTBDLENBQUMsQ0FBQztnQkFDM0QsTUFBTSxDQUFDLEVBQUUsQ0FBQztZQUNkLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7Ozs7O1FBTUk7SUFFRywyQ0FBWSxHQUFuQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBN0RhLDRCQUFPLEdBQUcsQ0FBQyxNQUFNLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztJQThEeEQsMkJBQUM7Q0FBQSxBQWhFRCxJQWdFQztBQWhFWSxvREFBb0I7QUFrRWpDLGtCQUFlLG9CQUFvQixDQUFDIn0=

/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SwDashboardViewService = /** @class */ (function () {
    /* @ngInject */
    SwDashboardViewService.$inject = ["$log", "swApi", "$state", "$location", "$q"];
    function SwDashboardViewService($log, swApi, $state, $location, $q) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.$state = $state;
        this.$location = $location;
        this.$q = $q;
        this.viewData = function (viewId, opid, netObjectId) {
            if (opid === void 0) { opid = ""; }
            if (netObjectId === void 0) { netObjectId = ""; }
            var queryParams = {
                swAlertOnError: false
            };
            if (opid) {
                queryParams.opid = opid;
            }
            if (netObjectId) {
                queryParams.netObject = netObjectId;
            }
            return _this.swApi.api(true).one("dashboardview", viewId)
                .withHttpConfig({ cache: true }).get(queryParams)
                .then(function (response) { return _this.camelCaseResponse(response.plain()); }, function (reason) {
                if ([404, 405, 406].indexOf(reason.status) > -1) {
                    var parameters = {
                        errorType: "",
                        errorCode: "",
                        message: reason.data.Message,
                        location: _this.$location.url()
                    };
                    _this.$state.go("Error", parameters);
                }
                _this.$log.error("Failure getting dashboard view data.");
                return _this.$q.reject();
            });
        };
        this.camelCaseResponse = function (response) {
            var formattedResponse = {};
            if (angular.isArray(response)) {
                formattedResponse = [];
            }
            angular.forEach(response, function (value, key) {
                if (angular.isArray(response)) {
                    formattedResponse.push(_this.camelCaseResponse(value));
                    return;
                }
                if (angular.isObject(value)) {
                    value = _this.camelCaseResponse(value);
                }
                formattedResponse[_.camelCase(key)] = value;
            });
            return formattedResponse;
        };
    }
    return SwDashboardViewService;
}());
exports.SwDashboardViewService = SwDashboardViewService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dEYXNoYm9hcmRWaWV3LXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzd0Rhc2hib2FyZFZpZXctc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQVNBO0lBQ0ksZUFBZTtJQUNmLGdDQUNZLElBQWlCLEVBQ2pCLEtBQW9CLEVBQ3BCLE1BQXdCLEVBQ3hCLFNBQTJCLEVBQzNCLEVBQWE7UUFMekIsaUJBTUk7UUFMUSxTQUFJLEdBQUosSUFBSSxDQUFhO1FBQ2pCLFVBQUssR0FBTCxLQUFLLENBQWU7UUFDcEIsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7UUFDeEIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0IsT0FBRSxHQUFGLEVBQUUsQ0FBVztRQUdsQixhQUFRLEdBQUcsVUFBQyxNQUFjLEVBQUUsSUFBaUIsRUFBRSxXQUF3QjtZQUEzQyxxQkFBQSxFQUFBLFNBQWlCO1lBQUUsNEJBQUEsRUFBQSxnQkFBd0I7WUFDMUUsSUFBSSxXQUFXLEdBQTZCO2dCQUN4QyxjQUFjLEVBQUUsS0FBSzthQUN4QixDQUFDO1lBRUYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDUCxXQUFXLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUM1QixDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDZCxXQUFXLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztZQUN4QyxDQUFDO1lBRUQsTUFBTSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsTUFBTSxDQUFDO2lCQUNuRCxjQUFjLENBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDO2lCQUM5QyxJQUFJLENBQ0QsVUFBQyxRQUFhLElBQUssT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQXhDLENBQXdDLEVBQzNELFVBQUMsTUFBVztnQkFDUixFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlDLElBQU0sVUFBVSxHQUEyQjt3QkFDdkMsU0FBUyxFQUFFLEVBQUU7d0JBQ2IsU0FBUyxFQUFFLEVBQUU7d0JBQ2IsT0FBTyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTzt3QkFDNUIsUUFBUSxFQUFFLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO3FCQUNqQyxDQUFDO29CQUVOLEtBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDcEMsQ0FBQztnQkFDRCxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2dCQUN4RCxNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM1QixDQUFDLENBQ0osQ0FBQztRQUNWLENBQUMsQ0FBQztRQUVNLHNCQUFpQixHQUFHLFVBQUMsUUFBYTtZQUN0QyxJQUFJLGlCQUFpQixHQUFRLEVBQUUsQ0FBQztZQUNoQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUIsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLENBQUM7WUFDRCxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxVQUFDLEtBQVUsRUFBRSxHQUFXO2dCQUM5QyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUIsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUN0RCxNQUFNLENBQUM7Z0JBQ1gsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUIsS0FBSyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUMsQ0FBQztnQkFDRCxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ2hELENBQUMsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxDQUFDLGlCQUFpQixDQUFDO1FBQzdCLENBQUMsQ0FBQztJQXJEQyxDQUFDO0lBdURSLDZCQUFDO0FBQUQsQ0FBQyxBQS9ERCxJQStEQztBQS9EWSx3REFBc0IifQ==

/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @ngdoc service
 * @name orion.services.swDemoService
 *
 * @description
 * Demo Service used by Orion Modules for displaying features in demo mode
 *
 **/
var SwDemoService = /** @class */ (function () {
    function SwDemoService($log, xuiToastService, swOrionDemoModeConstants, _t, constants) {
        var _this = this;
        this.$log = $log;
        this.xuiToastService = xuiToastService;
        this.swOrionDemoModeConstants = swOrionDemoModeConstants;
        this._t = _t;
        this.constants = constants;
        /**
         *  @ngdoc method
         *  @name isDemoMode
         *  @methodOf orion.services.swDemoService
         *  @description returns environment demoMode parameter value
        **/
        this.isDemoMode = function () {
            return _this.constants.environment.demoMode;
        };
        /**
         *  @ngdoc method
         *  @name showDemoErrorToast
         *  @methodOf orion.services.swDemoService
         *  @description Shows error toast message that feature is not available in demo mode
        **/
        this.showDemoErrorToast = function () {
            var title = _this.getFeatureNotAvailableMessage();
            var message = _this._t("To access all product features download our free 30 day evaluation software today.");
            var linkText = _this._t("Click here for details.");
            var linkUrl = "https://www.solarwinds.com/downloads/?CMPSource=DEMO_ORION";
            var anchor = "<a href=\"" + linkUrl + "\" target=\"_blank\">" + linkText + "</a>";
            _this.xuiToastService.warning(message + " " + anchor, title, { positionClass: "toast-top-center" });
        };
        /**
         *  @ngdoc method
         *  @name getFeatureNotAvailableMessage
         *  @methodOf orion.services.swDemoService
         *  @description Returns string with feature not available text
        **/
        this.getFeatureNotAvailableMessage = function () {
            return _this._t("THIS FEATURE IS NOT AVAILABLE IN THE ONLINE DEMO");
        };
        /**
         *  @ngdoc method
         *  @name isBlockedHrefInDemo
         *  @methodOf orion.services.swDemoService
         *  @description Checks if href is blocked in demo mode
         *  @param {string} href that should be checked for existence in blockedAnchors array
        **/
        this.isBlockedHrefInDemo = function (href) {
            var result = _.find(_this.swOrionDemoModeConstants.blockedAnchors, function (filterExp) {
                return new RegExp(filterExp, "i").test(href);
            }) !== undefined;
            return result;
        };
    }
    SwDemoService.$inject = ["$log", "xuiToastService", "swOrionDemoModeConstants", "getTextService", "constants"];
    return SwDemoService;
}());
exports.SwDemoService = SwDemoService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dEZW1vLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzd0RlbW8tc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUlBOzs7Ozs7O0lBT0k7QUFDSjtJQUdJLHVCQUNZLElBQWlCLEVBQ2pCLGVBQThCLEVBQzlCLHdCQUFpRCxFQUNqRCxFQUFvQyxFQUNwQyxTQUFxQjtRQUxqQyxpQkFNSTtRQUxRLFNBQUksR0FBSixJQUFJLENBQWE7UUFDakIsb0JBQWUsR0FBZixlQUFlLENBQWU7UUFDOUIsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUF5QjtRQUNqRCxPQUFFLEdBQUYsRUFBRSxDQUFrQztRQUNwQyxjQUFTLEdBQVQsU0FBUyxDQUFZO1FBR2pDOzs7OztXQUtHO1FBQ0ksZUFBVSxHQUFHO1lBQ2hCLE1BQU0sQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUM7UUFDL0MsQ0FBQyxDQUFDO1FBRUY7Ozs7O1dBS0c7UUFDSSx1QkFBa0IsR0FBRztZQUN4QixJQUFNLEtBQUssR0FBRyxLQUFJLENBQUMsNkJBQTZCLEVBQUUsQ0FBQztZQUNuRCxJQUFNLE9BQU8sR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLG9GQUFvRixDQUFDLENBQUM7WUFDOUcsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3BELElBQU0sT0FBTyxHQUFHLDREQUE0RCxDQUFDO1lBQzdFLElBQU0sTUFBTSxHQUFHLGVBQVksT0FBTyw2QkFBcUIsUUFBUSxTQUFNLENBQUM7WUFFdEUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUksT0FBTyxTQUFJLE1BQVEsRUFBRSxLQUFLLEVBQUUsRUFBQyxhQUFhLEVBQUUsa0JBQWtCLEVBQUMsQ0FBQyxDQUFDO1FBQ3JHLENBQUMsQ0FBQztRQUVGOzs7OztXQUtHO1FBQ0ksa0NBQTZCLEdBQUc7WUFDbkMsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsa0RBQWtELENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUM7UUFFRjs7Ozs7O1dBTUc7UUFDSSx3QkFBbUIsR0FBRyxVQUFDLElBQVk7WUFDdEMsSUFBTSxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsY0FBYyxFQUFFLFVBQUMsU0FBaUI7Z0JBQzlFLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pELENBQUMsQ0FBQyxLQUFLLFNBQVMsQ0FBQztZQUNyQixNQUFNLENBQUMsTUFBTSxDQUFDO1FBQ2xCLENBQUMsQ0FBQztJQWxEQyxDQUFDO0lBUlUscUJBQU8sR0FBRyxDQUFDLE1BQU0sRUFBRSxpQkFBaUIsRUFBRSwwQkFBMEIsRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLENBQUMsQ0FBQztJQTJEbkgsb0JBQUM7Q0FBQSxBQTVERCxJQTREQztBQTVEWSxzQ0FBYSJ9

/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var SwisDataMappingService = /** @class */ (function () {
    /* @ngInject */
    SwisDataMappingService.$inject = ["_t", "$log"];
    function SwisDataMappingService(_t, $log) {
        var _this = this;
        this._t = _t;
        this.$log = $log;
        this.baseStatuses = {
            raw: {
                "0": "unknown",
                "1": "up",
                "2": "down",
                "3": "warning",
                "4": "shutdown",
                "5": "testing",
                "6": "unknown",
                // "7": "", // notPresent
                // "8": "", // lowerLayerDown
                "9": "unmanaged",
                "10": "unplugged",
                "11": "external",
                "12": "unreachable",
                "14": "critical",
                "15": "warning",
                "16": "unknown",
                "17": "undefined",
                "19": "unknown",
                "22": "up",
                "24": "inactive",
                "25": "unknown",
                "26": "unknown",
                "27": "disabled",
                "28": "unknown",
                // "29": "", // otherCategory
                "30": "notrunning" // notRunning
            },
            translated: {
                "0": this._t("unknown"),
                "1": this._t("up"),
                "2": this._t("down"),
                "3": this._t("warning"),
                "4": this._t("shutdown"),
                "5": this._t("testing"),
                "6": this._t("unknown"),
                // "7": "", // notPresent
                // "8": "", // lowerLayerDown
                "9": this._t("unmanaged"),
                "10": this._t("unplugged"),
                "11": this._t("external"),
                "12": this._t("unreachable"),
                "14": this._t("critical"),
                "15": this._t("warning"),
                "16": this._t("unknown"),
                "17": this._t("undefined"),
                "19": this._t("unknown"),
                "22": this._t("up"),
                "24": this._t("inactive"),
                "25": this._t("unknown"),
                "26": this._t("unknown"),
                "27": this._t("disabled"),
                "28": this._t("unknown"),
                // "29": "", // otherCategory
                "30": this._t("notrunning") // notRunning
            }
        };
        this.defaultStatus = "";
        this.statuses = {};
        this.statusesTranslated = {};
        this.baseEntities = {
            "orion.apm.application": "application",
            "orion.apm.genericapplication": "application",
            "orion.apm.exchange.application": "application",
            "orion.apm.exchange.database": "database",
            "orion.apm.exchange.databasecopy": "database",
            "orion.apm.exchange.databasefile": "application-component",
            "orion.apm.exchange.mailbox": "application-component",
            "orion.apm.iis.application": "application",
            "orion.apm.iis.applicationpool": "pool",
            "orion.apm.iis.site": "website",
            "orion.apm.sqlserverapplication": "application",
            "orion.apm.component": "application-component",
            "orion.apm.sqldatabase": "database",
            "orion.cloud.aws.instances": "cloud-instance",
            "orion.cloud.aws.volumes": "volume",
            "orion.cloud.azure.instances": "cloud-instance",
            "orion.cloud.azure.volumes": "volume",
            "orion.dpi.applications": "application",
            "orion.groups": "group",
            //"orion.hardwarehealth.hardwarecategorystatus": "",
            //"orion.hardwarehealth.hardwareinfo": "",
            "orion.hardwarehealth.hardwareitem": "hardware-sensor",
            "orion.netpath.endpointservices": "path-endpoint",
            "orion.npm.interfaces": "network-interface",
            //"orion.npm.custompollerassignmentoninterface": "",
            //"orion.npm.custompollerassignmentonnode": "",
            "orion.nodes": "unknownnode",
            "orion.vim.clusters": "virtual-cluster",
            "orion.vim.datacenters": "virtual-datacenter",
            "orion.vim.hosts": "virtual-host",
            "orion.vim.vcenters": "virtual-center",
            "orion.vim.virtualmachines": "virtualmachine",
            "orion.volumes": "volume",
            "cortex.orion.cman.container": "container",
            "orion.apipoller.apipoller": "api",
            "orion.apipoller.valuetomonitor": "api-value"
        };
        this.defaultEntityIcon = "unknownnode";
        this.entitiesIcons = {};
        this.baseChildStatuses = {
            raw: {
                "0": "unknown",
                "2": "down",
                "3": "warning",
                "6": "unknown",
                "15": "warning",
                "16": "unknown",
                "19": "unknown",
                "25": "unknown",
                "26": "unknown",
                "28": "unknown",
            },
            translated: {
                "0": this._t("unknown"),
                "2": this._t("down"),
                "3": this._t("warning"),
                "6": this._t("unknown"),
                "15": this._t("warning"),
                "16": this._t("unknown"),
                "19": this._t("unknown"),
                "25": this._t("unknown"),
                "26": this._t("unknown"),
                "28": this._t("unknown"),
            }
        };
        this.defaultChildStatus = "";
        this.childStatuses = {};
        this.childStatusesTranslated = {};
        this.getOrDefault = function (map, key, defValue) {
            return key && map.hasOwnProperty(key)
                ? map[key]
                : defValue;
        };
        this.makeNumber = function (value) {
            var number = Number(value);
            return isNaN(number) ? "" : number.toString();
        };
        this.makeLower = function (value) {
            return (value == null) ? "" : value.toLowerCase();
        };
        this.extendStatusMapping = function (mapping) {
            return _this.extend(_this.statuses, mapping, "status", _this.makeNumber);
        };
        this.extendChildStatusMapping = function (mapping) {
            return _this.extend(_this.childStatuses, mapping, "childStatus", _this.makeNumber);
        };
        this.extendStatusMappingTranslated = function (mapping) {
            return _this.extend(_this.statusesTranslated, mapping, "status", _this.makeNumber);
        };
        this.extendChildStatusMappingTranslated = function (mapping) {
            return _this.extend(_this.childStatusesTranslated, mapping, "childStatus", _this.makeNumber);
        };
        this.extendEntityIconMapping = function (mapping) {
            return _this.extend(_this.entitiesIcons, mapping, "entity", _this.makeLower);
        };
        this.getStatus = function (statusId) {
            return _this.getOrDefault(_this.statuses, String(statusId), _this.defaultStatus);
        };
        this.getChildStatus = function (childStatusId) {
            return _this.getOrDefault(_this.childStatuses, String(childStatusId), _this.defaultChildStatus);
        };
        this.getStatusTranslated = function (statusId) {
            return _this.getOrDefault(_this.statusesTranslated, String(statusId), _this.defaultStatus);
        };
        this.getChildStatusTranslated = function (childStatusId) {
            return _this.getOrDefault(_this.childStatusesTranslated, String(childStatusId), _this.defaultChildStatus);
        };
        this.getEntityIcon = function (entityType) {
            return _this.getOrDefault(_this.entitiesIcons, entityType && entityType.toLowerCase(), _this.defaultEntityIcon);
        };
        this.extendStatusMapping(this.baseStatuses.raw);
        this.extendChildStatusMapping(this.baseChildStatuses.raw);
        this.extendStatusMappingTranslated(this.baseStatuses.translated);
        this.extendChildStatusMappingTranslated(this.baseChildStatuses.translated);
        this.extendEntityIconMapping(this.baseEntities);
    }
    SwisDataMappingService.prototype.extend = function (targetMap, sourceMap, name, mapKey) {
        var _this = this;
        _.each(Object.keys(sourceMap), function (key) {
            var mappedKey = mapKey ? mapKey(key) : key;
            if (targetMap.hasOwnProperty(mappedKey)) {
                _this.$log.warn("Mapping for " + name + " '" + mappedKey + "' already exists: " + targetMap[mappedKey]);
                return;
            }
            targetMap[mappedKey] = sourceMap[key];
        });
    };
    SwisDataMappingService = __decorate([
        __param(0, decorators_1.Inject("getTextService")),
        __metadata("design:paramtypes", [Function, Object])
    ], SwisDataMappingService);
    return SwisDataMappingService;
}());
exports.SwisDataMappingService = SwisDataMappingService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dpc0RhdGFNYXBwaW5nLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzd2lzRGF0YU1hcHBpbmctc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLDRDQUF1QztBQWdCdkM7SUFtS0ksZUFBZTtJQUNmLGdDQUVZLEVBQW9DLEVBQ3BDLElBQW9CO1FBSGhDLGlCQVdDO1FBVFcsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDcEMsU0FBSSxHQUFKLElBQUksQ0FBZ0I7UUFyS3hCLGlCQUFZLEdBQTBDO1lBQzFELEdBQUcsRUFBQztnQkFDQSxHQUFHLEVBQUcsU0FBUztnQkFDZixHQUFHLEVBQUcsSUFBSTtnQkFDVixHQUFHLEVBQUcsTUFBTTtnQkFDWixHQUFHLEVBQUcsU0FBUztnQkFDZixHQUFHLEVBQUcsVUFBVTtnQkFDaEIsR0FBRyxFQUFHLFNBQVM7Z0JBQ2YsR0FBRyxFQUFHLFNBQVM7Z0JBQ2YseUJBQXlCO2dCQUN6Qiw2QkFBNkI7Z0JBQzdCLEdBQUcsRUFBRyxXQUFXO2dCQUNqQixJQUFJLEVBQUUsV0FBVztnQkFDakIsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLElBQUksRUFBRSxhQUFhO2dCQUNuQixJQUFJLEVBQUUsVUFBVTtnQkFDaEIsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxVQUFVO2dCQUNoQixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsVUFBVTtnQkFDaEIsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsNkJBQTZCO2dCQUM3QixJQUFJLEVBQUUsWUFBWSxDQUFDLGFBQWE7YUFDbkM7WUFDRCxVQUFVLEVBQUU7Z0JBQ1osR0FBRyxFQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUN4QixHQUFHLEVBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLEdBQUcsRUFBRyxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztnQkFDckIsR0FBRyxFQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUN4QixHQUFHLEVBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0JBQ3pCLEdBQUcsRUFBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsR0FBRyxFQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUN4Qix5QkFBeUI7Z0JBQ3pCLDZCQUE2QjtnQkFDN0IsR0FBRyxFQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO2dCQUMxQixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7Z0JBQzFCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQztnQkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDO2dCQUM1QixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0JBQ3pCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUN4QixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7Z0JBQzFCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dCQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0JBQ3pCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUN4QixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0JBQ3pCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsNkJBQTZCO2dCQUM3QixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxhQUFhO2FBQ3hDO1NBQ0osQ0FBQztRQUNNLGtCQUFhLEdBQVcsRUFBRSxDQUFDO1FBQzNCLGFBQVEsR0FBYSxFQUFFLENBQUM7UUFDeEIsdUJBQWtCLEdBQWEsRUFBRSxDQUFDO1FBRWxDLGlCQUFZLEdBQWE7WUFDN0IsdUJBQXVCLEVBQUUsYUFBYTtZQUN0Qyw4QkFBOEIsRUFBRSxhQUFhO1lBQzdDLGdDQUFnQyxFQUFFLGFBQWE7WUFDL0MsNkJBQTZCLEVBQUUsVUFBVTtZQUN6QyxpQ0FBaUMsRUFBRSxVQUFVO1lBQzdDLGlDQUFpQyxFQUFFLHVCQUF1QjtZQUMxRCw0QkFBNEIsRUFBRSx1QkFBdUI7WUFDckQsMkJBQTJCLEVBQUUsYUFBYTtZQUMxQywrQkFBK0IsRUFBRSxNQUFNO1lBQ3ZDLG9CQUFvQixFQUFFLFNBQVM7WUFDL0IsZ0NBQWdDLEVBQUUsYUFBYTtZQUMvQyxxQkFBcUIsRUFBRSx1QkFBdUI7WUFDOUMsdUJBQXVCLEVBQUUsVUFBVTtZQUNuQywyQkFBMkIsRUFBRSxnQkFBZ0I7WUFDN0MseUJBQXlCLEVBQUUsUUFBUTtZQUNuQyw2QkFBNkIsRUFBRSxnQkFBZ0I7WUFDL0MsMkJBQTJCLEVBQUUsUUFBUTtZQUNyQyx3QkFBd0IsRUFBRSxhQUFhO1lBQ3ZDLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLG9EQUFvRDtZQUNwRCwwQ0FBMEM7WUFDMUMsbUNBQW1DLEVBQUUsaUJBQWlCO1lBQ3RELGdDQUFnQyxFQUFFLGVBQWU7WUFDakQsc0JBQXNCLEVBQUUsbUJBQW1CO1lBQzNDLG9EQUFvRDtZQUNwRCwrQ0FBK0M7WUFDL0MsYUFBYSxFQUFFLGFBQWE7WUFDNUIsb0JBQW9CLEVBQUUsaUJBQWlCO1lBQ3ZDLHVCQUF1QixFQUFFLG9CQUFvQjtZQUM3QyxpQkFBaUIsRUFBRSxjQUFjO1lBQ2pDLG9CQUFvQixFQUFFLGdCQUFnQjtZQUN0QywyQkFBMkIsRUFBRSxnQkFBZ0I7WUFDN0MsZUFBZSxFQUFFLFFBQVE7WUFDekIsNkJBQTZCLEVBQUUsV0FBVztZQUMxQywyQkFBMkIsRUFBRSxLQUFLO1lBQ2xDLGdDQUFnQyxFQUFFLFdBQVc7U0FDaEQsQ0FBQztRQUNNLHNCQUFpQixHQUFXLGFBQWEsQ0FBQztRQUMxQyxrQkFBYSxHQUFhLEVBQUUsQ0FBQztRQUU3QixzQkFBaUIsR0FBMEM7WUFDL0QsR0FBRyxFQUFDO2dCQUNBLEdBQUcsRUFBRyxTQUFTO2dCQUNmLEdBQUcsRUFBRyxNQUFNO2dCQUNaLEdBQUcsRUFBRyxTQUFTO2dCQUNmLEdBQUcsRUFBRyxTQUFTO2dCQUNmLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxTQUFTO2dCQUNmLElBQUksRUFBRSxTQUFTO2FBQ2xCO1lBQ0QsVUFBVSxFQUFDO2dCQUNQLEdBQUcsRUFBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsR0FBRyxFQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO2dCQUNyQixHQUFHLEVBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hCLEdBQUcsRUFBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUN4QixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUN4QixJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hCLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQzthQUMzQjtTQUNKLENBQUM7UUFDTSx1QkFBa0IsR0FBVyxFQUFFLENBQUM7UUFDaEMsa0JBQWEsR0FBYSxFQUFFLENBQUM7UUFDN0IsNEJBQXVCLEdBQWEsRUFBRSxDQUFDO1FBRXZDLGlCQUFZLEdBQUcsVUFDbkIsR0FBYSxFQUNiLEdBQVcsRUFDWCxRQUFnQjtZQUVoQixPQUFBLEdBQUcsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7Z0JBQ1YsQ0FBQyxDQUFDLFFBQVE7UUFGZCxDQUVjLENBQUM7UUFtQ1gsZUFBVSxHQUFHLFVBQUMsS0FBYTtZQUMvQixJQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEQsQ0FBQyxDQUFDO1FBRU0sY0FBUyxHQUFHLFVBQUMsS0FBYTtZQUM5QixNQUFNLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RELENBQUMsQ0FBQztRQUVLLHdCQUFtQixHQUFHLFVBQUMsT0FBaUI7WUFDM0MsT0FBQSxLQUFJLENBQUMsTUFBTSxDQUNQLEtBQUksQ0FBQyxRQUFRLEVBQ2IsT0FBTyxFQUNQLFFBQVEsRUFDUixLQUFJLENBQUMsVUFBVSxDQUNsQjtRQUxELENBS0MsQ0FBQztRQUVDLDZCQUF3QixHQUFHLFVBQUMsT0FBaUI7WUFDaEQsT0FBQSxLQUFJLENBQUMsTUFBTSxDQUNQLEtBQUksQ0FBQyxhQUFhLEVBQ2xCLE9BQU8sRUFDUCxhQUFhLEVBQ2IsS0FBSSxDQUFDLFVBQVUsQ0FDbEI7UUFMRCxDQUtDLENBQUM7UUFFSyxrQ0FBNkIsR0FBRyxVQUFDLE9BQWlCO1lBQ3pELE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FDUCxLQUFJLENBQUMsa0JBQWtCLEVBQ3ZCLE9BQU8sRUFDUCxRQUFRLEVBQ1IsS0FBSSxDQUFDLFVBQVUsQ0FDbEI7UUFMRCxDQUtDLENBQUM7UUFFQyx1Q0FBa0MsR0FBRyxVQUFDLE9BQWlCO1lBQzFELE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FDUCxLQUFJLENBQUMsdUJBQXVCLEVBQzVCLE9BQU8sRUFDUCxhQUFhLEVBQ2IsS0FBSSxDQUFDLFVBQVUsQ0FDbEI7UUFMRCxDQUtDLENBQUM7UUFFQyw0QkFBdUIsR0FBRyxVQUFDLE9BQWlCO1lBQy9DLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FDUCxLQUFJLENBQUMsYUFBYSxFQUNsQixPQUFPLEVBQ1AsUUFBUSxFQUNSLEtBQUksQ0FBQyxTQUFTLENBQ2pCO1FBTEQsQ0FLQyxDQUFDO1FBRUMsY0FBUyxHQUFHLFVBQUMsUUFBZ0I7WUFDaEMsT0FBQSxLQUFJLENBQUMsWUFBWSxDQUNiLEtBQUksQ0FBQyxRQUFRLEVBQ2IsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUNoQixLQUFJLENBQUMsYUFBYSxDQUNyQjtRQUpELENBSUMsQ0FBQztRQUVDLG1CQUFjLEdBQUcsVUFBQyxhQUFxQjtZQUMxQyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQ2IsS0FBSSxDQUFDLGFBQWEsRUFDbEIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxFQUNyQixLQUFJLENBQUMsa0JBQWtCLENBQzFCO1FBSkQsQ0FJQyxDQUFDO1FBRUssd0JBQW1CLEdBQUcsVUFBQyxRQUFnQjtZQUM5QyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQ2IsS0FBSSxDQUFDLGtCQUFrQixFQUN2QixNQUFNLENBQUMsUUFBUSxDQUFDLEVBQ2hCLEtBQUksQ0FBQyxhQUFhLENBQ3JCO1FBSkQsQ0FJQyxDQUFDO1FBRUMsNkJBQXdCLEdBQUcsVUFBQyxhQUFxQjtZQUNwRCxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQ2IsS0FBSSxDQUFDLHVCQUF1QixFQUM1QixNQUFNLENBQUMsYUFBYSxDQUFDLEVBQ3JCLEtBQUksQ0FBQyxrQkFBa0IsQ0FDMUI7UUFKRCxDQUlDLENBQUM7UUFFQyxrQkFBYSxHQUFHLFVBQUMsVUFBa0I7WUFDdEMsT0FBQSxLQUFJLENBQUMsWUFBWSxDQUNiLEtBQUksQ0FBQyxhQUFhLEVBQ2xCLFVBQVUsSUFBSSxVQUFVLENBQUMsV0FBVyxFQUFFLEVBQ3RDLEtBQUksQ0FBQyxpQkFBaUIsQ0FDekI7UUFKRCxDQUlDLENBQUM7UUEzRkYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUUxRCxJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsa0NBQWtDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRTNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQS9CTyx1Q0FBTSxHQUFkLFVBQ0ksU0FBbUIsRUFDbkIsU0FBbUIsRUFDbkIsSUFBWSxFQUNaLE1BQStCO1FBSm5DLGlCQWlCQztRQVhHLENBQUMsQ0FBQyxJQUFJLENBQ0YsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFDdEIsVUFBQyxHQUFXO1lBQ1IsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztZQUM3QyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEMsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWUsSUFBSSxVQUFLLFNBQVMsMEJBQXFCLFNBQVMsQ0FBQyxTQUFTLENBQUcsQ0FBQyxDQUFDO2dCQUM3RixNQUFNLENBQUM7WUFDWCxDQUFDO1lBQ0QsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQ0osQ0FBQztJQUNOLENBQUM7SUFqS1Esc0JBQXNCO1FBcUsxQixXQUFBLG1CQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTs7T0FyS3BCLHNCQUFzQixDQW9RbEM7SUFBRCw2QkFBQztDQUFBLEFBcFFELElBb1FDO0FBcFFZLHdEQUFzQiJ9

/***/ }),
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var swApi_service_1 = __webpack_require__(3);
/**
 * @ngdoc service
 * @name orion.services.swisService
 *
 * @description SWIS interface service
 *
 **/
var SwisService = /** @class */ (function () {
    function SwisService(swApi) {
        this.swApi = swApi;
    }
    Object.defineProperty(SwisService.prototype, "swisApi", {
        get: function () {
            return this.swApi.api(false).one("swis");
        },
        enumerable: true,
        configurable: true
    });
    SwisService.prototype.query = function (swql, parameters, options) {
        var method = parameters ? "QueryWithParameters" : "Query";
        var payload = parameters ? { query: swql, parameters: parameters } : { query: swql };
        return this.swApi.ws.one("Information.asmx")
            .post(method, payload, undefined, this.getQueryHeaders(options))
            .then(function (result) {
            var columns = result.d.Columns;
            var entities = _.map(result.d.Rows, function (row) { return _.reduce(columns, function (entity, item, index) {
                entity[item] = row[index];
                return entity;
            }, {}); });
            return {
                rows: entities,
                total: result.d.TotalRows,
            };
        });
    };
    /**
     *  @ngdoc method
     *  @name runQuery
     *  @methodOf orion.services.swisService
     *  @description Execute SWIS query and returns the data result.
     *  @param {string} swql SWIS query to execute
     *  @param {ISwisQueryParameters=} parameters SWIS query parameters
     *  @param {ISwisQueryOptions=} options SWIS query options
     *  @param {IUriQueryParams=} uriQueryParams Additional URI query parameters to add to the resulting http request
     *  @returns {IPromise<ISwisQueryResult<T>>} returns result of the query
     **/
    SwisService.prototype.runQuery = function (swql, parameters, options, uriQueryParams, httpConfig) {
        var elementToPost = { query: swql, parameters: parameters };
        return this.swisApi
            .withHttpConfig(httpConfig)
            .post("query", elementToPost, uriQueryParams, this.getQueryHeaders(options))
            .then(function (result) { return ({
            rows: result.Result,
            total: result.TotalRows
        }); });
    };
    SwisService.prototype.read = function (uri, options) {
        return this.swisApi
            .customGET("read", { uri: uri }, this.getQueryHeaders(options))
            .then(function (result) { return result.entity; });
    };
    SwisService.prototype.invoke = function (entity, verb, parameters, options) {
        return this.swisApi
            .post("invoke", { entity: entity, verb: verb, parameters: parameters }, undefined, this.getQueryHeaders(options));
    };
    SwisService.prototype.getQueryHeaders = function (options) {
        if (!options || !_.isObject(options)) {
            return undefined;
        }
        return {
            "X-Orion-SwisQueryId": options.queryId
        };
    };
    SwisService = __decorate([
        __param(0, decorators_1.Inject("swApi")),
        __metadata("design:paramtypes", [swApi_service_1.default])
    ], SwisService);
    return SwisService;
}());
exports.SwisService = SwisService;
exports.default = SwisService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dpcy1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3dpcy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBSUEsNENBQXVDO0FBQ3ZDLGlEQUEyQztBQUUzQzs7Ozs7O0lBTUk7QUFDSjtJQUVJLHFCQUVZLEtBQW1CO1FBQW5CLFVBQUssR0FBTCxLQUFLLENBQWM7SUFDNUIsQ0FBQztJQUVKLHNCQUFZLGdDQUFPO2FBQW5CO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QyxDQUFDOzs7T0FBQTtJQUVNLDJCQUFLLEdBQVosVUFDSSxJQUFZLEVBQ1osVUFBaUMsRUFDakMsT0FBMkI7UUFFM0IsSUFBTSxNQUFNLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQzVELElBQU0sT0FBTyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUM7UUFFdkYsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzthQUN2QyxJQUFJLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUMvRCxJQUFJLENBQUMsVUFBQyxNQUFNO1lBQ1QsSUFBTSxPQUFPLEdBQWEsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFFM0MsSUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FDbEIsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQ2IsVUFBQyxHQUFVLElBQUssT0FBQSxDQUFDLENBQUMsTUFBTSxDQUNwQixPQUFPLEVBQ1AsVUFBQyxNQUFXLEVBQUUsSUFBSSxFQUFFLEtBQWE7Z0JBQzdCLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLE1BQU0sQ0FBQyxNQUFXLENBQUM7WUFDdkIsQ0FBQyxFQUNELEVBQUUsQ0FDTCxFQVBlLENBT2YsQ0FDSixDQUFDO1lBRUYsTUFBTSxDQUFDO2dCQUNILElBQUksRUFBRSxRQUFlO2dCQUNyQixLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxTQUFTO2FBQzVCLENBQUM7UUFDTixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRDs7Ozs7Ozs7OztRQVVJO0lBQ0csOEJBQVEsR0FBZixVQUNJLElBQVksRUFDWixVQUFpQyxFQUNqQyxPQUEyQixFQUMzQixjQUFnQyxFQUNoQyxVQUFtQztRQUVuQyxJQUFNLGFBQWEsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxDQUFDO1FBQzlELE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTzthQUNkLGNBQWMsQ0FBQyxVQUFVLENBQUM7YUFDMUIsSUFBSSxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDM0UsSUFBSSxDQUFDLFVBQUMsTUFBTSxJQUFLLE9BQUEsQ0FBQztZQUNmLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTTtZQUNuQixLQUFLLEVBQUUsTUFBTSxDQUFDLFNBQVM7U0FDMUIsQ0FBQyxFQUhnQixDQUdoQixDQUFDLENBQUM7SUFDWixDQUFDO0lBRU0sMEJBQUksR0FBWCxVQUFlLEdBQVcsRUFBRSxPQUEyQjtRQUNuRCxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU87YUFDZCxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDOUQsSUFBSSxDQUFDLFVBQUMsTUFBTSxJQUFLLE9BQUEsTUFBTSxDQUFDLE1BQU0sRUFBYixDQUFhLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRU0sNEJBQU0sR0FBYixVQUNJLE1BQWMsRUFDZCxJQUFZLEVBQ1osVUFBa0IsRUFDbEIsT0FBMkI7UUFFM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPO2FBQ2QsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLE1BQU0sUUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLFVBQVUsWUFBQSxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRU8scUNBQWUsR0FBdkIsVUFBd0IsT0FBMEI7UUFDOUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ3JCLENBQUM7UUFFRCxNQUFNLENBQUM7WUFDSCxxQkFBcUIsRUFBRSxPQUFPLENBQUMsT0FBTztTQUN6QyxDQUFDO0lBQ04sQ0FBQztJQS9GUSxXQUFXO1FBR2YsV0FBQSxtQkFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO3lDQUNELHVCQUFZO09BSnRCLFdBQVcsQ0FnR3ZCO0lBQUQsa0JBQUM7Q0FBQSxBQWhHRCxJQWdHQztBQWhHWSxrQ0FBVztBQWtHeEIsa0JBQWUsV0FBVyxDQUFDIn0=

/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var TileStackService = /** @class */ (function () {
    function TileStackService($q, swUtil, swApi) {
        var _this = this;
        this.$q = $q;
        this.swUtil = swUtil;
        this.swApi = swApi;
        this.events = {};
        this.version = "1.0";
        this.systemColumns = [
            { Name: "Description", RefId: "System.ManagedEntity|Description", DefaultClause: null },
            { Name: "DisplayName", RefId: "System.ManagedEntity|DisplayName", DefaultClause: null },
            { Name: "DetailsUrl", RefId: "System.ManagedEntity|DetailsUrl", DefaultClause: null },
            { Name: "StatusIcon", RefId: "System.ManagedEntity|StatusIcon", DefaultClause: null },
            { Name: "Status2", RefId: "System.ManagedEntity|Status", DefaultClause: null, Cooked: true },
            { Name: "EntityType", RefId: "System.ManagedEntity|${rootentity}", DefaultClause: null },
            { Name: "PrimaryKey", RefId: "System.ManagedEntity|${primarykey}", DefaultClause: null }
        ];
        this.getCategories = function () {
            return _this.swUtil.cachePromise(function () {
                return _this.swApi.ws.one("Gen2Integration.asmx")
                    .post("GetCategories")
                    .then(function (result) {
                    return result.d;
                });
            }, null, null);
        };
        this.getGroups = function () {
            return _this.swUtil.cachePromise(function () {
                var request = {
                    request: {
                        DataTypeFilter: [],
                        IsStatisticFilter: null,
                        ApplicationTypeFilter: null,
                        DataSource: {
                            RefId: "74134204-b2e4-4fa8-9abe-4a8999876b6e",
                            Name: "All Nodes",
                            Type: 4,
                            CommandText: null,
                            MasterEntity: "Orion.Nodes",
                            Filter: null,
                            EntityUri: null,
                            DynamicSelectionType: 1
                        },
                        ExtraRequestParams: null
                    }
                };
                return _this.swApi.ws.one("Gen2Integration.asmx")
                    .post("GetGroups", request)
                    .then(function (result) {
                    return _this.swApi.transformDataTable(result.d.DataTable, result.d.TotalRows);
                });
            }, null, null);
        };
        this.getGroupProperties = function (group) {
            if (group.Properties) {
                return _this.$q.when(group.Properties);
            }
            var groupId = group.GroupID;
            return _this.swUtil.cachePromise(function () {
                var request = {
                    request: {
                        DataTypeFilter: [],
                        IsStatisticFilter: null,
                        ApplicationTypeFilter: null,
                        DataSource: {
                            RefId: "74134204-b2e4-4fa8-9abe-4a8999876b6e",
                            Name: "All Nodes",
                            Type: 4,
                            CommandText: null,
                            MasterEntity: groupId,
                            Filter: null,
                            EntityUri: null,
                            DynamicSelectionType: 1
                        },
                        ExtraRequestParams: null,
                        SearchTerm: "",
                        GroupID: groupId,
                        CategoryID: groupId,
                        FieldType: "",
                        FavoriteID: "classic",
                        Uri: ""
                    }
                };
                return _this.swApi.ws.one("Gen2Integration.asmx")
                    .post("GetColumns", request)
                    .then(function (result) {
                    var rows = _this.swApi.transformDataTable(result.d.DataTable, result.d.TotalRows).rows;
                    _.each(rows, function (row) {
                        row.Field = angular.fromJson(row.Field);
                    });
                    group.Properties = rows;
                    return group.Properties;
                });
            }, "getGroupProperties-" + groupId, null);
        };
        this.getEntities = function (groups, columns, sortBy, sortAscending, filterBy, rowStart, rowEnd) {
            if (columns === void 0) { columns = []; }
            // Make a copy of the filters so we can remove the MetaData. At this point it is not
            // necessary for our request and the server doesn't seem to like it.
            var filter = _.map(filterBy, function (f) {
                var copy = {};
                angular.copy(f, copy);
                copy.MetaData = null;
                return copy;
            });
            _.each(_this.systemColumns, function (sc) {
                columns.push(sc);
            });
            // Add the sort column if it's not already included.
            if (sortBy) {
                if (!_.some(columns, { Name: sortBy.Field.Name })) {
                    columns.push({ Name: sortBy.Field.Name, RefId: sortBy.ItemID, DefaultClause: null });
                }
            }
            // Add our ORDER BY clause to the SWQL command if defined.
            var postSelect = sortBy ?
                "ORDER BY X.{0} {1} " + sortBy.Field.Name + " " + (sortAscending ? "ASC" : "DESC") : null;
            var request = {
                content: angular.toJson({
                    Categories: groups && groups.length ? _.map(groups, "CategoryId") : null,
                    Columns: columns,
                    RowStart: rowStart,
                    RowLimit: rowEnd,
                    Filter: filter,
                    PostSelectClauseSwql: postSelect
                })
            };
            return _this.swApi.ws.one("Gen2Integration.asmx")
                .post("LoadListView", request)
                .then(function (result) {
                return _this.swApi.transformDataTable(result.d.Rows, result.d.Rows.TotalRows);
            });
        };
        this.getEntitySummaries = function (categories, facet, filterBy) {
            var request = {
                content: angular.toJson({
                    Categories: categories,
                    Filter: filterBy,
                    FacetRefId: facet
                })
            };
            return _this.swApi.ws.one("Gen2Integration.asmx")
                .post("LoadTileView", request)
                .then(function (result) {
                return result.d.FacetCounts;
            });
        };
        this.getDefaultFilterProperties = function () {
            return _this.swUtil.cachePromise(function () {
                return _this.swApi.ws.one("Gen2Integration.asmx")
                    .post("GetDefaultFilterProperties")
                    .then(function (response) {
                    return response.d;
                });
            }, null, null);
        };
        this.getFilterValues = function (categories, filters) {
            var request = {
                content: angular.toJson({
                    categories: categories && categories.length ? _.map(categories, "CategoryId") : null,
                    properties: _.map(filters, function (filter) {
                        return {
                            FieldRefId: filter.ItemID,
                            DisplayName: filter.Field.DisplayName,
                            ApplicationType: filter.Field.DataTypeInfo.ApplicationType
                        };
                    })
                })
            };
            return _this.swApi.ws.one("Gen2Integration.asmx")
                .post("LoadPropertyCounts", request)
                .then(function (result) {
                _.each(result.d, function (f) {
                    _.each(f.Values, function (v) {
                        v.LValue = v.LocalizedValue;
                    });
                });
                return result.d;
            });
        };
    }
    TileStackService.$inject = ["$q", "swUtil", "swApi"];
    return TileStackService;
}());
exports.TileStackService = TileStackService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGlsZVN0YWNrLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0aWxlU3RhY2stc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0NBQW9DOztBQVlwQztJQWtCSSwwQkFBb0IsRUFBYSxFQUFVLE1BQWdCLEVBQVUsS0FBb0I7UUFBekYsaUJBQ0M7UUFEbUIsT0FBRSxHQUFGLEVBQUUsQ0FBVztRQUFVLFdBQU0sR0FBTixNQUFNLENBQVU7UUFBVSxVQUFLLEdBQUwsS0FBSyxDQUFlO1FBZGpGLFdBQU0sR0FBUSxFQUFFLENBQUM7UUFFakIsWUFBTyxHQUFXLEtBQUssQ0FBQztRQUV4QixrQkFBYSxHQUFxQjtZQUN0QyxFQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLGtDQUFrQyxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUM7WUFDckYsRUFBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxrQ0FBa0MsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFDO1lBQ3JGLEVBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsaUNBQWlDLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBQztZQUNuRixFQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLGlDQUFpQyxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUM7WUFDbkYsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSw2QkFBNkIsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7WUFDMUYsRUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxvQ0FBb0MsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFDO1lBQ3RGLEVBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsb0NBQW9DLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBQztTQUN6RixDQUFDO1FBS0ssa0JBQWEsR0FBRztZQUNuQixNQUFNLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7Z0JBQzVCLE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUM7cUJBQzNDLElBQUksQ0FBQyxlQUFlLENBQUM7cUJBQ3JCLElBQUksQ0FBQyxVQUFDLE1BQVc7b0JBQ2QsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNuQixDQUFDLENBQUM7UUFFSyxjQUFTLEdBQUc7WUFDZixNQUFNLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7Z0JBQzVCLElBQU0sT0FBTyxHQUFpQztvQkFDMUMsT0FBTyxFQUFFO3dCQUNMLGNBQWMsRUFBRSxFQUFFO3dCQUNsQixpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixxQkFBcUIsRUFBRSxJQUFJO3dCQUMzQixVQUFVLEVBQUU7NEJBQ1IsS0FBSyxFQUFFLHNDQUFzQzs0QkFDN0MsSUFBSSxFQUFFLFdBQVc7NEJBQ2pCLElBQUksRUFBRSxDQUFDOzRCQUNQLFdBQVcsRUFBRSxJQUFJOzRCQUNqQixZQUFZLEVBQUUsYUFBYTs0QkFDM0IsTUFBTSxFQUFFLElBQUk7NEJBQ1osU0FBUyxFQUFFLElBQUk7NEJBQ2Ysb0JBQW9CLEVBQUUsQ0FBQzt5QkFDMUI7d0JBQ0Qsa0JBQWtCLEVBQUUsSUFBSTtxQkFDM0I7aUJBQ0osQ0FBQztnQkFFRixNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDO3FCQUMzQyxJQUFJLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQztxQkFDMUIsSUFBSSxDQUFDLFVBQUMsTUFBVztvQkFDZCxNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNqRixDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkIsQ0FBQyxDQUFDO1FBRUssdUJBQWtCLEdBQUcsVUFBQyxLQUFVO1lBQ25DLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzFDLENBQUM7WUFDRCxJQUFNLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBRTlCLE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztnQkFDNUIsSUFBTSxPQUFPLEdBQWlDO29CQUMxQyxPQUFPLEVBQUU7d0JBQ0wsY0FBYyxFQUFFLEVBQUU7d0JBQ2xCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLHFCQUFxQixFQUFFLElBQUk7d0JBQzNCLFVBQVUsRUFBRTs0QkFDUixLQUFLLEVBQUUsc0NBQXNDOzRCQUM3QyxJQUFJLEVBQUUsV0FBVzs0QkFDakIsSUFBSSxFQUFFLENBQUM7NEJBQ1AsV0FBVyxFQUFFLElBQUk7NEJBQ2pCLFlBQVksRUFBRSxPQUFPOzRCQUNyQixNQUFNLEVBQUUsSUFBSTs0QkFDWixTQUFTLEVBQUUsSUFBSTs0QkFDZixvQkFBb0IsRUFBRSxDQUFDO3lCQUMxQjt3QkFDRCxrQkFBa0IsRUFBRSxJQUFJO3dCQUN4QixVQUFVLEVBQUUsRUFBRTt3QkFDZCxPQUFPLEVBQUUsT0FBTzt3QkFDaEIsVUFBVSxFQUFFLE9BQU87d0JBQ25CLFNBQVMsRUFBRSxFQUFFO3dCQUNiLFVBQVUsRUFBRSxTQUFTO3dCQUNyQixHQUFHLEVBQUUsRUFBRTtxQkFDVjtpQkFDSixDQUFDO2dCQUVGLE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUM7cUJBQzNDLElBQUksQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDO3FCQUMzQixJQUFJLENBQUMsVUFBQyxNQUFXO29CQUNSLElBQUEsa0ZBQUksQ0FBMkU7b0JBRXJGLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQUMsR0FBUTt3QkFDbEIsR0FBRyxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDNUMsQ0FBQyxDQUFDLENBQUM7b0JBRUgsS0FBSyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ3hCLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO2dCQUM1QixDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBRSxxQkFBcUIsR0FBRyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsQ0FBQyxDQUFDO1FBRUssZ0JBQVcsR0FBRyxVQUFDLE1BQWEsRUFBRSxPQUFtQixFQUFFLE1BQWUsRUFBRSxhQUFzQixFQUMzRSxRQUFhLEVBQUUsUUFBYSxFQUFFLE1BQVc7WUFEMUIsd0JBQUEsRUFBQSxZQUFtQjtZQUVwRCxvRkFBb0Y7WUFDcEYsb0VBQW9FO1lBQ3BFLElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFVBQUMsQ0FBTTtnQkFDaEMsSUFBSSxJQUFJLEdBQVEsRUFBRSxDQUFDO2dCQUNuQixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7WUFFSCxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxhQUFhLEVBQUUsVUFBQyxFQUFFO2dCQUMxQixPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3JCLENBQUMsQ0FBQyxDQUFDO1lBRUgsb0RBQW9EO1lBQ3BELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFDLElBQUksRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM5QyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsTUFBTSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO2dCQUN2RixDQUFDO1lBQ0wsQ0FBQztZQUVELDBEQUEwRDtZQUMxRCxJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMsQ0FBQztnQkFDckIsd0JBQXNCLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxVQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBRXZGLElBQU0sT0FBTyxHQUFpQztnQkFDMUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUM7b0JBQ3BCLFVBQVUsRUFBRSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBQ3hFLE9BQU8sRUFBRSxPQUFPO29CQUNoQixRQUFRLEVBQUUsUUFBUTtvQkFDbEIsUUFBUSxFQUFFLE1BQU07b0JBQ2hCLE1BQU0sRUFBRSxNQUFNO29CQUNkLG9CQUFvQixFQUFFLFVBQVU7aUJBQ25DLENBQUM7YUFDTCxDQUFDO1lBRUYsTUFBTSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQztpQkFDM0MsSUFBSSxDQUFDLGNBQWMsRUFBRSxPQUFPLENBQUM7aUJBQzdCLElBQUksQ0FBQyxVQUFDLE1BQVc7Z0JBQ2QsTUFBTSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDakYsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFFSyx1QkFBa0IsR0FBRyxVQUFDLFVBQWUsRUFBRSxLQUFVLEVBQUUsUUFBYTtZQUNuRSxJQUFNLE9BQU8sR0FBaUM7Z0JBQzFDLE9BQU8sRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUNwQixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLFVBQVUsRUFBRSxLQUFLO2lCQUNwQixDQUFDO2FBQ0wsQ0FBQztZQUVGLE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUM7aUJBQzNDLElBQUksQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDO2lCQUM3QixJQUFJLENBQUMsVUFBQyxNQUFXO2dCQUNkLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVLLCtCQUEwQixHQUFHO1lBQ2hDLE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztnQkFDNUIsTUFBTSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQztxQkFDM0MsSUFBSSxDQUFDLDRCQUE0QixDQUFDO3FCQUNsQyxJQUFJLENBQUMsVUFBQyxRQUFhO29CQUNoQixNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVLLG9CQUFlLEdBQUcsVUFBQyxVQUFlLEVBQUUsT0FBWTtZQUNuRCxJQUFNLE9BQU8sR0FBaUM7Z0JBQzFDLE9BQU8sRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDO29CQUNwQixVQUFVLEVBQUUsVUFBVSxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO29CQUNwRixVQUFVLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsVUFBQyxNQUFlO3dCQUN2QyxNQUFNLENBQUM7NEJBQ0gsVUFBVSxFQUFFLE1BQU0sQ0FBQyxNQUFNOzRCQUN6QixXQUFXLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXOzRCQUNyQyxlQUFlLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsZUFBZTt5QkFDN0QsQ0FBQztvQkFDTixDQUFDLENBQUM7aUJBQ0wsQ0FBQzthQUNMLENBQUM7WUFFRixNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDO2lCQUMzQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsT0FBTyxDQUFDO2lCQUNuQyxJQUFJLENBQUMsVUFBQyxNQUFXO2dCQUNkLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxVQUFDLENBQU07b0JBQ3BCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxVQUFDLENBQU07d0JBQ3BCLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDcEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7SUF2TEYsQ0FBQztJQWpCYSx3QkFBTyxHQUFrQixDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7SUF5TXJFLHVCQUFDO0NBQUEsQUEzTUQsSUEyTUM7QUEzTVksNENBQWdCIn0=

/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ViewService = /** @class */ (function () {
    /** @ngInject */
    ViewService.$inject = ["$timeout", "getTextService", "$exceptionHandler", "$templateRequest", "swUtil", "swApi", "constants", "contentService"];
    function ViewService($timeout, getTextService, $exceptionHandler, $templateRequest, swUtil, swApi, constants, contentService) {
        var _this = this;
        this.$timeout = $timeout;
        this.getTextService = getTextService;
        this.$exceptionHandler = $exceptionHandler;
        this.$templateRequest = $templateRequest;
        this.swUtil = swUtil;
        this.swApi = swApi;
        this.constants = constants;
        this.contentService = contentService;
        this.getViewTemplate = function (viewName) {
            return _this.getView(viewName).then(function (view) {
                if (!!view.templateUrl) {
                    return _this.$templateRequest(view.templateUrl, true);
                }
                return "<xui-page-content page-title=\"{{$dashboard.title}}\"\n    class=\"sw-dashboard-page\" page-layout=\"fullWidth\" is-busy=\"$dashboard.isBusy\">\n    <sw-dashboard widgets=\"$dashboard.widgets\" hide-widget-headers=\"$dashboard.hideHeaders\">\n    </sw-dashboard>\n</xui-page-content>";
            });
        };
        this.saveResourceChanges = function (resources) { return _this.$timeout(function () { return true; }, 1000); };
        this.saveResourceUserSettings = function (resourceId, settings) {
            return _this.contentService
                .one("widgets", resourceId)
                .post("usersettings", _this.serializeSettings(settings))
                .then(function (result) { return result; });
        };
        this.saveLegacyResourceUserSettings = function (resourceId, settings) {
            return _this.swApi.api(false).one("content/widgets", resourceId)
                .post("legacyusersettings", _this.serializeSettings(settings))
                .then(function (result) { return result; });
        };
        this.saveUserProperties = function (resourceId, settings) {
            return _this.swApi.api(false)
                .one("content/widgets/", resourceId)
                .post("userproperties", _this.serializeSettings(settings))
                .then(function (result) { return result; });
        };
        this.getUserProperties = function (resourceId) {
            return _this.swApi.api(false)
                .one("content/widgets", resourceId)
                .one("userproperties")
                .get()
                .then(function (settings) { return _this.deserializeSettings(settings.plain()); });
        };
        this.getResources = function (viewId) {
            return _this.swUtil.cachePromise(function () {
                return _this.contentService
                    .one("views", viewId)
                    .one("widgets")
                    .getList()
                    .then(function (resources) { return _this.mapResources(resources); });
            }, "getResources-" + viewId, null);
        };
        this.getView = function (name) {
            return _this.getViews().then(function (viewGroups) {
                var viewGroup = _.find(viewGroups, function (group) {
                    return _.some(group.views, { name: name });
                });
                if (!!viewGroup) {
                    return _.find(viewGroup.views, { name: name });
                }
                return null;
            });
        };
        this.getViews = function () {
            return _this.swUtil.cachePromise(function () {
                return _this.swApi
                    .api(false)
                    .one("content")
                    .one("viewgroups")
                    .getList()
                    .then(function (viewGroups) { return _this.mapViewGroups(viewGroups); });
            }, null, null);
        };
        this.mapViewGroups = function (viewGroups) {
            return _.map(viewGroups, function (viewGroup) {
                return {
                    id: viewGroup.id,
                    name: viewGroup.name,
                    title: viewGroup.defaultTitle,
                    sortOrder: viewGroup.sortOrder,
                    isDashboards: viewGroup.name === "dashboards",
                    views: _this.mapViews(viewGroup.views),
                    viewGroups: _this.mapViewGroups(viewGroup.viewGroups)
                };
            });
        };
        this.mapViews = function (views) {
            return _.map(views, function (view) {
                return {
                    id: view.id,
                    name: view.name,
                    type: view.type,
                    url: view.url,
                    route: _this.getViewRoute(view),
                    title: view.defaultTitle,
                    templateUrl: _this.getViewTemplateUrl(view),
                    isFavorite: view.isFavorite || false,
                    settings: view.settings,
                    sortOrder: view.sortOrder
                };
            });
        };
        this.getViewRoute = function (view) {
            return view.url ? null : "views." + view.type + "({ viewName: '" + view.name + "' })";
        };
        this.getViewTemplateUrl = function (view) {
            if (!!view.type) {
                return view.templateUrl || "[orion]:/views/" + view.type + "/" + view.type + ".html";
            }
            return null;
        };
        this.mapResources = function (resources) {
            return _.map(resources, function (resource) {
                try {
                    return {
                        id: resource.id,
                        name: resource.name,
                        module: resource.module,
                        title: resource.title,
                        helpUrl: resource.helpUrl,
                        row: resource.layout ? resource.layout.row : 0,
                        column: resource.layout ? resource.layout.column : 0,
                        width: resource.layout ? resource.layout.width : 0,
                        height: resource.layout ? resource.layout.height : 0,
                        commands: _this.mapResourceCommands(resource),
                        canRemove: resource.canRemove,
                        settings: _this.deserializeSettings(resource.settings),
                        userSettings: _this.deserializeSettings(resource.userSettings),
                        html: resource.content.html,
                        script: resource.content.script,
                        style: resource.content.style,
                        isRemoved: false,
                        cssClass: resource.cssClass,
                        attachCommandAction: function (name, action) {
                            var command = _.find(this.commands, { name: name });
                            if (!command) {
                                throw new Error("Command " + name + " not found.");
                            }
                            command.action = action;
                        }
                    };
                }
                catch (ex) {
                    _this.$exceptionHandler(ex);
                    return {
                        title: "$map failure$",
                        exception: ex,
                        row: 0,
                        column: 0,
                        width: 5,
                        height: 5
                    };
                }
            });
        };
        this.mapResourceCommands = function (resource) {
            var commands = _.map(resource.commands, function (command) { return ({
                name: command.name,
                title: command.title,
                isSeparator: command.isSeparator,
                isPrimary: command.isPrimary,
                icon: command.icon
            }); });
            if (!!commands.length) {
                commands.push({ isSeparator: true });
            }
            if (resource.isConfigurable) {
                commands.push({
                    title: _this.getTextService("Core_Resources_Configure"),
                    name: "configure"
                });
            }
            commands.push({
                title: _this.getTextService("Core_Resources_PinToHome"),
                name: "pinToHome"
            });
            if (resource.CanRemove) {
                commands.push({
                    title: _this.getTextService("Remove"),
                    name: "remove"
                });
            }
            if (!!resource.HelpUrl) {
                commands.push({
                    isSeparator: true
                }, {
                    title: _this.getTextService("Help"),
                    name: "help"
                });
            }
            if (_this.constants.environment.name === "development") {
                commands.push({
                    isSeparator: true
                }, {
                    title: _this.getTextService("Details"),
                    name: "details"
                });
            }
            return commands;
        };
        this.deserializeSettings = function (jsonSettings) {
            return _.reduce(jsonSettings, function (result, value, key) {
                result[key] = angular.fromJson(value);
                return result;
            }, {});
        };
        this.parseMacros = function (macros, netobject) {
            return _this.swApi.api(true).one("macro").post("parse", _this.serializeSettings(macros), { netObject: netobject });
        };
    }
    ViewService.prototype.serializeSettings = function (settings) {
        var jsonSettings = {};
        for (var prop in settings) {
            if (settings.hasOwnProperty(prop)) {
                jsonSettings[prop] = angular.toJson(settings[prop]);
            }
        }
        return jsonSettings;
    };
    return ViewService;
}());
exports.ViewService = ViewService;
exports.default = ViewService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlldy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBa0JwQztJQUNJLGdCQUFnQjtJQUNoQixxQkFDWSxRQUF5QixFQUN6QixjQUF3QyxFQUN4QyxpQkFBMkMsRUFDM0MsZ0JBQXlDLEVBQ3pDLE1BQWdCLEVBQ2hCLEtBQW9CLEVBQ3BCLFNBQXFCLEVBQ3JCLGNBQXdCO1FBUnBDLGlCQVNJO1FBUlEsYUFBUSxHQUFSLFFBQVEsQ0FBaUI7UUFDekIsbUJBQWMsR0FBZCxjQUFjLENBQTBCO1FBQ3hDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBMEI7UUFDM0MscUJBQWdCLEdBQWhCLGdCQUFnQixDQUF5QjtRQUN6QyxXQUFNLEdBQU4sTUFBTSxDQUFVO1FBQ2hCLFVBQUssR0FBTCxLQUFLLENBQWU7UUFDcEIsY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUNyQixtQkFBYyxHQUFkLGNBQWMsQ0FBVTtRQUc3QixvQkFBZSxHQUFHLFVBQUMsUUFBZ0I7WUFDdEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBUztnQkFDekMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUNyQixNQUFNLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3pELENBQUM7Z0JBQ0QsTUFBTSxDQUFDLDZSQUlDLENBQUM7WUFDYixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVLLHdCQUFtQixHQUFHLFVBQUMsU0FBYyxJQUF5QixPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsY0FBTSxPQUFBLElBQUksRUFBSixDQUFJLEVBQUUsSUFBSSxDQUFDLEVBQS9CLENBQStCLENBQUM7UUFFOUYsNkJBQXdCLEdBQUcsVUFBQyxVQUFrQixFQUFFLFFBQWE7WUFDaEUsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjO2lCQUNyQixHQUFHLENBQUMsU0FBUyxFQUFFLFVBQVUsQ0FBQztpQkFDMUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3RELElBQUksQ0FBQyxVQUFDLE1BQVcsSUFBSyxPQUFBLE1BQU0sRUFBTixDQUFNLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUM7UUFFSyxtQ0FBOEIsR0FBRyxVQUFDLFVBQWtCLEVBQUUsUUFBYTtZQUN0RSxNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLFVBQVUsQ0FBQztpQkFDMUQsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDNUQsSUFBSSxDQUFDLFVBQUMsTUFBVyxJQUFLLE9BQUEsTUFBTSxFQUFOLENBQU0sQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztRQUVLLHVCQUFrQixHQUFHLFVBQUMsVUFBa0IsRUFBRSxRQUFhO1lBQzFELE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7aUJBQ3ZCLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRSxVQUFVLENBQUM7aUJBQ25DLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3hELElBQUksQ0FBQyxVQUFDLE1BQVcsSUFBSyxPQUFBLE1BQU0sRUFBTixDQUFNLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUM7UUFFSyxzQkFBaUIsR0FBRyxVQUFDLFVBQWtCO1lBQzFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7aUJBQ3ZCLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLENBQUM7aUJBQ2xDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQztpQkFDckIsR0FBRyxFQUFFO2lCQUNMLElBQUksQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBMUMsQ0FBMEMsQ0FBQyxDQUFDO1FBQ3RFLENBQUMsQ0FBQztRQUVLLGlCQUFZLEdBQUcsVUFBQyxNQUFjO1lBQ2pDLE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztnQkFDNUIsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjO3FCQUNyQixHQUFHLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQztxQkFDcEIsR0FBRyxDQUFDLFNBQVMsQ0FBQztxQkFDZCxPQUFPLEVBQUU7cUJBQ1QsSUFBSSxDQUFDLFVBQUMsU0FBc0IsSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEVBQTVCLENBQTRCLENBQUMsQ0FBQztZQUN4RSxDQUFDLEVBQUUsZUFBZSxHQUFHLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUM7UUFFSyxZQUFPLEdBQUcsVUFBQyxJQUFZO1lBQzFCLE1BQU0sQ0FBQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsVUFBOEI7Z0JBQ3ZELElBQU0sU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFVBQUMsS0FBdUI7b0JBQ3pELE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDL0MsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ2QsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUNuRCxDQUFDO2dCQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyxhQUFRLEdBQUc7WUFDZCxNQUFNLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7Z0JBQzVCLE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSztxQkFDWixHQUFHLENBQUMsS0FBSyxDQUFDO3FCQUNWLEdBQUcsQ0FBQyxTQUFTLENBQUM7cUJBQ2QsR0FBRyxDQUFDLFlBQVksQ0FBQztxQkFDakIsT0FBTyxFQUFFO3FCQUNULElBQUksQ0FBQyxVQUFDLFVBQXdCLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxFQUE5QixDQUE4QixDQUFDLENBQUM7WUFDNUUsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNuQixDQUFDLENBQUM7UUFFTSxrQkFBYSxHQUFHLFVBQUMsVUFBd0I7WUFDN0MsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFVBQUMsU0FBcUI7Z0JBQzNDLE1BQU0sQ0FBQztvQkFDSCxFQUFFLEVBQUUsU0FBUyxDQUFDLEVBQUU7b0JBQ2hCLElBQUksRUFBRSxTQUFTLENBQUMsSUFBSTtvQkFDcEIsS0FBSyxFQUFFLFNBQVMsQ0FBQyxZQUFZO29CQUM3QixTQUFTLEVBQUUsU0FBUyxDQUFDLFNBQVM7b0JBQzlCLFlBQVksRUFBRSxTQUFTLENBQUMsSUFBSSxLQUFLLFlBQVk7b0JBQzdDLEtBQUssRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7b0JBQ3JDLFVBQVUsRUFBRSxLQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUM7aUJBQ25DLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFTSxhQUFRLEdBQUcsVUFBQyxLQUFjO1lBQzlCLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxVQUFDLElBQVc7Z0JBQzVCLE1BQU0sQ0FBQztvQkFDSCxFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO29CQUNmLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtvQkFDZixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsS0FBSyxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO29CQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVk7b0JBQ3hCLFdBQVcsRUFBRSxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDO29CQUMxQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLO29CQUNwQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3ZCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztpQkFDNUIsQ0FBQztZQUNOLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRU0saUJBQVksR0FBRyxVQUFDLElBQVc7WUFDL0IsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7UUFDMUYsQ0FBQyxDQUFDO1FBRU0sdUJBQWtCLEdBQUcsVUFBQyxJQUFXO1lBQ3JDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxvQkFBa0IsSUFBSSxDQUFDLElBQUksU0FBSSxJQUFJLENBQUMsSUFBSSxVQUFPLENBQUM7WUFDL0UsQ0FBQztZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDO1FBWU0saUJBQVksR0FBRyxVQUFDLFNBQXNCO1lBQzFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxVQUFDLFFBQW1CO2dCQUN4QyxJQUFJLENBQUM7b0JBQ0QsTUFBTSxDQUFDO3dCQUNILEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRTt3QkFDZixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7d0JBQ25CLE1BQU0sRUFBRSxRQUFRLENBQUMsTUFBTTt3QkFDdkIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLO3dCQUNyQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU87d0JBQ3pCLEdBQUcsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDOUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNwRCxLQUFLLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2xELE1BQU0sRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDcEQsUUFBUSxFQUFFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUM7d0JBQzVDLFNBQVMsRUFBRSxRQUFRLENBQUMsU0FBUzt3QkFDN0IsUUFBUSxFQUFFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO3dCQUNyRCxZQUFZLEVBQUUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7d0JBQzdELElBQUksRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUk7d0JBQzNCLE1BQU0sRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU07d0JBQy9CLEtBQUssRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUs7d0JBQzdCLFNBQVMsRUFBRSxLQUFLO3dCQUNoQixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVE7d0JBQzNCLG1CQUFtQixFQUFFLFVBQVUsSUFBWSxFQUFFLE1BQVc7NEJBQ3BELElBQUksT0FBTyxHQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDOzRCQUN6RCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0NBQ1gsTUFBTSxJQUFJLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxHQUFHLGFBQWEsQ0FBQyxDQUFDOzRCQUN2RCxDQUFDOzRCQUNELE9BQU8sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO3dCQUM1QixDQUFDO3FCQUNKLENBQUM7Z0JBQ04sQ0FBQztnQkFBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNWLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDM0IsTUFBTSxDQUFDO3dCQUNILEtBQUssRUFBRSxlQUFlO3dCQUN0QixTQUFTLEVBQUUsRUFBRTt3QkFDYixHQUFHLEVBQUUsQ0FBQzt3QkFDTixNQUFNLEVBQUUsQ0FBQzt3QkFDVCxLQUFLLEVBQUUsQ0FBQzt3QkFDUixNQUFNLEVBQUUsQ0FBQztxQkFDWixDQUFDO2dCQUNOLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVNLHdCQUFtQixHQUFHLFVBQUMsUUFBYTtZQUN4QyxJQUFNLFFBQVEsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsVUFBQyxPQUFZLElBQUssT0FBQSxDQUFDO2dCQUN6RCxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUk7Z0JBQ2xCLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSztnQkFDcEIsV0FBVyxFQUFFLE9BQU8sQ0FBQyxXQUFXO2dCQUNoQyxTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVM7Z0JBQzVCLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSTthQUNiLENBQUEsRUFObUQsQ0FNbkQsQ0FBQyxDQUFDO1lBRVgsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFDekMsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDO29CQUNWLEtBQUssRUFBRSxLQUFJLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDO29CQUN0RCxJQUFJLEVBQUUsV0FBVztpQkFDcEIsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztZQUVELFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQ1YsS0FBSyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsMEJBQTBCLENBQUM7Z0JBQ3RELElBQUksRUFBRSxXQUFXO2FBQ3BCLENBQUMsQ0FBQztZQUVILEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsSUFBSSxDQUFDO29CQUNWLEtBQUssRUFBRSxLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztvQkFDcEMsSUFBSSxFQUFFLFFBQVE7aUJBQ2pCLENBQUMsQ0FBQztZQUNQLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQ1YsV0FBVyxFQUFFLElBQUk7aUJBQ3BCLEVBQUU7b0JBQ0MsS0FBSyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO29CQUNsQyxJQUFJLEVBQUUsTUFBTTtpQkFDZixDQUFDLENBQUM7WUFDUCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxLQUFLLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQ1YsV0FBVyxFQUFFLElBQUk7aUJBQ3BCLEVBQUU7b0JBQ0MsS0FBSyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDO29CQUNyQyxJQUFJLEVBQUUsU0FBUztpQkFDbEIsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztZQUVELE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDO1FBRU0sd0JBQW1CLEdBQUcsVUFBQyxZQUFpQjtZQUM1QyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FDWCxZQUFZLEVBQ1osVUFBQyxNQUFXLEVBQUUsS0FBYSxFQUFFLEdBQVc7Z0JBQ3BDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ2xCLENBQUMsRUFDRCxFQUFFLENBQ0wsQ0FBQztRQUNOLENBQUMsQ0FBQztRQUVLLGdCQUFXLEdBQUcsVUFBQyxNQUFXLEVBQUUsU0FBaUI7WUFDaEQsTUFBTSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUNqRCxLQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQzlCLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxDQUMzQixDQUFDO1FBQ04sQ0FBQyxDQUFDO0lBblBDLENBQUM7SUF3SEksdUNBQWlCLEdBQXpCLFVBQTBCLFFBQWE7UUFDbkMsSUFBSSxZQUFZLEdBQVEsRUFBRSxDQUFDO1FBQzNCLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3hELENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUN4QixDQUFDO0lBb0hMLGtCQUFDO0FBQUQsQ0FBQyxBQS9QRCxJQStQQztBQS9QWSxrQ0FBVztBQWlReEIsa0JBQWUsV0FBVyxDQUFDIn0=

/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var swApi_service_1 = __webpack_require__(3);
var decorators_1 = __webpack_require__(0);
var WebAdminService = /** @class */ (function () {
    function WebAdminService(swApi, constants) {
        var _this = this;
        this.swApi = swApi;
        this.constants = constants;
        this.getUser = function () {
            return _this.constants.user;
        };
        this.getUserLabel = function () {
            var ADGroupAccountMember = 4;
            var user = _this.constants.user;
            var label = user.AccountID;
            if (user.AccountType === ADGroupAccountMember && user.GroupInfo) {
                label = label + " - " + user.GroupInfo;
            }
            return label;
        };
        this.getUserSetting = function (name) {
            return _this.adminApi
                .post("GetUserSetting", { name: name })
                .then(function (result) {
                return result.d;
            });
        };
        this.saveUserSetting = function (name, value) {
            return _this.adminApi
                .post("SaveUserSetting", { name: name, value: value })
                .then(function (result) {
                return result.d;
            });
        };
        this.getWebSetting = function (name, defaultValue) {
            return _this.adminApi
                .post("GetWebSetting", { name: name, defaultValue: defaultValue })
                .then(function (result) {
                return result.d;
            });
        };
        this.saveWebSetting = function (name, value) {
            return _this.adminApi
                .post("SaveWebSetting", { name: name, value: value })
                .then(function (result) {
                return result.d;
            });
        };
    }
    Object.defineProperty(WebAdminService.prototype, "adminApi", {
        get: function () {
            return this.swApi.ws.one("WebAdmin.asmx");
        },
        enumerable: true,
        configurable: true
    });
    WebAdminService = __decorate([
        __param(0, decorators_1.Inject("swApi")),
        __param(1, decorators_1.Inject("constants")),
        __metadata("design:paramtypes", [swApi_service_1.SwApiService, Object])
    ], WebAdminService);
    return WebAdminService;
}());
exports.WebAdminService = WebAdminService;
exports.default = WebAdminService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2ViQWRtaW4tc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndlYkFkbWluLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFDQSxpREFBK0M7QUFFL0MsNENBQXVDO0FBRXZDO0lBQ0kseUJBRVksS0FBbUIsRUFFbkIsU0FBYztRQUoxQixpQkFLSTtRQUhRLFVBQUssR0FBTCxLQUFLLENBQWM7UUFFbkIsY0FBUyxHQUFULFNBQVMsQ0FBSztRQU9uQixZQUFPLEdBQUc7WUFDYixNQUFNLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFDL0IsQ0FBQyxDQUFBO1FBRU0saUJBQVksR0FBRztZQUNsQixJQUFNLG9CQUFvQixHQUFXLENBQUMsQ0FBQztZQUN2QyxJQUFNLElBQUksR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztZQUNqQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBRTNCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssb0JBQW9CLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlELEtBQUssR0FBTSxLQUFLLFdBQU0sSUFBSSxDQUFDLFNBQVcsQ0FBQztZQUMzQyxDQUFDO1lBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDLENBQUE7UUFFTSxtQkFBYyxHQUFHLFVBQUMsSUFBWTtZQUNqQyxNQUFNLENBQUMsS0FBSSxDQUFDLFFBQVE7aUJBQ2YsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDO2lCQUN0QyxJQUFJLENBQUMsVUFBQyxNQUFNO2dCQUNULE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFBO1FBRU0sb0JBQWUsR0FBRyxVQUFDLElBQVksRUFBRSxLQUFVO1lBQzlDLE1BQU0sQ0FBQyxLQUFJLENBQUMsUUFBUTtpQkFDZixJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQztpQkFDckQsSUFBSSxDQUFDLFVBQUMsTUFBTTtnQkFDVCxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNwQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQTtRQUVNLGtCQUFhLEdBQUcsVUFBQyxJQUFZLEVBQUUsWUFBaUI7WUFDbkQsTUFBTSxDQUFDLEtBQUksQ0FBQyxRQUFRO2lCQUNmLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsQ0FBQztpQkFDakUsSUFBSSxDQUFDLFVBQUMsTUFBTTtnQkFDVCxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNwQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQTtRQUVNLG1CQUFjLEdBQUcsVUFBQyxJQUFZLEVBQUUsS0FBVTtZQUM3QyxNQUFNLENBQUMsS0FBSSxDQUFDLFFBQVE7aUJBQ2YsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUM7aUJBQ3BELElBQUksQ0FBQyxVQUFDLE1BQU07Z0JBQ1QsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDcEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUE7SUFwREUsQ0FBQztJQUVKLHNCQUFZLHFDQUFRO2FBQXBCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM5QyxDQUFDOzs7T0FBQTtJQVZRLGVBQWU7UUFFbkIsV0FBQSxtQkFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBRWYsV0FBQSxtQkFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO3lDQURMLDRCQUFZO09BSHRCLGVBQWUsQ0EyRDNCO0lBQUQsc0JBQUM7Q0FBQSxBQTNERCxJQTJEQztBQTNEWSwwQ0FBZTtBQTZENUIsa0JBQWUsZUFBZSxDQUFDIn0=

/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SwisFilterOperator = SW.orion.SwisFilterOperator;
exports.uriFiltersParam = "filters";
var QueryFilterFactory = /** @class */ (function () {
    function QueryFilterFactory() {
    }
    QueryFilterFactory.prototype.createStaticUriQueryFormatter = function (staticText) {
        return function () { return staticText; };
    };
    QueryFilterFactory.prototype.createSimpleUriQueryOperator = function (operator) {
        return function (condition) {
            var result = (isNaN(Number(condition.value))) ? "'" + condition.value + "'" : condition.value;
            return condition.property + ":" + operator + ":" + result;
        };
    };
    QueryFilterFactory.prototype.createBetweenUriQueryOperator = function (operator) {
        return function (condition) { return condition.property + ":" + operator + ":" + condition.value; };
    };
    QueryFilterFactory.prototype.createLikeUriQueryOperator = function (operator) {
        return function (condition) { return condition.property + ":" + operator + ":" + condition.value; };
    };
    QueryFilterFactory.prototype.createInUriQueryOperator = function (operator) {
        return function (condition) {
            if (Array.isArray(condition.value)) {
                return condition.property + ":" + operator + ":" + condition.value.join(",");
            }
            else {
                return condition.property + ":" + operator + ":" + condition.value;
            }
        };
    };
    return QueryFilterFactory;
}());
var GlobalFilteringQueryStringBuilderService = /** @class */ (function () {
    function GlobalFilteringQueryStringBuilderService(parentBuilder) {
        this.queries = Array();
        this.queryFilterFactory = new QueryFilterFactory();
        this.queryBuilderMap = new Map([
            [undefined, this.queryFilterFactory.createStaticUriQueryFormatter("")],
            [
                SwisFilterOperator.Equals,
                this.queryFilterFactory.createSimpleUriQueryOperator(SwisFilterOperator.Equals),
            ],
            [
                SwisFilterOperator.EqualsNot,
                this.queryFilterFactory.createSimpleUriQueryOperator(SwisFilterOperator.EqualsNot),
            ],
            [
                SwisFilterOperator.Greater,
                this.queryFilterFactory.createSimpleUriQueryOperator(SwisFilterOperator.Greater),
            ],
            [
                SwisFilterOperator.GreaterOrEqual,
                this.queryFilterFactory.createSimpleUriQueryOperator(SwisFilterOperator.GreaterOrEqual),
            ],
            [
                SwisFilterOperator.Lower,
                this.queryFilterFactory.createSimpleUriQueryOperator(SwisFilterOperator.Lower),
            ],
            [
                SwisFilterOperator.LowerOrEqual,
                this.queryFilterFactory.createSimpleUriQueryOperator(SwisFilterOperator.LowerOrEqual),
            ],
            [
                SwisFilterOperator.Like,
                this.queryFilterFactory.createLikeUriQueryOperator(SwisFilterOperator.Like),
            ],
            [
                SwisFilterOperator.Between,
                this.queryFilterFactory.createBetweenUriQueryOperator(SwisFilterOperator.Between),
            ],
            [
                SwisFilterOperator.In,
                this.queryFilterFactory.createInUriQueryOperator(SwisFilterOperator.In),
            ],
        ]);
        this.stringQueries = [];
        this.parentQueryBuilder = parentBuilder;
    }
    GlobalFilteringQueryStringBuilderService.prototype.withParameters = function (condition) {
        var funct = this.queryBuilderMap.get(condition.operator);
        if (funct) {
            this.stringQueries.push(funct(condition));
        }
        return this;
    };
    GlobalFilteringQueryStringBuilderService.prototype.getFilterQueryString = function () {
        var result = "filters=";
        result = result.concat(this.stringQueries.join("-"));
        return result;
    };
    return GlobalFilteringQueryStringBuilderService;
}());
exports.GlobalFilteringQueryStringBuilderService = GlobalFilteringQueryStringBuilderService;
exports.default = GlobalFilteringQueryStringBuilderService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2xvYmFsRmlsdGVyaW5nUXVlcnlTdHJpbmdCdWlsZGVyLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJnbG9iYWxGaWx0ZXJpbmdRdWVyeVN0cmluZ0J1aWxkZXItc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0NBQW9DOztBQUdwQyxJQUFPLGtCQUFrQixHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUM7QUFHM0MsUUFBQSxlQUFlLEdBQVcsU0FBUyxDQUFDO0FBRWpEO0lBQUE7SUE2QkEsQ0FBQztJQTVCVSwwREFBNkIsR0FBcEMsVUFBcUMsVUFBZTtRQUNoRCxNQUFNLENBQUMsY0FBTSxPQUFBLFVBQVUsRUFBVixDQUFVLENBQUM7SUFDNUIsQ0FBQztJQUVNLHlEQUE0QixHQUFuQyxVQUFvQyxRQUE0QjtRQUM1RCxNQUFNLENBQUMsVUFBQyxTQUE4QjtZQUNsQyxJQUFNLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBSSxTQUFTLENBQUMsS0FBSyxNQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7WUFDM0YsTUFBTSxDQUFJLFNBQVMsQ0FBQyxRQUFRLFNBQUksUUFBUSxTQUFJLE1BQVEsQ0FBQztRQUN6RCxDQUFDLENBQUM7SUFDTixDQUFDO0lBRU0sMERBQTZCLEdBQXBDLFVBQXFDLFFBQTRCO1FBQzdELE1BQU0sQ0FBQyxVQUFDLFNBQThCLElBQUssT0FBRyxTQUFTLENBQUMsUUFBUSxTQUFJLFFBQVEsU0FBSSxTQUFTLENBQUMsS0FBTyxFQUF0RCxDQUFzRCxDQUFDO0lBQ3RHLENBQUM7SUFFTSx1REFBMEIsR0FBakMsVUFBa0MsUUFBNEI7UUFDMUQsTUFBTSxDQUFDLFVBQUMsU0FBOEIsSUFBSyxPQUFHLFNBQVMsQ0FBQyxRQUFRLFNBQUksUUFBUSxTQUFJLFNBQVMsQ0FBQyxLQUFPLEVBQXRELENBQXNELENBQUM7SUFDdEcsQ0FBQztJQUVNLHFEQUF3QixHQUEvQixVQUFnQyxRQUE0QjtRQUN4RCxNQUFNLENBQUMsVUFBQyxTQUE4QjtZQUNsQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLE1BQU0sQ0FBSSxTQUFTLENBQUMsUUFBUSxTQUFJLFFBQVEsU0FBSSxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUcsQ0FBQztZQUM1RSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osTUFBTSxDQUFJLFNBQVMsQ0FBQyxRQUFRLFNBQUksUUFBUSxTQUFJLFNBQVMsQ0FBQyxLQUFPLENBQUM7WUFDbEUsQ0FBQztRQUNMLENBQUMsQ0FBQztJQUNOLENBQUM7SUFDTCx5QkFBQztBQUFELENBQUMsQUE3QkQsSUE2QkM7QUFFRDtJQVVJLGtEQUNJLGFBQXdEO1FBTnBELFlBQU8sR0FBRyxLQUFLLEVBQXVCLENBQUM7UUFZdkMsdUJBQWtCLEdBQUcsSUFBSSxrQkFBa0IsRUFBRSxDQUFDO1FBRTlDLG9CQUFlLEdBQUcsSUFBSSxHQUFHLENBQzdCO1lBQ0ksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDZCQUE2QixDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3RFO2dCQUNJLGtCQUFrQixDQUFDLE1BQU07Z0JBQ3pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7YUFDbEY7WUFDRDtnQkFDSSxrQkFBa0IsQ0FBQyxTQUFTO2dCQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDO2FBQ3JGO1lBQ0Q7Z0JBQ0ksa0JBQWtCLENBQUMsT0FBTztnQkFDMUIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQzthQUNuRjtZQUNEO2dCQUNJLGtCQUFrQixDQUFDLGNBQWM7Z0JBQ2pDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUM7YUFDMUY7WUFDRDtnQkFDSSxrQkFBa0IsQ0FBQyxLQUFLO2dCQUN4QixJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDO2FBQ2pGO1lBQ0Q7Z0JBQ0ksa0JBQWtCLENBQUMsWUFBWTtnQkFDL0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQzthQUN4RjtZQUNEO2dCQUNJLGtCQUFrQixDQUFDLElBQUk7Z0JBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQywwQkFBMEIsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7YUFDOUU7WUFDRDtnQkFDSSxrQkFBa0IsQ0FBQyxPQUFPO2dCQUMxQixJQUFJLENBQUMsa0JBQWtCLENBQUMsNkJBQTZCLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDO2FBQ3BGO1lBQ0Q7Z0JBQ0ksa0JBQWtCLENBQUMsRUFBRTtnQkFDckIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLGtCQUFrQixDQUFDLEVBQUUsQ0FBQzthQUMxRTtTQUNKLENBQ0osQ0FBQztRQTlDRSxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsYUFBYSxDQUFDO0lBQzVDLENBQUM7SUE4Q00saUVBQWMsR0FBckIsVUFBc0IsU0FBOEI7UUFDaEQsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDUixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FDbkIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUNuQixDQUFDO1FBQ04sQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLHVFQUFvQixHQUEzQjtRQUNJLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQztRQUV4QixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUNMLCtDQUFDO0FBQUQsQ0FBQyxBQTdFRCxJQTZFQztBQTdFWSw0RkFBd0M7QUErRXJELGtCQUFlLHdDQUF3QyxDQUFDIn0=

/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dashboard_1 = __webpack_require__(186);
var developer_1 = __webpack_require__(188);
var i18nDemo_1 = __webpack_require__(203);
var orionError_1 = __webpack_require__(206);
var views_config_1 = __webpack_require__(207);
var subview_1 = __webpack_require__(208);
exports.default = function (module) {
    module.app().config(views_config_1.default);
    dashboard_1.default(module);
    developer_1.default(module);
    i18nDemo_1.default(module);
    orionError_1.default(module);
    subview_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlDQUFvQztBQUNwQyx5Q0FBb0M7QUFDcEMsdUNBQWtDO0FBQ2xDLDJDQUFzQztBQUN0QywrQ0FBOEM7QUFDOUMscUNBQWdDO0FBRWhDLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLHNCQUFnQixDQUFDLENBQUM7SUFFdEMsbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsQixtQkFBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xCLGtCQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakIsb0JBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQixpQkFBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3BCLENBQUMsQ0FBQyJ9

/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dashboard_controller_1 = __webpack_require__(187);
exports.default = function (module) {
    module.controller("DashboardController", dashboard_controller_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtEQUF5RDtBQUV6RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsRUFBRSw4QkFBbUIsQ0FBQyxDQUFDO0FBQ2xFLENBQUMsQ0FBQyJ9

/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DashboardController = /** @class */ (function () {
    function DashboardController($q, $state, $timeout, viewService, view) {
        var _this = this;
        this.$q = $q;
        this.$state = $state;
        this.$timeout = $timeout;
        this.viewService = viewService;
        this.view = view;
        this.$onInit = function () {
            _this.dashboard = _this.view;
            _this.title = _this.view.title;
            _this.getWidgets(_this.dashboard.id);
        };
        this.hideHeaders = false;
        this.getWidgets = function (viewId) {
            if (!_this.widgets) {
                _this.isBusy = true;
                return _this.viewService
                    .getResources(viewId)
                    .then(function (widgets) {
                    _this.widgets = widgets;
                    return widgets;
                }).finally(function () {
                    _this.isBusy = false;
                });
            }
            return _this.$q.when(_this.widgets);
        };
    }
    DashboardController.$inject = ["$q", "$state", "$timeout", "viewService", "view"];
    return DashboardController;
}());
exports.default = DashboardController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkYXNoYm9hcmQtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBO0lBR0ksNkJBQW9CLEVBQWdCLEVBQVUsTUFBZ0MsRUFDbEUsUUFBNEIsRUFBVSxXQUF5QixFQUMvRCxJQUFTO1FBRnJCLGlCQUdDO1FBSG1CLE9BQUUsR0FBRixFQUFFLENBQWM7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUEwQjtRQUNsRSxhQUFRLEdBQVIsUUFBUSxDQUFvQjtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFjO1FBQy9ELFNBQUksR0FBSixJQUFJLENBQUs7UUFHZCxZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUM7WUFDM0IsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUM3QixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdkMsQ0FBQyxDQUFDO1FBTUssZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFFbkIsZUFBVSxHQUFHLFVBQUMsTUFBVztZQUM3QixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXO3FCQUNsQixZQUFZLENBQUMsTUFBTSxDQUFDO3FCQUNwQixJQUFJLENBQUMsVUFBQyxPQUFZO29CQUNmLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO29CQUN2QixNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUNuQixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7b0JBQ1AsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQztZQUVELE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFBO0lBNUJELENBQUM7SUFMYSwyQkFBTyxHQUFHLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBbUNoRiwwQkFBQztDQUFBLEFBcENELElBb0NDO0FBRUQsa0JBQWUsbUJBQW1CLENBQUMifQ==

/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dialogs_1 = __webpack_require__(189);
var htmlElements_1 = __webpack_require__(191);
var entityPopover_1 = __webpack_require__(193);
__webpack_require__(200);
var developer_config_1 = __webpack_require__(201);
var developer_controller_1 = __webpack_require__(202);
exports.default = function (module) {
    dialogs_1.default(module);
    htmlElements_1.default(module);
    entityPopover_1.entityPopover(module);
    module.config(developer_config_1.default);
    module.controller("DeveloperController", developer_controller_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFDQUFnQztBQUNoQywrQ0FBMEM7QUFDMUMsaURBQWdEO0FBQ2hELDRCQUEwQjtBQUMxQix1REFBbUQ7QUFDbkQsK0RBQXlEO0FBRXpELGtCQUFlLFVBQUMsTUFBZTtJQUMzQixpQkFBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hCLHNCQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDckIsNkJBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUV0QixNQUFNLENBQUMsTUFBTSxDQUFDLDBCQUFpQixDQUFDLENBQUM7SUFDakMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsRUFBRSw4QkFBbUIsQ0FBQyxDQUFDO0FBQ2xFLENBQUMsQ0FBQyJ9

/***/ }),
/* 189 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(190);
exports.default = function (module) {
    // to be implemented after ts conversion
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1DQUFpQztBQUdqQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0Isd0NBQXdDO0FBQzVDLENBQUMsQ0FBQyJ9

/***/ }),
/* 190 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 191 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var htmlElements_config_1 = __webpack_require__(192);
exports.default = function (module) {
    htmlElements_config_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDZEQUF1RDtBQUV2RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsNkJBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDL0IsQ0FBQyxDQUFDIn0=

/***/ }),
/* 192 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    function htmlElementsConfigFn($stateProvider) {
        $stateProvider
            .state("html-elements", {
            title: "Html elements",
            url: "/developer/html-elements",
            template: __webpack_require__(32)
        });
    }
    htmlElementsConfigFn.$inject = ["$stateProvider"];
    module.config(htmlElementsConfigFn);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHRtbEVsZW1lbnRzLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImh0bWxFbGVtZW50cy1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsOEJBQThCLGNBQW9DO1FBQzlELGNBQWM7YUFDVCxLQUFLLENBQUMsZUFBZSxFQUFRO1lBQzFCLEtBQUssRUFBRSxlQUFlO1lBQ3RCLEdBQUcsRUFBRSwwQkFBMEI7WUFDL0IsUUFBUSxFQUFFLE9BQU8sQ0FBUyxzQkFBc0IsQ0FBQztTQUNwRCxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUVsRCxNQUFNLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLENBQUM7QUFDeEMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 193 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var entityPopover_config_1 = __webpack_require__(194);
exports.entityPopover = function (module) {
    entityPopover_config_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtEQUF5RDtBQUU1QyxRQUFBLGFBQWEsR0FBRyxVQUFDLE1BQWU7SUFDekMsOEJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDaEMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 194 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var fake_data_1 = __webpack_require__(195);
var fakeDataHttpInterceptor_1 = __webpack_require__(198);
exports.default = function (module) {
    module.config(function ($stateProvider) {
        $stateProvider
            .state("entity-popover", {
            title: "Entity Popover demo",
            url: "/developer/entity-popover",
            template: __webpack_require__(31)
        });
    }, ["$stateProvider"]);
    var rxLocation = /\/ui\/developer\/entity\-popover/;
    function createInterceptor($location) {
        if (!rxLocation.test($location.absUrl())) {
            return {};
        }
        return new fakeDataHttpInterceptor_1.FakeDataHttpInterceptor(fake_data_1.MockData);
    }
    createInterceptor.$inject = ["$location"];
    module.factory("swEntityPopoverFakeDataHttpInterceptor", createInterceptor);
    module.config(function ($httpProvider) {
        $httpProvider.interceptors.push("swEntityPopoverFakeDataHttpInterceptor");
    }, ["$httpProvider"]);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5UG9wb3Zlci1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnRpdHlQb3BvdmVyLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlDQUF1QztBQUN2QyxxRUFBb0U7QUFFcEUsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQ1QsVUFBVSxjQUFvQztRQUMxQyxjQUFjO2FBQ1QsS0FBSyxDQUFDLGdCQUFnQixFQUFRO1lBQzNCLEtBQUssRUFBRSxxQkFBcUI7WUFDNUIsR0FBRyxFQUFFLDJCQUEyQjtZQUNoQyxRQUFRLEVBQUUsT0FBTyxDQUFTLHNCQUFzQixDQUFDO1NBQ3BELENBQUMsQ0FBQztJQUNYLENBQUMsRUFDRCxDQUFDLGdCQUFnQixDQUFDLENBQ3JCLENBQUM7SUFFRixJQUFNLFVBQVUsR0FBRyxrQ0FBa0MsQ0FBQztJQUV0RCwyQkFBMkIsU0FBOEI7UUFDckQsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ2QsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLGlEQUF1QixDQUFDLG9CQUFRLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBQ0QsaUJBQWlCLENBQUMsT0FBTyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7SUFFMUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyx3Q0FBd0MsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBRTVFLE1BQU0sQ0FBQyxNQUFNLENBQ1QsVUFBVSxhQUErQjtRQUNyQyxhQUFhLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO0lBQzlFLENBQUMsRUFDRCxDQUFDLGVBQWUsQ0FBQyxDQUNwQixDQUFDO0FBQ04sQ0FBQyxDQUFDIn0=

/***/ }),
/* 195 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var fake_data_tooltip_enabled_1 = __webpack_require__(196);
var fake_data_tooltip_data_1 = __webpack_require__(197);
exports.MockData = [
    fake_data_tooltip_enabled_1.FakeTooltipEnabled,
    fake_data_tooltip_data_1.FakeTooltipData
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFrZS1kYXRhLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZmFrZS1kYXRhLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EseUVBQWlFO0FBQ2pFLG1FQUEyRDtBQUU5QyxRQUFBLFFBQVEsR0FBcUI7SUFDdEMsOENBQWtCO0lBQ2xCLHdDQUFlO0NBQ2xCLENBQUMifQ==

/***/ }),
/* 196 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable:max-line-length
// tslint:disable:whitespace
var dataTooltipEnabled = [
    ["generic", "AAT", "Orion.AlertActive", "C", "Orion.Groups", "N", "Orion.Nodes", "V", "Orion.Volume", "I", "Orion.NPM.Interfaces", "NVS", "Orion.NPM.VSANs", "TEST", "Orion.Test"]
];
// tslint:enable:max-line-length
// tslint:enable:whitespace
exports.FakeTooltipEnabled = {
    urlMatcher: /\/api2\/tooltip\/types\/enabled/,
    id: function () { return "tooltip-enabled"; },
    data: dataTooltipEnabled
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFrZS1kYXRhLXRvb2x0aXAtZW5hYmxlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZha2UtZGF0YS10b29sdGlwLWVuYWJsZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSxpQ0FBaUM7QUFDakMsNEJBQTRCO0FBQzVCLElBQU0sa0JBQWtCLEdBQWU7SUFDbkMsQ0FBQyxTQUFTLEVBQUMsS0FBSyxFQUFDLG1CQUFtQixFQUFDLEdBQUcsRUFBQyxjQUFjLEVBQUMsR0FBRyxFQUFDLGFBQWEsRUFBQyxHQUFHLEVBQUMsY0FBYyxFQUFDLEdBQUcsRUFBQyxzQkFBc0IsRUFBQyxLQUFLLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxFQUFDLFlBQVksQ0FBQztDQUN2SyxDQUFDO0FBQ0YsZ0NBQWdDO0FBQ2hDLDJCQUEyQjtBQUVkLFFBQUEsa0JBQWtCLEdBQXdCO0lBQ25ELFVBQVUsRUFBRSxpQ0FBaUM7SUFDN0MsRUFBRSxFQUFFLGNBQU0sT0FBQSxpQkFBaUIsRUFBakIsQ0FBaUI7SUFDM0IsSUFBSSxFQUFFLGtCQUFrQjtDQUMzQixDQUFDIn0=

/***/ }),
/* 197 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable:max-line-length
var dataTooltipData = [
    {
        "identifiers": {
            "opid": "0_Orion.Nodes_1",
            "netObject": "N:1",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=1",
            "entityType": "Orion.Nodes",
            "instanceSiteId": 0
        },
        "title": "QA-BRN-RMEL-01",
        "status": "2",
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:1&NoTip",
                "permissionRequired": null,
                "subItems": null
            }, {
                "title": "Edit Node",
                "action": null,
                "url": "/Orion/Nodes/NodeProperties.aspx?Nodes=1",
                "permissionRequired": "nodeManagement",
                "subItems": null
            }, {
                "title": "Mute Alerts Now",
                "action": {
                    "name": "swAlertSuppressionService",
                    "method": "SuppressAlerts",
                    "params": {
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=1"
                    }
                },
                "url": null,
                "permissionRequired": "alertManagement",
                "subItems": null
            }, {
                "title": "Unmanage Now",
                "action": {
                    "name": "swEntityManagementService",
                    "method": "Unmanage",
                    "params": {
                        "entityType": "Orion.Nodes",
                        "entityId": 1
                    }
                },
                "url": null,
                "permissionRequired": "unmanage",
                "subItems": null
            }],
        "data": [{
                "label": "IP Address",
                "type": "System.String",
                "value": "10.140.27.2",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Machine Type",
                "type": "System.String",
                "value": "Windows 7 Workstation",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Average Response Time",
                "type": "System.Int32",
                "value": -1,
                "unit": "ms",
                "color": "ok",
                "cp": false
            }, {
                "label": "Percent Loss",
                "type": "System.Double",
                "value": 100.0,
                "unit": "%",
                "color": "critical",
                "cp": false
            }, {
                "label": "Percent Memory Used",
                "type": "System.Int32",
                "value": 80,
                "unit": "%",
                "color": "critical",
                "cp": false
            }, {
                "label": "City",
                "type": "System.String",
                "value": null,
                "unit": null,
                "color": null,
                "cp": true
            }, {
                "label": "Comments",
                "type": "System.String",
                "value": null,
                "unit": null,
                "color": null,
                "cp": true
            }, {
                "label": "Department",
                "type": "System.String",
                "value": null,
                "unit": null,
                "color": null,
                "cp": true
            }, {
                "label": "This is one long label to demonstrate ellipsis in property label",
                "type": "System.String",
                "value": "not important",
                "unit": null,
                "color": null,
                "cp": true
            }, {
                "label": "Ellipsis value",
                "type": "System.String",
                "value": "This value should be long enough for ellipsis to kick in, demonstrating our ability to use ellipsis on property value",
                "unit": null,
                "color": null,
                "cp": true
            }],
        "extensionData": {}
    }, {
        "identifiers": {
            "opid": "0_Orion.Nodes_2",
            "netObject": "N:2",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=2",
            "entityType": "Orion.Nodes",
            "instanceSiteId": 0
        },
        "title": "QA-BRN-ZDYR-04",
        "status": "2",
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:2&NoTip",
                "permissionRequired": null,
                "subItems": null
            }, {
                "title": "Edit Node",
                "action": null,
                "url": "/Orion/Nodes/NodeProperties.aspx?Nodes=2",
                "permissionRequired": "nodeManagement",
                "subItems": null
            }, {
                "title": "Mute Alerts Now",
                "action": {
                    "name": "swAlertSuppressionService",
                    "method": "SuppressAlerts",
                    "params": {
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=2"
                    }
                },
                "url": null,
                "permissionRequired": "alertManagement",
                "subItems": null
            }, {
                "title": "Unmanage Now",
                "action": {
                    "name": "swEntityManagementService",
                    "method": "Unmanage",
                    "params": {
                        "entityType": "Orion.Nodes",
                        "entityId": 2
                    }
                },
                "url": null,
                "permissionRequired": "unmanage",
                "subItems": null
            }],
        "data": [{
                "label": "IP Address",
                "type": "System.String",
                "value": "10.140.27.3",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Machine Type",
                "type": "System.String",
                "value": "Windows 2012 R2 Server",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Average Response Time",
                "type": "System.Int32",
                "value": -1,
                "unit": "ms",
                "color": "ok",
                "cp": false
            }, {
                "label": "Percent Loss",
                "type": "System.Double",
                "value": 100.0,
                "unit": "%",
                "color": "critical",
                "cp": false
            }, {
                "label": "CPU Load",
                "type": "System.Int32",
                "value": 5,
                "unit": "%",
                "color": "warning",
                "cp": false
            }, {
                "label": "Percent Memory Used",
                "type": "System.Int32",
                "value": 70,
                "unit": "%",
                "color": "critical",
                "cp": false
            }, {
                "label": "City",
                "type": "System.String",
                "value": null,
                "unit": null,
                "color": null,
                "cp": true
            }, {
                "label": "Comments",
                "type": "System.String",
                "value": null,
                "unit": null,
                "color": null,
                "cp": true
            }, {
                "label": "Department",
                "type": "System.String",
                "value": null,
                "unit": null,
                "color": null,
                "cp": true
            }],
        "extensionData": {}
    }, {
        "identifiers": {
            "opid": "0_Orion.Volumes_1",
            "netObject": "V:1",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=1/Volumes/VolumeID=1",
            "entityType": "Orion.Volumes",
            "instanceSiteId": 0
        },
        "title": "Physical Memory",
        "status": "12",
        "childStatus": null,
        "noTip": null,
        "menu": [],
        "data": [{
                "label": "Caption",
                "type": "System.String",
                "value": "Physical Memory",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Volume Type",
                "type": "System.String",
                "value": "RAM",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Size",
                "type": "System.Double",
                "value": 5.75,
                "unit": "GB",
                "color": null,
                "cp": false
            }, {
                "label": "Percent used",
                "type": "System.Single",
                "value": 80,
                "unit": "%",
                "color": null,
                "cp": false
            }],
        "extensionData": {}
    }, {
        "identifiers": {
            "opid": "0_Orion.Groups_1",
            "netObject": "C:1",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=1",
            "entityType": "Orion.Groups",
            "instanceSiteId": 0
        },
        "title": "Group (all ok)",
        "status": "1",
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:1&NoTip",
                "permissionRequired": null,
                "subItems": null
            }, {
                "title": "Edit Group",
                "action": null,
                "url": "/Orion/Admin/Containers/EditContainer.aspx?id=1",
                "permissionRequired": "nodeManagement",
                "subItems": null
            }, {
                "title": "Mute Alerts Now",
                "action": {
                    "name": "swAlertSuppressionService",
                    "method": "SuppressAlerts",
                    "params": {
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=1"
                    }
                },
                "url": null,
                "permissionRequired": "alertManagement",
                "subItems": null
            }],
        "data": [],
        "extensionData": {
            "container": {
                "members": [],
                "moreMembers": 0,
                "detailUrl": "/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:1&NoTip"
            }
        }
    }, {
        "identifiers": {
            "opid": "0_Orion.Groups_2",
            "netObject": "C:2",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=2",
            "entityType": "Orion.Groups",
            "instanceSiteId": 0
        },
        "title": "Group (some problems)",
        "status": "3",
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:2&NoTip",
                "permissionRequired": null,
                "subItems": null
            }, {
                "title": "Edit Group",
                "action": null,
                "url": "/Orion/Admin/Containers/EditContainer.aspx?id=2",
                "permissionRequired": "nodeManagement",
                "subItems": null
            }, {
                "title": "Mute Alerts Now",
                "action": {
                    "name": "swAlertSuppressionService",
                    "method": "SuppressAlerts",
                    "params": {
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=2"
                    }
                },
                "url": null,
                "permissionRequired": "alertManagement",
                "subItems": null
            }],
        "data": [],
        "extensionData": {
            "container": {
                "members": [{
                        "displayName": "DEV-BRN-SHRB-02 is actually a very long name to display in one row",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=2/Members/MemberPrimaryID=33,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "QA-BRN-WHD-09 is also very long although you wouldn't think so on the first impression",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=2/Members/MemberPrimaryID=5,MemberEntityType=\"Orion.Nodes\""
                    }],
                "moreMembers": 0,
                "detailUrl": "/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:2&NoTip"
            }
        }
    }, {
        "identifiers": {
            "opid": "0_Orion.Groups_3",
            "netObject": "C:3",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3",
            "entityType": "Orion.Groups",
            "instanceSiteId": 0
        },
        "title": "Group (lots of problems)",
        "status": "3",
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:3&NoTip",
                "permissionRequired": null,
                "subItems": null
            }, {
                "title": "Edit Group",
                "action": null,
                "url": "/Orion/Admin/Containers/EditContainer.aspx?id=3",
                "permissionRequired": "nodeManagement",
                "subItems": null
            }, {
                "title": "Mute Alerts Now",
                "action": {
                    "name": "swAlertSuppressionService",
                    "method": "SuppressAlerts",
                    "params": {
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3"
                    }
                },
                "url": null,
                "permissionRequired": "alertManagement",
                "subItems": null
            }],
        "data": [],
        "extensionData": {
            "container": {
                "members": [{
                        "displayName": "DEV-BRN-SHRB-02",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=33,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "QA-BRN-WHD-09",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=5,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DESKTOP-1EV4JJ4",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=11,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DEV-BRN-DNAV-01",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=37,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DEV-BRN-JFRY-10",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=21,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DEV-BRN-JLOL-01",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=18,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DEV-BRN-JTUR-03",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=8,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DEV-BRN-JUHL-08",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=41,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DEV-BRN-JUHL-09",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=43,MemberEntityType=\"Orion.Nodes\""
                    }, {
                        "displayName": "DEV-BRN-LEM-INS",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Groups/ContainerID=3/Members/MemberPrimaryID=19,MemberEntityType=\"Orion.Nodes\""
                    }],
                "moreMembers": 23,
                "detailUrl": "/Orion/NetPerfMon/ContainerDetails.aspx?NetObject=C:3&NoTip"
            }
        }
    }, {
        "identifiers": {
            "opid": "0_Orion.AlertActive_1",
            "netObject": "AAT:1",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.AlertActive/AlertActiveID=1,AlertObjectID=1",
            "entityType": "Orion.AlertActive",
            "instanceSiteId": 0
        },
        "title": "NoTip",
        "status": null,
        "childStatus": null,
        "noTip": true,
        "menu": [],
        "data": [],
        "extensionData": {}
    }, {
        "identifiers": {
            "opid": "0_Orion.AlertActive_2",
            "netObject": "AAT:2",
            "uri": null,
            "entityType": "Orion.AlertActive",
            "instanceSiteId": 0
        },
        "title": "Active Global Alert | 9 Nodes",
        "status": null,
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:2&NoTip",
                "permissionRequired": null,
                "subItems": null
            }],
        "data": [],
        "extensionData": {
            "activeAlert": {
                "alert": {
                    "alertActiveID": 25136,
                    "alertObjectID": 3,
                    "acknowledged": null,
                    "acknowledgedBy": null,
                    "acknowledgedDateTime": null,
                    "acknowledgedNote": null,
                    "triggeredDateTime": "2017-11-01T12:47:21.487Z",
                    "triggeredMessage": "Overly Overwhelmed was triggered.",
                    "numberOfNotes": null,
                    "lastExecutedEscalationLevel": null,
                    "alertObjects": null,
                    "associatedAlertObjects": [{
                            "alertObjectID": 2,
                            "alertID": 2,
                            "entityUri": null,
                            "entityType": "Orion.Nodes",
                            "entityCaption": "9 Nodes",
                            "entityDetailsUrl": null,
                            "entityNetObjectId": "",
                            "relatedNodeUri": "",
                            "relatedNodeId": null,
                            "relatedNodeDetailsUrl": "",
                            "relatedNodeCaption": "",
                            "realEntityUri": null,
                            "realEntityType": "Orion.Nodes",
                            "triggeredCount": 1,
                            "lastTriggeredDateTime": "2017-11-01T12:47:21.69Z",
                            "context": null,
                            "alertNote": null,
                            "alertConfigurations": null,
                            "associatedAlertConfigurations": null,
                            "alertHistory": null,
                            "associatedAlertHistory": null,
                            "node": null,
                            "associatedNode": null,
                            "alertActive": null,
                            "associatedAlertActive": null,
                            "displayName": null,
                            "description": null,
                            "instanceType": "Orion.AlertObjects",
                            "uri": null,
                            "instanceSiteId": 0,
                            "orionSite": null,
                            "associatedOrionSite": null
                        }],
                    "alertActiveObjects": null,
                    "associatedAlertActiveObjects": null,
                    "displayName": null,
                    "description": null,
                    "instanceType": "Orion.AlertActive",
                    "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.AlertActive/AlertActiveID=25136,AlertObjectID=2",
                    "instanceSiteId": 0,
                    "orionSite": null,
                    "associatedOrionSite": null
                },
                "hints": [{
                        "displayName": "QA-BRN-ZDYR-04",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=2"
                    }, {
                        "displayName": "DEV-BRN-MKUN-01",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=4"
                    }, {
                        "displayName": "QA-BRN-WHD-09",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=5"
                    }, {
                        "displayName": "QA-BRN-JKLI-03",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=6"
                    }, {
                        "displayName": "QA-BRN-JMEL-05",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=7"
                    }, {
                        "displayName": "DEV-BRN-JTUR-03",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=8"
                    }, {
                        "displayName": "QA-BRN-MSLE-01",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=9"
                    }, {
                        "displayName": "PM-BRN-TNOV-02",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=10"
                    }, {
                        "displayName": "DESKTOP-1EV4JJ4",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=11"
                    }],
                "moreHints": 0,
                "detailUrl": "/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:2&NoTip"
            }
        }
    }, {
        "identifiers": {
            "opid": "0_Orion.AlertActive_3",
            "netObject": "AAT:3",
            "uri": null,
            "entityType": "Orion.AlertActive",
            "instanceSiteId": 0
        },
        "title": "Active Global Alert | 29 Nodes",
        "status": null,
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:3&NoTip",
                "permissionRequired": null,
                "subItems": null
            }],
        "data": [],
        "extensionData": {
            "activeAlert": {
                "alert": {
                    "alertActiveID": 25136,
                    "alertObjectID": 3,
                    "acknowledged": null,
                    "acknowledgedBy": null,
                    "acknowledgedDateTime": null,
                    "acknowledgedNote": null,
                    "triggeredDateTime": "2017-11-01T12:47:21.487Z",
                    "triggeredMessage": "Overly Overwhelmed was triggered.",
                    "numberOfNotes": null,
                    "lastExecutedEscalationLevel": null,
                    "alertObjects": null,
                    "associatedAlertObjects": [{
                            "alertObjectID": 3,
                            "alertID": 2,
                            "entityUri": null,
                            "entityType": "Orion.Nodes",
                            "entityCaption": "29 Nodes",
                            "entityDetailsUrl": null,
                            "entityNetObjectId": "",
                            "relatedNodeUri": "",
                            "relatedNodeId": null,
                            "relatedNodeDetailsUrl": "",
                            "relatedNodeCaption": "",
                            "realEntityUri": null,
                            "realEntityType": "Orion.Nodes",
                            "triggeredCount": 1,
                            "lastTriggeredDateTime": "2017-11-01T12:47:21.69Z",
                            "context": null,
                            "alertNote": null,
                            "alertConfigurations": null,
                            "associatedAlertConfigurations": null,
                            "alertHistory": null,
                            "associatedAlertHistory": null,
                            "node": null,
                            "associatedNode": null,
                            "alertActive": null,
                            "associatedAlertActive": null,
                            "displayName": null,
                            "description": null,
                            "instanceType": "Orion.AlertObjects",
                            "uri": null,
                            "instanceSiteId": 0,
                            "orionSite": null,
                            "associatedOrionSite": null
                        }],
                    "alertActiveObjects": null,
                    "associatedAlertActiveObjects": null,
                    "displayName": null,
                    "description": null,
                    "instanceType": "Orion.AlertActive",
                    "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.AlertActive/AlertActiveID=25136,AlertObjectID=3",
                    "instanceSiteId": 0,
                    "orionSite": null,
                    "associatedOrionSite": null
                },
                "hints": [{
                        "displayName": "QA-BRN-ZDYR-04",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=2"
                    }, {
                        "displayName": "DEV-BRN-MKUN-01",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=4"
                    }, {
                        "displayName": "QA-BRN-WHD-09",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=5"
                    }, {
                        "displayName": "QA-BRN-JKLI-03",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=6"
                    }, {
                        "displayName": "QA-BRN-JMEL-05",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=7"
                    }, {
                        "displayName": "DEV-BRN-JTUR-03",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=8"
                    }, {
                        "displayName": "QA-BRN-MSLE-01",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=9"
                    }, {
                        "displayName": "PM-BRN-TNOV-02",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=10"
                    }, {
                        "displayName": "DESKTOP-1EV4JJ4",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=11"
                    }, {
                        "displayName": "QA-BRN-JPSO-02",
                        "entityType": "Orion.Nodes",
                        "status": 2,
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=12"
                    }],
                "moreHints": 19,
                "detailUrl": "/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:3&NoTip"
            }
        }
    }, {
        "identifiers": {
            "opid": "0_Orion.NPM.Interfaces_1",
            "netObject": "I:1",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=3/Interfaces/InterfaceID=1",
            "entityType": "Orion.NPM.Interfaces",
            "instanceSiteId": 0
        },
        "title": "Software Loopback Interface 1 · Loopback Pseudo-Interface 1",
        "status": "12",
        "childStatus": null,
        "noTip": null,
        "menu": [{
                "title": "Go To Details",
                "action": null,
                "url": "/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:1&NoTip&view=InterfaceDetails",
                "permissionRequired": null,
                "subItems": null
            }, {
                "title": "Edit Interface",
                "action": null,
                "url": "/Orion/Interfaces/InterfaceProperties.aspx?Interfaces=1",
                "permissionRequired": null,
                "subItems": null
            }, {
                "title": "Mute Alerts Now",
                "action": {
                    "name": "swAlertSuppressionService",
                    "method": "SuppressAlerts",
                    "params": {
                        "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.Nodes/NodeID=3/Interfaces/InterfaceID=1"
                    }
                },
                "url": null,
                "permissionRequired": "alertManagement",
                "subItems": null
            }, {
                "title": "Unmanage Now",
                "action": {
                    "name": "swEntityManagementService",
                    "method": "Unmanage",
                    "params": {
                        "entityType": "Orion.NPM.Interfaces",
                        "entityId": 1
                    }
                },
                "url": null,
                "permissionRequired": "unmanage",
                "subItems": null
            }],
        "data": [{
                "label": "Received Percent Utilization",
                "type": "System.Single",
                "value": 0.0,
                "unit": "%",
                "color": "ok",
                "cp": false
            }, {
                "label": "Transmit Percent Utilization",
                "type": "System.Single",
                "value": 0.0,
                "unit": "%",
                "color": "ok",
                "cp": false
            }, {
                "label": "Operational Status",
                "type": "System.Int16",
                "value": "Unknown",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Administrative Status",
                "type": "System.Int16",
                "value": "Unknown",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Interface Type Description",
                "type": "System.String",
                "value": "Loopback",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Transmit bps",
                "type": "System.Single",
                "value": 0,
                "unit": "bps",
                "color": null,
                "cp": false
            }, {
                "label": "Received bps",
                "type": "System.Single",
                "value": 0,
                "unit": "bps",
                "color": null,
                "cp": false
            }],
        "extensionData": {}
    }, {
        "identifiers": {
            "opid": "0_Orion.NPM.VSANs_1",
            "netObject": "NVS:1",
            "uri": "swis://EZEL-BRN-TEST1.swdev.local/Orion/Orion.NPM.VSANs/ID=1",
            "entityType": "Orion.NPM.VSANs",
            "instanceSiteId": 0
        },
        "title": "Local_Host_VSAN",
        "status": "2",
        "childStatus": null,
        "noTip": null,
        "menu": [],
        "data": [{
                "label": "Admin State Description",
                "type": "System.String",
                "value": "Active",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Media Type Description",
                "type": "System.String",
                "value": "FibreChannel",
                "unit": "",
                "color": null,
                "cp": false
            }, {
                "label": "Load Balancing Type Description",
                "type": "System.String",
                "value": "srcIdDestIdOxId",
                "unit": "",
                "color": null,
                "cp": false
            }],
        "extensionData": {}
    }
];
// tslint:enable:whitespace
exports.FakeTooltipData = {
    urlMatcher: /\/api2\/tooltip\/data\/(\d+)\/([a-zA-Z\d.]+)\/(\d+)\/([a-zA-Z\d]+)/,
    dataMatcher: function (matches) {
        var siteId = matches[1], entityType = matches[2], entityId = matches[3], context = matches[4];
        var siteIdNum = Number(siteId);
        return function (item) {
            return item.identifiers.instanceSiteId === siteIdNum
                && (item.identifiers.netObject === entityType + ":" + entityId
                    || item.identifiers.opid === siteId + "_" + entityType + "_" + entityId);
        };
    },
    id: function (item) { return "tooltip-data|" + (item && item.identifiers.opid); },
    data: dataTooltipData
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFrZS1kYXRhLXRvb2x0aXAtZGF0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZha2UtZGF0YS10b29sdGlwLWRhdGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQSxpQ0FBaUM7QUFDakMsSUFBTSxlQUFlLEdBQTBCO0lBQzNDO1FBQ0ksYUFBYSxFQUFFO1lBQ1gsTUFBTSxFQUFFLGlCQUFpQjtZQUN6QixXQUFXLEVBQUUsS0FBSztZQUNsQixLQUFLLEVBQUUsOERBQThEO1lBQ3JFLFlBQVksRUFBRSxhQUFhO1lBQzNCLGdCQUFnQixFQUFFLENBQUM7U0FDdEI7UUFDRCxPQUFPLEVBQUUsZ0JBQWdCO1FBQ3pCLFFBQVEsRUFBRSxHQUFHO1FBQ2IsYUFBYSxFQUFFLElBQUk7UUFDbkIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsZUFBZTtnQkFDeEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLHdEQUF3RDtnQkFDL0Qsb0JBQW9CLEVBQUUsSUFBSTtnQkFDMUIsVUFBVSxFQUFFLElBQUk7YUFDbkIsRUFBRTtnQkFDQyxPQUFPLEVBQUUsV0FBVztnQkFDcEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLDBDQUEwQztnQkFDakQsb0JBQW9CLEVBQUUsZ0JBQWdCO2dCQUN0QyxVQUFVLEVBQUUsSUFBSTthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLFFBQVEsRUFBRTtvQkFDTixNQUFNLEVBQUUsMkJBQTJCO29CQUNuQyxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixRQUFRLEVBQUU7d0JBQ04sS0FBSyxFQUFFLDhEQUE4RDtxQkFDeEU7aUJBQ0o7Z0JBQ0QsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsb0JBQW9CLEVBQUUsaUJBQWlCO2dCQUN2QyxVQUFVLEVBQUUsSUFBSTthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixRQUFRLEVBQUU7b0JBQ04sTUFBTSxFQUFFLDJCQUEyQjtvQkFDbkMsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLFFBQVEsRUFBRTt3QkFDTixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsVUFBVSxFQUFFLENBQUM7cUJBQ2hCO2lCQUNKO2dCQUNELEtBQUssRUFBRSxJQUFJO2dCQUNYLG9CQUFvQixFQUFFLFVBQVU7Z0JBQ2hDLFVBQVUsRUFBRSxJQUFJO2FBQ25CLENBQUM7UUFDRixNQUFNLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsWUFBWTtnQkFDckIsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxhQUFhO2dCQUN0QixNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLGNBQWM7Z0JBQ3ZCLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsdUJBQXVCO2dCQUNoQyxNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLHVCQUF1QjtnQkFDaEMsTUFBTSxFQUFFLGNBQWM7Z0JBQ3RCLE9BQU8sRUFBRSxDQUFDLENBQUM7Z0JBQ1gsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsTUFBTSxFQUFFLEdBQUc7Z0JBQ1gsT0FBTyxFQUFFLFVBQVU7Z0JBQ25CLElBQUksRUFBRSxLQUFLO2FBQ2QsRUFBRTtnQkFDQyxPQUFPLEVBQUUscUJBQXFCO2dCQUM5QixNQUFNLEVBQUUsY0FBYztnQkFDdEIsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsTUFBTSxFQUFFLEdBQUc7Z0JBQ1gsT0FBTyxFQUFFLFVBQVU7Z0JBQ25CLElBQUksRUFBRSxLQUFLO2FBQ2QsRUFBRTtnQkFDQyxPQUFPLEVBQUUsTUFBTTtnQkFDZixNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLElBQUk7YUFDYixFQUFFO2dCQUNDLE9BQU8sRUFBRSxVQUFVO2dCQUNuQixNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLElBQUk7YUFDYixFQUFFO2dCQUNDLE9BQU8sRUFBRSxZQUFZO2dCQUNyQixNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLElBQUk7YUFDYixFQUFFO2dCQUNDLE9BQU8sRUFBRSxrRUFBa0U7Z0JBQzNFLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsZUFBZTtnQkFDeEIsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLElBQUk7YUFDYixFQUFFO2dCQUNDLE9BQU8sRUFBRSxnQkFBZ0I7Z0JBQ3pCLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsdUhBQXVIO2dCQUNoSSxNQUFNLEVBQUUsSUFBSTtnQkFDWixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsSUFBSTthQUNiLENBQUM7UUFDRixlQUFlLEVBQUUsRUFBRTtLQUN0QixFQUFFO1FBQ0MsYUFBYSxFQUFFO1lBQ1gsTUFBTSxFQUFFLGlCQUFpQjtZQUN6QixXQUFXLEVBQUUsS0FBSztZQUNsQixLQUFLLEVBQUUsOERBQThEO1lBQ3JFLFlBQVksRUFBRSxhQUFhO1lBQzNCLGdCQUFnQixFQUFFLENBQUM7U0FDdEI7UUFDRCxPQUFPLEVBQUUsZ0JBQWdCO1FBQ3pCLFFBQVEsRUFBRSxHQUFHO1FBQ2IsYUFBYSxFQUFFLElBQUk7UUFDbkIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsZUFBZTtnQkFDeEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLHdEQUF3RDtnQkFDL0Qsb0JBQW9CLEVBQUUsSUFBSTtnQkFDMUIsVUFBVSxFQUFFLElBQUk7YUFDbkIsRUFBRTtnQkFDQyxPQUFPLEVBQUUsV0FBVztnQkFDcEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLDBDQUEwQztnQkFDakQsb0JBQW9CLEVBQUUsZ0JBQWdCO2dCQUN0QyxVQUFVLEVBQUUsSUFBSTthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLFFBQVEsRUFBRTtvQkFDTixNQUFNLEVBQUUsMkJBQTJCO29CQUNuQyxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixRQUFRLEVBQUU7d0JBQ04sS0FBSyxFQUFFLDhEQUE4RDtxQkFDeEU7aUJBQ0o7Z0JBQ0QsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsb0JBQW9CLEVBQUUsaUJBQWlCO2dCQUN2QyxVQUFVLEVBQUUsSUFBSTthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixRQUFRLEVBQUU7b0JBQ04sTUFBTSxFQUFFLDJCQUEyQjtvQkFDbkMsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLFFBQVEsRUFBRTt3QkFDTixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsVUFBVSxFQUFFLENBQUM7cUJBQ2hCO2lCQUNKO2dCQUNELEtBQUssRUFBRSxJQUFJO2dCQUNYLG9CQUFvQixFQUFFLFVBQVU7Z0JBQ2hDLFVBQVUsRUFBRSxJQUFJO2FBQ25CLENBQUM7UUFDRixNQUFNLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsWUFBWTtnQkFDckIsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxhQUFhO2dCQUN0QixNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLGNBQWM7Z0JBQ3ZCLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsd0JBQXdCO2dCQUNqQyxNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLHVCQUF1QjtnQkFDaEMsTUFBTSxFQUFFLGNBQWM7Z0JBQ3RCLE9BQU8sRUFBRSxDQUFDLENBQUM7Z0JBQ1gsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsTUFBTSxFQUFFLEdBQUc7Z0JBQ1gsT0FBTyxFQUFFLFVBQVU7Z0JBQ25CLElBQUksRUFBRSxLQUFLO2FBQ2QsRUFBRTtnQkFDQyxPQUFPLEVBQUUsVUFBVTtnQkFDbkIsTUFBTSxFQUFFLGNBQWM7Z0JBQ3RCLE9BQU8sRUFBRSxDQUFDO2dCQUNWLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxTQUFTO2dCQUNsQixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLHFCQUFxQjtnQkFDOUIsTUFBTSxFQUFFLGNBQWM7Z0JBQ3RCLE9BQU8sRUFBRSxFQUFFO2dCQUNYLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxVQUFVO2dCQUNuQixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLE1BQU07Z0JBQ2YsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxJQUFJO2dCQUNiLE1BQU0sRUFBRSxJQUFJO2dCQUNaLE9BQU8sRUFBRSxJQUFJO2dCQUNiLElBQUksRUFBRSxJQUFJO2FBQ2IsRUFBRTtnQkFDQyxPQUFPLEVBQUUsVUFBVTtnQkFDbkIsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxJQUFJO2dCQUNiLE1BQU0sRUFBRSxJQUFJO2dCQUNaLE9BQU8sRUFBRSxJQUFJO2dCQUNiLElBQUksRUFBRSxJQUFJO2FBQ2IsRUFBRTtnQkFDQyxPQUFPLEVBQUUsWUFBWTtnQkFDckIsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxJQUFJO2dCQUNiLE1BQU0sRUFBRSxJQUFJO2dCQUNaLE9BQU8sRUFBRSxJQUFJO2dCQUNiLElBQUksRUFBRSxJQUFJO2FBQ2IsQ0FBQztRQUNGLGVBQWUsRUFBRSxFQUFFO0tBQ3RCLEVBQUU7UUFDQyxhQUFhLEVBQUU7WUFDWCxNQUFNLEVBQUUsbUJBQW1CO1lBQzNCLFdBQVcsRUFBRSxLQUFLO1lBQ2xCLEtBQUssRUFBRSxpRkFBaUY7WUFDeEYsWUFBWSxFQUFFLGVBQWU7WUFDN0IsZ0JBQWdCLEVBQUUsQ0FBQztTQUN0QjtRQUNELE9BQU8sRUFBRSxpQkFBaUI7UUFDMUIsUUFBUSxFQUFFLElBQUk7UUFDZCxhQUFhLEVBQUUsSUFBSTtRQUNuQixPQUFPLEVBQUUsSUFBSTtRQUNiLE1BQU0sRUFBRSxFQUFFO1FBQ1YsTUFBTSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLFNBQVM7Z0JBQ2xCLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLGFBQWE7Z0JBQ3RCLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsS0FBSztnQkFDZCxNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsS0FBSzthQUNkLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLE1BQU07Z0JBQ2YsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxJQUFJO2dCQUNiLE1BQU0sRUFBRSxJQUFJO2dCQUNaLE9BQU8sRUFBRSxJQUFJO2dCQUNiLElBQUksRUFBRSxLQUFLO2FBQ2QsRUFBRTtnQkFDQyxPQUFPLEVBQUUsY0FBYztnQkFDdkIsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxFQUFFO2dCQUNYLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxJQUFJO2dCQUNiLElBQUksRUFBRSxLQUFLO2FBQ2QsQ0FBQztRQUNGLGVBQWUsRUFBRSxFQUFFO0tBQ3RCLEVBQUU7UUFDQyxhQUFhLEVBQUU7WUFDWCxNQUFNLEVBQUUsa0JBQWtCO1lBQzFCLFdBQVcsRUFBRSxLQUFLO1lBQ2xCLEtBQUssRUFBRSxvRUFBb0U7WUFDM0UsWUFBWSxFQUFFLGNBQWM7WUFDNUIsZ0JBQWdCLEVBQUUsQ0FBQztTQUN0QjtRQUNELE9BQU8sRUFBRSxnQkFBZ0I7UUFDekIsUUFBUSxFQUFFLEdBQUc7UUFDYixhQUFhLEVBQUUsSUFBSTtRQUNuQixPQUFPLEVBQUUsSUFBSTtRQUNiLE1BQU0sRUFBRSxDQUFDO2dCQUNMLE9BQU8sRUFBRSxlQUFlO2dCQUN4QixRQUFRLEVBQUUsSUFBSTtnQkFDZCxLQUFLLEVBQUUsNkRBQTZEO2dCQUNwRSxvQkFBb0IsRUFBRSxJQUFJO2dCQUMxQixVQUFVLEVBQUUsSUFBSTthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxZQUFZO2dCQUNyQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxLQUFLLEVBQUUsaURBQWlEO2dCQUN4RCxvQkFBb0IsRUFBRSxnQkFBZ0I7Z0JBQ3RDLFVBQVUsRUFBRSxJQUFJO2FBQ25CLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLGlCQUFpQjtnQkFDMUIsUUFBUSxFQUFFO29CQUNOLE1BQU0sRUFBRSwyQkFBMkI7b0JBQ25DLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLFFBQVEsRUFBRTt3QkFDTixLQUFLLEVBQUUsb0VBQW9FO3FCQUM5RTtpQkFDSjtnQkFDRCxLQUFLLEVBQUUsSUFBSTtnQkFDWCxvQkFBb0IsRUFBRSxpQkFBaUI7Z0JBQ3ZDLFVBQVUsRUFBRSxJQUFJO2FBQ25CLENBQUM7UUFDRixNQUFNLEVBQUUsRUFBRTtRQUNWLGVBQWUsRUFBRTtZQUNiLFdBQVcsRUFBRTtnQkFDVCxTQUFTLEVBQUUsRUFBRTtnQkFDYixhQUFhLEVBQUUsQ0FBQztnQkFDaEIsV0FBVyxFQUFFLDZEQUE2RDthQUM3RTtTQUNKO0tBQ0osRUFBRTtRQUNDLGFBQWEsRUFBRTtZQUNYLE1BQU0sRUFBRSxrQkFBa0I7WUFDMUIsV0FBVyxFQUFFLEtBQUs7WUFDbEIsS0FBSyxFQUFFLG9FQUFvRTtZQUMzRSxZQUFZLEVBQUUsY0FBYztZQUM1QixnQkFBZ0IsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsT0FBTyxFQUFFLHVCQUF1QjtRQUNoQyxRQUFRLEVBQUUsR0FBRztRQUNiLGFBQWEsRUFBRSxJQUFJO1FBQ25CLE9BQU8sRUFBRSxJQUFJO1FBQ2IsTUFBTSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLGVBQWU7Z0JBQ3hCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLEtBQUssRUFBRSw2REFBNkQ7Z0JBQ3BFLG9CQUFvQixFQUFFLElBQUk7Z0JBQzFCLFVBQVUsRUFBRSxJQUFJO2FBQ25CLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLFlBQVk7Z0JBQ3JCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLEtBQUssRUFBRSxpREFBaUQ7Z0JBQ3hELG9CQUFvQixFQUFFLGdCQUFnQjtnQkFDdEMsVUFBVSxFQUFFLElBQUk7YUFDbkIsRUFBRTtnQkFDQyxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixRQUFRLEVBQUU7b0JBQ04sTUFBTSxFQUFFLDJCQUEyQjtvQkFDbkMsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsUUFBUSxFQUFFO3dCQUNOLEtBQUssRUFBRSxvRUFBb0U7cUJBQzlFO2lCQUNKO2dCQUNELEtBQUssRUFBRSxJQUFJO2dCQUNYLG9CQUFvQixFQUFFLGlCQUFpQjtnQkFDdkMsVUFBVSxFQUFFLElBQUk7YUFDbkIsQ0FBQztRQUNGLE1BQU0sRUFBRSxFQUFFO1FBQ1YsZUFBZSxFQUFFO1lBQ2IsV0FBVyxFQUFFO2dCQUNULFNBQVMsRUFBRSxDQUFDO3dCQUNSLGFBQWEsRUFBRSxvRUFBb0U7d0JBQ25GLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxFQUFFO3dCQUNDLGFBQWEsRUFBRSx3RkFBd0Y7d0JBQ3ZHLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsK0hBQStIO3FCQUN6SSxDQUFDO2dCQUNGLGFBQWEsRUFBRSxDQUFDO2dCQUNoQixXQUFXLEVBQUUsNkRBQTZEO2FBQzdFO1NBQ0o7S0FDSixFQUFFO1FBQ0MsYUFBYSxFQUFFO1lBQ1gsTUFBTSxFQUFFLGtCQUFrQjtZQUMxQixXQUFXLEVBQUUsS0FBSztZQUNsQixLQUFLLEVBQUUsb0VBQW9FO1lBQzNFLFlBQVksRUFBRSxjQUFjO1lBQzVCLGdCQUFnQixFQUFFLENBQUM7U0FDdEI7UUFDRCxPQUFPLEVBQUUsMEJBQTBCO1FBQ25DLFFBQVEsRUFBRSxHQUFHO1FBQ2IsYUFBYSxFQUFFLElBQUk7UUFDbkIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsZUFBZTtnQkFDeEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLDZEQUE2RDtnQkFDcEUsb0JBQW9CLEVBQUUsSUFBSTtnQkFDMUIsVUFBVSxFQUFFLElBQUk7YUFDbkIsRUFBRTtnQkFDQyxPQUFPLEVBQUUsWUFBWTtnQkFDckIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLGlEQUFpRDtnQkFDeEQsb0JBQW9CLEVBQUUsZ0JBQWdCO2dCQUN0QyxVQUFVLEVBQUUsSUFBSTthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLFFBQVEsRUFBRTtvQkFDTixNQUFNLEVBQUUsMkJBQTJCO29CQUNuQyxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixRQUFRLEVBQUU7d0JBQ04sS0FBSyxFQUFFLG9FQUFvRTtxQkFDOUU7aUJBQ0o7Z0JBQ0QsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsb0JBQW9CLEVBQUUsaUJBQWlCO2dCQUN2QyxVQUFVLEVBQUUsSUFBSTthQUNuQixDQUFDO1FBQ0YsTUFBTSxFQUFFLEVBQUU7UUFDVixlQUFlLEVBQUU7WUFDYixXQUFXLEVBQUU7Z0JBQ1QsU0FBUyxFQUFFLENBQUM7d0JBQ1IsYUFBYSxFQUFFLGlCQUFpQjt3QkFDaEMsWUFBWSxFQUFFLGFBQWE7d0JBQzNCLFFBQVEsRUFBRSxDQUFDO3dCQUNYLEtBQUssRUFBRSxnSUFBZ0k7cUJBQzFJLEVBQUU7d0JBQ0MsYUFBYSxFQUFFLGVBQWU7d0JBQzlCLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsK0hBQStIO3FCQUN6SSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsK0hBQStIO3FCQUN6SSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsZ0lBQWdJO3FCQUMxSSxDQUFDO2dCQUNGLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsNkRBQTZEO2FBQzdFO1NBQ0o7S0FDSixFQUFFO1FBQ0MsYUFBYSxFQUFFO1lBQ1gsTUFBTSxFQUFFLHVCQUF1QjtZQUMvQixXQUFXLEVBQUUsT0FBTztZQUNwQixLQUFLLEVBQUUsMkZBQTJGO1lBQ2xHLFlBQVksRUFBRSxtQkFBbUI7WUFDakMsZ0JBQWdCLEVBQUUsQ0FBQztTQUN0QjtRQUNELE9BQU8sRUFBRSxPQUFPO1FBQ2hCLFFBQVEsRUFBRSxJQUFJO1FBQ2QsYUFBYSxFQUFFLElBQUk7UUFDbkIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsRUFBRTtRQUNWLE1BQU0sRUFBRSxFQUFFO1FBQ1YsZUFBZSxFQUFFLEVBQUU7S0FDdEIsRUFBRTtRQUNDLGFBQWEsRUFBRTtZQUNYLE1BQU0sRUFBRSx1QkFBdUI7WUFDL0IsV0FBVyxFQUFFLE9BQU87WUFDcEIsS0FBSyxFQUFFLElBQUk7WUFDWCxZQUFZLEVBQUUsbUJBQW1CO1lBQ2pDLGdCQUFnQixFQUFFLENBQUM7U0FDdEI7UUFDRCxPQUFPLEVBQUUsK0JBQStCO1FBQ3hDLFFBQVEsRUFBRSxJQUFJO1FBQ2QsYUFBYSxFQUFFLElBQUk7UUFDbkIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsZUFBZTtnQkFDeEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLGlFQUFpRTtnQkFDeEUsb0JBQW9CLEVBQUUsSUFBSTtnQkFDMUIsVUFBVSxFQUFFLElBQUk7YUFDbkIsQ0FBQztRQUNGLE1BQU0sRUFBRSxFQUFFO1FBQ1YsZUFBZSxFQUFFO1lBQ2IsYUFBYSxFQUFFO2dCQUNYLE9BQU8sRUFBRTtvQkFDTCxlQUFlLEVBQUUsS0FBSztvQkFDdEIsZUFBZSxFQUFFLENBQUM7b0JBQ2xCLGNBQWMsRUFBRSxJQUFJO29CQUNwQixnQkFBZ0IsRUFBRSxJQUFJO29CQUN0QixzQkFBc0IsRUFBRSxJQUFJO29CQUM1QixrQkFBa0IsRUFBRSxJQUFJO29CQUN4QixtQkFBbUIsRUFBRSwwQkFBMEI7b0JBQy9DLGtCQUFrQixFQUFFLG1DQUFtQztvQkFDdkQsZUFBZSxFQUFFLElBQUk7b0JBQ3JCLDZCQUE2QixFQUFFLElBQUk7b0JBQ25DLGNBQWMsRUFBRSxJQUFJO29CQUNwQix3QkFBd0IsRUFBRSxDQUFDOzRCQUN2QixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsU0FBUyxFQUFFLENBQUM7NEJBQ1osV0FBVyxFQUFFLElBQUk7NEJBQ2pCLFlBQVksRUFBRSxhQUFhOzRCQUMzQixlQUFlLEVBQUUsU0FBUzs0QkFDMUIsa0JBQWtCLEVBQUUsSUFBSTs0QkFDeEIsbUJBQW1CLEVBQUUsRUFBRTs0QkFDdkIsZ0JBQWdCLEVBQUUsRUFBRTs0QkFDcEIsZUFBZSxFQUFFLElBQUk7NEJBQ3JCLHVCQUF1QixFQUFFLEVBQUU7NEJBQzNCLG9CQUFvQixFQUFFLEVBQUU7NEJBQ3hCLGVBQWUsRUFBRSxJQUFJOzRCQUNyQixnQkFBZ0IsRUFBRSxhQUFhOzRCQUMvQixnQkFBZ0IsRUFBRSxDQUFDOzRCQUNuQix1QkFBdUIsRUFBRSx5QkFBeUI7NEJBQ2xELFNBQVMsRUFBRSxJQUFJOzRCQUNmLFdBQVcsRUFBRSxJQUFJOzRCQUNqQixxQkFBcUIsRUFBRSxJQUFJOzRCQUMzQiwrQkFBK0IsRUFBRSxJQUFJOzRCQUNyQyxjQUFjLEVBQUUsSUFBSTs0QkFDcEIsd0JBQXdCLEVBQUUsSUFBSTs0QkFDOUIsTUFBTSxFQUFFLElBQUk7NEJBQ1osZ0JBQWdCLEVBQUUsSUFBSTs0QkFDdEIsYUFBYSxFQUFFLElBQUk7NEJBQ25CLHVCQUF1QixFQUFFLElBQUk7NEJBQzdCLGFBQWEsRUFBRSxJQUFJOzRCQUNuQixhQUFhLEVBQUUsSUFBSTs0QkFDbkIsY0FBYyxFQUFFLG9CQUFvQjs0QkFDcEMsS0FBSyxFQUFFLElBQUk7NEJBQ1gsZ0JBQWdCLEVBQUUsQ0FBQzs0QkFDbkIsV0FBVyxFQUFFLElBQUk7NEJBQ2pCLHFCQUFxQixFQUFFLElBQUk7eUJBQzlCLENBQUM7b0JBQ0Ysb0JBQW9CLEVBQUUsSUFBSTtvQkFDMUIsOEJBQThCLEVBQUUsSUFBSTtvQkFDcEMsYUFBYSxFQUFFLElBQUk7b0JBQ25CLGFBQWEsRUFBRSxJQUFJO29CQUNuQixjQUFjLEVBQUUsbUJBQW1CO29CQUNuQyxLQUFLLEVBQUUsK0ZBQStGO29CQUN0RyxnQkFBZ0IsRUFBRSxDQUFDO29CQUNuQixXQUFXLEVBQUUsSUFBSTtvQkFDakIscUJBQXFCLEVBQUUsSUFBSTtpQkFDOUI7Z0JBQ0QsT0FBTyxFQUFFLENBQUM7d0JBQ04sYUFBYSxFQUFFLGdCQUFnQjt3QkFDL0IsWUFBWSxFQUFFLGFBQWE7d0JBQzNCLFFBQVEsRUFBRSxDQUFDO3dCQUNYLEtBQUssRUFBRSw4REFBOEQ7cUJBQ3hFLEVBQUU7d0JBQ0MsYUFBYSxFQUFFLGlCQUFpQjt3QkFDaEMsWUFBWSxFQUFFLGFBQWE7d0JBQzNCLFFBQVEsRUFBRSxDQUFDO3dCQUNYLEtBQUssRUFBRSw4REFBOEQ7cUJBQ3hFLEVBQUU7d0JBQ0MsYUFBYSxFQUFFLGVBQWU7d0JBQzlCLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsOERBQThEO3FCQUN4RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxnQkFBZ0I7d0JBQy9CLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsOERBQThEO3FCQUN4RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxnQkFBZ0I7d0JBQy9CLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsOERBQThEO3FCQUN4RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsOERBQThEO3FCQUN4RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxnQkFBZ0I7d0JBQy9CLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsOERBQThEO3FCQUN4RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxnQkFBZ0I7d0JBQy9CLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsK0RBQStEO3FCQUN6RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsK0RBQStEO3FCQUN6RSxDQUFDO2dCQUNGLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFdBQVcsRUFBRSxpRUFBaUU7YUFDakY7U0FDSjtLQUNKLEVBQUU7UUFDQyxhQUFhLEVBQUU7WUFDWCxNQUFNLEVBQUUsdUJBQXVCO1lBQy9CLFdBQVcsRUFBRSxPQUFPO1lBQ3BCLEtBQUssRUFBRSxJQUFJO1lBQ1gsWUFBWSxFQUFFLG1CQUFtQjtZQUNqQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsT0FBTyxFQUFFLGdDQUFnQztRQUN6QyxRQUFRLEVBQUUsSUFBSTtRQUNkLGFBQWEsRUFBRSxJQUFJO1FBQ25CLE9BQU8sRUFBRSxJQUFJO1FBQ2IsTUFBTSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLGVBQWU7Z0JBQ3hCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLEtBQUssRUFBRSxpRUFBaUU7Z0JBQ3hFLG9CQUFvQixFQUFFLElBQUk7Z0JBQzFCLFVBQVUsRUFBRSxJQUFJO2FBQ25CLENBQUM7UUFDRixNQUFNLEVBQUUsRUFBRTtRQUNWLGVBQWUsRUFBRTtZQUNiLGFBQWEsRUFBRTtnQkFDWCxPQUFPLEVBQUU7b0JBQ0wsZUFBZSxFQUFFLEtBQUs7b0JBQ3RCLGVBQWUsRUFBRSxDQUFDO29CQUNsQixjQUFjLEVBQUUsSUFBSTtvQkFDcEIsZ0JBQWdCLEVBQUUsSUFBSTtvQkFDdEIsc0JBQXNCLEVBQUUsSUFBSTtvQkFDNUIsa0JBQWtCLEVBQUUsSUFBSTtvQkFDeEIsbUJBQW1CLEVBQUUsMEJBQTBCO29CQUMvQyxrQkFBa0IsRUFBRSxtQ0FBbUM7b0JBQ3ZELGVBQWUsRUFBRSxJQUFJO29CQUNyQiw2QkFBNkIsRUFBRSxJQUFJO29CQUNuQyxjQUFjLEVBQUUsSUFBSTtvQkFDcEIsd0JBQXdCLEVBQUUsQ0FBQzs0QkFDdkIsZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFNBQVMsRUFBRSxDQUFDOzRCQUNaLFdBQVcsRUFBRSxJQUFJOzRCQUNqQixZQUFZLEVBQUUsYUFBYTs0QkFDM0IsZUFBZSxFQUFFLFVBQVU7NEJBQzNCLGtCQUFrQixFQUFFLElBQUk7NEJBQ3hCLG1CQUFtQixFQUFFLEVBQUU7NEJBQ3ZCLGdCQUFnQixFQUFFLEVBQUU7NEJBQ3BCLGVBQWUsRUFBRSxJQUFJOzRCQUNyQix1QkFBdUIsRUFBRSxFQUFFOzRCQUMzQixvQkFBb0IsRUFBRSxFQUFFOzRCQUN4QixlQUFlLEVBQUUsSUFBSTs0QkFDckIsZ0JBQWdCLEVBQUUsYUFBYTs0QkFDL0IsZ0JBQWdCLEVBQUUsQ0FBQzs0QkFDbkIsdUJBQXVCLEVBQUUseUJBQXlCOzRCQUNsRCxTQUFTLEVBQUUsSUFBSTs0QkFDZixXQUFXLEVBQUUsSUFBSTs0QkFDakIscUJBQXFCLEVBQUUsSUFBSTs0QkFDM0IsK0JBQStCLEVBQUUsSUFBSTs0QkFDckMsY0FBYyxFQUFFLElBQUk7NEJBQ3BCLHdCQUF3QixFQUFFLElBQUk7NEJBQzlCLE1BQU0sRUFBRSxJQUFJOzRCQUNaLGdCQUFnQixFQUFFLElBQUk7NEJBQ3RCLGFBQWEsRUFBRSxJQUFJOzRCQUNuQix1QkFBdUIsRUFBRSxJQUFJOzRCQUM3QixhQUFhLEVBQUUsSUFBSTs0QkFDbkIsYUFBYSxFQUFFLElBQUk7NEJBQ25CLGNBQWMsRUFBRSxvQkFBb0I7NEJBQ3BDLEtBQUssRUFBRSxJQUFJOzRCQUNYLGdCQUFnQixFQUFFLENBQUM7NEJBQ25CLFdBQVcsRUFBRSxJQUFJOzRCQUNqQixxQkFBcUIsRUFBRSxJQUFJO3lCQUM5QixDQUFDO29CQUNGLG9CQUFvQixFQUFFLElBQUk7b0JBQzFCLDhCQUE4QixFQUFFLElBQUk7b0JBQ3BDLGFBQWEsRUFBRSxJQUFJO29CQUNuQixhQUFhLEVBQUUsSUFBSTtvQkFDbkIsY0FBYyxFQUFFLG1CQUFtQjtvQkFDbkMsS0FBSyxFQUFFLCtGQUErRjtvQkFDdEcsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDbkIsV0FBVyxFQUFFLElBQUk7b0JBQ2pCLHFCQUFxQixFQUFFLElBQUk7aUJBQzlCO2dCQUNELE9BQU8sRUFBRSxDQUFDO3dCQUNOLGFBQWEsRUFBRSxnQkFBZ0I7d0JBQy9CLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsOERBQThEO3FCQUN4RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxhQUFhO3dCQUMzQixRQUFRLEVBQUUsQ0FBQzt3QkFDWCxLQUFLLEVBQUUsOERBQThEO3FCQUN4RSxFQUFFO3dCQUNDLGFBQWEsRUFBRSxlQUFlO3dCQUM5QixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLDhEQUE4RDtxQkFDeEUsRUFBRTt3QkFDQyxhQUFhLEVBQUUsZ0JBQWdCO3dCQUMvQixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLDhEQUE4RDtxQkFDeEUsRUFBRTt3QkFDQyxhQUFhLEVBQUUsZ0JBQWdCO3dCQUMvQixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLDhEQUE4RDtxQkFDeEUsRUFBRTt3QkFDQyxhQUFhLEVBQUUsaUJBQWlCO3dCQUNoQyxZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLDhEQUE4RDtxQkFDeEUsRUFBRTt3QkFDQyxhQUFhLEVBQUUsZ0JBQWdCO3dCQUMvQixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLDhEQUE4RDtxQkFDeEUsRUFBRTt3QkFDQyxhQUFhLEVBQUUsZ0JBQWdCO3dCQUMvQixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLCtEQUErRDtxQkFDekUsRUFBRTt3QkFDQyxhQUFhLEVBQUUsaUJBQWlCO3dCQUNoQyxZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLCtEQUErRDtxQkFDekUsRUFBRTt3QkFDQyxhQUFhLEVBQUUsZ0JBQWdCO3dCQUMvQixZQUFZLEVBQUUsYUFBYTt3QkFDM0IsUUFBUSxFQUFFLENBQUM7d0JBQ1gsS0FBSyxFQUFFLCtEQUErRDtxQkFDekUsQ0FBQztnQkFDRixXQUFXLEVBQUUsRUFBRTtnQkFDZixXQUFXLEVBQUUsaUVBQWlFO2FBQ2pGO1NBQ0o7S0FDSixFQUFFO1FBQ0MsYUFBYSxFQUFFO1lBQ1gsTUFBTSxFQUFFLDBCQUEwQjtZQUNsQyxXQUFXLEVBQUUsS0FBSztZQUNsQixLQUFLLEVBQUUsdUZBQXVGO1lBQzlGLFlBQVksRUFBRSxzQkFBc0I7WUFDcEMsZ0JBQWdCLEVBQUUsQ0FBQztTQUN0QjtRQUNELE9BQU8sRUFBRSw2REFBNkQ7UUFDdEUsUUFBUSxFQUFFLElBQUk7UUFDZCxhQUFhLEVBQUUsSUFBSTtRQUNuQixPQUFPLEVBQUUsSUFBSTtRQUNiLE1BQU0sRUFBRSxDQUFDO2dCQUNMLE9BQU8sRUFBRSxlQUFlO2dCQUN4QixRQUFRLEVBQUUsSUFBSTtnQkFDZCxLQUFLLEVBQUUsbUZBQW1GO2dCQUMxRixvQkFBb0IsRUFBRSxJQUFJO2dCQUMxQixVQUFVLEVBQUUsSUFBSTthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxnQkFBZ0I7Z0JBQ3pCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLEtBQUssRUFBRSx5REFBeUQ7Z0JBQ2hFLG9CQUFvQixFQUFFLElBQUk7Z0JBQzFCLFVBQVUsRUFBRSxJQUFJO2FBQ25CLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLGlCQUFpQjtnQkFDMUIsUUFBUSxFQUFFO29CQUNOLE1BQU0sRUFBRSwyQkFBMkI7b0JBQ25DLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLFFBQVEsRUFBRTt3QkFDTixLQUFLLEVBQUUsdUZBQXVGO3FCQUNqRztpQkFDSjtnQkFDRCxLQUFLLEVBQUUsSUFBSTtnQkFDWCxvQkFBb0IsRUFBRSxpQkFBaUI7Z0JBQ3ZDLFVBQVUsRUFBRSxJQUFJO2FBQ25CLEVBQUU7Z0JBQ0MsT0FBTyxFQUFFLGNBQWM7Z0JBQ3ZCLFFBQVEsRUFBRTtvQkFDTixNQUFNLEVBQUUsMkJBQTJCO29CQUNuQyxRQUFRLEVBQUUsVUFBVTtvQkFDcEIsUUFBUSxFQUFFO3dCQUNOLFlBQVksRUFBRSxzQkFBc0I7d0JBQ3BDLFVBQVUsRUFBRSxDQUFDO3FCQUNoQjtpQkFDSjtnQkFDRCxLQUFLLEVBQUUsSUFBSTtnQkFDWCxvQkFBb0IsRUFBRSxVQUFVO2dCQUNoQyxVQUFVLEVBQUUsSUFBSTthQUNuQixDQUFDO1FBQ0YsTUFBTSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLDhCQUE4QjtnQkFDdkMsTUFBTSxFQUFFLGVBQWU7Z0JBQ3ZCLE9BQU8sRUFBRSxHQUFHO2dCQUNaLE1BQU0sRUFBRSxHQUFHO2dCQUNYLE9BQU8sRUFBRSxJQUFJO2dCQUNiLElBQUksRUFBRSxLQUFLO2FBQ2QsRUFBRTtnQkFDQyxPQUFPLEVBQUUsOEJBQThCO2dCQUN2QyxNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLEdBQUc7Z0JBQ1osTUFBTSxFQUFFLEdBQUc7Z0JBQ1gsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSxvQkFBb0I7Z0JBQzdCLE1BQU0sRUFBRSxjQUFjO2dCQUN0QixPQUFPLEVBQUUsU0FBUztnQkFDbEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSx1QkFBdUI7Z0JBQ2hDLE1BQU0sRUFBRSxjQUFjO2dCQUN0QixPQUFPLEVBQUUsU0FBUztnQkFDbEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSw0QkFBNEI7Z0JBQ3JDLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsVUFBVTtnQkFDbkIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSxjQUFjO2dCQUN2QixNQUFNLEVBQUUsZUFBZTtnQkFDdkIsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxDQUFDO1FBQ0YsZUFBZSxFQUFFLEVBQUU7S0FDdEIsRUFBRTtRQUNDLGFBQWEsRUFBRTtZQUNYLE1BQU0sRUFBRSxxQkFBcUI7WUFDN0IsV0FBVyxFQUFFLE9BQU87WUFDcEIsS0FBSyxFQUFFLDhEQUE4RDtZQUNyRSxZQUFZLEVBQUUsaUJBQWlCO1lBQy9CLGdCQUFnQixFQUFFLENBQUM7U0FDdEI7UUFDRCxPQUFPLEVBQUUsaUJBQWlCO1FBQzFCLFFBQVEsRUFBRSxHQUFHO1FBQ2IsYUFBYSxFQUFFLElBQUk7UUFDbkIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsRUFBRTtRQUNWLE1BQU0sRUFBRSxDQUFDO2dCQUNMLE9BQU8sRUFBRSx5QkFBeUI7Z0JBQ2xDLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsUUFBUTtnQkFDakIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSx3QkFBd0I7Z0JBQ2pDLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsY0FBYztnQkFDdkIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7YUFDZCxFQUFFO2dCQUNDLE9BQU8sRUFBRSxpQ0FBaUM7Z0JBQzFDLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixNQUFNLEVBQUUsRUFBRTtnQkFDVixPQUFPLEVBQUUsSUFBSTtnQkFDYixJQUFJLEVBQUUsS0FBSzthQUNkLENBQUM7UUFDRixlQUFlLEVBQUUsRUFBRTtLQUN0QjtDQUNKLENBQUM7QUFDRiwyQkFBMkI7QUFFZCxRQUFBLGVBQWUsR0FBbUM7SUFDM0QsVUFBVSxFQUFFLG9FQUFvRTtJQUNoRixXQUFXLEVBQUUsVUFBQyxPQUF3QjtRQUN6QixJQUFBLG1CQUFNLEVBQUUsdUJBQVUsRUFBRSxxQkFBUSxFQUFFLG9CQUFPLENBQVk7UUFDMUQsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sQ0FBQyxVQUFDLElBQXlCO1lBQzdCLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEtBQUssU0FBUzttQkFDMUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsS0FBUSxVQUFVLFNBQUksUUFBVTt1QkFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEtBQVEsTUFBTSxTQUFJLFVBQVUsU0FBSSxRQUFVLENBQUM7UUFGdkUsQ0FFdUUsQ0FBQztJQUNoRixDQUFDO0lBQ0QsRUFBRSxFQUFFLFVBQUMsSUFBeUIsSUFBSyxPQUFBLG1CQUFnQixJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUUsRUFBL0MsQ0FBK0M7SUFDbEYsSUFBSSxFQUFFLGVBQWU7Q0FDeEIsQ0FBQyJ9

/***/ }),
/* 198 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var fakeDataProcessor_1 = __webpack_require__(199);
var FakeDataHttpInterceptor = /** @class */ (function () {
    function FakeDataHttpInterceptor(data) {
        var _this = this;
        this.request = function (request) {
            var fakeData = _this.mockData.getId(request.url);
            if (fakeData) {
                request.params[FakeDataHttpInterceptor.paramData] = fakeData;
                request.url = FakeDataHttpInterceptor.flagMock;
                return request;
            }
            return request;
        };
        this.response = function (response) {
            if (response.config.url === FakeDataHttpInterceptor.flagMock) {
                var dataId = response.config.params[FakeDataHttpInterceptor.paramData];
                response.data = _this.mockData.getData(dataId);
                if (!response.data) {
                    response.status = 404;
                    response.statusText = "Not Found";
                }
            }
            return response;
        };
        this.responseError = function (response) {
            if (response.config.url === FakeDataHttpInterceptor.flagMock) {
                var dataId = response.config.params[FakeDataHttpInterceptor.paramData];
                response.data = _this.mockData.getData(dataId);
                if (response.data) {
                    response.status = 200;
                    response.statusText = "OK";
                }
            }
            return response;
        };
        this.mockData = new fakeDataProcessor_1.FakeDataProcessor(data);
        return this;
    }
    FakeDataHttpInterceptor.flagMock = "data://#NOWAYJOSE";
    FakeDataHttpInterceptor.paramData = "##data";
    return FakeDataHttpInterceptor;
}());
exports.FakeDataHttpInterceptor = FakeDataHttpInterceptor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFrZURhdGFIdHRwSW50ZXJjZXB0b3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmYWtlRGF0YUh0dHBJbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHlEQUFtRTtBQUVuRTtJQU9JLGlDQUNJLElBQW9CO1FBRHhCLGlCQUtDO1FBRU0sWUFBTyxHQUFHLFVBQUMsT0FBMEI7WUFDeEMsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsT0FBTyxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxRQUFRLENBQUM7Z0JBQzdELE9BQU8sQ0FBQyxHQUFHLEdBQUcsdUJBQXVCLENBQUMsUUFBUSxDQUFDO2dCQUMvQyxNQUFNLENBQUMsT0FBTyxDQUFDO1lBQ25CLENBQUM7WUFDRCxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVLLGFBQVEsR0FBRyxVQUFDLFFBQXdDO1lBQ3ZELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQzNELElBQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6RSxRQUFRLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNqQixRQUFRLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztvQkFDdEIsUUFBUSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUM7Z0JBQ3RDLENBQUM7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUM7UUFFSyxrQkFBYSxHQUFHLFVBQUMsUUFBd0M7WUFDNUQsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEtBQUssdUJBQXVCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDM0QsSUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pFLFFBQVEsQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzlDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNoQixRQUFRLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztvQkFDdEIsUUFBUSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQy9CLENBQUM7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUM7UUFwQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLHFDQUFpQixDQUFJLElBQUksQ0FBQyxDQUFDO1FBQy9DLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQVZjLGdDQUFRLEdBQUcsbUJBQW1CLENBQUM7SUFDL0IsaUNBQVMsR0FBRyxRQUFRLENBQUM7SUE0Q3hDLDhCQUFDO0NBQUEsQUEvQ0QsSUErQ0M7QUEvQ1ksMERBQXVCIn0=

/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function defaultId(item) {
    return JSON.stringify(item);
}
var FakeDataProcessor = /** @class */ (function () {
    function FakeDataProcessor(data, options) {
        this.mapData = {};
        this.data = data || [];
        this.options = _.assign({}, FakeDataProcessor.defaultOptions, options);
    }
    FakeDataProcessor.prototype.getData = function (id) {
        return this.mapData[id];
    };
    FakeDataProcessor.prototype.setData = function (id, value) {
        this.mapData[id] = value;
        return id;
    };
    FakeDataProcessor.prototype.getId = function (url) {
        var datasets = _.filter(this.data, function (item) { return item.urlMatcher.test(url); });
        if (datasets.length === 0) {
            return null;
        }
        if (datasets.length > 1) {
            console.warn("more matching fake datasets found, will use first");
        }
        var dataset = datasets[0];
        var matches = dataset.urlMatcher.exec(url);
        var data = dataset.dataMatcher
            ? _.filter(dataset.data, dataset.dataMatcher(matches))[0]
            : dataset.data[0];
        if (!data && this.options.fallthrough) {
            return null;
        }
        var id = (dataset.id || defaultId)(data);
        return this.setData(id, data);
    };
    FakeDataProcessor.defaultOptions = {
        fallthrough: true
    };
    return FakeDataProcessor;
}());
exports.FakeDataProcessor = FakeDataProcessor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFrZURhdGFQcm9jZXNzb3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmYWtlRGF0YVByb2Nlc3Nvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQWlCQSxtQkFBbUIsSUFBUztJQUN4QixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUNoQyxDQUFDO0FBRUQ7SUFVSSwyQkFDSSxJQUFvQixFQUNwQixPQUFtQztRQU0vQixZQUFPLEdBQXdCLEVBQUUsQ0FBQztRQUp0QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUlNLG1DQUFPLEdBQWQsVUFBZSxFQUFVO1FBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFTSxtQ0FBTyxHQUFkLFVBQWUsRUFBVSxFQUFFLEtBQVU7UUFDakMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDekIsTUFBTSxDQUFDLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFTSxpQ0FBSyxHQUFaLFVBQWEsR0FBVztRQUNwQixJQUFNLFFBQVEsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUNyQixJQUFJLENBQUMsSUFBSSxFQUNULFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQXpCLENBQXlCLENBQ3BDLENBQUM7UUFDRixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEIsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsbURBQW1ELENBQUMsQ0FBQztRQUN0RSxDQUFDO1FBQ0QsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVCLElBQU0sT0FBTyxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzdDLElBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxXQUFXO1lBQzVCLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6RCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUV0QixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0QsSUFBTSxFQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBakRhLGdDQUFjLEdBQThCO1FBQ3RELFdBQVcsRUFBRSxJQUFJO0tBQ3BCLENBQUM7SUFnRE4sd0JBQUM7Q0FBQSxBQXBERCxJQW9EQztBQXBEWSw4Q0FBaUIifQ==

/***/ }),
/* 200 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
function developerConfigFn($stateProvider) {
    $stateProvider
        .state("developer", {
        title: "Developer Settings",
        url: "/developer",
        controller: "DeveloperController",
        controllerAs: "viewModel",
        template: __webpack_require__(29)
    })
        .state("developerAuthAdmin", {
        title: "Developer Auth Admin",
        url: "/developer/auth/admin",
        template: "<h3>Access Granted!</h3>",
        params: {
            errorType: "auth",
            errorCode: "403",
            message: "Access Denied",
            location: "/developer/auth/admin"
        },
        authProfile: {
            permissions: [],
            roles: ["admin"]
        }
    })
        .state("developerAuthGuest", {
        title: "Developer Auth Guest",
        url: "/developer/auth/guest",
        template: "<h3>Access Granted!</h3>",
        params: {
            errorType: "auth",
            errorCode: "403",
            message: "Access Denied",
            location: "/developer/auth/guest"
        },
        authProfile: {
            permissions: ["nodeManagement", "alertManagement"],
            roles: []
        }
    });
}
developerConfigFn.$inject = ["$stateProvider"];
exports.default = developerConfigFn;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV2ZWxvcGVyLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRldmVsb3Blci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFJdkMsMkJBQTRCLGNBQThCO0lBQ3RELGNBQWM7U0FDVCxLQUFLLENBQUMsV0FBVyxFQUFXO1FBQ3pCLEtBQUssRUFBRSxvQkFBb0I7UUFDM0IsR0FBRyxFQUFFLFlBQVk7UUFDakIsVUFBVSxFQUFFLHFCQUFxQjtRQUNqQyxZQUFZLEVBQUUsV0FBVztRQUN6QixRQUFRLEVBQUUsT0FBTyxDQUFTLGtCQUFrQixDQUFDO0tBQ2hELENBQUM7U0FDRCxLQUFLLENBQUMsb0JBQW9CLEVBQVc7UUFDbEMsS0FBSyxFQUFFLHNCQUFzQjtRQUM3QixHQUFHLEVBQUUsdUJBQXVCO1FBQzVCLFFBQVEsRUFBRSwwQkFBMEI7UUFDcEMsTUFBTSxFQUFFO1lBQ0osU0FBUyxFQUFFLE1BQU07WUFDakIsU0FBUyxFQUFFLEtBQUs7WUFDaEIsT0FBTyxFQUFFLGVBQWU7WUFDeEIsUUFBUSxFQUFFLHVCQUF1QjtTQUNwQztRQUNELFdBQVcsRUFBRTtZQUNULFdBQVcsRUFBRSxFQUFFO1lBQ2YsS0FBSyxFQUFFLENBQUMsT0FBTyxDQUFDO1NBQ25CO0tBQ0osQ0FBQztTQUNELEtBQUssQ0FBQyxvQkFBb0IsRUFBWTtRQUNuQyxLQUFLLEVBQUUsc0JBQXNCO1FBQzdCLEdBQUcsRUFBRSx1QkFBdUI7UUFDNUIsUUFBUSxFQUFFLDBCQUEwQjtRQUNwQyxNQUFNLEVBQUU7WUFDSixTQUFTLEVBQUUsTUFBTTtZQUNqQixTQUFTLEVBQUUsS0FBSztZQUNoQixPQUFPLEVBQUUsZUFBZTtZQUN4QixRQUFRLEVBQUUsdUJBQXVCO1NBQ3BDO1FBQ0QsV0FBVyxFQUFFO1lBQ1QsV0FBVyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsaUJBQWlCLENBQUM7WUFDbEQsS0FBSyxFQUFFLEVBQUU7U0FDWjtLQUNKLENBQUMsQ0FBQztBQUNYLENBQUM7QUFFRCxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0FBRS9DLGtCQUFlLGlCQUFpQixDQUFDIn0=

/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var DeveloperController = /** @class */ (function () {
    function DeveloperController($cookies, $log, $scope, $q, constants, contentService, dialogService, orionImprovementDialogService, pingService, salesTriggerDialogService, swApi, swConnectionService, swisService, textService, toastService) {
        var _this = this;
        this.$cookies = $cookies;
        this.$log = $log;
        this.$scope = $scope;
        this.$q = $q;
        this.constants = constants;
        this.contentService = contentService;
        this.dialogService = dialogService;
        this.orionImprovementDialogService = orionImprovementDialogService;
        this.pingService = pingService;
        this.salesTriggerDialogService = salesTriggerDialogService;
        this.swApi = swApi;
        this.swConnectionService = swConnectionService;
        this.swisService = swisService;
        this.textService = textService;
        this.toastService = toastService;
        this.activationTitle = "Activation License";
        this.apiEndpoint = this.constants.apiEndpoint;
        this.connectionType = "online";
        this.demoFeaturesConfluencePage = "https://cp.solarwinds.com/display/UIFRD/Demo+Features";
        this.featureToggles = this.constants.featureToggles;
        this.license = { productName: "Core", licenseVersion: "2010" };
        this.offline = false;
        this.resourceLocation = decodeURI(this.$cookies.get("resourceLocation"));
        this.salesTriggerType = "Eval";
        this.suppressToastOnForceError = false;
        this.$onInit = function () {
            _this.swConnectionService.registerConnectionLost(_this.onConnectionLost);
            _this.swConnectionService.registerConnectionRestored(_this.onConnectionRestored);
            _this.$scope.$on("$destroy", function () {
                _this.swConnectionService.unregisterConnectionLost(_this.onConnectionLost);
                _this.swConnectionService.unregisterConnectionRestored(_this.onConnectionRestored);
            });
        };
        this.deleteData = function () {
            _this.pingService.delete().then(function (data) {
                _this.toastService.success(JSON.stringify(data), "Server Response - With Demo Toast");
            });
        };
        this.discoverResources = function (path) {
            var params = !!path ? { path: path } : null;
            _this.contentService.one("res").getList("discover", params).then(function (response) {
                _this.dialogService
                    .showModal({
                    template: __webpack_require__(30)
                }, {
                    title: "Discovered Resources",
                    resources: _this.contentService.stripRestangular(response),
                    hideCancel: true,
                    buttons: [
                        {
                            name: "ok",
                            cssClass: "xui-btn-primary",
                            text: "OK",
                            action: function (dialog) {
                                return _this.$q.when(true);
                            }
                        }
                    ]
                });
            });
        };
        this.forceError = function () {
            if (_this.suppressToastOnForceError) {
                return _this.forceErrorSuppressed();
            }
            _this.swApi.api(false).one("forceerror").get({ "!swSuppressToastOnFailure": true })
                .then(function (data) {
                _this.$log.info("This should have failed " + data);
            })
                .catch(function (error) {
                _this.$log.error("error block in controller: " + error);
            });
        };
        this.forceErrorSuppressed = function () {
            var promise = _this.swApi.api(false).one("forceerror").withHttpConfig({
                params: { swAlertOnError: false }
            }).get();
            promise.swSuppressToastOnFailure = true;
            promise
                .then(function (data) {
                _this.$log.info("This should have failed " + data);
            })
                .catch(function (error) {
                _this.$log.error("error block in controller: " + error);
            });
        };
        this.postSuppressError = function () {
            _this.pingService.post()
                .then(function (data) {
                _this.toastService.success(JSON.stringify(data), "Server Response - No Demo Toast");
            })
                .catch(function (error) {
                _this.toastService.error(error.message, "Error From Controller");
            });
        };
        this.save = function () {
            _this.$cookies.put("resourceLocation", encodeURI(_this.resourceLocation));
            _this.constants.environment.resourceLocation = _this.resourceLocation;
            _this.$scope.$$childHead.developerForm.$setPristine();
            _this.toastService.success(_this.textService("Your changes have been saved."));
        };
        this.showOrionImprovementDialog = function () {
            _this.orionImprovementDialogService.show(true);
        };
        this.showSalesTriggerDialog = function () {
            _this.salesTriggerDialogService.show(_this.salesTriggerType);
        };
        this.testConnection = function (adminsOnly) {
            _this.pingService.get(adminsOnly)
                .then(function (data) {
                _this.toastService.success("Server response was: " + JSON.stringify(data), "API Connect");
            }).catch(function (error) {
                _this.$log.error(error.message);
            });
        };
        this.updateData = function () {
            _this.pingService.put()
                .then(function (data) {
                _this.dialogService.showMessage({ title: "Mock Data Response", message: JSON.stringify(data) });
            })
                .catch(function (error) {
                _this.$log.error(error.message, "Error From Controller");
            });
        };
        this.onClipboardSuccess = function () {
            _this.toastService.success("Copied");
        };
        this.onLicenseActivate = function (result) {
            if (result.status.toLowerCase() === "success") {
                _this.toastService.success("Activation has been performed successfully");
            }
        };
        this.onConnectionLost = function () {
            _this.offline = true;
        };
        this.onConnectionRestored = function () {
            _this.offline = false;
        };
    }
    DeveloperController.$inject = [
        "$cookies", "$log", "$scope", "$q", "constants", "contentService", "xuiDialogService",
        "swOrionImprovementDialogService", "swPingService", "swSalesTriggerDialogService", "swApi",
        "swConnectionService", "swisService", "getTextService", "xuiToastService",
    ];
    return DeveloperController;
}());
exports.default = DeveloperController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV2ZWxvcGVyLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkZXZlbG9wZXItY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQXFCdkM7SUFrQkksNkJBQW9CLFFBQW9DLEVBQzVDLElBQWlCLEVBQ2pCLE1BQWMsRUFDZCxFQUFhLEVBQ2IsU0FBcUIsRUFDckIsY0FBd0IsRUFDeEIsYUFBNkIsRUFDN0IsNkJBQTZELEVBQzdELFdBQXlCLEVBQ3pCLHlCQUFxRCxFQUNyRCxLQUFvQixFQUNwQixtQkFBdUMsRUFDdkMsV0FBeUIsRUFDekIsV0FBcUMsRUFDckMsWUFBMkI7UUFkdkMsaUJBZUM7UUFmbUIsYUFBUSxHQUFSLFFBQVEsQ0FBNEI7UUFDNUMsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNqQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsT0FBRSxHQUFGLEVBQUUsQ0FBVztRQUNiLGNBQVMsR0FBVCxTQUFTLENBQVk7UUFDckIsbUJBQWMsR0FBZCxjQUFjLENBQVU7UUFDeEIsa0JBQWEsR0FBYixhQUFhLENBQWdCO1FBQzdCLGtDQUE2QixHQUE3Qiw2QkFBNkIsQ0FBZ0M7UUFDN0QsZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFDekIsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUE0QjtRQUNyRCxVQUFLLEdBQUwsS0FBSyxDQUFlO1FBQ3BCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBb0I7UUFDdkMsZ0JBQVcsR0FBWCxXQUFXLENBQWM7UUFDekIsZ0JBQVcsR0FBWCxXQUFXLENBQTBCO1FBQ3JDLGlCQUFZLEdBQVosWUFBWSxDQUFlO1FBekIvQixvQkFBZSxHQUFHLG9CQUFvQixDQUFDO1FBQ3ZDLGdCQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7UUFDekMsbUJBQWMsR0FBRyxRQUFRLENBQUM7UUFDMUIsK0JBQTBCLEdBQUcsdURBQXVELENBQUM7UUFDckYsbUJBQWMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQztRQUMvQyxZQUFPLEdBQUcsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsQ0FBQztRQUMxRCxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLHFCQUFnQixHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7UUFDcEUscUJBQWdCLEdBQXFCLE1BQU0sQ0FBQztRQUM1Qyw4QkFBeUIsR0FBRyxLQUFLLENBQUM7UUFtQm5DLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUN2RSxLQUFJLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFFL0UsS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFO2dCQUN4QixLQUFJLENBQUMsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3pFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyw0QkFBNEIsQ0FBQyxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUNyRixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVLLGVBQVUsR0FBRztZQUNoQixLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQVM7Z0JBQ3JDLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsbUNBQW1DLENBQUMsQ0FBQztZQUN6RixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVLLHNCQUFpQixHQUFHLFVBQUMsSUFBWTtZQUNwQyxJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUV0QyxLQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQWE7Z0JBQzFFLEtBQUksQ0FBQyxhQUFhO3FCQUNiLFNBQVMsQ0FBQztvQkFDUCxRQUFRLEVBQUUsT0FBTyxDQUFTLGlDQUFpQyxDQUFDO2lCQUMvRCxFQUFrQjtvQkFDZixLQUFLLEVBQUUsc0JBQXNCO29CQUM3QixTQUFTLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7b0JBQ3pELFVBQVUsRUFBRSxJQUFJO29CQUNoQixPQUFPLEVBQUU7d0JBQ0w7NEJBQ0ksSUFBSSxFQUFFLElBQUk7NEJBQ1YsUUFBUSxFQUFFLGlCQUFpQjs0QkFDM0IsSUFBSSxFQUFFLElBQUk7NEJBQ1YsTUFBTSxFQUFFLFVBQUMsTUFBdUI7Z0NBQzVCLE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDOUIsQ0FBQzt5QkFDSjtxQkFDSjtpQkFDSixDQUFDLENBQUM7WUFDWCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVLLGVBQVUsR0FBRztZQUNoQixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxNQUFNLENBQUMsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDdkMsQ0FBQztZQUVELEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSwyQkFBMkIsRUFBRSxJQUFJLEVBQUUsQ0FBQztpQkFDN0UsSUFBSSxDQUFDLFVBQUMsSUFBUztnQkFDWixLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsQ0FBQztZQUN0RCxDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLFVBQUMsS0FBVTtnQkFDZCxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUMzRCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVLLHlCQUFvQixHQUFHO1lBQzFCLElBQU0sT0FBTyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxjQUFjLENBQUM7Z0JBQ25FLE1BQU0sRUFBRSxFQUFFLGNBQWMsRUFBRSxLQUFLLEVBQUU7YUFDcEMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBRUgsT0FBUSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQztZQUUvQyxPQUFPO2lCQUNGLElBQUksQ0FBQyxVQUFDLElBQVM7Z0JBQ1osS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDdEQsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxVQUFDLEtBQVU7Z0JBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDM0QsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFFSyxzQkFBaUIsR0FBRztZQUN2QixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRTtpQkFDbEIsSUFBSSxDQUFDLFVBQUMsSUFBUztnQkFDWixLQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLGlDQUFpQyxDQUFDLENBQUM7WUFDdkYsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxVQUFDLEtBQVU7Z0JBQ2QsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO1lBQ3BFLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRUssU0FBSSxHQUFHO1lBQ1YsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsU0FBUyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDeEUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDO1lBQzlELEtBQUksQ0FBQyxNQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUM1RCxLQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQztRQUNqRixDQUFDLENBQUM7UUFFSywrQkFBMEIsR0FBRztZQUNoQyxLQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xELENBQUMsQ0FBQztRQUVLLDJCQUFzQixHQUFHO1lBQzVCLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDL0QsQ0FBQyxDQUFDO1FBRUssbUJBQWMsR0FBRyxVQUFDLFVBQW1CO1lBQ3hDLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQztpQkFDM0IsSUFBSSxDQUFDLFVBQUMsSUFBUztnQkFDWixLQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQywwQkFBd0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUcsRUFBRSxhQUFhLENBQUMsQ0FBQztZQUM3RixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxLQUFVO2dCQUNoQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFFSyxlQUFVLEdBQUc7WUFDaEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUU7aUJBQ2pCLElBQUksQ0FBQyxVQUFDLElBQVM7Z0JBQ1osS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsb0JBQW9CLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ25HLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsVUFBQyxLQUFVO2dCQUNkLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztZQUM1RCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVNLHVCQUFrQixHQUFHO1lBQ3pCLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hDLENBQUMsQ0FBQztRQUVNLHNCQUFpQixHQUFHLFVBQUMsTUFBVztZQUNwQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVDLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7WUFDNUUsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLHFCQUFnQixHQUFHO1lBQ3ZCLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLENBQUMsQ0FBQztRQUVNLHlCQUFvQixHQUFHO1lBQzNCLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLENBQUMsQ0FBQztJQXJJRixDQUFDO0lBaENhLDJCQUFPLEdBQUc7UUFDcEIsVUFBVSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxnQkFBZ0IsRUFBRSxrQkFBa0I7UUFDckYsaUNBQWlDLEVBQUUsZUFBZSxFQUFFLDZCQUE2QixFQUFFLE9BQU87UUFDMUYscUJBQXFCLEVBQUUsYUFBYSxFQUFFLGdCQUFnQixFQUFFLGlCQUFpQjtLQUM1RSxDQUFDO0lBa0tOLDBCQUFDO0NBQUEsQUF2S0QsSUF1S0M7a0JBdktvQixtQkFBbUIifQ==

/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var i18nDemo_config_1 = __webpack_require__(204);
var i18nDemo_controller_1 = __webpack_require__(205);
exports.default = function (module) {
    module.controller("i18nDemoController", i18nDemo_controller_1.default);
    module.app().config(i18nDemo_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFEQUF1QztBQUN2Qyw2REFBdUQ7QUFFdkQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsb0JBQW9CLEVBQUUsNkJBQWtCLENBQUMsQ0FBQztJQUM1RCxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLHlCQUFNLENBQUMsQ0FBQztBQUNoQyxDQUFDLENBQUMifQ==

/***/ }),
/* 204 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider) {
    $stateProvider.state("i18nDemo", {
        i18Title: "_t(Localization Demo)",
        url: "/i18nDemo",
        controller: "i18nDemoController",
        controllerAs: "vm",
        template: __webpack_require__(33),
    });
}
exports.default = config;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaTE4bkRlbW8tY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaTE4bkRlbW8tY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBSXZDLGdCQUFnQjtBQUNoQixnQkFBZ0IsY0FBeUM7SUFDckQsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQWM7UUFDekMsUUFBUSxFQUFFLHVCQUF1QjtRQUNqQyxHQUFHLEVBQUUsV0FBVztRQUNoQixVQUFVLEVBQUUsb0JBQW9CO1FBQ2hDLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQVMsaUJBQWlCLENBQUM7S0FDL0MsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUVELGtCQUFlLE1BQU0sQ0FBQyJ9

/***/ }),
/* 205 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// SW.orion.controller("i18nDemoController",
//     controller,
//     ["$scope", "$q", "getTextService", "swApi"]);
var I18nDemoController = /** @class */ (function () {
    function I18nDemoController($scope, $q, _t, swApi) {
        var _this = this;
        this.$scope = $scope;
        this.$q = $q;
        this._t = _t;
        this.swApi = swApi;
        this.emptyArray = [];
        this.$onInit = function () {
            _this.something = _this._t("いろはにほへと ちりぬるを わかよたれそ つねならむ うゐのおくやま けふこえて あさきゆめみし ゑひもせす");
            _this.takenFromApiText = _this._t("Loading...");
            _this.takenFromApiJson = _this._t("Loading...");
            _this.emptyArray = [];
            _this.todayDate = "/Date(" + new Date().getTime() + ")/";
            _this.swApi.api(false).one("/i18nDemo/text").get().then(function (response) {
                _this.takenFromApiText = response;
            });
            _this.swApi.api(false).one("/i18nDemo/json").get().then(function (response) {
                _this.takenFromApiJson = response;
            });
        };
    }
    I18nDemoController.$inject = ["$scope", "$q", "getTextService", "swApi"];
    return I18nDemoController;
}());
exports.default = I18nDemoController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaTE4bkRlbW8tY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImkxOG5EZW1vLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFJdkMsNENBQTRDO0FBQzVDLGtCQUFrQjtBQUNsQixvREFBb0Q7QUFFcEQ7SUFTSSw0QkFBb0IsTUFBaUIsRUFBVSxFQUFnQixFQUMzQyxFQUFvQyxFQUFVLEtBQW9CO1FBRHRGLGlCQUMwRjtRQUR0RSxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQVUsT0FBRSxHQUFGLEVBQUUsQ0FBYztRQUMzQyxPQUFFLEdBQUYsRUFBRSxDQUFrQztRQUFVLFVBQUssR0FBTCxLQUFLLENBQWU7UUFKL0UsZUFBVSxHQUFVLEVBQUUsQ0FBQztRQU12QixZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsd0RBQXdELENBQUMsQ0FBQztZQUNuRixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUM5QyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUM5QyxLQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUNyQixLQUFJLENBQUMsU0FBUyxHQUFHLFdBQVMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsT0FBSSxDQUFDO1lBRW5ELEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQWdCO2dCQUNwRSxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBZ0I7Z0JBQ3BFLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7SUFoQnVGLENBQUM7SUFUNUUsMEJBQU8sR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxDQUFDLENBQUM7SUEwQnhFLHlCQUFDO0NBQUEsQUEzQkQsSUEyQkM7QUFFRCxrQkFBZSxrQkFBa0IsQ0FBQyJ9

/***/ }),
/* 206 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(53);
__webpack_require__(54);
var orionError_controller_1 = __webpack_require__(53);
var orionError_config_1 = __webpack_require__(54);
exports.default = function (module) {
    module.controller("OrionErrorPageController", orionError_controller_1.default);
    module.app().config(orionError_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1DQUFpQztBQUNqQywrQkFBNkI7QUFDN0IsaUVBQStEO0FBQy9ELHlEQUF5QztBQUV6QyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFVBQVUsQ0FBQywwQkFBMEIsRUFBRSwrQkFBd0IsQ0FBQyxDQUFDO0lBQ3hFLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsMkJBQU0sQ0FBQyxDQUFDO0FBQ2hDLENBQUMsQ0FBQyJ9

/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider) {
    $stateProvider
        .state("views", {
        "abstract": true,
        template: "<ui-view class=\"animated fadeIn\" />"
    })
        .state("views.dashboard", {
        url: "/dashboards/{viewName}",
        templateProvider: provideTemplate(),
        resolve: resolveView(),
        controller: "DashboardController",
        controllerAs: "$dashboard"
    });
    function provideTemplate() {
        return ["$stateParams", "viewService",
            function ($stateParams, viewService) {
                return viewService.getViewTemplate($stateParams.viewName);
            }
        ];
    }
    function resolveView() {
        return {
            view: ["$rootScope", "$stateParams", "viewService",
                function ($rootScope, $stateParams, viewService) {
                    return viewService.getView($stateParams.viewName).then(function (view) {
                        $rootScope.title = view.title;
                        return view;
                    });
                }
            ]
        };
    }
}
exports.default = config;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlld3MtY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmlld3MtY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBU0EsZ0JBQWdCO0FBQ2hCLGdCQUFnQixjQUFvQztJQUVoRCxjQUFjO1NBQ1QsS0FBSyxDQUFDLE9BQU8sRUFBRTtRQUNaLFVBQVUsRUFBRSxJQUFJO1FBQ2hCLFFBQVEsRUFBRSx1Q0FBcUM7S0FDbEQsQ0FBQztTQUNELEtBQUssQ0FBQyxpQkFBaUIsRUFBRTtRQUN0QixHQUFHLEVBQUUsd0JBQXdCO1FBQzdCLGdCQUFnQixFQUFFLGVBQWUsRUFBRTtRQUNuQyxPQUFPLEVBQUUsV0FBVyxFQUFFO1FBQ3RCLFVBQVUsRUFBRSxxQkFBcUI7UUFDakMsWUFBWSxFQUFFLFlBQVk7S0FDN0IsQ0FBQyxDQUFDO0lBRVA7UUFDSSxNQUFNLENBQUMsQ0FBQyxjQUFjLEVBQUUsYUFBYTtZQUNqQyxVQUFDLFlBQStCLEVBQUUsV0FBa0M7Z0JBQ2hFLE1BQU0sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM5RCxDQUFDO1NBQ0osQ0FBQztJQUNOLENBQUM7SUFFRDtRQUNJLE1BQU0sQ0FBQztZQUNILElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxjQUFjLEVBQUUsYUFBYTtnQkFDOUMsVUFBQyxVQUEyQixFQUFFLFlBQStCLEVBQ3pELFdBQWtDO29CQUNsQyxNQUFNLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBUzt3QkFDN0QsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO3dCQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDO29CQUNoQixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2FBQ0o7U0FDSixDQUFDO0lBQ04sQ0FBQztBQUNMLENBQUM7QUFFRCxrQkFBZSxNQUFNLENBQUMifQ==

/***/ }),
/* 208 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(209);
var subview_config_1 = __webpack_require__(210);
var subview_controller_1 = __webpack_require__(211);
var baseSubview_controller_1 = __webpack_require__(212);
exports.default = function (module) {
    module.config(subview_config_1.subviewConfigFn);
    module.controller("SubviewController", subview_controller_1.SubviewController);
    module.controller("BaseSubviewController", baseSubview_controller_1.BaseSubviewController);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDBCQUF3QjtBQUN4QixtREFBbUQ7QUFDbkQsMkRBQXlEO0FBQ3pELG1FQUFpRTtBQUVqRSxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQ0FBZSxDQUFDLENBQUM7SUFDL0IsTUFBTSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsRUFBRSxzQ0FBaUIsQ0FBQyxDQUFDO0lBQzFELE1BQU0sQ0FBQyxVQUFVLENBQUMsdUJBQXVCLEVBQUUsOENBQXFCLENBQUMsQ0FBQztBQUN0RSxDQUFDLENBQUMifQ==

/***/ }),
/* 209 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 210 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
subviewConfigFn.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function subviewConfigFn($stateProvider) {
    $stateProvider
        .state("subview", {
        abstract: true,
        parent: "basesubview",
        controller: "SubviewController",
        controllerAs: "viewModel",
        template: __webpack_require__(36),
    })
        .state("basesubview", {
        abstract: true,
        controller: "BaseSubviewController",
        controllerAs: "baseViewModel",
        template: __webpack_require__(35),
        resolve: {
            swSubviewInfo: function () {
                //This is an initially empty object used for passing info between ui-views
                return {};
            }
        }
    });
}
exports.subviewConfigFn = subviewConfigFn;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Vidmlldy1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdWJ2aWV3LWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUl2QyxnQkFBZ0I7QUFDaEIseUJBQWlDLGNBQThCO0lBQzNELGNBQWM7U0FDVCxLQUFLLENBQUMsU0FBUyxFQUFXO1FBQ3ZCLFFBQVEsRUFBRSxJQUFJO1FBQ2QsTUFBTSxFQUFFLGFBQWE7UUFDckIsVUFBVSxFQUFFLG1CQUFtQjtRQUMvQixZQUFZLEVBQUUsV0FBVztRQUN6QixRQUFRLEVBQUUsT0FBTyxDQUFTLGdCQUFnQixDQUFDO0tBQzlDLENBQUM7U0FDRCxLQUFLLENBQUMsYUFBYSxFQUFXO1FBQzNCLFFBQVEsRUFBRSxJQUFJO1FBQ2QsVUFBVSxFQUFFLHVCQUF1QjtRQUNuQyxZQUFZLEVBQUUsZUFBZTtRQUM3QixRQUFRLEVBQUUsT0FBTyxDQUFTLG9CQUFvQixDQUFDO1FBQy9DLE9BQU8sRUFBRTtZQUNMLGFBQWEsRUFBRTtnQkFDWCwwRUFBMEU7Z0JBQzFFLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFDZCxDQUFDO1NBQ0o7S0FDSixDQUFDLENBQUM7QUFDWCxDQUFDO0FBckJELDBDQXFCQyJ9

/***/ }),
/* 211 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SubviewController = /** @class */ (function () {
    /** @ngInject */
    SubviewController.$inject = ["swTooltipService", "swDateTimeService", "pollingService", "swSubviewInfo", "$q", "$scope"];
    function SubviewController(swTooltipService, swDateTimeService, pollingService, swSubviewInfo, $q, $scope) {
        var _this = this;
        this.swTooltipService = swTooltipService;
        this.swDateTimeService = swDateTimeService;
        this.pollingService = pollingService;
        this.swSubviewInfo = swSubviewInfo;
        this.$q = $q;
        this.$scope = $scope;
        this.$onInit = function () {
            _this.swSubviewInfo.resolved.then(function (response) {
                _this.activeTab = response.activeTab;
                _this.breadcrumbs = response.breadcrumbs;
                _this.objectName = response.objectName;
                _this.opid = response.opid;
                _this.entityType = response.entityType;
                _this.swisUri = response.swisUri;
                _this.pageTitle = _.filter([
                    _this.activeTab.groupName,
                    _this.objectName,
                    _this.activeTab.shortTitle
                ]).join(" - ");
                _this.pollingService.register(_this.updateHeaderValues, true);
                return response;
            }).catch(function () {
                return _this.$q.reject();
            });
            _this.swSubviewInfo.updateHeaderValues = _this.updateHeaderValues;
            _this.$scope.$on("$destroy", function () {
                _this.pollingService.unregister(_this.updateHeaderValues);
            });
        };
        this.breadcrumbs = [];
        this.status = 0;
        this.updateHeaderValues = function () {
            _this.serverTime = _this.swDateTimeService.getOrionNowMoment().format("dddd, LL LTS");
            if (_this.swSubviewInfo && _this.swSubviewInfo.queryParams["netobject"]) {
                var netObj = _this.swSubviewInfo.queryParams["netobject"];
                var splitNetObj = netObj.split(":");
                _this.swTooltipService.getData("0", splitNetObj[0], "default", splitNetObj[1])
                    .then(function (response) {
                    _this.status = parseInt(response.status, 10);
                    _this.childStatus = parseInt(response.childStatus, 10);
                });
            }
        };
    }
    return SubviewController;
}());
exports.SubviewController = SubviewController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Vidmlldy1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3Vidmlldy1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBeUJ2QztJQUNJLGdCQUFnQjtJQUNoQiwyQkFBb0IsZ0JBQWlDLEVBQ3pDLGlCQUFtQyxFQUNuQyxjQUErQixFQUMvQixhQUE2QixFQUM3QixFQUFhLEVBQ2IsTUFBYztRQUwxQixpQkFNQztRQU5tQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWlCO1FBQ3pDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWlCO1FBQy9CLGtCQUFhLEdBQWIsYUFBYSxDQUFnQjtRQUM3QixPQUFFLEdBQUYsRUFBRSxDQUFXO1FBQ2IsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUVuQixZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRO2dCQUN0QyxLQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3BDLEtBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztnQkFDeEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDO2dCQUN0QyxLQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQztnQkFDdEMsS0FBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO2dCQUNoQyxLQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBQ3RCLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUztvQkFDeEIsS0FBSSxDQUFDLFVBQVU7b0JBQ2YsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVO2lCQUM1QixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNmLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDNUQsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUNwQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ0wsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDNUIsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQztZQUVoRSxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUU7Z0JBQ3hCLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQzVELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBS0ssZ0JBQVcsR0FBa0IsRUFBRSxDQUFDO1FBQ2hDLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFRbEIsdUJBQWtCLEdBQUc7WUFDekIsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7WUFFcEYsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGFBQWEsSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BFLElBQU0sTUFBTSxHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMzRCxJQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN0QyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDeEUsSUFBSSxDQUFDLFVBQUMsUUFBUTtvQkFDWCxLQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUM1QyxLQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUMxRCxDQUFDLENBQUMsQ0FBQztZQUNYLENBQUM7UUFDTCxDQUFDLENBQUM7SUFuREYsQ0FBQztJQW9ETCx3QkFBQztBQUFELENBQUMsQUE1REQsSUE0REM7QUE1RFksOENBQWlCIn0=

/***/ }),
/* 212 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var BaseSubviewController = /** @class */ (function () {
    /** @ngInject */
    BaseSubviewController.$inject = ["$location", "swDashboardViewService", "swSubviewInfo", "$q", "swOrionViewService"];
    function BaseSubviewController($location, swDashboardViewService, swSubviewInfo, $q, swOrionViewService) {
        var _this = this;
        this.$location = $location;
        this.swDashboardViewService = swDashboardViewService;
        this.swSubviewInfo = swSubviewInfo;
        this.$q = $q;
        this.swOrionViewService = swOrionViewService;
        this.$onInit = function () {
            var queryParams = _this.$location.search();
            var caseInsensitiveQueryParams = {};
            angular.forEach(queryParams, function (value, key) {
                caseInsensitiveQueryParams[key.toLowerCase()] = value;
            });
            if (!_this.swOrionViewService.activeViewInfo) {
                _this.swOrionViewService.activeViewInfo = { viewId: caseInsensitiveQueryParams.viewid };
            }
            _this.swSubviewInfo.queryParams = caseInsensitiveQueryParams;
            _this.swSubviewInfo.resolved = _this.swDashboardViewService.viewData(caseInsensitiveQueryParams.viewid, caseInsensitiveQueryParams.opid, caseInsensitiveQueryParams.netobject)
                .then(function (response) {
                _this.activeTab = _.find(response.tabs, { viewId: parseInt(caseInsensitiveQueryParams.viewid, 10) });
                response.activeTab = _this.activeTab;
                _this.navSelection = [_this.activeTab.pagePath];
                _this.tabs = response.tabs;
                _this.showSidebar(!!_this.tabs.length);
                return response;
            }).catch(function () {
                _this.showSidebar(false);
                return _this.$q.reject();
            });
        };
        this.tabs = [];
        this.settings = {
            display: true,
            position: "left",
            shrink: false,
            expandable: true
        };
        this.navSelection = ["/"];
        this.getIconPath = function (iconFile) {
            return "/Orion/Images/SubViewIcons/" + iconFile;
        };
        this.showSidebar = function (show) {
            _this.settings.display = show;
        };
    }
    return BaseSubviewController;
}());
exports.BaseSubviewController = BaseSubviewController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZVN1YnZpZXctY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImJhc2VTdWJ2aWV3LWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFVdkM7SUFDSSxnQkFBZ0I7SUFDaEIsK0JBQW9CLFNBQTJCLEVBQ25DLHNCQUE2QyxFQUM3QyxhQUE2QixFQUM3QixFQUFhLEVBQ2Isa0JBQXlDO1FBSnJELGlCQUtDO1FBTG1CLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQ25DLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBdUI7UUFDN0Msa0JBQWEsR0FBYixhQUFhLENBQWdCO1FBQzdCLE9BQUUsR0FBRixFQUFFLENBQVc7UUFDYix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQXVCO1FBRzlDLFlBQU8sR0FBRztZQUNiLElBQU0sV0FBVyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDNUMsSUFBSSwwQkFBMEIsR0FBUSxFQUFFLENBQUM7WUFDekMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBQyxLQUFVLEVBQUUsR0FBVztnQkFDakQsMEJBQTBCLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzFELENBQUMsQ0FBQyxDQUFDO1lBRUgsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDMUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSwwQkFBMEIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUMzRixDQUFDO1lBRUQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsMEJBQTBCLENBQUM7WUFDNUQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FDOUQsMEJBQTBCLENBQUMsTUFBTSxFQUNqQywwQkFBMEIsQ0FBQyxJQUFJLEVBQy9CLDBCQUEwQixDQUFDLFNBQVMsQ0FBQztpQkFDcEMsSUFBSSxDQUFDLFVBQUMsUUFBUTtnQkFDWCxLQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsMEJBQTBCLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDcEcsUUFBUSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDO2dCQUNwQyxLQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDOUMsS0FBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNyQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3BCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDTCxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN4QixNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM1QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVLLFNBQUksR0FBb0IsRUFBRSxDQUFDO1FBRzNCLGFBQVEsR0FBcUI7WUFDaEMsT0FBTyxFQUFFLElBQUk7WUFDYixRQUFRLEVBQUUsTUFBTTtZQUNoQixNQUFNLEVBQUUsS0FBSztZQUNiLFVBQVUsRUFBRSxJQUFJO1NBQ25CLENBQUM7UUFFSyxpQkFBWSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFckIsZ0JBQVcsR0FBRyxVQUFDLFFBQWdCO1lBQ2xDLE1BQU0sQ0FBQyw2QkFBNkIsR0FBRyxRQUFRLENBQUM7UUFDcEQsQ0FBQyxDQUFDO1FBRU0sZ0JBQVcsR0FBRyxVQUFDLElBQWE7WUFDaEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsQ0FBQztJQWpERixDQUFDO0lBa0RMLDRCQUFDO0FBQUQsQ0FBQyxBQXpERCxJQXlEQztBQXpEWSxzREFBcUIifQ==

/***/ }),
/* 213 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var entityIcon_filter_1 = __webpack_require__(214);
var status_filter_1 = __webpack_require__(215);
exports.default = function (module) {
    module.filter("swEntityIcon", entityIcon_filter_1.entityIconFilter);
    module.filter("swStatus", status_filter_1.statusFilter);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlEQUF1RDtBQUN2RCxpREFBK0M7QUFFL0Msa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLG9DQUFnQixDQUFDLENBQUM7SUFDaEQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsNEJBQVksQ0FBQyxDQUFDO0FBQzVDLENBQUMsQ0FBQyJ9

/***/ }),
/* 214 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/**
* @ngdoc filter
* @name orion.filters:swEntityIcon
* @restrict A
*
* @description
* Filter used for mapping swis entity type to proper Apollo icon.
*
* @param {string} entity The source swis entity type.
*
* @example swEntityIcon filter example
* <example module="orion">
    <file src="src/filters/docs/entityIconAndStatus-example.html" name="index.html"></file>
    <file src="src/filters/docs/entityIconAndStatus-example.js" name="app.js"></file>
  </example>
*/
entityIconFilter.$inject = ["swSwisDataMappingService"];
Object.defineProperty(exports, "__esModule", { value: true });
/* @ngInject */
function entityIconFilter(swSwisDataMappingService) {
    return function (entity) {
        return swSwisDataMappingService.getEntityIcon(entity);
    };
}
exports.entityIconFilter = entityIconFilter;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5SWNvbi1maWx0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJlbnRpdHlJY29uLWZpbHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUM7Ozs7Ozs7Ozs7Ozs7OztFQWVFOztBQUVILGVBQWU7QUFDZiwwQkFBaUMsd0JBQTBEO0lBRXZGLE1BQU0sQ0FBQyxVQUFDLE1BQWM7UUFDbEIsTUFBTSxDQUFDLHdCQUF3QixDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxRCxDQUFDLENBQUM7QUFDTixDQUFDO0FBTEQsNENBS0MifQ==

/***/ }),
/* 215 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
statusFilter.$inject = ["swSwisDataMappingService"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
* @ngdoc filter
* @name orion.filters:swStatus
* @restrict A
*
* @description
* Filter used for mapping swis status to proper Apollo status.
*
* @param {number} statusId The source swis status id.
* @param {number} childStatusId The source swis child status id.
*
* @example swStatus filter example
* <example module="orion">
    <file src="src/filters/docs/entityIconAndStatus-example.html" name="index.html"></file>
    <file src="src/filters/docs/entityIconAndStatus-example.js" name="app.js"></file>
  </example>
*/
/* @ngInject */
function statusFilter(swSwisDataMappingService) {
    return function (statusId, childStatusId) {
        var status = swSwisDataMappingService.getStatus(statusId);
        if (!status) {
            return "";
        }
        var childStatus = swSwisDataMappingService.getChildStatus(childStatusId);
        return (!childStatus || childStatus === status)
            ? status
            : status + "-child-" + childStatus;
    };
}
exports.statusFilter = statusFilter;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdHVzLWZpbHRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInN0YXR1cy1maWx0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9DQUFvQzs7QUFFbkM7Ozs7Ozs7Ozs7Ozs7Ozs7RUFnQkU7QUFFSCxlQUFlO0FBQ2Ysc0JBQTZCLHdCQUEwRDtJQUVuRixNQUFNLENBQUMsVUFBQyxRQUFnQixFQUFFLGFBQXNCO1FBQzVDLElBQU0sTUFBTSxHQUFHLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM1RCxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDVixNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ2QsQ0FBQztRQUNELElBQU0sV0FBVyxHQUFHLHdCQUF3QixDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMzRSxNQUFNLENBQUMsQ0FBQyxDQUFDLFdBQVcsSUFBSSxXQUFXLEtBQUssTUFBTSxDQUFDO1lBQzNDLENBQUMsQ0FBQyxNQUFNO1lBQ1IsQ0FBQyxDQUFJLE1BQU0sZUFBVSxXQUFhLENBQUM7SUFDM0MsQ0FBQyxDQUFDO0FBQ04sQ0FBQztBQVpELG9DQVlDIn0=

/***/ }),
/* 216 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var megaMenu_1 = __webpack_require__(217);
var errorInspector_1 = __webpack_require__(253);
exports.default = function (module) {
    var componentsModule = angular.module("orion.components");
    megaMenu_1.megaMenu(componentsModule);
    errorInspector_1.errorInspector(componentsModule);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLHVDQUFzQztBQUN0QyxtREFBa0Q7QUFFbEQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLElBQU0sZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBQzVELG1CQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUMzQiwrQkFBYyxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFDckMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 217 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var megaMenu_component_1 = __webpack_require__(218);
var headerLogo_1 = __webpack_require__(221);
var userInfo_1 = __webpack_require__(225);
var helpLink_1 = __webpack_require__(229);
var navigationMenu_1 = __webpack_require__(232);
var orionSearch_1 = __webpack_require__(237);
var notifications_1 = __webpack_require__(241);
var evaluationBanner_1 = __webpack_require__(245);
var dashboardEditPanel_1 = __webpack_require__(249);
function megaMenu(module) {
    module.component("swMegaMenu", new megaMenu_component_1.MegaMenuComponent());
    headerLogo_1.headerLogo(module);
    userInfo_1.userInfo(module);
    helpLink_1.helpLink(module);
    navigationMenu_1.navigationMenu(module);
    notifications_1.notifications(module);
    orionSearch_1.orionSearch(module);
    evaluationBanner_1.evaluationBanner(module);
    dashboardEditPanel_1.dashboardEditPanel(module);
}
exports.megaMenu = megaMenu;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJEQUF5RDtBQUV6RCwyQ0FBMEM7QUFDMUMsdUNBQXNDO0FBQ3RDLHVDQUFzQztBQUN0QyxtREFBa0Q7QUFDbEQsNkNBQTRDO0FBQzVDLGlEQUFnRDtBQUNoRCx1REFBc0Q7QUFDdEQsMkRBQTBEO0FBRTFELGtCQUF5QixNQUFlO0lBQ3BDLE1BQU0sQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLElBQUksc0NBQWlCLEVBQUUsQ0FBQyxDQUFDO0lBRXhELHVCQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbkIsbUJBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQixtQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pCLCtCQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkIsNkJBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN0Qix5QkFBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3BCLG1DQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3pCLHVDQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQy9CLENBQUM7QUFYRCw0QkFXQyJ9

/***/ }),
/* 218 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(219);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var megaMenu_controller_1 = __webpack_require__(220);
var MegaMenuComponent = /** @class */ (function () {
    function MegaMenuComponent() {
        this.template = __webpack_require__(13);
        this.controller = megaMenu_controller_1.MegaMenuController;
    }
    return MegaMenuComponent;
}());
exports.MegaMenuComponent = MegaMenuComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVnYU1lbnUtY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWVnYU1lbnUtY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsNkRBQTJEO0FBRTNEO0lBQUE7UUFDVyxhQUFRLEdBQUcsT0FBTyxDQUFTLDJCQUEyQixDQUFDLENBQUM7UUFDeEQsZUFBVSxHQUFHLHdDQUFrQixDQUFDO0lBQzNDLENBQUM7SUFBRCx3QkFBQztBQUFELENBQUMsQUFIRCxJQUdDO0FBSFksOENBQWlCIn0=

/***/ }),
/* 219 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 220 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var MegaMenuController = /** @class */ (function () {
    /** @ngInject */
    MegaMenuController.$inject = ["$element"];
    function MegaMenuController($element) {
        var _this = this;
        this.$element = $element;
        this.$onInit = function () {
            _this.$element.addClass("sw-mega-menu");
        };
    }
    return MegaMenuController;
}());
exports.MegaMenuController = MegaMenuController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVnYU1lbnUtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1lZ2FNZW51LWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQiw0QkFDWSxRQUEwQjtRQUR0QyxpQkFFSztRQURPLGFBQVEsR0FBUixRQUFRLENBQWtCO1FBRy9CLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQztJQUpFLENBQUM7SUFLVCx5QkFBQztBQUFELENBQUMsQUFURCxJQVNDO0FBVFksZ0RBQWtCIn0=

/***/ }),
/* 221 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var headerLogo_component_1 = __webpack_require__(222);
function headerLogo(module) {
    module.component("swHeaderLogo", new headerLogo_component_1.HeaderLogoComponent());
}
exports.headerLogo = headerLogo;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtEQUE2RDtBQUU3RCxvQkFBMkIsTUFBZTtJQUN0QyxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxJQUFJLDBDQUFtQixFQUFFLENBQUMsQ0FBQztBQUNoRSxDQUFDO0FBRkQsZ0NBRUMifQ==

/***/ }),
/* 222 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(223);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var headerLogo_controller_1 = __webpack_require__(224);
var HeaderLogoComponent = /** @class */ (function () {
    function HeaderLogoComponent() {
        this.template = __webpack_require__(11);
        this.controller = headerLogo_controller_1.HeaderLogoController;
    }
    return HeaderLogoComponent;
}());
exports.HeaderLogoComponent = HeaderLogoComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyTG9nby1jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJoZWFkZXJMb2dvLWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlFQUErRDtBQUUvRDtJQUFBO1FBQ1csYUFBUSxHQUFHLE9BQU8sQ0FBUyw2QkFBNkIsQ0FBQyxDQUFDO1FBQzFELGVBQVUsR0FBRyw0Q0FBb0IsQ0FBQztJQUM3QyxDQUFDO0lBQUQsMEJBQUM7QUFBRCxDQUFDLEFBSEQsSUFHQztBQUhZLGtEQUFtQiJ9

/***/ }),
/* 223 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 224 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var HeaderLogoController = /** @class */ (function () {
    /** @ngInject */
    HeaderLogoController.$inject = ["webAdminService"];
    function HeaderLogoController(webAdminService) {
        var _this = this;
        this.webAdminService = webAdminService;
        this.$onInit = function () {
            _this.resolveImgSrc();
        };
    }
    HeaderLogoController.prototype.resolveImgSrc = function () {
        var _this = this;
        this.getWebSettings("Site Logo Image")
            .then(function (image) {
            if (image && image !== "" && image !== "nologo") {
                return _this.createDataUrl(image);
            }
            return _this.getWebSettings("Site Logo");
        })
            .then(function (uri) {
            if (uri) {
                _this.logoUri = uri;
            }
        });
    };
    HeaderLogoController.prototype.getWebSettings = function (settingKey) {
        return this.webAdminService
            .getWebSetting(settingKey, "")
            .then(function (settingValue) { return settingValue; });
    };
    HeaderLogoController.prototype.createDataUrl = function (base64Data) {
        return "data:" + this.getMimeType(base64Data) + ";base64," + base64Data;
    };
    HeaderLogoController.prototype.getMimeType = function (base64Data) {
        var rawData = atob(base64Data);
        if (rawData.length >= 2 && rawData.charCodeAt(0) === 0xff && rawData.charCodeAt(1) === 0xd8) {
            return "image/jpeg";
        }
        if (rawData.length >= 3 && rawData.charCodeAt(0) === 0x47 && rawData.charCodeAt(1) === 0x49 && rawData.charCodeAt(2) === 0x46) {
            return "image/gif";
        }
        if (rawData.length >= 8 &&
            rawData.charCodeAt(0) === 0x89 && rawData.charCodeAt(1) === 0x50 && rawData.charCodeAt(2) === 0x4e && rawData.charCodeAt(3) === 0x47 &&
            rawData.charCodeAt(4) === 0x0d && rawData.charCodeAt(5) === 0x0a && rawData.charCodeAt(6) === 0x1a && rawData.charCodeAt(7) === 0x0a) {
            return "image/png";
        }
        return "image/jpeg"; // deafault - browsers can figure it out if it's wrong
    };
    return HeaderLogoController;
}());
exports.HeaderLogoController = HeaderLogoController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyTG9nby1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaGVhZGVyTG9nby1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0E7SUFHSSxnQkFBZ0I7SUFDaEIsOEJBQW9CLGVBQWlDO1FBQXJELGlCQUEwRDtRQUF0QyxvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFFOUMsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQztJQUp1RCxDQUFDO0lBTWxELDRDQUFhLEdBQXJCO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDO2FBQ2pDLElBQUksQ0FBQyxVQUFBLEtBQUs7WUFDUCxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksS0FBSyxLQUFLLEVBQUUsSUFBSSxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsQ0FBQztZQUVELE1BQU0sQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzVDLENBQUMsQ0FBQzthQUNELElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDTCxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNOLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3ZCLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTyw2Q0FBYyxHQUF0QixVQUF1QixVQUFrQjtRQUNyQyxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWU7YUFDdEIsYUFBYSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7YUFDN0IsSUFBSSxDQUFDLFVBQUMsWUFBb0IsSUFBSyxPQUFBLFlBQVksRUFBWixDQUFZLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRU8sNENBQWEsR0FBckIsVUFBc0IsVUFBa0I7UUFDcEMsTUFBTSxDQUFDLFVBQVEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQVcsVUFBWSxDQUFDO0lBQ3ZFLENBQUM7SUFFTywwQ0FBVyxHQUFuQixVQUFvQixVQUFrQjtRQUNsQyxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFGLE1BQU0sQ0FBQyxZQUFZLENBQUM7UUFDeEIsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztZQUM1SCxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3ZCLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDbkIsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJO1lBQ3BJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztZQUV2SSxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3ZCLENBQUM7UUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsc0RBQXNEO0lBQy9FLENBQUM7SUFDTCwyQkFBQztBQUFELENBQUMsQUFyREQsSUFxREM7QUFyRFksb0RBQW9CIn0=

/***/ }),
/* 225 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var userInfo_component_1 = __webpack_require__(226);
function userInfo(module) {
    module.component("swUserInfo", new userInfo_component_1.UserInfoComponent());
}
exports.userInfo = userInfo;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJEQUF5RDtBQUV6RCxrQkFBeUIsTUFBZTtJQUNwQyxNQUFNLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxJQUFJLHNDQUFpQixFQUFFLENBQUMsQ0FBQztBQUM1RCxDQUFDO0FBRkQsNEJBRUMifQ==

/***/ }),
/* 226 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(227);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var userInfo_controller_1 = __webpack_require__(228);
var UserInfoComponent = /** @class */ (function () {
    function UserInfoComponent() {
        this.template = __webpack_require__(17);
        this.controller = userInfo_controller_1.UserInfoController;
    }
    return UserInfoComponent;
}());
exports.UserInfoComponent = UserInfoComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlckluZm8tY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlckluZm8tY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsNkRBQTJEO0FBRTNEO0lBQUE7UUFDVyxhQUFRLEdBQUcsT0FBTyxDQUFTLDJCQUEyQixDQUFDLENBQUM7UUFDeEQsZUFBVSxHQUFHLHdDQUFrQixDQUFDO0lBQzNDLENBQUM7SUFBRCx3QkFBQztBQUFELENBQUMsQUFIRCxJQUdDO0FBSFksOENBQWlCIn0=

/***/ }),
/* 227 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 228 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var UserInfoController = /** @class */ (function () {
    /** @ngInject */
    UserInfoController.$inject = ["webAdminService", "swOrionNavigationService"];
    function UserInfoController(webAdminService, swOrionNavigationService) {
        var _this = this;
        this.webAdminService = webAdminService;
        this.swOrionNavigationService = swOrionNavigationService;
        this.logout = function () {
            _this.swOrionNavigationService.goToLogout();
        };
    }
    Object.defineProperty(UserInfoController.prototype, "userLabel", {
        get: function () {
            return this.webAdminService.getUserLabel();
        },
        enumerable: true,
        configurable: true
    });
    return UserInfoController;
}());
exports.UserInfoController = UserInfoController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlckluZm8tY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInVzZXJJbmZvLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQiw0QkFDWSxlQUFpQyxFQUNqQyx3QkFBaUQ7UUFGN0QsaUJBR0s7UUFGTyxvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFDakMsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUF5QjtRQU90RCxXQUFNLEdBQUc7WUFDWixLQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDL0MsQ0FBQyxDQUFDO0lBUkUsQ0FBQztJQUVMLHNCQUFXLHlDQUFTO2FBQXBCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDL0MsQ0FBQzs7O09BQUE7SUFLTCx5QkFBQztBQUFELENBQUMsQUFkRCxJQWNDO0FBZFksZ0RBQWtCIn0=

/***/ }),
/* 229 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var helpLink_component_1 = __webpack_require__(230);
function helpLink(module) {
    module.component("swMegaMenuHelpLink", new helpLink_component_1.MegaMenuHelpLinkComponent());
}
exports.helpLink = helpLink;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJEQUFpRTtBQUVqRSxrQkFBeUIsTUFBZTtJQUNwQyxNQUFNLENBQUMsU0FBUyxDQUFDLG9CQUFvQixFQUFFLElBQUksOENBQXlCLEVBQUUsQ0FBQyxDQUFDO0FBQzVFLENBQUM7QUFGRCw0QkFFQyJ9

/***/ }),
/* 230 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(231);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MegaMenuHelpLinkComponent = /** @class */ (function () {
    function MegaMenuHelpLinkComponent() {
        this.template = __webpack_require__(12);
    }
    return MegaMenuHelpLinkComponent;
}());
exports.MegaMenuHelpLinkComponent = MegaMenuHelpLinkComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVscExpbmstY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaGVscExpbmstY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFBQTtRQUNXLGFBQVEsR0FBRyxPQUFPLENBQVMsMkJBQTJCLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBQUQsZ0NBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQztBQUZZLDhEQUF5QiJ9

/***/ }),
/* 231 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 232 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var navigationMenu_component_1 = __webpack_require__(233);
function navigationMenu(module) {
    module.component("swNavigationMenu", new navigationMenu_component_1.NavigationMenuComponent());
}
exports.navigationMenu = navigationMenu;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVFQUFxRTtBQUVyRSx3QkFBK0IsTUFBZTtJQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLElBQUksa0RBQXVCLEVBQUUsQ0FBQyxDQUFDO0FBQ3hFLENBQUM7QUFGRCx3Q0FFQyJ9

/***/ }),
/* 233 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(234);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigationMenu_controller_1 = __webpack_require__(235);
var NavigationMenuComponent = /** @class */ (function () {
    function NavigationMenuComponent() {
        this.template = __webpack_require__(14);
        this.controller = navigationMenu_controller_1.default;
    }
    return NavigationMenuComponent;
}());
exports.NavigationMenuComponent = NavigationMenuComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbk1lbnUtY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibmF2aWdhdGlvbk1lbnUtY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EseUVBQW1FO0FBRW5FO0lBQUE7UUFDVyxhQUFRLEdBQUcsT0FBTyxDQUFTLGlDQUFpQyxDQUFDLENBQUM7UUFDOUQsZUFBVSxHQUFHLG1DQUF3QixDQUFDO0lBQ2pELENBQUM7SUFBRCw4QkFBQztBQUFELENBQUMsQUFIRCxJQUdDO0FBSFksMERBQXVCIn0=

/***/ }),
/* 234 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 235 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var angular_1 = __webpack_require__(236);
var inject_1 = __webpack_require__(38);
function getNext(dict, value) {
    var values = Object.keys(dict).map(function (k) { return dict[k]; });
    return values.filter(function (v) { return v !== value; })[0];
}
var NavigationMenuController = /** @class */ (function () {
    function NavigationMenuController($element, $log, $q, $timeout, $window, $document, swApi, swConstants, viewService, webAdmin, swDemoService, swMegaMenuService) {
        var _this = this;
        this.$element = $element;
        this.$log = $log;
        this.$q = $q;
        this.$timeout = $timeout;
        this.$window = $window;
        this.$document = $document;
        this.swApi = swApi;
        this.swConstants = swConstants;
        this.viewService = viewService;
        this.webAdmin = webAdmin;
        this.swDemoService = swDemoService;
        this.swMegaMenuService = swMegaMenuService;
        this._elements = {};
        this.menuStyle = "";
        this.$onInit = function () {
            _this.$element.addClass("sw-navigation-menu");
            if (_this.isEvaluationBannerVisible()) {
                _this.$form.addClass("sw-eval-mode");
                _this.$document.find("body").addClass("sw-eval-mode");
            }
            _this.loadData()
                .finally(function () { return _this.$timeout(_this.initUi); });
        };
        this.isDashboards = function (menuItem) { return menuItem.name === "dashboards"; };
        this.itemTarget = function (menuItem) { return (menuItem && menuItem.openInNewWindow) ? "_blank" : "_self"; };
        this.initUi = function () {
            // jQuery menu-aim plugin for handling mouse movement over the compact menu.
            // https://github.com/kamens/jQuery-menu-aim
            _this.$menuList.menuAim({
                activate: function (row) {
                    angular_1.element(row).addClass("menu-active");
                },
                deactivate: function (row) {
                    angular_1.element(row).removeClass("menu-active");
                },
                exitMenu: function () {
                    return false;
                }
            });
            var dashboardViewGroup = _.filter(_this.menuItems, _this.isDashboards)[0];
            var dashboardGroups = dashboardViewGroup && dashboardViewGroup.items || [];
            _this.menuStyle = _this.menuStyleValue(_this.swConstants.user && _this.swConstants.user.menuStyle)
                || ((dashboardGroups.length > 4) ? _this.menus.secondary : _this.menus.primary);
            _this.setupEventHandlers();
        };
        this.loadData = function () {
            return _this.swMegaMenuService.getItems()
                .then(function (items) {
                _this.isError = false;
                var user = _this.webAdmin.getUser();
                var isManageDashboards = function (item) { return item && item.name === "manage-dashboards"; };
                var settingsGroup = items.filter(function (menuGroup) { return menuGroup.name === "settings"; })[0];
                if (settingsGroup) {
                    settingsGroup.items = (settingsGroup.items || []).filter(function (item) { return (user.AllowAdmin || _this.swDemoService.isDemoMode()) && !isManageDashboards(item)
                        || _this.swConstants.environment.enableFusionDashboards &&
                            _this.swConstants.user.AllowManageDashboards &&
                            isManageDashboards(item); });
                }
                // Deletes settings group when non admin is logged and fusion dashboards are disabled
                _this.menuItems = items.filter(function (item) { return item && item.items && item.items.length; });
            }, function (error) {
                _this.$log.debug(error);
                _this.isError = true;
                _this.menuItems = null;
                _this.swMegaMenuService.showError(error);
            });
        };
        this.cache = function (name, getter) {
            return _this._elements[name] || (_this._elements[name] = getter());
        };
        this.constants = {
            minWidth: 1024,
            pinTimeout: 200,
            unpinTimeout: 300,
            fadeIn: 300,
            fadeOut: 100,
            toggleFadeIn: 600,
            toggleFadeOut: 300,
        };
        this.menus = {
            primary: "menu-primary",
            secondary: "menu-secondary",
        };
        this.clientState = {
            forceScroll: false,
            maxScroll: 0,
            menuPinned: false,
            pinTimer: null,
            timestampMenuOpened: 0,
        };
        this.cancelPinTimer = function () {
            _this.$timeout.cancel(_this.clientState.pinTimer);
            _this.clientState.pinTimer = null;
        };
        this.setupEventHandlers = function () {
            _this.$window.addEventListener("resize", _.throttle(_this.smartResizeHandler, 100), false);
            _this.$window.addEventListener("scroll", _.throttle(_this.smartScrollHandler, 25), false);
            _this.smartResizeHandler();
            _this.smartScrollHandler();
            // Close the menu when a menu item is clicked.
            _this.$menu.on("click", ".sub-menu-container > ul > li > a", function (event) {
                _this.closeMenu(_this.$menuTabsOpen);
            });
            // Handle mouseenter/mouseleave/click events on the top level menus.
            _this.$menuTabs
                .on("mouseenter", function (event) {
                var $menuItem = angular_1.element(event.currentTarget);
                if ($menuItem.is(".menu-open")) {
                    return;
                }
                _this.clientState.pinTimer = _this.$timeout(function () {
                    _this.unpinMenu(_this.$menuTabsOpen);
                    _this.openMenu($menuItem);
                    _this.setHeights($menuItem);
                }, _this.constants.unpinTimeout);
            })
                .on("mouseleave", function (event) {
                if (_this.clientState.menuPinned) {
                    return;
                }
                _this.cancelPinTimer();
                var $menuItem = angular_1.element(event.currentTarget);
                _this.$timeout(function () {
                    if (!$menuItem.is(":hover")) {
                        _this.closeMenu($menuItem);
                    }
                }, _this.constants.pinTimeout);
            })
                .on("click", "> a", function (event) {
                _this.cancelPinTimer();
                var $menuItem = angular_1.element(event.currentTarget).parent();
                var isOpen = $menuItem.is(".menu-open");
                if (isOpen && (Math.abs(Date.now() - _this.clientState.timestampMenuOpened) < 400)) {
                    return;
                }
                if (isOpen) {
                    _this.unpinMenu($menuItem);
                }
                else {
                    _this.pinMenu($menuItem);
                }
                return false;
            });
        };
        // setup a smartresize function that debounces the resize events
        this.smartResizeHandler = function () {
            if (_this.$window.innerWidth < _this.constants.minWidth) {
                _this.clientState.maxScroll = Math.abs(_this.$window.innerWidth - _this.constants.minWidth);
                _this.$userSection.css("right", -1 * _this.clientState.maxScroll + "px");
                _this.clientState.forceScroll = true;
            }
            else {
                _this.$userSection.css("right", 0);
                _this.clientState.forceScroll = false;
            }
        };
        // same thing for scrolling
        this.smartScrollHandler = function () {
            if (!_this.clientState.forceScroll) {
                return;
            }
            if (_this.$window.pageXOffset > 0) {
                var offset = -1 * Math.min(_this.clientState.maxScroll, _this.$window.pageXOffset);
                _this.$navScrollContainer.css("left", offset + "px");
            }
            else {
                _this.$navScrollContainer.css("left", 0);
            }
        };
        // Helper function to handle user interaction outside a given element.
        this.clickOutside = function ($element) {
            var deferred = _this.$q.defer();
            var $body = angular_1.element("body");
            $body
                .on("click", function (e) {
                if (angular_1.element(e.target).closest($element).length === 0) {
                    $body.off("click");
                    deferred.resolve();
                    return false;
                }
            })
                .on("keydown", function (e) {
                // Allow menus to be closed with the escape key.
                if (e.which === 27) {
                    $body.off("keydown");
                    deferred.resolve();
                    return false;
                }
            });
            return deferred.promise;
        };
        this.setHeights = function ($menuItem) {
            // Set the maximum height of the menu based on the window height.
            var $list = $menuItem.find(".mega-submenu-container > ul");
            var $items = $list.find("> li");
            var windowHeight = angular_1.element(_this.$window).height();
            var menuHeight = Math.round(windowHeight * 0.7);
            $list.css("max-height", menuHeight);
            if ($list[0] !== _this.$menu[0] || _this.isMenuSecondary) {
                $items.css("height", "auto");
                return;
            }
            // Align the heights of the rows to the list with the largest height.
            var itemWidths = $items.get().map(function (el) { return angular_1.element(el).width(); });
            var maxItemWidth = Math.max.apply(Math, itemWidths);
            var columns = Math.floor($list.width() / maxItemWidth);
            if (columns < 2) {
                return;
            }
            _.forEach(_.chunk(_this.$menuItems.get(), columns), function (chunk) {
                var chunkHeights = chunk.map(function (el) { return angular_1.element(el).height(); });
                var maxHeight = Math.max.apply(Math, chunkHeights);
                angular_1.element(chunk).css({
                    height: maxHeight,
                });
            });
        };
        this.toggleMenuStyle = function () {
            // We need to use opacity rather than display so the size of the menu is available for setHeights().
            _this.$menu.animate({ opacity: 0.0 }, _this.constants.toggleFadeOut, function () {
                _this.pinMenu(_this.$menuTabsOpen);
                var newStyle = getNext(_this.menus, _this.menuStyle);
                // Persist the user's setting.
                _this.swApi.ws.one("NodeManagement.asmx")
                    .post("SaveUserSetting", {
                    name: "MegaMenu-Style",
                    value: newStyle
                })
                    .finally(function () {
                    _this.menuStyle = newStyle;
                    _this.setHeights(_this.$menu);
                    _this.$menu.animate({ opacity: 1.0 }, _this.constants.toggleFadeIn);
                });
            });
        };
        this.isEvaluationBannerVisible = function () {
            return (_this.swConstants.license.EvalModuleCount > 0 || _this.swConstants.license.ExpiredModuleCount > 0);
        };
    }
    Object.defineProperty(NavigationMenuController.prototype, "hasDashboards", {
        get: function () {
            return this.menuItems.some(this.isDashboards);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "isLoading", {
        get: function () {
            return this.menuItems == null;
        },
        enumerable: true,
        configurable: true
    });
    NavigationMenuController.prototype.showToggle = function (menuItem) {
        return menuItem && this.isDashboards(menuItem) &&
            ((menuItem.items && menuItem.items.length > 1) || (this.menuStyle === this.menus.secondary));
    };
    NavigationMenuController.prototype.menuStyleValue = function (value) {
        var _this = this;
        var values = Object.keys(this.menus).map(function (k) { return _this.menus[k]; });
        if (values.indexOf(value) !== -1) {
            return value;
        }
        return "";
    };
    Object.defineProperty(NavigationMenuController.prototype, "$menu", {
        get: function () {
            var _this = this;
            return this.cache("menu", function () { return _this.$element.find(".mega-submenu.full-menu"); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "$menuList", {
        get: function () {
            var _this = this;
            return this.cache("menuList", function () { return _this.$element.find(".mega-submenu-container > ul"); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "$menuItems", {
        get: function () {
            var _this = this;
            return this.cache("menuItems", function () { return _this.$menuList.find("> li"); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "$form", {
        get: function () {
            return this.cache("form", function () { return angular_1.element("#content"); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "$navScrollContainer", {
        get: function () {
            return this.cache("navScrollContainer", function () { return angular_1.element("#swNavScroll"); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "$userSection", {
        get: function () {
            return this.cache("userSection", function () { return angular_1.element("#userName"); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "$menuTabs", {
        get: function () {
            var _this = this;
            return this.cache("tabs", function () { return _this.$element.find("> ul > li"); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuController.prototype, "$menuTabsOpen", {
        get: function () {
            return this.$menuTabs.filter(".menu-open");
        },
        enumerable: true,
        configurable: true
    });
    NavigationMenuController.prototype.openMenu = function ($menuItem) {
        if (!$menuItem.length) {
            return;
        }
        this.clientState.timestampMenuOpened = Date.now();
        $menuItem
            .addClass("menu-open")
            .find("> a")
            .focus()
            .end()
            .find("> .mega-submenu")
            .fadeIn(this.constants.fadeIn);
    };
    NavigationMenuController.prototype.closeMenu = function ($menuItem) {
        if (!$menuItem.length) {
            return;
        }
        $menuItem
            .removeClass("menu-open")
            .find("> a")
            .blur()
            .end()
            .find("> .mega-submenu")
            .fadeOut(this.constants.fadeOut);
    };
    NavigationMenuController.prototype.unpinMenu = function ($menuItem) {
        this.clientState.menuPinned = false;
        this.closeMenu($menuItem);
    };
    NavigationMenuController.prototype.pinMenu = function ($menuItem) {
        var _this = this;
        this.clientState.menuPinned = true;
        this.closeMenu(this.$menuTabsOpen);
        this.openMenu($menuItem);
        this.clickOutside($menuItem)
            .then(function () { return _this.unpinMenu($menuItem); });
    };
    Object.defineProperty(NavigationMenuController.prototype, "isMenuSecondary", {
        get: function () {
            return this.menuStyle === this.menus.secondary;
        },
        enumerable: true,
        configurable: true
    });
    NavigationMenuController = __decorate([
        __param(0, inject_1.default("$element")),
        __param(1, inject_1.default("$log")),
        __param(2, inject_1.default("$q")),
        __param(3, inject_1.default("$timeout")),
        __param(4, inject_1.default("$window")),
        __param(5, inject_1.default("$document")),
        __param(6, inject_1.default("swApi")),
        __param(7, inject_1.default("constants")),
        __param(8, inject_1.default("viewService")),
        __param(9, inject_1.default("webAdminService")),
        __param(10, inject_1.default("swDemoService")),
        __param(11, inject_1.default("swMegaMenuService")),
        __metadata("design:paramtypes", [Object, Object, Function, Function, Object, Object, Object, Object, Object, Object, Object, Object])
    ], NavigationMenuController);
    return NavigationMenuController;
}());
exports.default = NavigationMenuController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbk1lbnUtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5hdmlnYXRpb25NZW51LWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtQ0FFaUI7QUFNakIscURBQWdEO0FBRWhELGlCQUFpQixJQUE2QixFQUFFLEtBQWE7SUFDekQsSUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQVAsQ0FBTyxDQUFDLENBQUM7SUFDbkQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLEtBQUssS0FBSyxFQUFYLENBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzlDLENBQUM7QUFFRDtJQUdJLGtDQUVZLFFBQTBCLEVBRTFCLElBQXNCLEVBRXRCLEVBQWEsRUFFYixRQUF5QixFQUV6QixPQUF1QixFQUV2QixTQUEyQixFQUUzQixLQUFvQixFQUVwQixXQUF1QixFQUV2QixXQUF5QixFQUV6QixRQUEwQixFQUUxQixhQUEyQixFQUUzQixpQkFBbUM7UUF4Qi9DLGlCQXlCSTtRQXZCUSxhQUFRLEdBQVIsUUFBUSxDQUFrQjtRQUUxQixTQUFJLEdBQUosSUFBSSxDQUFrQjtRQUV0QixPQUFFLEdBQUYsRUFBRSxDQUFXO1FBRWIsYUFBUSxHQUFSLFFBQVEsQ0FBaUI7UUFFekIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFFdkIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFFM0IsVUFBSyxHQUFMLEtBQUssQ0FBZTtRQUVwQixnQkFBVyxHQUFYLFdBQVcsQ0FBWTtRQUV2QixnQkFBVyxHQUFYLFdBQVcsQ0FBYztRQUV6QixhQUFRLEdBQVIsUUFBUSxDQUFrQjtRQUUxQixrQkFBYSxHQUFiLGFBQWEsQ0FBYztRQUUzQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBMUJ2QyxjQUFTLEdBQUcsRUFBeUMsQ0FBQztRQThCdkQsY0FBUyxHQUFXLEVBQUUsQ0FBQztRQWdCdkIsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUU3QyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNwQyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUVELEtBQUksQ0FBQyxRQUFRLEVBQUU7aUJBQ1YsT0FBTyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsRUFBMUIsQ0FBMEIsQ0FBQyxDQUFDO1FBQ25ELENBQUMsQ0FBQztRQUVLLGlCQUFZLEdBQUcsVUFBQyxRQUF1QixJQUFLLE9BQUEsUUFBUSxDQUFDLElBQUksS0FBSyxZQUFZLEVBQTlCLENBQThCLENBQUM7UUFFM0UsZUFBVSxHQUFHLFVBQUMsUUFBdUIsSUFBSyxPQUFBLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQTNELENBQTJELENBQUM7UUFVckcsV0FBTSxHQUFHO1lBQ2IsNEVBQTRFO1lBQzVFLDRDQUE0QztZQUM1QyxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQztnQkFDbkIsUUFBUSxFQUFFLFVBQVUsR0FBcUI7b0JBQ3JDLGlCQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUN6QyxDQUFDO2dCQUNELFVBQVUsRUFBRSxVQUFVLEdBQXFCO29CQUN2QyxpQkFBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDNUMsQ0FBQztnQkFDRCxRQUFRLEVBQUU7b0JBQ04sTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDakIsQ0FBQzthQUNKLENBQUMsQ0FBQztZQUVILElBQU0sa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsU0FBUyxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxRSxJQUFNLGVBQWUsR0FBRyxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1lBRTdFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7bUJBQ3ZGLENBQUMsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVsRixLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM5QixDQUFDLENBQUM7UUFFTSxhQUFRLEdBQUc7WUFDZixPQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUU7aUJBQzVCLElBQUksQ0FBQyxVQUFBLEtBQUs7Z0JBQ1AsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLElBQU0sSUFBSSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3JDLElBQU0sa0JBQWtCLEdBQUcsVUFBQyxJQUFtQixJQUFLLE9BQUEsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssbUJBQW1CLEVBQXpDLENBQXlDLENBQUM7Z0JBRTlGLElBQU0sYUFBYSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQyxTQUFTLElBQUssT0FBQSxTQUFTLENBQUMsSUFBSSxLQUFLLFVBQVUsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRixFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUNoQixhQUFhLENBQUMsS0FBSyxHQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQ3BELFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQzsyQkFDcEYsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsc0JBQXNCOzRCQUNuRCxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxxQkFBcUI7NEJBQzNDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUhyQixDQUdxQixDQUNsQyxDQUFDO2dCQUNOLENBQUM7Z0JBRUQscUZBQXFGO2dCQUNyRixLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBdkMsQ0FBdUMsQ0FBQyxDQUFDO1lBQ3JGLENBQUMsRUFBRSxVQUFDLEtBQVU7Z0JBQ1YsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2dCQUNwQixLQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdEIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QyxDQUFDLENBQUM7UUF2Qk4sQ0F1Qk0sQ0FBQztRQUVILFVBQUssR0FBRyxVQUFDLElBQVksRUFBRSxNQUE4QjtZQUN6RCxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDO1FBQXpELENBQXlELENBQUM7UUE0QjdDLGNBQVMsR0FBRztZQUN6QixRQUFRLEVBQUUsSUFBSTtZQUNkLFVBQVUsRUFBRSxHQUFHO1lBQ2YsWUFBWSxFQUFFLEdBQUc7WUFDakIsTUFBTSxFQUFFLEdBQUc7WUFDWCxPQUFPLEVBQUUsR0FBRztZQUNaLFlBQVksRUFBRSxHQUFHO1lBQ2pCLGFBQWEsRUFBRSxHQUFHO1NBQ3JCLENBQUM7UUFFZSxVQUFLLEdBQUc7WUFDckIsT0FBTyxFQUFFLGNBQWM7WUFDdkIsU0FBUyxFQUFFLGdCQUFnQjtTQUM5QixDQUFDO1FBRU0sZ0JBQVcsR0FBRztZQUNsQixXQUFXLEVBQUUsS0FBSztZQUNsQixTQUFTLEVBQUUsQ0FBQztZQUNaLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFFBQVEsRUFBRSxJQUFzQjtZQUNoQyxtQkFBbUIsRUFBRSxDQUFDO1NBQ3pCLENBQUM7UUFFTSxtQkFBYyxHQUFHO1lBQ3JCLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDaEQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JDLENBQUMsQ0FBQztRQUVNLHVCQUFrQixHQUFHO1lBQ3pCLEtBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQ3pCLFFBQVEsRUFDUixDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxHQUFHLENBQUMsRUFDeEMsS0FBSyxDQUNSLENBQUM7WUFFRixLQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUN6QixRQUFRLEVBQ1IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxDQUFDLEVBQ3ZDLEtBQUssQ0FDUixDQUFDO1lBRUYsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDMUIsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFFMUIsOENBQThDO1lBQzlDLEtBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxVQUFDLEtBQUs7Z0JBQzlELEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxDQUFDO1lBRUgsb0VBQW9FO1lBQ3BFLEtBQUksQ0FBQyxTQUFTO2lCQUNULEVBQUUsQ0FBQyxZQUFZLEVBQUUsVUFBQyxLQUFLO2dCQUNwQixJQUFNLFNBQVMsR0FBRyxpQkFBTyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDL0MsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzdCLE1BQU0sQ0FBQztnQkFDWCxDQUFDO2dCQUNELEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ3RDLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUNuQyxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QixLQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUMvQixDQUFDLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNwQyxDQUFDLENBQUM7aUJBQ0QsRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFDLEtBQUs7Z0JBQ3BCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDOUIsTUFBTSxDQUFDO2dCQUNYLENBQUM7Z0JBQ0QsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN0QixJQUFNLFNBQVMsR0FBRyxpQkFBTyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFFL0MsS0FBSSxDQUFDLFFBQVEsQ0FBQztvQkFDVixFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMxQixLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUM5QixDQUFDO2dCQUNMLENBQUMsRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQztpQkFDRCxFQUFFLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFDLEtBQUs7Z0JBQ3RCLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdEIsSUFBTSxTQUFTLEdBQUcsaUJBQU8sQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUMsTUFBTSxFQUFzQixDQUFDO2dCQUM1RSxJQUFNLE1BQU0sR0FBRyxTQUFTLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUUxQyxFQUFFLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoRixNQUFNLENBQUM7Z0JBQ1gsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNULEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzlCLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osS0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDNUIsQ0FBQztnQkFFRCxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFBO1FBRUQsZ0VBQWdFO1FBQ3hELHVCQUFrQixHQUFHO1lBQ3pCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDcEQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLE9BQUksQ0FBQyxDQUFDO2dCQUN2RSxLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDbEMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBQ3pDLENBQUM7UUFDTCxDQUFDLENBQUE7UUFFRCwyQkFBMkI7UUFDbkIsdUJBQWtCLEdBQUc7WUFDekIsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixJQUFNLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLEtBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ25GLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFLLE1BQU0sT0FBSSxDQUFDLENBQUM7WUFDeEQsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDLENBQUE7UUFFRCxzRUFBc0U7UUFDOUQsaUJBQVksR0FBRyxVQUFDLFFBQTBCO1lBQzlDLElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFRLENBQUM7WUFDdkMsSUFBTSxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUU5QixLQUFLO2lCQUNBLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDO2dCQUNwQixFQUFFLENBQUMsQ0FBQyxpQkFBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ25ELEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ25CLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDbkIsTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDakIsQ0FBQztZQUNMLENBQUMsQ0FBQztpQkFDRCxFQUFFLENBQUMsU0FBUyxFQUFFLFVBQVUsQ0FBQztnQkFDdEIsZ0RBQWdEO2dCQUNoRCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ2pCLEtBQUssQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDbkIsTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDakIsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBRVAsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7UUFDNUIsQ0FBQyxDQUFBO1FBRU8sZUFBVSxHQUFHLFVBQUMsU0FBMkI7WUFDN0MsaUVBQWlFO1lBQ2pFLElBQU0sS0FBSyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQztZQUM3RCxJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRWxDLElBQU0sWUFBWSxHQUFHLGlCQUFPLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3BELElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ2xELEtBQUssQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBRXBDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUNyRCxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDN0IsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUVELHFFQUFxRTtZQUNyRSxJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUMsRUFBRSxJQUFLLE9BQUEsaUJBQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBbkIsQ0FBbUIsQ0FBQyxDQUFDO1lBQ2pFLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLE9BQVIsSUFBSSxFQUFRLFVBQVUsQ0FBQyxDQUFDO1lBQzdDLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLFlBQVksQ0FBQyxDQUFDO1lBQ3pELEVBQUUsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNkLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxDQUFDLENBQUMsT0FBTyxDQUNMLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFDdkMsVUFBQyxLQUFvQjtnQkFDakIsSUFBTSxZQUFZLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFBLEVBQUUsSUFBSSxPQUFBLGlCQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQXBCLENBQW9CLENBQUMsQ0FBQztnQkFDM0QsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsT0FBUixJQUFJLEVBQVEsWUFBWSxDQUFDLENBQUM7Z0JBQzVDLGlCQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDO29CQUNmLE1BQU0sRUFBRSxTQUFTO2lCQUNwQixDQUFDLENBQUM7WUFDUCxDQUFDLENBQ0osQ0FBQztRQUNOLENBQUMsQ0FBQTtRQWlETSxvQkFBZSxHQUFHO1lBQ3JCLG9HQUFvRztZQUNwRyxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FDZCxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsRUFDaEIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQzVCO2dCQUNJLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUVqQyxJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsS0FBSSxDQUFDLEtBQUssRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBRXJELDhCQUE4QjtnQkFDOUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDO3FCQUNuQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7b0JBQ3JCLElBQUksRUFBRSxnQkFBZ0I7b0JBQ3RCLEtBQUssRUFBRSxRQUFRO2lCQUNsQixDQUFDO3FCQUNELE9BQU8sQ0FBQztvQkFDTCxLQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztvQkFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzVCLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxFQUFFLEtBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3RFLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUNKLENBQUM7UUFDTixDQUFDLENBQUE7UUFFTyw4QkFBeUIsR0FBRztZQUNoQyxNQUFNLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzdHLENBQUMsQ0FBQTtJQXRYRSxDQUFDO0lBTUosc0JBQVcsbURBQWE7YUFBeEI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2xELENBQUM7OztPQUFBO0lBRUQsc0JBQVcsK0NBQVM7YUFBcEI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUM7UUFDbEMsQ0FBQzs7O09BQUE7SUFFTSw2Q0FBVSxHQUFqQixVQUFrQixRQUF1QjtRQUNyQyxNQUFNLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO1lBQzFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDckcsQ0FBQztJQWtCTyxpREFBYyxHQUF0QixVQUF1QixLQUFhO1FBQXBDLGlCQU1DO1FBTEcsSUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUMsS0FBSSxDQUFDLEtBQW1DLENBQUMsQ0FBQyxDQUFDLEVBQTVDLENBQTRDLENBQUMsQ0FBQztRQUM5RixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFDRCxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQXVERCxzQkFBWSwyQ0FBSzthQUFqQjtZQUFBLGlCQUVDO1lBREcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxFQUE3QyxDQUE2QyxDQUFDLENBQUM7UUFDbkYsQ0FBQzs7O09BQUE7SUFDRCxzQkFBWSwrQ0FBUzthQUFyQjtZQUFBLGlCQUVDO1lBREcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxFQUFsRCxDQUFrRCxDQUFDLENBQUM7UUFDNUYsQ0FBQzs7O09BQUE7SUFDRCxzQkFBWSxnREFBVTthQUF0QjtZQUFBLGlCQUVDO1lBREcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO1FBQ3RFLENBQUM7OztPQUFBO0lBQ0Qsc0JBQVksMkNBQUs7YUFBakI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsY0FBTSxPQUFBLGlCQUFPLENBQUMsVUFBVSxDQUFDLEVBQW5CLENBQW1CLENBQUMsQ0FBQztRQUN6RCxDQUFDOzs7T0FBQTtJQUNELHNCQUFZLHlEQUFtQjthQUEvQjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLG9CQUFvQixFQUFFLGNBQU0sT0FBQSxpQkFBTyxDQUFDLGNBQWMsQ0FBQyxFQUF2QixDQUF1QixDQUFDLENBQUM7UUFDM0UsQ0FBQzs7O09BQUE7SUFDRCxzQkFBWSxrREFBWTthQUF4QjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxjQUFNLE9BQUEsaUJBQU8sQ0FBQyxXQUFXLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO1FBQ2pFLENBQUM7OztPQUFBO0lBQ0Qsc0JBQVksK0NBQVM7YUFBckI7WUFBQSxpQkFFQztZQURHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxjQUFNLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQS9CLENBQStCLENBQUMsQ0FBQztRQUNyRSxDQUFDOzs7T0FBQTtJQUVELHNCQUFZLG1EQUFhO2FBQXpCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBcUIsQ0FBQztRQUNuRSxDQUFDOzs7T0FBQTtJQW9MTywyQ0FBUSxHQUFoQixVQUFpQixTQUEyQjtRQUN4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUVsRCxTQUFTO2FBQ0osUUFBUSxDQUFDLFdBQVcsQ0FBQzthQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDO2FBQ1AsS0FBSyxFQUFFO2FBQ1AsR0FBRyxFQUFFO2FBQ1QsSUFBSSxDQUFDLGlCQUFpQixDQUFDO2FBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTyw0Q0FBUyxHQUFqQixVQUFrQixTQUEyQjtRQUN6QyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxTQUFTO2FBQ0osV0FBVyxDQUFDLFdBQVcsQ0FBQzthQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDO2FBQ1AsSUFBSSxFQUFFO2FBQ04sR0FBRyxFQUFFO2FBQ1QsSUFBSSxDQUFDLGlCQUFpQixDQUFDO2FBQ3ZCLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFTyw0Q0FBUyxHQUFqQixVQUFrQixTQUEyQjtRQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRU8sMENBQU8sR0FBZixVQUFnQixTQUEyQjtRQUEzQyxpQkFPQztRQU5HLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXpCLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2FBQ3ZCLElBQUksQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCxzQkFBVyxxREFBZTthQUExQjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO1FBQ25ELENBQUM7OztPQUFBO0lBclhnQix3QkFBd0I7UUFJcEMsV0FBQSxnQkFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBRWxCLFdBQUEsZ0JBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUVkLFdBQUEsZ0JBQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUVaLFdBQUEsZ0JBQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUVsQixXQUFBLGdCQUFNLENBQUMsU0FBUyxDQUFDLENBQUE7UUFFakIsV0FBQSxnQkFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRW5CLFdBQUEsZ0JBQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUVmLFdBQUEsZ0JBQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUVuQixXQUFBLGdCQUFNLENBQUMsYUFBYSxDQUFDLENBQUE7UUFFckIsV0FBQSxnQkFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUE7UUFFekIsWUFBQSxnQkFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBRXZCLFlBQUEsZ0JBQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBOztPQTFCZix3QkFBd0IsQ0FtWjVDO0lBQUQsK0JBQUM7Q0FBQSxBQW5aRCxJQW1aQztrQkFuWm9CLHdCQUF3QiJ9

/***/ }),
/* 236 */
/***/ (function(module, exports) {

module.exports = angular;

/***/ }),
/* 237 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionSearch_component_1 = __webpack_require__(238);
function orionSearch(module) {
    module.component("swOrionSearchComponent", new orionSearch_component_1.OrionSearchComponent());
}
exports.orionSearch = orionSearch;
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlFQUErRDtBQUUvRCxxQkFBNEIsTUFBZTtJQUN2QyxNQUFNLENBQUMsU0FBUyxDQUFDLHdCQUF3QixFQUFFLElBQUksNENBQW9CLEVBQUUsQ0FBQyxDQUFDO0FBQzNFLENBQUM7QUFGRCxrQ0FFQztBQUFBLENBQUMifQ==

/***/ }),
/* 238 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(239);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var orionSearch_controller_1 = __webpack_require__(240);
var OrionSearchComponent = /** @class */ (function () {
    function OrionSearchComponent() {
        this.template = __webpack_require__(16);
        this.controller = orionSearch_controller_1.OrionSearchController;
        this.controllerAs = "vm";
    }
    return OrionSearchComponent;
}());
exports.OrionSearchComponent = OrionSearchComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25TZWFyY2gtY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib3Jpb25TZWFyY2gtY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsbUVBQWlFO0FBRWpFO0lBQUE7UUFDVyxhQUFRLEdBQUcsT0FBTyxDQUFTLDhCQUE4QixDQUFDLENBQUM7UUFDM0QsZUFBVSxHQUFHLDhDQUFxQixDQUFDO1FBQ25DLGlCQUFZLEdBQUcsSUFBSSxDQUFDO0lBQy9CLENBQUM7SUFBRCwyQkFBQztBQUFELENBQUMsQUFKRCxJQUlDO0FBSlksb0RBQW9CIn0=

/***/ }),
/* 239 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 240 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var searchPageUrl = "/ui/search";
var OrionSearchController = /** @class */ (function () {
    function OrionSearchController($window, $log, $q, $element, searchHistoryService) {
        var _this = this;
        this.$window = $window;
        this.$log = $log;
        this.$q = $q;
        this.$element = $element;
        this.searchHistoryService = searchHistoryService;
        this.isOpen = false;
        this.$onInit = function () {
            _this.$element.addClass("sw-orion-search-component");
        };
    }
    OrionSearchController.prototype.onSearch = function (value) {
        this.searchValue = value;
        if (!this.searchValue) {
            return;
        }
        this.searchValue = this.searchValue.trim();
        this.searchHistoryService.addToHistory(this.searchValue);
        this.redirectToSearch(this.searchValue);
        return this.$q.defer();
    };
    OrionSearchController.prototype.onToggle = function (isOpen) {
        var _this = this;
        if (isOpen) {
            this.searchHistoryService.getHistory(5).then(function (history) {
                _this.searchHistory = history;
                var toBeFocused = _this.$element.find(".xui-search__input-control");
                toBeFocused.focus();
            });
        }
        this.isOpen = isOpen;
    };
    OrionSearchController.prototype.redirectToSearch = function (searchValue) {
        var url = this.$window.location.origin + searchPageUrl + "?q=" + searchValue;
        this.$window.open(url, "_self");
    };
    OrionSearchController.$inject = ["$window", "$log", "$q", "$element", "swSearchHistoryService"];
    return OrionSearchController;
}());
exports.OrionSearchController = OrionSearchController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25TZWFyY2gtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9yaW9uU2VhcmNoLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFRQSxJQUFNLGFBQWEsR0FBVyxZQUFZLENBQUM7QUFFM0M7SUFNSSwrQkFDWSxPQUF1QixFQUN2QixJQUFpQixFQUNqQixFQUFhLEVBQ2IsUUFBMEIsRUFDMUIsb0JBQTJDO1FBTHZELGlCQU1LO1FBTE8sWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFDdkIsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNqQixPQUFFLEdBQUYsRUFBRSxDQUFXO1FBQ2IsYUFBUSxHQUFSLFFBQVEsQ0FBa0I7UUFDMUIseUJBQW9CLEdBQXBCLG9CQUFvQixDQUF1QjtRQVRoRCxXQUFNLEdBQVksS0FBSyxDQUFDO1FBWXhCLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDO0lBSkUsQ0FBQztJQU1FLHdDQUFRLEdBQWYsVUFBZ0IsS0FBYTtRQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFM0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4QyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRU0sd0NBQVEsR0FBZixVQUFnQixNQUFlO1FBQS9CLGlCQVVDO1FBVEcsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNULElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsT0FBc0I7Z0JBQ2hFLEtBQUksQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDO2dCQUM3QixJQUFNLFdBQVcsR0FBcUIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQztnQkFDdkYsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3pCLENBQUM7SUFDTyxnREFBZ0IsR0FBeEIsVUFBeUIsV0FBbUI7UUFDeEMsSUFBTSxHQUFHLEdBQVcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLGFBQWEsR0FBRyxLQUFLLEdBQUcsV0FBVyxDQUFDO1FBQ3ZGLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBM0NhLDZCQUFPLEdBQUcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsd0JBQXdCLENBQUMsQ0FBQztJQTRDNUYsNEJBQUM7Q0FBQSxBQTdDRCxJQTZDQztBQTdDWSxzREFBcUIifQ==

/***/ }),
/* 241 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var notifications_component_1 = __webpack_require__(242);
function notifications(module) {
    module.component("swNotificationsComponent", new notifications_component_1.NotificationsComponent());
}
exports.notifications = notifications;
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFFQUFtRTtBQUVuRSx1QkFBK0IsTUFBZTtJQUMxQyxNQUFNLENBQUMsU0FBUyxDQUFDLDBCQUEwQixFQUFFLElBQUksZ0RBQXNCLEVBQUUsQ0FBQyxDQUFDO0FBQy9FLENBQUM7QUFGRCxzQ0FFQztBQUFBLENBQUMifQ==

/***/ }),
/* 242 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(243);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var notifications_controller_1 = __webpack_require__(244);
var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent() {
        this.template = __webpack_require__(15);
        this.controller = notifications_controller_1.NotificationsController;
    }
    return NotificationsComponent;
}());
exports.NotificationsComponent = NotificationsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy1jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJub3RpZmljYXRpb25zLWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVFQUFxRTtBQUVyRTtJQUFBO1FBQ1csYUFBUSxHQUFHLE9BQU8sQ0FBUyxnQ0FBZ0MsQ0FBQyxDQUFDO1FBQzdELGVBQVUsR0FBRyxrREFBdUIsQ0FBQztJQUNoRCxDQUFDO0lBQUQsNkJBQUM7QUFBRCxDQUFDLEFBSEQsSUFHQztBQUhZLHdEQUFzQiJ9

/***/ }),
/* 243 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 244 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NotificationsController = /** @class */ (function () {
    /** @ngInject */
    NotificationsController.$inject = ["notifyService"];
    function NotificationsController(notifyService) {
        var _this = this;
        this.notifyService = notifyService;
        this.$onInit = function () {
            _this.updateNotifications();
        };
        this.isOpen = false;
        this.notifications = [];
        this.updateNotifications = function () {
            _this.notifyService.checkForUpdates().then(function () {
                _this.notifications = _this.notifyService.getNotifications();
            });
        };
        this.dismissNotification = function ($event, notification) {
            $event.stopPropagation();
            _this.notifyService.dismissNotification(notification).then(function () {
                _this.updateNotifications();
            });
        };
        this.dismissAllNotifications = function ($event) {
            $event.stopPropagation();
            _this.notifyService.dismissAllNotifications().then(function () {
                _this.updateNotifications();
            });
        };
        this.onToggle = function (isOpen) { return _this.isOpen = isOpen; };
        this.getCountText = function () { return "" + _this.notifications.length; };
    }
    Object.defineProperty(NotificationsController.prototype, "isError", {
        get: function () {
            return this.notifyService.hasError();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NotificationsController.prototype, "isEmpty", {
        get: function () {
            return (this.notifications.length === 0);
        },
        enumerable: true,
        configurable: true
    });
    return NotificationsController;
}());
exports.NotificationsController = NotificationsController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9ucy1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0E7SUFDSSxnQkFBZ0I7SUFDaEIsaUNBQ1ksYUFBNkI7UUFEekMsaUJBR0M7UUFGVyxrQkFBYSxHQUFiLGFBQWEsQ0FBZ0I7UUFJbEMsWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBRUssV0FBTSxHQUFZLEtBQUssQ0FBQztRQUV4QixrQkFBYSxHQUFvQixFQUFFLENBQUM7UUFVbkMsd0JBQW1CLEdBQUc7WUFDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQy9ELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUcsVUFBQyxNQUFxQixFQUFFLFlBQTJCO1lBQzVFLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN6QixLQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdEQsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFDL0IsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyw0QkFBdUIsR0FBRyxVQUFDLE1BQXFCO1lBQ25ELE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN6QixLQUFJLENBQUMsYUFBYSxDQUFDLHVCQUF1QixFQUFFLENBQUMsSUFBSSxDQUFDO2dCQUM5QyxLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztZQUMvQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVLLGFBQVEsR0FBRyxVQUFDLE1BQWUsSUFBSyxPQUFBLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxFQUFwQixDQUFvQixDQUFDO1FBRXJELGlCQUFZLEdBQUcsY0FBYyxPQUFBLEtBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFRLEVBQTlCLENBQThCLENBQUM7SUF4Q25FLENBQUM7SUFVRCxzQkFBVyw0Q0FBTzthQUFsQjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3pDLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsNENBQU87YUFBbEI7WUFDSSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQztRQUM3QyxDQUFDOzs7T0FBQTtJQXlCTCw4QkFBQztBQUFELENBQUMsQUE5Q0QsSUE4Q0M7QUE5Q1ksMERBQXVCIn0=

/***/ }),
/* 245 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var evaluationBanner_component_1 = __webpack_require__(246);
function evaluationBanner(module) {
    module.component("swEvaluationBanner", new evaluationBanner_component_1.EvaluationBanner());
}
exports.evaluationBanner = evaluationBanner;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJFQUFnRTtBQUVoRSwwQkFBaUMsTUFBZTtJQUM1QyxNQUFNLENBQUMsU0FBUyxDQUFDLG9CQUFvQixFQUFFLElBQUksNkNBQWdCLEVBQUUsQ0FBQyxDQUFDO0FBQ25FLENBQUM7QUFGRCw0Q0FFQyJ9

/***/ }),
/* 246 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(247);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var evaluationBanner_controller_1 = __webpack_require__(248);
var EvaluationBanner = /** @class */ (function () {
    function EvaluationBanner() {
        this.template = __webpack_require__(10);
        this.controller = evaluationBanner_controller_1.EvaluationBannerController;
    }
    return EvaluationBanner;
}());
exports.EvaluationBanner = EvaluationBanner;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZhbHVhdGlvbkJhbm5lci1jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJldmFsdWF0aW9uQmFubmVyLWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDZFQUEyRTtBQUUzRTtJQUFBO1FBQ1csYUFBUSxHQUFHLE9BQU8sQ0FBUyxtQ0FBbUMsQ0FBQyxDQUFDO1FBQ2hFLGVBQVUsR0FBRyx3REFBMEIsQ0FBQztJQUNuRCxDQUFDO0lBQUQsdUJBQUM7QUFBRCxDQUFDLEFBSEQsSUFHQztBQUhZLDRDQUFnQiJ9

/***/ }),
/* 247 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 248 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var EvaluationBannerController = /** @class */ (function () {
    /** @ngInject */
    EvaluationBannerController.$inject = ["swLicenseService", "swSalesTriggerDialogService", "constants"];
    function EvaluationBannerController(swLicenseService, swSalesTriggerDialogService, constants) {
        var _this = this;
        this.swLicenseService = swLicenseService;
        this.swSalesTriggerDialogService = swSalesTriggerDialogService;
        this.constants = constants;
        this.$onInit = function () {
            _this.evaluatedModuleCount = _this.constants.license.EvalModuleCount;
            _this.expiredModuleCount = _this.constants.license.ExpiredModuleCount;
        };
        this.showDetailsDialog = function () {
            _this.swSalesTriggerDialogService.show("Eval");
        };
        this.isVisible = function () {
            return (_this.evaluatedModuleCount > 0 || _this.expiredModuleCount > 0);
        };
    }
    Object.defineProperty(EvaluationBannerController.prototype, "paramEval", {
        get: function () {
            return [this.evaluatedModuleCount || 0].map(String);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EvaluationBannerController.prototype, "paramExpired", {
        get: function () {
            return [this.expiredModuleCount || 0].map(String);
        },
        enumerable: true,
        configurable: true
    });
    return EvaluationBannerController;
}());
exports.EvaluationBannerController = EvaluationBannerController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZhbHVhdGlvbkJhbm5lci1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZXZhbHVhdGlvbkJhbm5lci1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsb0NBQ1ksZ0JBQWlDLEVBQ2pDLDJCQUF1RCxFQUN2RCxTQUFxQjtRQUhqQyxpQkFJSztRQUhPLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFDakMsZ0NBQTJCLEdBQTNCLDJCQUEyQixDQUE0QjtRQUN2RCxjQUFTLEdBQVQsU0FBUyxDQUFZO1FBYzFCLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUM7WUFDbkUsS0FBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDO1FBQ3hFLENBQUMsQ0FBQztRQUVLLHNCQUFpQixHQUFHO1lBQ3ZCLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEQsQ0FBQyxDQUFDO1FBRUssY0FBUyxHQUFHO1lBQ2YsT0FBQSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxDQUFDLElBQUksS0FBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQztRQUE5RCxDQUE4RCxDQUFDO0lBdkIvRCxDQUFDO0lBS0wsc0JBQVcsaURBQVM7YUFBcEI7WUFDSSxNQUFNLENBQUMsQ0FBRSxJQUFJLENBQUMsb0JBQW9CLElBQUksQ0FBQyxDQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFELENBQUM7OztPQUFBO0lBRUQsc0JBQVcsb0RBQVk7YUFBdkI7WUFDSSxNQUFNLENBQUMsQ0FBRSxJQUFJLENBQUMsa0JBQWtCLElBQUksQ0FBQyxDQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hELENBQUM7OztPQUFBO0lBYUwsaUNBQUM7QUFBRCxDQUFDLEFBOUJELElBOEJDO0FBOUJZLGdFQUEwQiJ9

/***/ }),
/* 249 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dashboardEditPanel_component_1 = __webpack_require__(250);
function dashboardEditPanel(module) {
    module.component("swDashboardEditPanel", new dashboardEditPanel_component_1.DashboardEditPanelComponent());
}
exports.dashboardEditPanel = dashboardEditPanel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtFQUE2RTtBQUU3RSw0QkFBbUMsTUFBZTtJQUM5QyxNQUFNLENBQUMsU0FBUyxDQUFDLHNCQUFzQixFQUFFLElBQUksMERBQTJCLEVBQUUsQ0FBQyxDQUFDO0FBQ2hGLENBQUM7QUFGRCxnREFFQyJ9

/***/ }),
/* 250 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(251);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dashboardEditPanel_controller_1 = __webpack_require__(252);
var DashboardEditPanelComponent = /** @class */ (function () {
    function DashboardEditPanelComponent() {
        this.template = __webpack_require__(9);
        this.controller = dashboardEditPanel_controller_1.DashboardEditPanelController;
    }
    return DashboardEditPanelComponent;
}());
exports.DashboardEditPanelComponent = DashboardEditPanelComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkRWRpdFBhbmVsLWNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhc2hib2FyZEVkaXRQYW5lbC1jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxpRkFBK0U7QUFFL0U7SUFBQTtRQUNXLGFBQVEsR0FBRyxPQUFPLENBQVMscUNBQXFDLENBQUMsQ0FBQztRQUNsRSxlQUFVLEdBQUcsNERBQTRCLENBQUM7SUFDckQsQ0FBQztJQUFELGtDQUFDO0FBQUQsQ0FBQyxBQUhELElBR0M7QUFIWSxrRUFBMkIifQ==

/***/ }),
/* 251 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 252 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DashboardEditPanelController = /** @class */ (function () {
    function DashboardEditPanelController() {
    }
    return DashboardEditPanelController;
}());
exports.DashboardEditPanelController = DashboardEditPanelController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkRWRpdFBhbmVsLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkYXNoYm9hcmRFZGl0UGFuZWwtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQUE7SUFDQSxDQUFDO0lBQUQsbUNBQUM7QUFBRCxDQUFDLEFBREQsSUFDQztBQURZLG9FQUE0QiJ9

/***/ }),
/* 253 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var errorInspector_component_1 = __webpack_require__(254);
var errorInspector_service_1 = __webpack_require__(257);
function errorInspector(module) {
    module.component("swErrorInspector", new errorInspector_component_1.ErrorInspectorComponent());
    module.service("swErrorInspectorService", errorInspector_service_1.ErrorInspectorService);
}
exports.errorInspector = errorInspector;
exports.default = errorInspector;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVFQUFxRTtBQUNyRSxtRUFBaUU7QUFFakUsd0JBQStCLE1BQWU7SUFDMUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLGtEQUF1QixFQUFFLENBQUMsQ0FBQztJQUNwRSxNQUFNLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLDhDQUFxQixDQUFDLENBQUM7QUFDckUsQ0FBQztBQUhELHdDQUdDO0FBRUQsa0JBQWUsY0FBYyxDQUFDIn0=

/***/ }),
/* 254 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(255);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var errorInspector_controller_1 = __webpack_require__(256);
var ErrorInspectorComponent = /** @class */ (function () {
    function ErrorInspectorComponent() {
        this.template = __webpack_require__(8);
        this.controller = errorInspector_controller_1.ErrorInspectorController;
        this.bindings = {
            "error": "<",
        };
    }
    return ErrorInspectorComponent;
}());
exports.ErrorInspectorComponent = ErrorInspectorComponent;
exports.default = ErrorInspectorComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3JJbnNwZWN0b3ItY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZXJyb3JJbnNwZWN0b3ItY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EseUVBQXVFO0FBRXZFO0lBQUE7UUFDVyxhQUFRLEdBQUcsT0FBTyxDQUFTLGlDQUFpQyxDQUFDLENBQUM7UUFDOUQsZUFBVSxHQUFHLG9EQUF3QixDQUFDO1FBQ3RDLGFBQVEsR0FBRztZQUNkLE9BQU8sRUFBRSxHQUFHO1NBQ2YsQ0FBQztJQUNOLENBQUM7SUFBRCw4QkFBQztBQUFELENBQUMsQUFORCxJQU1DO0FBTlksMERBQXVCO0FBUXBDLGtCQUFlLHVCQUF1QixDQUFDIn0=

/***/ }),
/* 255 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 256 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var ErrorInspectorController = /** @class */ (function () {
    function ErrorInspectorController(_t, xuiToastService) {
        var _this = this;
        this._t = _t;
        this.xuiToastService = xuiToastService;
        this.copySuccess = function () {
            _this.xuiToastService.info(_this._t("Error details have been copied to the clipboard."), _this._t("Copied"));
        };
    }
    Object.defineProperty(ErrorInspectorController.prototype, "message", {
        get: function () {
            return this.error && this.error.message;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ErrorInspectorController.prototype, "detail", {
        get: function () {
            return this.error && this.error.detail;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ErrorInspectorController.prototype, "hints", {
        get: function () {
            return this.error && this.error.hints;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ErrorInspectorController.prototype, "hasError", {
        get: function () {
            return !!this.detail;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ErrorInspectorController.prototype, "hasHints", {
        get: function () {
            return this.hints && this.hints.length > 0;
        },
        enumerable: true,
        configurable: true
    });
    ErrorInspectorController = __decorate([
        __param(0, decorators_1.Inject("getTextService")),
        __param(1, decorators_1.Inject("xuiToastService")),
        __metadata("design:paramtypes", [Function, Object])
    ], ErrorInspectorController);
    return ErrorInspectorController;
}());
exports.ErrorInspectorController = ErrorInspectorController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3JJbnNwZWN0b3ItY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVycm9ySW5zcGVjdG9yLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFFQSwrQ0FBMEM7QUFFMUM7SUFDSSxrQ0FFWSxFQUFvQyxFQUVwQyxlQUFpQztRQUo3QyxpQkFNQztRQUpXLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBRXBDLG9CQUFlLEdBQWYsZUFBZSxDQUFrQjtRQTBCdEMsZ0JBQVcsR0FBRztZQUNqQixLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FDckIsS0FBSSxDQUFDLEVBQUUsQ0FBQyxrREFBa0QsQ0FBQyxFQUMzRCxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUNwQixDQUFDO1FBQ04sQ0FBQyxDQUFBO0lBN0JELENBQUM7SUFJRCxzQkFBVyw2Q0FBTzthQUFsQjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzVDLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsNENBQU07YUFBakI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUMzQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFXLDJDQUFLO2FBQWhCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDMUMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVyw4Q0FBUTthQUFuQjtZQUNJLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN6QixDQUFDOzs7T0FBQTtJQUVELHNCQUFXLDhDQUFRO2FBQW5CO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLENBQUM7OztPQUFBO0lBN0JRLHdCQUF3QjtRQUU1QixXQUFBLG1CQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUV4QixXQUFBLG1CQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQTs7T0FKckIsd0JBQXdCLENBcUNwQztJQUFELCtCQUFDO0NBQUEsQUFyQ0QsSUFxQ0M7QUFyQ1ksNERBQXdCIn0=

/***/ }),
/* 257 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = __webpack_require__(0);
var ErrorInspectorService = /** @class */ (function () {
    function ErrorInspectorService(_t) {
        this._t = _t;
        this.resources = this.initResources();
        this.initResources();
    }
    ErrorInspectorService.prototype.formatResponse = function (response) {
        if (!response) {
            return;
        }
        var result = {};
        result.message = response.data.exceptionMessage || response.data.message;
        result.detail = JSON.stringify(response, null, "\t");
        result.hints = this.guessHints(response);
        return result;
    };
    ErrorInspectorService.prototype.guessHints = function (response) {
        var hints = [];
        if (!response || !response.data || !response.data.exceptionType) {
            return hints;
        }
        if (response.data.exceptionType === "System.ServiceModel.EndpointNotFoundException") {
            hints.push(this.resources.connection);
            var rxSwis3 = new RegExp("/SolarWinds/InformationService/v3");
            var rxSwis2 = new RegExp("/SolarWinds/InformationService");
            if (rxSwis3.test(response.data.errorMessage)) {
                hints.push(this.resources.swis3);
            }
            else if (rxSwis2.test(response.data.errorMessage)) {
                hints.push(this.resources.swis2);
            }
            else {
                hints.push(this.resources.swis3, this.resources.bizhost);
            }
        }
        hints.push(this.resources.hint1, this.resources.hint2, this.resources.hint3, this.resources.final);
        return hints;
    };
    ErrorInspectorService.prototype.initResources = function () {
        var services = this._t("On your primary SolarWinds server, click <b>Start &gt; Administrative Tools &gt; Services</b>.");
        return {
            connection: this._t("Confirm that the local SolarWinds server can communicate with the primary SolarWinds server on TCP port 17777."),
            bizhost: this._t("Confirm that the SolarWinds Orion Module Engine service is running.")
                + ("<ul><li>" + services + "</li><li>")
                + this._t("Right-click <b>SolarWinds Orion Module Engine</b>, and then click <b>Start</b>.")
                + "</li></ul>",
            swis2: this._t("Confirm that the SolarWinds Information Service is running.")
                + ("<ul><li>" + services + "</li><li>")
                + this._t("Right-click <b>SolarWinds Information Service</b>, and then click <b>Start</b>.")
                + "</li></ul>",
            swis3: this._t("Confirm that the SolarWinds Information Service is running.")
                + ("<ul><li>" + services + "</li><li>")
                + this._t("Right-click <b>SolarWinds Information Service V3</b>, and then click <b>Start</b>.")
                + "</li></ul>",
            hint1: this._t("Examine the specific details of the error for environmental hints to help resolve your issue."),
            hint2: this._t("Refresh the web console browser."),
            hint3: this._t("If still unresolved, click the troubleshooting help links below."),
            final: this._t("After completing any changes, repeat the same task that generated this error."),
        };
    };
    ErrorInspectorService = __decorate([
        __param(0, decorators_1.Inject("getTextService")),
        __metadata("design:paramtypes", [Function])
    ], ErrorInspectorService);
    return ErrorInspectorService;
}());
exports.ErrorInspectorService = ErrorInspectorService;
exports.default = ErrorInspectorService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3JJbnNwZWN0b3Itc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVycm9ySW5zcGVjdG9yLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFFQSwrQ0FBMEM7QUFFMUM7SUFDSSwrQkFFWSxFQUFvQztRQUFwQyxPQUFFLEdBQUYsRUFBRSxDQUFrQztRQThDeEMsY0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQTVDckMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFTSw4Q0FBYyxHQUFyQixVQUFzQixRQUFtQjtRQUNyQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDWixNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsSUFBTSxNQUFNLEdBQWUsRUFBRSxDQUFDO1FBQzlCLE1BQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN6RSxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRCxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRU8sMENBQVUsR0FBbEIsVUFBbUIsUUFBbUI7UUFDbEMsSUFBTSxLQUFLLEdBQWEsRUFBRSxDQUFDO1FBRTNCLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUM5RCxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSywrQ0FBK0MsQ0FBQyxDQUFDLENBQUM7WUFDbEYsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RDLElBQU0sT0FBTyxHQUFHLElBQUksTUFBTSxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFDaEUsSUFBTSxPQUFPLEdBQUcsSUFBSSxNQUFNLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztZQUM3RCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM3RCxDQUFDO1FBQ0wsQ0FBQztRQUVELEtBQUssQ0FBQyxJQUFJLENBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQ3BCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQ3ZCLENBQUM7UUFFRixNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFJTyw2Q0FBYSxHQUFyQjtRQUNJLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQ3BCLGdHQUFnRyxDQUNuRyxDQUFDO1FBQ0YsTUFBTSxDQUFDO1lBQ0gsVUFBVSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsZ0hBQWdILENBQUM7WUFDckksT0FBTyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMscUVBQXFFLENBQUM7bUJBQ2pGLGFBQVcsUUFBUSxjQUFXLENBQUE7a0JBQzlCLElBQUksQ0FBQyxFQUFFLENBQUMsaUZBQWlGLENBQUM7a0JBQzFGLFlBQVk7WUFDbEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsNkRBQTZELENBQUM7bUJBQ3ZFLGFBQVcsUUFBUSxjQUFXLENBQUE7a0JBQzlCLElBQUksQ0FBQyxFQUFFLENBQUMsaUZBQWlGLENBQUM7a0JBQzFGLFlBQVk7WUFDbEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsNkRBQTZELENBQUM7bUJBQ3ZFLGFBQVcsUUFBUSxjQUFXLENBQUE7a0JBQzlCLElBQUksQ0FBQyxFQUFFLENBQUMsb0ZBQW9GLENBQUM7a0JBQzdGLFlBQVk7WUFDbEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsK0ZBQStGLENBQUM7WUFDL0csS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsa0NBQWtDLENBQUM7WUFDbEQsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsa0VBQWtFLENBQUM7WUFDbEYsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsK0VBQStFLENBQUM7U0FDbEcsQ0FBQztJQUNOLENBQUM7SUExRVEscUJBQXFCO1FBRXpCLFdBQUEsbUJBQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBOztPQUZwQixxQkFBcUIsQ0E0RWpDO0lBQUQsNEJBQUM7Q0FBQSxBQTVFRCxJQTRFQztBQTVFWSxzREFBcUI7QUE4RWxDLGtCQUFlLHFCQUFxQixDQUFDIn0=

/***/ })
/******/ ]);
//# sourceMappingURL=apollo-website.js.map