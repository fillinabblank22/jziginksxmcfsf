/*!
 * @solarwinds/i18n-gettext 6.1.0-1440
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(6);
var mod = angular.module("i18n-gettext-tools", [
    "pascalprecht.translate",
]).config(["$translateProvider", "constants",
    function ($translateProvider, constants) {
        $translateProvider.useMissingTranslationHandler("missingTranslationFactory");
        $translateProvider.useInterpolation("$translateNoInterpolation");
    }]).factory("missingTranslationFactory", function () {
    return function () {
        return "\u2718";
    };
}).factory("$translateNoInterpolation", function () {
    return {
        setLocale: function () { return undefined; },
        interpolate: function (text) { return text; }
    };
});
index_1.default(mod);
index_2.default(mod);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDRDQUE0QztBQUM1QywwQ0FBd0M7QUFFeEMsSUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRTtJQUN2Qyx3QkFBd0I7Q0FDM0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLG9CQUFvQixFQUFFLFdBQVc7SUFDNUMsVUFBQyxrQkFBd0QsRUFBRSxTQUFjO1FBRXJFLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDN0Usa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsMkJBQTJCLENBQUMsQ0FBQztJQUVyRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQywyQkFBMkIsRUFBRTtJQUNyQyxNQUFNLENBQUM7UUFDSCxNQUFNLENBQUMsUUFBRyxDQUFDO0lBQ2YsQ0FBQyxDQUFDO0FBQ04sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLDJCQUEyQixFQUFFO0lBQ3BDLE1BQU0sQ0FBQztRQUNILFNBQVMsRUFBRSxjQUFZLE9BQUEsU0FBUyxFQUFULENBQVM7UUFDaEMsV0FBVyxFQUFFLFVBQUMsSUFBUyxJQUFZLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0tBQ3BELENBQUM7QUFDTixDQUFDLENBQUMsQ0FBQztBQUVQLGVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNoQixlQUFRLENBQUMsR0FBRyxDQUFDLENBQUMifQ==

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var gettext_directive_1 = __webpack_require__(4);
exports.default = function (mod) {
    mod.directive("t", ["getTextService", "$parse", "$interpolate", gettext_directive_1.GetText.getTextDirectiveFactory])
        .directive("ta", ["getTextService", "$parse", "$interpolate", gettext_directive_1.GetText.getTextAttributeDirectiveFactory]);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHlEQUE4QztBQUU5QyxrQkFBZSxVQUFDLEdBQVE7SUFDcEIsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFLDJCQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQztTQUM3RixTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLGNBQWMsRUFBRSwyQkFBTyxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQztBQUNoSCxDQUFDLENBQUMifQ==

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../typings/ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var utils = __webpack_require__(5);
var GetText;
(function (GetText) {
    function processParams(params, context, $parse, $interpolate, callback) {
        context.$watch(function () {
            var parsed = $parse(params)(context);
            callback($.map(parsed, function (item) {
                return $interpolate(item)(context);
            }));
        });
    }
    function getTextDirectiveFactory(getTextService, $parse, $interpolate) {
        return {
            restrict: "AE",
            scope: false,
            compile: function (tElement, tAttributes, transclude) {
                var text = tElement.html();
                var newText = getTextService(text);
                var params = tAttributes["t"];
                if (params) {
                    return {
                        pre: function (scope, instanceElement, instanceAttributes) {
                            processParams(params, scope, $parse, $interpolate, function (parsed) {
                                var formattedText = utils.format(newText, parsed);
                                instanceElement.html(formattedText);
                            });
                        }
                    };
                }
                tElement.html(newText);
            }
        };
    }
    GetText.getTextDirectiveFactory = getTextDirectiveFactory;
    function getTextAttributeDirectiveFactory(getTextService, $parse, $interpolate) {
        return {
            restrict: "A",
            scope: false,
            compile: function (tElement, tAttributes, transclude) {
                var params = tAttributes["ta"];
                if (params) {
                    return {
                        pre: function (scope, instanceElement, instanceAttributes) {
                            processParams(params, scope, $parse, $interpolate, function (parsed) {
                                for (var attribute in instanceAttributes) {
                                    if (instanceAttributes.hasOwnProperty(attribute)) {
                                        var attValue = instanceAttributes[attribute];
                                        if (attValue &&
                                            typeof attValue === "string" &&
                                            attValue.indexOf("_t(") === 0 &&
                                            attValue.lastIndexOf(")") === attValue.length - 1) {
                                            var text = attValue.substring(3, attValue.length - 1);
                                            var newText = utils.format(getTextService(text), parsed);
                                            instanceAttributes.$set(attribute, newText);
                                        }
                                    }
                                }
                            });
                        }
                    };
                }
                else {
                    for (var attribute in tAttributes) {
                        if (tAttributes.hasOwnProperty(attribute)) {
                            var attValue = tAttributes[attribute];
                            if (attValue &&
                                typeof attValue === "string" &&
                                attValue.indexOf("_t(") === 0 &&
                                attValue.lastIndexOf(")") === attValue.length - 1) {
                                var text = attValue.substring(3, attValue.length - 1);
                                var newText = getTextService(text);
                                tAttributes.$set(attribute, newText);
                            }
                        }
                    }
                }
            }
        };
    }
    GetText.getTextAttributeDirectiveFactory = getTextAttributeDirectiveFactory;
})(GetText = exports.GetText || (exports.GetText = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0dGV4dC1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJnZXR0ZXh0LWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsK0NBQStDOztBQUcvQyx1Q0FBeUM7QUFFekMsSUFBYyxPQUFPLENBcUdwQjtBQXJHRCxXQUFjLE9BQU87SUFDakIsdUJBQ0ksTUFBVyxFQUNYLE9BQXVCLEVBQ3ZCLE1BQTZCLEVBQzdCLFlBQXlDLEVBQ3pDLFFBQWE7UUFDYixPQUFPLENBQUMsTUFBTSxDQUFDO1lBQ1gsSUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxVQUFDLElBQVM7Z0JBQzdCLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNSLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlDQUNJLGNBQWtELEVBQ2xELE1BQTZCLEVBQzdCLFlBQXlDO1FBQ3pDLE1BQU0sQ0FBQztZQUNILFFBQVEsRUFBRSxJQUFJO1lBQ2QsS0FBSyxFQUFFLEtBQUs7WUFDWixPQUFPLEVBQUUsVUFBQyxRQUFrQyxFQUNwQyxXQUFnQyxFQUNoQyxVQUF1QztnQkFFM0MsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM3QixJQUFNLE9BQU8sR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3JDLElBQU0sTUFBTSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFFaEMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDVCxNQUFNLENBQUM7d0JBQ0gsR0FBRyxFQUFFLFVBQUMsS0FBcUIsRUFDdkIsZUFBeUMsRUFDekMsa0JBQXdDOzRCQUNwQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFVBQUMsTUFBVztnQ0FDM0QsSUFBTSxhQUFhLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0NBQ3BELGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7NEJBQ3hDLENBQUMsQ0FBQyxDQUFDO3dCQUNYLENBQUM7cUJBQ0osQ0FBQztnQkFDTixDQUFDO2dCQUVELFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDM0IsQ0FBQztTQUNKLENBQUM7SUFDTixDQUFDO0lBL0JlLCtCQUF1QiwwQkErQnRDLENBQUE7SUFFRCwwQ0FDSSxjQUFrRCxFQUNsRCxNQUE2QixFQUM3QixZQUF5QztRQUN6QyxNQUFNLENBQUM7WUFDSCxRQUFRLEVBQUUsR0FBRztZQUNiLEtBQUssRUFBRSxLQUFLO1lBQ1osT0FBTyxFQUFFLFVBQUMsUUFBa0MsRUFDcEMsV0FBZ0MsRUFDaEMsVUFBdUM7Z0JBQzNDLElBQU0sTUFBTSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDVCxNQUFNLENBQUM7d0JBQ0gsR0FBRyxFQUFFLFVBQUMsS0FBcUIsRUFDdkIsZUFBeUMsRUFDekMsa0JBQXVDOzRCQUNuQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFVBQUMsTUFBVztnQ0FDM0QsR0FBRyxDQUFDLENBQUMsSUFBSSxTQUFTLElBQUksa0JBQWtCLENBQUMsQ0FBQyxDQUFDO29DQUN2QyxFQUFFLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dDQUMvQyxJQUFNLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQzt3Q0FDL0MsRUFBRSxDQUFDLENBQUMsUUFBUTs0Q0FDUixPQUFPLFFBQVEsS0FBSyxRQUFROzRDQUM1QixRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7NENBQzdCLFFBQVEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRDQUVwRCxJQUFNLElBQUksR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDOzRDQUN4RCxJQUFNLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQzs0Q0FDM0Qsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQzt3Q0FDaEQsQ0FBQztvQ0FDTCxDQUFDO2dDQUNMLENBQUM7NEJBQ0wsQ0FBQyxDQUFDLENBQUM7d0JBQ1gsQ0FBQztxQkFDSixDQUFDO2dCQUNOLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osR0FBRyxDQUFDLENBQUMsSUFBSSxTQUFTLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQzt3QkFDaEMsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3hDLElBQUksUUFBUSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQzs0QkFDdEMsRUFBRSxDQUFDLENBQUMsUUFBUTtnQ0FDUixPQUFPLFFBQVEsS0FBSyxRQUFRO2dDQUM1QixRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7Z0NBQzdCLFFBQVEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUVwRCxJQUFNLElBQUksR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dDQUN4RCxJQUFNLE9BQU8sR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ3JDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDOzRCQUN6QyxDQUFDO3dCQUNMLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQztTQUNKLENBQUM7SUFDTixDQUFDO0lBcERlLHdDQUFnQyxtQ0FvRC9DLENBQUE7QUFDTCxDQUFDLEVBckdhLE9BQU8sR0FBUCxlQUFPLEtBQVAsZUFBTyxRQXFHcEIifQ==

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable */
function format(source, params) {
    var toString = function (obj, format) {
        var ctor = function (o) {
            if (typeof o === "number") {
                return Number;
            }
            else if (typeof o === "boolean") {
                return Boolean;
            }
            else if (typeof o === "string") {
                return String;
            }
            else {
                return o.constructor;
            }
        }(obj);
        var proto = ctor.prototype;
        var formatter = typeof obj !== "string" ?
            proto ? proto.format || proto.toString : obj.format || obj.toString : obj.toString;
        if (formatter) {
            if (typeof format === "undefined" || format === "") {
                return formatter.call(obj);
            }
            else {
                return formatter.call(obj, format);
            }
        }
        else {
            return "";
        }
    };
    if (arguments.length === 1) {
        return function () {
            return format.apply(null, [source].concat(Array.prototype.slice.call(arguments, 0)));
        };
    }
    if (arguments.length === 2 && typeof params !== "object") {
        params = [params];
    }
    if (arguments.length > 2) {
        params = Array.prototype.slice.call(arguments, 1);
    }
    source = source.replace(/\{\{|\}\}|\{([^}: ]+?)(?::([^}]*?))?\}/g, function (match, num, format) {
        if (match === "{{") {
            return "{";
        }
        if (match === "}}") {
            return "}";
        }
        if (typeof params[num] !== "undefined" && params[num] !== null) {
            return toString(params[num], format);
        }
        else {
            return "";
        }
    });
    return source;
}
exports.format = format;
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZm9ybWF0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsb0JBQW9CO0FBQ3BCLGdCQUF1QixNQUFVLEVBQUcsTUFBVztJQUN2QyxJQUFJLFFBQVEsR0FBRyxVQUFVLEdBQVEsRUFBRSxNQUFXO1FBQzFDLElBQUksSUFBSSxHQUFHLFVBQVUsQ0FBQztZQUNsQixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ2xCLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDaEMsTUFBTSxDQUFDLE9BQU8sQ0FBQztZQUNuQixDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDbEIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQ3pCLENBQUM7UUFDTCxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDUCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzNCLElBQUksU0FBUyxHQUFHLE9BQU8sR0FBRyxLQUFLLFFBQVE7WUFDdkMsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUNuRixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ1osRUFBRSxDQUFDLENBQUMsT0FBTyxNQUFNLEtBQUssV0FBVyxJQUFJLE1BQU0sS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ2QsQ0FBQztJQUNMLENBQUMsQ0FBQztJQUNGLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN6QixNQUFNLENBQUM7WUFDSCxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekYsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQUNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDdkQsTUFBTSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QixNQUFNLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBQ0QsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMseUNBQXlDLEVBQzdELFVBQVUsS0FBVSxFQUFFLEdBQVEsRUFBRSxNQUFVO1FBQ3RDLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDZixDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDakIsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNmLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxXQUFXLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDN0QsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDekMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxDQUFDLEVBQUUsQ0FBQztRQUNkLENBQUM7SUFDVCxDQUFDLENBQUMsQ0FBQztJQUNILE1BQU0sQ0FBQyxNQUFNLENBQUM7QUFDbEIsQ0FBQztBQXBETCx3QkFvREs7QUFBQSxDQUFDIn0=

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var gettext_service_1 = __webpack_require__(7);
exports.default = function (mod) {
    mod.service("getTextService", ["$translate", "constants", gettext_service_1.GetText.getTextServiceFactory])
        .service("getTextServiceEx", ["getTextService", gettext_service_1.GetText.getTextServiceExFactory]);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHFEQUE0QztBQUU1QyxrQkFBZSxVQUFDLEdBQVE7SUFDcEIsR0FBRyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFlBQVksRUFBRSxXQUFXLEVBQUUseUJBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1NBQ2xGLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLGdCQUFnQixFQUFFLHlCQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO0FBQzVGLENBQUMsQ0FBQyJ9

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var G = __webpack_require__(8);
var GetText;
(function (GetText) {
    ;
    ;
    function getTextServiceFactory($translate, constants) {
        var decorateFoundString;
        var decorateMissingString;
        if (constants.environment && constants.environment.debugI18n) {
            decorateFoundString = function (input) {
                return "\u2714 " + input + " \u2714";
            };
            decorateMissingString = function (input) {
                return "\u2718 " + input + " \u2718";
            };
        }
        else {
            decorateFoundString = decorateMissingString = function (input) { return input; };
        }
        return function (input) {
            if (typeof input === "undefined") {
                return input;
            }
            var hash = G.GetText.GetToken(input);
            var retval = $translate.instant(hash);
            if (retval !== "✘") {
                return decorateFoundString(retval);
            }
            return decorateMissingString(input.trim());
        };
    }
    GetText.getTextServiceFactory = getTextServiceFactory;
    function getTextServiceExFactory($translate) {
        return {
            translate: $translate,
            translateIfNeeded: function (input) {
                if (input.length > 4) {
                    var inputLowerCase = input.toLowerCase();
                    if ((inputLowerCase.substring(0, 3) === "_t(") &&
                        (inputLowerCase[inputLowerCase.length - 1] === ")")) {
                        return $translate(input.substring(3, input.length - 1));
                    }
                }
                return input;
            }
        };
    }
    GetText.getTextServiceExFactory = getTextServiceExFactory;
})(GetText = exports.GetText || (exports.GetText = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0dGV4dC1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZ2V0dGV4dC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXVDO0FBRXZDLElBQWMsT0FBTyxDQW1FcEI7QUFuRUQsV0FBYyxPQUFPO0lBSWhCLENBQUM7SUFVRCxDQUFDO0lBRUYsK0JBQ0ksVUFBMEMsRUFDMUMsU0FBZTtRQUVmLElBQUksbUJBQThDLENBQUM7UUFDbkQsSUFBSSxxQkFBZ0QsQ0FBQztRQUVyRCxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxJQUFJLFNBQVMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMzRCxtQkFBbUIsR0FBRyxVQUFDLEtBQWE7Z0JBQ2hDLE1BQU0sQ0FBQyxZQUFLLEtBQUssWUFBSSxDQUFDO1lBQzFCLENBQUMsQ0FBQztZQUNGLHFCQUFxQixHQUFHLFVBQUMsS0FBYTtnQkFDbEMsTUFBTSxDQUFDLFlBQUssS0FBSyxZQUFJLENBQUM7WUFDMUIsQ0FBQyxDQUFDO1FBQ04sQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osbUJBQW1CLEdBQUcscUJBQXFCLEdBQUcsVUFBQyxLQUFhLElBQWEsT0FBQSxLQUFLLEVBQUwsQ0FBSyxDQUFDO1FBQ25GLENBQUM7UUFFRCxNQUFNLENBQUMsVUFBQyxLQUFhO1lBQ2pCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDakIsQ0FBQztZQUVELElBQU0sSUFBSSxHQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRS9DLElBQU0sTUFBTSxHQUFXLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEQsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pCLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN2QyxDQUFDO1lBRUQsTUFBTSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQztJQUNOLENBQUM7SUFoQ2UsNkJBQXFCLHdCQWdDcEMsQ0FBQTtJQUVELGlDQUF3QyxVQUFvQztRQUV4RSxNQUFNLENBQUM7WUFDSCxTQUFTLEVBQUUsVUFBVTtZQUNyQixpQkFBaUIsRUFBakIsVUFBa0IsS0FBYztnQkFDNUIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNuQixJQUFNLGNBQWMsR0FBWSxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3BELEVBQUUsQ0FBQyxDQUFFLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDO3dCQUN2QyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBRSxDQUFDLENBQUMsQ0FBQzt3QkFDM0QsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzVELENBQUM7Z0JBQ0wsQ0FBQztnQkFFRCxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2pCLENBQUM7U0FDSixDQUFDO0lBQ04sQ0FBQztJQWhCZSwrQkFBdUIsMEJBZ0J0QyxDQUFBO0FBQ0wsQ0FBQyxFQW5FYSxPQUFPLEdBQVAsZUFBTyxLQUFQLGVBQU8sUUFtRXBCIn0=

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable */
var murmurHash3_1 = __webpack_require__(9);
var GetText;
(function (GetText) {
    function isWhiteSpace(c) {
        return c === 32 /*' '*/ || c === 10 /*'\n'*/ || c === 13 /*'\r'*/ || c === 9 /* '\t' */;
    }
    function normalizeString(str) {
        var normalizedChars = [];
        var wasWhitespace = false;
        for (var i = 0; i < str.length; i++) {
            var c = str.charCodeAt(i);
            switch (wasWhitespace) {
                case false:
                    if (isWhiteSpace(c)) {
                        wasWhitespace = true;
                    }
                    else {
                        normalizedChars.push(c);
                    }
                    break;
                case true:
                    if (!isWhiteSpace(c)) {
                        wasWhitespace = false;
                        normalizedChars.push(32 /* ' ' */);
                        normalizedChars.push(c);
                    }
                    break;
            }
        }
        return String.fromCharCode.apply(null, normalizedChars);
    }
    function toUTF8Array(str) {
        var utf8 = [];
        for (var i = 0; i < str.length; i++) {
            var charcode = str.charCodeAt(i);
            if (charcode < 0x80)
                utf8.push(charcode);
            else if (charcode < 0x800) {
                utf8.push(0xc0 | (charcode >> 6), 0x80 | (charcode & 0x3f));
            }
            else if (charcode < 0xd800 || charcode >= 0xe000) {
                utf8.push(0xe0 | (charcode >> 12), 0x80 | ((charcode >> 6) & 0x3f), 0x80 | (charcode & 0x3f));
            }
            else {
                i++;
                // UTF-16 encodes 0x10000-0x10FFFF by
                // subtracting 0x10000 and splitting the
                // 20 bits of 0x0-0xFFFFF into two halves
                charcode = 0x10000 + (((charcode & 0x3ff) << 10)
                    | (str.charCodeAt(i) & 0x3ff));
                utf8.push(0xf0 | (charcode >> 18), 0x80 | ((charcode >> 12) & 0x3f), 0x80 | ((charcode >> 6) & 0x3f), 0x80 | (charcode & 0x3f));
            }
        }
        return utf8;
    }
    function GetToken(input) {
        var utf8 = toUTF8Array(normalizeString(input.trim()));
        var hash = "i18n_" + (("0000000" + murmurHash3_1.murmurHash3.hash32(utf8).toString(16)).substr(-8)).toUpperCase();
        return hash;
    }
    GetText.GetToken = GetToken;
})(GetText = exports.GetText || (exports.GetText = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGFzaGluZy1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaGFzaGluZy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsb0JBQW9CO0FBQ3BCLDZDQUE0QztBQUU1QyxJQUFjLE9BQU8sQ0F1RXBCO0FBdkVELFdBQWMsT0FBTztJQUNqQixzQkFBc0IsQ0FBUztRQUMzQixNQUFNLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxPQUFPLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxRQUFRLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUM7SUFDNUYsQ0FBQztJQUVELHlCQUF5QixHQUFXO1FBQ2hDLElBQUksZUFBZSxHQUFtQixFQUFFLENBQUM7UUFDekMsSUFBSSxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzFCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFMUIsTUFBTSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDcEIsS0FBSyxLQUFLO29CQUNOLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2xCLGFBQWEsR0FBRyxJQUFJLENBQUM7b0JBQ3pCLENBQUM7b0JBQ0QsSUFBSSxDQUFDLENBQUM7d0JBQ0YsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUIsQ0FBQztvQkFDRCxLQUFLLENBQUM7Z0JBQ1YsS0FBSyxJQUFJO29CQUNMLEVBQUUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsYUFBYSxHQUFHLEtBQUssQ0FBQzt3QkFDdEIsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ25DLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzVCLENBQUM7b0JBQ0QsS0FBSyxDQUFDO1lBQ2QsQ0FBQztRQUNMLENBQUM7UUFFRCxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxxQkFBcUIsR0FBWTtRQUM3QixJQUFNLElBQUksR0FBbUIsRUFBRSxDQUFDO1FBQ2hDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ2xDLElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakMsRUFBRSxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLEVBQzVCLElBQUksR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLENBQUM7WUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxHQUFHLE1BQU0sSUFBSSxRQUFRLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLEVBQzdCLElBQUksR0FBRyxDQUFDLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUMvQixJQUFJLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNsQyxDQUFDO1lBRUQsSUFBSSxDQUFDLENBQUM7Z0JBQ0YsQ0FBQyxFQUFFLENBQUM7Z0JBQ0oscUNBQXFDO2dCQUNyQyx3Q0FBd0M7Z0JBQ3hDLHlDQUF5QztnQkFDekMsUUFBUSxHQUFHLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO3NCQUMxQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLEVBQzdCLElBQUksR0FBRyxDQUFDLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUNoQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFDL0IsSUFBSSxHQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbEMsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxrQkFBeUIsS0FBYTtRQUVsQyxJQUFNLElBQUksR0FBRyxXQUFXLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFeEQsSUFBTSxJQUFJLEdBQVcsVUFBUSxDQUFDLENBQUMsU0FBUyxHQUFHLHlCQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFJLENBQUM7UUFDOUcsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBTmUsZ0JBQVEsV0FNdkIsQ0FBQTtBQUNMLENBQUMsRUF2RWEsT0FBTyxHQUFQLGVBQU8sS0FBUCxlQUFPLFFBdUVwQiJ9

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/* tslint:disable */
// +----------------------------------------------------------------------+
// | murmurHash3.js v2.1.2 (http://github.com/karanlyons/murmurHash.js)   |
// | A javascript implementation of MurmurHash3's x86 hashing algorithms. |
// |----------------------------------------------------------------------|
// | Copyright (c) 2012 Karan Lyons                                       |
// | Freely distributable under the MIT license.                          |
// +----------------------------------------------------------------------+
Object.defineProperty(exports, "__esModule", { value: true });
var murmurHash3;
(function (murmurHash3) {
    function hash32(key, seed) {
        if (seed === void 0) { seed = 0; }
        var remainder = key.length % 4;
        var bytes = key.length - remainder;
        var h1 = seed;
        var k1 = 0;
        var c1 = 0xcc9e2d51;
        var c2 = 0x1b873593;
        for (var i = 0; i < bytes; i = i + 4) {
            k1 = ((key[i] & 0xff)) | ((key[i + 1] & 0xff) << 8) | ((key[i + 2] & 0xff) << 16) | ((key[i + 3] & 0xff) << 24);
            k1 = _x86Multiply(k1, c1);
            k1 = _x86Rotl(k1, 15);
            k1 = _x86Multiply(k1, c2);
            h1 ^= k1;
            h1 = _x86Rotl(h1, 13);
            h1 = _x86Multiply(h1, 5) + 0xe6546b64;
        }
        k1 = 0;
        switch (remainder) {
            case 3:
                k1 ^= (key[i + 2] & 0xff) << 16;
            case 2:
                k1 ^= (key[i + 1] & 0xff) << 8;
            case 1:
                k1 ^= (key[i] & 0xff);
                k1 = _x86Multiply(k1, c1);
                k1 = _x86Rotl(k1, 15);
                k1 = _x86Multiply(k1, c2);
                h1 ^= k1;
        }
        h1 ^= key.length;
        h1 = _x86Fmix(h1);
        return h1 >>> 0;
    }
    murmurHash3.hash32 = hash32;
    function _x86Multiply(m, n) {
        return ((m & 0xffff) * n) + ((((m >>> 16) * n) & 0xffff) << 16);
    }
    function _x86Rotl(m, n) {
        return (m << n) | (m >>> (32 - n));
    }
    function _x86Fmix(h) {
        h ^= h >>> 16;
        h = _x86Multiply(h, 0x85ebca6b);
        h ^= h >>> 13;
        h = _x86Multiply(h, 0xc2b2ae35);
        h ^= h >>> 16;
        return h;
    }
})(murmurHash3 = exports.murmurHash3 || (exports.murmurHash3 = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVybXVySGFzaDMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtdXJtdXJIYXNoMy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0JBQW9CO0FBQ3BCLDJFQUEyRTtBQUMzRSwyRUFBMkU7QUFDM0UsMkVBQTJFO0FBQzNFLDJFQUEyRTtBQUMzRSwyRUFBMkU7QUFDM0UsMkVBQTJFO0FBQzNFLDJFQUEyRTs7QUFFM0UsSUFBYyxXQUFXLENBaUV4QjtBQWpFRCxXQUFjLFdBQVc7SUFDdkIsZ0JBQXVCLEdBQWtCLEVBQUUsSUFBaUI7UUFBakIscUJBQUEsRUFBQSxRQUFpQjtRQUN4RCxJQUFJLFNBQVMsR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUMvQixJQUFJLEtBQUssR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQztRQUVuQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFFZCxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFWCxJQUFJLEVBQUUsR0FBRyxVQUFVLENBQUM7UUFDcEIsSUFBSSxFQUFFLEdBQUcsVUFBVSxDQUFDO1FBRXBCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7WUFDckMsRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUVoSCxFQUFFLEdBQUcsWUFBWSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUMxQixFQUFFLEdBQUcsUUFBUSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0QixFQUFFLEdBQUcsWUFBWSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUUxQixFQUFFLElBQUksRUFBRSxDQUFDO1lBQ1QsRUFBRSxHQUFHLFFBQVEsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDdEIsRUFBRSxHQUFHLFlBQVksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDO1FBQ3hDLENBQUM7UUFFRCxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVAsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNsQixLQUFLLENBQUM7Z0JBQ0osRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFbEMsS0FBSyxDQUFDO2dCQUNKLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRWpDLEtBQUssQ0FBQztnQkFDSixFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQ3RCLEVBQUUsR0FBRyxZQUFZLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUMxQixFQUFFLEdBQUcsUUFBUSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDdEIsRUFBRSxHQUFHLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzFCLEVBQUUsSUFBSSxFQUFFLENBQUM7UUFDYixDQUFDO1FBRUQsRUFBRSxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUM7UUFDakIsRUFBRSxHQUFHLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVsQixNQUFNLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBNUNlLGtCQUFNLFNBNENyQixDQUFBO0lBRUQsc0JBQXNCLENBQVUsRUFBRSxDQUFVO1FBQ3hDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRCxrQkFBa0IsQ0FBVSxFQUFFLENBQVU7UUFDcEMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUdELGtCQUFrQixDQUFVO1FBQzFCLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2QsQ0FBQyxHQUFJLFlBQVksQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDakMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDZCxDQUFDLEdBQUksWUFBWSxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNqQyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVkLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0FBQ0gsQ0FBQyxFQWpFYSxXQUFXLEdBQVgsbUJBQVcsS0FBWCxtQkFBVyxRQWlFeEIifQ==

/***/ })
/******/ ]);
//# sourceMappingURL=i18n-gettext.js.map