/*!
 * @solarwinds/ipam 2020.2.5-223
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 45);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestService = /** @class */ (function () {
    /** @ngInject */
    IpRequestService.$inject = ["swApi"];
    function IpRequestService(swApi) {
        this.swApi = swApi;
    }
    IpRequestService.prototype.getIpRequestId = function (alertId) {
        return this.swApi.api(false).one("ipam/ipRequests/idByAlertId/" + alertId).get();
    };
    IpRequestService.prototype.getIpRequestDetails = function (ipRequestId) {
        return this.swApi.api(false).one("ipam/ipRequests/" + ipRequestId).get();
    };
    IpRequestService.prototype.getRequesterDetails = function (ipRequestId) {
        return this.swApi.api(false).one("ipam/ipRequests/" + ipRequestId + "/requesterDetails").get();
    };
    IpRequestService.prototype.updateIpRequestDetails = function (ipRequestDetails) {
        return this.swApi
            .api(false)
            .all("ipam/ipRequests")
            .customPUT(ipRequestDetails)
            .then(function (response) {
            return response;
        });
    };
    IpRequestService.prototype.getAvailableSubnets = function (requestedIpsCount) {
        return this.swApi.api(false).one("ipam/ipRequests/subnetsAvailableForReservation?requestedIpsCount=" + requestedIpsCount).get();
    };
    IpRequestService.prototype.getAvailableIpsWithCustomProperties = function (subnetId, ipCount) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddress?subnetId=" + subnetId + "&addressCount=" + ipCount + "&withCustomProperties=true")
            .get();
    };
    IpRequestService.prototype.getAvailableIpsWithoutCustomProperties = function (subnetId, ipCount) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddress?subnetId=" + subnetId + "&addressCount=" + ipCount)
            .get();
    };
    IpRequestService.prototype.getAvailableIpsForSubnet = function (subnetId) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddressesForSubnet?subnetId=" + subnetId)
            .get();
    };
    IpRequestService.prototype.getDhcpServersBySubnetId = function (subnetId) {
        return this.swApi.api(false).one("ipam/ipRequests/dhcpServersBySubnetId/" + subnetId).get();
    };
    IpRequestService.prototype.getSubnetDetails = function (subnetId) {
        return this.swApi.api(false).one("ipam/ipRequests/subnetDetails/" + subnetId).get();
    };
    IpRequestService.prototype.getNodesCustomPropertiesValues = function (ipNodeId) {
        return this.swApi.api(false).one("ipam/ipRequests/ipCustomPropertiesValues/" + ipNodeId).get();
    };
    return IpRequestService;
}());
exports.IpRequestService = IpRequestService;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestWidgetService = /** @class */ (function () {
    /** @ngInject */
    IpRequestWidgetService.$inject = ["swNetObjectOpidConverterService", "$q"];
    function IpRequestWidgetService(swNetObjectOpidConverterService, $q) {
        this.swNetObjectOpidConverterService = swNetObjectOpidConverterService;
        this.$q = $q;
    }
    IpRequestWidgetService.prototype.extractAlertId = function (opId) {
        var _this = this;
        return this.swNetObjectOpidConverterService
            .opidsToNetObjects([opId])
            .then(function (result) { return _this.handleConvertedNetObjects(result); });
    };
    IpRequestWidgetService.prototype.handleConvertedNetObjects = function (result) {
        if (result.length !== 1) {
            return this.$q.reject("No NetObjects found");
        }
        var netObject = result[0].netObject;
        var regex = /AAT:(\d+)/;
        var match = regex.exec(netObject);
        if (match === null) {
            return this.$q.reject("Cannot find Active Alert Id");
        }
        var alertId = Number(match[1]);
        return this.$q.resolve(alertId);
    };
    return IpRequestWidgetService;
}());
exports.IpRequestWidgetService = IpRequestWidgetService;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var stepExitResult_1 = __webpack_require__(85);
var Step = /** @class */ (function () {
    /** @ngInject */
    Step.$inject = ["vm", "$q", "swApi", "$log", "$translate", "$scope", "requesterCustomFieldsService", "ipRequestService", "getTextService"];
    function Step(vm, $q, swApi, $log, $translate, $scope, requesterCustomFieldsService, ipRequestService, getTextService) {
        this.vm = vm;
        this.$q = $q;
        this.swApi = swApi;
        this.$log = $log;
        this.$translate = $translate;
        this.$scope = $scope;
        this.requesterCustomFieldsService = requesterCustomFieldsService;
        this.ipRequestService = ipRequestService;
        this.getTextService = getTextService;
        this.visited = false;
        this.active = false;
        this.complete = false;
        this.icon = "step";
        this.exitResult = new stepExitResult_1.default();
        this._t = getTextService;
    }
    Step.prototype.onEnter = function (data) {
        return this.$q.resolve(true);
    };
    Step.prototype.onExit = function () {
        this.setExitResult(false);
        if (!this.ipRequest_form || this.validateForm(this.ipRequest_form)) {
            this.exitResult.result = true;
        }
        return this.$q.resolve(this.exitResult);
    };
    Step.prototype.onFinish = function () {
        return this.$q.resolve(true);
    };
    Step.prototype.canGoNext = function () {
        return true;
    };
    Step.prototype.validateForm = function (form) {
        if (!this.isFormValid(form)) {
            this.setDirtyOnFieldsWithError(form);
            return false;
        }
        return true;
    };
    Step.prototype.setDirtyOnFieldsWithError = function (form) {
        angular.forEach(form.$error.required, function (field) {
            field.$setDirty();
        });
    };
    Step.prototype.isFormValid = function (form) {
        return form != null && form.$valid;
    };
    Step.prototype.setExitResult = function (result, data) {
        if (data === void 0) { data = null; }
        this.exitResult.result = result;
        this.exitResult.data = data;
    };
    return Step;
}());
exports.default = Step;


/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title=\"_t(Cloud DNS Zones)\" class=xui-page-content page-layout=fill _ta> <div class=ipam-cloud__body> <div class=ipam-cloud__sidebar> <xui-sidebar-container layout=fill> <xui-filtered-list class=\"xui-edge-definer ipam-cloud__filtered-list\" items-source=vm.itemsSource on-refresh=\"vm.refreshData(filters, pagination, sorting, searching, filterModels)\" filter-properties=vm.filterProperties available-filter-properties=vm.filterProperties options=vm.options filter-values=vm.filterValues pagination-data=vm.pagination sorting=vm.sorting remote-control=vm.control filter-property-states=vm.innerFilterPropertyStates on-filter-property-states-change=vm.innerOnFilterPropertyStatesChange() controller=vm layout=fill> </xui-filtered-list> <xui-sidebar settings=vm.sideBarSettings style=\"\"> <div> <div class=sidebar-header> <div class=sidebar-zoneName> <a target=_blank ng-href=\"/ui/ipam/cloudDnsRecords?zoneId={{vm.selectedZone.dnsZoneId}}\" display-style=link>{{vm.selectedZone.domainName}}</a> </div> <div class=sidebar-close> <xui-icon icon-size=small stroked icon=close ng-click=vm.toggleSidebar() /> </div> </div> <table class=sidebar-table> <tr> <td> <div> <table class=sidebar-table> <colgroup> <col class=sidebar-table-header /> <col class=sidebar-table-value /> </colgroup> <tbody> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Type</td> <td class=sidebar-table-td>{{vm.selectedZone.type}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Record Set Count</td> <td class=sidebar-table-td>{{vm.selectedZone.recordSetCount}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Comment</td> <td class=sidebar-table-td>{{vm.selectedZone.comment}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Hosted Zone ID</td> <td class=sidebar-table-td>{{vm.selectedZone.id}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Name Servers</td> <td class=sidebar-table-td>{{vm.selectedZone.nameServers}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Tags</td> <td class=sidebar-table-td>{{vm.selectedZone.tags}}</td> </tr> </tbody> </table> </div> </td> </tr> </table> </div> </xui-sidebar> </xui-sidebar-container> </div> </div> </xui-page-content> <script type=text/ng-template id=item-template> <div class=\"row\">\r\n        <div class=\"col-md-4\">\r\n            <a target=\"_blank\" ng-href='/ui/ipam/cloudDnsRecords?zoneId={{::item.dnsZoneId}}' display-style='link'>{{::item.domainName}}</a>\r\n        </div>\r\n        <div class=\"col-md-2\">\r\n            {{::item.type}}\r\n        </div>\r\n        <div class=\"col-md-2 ipam-cloud\">\r\n            <div class=\"hideOverflow\">{{::item.id }}</div>            \r\n        </div>\r\n        <div class=\"col-md-2\">\r\n            {{::item.account}}\r\n        </div>\r\n        <div class=\"col-md-1\">\r\n            {{::item.provider}}\r\n        </div>\r\n        <div class=\"col-md-1\">\r\n            <button type=\"button\" ng-disabled=\"isDisabled\" uib-tooltip=\"\" uib-tooltip-append-to-body=\"\" class=\"xui-button btn xui-btn-tertiary icon-left\" role=\"button\" ng-click=\"vm.getDnsZone(item.dnsZoneId)\" display-style=\"tertiary\" icon=\"caret-right\" is-empty=\"true\" aria-hidden=\"false\">\r\n                <i uib-tooltip=\"\" uib-tooltip-append-to-body=\"\" is-dynamic=\"true\" icon=\"caret-right\" class=\"xui-icon xui-icon-caret-right  filled \">\r\n                    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" viewBox=\"0 0 20 20\">\r\n                        <defs>\r\n                            <style>\r\n                                .a {\r\n                                    fill: #297994;\r\n                                }\r\n                            </style>\r\n                        </defs>\r\n                        <title>caret-right</title>\r\n                        <polygon class=\"a\" points=\"8.71 15.71 7.29 14.29 11.59 10 7.29 5.71 8.71 4.29 14.41 10 8.71 15.71\"></polygon>\r\n                    </svg>\r\n                </i>\r\n                <div class=\"xui-button-content\"> </div>\r\n            </button>\r\n        </div>\r\n    </div> </script> ";

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title={{vm.pageTitle}} class=xui-page-content page-layout=fill> <div class=ipam-cloud__body> <div class=ipam-cloud__sidebar> <xui-sidebar-container layout=fill> <xui-filtered-list class=\"xui-edge-definer ipam-cloud__filtered-list\" items-source=vm.itemsSource on-refresh=\"vm.refreshData(filters, pagination, sorting, searching, filterModels)\" filter-properties=vm.filterProperties available-filter-properties=vm.filterProperties options=vm.options filter-values=vm.filterValues pagination-data=vm.pagination sorting=vm.sorting remote-control=vm.control filter-property-states=vm.innerFilterPropertyStates on-filter-property-states-change=vm.innerOnFilterPropertyStatesChange() controller=vm layout=fill> </xui-filtered-list> <xui-sidebar settings=vm.sideBarSettings style=\"\"> <div> <div class=sidebar-header> <div class=sidebar-zoneName>{{vm.selectedRecord.Name}}</div> <div class=sidebar-close> <xui-icon icon-size=small stroked icon=close ng-click=vm.toggleSidebar() /> </div> </div> <table class=sidebar-table> <tr> <td> <div> <table class=sidebar-table> <colgroup> <col class=sidebar-table-header /> <col class=sidebar-table-value /> </colgroup> <tbody> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Type</td> <td class=sidebar-table-td>{{vm.selectedRecord.Type}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Alias</td> <td class=sidebar-table-td>{{vm.selectedRecord.Alias}}</td> </tr> <tr class=sidebar-table-tr ng-if=vm.selectedRecord.IsAlias> <td class=sidebar-table-header _t>Alias Target</td> <td class=sidebar-table-td>{{vm.selectedRecord.AliasDnsName}}</td> </tr> <tr class=sidebar-table-tr ng-if=vm.selectedRecord.IsAlias> <td class=sidebar-table-header _t>Alias Hosted Zone ID</td> <td class=sidebar-table-td>{{vm.selectedRecord.AliasHostedZoneId}}</td> </tr> <tr class=sidebar-table-tr ng-if=!vm.selectedRecord.IsAlias> <td class=sidebar-table-header _t>TTL (Seconds)</td> <td class=sidebar-table-td>{{vm.selectedRecord.Ttl}}</td> </tr> <tr class=sidebar-table-tr ng-if=!vm.selectedRecord.IsAlias> <td class=sidebar-table-header _t>Value</td> <td class=sidebar-table-td>{{vm.selectedRecord.Value}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Routing Policy</td> <td class=sidebar-table-td>{{vm.selectedRecord.RoutingPolicy}}</td> </tr> <tr class=sidebar-table-tr ng-if=\"vm.selectedRecord.RoutingPolicyId == 'Weighted'\"> <td class=sidebar-table-header _t>Weight</td> <td class=sidebar-table-td>{{vm.selectedRecord.Weight}}</td> </tr> <tr class=sidebar-table-tr ng-if=\"vm.selectedRecord.RoutingPolicyId == 'Latency'\"> <td class=sidebar-table-header _t>Region</td> <td class=sidebar-table-td>{{vm.selectedRecord.Region}}</td> </tr> <tr class=sidebar-table-tr ng-if=\"vm.selectedRecord.RoutingPolicyId == 'Failover'\"> <td class=sidebar-table-header _t>Failover</td> <td class=sidebar-table-td>{{vm.selectedRecord.Failover}}</td> </tr> <tr class=sidebar-table-tr ng-if=\"vm.selectedRecord.RoutingPolicyId == 'Geolocation'\"> <td class=sidebar-table-header _t>Geolocation</td> <td class=sidebar-table-td>{{vm.selectedRecord.Geolocation}}</td> </tr> <tr class=sidebar-table-tr ng-if=\"vm.selectedRecord.RoutingPolicyId != 'Simple'\"> <td class=sidebar-table-header _t>Set ID</td> <td class=sidebar-table-td>{{vm.selectedRecord.SetIdentifier}}</td> </tr> <tr class=sidebar-table-tr ng-if=vm.selectedRecord.IsAlias> <td class=sidebar-table-header _t>Evaluate Target Health</td> <td class=sidebar-table-td>{{vm.selectedRecord.AliasEvaluateTargetHealth}}</td> </tr> <tr class=sidebar-table-tr> <td class=sidebar-table-header _t>Associate with Health Check</td> <td class=sidebar-table-td>{{vm.selectedRecord.AssociateWithHealthCheck}}</td> </tr> <tr class=sidebar-table-tr ng-if=\"vm.selectedRecord.HealthCheckId != '-'\"> <td class=sidebar-table-header _t>Health Check ID</td> <td class=sidebar-table-td>{{vm.selectedRecord.HealthCheckId}}</td> </tr> </tbody> </table> </div> </td> </tr> </table> </div> </xui-sidebar> </xui-sidebar-container> </div> </div> </xui-page-content> <script type=text/ng-template id=item-template> <div class=\"row\">\r\n        <div class=\"col-md-3\">\r\n            {{::item.name}}\r\n        </div>\r\n        <div class=\"col-md-2\">\r\n            {{::item.type}}\r\n        </div>\r\n        <div class=\"col-md-3\">\r\n            <div class=\"ipam-grid-record-value\">\r\n                {{::item.value}}\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-2\">\r\n            {{::item.ttl}}\r\n        </div>\r\n        <div class=\"col-md-1\">\r\n            <button type=\"button\" ng-disabled=\"isDisabled\" uib-tooltip=\"\" uib-tooltip-append-to-body=\"\" class=\"xui-button btn xui-btn-tertiary icon-left\" role=\"button\" ng-click=\"vm.getDnsRecord(item.id)\" display-style=\"tertiary\" icon=\"caret-right\" is-empty=\"true\" aria-hidden=\"false\">\r\n                <i uib-tooltip=\"\" uib-tooltip-append-to-body=\"\" is-dynamic=\"true\" icon=\"caret-right\" class=\"xui-icon xui-icon-caret-right  filled \">\r\n                    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" viewBox=\"0 0 20 20\">\r\n                        <defs>\r\n                            <style>\r\n                                .a {\r\n                                    fill: #297994;\r\n                                }\r\n                            </style>\r\n                        </defs>\r\n                        <title>caret-right</title>\r\n                        <polygon class=\"a\" points=\"8.71 15.71 7.29 14.29 11.59 10 7.29 5.71 8.71 4.29 14.41 10 8.71 15.71\"></polygon>\r\n                    </svg>\r\n                </i>\r\n                <div class=\"xui-button-content\"> </div>\r\n            </button>\r\n        </div>\r\n    </div> </script> ";

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "<div class=\"breadcrumbs xui-padding-lgr xui-padding-lgl xui-padding-lgt\"> <a class=xui-text-a href=/Orion/Admin/ _t>Admin</a> <xui-icon icon=triangle-right icon-size=small></xui-icon> <a class=xui-text-a href=/Orion/CloudMonitoring/Admin/CloudMonitoringSettings.aspx _t>Cloud Infrastructure Monitoring</a> <xui-icon icon=triangle-right icon-size=small></xui-icon> <a class=xui-text-a href=/ui/clm/accounts target=_self _t>Manage Cloud Accounts</a> <xui-icon icon=triangle-right icon-size=small></xui-icon> <span class=xui-text-s _t>Edit IPAM Settings ({{wizard.account.Name|| \"Account\"}})</span> </div> <xui-page-content is-busy=wizard.isUpdating busy-message={{wizard.savingMessage}} _ta> <error-info class=clm-error-wrapper ng-if=\"wizard.data.data['error']\"> <div id=clmErrorInfo class=\"xui-padding-lgh xui-padding-mdv clm-error-info\"> <i uib-tooltip=\"\" uib-tooltip-append-to-body=\"\" icon=severity_error class=\"xui-icon xui-icon-severity_error filled\"> <svg version=1.1 id=Layer_1 xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink xmlns:a=http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/ x=0px y=0px viewBox=\"0 0 20 20\" width=20 height=20 enable-background=\"new 0 0 20 20\" xml:space=preserve> <polygon fill=#D50000 points=\"5.858,20 0,14.143 0,5.858 5.858,0 14.143,0 20,5.858 20,14.143 14.143,20 \"></polygon> <path fill=none stroke=#FFFFFF stroke-width=1.5 stroke-miterlimit=10 d=\"M14,6l-8,8 M6,6l8,8\"></path> </svg> </i> <span class=\"xui-text-critical xui-margin-lgl\">{{wizard.data.data[\"error\"]}}</span> </div> </error-info> <xui-page-header> <div class=\"xui-page-content__header clm-edit-ipam-settings__header\"> <span class=\"xui-h1 xui-page-content__header-title\" _t> Edit IPAM settings of <strong>{{wizard.account.Name || \"Account\"}}</strong> </span> </div> </xui-page-header> <div class=clm-edit-ipam-settings> <div ng-include src=\"'/Orion/IPAM/Admin/AccountWizard/IpamSettingsWizardStep.html'\" ng-init=wizard.onTemplateIncluded()> </div> <div class=\"xui-wizard__footer clm-edit-ipam-settings__footer\"> <xui-button id=Save display-style=primary is-disabled=\"wizard.isInvalid || wizard.isUpdating\" ng-click=wizard.save() _t> Save </xui-button> <xui-button id=Cancel display-style=tertiary ng-click=wizard.redirectToManageAccounts() _t> Cancel </xui-button> </div> </div> </xui-page-content> ";

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IPRequest = /** @class */ (function () {
    function IPRequest() {
    }
    return IPRequest;
}());
exports.default = IPRequest;


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = "<breadcrumbs></breadcrumbs> <xui-page-content class=xui-page-content--no-padding page-title=\"_t(IP Address Request)\" _ta Templates page-layout=full-width id=iprequest-wizard> <div class=iprequest-wizard> <xui-wizard on-next-step=\"vm.onNextStep(from, to, cancellation)\" on-enter-step=vm.onEnterStep(step) ng-model=vm current-step=vm.currentWizardStep current-step-index=vm.currentStepIndex navigation=headerDisabled can-go-back=vm.canGoBack() finish-text=\"_t(Request another IP Address(es))\" _ta next-busy-text=\"_t(Please wait...)\" _ta can-go-next=vm.canGoNext() on-finish=vm.onFinish() on-cancel=vm.onCancel() additional-buttons=vm.additionalButtons> <xui-wizard-step ng-repeat=\"step in vm.steps\" label={{::step.label}} description={{::step.description}} next-text={{step.nextText}} title={{::step.title}} short-title={{::step.shortTitle}}> <ng-include src=step.templateUrl></ng-include> </xui-wizard-step> </xui-wizard> </div> </xui-page-content> ";

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpamErrorHandlerService = /** @class */ (function () {
    /** @ngInject */
    IpamErrorHandlerService.$inject = ["toastService"];
    function IpamErrorHandlerService(toastService) {
        this.toastService = toastService;
    }
    IpamErrorHandlerService.prototype.handle = function (ex) {
        if (ex.status === 404) {
            this.toastService.error("The entity does not exist in the database.");
        }
        else if (ex.status === 500) {
            this.toastService.error("Internal server error.");
        }
        if (typeof ex.data === "string" && ex.data) {
            this.toastService.error(ex.data);
        }
        else if (ex.data != null && ex.data.message != null) {
            this.toastService.error(ex.data.message);
        }
    };
    return IpamErrorHandlerService;
}());
exports.IpamErrorHandlerService = IpamErrorHandlerService;


/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=\"sw-ipam-iprequest-custom-property-editor-value xui-dropdown--justified\"> <xui-dropdown ng-model=item.Value is-required=item.Required is-editable=true clear-on-blur=true items-source=vm.boolSource></xui-dropdown> </div> ";

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=sw-ipam-iprequest-custom-property-editor-value> <sw-ipam-date-time-picker required=item.Required value=item.Value> </sw-ipam-date-time-picker> </div>";

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-costom-property-editor> <div class=\"xui-text-h3 xui-margin-lgb\" ng-show=vm.itemsSource.length _t>{{::vm.title}}</div> <ul> <li ng-repeat=\"item in vm.itemsSource\"> <div ng-include=vm.getTemplate(item)></div> </li> </ul> </div>";

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-iprequest-custom-property-editor-value> <xui-textbox ng-model=item.Value caption={{item.Name}} is-required=item.Required xui-integer-validator> <div ng-message=integer _t>Value should be integer!</div> </xui-textbox> </div> ";

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-iprequest-custom-property-editor-value> <xui-textbox ng-model=item.Value caption={{item.Name}} is-required=item.Required ng-pattern=/^-?\\d*(\\.\\d+)?$/ > <div ng-message=pattern _t>Value should be a floating point number!</div> </xui-textbox> </div> ";

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=\"sw-ipam-iprequest-custom-property-editor-value xui-dropdown--justified\"> <xui-dropdown ng-model=item.Value is-required=item.Required is-editable=true clear-on-blur=true items-source=item.RestrictedValues></xui-dropdown> </div> ";

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=sw-ipam-iprequest-custom-property-editor-value> <xui-textbox ng-model=item.Value is-required=item.Required></xui-textbox> </div> ";

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<div id=ipamDateTimePicker class=\"sw-ipam-date-time-picker row\"> <div class=col-md-4> <xui-date-picker ng-model=vm.value is-required=vm.required preserve-insignificant=true> <div ng-message=date _t>Chosen date is invalid!</div> </xui-date-picker> </div> <div class=\"col-md-8 xui-dropdown--justified\"> <xui-dropdown ng-model=vm.chosenTime is-required=vm.required is-editable=true clear-on-blur=true items-source=vm.timeSource on-changed=vm.dateTimeChanged()></xui-dropdown> </div> </div>";

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div> <form name=editAddress> <xui-textbox name=hostnameEdit caption=Hostname ng-model=vm.dialogOptions.viewModel.Hostname.Value validators=\"maxlength=255\" ng-pattern=/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$/ ng-trim=true is-required=vm.dialogOptions.viewModel.Hostname.IsMandatory> <span ng-show=editAddress.hostnameEdit.$error.pattern _t>Invalid hostname!</span> </xui-textbox> <xui-textbox name=macAddressEdit caption=\"MAC Address\" ng-model=vm.dialogOptions.viewModel.MacAddress.Value validators=\"maxlength=40\" ng-pattern=/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/ is-required=vm.dialogOptions.viewModel.MacAddress.IsMandatory> <span ng-show=editAddress.macAddressEdit.$error.pattern _t>Invalid MAC address!</span> </xui-textbox> <br/> <sw-ipam-custom-property-editor items-source=vm.dialogOptions.viewModel.CustomProperties title=\"Custom Properties\"/> </form> </div> </xui-dialog>";

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = "<div id=ipamIpAddressRequestWidget class=sw-ip-address-requests> <div> <xui-filtered-list-v2 state=vm.listState dispatcher=vm.listDispatcher remote-control=vm.listControl controller=vm> <sw-grid-toolbar-container> <xui-toolbar> <div ng-if=\"vm.subnetDetails && vm.subnetDetails.GroupId > 0\" class=\"ipaddressrequests-widget-subnetDetails ipaddressrequests-widget-friendlyNameDiv\"> <b>Subnet:</b> {{vm.subnetDetails.Address}}/{{vm.subnetDetails.Cidr}} ({{vm.subnetDetails.FriendlyName}}) </div> <xui-toolbar-item> <xui-button display-style=primary is-disabled=vm.disableSubnetSelector ng-click=vm.selectSubnet() _t> Select subnet </xui-button> </xui-toolbar-item> </xui-toolbar> </sw-grid-toolbar-container> </xui-filtered-list-v2> </div> <div class=ipaddressrequests-widget-submitButtonRow> <span class=ipaddressrequests-widget-summaryLabel _t> {{vm.allItemsCount}} <span _t>Total</span>, {{vm.acceptedCount}} <span _t>Accepted</span>, {{vm.deniedCount}} <span _t>Denied IPs</span> </span> <div ng-show=!vm.ipRequestDetails.IsDisabled class=ipaddressrequests-widget-submitButton> <xui-button display-style=secondary ng-click=vm.reject() _t> Deny Request </xui-button> </div> <div class=ipaddressrequests-widget-submitButton> <xui-button is-disabled=vm.isDisabledSubmitButton display-style=primary ng-click=vm.submit()> {{vm.submitButtonText}} </xui-button> </div> </div> </div> ";

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<div id=ipamIpAddressSelector class=sw-ipam-ipAddress-selector> <xui-search is-disabled=vm.isDisabled value=vm.ngModel placeholder={{::vm.placeholder}} on-search=vm.onSearch(value) on-change=vm.onChange(vm.ngModel) items-source=vm.ipAddresses display-value=vm.displayValue ng-blur=vm.onBlur(vm.ngModel) search-on-change=true> </xui-search> </div>";

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = "<ul class=ipaddressrequests-widget__popover-ul> <li class=ipaddressrequests-widget__popover-li ng-repeat=\"cp in item.NotEmptyCustomProperties\"> <div class=xui-text-l> <span><b>{{cp.Name}}</b></span> </div> <div class=xui-text-p> <span>{{cp.Value.toLocaleString()}}</span> </div> <hr ng-if=!$last /> </li> </ul> ";

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=\"!(vm.allItems && vm.allItems.length == 0)\" class=\"row ipaddressrequests-widget__header\"> <div class=col-xs-2> <span _t>Status</span> </div> <div class=col-xs-3> <span _t>IP Address</span> </div> <div class=col-xs-2> <span _t>Hostname</span> </div> <div class=col-xs-2> <span _t>MAC Address</span> </div> <div class=col-xs-2> <span _t>Custom properties</span> </div> <div class=col-xs-1> </div> </div> ";

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = "<div class=row> <div class=col-xs-2> <xui-switch ng-model=item.IsAccepted ng-disabled=item.IsDisabled /> </div> <div id=ipamIpAddressSelector class=col-xs-3> <sw-ipam-ipAddress-selector ng-model=item ip-addresses=vm.ipAddresses is-disabled=item.IsDisabled on-item-select=vm.handleOnSelect> </sw-ipam-ipAddress-selector> </div> <div class=col-xs-2> {{vm.showNoneIfEmpty(item.Hostname.Value)}} </div> <div class=col-xs-2> {{vm.showNoneIfEmpty(item.MacAddress.Value)}} </div> <div class=col-xs-2> <xui-popover ng-show=\"vm.countCustomProperties(item.NotEmptyCustomProperties) > 0\" xui-popover-title=\"_t(Custom Properties)\" _ta xui-popover-content=vm.getCpPopoverTemplate() xui-popover-trigger=mouseenter> <span class=ipaddressrequests-widget-customPropertyDotted> {{vm.countCustomProperties(item.NotEmptyCustomProperties)}} of {{vm.countCustomProperties(item.CustomProperties)}} </span> </xui-popover> <span ng-show=\"vm.countCustomProperties(item.NotEmptyCustomProperties) === 0 && vm.countCustomProperties(item.CustomProperties) > 0\"> {{vm.countCustomProperties(item.NotEmptyCustomProperties)}} of {{vm.countCustomProperties(item.CustomProperties)}} </span> <span ng-show=\"vm.countCustomProperties(item.NotEmptyCustomProperties) === 0 && vm.countCustomProperties(item.CustomProperties) === 0\"> (none) </span> </div> <div class=col-xs-1 ng-show=!item.IsDisabled> <xui-button display-style=link icon=edit size=small ng-click=vm.editIpRequestAddress(item)></xui-button> </div> </div> ";

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div> <form name=submitRequest> <p>{{vm.dialogOptions.message}}</p> <p _t>This cannot be undone.</p> <div ng-if=\"vm.dialogOptions.viewModel.DhcpServers && vm.dialogOptions.viewModel.DhcpServers.length > 0\"> <b><p _t>For DHCP reservation, select DHCP server:</p></b> <xui-dropdown is-required=\"vm.dialogOptions.viewModel.DhcpServers && vm.dialogOptions.viewModel.DhcpServers.length > 0\" display-value=FriendlyName class=xui-dropdown--justified ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpServer items-source=vm.dialogOptions.viewModel.DhcpServers> <div ng-message=required>This field is required</div> </xui-dropdown> <b><p _t>Supported Types:</p></b> <xui-radio ng-value=1 class=ipaddressrequests-widget-radio ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpReservationType _t> DHCP only </xui-radio> <xui-radio ng-value=2 class=ipaddressrequests-widget-radio ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpReservationType _t> BOOTP only </xui-radio> <xui-radio ng-value=3 class=ipaddressrequests-widget-radio ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpReservationType _t> Both </xui-radio> </div> <xui-textbox rows=4 caption=\"_t(Comment for requester:)\" _ta ng-model=vm.dialogOptions.viewModel.RequestDetails.AdminComment> </xui-textbox> </form> </div> </xui-dialog>";

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-subnet-selector> <xui-filtered-list-v2 state=vm.listState dispatcher=vm.listDispatcher remote-control=vm.listControl controller=vm></xui-filtered-list-v2> </div> ";

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<div class=row> <div class=\"subnet-status-icon col-md-1\"> <xui-icon icon=subnet status={{item.StatusIconPostfix}} css-class=icon-test-style></xui-icon> </div> <div class=col-md-7> <div class=\"xui-text-l ipaddressrequests-widget-friendlyNameDiv\">{{item.FriendlyName}}</div> <div class=xui-text-s>{{item.Address}}/{{item.Cidr}}</div> </div> <div class=col-md-2> <div class=xui-text-l>{{item.AvailableCount ? item.AvailableCount : 0}}</div> <div class=xui-text-s _t>Available IPs</div> </div> <div class=col-md-2> <xui-button ng-click=vm.onSelectClick(item.GroupId) display-style=primary _t> Select </xui-button> </div> </div> ";

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <sw-ipam-subnet-selector ip-count=vm.dialogOptions.viewModel.IpCount selected-group-id=vm.dialogOptions.viewModel.GroupId /> </xui-dialog> ";

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-requester-details> <ul> <li ng-repeat=\"item in vm.listItems\" class=requesterDetails-widget> <div class=xui-text-l> <span class=display-name>{{item.DisplayName}}</span> </div> <div class=xui-text-p> <span>{{item.Value}}</span> </div> <hr ng-if=!$last /> </li> </ul> </div> ";

/***/ }),
/* 29 */,
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ipRequestSettingsFieldsData_1 = __webpack_require__(64);
var NewListItemHandlerRegistry_1 = __webpack_require__(65);
var TextBoxNewListItemHandler_1 = __webpack_require__(66);
var DropDownNewListItemHandler_1 = __webpack_require__(67);
var IpRequestSettingsController = /** @class */ (function () {
    /** @ngInject */
    IpRequestSettingsController.$inject = ["$window", "ipamErrorHandlerService", "xuiDialogService", "ipRequestSettingsService"];
    function IpRequestSettingsController($window, ipamErrorHandlerService, xuiDialogService, ipRequestSettingsService) {
        var _this = this;
        this.$window = $window;
        this.ipamErrorHandlerService = ipamErrorHandlerService;
        this.xuiDialogService = xuiDialogService;
        this.ipRequestSettingsService = ipRequestSettingsService;
        this.newListItemRegistry = new NewListItemHandlerRegistry_1.NewListItemHandlerRegistry();
        this.buttonSaveClick = function () {
            if (!_this.isValid()) {
                return;
            }
            _this.ipRequestSettingsService
                .updateSettings(_this.ipRequestSettings)
                .then(function (response) {
                if (response) {
                    _this.redirectToManageAccounts();
                }
            })
                .catch(function (ex) {
                _this.ipamErrorHandlerService.handle(ex);
            });
        };
        this.buttonCancelClick = function () {
            _this.redirectToManageAccounts();
        };
    }
    IpRequestSettingsController.prototype.$onInit = function () {
        this.getSettings();
        this.newListItemRegistry.register(new TextBoxNewListItemHandler_1.TextBoxNewListItemHandler("Requester Details", "ADD NEW FIELD"));
        this.newListItemRegistry.register(new DropDownNewListItemHandler_1.DropDownNewListItemHandler("Custom Properties", "ADD CUSTOM PROPERTY"));
    };
    IpRequestSettingsController.prototype.onRemove = function (value) {
        var requesterDetails = this.ipRequestSettings.requesterDetailsFieldsList;
        requesterDetails.forEach(function (item, index) {
            if (item.fieldName === value) {
                requesterDetails
                    .splice(index, 1);
            }
        });
    };
    IpRequestSettingsController.prototype.onAdd = function (value) {
        var obj = new ipRequestSettingsFieldsData_1.default();
        obj.fieldName = value.trim().replace(/[\s]/g, "");
        obj.isRequired = false;
        obj.description = value;
        this.ipRequestSettings
            .requesterDetailsFieldsList
            .push(obj);
    };
    IpRequestSettingsController.prototype.onAddCP = function (selectedItem) {
        var _this = this;
        var obj = new ipRequestSettingsFieldsData_1.default();
        obj.fieldName = selectedItem.name;
        obj.isRequired = selectedItem.mandatory;
        obj.description = selectedItem.name;
        obj.isOrionRequired = selectedItem.mandatory;
        this.ipRequestSettings
            .customPropertiesData
            .customPropertiesFieldsList.push(obj);
        this.filteredCustomProperties
            .forEach(function (item, index) {
            if (item.name === selectedItem.name) {
                _this.filteredCustomProperties
                    .splice(index, 1);
            }
        });
    };
    IpRequestSettingsController.prototype.onRemoveCP = function (selectedItem) {
        var _this = this;
        var customPropertiesData = this.ipRequestSettings
            .customPropertiesData;
        customPropertiesData
            .customPropertiesFieldsList
            .forEach(function (item, index) {
            if (item.fieldName === selectedItem) {
                customPropertiesData
                    .customPropertiesFieldsList
                    .splice(index, 1);
                var obj = { name: selectedItem, mandatory: item.isOrionRequired };
                _this.filteredCustomProperties.push(obj);
            }
        });
    };
    IpRequestSettingsController.prototype.redirectToManageCustomProperties = function () {
        this.$window.location.href = "/Orion/Admin/CPE/Default.aspx";
    };
    IpRequestSettingsController.prototype.isValid = function () {
        this.setDirtyOnFieldsWithError();
        return this.ipRequestSettingsForm.$valid;
    };
    IpRequestSettingsController.prototype.setDirtyOnFieldsWithError = function () {
        angular.forEach(this.ipRequestSettingsForm.$error.required, function (field) {
            field.$setDirty();
        });
    };
    IpRequestSettingsController.prototype.redirectToManageAccounts = function () {
        this.$window.location.href = "/Orion/IPAM/Admin/Admin.Overview.aspx";
    };
    IpRequestSettingsController.prototype.getSettings = function () {
        var _this = this;
        this.ipRequestSettingsService
            .getSettings()
            .then(function (result) {
            _this.ipRequestSettings = result;
            var customProperties = _this.ipRequestSettings
                .customPropertiesData
                .allCustomProperties;
            var customPropertiesFieldsList = _this.ipRequestSettings
                .customPropertiesData
                .customPropertiesFieldsList;
            _this.ipRequestSettings.customPropertiesData.customPropertiesFieldsList.forEach(function (element) {
                var mandatoryInfo = _.find(_this.ipRequestSettings.customPropertiesData.allCustomProperties, function (cp) { return element.fieldName === cp.name; });
                if (mandatoryInfo !== undefined) {
                    element.isOrionRequired = mandatoryInfo.mandatory;
                    if (element.isOrionRequired && !element.isRequired) {
                        _this.xuiDialogService.showWarning({
                            title: "Action required",
                            message: "You have made changes to Orion Custom Properties. Please save your IP Request Settings."
                        });
                    }
                    element.isRequired = element.isOrionRequired ? true : element.isRequired;
                }
            });
            var isNotInCustomPropertiesFieldsList = function (customProperty) {
                return _.find(customPropertiesFieldsList, function (filteredCp) { return customProperty.name === filteredCp.fieldName; }) === undefined;
            };
            _this.filteredCustomProperties = customProperties.filter(function (cp) { return isNotInCustomPropertiesFieldsList(cp); });
        })
            .catch(function (ex) {
            _this.ipamErrorHandlerService.handle(ex);
        });
    };
    return IpRequestSettingsController;
}());
exports.default = IpRequestSettingsController;


/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title=\"_t(IP Request Settings)\" class=xui-page-content _ta> <div ng-form=vm.ipRequestSettingsForm> <div class=iprequest-settings-page> <div class=row> <div class=\"col-sm-6 col-md-6\"> <div class=\"xui-text-h2 xui-margin-lgb\" _t> Requester Details fields </div> <sw-ipam-fieldslist-editor items-source=vm.ipRequestSettings.requesterDetailsFieldsList new-list-item-handler=\"vm.newListItemRegistry.get('text-box-template')\" on-remove-button-click=vm.onRemove(value) on-save-button-click=vm.onAdd(value)> </sw-ipam-fieldslist-editor> </div> </div> </div> <xui-divider></xui-divider> <div class=iprequest-settings-page> <div class=row> <div class=\"col-sm-6 col-md-6\"> <div class=\"xui-text-h2 xui-margin-lgb\" _t> Basic fields settings </div> <div class=\"iprequest-settings-page-fieldname col-md-4\"> <span> <b>Host Name</b> </span> </div> <div class=\"iprequest-settings-page-checkbox col-md-2\"> <xui-checkbox ng-model=vm.ipRequestSettings.hostnameRequired _t>Required </xui-checkbox> </div> </div> </div> <div class=row> <div class=\"col-sm-6 col-md-6\"> <div class=\"iprequest-settings-page-fieldname col-md-4\"> <span> <b>MAC Address</b> </span> </div> <div class=\"iprequest-settings-page-checkbox col-md-2\"> <xui-checkbox ng-model=vm.ipRequestSettings.macAddressRequired _t>Required </xui-checkbox> </div> </div> </div> </div> <xui-divider></xui-divider> <div class=iprequest-settings-page> <div class=row> <div class=\"col-sm-6 col-md-6\"> <div class=\"xui-text-h2 xui-margin-lgb\" _t> Custom Properties </div> <sw-ipam-fieldslist-editor items-source=vm.ipRequestSettings.customPropertiesData.customPropertiesFieldsList new-list-item-handler=\"vm.newListItemRegistry.get('drop-down-template')\" on-remove-button-click=vm.onRemoveCP(value) on-save-button-click=vm.onAddCP(value) dropdown-items-source=vm.filteredCustomProperties> </sw-ipam-fieldslist-editor> </div> </div> <div class=row> <div class=\"col-sm-6 col-md-6\"> <div ng-show=\"vm.filteredCustomProperties.length === 0 && vm.ipRequestSettings.customPropertiesData.customPropertiesFieldsList.length === 0\" class=col-md-6> <span><b _t>NO CUSTOM PROPERTIES DEFINED FOR IP NODES</b></span> </div> </div> </div> <div class=row> <div class=\"col-sm-6 col-md-6\"> <xui-button size=large display-style=tertiary icon=edit ng-click=vm.redirectToManageCustomProperties() _t>Manage custom properties</xui-button> </div> </div> </div> <xui-divider></xui-divider> <div class=iprequest-settings-page> <div class=row> <div class=\"col-sm-6 col-md-6\"> <div class=\"xui-text-h2 xui-margin-lgb\" _t> Notification Settings </div> <xui-textbox ng-model=vm.ipRequestSettings.adminsEmails caption=\"Admins to notify\" ng-pattern=/^(,?[0-9a-zA-Z:\\s]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})+$/ > <div ng-message=pattern _t>This field must be a valid email address</div> </xui-textbox> <div class=\"xui-textbox__help xui-help-hint xui-input-help\" _t> E-mail addresses should be separated by commas. Click <a href=/Orion/Admin/AlertSettings.aspx _t>here</a> to manage your SMTP server settings. </div> <xui-textbox rows=4 ng-model=vm.ipRequestSettings.adminSignature caption=\"_t(Email signature for Admin)\" _ta help-text=\"_t(This text will appear at the bottom of system sent emails)\" _ta> </xui-textbox> <xui-textbox rows=4 ng-model=vm.ipRequestSettings.requesterSignature caption=\"_t(Email signature for Requester)\" _ta help-text=\"_t(This text will appear at the bottom of system sent emails)\" _ta> </xui-textbox> </div> </div> </div> <xui-divider></xui-divider> <div class=iprequest-settings-page> <div class=row> <div class=\"col-sm-6 col-md-6\"> <div class=\"xui-text-h2 xui-margin-lgb\" _t> Subnet Settings </div> <div class=\"iprequest-settings-page-fieldname col-md-4\"> <span> <b _t>Exclude scopes from subnet selection</b> </span> </div> <div class=\"iprequest-settings-page-checkbox col-md-2\"> <xui-checkbox ng-model=vm.ipRequestSettings.excludeScopes _t>Required </xui-checkbox> </div> </div> </div> </div> <div class=iprequest-settings-page-footer> <xui-page-footer> <div class=text-left> <xui-button ng-click=vm.buttonSaveClick() display-style=primary> <span _t>Save Changes</span> </xui-button> <xui-button ng-click=vm.buttonCancelClick() display-style=tertiary> <span _t>Cancel</span> </xui-button> </div> </xui-page-footer> </div> </div> </xui-page-content> ";

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = "<div class=row> <div class=col-md-4> <div class=subnet-status-icon> <xui-icon icon=subnet status={{item.StatusIconPostfix}} css-class=icon-test-style></xui-icon> </div> <div class=col-md-10> <div class=xui-text-l>{{item.FriendlyName}}</div> <div class=xui-text-s>{{item.Address}}/{{item.Cidr}}</div> </div> </div> <div class=col-md-4> <div class=xui-text-l>{{item.Location}}</div> <div class=xui-text-s ng-show=item.Location _t>Location</div> </div> <div class=col-md-4> <div class=xui-text-l>{{item.AvailableCount ? item.AvailableCount : 0}}</div> <div class=xui-text-s _t>Available IPs</div> </div> </div> ";

/***/ }),
/* 33 */
/***/ (function(module, exports) {

module.exports = "<xui-filtered-list id=sw-ipam-subnet-selector-wizard class=\"xui-edge-definer ipam-subnet-selector__filtered-list\" items-source=vm.itemsSource on-refresh=\"vm.refreshData(filters, pagination, sorting, searching)\" filter-properties=vm.filterProperties filter-values=vm.filterValues available-filter-properties=vm.filterProperties options=vm.optionsNotEmpty pagination-data=vm.pagination sorting=vm.sorting remote-control=vm.filteredListRemoteControl selection=vm.selection controller=vm busy-message=_t(Loading...) _ta busy-show-cancel-button=false> </xui-filtered-list> ";

/***/ }),
/* 34 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var FieldsListEditorController = /** @class */ (function () {
    /** @ngInject */
    FieldsListEditorController.$inject = ["$templateCache", "toastService"];
    function FieldsListEditorController($templateCache, toastService) {
        var _this = this;
        this.$templateCache = $templateCache;
        this.toastService = toastService;
        this.setAddNewItemButtonVisibility = function (visible) {
            _this.newListItemHandler.isAddNewItemButtonVisible = visible;
        };
        this.removeItem = function (name) {
            _this.onRemoveButtonClick(name);
        };
    }
    FieldsListEditorController.prototype.$onInit = function () {
        this.dropdownItemsSource = [];
    };
    FieldsListEditorController.prototype.onAdd = function () {
        if (!this.isValid()) {
            if (this.newListItemHandler.invalidMessage) {
                this.toastService.error(this.newListItemHandler.invalidMessage);
            }
            return;
        }
        this.onSaveButtonClick({ value: this.newListItemHandler.fieldPropertyValue });
        this.resetAddControls();
    };
    FieldsListEditorController.prototype.onCancelButtonClick = function () {
        this.resetAddControls();
    };
    FieldsListEditorController.prototype.getTemplateUrl = function () {
        if (this.isValidTemplate(this.newListItemHandler.templateId)) {
            return this.newListItemHandler.templateId;
        }
    };
    FieldsListEditorController.prototype.resetAddControls = function () {
        this.newListItemHandler.init();
        this.setAddNewItemButtonVisibility(true);
    };
    FieldsListEditorController.prototype.isValidTemplate = function (templateUrl) {
        return this.$templateCache.get(templateUrl);
    };
    FieldsListEditorController.prototype.isValid = function () {
        this.setDirtyOnFieldsWithError();
        return this.fieldsListEditor.$valid && this.newListItemHandler.isValid(this.itemsSource);
    };
    FieldsListEditorController.prototype.setDirtyOnFieldsWithError = function () {
        angular.forEach(this.fieldsListEditor.$error.required, function (field) {
            field.$setDirty();
        });
    };
    return FieldsListEditorController;
}());
exports.FieldsListEditorController = FieldsListEditorController;


/***/ }),
/* 36 */
/***/ (function(module, exports) {

module.exports = "<div ng-form=vm.fieldsListEditor class=sw-ipam-fieldslist-editor> <ul> <li ng-repeat=\"item in vm.itemsSource\"> <div class=\"container fields\"> <div class=\"sw-ipam-fieldslist-editor-fieldname col-md-3\"> <span><b>{{item.description}}</b></span> </div> <div class=\"sw-ipam-fieldslist-editor-checkbox col-md-1\"> <xui-checkbox ng-model=item.isRequired is-disabled=item.isOrionRequired _t>Required </xui-checkbox> </div> <div class=col-md-2> <div ng-show=!item.isMandatory> <xui-button size=large display-style=tertiary icon=delete ng-click=\"vm.removeItem({value: item.fieldName})\" _t>Remove </xui-button> </div> </div> </div> </li> <li> <div class=fixed-top ng-include=vm.getTemplateUrl()></div> </li> </ul> </div> ";

/***/ }),
/* 37 */
/***/ (function(module, exports) {

module.exports = "<div class=\"container fields\"> <div ng-show=\"vm.newListItemHandler.isAddNewItemButtonVisible && vm.dropdownItemsSource.length > 0\" class=\"sw-ipam-fieldslist-editor-button col-md-2\"> <xui-button size=large display-style=tertiary icon=add ng-click=vm.setAddNewItemButtonVisibility(false) _t>{{vm.newListItemHandler.addNewItemButtonText}}</xui-button> </div> <div ng-show=!vm.newListItemHandler.isAddNewItemButtonVisible class=\"sw-ipam-fieldslist-editor-field-property col-md-3\"> <div class=xui-dropdown--justified> <xui-dropdown ng-model=vm.newListItemHandler.fieldPropertyValue items-source=vm.dropdownItemsSource dropdown-append-to-body=true placeholder={{::vm.newListItemHandler.placeholder}} display-value=name> </xui-dropdown> </div> </div> <div ng-show=!vm.newListItemHandler.isAddNewItemButtonVisible class=\"sw-ipam-fieldslist-editor-button col-md-1\"> <xui-button size=large display-style=tertiary icon=add ng-click=vm.onAdd() _t>ADD </xui-button> </div> <div ng-show=!vm.newListItemHandler.isAddNewItemButtonVisible class=col-md-1> <xui-button size=large display-style=tertiary icon=cancel ng-click=vm.onCancelButtonClick() _t>CANCEL </xui-button> </div> </div> ";

/***/ }),
/* 38 */
/***/ (function(module, exports) {

module.exports = "<div class=\"container fields\"> <div ng-show=vm.newListItemHandler.isAddNewItemButtonVisible class=\"sw-ipam-fieldslist-editor-button col-md-2\"> <xui-button size=large display-style=tertiary icon=add ng-click=vm.setAddNewItemButtonVisibility(false) _t>{{vm.newListItemHandler.addNewItemButtonText}}</xui-button> </div> <div ng-show=!vm.newListItemHandler.isAddNewItemButtonVisible class=\"sw-ipam-fieldslist-editor-field-property col-md-3\"> <xui-textbox ng-model=vm.newListItemHandler.fieldPropertyValue is-required=!vm.newListItemHandler.isAddNewItemButtonVisible validators=\"maxlength=255\"> <div ng-message=required _t>This field is required</div> </xui-textbox> </div> <div ng-show=!vm.newListItemHandler.isAddNewItemButtonVisible class=\"sw-ipam-fieldslist-editor-button col-md-1\"> <xui-button size=large display-style=tertiary icon=add ng-click=vm.onAdd() _t>ADD </xui-button> </div> <div ng-show=!vm.newListItemHandler.isAddNewItemButtonVisible class=col-md-1> <xui-button size=large display-style=tertiary icon=cancel ng-click=vm.onCancelButtonClick() _t>CANCEL </xui-button> </div> </div>";

/***/ }),
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(46);
module.exports = __webpack_require__(47);


/***/ }),
/* 46 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(48);
var config_1 = __webpack_require__(49);
var index_1 = __webpack_require__(50);
var index_2 = __webpack_require__(52);
var index_3 = __webpack_require__(70);
var index_4 = __webpack_require__(98);
var templates_1 = __webpack_require__(103);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("ipam.services", []);
angular.module("ipam.templates", []);
angular.module("ipam.components", []);
angular.module("ipam.filters", []);
angular.module("ipam.providers", []);
angular.module("ipam", [
    "orion",
    "filtered-list",
    "ipam.services",
    "ipam.templates",
    "ipam.components",
    "ipam.filters",
    "ipam.providers"
]);
// create and register Xui (Orion) module wrapper
var ipam = Xui.registerModule("ipam");
exports.default = ipam;


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: ipam");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(51);
exports.default = function (module) {
    module.service("ipamConstants", constants_1.default);
};


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(53);
var index_2 = __webpack_require__(58);
var index_3 = __webpack_require__(63);
exports.default = function (module) {
    // register views
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
};
var rootState = function ($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'ipam/cloud' url
    $stateProvider.state("ipam", {
        url: "/ipam",
        controller: function ($state) {
            $state.go("home");
        }
    });
};


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var cloudDnsZones_controller_1 = __webpack_require__(54);
var cloudDnsZones_config_1 = __webpack_require__(56);
__webpack_require__(4);
__webpack_require__(57);
exports.default = function (module) {
    module.controller("CloudDnsZonesController", cloudDnsZones_controller_1.default);
    module.config(cloudDnsZones_config_1.default);
};


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var cloudDnsZone_1 = __webpack_require__(55);
function isRejected(promise) {
    return promise.$$state.status === 2;
}
var CloudDnsZonesController = /** @class */ (function () {
    /** @ngInject */
    CloudDnsZonesController.$inject = ["$scope", "$log", "swApi", "$timeout", "$templateCache", "$q", "$location", "swSwisDataMappingService", "swSearchHistoryService", "$translate", "$httpParamSerializerJQLike"];
    function CloudDnsZonesController($scope, $log, swApi, $timeout, $templateCache, $q, $location, swSwisDataMappingService, swSearchHistoryService, $translate, $httpParamSerializerJQLike) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.swApi = swApi;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.$q = $q;
        this.$location = $location;
        this.swSwisDataMappingService = swSwisDataMappingService;
        this.swSearchHistoryService = swSearchHistoryService;
        this.$translate = $translate;
        this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
        this.filterValues = {};
        this.filterPropertyGroups = [];
        this.itemsSource = [];
        this.rawData = [];
        this.firstLoadHappened = false;
        this.usedSearchTerm = "";
        this.getFilterProperties = function () {
            return _this.swApi.api(false).one(_this.filtersUrl).get()
                .then(function (result) {
                return result;
            });
        };
        this.defaultSystemProps = [];
        this.defaultCustomProps = [];
        this.searchRemoteControl = {};
        this.filteredListRemoteControl = {};
        this.searchUrl = "ipam/cloudDnsZones/get";
        this.filtersUrl = "ipam/cloudDnsZones/filters";
        this.availableFiltersUrl = "search/metadata/available-filters";
        this.vendorFilterTemplateUrl = "search-filter-vendor-template";
        this.propertiesToRequest = [
            "status",
            "childStatus",
            "caption",
            "displayName",
            "detailsUrl",
            "node.displayName",
            "node.detailsUrl",
            "serverName",
            "sysName",
            "vendor",
            "vendorIcon",
            "ipAddress",
            "instanceType",
            "machineType"
        ];
        this.onHistoryItemClicked = function (search) {
            _this.searchRemoteControl.search(search);
        };
        this.onSearch = function (item) {
            if (!item) {
                _this.usedSearchTerm = "";
                _this.pagination.total = 1;
                return _this.instant();
            }
            _this.searchTerm = item;
            _this.swSearchHistoryService.addToHistory(item);
            _this.usedFilterValues = null;
            return _this.filteredListRemoteControl.refreshListData();
        };
        this.cancel = function () {
            _this.rejectPendingRefresh();
            _this.$log.info("Search cancelled by the user.");
        };
        this.clear = function () { return _this.onSearch(""); };
        this.formatOrderBy = function (sorting) {
            if (!sorting) {
                return "";
            }
            return "" + (sorting.direction.toLowerCase() === "asc" ? "+" : "-") + sorting.sortBy.id;
        };
        this.showEmptySearch = function () {
            return (_this.pagination.total !== 0
                && _this.usedSearchTerm === ""
                && _this.firstLoadHappened);
        };
        this.hasAnyFilter = function (usedFilters) {
            return (usedFilters
                && Object.keys(usedFilters).some(function (key) { return usedFilters[key].length > 0; }));
        };
        this.showNoResult = function () {
            return (_this.pagination.total === 0
                && _this.usedSearchTerm !== ""
                && _this.firstLoadHappened
                && !_this.hasAnyFilter(_this.usedFilterValues));
        };
        this.showFilteredList = function () {
            return (!_this.showEmptySearch()
                && !_this.showNoResult()
                && _this.firstLoadHappened);
        };
        this.getSearchFilters = function (filterValues) {
            if (!filterValues) {
                return [""];
            }
            return _.map(_.filter(Object.keys(filterValues), function (filterId) { return filterValues[filterId].length > 0; }), function (filterId) { return filterId + ":" + filterValues[filterId].join("|"); });
        };
        this.getDataForFilter = function (url, data) {
            return _this.useApi.one(url + "?" + _this.$httpParamSerializerJQLike(data)).get();
        };
        this.getFilters = function (searching) {
            return _this.getDataForFilter(_this.filtersUrl, {
                "filters": _.map(_this.filterProperties, function (filter) { return filter.refId; }),
                "q": _this.searchTerm || searching,
                "filterBy": _this.getSearchFilters(_this.filterValues)
            });
        };
    }
    CloudDnsZonesController.prototype.initPagination = function () {
        this.pagination = this.pagination || {};
        this.pagination.pageSize = this.options.pageSize;
        this.pagination.page = this.pagination.page || 1;
        this.pagination.total = 5;
    };
    CloudDnsZonesController.prototype.$onInit = function () {
        var _this = this;
        var itemTemplate = __webpack_require__(3);
        this.searchText = this.searchText || this.$translate.instant("orion_search_searchText");
        this.$templateCache.put("search-item-template", itemTemplate);
        this.$templateCache.put(this.vendorFilterTemplateUrl, "<sw-vendor-icon vendor=\"{{::item.vendor}}\" css-class=\"sw-search__item-custom-property\">\n             </sw-vendor-icon> {{::item.title}}");
        this.sideBarSettings = this.sideBarSettings || {};
        this.options = {
            selectionProperty: "DisplayName",
            showAddRemoveFilterProperty: false,
            hideSearch: false,
            rowPadding: "none",
            showEmptyFilterProperties: true,
            categorizedFilterPanel: false,
            showMorePropertyValuesThreshold: 10,
            allowSelectAllPages: false,
            pageSize: 10
        };
        this.panelOptions = {
            propertyPickerOptions: {
                gridSorting: {
                    direction: "original"
                }
            }
        };
        this.sorting = {
            sortableColumns: [
                { id: "domainName", label: "Domain Name" },
                { id: "type", label: "Type" },
                { id: "account", label: "Account Name" },
                { id: "provider", label: "Cloud Provider" }
            ],
            sortBy: {
                id: "domainName",
                label: "Domain Name"
            },
            direction: "asc"
        };
        return this.$timeout(function () {
            _this.propertiesToRequest = _this.updatePropertiesToRequest(_this.propertiesToRequest, _this.rowDisplaySettings);
            _this.searchTerm = _this.$location.search().q;
            _this.initPagination();
        });
    };
    CloudDnsZonesController.prototype.rejectPendingRefresh = function () {
        if (this.searchToken) {
            this.searchToken.reject();
            this.searchToken = null;
        }
    };
    CloudDnsZonesController.prototype.refreshData = function (filters, pagination, sorting, searching, filterModels) {
        var _this = this;
        var token = this.$q.defer();
        this.getFilters(searching)
            .then(function (filterProperties) {
            if (isRejected(token.promise)) {
                _this.$log.warn("Search has been cancelled.");
                return;
            }
            _this.filterProperties = filterProperties;
        });
        if (!this.pagination) {
            return;
        }
        this.getDataForFilter(this.searchUrl, {
            "q": this.searchTerm || searching,
            "skip": ((this.pagination.page - 1) * this.pagination.pageSize),
            "top": this.pagination.pageSize,
            "orderBy": this.formatOrderBy(sorting),
            "filterBy": this.getSearchFilters(this.filterValues),
            "propertyToSelect": this.propertiesToRequest
        })
            .then(function (response) {
            _this.usedSearchTerm = _this.searchTerm;
            _this.firstLoadHappened = true;
            if (!response || !response.Items) {
                _this.$log.warn("Received empty search result.");
                _this.searchToken = null;
                token.resolve();
                return;
            }
            var rawData = [];
            angular.forEach(response.Items, function (zone, key) {
                var z = new cloudDnsZone_1.default(zone);
                rawData.push({
                    domainName: z.domainName,
                    account: z.account,
                    type: z.type,
                    id: z.id,
                    provider: z.provider,
                    dnsZoneId: z.dnsZoneId,
                    $templateUrl: "item-template"
                });
            });
            _this.itemsSource = angular.copy(rawData);
            _this.pagination.total = response.TotalCount;
            _this.searchToken = null;
            token.resolve();
        })
            .catch(function (e) {
            _this.$log.error("Search result:" + e);
        });
        return token.promise;
    };
    CloudDnsZonesController.prototype.updatePropertiesToRequest = function (propertiesToRequest, rowDisplaySettings) {
        var allSettingProperties = _.reduce(this.rowDisplaySettings, function (list, settings, key) {
            return settings
                ? _.concat(list, settings.customProps, settings.systemProps)
                : list;
        }, []);
        return _.uniq(_.concat(propertiesToRequest, allSettingProperties));
    };
    CloudDnsZonesController.prototype.instant = function (value) {
        return this.$q(function (resolve) { return resolve(value); });
    };
    Object.defineProperty(CloudDnsZonesController.prototype, "useApi", {
        get: function () {
            return this.swApi.api(false);
        },
        enumerable: true,
        configurable: true
    });
    CloudDnsZonesController.prototype.toggleSidebar = function () {
        this.sideBarSettings.display = !this.sideBarSettings.display;
    };
    ;
    CloudDnsZonesController.prototype.getDnsZone = function (id) {
        var _this = this;
        var url = this.searchUrl + ("/" + id);
        this.swApi.api(false).one(url).get()
            .then(function (response) {
            if (!response) {
                _this.$log.warn("Received empty result.");
                return;
            }
            _this.selectedZone = new cloudDnsZone_1.default(response);
        })
            .catch(function (e) {
            _this.$log.error("Error :" + e);
        });
        this.toggleSidebar();
    };
    ;
    return CloudDnsZonesController;
}());
exports.default = CloudDnsZonesController;


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var CloudDnsZone = /** @class */ (function () {
    function CloudDnsZone(data) {
        this.domainName = data.DomainName;
        this.type = data.IsPrivate ? "Private" : "Public";
        this.id = data.Id;
        this.account = data.Account;
        this.provider = data.Provider === 1 ? "Azure" : "AWS";
        this.comment = data.Comment;
        this.nameServers = data.NameServers;
        this.tags = data.Tags;
        this.dnsZoneId = data.DnsZoneId;
        this.recordSetCount = data.RecordSetCount;
    }
    return CloudDnsZone;
}());
exports.default = CloudDnsZone;


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'ipam/cloudDnsZones' url
    $stateProvider.state("cloudDnsZones", {
        parent: "basesubview",
        url: "/ipam/cloudDnsZones",
        i18Title: "_t(Cloud DNS Zones)",
        controller: "CloudDnsZonesController",
        controllerAs: "vm",
        template: __webpack_require__(3)
    });
}
;
exports.default = config;


/***/ }),
/* 57 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var cloudDnsRecords_controller_1 = __webpack_require__(59);
var cloudDnsRecords_config_1 = __webpack_require__(60);
__webpack_require__(4);
__webpack_require__(61);
__webpack_require__(62);
exports.default = function (module) {
    module.controller("CloudDnsRecordsController", cloudDnsRecords_controller_1.default);
    module.config(cloudDnsRecords_config_1.default);
};


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
function isRejected(promise) {
    return promise.$$state.status === 2;
}
var CloudDnsRecordsController = /** @class */ (function () {
    /** @ngInject */
    CloudDnsRecordsController.$inject = ["$scope", "$log", "swApi", "$timeout", "$templateCache", "$q", "$location", "swSwisDataMappingService", "swSearchHistoryService", "$translate", "$httpParamSerializerJQLike", "getTextService"];
    function CloudDnsRecordsController($scope, $log, swApi, $timeout, $templateCache, $q, $location, swSwisDataMappingService, swSearchHistoryService, $translate, $httpParamSerializerJQLike, getTextService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.swApi = swApi;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.$q = $q;
        this.$location = $location;
        this.swSwisDataMappingService = swSwisDataMappingService;
        this.swSearchHistoryService = swSearchHistoryService;
        this.$translate = $translate;
        this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
        this.getTextService = getTextService;
        this.filterValues = {};
        this.itemsSource = [];
        this.rawData = [];
        this.firstLoadHappened = false;
        this.usedSearchTerm = "";
        this.getFilterProperties = function () {
            return _this.swApi.api(false).one(_this.filtersUrl).get()
                .then(function (result) {
                return result;
            });
        };
        this.defaultSystemProps = [];
        this.defaultCustomProps = [];
        this.searchRemoteControl = {};
        this.filteredListRemoteControl = {};
        this.controllerUrl = "ipam/cloudDnsRecords";
        this.getUrl = "ipam/cloudDnsRecords/get";
        this.filtersUrl = "filters";
        this.propertiesToRequest = [
            "status",
            "childStatus",
            "caption",
            "displayName",
            "detailsUrl",
            "node.displayName",
            "node.detailsUrl",
            "serverName",
            "sysName",
            "vendor",
            "vendorIcon",
            "ipAddress",
            "instanceType",
            "machineType"
        ];
        this.onHistoryItemClicked = function (search) {
            _this.searchRemoteControl.search(search);
        };
        this.onSearch = function (item) {
            if (!item) {
                _this.usedSearchTerm = "";
                _this.pagination.total = 1;
                return _this.instant();
            }
            _this.searchTerm = item;
            _this.swSearchHistoryService.addToHistory(item);
            _this.usedFilterValues = null;
            return _this.filteredListRemoteControl.refreshListData();
        };
        this.cancel = function () {
            _this.rejectPendingRefresh();
            _this.$log.info("Search cancelled by the user.");
        };
        this.clear = function () { return _this.onSearch(""); };
        this.formatOrderBy = function (sorting) {
            return "" + (sorting.direction.toLowerCase() === "asc" ? "+" : "-") + sorting.sortBy.id;
        };
        this.showEmptySearch = function () {
            return (_this.pagination.total !== 0
                && _this.usedSearchTerm === ""
                && _this.firstLoadHappened);
        };
        this.showNoResult = function () {
            return (_this.pagination.total === 0
                && _this.usedSearchTerm !== ""
                && _this.firstLoadHappened
                && !_this.hasAnyFilter(_this.usedFilterValues));
        };
        this.showFilteredList = function () {
            return (!_this.showEmptySearch()
                && !_this.showNoResult()
                && _this.firstLoadHappened);
        };
        this.getSearchFilters = function (filterValues) {
            return _.map(_.filter(Object.keys(filterValues), function (filterId) { return filterValues[filterId].length > 0; }), function (filterId) { return filterId + ":" + filterValues[filterId].join("|"); });
        };
        this.hasAnyFilter = function (usedFilters) {
            return (usedFilters
                && Object.keys(usedFilters).some(function (key) { return usedFilters[key].length > 0; }));
        };
        this.getFilters = function () {
            return _this.getDataForFilter(_this.controllerUrl + "/" + _this.filtersUrl + ("/" + _this.dnsZoneId), {
                "filters": _.map(_this.filterProperties, function (filter) { return filter.refId; }),
                "q": _this.searchTerm || "",
                "filterBy": _this.getSearchFilters(_this.filterValues)
            });
        };
        this.getDataForFilter = function (url, data) {
            return _this.useApi.one(url + "?" + _this.$httpParamSerializerJQLike(data)).get();
        };
    }
    CloudDnsRecordsController.prototype.initPagination = function () {
        this.pagination = this.pagination || {};
        this.pagination.pageSize = this.options.pageSize;
        this.pagination.page = this.pagination.page || 1;
        this.pagination.total = 5;
    };
    CloudDnsRecordsController.prototype.$onInit = function () {
        var _this = this;
        var itemTemplate = __webpack_require__(5);
        this.searchText = this.searchText || this.$translate.instant("orion_search_searchText");
        this._t = this.getTextService;
        this.$templateCache.put("search-item-template", itemTemplate);
        this.sideBarSettings = this.sideBarSettings || {};
        this.dnsZoneId = this.$location.search().zoneId;
        this.options = {
            selectionProperty: "DisplayName",
            showAddRemoveFilterProperty: false,
            hideSearch: false,
            rowPadding: "none",
            showEmptyFilterProperties: true,
            categorizedFilterPanel: false,
            showMorePropertyValuesThreshold: 10,
            allowSelectAllPages: false,
            pageSize: 10
        };
        this.panelOptions = {
            propertyPickerOptions: {
                gridSorting: {
                    direction: "original"
                }
            }
        };
        this.sorting = {
            sortableColumns: [
                { id: "recordName", label: "Record Name" },
                { id: "type", label: "Type" },
                { id: "value", label: "Value" },
                { id: "Ttl", label: "TTL" }
            ],
            sortBy: {
                id: "recordName",
                label: "Record Name"
            },
            direction: "asc"
        };
        return this.$timeout(function () {
            _this.propertiesToRequest = _this.updatePropertiesToRequest(_this.propertiesToRequest, _this.rowDisplaySettings);
            _this.searchTerm = _this.$location.search().q;
            _this.initPagination();
        });
    };
    CloudDnsRecordsController.prototype.rejectPendingRefresh = function () {
        if (this.searchToken) {
            this.searchToken.reject();
            this.searchToken = null;
        }
    };
    CloudDnsRecordsController.prototype.refreshData = function (filters, pagination, sorting, searching, filterModels) {
        var _this = this;
        this.usedFilterValues = filters;
        this.rejectPendingRefresh();
        var deferred = this.$q.defer();
        this.searchToken = deferred;
        this.getFilters()
            .then(function (filterProperties) {
            if (isRejected(deferred.promise)) {
                _this.$log.warn("Search has been cancelled.");
                return;
            }
            _this.filterProperties = filterProperties;
        });
        if (!this.pagination) {
            return;
        }
        return this.getDataForFilter(this.getUrl, {
            "dnsZoneId": this.dnsZoneId,
            "q": this.searchTerm || searching,
            "skip": ((this.pagination.page - 1) * this.pagination.pageSize),
            "top": this.pagination.pageSize,
            "orderBy": this.formatOrderBy(sorting),
            "filterBy": this.getSearchFilters(this.filterValues),
            "propertyToSelect": this.propertiesToRequest
        })
            .then(function (response) {
            if (!response) {
                _this.$log.warn("Received empty search result.");
                _this.searchToken = null;
                return;
            }
            var rawData = [];
            angular.forEach(response.Items, function (rec, key) {
                rawData.push({
                    recordName: rec.Id,
                    name: rec.Name,
                    type: rec.Type,
                    value: rec.Value,
                    ttl: rec.Ttl,
                    id: rec.Id,
                    $templateUrl: "item-template"
                });
            });
            _this.itemsSource = angular.copy(rawData);
            _this.pageTitle = String.format(_this._t("Cloud DNS Records - {0}"), response.DnsZoneName);
            _this.pagination.total = response.TotalCount;
        });
    };
    CloudDnsRecordsController.prototype.getDnsRecord = function (id) {
        var _this = this;
        var url = this.getUrl + ("/" + id);
        this.swApi.api(false).one(url).get()
            .then(function (response) {
            if (!response) {
                _this.$log.warn("Received empty result.");
                return;
            }
            _this.selectedRecord = response;
        })
            .catch(function (e) {
            _this.$log.error("Error :" + e);
        });
        this.toggleSidebar();
    };
    ;
    CloudDnsRecordsController.prototype.updatePropertiesToRequest = function (propertiesToRequest, rowDisplaySettings) {
        var allSettingProperties = _.reduce(this.rowDisplaySettings, function (list, settings, key) {
            return settings
                ? _.concat(list, settings.customProps, settings.systemProps)
                : list;
        }, []);
        return _.uniq(_.concat(propertiesToRequest, allSettingProperties));
    };
    CloudDnsRecordsController.prototype.instant = function (value) {
        return this.$q(function (resolve) { return resolve(value); });
    };
    Object.defineProperty(CloudDnsRecordsController.prototype, "useApi", {
        get: function () {
            return this.swApi.api(false);
        },
        enumerable: true,
        configurable: true
    });
    CloudDnsRecordsController.prototype.toggleSidebar = function () {
        this.sideBarSettings.display = !this.sideBarSettings.display;
    };
    ;
    return CloudDnsRecordsController;
}());
exports.default = CloudDnsRecordsController;


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'ipam/cloudDnsZones' url
    $stateProvider.state("cloudDnsRecords", {
        url: "/ipam/cloudDnsRecords?zoneId",
        i18Title: "_t(Cloud DNS Records)",
        controller: "CloudDnsRecordsController",
        controllerAs: "vm",
        template: __webpack_require__(5)
    });
}
;
exports.default = config;


/***/ }),
/* 61 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 62 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ipRequestSettings_controller_1 = __webpack_require__(30);
var ipRequestSettings_config_1 = __webpack_require__(68);
__webpack_require__(69);
exports.default = function (module) {
    module.controller("IpRequestSettingsController", ipRequestSettings_controller_1.default);
    module.config(ipRequestSettings_config_1.default);
};


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestSettingsFieldsData = /** @class */ (function () {
    function IpRequestSettingsFieldsData() {
    }
    return IpRequestSettingsFieldsData;
}());
exports.default = IpRequestSettingsFieldsData;


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NewListItemHandlerRegistry = /** @class */ (function () {
    function NewListItemHandlerRegistry() {
        this.registry = {};
    }
    NewListItemHandlerRegistry.prototype.register = function (handler) {
        this.registry[handler.templateId] = handler;
    };
    NewListItemHandlerRegistry.prototype.get = function (key) {
        if (!(key in this.registry)) {
            throw new Error("Invalid type: " + key);
        }
        return this.registry[key];
    };
    return NewListItemHandlerRegistry;
}());
exports.NewListItemHandlerRegistry = NewListItemHandlerRegistry;


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var TextBoxNewListItemHandler = /** @class */ (function () {
    function TextBoxNewListItemHandler(controlHeader, addNewItemButtonText) {
        this.invalidMessage = "";
        this.templateId = "text-box-template";
        this.isAddNewItemButtonVisible = true;
        this.controlHeader = controlHeader;
        this.addNewItemButtonText = addNewItemButtonText;
    }
    TextBoxNewListItemHandler.prototype.init = function () {
        this.fieldPropertyValue = "";
    };
    TextBoxNewListItemHandler.prototype.isValid = function (data) {
        var isExistOnList = false;
        var value = this.fieldPropertyValue.trim().replace(/[\s]/g, "");
        data.forEach(function (val) {
            if (val.fieldName === value) {
                isExistOnList = true;
            }
        });
        if ((isExistOnList || (this.fieldPropertyValue == null || this.fieldPropertyValue.length === 0)) && !this.isAddNewItemButtonVisible) {
            if (isExistOnList) {
                this.invalidMessage = this.controlHeader + " field with name: " + this.fieldPropertyValue + " is already defined";
            }
            return false;
        }
        else {
            return true;
        }
    };
    return TextBoxNewListItemHandler;
}());
exports.TextBoxNewListItemHandler = TextBoxNewListItemHandler;


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DropDownNewListItemHandler = /** @class */ (function () {
    function DropDownNewListItemHandler(controlHeader, addNewItemButtonText) {
        this.placeholder = "select item";
        this.isRequired = true;
        this.templateId = "drop-down-template";
        this.isAddNewItemButtonVisible = true;
        this.controlHeader = controlHeader;
        this.addNewItemButtonText = addNewItemButtonText;
    }
    DropDownNewListItemHandler.prototype.init = function () {
        this.fieldPropertyValue = null;
    };
    DropDownNewListItemHandler.prototype.isValid = function () {
        if ((this.fieldPropertyValue == null || this.fieldPropertyValue.length === 0) && !this.isAddNewItemButtonVisible) {
            return false;
        }
        else {
            return true;
        }
    };
    return DropDownNewListItemHandler;
}());
exports.DropDownNewListItemHandler = DropDownNewListItemHandler;


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
var ipRequestSettings_controller_1 = __webpack_require__(30);
/** @ngInject */
function config($stateProvider) {
    $stateProvider.state("ipRequestSettings", {
        url: "/ipam/ipRequestSettings",
        i18Title: "_t(IP Request Settings)",
        controller: ipRequestSettings_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(31)
    });
}
;
exports.default = config;


/***/ }),
/* 69 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(71);
var index_2 = __webpack_require__(80);
var index_3 = __webpack_require__(92);
var index_4 = __webpack_require__(96);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
};


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var edit_ipam_settings_component_1 = __webpack_require__(72);
var edit_ipam_settings_controller_1 = __webpack_require__(73);
var edit_ipam_settings_config_1 = __webpack_require__(78);
__webpack_require__(79);
exports.default = function (module) {
    module.component("editIpamSettings", edit_ipam_settings_component_1.default);
    module.controller("EditIpamSettingsController", edit_ipam_settings_controller_1.default);
    module.config(edit_ipam_settings_config_1.default);
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var EditIpamSettingsComponent = /** @class */ (function () {
    function EditIpamSettingsComponent() {
        this.template = __webpack_require__(6);
        this.controllerAs = "wizard";
        this.controller = "EditIpamSettingsController";
        this.restrict = "E";
    }
    return EditIpamSettingsComponent;
}());
exports.default = EditIpamSettingsComponent;


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(74);
var EditIpamSettingsController = /** @class */ (function () {
    /** @ngInject */
    EditIpamSettingsController.$inject = ["accountsService", "toastService", "swDemoService", "$routeParams", "$stateParams", "$q", "$window", "$timeout", "getTextService"];
    function EditIpamSettingsController(accountsService, toastService, swDemoService, $routeParams, $stateParams, $q, $window, $timeout, getTextService) {
        this.accountsService = accountsService;
        this.toastService = toastService;
        this.swDemoService = swDemoService;
        this.$routeParams = $routeParams;
        this.$stateParams = $stateParams;
        this.$q = $q;
        this.$window = $window;
        this.$timeout = $timeout;
        this.getTextService = getTextService;
        this.isUpdating = false;
        this.isInvalid = false;
    }
    EditIpamSettingsController.prototype.$onInit = function () {
        this.initializeData();
    };
    EditIpamSettingsController.prototype.initializeData = function () {
        var _this = this;
        this.init();
        this.accountsService.getIpamSettingsByAccountId(this.data.accountId).then(function (data) {
            if (data) {
                var isDisableDnsScanning = data.DnsScanEnabledCount >= 1;
                var isDisableVirtualNetwork = data.NetworkScanEnabledCount >= 1;
                var dnsScanEnabled = !data.DnsScanEnabled;
                var networkScanEnabled = !data.NetworkScanEnabled;
                _this.data.data["ipam-settings"].isDisableDnsScanning = dnsScanEnabled && isDisableDnsScanning;
                _this.data.data["ipam-settings"].isDisableVirtualNetwork = networkScanEnabled && isDisableVirtualNetwork;
                _this.data.data["ipam-settings"].isDnsScanning = data.DnsScanEnabled;
                _this.data.data["ipam-settings"].isVirtualNetwork = data.NetworkScanEnabled;
                _this.data.data["ipam-settings"].isDnsScanningOrigin = data.DnsScanEnabled;
                _this.data.data["ipam-settings"].isVirtualNetworkOrigin = data.NetworkScanEnabled;
                _this.data.data["ipam-settings"].dnsScanInterval = data.DnsScanInterval > 0 ? data.DnsScanInterval : 4;
                _this.data.data["ipam-settings"].scanInterval =
                    data.NetworkScanInterval > 0 ? data.NetworkScanInterval : 4;
                _this.data.data["ipam-settings"].cloudAccountSettingId = data.CloudAccountSettingId;
                if (data.DnsScanEnabled) {
                    _this.data.data["ipam-settings"].selectedDNSInterval =
                        _this.data.data["ipam-settings"].timeOptions.find(function (i) { return i.id === data.DnsTimeUnit; });
                }
                if (data.NetworkScanEnabled) {
                    _this.data.data["ipam-settings"].selectedVirtualNetworkInterval =
                        _this.data.data["ipam-settings"].timeOptions.find(function (i) { return i.id === data.NetworkTimeUnit; });
                }
            }
        });
    };
    EditIpamSettingsController.prototype.init = function () {
        this.data = new index_1.InstanceContext();
        this.data.providerType = this.getProviderTypeFromUrl();
        this.data.accountId = this.getAccountIdFromUrl();
        this.data.data["cloud-settings-selected-provider"] = { id: 0 };
        this.data.data["polling-options-accountId"] = 0;
        this.data.data["ipam-settings"] = {
            isDisableDnsScanning: false,
            isDisableVirtualNetwork: false,
            isVirtualNetwork: true,
            isDnsScanning: true,
            dnsScanInterval: 4,
            scanInterval: 4,
            cloudAccountSettingId: 0,
            isDnsScanningOrigin: false,
            isVirtualNetworkOrigin: false
        };
        this.data.data["ipam-settings"].timeOptions = [
            { id: 2, name: "Minutes" },
            { id: 3, name: "Hours" },
            { id: 4, name: "Days" }
        ];
        this.data.data["ipam-settings"].selectedDNSInterval = this.data.data["ipam-settings"].timeOptions[1];
        this.data.data["ipam-settings"].selectedVirtualNetworkInterval = this.data.data["ipam-settings"].timeOptions[1];
        if (!this.data.providerType || !this.data.accountId) {
            this.$onInit();
        }
    };
    EditIpamSettingsController.prototype.checkFormOnValidation = function () {
        var _this = this;
        this.isInvalid = false;
        var gridPlugin = this.getGridPlugin();
        this.delay(function () {
            if (!gridPlugin.isValid(_this.data)) {
                _this.data.data["error"] = "You have entered an invalid time value(s). Please try again.";
                _this.isInvalid = true;
            }
        }, 50);
    };
    EditIpamSettingsController.prototype.save = function () {
        var _this = this;
        this.isUpdating = true;
        this.savingMessage = "Saving Ipam settings...";
        this.data.data["error"] = undefined;
        var gridPlugin = this.getGridPlugin();
        if (!gridPlugin.isValid(this.data)) {
            this.data.data["error"] = "You have entered an invalid time value(s). Please try again.";
            this.isUpdating = false;
            this.savingMessage = "";
        }
        else {
            gridPlugin.onUpdate(this.data)
                .then(function () {
                if (_this.data.data["error"]) {
                    _this.toastService.error(_this.getTextService(_this.data.data["error"].responseText), _this.getTextService("Error while updating instances"));
                    _this.data.data["error"] = undefined;
                }
                else {
                    _this.redirectToManageAccounts();
                }
                _this.delay(function () {
                    _this.isUpdating = false;
                    _this.savingMessage = "";
                }, 100);
            }, function (reason) {
                _this.toastService.error(_this.getTextService(reason), _this.getTextService("Error while updating instances"));
                _this.delay(function () {
                    _this.isUpdating = false;
                    _this.savingMessage = "";
                }, 100);
            });
        }
    };
    EditIpamSettingsController.prototype.getGridPlugin = function () {
        return this.$window["cloudAccountIpamSettings"];
    };
    EditIpamSettingsController.prototype.onTemplateIncluded = function () {
        this.loadAccount(this.data.providerType, this.data.accountId);
    };
    EditIpamSettingsController.prototype.getAccountIdFromUrl = function () {
        return parseInt(this.$stateParams.accountId || "0", 10);
    };
    EditIpamSettingsController.prototype.getProviderTypeFromUrl = function () {
        var providerType = this.$stateParams.providerType;
        switch (providerType) {
            case "1":
                return index_1.ProviderType.Aws;
            case "2":
                return index_1.ProviderType.Azure;
            default:
                return index_1.ProviderType.Unknown;
        }
    };
    EditIpamSettingsController.prototype.redirectToManageAccounts = function () {
        this.$window.location.href = "/ui/clm/accounts";
    };
    EditIpamSettingsController.prototype.delay = function (action, delay) {
        this.$timeout(action, delay);
    };
    EditIpamSettingsController.prototype.loadAccount = function (providerType, accountId) {
        var _this = this;
        var loadPromise;
        var that = this;
        this.data.data["cloud-settings-selected-provider"].id = providerType;
        this.data.data["polling-options-accountId"] = accountId;
        switch (providerType) {
            case index_1.ProviderType.Aws:
                loadPromise = this.accountsService.getAwsAccount(accountId);
                break;
            case index_1.ProviderType.Azure:
                loadPromise = this.accountsService.getAzureAccount(accountId);
                break;
            default:
                loadPromise = this.$q.reject(this.getTextService("Not supported account type"));
                break;
        }
        loadPromise.then(function (account) {
            _this.account = account;
        });
    };
    return EditIpamSettingsController;
}());
exports.default = EditIpamSettingsController;


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(75));
__export(__webpack_require__(76));
__export(__webpack_require__(7));
__export(__webpack_require__(77));


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ProviderType;
(function (ProviderType) {
    ProviderType[ProviderType["Aws"] = 1] = "Aws";
    ProviderType[ProviderType["Azure"] = 2] = "Azure";
    ProviderType[ProviderType["Unknown"] = -1] = "Unknown";
})(ProviderType = exports.ProviderType || (exports.ProviderType = {}));


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var InstanceContext = /** @class */ (function () {
    function InstanceContext() {
        this.data = {};
    }
    return InstanceContext;
}());
exports.InstanceContext = InstanceContext;


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilterCollection = /** @class */ (function () {
    function FilterCollection($q, filters) {
        var _this = this;
        this.$q = $q;
        this.filters = filters;
        this.filterValues = {};
        this.filterProperties = {};
        this.filterPropertiesDeferred = this.$q.defer();
        this.filterGroups = ["object"];
        this.getFilterProperties = function () {
            return _this.filterPropertiesDeferred.promise;
        };
        this.filtersDict = {};
        this.build = function (subnets) {
            for (var _i = 0, _a = _this.filters; _i < _a.length; _i++) {
                var filter = _a[_i];
                var filterValues = _this.getFilterValues(subnets, filter);
                _this.filterProperties[filter.refId].values = filterValues;
            }
            _this.filterPropertiesDeferred.resolve(_this.filterProperties);
            _this.ExpandFilterProperties(_this.filterPropertyStates);
        };
        this.filter = function (subnets) {
            var result = subnets;
            for (var key in _this.filterValues) {
                if (_this.filterValues.hasOwnProperty(key)) {
                    var selectedValues = _this.filterValues[key];
                    result = _this.filterImpl(result, selectedValues, key);
                }
            }
            return result;
        };
        this.filterImpl = function (subnets, selectedValues, key) {
            if (selectedValues.length === 0) {
                return subnets;
            }
            var filter = _this.filtersDict[key];
            var _loop_1 = function (value) {
                subnets = _.filter(subnets, function (subnet) { return filter.propertySelector(subnet) === value; });
            };
            for (var _i = 0, selectedValues_1 = selectedValues; _i < selectedValues_1.length; _i++) {
                var value = selectedValues_1[_i];
                _loop_1(value);
            }
            return subnets;
        };
        this.getFilterValues = function (subnets, filter) {
            var filters = [];
            var grouped = _.groupBy(subnets, function (s) { return filter.propertySelector(s); });
            for (var key in grouped) {
                if (grouped.hasOwnProperty(key)) {
                    var filterValue = {
                        id: key,
                        title: key,
                        count: grouped[key].length
                    };
                    filters.push(filterValue);
                }
            }
            return filters;
        };
        _.forEach(filters, function (filter) {
            var property = {
                refId: filter.refId,
                title: filter.title,
                values: []
            };
            _this.filterProperties[filter.refId] = property;
            _this.filterValues[filter.refId] = [];
            _this.filtersDict[filter.refId] = filter;
        });
    }
    FilterCollection.prototype.ExpandFilterProperties = function (states) {
        _.forEach(states, function (state) {
            state.isOpen = true;
        });
    };
    ;
    return FilterCollection;
}());
exports.default = FilterCollection;


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state("editIpamSettings", {
        url: "/ipam/editIpamSettings?providerType&accountId",
        i18Title: "_t(Cloud IPAM Settings)",
        controller: "EditIpamSettingsController",
        controllerAs: "wizard",
        template: __webpack_require__(6)
    });
}
;
exports.default = config;


/***/ }),
/* 79 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ipRequest_component_1 = __webpack_require__(81);
var ipRequest_controller_1 = __webpack_require__(82);
var ipRequest_config_1 = __webpack_require__(90);
__webpack_require__(91);
exports.default = function (module) {
    module.component("ipRequest", ipRequest_component_1.default);
    module.controller("IPRequestController", ipRequest_controller_1.default);
    module.config(ipRequest_config_1.default);
};


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IPRequestComponent = /** @class */ (function () {
    function IPRequestComponent() {
        this.template = __webpack_require__(8);
        this.controllerAs = "vm";
        this.controller = "IPRequestController";
        this.restrict = "E";
    }
    return IPRequestComponent;
}());
exports.default = IPRequestComponent;


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ipRequest_1 = __webpack_require__(7);
var stepsProvider_1 = __webpack_require__(83);
var dataStepsProvider_1 = __webpack_require__(89);
var IPRequestController = /** @class */ (function () {
    /** @ngInject */
    IPRequestController.$inject = ["profileService", "$q", "swApi", "$log", "$stateParams", "$translate", "$scope", "$timeout", "requesterCustomFieldsService", "ipRequestService", "getTextService"];
    function IPRequestController(profileService, $q, swApi, $log, $stateParams, $translate, $scope, $timeout, requesterCustomFieldsService, ipRequestService, getTextService) {
        var _this = this;
        this.profileService = profileService;
        this.$q = $q;
        this.swApi = swApi;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.$translate = $translate;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.requesterCustomFieldsService = requesterCustomFieldsService;
        this.ipRequestService = ipRequestService;
        this.getTextService = getTextService;
        this.cancelActionUrl = "/";
        // delay in miliseconds while preceding to next step
        this.delay = 500;
        this.items = [];
        this.additionalButtons = [];
        this.doneButton = {
            name: "done",
            text: this.getTextService("Done"),
            isDisabled: false,
            isHidden: false,
            action: function (dialog) {
                _this.onCancel();
                return true;
            }
        };
        this.knownSubnetHandle = function (known) {
            var subnetSelectorIdx = _this.steps.indexOf(_this.subnetSelectorStep);
            var availableAddressesIdx = _this.steps.indexOf(_this.availableAddressesStep);
            if (known) {
                if (subnetSelectorIdx === -1) {
                    _this.steps.splice(1, 0, _this.subnetSelectorStep);
                }
                if (availableAddressesIdx === -1) {
                    _this.steps.splice(2, 0, _this.availableAddressesStep);
                }
            }
            else {
                if (availableAddressesIdx !== -1) {
                    _this.steps.splice(availableAddressesIdx, 1);
                }
                if (subnetSelectorIdx !== -1) {
                    _this.steps.splice(subnetSelectorIdx, 1);
                }
                if (_this._wizardStep) {
                    _this._wizardStep.complete = false;
                }
            }
        };
    }
    Object.defineProperty(IPRequestController.prototype, "currentWizardStep", {
        get: function () {
            return this._wizardStep;
        },
        set: function (step) {
            this._wizardStep = step;
            this.currentStep = step ? this.stepsProvider.getStepByLabel(step.label) : null;
        },
        enumerable: true,
        configurable: true
    });
    IPRequestController.prototype.$onInit = function () {
        this.dataStepsProvider = new dataStepsProvider_1.default();
        this.dataStepsProvider.ipRequest_set(new ipRequest_1.default());
        this.dataStepsProvider.knownSubnetHandler_set(this.knownSubnetHandle);
        this.stepsProvider = new stepsProvider_1.default(this, this.$q, this.swApi, this.$log, this.$translate, this.$scope, this.requesterCustomFieldsService, this.ipRequestService, this.getTextService);
        this.steps = this.stepsProvider.getSteps();
        this.subnetSelectorStep = this.stepsProvider.getStepByLabel("subnetSelector");
        this.availableAddressesStep = this.stepsProvider.getStepByLabel("addresses");
        this.getProfileInfo();
    };
    IPRequestController.prototype.getProfileInfo = function () {
        var _this = this;
        this.profileService.getProfile().then(function (data) {
            _this.IsIpamIpRequestHomePage = data.IsIpamIpRequestHomePage;
        });
    };
    IPRequestController.prototype.canGoNext = function () {
        return this.currentStep && this.currentStep.canGoNext();
    };
    IPRequestController.prototype.onNextStep = function (from, to, cancellation) {
        var _this = this;
        var deferred = this.$q.defer();
        var prevStep = this.stepsProvider.getStepByLabel(from.label);
        var nextStep = this.stepsProvider.getStepByLabel(to.label);
        // We delay the execution to make sure scope changes propagate in Internet Explorer
        this.executeDelayed(function () {
            prevStep.onExit().then(function (onExitResult) {
                if (onExitResult.result) {
                    _this.dataStepsProvider.dataFromExitStep(prevStep.label, onExitResult.data);
                    deferred.resolve(true);
                }
                else {
                    deferred.resolve(false);
                }
            })
                .catch(function (e) {
                _this.$log.error("Error :" + e);
                deferred.reject(false);
            });
        }, this.delay);
        return deferred.promise;
    };
    IPRequestController.prototype.onEnterStep = function (step) {
        var _this = this;
        var s = this.stepsProvider.getStepByLabel(step.label);
        if (s) {
            s.onEnter(this.dataStepsProvider.dataForEnterStep(s.label))
                .then(function (data) {
                _this.additionalButtons = s.additionalButtons;
                if (step.label === "confirmation" && _this.additionalButtons) {
                    _this.additionalButtons.push(_this.doneButton);
                }
            });
        }
    };
    IPRequestController.prototype.canGoBack = function () {
        return this.currentStep && this.currentStep.label !== "confirmation";
    };
    IPRequestController.prototype.onFinish = function () {
        this.dataStepsProvider.keepPersonalInfo();
        window.location.replace("/ui/ipam/ipRequest");
    };
    IPRequestController.prototype.onCancel = function () {
        this.IsIpamIpRequestHomePage ?
            this.$scope.$parent.shell.logout() :
            window.location.replace(this.cancelActionUrl);
    };
    // Execute delay action after specified miliseconds
    IPRequestController.prototype.executeDelayed = function (action, delay) {
        if (delay === void 0) { delay = 500; }
        if (delay <= 0) {
            action();
        }
        else {
            this.$timeout(action, delay);
        }
    };
    return IPRequestController;
}());
exports.default = IPRequestController;


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var requestDetailsStep_1 = __webpack_require__(84);
var confirmationStep_1 = __webpack_require__(86);
var subnetSelectorStep_1 = __webpack_require__(87);
var availableAddressesStep_1 = __webpack_require__(88);
var StepsProvider = /** @class */ (function () {
    /** @ngInject */
    StepsProvider.$inject = ["vm", "$q", "swApi", "$log", "$translate", "$scope", "requesterCustomFieldsService", "ipRequestService", "getTextService"];
    function StepsProvider(vm, $q, swApi, $log, $translate, $scope, requesterCustomFieldsService, ipRequestService, getTextService) {
        this.vm = vm;
        this.$q = $q;
        this.swApi = swApi;
        this.$log = $log;
        this.$translate = $translate;
        this.$scope = $scope;
        this.requesterCustomFieldsService = requesterCustomFieldsService;
        this.ipRequestService = ipRequestService;
        this.getTextService = getTextService;
        this.steps = [];
        this.steps.push(new requestDetailsStep_1.RequestDetailsStep(this.vm, this.$q, this.swApi, this.$log, this.$translate, this.$scope, this.requesterCustomFieldsService, this.ipRequestService, this.getTextService));
        this.steps.push(new subnetSelectorStep_1.SubnetSelectorStep(this.vm, this.$q, this.swApi, this.$log, this.$translate, this.$scope, this.requesterCustomFieldsService, this.ipRequestService, this.getTextService));
        this.steps.push(new availableAddressesStep_1.AvailableAddressesStep(this.vm, this.$q, this.swApi, this.$log, this.$translate, this.$scope, this.requesterCustomFieldsService, this.ipRequestService, this.getTextService));
        this.steps.push(new confirmationStep_1.ConfirmationStep(this.vm, this.$q, this.swApi, this.$log, this.$translate, this.$scope, this.requesterCustomFieldsService, this.ipRequestService, this.getTextService));
    }
    StepsProvider.prototype.getSteps = function () {
        return this.steps;
    };
    StepsProvider.prototype.getStepByLabel = function (label) {
        return _.find(this.steps, function (step) { return step.label === label; });
    };
    StepsProvider.prototype.getStepInfoByLabel = function (label) {
        var step = _.find(this.steps, function (s) { return s.label === label; });
        if (step) {
            return { step: step, index: this.steps.indexOf(step) };
        }
        return null;
    };
    return StepsProvider;
}());
exports.default = StepsProvider;


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ipRequest_1 = __webpack_require__(7);
var step_1 = __webpack_require__(2);
var RequestDetailsStep = /** @class */ (function (_super) {
    __extends(RequestDetailsStep, _super);
    function RequestDetailsStep() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = "request";
        _this.shortTitle = _this._t("Request Details");
        _this.nextText = _this._t("Request Address(es)");
        _this.templateUrl = "/ui/modules/ipam/templates/ipRequest/requestDetailsStep.html";
        _this.requestAddressCount = 1;
        return _this;
    }
    Object.defineProperty(RequestDetailsStep.prototype, "knownSubnet", {
        get: function () {
            return this._knownSubnet;
        },
        set: function (value) {
            this._knownSubnet = value;
            this.nextText = this._knownSubnet ? this._t("Next") : this._t("Request Address(es)");
            if (this.knownSubnetHandler) {
                this.knownSubnetHandler(this._knownSubnet);
            }
        },
        enumerable: true,
        configurable: true
    });
    RequestDetailsStep.prototype.onEnter = function (data) {
        var _this = this;
        this.knownSubnetHandler = this.knownSubnetHandler || data.knownSubnetHandler;
        this.knownSubnet = this.knownSubnet || false;
        this.firstName = this.firstName || data.firstName;
        this.lastName = this.lastName || data.lastName;
        this.phone = this.phone || data.phone;
        this.email = this.email || data.email;
        var deferred = this.$q.defer();
        this.swApi
            .api(false)
            .one("ipam/profile/hasAvailableSubnets")
            .get(data)
            .then(function (response) {
            _this.hasAvailableSubnets = response;
        })
            .catch(function (e) {
            _this.$log.error("Error :" + e);
            deferred.reject(false);
            return deferred.promise;
        });
        if (!this.customFields) {
            if (!data.customFields) {
                this.getCustomFields();
            }
            else {
                this.customFields = data.customFields;
            }
        }
        deferred.resolve(true);
        return deferred.promise;
    };
    RequestDetailsStep.prototype.onExit = function () {
        var _this = this;
        this.setExitResult(false);
        var deferred = this.$q.defer();
        _super.prototype.onExit.call(this).then(function (onExitResult) {
            if (onExitResult.result) {
                var data = new ipRequest_1.default();
                data.RequestAddressCount = _this.requestAddressCount;
                data.FirstName = _this.toHTML(_this.firstName);
                data.LastName = _this.toHTML(_this.lastName);
                data.Phone = _this.toHTML(_this.phone);
                data.Email = _this.email;
                data.Comment = _this.toHTML(_this.comment);
                data.RequesterCustomFields = _this.customFields;
                onExitResult.data = {
                    ipRequest: data,
                    knownSubnet: _this._knownSubnet
                };
                if (!_this._knownSubnet) {
                    _this.swApi
                        .api(false)
                        .all("ipam/ipRequests")
                        .customPOST(data)
                        .then(function (response) {
                        onExitResult.data.ipRequest.IpRequestId = response.IpRequestId;
                        deferred.resolve(_this.exitResult);
                    })
                        .catch(function (e) {
                        _this.$log.error("Error :" + e);
                        deferred.reject(_this.exitResult);
                        return deferred.promise;
                    });
                }
                else {
                    deferred.resolve(_this.exitResult);
                }
            }
            else {
                deferred.resolve(_this.exitResult);
            }
        });
        return deferred.promise;
    };
    RequestDetailsStep.prototype.toHTML = function (input) {
        if (input) {
            return new DOMParser().parseFromString(input, "text/html").documentElement.textContent;
        }
        return input;
    };
    RequestDetailsStep.prototype.getCustomFields = function () {
        var _this = this;
        this.requesterCustomFieldsService
            .getCustomFields()
            .then(function (result) {
            _this.customFields = result;
        });
    };
    return RequestDetailsStep;
}(step_1.default));
exports.RequestDetailsStep = RequestDetailsStep;


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var StepExitResult = /** @class */ (function () {
    function StepExitResult() {
    }
    return StepExitResult;
}());
exports.default = StepExitResult;


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var step_1 = __webpack_require__(2);
var ConfirmationStep = /** @class */ (function (_super) {
    __extends(ConfirmationStep, _super);
    function ConfirmationStep() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = "confirmation";
        _this.shortTitle = "Confirmation";
        _this.templateUrl = "/ui/modules/ipam/templates/ipRequest/confirmationStep.html";
        _this.notEnoughAddress = false;
        _this.itemsPaging = [];
        _this.pagination = {
            currentPage: 1,
            pageSize: 10,
            adjacent: 3
        };
        _this.additionalButtons = [];
        _this.printButton = {
            name: "print",
            text: _this._t("Print"),
            isDisabled: false,
            isHidden: false,
            action: function (dialog) {
                window.print();
                return true;
            }
        };
        return _this;
    }
    Object.defineProperty(ConfirmationStep.prototype, "mode", {
        get: function () {
            return this.ipReserved ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    ConfirmationStep.prototype.onPageChanged = function (page, pageSize, total) {
        this.itemsPaging = this.ipRequest.Addresses.slice(((page - 1) * pageSize), Math.min(total, (page * pageSize)));
    };
    ConfirmationStep.prototype.onEnter = function (data) {
        this.ipReserved = data.ipReserved;
        this.ipRequest = data.ipRequest;
        if (this.ipReserved) {
            this.itemsPaging = data.ipRequest;
            this.pagination.total = this.ipRequest.Addresses.length;
            this.onPageChanged(this.pagination.currentPage, this.pagination.pageSize, this.pagination.total);
            this.additionalButtons.push(this.printButton);
        }
        var cancelBtn = document.getElementsByClassName("xui-wizard__cancel-button")[0];
        cancelBtn.classList.add("ng-hide");
        return this.$q.resolve(true);
    };
    return ConfirmationStep;
}(step_1.default));
exports.ConfirmationStep = ConfirmationStep;


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var step_1 = __webpack_require__(2);
var SubnetSelectorStep = /** @class */ (function (_super) {
    __extends(SubnetSelectorStep, _super);
    function SubnetSelectorStep() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = "subnetSelector";
        _this.shortTitle = _this._t("Subnet Selector");
        _this.nextText = _this._t("Next");
        return _this;
    }
    SubnetSelectorStep.prototype.onEnter = function (data) {
        var deferred = this.$q.defer();
        this.templateUrl = "/ui/modules/ipam/templates/ipRequest/subnetSelectorStep.html";
        this.requestedIpsCount = data.RequestAddressCount;
        deferred.resolve(true);
        return deferred.promise;
    };
    SubnetSelectorStep.prototype.onExit = function () {
        var _this = this;
        this.setExitResult(false);
        var deferred = this.$q.defer();
        _super.prototype.onExit.call(this).then(function (onExitResult) {
            if (onExitResult.result) {
                var item = _this.selection.items[0];
                onExitResult.data = {
                    subnetId: item.GroupId,
                    subnet: item.Address + "/" + item.Cidr
                };
            }
            deferred.resolve(_this.exitResult);
        });
        return deferred.promise;
    };
    SubnetSelectorStep.prototype.canGoNext = function () {
        var canGoNext = false;
        if (!angular.isUndefined(this.selection) && !angular.isUndefined(this.selection.items)) {
            canGoNext = angular.isArray(this.selection.items) && this.selection.items.length > 0;
        }
        return canGoNext;
    };
    return SubnetSelectorStep;
}(step_1.default));
exports.SubnetSelectorStep = SubnetSelectorStep;


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var step_1 = __webpack_require__(2);
var AvailableAddressesStep = /** @class */ (function (_super) {
    __extends(AvailableAddressesStep, _super);
    function AvailableAddressesStep() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = "addresses";
        _this.shortTitle = _this._t("Available IP Address(es)");
        _this.nextText = _this._t("Reserve Address(es)");
        _this.templateUrl = "/ui/modules/ipam/templates/ipRequest/availableAddressesStep.html";
        _this.notEnoughAddress = false;
        _this.items = [];
        _this.itemsPaging = [];
        _this.hostnameRequired = false;
        _this.macAddressRequired = false;
        _this.isBusy = true;
        _this.pagination = {
            currentPage: 1,
            pageSize: 10,
            adjacent: 3
        };
        _this.gridOptions = {
            hideSearch: true
        };
        return _this;
    }
    AvailableAddressesStep.prototype.onEnter = function (data) {
        var _this = this;
        this.ipRequest = data.ipRequest;
        var deferred = this.$q.defer();
        if (this.ipRequest &&
            (this.subnetId !== this.ipRequest.SubnetId ||
                this.addressCount !== this.ipRequest.RequestAddressCount)) {
            this.subnetId = this.ipRequest.SubnetId;
            this.addressCount = this.ipRequest.RequestAddressCount;
            this.ipRequestService
                .getAvailableIpsWithoutCustomProperties(this.subnetId, this.addressCount)
                .then(function (response) {
                _this.notEnoughAddress = response.length < _this.addressCount;
                _this.nextText = _this.notEnoughAddress ? _this._t("Request Address(es)") : _this._t("Reserve Address(es)");
                if (!_this.notEnoughAddress) {
                    _this.items = response;
                    _this.isBusy = false;
                    _this.hostnameRequired = response[0].HostName.IsMandatory;
                    _this.macAddressRequired = response[0].MacAddress.IsMandatory;
                    _this.pagination.total = _this.items.length;
                    _this.onPageChanged(_this.pagination.currentPage, _this.pagination.pageSize, _this.pagination.total);
                }
                deferred.resolve(true);
            })
                .catch(function (e) {
                _this.$log.error("Error :" + e);
                deferred.reject(false);
            });
        }
        return deferred.promise;
    };
    AvailableAddressesStep.prototype.onPageChanged = function (page, pageSize, total) {
        this.itemsPaging = this.items.slice(((page - 1) * pageSize), Math.min(total, (page * pageSize)));
    };
    AvailableAddressesStep.prototype.onExit = function () {
        var _this = this;
        this.setExitResult(false);
        var deferred = this.$q.defer();
        _super.prototype.onExit.call(this).then(function (onExitResult) {
            if (onExitResult.result) {
                _this.ipRequest.Addresses = _this.items;
                _this.swApi
                    .api(false)
                    .all("ipam/ipRequests")
                    .customPOST(_this.ipRequest)
                    .then(function () {
                    _this.setExitResult(true, {
                        addressList: _this.items,
                        ipReserved: true
                    });
                    deferred.resolve(_this.exitResult);
                })
                    .catch(function (e) {
                    _this.$log.error("Error :" + e);
                    _this.setExitResult(false);
                    deferred.reject(_this.exitResult);
                    return deferred.promise;
                });
            }
            else {
                angular.forEach(_this.ipRequest_form.$error.required, function (form) {
                    angular.forEach(form.$error.required, function (item) {
                        item.$setDirty();
                    });
                });
                deferred.resolve(_this.exitResult);
            }
        });
        return deferred.promise;
    };
    return AvailableAddressesStep;
}(step_1.default));
exports.AvailableAddressesStep = AvailableAddressesStep;


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DataStepsProvider = /** @class */ (function () {
    function DataStepsProvider() {
    }
    DataStepsProvider.prototype.ipRequest_set = function (value) {
        this.ipRequest = value;
    };
    DataStepsProvider.prototype.knownSubnetHandler_set = function (value) {
        this.knownSubnetHandler = value;
    };
    DataStepsProvider.prototype.keepPersonalInfo = function () {
        sessionStorage.setItem("personalInfo", JSON.stringify({
            firstName: this.ipRequest.FirstName,
            lastName: this.ipRequest.LastName,
            phone: this.ipRequest.Phone,
            email: this.ipRequest.Email,
            customFields: this.ipRequest.RequesterCustomFields
        }));
    };
    DataStepsProvider.prototype.dataForEnterStep = function (label) {
        switch (label) {
            case "request":
                var personalInfo = JSON.parse(sessionStorage.getItem("personalInfo"));
                sessionStorage.removeItem("personalInfo");
                return {
                    firstName: personalInfo ? personalInfo.firstName : null,
                    lastName: personalInfo ? personalInfo.lastName : null,
                    phone: personalInfo ? personalInfo.phone : null,
                    email: personalInfo ? personalInfo.email : null,
                    knownSubnetHandler: this.knownSubnetHandler,
                    customFields: personalInfo ? personalInfo.customFields : null
                };
            case "subnetSelector":
                return {
                    RequestAddressCount: this.ipRequest.RequestAddressCount
                };
            case "addresses":
                return {
                    ipRequest: this.ipRequest
                };
            case "confirmation":
                return {
                    ipRequest: this.ipRequest,
                    ipReserved: this.ipReserved
                };
        }
    };
    DataStepsProvider.prototype.dataFromExitStep = function (label, data) {
        switch (label) {
            case "request":
                this.ipRequest = data.ipRequest;
                break;
            case "subnetSelector":
                this.ipRequest.SubnetId = data.subnetId;
                this.ipRequest.Subnet = data.subnet;
                break;
            case "addresses":
                this.ipRequest.Addresses = data.addressList;
                this.ipReserved = data.ipReserved;
                break;
        }
    };
    return DataStepsProvider;
}());
exports.default = DataStepsProvider;


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state("ipRequest", {
        url: "/ipam/ipRequest",
        i18Title: "_t(IP Address Request)",
        controller: "IPRequestController",
        controllerAs: "vm",
        template: __webpack_require__(8)
    });
}
;
exports.default = config;


/***/ }),
/* 91 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var subnetsList_controller_1 = __webpack_require__(93);
var subnetsList_component_1 = __webpack_require__(94);
__webpack_require__(4);
__webpack_require__(95);
exports.default = function (module) {
    module.component("subnetsList", subnetsList_component_1.default);
    module.controller("subnetsListController", subnetsList_controller_1.default);
};


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SubnetsListController = /** @class */ (function () {
    /** @ngInject */
    SubnetsListController.$inject = ["$scope", "$log", "swApi", "$timeout", "$templateCache", "$q", "$location", "$translate", "$httpParamSerializerJQLike", "swSearchHistoryService"];
    function SubnetsListController($scope, $log, swApi, $timeout, $templateCache, $q, $location, $translate, $httpParamSerializerJQLike, swSearchHistoryService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.swApi = swApi;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.$q = $q;
        this.$location = $location;
        this.$translate = $translate;
        this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
        this.swSearchHistoryService = swSearchHistoryService;
        this.itemsSource = [];
        this.filterValues = {};
        this.usedSearchTerm = "";
        this.firstLoadHappened = false;
        this.previousRefreshDataRequest = null;
        this.filtersLoaded = false;
        this.currentRefreshDataQuery = null;
        this.lastRequestTimestamp = 0;
        this.filtersUrl = "ipam/ipRequests/filters";
        this.getGroupsUrl = "ipam/ipRequests/groups";
        this.vendorFilterTemplateUrl = "search-filter-vendor-template";
        this.searchRemoteControl = {};
        this.filteredListRemoteControl = {};
        this.propertiesToRequest = [
            "status",
            "childStatus",
            "caption",
            "displayName",
            "detailsUrl",
            "node.displayName",
            "node.detailsUrl",
            "serverName",
            "sysName",
            "vendor",
            "vendorIcon",
            "ipAddress",
            "instanceType",
            "machineType"
        ];
        this.onSearch = function (item) {
            if (!item) {
                _this.usedSearchTerm = "";
                _this.pagination.total = 1;
                return _this.instant();
            }
            _this.searchTerm = item;
            _this.swSearchHistoryService.addToHistory(item);
            return _this.filteredListRemoteControl.refreshListData();
        };
        this.getDataForFilter = function (url, data) {
            return _this.useApi.one(url + "?" + _this.$httpParamSerializerJQLike(data)).get();
        };
        this.getFilters = function (search) {
            return _this.getDataForFilter(_this.filtersUrl, {
                "filters": _.map(_this.filterProperties, function (filter) { return filter.title; }),
                "q": _this.searchTerm || search,
                "filterBy": _this.getSearchFilters(_this.filterValues),
                "having": _this.requestedCount,
            });
        };
        this.getSearchFilters = function (values) {
            if (!values) {
                return [""];
            }
            return _.map(_.filter(Object.keys(values), function (filterId) { return values[filterId].length > 0; }), function (filterId) { return filterId + ":" + values[filterId].join("|"); });
        };
        this.hasAnyFilter = function (usedFilters) {
            return (usedFilters
                && Object.keys(usedFilters).some(function (key) { return usedFilters[key].length > 0; }));
        };
        this.cancel = function () {
            _this.rejectPendingRefresh();
            _this.$log.info("Search cancelled by the user.");
        };
        this.formatOrderBy = function (sorting) {
            if (!sorting) {
                return "";
            }
            return "" + (sorting.direction.toLowerCase() === "asc" ? "+" : "-") + sorting.sortBy.id;
        };
        this.makeRefreshDataRequestObject = function (searching, sorting) {
            return {
                requestedCount: _this.requestedCount,
                pageSize: _this.pagination.pageSize,
                page: _this.pagination.page,
                searchTerm: _this.searchTerm,
                searching: searching,
                sortById: sorting.sortBy.id,
                sortByLabel: sorting.sortBy.label,
                sortingDirection: sorting.direction
            };
        };
        this.isRefreshDataRequestChanged = function (previous, current) {
            if (previous === null || current === null) {
                return true;
            }
            return previous.requestedCount !== current.requestedCount
                || previous.page !== current.page
                || previous.pageSize !== current.pageSize
                || previous.searchTerm !== current.searchTerm
                || previous.searching !== current.searching
                || previous.sortingDirection !== current.sortingDirection
                || previous.sortById !== current.sortById
                || previous.sortByLabel !== current.sortByLabel;
        };
        this.areFilterValuesDifferent = function () {
            var previousOwnProperties = _this.getOwnProperties(_this.previousFilterValues);
            var currentOwnProperties = _this.getOwnProperties(_this.filterValues);
            if (previousOwnProperties.length === 0 && _this.filtersLoaded === true) {
                _this.previousFilterValues = _.cloneDeep(_this.filterValues);
                return false;
            }
            if (previousOwnProperties.length !== currentOwnProperties.length) {
                return true;
            }
            for (var _i = 0, previousOwnProperties_1 = previousOwnProperties; _i < previousOwnProperties_1.length; _i++) {
                var prop = previousOwnProperties_1[_i];
                if (currentOwnProperties.indexOf(prop) === -1) {
                    return true;
                }
                var prevArray = _this.previousFilterValues[prop];
                var currArray = _this.filterValues[prop];
                if (!_.isEqual(prevArray, currArray)) {
                    return true;
                }
            }
            return false;
        };
        this.getOwnProperties = function (obj) {
            var result = [];
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    result.push(prop);
                }
            }
            return result;
        };
    }
    SubnetsListController.prototype.initPagination = function () {
        this.pagination = this.pagination || {};
        this.pagination.pageSize = this.optionsNotEmpty.pageSize;
        this.pagination.page = this.pagination.page || 1;
        this.pagination.total = 5;
    };
    SubnetsListController.prototype.$onInit = function () {
        var _this = this;
        this.$templateCache.put("subnet-item-template", __webpack_require__(32));
        this.searchText = this.searchText || this.$translate.instant("orion_search_searchText");
        this.$templateCache.put(this.vendorFilterTemplateUrl, "<sw-vendor-icon vendor=\"{{::item.vendor}}\" css-class=\"sw-search__item-custom-property\">\n             </sw-vendor-icon> {{::item.title}}");
        this.panelOptions = {
            propertyPickerOptions: {
                gridSorting: {
                    direction: "original"
                }
            }
        };
        this.optionsNotEmpty = {
            selectionMode: "single",
            sidebarTitle: "Filter the results",
            templateUrl: "subnet-item-template",
            showAddRemoveFilterProperty: false,
            searchDebounce: 500,
            pageSize: 8
        };
        this.sorting = {
            sortableColumns: [
                { id: "address", label: "Address" },
                { id: "cidr", label: "CIDR" },
                { id: "friendlyName", label: "Friendly Name" },
                { id: "location", label: "Location" }
            ],
            sortBy: {
                id: "address",
                label: "Address"
            },
            direction: "asc"
        };
        this.$scope.selection = this.selection;
        this.requestedCount = this.$scope.requestedCount;
        this.initPagination();
        return this.$timeout(function () {
            _this.propertiesToRequest = _this.updatePropertiesToRequest(_this.propertiesToRequest, _this.rowDisplaySettings);
            _this.searchTerm = _this.$location.search().q;
        });
    };
    SubnetsListController.prototype.instant = function (value) {
        return this.$q(function (resolve) { return resolve(value); });
    };
    Object.defineProperty(SubnetsListController.prototype, "useApi", {
        get: function () {
            return this.swApi.api(false);
        },
        enumerable: true,
        configurable: true
    });
    SubnetsListController.prototype.rejectPendingRefresh = function () {
        if (this.searchToken) {
            this.searchToken.reject();
            this.searchToken = null;
        }
    };
    SubnetsListController.prototype.changeNumberOfRequestedIps = function (count) {
        this.requestedCount = count;
        this.pagination.page = 1;
        this.filteredListRemoteControl.refreshListData();
    };
    SubnetsListController.prototype.refreshData = function (filters, pagination, sorting, searching) {
        var _this = this;
        this.changeFiltersWidth();
        if (!this.pagination) {
            return this.$q.resolve();
        }
        if (!this.requestedCount) {
            return this.$q.resolve();
        }
        var currentRequestParameters = this.makeRefreshDataRequestObject(searching, sorting);
        if (!this.isRefreshDataRequestChanged(this.previousRefreshDataRequest, currentRequestParameters)) {
            var filterValuesDifferent = this.areFilterValuesDifferent();
            if (!filterValuesDifferent) {
                return this.currentRefreshDataQuery || this.$q.resolve();
            }
        }
        var getFiltersPromise = this.getFilters(searching);
        var getGroupsPromise = this.getDataForFilter(this.getGroupsUrl, {
            "q": this.searchTerm || searching,
            "skip": ((this.pagination.page - 1) * this.pagination.pageSize),
            "top": this.pagination.pageSize,
            "orderBy": this.formatOrderBy(sorting),
            "filterBy": this.getSearchFilters(this.filterValues),
            "having": this.requestedCount,
            "propertyToSelect": this.propertiesToRequest
        })
            .catch(function (e) {
            _this.$log.error("Search result:" + e);
        });
        this.previousRefreshDataRequest = this.makeRefreshDataRequestObject(searching, sorting);
        this.previousFilterValues = _.cloneDeep(this.filterValues);
        var time = new Date().getTime();
        this.lastRequestTimestamp = time;
        var promise = this.$q.all([getFiltersPromise, getGroupsPromise])
            .then(function (results) {
            if (_this.lastRequestTimestamp !== 0 && _this.lastRequestTimestamp !== time) {
                return _this.currentRefreshDataQuery || _this.$q.resolve();
            }
            _this.handleGetFiltersResponse(results[0]);
            _this.handleGetGroupsResponse(results[1]);
            _this.currentRefreshDataQuery = null;
            _this.lastRequestTimestamp = 0;
        });
        this.currentRefreshDataQuery = promise;
        return promise;
    };
    SubnetsListController.prototype.handleGetFiltersResponse = function (filterProperties) {
        this.filtersLoaded = true;
        this.filterProperties = filterProperties;
    };
    SubnetsListController.prototype.handleGetGroupsResponse = function (response) {
        this.usedSearchTerm = this.searchTerm;
        this.pagination.total = response.TotalCount;
        if (!response || !response.Items) {
            this.$log.warn("Received empty search result.");
            this.searchToken = null;
            return;
        }
        this.itemsSource = angular.copy(response.Items);
        this.$scope.selection = this.selection;
        this.searchToken = null;
    };
    SubnetsListController.prototype.updatePropertiesToRequest = function (propertiesToRequest, rowDisplaySettings) {
        var allSettingProperties = _.reduce(this.rowDisplaySettings, function (list, settings, key) {
            return settings
                ? _.concat(list, settings.customProps, settings.systemProps)
                : list;
        }, []);
        return _.uniq(_.concat(propertiesToRequest, allSettingProperties));
    };
    SubnetsListController.prototype.changeFiltersWidth = function () {
        // this is hack for long scroll bar on IE
        var subnetSelectorWizard = document.getElementById("sw-ipam-subnet-selector-wizard");
        var elements = subnetSelectorWizard.getElementsByClassName("xui-chiclets__main-cell");
        angular.forEach(elements, function (elem) {
            elem.style.width = "auto";
        });
    };
    return SubnetsListController;
}());
exports.default = SubnetsListController;


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SubnetsListComponent = /** @class */ (function () {
    function SubnetsListComponent() {
        this.template = __webpack_require__(33);
        this.controllerAs = "vm";
        this.controller = "subnetsListController";
        this.restrict = "E";
        this.scope = {
            selection: "=",
            step: "=",
            requestedCount: "="
        };
        this.link = function (scope, element, attrs, ctrl) {
            scope.$watch(function () { return scope.requestedCount; }, function () {
                ctrl.changeNumberOfRequestedIps(scope.requestedCount);
            });
        };
    }
    return SubnetsListComponent;
}());
exports.default = SubnetsListComponent;


/***/ }),
/* 95 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var fieldsList_editor_directive_1 = __webpack_require__(97);
var fieldsList_editor_controller_1 = __webpack_require__(35);
__webpack_require__(34);
exports.default = function (module) {
    templates.$inject = ["$templateCache"];
    module.component("swIpamFieldslistEditor", fieldsList_editor_directive_1.FieldsListEditor);
    module.controller("fieldsListEditorController", fieldsList_editor_controller_1.FieldsListEditorController);
    /* @ngInject */
    function templates($templateCache) {
        $templateCache.put("drop-down-template", __webpack_require__(37));
        $templateCache.put("text-box-template", __webpack_require__(38));
    }
    module.app().run(templates);
};


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(34);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fieldsList_editor_controller_1 = __webpack_require__(35);
var FieldsListEditor = /** @class */ (function () {
    /** @ngInject */
    FieldsListEditor.$inject = ["swUtil"];
    function FieldsListEditor(swUtil) {
        this.swUtil = swUtil;
        this.restrict = "E";
        this.template = __webpack_require__(36);
        this.scope = {};
        this.bindToController = {
            itemsSource: "=",
            newListItemHandler: "=",
            dropdownItemsSource: "=?",
            onRemoveButtonClick: "&",
            onSaveButtonClick: "&"
        };
        this.controller = fieldsList_editor_controller_1.FieldsListEditorController;
        this.controllerAs = "vm";
    }
    return FieldsListEditor;
}());
exports.FieldsListEditor = FieldsListEditor;


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accounts_service_1 = __webpack_require__(99);
var profile_service_1 = __webpack_require__(100);
var iprequestsettings_service_1 = __webpack_require__(101);
var ipamErrorHandler_service_1 = __webpack_require__(9);
var ipRequestWidgetService_1 = __webpack_require__(1);
var ipRequestService_1 = __webpack_require__(0);
var requesterCustomFields_service_1 = __webpack_require__(102);
exports.default = function (module) {
    module.service("accountsService", accounts_service_1.AccountsService);
    module.service("profileService", profile_service_1.ProfileService);
    module.service("ipRequestSettingsService", iprequestsettings_service_1.IpRequestSettingsService);
    module.service("ipamErrorHandlerService", ipamErrorHandler_service_1.IpamErrorHandlerService);
    module.service("ipRequestWidgetService", ipRequestWidgetService_1.IpRequestWidgetService);
    module.service("ipRequestService", ipRequestService_1.IpRequestService);
    module.service("requesterCustomFieldsService", requesterCustomFields_service_1.RequesterCustomFieldsService);
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AccountsService = /** @class */ (function () {
    /** @ngInject */
    AccountsService.$inject = ["$http", "$q", "toastService", "getTextService", "swApi"];
    function AccountsService($http, $q, toastService, getTextService, swApi) {
        var _this = this;
        this.$http = $http;
        this.$q = $q;
        this.toastService = toastService;
        this.getTextService = getTextService;
        this.swApi = swApi;
        this.getCloudIpamSettingsUrl = "ipam/CloudAccount/get";
        this.getIpamSettingsByAccountId = function (accountId) {
            var url = _this.getCloudIpamSettingsUrl + ("/" + accountId);
            return _this.swApi.api(false).one(url).get()
                .then(function (result) {
                return result;
            });
        };
    }
    AccountsService.prototype.getAwsAccount = function (accountId) {
        var _this = this;
        var deferred = this.$q.defer();
        var url = "/api/CloudAccount/GetAwsCloudAccount?cloudAccountId=" + accountId;
        this.$http.get(url)
            .then(function (response) {
            deferred.resolve(response.data);
        }, function (reason) {
            _this.toastService.error(_this.getTextService("Aws Account not found"), _this.getTextService("Account not found"));
            deferred.reject(null);
        });
        return deferred.promise;
    };
    AccountsService.prototype.getAzureAccount = function (accountId) {
        var _this = this;
        var deferred = this.$q.defer();
        var url = "/api/CloudAccount/GetAzureCloudAccount?cloudAccountId=" + accountId;
        this.$http.get(url)
            .then(function (response) {
            deferred.resolve(response.data);
        }, function (reason) {
            _this.toastService.error(_this.getTextService("Azure Account not found"), _this.getTextService("Account not found"));
            deferred.reject(null);
        });
        return deferred.promise;
    };
    return AccountsService;
}());
exports.AccountsService = AccountsService;


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ProfileService = /** @class */ (function () {
    /** @ngInject */
    ProfileService.$inject = ["$http", "$q", "toastService", "getTextService", "swApi"];
    function ProfileService($http, $q, toastService, getTextService, swApi) {
        var _this = this;
        this.$http = $http;
        this.$q = $q;
        this.toastService = toastService;
        this.getTextService = getTextService;
        this.swApi = swApi;
        this.getRequestUrl = "ipam/profile";
        this.getProfile = function () {
            var url = _this.getRequestUrl;
            return _this.swApi.api(false).one(url).get()
                .then(function (result) {
                return result;
            });
        };
    }
    return ProfileService;
}());
exports.ProfileService = ProfileService;


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestSettingsService = /** @class */ (function () {
    /** @ngInject */
    IpRequestSettingsService.$inject = ["swApi"];
    function IpRequestSettingsService(swApi) {
        this.swApi = swApi;
        this.url = "ipam/IpRequestSettings";
    }
    IpRequestSettingsService.prototype.getSettings = function () {
        return this.swApi
            .api(false)
            .one(this.url)
            .get();
    };
    ;
    IpRequestSettingsService.prototype.updateSettings = function (ipRequestSettings) {
        return this.swApi
            .api(false)
            .one(this.url)
            .customPUT(ipRequestSettings)
            .then(function (response) {
            return response;
        });
    };
    return IpRequestSettingsService;
}());
exports.IpRequestSettingsService = IpRequestSettingsService;


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var RequesterCustomFieldsService = /** @class */ (function () {
    /** @ngInject */
    RequesterCustomFieldsService.$inject = ["$q", "swApi"];
    function RequesterCustomFieldsService($q, swApi) {
        this.$q = $q;
        this.swApi = swApi;
        this.getUrl = "ipam/IPRequests/RequesterCustomFields";
    }
    RequesterCustomFieldsService.prototype.getCustomFields = function () {
        return this.swApi
            .api(false)
            .one(this.getUrl)
            .get();
    };
    ;
    return RequesterCustomFieldsService;
}());
exports.RequesterCustomFieldsService = RequesterCustomFieldsService;


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(104);
    req.keys().forEach(function (r) {
        var key = "ipam" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/editIpamSettings/edit-ipam-settings.html": 6,
	"./components/fieldsList/fieldsList-editor-directive.html": 36,
	"./components/fieldsList/handler/dropDown/dropDown-template.html": 37,
	"./components/fieldsList/handler/textBox/textBox-template.html": 38,
	"./components/ipRequest/ipRequest.html": 8,
	"./components/subnetsList/subnet-item-template.html": 32,
	"./components/subnetsList/subnetsList.html": 33,
	"./templates/ipRequest/availableAddressesStep.html": 105,
	"./templates/ipRequest/confirmationStep.html": 106,
	"./templates/ipRequest/requestDetailsStep.html": 107,
	"./templates/ipRequest/subnetSelectorStep.html": 108,
	"./views/cloudDnsRecords/cloudDnsRecords.html": 5,
	"./views/cloudDnsZones/cloudDnsZones.html": 3,
	"./views/ipRequestSettings/ipRequestSettings.html": 31,
	"./widgets/ipAddressRequests/customPropertyEditor/customPropertyBoolItem.html": 10,
	"./widgets/ipAddressRequests/customPropertyEditor/customPropertyDateItem.html": 11,
	"./widgets/ipAddressRequests/customPropertyEditor/customPropertyEditor-directive.html": 12,
	"./widgets/ipAddressRequests/customPropertyEditor/customPropertyIntegerItem.html": 13,
	"./widgets/ipAddressRequests/customPropertyEditor/customPropertyRealItem.html": 14,
	"./widgets/ipAddressRequests/customPropertyEditor/customPropertyRestrictedItem.html": 15,
	"./widgets/ipAddressRequests/customPropertyEditor/customPropertyTextItem.html": 16,
	"./widgets/ipAddressRequests/customPropertyEditor/dateTimePicker/dateTimePicker-directive.html": 17,
	"./widgets/ipAddressRequests/editIpRequestAddressTemplate.html": 18,
	"./widgets/ipAddressRequests/ipAddressRequests.html": 19,
	"./widgets/ipAddressRequests/ipAddressSelector/ipAddressSelector-directive.html": 20,
	"./widgets/ipAddressRequests/ipRequestCpPopoverTemplate.html": 21,
	"./widgets/ipAddressRequests/ipRequestHeaderTemplate.html": 22,
	"./widgets/ipAddressRequests/ipRequestNodeTemplate.html": 23,
	"./widgets/ipAddressRequests/submitIpRequestAddressTemplate.html": 24,
	"./widgets/ipAddressRequests/subnetSelector/subnetRowTemplate.html": 109,
	"./widgets/ipAddressRequests/subnetSelector/subnetSelector-directive.html": 25,
	"./widgets/ipAddressRequests/subnetSelector/subnetSelectorRowTemplate.html": 26,
	"./widgets/ipAddressRequests/subnetSelectorDialogTemplate.html": 27,
	"./widgets/requesterDetails/requesterDetails.html": 28
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 104;

/***/ }),
/* 105 */
/***/ (function(module, exports) {

module.exports = "<div xui-busy=step.isBusy xui-busy-message=_t(Loading...) _ta> <xui-message type=warning ng-if=step.notEnoughAddress _t> <b>There are not enough IP addresses in this subnet to accommodate your request.</b> <br/> You may submit a request to the IPAM Administrator or cancel. </xui-message> <div class=\"container-fluid available-address-step\" ng-if=!step.notEnoughAddress ng-form=step.ipRequest_form> <div _t> The following IP address(es) will be reserved on subnet: <b>{{step.ipRequest.Subnet}}</b>. Enter the Hostname and MAC Address, if known. </div> <div class=\"row grid-title\"> <div class=\"col-md-3 col-lg-3\"> <label class=xui-textbox__control-label _t>Available IP Address</label> </div> <div class=\"col-md-3 col-lg-3\"> <label class=xui-textbox__control-label _t>Host Name</label> <span class=xui-textbox__state-text ng-show=!step.hostnameRequired _t>Optional</span> </div> <div class=\"col-md-1 col-lg-1\"></div> <div class=\"col-md-3 col-lg-3\"> <label class=xui-textbox__control-label _t>Mac Address</label> <span class=xui-textbox__state-text ng-show=!step.macAddressRequired _t>Optional</span> </div> </div> <xui-listview items-source=step.itemsPaging template-url=addresses-template> </xui-listview> <xui-pager class=small page=step.pagination.currentPage page-size=step.pagination.pageSize total=step.pagination.total adjacent={{step.pagination.adjacent}} dots=... scroll-top=false hide-if-empty=true show-prev-next=true pager-action=\"step.onPageChanged(page, pageSize, total)\"> </xui-pager> </div> </div> <script type=text/ng-template id=addresses-template> <div class=\"row\">        \r\n        <div class=\"col-md-3 col-lg-3 ip-address\">{{::item.Address}}</div>\r\n        <div class=\"col-md-3 col-lg-3\">\r\n            <xui-textbox ng-model=\"item.HostName.Value\" \r\n            validators=\"maxlength=255\" ng-pattern=\"/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$/\" \r\n            ng-trim=\"true\" \r\n            is-required=\"item.HostName.IsMandatory\">\r\n        </xui-textbox>            \r\n        </div>\r\n        <div class=\"col-md-1 col-lg-1\"></div>\r\n        <div class=\"col-md-3 col-lg-3\">\r\n            <xui-textbox ng-model=\"item.MacAddress.Value\" \r\n            validators=\"maxlength=40\" \r\n            ng-pattern=\"'^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$'\" \r\n            is-required=\"item.MacAddress.IsMandatory\">\r\n        </xui-textbox>\r\n        </div>\r\n    </div> </script>";

/***/ }),
/* 106 */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid confirmation-step\"> <div> <xui-message type=ok automation=iprequest-confirmMsgRequested _t> <span> <b>Your request for {{step.ipRequest.RequestAddressCount}} IP Address(es) <span ng-if=step.mode> on subnet {{step.ipRequest.Subnet}}</span> has</b> been sent to your Administrator. You will be contacted shortly. </span> </xui-message> </div> <div ng-if=step.mode> <div class=\"row grid-title\"> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Requested IP Address</label> </div> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Subnet</label> </div> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Host Name</label> <span class=xui-textbox__state-text _t>Optional</span> </div> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Mac Address</label> <span class=xui-textbox__state-text _t>Optional</span> </div> </div> <div class=reserved-ip-info> <xui-listview items-source=step.itemsPaging template-url=addresses-template-view> </xui-listview> </div> <xui-pager class=small page=step.pagination.currentPage page-size=step.pagination.pageSize total=step.pagination.total adjacent={{step.pagination.adjacent}} dots=... scroll-top=false hide-if-empty=true show-prev-next=true pager-action=\"step.onPageChanged(page, pageSize, total)\"> </xui-pager> </div> </div> <div class=\"to-print xui\"> <div class=visible-print-block> <div class=\"row grid-title\"> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Requested IP Address</label> </div> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Subnet</label> </div> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Host Name</label> <span class=xui-textbox__state-text _t>Optional</span> </div> <div class=col-xs-3> <label class=xui-textbox__control-label _t>Mac Address</label> <span class=xui-textbox__state-text _t>Optional</span> </div> </div> <div ng-repeat=\"item in step.ipRequest.Addresses\" class=row> <div class=\"col-xs-3 ip-address\">{{::item.Address}}</div> <div class=col-xs-3>{{::item.SubnetFriendlyName}}<br>{{::item.SubnetAddress}}/{{::item.SubnetCidr}}</div> <div class=col-xs-3>{{::item.HostName.Value}}</div> <div class=col-xs-3>{{::item.MacAddress.Value}}</div> </div> </div> </div> <script type=text/ng-template id=addresses-template-view> <div class=\"row\">\r\n        <div class=\"col-xs-3 ip-address\">{{::item.Address}}</div>\r\n        <div class=\"col-xs-3\">{{::item.SubnetFriendlyName}}<br>{{::item.SubnetAddress}}/{{::item.SubnetCidr}}</div>\r\n        <div class=\"col-xs-3\">{{::item.HostName.Value}}</div>\r\n        <div class=\"col-xs-3\">{{::item.MacAddress.Value}}</div>\r\n    </div> </script>";

/***/ }),
/* 107 */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid request-details-step\" ng-form=step.ipRequest_form> <div class=row> <div class=\"col-sm-4 col-md-4\"> <div class=\"xui-text-h2 xui-margin-lgb\" _t> Request Details </div> </div> </div> <div class=row> <div class=\"col-sm-4 col-md-4\"> <xui-textbox ng-model=step.requestAddressCount caption=\"How many IP addresses do you need?\" is-required=true type=number validators=\"required,min=1,max=999\" automation=iprequest-ipaddressCount></xui-textbox> </div> </div> <div ng-show=step.hasAvailableSubnets class=row> <div class=\"col-sm-12 col-md-12 select-subnet\"> <div class=xui-textbox__control-label-container> <label class=xui-textbox__control-label _t>Would you like to select a subnet for your IP Address(es)?</label> </div> <xui-radio ng-model=step.knownSubnet ng-value=false automation=iprequest-selectSubnets _t>No</xui-radio> <xui-radio ng-model=step.knownSubnet ng-value=true automation=iprequest-selectSubnets _t>Yes (selection occurs on next step)</xui-radio> </div> </div> <div class=row> <div class=\"col-sm-4 col-md-4\"> <xui-textbox ng-model=step.comment caption=\"Additional Comments? (i.e. department/location/ other)\" rows=3 validators=\"maxlength=1000\" automation=iprequest-comments></xui-textbox> </div> </div> <xui-divider></xui-divider> <div class=row> <div class=\"col-sm-4 col-md-4\"> <div class=\"xui-text-h2 xui-margin-lgb\" _t> Your Contact Info </div> <xui-textbox ng-model=step.firstName caption=\"_t(First Name)\" _ta validators=\"required,maxlength=100\" automation=iprequest-firstName></xui-textbox> <xui-textbox ng-model=step.lastName caption=\"_t(Last Name)\" _ta validators=\"required,maxlength=100\" automation=iprequest-lastName></xui-textbox> <xui-textbox ng-model=step.phone caption=_t(Phone) _ta type=phone validators=\"maxlength=100\" automation=iprequest-phone></xui-textbox> <xui-textbox ng-model=step.email caption=_t(Email) _ta type=email validators=\"required,maxlength=255\" automation=iprequest-email></xui-textbox> <ul> <li ng-repeat=\"item in step.customFields\"> <xui-textbox ng-model=item.Value caption={{item.DisplayName}} is-required=item.IsRequired></xui-textbox> </li> </ul> </div> </div> </div> ";

/***/ }),
/* 108 */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid subnet-selector-step\"> <subnets-list step=step selection=step.selection requested-count=step.requestedIpsCount /> </div> ";

/***/ }),
/* 109 */
/***/ (function(module, exports) {

module.exports = "<div class=ipaddressrequests-widget-row-fluid> <div class=col-sm-8> <div class=xui-text-l>{{item.FriendlyName}}</div> <div class=xui-text-s>{{item.Address}} / {{item.Cidr}}</div> </div> <div class=col-sm-4> <div class=xui-text-l>{{item.AvailableCount ? item.AvailableCount : 0}} IPs</div> </div> </div> ";

/***/ })
/******/ ]);
//# sourceMappingURL=ipam.js.map