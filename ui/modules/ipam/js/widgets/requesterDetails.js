/*!
 * @solarwinds/ipam 2020.2.5-223
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 126);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestService = /** @class */ (function () {
    /** @ngInject */
    IpRequestService.$inject = ["swApi"];
    function IpRequestService(swApi) {
        this.swApi = swApi;
    }
    IpRequestService.prototype.getIpRequestId = function (alertId) {
        return this.swApi.api(false).one("ipam/ipRequests/idByAlertId/" + alertId).get();
    };
    IpRequestService.prototype.getIpRequestDetails = function (ipRequestId) {
        return this.swApi.api(false).one("ipam/ipRequests/" + ipRequestId).get();
    };
    IpRequestService.prototype.getRequesterDetails = function (ipRequestId) {
        return this.swApi.api(false).one("ipam/ipRequests/" + ipRequestId + "/requesterDetails").get();
    };
    IpRequestService.prototype.updateIpRequestDetails = function (ipRequestDetails) {
        return this.swApi
            .api(false)
            .all("ipam/ipRequests")
            .customPUT(ipRequestDetails)
            .then(function (response) {
            return response;
        });
    };
    IpRequestService.prototype.getAvailableSubnets = function (requestedIpsCount) {
        return this.swApi.api(false).one("ipam/ipRequests/subnetsAvailableForReservation?requestedIpsCount=" + requestedIpsCount).get();
    };
    IpRequestService.prototype.getAvailableIpsWithCustomProperties = function (subnetId, ipCount) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddress?subnetId=" + subnetId + "&addressCount=" + ipCount + "&withCustomProperties=true")
            .get();
    };
    IpRequestService.prototype.getAvailableIpsWithoutCustomProperties = function (subnetId, ipCount) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddress?subnetId=" + subnetId + "&addressCount=" + ipCount)
            .get();
    };
    IpRequestService.prototype.getAvailableIpsForSubnet = function (subnetId) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddressesForSubnet?subnetId=" + subnetId)
            .get();
    };
    IpRequestService.prototype.getDhcpServersBySubnetId = function (subnetId) {
        return this.swApi.api(false).one("ipam/ipRequests/dhcpServersBySubnetId/" + subnetId).get();
    };
    IpRequestService.prototype.getSubnetDetails = function (subnetId) {
        return this.swApi.api(false).one("ipam/ipRequests/subnetDetails/" + subnetId).get();
    };
    IpRequestService.prototype.getNodesCustomPropertiesValues = function (ipNodeId) {
        return this.swApi.api(false).one("ipam/ipRequests/ipCustomPropertiesValues/" + ipNodeId).get();
    };
    return IpRequestService;
}());
exports.IpRequestService = IpRequestService;


/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestWidgetService = /** @class */ (function () {
    /** @ngInject */
    IpRequestWidgetService.$inject = ["swNetObjectOpidConverterService", "$q"];
    function IpRequestWidgetService(swNetObjectOpidConverterService, $q) {
        this.swNetObjectOpidConverterService = swNetObjectOpidConverterService;
        this.$q = $q;
    }
    IpRequestWidgetService.prototype.extractAlertId = function (opId) {
        var _this = this;
        return this.swNetObjectOpidConverterService
            .opidsToNetObjects([opId])
            .then(function (result) { return _this.handleConvertedNetObjects(result); });
    };
    IpRequestWidgetService.prototype.handleConvertedNetObjects = function (result) {
        if (result.length !== 1) {
            return this.$q.reject("No NetObjects found");
        }
        var netObject = result[0].netObject;
        var regex = /AAT:(\d+)/;
        var match = regex.exec(netObject);
        if (match === null) {
            return this.$q.reject("Cannot find Active Alert Id");
        }
        var alertId = Number(match[1]);
        return this.$q.resolve(alertId);
    };
    return IpRequestWidgetService;
}());
exports.IpRequestWidgetService = IpRequestWidgetService;


/***/ }),

/***/ 126:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(127);


/***/ }),

/***/ 127:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var requesterDetails_controller_1 = __webpack_require__(128);
var ipRequestService_1 = __webpack_require__(0);
var ipRequestWidgetService_1 = __webpack_require__(1);
__webpack_require__(44);
exports.default = function (module) {
    module.controller("RequesterDetailsController", requesterDetails_controller_1.RequesterDetailsController);
};
Xui.module.service("ipRequestWidgetService", ipRequestWidgetService_1.IpRequestWidgetService);
Xui.module.service("ipRequestService", ipRequestService_1.IpRequestService);


/***/ }),

/***/ 128:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
__webpack_require__(44);
var widgets_1 = __webpack_require__(29);
var ipRequestService_1 = __webpack_require__(0);
exports.WidgetSettingsName = "ipamRequesterDetailsWidgetSettings";
var RequesterDetailsController = /** @class */ (function () {
    /** @ngInject */
    RequesterDetailsController.$inject = ["ipRequestWidgetService", "ipRequestService"];
    function RequesterDetailsController(ipRequestWidgetService, ipRequestService) {
        var _this = this;
        this.ipRequestWidgetService = ipRequestWidgetService;
        this.ipRequestService = ipRequestService;
        this.onLoad = function (config) {
            var opId;
            if (!config.settings.opid) {
                config.setOverlayType("dataNotApplicable");
                return;
            }
            else {
                opId = config.settings.opid;
            }
            _this.ipRequestWidgetService.extractAlertId(opId)
                .then(function (result) {
                _this.alertId = result;
                _this.fetchData().then(function (items) {
                    _this.listItems = items;
                });
            })
                .catch(function (reason) {
                config.setOverlayType("dataNotAvailable");
            });
        };
    }
    RequesterDetailsController.prototype.fetchData = function () {
        var _this = this;
        return this.ipRequestService
            .getIpRequestId(this.alertId)
            .then(function (result) { _this.ipRequestId = result; })
            .then(function () { return _this.ipRequestService.getRequesterDetails(_this.ipRequestId); })
            .then(function (result) { return result; });
    };
    RequesterDetailsController = __decorate([
        widgets_1.Widget({
            selector: "swRequesterDetails",
            template: __webpack_require__(28),
            controllerAs: "vm",
            settingName: exports.WidgetSettingsName,
        }),
        __metadata("design:paramtypes", [Object, ipRequestService_1.IpRequestService])
    ], RequesterDetailsController);
    return RequesterDetailsController;
}());
exports.RequesterDetailsController = RequesterDetailsController;


/***/ }),

/***/ 28:
/***/ (function(module, exports) {

module.exports = "<div class=sw-requester-details> <ul> <li ng-repeat=\"item in vm.listItems\" class=requesterDetails-widget> <div class=xui-text-l> <span class=display-name>{{item.DisplayName}}</span> </div> <div class=xui-text-p> <span>{{item.Value}}</span> </div> <hr ng-if=!$last /> </li> </ul> </div> ";

/***/ }),

/***/ 29:
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ }),

/***/ 44:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

/******/ });
//# sourceMappingURL=requesterDetails.js.map