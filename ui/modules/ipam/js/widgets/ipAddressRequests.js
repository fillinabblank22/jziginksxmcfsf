/*!
 * @solarwinds/ipam 2020.2.5-223
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 110);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestService = /** @class */ (function () {
    /** @ngInject */
    IpRequestService.$inject = ["swApi"];
    function IpRequestService(swApi) {
        this.swApi = swApi;
    }
    IpRequestService.prototype.getIpRequestId = function (alertId) {
        return this.swApi.api(false).one("ipam/ipRequests/idByAlertId/" + alertId).get();
    };
    IpRequestService.prototype.getIpRequestDetails = function (ipRequestId) {
        return this.swApi.api(false).one("ipam/ipRequests/" + ipRequestId).get();
    };
    IpRequestService.prototype.getRequesterDetails = function (ipRequestId) {
        return this.swApi.api(false).one("ipam/ipRequests/" + ipRequestId + "/requesterDetails").get();
    };
    IpRequestService.prototype.updateIpRequestDetails = function (ipRequestDetails) {
        return this.swApi
            .api(false)
            .all("ipam/ipRequests")
            .customPUT(ipRequestDetails)
            .then(function (response) {
            return response;
        });
    };
    IpRequestService.prototype.getAvailableSubnets = function (requestedIpsCount) {
        return this.swApi.api(false).one("ipam/ipRequests/subnetsAvailableForReservation?requestedIpsCount=" + requestedIpsCount).get();
    };
    IpRequestService.prototype.getAvailableIpsWithCustomProperties = function (subnetId, ipCount) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddress?subnetId=" + subnetId + "&addressCount=" + ipCount + "&withCustomProperties=true")
            .get();
    };
    IpRequestService.prototype.getAvailableIpsWithoutCustomProperties = function (subnetId, ipCount) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddress?subnetId=" + subnetId + "&addressCount=" + ipCount)
            .get();
    };
    IpRequestService.prototype.getAvailableIpsForSubnet = function (subnetId) {
        return this.swApi.api(false)
            .one("ipam/ipRequests/availableIpAddressesForSubnet?subnetId=" + subnetId)
            .get();
    };
    IpRequestService.prototype.getDhcpServersBySubnetId = function (subnetId) {
        return this.swApi.api(false).one("ipam/ipRequests/dhcpServersBySubnetId/" + subnetId).get();
    };
    IpRequestService.prototype.getSubnetDetails = function (subnetId) {
        return this.swApi.api(false).one("ipam/ipRequests/subnetDetails/" + subnetId).get();
    };
    IpRequestService.prototype.getNodesCustomPropertiesValues = function (ipNodeId) {
        return this.swApi.api(false).one("ipam/ipRequests/ipCustomPropertiesValues/" + ipNodeId).get();
    };
    return IpRequestService;
}());
exports.IpRequestService = IpRequestService;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestWidgetService = /** @class */ (function () {
    /** @ngInject */
    IpRequestWidgetService.$inject = ["swNetObjectOpidConverterService", "$q"];
    function IpRequestWidgetService(swNetObjectOpidConverterService, $q) {
        this.swNetObjectOpidConverterService = swNetObjectOpidConverterService;
        this.$q = $q;
    }
    IpRequestWidgetService.prototype.extractAlertId = function (opId) {
        var _this = this;
        return this.swNetObjectOpidConverterService
            .opidsToNetObjects([opId])
            .then(function (result) { return _this.handleConvertedNetObjects(result); });
    };
    IpRequestWidgetService.prototype.handleConvertedNetObjects = function (result) {
        if (result.length !== 1) {
            return this.$q.reject("No NetObjects found");
        }
        var netObject = result[0].netObject;
        var regex = /AAT:(\d+)/;
        var match = regex.exec(netObject);
        if (match === null) {
            return this.$q.reject("Cannot find Active Alert Id");
        }
        var alertId = Number(match[1]);
        return this.$q.resolve(alertId);
    };
    return IpRequestWidgetService;
}());
exports.IpRequestWidgetService = IpRequestWidgetService;


/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpamErrorHandlerService = /** @class */ (function () {
    /** @ngInject */
    IpamErrorHandlerService.$inject = ["toastService"];
    function IpamErrorHandlerService(toastService) {
        this.toastService = toastService;
    }
    IpamErrorHandlerService.prototype.handle = function (ex) {
        if (ex.status === 404) {
            this.toastService.error("The entity does not exist in the database.");
        }
        else if (ex.status === 500) {
            this.toastService.error("Internal server error.");
        }
        if (typeof ex.data === "string" && ex.data) {
            this.toastService.error(ex.data);
        }
        else if (ex.data != null && ex.data.message != null) {
            this.toastService.error(ex.data.message);
        }
    };
    return IpamErrorHandlerService;
}());
exports.IpamErrorHandlerService = IpamErrorHandlerService;


/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=\"sw-ipam-iprequest-custom-property-editor-value xui-dropdown--justified\"> <xui-dropdown ng-model=item.Value is-required=item.Required is-editable=true clear-on-blur=true items-source=vm.boolSource></xui-dropdown> </div> ";

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=sw-ipam-iprequest-custom-property-editor-value> <sw-ipam-date-time-picker required=item.Required value=item.Value> </sw-ipam-date-time-picker> </div>";

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-costom-property-editor> <div class=\"xui-text-h3 xui-margin-lgb\" ng-show=vm.itemsSource.length _t>{{::vm.title}}</div> <ul> <li ng-repeat=\"item in vm.itemsSource\"> <div ng-include=vm.getTemplate(item)></div> </li> </ul> </div>";

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-iprequest-custom-property-editor-value> <xui-textbox ng-model=item.Value caption={{item.Name}} is-required=item.Required xui-integer-validator> <div ng-message=integer _t>Value should be integer!</div> </xui-textbox> </div> ";

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-iprequest-custom-property-editor-value> <xui-textbox ng-model=item.Value caption={{item.Name}} is-required=item.Required ng-pattern=/^-?\\d*(\\.\\d+)?$/ > <div ng-message=pattern _t>Value should be a floating point number!</div> </xui-textbox> </div> ";

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=\"sw-ipam-iprequest-custom-property-editor-value xui-dropdown--justified\"> <xui-dropdown ng-model=item.Value is-required=item.Required is-editable=true clear-on-blur=true items-source=item.RestrictedValues></xui-dropdown> </div> ";

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = "<h6 class=xui-text-l>{{item.Name}}</h6> <div class=sw-ipam-iprequest-custom-property-editor-value> <xui-textbox ng-model=item.Value is-required=item.Required></xui-textbox> </div> ";

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<div id=ipamDateTimePicker class=\"sw-ipam-date-time-picker row\"> <div class=col-md-4> <xui-date-picker ng-model=vm.value is-required=vm.required preserve-insignificant=true> <div ng-message=date _t>Chosen date is invalid!</div> </xui-date-picker> </div> <div class=\"col-md-8 xui-dropdown--justified\"> <xui-dropdown ng-model=vm.chosenTime is-required=vm.required is-editable=true clear-on-blur=true items-source=vm.timeSource on-changed=vm.dateTimeChanged()></xui-dropdown> </div> </div>";

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div> <form name=editAddress> <xui-textbox name=hostnameEdit caption=Hostname ng-model=vm.dialogOptions.viewModel.Hostname.Value validators=\"maxlength=255\" ng-pattern=/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$/ ng-trim=true is-required=vm.dialogOptions.viewModel.Hostname.IsMandatory> <span ng-show=editAddress.hostnameEdit.$error.pattern _t>Invalid hostname!</span> </xui-textbox> <xui-textbox name=macAddressEdit caption=\"MAC Address\" ng-model=vm.dialogOptions.viewModel.MacAddress.Value validators=\"maxlength=40\" ng-pattern=/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/ is-required=vm.dialogOptions.viewModel.MacAddress.IsMandatory> <span ng-show=editAddress.macAddressEdit.$error.pattern _t>Invalid MAC address!</span> </xui-textbox> <br/> <sw-ipam-custom-property-editor items-source=vm.dialogOptions.viewModel.CustomProperties title=\"Custom Properties\"/> </form> </div> </xui-dialog>";

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = "<div id=ipamIpAddressRequestWidget class=sw-ip-address-requests> <div> <xui-filtered-list-v2 state=vm.listState dispatcher=vm.listDispatcher remote-control=vm.listControl controller=vm> <sw-grid-toolbar-container> <xui-toolbar> <div ng-if=\"vm.subnetDetails && vm.subnetDetails.GroupId > 0\" class=\"ipaddressrequests-widget-subnetDetails ipaddressrequests-widget-friendlyNameDiv\"> <b>Subnet:</b> {{vm.subnetDetails.Address}}/{{vm.subnetDetails.Cidr}} ({{vm.subnetDetails.FriendlyName}}) </div> <xui-toolbar-item> <xui-button display-style=primary is-disabled=vm.disableSubnetSelector ng-click=vm.selectSubnet() _t> Select subnet </xui-button> </xui-toolbar-item> </xui-toolbar> </sw-grid-toolbar-container> </xui-filtered-list-v2> </div> <div class=ipaddressrequests-widget-submitButtonRow> <span class=ipaddressrequests-widget-summaryLabel _t> {{vm.allItemsCount}} <span _t>Total</span>, {{vm.acceptedCount}} <span _t>Accepted</span>, {{vm.deniedCount}} <span _t>Denied IPs</span> </span> <div ng-show=!vm.ipRequestDetails.IsDisabled class=ipaddressrequests-widget-submitButton> <xui-button display-style=secondary ng-click=vm.reject() _t> Deny Request </xui-button> </div> <div class=ipaddressrequests-widget-submitButton> <xui-button is-disabled=vm.isDisabledSubmitButton display-style=primary ng-click=vm.submit()> {{vm.submitButtonText}} </xui-button> </div> </div> </div> ";

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<div id=ipamIpAddressSelector class=sw-ipam-ipAddress-selector> <xui-search is-disabled=vm.isDisabled value=vm.ngModel placeholder={{::vm.placeholder}} on-search=vm.onSearch(value) on-change=vm.onChange(vm.ngModel) items-source=vm.ipAddresses display-value=vm.displayValue ng-blur=vm.onBlur(vm.ngModel) search-on-change=true> </xui-search> </div>";

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = "<ul class=ipaddressrequests-widget__popover-ul> <li class=ipaddressrequests-widget__popover-li ng-repeat=\"cp in item.NotEmptyCustomProperties\"> <div class=xui-text-l> <span><b>{{cp.Name}}</b></span> </div> <div class=xui-text-p> <span>{{cp.Value.toLocaleString()}}</span> </div> <hr ng-if=!$last /> </li> </ul> ";

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=\"!(vm.allItems && vm.allItems.length == 0)\" class=\"row ipaddressrequests-widget__header\"> <div class=col-xs-2> <span _t>Status</span> </div> <div class=col-xs-3> <span _t>IP Address</span> </div> <div class=col-xs-2> <span _t>Hostname</span> </div> <div class=col-xs-2> <span _t>MAC Address</span> </div> <div class=col-xs-2> <span _t>Custom properties</span> </div> <div class=col-xs-1> </div> </div> ";

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = "<div class=row> <div class=col-xs-2> <xui-switch ng-model=item.IsAccepted ng-disabled=item.IsDisabled /> </div> <div id=ipamIpAddressSelector class=col-xs-3> <sw-ipam-ipAddress-selector ng-model=item ip-addresses=vm.ipAddresses is-disabled=item.IsDisabled on-item-select=vm.handleOnSelect> </sw-ipam-ipAddress-selector> </div> <div class=col-xs-2> {{vm.showNoneIfEmpty(item.Hostname.Value)}} </div> <div class=col-xs-2> {{vm.showNoneIfEmpty(item.MacAddress.Value)}} </div> <div class=col-xs-2> <xui-popover ng-show=\"vm.countCustomProperties(item.NotEmptyCustomProperties) > 0\" xui-popover-title=\"_t(Custom Properties)\" _ta xui-popover-content=vm.getCpPopoverTemplate() xui-popover-trigger=mouseenter> <span class=ipaddressrequests-widget-customPropertyDotted> {{vm.countCustomProperties(item.NotEmptyCustomProperties)}} of {{vm.countCustomProperties(item.CustomProperties)}} </span> </xui-popover> <span ng-show=\"vm.countCustomProperties(item.NotEmptyCustomProperties) === 0 && vm.countCustomProperties(item.CustomProperties) > 0\"> {{vm.countCustomProperties(item.NotEmptyCustomProperties)}} of {{vm.countCustomProperties(item.CustomProperties)}} </span> <span ng-show=\"vm.countCustomProperties(item.NotEmptyCustomProperties) === 0 && vm.countCustomProperties(item.CustomProperties) === 0\"> (none) </span> </div> <div class=col-xs-1 ng-show=!item.IsDisabled> <xui-button display-style=link icon=edit size=small ng-click=vm.editIpRequestAddress(item)></xui-button> </div> </div> ";

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div> <form name=submitRequest> <p>{{vm.dialogOptions.message}}</p> <p _t>This cannot be undone.</p> <div ng-if=\"vm.dialogOptions.viewModel.DhcpServers && vm.dialogOptions.viewModel.DhcpServers.length > 0\"> <b><p _t>For DHCP reservation, select DHCP server:</p></b> <xui-dropdown is-required=\"vm.dialogOptions.viewModel.DhcpServers && vm.dialogOptions.viewModel.DhcpServers.length > 0\" display-value=FriendlyName class=xui-dropdown--justified ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpServer items-source=vm.dialogOptions.viewModel.DhcpServers> <div ng-message=required>This field is required</div> </xui-dropdown> <b><p _t>Supported Types:</p></b> <xui-radio ng-value=1 class=ipaddressrequests-widget-radio ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpReservationType _t> DHCP only </xui-radio> <xui-radio ng-value=2 class=ipaddressrequests-widget-radio ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpReservationType _t> BOOTP only </xui-radio> <xui-radio ng-value=3 class=ipaddressrequests-widget-radio ng-model=vm.dialogOptions.viewModel.RequestDetails.DhcpReservationType _t> Both </xui-radio> </div> <xui-textbox rows=4 caption=\"_t(Comment for requester:)\" _ta ng-model=vm.dialogOptions.viewModel.RequestDetails.AdminComment> </xui-textbox> </form> </div> </xui-dialog>";

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-ipam-subnet-selector> <xui-filtered-list-v2 state=vm.listState dispatcher=vm.listDispatcher remote-control=vm.listControl controller=vm></xui-filtered-list-v2> </div> ";

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<div class=row> <div class=\"subnet-status-icon col-md-1\"> <xui-icon icon=subnet status={{item.StatusIconPostfix}} css-class=icon-test-style></xui-icon> </div> <div class=col-md-7> <div class=\"xui-text-l ipaddressrequests-widget-friendlyNameDiv\">{{item.FriendlyName}}</div> <div class=xui-text-s>{{item.Address}}/{{item.Cidr}}</div> </div> <div class=col-md-2> <div class=xui-text-l>{{item.AvailableCount ? item.AvailableCount : 0}}</div> <div class=xui-text-s _t>Available IPs</div> </div> <div class=col-md-2> <xui-button ng-click=vm.onSelectClick(item.GroupId) display-style=primary _t> Select </xui-button> </div> </div> ";

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <sw-ipam-subnet-selector ip-count=vm.dialogOptions.viewModel.IpCount selected-group-id=vm.dialogOptions.viewModel.GroupId /> </xui-dialog> ";

/***/ }),
/* 28 */,
/* 29 */
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ }),
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SubnetSelectorController = /** @class */ (function () {
    /** @ngInject */
    SubnetSelectorController.$inject = ["getTextService", "xuiFilteredListService", "$templateCache", "$timeout", "swApi", "$httpParamSerializerJQLike"];
    function SubnetSelectorController(getTextService, xuiFilteredListService, $templateCache, $timeout, swApi, $httpParamSerializerJQLike) {
        var _this = this;
        this.getTextService = getTextService;
        this.xuiFilteredListService = xuiFilteredListService;
        this.$templateCache = $templateCache;
        this.$timeout = $timeout;
        this.swApi = swApi;
        this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
        this.getGroupsUrl = "ipam/ipRequests/groups";
        this.subnetId = null;
        this.pagination = {
            page: 1,
            pageSize: 8,
            total: 8
        };
        this.friendlyNameSortableColumn = {
            id: "FriendlyName",
            label: this.getTextService("Friendly Name")
        };
        this.sorting = {
            sortableColumns: [
                this.friendlyNameSortableColumn,
                {
                    id: "Address",
                    label: this.getTextService("Address")
                },
                {
                    id: "AvailableCount",
                    label: this.getTextService("Available Addresses")
                }
            ],
            sortBy: this.friendlyNameSortableColumn,
            direction: "asc"
        };
        this.propertiesToRequest = [
            "status",
            "childStatus",
            "caption",
            "displayName",
            "detailsUrl",
            "node.displayName",
            "node.detailsUrl",
            "serverName",
            "sysName",
            "vendor",
            "vendorIcon",
            "ipAddress",
            "instanceType",
            "machineType"
        ];
        this.getDataForFilter = function (url, data) {
            return _this.swApi.api(false).one(url + "?" + _this.$httpParamSerializerJQLike(data)).get();
        };
        this.formatOrderBy = function (sorting) {
            if (!sorting) {
                return "";
            }
            return "" + (sorting.direction.toLowerCase() === "asc" ? "+" : "-") + sorting.sortBy.id;
        };
    }
    SubnetSelectorController.prototype.$onInit = function () {
        this.createListState();
        this.createListDispatcher();
        this.$templateCache.put("ipam-ip-request-subnet-selector-row-template", __webpack_require__(26));
    };
    SubnetSelectorController.prototype.onSelectClick = function (groupId) {
        this.selectedGroupId = groupId;
        this.$timeout(0, false).then(function () {
            $(".modal-footer > button:visible").trigger("click");
        });
    };
    SubnetSelectorController.prototype.createListState = function () {
        this.listState = {
            sorting: this.sorting,
            pagination: this.pagination,
            options: {
                busyShowCancelButton: false,
                busyDisabled: false,
                busyMessage: this.getTextService("Loading..."),
                trackBy: "GroupId",
                selectionProperty: "GroupId",
                templateUrl: "ipam-ip-request-subnet-selector-row-template",
                allowSelectAllPages: false,
                rowPadding: "narrow",
                filterMode: "none",
                stripe: false,
                searchableColumns: ["FriendlyName", "Address"],
            }
        };
    };
    SubnetSelectorController.prototype.createListDispatcher = function () {
        var options = {
            dataSource: {
                getItems: this.createGetItemsSource()
            }
        };
        this.listDispatcher = this.xuiFilteredListService.getDispatcherInstance(this.xuiFilteredListService.getModel(this, "listState"), options);
    };
    SubnetSelectorController.prototype.createGetItemsSource = function () {
        var _this = this;
        return function (params) {
            return _this.getItemsSource.bind(_this, params)();
        };
    };
    SubnetSelectorController.prototype.getItemsSource = function (params) {
        return this.fetchData(params);
    };
    SubnetSelectorController.prototype.fetchData = function (params) {
        return this.getDataForFilter(this.getGroupsUrl, {
            "q": params.search,
            "skip": ((params.pagination.page - 1) * params.pagination.pageSize),
            "top": params.pagination.pageSize,
            "orderBy": this.formatOrderBy(params.sorting),
            "filterBy": [],
            "propertyToSelect": this.propertiesToRequest,
            "having": this.ipCount
        }).then(function (response) {
            return {
                items: response.Items.map(function (item) { return item; }),
                total: response.TotalCount
            };
        });
    };
    return SubnetSelectorController;
}());
exports.SubnetSelectorController = SubnetSelectorController;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var CustomPropertyEditorController = /** @class */ (function () {
    /** @ngInject */
    CustomPropertyEditorController.$inject = ["$templateCache"];
    function CustomPropertyEditorController($templateCache) {
        this.$templateCache = $templateCache;
        this.boolSource = ["True", "False"];
    }
    CustomPropertyEditorController.prototype.$onInit = function () {
        this.$templateCache.put("custom-property-text-template", __webpack_require__(16));
        this.$templateCache.put("custom-property-restricted-template", __webpack_require__(15));
        this.$templateCache.put("custom-property-integer-template", __webpack_require__(13));
        this.$templateCache.put("custom-property-real-template", __webpack_require__(14));
        this.$templateCache.put("custom-property-date-template", __webpack_require__(11));
        this.$templateCache.put("custom-property-bool-template", __webpack_require__(10));
    };
    CustomPropertyEditorController.prototype.getTemplate = function (item) {
        if (item.RestrictedValues && item.RestrictedValues.length) {
            return "custom-property-restricted-template";
        }
        switch (item.Type) {
            case "nvarchar":
                return "custom-property-text-template";
            case "int":
                return "custom-property-integer-template";
            case "real":
                return "custom-property-real-template";
            case "datetime":
                return "custom-property-date-template";
            case "bit":
                return "custom-property-bool-template";
            default:
                return "custom-property-text-template";
        }
    };
    return CustomPropertyEditorController;
}());
exports.CustomPropertyEditorController = CustomPropertyEditorController;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpAddressSelectorController = /** @class */ (function () {
    /** @ngInject */
    IpAddressSelectorController.$inject = ["getTextService"];
    function IpAddressSelectorController(getTextService) {
        this.getTextService = getTextService;
        this.displayValue = "IpAddress";
    }
    IpAddressSelectorController.prototype.$onInit = function () {
        this.placeholder = this.getTextService("Search for IP");
        this.defaultValue = this.decodeIpRequestAddressDetails(this.ngModel);
    };
    IpAddressSelectorController.prototype.decodeIpRequestAddressDetails = function (object) {
        return JSON.parse(JSON.stringify(object));
    };
    IpAddressSelectorController.prototype.onSearch = function (value) {
        var addressDetails = this.decodeIpRequestAddressDetails(value);
        if (this.validateIpRequestAddressDetails(addressDetails)) {
            this.onItemSelect()(addressDetails, this.defaultValue);
        }
    };
    ;
    IpAddressSelectorController.prototype.onChange = function (value) {
        var addressDetails = this.decodeIpRequestAddressDetails(value);
        if (this.validateIpRequestAddressDetails(addressDetails)) {
            this.defaultValue = addressDetails;
        }
    };
    ;
    IpAddressSelectorController.prototype.onBlur = function (value) {
        var addressDetails = this.decodeIpRequestAddressDetails(value);
        if (!addressDetails.hasOwnProperty(this.displayValue)) {
            this.ngModel = this.defaultValue;
        }
    };
    ;
    IpAddressSelectorController.prototype.validateIpRequestAddressDetails = function (object) {
        return object.hasOwnProperty(this.displayValue)
            && object.IpAddress.match(/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/);
    };
    return IpAddressSelectorController;
}());
exports.IpAddressSelectorController = IpAddressSelectorController;


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DateTimePickerController = /** @class */ (function () {
    function DateTimePickerController() {
    }
    /** @ngInject */
    DateTimePickerController.prototype.$onInit = function () {
        if (this.value) {
            this.chosenTime = moment(this.value).format("LT").toString();
        }
        this.timeSource = this.generateTimeItems();
    };
    DateTimePickerController.prototype.dateTimeChanged = function () {
        if (!this.value) {
            this.value = new Date();
        }
        if (this.chosenTime) {
            var dateStr = this.value.toDateString();
            var newDateStr = dateStr + " " + this.chosenTime;
            this.value = new Date(newDateStr);
        }
    };
    DateTimePickerController.prototype.generateTimeItems = function () {
        var time = moment().hour(0).startOf("hour");
        var initialDay = time.dayOfYear();
        var times = [];
        while (time.dayOfYear() === initialDay) {
            times.push(time.clone().format("LT").toString());
            time.add(30, "minute");
        }
        return times;
    };
    return DateTimePickerController;
}());
exports.DateTimePickerController = DateTimePickerController;


/***/ }),
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(111);


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ipAddressRequests_controller_1 = __webpack_require__(112);
var ipRequestWidgetService_1 = __webpack_require__(1);
var ipRequestService_1 = __webpack_require__(0);
var ipamErrorHandler_service_1 = __webpack_require__(9);
__webpack_require__(39);
var subnetSelector_directive_1 = __webpack_require__(120);
var subnetSelector_controller_1 = __webpack_require__(40);
var customPropertyEditor_directive_1 = __webpack_require__(121);
var customPropertyEditor_controller_1 = __webpack_require__(41);
__webpack_require__(122);
var ipAddressSelector_directive_1 = __webpack_require__(123);
var ipAddressSelector_controller_1 = __webpack_require__(42);
__webpack_require__(124);
var dateTimePicker_directive_1 = __webpack_require__(125);
var dateTimePicker_controller_1 = __webpack_require__(43);
exports.default = function (module) {
    module.controller("IpAddressRequestsController", ipAddressRequests_controller_1.IpAddressRequestsController);
};
Xui.module.service("ipRequestWidgetService", ipRequestWidgetService_1.IpRequestWidgetService);
Xui.module.service("ipRequestService", ipRequestService_1.IpRequestService);
Xui.module.service("ipamErrorHandlerService", ipamErrorHandler_service_1.IpamErrorHandlerService);
Xui.module.component("swIpamSubnetSelector", subnetSelector_directive_1.SubnetSelector);
Xui.module.controller("SubnetSelectorController", subnetSelector_controller_1.SubnetSelectorController);
Xui.module.component("swIpamCustomPropertyEditor", customPropertyEditor_directive_1.CustomPropertyEditor);
Xui.module.controller("CustomPropertyEditorController", customPropertyEditor_controller_1.CustomPropertyEditorController);
Xui.module.component("swIpamIpaddressSelector", ipAddressSelector_directive_1.IpAddressSelector);
Xui.module.controller("IpaddressSelectorController", ipAddressSelector_controller_1.IpAddressSelectorController);
Xui.module.component("swIpamDateTimePicker", dateTimePicker_directive_1.DateTimePicker);
Xui.module.controller("DateTimePickerController", dateTimePicker_controller_1.DateTimePickerController);


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(29);
var IpRequestAddressDetails_1 = __webpack_require__(113);
var ipRequestService_1 = __webpack_require__(0);
var ipRequestDetails_1 = __webpack_require__(114);
var filteredListHelper_1 = __webpack_require__(115);
__webpack_require__(39);
var ipRequestCustomProperty_1 = __webpack_require__(116);
var ipRequestAddressMandatoryInfo_1 = __webpack_require__(117);
var ipRequestProcessRequest_1 = __webpack_require__(118);
var dhcpReservationType_1 = __webpack_require__(119);
exports.WidgetSettingsName = "ipamIpAddressRequestsWidgetSettings";
var IpAddressRequestsController = /** @class */ (function () {
    /** @ngInject */
    IpAddressRequestsController.$inject = ["ipRequestService", "ipRequestWidgetService", "ipamErrorHandlerService", "$window", "$q", "$templateCache", "xuiFilteredListService", "xuiDialogService", "getTextService", "swDemoService", "toastService", "xuiSearchingService"];
    function IpAddressRequestsController(ipRequestService, ipRequestWidgetService, ipamErrorHandlerService, $window, $q, $templateCache, xuiFilteredListService, xuiDialogService, getTextService, swDemoService, toastService, xuiSearchingService) {
        var _this = this;
        this.ipRequestService = ipRequestService;
        this.ipRequestWidgetService = ipRequestWidgetService;
        this.ipamErrorHandlerService = ipamErrorHandlerService;
        this.$window = $window;
        this.$q = $q;
        this.$templateCache = $templateCache;
        this.xuiFilteredListService = xuiFilteredListService;
        this.xuiDialogService = xuiDialogService;
        this.getTextService = getTextService;
        this.swDemoService = swDemoService;
        this.toastService = toastService;
        this.xuiSearchingService = xuiSearchingService;
        this.ipAddresses = [];
        this.dhcpServers = [];
        this.handleOnSelect = function (newItem, defaultValue) {
            var ip = _.find(_this.allItems, function (item) { return item.IpAddress === defaultValue.IpAddress; });
            if (ip) {
                var indexAllItemsToRemove = _.findIndex(_this.allItems, function (ipItem) { return ipItem.IpAddress === defaultValue.IpAddress; });
                if (indexAllItemsToRemove > -1) {
                    _this.allItems.splice(indexAllItemsToRemove, 1);
                }
                _this.updateIdForNewItem(newItem, defaultValue);
                _this.getCustomPropertiesForNewIpAddress(newItem);
                _this.allItems.push(newItem);
                var indexIpAddressToRemove = _.findIndex(_this.ipAddresses, function (ipItem) { return ipItem.IpAddress === newItem.IpAddress; });
                if (indexIpAddressToRemove > -1) {
                    _this.ipAddresses.splice(indexIpAddressToRemove, 1);
                }
                _this.ipAddresses.push(defaultValue);
                _this.refreshListData();
            }
        };
        this.showNoneIfEmpty = function (value) {
            if (value && value.length > 0) {
                return value;
            }
            return _this.getTextService("(none)");
        };
        this.countCustomProperties = function (cpArray) {
            if (cpArray && cpArray.length > 0) {
                return cpArray.length;
            }
            return 0;
        };
        this.onLoad = function (config) {
            var opId;
            if (!config.settings.opid) {
                config.setOverlayType("dataNotApplicable");
                return;
            }
            else {
                opId = config.settings.opid;
            }
            _this.ipRequestWidgetService.extractAlertId(opId)
                .then(function (result) {
                _this.alertId = result;
                _this.listControl.refreshListData();
                _this.appendSearchDropdownToBody();
                _this.disableReload();
            })
                .catch(function () {
                config.setOverlayType("dataNotAvailable");
            });
        };
        this.submit = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return;
            }
            if (!_this.ipRequestDetails.HasValidLicense) {
                var title = _this.getTextService("Your IP Address Manager License is currently unavailable");
                var description = _this.getTextService("The IP Address Manager license has either expired or is unavailable for technical reasons. "
                    + "Update your license through <a href='/ui/licensing/license-manager' _t>License manager</a> "
                    + " or contact <a href='https://www.solarwinds.com/company/contact-us' _t>SolarWinds Customer Service</a>.");
                _this.toastService.warning(description, title, { positionClass: "toast-top-center", timeOut: 5000 });
                return;
            }
            var details = new ipRequestDetails_1.IpRequestDetails();
            details.AddressesDetails = _this.allItems;
            details.IpRequestId = _this.ipRequestId;
            details.GroupId = _this.ipRequestDetails.GroupId;
            var titleDescription = _this.getTextService("Process request");
            var dialogDescription = _this.getTextService("Submitting this request will approve/deny the requested IP address(es).");
            var ipRequestProcessRequest = new ipRequestProcessRequest_1.IPRequestProcessRequest();
            ipRequestProcessRequest.DhcpServers = _this.dhcpServers;
            ipRequestProcessRequest.RequestDetails = details;
            ipRequestProcessRequest.RequestDetails.DhcpServer = _this.dhcpServers[0];
            ipRequestProcessRequest.RequestDetails.DhcpReservationType = dhcpReservationType_1.DhcpReservationType.DhcpOnly;
            _this.showSubmitRequestDialog(titleDescription, dialogDescription, ipRequestProcessRequest, function (isReserveOnDhcp) { return _this.processRequest(isReserveOnDhcp, details); });
        };
        this.reject = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return;
            }
            var details = new ipRequestDetails_1.IpRequestDetails();
            details.IpRequestId = _this.ipRequestId;
            details.AddressesDetails = _this.allItems;
            details.GroupId = _this.ipRequestDetails.GroupId;
            var buttonDescription = _this.getTextService("Deny Request");
            var dialogDescription = _this.getTextService("Denying this request will remove the requested IP address(es).");
            var ipRequestProcessRequest = new ipRequestProcessRequest_1.IPRequestProcessRequest();
            ipRequestProcessRequest.RequestDetails = details;
            _this.showRejectRequestDialog(buttonDescription, buttonDescription, dialogDescription, ipRequestProcessRequest, function () { return _this.rejectRequest(details); });
        };
        this.sorting = {
            sortableColumns: [
                {
                    id: "IsAccepted",
                    label: this.getTextService("Status")
                },
                {
                    id: "IpAddress",
                    label: this.getTextService("IP")
                },
                {
                    id: "Hostname.Value",
                    label: this.getTextService("Hostname")
                },
                {
                    id: "MacAddress.Value",
                    label: this.getTextService("MAC")
                }
            ],
            sortBy: {
                id: "Id",
                label: this.getTextService("None")
            },
            direction: "asc"
        };
        this.pagination = {
            page: 1,
            pageSize: 5,
            total: 5
        };
    }
    Object.defineProperty(IpAddressRequestsController.prototype, "allItemsCount", {
        get: function () {
            return this.ipRequestDetails ? this.ipRequestDetails.IpCount : 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IpAddressRequestsController.prototype, "acceptedCount", {
        get: function () {
            return _.sumBy(this.allItems, function (item) { return Number(item.IsAccepted); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IpAddressRequestsController.prototype, "deniedCount", {
        get: function () {
            return _.sumBy(this.allItems, function (item) { return Number(!item.IsAccepted); });
        },
        enumerable: true,
        configurable: true
    });
    IpAddressRequestsController.prototype.getCpPopoverTemplate = function () {
        return this.$templateCache.get("ipam-ip-request-cp-popover-template");
    };
    IpAddressRequestsController.prototype.$onInit = function () {
        this.filteredListHelper = new filteredListHelper_1.FilteredListHelper(this.xuiSearchingService);
        this.createListState();
        this.createListDispatcher();
        this.$templateCache.put("ipam-ip-request-cp-popover-template", __webpack_require__(21));
        this.$templateCache.put("ipam-ip-request-header-template", __webpack_require__(22));
        this.$templateCache.put("ipam-ip-request-node-template", __webpack_require__(23));
        this.$templateCache.put("edit-ip-request-address-template", __webpack_require__(18));
        this.$templateCache.put("submit-ip-request-address-template", __webpack_require__(24));
        this.$templateCache.put("subnet-selector-dialog-template", __webpack_require__(27));
    };
    IpAddressRequestsController.prototype.showSubmitRequestDialog = function (title, message, ipRequestProcessRequest, callback) {
        var _this = this;
        return this.xuiDialogService.showModal({ templateUrl: "submit-ip-request-address-template" }, {
            title: title,
            message: message,
            hideCancel: true,
            buttons: [
                {
                    name: "cancel",
                    displayStyle: "tertiary",
                    text: this.getTextService("Cancel"),
                    actionText: this.getTextService("Please wait..."),
                    cssClass: "ipaddressrequests-widget-cancelButton",
                    action: function () {
                        _this.resetAdminComment();
                        return true;
                    }
                },
                {
                    isHidden: !this.hasDhcpServers(),
                    name: "submit",
                    displayStyle: "primary",
                    text: this.getTextService("Reserve on DHCP Server"),
                    actionText: this.getTextService("Please wait..."),
                    action: function (dialog) {
                        if (dialog.$valid) {
                            return callback(true);
                        }
                        angular.forEach(dialog.submitRequest.$error.required, function (field) {
                            field.$setDirty();
                        });
                        return false;
                    }
                },
                {
                    name: "submit",
                    displayStyle: "primary",
                    text: this.getTextService("Reserve IPAM only"),
                    actionText: this.getTextService("Please wait..."),
                    action: function () {
                        return callback(false);
                    }
                }
            ],
            viewModel: ipRequestProcessRequest
        });
    };
    IpAddressRequestsController.prototype.showRejectRequestDialog = function (btnText, title, message, ipRequestProcessRequest, callback) {
        var _this = this;
        return this.xuiDialogService.showModal({ templateUrl: "submit-ip-request-address-template" }, {
            title: title,
            message: message,
            hideCancel: true,
            buttons: [
                {
                    name: "cancel",
                    displayStyle: "tertiary",
                    text: this.getTextService("Cancel"),
                    actionText: this.getTextService("Please wait..."),
                    cssClass: "ipaddressrequests-widget-cancelButton",
                    action: function () {
                        _this.resetAdminComment();
                        return true;
                    }
                },
                {
                    name: "reject",
                    displayStyle: "primary",
                    text: btnText,
                    actionText: this.getTextService("Please wait..."),
                    action: function () {
                        return callback();
                    }
                }
            ],
            viewModel: ipRequestProcessRequest
        });
    };
    Object.defineProperty(IpAddressRequestsController.prototype, "submitButtonText", {
        get: function () {
            if (this.ipRequestDetails && this.ipRequestDetails.IsDisabled) {
                return this.getTextService("Request is processed");
            }
            return this.getTextService("Process request");
        },
        enumerable: true,
        configurable: true
    });
    IpAddressRequestsController.prototype.redirectToAlerts = function () {
        this.$window.location.href = "/orion/netperfmon/alerts.aspx";
    };
    IpAddressRequestsController.prototype.processRequest = function (isReserveOnDhcp, details) {
        var _this = this;
        var isInvalid = false;
        var exception = {
            data: "You must specify Hostname and MAC address for each IP in order to send reservation to DHCP server"
        };
        details.IsReserveOnDhcp = isReserveOnDhcp;
        if (isReserveOnDhcp) {
            angular.forEach(details.AddressesDetails, function (ipAddress) {
                if (ipAddress.IsAccepted
                    && (ipAddress.Hostname.Value === null || ipAddress.MacAddress.Value === null)) {
                    isInvalid = true;
                    _this.ipamErrorHandlerService.handle(exception);
                }
            });
        }
        if (isInvalid) {
            return this.$q.resolve(false);
        }
        var result = this.ipRequestService
            .updateIpRequestDetails(details)
            .then(function () {
            _this.redirectToAlerts();
            return true;
        })
            .catch(function (ex) {
            _this.ipamErrorHandlerService.handle(ex);
            return false;
        });
        return this.$q.resolve(result);
    };
    IpAddressRequestsController.prototype.rejectRequest = function (details) {
        var _this = this;
        details.AddressesDetails.forEach(function (ipAddressRequest) {
            ipAddressRequest.IsAccepted = false;
        });
        var result = this.ipRequestService
            .updateIpRequestDetails(details)
            .then(function () {
            _this.redirectToAlerts();
            return true;
        })
            .catch(function (ex) {
            _this.ipamErrorHandlerService.handle(ex);
            return false;
        });
        return this.$q.resolve(result);
    };
    IpAddressRequestsController.prototype.selectedSubnetChanged = function (subnetId) {
        var _this = this;
        this.listState.busy = true;
        if (subnetId !== 0) {
            this.isDisabledSubmitButton = false;
        }
        this.ipRequestService.getDhcpServersBySubnetId(subnetId)
            .then(function (output) {
            _this.dhcpServers = output;
        });
        var id = 0;
        this.ipRequestDetails.GroupId = subnetId;
        var getAllItemsPromise = this.$q.resolve();
        if (this.originalGroupId === subnetId) {
            this.allItems = this.ipRequestDetails.AddressesDetails;
        }
        else {
            getAllItemsPromise =
                this.ipRequestService.getAvailableIpsWithCustomProperties(subnetId, this.ipRequestDetails.IpCount)
                    .then(function (result) {
                    _this.allItems = result.map(function (address) {
                        var ipAddress = new IpRequestAddressDetails_1.IpRequestAddressDetails();
                        ipAddress.IpAddress = address.Address;
                        ipAddress.IpNodeId = address.IpNodeId;
                        ipAddress.IpRequestAddressId = _this.ipRequestId;
                        ipAddress.CustomProperties = address.CustomProperties;
                        ipAddress.IsAccepted = true;
                        ipAddress.Id = id++;
                        var loadedIpAddressDetails = _.find(_this.ipRequestDetails.AddressesDetails, function (item) { return item.Id === ipAddress.Id; });
                        if (loadedIpAddressDetails) {
                            ipAddress.MacAddress = loadedIpAddressDetails.MacAddress;
                            ipAddress.Hostname = loadedIpAddressDetails.Hostname;
                        }
                        else {
                            ipAddress.MacAddress = address.MacAddress;
                            ipAddress.Hostname = address.HostName;
                        }
                        return ipAddress;
                    });
                });
        }
        getAllItemsPromise.then(function () {
            _this.ipRequestService.getAvailableIpsForSubnet(subnetId)
                .then(function (result) {
                var ipAddresses = result.map(function (address) { return ({
                    IpAddress: address.Address,
                    Hostname: address.HostName,
                    MacAddress: address.MacAddress,
                    IpNodeId: address.IpNodeId,
                    IpRequestAddressId: _this.ipRequestId,
                    CustomProperties: address.CustomProperties,
                    IsAccepted: true,
                    IsEdited: false
                }); }).filter(function (ip) { return _this.allItems.map(function (x) { return x.IpAddress; }).indexOf(ip.IpAddress) === -1; });
                _this.ipAddresses = ipAddresses;
                _this.changeSearchControlVisibility(false);
                _this.listState.options.emptyData = _this.emptyData();
                _this.listState.busy = false;
                _this.listControl.refreshListData();
            });
        });
    };
    IpAddressRequestsController.prototype.editIpRequestAddress = function (details) {
        var _this = this;
        var original = new IpRequestAddressDetails_1.IpRequestAddressDetails();
        original.IpNodeId = details.IpNodeId;
        original.Hostname = new ipRequestAddressMandatoryInfo_1.IpRequestAddressMandatoryInfo();
        original.Hostname.IsMandatory = details.Hostname.IsMandatory;
        original.Hostname.Value = details.Hostname.Value;
        original.MacAddress = new ipRequestAddressMandatoryInfo_1.IpRequestAddressMandatoryInfo;
        original.MacAddress.Value = details.MacAddress.Value;
        original.MacAddress.IsMandatory = details.MacAddress.IsMandatory;
        original.CustomProperties = this.cloneCustomProperties(details);
        var title = this.getTextService("Edit request for") + " " + details.IpAddress;
        this.xuiDialogService.showModal({ size: "sm", templateUrl: "edit-ip-request-address-template" }, {
            title: title,
            hideCancel: true,
            buttons: [
                {
                    name: "cancel",
                    displayStyle: "tertiary",
                    text: this.getTextService("Cancel"),
                    actionText: this.getTextService("Please wait..."),
                    cssClass: "ipaddressrequests-widget-cancelButton",
                    action: function () {
                        _this.resetIpAddress(original);
                        details.NotEmptyCustomProperties
                            = _this.getNotEmptyCustomProperties(details.CustomProperties);
                        return true;
                    }
                },
                {
                    name: "submit",
                    displayStyle: "primary",
                    text: this.getTextService("Submit"),
                    actionText: this.getTextService("Please wait..."),
                    action: function (dialogDescription) {
                        details.NotEmptyCustomProperties
                            = _this.getNotEmptyCustomProperties(details.CustomProperties);
                        if (dialogDescription.$valid) {
                            details.IsEdited = true;
                            return true;
                        }
                        angular.forEach(dialogDescription.editAddress.$error.required, function (field) {
                            field.$setTouched();
                        });
                        return false;
                    },
                }
            ],
            viewModel: details
        });
    };
    IpAddressRequestsController.prototype.selectSubnet = function () {
        var _this = this;
        var model = {
            IpCount: this.ipRequestDetails.IpCount,
            GroupId: 0
        };
        this.xuiDialogService.showModal({
            size: "lg",
            templateUrl: "subnet-selector-dialog-template"
        }, {
            title: this.getTextService("Select subnet"),
            hideCancel: true,
            buttons: [
                {
                    name: "cancel",
                    displayStyle: "tertiary",
                    text: this.getTextService("Cancel"),
                    actionText: this.getTextService("Please wait..."),
                    action: function () {
                        return true;
                    }
                }
            ],
            viewModel: model
        }).then(function () {
            if (model.GroupId > 0) {
                _this.selectedSubnetChanged(model.GroupId);
                _this.getSubnetDetails(model.GroupId);
            }
        });
    };
    IpAddressRequestsController.prototype.refreshListData = function () {
        this.listControl.refreshListData();
        this.ipAddresses = this.ipAddresses.slice()
            .sort(function (a, b) {
            var aValue = a.IpAddress;
            var bValue = b.IpAddress;
            var num1 = Number(aValue.split(".").map(function (num) { return ("000" + num).slice(-3); }).join(""));
            var num2 = Number(bValue.split(".").map(function (num) { return ("000" + num).slice(-3); }).join(""));
            return num1 - num2;
        });
    };
    IpAddressRequestsController.prototype.getCustomPropertiesForNewIpAddress = function (newItem) {
        var _this = this;
        this.ipRequestService.getNodesCustomPropertiesValues(newItem.IpNodeId)
            .then(function (res) {
            newItem.CustomProperties.forEach(function (cp) {
                var changedCustomProperty = _.find(res, function (item) { return item.Name === cp.Name; });
                cp.Value = changedCustomProperty ? changedCustomProperty.Value : cp.Value;
            });
            newItem.NotEmptyCustomProperties
                = _this.getNotEmptyCustomProperties(newItem.CustomProperties);
        });
    };
    IpAddressRequestsController.prototype.disableReload = function () {
        $("[data-resource-loading-mode]").each(function () {
            $(this).attr("data-resource-loading-mode", "Disabled");
        });
    };
    IpAddressRequestsController.prototype.hasDhcpServers = function () {
        return this.dhcpServers && this.dhcpServers.length > 0;
    };
    IpAddressRequestsController.prototype.updateIdForNewItem = function (newItem, defaultValue) {
        newItem.Id = defaultValue.Id;
    };
    IpAddressRequestsController.prototype.changeSearchControlVisibility = function (value) {
        // this is hack to hide search field on filtered-list-v2
        var ipAddressRequestWidget = document.getElementById("ipamIpAddressRequestWidget");
        var elements = ipAddressRequestWidget.getElementsByClassName("xui-search__group input-group");
        var hiddenClassName = "hidden";
        angular.forEach(elements, function (elem) {
            if (value) {
                elem.classList.add(hiddenClassName);
            }
            else {
                elem.classList.remove(hiddenClassName);
            }
        });
    };
    IpAddressRequestsController.prototype.appendSearchDropdownToBody = function () {
        // this is hack to append search dropdown to body
        var element = document.querySelector("sw-ip-address-requests").parentElement;
        element.classList.add("ipaddressrequests-widget-search-append-to-body");
    };
    IpAddressRequestsController.prototype.resetAdminComment = function () {
        this.ipRequestDetails.AdminComment = "";
    };
    IpAddressRequestsController.prototype.resetIpAddress = function (details) {
        var item = _.find(this.allItems, function (i) { return i.IpNodeId === details.IpNodeId; });
        item.Hostname = details.Hostname;
        item.MacAddress = details.MacAddress;
        item.CustomProperties = details.CustomProperties;
    };
    IpAddressRequestsController.prototype.cloneCustomProperties = function (details) {
        var original = new Array();
        details.CustomProperties.forEach(function (element) {
            var cp = new ipRequestCustomProperty_1.IpRequestCustomProperty;
            cp.Name = element.Name;
            cp.Required = element.Required;
            cp.RestrictedValues = element.RestrictedValues;
            cp.Type = element.Type;
            cp.Value = element.Value;
            original.push(cp);
        });
        return original;
    };
    IpAddressRequestsController.prototype.createListState = function () {
        this.listState = {
            sorting: this.sorting,
            pagination: this.pagination,
            options: {
                headerTemplateUrl: "ipam-ip-request-header-template",
                busyShowCancelButton: false,
                busyDisabled: false,
                busyMessage: this.getTextService("Loading..."),
                trackBy: "ipAddress",
                templateUrl: "ipam-ip-request-node-template",
                allowSelectAllPages: false,
                rowPadding: "narrow",
                filterMode: "none",
                stripe: false,
                searchableColumns: ["IpAddress", "Hostname", "MacAddress"],
                emptyData: {
                    compactMode: false,
                    image: "ok-robot",
                    title: "",
                    description: "",
                    templateUrl: null,
                    viewModel: {}
                }
            }
        };
    };
    IpAddressRequestsController.prototype.emptyDataSelectSubnet = function () {
        return {
            compactMode: false,
            image: "ok-robot",
            title: this.getTextService("Select a subnet using the button above..."),
            description: "",
            templateUrl: null,
            viewModel: {}
        };
    };
    IpAddressRequestsController.prototype.emptyData = function () {
        return {
            compactMode: false,
            image: "no-data-to-show",
            title: "",
            description: "No data to show",
            templateUrl: null,
            viewModel: {}
        };
    };
    IpAddressRequestsController.prototype.createListDispatcher = function () {
        var options = {
            dataSource: {
                getItems: this.createGetItemsSource()
            }
        };
        this.listDispatcher = this.xuiFilteredListService.getDispatcherInstance(this.xuiFilteredListService.getModel(this, "listState"), options);
    };
    IpAddressRequestsController.prototype.createGetItemsSource = function () {
        var _this = this;
        return function (params) {
            return _this.getItemsSource.bind(_this, params)();
        };
    };
    IpAddressRequestsController.prototype.getItemsSource = function (params) {
        var _this = this;
        if (this.alertId == null) {
            return this.$q.resolve({ items: [], total: 0 });
        }
        if (this.allItems == null) {
            return this.fetchIpRequestDetails()
                .then(function () {
                return _this.filteredListHelper.applyLimitations(_this.allItems, params);
            });
        }
        return this.$q.resolve(this.filteredListHelper.applyLimitations(this.allItems, params));
    };
    IpAddressRequestsController.prototype.fetchIpRequestDetails = function () {
        var _this = this;
        return this.ipRequestService
            .getIpRequestId(this.alertId)
            .then(function (result) { _this.ipRequestId = result; })
            .then(function () { return _this.ipRequestService.getIpRequestDetails(_this.ipRequestId); })
            .then(function (result) {
            _this.originalGroupId = result.GroupId;
            var groupId = result.GroupId;
            _this.ipRequestDetails = result;
            _this.allItems = result.AddressesDetails;
            var id = 0;
            _this.allItems.forEach(function (ipAddressRequest) {
                ipAddressRequest.Id = id++;
                ipAddressRequest.NotEmptyCustomProperties
                    = _this.getNotEmptyCustomProperties(ipAddressRequest.CustomProperties);
            });
            if (result.IsDisabled) {
                _this.isDisabledSubmitButton = true;
                _this.disableSubnetSelector = true;
            }
            if (groupId === 0 || !result.IsSubnetExists) {
                _this.isDisabledSubmitButton = true;
                _this.clearIpList();
            }
            else {
                _this.ipRequestService.getDhcpServersBySubnetId(groupId)
                    .then(function (output) {
                    _this.dhcpServers = output;
                });
                _this.ipRequestService.getAvailableIpsForSubnet(groupId)
                    .then(function (res) {
                    var ipAddresses = res.map(function (address) { return ({
                        IpAddress: address.Address,
                        Hostname: address.HostName,
                        MacAddress: address.MacAddress,
                        IpNodeId: address.IpNodeId,
                        IpRequestAddressId: _this.ipRequestId,
                        CustomProperties: address.CustomProperties,
                        IsAccepted: true,
                        IsEdited: false
                    }); }).filter(function (ip) { return _this.allItems.map(function (x) { return x.IpAddress; }).indexOf(ip.IpAddress) === -1; });
                    _this.ipAddresses = ipAddresses;
                });
                _this.getSubnetDetails(groupId);
            }
            if (_this.allItems && _this.allItems.length === 0) {
                _this.changeSearchControlVisibility(true);
                _this.listState.options.emptyData = _this.emptyDataSelectSubnet();
            }
            else {
                _this.listState.options.emptyData = _this.emptyData();
            }
        });
    };
    IpAddressRequestsController.prototype.getNotEmptyCustomProperties = function (customProperties) {
        return customProperties.filter(function (cp) { return (cp.Value !== null && cp.Value.toString().length > 0); });
    };
    IpAddressRequestsController.prototype.getSubnetDetails = function (groupId) {
        var _this = this;
        this.ipRequestService.getSubnetDetails(groupId)
            .then(function (res) {
            _this.subnetDetails = res;
        });
    };
    IpAddressRequestsController.prototype.clearIpList = function () {
        this.allItems = [];
    };
    IpAddressRequestsController = __decorate([
        widgets_1.Widget({
            selector: "swIpAddressRequests",
            template: __webpack_require__(19),
            controllerAs: "vm",
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [ipRequestService_1.IpRequestService, Object, Object, Object, Function, Object, Object, Object, Function, Object, Object, Object])
    ], IpAddressRequestsController);
    return IpAddressRequestsController;
}());
exports.IpAddressRequestsController = IpAddressRequestsController;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestAddressDetails = /** @class */ (function () {
    function IpRequestAddressDetails() {
    }
    return IpRequestAddressDetails;
}());
exports.IpRequestAddressDetails = IpRequestAddressDetails;


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestDetails = /** @class */ (function () {
    function IpRequestDetails() {
    }
    return IpRequestDetails;
}());
exports.IpRequestDetails = IpRequestDetails;


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilteredListHelper = /** @class */ (function () {
    function FilteredListHelper(xuiSearchingService) {
        this.xuiSearchingService = xuiSearchingService;
    }
    FilteredListHelper.prototype.applyLimitations = function (items, params) {
        var afterSearch = this.applySearch(items, params.search);
        var afterSorting = this.applySorting(afterSearch, params.sorting);
        var afterPagination = this.applyPagination(afterSorting, params.pagination);
        return {
            items: afterPagination,
            total: afterSearch.length
        };
    };
    FilteredListHelper.prototype.applySorting = function (items, sorting) {
        if (sorting.sortBy.id === "IpAddress") {
            //https://stackoverflow.com/questions/48618635/require-sorting-on-ip-address-using-js
            var sorted = items.slice()
                .sort(function (a, b) {
                var aValue = a.IpAddress;
                var bValue = b.IpAddress;
                var num1 = Number(aValue.split(".").map(function (num) { return ("000" + num).slice(-3); }).join(""));
                var num2 = Number(bValue.split(".").map(function (num) { return ("000" + num).slice(-3); }).join(""));
                return num1 - num2;
            });
            return sorting.direction === "desc" ? sorted.reverse() : sorted;
        }
        return _.orderBy(items, [sorting.sortBy.id], [sorting.direction]);
    };
    FilteredListHelper.prototype.applyPagination = function (items, pagination) {
        var start = (pagination.page - 1) * pagination.pageSize;
        var end = pagination.page * pagination.pageSize;
        return _.slice(items, start, end);
    };
    FilteredListHelper.prototype.applySearch = function (items, search) {
        if (!search) {
            return items;
        }
        var searchItems = items.map(function (item) {
            return { IpAddress: item.IpAddress, Hostname: item.Hostname.Value, MacAddress: item.MacAddress.Value };
        });
        var foundItems = this.xuiSearchingService.search(searchItems, ["Hostname", "IpAddress", "MacAddress"], search);
        return items.filter(function (i) { return _.find(foundItems, function (f) { return f.Hostname === i.Hostname.Value && f.MacAddress === i.MacAddress.Value && f.IpAddress === i.IpAddress; }); });
    };
    return FilteredListHelper;
}());
exports.FilteredListHelper = FilteredListHelper;


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestCustomProperty = /** @class */ (function () {
    function IpRequestCustomProperty() {
    }
    return IpRequestCustomProperty;
}());
exports.IpRequestCustomProperty = IpRequestCustomProperty;


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IpRequestAddressMandatoryInfo = /** @class */ (function () {
    function IpRequestAddressMandatoryInfo() {
    }
    return IpRequestAddressMandatoryInfo;
}());
exports.IpRequestAddressMandatoryInfo = IpRequestAddressMandatoryInfo;


/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IPRequestProcessRequest = /** @class */ (function () {
    function IPRequestProcessRequest() {
    }
    return IPRequestProcessRequest;
}());
exports.IPRequestProcessRequest = IPRequestProcessRequest;


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DhcpReservationType;
(function (DhcpReservationType) {
    DhcpReservationType[DhcpReservationType["Unknown"] = 0] = "Unknown";
    DhcpReservationType[DhcpReservationType["DhcpOnly"] = 1] = "DhcpOnly";
    DhcpReservationType[DhcpReservationType["BootpOnly"] = 2] = "BootpOnly";
    DhcpReservationType[DhcpReservationType["Both"] = 3] = "Both";
})(DhcpReservationType = exports.DhcpReservationType || (exports.DhcpReservationType = {}));


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var subnetSelector_controller_1 = __webpack_require__(40);
var SubnetSelector = /** @class */ (function () {
    function SubnetSelector() {
        this.restrict = "E";
        this.template = __webpack_require__(25);
        this.scope = {};
        this.bindToController = {
            ipCount: "=",
            selectedGroupId: "="
        };
        this.controller = subnetSelector_controller_1.SubnetSelectorController;
        this.controllerAs = "vm";
    }
    return SubnetSelector;
}());
exports.SubnetSelector = SubnetSelector;


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var customPropertyEditor_controller_1 = __webpack_require__(41);
var CustomPropertyEditor = /** @class */ (function () {
    function CustomPropertyEditor() {
        this.restrict = "E";
        this.template = __webpack_require__(12);
        this.scope = {};
        this.bindToController = {
            itemsSource: "=",
            title: "@"
        };
        this.controller = customPropertyEditor_controller_1.CustomPropertyEditorController;
        this.controllerAs = "vm";
    }
    return CustomPropertyEditor;
}());
exports.CustomPropertyEditor = CustomPropertyEditor;


/***/ }),
/* 122 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ipAddressSelector_controller_1 = __webpack_require__(42);
var IpAddressSelector = /** @class */ (function () {
    function IpAddressSelector() {
        this.require = ["^form", "ngModel"];
        this.restrict = "E";
        this.template = __webpack_require__(20);
        this.scope = {};
        this.bindToController = {
            ngModel: "=ngModel",
            ipAddresses: "=",
            isDisabled: "<?",
            onItemSelect: "&"
        };
        this.controller = ipAddressSelector_controller_1.IpAddressSelectorController;
        this.controllerAs = "vm";
    }
    return IpAddressSelector;
}());
exports.IpAddressSelector = IpAddressSelector;


/***/ }),
/* 124 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dateTimePicker_controller_1 = __webpack_require__(43);
var DateTimePicker = /** @class */ (function () {
    function DateTimePicker() {
        this.restrict = "E";
        this.template = __webpack_require__(17);
        this.scope = {};
        this.bindToController = {
            required: "=",
            value: "="
        };
        this.controller = dateTimePicker_controller_1.DateTimePickerController;
        this.controllerAs = "vm";
    }
    return DateTimePicker;
}());
exports.DateTimePicker = DateTimePicker;


/***/ })
/******/ ]);
//# sourceMappingURL=ipAddressRequests.js.map