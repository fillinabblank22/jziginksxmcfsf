/*!
 * @solarwinds/cli-frontend 7.12.4-788
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content id=pageID page-title=\"Page Title\"> <div class=cli-newPage-page> New page content </div> </xui-page-content> ";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<div class=cli-credentials-notification ng-show=vm.isDisplayed> <xui-message type=info> <span ng-transclude></span> </xui-message> </div>";

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(3);
module.exports = __webpack_require__(4);


/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(5);
var config_1 = __webpack_require__(6);
var index_1 = __webpack_require__(7);
var index_2 = __webpack_require__(9);
var index_3 = __webpack_require__(13);
var index_4 = __webpack_require__(17);
var templates_1 = __webpack_require__(19);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsaUNBQWlDOztBQUVqQzs7SUFFSTtBQUVKLG9DQUErQjtBQUMvQiwwQ0FBcUM7QUFDckMsd0NBQW9DO0FBQ3BDLHVDQUFrQztBQUNsQyw0Q0FBNEM7QUFDNUMsMENBQXdDO0FBQ3hDLGdEQUEyQztBQUUzQyxlQUFNLENBQUMsYUFBRyxDQUFDLENBQUM7QUFDWixnQkFBTSxDQUFDLGFBQUcsQ0FBQyxDQUFDO0FBQ1osZUFBSyxDQUFDLGFBQUcsQ0FBQyxDQUFDO0FBQ1gsZUFBVSxDQUFDLGFBQUcsQ0FBQyxDQUFDO0FBQ2hCLGVBQVEsQ0FBQyxhQUFHLENBQUMsQ0FBQztBQUNkLG1CQUFTLENBQUMsYUFBRyxDQUFDLENBQUMifQ==

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("cli.services", []);
angular.module("cli.templates", []);
angular.module("cli.components", []);
angular.module("cli.filters", []);
angular.module("cli.providers", []);
angular.module("cli", [
    "orion",
    "cli.services",
    "cli.templates",
    "cli.components",
    "cli.filters",
    "cli.providers"
]);
// create and register Xui (Orion) module wrapper
var cliClient = Xui.registerModule("cli");
exports.default = cliClient;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBRXBDLHlCQUF5QjtBQUN6QixPQUFPLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsQ0FBQztBQUNuQyxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxFQUFFLENBQUMsQ0FBQztBQUNwQyxPQUFPLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3JDLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ2xDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3BDLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFO0lBQ2xCLE9BQU87SUFDUCxjQUFjO0lBQ2QsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2IsZUFBZTtDQUNsQixDQUFDLENBQUM7QUFFSCxpREFBaUQ7QUFDakQsSUFBTSxTQUFTLEdBQUcsR0FBRyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUM1QyxrQkFBZSxTQUFTLENBQUMifQ==

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: cli");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBSXBDLGdCQUFnQjtBQUNoQixhQUFjLElBQW1CO0lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztBQUNqQyxDQUFDO0FBRUQsc0ZBQXNGO0FBQ3RGLDBCQUEwQjtBQUUxQixrQkFBZSxVQUFDLE1BQWM7SUFDMUIsTUFBTSxDQUFDLEdBQUcsRUFBRTtTQUNQLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNsQixDQUFDLENBQUMifQ==

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(8);
exports.default = function (module) {
    module.service("cliConstants", constants_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlDQUFvQztBQUVwQyxrQkFBZSxVQUFDLE1BQWM7SUFDMUIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsbUJBQVMsQ0FBQyxDQUFDO0FBQzlDLENBQUMsQ0FBQyJ9

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBRXBDO0lBQUE7SUFDQSxDQUFDO0lBQUQsZ0JBQUM7QUFBRCxDQUFDLEFBREQsSUFDQzs7QUFBQSxDQUFDIn0=

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(10);
exports.default = function (module) {
    // register views
    index_1.default(module);
};
var rootState = function ($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'cli/newPage' url
    $stateProvider.state("cli", {
        url: "/cli",
        controller: function ($state) {
            $state.go("home");
        }
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlDQUFzQztBQUV0QyxrQkFBZSxVQUFDLE1BQWM7SUFDNUIsaUJBQWlCO0lBQ2YsZUFBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3BCLENBQUMsQ0FBQztBQUVGLElBQU0sU0FBUyxHQUFHLFVBQUMsY0FBbUMsRUFBRSxrQkFBMkM7SUFDL0Ysc0VBQXNFO0lBQ3RFLGNBQWMsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1FBQ3hCLEdBQUcsRUFBRSxNQUFNO1FBQ1gsVUFBVSxFQUFFLFVBQUMsTUFBMEI7WUFDbkMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0QixDQUFDO0tBQ0osQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDIn0=

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var newPage_controller_1 = __webpack_require__(11);
var newPage_config_1 = __webpack_require__(12);
exports.default = function (module) {
    module.controller("NewPageController", newPage_controller_1.default);
    module.config(newPage_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJEQUFxRDtBQUNyRCxtREFBNkM7QUFFN0Msa0JBQWUsVUFBQyxNQUFjO0lBQzFCLE1BQU0sQ0FBQyxVQUFVLENBQUMsbUJBQW1CLEVBQUUsNEJBQWlCLENBQUMsQ0FBQztJQUMxRCxNQUFNLENBQUMsTUFBTSxDQUFDLHdCQUFhLENBQUMsQ0FBQztBQUNqQyxDQUFDLENBQUMifQ==

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NewPageController = /** @class */ (function () {
    function NewPageController() {
    }
    return NewPageController;
}());
exports.default = NewPageController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3UGFnZS1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibmV3UGFnZS1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDO0lBQUE7SUFFQSxDQUFDO0lBQUQsd0JBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQyJ9

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'cli/newPage' url
    $stateProvider.state("newPage", {
        url: "/cli/newPage",
        controller: "NewPageController",
        controllerAs: "vm",
        template: __webpack_require__(0)
    });
}
;
exports.default = config;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3UGFnZS1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuZXdQYWdlLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QyxnQkFBZ0I7QUFDaEIsZ0JBQWlCLGNBQW1DLEVBQUUsa0JBQTJDO0lBQzdGLHNFQUFzRTtJQUN0RSxjQUFjLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRTtRQUM1QixHQUFHLEVBQUUsY0FBYztRQUNuQixVQUFVLEVBQUUsbUJBQW1CO1FBQy9CLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQVMsZ0JBQWdCLENBQUM7S0FDOUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUFBLENBQUM7QUFFRixrQkFBZSxNQUFNLENBQUMifQ==

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var missingCredentialsNotification_1 = __webpack_require__(14);
exports.default = function (module) {
    missingCredentialsNotification_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1GQUE4RTtBQUU5RSxrQkFBZSxVQUFDLE1BQWM7SUFDNUIsd0NBQThCLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDekMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var missingCredentialsNotification_directive_1 = __webpack_require__(15);
exports.default = function (module) {
    module.component("cliMissingCredentialsNotification", missingCredentialsNotification_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLHVHQUFvRjtBQUVwRixrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxtQ0FBbUMsRUFBRSxrREFBMEIsQ0FBQyxDQUFDO0FBQ3RGLENBQUMsQ0FBQyJ9

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var missingCredentialsNotification_controller_1 = __webpack_require__(16);
var CliCredentialsNotification = /** @class */ (function () {
    function CliCredentialsNotification() {
        this.restrict = "E";
        this.transclude = true;
        this.replace = true;
        this.template = __webpack_require__(1);
        this.controller = missingCredentialsNotification_controller_1.default;
        this.controllerAs = "vm";
        this.scope = {};
        this.bindToController = {
            nodeId: "@"
        };
    }
    return CliCredentialsNotification;
}());
exports.default = CliCredentialsNotification;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWlzc2luZ0NyZWRlbnRpYWxzTm90aWZpY2F0aW9uLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1pc3NpbmdDcmVkZW50aWFsc05vdGlmaWNhdGlvbi1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSx5R0FBbUc7QUFFbkc7SUFBQTtRQUVXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixhQUFRLEdBQVcsT0FBTyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7UUFDcEUsZUFBVSxHQUFHLG1EQUF3QyxDQUFDO1FBQ3RELGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxxQkFBZ0IsR0FBRztZQUN0QixNQUFNLEVBQUUsR0FBRztTQUNkLENBQUM7SUFDTixDQUFDO0lBQUQsaUNBQUM7QUFBRCxDQUFDLEFBWkQsSUFZQyJ9

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var MissingCredentialsNotificationController = /** @class */ (function () {
    /** @ngInject */
    MissingCredentialsNotificationController.$inject = ["credentialsService"];
    function MissingCredentialsNotificationController(credentialsService) {
        this.credentialsService = credentialsService;
        this.isDisplayed = false;
    }
    MissingCredentialsNotificationController.prototype.$onInit = function () {
        var _this = this;
        this.credentialsService.hasCliCredentials(this.nodeId).then(function (hasCredentials) {
            _this.isDisplayed = !hasCredentials;
        });
    };
    return MissingCredentialsNotificationController;
}());
exports.default = MissingCredentialsNotificationController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWlzc2luZ0NyZWRlbnRpYWxzTm90aWZpY2F0aW9uLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtaXNzaW5nQ3JlZGVudGlhbHNOb3RpZmljYXRpb24tY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBO0lBSUksZ0JBQWdCO0lBQ2hCLGtEQUFvQixrQkFBdUM7UUFBdkMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQUhwRCxnQkFBVyxHQUFZLEtBQUssQ0FBQztJQUcyQixDQUFDO0lBRXpELDBEQUFPLEdBQWQ7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsY0FBYztZQUN0RSxLQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsY0FBYyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNMLCtDQUFDO0FBQUQsQ0FBQyxBQVpELElBWUMifQ==

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var credentials_service_1 = __webpack_require__(18);
exports.default = function (module) {
    module.service("credentialsService", credentials_service_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDZEQUF1RDtBQUV2RCxrQkFBZSxVQUFDLE1BQWM7SUFDMUIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSw2QkFBa0IsQ0FBQyxDQUFDO0FBQzdELENBQUMsQ0FBQyJ9

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiHasCliCredentialsUrlFragment = "cli/credentials/nodes/{nodeId}/existence";
var CredentialsService = /** @class */ (function () {
    /** @ngInject */
    CredentialsService.$inject = ["swApi"];
    function CredentialsService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.hasCliCredentials = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiHasCliCredentialsUrlFragment.replace("{nodeId}", nodeId.toString()))
                .get()
                .then(function (hasCredentials) {
                return hasCredentials;
            });
        };
    }
    return CredentialsService;
}());
exports.default = CredentialsService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlZGVudGlhbHMtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNyZWRlbnRpYWxzLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFJYSxRQUFBLGlDQUFpQyxHQUFHLDBDQUEwQyxDQUFDO0FBRTVGO0lBRUksZ0JBQWdCO0lBQ2hCLDRCQUNZLEtBQW9CO1FBRGhDLGlCQUVLO1FBRE8sVUFBSyxHQUFMLEtBQUssQ0FBZTtRQUd6QixzQkFBaUIsR0FBRyxVQUFDLE1BQWM7WUFDdEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxLQUFLO2lCQUNaLEdBQUcsQ0FBQyxLQUFLLENBQUM7aUJBQ1YsR0FBRyxDQUFDLHlDQUFpQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7aUJBQzdFLEdBQUcsRUFBRTtpQkFDTCxJQUFJLENBQUMsVUFBQSxjQUFjO2dCQUNoQixNQUFNLENBQUMsY0FBYyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFBO0lBVkcsQ0FBQztJQVdULHlCQUFDO0FBQUQsQ0FBQyxBQWhCRCxJQWdCQyJ9

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(20);
    req.keys().forEach(function (r) {
        var key = "cli" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidGVtcGxhdGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxrQ0FBa0M7O0FBSWxDOzs7O0dBSUc7QUFDSCxtQkFBbUIsY0FBdUM7SUFDdEQsSUFBTSxHQUFHLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLDJCQUEyQixDQUFDLENBQUM7SUFDckUsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQUs7UUFDckIsSUFBTSxHQUFHLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEMsSUFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLGNBQWMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xDLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUVELGtCQUFlLFVBQUMsTUFBYztJQUMxQixNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQ2hDLENBQUMsQ0FBQyJ9

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/missingCredentialsNotification/missingCredentialsNotification.html": 1,
	"./views/newPage/newPage.html": 0
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 20;

/***/ })
/******/ ]);
//# sourceMappingURL=cli.js.map