/*!
 * @solarwinds/ucs 2020.2.5-146
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="./ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(3);
var config_1 = __webpack_require__(4);
var index_1 = __webpack_require__(5);
var index_2 = __webpack_require__(7);
var templates_1 = __webpack_require__(8);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("ucs.templates", []);
angular.module("ucs.filters", []);
angular.module("ucs.providers", []);
angular.module("ucs", [
    "orion",
    "ucs.templates",
    "ucs.filters",
    "ucs.providers"
]);
// create and register Xui (Orion) module wrapper
var servicegroups = Xui.registerModule("ucs");
exports.default = servicegroups;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: ucs");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(6);
exports.default = function (module) {
    module.service("ucsConstants", constants_1.default);
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    // register views
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(9);
    req.keys().forEach(function (r) {
        var key = "ucs" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./rowTemplates/eventsWidgetRow.html": 10,
	"./rowTemplates/fansWidgetRow.html": 11,
	"./rowTemplates/psusWidgetRow.html": 12
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 9;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = "<div class=audit-container> <div> <b>{{item.name}}</b>| {{item.created | date: item.createdDateTimeFormat }} </div><span><i>{{item.description}}</i></span> </div> ";

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "<div style=display:flex;justify-content:space-between> <div class=pull-left style=margin-top:3px;min-width:32px> <xui-icon icon=fan icon-size=medium text-alignment=right></xui-icon> </div> <div class=pull-left> <div class=text_wrap> {{item.name}} </div> <div class=text_wrap> <span _t>on Fabric Module</span> {{item.module}} </div> </div> <div class=pull-right style=width:100%> <div class=\"pull-right text_wrap\" style=font-weight:700> {{item.status}} </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Status </div> </div> </div>";

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<div style=display:flex;justify-content:space-between> <div class=\"display: flex; justify-content: flex-start height:100%;\"> <div class=pull-left style=margin-top:3px;min-width:32px> <xui-icon icon=psu icon-size=medium text-alignment=right></xui-icon> </div> <div class=pull-left> <div class=text_wrap> {{item.name}} </div> </div> </div> <div class=pull-right> <div style=display:flex;justify-content:space-between> <div class=pull-right style=width:100%> <div class=\"pull-right text_wrap\" style=font-weight:700> {{item.power}} </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Power </div> </div> <div class=pull-right style=width:20px> </div> <div class=pull-right style=width:100%> <div class=\"pull-right text_wrap\" style=font-weight:700> {{item.status}} </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Status </div> </div> </div> </div> </div>";

/***/ })
/******/ ]);
//# sourceMappingURL=ucs.js.map