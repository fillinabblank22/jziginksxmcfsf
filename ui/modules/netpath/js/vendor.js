/*!
 * @solarwinds/charts 1.32.0-961
 * @copyright 2018 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(3);
var components_1 = __webpack_require__(4);
var config_1 = __webpack_require__(9);
var templates_1 = __webpack_require__(10);
templates_1.default(app_1.default);
config_1.default(app_1.default);
components_1.default(app_1.default);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsK0JBQStCOztBQUUvQixvQ0FBa0M7QUFDbEMsMkNBQXNDO0FBQ3RDLDBDQUFxQztBQUNyQyxnREFBMkM7QUFFM0MsbUJBQVMsQ0FBQyxhQUFNLENBQUMsQ0FBQztBQUNsQixnQkFBTSxDQUFDLGFBQU0sQ0FBQyxDQUFDO0FBQ2Ysb0JBQVUsQ0FBQyxhQUFNLENBQUMsQ0FBQyJ9

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
angular.module("sw-charts.services", []);
angular.module("sw-charts.components", []);
angular.module("sw-charts.filters", []);
angular.module("sw-charts.providers", []);
angular.module("sw-charts", [
    "xui",
    "sw-charts.services",
    "sw-charts.components",
    "sw-charts.filters",
    "sw-charts.providers"
]);
exports.default = Xui.registerModule("sw-charts");
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxDQUFDLE1BQU0sQ0FDVixvQkFBb0IsRUFDcEIsRUFBRSxDQUFDLENBQUM7QUFFUixPQUFPLENBQUMsTUFBTSxDQUNWLHNCQUFzQixFQUN0QixFQUFFLENBQUMsQ0FBQztBQUVSLE9BQU8sQ0FBQyxNQUFNLENBQ1YsbUJBQW1CLEVBQ25CLEVBQUUsQ0FBQyxDQUFDO0FBRVIsT0FBTyxDQUFDLE1BQU0sQ0FDVixxQkFBcUIsRUFDckIsRUFBRSxDQUFDLENBQUM7QUFFUixPQUFPLENBQUMsTUFBTSxDQUNWLFdBQVcsRUFDWDtJQUNJLEtBQUs7SUFDTCxvQkFBb0I7SUFDcEIsc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixxQkFBcUI7Q0FDeEIsQ0FBQyxDQUFDO0FBRVAsa0JBQWUsR0FBRyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQyJ9

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var chart_1 = __webpack_require__(5);
exports.default = function (module) {
    chart_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlDQUE0QjtBQUU1QixrQkFBZSxVQUFDLE1BQWU7SUFDM0IsZUFBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ2xCLENBQUMsQ0FBQyJ9

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var chart_component_1 = __webpack_require__(6);
var chart_controller_1 = __webpack_require__(8);
exports.default = function (module) {
    module.controller("swChartController", chart_controller_1.default);
    module.component("swChart", chart_component_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFEQUFzQztBQUN0Qyx1REFBaUQ7QUFFakQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsbUJBQW1CLEVBQUUsMEJBQWUsQ0FBQyxDQUFDO0lBQ3hELE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLHlCQUFLLENBQUMsQ0FBQztBQUN2QyxDQUFDLENBQUMifQ==

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(7);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @ngdoc directive
 * @name sw-charts.directive:swChart
 * @restrict E
 *
 * @description
 * Chart.
 *
 * @parameters
 *
 * @example
 *    <example module="sw-charts">
 *        <file name="index.html">
 *            <link rel="stylesheet" type="text/css" href="_vendor-css-url_">
 *            <link rel="stylesheet" type="text/css" href="_xui-css-url_">
 *            <base href="/">
 *            <div class="sw-charts" ng-controller="ExampleController as ctrl">
 *               <sw-chart type="bar" config="vm.config"></sw-chart>
 *            </div>
 *        </file>
 *        <file name="app.js">
 *            SW.Charts.controller('ExampleController', function() {
 *                this.vm = {
 *                   config: {
 *                       xAxis: {
 *                           categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
 *                                        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
 *                       },
 *                       yAxis: {
 *                           title: {
 *                               text: 'Temperature (°C)'
 *                           },
 *                           plotLines: [{
 *                               value: 0,
 *                               width: 1,
 *                               color: '#808080'
 *                           }]
 *                       },
 *                       tooltip: {
 *                           valueSuffix: '°C'
 *                       },
 *                       legend: {
 *                           layout: 'vertical',
 *                           align: 'right',
 *                           verticalAlign: 'middle',
 *                           borderWidth: 0
 *                       },
 *                       series: [{
 *                           name: 'Tokyo',
 *                           data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
 *                       }, {
 *                           name: 'New York',
 *                           data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
 *                       }, {
 *                           name: 'Berlin',
 *                           data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
 *                       }, {
 *                           name: 'London',
 *                           data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
 *                       }]
 *                   }
 *                };
 *            });
 *        </file>
 *    </example>
 */
var Chart = /** @class */ (function () {
    function Chart(swUtil) {
        var _this = this;
        this.swUtil = swUtil;
        this.restrict = "E";
        this.templateUrl = "charts/components/chart/chart-component.html";
        this.replace = true;
        this.transclude = false;
        this.scope = {};
        this.bindToController = {
            name: "@?",
            type: "@?",
            title: "@?",
            subtitle: "@?",
            config: "="
        };
        this.controller = "swChartController";
        this.controllerAs = "vm";
        this.compile = function (element, attrs) {
            var global = Highcharts.getOptions();
            global.lang.noData = "nothing to show";
            return { post: _this.postLink };
        };
        this.postLink = function (scope, element, attrs, ctrl) {
            var chart;
            var cleanup = function () {
                if (chart) {
                    chart.destroy();
                    chart = null;
                }
            };
            //name
            _this.swUtil.initComponent(attrs, "chart");
            // Highcarts needs help adjusting it's size.
            scope.$watch(function () {
                return element[0].clientWidth;
            }, function () {
                if (chart) {
                    chart.reflow();
                }
            });
            scope.$watch("vm.config", function (newConfig, oldConfig) {
                if (!newConfig) {
                    return;
                }
                var chartConfig = _.assign({
                    chart: {
                        type: ctrl.type || "bar"
                    },
                    tooltip: {
                        borderRadius: 0,
                        backgroundColor: "#FFFFFF"
                    },
                    title: {
                        text: ctrl.title
                    },
                    subtitle: {
                        text: ctrl.subtitle
                    },
                    credits: {
                        enabled: false
                    },
                    noData: {
                        style: {
                            fontSize: "30px",
                            color: "#E5E5E5"
                        }
                    }
                }, newConfig);
                cleanup();
                element.highcharts(chartConfig, function (c) {
                    chart = c;
                });
                scope.$on("$destroy", cleanup);
            });
        };
    }
    Chart.$inject = ["swUtil"];
    return Chart;
}());
exports.default = Chart;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhcnQtY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2hhcnQtY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBaUVHO0FBQ0g7SUFrQkksZUFBb0IsTUFBVztRQUEvQixpQkFDQztRQURtQixXQUFNLEdBQU4sTUFBTSxDQUFLO1FBZnhCLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixnQkFBVyxHQUFHLDhDQUE4QyxDQUFDO1FBQzdELFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxxQkFBZ0IsR0FBRztZQUN0QixJQUFJLEVBQUUsSUFBSTtZQUNWLElBQUksRUFBRSxJQUFJO1lBQ1YsS0FBSyxFQUFFLElBQUk7WUFDWCxRQUFRLEVBQUUsSUFBSTtZQUNkLE1BQU0sRUFBRSxHQUFHO1NBQ2QsQ0FBQztRQUNLLGVBQVUsR0FBRyxtQkFBbUIsQ0FBQztRQUNqQyxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUtwQixZQUFPLEdBQUcsVUFBQyxPQUE0QixFQUFFLEtBQXFCO1lBQ2pFLElBQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxpQkFBaUIsQ0FBQztZQUV2QyxNQUFNLENBQUMsRUFBQyxJQUFJLEVBQUMsS0FBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQztRQUVNLGFBQVEsR0FBRyxVQUFDLEtBQWdCLEVBQUUsT0FBNEIsRUFBRSxLQUFxQixFQUNyRSxJQUFxQjtZQUNyQyxJQUFJLEtBQTRCLENBQUM7WUFDakMsSUFBTSxPQUFPLEdBQUc7Z0JBQ1osRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDUixLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ2hCLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQ2pCLENBQUM7WUFDTCxDQUFDLENBQUM7WUFFRixNQUFNO1lBQ04sS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRTFDLDRDQUE0QztZQUM1QyxLQUFLLENBQUMsTUFBTSxDQUFDO2dCQUNMLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQ2xDLENBQUMsRUFBRTtnQkFDQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNSLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQztZQUNMLENBQUMsQ0FDSixDQUFDO1lBRUYsS0FBSyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsVUFBQyxTQUFjLEVBQUUsU0FBYztnQkFDckQsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNiLE1BQU0sQ0FBQztnQkFDWCxDQUFDO2dCQUVELElBQU0sV0FBVyxHQUFRLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBQzlCLEtBQUssRUFBRTt3QkFDSCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksSUFBSSxLQUFLO3FCQUMzQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ0wsWUFBWSxFQUFFLENBQUM7d0JBQ2YsZUFBZSxFQUFFLFNBQVM7cUJBQzdCO29CQUNELEtBQUssRUFBRTt3QkFDSCxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUs7cUJBQ25CO29CQUNELFFBQVEsRUFBRTt3QkFDTixJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVE7cUJBQ3RCO29CQUNELE9BQU8sRUFBRTt3QkFDTCxPQUFPLEVBQUUsS0FBSztxQkFDakI7b0JBQ0QsTUFBTSxFQUFFO3dCQUNKLEtBQUssRUFBRTs0QkFDSCxRQUFRLEVBQUUsTUFBTTs0QkFDaEIsS0FBSyxFQUFFLFNBQVM7eUJBQ25CO3FCQUNKO2lCQUNKLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBRWQsT0FBTyxFQUFFLENBQUM7Z0JBRVYsT0FBTyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsVUFBQyxDQUF3QjtvQkFDckQsS0FBSyxHQUFHLENBQUMsQ0FBQztnQkFDZCxDQUFDLENBQUMsQ0FBQztnQkFFSCxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNuQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztJQXRFRixDQUFDO0lBbEJhLGFBQU8sR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBeUZ2QyxZQUFDO0NBQUEsQUExRkQsSUEwRkM7QUFFRCxrQkFBZSxLQUFLLENBQUMifQ==

/***/ }),
/* 7 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ChartController = /** @class */ (function () {
    function ChartController() {
    }
    return ChartController;
}());
exports.default = ChartController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhcnQtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNoYXJ0LWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQUFBO0lBTUEsQ0FBQztJQUFELHNCQUFDO0FBQUQsQ0FBQyxBQU5ELElBTUM7QUFFRCxrQkFBZSxlQUFlLENBQUMifQ==

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("module-run: sw-charts");
}
exports.default = function (module) {
    return module.app()
        .constant("swChartConstants", {})
        .run(run);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsZ0JBQWdCO0FBQ2hCLGFBQWEsSUFBb0I7SUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO0FBQ3ZDLENBQUM7QUFFRCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUU7U0FDZCxRQUFRLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxDQUFDO1NBQ2hDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNsQixDQUFDLENBQUMifQ==

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(11);
    req.keys().forEach(function (r) {
        var key = "charts" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidGVtcGxhdGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7Ozs7R0FJRztBQUNILG1CQUFtQixjQUF3QztJQUN2RCxJQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztJQUNyRSxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBTTtRQUN0QixJQUFNLEdBQUcsR0FBRyxRQUFRLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuQyxJQUFNLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEIsY0FBYyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDO0FBRUQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDaEMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/chart/chart-component.html": 12
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 11;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-chart> </div> ";

/***/ })
/******/ ]);
//# sourceMappingURL=charts.js.map