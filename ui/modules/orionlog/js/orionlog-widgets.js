/*!
 * solarwinds-orionlog 2.5.0
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(2);
__webpack_require__(3);
var ologDateTimeFilter_1 = __webpack_require__(4);
angular.module("orionlog.filters", []);
angular.module("orionlog", [
    "orionlog.filters"
]);
var orionlog = Xui.registerModule("orionlog");
orionlog.filter("ologDateTime", ologDateTimeFilter_1.default);
exports.default = orionlog;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


ologDateTimeFilter.$inject = ["$filter"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function ologDateTimeFilter($filter) {
    return function (input, format) {
        switch (format) {
            case "shortDate":
                return moment(input).format("l");
            case "shortDateTime":
                return moment(input).format("l LT");
            case "shortDateTimeWithSeconds":
                return moment(input).format("l LTS");
            case "shortDateTimeWithMs":
                return moment(input).format("l h:mm:ss.SSS A");
            case "mediumDateTime":
                return moment(input).format("ll LT");
            case "mediumDateTimeWithSeconds":
                return moment(input).format("ll LTS");
            case "mediumDateTimeWithMs":
                return moment(input).format("ll h:mm:ss.SSS A");
            case "12HourTime":
                return moment(input).format("LT");
            case "12HourTimeWithSeconds":
                return moment(input).format("LTS");
            case "12HourTimeWithMs":
                return moment(input).format("h:mm:ss.SSS A");
            default:
                return moment(input).format("l LT");
        }
    };
}
exports.default = ologDateTimeFilter;


/***/ })
/******/ ]);
//# sourceMappingURL=orionlog-widgets.js.map