/*!
 * @solarwinds/hardwarehealth 2020.2.5-113
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<div class=hwh-hardware-details-widget> <div class=hwh-hardware-details-widget__header ng-if=ctrl.info> <div class=\"hwh-hardware-details-widget__header__icon xui-margin-md\"> <xui-icon icon-size=large icon={{::ctrl.info.icon}} status={{::ctrl.info.status}} /> </div> <div class=hwh-hardware-details-widget__header__name> <div class=xui-text-bg><b>{{ctrl.info.name}}</b></div> <div ng-repeat=\"ancestor in ctrl.info.ancestorInfo\"> <span _t>on</span> <a href={{ancestor.url}}>{{ancestor.displayName}}</a> </div> </div> </div> <xui-listview class=hwh-hardware-details-widget__list items-source=ctrl.info.rows template-url=hardware-details-row-template row-padding=compact show-empty=false></xui-listview> <div class=hwh-hardware-details-widget__busy ng-if=ctrl.config.widget.isBusy></div> </div> ";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<div class=hwh-hardware-details-widget__list__row> <div class=hwh-hardware-details-widget__list__row__left-column> <b>{{item.name}}</b> </div> <div class=hwh-hardware-details-widget__list__row__right-column> <span class=xui-margin-smr ng-if=item.vendorIcon> <sw-vendor-icon size=small vendor={{::item.vendorIcon}}></sw-vendor-icon> </span> <span ng-if=item.url> <a href={{::item.url}}>{{::item | hardwareDetailsValueFilter}}</a> </span> <span ng-if=!item.url> {{::item | hardwareDetailsValueFilter}} </span> </div> </div> ";

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(5);
module.exports = __webpack_require__(6);


/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="./ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(7);
var config_1 = __webpack_require__(8);
var index_1 = __webpack_require__(9);
var index_2 = __webpack_require__(11);
var templates_1 = __webpack_require__(12);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("hardwarehealth.services", []);
angular.module("hardwarehealth.templates", []);
angular.module("hardwarehealth.components", []);
angular.module("hardwarehealth.filters", []);
angular.module("hardwarehealth.providers", []);
angular.module("hardwarehealth", [
    "orion",
    "hardwarehealth.services",
    "hardwarehealth.templates",
    "hardwarehealth.components",
    "hardwarehealth.filters",
    "hardwarehealth.providers"
]);
// create and register Xui (Orion) module wrapper
var servicegroups = Xui.registerModule("hardwarehealth");
exports.default = servicegroups;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: hardwarehealth");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(10);
exports.default = function (module) {
    module.service("hardwarehealthConstants", constants_1.default);
};


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    // register views
};
var rootState = function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state("hardwarehealth", {
        url: "/hardwarehealth",
        controller: function ($state) {
            $state.go("dashboard");
        }
    });
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(13);
    req.keys().forEach(function (r) {
        var key = "hardwarehealth" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./widgets/hardwareDetailsWidget/hardwareDetailsWidget-directive.html": 0,
	"./widgets/hardwareDetailsWidget/hardwareDetailsWidget-row.html": 1
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 13;

/***/ })
/******/ ]);
//# sourceMappingURL=hardwarehealth.js.map