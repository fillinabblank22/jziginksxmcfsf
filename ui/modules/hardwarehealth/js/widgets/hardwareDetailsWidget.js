/*!
 * @solarwinds/hardwarehealth 2020.2.5-113
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 14);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<div class=hwh-hardware-details-widget> <div class=hwh-hardware-details-widget__header ng-if=ctrl.info> <div class=\"hwh-hardware-details-widget__header__icon xui-margin-md\"> <xui-icon icon-size=large icon={{::ctrl.info.icon}} status={{::ctrl.info.status}} /> </div> <div class=hwh-hardware-details-widget__header__name> <div class=xui-text-bg><b>{{ctrl.info.name}}</b></div> <div ng-repeat=\"ancestor in ctrl.info.ancestorInfo\"> <span _t>on</span> <a href={{ancestor.url}}>{{ancestor.displayName}}</a> </div> </div> </div> <xui-listview class=hwh-hardware-details-widget__list items-source=ctrl.info.rows template-url=hardware-details-row-template row-padding=compact show-empty=false></xui-listview> <div class=hwh-hardware-details-widget__busy ng-if=ctrl.config.widget.isBusy></div> </div> ";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<div class=hwh-hardware-details-widget__list__row> <div class=hwh-hardware-details-widget__list__row__left-column> <b>{{item.name}}</b> </div> <div class=hwh-hardware-details-widget__list__row__right-column> <span class=xui-margin-smr ng-if=item.vendorIcon> <sw-vendor-icon size=small vendor={{::item.vendorIcon}}></sw-vendor-icon> </span> <span ng-if=item.url> <a href={{::item.url}}>{{::item | hardwareDetailsValueFilter}}</a> </span> <span ng-if=!item.url> {{::item | hardwareDetailsValueFilter}} </span> </div> </div> ";

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetService = /** @class */ (function () {
    function WidgetService() {
    }
    WidgetService.prototype.hideParentWidget = function (instanceElement) {
        // temp hack until Apollo provides solution
        if (!instanceElement) {
            return false;
        }
        var wrapper = instanceElement.closest(".ResourceWrapper");
        if (wrapper.length === 0) {
            return false;
        }
        wrapper.addClass("HiddenResourceWrapper");
        return true;
    };
    return WidgetService;
}());
exports.WidgetService = WidgetService;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var HardwareDetailsService = /** @class */ (function () {
    /** @ngInject */
    HardwareDetailsService.$inject = ["swApi"];
    function HardwareDetailsService(swApi) {
        this.swApi = swApi;
    }
    HardwareDetailsService.prototype.getInfo = function (endpoint, opid) {
        return this.swApi.api(false)
            .one(endpoint + "?opid=" + opid)
            .get();
    };
    return HardwareDetailsService;
}());
exports.HardwareDetailsService = HardwareDetailsService;


/***/ }),
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(15);


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(16);
var WidgetService_1 = __webpack_require__(2);
var hardwareDetailsWidget_controller_1 = __webpack_require__(17);
var HardwareDetailsService_1 = __webpack_require__(3);
var hardwareDetailsValueFilter_1 = __webpack_require__(19);
var DateTimeService_1 = __webpack_require__(21);
angular.module("widgets")
    .service("widgetService", WidgetService_1.WidgetService)
    .service("hardwareDetailsService", HardwareDetailsService_1.HardwareDetailsService)
    .service("dateTimeService", DateTimeService_1.DateTimeService)
    .filter("hardwareDetailsValueFilter", ["dateTimeService", hardwareDetailsValueFilter_1.HardwareDetailsValueFilter.valueFilter])
    .controller("hardwarehealthHardwareDetailsWidget", hardwareDetailsWidget_controller_1.HardwareDetailsWidget);


/***/ }),
/* 16 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts"/>
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(18);
var HardwareDetailsService_1 = __webpack_require__(3);
var WidgetService_1 = __webpack_require__(2);
var HardwareDetailsWidget = /** @class */ (function () {
    /** @ngInject */
    HardwareDetailsWidget.$inject = ["$templateCache", "$element", "hardwareDetailsService", "widgetService"];
    function HardwareDetailsWidget($templateCache, $element, hardwareDetailsService, widgetService) {
        this.$templateCache = $templateCache;
        this.$element = $element;
        this.hardwareDetailsService = hardwareDetailsService;
        this.widgetService = widgetService;
    }
    HardwareDetailsWidget.prototype.$onInit = function () {
        this.$templateCache.put("hardware-details-row-template", __webpack_require__(1));
    };
    HardwareDetailsWidget.prototype.onLoad = function (config) {
        var _this = this;
        this.config = config;
        this.config.isBusy(true);
        var opid = this.config.settings.opid;
        var endpoint = this.config.settings.endpoint;
        this.hardwareDetailsService.getInfo(endpoint, opid).then(function (info) {
            if (!info) {
                _this.widgetService.hideParentWidget(_this.$element);
            }
            else {
                _this.info = info;
                _this.config.isBusy(false);
            }
        });
    };
    HardwareDetailsWidget = __decorate([
        widgets_1.Widget({
            selector: "hardwarehealthHardwareDetailsWidget",
            template: __webpack_require__(0),
            controllerAs: "ctrl",
            settingName: "hardwarehealthHardwareDetailsWidgetSettings"
        }),
        __metadata("design:paramtypes", [Object, Object, HardwareDetailsService_1.HardwareDetailsService,
            WidgetService_1.WidgetService])
    ], HardwareDetailsWidget);
    return HardwareDetailsWidget;
}());
exports.HardwareDetailsWidget = HardwareDetailsWidget;


/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var rowDateFormatHelper_1 = __webpack_require__(20);
var HardwareDetailsValueFilter = /** @class */ (function () {
    function HardwareDetailsValueFilter() {
    }
    /** @ngInject */
    HardwareDetailsValueFilter.filter = function (row, dateTimeService) {
        dateTimeService.GetCurrentYear();
        switch (row.type) {
            case "string":
                return row.value;
            case "datetime":
                return rowDateFormatHelper_1.RowDateFormatHelper.formatDate(row.value, dateTimeService);
            default:
                return row.value.toString();
        }
    };
    HardwareDetailsValueFilter.filter.$inject = ["row", "dateTimeService"];
    ;
    //Function valueFilter wraps filter to provide dependency injection to static method(Required to mock current year provider in unit test)
    HardwareDetailsValueFilter.valueFilter = function (dateTimeService) {
        return function (row) {
            return HardwareDetailsValueFilter.filter(row, dateTimeService);
        };
    };
    ;
    return HardwareDetailsValueFilter;
}());
exports.HardwareDetailsValueFilter = HardwareDetailsValueFilter;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var RowDateFormatHelper = /** @class */ (function () {
    function RowDateFormatHelper() {
    }
    RowDateFormatHelper.formatDate = function (dateValue, dateTimeService) {
        if (new Date(dateValue).getFullYear().valueOf() === dateTimeService.GetCurrentYear().valueOf()) {
            return moment(dateValue).format("DD MMM, h:mm A");
        }
        return moment(dateValue).format("DD MMM YYYY, h:mm A");
    };
    return RowDateFormatHelper;
}());
exports.RowDateFormatHelper = RowDateFormatHelper;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DateTimeService = /** @class */ (function () {
    function DateTimeService() {
    }
    DateTimeService.prototype.GetCurrentYear = function () {
        return new Date().getFullYear();
    };
    return DateTimeService;
}());
exports.DateTimeService = DateTimeService;


/***/ })
/******/ ]);
//# sourceMappingURL=hardwareDetailsWidget.js.map