/*!
 * @solarwinds/samappoptics 2020.2.5-160
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 498);
/******/ })
/************************************************************************/
/******/ ({

/***/ 172:
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IntegrationsService = /** @class */ (function () {
    /** @ngInject */
    IntegrationsService.$inject = ["swApi"];
    function IntegrationsService(swApi) {
        this.swApi = swApi;
        this.serviceBase = "samappoptics/integrations";
    }
    IntegrationsService.prototype.getAll = function () {
        return this.swApi.api(false).all(this.serviceBase + "/getAll").getList();
    };
    IntegrationsService.prototype.get = function (query) {
        return this.swApi.api(false).one(this.serviceBase + "/get").post("", query);
    };
    IntegrationsService.prototype.getNodesForIntegration = function () {
        return this.swApi.api(false).all(this.serviceBase + "/getNodesForIntegration").getList();
    };
    IntegrationsService.prototype.register = function (user) {
        return this.swApi
            .api(false)
            .one(this.serviceBase + "/register/")
            .post("", user);
    };
    IntegrationsService.prototype.getCountriesForRegistration = function () {
        return this.swApi.api(false).all(this.serviceBase + "/getCountriesForRegistration").getList();
    };
    IntegrationsService.prototype.getStatesForCountry = function (countryCode) {
        return this.swApi.api(false).all(this.serviceBase + "/getStatesForCountry/" + countryCode).getList();
    };
    IntegrationsService.prototype.addIntegrations = function (nodeIds) {
        this.swApi.api(false).one(this.serviceBase + "/AddIntegrations/").post("", nodeIds);
    };
    IntegrationsService.prototype.removeIntegrations = function (integrationIds) {
        this.swApi.api(false).one(this.serviceBase + "/RemoveIntegrations/").post("", integrationIds);
    };
    IntegrationsService.prototype.getAppOpticsSummaryRecords = function () {
        return this.swApi.api(false).all(this.serviceBase + "/getAppOpticsSummaryRecords").getList();
    };
    IntegrationsService.prototype.getLicenseStatus = function () {
        return this.swApi.api(false).one(this.serviceBase + "/getLicenseStatus").get();
    };
    return IntegrationsService;
}());
exports.IntegrationsService = IntegrationsService;


/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var OrionStatus_1 = __webpack_require__(41);
var StatusesIconsProvider = /** @class */ (function () {
    function StatusesIconsProvider() {
        this.createStatusesIconsMap();
    }
    StatusesIconsProvider.prototype.createStatusesIconsMap = function () {
        this.statusesIcons = new Map();
        this.statusesIcons.set(0, "status_unknown");
        this.statusesIcons.set(1, "status_up");
        this.statusesIcons.set(2, "status_down");
        this.statusesIcons.set(3, "status_warning");
        this.statusesIcons.set(4, "state_shutdown");
        this.statusesIcons.set(9, "status_unmanaged");
        this.statusesIcons.set(10, "status_unplugged");
        this.statusesIcons.set(11, "status_external");
        this.statusesIcons.set(12, "status_unreachable");
        this.statusesIcons.set(14, "status_critical");
        this.statusesIcons.set(17, "status_undefined");
        this.statusesIcons.set(19, "status_unreachable");
        this.statusesIcons.set(24, "status_inactive");
        this.statusesIcons.set(27, "status_disabled");
        this.statusesIcons.set(30, "status_notrunning");
    };
    ;
    StatusesIconsProvider.prototype.getStatusIcon = function (status) {
        if (!this.statusesIcons.has(status)) {
            return "";
        }
        return OrionStatus_1.OrionStatus[status].toLowerCase();
    };
    return StatusesIconsProvider;
}());
exports.StatusesIconsProvider = StatusesIconsProvider;


/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


//     This code was generated by a Reinforced.Typings tool. 
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
Object.defineProperty(exports, "__esModule", { value: true });
var OrionStatus;
(function (OrionStatus) {
    OrionStatus[OrionStatus["Unknown"] = 0] = "Unknown";
    OrionStatus[OrionStatus["Up"] = 1] = "Up";
    OrionStatus[OrionStatus["Down"] = 2] = "Down";
    OrionStatus[OrionStatus["Warning"] = 3] = "Warning";
    OrionStatus[OrionStatus["Shutdown"] = 4] = "Shutdown";
    OrionStatus[OrionStatus["Testing"] = 5] = "Testing";
    OrionStatus[OrionStatus["Dormant"] = 6] = "Dormant";
    OrionStatus[OrionStatus["NotPresent"] = 7] = "NotPresent";
    OrionStatus[OrionStatus["LowerLayerDown"] = 8] = "LowerLayerDown";
    OrionStatus[OrionStatus["Unmanaged"] = 9] = "Unmanaged";
    OrionStatus[OrionStatus["Unplugged"] = 10] = "Unplugged";
    OrionStatus[OrionStatus["External"] = 11] = "External";
    OrionStatus[OrionStatus["Unreachable"] = 12] = "Unreachable";
    OrionStatus[OrionStatus["Critical"] = 14] = "Critical";
    OrionStatus[OrionStatus["PartlyAvailable"] = 15] = "PartlyAvailable";
    OrionStatus[OrionStatus["Misconfigured"] = 16] = "Misconfigured";
    OrionStatus[OrionStatus["Undefined"] = 17] = "Undefined";
    OrionStatus[OrionStatus["Unconfirmed"] = 19] = "Unconfirmed";
    OrionStatus[OrionStatus["Active"] = 22] = "Active";
    OrionStatus[OrionStatus["Inactive"] = 24] = "Inactive";
    OrionStatus[OrionStatus["Expired"] = 25] = "Expired";
    OrionStatus[OrionStatus["MonitoringDisabled"] = 26] = "MonitoringDisabled";
    OrionStatus[OrionStatus["Disabled"] = 27] = "Disabled";
    OrionStatus[OrionStatus["NotLicensed"] = 28] = "NotLicensed";
    OrionStatus[OrionStatus["OtherCategory"] = 29] = "OtherCategory";
    OrionStatus[OrionStatus["NotRunning"] = 30] = "NotRunning";
})(OrionStatus = exports.OrionStatus || (exports.OrionStatus = {}));


/***/ }),

/***/ 498:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(499);


/***/ }),

/***/ 499:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(172);
var appOpticsSummary_controller_1 = __webpack_require__(500);
var integrationsService_1 = __webpack_require__(25);
__webpack_require__(502);
var orionStatusNameFilter_1 = __webpack_require__(62);
var statusesIconsProvider_1 = __webpack_require__(40);
widgets_1.TemplateLateLoader.load({
    "appOpticsSummary-item-template": __webpack_require__(61)
});
angular.module("widgets")
    .filter("orionStatusName", orionStatusNameFilter_1.OrionStatusNameFilter)
    .service("statusesIconsProvider", statusesIconsProvider_1.StatusesIconsProvider)
    .service("integrationsService", integrationsService_1.IntegrationsService)
    .controller("containerController", appOpticsSummary_controller_1.AppOpticsSummaryController);


/***/ }),

/***/ 500:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(172);
var integrationsService_1 = __webpack_require__(25);
var widgetServiceHelper_1 = __webpack_require__(501);
var AppOpticsSummaryController = /** @class */ (function () {
    /** @ngInject */
    AppOpticsSummaryController.$inject = ["$log", "getTextService", "integrationsService", "$element"];
    function AppOpticsSummaryController($log, getTextService, integrationsService, $element) {
        var _this = this;
        this.$log = $log;
        this.getTextService = getTextService;
        this.integrationsService = integrationsService;
        this.$element = $element;
        this.$onInit = function () {
            _this.initProperties();
            _this.addManageIntegrationsButton();
        };
        this.onLoad = function (config) {
            _this.config = config;
            _this.settings = config.settings;
            _this.integrationsService.getAppOpticsSummaryRecords().then(function (result) {
                if (result.length === 0) {
                    widgetServiceHelper_1.default.hideParentWidget(_this.$element);
                }
                _this.itemsSource = result;
            });
        };
        this.onSettingsChanged = function (settings) {
            _this.settings = settings;
        };
    }
    AppOpticsSummaryController.prototype.initProperties = function () {
        this.itemsSource = [];
        this._t = this.getTextService;
        this.gridOptions = {
            triggerSearchOnChange: true,
            searchDebounce: 500,
            hidePagination: false,
            pagerAdjacent: 0,
            searchableColumns: ["hostName"],
            searchPlaceholder: this._t("Search...")
        };
        this.gridPagination = {
            page: 1,
            pageSize: 10
        };
        this.sorting = {
            sortableColumns: [{
                    id: "applicationPoolName",
                    label: this._t("Pool Name")
                }],
            sortBy: {
                id: "applicationPoolName",
                label: this._t("Pool Name")
            }
        };
        this.noItemState = {
            compactMode: true,
            description: this._t("SolarWinds AppOptics is not configured or the license is not current. Go to AppOptics Deployment Summary to set up this feature."),
            image: "no-data-to-show"
        };
    };
    AppOpticsSummaryController.prototype.addManageIntegrationsButton = function () {
        var wrapper = this.$element.closest(".ResourceWrapper");
        var header = wrapper.children(".HeaderBar");
        var integrationButton = "<a href=\"/ui/samappoptics/integration\" class=\"sw-btn sw-btn-resource apo-service__manage-integration-link\">"
            + this._t("AppOptics Deployment Summary") + "</a>";
        header.children().first().before(integrationButton);
    };
    AppOpticsSummaryController = __decorate([
        widgets_1.Widget({
            selector: "appOpticsWidget",
            template: __webpack_require__(60),
            controllerAs: "vm",
            settingName: "swAppOpticsSettings"
        }),
        __metadata("design:paramtypes", [Object, Function, integrationsService_1.IntegrationsService, Object])
    ], AppOpticsSummaryController);
    return AppOpticsSummaryController;
}());
exports.AppOpticsSummaryController = AppOpticsSummaryController;


/***/ }),

/***/ 501:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetServiceHelper = /** @class */ (function () {
    function WidgetServiceHelper() {
    }
    WidgetServiceHelper.hideParentWidget = function (instanceElement) {
        // Temp hack until Apollo provides solution.
        if (instanceElement == null) {
            return false;
        }
        var wrapper = instanceElement.closest(".ResourceWrapper");
        if (wrapper.length === 0) {
            return false;
        }
        wrapper.addClass("HiddenResourceWrapper");
        return true;
    };
    return WidgetServiceHelper;
}());
exports.default = WidgetServiceHelper;


/***/ }),

/***/ 502:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 60:
/***/ (function(module, exports) {

module.exports = "<xui-grid items-source=vm.itemsSource template-url=appOpticsSummary-item-template pagination-data=vm.gridPagination smart-mode=true sorting-data=vm.sorting options=vm.gridOptions empty-data=vm.noItemState hide-toolbar=true> </xui-grid> ";

/***/ }),

/***/ 61:
/***/ (function(module, exports) {

module.exports = "<div class=apo-service> <div class=apo-application-pool> <xui-icon icon=\"status_{{item.applicationPoolStatus | orionStatusName}}\"></xui-icon> <a ng-href={{item.applicationPoolUrl}} class=apo-application-pool__name>{{item.applicationPoolName}}</a> </div> <div class=apo-integration> <div class=\"xui-margin-mdl apo-integration__name\"> <div class=apo-integration__application> <xui-icon icon=application status=\"{{item.applicationStatus | orionStatusName}}\" icon-size=small></xui-icon> <a ng-href={{item.applicationUrl}}>{{item.applicationName}}</a> </div> <div class=apo-integration__host> <span _t>on</span> <xui-icon icon=server status=\"{{item.hostStatus | orionStatusName}}\" icon-size=small></xui-icon> <a ng-href={{item.hostUrl}}>{{item.hostName}}</a> </div> </div> </div> </div> ";

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function OrionStatusNameFilter(statusesIconsProvider) {
    return function (input) { return statusesIconsProvider.getStatusIcon(input); };
}
exports.OrionStatusNameFilter = OrionStatusNameFilter;
OrionStatusNameFilter.$inject = ["statusesIconsProvider"];


/***/ })

/******/ });
//# sourceMappingURL=appOpticsSummary.js.map