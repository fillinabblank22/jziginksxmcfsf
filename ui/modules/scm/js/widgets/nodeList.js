/*!
 * @solarwinds/scm 2020.2.5
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 537);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__(71));
var util_1 = __webpack_require__(227);
exports.bundle = util_1.bundle;
var directives_1 = __webpack_require__(230);
exports.Directive = directives_1.Directive;
exports.Component = directives_1.Component;
exports.NgModule = directives_1.NgModule;
exports.Attr = directives_1.Attr;
exports.Input = directives_1.Input;
exports.Output = directives_1.Output;
exports.HostBinding = directives_1.HostBinding;
exports.HostListener = directives_1.HostListener;
exports.ViewChild = directives_1.ViewChild;
exports.ViewChildren = directives_1.ViewChildren;
exports.ContentChild = directives_1.ContentChild;
exports.ContentChildren = directives_1.ContentChildren;
var pipes_1 = __webpack_require__(231);
exports.Pipe = pipes_1.Pipe;
__export(__webpack_require__(232));
var lang_1 = __webpack_require__(1);
exports.enableProdMode = lang_1.enableProdMode;
var facade_1 = __webpack_require__(234);
exports.EventEmitter = facade_1.EventEmitter;
//# sourceMappingURL=core.js.map

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {
var globalScope;
if (typeof window === 'undefined') {
    globalScope = global;
}
else {
    globalScope = window;
}
// Need to declare a new variable for global here since TypeScript
// exports the original value of the symbol.
var _global = globalScope;
exports.global = _global;
// ===============
// implementations
// ===============
/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\n\\]|\\.)*?\1)\]/;
var reIsPlainProp = /^\w*$/;
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\n\\]|\\.)*?)\2)\]/g;
/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;
/** Used to detect unsigned integer values. */
var reIsUint = /^\d+$/;
/**
 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
 * of an array-like value.
 */
var MAX_SAFE_INTEGER = 9007199254740991;
var argsTag = '[object Arguments]';
var _devMode = true;
/**
 * Disable Angular's development mode, which turns off assertions and other
 * checks within the framework.
 *
 * One important assertion this disables verifies that a change detection pass
 * does not result in additional changes to any bindings (also known as
 * unidirectional data flow).
 */
function enableProdMode() {
    _devMode = false;
}
exports.enableProdMode = enableProdMode;
function assertionsEnabled() {
    return _devMode;
}
exports.assertionsEnabled = assertionsEnabled;
function isPresent(obj) {
    return obj !== undefined && obj !== null;
}
exports.isPresent = isPresent;
function isBlank(obj) {
    return obj === undefined || obj === null;
}
exports.isBlank = isBlank;
function isString(obj) {
    return typeof obj === "string";
}
exports.isString = isString;
function isFunction(obj) {
    return typeof obj === "function";
}
exports.isFunction = isFunction;
function isBoolean(obj) {
    return typeof obj === "boolean";
}
exports.isBoolean = isBoolean;
function isArray(obj) {
    return Array.isArray(obj);
}
exports.isArray = isArray;
function isNumber(obj) {
    return typeof obj === 'number';
}
exports.isNumber = isNumber;
function isDate(obj) {
    return obj instanceof Date && !isNaN(obj.valueOf());
}
exports.isDate = isDate;
function isType(obj) {
    return isFunction(obj);
}
exports.isType = isType;
function isStringMap(obj) {
    return typeof obj === 'object' && obj !== null;
}
exports.isStringMap = isStringMap;
function isPromise(obj) {
    return obj instanceof _global.Promise;
}
exports.isPromise = isPromise;
function isPromiseLike(obj) {
    return Boolean(isPresent(obj) && obj.then);
}
exports.isPromiseLike = isPromiseLike;
function isObservable(obj) {
    return Boolean(isPresent(obj) && obj.subscribe);
}
exports.isObservable = isObservable;
function isPromiseOrObservable(obj) {
    return isPromiseLike(obj) || isObservable(obj);
}
exports.isPromiseOrObservable = isPromiseOrObservable;
function isScope(obj) {
    return isPresent(obj) && obj.$digest && obj.$on;
}
exports.isScope = isScope;
function isSubscription(obj) {
    return isPresent(obj) && obj.unsubscribe;
}
exports.isSubscription = isSubscription;
function isJsObject(o) {
    return o !== null && (typeof o === "function" || typeof o === "object");
}
exports.isJsObject = isJsObject;
function isArguments(value) {
    // Safari 8.1 incorrectly makes `arguments.callee` enumerable in strict mode.
    return ('length' in value) && Object.prototype.hasOwnProperty.call(value, 'callee') &&
        (!Object.prototype.propertyIsEnumerable.call(value, 'callee') || Object.prototype.toString.call(value) == argsTag);
}
exports.isArguments = isArguments;
function noop() { }
exports.noop = noop;
function stringify(token) {
    if (typeof token === 'string') {
        return token;
    }
    if (token === undefined || token === null) {
        return '' + token;
    }
    if (token.name) {
        return token.name;
    }
    var res = token.toString();
    var newLineIndex = res.indexOf("\n");
    return (newLineIndex === -1)
        ? res
        : res.substring(0, newLineIndex).replace('\r', '');
}
exports.stringify = stringify;
/**
 * Converts `value` to a string if it's not one. An empty string is returned
 * for `null` or `undefined` values.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
    return value == null ? '' : (value + '');
}
exports.baseToString = baseToString;
/**
 * Converts `value` to property path array if it's not one.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {Array} Returns the property path array.
 */
function toPath(value) {
    if (isArray(value)) {
        return value;
    }
    //return value.split('.');
    var result = [];
    baseToString(value).replace(rePropName, function (match, number, quote, string) {
        var resultValue = quote
            ? string.replace(reEscapeChar, '$1')
            : (number || match);
        result.push(resultValue);
        return resultValue;
    });
    return result;
}
exports.toPath = toPath;
function assign(destination) {
    var sources = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        sources[_i - 1] = arguments[_i];
    }
    var envAssign = _global.Object['assign'] || _global.angular.extend;
    return envAssign.apply(void 0, [destination].concat(sources));
}
exports.assign = assign;
var ATTRS_BOUNDARIES = /\[|\]/g;
var COMPONENT_SELECTOR = /^\[?[\w|-]*\]?$/;
var SKEWER_CASE = /-(\w)/g;
function resolveDirectiveNameFromSelector(selector) {
    if (!selector.match(COMPONENT_SELECTOR)) {
        throw new Error("Only selectors matching element names or base attributes are supported, got: " + selector);
    }
    return selector
        .trim()
        .replace(ATTRS_BOUNDARIES, '')
        .replace(SKEWER_CASE, function (all, letter) { return letter.toUpperCase(); });
}
exports.resolveDirectiveNameFromSelector = resolveDirectiveNameFromSelector;
function getTypeName(type) {
    var typeName = getFuncName(type);
    return firstToLowerCase(typeName);
}
exports.getTypeName = getTypeName;
/**
 *
 * @param {Function}  func
 * @returns {string}
 * @private
 */
function getFuncName(func) {
    var parsedFnStatement = /function\s*([^\s(]+)/.exec(stringify(func));
    var _a = parsedFnStatement || [], _b = _a[1], name = _b === void 0 ? '' : _b;
    // if Function.name doesn't exist exec will find match otherwise return name property
    return name || stringify(func);
}
exports.getFuncName = getFuncName;
/**
 * controller instance of directive is exposed on jqLiteElement.data()
 * under the name: `$` + Ctor + `Controller`
 * @param name
 * @returns {string}
 */
function controllerKey(name) {
    return '$' + name + 'Controller';
}
exports.controllerKey = controllerKey;
function hasCtorInjectables(Type) {
    return (Array.isArray(Type.$inject) && Type.$inject.length !== 0);
}
exports.hasCtorInjectables = hasCtorInjectables;
function firstToLowerCase(value) {
    return _firstTo(value, String.prototype.toLowerCase);
}
exports.firstToLowerCase = firstToLowerCase;
function firstToUpperCase(value) {
    return _firstTo(value, String.prototype.toUpperCase);
}
exports.firstToUpperCase = firstToUpperCase;
function _firstTo(value, cb) {
    return cb.call(value.charAt(0)) + value.substring(1);
}
function normalizeBlank(obj) {
    return isBlank(obj) ? null : obj;
}
exports.normalizeBlank = normalizeBlank;
function normalizeBool(obj) {
    return isBlank(obj) ? false : obj;
}
exports.normalizeBool = normalizeBool;
function print(obj) {
    console.log(obj);
}
exports.print = print;
/**
 * Angular 2 setValueOnPath
 * supports only `.` path separator
 * @param global
 * @param path
 * @param value
 */
function setValueOnPath(global, path, value) {
    var parts = path.split('.');
    var obj = global;
    while (parts.length > 1) {
        var name = parts.shift();
        if (obj.hasOwnProperty(name) && isPresent(obj[name])) {
            obj = obj[name];
        }
        else {
            obj = obj[name] = {};
        }
    }
    if (obj === undefined || obj === null) {
        obj = {};
    }
    obj[parts.shift()] = value;
}
exports.setValueOnPath = setValueOnPath;
/**
 * Converts `value` to an object if it's not one.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {Object} Returns the object.
 */
function toObject(value) {
    return isJsObject(value)
        ? value
        : Object(value);
}
exports.toObject = toObject;
/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
    if (length === void 0) { length = MAX_SAFE_INTEGER; }
    value = (isNumber(value) || reIsUint.test(value))
        ? +value
        : -1;
    return value > -1 && value % 1 == 0 && value < length;
}
exports.isIndex = isIndex;
/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
    if ((isString(value) && reIsPlainProp.test(value)) || isNumber(value)) {
        return true;
    }
    if (isArray(value)) {
        return false;
    }
    var result = !reIsDeepProp.test(value);
    return result || (object != null && value in toObject(object));
}
exports.isKey = isKey;
//# sourceMappingURL=lang.js.map
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(73)))

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(4);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var INFINITY = 1 / 0;
/**
 * Wraps Javascript Objects
 */
var StringMapWrapper = (function () {
    function StringMapWrapper() {
    }
    StringMapWrapper.create = function () {
        // Note: We are not using Object.create(null) here due to
        // performance!
        // http://jsperf.com/ng2-object-create-null
        return {};
    };
    StringMapWrapper.contains = function (map, key) {
        return map.hasOwnProperty(key);
    };
    /**
     * The base implementation of `getValueFromPath` without support for string paths
     * and default values.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array} path The path of the property to get.
     * @param {string} [pathKey] The key representation of path.
     * @returns {*} Returns the resolved value.
     */
    StringMapWrapper.baseGet = function (object, path, pathKey) {
        if (object == null) {
            return;
        }
        object = lang_1.toObject(object);
        if (pathKey !== undefined && pathKey in object) {
            path = [pathKey];
        }
        var index = 0, length = path.length;
        while (object != null && index < length) {
            object = lang_1.toObject(object)[path[index++]];
        }
        return (index && index == length)
            ? object
            : undefined;
    };
    /**
     * Gets the property value at `path` of `object`. If the resolved value is
     * `undefined` the `defaultValue` is used in its place.
     *
     * @static
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the property to get.
     * @param {*} [defaultValue] The value returned if the resolved value is `undefined`.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }] };
     *
     * _.get(object, 'a[0].b.c');
     * // => 3
     *
     * _.get(object, ['a', '0', 'b', 'c']);
     * // => 3
     *
     * _.get(object, 'a.b.c', 'default');
     * // => 'default'
     */
    StringMapWrapper.getValueFromPath = function (object, path, defaultValue) {
        var result = object == null
            ? undefined
            : StringMapWrapper.baseGet(object, lang_1.toPath(path), (path + ''));
        return result === undefined
            ? defaultValue
            : result;
    };
    /**
     * Sets the property value of `path` on `object`. If a portion of `path`
     * does not exist it's created.
     *
     * @static
     * @param {Object} object The object to augment.
     * @param {Array|string} path The path of the property to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }] };
     *
     * _.set(object, 'a[0].b.c', 4);
     * console.log(object.a[0].b.c);
     * // => 4
     *
     * _.set(object, 'x[0].y.z', 5);
     * console.log(object.x[0].y.z);
     * // => 5
     */
    StringMapWrapper.setValueInPath = function (object, path, value) {
        if (object == null) {
            return object;
        }
        var pathKey = (path + '');
        path = (object[pathKey] != null || lang_1.isKey(path, object))
            ? [pathKey]
            : lang_1.toPath(path);
        var index = -1, length = path.length, lastIndex = length - 1, nested = object;
        while (nested != null && ++index < length) {
            var key = path[index];
            if (lang_1.isJsObject(nested)) {
                if (index == lastIndex) {
                    nested[key] = value;
                }
                else if (nested[key] == null) {
                    nested[key] = lang_1.isIndex(path[index + 1])
                        ? []
                        : {};
                }
            }
            nested = nested[key];
        }
        return object;
    };
    StringMapWrapper.get = function (map, key) {
        return map.hasOwnProperty(key)
            ? map[key]
            : undefined;
    };
    StringMapWrapper.set = function (map, key, value) { map[key] = value; };
    StringMapWrapper.keys = function (map) { return Object.keys(map); };
    StringMapWrapper.size = function (map) { return StringMapWrapper.keys(map).length; };
    StringMapWrapper.isEmpty = function (map) {
        for (var prop in map) {
            return false;
        }
        return true;
    };
    StringMapWrapper.delete = function (map, key) { delete map[key]; };
    StringMapWrapper.forEach = function (map, callback) {
        for (var prop in map) {
            if (map.hasOwnProperty(prop)) {
                callback(map[prop], prop);
            }
        }
    };
    StringMapWrapper.values = function (map) {
        return Object.keys(map).reduce(function (r, a) {
            r.push(map[a]);
            return r;
        }, []);
    };
    StringMapWrapper.merge = function (m1, m2) {
        var m = {};
        for (var attr in m1) {
            if (m1.hasOwnProperty(attr)) {
                m[attr] = m1[attr];
            }
        }
        for (var attr in m2) {
            if (m2.hasOwnProperty(attr)) {
                m[attr] = m2[attr];
            }
        }
        return m;
    };
    StringMapWrapper.equals = function (m1, m2) {
        var k1 = Object.keys(m1);
        var k2 = Object.keys(m2);
        if (k1.length != k2.length) {
            return false;
        }
        var key;
        for (var i = 0; i < k1.length; i++) {
            key = k1[i];
            if (m1[key] !== m2[key]) {
                return false;
            }
        }
        return true;
    };
    StringMapWrapper.assign = function (target) {
        var sources = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            sources[_i - 1] = arguments[_i];
        }
        if (!lang_1.isPresent(target)) {
            throw new TypeError('Object.assign cannot be called with null or undefined');
        }
        var hasOwnProperty = Object.prototype.hasOwnProperty;
        if (Object.assign) {
            return (_a = Object).assign.apply(_a, [target].concat(sources));
        }
        var from;
        var to = Object(target);
        for (var s = 0; s < sources.length; s++) {
            from = Object(sources[s]);
            for (var key in from) {
                if (hasOwnProperty.call(from, key)) {
                    to[key] = from[key];
                }
            }
        }
        return to;
        var _a;
    };
    return StringMapWrapper;
}());
exports.StringMapWrapper = StringMapWrapper;
var ListWrapper = (function () {
    function ListWrapper() {
    }
    ListWrapper.create = function () { return []; };
    ListWrapper.size = function (array) { return array.length; };
    // JS has no way to express a statically fixed size list, but dart does so we
    // keep both methods.
    ListWrapper.createFixedSize = function (size) { return new Array(size); };
    ListWrapper.createGrowableSize = function (size) { return new Array(size); };
    ListWrapper.clone = function (array) { return array.slice(0); };
    ListWrapper.forEachWithIndex = function (array, fn) {
        for (var i = 0; i < array.length; i++) {
            fn(array[i], i);
        }
    };
    ListWrapper.first = function (array) {
        if (!array)
            return null;
        return array[0];
    };
    ListWrapper.last = function (array) {
        if (!array || array.length == 0)
            return null;
        return array[array.length - 1];
    };
    ListWrapper.indexOf = function (array, value, startIndex) {
        if (startIndex === void 0) { startIndex = 0; }
        return array.indexOf(value, startIndex);
    };
    ListWrapper.contains = function (list, el) { return list.indexOf(el) !== -1; };
    ListWrapper.reversed = function (array) {
        var a = ListWrapper.clone(array);
        return a.reverse();
    };
    ListWrapper.concat = function (a, b) { return a.concat(b); };
    ListWrapper.insert = function (list, index, value) { list.splice(index, 0, value); };
    ListWrapper.removeAt = function (list, index) {
        var res = list[index];
        list.splice(index, 1);
        return res;
    };
    ListWrapper.removeAll = function (list, items) {
        for (var i = 0; i < items.length; ++i) {
            var index = list.indexOf(items[i]);
            list.splice(index, 1);
        }
    };
    ListWrapper.remove = function (list, el) {
        var index = list.indexOf(el);
        if (index > -1) {
            list.splice(index, 1);
            return true;
        }
        return false;
    };
    ListWrapper.clear = function (list) { list.length = 0; };
    ListWrapper.isEmpty = function (list) { return list.length == 0; };
    ListWrapper.fill = function (list, value, start, end) {
        if (start === void 0) { start = 0; }
        if (end === void 0) { end = null; }
        if (!Array.prototype.fill) {
            Array.prototype.fill = function (value) {
                // Steps 1-2.
                if (this == null) {
                    throw new TypeError('this is null or not defined');
                }
                var O = Object(this);
                // Steps 3-5.
                var len = O.length >>> 0;
                // Steps 6-7.
                var start = arguments[1];
                var relativeStart = start >> 0;
                // Step 8.
                var k = relativeStart < 0
                    ? Math.max(len + relativeStart, 0)
                    : Math.min(relativeStart, len);
                // Steps 9-10.
                var end = arguments[2];
                var relativeEnd = end === undefined
                    ? len
                    : end >> 0;
                // Step 11.
                var final = relativeEnd < 0
                    ? Math.max(len + relativeEnd, 0)
                    : Math.min(relativeEnd, len);
                // Step 12.
                while (k < final) {
                    O[k] = value;
                    k++;
                }
                // Step 13.
                return O;
            };
        }
        list.fill(value, start, end === null
            ? list.length
            : end);
    };
    ListWrapper.equals = function (a, b) {
        if (a.length != b.length)
            return false;
        for (var i = 0; i < a.length; ++i) {
            if (a[i] !== b[i])
                return false;
        }
        return true;
    };
    ListWrapper.slice = function (l, from, to) {
        if (from === void 0) { from = 0; }
        if (to === void 0) { to = null; }
        return l.slice(from, to === null
            ? undefined
            : to);
    };
    ListWrapper.splice = function (l, from, length) { return l.splice(from, length); };
    ListWrapper.sort = function (l, compareFn) {
        if (lang_1.isPresent(compareFn)) {
            l.sort(compareFn);
        }
        else {
            l.sort();
        }
    };
    ListWrapper.toString = function (l) { return l.toString(); };
    ListWrapper.toJSON = function (l) { return JSON.stringify(l); };
    ListWrapper.maximum = function (list, predicate) {
        if (list.length == 0) {
            return null;
        }
        var solution = null;
        var maxValue = -Infinity;
        for (var index = 0; index < list.length; index++) {
            var candidate = list[index];
            if (lang_1.isBlank(candidate)) {
                continue;
            }
            var candidateValue = predicate(candidate);
            if (candidateValue > maxValue) {
                solution = candidate;
                maxValue = candidateValue;
            }
        }
        return solution;
    };
    ListWrapper.find = function (arr, predicate, ctx) {
        if (lang_1.isFunction(Array.prototype['find'])) {
            return arr.find(predicate, ctx);
        }
        ctx = ctx || this;
        var length = arr.length;
        var i;
        if (!lang_1.isFunction(predicate)) {
            throw new TypeError(predicate + " is not a function");
        }
        for (i = 0; i < length; i++) {
            if (predicate.call(ctx, arr[i], i, arr)) {
                return arr[i];
            }
        }
        return undefined;
    };
    ListWrapper.findIndex = function (arr, predicate, ctx) {
        if (lang_1.isFunction(Array.prototype['findIndex'])) {
            return arr.findIndex(predicate, ctx);
        }
        if (!lang_1.isFunction(predicate)) {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(arr);
        var len = list.length;
        if (len === 0) {
            return -1;
        }
        for (var i = 0; i < len; i++) {
            if (predicate.call(ctx, list[i], i, list)) {
                return i;
            }
        }
        return -1;
    };
    ListWrapper.isFlattenable = function (value) {
        return lang_1.isArray(value) || lang_1.isArguments(value);
    };
    /**
     * Appends the elements of `values` to `array`.
     *
     * @private
     * @param {Array} array The array to modify.
     * @param {Array} values The values to append.
     * @returns {Array} Returns `array`.
     */
    ListWrapper.arrayPush = function (array, values) {
        var index = -1, length = values.length, offset = array.length;
        while (++index < length) {
            array[offset + index] = values[index];
        }
        return array;
    };
    /**
     * The base implementation of `_.flatten` with support for restricting flattening.
     *
     * @private
     * @param {Array} array The array to flatten.
     * @param {number} depth The maximum recursion depth.
     * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
     * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
     * @param {Array} [result=[]] The initial result value.
     * @returns {Array} Returns the new flattened array.
     */
    ListWrapper.baseFlatten = function (array, depth, predicate, isStrict, result) {
        if (predicate === void 0) { predicate = ListWrapper.isFlattenable; }
        if (isStrict === void 0) { isStrict = false; }
        if (result === void 0) { result = []; }
        var index = -1;
        var length = array.length;
        while (++index < length) {
            var value = array[index];
            if (depth > 0 && predicate(value)) {
                if (depth > 1) {
                    // Recursively flatten arrays (susceptible to call stack limits).
                    ListWrapper.baseFlatten(value, depth - 1, predicate, isStrict, result);
                }
                else {
                    ListWrapper.arrayPush(result, value);
                }
            }
            else if (!isStrict) {
                result[result.length] = value;
            }
        }
        return result;
    };
    /**
     * Flattens `array` a single level deep.
     *
     * @static
     * @param {Array} array The array to flatten.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * _.flatten([1, [2, [3, [4]], 5]]);
     * // => [1, 2, [3, [4]], 5]
     */
    ListWrapper.flatten = function (array) {
        var length = array ? array.length : 0;
        return length ? ListWrapper.baseFlatten(array, 1) : [];
    };
    /**
     * Recursively flattens `array`.
     *
     * @static
     * @param {Array} array The array to flatten.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * _.flattenDeep([1, [2, [3, [4]], 5]]);
     * // => [1, 2, 3, 4, 5]
     */
    ListWrapper.flattenDeep = function (array) {
        var length = array
            ? array.length
            : 0;
        return length
            ? ListWrapper.baseFlatten(array, INFINITY)
            : [];
    };
    return ListWrapper;
}());
exports.ListWrapper = ListWrapper;
//# sourceMappingURL=collections.js.map

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var Map = __webpack_require__(164);
var $export = __webpack_require__(41);
var shared = __webpack_require__(23)('metadata');
var store = shared.store || (shared.store = new (__webpack_require__(183))());

var getOrCreateMetadataMap = function (target, targetKey, create) {
  var targetMetadata = store.get(target);
  if (!targetMetadata) {
    if (!create) return undefined;
    store.set(target, targetMetadata = new Map());
  }
  var keyMetadata = targetMetadata.get(targetKey);
  if (!keyMetadata) {
    if (!create) return undefined;
    targetMetadata.set(targetKey, keyMetadata = new Map());
  } return keyMetadata;
};
var ordinaryHasOwnMetadata = function (MetadataKey, O, P) {
  var metadataMap = getOrCreateMetadataMap(O, P, false);
  return metadataMap === undefined ? false : metadataMap.has(MetadataKey);
};
var ordinaryGetOwnMetadata = function (MetadataKey, O, P) {
  var metadataMap = getOrCreateMetadataMap(O, P, false);
  return metadataMap === undefined ? undefined : metadataMap.get(MetadataKey);
};
var ordinaryDefineOwnMetadata = function (MetadataKey, MetadataValue, O, P) {
  getOrCreateMetadataMap(O, P, true).set(MetadataKey, MetadataValue);
};
var ordinaryOwnMetadataKeys = function (target, targetKey) {
  var metadataMap = getOrCreateMetadataMap(target, targetKey, false);
  var keys = [];
  if (metadataMap) metadataMap.forEach(function (_, key) { keys.push(key); });
  return keys;
};
var toMetaKey = function (it) {
  return it === undefined || typeof it == 'symbol' ? it : String(it);
};
var exp = function (O) {
  $export($export.S, 'Reflect', O);
};

module.exports = {
  store: store,
  map: getOrCreateMetadataMap,
  has: ordinaryHasOwnMetadata,
  get: ordinaryGetOwnMetadata,
  set: ordinaryDefineOwnMetadata,
  keys: ordinaryOwnMetadataKeys,
  key: toMetaKey,
  exp: exp
};


/***/ }),
/* 6 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(23)('wks');
var uid = __webpack_require__(24);
var Symbol = __webpack_require__(6).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(16)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
/**
 * `DependencyMetadata` is used by the framework to extend DI.
 * This is internal to Angular and should not be used directly.
 */
var DependencyMetadata = (function () {
    function DependencyMetadata() {
    }
    Object.defineProperty(DependencyMetadata.prototype, "token", {
        get: function () { return null; },
        enumerable: true,
        configurable: true
    });
    return DependencyMetadata;
}());
exports.DependencyMetadata = DependencyMetadata;
/**
 * A parameter metadata that specifies a dependency.
 *
 * ### Example ([live demo](http://plnkr.co/edit/6uHYJK?p=preview))
 *
 * ```typescript
 * class Engine {}
 *
 * @Injectable()
 * class Car {
 *   engine;
 *   constructor(@Inject("MyEngine") engine:Engine) {
 *     this.engine = engine;
 *   }
 * }
 *
 * var injector = Injector.resolveAndCreate([
 *  provide("MyEngine", {useClass: Engine}),
 *  Car
 * ]);
 *
 * expect(injector.get(Car).engine instanceof Engine).toBe(true);
 * ```
 *
 * When `@Inject()` is not present, {@link Injector} will use the type annotation of the parameter.
 *
 * ### Example
 *
 * ```typescript
 * class Engine {}
 *
 * @Injectable()
 * class Car {
 *   constructor(public engine: Engine) {} //same as constructor(@Inject(Engine) engine:Engine)
 * }
 *
 * var injector = Injector.resolveAndCreate([Engine, Car]);
 * expect(injector.get(Car).engine instanceof Engine).toBe(true);
 * ```
 */
var InjectMetadata = (function () {
    function InjectMetadata(token) {
        this.token = token;
    }
    InjectMetadata.paramDecoratorForNonConstructor = function (annotationInstance, target, propertyKey, paramIndex) {
        var annotateMethod = target[propertyKey];
        annotateMethod.$inject = annotateMethod.$inject || [];
        annotateMethod.$inject[paramIndex] = annotationInstance.token;
    };
    InjectMetadata.prototype.toString = function () { return "@Inject(" + lang_1.stringify(this.token) + ")"; };
    return InjectMetadata;
}());
exports.InjectMetadata = InjectMetadata;
/**
 * A parameter metadata that marks a dependency as optional. {@link Injector} provides `null` if
 * the dependency is not found.
 *
 * ### Example ([live demo](http://plnkr.co/edit/AsryOm?p=preview))
 *
 * ```typescript
 * class Engine {}
 *
 * @Injectable()
 * class Car {
 *   engine;
 *   constructor(@Optional() engine:Engine) {
 *     this.engine = engine;
 *   }
 * }
 *
 * var injector = Injector.resolveAndCreate([Car]);
 * expect(injector.get(Car).engine).toBeNull();
 * ```
 */
var OptionalMetadata = (function () {
    function OptionalMetadata() {
    }
    OptionalMetadata.prototype.toString = function () { return "@Optional()"; };
    return OptionalMetadata;
}());
exports.OptionalMetadata = OptionalMetadata;
/**
 * A marker metadata that marks a class as available to {@link Injector} for creation.
 *
 * ```typescript
 * @Injectable()
 * class UsefulService {}
 *
 * @Injectable()
 * class NeedsService {
 *   constructor(public service:UsefulService) {}
 * }
 *
 * var injector = Injector.resolveAndCreate([NeedsService, UsefulService]);
 * expect(injector.get(NeedsService).service instanceof UsefulService).toBe(true);
 * ```
 * {@link Injector} will throw {@link NoAnnotationError} when trying to instantiate a class that
 * does not have `@Injectable` marker, as shown in the example below.
 *
 * ```typescript
 * class UsefulService {}
 *
 * class NeedsService {
 *   constructor(public service:UsefulService) {}
 * }
 *
 * var injector = Injector.resolveAndCreate([NeedsService, UsefulService]);
 * expect(() => injector.get(NeedsService)).toThrowError();
 * ```
 */
var InjectableMetadata = (function () {
    function InjectableMetadata(_id) {
        this._id = _id;
    }
    Object.defineProperty(InjectableMetadata.prototype, "id", {
        get: function () { return this._id; },
        set: function (newID) { this._id = newID; },
        enumerable: true,
        configurable: true
    });
    return InjectableMetadata;
}());
exports.InjectableMetadata = InjectableMetadata;
/**
 * Specifies that an {@link Injector} should retrieve a dependency only from itself.
 *
 * ### Example ([live demo](http://plnkr.co/edit/NeagAg?p=preview))
 *
 * ```typescript
 * class Dependency {
 * }
 *
 * @Injectable()
 * class NeedsDependency {
 *   dependency;
 *   constructor(@Self() dependency:Dependency) {
 *     this.dependency = dependency;
 *   }
 * }
 *
 * var inj = Injector.resolveAndCreate([Dependency, NeedsDependency]);
 * var nd = inj.get(NeedsDependency);
 *
 * expect(nd.dependency instanceof Dependency).toBe(true);
 *
 * var inj = Injector.resolveAndCreate([Dependency]);
 * var child = inj.resolveAndCreateChild([NeedsDependency]);
 * expect(() => child.get(NeedsDependency)).toThrowError();
 * ```
 */
var SelfMetadata = (function () {
    function SelfMetadata() {
    }
    SelfMetadata.prototype.toString = function () { return "@Self()"; };
    return SelfMetadata;
}());
exports.SelfMetadata = SelfMetadata;
/**
 * Specifies that the dependency resolution should start from the parent injector.
 *
 * ### Example ([live demo](http://plnkr.co/edit/Wchdzb?p=preview))
 *
 * ```typescript
 * class Dependency {
 * }
 *
 * @Injectable()
 * class NeedsDependency {
 *   dependency;
 *   constructor(@SkipSelf() dependency:Dependency) {
 *     this.dependency = dependency;
 *   }
 * }
 *
 * var parent = Injector.resolveAndCreate([Dependency]);
 * var child = parent.resolveAndCreateChild([NeedsDependency]);
 * expect(child.get(NeedsDependency).dependency instanceof Depedency).toBe(true);
 *
 * var inj = Injector.resolveAndCreate([Dependency, NeedsDependency]);
 * expect(() => inj.get(NeedsDependency)).toThrowError();
 * ```
 */
var SkipSelfMetadata = (function () {
    function SkipSelfMetadata() {
    }
    SkipSelfMetadata.prototype.toString = function () { return "@SkipSelf()"; };
    return SkipSelfMetadata;
}());
exports.SkipSelfMetadata = SkipSelfMetadata;
/**
 * Specifies that an injector should retrieve a dependency from any injector until reaching the
 * closest host.
 *
 * In Angular, a component element is automatically declared as a host for all the injectors in
 * its view.
 *
 * ### Example ([live demo](http://plnkr.co/edit/GX79pV?p=preview))
 *
 * In the following example `App` contains `ParentCmp`, which contains `ChildDirective`.
 * So `ParentCmp` is the host of `ChildDirective`.
 *
 * `ChildDirective` depends on two services: `HostService` and `OtherService`.
 * `HostService` is defined at `ParentCmp`, and `OtherService` is defined at `App`.
 *
 *```typescript
 * class OtherService {}
 * class HostService {}
 *
 * @Directive({
 *   selector: 'child-directive'
 * })
 * class ChildDirective {
 *   constructor(@Optional() @Host() os:OtherService, @Optional() @Host() hs:HostService){
 *     console.log("os is null", os);
 *     console.log("hs is NOT null", hs);
 *   }
 * }
 *
 * @Component({
 *   selector: 'parent-cmp',
 *   providers: [HostService],
 *   template: `
 *     Dir: <child-directive></child-directive>
 *   `,
 *   directives: [ChildDirective]
 * })
 * class ParentCmp {
 * }
 *
 * @Component({
 *   selector: 'app',
 *   providers: [OtherService],
 *   template: `
 *     Parent: <parent-cmp></parent-cmp>
 *   `,
 *   directives: [ParentCmp]
 * })
 * class App {
 * }
 *
 * bootstrap(App);
 *```
 */
var HostMetadata = (function () {
    function HostMetadata() {
    }
    HostMetadata.prototype.toString = function () { return "@Host()"; };
    return HostMetadata;
}());
exports.HostMetadata = HostMetadata;
//# sourceMappingURL=metadata.js.map

/***/ }),
/* 10 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var reflector_1 = __webpack_require__(200);
var reflection_capabilities_1 = __webpack_require__(202);
/**
 * The {@link Reflector} used internally in Angular to access metadata
 * about symbols.
 */
exports.reflector = new reflector_1.Reflector(new reflection_capabilities_1.ReflectionCapabilities());
//# sourceMappingURL=reflection.js.map

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var metadata_1 = __webpack_require__(9);
/**
 * Directives allow you to attach behavior to elements in the DOM.
 *
 * {@link DirectiveMetadata}s with an embedded view are called {@link ComponentMetadata}s.
 *
 * A directive consists of a single directive annotation and a controller class. When the
 * directive's `selector` matches
 * elements in the DOM, the following steps occur:
 *
 * 1. For each directive, the `ElementInjector` attempts to resolve the directive's constructor
 * arguments.
 * 2. Angular instantiates directives for each matched element using `ElementInjector` in a
 * depth-first order,
 *    as declared in the HTML.
 *
 * ## Understanding How Injection Works
 *
 * There are three stages of injection resolution.
 * - *Pre-existing Injectors*:
 *   - The terminal {@link Injector} cannot resolve dependencies. It either throws an error or, if
 * the dependency was
 *     specified as `@Optional`, returns `null`.
 *   - The platform injector resolves browser singleton resources, such as: cookies, title,
 * location, and others.
 * - *Component Injectors*: Each component instance has its own {@link Injector}, and they follow
 * the same parent-child hierarchy
 *     as the component instances in the DOM.
 * - *Element Injectors*: Each component instance has a Shadow DOM. Within the Shadow DOM each
 * element has an `ElementInjector`
 *     which follow the same parent-child hierarchy as the DOM elements themselves.
 *
 * When a template is instantiated, it also must instantiate the corresponding directives in a
 * depth-first order. The
 * current `ElementInjector` resolves the constructor dependencies for each directive.
 *
 * Angular then resolves dependencies as follows, according to the order in which they appear in the
 * {@link ViewMetadata}:
 *
 * 1. Dependencies on the current element
 * 2. Dependencies on element injectors and their parents until it encounters a Shadow DOM boundary
 * 3. Dependencies on component injectors and their parents until it encounters the root component
 * 4. Dependencies on pre-existing injectors
 *
 *
 * The `ElementInjector` can inject other directives, element-specific special objects, or it can
 * delegate to the parent
 * injector.
 *
 * To inject other directives, declare the constructor parameter as:
 * - `directive:DirectiveType`: a directive on the current element only
 * - `@Host() directive:DirectiveType`: any directive that matches the type between the current
 * element and the
 *    Shadow DOM root.
 * - `@Query(DirectiveType) query:QueryList<DirectiveType>`: A live collection of direct child
 * directives.
 * - `@QueryDescendants(DirectiveType) query:QueryList<DirectiveType>`: A live collection of any
 * child directives.
 *
 * To inject element-specific special objects, declare the constructor parameter as:
 * - `element: ElementRef` to obtain a reference to logical element in the view.
 * - `viewContainer: ViewContainerRef` to control child template instantiation, for
 * {@link DirectiveMetadata} directives only
 * - `bindingPropagation: BindingPropagation` to control change detection in a more granular way.
 *
 * ### Example
 *
 * The following example demonstrates how dependency injection resolves constructor arguments in
 * practice.
 *
 *
 * Assume this HTML template:
 *
 * ```
 * <div dependency="1">
 *   <div dependency="2">
 *     <div dependency="3" my-directive>
 *       <div dependency="4">
 *         <div dependency="5"></div>
 *       </div>
 *       <div dependency="6"></div>
 *     </div>
 *   </div>
 * </div>
 * ```
 *
 * With the following `dependency` decorator and `SomeService` injectable class.
 *
 * ```
 * @Injectable()
 * class SomeService {
 * }
 *
 * @Directive({
 *   selector: '[dependency]',
 *   inputs: [
 *     'id: dependency'
 *   ]
 * })
 * class Dependency {
 *   id:string;
 * }
 * ```
 *
 * Let's step through the different ways in which `MyDirective` could be declared...
 *
 *
 * ### No injection
 *
 * Here the constructor is declared with no arguments, therefore nothing is injected into
 * `MyDirective`.
 *
 * ```
 * @Directive({ selector: '[my-directive]' })
 * class MyDirective {
 *   constructor() {
 *   }
 * }
 * ```
 *
 * This directive would be instantiated with no dependencies.
 *
 *
 * ### Component-level injection
 *
 * Directives can inject any injectable instance from the closest component injector or any of its
 * parents.
 *
 * Here, the constructor declares a parameter, `someService`, and injects the `SomeService` type
 * from the parent
 * component's injector.
 * ```
 * @Directive({ selector: '[my-directive]' })
 * class MyDirective {
 *   constructor(someService: SomeService) {
 *   }
 * }
 * ```
 *
 * This directive would be instantiated with a dependency on `SomeService`.
 *
 *
 * ### Injecting a directive from the current element
 *
 * Directives can inject other directives declared on the current element.
 *
 * ```
 * @Directive({ selector: '[my-directive]' })
 * class MyDirective {
 *   constructor(dependency: Dependency) {
 *     expect(dependency.id).toEqual(3);
 *   }
 * }
 * ```
 * This directive would be instantiated with `Dependency` declared at the same element, in this case
 * `dependency="3"`.
 *
 * ### Injecting a directive from any ancestor elements
 *
 * Directives can inject other directives declared on any ancestor element (in the current Shadow
 * DOM), i.e. on the current element, the
 * parent element, or its parents.
 * ```
 * @Directive({ selector: '[my-directive]' })
 * class MyDirective {
 *   constructor(@Host() dependency: Dependency) {
 *     expect(dependency.id).toEqual(2);
 *   }
 * }
 * ```
 *
 * `@Host` checks the current element, the parent, as well as its parents recursively. If
 * `dependency="2"` didn't
 * exist on the direct parent, this injection would
 * have returned
 * `dependency="1"`.
 *
 *
 * ### Injecting a live collection of direct child directives
 *
 *
 * A directive can also query for other child directives. Since parent directives are instantiated
 * before child directives, a directive can't simply inject the list of child directives. Instead,
 * the directive injects a {@link QueryList}, which updates its contents as children are added,
 * removed, or moved by a directive that uses a {@link ViewContainerRef} such as a `ngFor`, an
 * `ngIf`, or an `ngSwitch`.
 *
 * ```
 * @Directive({ selector: '[my-directive]' })
 * class MyDirective {
 *   constructor(@Query(Dependency) dependencies:QueryList<Dependency>) {
 *   }
 * }
 * ```
 *
 * This directive would be instantiated with a {@link QueryList} which contains `Dependency` 4 and
 * `Dependency` 6. Here, `Dependency` 5 would not be included, because it is not a direct child.
 *
 * ### Injecting a live collection of descendant directives
 *
 * By passing the descendant flag to `@Query` above, we can include the children of the child
 * elements.
 *
 * ```
 * @Directive({ selector: '[my-directive]' })
 * class MyDirective {
 *   constructor(@Query(Dependency, {descendants: true}) dependencies:QueryList<Dependency>) {
 *   }
 * }
 * ```
 *
 * This directive would be instantiated with a Query which would contain `Dependency` 4, 5 and 6.
 *
 * ### Optional injection
 *
 * The normal behavior of directives is to return an error when a specified dependency cannot be
 * resolved. If you
 * would like to inject `null` on unresolved dependency instead, you can annotate that dependency
 * with `@Optional()`.
 * This explicitly permits the author of a template to treat some of the surrounding directives as
 * optional.
 *
 * ```
 * @Directive({ selector: '[my-directive]' })
 * class MyDirective {
 *   constructor(@Optional() dependency:Dependency) {
 *   }
 * }
 * ```
 *
 * This directive would be instantiated with a `Dependency` directive found on the current element.
 * If none can be
 * found, the injector supplies `null` instead of throwing an error.
 *
 * ### Example
 *
 * Here we use a decorator directive to simply define basic tool-tip behavior.
 *
 * ```
 * @Directive({
 *   selector: '[tooltip]',
 *   inputs: [
 *     'text: tooltip'
 *   ],
 *   host: {
 *     '(mouseenter)': 'onMouseEnter()',
 *     '(mouseleave)': 'onMouseLeave()'
 *   }
 * })
 * class Tooltip{
 *   text:string;
 *   overlay:Overlay; // NOT YET IMPLEMENTED
 *   overlayManager:OverlayManager; // NOT YET IMPLEMENTED
 *
 *   constructor(overlayManager:OverlayManager) {
 *     this.overlay = overlay;
 *   }
 *
 *   onMouseEnter() {
 *     // exact signature to be determined
 *     this.overlay = this.overlayManager.open(text, ...);
 *   }
 *
 *   onMouseLeave() {
 *     this.overlay.close();
 *     this.overlay = null;
 *   }
 * }
 * ```
 * In our HTML template, we can then add this behavior to a `<div>` or any other element with the
 * `tooltip` selector,
 * like so:
 *
 * ```
 * <div tooltip="some text here"></div>
 * ```
 *
 * Directives can also control the instantiation, destruction, and positioning of inline template
 * elements:
 *
 * A directive uses a {@link ViewContainerRef} to instantiate, insert, move, and destroy views at
 * runtime.
 * The {@link ViewContainerRef} is created as a result of `<template>` element, and represents a
 * location in the current view
 * where these actions are performed.
 *
 * Views are always created as children of the current {@link ViewMetadata}, and as siblings of the
 * `<template>` element. Thus a
 * directive in a child view cannot inject the directive that created it.
 *
 * Since directives that create views via ViewContainers are common in Angular, and using the full
 * `<template>` element syntax is wordy, Angular
 * also supports a shorthand notation: `<li *foo="bar">` and `<li template="foo: bar">` are
 * equivalent.
 *
 * Thus,
 *
 * ```
 * <ul>
 *   <li *foo="bar" title="text"></li>
 * </ul>
 * ```
 *
 * Expands in use to:
 *
 * ```
 * <ul>
 *   <template [foo]="bar">
 *     <li title="text"></li>
 *   </template>
 * </ul>
 * ```
 *
 * Notice that although the shorthand places `*foo="bar"` within the `<li>` element, the binding for
 * the directive
 * controller is correctly instantiated on the `<template>` element rather than the `<li>` element.
 *
 * ## Lifecycle hooks
 *
 * When the directive class implements some {@link angular2/lifecycle_hooks} the callbacks are
 * called by the change detection at defined points in time during the life of the directive.
 *
 * ### Example
 *
 * Let's suppose we want to implement the `unless` behavior, to conditionally include a template.
 *
 * Here is a simple directive that triggers on an `unless` selector:
 *
 * ```
 * @Directive({
 *   selector: '[unless]',
 *   inputs: ['unless']
 * })
 * export class Unless {
 *   viewContainer: ViewContainerRef;
 *   templateRef: TemplateRef;
 *   prevCondition: boolean;
 *
 *   constructor(viewContainer: ViewContainerRef, templateRef: TemplateRef) {
 *     this.viewContainer = viewContainer;
 *     this.templateRef = templateRef;
 *     this.prevCondition = null;
 *   }
 *
 *   set unless(newCondition) {
 *     if (newCondition && (isBlank(this.prevCondition) || !this.prevCondition)) {
 *       this.prevCondition = true;
 *       this.viewContainer.clear();
 *     } else if (!newCondition && (isBlank(this.prevCondition) || this.prevCondition)) {
 *       this.prevCondition = false;
 *       this.viewContainer.create(this.templateRef);
 *     }
 *   }
 * }
 * ```
 *
 * We can then use this `unless` selector in a template:
 * ```
 * <ul>
 *   <li *unless="expr"></li>
 * </ul>
 * ```
 *
 * Once the directive instantiates the child view, the shorthand notation for the template expands
 * and the result is:
 *
 * ```
 * <ul>
 *   <template [unless]="exp">
 *     <li></li>
 *   </template>
 *   <li></li>
 * </ul>
 * ```
 *
 * Note also that although the `<li></li>` template still exists inside the `<template></template>`,
 * the instantiated
 * view occurs on the second `<li></li>` which is a sibling to the `<template>` element.
 */
var DirectiveMetadata = (function (_super) {
    __extends(DirectiveMetadata, _super);
    function DirectiveMetadata(_a) {
        var _b = _a === void 0 ? {} : _a, selector = _b.selector, inputs = _b.inputs, attrs = _b.attrs, outputs = _b.outputs, host = _b.host, providers = _b.providers, exportAs = _b.exportAs, queries = _b.queries, legacy = _b.legacy;
        var _this = _super.call(this) || this;
        _this.selector = selector;
        _this._inputs = inputs;
        _this._attrs = attrs;
        _this._outputs = outputs;
        _this.host = host;
        _this.exportAs = exportAs;
        _this.queries = queries;
        _this._providers = providers;
        _this.legacy = legacy;
        return _this;
    }
    Object.defineProperty(DirectiveMetadata.prototype, "inputs", {
        /**
         * Enumerates the set of data-bound input properties for a directive
         *
         * Angular automatically updates input properties during change detection.
         *
         * The `inputs` property defines a set of `directiveProperty` to `bindingProperty`
         * configuration:
         *
         * - `directiveProperty` specifies the component property where the value is written.
         * - `bindingProperty` specifies the DOM property where the value is read from.
         *
         * When `bindingProperty` is not provided, it is assumed to be equal to `directiveProperty`.
         *
         * ### Example ([live demo](http://plnkr.co/edit/ivhfXY?p=preview))
         *
         * The following example creates a component with two data-bound properties.
         *
         * ```typescript
         * @Component({
         *   selector: 'bank-account',
         *   inputs: ['bankName', 'id: account-id'],
         *   template: `
         *     Bank Name: {{bankName}}
         *     Account Id: {{id}}
         *   `
         * })
         * class BankAccount {
         *   bankName: string;
         *   id: string;
         *
         *   // this property is not bound, and won't be automatically updated by Angular
         *   normalizedBankName: string;
         * }
         *
         * @Component({
         *   selector: 'app',
         *   template: `
         *     <bank-account bank-name="RBC" account-id="4747"></bank-account>
         *   `,
         *   directives: [BankAccount]
         * })
         * class App {}
         *
         * bootstrap(App);
         * ```
         *
         */
        get: function () {
            return this._inputs;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DirectiveMetadata.prototype, "attrs", {
        /**
         * Enumerates the set of attribute-bound input properties for a directive
         *
         * Angular automatically updates input properties during change detection.
         *
         * The `attrs` property defines a set of `directiveProperty` to `bindingProperty`
         * configuration:
         *
         * - `directiveProperty` specifies the component property where the value is written.
         * - `bindingProperty` specifies the DOM property where the value is read from.
         *
         * When `bindingProperty` is not provided, it is assumed to be equal to `directiveProperty`.
         *
         * #### Behind the scenes:
         * This is just Angular 1 `@` binding on `bindToController` object.
         *
         * So this `attrs: ['bankName', 'id: account-id']`
         * is equal to: `bindToController{bankName:'@',id:'@accountId'}`
         *
         *
         * The following example creates a component with two attribute-bound properties.
         *
         * ```typescript
         * @Component({
         *   selector: 'bank-account',
         *   attrs: ['bankName', 'id: accountId'],
         *   template: `
         *     Bank Name: {{ctrl.bankName}}
         *     Account Id: {{ctrl.id}}
         *   `
         * })
         * class BankAccount {
         *   bankName: string;
         *   id: string;
         *
         *   // this property is not bound, and won't be automatically updated by Angular
         *   normalizedBankName: string;
         * }
         *
         * @Component({
         *   selector: 'app',
         *   template: `
         *     <bank-account bank-name="RBC" account-id="4747"></bank-account>
         *   `
         * })
         * class App {}
         *
         * ```
         *
         * Will output:
         * ```html
         * <bank-account bank-name="RBC" account-id="4747">
         *    Bank Name: RBC
         *    Account Id: 4747
         * </bank-account>
         * ```
         *
         */
        get: function () {
            return this._attrs;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DirectiveMetadata.prototype, "outputs", {
        /**
         * Enumerates the set of event-bound output properties.
         *
         * When an output property emits an event, an event handler attached to that event
         * the template is invoked.
         *
         * The `outputs` property defines a set of `directiveProperty` to `bindingProperty`
         * configuration:
         *
         * - `directiveProperty` specifies the component property that emits events.
         * - `bindingProperty` specifies the DOM property the event handler is attached to.
         *
         * ### Example ([live demo](http://plnkr.co/edit/d5CNq7?p=preview))
         *
         * ```typescript
         * @Directive({
         *   selector: 'interval-dir',
         *   outputs: ['everySecond', 'five5Secs: everyFiveSeconds']
         * })
         * class IntervalDir {
         *   everySecond = new EventEmitter();
         *   five5Secs = new EventEmitter();
         *
         *   constructor() {
         *     setInterval(() => this.everySecond.emit("event"), 1000);
         *     setInterval(() => this.five5Secs.emit("event"), 5000);
         *   }
         * }
         *
         * @Component({
         *   selector: 'app',
         *   template: `
         *     <interval-dir (every-second)="everySecond()" (every-five-seconds)="everyFiveSeconds()">
         *     </interval-dir>
         *   `,
         *   directives: [IntervalDir]
         * })
         * class App {
         *   everySecond() { console.log('second'); }
         *   everyFiveSeconds() { console.log('five seconds'); }
         * }
         * bootstrap(App);
         * ```
         *
         */
        get: function () {
            return this._outputs;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DirectiveMetadata.prototype, "providers", {
        /**
         * Defines the set of injectable objects that are visible to a Directive and its light DOM
         * children.
         *
         * ## Simple Example
         *
         * Here is an example of a class that can be injected:
         *
         * ```
         * class Greeter {
         *    greet(name:string) {
         *      return 'Hello ' + name + '!';
         *    }
         * }
         *
         * @Directive({
         *   selector: 'greet',
         *   bindings: [
         *     Greeter
         *   ]
         * })
         * class HelloWorld {
         *   greeter:Greeter;
         *
         *   constructor(greeter:Greeter) {
         *     this.greeter = greeter;
         *   }
         * }
         * ```
         */
        get: function () {
            return this._providers;
        },
        enumerable: true,
        configurable: true
    });
    return DirectiveMetadata;
}(metadata_1.InjectableMetadata));
exports.DirectiveMetadata = DirectiveMetadata;
/**
 * Declare reusable UI building blocks for an application.
 *
 * Each Angular component requires a single `@Component` annotation. The
 * `@Component`
 * annotation specifies when a component is instantiated, and which properties and hostListeners it
 * binds to.
 *
 * When a component is instantiated, Angular
 * - creates a shadow DOM for the component.
 * - loads the selected template into the shadow DOM.
 * - creates all the injectable objects configured with `providers` and `viewProviders`.
 *
 * All template expressions and statements are then evaluated against the component instance.
 *
 * ## Lifecycle hooks
 *
 * When the component class implements some {@link angular2/lifecycle_hooks} the callbacks are
 * called by the change detection at defined points in time during the life of the component.
 *
 * ### Example
 *
 * {@example core/ts/metadata/metadata.ts region='component'}
 */
var ComponentMetadata = (function (_super) {
    __extends(ComponentMetadata, _super);
    function ComponentMetadata(_a) {
        var _b = _a === void 0 ? {} : _a, selector = _b.selector, inputs = _b.inputs, attrs = _b.attrs, outputs = _b.outputs, host = _b.host, exportAs = _b.exportAs, moduleId = _b.moduleId, providers = _b.providers, viewProviders = _b.viewProviders, _c = _b.changeDetection, changeDetection = _c === void 0 ? 1 /* Default */ : _c, queries = _b.queries, templateUrl = _b.templateUrl, template = _b.template, styleUrls = _b.styleUrls, styles = _b.styles, legacy = _b.legacy;
        var _this = _super.call(this, {
            selector: selector,
            inputs: inputs,
            attrs: attrs,
            outputs: outputs,
            host: host,
            exportAs: exportAs,
            providers: providers,
            queries: queries,
            legacy: legacy
        }) || this;
        _this.changeDetection = changeDetection;
        _this._viewProviders = viewProviders;
        _this.templateUrl = templateUrl;
        _this.template = template;
        _this.styleUrls = styleUrls;
        _this.styles = styles;
        _this.moduleId = moduleId;
        return _this;
    }
    Object.defineProperty(ComponentMetadata.prototype, "viewProviders", {
        /**
         * Defines the set of injectable objects that are visible to its view DOM children.
         *
         * ## Simple Example
         *
         * Here is an example of a class that can be injected:
         *
         * ```
         * class Greeter {
         *    greet(name:string) {
         *      return 'Hello ' + name + '!';
         *    }
         * }
         *
         * @Directive({
         *   selector: 'needs-greeter'
         * })
         * class NeedsGreeter {
         *   greeter:Greeter;
         *
         *   constructor(greeter:Greeter) {
         *     this.greeter = greeter;
         *   }
         * }
         *
         * @Component({
         *   selector: 'greet',
         *   viewProviders: [
         *     Greeter
         *   ],
         *   template: `<needs-greeter></needs-greeter>`,
         *   directives: [NeedsGreeter]
         * })
         * class HelloWorld {
         * }
         *
         * ```
         */
        get: function () {
            return this._viewProviders;
        },
        enumerable: true,
        configurable: true
    });
    return ComponentMetadata;
}(DirectiveMetadata));
exports.ComponentMetadata = ComponentMetadata;
/**
 * Declares a data-bound input property.
 *
 * Angular automatically updates data-bound properties during change detection.
 *
 * `InputMetadata` takes an optional parameter that specifies the name
 * used when instantiating a component in the template. When not provided,
 * the name of the decorated property is used.
 *
 * ### Example
 *
 * The following example creates a component with two input properties.
 *
 * ```typescript
 * @Component({
 *   selector: 'bank-account',
 *   template: `
 *     Bank Name: {{bankName}}
 *     Account Id: {{id}}
 *   `
 * })
 * class BankAccount {
 *   @Input() bankName: string;
 *   @Input('account-id') id: string;
 *
 *   // this property is not bound, and won't be automatically updated by Angular
 *   normalizedBankName: string;
 * }
 *
 * @Component({
 *   selector: 'app',
 *   template: `
 *     <bank-account bank-name="RBC" account-id="4747"></bank-account>
 *   `,
 *   directives: [BankAccount]
 * })
 * class App {}
 *
 * bootstrap(App);
 * ```
 */
var InputMetadata = (function () {
    /**
     *
     * @param {string?} bindingPropertyName Name used when instantiating a component in the template.
     */
    function InputMetadata(bindingPropertyName) {
        this.bindingPropertyName = bindingPropertyName;
    }
    return InputMetadata;
}());
exports.InputMetadata = InputMetadata;
/**
 * Declares a data-bound attribute property.
 *
 * Angular automatically updates data-bound properties during change detection.
 *
 * `AttrMetadata` takes an optional parameter that specifies the name
 * used when instantiating a component in the template. When not provided,
 * the name of the decorated property is used.
 *
 *
 * #### Behind the scenes:
 * This is just Angular 1 `@` binding on `bindToController` object.
 *
 * So this `attrs: ['bankName', 'id: account-id']`
 * is equal to: `bindToController{bankName:'@',id:'@accountId'}`
 *
 * ### Example
 *
 * The following example creates a component with two attribute-bound properties.
 *
 * ```typescript
 * @Component({
 *   selector: 'bank-account',
 *   template: `
 *     Bank Name: {{ctrl.bankName}}
 *     Account Id: {{ctrl.id}}
 *   `
 * })
 * class BankAccount {
 *    @Attr() bankName: string;
 *    @Attr('accountId') id: string;
 *
 *   // this property is not bound, and won't be automatically updated by Angular
 *   normalizedBankName: string;
 * }
 *
 * @Component({
 *   selector: 'app',
 *   template: `
 *     <bank-account bank-name="RBC" account-id="4747"></bank-account>
 *   `
 * })
 * class App {}
 * ```
 *
 * Will output:
 * ```html
 * <bank-account bank-name="RBC" account-id="4747">
 *    Bank Name: RBC
 *    Account Id: 4747
 * </bank-account>
 * ```
 *
 */
var AttrMetadata = (function () {
    /**
     *
     * @param {string?} bindingPropertyName Name used when instantiating a component in the template.
     */
    function AttrMetadata(bindingPropertyName) {
        this.bindingPropertyName = bindingPropertyName;
    }
    return AttrMetadata;
}());
exports.AttrMetadata = AttrMetadata;
/**
 * Declares an event-bound output property.
 *
 * When an output property emits an event, an event handler attached to that event
 * the template is invoked.
 *
 * `OutputMetadata` takes an optional parameter that specifies the name
 * used when instantiating a component in the template. When not provided,
 * the name of the decorated property is used.
 *
 * ### Example
 *
 * ```typescript
 * @Directive({
 *   selector: 'interval-dir',
 * })
 * class IntervalDir {
 *   @Output() everySecond = new EventEmitter();
 *   @Output('everyFiveSeconds') five5Secs = new EventEmitter();
 *
 *   constructor() {
 *     setInterval(() => this.everySecond.emit("event"), 1000);
 *     setInterval(() => this.five5Secs.emit("event"), 5000);
 *   }
 * }
 *
 * @Component({
 *   selector: 'app',
 *   template: `
 *     <interval-dir (every-second)="everySecond()" (every-five-seconds)="everyFiveSeconds()">
 *     </interval-dir>
 *   `,
 *   directives: [IntervalDir]
 * })
 * class App {
 *   everySecond() { console.log('second'); }
 *   everyFiveSeconds() { console.log('five seconds'); }
 * }
 * bootstrap(App);
 * ```
 */
var OutputMetadata = (function () {
    /**
     *
     * @param {string?} bindingPropertyName Name used when instantiating a component in the template.
     */
    function OutputMetadata(bindingPropertyName) {
        this.bindingPropertyName = bindingPropertyName;
    }
    return OutputMetadata;
}());
exports.OutputMetadata = OutputMetadata;
/**
 * Declares a host property binding.
 *
 * Angular automatically checks host property bindings during change detection.
 * If a binding changes, it will update the host element of the directive.
 *
 * `HostBindingMetadata` takes an optional parameter that specifies the property
 * name of the host element that will be updated. When not provided,
 * the class property name is used.
 *
 * ### Example
 *
 * The following example creates a directive that sets the `valid` and `invalid` classes
 * on the DOM element that has ngModel directive on it.
 *
 * ```typescript
 * @Directive({selector: '[ngModel]'})
 * class NgModelStatus {
 *   constructor(public control:NgModel) {}
 *   @HostBinding('[class.valid]') get valid { return this.control.valid; }
 *   @HostBinding('[class.invalid]') get invalid { return this.control.invalid; }
 * }
 *
 * @Component({
 *   selector: 'app',
 *   template: `<input [(ngModel)]="prop">`,
 *   directives: [FORM_DIRECTIVES, NgModelStatus]
 * })
 * class App {
 *   prop;
 * }
 *
 * bootstrap(App);
 * ```
 */
var HostBindingMetadata = (function () {
    function HostBindingMetadata(hostPropertyName) {
        this.hostPropertyName = hostPropertyName;
    }
    return HostBindingMetadata;
}());
exports.HostBindingMetadata = HostBindingMetadata;
/**
 * Declares a host listener.
 *
 * Angular will invoke the decorated method when the host element emits the specified event.
 *
 * If the decorated method returns `false`, then `preventDefault` is applied on the DOM
 * event.
 *
 * ### Example
 *
 * The following example declares a directive that attaches a click listener to the button and
 * counts clicks.
 *
 * ```typescript
 * @Directive({selector: 'button[counting]'})
 * class CountClicks {
 *   numberOfClicks = 0;
 *
 *   @HostListener('click', ['$event.target'])
 *   onClick(btn) {
 *     console.log("button", btn, "number of clicks:", this.numberOfClicks++);
 *   }
 * }
 *
 * @Component({
 *   selector: 'app',
 *   template: `<button counting>Increment</button>`,
 *   directives: [CountClicks]
 * })
 * class App {}
 *
 * bootstrap(App);
 * ```
 */
var HostListenerMetadata = (function () {
    function HostListenerMetadata(eventName, args) {
        this.eventName = eventName;
        this.args = args;
    }
    return HostListenerMetadata;
}());
exports.HostListenerMetadata = HostListenerMetadata;
/**
 * Declares an Angular Module.
 */
var NgModuleMetadata = (function (_super) {
    __extends(NgModuleMetadata, _super);
    function NgModuleMetadata(options) {
        if (options === void 0) { options = {}; }
        var _this = 
        // We cannot use destructuring of the constructor argument because `exports` is a
        // protected symbol in CommonJS and closure tries to aggressively optimize it away.
        _super.call(this) || this;
        _this._providers = options.providers;
        _this.declarations = options.declarations;
        _this.imports = options.imports;
        return _this;
    }
    Object.defineProperty(NgModuleMetadata.prototype, "providers", {
        get: function () { return this._providers; },
        enumerable: true,
        configurable: true
    });
    return NgModuleMetadata;
}(metadata_1.InjectableMetadata));
exports.NgModuleMetadata = NgModuleMetadata;
//# sourceMappingURL=metadata_directives.js.map

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(2);
var IE8_DOM_DEFINE = __webpack_require__(59);
var toPrimitive = __webpack_require__(61);
var dP = Object.defineProperty;

exports.f = __webpack_require__(8) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(4);
module.exports = function (it, TYPE) {
  if (!isObject(it) || it._t !== TYPE) throw TypeError('Incompatible receiver, ' + TYPE + ' required!');
  return it;
};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var reflection_1 = __webpack_require__(11);
var pipe_provider_1 = __webpack_require__(204);
var directive_provider_1 = __webpack_require__(206);
var collections_1 = __webpack_require__(3);
var forward_ref_1 = __webpack_require__(20);
var exceptions_1 = __webpack_require__(75);
var provider_util_1 = __webpack_require__(21);
var provider_util_2 = __webpack_require__(21);
var provider_util_3 = __webpack_require__(21);
var Provider = (function () {
    function Provider(token, _a) {
        var useClass = _a.useClass, useValue = _a.useValue, useExisting = _a.useExisting, useFactory = _a.useFactory, deps = _a.deps, multi = _a.multi;
        this.token = token;
        this.useClass = useClass;
        this.useValue = useValue;
        this.useExisting = useExisting;
        this.useFactory = useFactory;
        this.dependencies = deps;
        this._multi = multi;
    }
    Object.defineProperty(Provider.prototype, "multi", {
        /**
         * Creates multiple providers matching the same token (a multi-provider).
         *
         * Multi-providers are used for creating pluggable service, where the system comes
         * with some default providers, and the user can register additional providers.
         * The combination of the default providers and the additional providers will be
         * used to drive the behavior of the system.
         *
         * ### Example
         *
         * ```typescript
         * var injector = Injector.resolveAndCreate([
         *   new Provider("Strings", { useValue: "String1", multi: true}),
         *   new Provider("Strings", { useValue: "String2", multi: true})
         * ]);
         *
         * expect(injector.get("Strings")).toEqual(["String1", "String2"]);
         * ```
         *
         * Multi-providers and regular providers cannot be mixed. The following
         * will throw an exception:
         *
         * ```typescript
         * var injector = Injector.resolveAndCreate([
         *   new Provider("Strings", { useValue: "String1", multi: true }),
         *   new Provider("Strings", { useValue: "String2"})
         * ]);
         * ```
         */
        get: function () { return lang_1.normalizeBool(this._multi); },
        enumerable: true,
        configurable: true
    });
    return Provider;
}());
exports.Provider = Provider;
var ProviderBuilder = (function () {
    function ProviderBuilder() {
    }
    ProviderBuilder.createFromType = function (type, _a) {
        var useClass = _a.useClass, useValue = _a.useValue, useFactory = _a.useFactory, deps = _a.deps;
        // ...provide('myFactory',{useFactory: () => () => { return new Foo(); } })
        if (lang_1.isPresent(useFactory)) {
            var factoryToken = getInjectableName(type);
            var injectableDeps = lang_1.isArray(deps) ? deps.map(getInjectableName) : [];
            useFactory.$inject = injectableDeps;
            return [
                factoryToken,
                useFactory
            ];
        }
        // ...provide(opaqueTokenInst,{useValue: {foo:12312} })
        // ...provide('myValue',{useValue: {foo:12312} })
        if (lang_1.isPresent(useValue)) {
            var valueToken = getInjectableName(type);
            return [
                valueToken,
                useValue
            ];
        }
        var injectableType = lang_1.isString(type) || provider_util_1.isOpaqueToken(type)
            ? forward_ref_1.resolveForwardRef(useClass)
            : forward_ref_1.resolveForwardRef(type);
        var overrideName = lang_1.isString(type) || provider_util_1.isOpaqueToken(type)
            ? getInjectableName(type)
            : '';
        if (!lang_1.isType(injectableType)) {
            throw new Error("\n      Provider registration: \"" + lang_1.stringify(injectableType) + "\":\n      =======================================================\n      token " + lang_1.stringify(injectableType) + " must be type of Type, You cannot provide none class\n      ");
        }
        /**
         *
         * @type {any[]}
         */
        var annotations = reflection_1.reflector.annotations(injectableType);
        var rootAnnotation = annotations[0];
        // No Annotation === it's config function !!!
        // NOTE: we are not checking anymore if user annotated the class or not,
        // we cannot do that anymore at the costs for nic config functions registration
        if (collections_1.ListWrapper.isEmpty(annotations)) {
            return [injectableType];
        }
        if (collections_1.ListWrapper.size(annotations) > 1) {
            var hasComponentAnnotation = annotations.some(function (meta) { return provider_util_2.isComponent(meta); });
            var hasNotAllowedSecondAnnotation = annotations.some(function (meta) {
                return provider_util_1.isDirectiveLike(meta) || provider_util_1.isService(meta) || provider_util_1.isPipe(meta);
            });
            if (!hasComponentAnnotation || (hasNotAllowedSecondAnnotation && hasComponentAnnotation)) {
                throw Error("\n        Provider registration: \"" + lang_1.stringify(injectableType) + "\":\n        =======================================================\n        - you cannot use more than 1 class decorator,\n        - you've used " + annotations.map(function (meta) { return lang_1.stringify(meta.constructor); }) + "\n        Multiple class decorators are allowed only for component class: [ @Component, @StateConfig? ]\n        ");
            }
        }
        injectableType.$inject = _dependenciesFor(injectableType);
        if (provider_util_1.isPipe(rootAnnotation)) {
            return pipe_provider_1.pipeProvider.createFromType(injectableType);
        }
        if (provider_util_1.isDirectiveLike(rootAnnotation)) {
            return directive_provider_1.directiveProvider.createFromType(injectableType);
        }
        if (provider_util_1.isService(rootAnnotation)) {
            return [
                overrideName || rootAnnotation.id,
                injectableType
            ];
        }
    };
    return ProviderBuilder;
}());
/**
 * should extract the string token from provided Type and add $inject angular 1 annotation to constructor if @Inject
 * was used
 * @returns {[string,Type]}
 * @deprecated
 */
function provide(type, _a) {
    var _b = _a === void 0 ? {} : _a, useClass = _b.useClass, useValue = _b.useValue, useFactory = _b.useFactory, deps = _b.deps;
    return ProviderBuilder.createFromType(type, { useClass: useClass, useValue: useValue, useFactory: useFactory, deps: deps });
}
exports.provide = provide;
/**
 * creates $inject array Angular 1 DI annotation strings for provided Type
 * @param typeOrFunc
 * @returns {any}
 * @private
 * @internal
 */
function _dependenciesFor(typeOrFunc) {
    var params = reflection_1.reflector.parameters(typeOrFunc);
    if (lang_1.isBlank(params))
        return [];
    if (params.some(function (param) { return lang_1.isBlank(param) || collections_1.ListWrapper.isEmpty(param); })) {
        throw new Error(exceptions_1.getErrorMsg(typeOrFunc, "you cannot have holes in constructor DI injection"));
    }
    return params
        .map(function (p) { return _extractToken(p); });
}
exports._dependenciesFor = _dependenciesFor;
/**
 * should extract service/values/directives/pipes token from constructor @Inject() paramMetadata
 * @param metadata
 * @private
 * @internal
 */
function _extractToken(metadata) {
    // this is token obtained via design:paramtypes via Reflect.metadata
    var paramMetadata = metadata.filter(lang_1.isType)[0];
    // this is token obtained from @Inject() usage  for DI
    var injectMetadata = metadata.filter(provider_util_3.isInjectMetadata)[0];
    if (lang_1.isBlank(injectMetadata) && lang_1.isBlank(paramMetadata)) {
        return;
    }
    var _a = (injectMetadata || {}).token, token = _a === void 0 ? undefined : _a;
    var injectable = forward_ref_1.resolveForwardRef(token) || paramMetadata;
    return getInjectableName(injectable);
}
exports._extractToken = _extractToken;
/**
 *  A utility function that can be used to get the angular 1 injectable's name. Needed for some cases, since
 *  injectable names are auto-created.
 *
 *  Works for string/OpaqueToken/Type
 *  Note: Type must be decorated otherwise it throws
 *
 *  @example
 *  ```typescript
 *  import { Injectable, getInjectableName } from 'ng-metadata/core';
 *  // this is given some random name like 'myService48' when it's created with `module.service`
 *
 *  @Injectable
 *  class MyService {}
 *
 *  console.log(getInjectableName(MyService)); // 'myService48'
 *  ```
 *
 * @param {ProviderType}  injectable
 * @returns {string}
 */
function getInjectableName(injectable) {
    // @Inject('foo') foo
    if (lang_1.isString(injectable)) {
        return injectable;
    }
    // const fooToken = new OpaqueToken('foo')
    // @Inject(fooToken) foo
    if (provider_util_1.isOpaqueToken(injectable)) {
        return injectable.desc;
    }
    // @Injectable()
    // class SomeService(){}
    //
    // @Inject(SomeService) someSvc
    // someSvc: SomeService
    if (lang_1.isType(injectable)) {
        // only the first class annotations is injectable
        var annotation = reflection_1.reflector.annotations(injectable)[0];
        if (lang_1.isBlank(annotation)) {
            throw new Error("\n        cannot get injectable name token from none decorated class " + lang_1.getFuncName(injectable) + "\n        Only decorated classes by one of [ @Injectable,@Directive,@Component,@Pipe ], can be injected by reference\n      ");
        }
        if (provider_util_1.isPipe(annotation)) {
            return annotation.name;
        }
        if (provider_util_1.isDirectiveLike(annotation)) {
            return lang_1.resolveDirectiveNameFromSelector(annotation.selector);
        }
        if (provider_util_1.isService(annotation)) {
            return annotation.id;
        }
    }
}
exports.getInjectableName = getInjectableName;
/**
 *
 * @param metadata
 * @returns {boolean}
 * @private
 * @internal
 * @deprecated
 *
 * @TODO: delete this
 */
function _areAllDirectiveInjectionsAtTail(metadata) {
    return metadata.every(function (paramMetadata, idx, arr) {
        var isCurrentDirectiveInjection = paramMetadata.length > 1;
        var hasPrev = idx > 0;
        var hasNext = idx < arr.length - 1;
        if (hasPrev) {
            var prevInjection = arr[idx - 1];
            var isPrevDirectiveInjection = prevInjection.length > 1;
            if (isPrevDirectiveInjection && !isCurrentDirectiveInjection) {
                return false;
            }
        }
        if (hasNext) {
            var nextInjection = arr[idx + 1];
            var isNextDirectiveInjection = nextInjection.length > 1;
            if (!isNextDirectiveInjection && isNextDirectiveInjection) {
                return false;
            }
        }
        return true;
    });
}
exports._areAllDirectiveInjectionsAtTail = _areAllDirectiveInjectionsAtTail;
//# sourceMappingURL=provider.js.map

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.12' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(6);
var hide = __webpack_require__(25);
var has = __webpack_require__(10);
var SRC = __webpack_require__(24)('src');
var $toString = __webpack_require__(170);
var TO_STRING = 'toString';
var TPL = ('' + $toString).split(TO_STRING);

__webpack_require__(17).inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(68);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
/**
 * Allows to refer to references which are not yet defined.
 *
 * For instance, `forwardRef` is used when the `token` which we need to refer to for the purposes of
 * DI is declared,
 * but not yet defined. It is also used when the `token` which we use when creating a query is not
 * yet defined.
 *
 * ### Example
 * {@example core/di/ts/forward_ref/forward_ref.ts region='forward_ref'}
 */
function forwardRef(forwardRefFn) {
    forwardRefFn.__forward_ref__ = forwardRef;
    forwardRefFn.toString = function () { return lang_1.stringify(this()); };
    return forwardRefFn;
}
exports.forwardRef = forwardRef;
/**
 * Lazily retrieves the reference value from a forwardRef.
 *
 * Acts as the identity function when given a non-forward-ref value.
 *
 * ### Example ([live demo](http://plnkr.co/edit/GU72mJrk1fiodChcmiDR?p=preview))
 *
 * ```typescript
 * var ref = forwardRef(() => "refValue");
 * expect(resolveForwardRef(ref)).toEqual("refValue");
 * expect(resolveForwardRef("regularValue")).toEqual("regularValue");
 * ```
 *
 * See: {@link forwardRef}
 */
function resolveForwardRef(type) {
    if (_isForwardRef(type)) {
        return type();
    }
    else {
        return type;
    }
    function _isForwardRef(type) {
        return lang_1.isFunction(type) && type.hasOwnProperty('__forward_ref__') && type.__forward_ref__ === forwardRef;
    }
}
exports.resolveForwardRef = resolveForwardRef;
//# sourceMappingURL=forward_ref.js.map

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var metadata_directives_1 = __webpack_require__(12);
var metadata_1 = __webpack_require__(45);
var provider_1 = __webpack_require__(15);
var opaque_token_1 = __webpack_require__(74);
var metadata_2 = __webpack_require__(9);
function isProviderLiteral(obj) {
    return obj && typeof obj == 'object' && obj.provide;
}
exports.isProviderLiteral = isProviderLiteral;
function createProvider(obj) {
    return new provider_1.Provider(obj.provide, obj);
}
exports.createProvider = createProvider;
function isOpaqueToken(obj) {
    return obj instanceof opaque_token_1.OpaqueToken;
}
exports.isOpaqueToken = isOpaqueToken;
function isDirectiveLike(annotation) {
    return lang_1.isString(annotation.selector) && annotation instanceof metadata_directives_1.DirectiveMetadata;
}
exports.isDirectiveLike = isDirectiveLike;
function isDirective(annotation) {
    return isDirectiveLike(annotation) && !_hasTemplate(annotation);
}
exports.isDirective = isDirective;
function isComponent(annotation) {
    return lang_1.isString(annotation.selector) && _hasTemplate(annotation) && annotation instanceof metadata_directives_1.ComponentMetadata;
}
exports.isComponent = isComponent;
function _hasTemplate(annotation) {
    return lang_1.isPresent(annotation.template || annotation.templateUrl);
}
function isService(annotation) {
    return annotation instanceof metadata_2.InjectableMetadata;
}
exports.isService = isService;
function isPipe(annotation) {
    return lang_1.isString(annotation.name) && annotation instanceof metadata_1.PipeMetadata;
}
exports.isPipe = isPipe;
function isInjectMetadata(injectMeta) {
    return injectMeta instanceof metadata_2.InjectMetadata;
}
exports.isInjectMetadata = isInjectMetadata;
function isNgModule(annotation) {
    return annotation instanceof metadata_directives_1.NgModuleMetadata;
}
exports.isNgModule = isNgModule;
//# sourceMappingURL=provider_util.js.map

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    // public static VNextToggle = true;  // uncomment this constant when we start to use feature toggle again
    Constants.EscapeKeyPress = "escape key press"; // string when "Possibly unhandled rejection: escape key press" is thrown on modal closing by ESC
    Constants.DiffColorsSettingName = "diffColors";
    Constants.SwInventoryProfileUniqueId = "36C877F7-0041-44F7-9AFB-C677C26FD575";
    Constants.HwInventoryProfileUniqueId = "5F9C5809-2B80-4920-A2A8-438E6E6362AC";
    Constants.DisplayWhoUserSettingName = "displayWho";
    Constants.ScmHideGettingStartedPopover = "SCM.HideGettingStartedPopover";
    Constants.ScmSummaryShowGettingStartedLink = "SCM.SummaryShowGettingStartedLink";
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(17);
var global = __webpack_require__(6);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(66) ? 'pure' : 'global',
  copyright: '© 2020 Denis Pushkarev (zloirock.ru)'
});


/***/ }),
/* 24 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(13);
var createDesc = __webpack_require__(38);
module.exports = __webpack_require__(8) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__(19);
var call = __webpack_require__(171);
var isArrayIter = __webpack_require__(172);
var anObject = __webpack_require__(2);
var toLength = __webpack_require__(35);
var getIterFn = __webpack_require__(173);
var BREAK = {};
var RETURN = {};
var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
  var f = ctx(fn, that, entries ? 2 : 1);
  var index = 0;
  var length, step, iterator, result;
  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
  // fast case for arrays with default iterator
  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
    if (result === BREAK || result === RETURN) return result;
  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
    result = call(iterator, f, step.value, entries);
    if (result === BREAK || result === RETURN) return result;
  }
};
exports.BREAK = BREAK;
exports.RETURN = RETURN;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(10);
var toObject = __webpack_require__(43);
var IE_PROTO = __webpack_require__(36)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(24)('meta');
var isObject = __webpack_require__(4);
var has = __webpack_require__(10);
var setDesc = __webpack_require__(13).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(16)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var reflection_1 = __webpack_require__(11);
var metadata_1 = __webpack_require__(9);
var key_1 = __webpack_require__(203);
function makeDecorator(AnnotationCls, chainFn) {
    if (chainFn === void 0) { chainFn = null; }
    function DecoratorFactory(objOrType) {
        var annotationInstance = new AnnotationCls(objOrType);
        if (this instanceof AnnotationCls) {
            return annotationInstance;
        }
        else {
            //var chainAnnotation = isFunction( this ) && this.annotations instanceof Array
            //  ? this.annotations
            //  : [];
            //chainAnnotation.push(annotationInstance);
            if (chainFn) {
                chainFn(TypeDecorator);
            }
            return TypeDecorator;
        }
        function TypeDecorator(cls) {
            /**
             * here we are creating generated name for Services
             * so we can acquire the key for AngularJS DI
             * and we have unique names after mangling our JS
             */
            if (annotationInstance instanceof metadata_1.InjectableMetadata) {
                // set id if it was explicitly provided by user @Injectable('mySvc') otherwise generate
                annotationInstance.id = annotationInstance.id || key_1.globalKeyRegistry.get(cls);
            }
            var annotations = reflection_1.reflector.ownAnnotations(cls);
            annotations = annotations || [];
            annotations.push(annotationInstance);
            reflection_1.reflector.registerAnnotations(annotations, cls);
            return cls;
        }
    }
    DecoratorFactory.prototype = Object.create(AnnotationCls.prototype);
    return DecoratorFactory;
}
exports.makeDecorator = makeDecorator;
function makeParamDecorator(annotationCls, overrideParamDecorator) {
    if (overrideParamDecorator === void 0) { overrideParamDecorator = null; }
    function ParamDecoratorFactory() {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        // create new annotation instance with annotation decorator on proto
        var annotationInstance = Object.create(annotationCls.prototype);
        annotationCls.apply(annotationInstance, args);
        if (this instanceof annotationCls) {
            return annotationInstance;
        }
        else {
            //(ParamDecorator as any).annotation = annotationInstance;
            return ParamDecorator;
        }
        /**
         * paramDecorators are 2 dimensional arrays
         * @param cls
         * @param unusedKey
         * @param index
         * @returns {any}
         * @constructor
         */
        function ParamDecorator(cls, unusedKey, index) {
            // this is special behaviour for non constructor param Injection
            if (lang_1.isFunction(overrideParamDecorator) && lang_1.isPresent(unusedKey)) {
                return overrideParamDecorator(annotationInstance, cls, unusedKey, index);
            }
            var parameters = reflection_1.reflector.rawParameters(cls);
            parameters = parameters || [];
            // there might be gaps if some in between parameters do not have annotations.
            // we pad with nulls.
            while (parameters.length <= index) {
                parameters.push(null);
            }
            parameters[index] = parameters[index] || [];
            var annotationsForParam = parameters[index];
            annotationsForParam.push(annotationInstance);
            reflection_1.reflector.registerParameters(parameters, cls);
            return cls;
        }
    }
    ParamDecoratorFactory.prototype = Object.create(annotationCls.prototype);
    return ParamDecoratorFactory;
}
exports.makeParamDecorator = makeParamDecorator;
function makePropDecorator(decoratorCls) {
    function PropDecoratorFactory() {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var decoratorInstance = Object.create(decoratorCls.prototype);
        decoratorCls.apply(decoratorInstance, args);
        // check if this decorator was already invoked
        // - if it was return it again, just with newly applied arguments
        // - this is possible thanks to PropDecoratorFactory.prototype = Object.create(decoratorCls.prototype);
        if (this instanceof decoratorCls) {
            return decoratorInstance;
        }
        else {
            return function PropDecorator(target, name) {
                var meta = reflection_1.reflector.ownPropMetadata(target.constructor);
                meta = meta || {};
                meta[name] = meta[name] || [];
                meta[name].unshift(decoratorInstance);
                reflection_1.reflector.registerPropMetadata(meta, target.constructor);
            };
        }
    }
    // caching
    PropDecoratorFactory.prototype = Object.create(decoratorCls.prototype);
    return PropDecoratorFactory;
}
exports.makePropDecorator = makePropDecorator;
//# sourceMappingURL=decorators.js.map

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var lang_1 = __webpack_require__(1);
var forward_ref_1 = __webpack_require__(20);
var metadata_1 = __webpack_require__(9);
/**
 * Declares an injectable parameter to be a live list of directives or variable
 * bindings from the content children of a directive.
 *
 * ### Example ([live demo](http://plnkr.co/edit/lY9m8HLy7z06vDoUaSN2?p=preview))
 *
 * Assume that `<tabs>` component would like to get a list its children `<pane>`
 * components as shown in this example:
 *
 * ```html
 * <tabs>
 *   <pane title="Overview">...</pane>
 *   <pane *ngFor="#o of objects" [title]="o.title">{{o.text}}</pane>
 * </tabs>
 * ```
 *
 * The preferred solution is to query for `Pane` directives using this decorator.
 *
 * ```javascript
 * @Component({
 *   selector: 'pane',
 *   inputs: ['title']
 * })
 * class Pane {
 *   title:string;
 * }
 *
 * @Component({
 *  selector: 'tabs',
 *  template: `
 *    <ul>
 *      <li *ngFor="#pane of panes">{{pane.title}}</li>
 *    </ul>
 *    <content></content>
 *  `
 * })
 * class Tabs {
 *   panes: QueryList<Pane>;
 *   constructor(@Query(Pane) panes:QueryList<Pane>) {
  *    this.panes = panes;
  *  }
 * }
 * ```
 *
 * A query can look for variable bindings by passing in a string with desired binding symbol.
 *
 * ### Example ([live demo](http://plnkr.co/edit/sT2j25cH1dURAyBRCKx1?p=preview))
 * ```html
 * <seeker>
 *   <div #findme>...</div>
 * </seeker>
 *
 * @Component({ selector: 'seeker' })
 * class Seeker {
 *   constructor(@Query('findme') elList: QueryList<ElementRef>) {...}
 * }
 * ```
 *
 * In this case the object that is injected depend on the type of the variable
 * binding. It can be an ElementRef, a directive or a component.
 *
 * Passing in a comma separated list of variable bindings will query for all of them.
 *
 * ```html
 * <seeker>
 *   <div #find-me>...</div>
 *   <div #find-me-too>...</div>
 * </seeker>
 *
 *  @Component({
 *   selector: 'seeker'
 * })
 * class Seeker {
 *   constructor(@Query('findMe, findMeToo') elList: QueryList<ElementRef>) {...}
 * }
 * ```
 *
 * Configure whether query looks for direct children or all descendants
 * of the querying element, by using the `descendants` parameter.
 * It is set to `false` by default.
 *
 * ### Example ([live demo](http://plnkr.co/edit/wtGeB977bv7qvA5FTYl9?p=preview))
 * ```html
 * <container #first>
 *   <item>a</item>
 *   <item>b</item>
 *   <container #second>
 *     <item>c</item>
 *   </container>
 * </container>
 * ```
 *
 * When querying for items, the first container will see only `a` and `b` by default,
 * but with `Query(TextDirective, {descendants: true})` it will see `c` too.
 *
 * The queried directives are kept in a depth-first pre-order with respect to their
 * positions in the DOM.
 *
 * Query does not look deep into any subcomponent views.
 *
 * Query is updated as part of the change-detection cycle. Since change detection
 * happens after construction of a directive, QueryList will always be empty when observed in the
 * constructor.
 *
 * The injected object is an unmodifiable live list.
 * See {@link QueryList} for more details.
 */
var QueryMetadata = (function (_super) {
    __extends(QueryMetadata, _super);
    function QueryMetadata(_selector, _a) {
        var _b = _a === void 0 ? {} : _a, _c = _b.descendants, descendants = _c === void 0 ? false : _c, _d = _b.first, first = _d === void 0 ? false : _d;
        var _this = _super.call(this) || this;
        _this._selector = _selector;
        _this.descendants = descendants;
        _this.first = first;
        return _this;
    }
    Object.defineProperty(QueryMetadata.prototype, "isViewQuery", {
        /**
         * always `false` to differentiate it with {@link ViewQueryMetadata}.
         */
        get: function () { return false; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QueryMetadata.prototype, "selector", {
        /**
         * what this is querying for.
         */
        get: function () { return forward_ref_1.resolveForwardRef(this._selector); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QueryMetadata.prototype, "isVarBindingQuery", {
        /**
         * whether this is querying for a variable binding or a directive.
         */
        get: function () { return lang_1.isString(this.selector); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QueryMetadata.prototype, "varBindings", {
        /**
         * returns a list of variable bindings this is querying for.
         * Only applicable if this is a variable bindings query.
         */
        get: function () {
            return this.isVarBindingQuery
                ? this.selector.split(',')
                : [];
        },
        enumerable: true,
        configurable: true
    });
    QueryMetadata.prototype.toString = function () { return "@Query(" + lang_1.stringify(this.selector) + ")"; };
    return QueryMetadata;
}(metadata_1.DependencyMetadata));
exports.QueryMetadata = QueryMetadata;
/**
 * Configures a content query.
 *
 * Content queries are set before the `ngAfterContentInit` callback is called.
 *
 * ### Example
 *
 * ```
 * @Directive({
 *   selector: 'someDir'
 * })
 * class SomeDir {
 *   @ContentChildren(ChildDirective) contentChildren: QueryList<ChildDirective>;
 *
 *   ngAfterContentInit() {
 *     // contentChildren is set
 *   }
 * }
 * ```
 */
var ContentChildrenMetadata = (function (_super) {
    __extends(ContentChildrenMetadata, _super);
    function ContentChildrenMetadata(_selector, _a) {
        var _b = (_a === void 0 ? {} : _a).descendants, descendants = _b === void 0 ? false : _b;
        return _super.call(this, _selector, { descendants: descendants }) || this;
    }
    return ContentChildrenMetadata;
}(QueryMetadata));
exports.ContentChildrenMetadata = ContentChildrenMetadata;
/**
 * Configures a content query.
 *
 * Content queries are set before the `ngAfterContentInit` callback is called.
 *
 * ### Example
 *
 * ```
 * @Directive({
 *   selector: 'someDir'
 * })
 * class SomeDir {
 *   @ContentChild(ChildDirective) contentChild;
 *
 *   ngAfterContentInit() {
 *     // contentChild is set
 *   }
 * }
 * ```
 */
var ContentChildMetadata = (function (_super) {
    __extends(ContentChildMetadata, _super);
    function ContentChildMetadata(_selector) {
        return _super.call(this, _selector, { descendants: true, first: true }) || this;
    }
    return ContentChildMetadata;
}(QueryMetadata));
exports.ContentChildMetadata = ContentChildMetadata;
/**
 * Similar to {@link QueryMetadata}, but querying the component view, instead of
 * the content children.
 *
 * ### Example ([live demo](http://plnkr.co/edit/eNsFHDf7YjyM6IzKxM1j?p=preview))
 *
 * ```javascript
 * @Component({...})
 * @View({
 *   template: `
 *     <item> a </item>
 *     <item> b </item>
 *     <item> c </item>
 *   `
 * })
 * class MyComponent {
 *   shown: boolean;
 *
 *   constructor(private @Query(Item) items:QueryList<Item>) {
 *     items.onChange(() => console.log(items.length));
 *   }
 * }
 * ```
 *
 * Supports the same querying parameters as {@link QueryMetadata}, except
 * `descendants`. This always queries the whole view.
 *
 * As `shown` is flipped between true and false, items will contain zero of one
 * items.
 *
 * Specifies that a {@link QueryList} should be injected.
 *
 * The injected object is an iterable and observable live list.
 * See {@link QueryList} for more details.
 */
var ViewQueryMetadata = (function (_super) {
    __extends(ViewQueryMetadata, _super);
    function ViewQueryMetadata(_selector, _a) {
        var _b = _a === void 0 ? {} : _a, _c = _b.descendants, descendants = _c === void 0 ? false : _c, _d = _b.first, first = _d === void 0 ? false : _d;
        return _super.call(this, _selector, { descendants: descendants, first: first }) || this;
    }
    Object.defineProperty(ViewQueryMetadata.prototype, "isViewQuery", {
        /**
         * always `true` to differentiate it with {@link QueryMetadata}.
         */
        get: function () { return true; },
        enumerable: true,
        configurable: true
    });
    ViewQueryMetadata.prototype.toString = function () { return "@ViewQuery(" + lang_1.stringify(this.selector) + ")"; };
    return ViewQueryMetadata;
}(QueryMetadata));
exports.ViewQueryMetadata = ViewQueryMetadata;
/**
 * Configures a view query.
 *
 * View queries are set before the `ngAfterViewInit` callback is called.
 *
 * ### Example
 *
 * ```
 * @Component({
 *   selector: 'someDir',
 *   templateUrl: 'someTemplate',
 *   directives: [ItemDirective]
 * })
 * class SomeDir {
 *   @ViewChildren(ItemDirective) viewChildren: QueryList<ItemDirective>;
 *
 *   ngAfterViewInit() {
 *     // viewChildren is set
 *   }
 * }
 * ```
 */
var ViewChildrenMetadata = (function (_super) {
    __extends(ViewChildrenMetadata, _super);
    function ViewChildrenMetadata(_selector) {
        return _super.call(this, _selector, { descendants: true }) || this;
    }
    return ViewChildrenMetadata;
}(ViewQueryMetadata));
exports.ViewChildrenMetadata = ViewChildrenMetadata;
/**
 * Configures a view query.
 *
 * View queries are set before the `ngAfterViewInit` callback is called.
 *
 * ### Example
 *
 * ```
 * @Component({
 *   selector: 'someDir',
 *   templateUrl: 'someTemplate',
 *   directives: [ItemDirective]
 * })
 * class SomeDir {
 *   @ViewChild(ItemDirective) viewChild:ItemDirective;
 *
 *   ngAfterViewInit() {
 *     // viewChild is set
 *   }
 * }
 * ```
 */
var ViewChildMetadata = (function (_super) {
    __extends(ViewChildMetadata, _super);
    function ViewChildMetadata(_selector) {
        return _super.call(this, _selector, { descendants: true, first: true }) || this;
    }
    return ViewChildMetadata;
}(ViewQueryMetadata));
exports.ViewChildMetadata = ViewChildMetadata;
//# sourceMappingURL=metadata_di.js.map

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var decorators_1 = __webpack_require__(29);
var metadata_di_1 = __webpack_require__(30);
var metadata_directives_1 = __webpack_require__(12);
exports.Component = decorators_1.makeDecorator(metadata_directives_1.ComponentMetadata);
exports.Directive = decorators_1.makeDecorator(metadata_directives_1.DirectiveMetadata);
exports.ContentChildren = decorators_1.makePropDecorator(metadata_di_1.ContentChildrenMetadata);
exports.ContentChild = decorators_1.makePropDecorator(metadata_di_1.ContentChildMetadata);
exports.ViewChildren = decorators_1.makePropDecorator(metadata_di_1.ViewChildrenMetadata);
exports.ViewChild = decorators_1.makePropDecorator(metadata_di_1.ViewChildMetadata);
exports.Input = decorators_1.makePropDecorator(metadata_directives_1.InputMetadata);
/**
 *
 * @type {any}
 * @deprecated use @Input('@') instead. Will be removed in 2.0
 */
exports.Attr = decorators_1.makePropDecorator(metadata_directives_1.AttrMetadata);
exports.Output = decorators_1.makePropDecorator(metadata_directives_1.OutputMetadata);
exports.HostBinding = decorators_1.makePropDecorator(metadata_directives_1.HostBindingMetadata);
exports.HostListener = decorators_1.makePropDecorator(metadata_directives_1.HostListenerMetadata);
/**
 * Declares an ng module.
 * @experimental
 * @Annotation
 */
exports.NgModule = decorators_1.makeDecorator(metadata_directives_1.NgModuleMetadata);
//# sourceMappingURL=decorators.js.map

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(33);
var defined = __webpack_require__(64);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(34);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 34 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(65);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(23)('keys');
var uid = __webpack_require__(24);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

var redefine = __webpack_require__(18);
module.exports = function (target, src, safe) {
  for (var key in src) redefine(target, key, src[key], safe);
  return target;
};


/***/ }),
/* 38 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 39 */
/***/ (function(module, exports) {

module.exports = function (it, Constructor, name, forbiddenField) {
  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
    throw TypeError(name + ': incorrect invocation!');
  } return it;
};


/***/ }),
/* 40 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(6);
var core = __webpack_require__(17);
var hide = __webpack_require__(25);
var redefine = __webpack_require__(18);
var ctx = __webpack_require__(19);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(13).f;
var has = __webpack_require__(10);
var TAG = __webpack_require__(7)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(64);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__(6);
var $export = __webpack_require__(41);
var redefine = __webpack_require__(18);
var redefineAll = __webpack_require__(37);
var meta = __webpack_require__(28);
var forOf = __webpack_require__(26);
var anInstance = __webpack_require__(39);
var isObject = __webpack_require__(4);
var fails = __webpack_require__(16);
var $iterDetect = __webpack_require__(179);
var setToStringTag = __webpack_require__(42);
var inheritIfRequired = __webpack_require__(180);

module.exports = function (NAME, wrapper, methods, common, IS_MAP, IS_WEAK) {
  var Base = global[NAME];
  var C = Base;
  var ADDER = IS_MAP ? 'set' : 'add';
  var proto = C && C.prototype;
  var O = {};
  var fixMethod = function (KEY) {
    var fn = proto[KEY];
    redefine(proto, KEY,
      KEY == 'delete' ? function (a) {
        return IS_WEAK && !isObject(a) ? false : fn.call(this, a === 0 ? 0 : a);
      } : KEY == 'has' ? function has(a) {
        return IS_WEAK && !isObject(a) ? false : fn.call(this, a === 0 ? 0 : a);
      } : KEY == 'get' ? function get(a) {
        return IS_WEAK && !isObject(a) ? undefined : fn.call(this, a === 0 ? 0 : a);
      } : KEY == 'add' ? function add(a) { fn.call(this, a === 0 ? 0 : a); return this; }
        : function set(a, b) { fn.call(this, a === 0 ? 0 : a, b); return this; }
    );
  };
  if (typeof C != 'function' || !(IS_WEAK || proto.forEach && !fails(function () {
    new C().entries().next();
  }))) {
    // create collection constructor
    C = common.getConstructor(wrapper, NAME, IS_MAP, ADDER);
    redefineAll(C.prototype, methods);
    meta.NEED = true;
  } else {
    var instance = new C();
    // early implementations not supports chaining
    var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance;
    // V8 ~  Chromium 40- weak-collections throws on primitives, but should return false
    var THROWS_ON_PRIMITIVES = fails(function () { instance.has(1); });
    // most early implementations doesn't supports iterables, most modern - not close it correctly
    var ACCEPT_ITERABLES = $iterDetect(function (iter) { new C(iter); }); // eslint-disable-line no-new
    // for early implementations -0 and +0 not the same
    var BUGGY_ZERO = !IS_WEAK && fails(function () {
      // V8 ~ Chromium 42- fails only with 5+ elements
      var $instance = new C();
      var index = 5;
      while (index--) $instance[ADDER](index, index);
      return !$instance.has(-0);
    });
    if (!ACCEPT_ITERABLES) {
      C = wrapper(function (target, iterable) {
        anInstance(target, C, NAME);
        var that = inheritIfRequired(new Base(), target, C);
        if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
        return that;
      });
      C.prototype = proto;
      proto.constructor = C;
    }
    if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
      fixMethod('delete');
      fixMethod('has');
      IS_MAP && fixMethod('get');
    }
    if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);
    // weak collections should not contains .clear method
    if (IS_WEAK && proto.clear) delete proto.clear;
  }

  setToStringTag(C, NAME);

  O[NAME] = C;
  $export($export.G + $export.W + $export.F * (C != Base), O);

  if (!IS_WEAK) common.setStrong(C, NAME, IS_MAP);

  return C;
};


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var lang_1 = __webpack_require__(1);
var metadata_1 = __webpack_require__(9);
/**
 * Declare reusable pipe function.
 *
 * A "pure" pipe is only re-evaluated when either the input or any of the arguments change.
 *
 * When not specified, pipes default to being pure.
 *
 * ### Example
 *
 * {@example core/ts/metadata/metadata.ts region='pipe'}
 */
var PipeMetadata = (function (_super) {
    __extends(PipeMetadata, _super);
    function PipeMetadata(_a) {
        var name = _a.name, pure = _a.pure;
        var _this = _super.call(this) || this;
        _this.name = name;
        _this._pure = pure;
        return _this;
    }
    Object.defineProperty(PipeMetadata.prototype, "pure", {
        get: function () { return lang_1.isPresent(this._pure) ? this._pure : true; },
        enumerable: true,
        configurable: true
    });
    return PipeMetadata;
}(metadata_1.InjectableMetadata));
exports.PipeMetadata = PipeMetadata;
//# sourceMappingURL=metadata.js.map

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LifecycleHooks;
(function (LifecycleHooks) {
    LifecycleHooks[LifecycleHooks["OnInit"] = 0] = "OnInit";
    LifecycleHooks[LifecycleHooks["OnDestroy"] = 1] = "OnDestroy";
    LifecycleHooks[LifecycleHooks["DoCheck"] = 2] = "DoCheck";
    LifecycleHooks[LifecycleHooks["OnChanges"] = 3] = "OnChanges";
    LifecycleHooks[LifecycleHooks["AfterContentInit"] = 4] = "AfterContentInit";
    LifecycleHooks[LifecycleHooks["AfterContentChecked"] = 5] = "AfterContentChecked";
    LifecycleHooks[LifecycleHooks["AfterViewInit"] = 6] = "AfterViewInit";
    LifecycleHooks[LifecycleHooks["AfterViewChecked"] = 7] = "AfterViewChecked";
    LifecycleHooks[LifecycleHooks["_OnChildrenChanged"] = 8] = "_OnChildrenChanged";
})(LifecycleHooks = exports.LifecycleHooks || (exports.LifecycleHooks = {}));
/**
 * @internal
 */
exports.LIFECYCLE_HOOKS_VALUES = [
    LifecycleHooks.OnInit,
    LifecycleHooks.OnDestroy,
    LifecycleHooks.DoCheck,
    LifecycleHooks.OnChanges,
    LifecycleHooks.AfterContentInit,
    LifecycleHooks.AfterContentChecked,
    LifecycleHooks.AfterViewInit,
    LifecycleHooks.AfterViewChecked,
    LifecycleHooks._OnChildrenChanged
];
var ChildrenChangeHook;
(function (ChildrenChangeHook) {
    ChildrenChangeHook[ChildrenChangeHook["FromView"] = 0] = "FromView";
    ChildrenChangeHook[ChildrenChangeHook["FromContent"] = 1] = "FromContent";
})(ChildrenChangeHook = exports.ChildrenChangeHook || (exports.ChildrenChangeHook = {}));
//# sourceMappingURL=directive_lifecycle_interfaces.js.map

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var metadata_directives_1 = __webpack_require__(12);
/**
 * use #isDirective instead
 * @deprecated
 */
function isAttrDirective(metadata) {
    return metadata instanceof metadata_directives_1.DirectiveMetadata && !(metadata instanceof metadata_directives_1.ComponentMetadata);
}
exports.isAttrDirective = isAttrDirective;
/**
 * use #isComponent instead
 * @deprecated
 */
function isComponentDirective(metadata) {
    return metadata instanceof metadata_directives_1.ComponentMetadata;
}
exports.isComponentDirective = isComponentDirective;
/**
 *
 * @param scope
 * @param element
 * @param ctrl
 * @param implementsNgOnDestroy
 * @param watchersToDispose
 * @param observersToDispose
 * @private
 */
function _setupDestroyHandler(scope, element, ctrl, implementsNgOnDestroy, watchersToDispose, observersToDispose) {
    if (watchersToDispose === void 0) { watchersToDispose = []; }
    if (observersToDispose === void 0) { observersToDispose = []; }
    scope.$on('$destroy', function () {
        if (implementsNgOnDestroy) {
            ctrl.ngOnDestroy();
        }
        watchersToDispose.forEach(function (_watcherDispose) { return _watcherDispose(); });
        observersToDispose.forEach(function (_observerDispose) { return _observerDispose(); });
    });
}
exports._setupDestroyHandler = _setupDestroyHandler;
//# sourceMappingURL=directives_utils.js.map

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {
/**
 * window: browser in DOM main thread
 * self: browser in WebWorker
 * global: Node.js/other
 */
exports.root = (typeof window == 'object' && window.window === window && window
    || typeof self == 'object' && self.self === self && self
    || typeof global == 'object' && global.global === global && global);
if (!exports.root) {
    throw new Error('RxJS could not find any global context (window, self, global)');
}
//# sourceMappingURL=root.js.map
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(73)))

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var isArray_1 = __webpack_require__(215);
var isObject_1 = __webpack_require__(216);
var isFunction_1 = __webpack_require__(81);
var tryCatch_1 = __webpack_require__(217);
var errorObject_1 = __webpack_require__(82);
var UnsubscriptionError_1 = __webpack_require__(218);
/**
 * Represents a disposable resource, such as the execution of an Observable. A
 * Subscription has one important method, `unsubscribe`, that takes no argument
 * and just disposes the resource held by the subscription.
 *
 * Additionally, subscriptions may be grouped together through the `add()`
 * method, which will attach a child Subscription to the current Subscription.
 * When a Subscription is unsubscribed, all its children (and its grandchildren)
 * will be unsubscribed as well.
 *
 * @class Subscription
 */
var Subscription = (function () {
    /**
     * @param {function(): void} [unsubscribe] A function describing how to
     * perform the disposal of resources when the `unsubscribe` method is called.
     */
    function Subscription(unsubscribe) {
        /**
         * A flag to indicate whether this Subscription has already been unsubscribed.
         * @type {boolean}
         */
        this.closed = false;
        this._parent = null;
        this._parents = null;
        this._subscriptions = null;
        if (unsubscribe) {
            this._unsubscribe = unsubscribe;
        }
    }
    /**
     * Disposes the resources held by the subscription. May, for instance, cancel
     * an ongoing Observable execution or cancel any other type of work that
     * started when the Subscription was created.
     * @return {void}
     */
    Subscription.prototype.unsubscribe = function () {
        var hasErrors = false;
        var errors;
        if (this.closed) {
            return;
        }
        var _a = this, _parent = _a._parent, _parents = _a._parents, _unsubscribe = _a._unsubscribe, _subscriptions = _a._subscriptions;
        this.closed = true;
        this._parent = null;
        this._parents = null;
        // null out _subscriptions first so any child subscriptions that attempt
        // to remove themselves from this subscription will noop
        this._subscriptions = null;
        var index = -1;
        var len = _parents ? _parents.length : 0;
        // if this._parent is null, then so is this._parents, and we
        // don't have to remove ourselves from any parent subscriptions.
        while (_parent) {
            _parent.remove(this);
            // if this._parents is null or index >= len,
            // then _parent is set to null, and the loop exits
            _parent = ++index < len && _parents[index] || null;
        }
        if (isFunction_1.isFunction(_unsubscribe)) {
            var trial = tryCatch_1.tryCatch(_unsubscribe).call(this);
            if (trial === errorObject_1.errorObject) {
                hasErrors = true;
                errors = errors || (errorObject_1.errorObject.e instanceof UnsubscriptionError_1.UnsubscriptionError ?
                    flattenUnsubscriptionErrors(errorObject_1.errorObject.e.errors) : [errorObject_1.errorObject.e]);
            }
        }
        if (isArray_1.isArray(_subscriptions)) {
            index = -1;
            len = _subscriptions.length;
            while (++index < len) {
                var sub = _subscriptions[index];
                if (isObject_1.isObject(sub)) {
                    var trial = tryCatch_1.tryCatch(sub.unsubscribe).call(sub);
                    if (trial === errorObject_1.errorObject) {
                        hasErrors = true;
                        errors = errors || [];
                        var err = errorObject_1.errorObject.e;
                        if (err instanceof UnsubscriptionError_1.UnsubscriptionError) {
                            errors = errors.concat(flattenUnsubscriptionErrors(err.errors));
                        }
                        else {
                            errors.push(err);
                        }
                    }
                }
            }
        }
        if (hasErrors) {
            throw new UnsubscriptionError_1.UnsubscriptionError(errors);
        }
    };
    /**
     * Adds a tear down to be called during the unsubscribe() of this
     * Subscription.
     *
     * If the tear down being added is a subscription that is already
     * unsubscribed, is the same reference `add` is being called on, or is
     * `Subscription.EMPTY`, it will not be added.
     *
     * If this subscription is already in an `closed` state, the passed
     * tear down logic will be executed immediately.
     *
     * @param {TeardownLogic} teardown The additional logic to execute on
     * teardown.
     * @return {Subscription} Returns the Subscription used or created to be
     * added to the inner subscriptions list. This Subscription can be used with
     * `remove()` to remove the passed teardown logic from the inner subscriptions
     * list.
     */
    Subscription.prototype.add = function (teardown) {
        if (!teardown || (teardown === Subscription.EMPTY)) {
            return Subscription.EMPTY;
        }
        if (teardown === this) {
            return this;
        }
        var subscription = teardown;
        switch (typeof teardown) {
            case 'function':
                subscription = new Subscription(teardown);
            case 'object':
                if (subscription.closed || typeof subscription.unsubscribe !== 'function') {
                    return subscription;
                }
                else if (this.closed) {
                    subscription.unsubscribe();
                    return subscription;
                }
                else if (typeof subscription._addParent !== 'function' /* quack quack */) {
                    var tmp = subscription;
                    subscription = new Subscription();
                    subscription._subscriptions = [tmp];
                }
                break;
            default:
                throw new Error('unrecognized teardown ' + teardown + ' added to Subscription.');
        }
        var subscriptions = this._subscriptions || (this._subscriptions = []);
        subscriptions.push(subscription);
        subscription._addParent(this);
        return subscription;
    };
    /**
     * Removes a Subscription from the internal list of subscriptions that will
     * unsubscribe during the unsubscribe process of this Subscription.
     * @param {Subscription} subscription The subscription to remove.
     * @return {void}
     */
    Subscription.prototype.remove = function (subscription) {
        var subscriptions = this._subscriptions;
        if (subscriptions) {
            var subscriptionIndex = subscriptions.indexOf(subscription);
            if (subscriptionIndex !== -1) {
                subscriptions.splice(subscriptionIndex, 1);
            }
        }
    };
    Subscription.prototype._addParent = function (parent) {
        var _a = this, _parent = _a._parent, _parents = _a._parents;
        if (!_parent || _parent === parent) {
            // If we don't have a parent, or the new parent is the same as the
            // current parent, then set this._parent to the new parent.
            this._parent = parent;
        }
        else if (!_parents) {
            // If there's already one parent, but not multiple, allocate an Array to
            // store the rest of the parent Subscriptions.
            this._parents = [parent];
        }
        else if (_parents.indexOf(parent) === -1) {
            // Only add the new parent to the _parents list if it's not already there.
            _parents.push(parent);
        }
    };
    Subscription.EMPTY = (function (empty) {
        empty.closed = true;
        return empty;
    }(new Subscription()));
    return Subscription;
}());
exports.Subscription = Subscription;
function flattenUnsubscriptionErrors(errors) {
    return errors.reduce(function (errs, err) { return errs.concat((err instanceof UnsubscriptionError_1.UnsubscriptionError) ? err.errors : err); }, []);
}
//# sourceMappingURL=Subscription.js.map

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var root_1 = __webpack_require__(48);
var Symbol = root_1.root.Symbol;
exports.$$rxSubscriber = (typeof Symbol === 'function' && typeof Symbol.for === 'function') ?
    Symbol.for('rxSubscriber') : '@@rxSubscriber';
//# sourceMappingURL=rxSubscriber.js.map

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AssignProfileWizardStepName;
(function (AssignProfileWizardStepName) {
    AssignProfileWizardStepName["Profiles"] = "profiles";
    AssignProfileWizardStepName["Nodes"] = "nodes";
    AssignProfileWizardStepName["Credentials"] = "credentials";
    AssignProfileWizardStepName["Confirm"] = "confirm";
})(AssignProfileWizardStepName = exports.AssignProfileWizardStepName || (exports.AssignProfileWizardStepName = {}));


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var mappings_1 = __webpack_require__(95);
var NodeConfigIssueService = /** @class */ (function () {
    function NodeConfigIssueService($q, _t, swUtil, modalService, displayService, datetimeFilter, locationService, profileElementService, scmAuthorizationService) {
        this.$q = $q;
        this._t = _t;
        this.swUtil = swUtil;
        this.modalService = modalService;
        this.displayService = displayService;
        this.datetimeFilter = datetimeFilter;
        this.locationService = locationService;
        this.profileElementService = profileElementService;
        this.scmAuthorizationService = scmAuthorizationService;
    }
    NodeConfigIssueService.prototype.getIssueDescription = function (issue, isWindows) {
        if (isWindows) {
            return this.getIssueDescriptionByOsType(issue, "Windows" /* Windows */);
        }
        else {
            return this.getIssueDescriptionByOsType(issue, "Linux" /* Linux */);
        }
    };
    NodeConfigIssueService.prototype.getIssueDescriptionByOsType = function (issue, nodeOsType) {
        var date = this._t("N/A");
        var time = this._t("N/A");
        if (!_.isNil(issue.timestamp)) {
            date = moment(issue.timestamp).format("ll");
            time = moment(issue.timestamp).format("LT");
        }
        switch (issue.type) {
            case "AgentIssue" /* AgentIssue */:
                return this._t("Agent is not working properly. Check Agent management for more information.");
            case "AgentMissing" /* AgentMissing */:
                return this._t("Files, registry entries, and script outputs are not being collected. To enable their collection, "
                    + "deploy an agent on this node through Deploy agent. Note that collecting server configuration data via  "
                    + "Orion agent is available only on supported operating systems.");
            case "ScmPluginIssue" /* ScmPluginIssue */:
                return this._t("SCM plugin was not deployed properly. Check Agent management for more information.");
            case "ScmPluginNotApplicable" /* ScmPluginNotApplicable */:
                switch (nodeOsType) {
                    case "Windows" /* Windows */:
                        return this._t("Files, registry entries, and script outputs are not being collected. Their collection is supported only on Windows "
                            + "2008 R2 SP1 and later. To resolve this message, unassign profiles with file, registry, or script elements from this node.");
                    case "Linux" /* Linux */:
                        return this._t("Files and script outputs are not being collected. SCM is unable to monitor server configurations on this node, "
                            + "as the Linux kernel version is not supported by the SCM plugin for Orion Linux agent.");
                    default:
                        return null;
                }
            case "ScmPluginNotResponding" /* ScmPluginNotResponding */:
                return this._t("SCM agent plugin is not responding. Check the documentation for more information.");
            case "AssetInventoryMissing" /* AssetInventoryMissing */:
                return this._t("Data is not being collected for this profile. To collect this data, " +
                    "turn on Asset Inventory on this node through List Resources.");
            case "FimNotSupportedVersion" /* FimNotSupportedVersion */:
            case "FimVersionError" /* FimVersionError */:
            case "FimInstallError" /* FimInstallError */:
            case "FimLoadError" /* FimLoadError */:
            case "FimInitializationTimeout" /* FimInitializationTimeout */:
                return this._t("'Who made the change' detection is not working properly. " +
                    "FIM driver required for 'who made the change' detection is not working properly.");
            case "accessDenied" /* AccessDenied */:
            case "unauthorizedAccess" /* UnauthorizedAccess */:
                return this.swUtil.formatString(this._t("Access denied on {0} at {1}. " +
                    "This element was not readable due to insufficient permissions or being locked by another process. " +
                    "The item's content may not be up to date."), date, time);
            case "parsedFileNotFound" /* ParsedFileNotFound */:
                return this.swUtil.formatString(this._t("File \"{0}\" not found."), issue.element.displayName);
            case "parsingFailed" /* ParsingFailed */:
                return this.swUtil.formatString(this._t("Parsing failed: {0}"), issue.message);
            case "scriptExecutionTimeout" /* ScriptExecutionTimeout */:
                var timeoutType = void 0;
                switch (issue.element.type) {
                    case "powershell" /* PowerShell */:
                        timeoutType = "PowerShell script";
                        break;
                    case "script" /* Script */:
                        timeoutType = "Linux script";
                        break;
                    case "database" /* Database */:
                        timeoutType = "Database query";
                        break;
                }
                var timeOut = issue.element.settings.pairs["pollingtimeout" /* PollingTimeout */];
                var timeOutString = moment.duration(timeOut).asSeconds().toString();
                return this.swUtil.formatString(this._t("{0} exceeded timeout of {1} seconds."), timeoutType, timeOutString);
            case "scriptExecutionError" /* ScriptExecutionError */:
                var scriptErrorType = "Linux script";
                if (issue.element.type === "powershell" /* PowerShell */) {
                    scriptErrorType = "PowerShell script";
                }
                return this.swUtil.formatString(this._t("Error executing the {0} on {1} at {2}. " +
                    "The item's content may not be up to date."), scriptErrorType, date, time);
            case "invalidConfigurationError" /* InvalidConfigurationError */:
                switch (nodeOsType) {
                    case "Windows" /* Windows */:
                        return this.swUtil.formatString(this._t("Profile '{0}' contains element types that cannot be polled on Windows nodes. " +
                            "Files with Linux filepaths and Linux script outputs cannot be collected on Windows nodes."), issue.profile.name);
                    case "Linux" /* Linux */:
                        return this.swUtil.formatString(this._t("Profile '{0}' contains element types that cannot be polled on Linux nodes. " +
                            "Files with Windows filepaths, Registry entries and PowerShell outputs cannot be collected on Linux nodes."), issue.profile.name);
                    default:
                        return "";
                }
            case "unableToFindCredentials" /* UnableToFindCredentials */:
                return this.swUtil.formatString(this._t("Profile '{0}' contains elements for which no credentials have been specified."), issue.profile.name);
            default:
                return this.swUtil.formatString(this._t("Polling failed on this item on {0} at {1}. " +
                    "Please see the logs for details. The item's content may not be up to date."), date, time);
        }
    };
    NodeConfigIssueService.prototype.showIssueDialog = function (issue, nodeId) {
        if (issue.type === "invalidConfigurationError" /* InvalidConfigurationError */) {
            var profileId = issue.profile.profileId;
            return this.showInvalidConfigurationIssueDialog(profileId, nodeId);
        }
        else if (issue.type === "unableToFindCredentials" /* UnableToFindCredentials */) {
            var profileId = issue.profile.profileId;
            return this.showUnableToFindCredentialsIssueDialog(profileId, nodeId);
        }
        else if (issue.element) {
            return this.showElementIssueDialog(issue);
        }
        else {
            return this.showServerIssueDialog(issue);
        }
    };
    NodeConfigIssueService.prototype.showElementIssueDialog = function (issue) {
        var viewModel = {
            dateTime: this.datetimeFilter(issue.timestamp, "long"),
            type: mappings_1.ProfileElementTypeFullText[issue.element.type],
            name: this.displayService.getElementName(issue.element),
            path: this.displayService.getElementNameInfo(issue.element),
            profile: issue.profile.name,
            message: issue.message,
            helpLink: undefined
        };
        if (issue.type != null && issue.type === "invalidConfigurationError" /* InvalidConfigurationError */) {
            this.locationService.getHelpLink("OrionSCMExplanationsOfErrorMessagesErrorsAssigningElementsToIncompatiblePlatforms").then(function (link) {
                viewModel.helpLink = link;
            });
        }
        else {
            this.locationService.getHelpLink("OrionSCMExplanationsOfErrorMessagesElementErrors").then(function (link) {
                viewModel.helpLink = link;
            });
        }
        return this.modalService.showModal({
            template: __webpack_require__(141),
            size: "md",
        }, {
            title: this._t("Polling issue detail"),
            status: "warning",
            viewModel: viewModel,
            cancelButtonText: this._t("Close"),
        });
    };
    NodeConfigIssueService.prototype.showServerIssueDialog = function (issue) {
        var viewModel = {
            message: issue.message,
            helpLink: undefined
        };
        this.locationService.getHelpLink("OrionSCMExplanationsOfErrorMessagesServerConfigurationErrors").then(function (link) {
            viewModel.helpLink = link;
        });
        return this.modalService.showModal({
            template: __webpack_require__(142),
            size: "md",
        }, {
            title: this._t("Polling issue detail"),
            status: "warning",
            viewModel: viewModel,
            cancelButtonText: this._t("Close"),
        });
    };
    NodeConfigIssueService.prototype.showInvalidConfigurationIssueDialog = function (profileId, nodeId) {
        var _this = this;
        var viewModel = {
            message: undefined,
            incompatibleProfileElements: undefined,
            helpLink: undefined
        };
        this.profileElementService.getForInvalidConfigurationIssues(profileId, nodeId).then(function (invalidConfigurationIssues) {
            var message = invalidConfigurationIssues.isWindows ?
                _this._t("Profile '{0}' contains element types that cannot be polled on Windows nodes. "
                    + "Files with Linux filepaths and Linux script outputs cannot be collected on Windows nodes.")
                : _this._t("Profile '{0}' contains element types that cannot be polled on Linux nodes. "
                    + "Files with Windows filepaths, Registry entries and PowerShell outputs cannot be collected on Linux nodes.");
            viewModel.message = _this.swUtil.formatString(message, invalidConfigurationIssues.profileName);
            viewModel.incompatibleProfileElements = _this.prepareInvalidConfigurationIssues(invalidConfigurationIssues.incompatibleProfileElements, invalidConfigurationIssues.isWindows);
        });
        this.locationService.getHelpLink("OrionSCMExplanationsOfErrorMessagesErrorsAssigningElementsToIncompatiblePlatforms").then(function (link) {
            viewModel.helpLink = link;
        });
        return this.modalService.showModal({
            template: __webpack_require__(143),
            size: "md",
        }, {
            title: this._t("Error assigning elements to incompatible node"),
            status: "warning",
            viewModel: viewModel,
            cancelButtonText: this._t("Close"),
        });
    };
    NodeConfigIssueService.prototype.showUnableToFindCredentialsIssueDialog = function (profileId, nodeId) {
        var _this = this;
        var viewModel = {};
        return this.$q.all([
            this.profileElementService.getForUnableToFindCredentialsIssues(profileId, nodeId),
            this.scmAuthorizationService.getIsScmUserAuthorized(),
            this.locationService.getHelpLink("OrionSCMSpecifyElementCredentials"),
        ]).then(function (_a) {
            var missingCredentialsIssues = _a[0], isAdmin = _a[1], helplink = _a[2];
            var message = _this._t("Profile '{0}' contains elements for which no credentials have been specified: ");
            viewModel.message = _this.swUtil.formatString(message, missingCredentialsIssues.profileName);
            viewModel.elementsWithoutCredentials = _this.prepareUnableToFindCredentialsIssues(missingCredentialsIssues.missingCredentialsElements);
            viewModel.profileName = missingCredentialsIssues.profileName;
            viewModel.editProfileLink = _this.locationService.getAssignedElementsUrl();
            viewModel.isAuthorized = isAdmin;
            viewModel.helpLink = helplink;
        }).then(function () {
            _this.modalService.showModal({
                template: __webpack_require__(144),
                size: "md",
            }, {
                title: _this._t("Missing credentials in profile elements."),
                status: "warning",
                viewModel: viewModel,
                cancelButtonText: _this._t("Close"),
            });
        });
    };
    NodeConfigIssueService.prototype.prepareUnableToFindCredentialsIssues = function (profileElements) {
        var _this = this;
        var issues = [];
        _(profileElements).sortBy(function (x) { return x.type; }).forEach(function (element) {
            var issue;
            switch (element.type) {
                case "database" /* Database */:
                    issue = _this.swUtil.formatString(_this._t("Database query '{0}'"), element.displayName);
                    break;
                case "script" /* Script */:
                    issue = _this.swUtil.formatString(_this._t("Linux script '{0}'"), element.displayName);
                    break;
                case "file" /* File */:
                    issue = _this.swUtil.formatString(_this._t("File '{0}'"), element.displayName);
                    break;
                case "powershell" /* PowerShell */:
                    issue = _this.swUtil.formatString(_this._t("PowerShell script '{0}'"), element.displayName);
                    break;
                case "parsedFile" /* ParsedFile */:
                    issue = _this.swUtil.formatString(_this._t("Parsed file '{0}'"), element.displayName);
                    break;
                case "registry" /* Registry */:
                    issue = _this.swUtil.formatString(_this._t("Registry '{0}'"), element.displayName);
                    break;
                case "swisQuery" /* SwisQuery */:
                    issue = _this.swUtil.formatString(_this._t("Internal query '{0}'"), element.displayName);
                    break;
                default:
                    issue = _this.swUtil.formatString(_this._t("Element '{0}'"), element.displayName);
            }
            issues.push(issue);
        });
        return issues;
    };
    NodeConfigIssueService.prototype.prepareInvalidConfigurationIssues = function (profileElements, isWindows) {
        var _this = this;
        var issues = [];
        profileElements.forEach(function (element) {
            var issue;
            switch (element.type) {
                case "file" /* File */:
                    issue = isWindows ?
                        _this.swUtil.formatString(_this._t("File '{0}' with Linux filepath can be collected only on Linux nodes."
                            + " This node is either non-Linux or an unrecognized platform."), element.displayName)
                        : _this.swUtil.formatString(_this._t("File '{0}' with Windows filepath can be collected only on Windows nodes."
                            + " This node is either non-Windows or an unrecognized platform."), element.displayName);
                    break;
                case "script" /* Script */:
                    issue = _this.swUtil.formatString(_this._t("Linux script '{0}' output can be collected only on Linux nodes."
                        + " This node is either non-Linux or an unrecognized platform"), element.displayName);
                    break;
                case "registry" /* Registry */:
                    issue = _this.swUtil.formatString(_this._t("Registry entry '{0}' can be collected only on Windows nodes."
                        + " This node is either non-Windows or an unrecognized platform."), element.displayName);
                    break;
                case "powershell" /* PowerShell */:
                    issue = _this.swUtil.formatString(_this._t("PowerShell script '{0}' can be collected only on Windows nodes."
                        + " This node is either non-Windows or an unrecognized platform."), element.displayName);
                    break;
                default:
                    issue = isWindows ?
                        _this.swUtil.formatString(_this._t("Element '{0}' cannot be collected on Windows node."), element.displayName)
                        : _this.swUtil.formatString(_this._t("Element '{0}' cannot be collected on Linux node."), element.displayName);
            }
            issues.push(issue);
        });
        return issues;
    };
    NodeConfigIssueService.prototype.isFimIssue = function (issue) {
        var fimIssues = [
            "FimInitializationTimeout" /* FimInitializationTimeout */,
            "FimInstallError" /* FimInstallError */,
            "FimLoadError" /* FimLoadError */,
            "FimNotSupportedVersion" /* FimNotSupportedVersion */,
            "FimVersionError" /* FimVersionError */
        ];
        return _.includes(fimIssues, issue.type);
    };
    NodeConfigIssueService.prototype.isAgentIssue = function (issue) {
        var agentIssues = [
            "AgentIssue" /* AgentIssue */,
            "AgentMissing" /* AgentMissing */,
            "ScmPluginIssue" /* ScmPluginIssue */,
            "ScmPluginNotApplicable" /* ScmPluginNotApplicable */,
            "ScmPluginNotResponding" /* ScmPluginNotResponding */,
        ];
        return _.includes(agentIssues, issue.type);
    };
    NodeConfigIssueService.prototype.isAssetInventoryIssue = function (issue) {
        var aiIssues = [
            "AssetInventoryMissing" /* AssetInventoryMissing */
        ];
        return _.includes(aiIssues, issue.type);
    };
    NodeConfigIssueService = __decorate([
        core_1.Injectable("scmNodeConfigIssueService"),
        __param(0, core_1.Inject("$q")),
        __param(1, core_1.Inject("getTextService")),
        __param(2, core_1.Inject("swUtil")),
        __param(3, core_1.Inject("scmModalService")),
        __param(4, core_1.Inject("scmElementDisplayService")),
        __param(5, core_1.Inject("scmDatetimeFilter")),
        __param(6, core_1.Inject("scmLocationService")),
        __param(7, core_1.Inject("scmProfileElementService")),
        __param(8, core_1.Inject("scmAuthorizationService"))
    ], NodeConfigIssueService);
    return NodeConfigIssueService;
}());
exports.NodeConfigIssueService = NodeConfigIssueService;


/***/ }),
/* 53 */
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var WhoExclusionListService = /** @class */ (function () {
    function WhoExclusionListService(scmApi, scmApiHelper) {
        this.scmApi = scmApi;
        this.scmApiHelper = scmApiHelper;
    }
    WhoExclusionListService.prototype.addToExclusionList = function (nodeIds) {
        var _this = this;
        return this.scmApi
            .all("fimDisabledNodes/disableFim")
            .post(nodeIds)
            .then(function (value) { return _this.scmApiHelper.handleNumericValue(value); });
    };
    WhoExclusionListService.prototype.removeFromExclusionList = function (nodeIds) {
        var _this = this;
        return this.scmApi
            .all("fimDisabledNodes/enableFim")
            .post(nodeIds)
            .then(function (value) { return _this.scmApiHelper.handleNumericValue(value); });
    };
    WhoExclusionListService.prototype.getExcludedNodesCount = function () {
        var _this = this;
        return this.scmApi
            .one("fimDisabledNodes/count")
            .get()
            .then(function (value) { return _this.scmApiHelper.handleNumericValue(value); });
    };
    WhoExclusionListService = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject("scmApi")),
        __param(1, core_1.Inject("scmApiHelperService"))
    ], WhoExclusionListService);
    return WhoExclusionListService;
}());
exports.WhoExclusionListService = WhoExclusionListService;


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(163);
__webpack_require__(190);
__webpack_require__(191);
__webpack_require__(192);
__webpack_require__(195);
__webpack_require__(196);
__webpack_require__(197);
__webpack_require__(198);
__webpack_require__(199);
module.exports = __webpack_require__(17).Reflect;


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var notExistingFileVersionId = -1;
var LocationService = /** @class */ (function () {
    /** @ngInject */
    LocationService.$inject = ["scmApi", "swDemoService", "$window"];
    function LocationService(scmApi, swDemoService, $window) {
        this.scmApi = scmApi;
        this.swDemoService = swDemoService;
        this.$window = $window;
        this.helpLinkCache = {};
    }
    LocationService.prototype.getContentDiffUrl = function () {
        var discriminant = arguments[3];
        if (typeof discriminant === "undefined") {
            return this.getContentDiffUrlByVersion.apply(this, arguments);
        }
        else if (typeof discriminant === "number") {
            return this.getContentDiffUrlByBothVersions.apply(this, arguments);
        }
        return this.getContentDiffUrlByChangeType.apply(this, arguments);
    };
    LocationService.prototype.getContentDiffUrlByVersion = function (nodeId, polledElementId, version) {
        return this.getContentDiffUrlByChangeType(nodeId, polledElementId, version, "update" /* Update */);
    };
    LocationService.prototype.getContentDiffUrlByChangeType = function (nodeId, polledElementId, version, changeType) {
        var rightVersion = version;
        var leftVersion = notExistingFileVersionId;
        if (changeType !== "unknown" /* Unknown */) {
            leftVersion = version > 1 ? version - 1 : notExistingFileVersionId;
        }
        return this.getContentDiffUrlByBothVersions(nodeId, polledElementId, leftVersion, rightVersion);
    };
    LocationService.prototype.getContentDiffUrlByBothVersions = function (nodeId, polledElementId, leftVersion, rightVersion) {
        return "/apps/scm/contentdiff/node/" + nodeId + "/element/" + polledElementId + "/leftVersion/" + leftVersion + "/rightVersion/" + rightVersion;
    };
    LocationService.prototype.getCompareUrl = function (nodeId, srcDate, dstDate) {
        var params = [];
        if (srcDate) {
            params.push("srcDate=" + moment(srcDate).add(1, "ms").toISOString()); // +1ms compensates truncating Datetime(7) to Datetime(3)
        }
        if (dstDate) {
            params.push("dstDate=" + moment(dstDate).add(1, "ms").toISOString()); // +1ms compensates truncating Datetime(7) to Datetime(3)
        }
        return "/ui/scm/compare/node/" + nodeId + "?" + params.join("&");
    };
    LocationService.prototype.getListResourcesUrl = function (nodeId, returnBack) {
        if (returnBack === void 0) { returnBack = true; }
        var returnPart;
        if (returnBack) {
            returnPart = "&ReturnTo=" + btoa(window.location.href);
        }
        else {
            returnPart = "";
        }
        return "/Orion/Nodes/ListResources.aspx?Nodes=" + nodeId + returnPart;
    };
    LocationService.prototype.getDeployAgentUrl = function (nodeIds) {
        var nodes;
        if (Array.isArray(nodeIds)) {
            nodes = nodeIds.join("|");
        }
        else {
            nodes = nodeIds.toString();
        }
        return "/Orion/AgentManagement/Admin/Deployment/Default.aspx?nodeIds=" + nodes;
    };
    LocationService.prototype.deployAgent = function (nodeIds) {
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return;
        }
        this.$window.open(this.getDeployAgentUrl(nodeIds), "_blank");
    };
    LocationService.prototype.getToNodeDetailsUrl = function (nodeId) {
        return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId;
    };
    LocationService.prototype.getHelpLink = function (topic) {
        if (this.helpLinkCache[topic]) {
            return this.helpLinkCache[topic];
        }
        return this.helpLinkCache[topic] = this.scmApi
            .one("helplink", topic)
            .get();
    };
    LocationService.prototype.getHelpLinks = function (topics) {
        return this.scmApi
            .all("helplink")
            .post(topics);
    };
    LocationService.prototype.getTestJobResultUrl = function (tempElementId) {
        return "/ui/scm/testjobresults/element/" + tempElementId;
    };
    LocationService.prototype.getManageProfilesUrl = function (openDialog) {
        if (openDialog) {
            return "/ui/scm/settings/manageProfiles?addProfile";
        }
        return "/ui/scm/settings/manageProfiles";
    };
    LocationService.prototype.getAssignedElementsUrl = function () {
        return "/ui/scm/settings/assignedElements";
    };
    LocationService.prototype.getComplianceSummaryUrl = function () {
        return "/apps/scm/dashboard/policy";
    };
    LocationService.prototype.getAssignPoliciesUrl = function () {
        return "/ui/scm/settings/policies";
    };
    LocationService.prototype.getEditProfileUrl = function (profileId) {
        return "/ui/scm/settings/manageProfiles?editProfile=" + profileId;
    };
    LocationService.prototype.getGuideUrl = function () {
        return "https://documentation.solarwinds.com/en/success_center/scm/Content/SCM_Getting_Started_Guide.htm";
    };
    LocationService.prototype.getSuccessUrl = function () {
        return "https://support.solarwinds.com/SuccessCenter/s/server-configuration-monitor-scm";
    };
    LocationService.prototype.getReleaseNotesUrl = function () {
        return "https://documentation.solarwinds.com/en/success_center/scm/content/release_notes/release_notes.htm";
    };
    LocationService.prototype.getThwackUrl = function () {
        return "https://thwack.solarwinds.com/community/systems-management/server-configuration-monitor-scm";
    };
    return LocationService;
}());
exports.LocationService = LocationService;


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    module.service("scmWidgetsService", WidgetsService);
};
var WidgetsService = /** @class */ (function () {
    /** @ngInject */
    WidgetsService.$inject = ["$window"];
    function WidgetsService($window) {
        this.$window = $window;
    }
    WidgetsService.prototype.getViewNodeId = function () {
        var viewNetObject = this.getViewNetObject();
        if (viewNetObject && viewNetObject.prefix === "N") {
            return +viewNetObject.value;
        }
        return null;
    };
    WidgetsService.prototype.getViewNetObject = function () {
        var netObject = decodeURIComponent(this.$window.location.search).match(/NetObject=([A-Z0-9]+):(.+?)(?:&|$)/i);
        if (netObject) {
            return { prefix: netObject[1], value: netObject[2] };
        }
        else {
            return null;
        }
    };
    WidgetsService.prototype.goTo = function (relativeUrl) {
        this.$window.location.href = this.$window.location.origin + relativeUrl;
    };
    return WidgetsService;
}());
exports.WidgetsService = WidgetsService;


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var dP = __webpack_require__(13).f;
var create = __webpack_require__(62);
var redefineAll = __webpack_require__(37);
var ctx = __webpack_require__(19);
var anInstance = __webpack_require__(39);
var forOf = __webpack_require__(26);
var $iterDefine = __webpack_require__(175);
var step = __webpack_require__(177);
var setSpecies = __webpack_require__(178);
var DESCRIPTORS = __webpack_require__(8);
var fastKey = __webpack_require__(28).fastKey;
var validate = __webpack_require__(14);
var SIZE = DESCRIPTORS ? '_s' : 'size';

var getEntry = function (that, key) {
  // fast case
  var index = fastKey(key);
  var entry;
  if (index !== 'F') return that._i[index];
  // frozen object case
  for (entry = that._f; entry; entry = entry.n) {
    if (entry.k == key) return entry;
  }
};

module.exports = {
  getConstructor: function (wrapper, NAME, IS_MAP, ADDER) {
    var C = wrapper(function (that, iterable) {
      anInstance(that, C, NAME, '_i');
      that._t = NAME;         // collection type
      that._i = create(null); // index
      that._f = undefined;    // first entry
      that._l = undefined;    // last entry
      that[SIZE] = 0;         // size
      if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
    });
    redefineAll(C.prototype, {
      // 23.1.3.1 Map.prototype.clear()
      // 23.2.3.2 Set.prototype.clear()
      clear: function clear() {
        for (var that = validate(this, NAME), data = that._i, entry = that._f; entry; entry = entry.n) {
          entry.r = true;
          if (entry.p) entry.p = entry.p.n = undefined;
          delete data[entry.i];
        }
        that._f = that._l = undefined;
        that[SIZE] = 0;
      },
      // 23.1.3.3 Map.prototype.delete(key)
      // 23.2.3.4 Set.prototype.delete(value)
      'delete': function (key) {
        var that = validate(this, NAME);
        var entry = getEntry(that, key);
        if (entry) {
          var next = entry.n;
          var prev = entry.p;
          delete that._i[entry.i];
          entry.r = true;
          if (prev) prev.n = next;
          if (next) next.p = prev;
          if (that._f == entry) that._f = next;
          if (that._l == entry) that._l = prev;
          that[SIZE]--;
        } return !!entry;
      },
      // 23.2.3.6 Set.prototype.forEach(callbackfn, thisArg = undefined)
      // 23.1.3.5 Map.prototype.forEach(callbackfn, thisArg = undefined)
      forEach: function forEach(callbackfn /* , that = undefined */) {
        validate(this, NAME);
        var f = ctx(callbackfn, arguments.length > 1 ? arguments[1] : undefined, 3);
        var entry;
        while (entry = entry ? entry.n : this._f) {
          f(entry.v, entry.k, this);
          // revert to the last existing entry
          while (entry && entry.r) entry = entry.p;
        }
      },
      // 23.1.3.7 Map.prototype.has(key)
      // 23.2.3.7 Set.prototype.has(value)
      has: function has(key) {
        return !!getEntry(validate(this, NAME), key);
      }
    });
    if (DESCRIPTORS) dP(C.prototype, 'size', {
      get: function () {
        return validate(this, NAME)[SIZE];
      }
    });
    return C;
  },
  def: function (that, key, value) {
    var entry = getEntry(that, key);
    var prev, index;
    // change existing entry
    if (entry) {
      entry.v = value;
    // create new entry
    } else {
      that._l = entry = {
        i: index = fastKey(key, true), // <- index
        k: key,                        // <- key
        v: value,                      // <- value
        p: prev = that._l,             // <- previous entry
        n: undefined,                  // <- next entry
        r: false                       // <- removed
      };
      if (!that._f) that._f = entry;
      if (prev) prev.n = entry;
      that[SIZE]++;
      // add to index
      if (index !== 'F') that._i[index] = entry;
    } return that;
  },
  getEntry: getEntry,
  setStrong: function (C, NAME, IS_MAP) {
    // add .keys, .values, .entries, [@@iterator]
    // 23.1.3.4, 23.1.3.8, 23.1.3.11, 23.1.3.12, 23.2.3.5, 23.2.3.8, 23.2.3.10, 23.2.3.11
    $iterDefine(C, NAME, function (iterated, kind) {
      this._t = validate(iterated, NAME); // target
      this._k = kind;                     // kind
      this._l = undefined;                // previous
    }, function () {
      var that = this;
      var kind = that._k;
      var entry = that._l;
      // revert to the last existing entry
      while (entry && entry.r) entry = entry.p;
      // get next entry
      if (!that._t || !(that._l = entry = entry ? entry.n : that._t._f)) {
        // or finish the iteration
        that._t = undefined;
        return step(1);
      }
      // return step by kind
      if (kind == 'keys') return step(0, entry.k);
      if (kind == 'values') return step(0, entry.v);
      return step(0, [entry.k, entry.v]);
    }, IS_MAP ? 'entries' : 'values', !IS_MAP, true);

    // add [@@species], 23.1.2.2, 23.2.2.2
    setSpecies(NAME);
  }
};


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(8) && !__webpack_require__(16)(function () {
  return Object.defineProperty(__webpack_require__(60)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(4);
var document = __webpack_require__(6).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(4);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(2);
var dPs = __webpack_require__(165);
var enumBugKeys = __webpack_require__(67);
var IE_PROTO = __webpack_require__(36)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(60)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(169).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(166);
var enumBugKeys = __webpack_require__(67);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 64 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 65 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 66 */
/***/ (function(module, exports) {

module.exports = false;


/***/ }),
/* 67 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 68 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 69 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

// 0 -> Array#forEach
// 1 -> Array#map
// 2 -> Array#filter
// 3 -> Array#some
// 4 -> Array#every
// 5 -> Array#find
// 6 -> Array#findIndex
var ctx = __webpack_require__(19);
var IObject = __webpack_require__(33);
var toObject = __webpack_require__(43);
var toLength = __webpack_require__(35);
var asc = __webpack_require__(184);
module.exports = function (TYPE, $create) {
  var IS_MAP = TYPE == 1;
  var IS_FILTER = TYPE == 2;
  var IS_SOME = TYPE == 3;
  var IS_EVERY = TYPE == 4;
  var IS_FIND_INDEX = TYPE == 6;
  var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
  var create = $create || asc;
  return function ($this, callbackfn, that) {
    var O = toObject($this);
    var self = IObject(O);
    var f = ctx(callbackfn, that, 3);
    var length = toLength(self.length);
    var index = 0;
    var result = IS_MAP ? create($this, length) : IS_FILTER ? create($this, 0) : undefined;
    var val, res;
    for (;length > index; index++) if (NO_HOLES || index in self) {
      val = self[index];
      res = f(val, index, O);
      if (TYPE) {
        if (IS_MAP) result[index] = res;   // map
        else if (res) switch (TYPE) {
          case 3: return true;             // some
          case 5: return val;              // find
          case 6: return index;            // findIndex
          case 2: result.push(val);        // filter
        } else if (IS_EVERY) return false; // every
      }
    }
    return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : result;
  };
};


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__(72));
__export(__webpack_require__(9));
var opaque_token_1 = __webpack_require__(74);
exports.OpaqueToken = opaque_token_1.OpaqueToken;
var forward_ref_1 = __webpack_require__(20);
exports.forwardRef = forward_ref_1.forwardRef;
var provider_1 = __webpack_require__(15);
exports.provide = provider_1.provide;
exports.getInjectableName = provider_1.getInjectableName;
//# sourceMappingURL=di.js.map

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var metadata_1 = __webpack_require__(9);
var decorators_1 = __webpack_require__(29);
/**
 * Factory for creating {@link InjectMetadata}.
 */
exports.Inject = decorators_1.makeParamDecorator(metadata_1.InjectMetadata, metadata_1.InjectMetadata.paramDecoratorForNonConstructor);
/**
 * Factory for creating {@link OptionalMetadata}.
 */
exports.Optional = decorators_1.makeParamDecorator(metadata_1.OptionalMetadata);
/**
 * Factory for creating {@link InjectableMetadata}.
 */
exports.Injectable = decorators_1.makeDecorator(metadata_1.InjectableMetadata);
/**
 * Factory for creating {@link SelfMetadata}.
 */
exports.Self = decorators_1.makeParamDecorator(metadata_1.SelfMetadata);
/**
 * Factory for creating {@link HostMetadata}.
 */
exports.Host = decorators_1.makeParamDecorator(metadata_1.HostMetadata);
/**
 * Factory for creating {@link SkipSelfMetadata}.
 */
exports.SkipSelf = decorators_1.makeParamDecorator(metadata_1.SkipSelfMetadata);
//# sourceMappingURL=decorators.js.map

/***/ }),
/* 73 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Creates a token that can be used in a DI Provider.
 *
 * ### Example ([live demo](http://plnkr.co/edit/Ys9ezXpj2Mnoy3Uc8KBp?p=preview))
 *
 * ```typescript
 * var t = new OpaqueToken("value");
 *
 * var injector = Injector.resolveAndCreate([
 *   provide(t, {useValue: "providedValue"})
 * ]);
 *
 * expect(injector.get(t)).toEqual("providedValue");
 * ```
 *
 * Using an `OpaqueToken` is preferable to using strings as tokens because of possible collisions
 * caused by multiple providers using the same string as two different tokens.
 *
 * Using an `OpaqueToken` is preferable to using an `Object` as tokens because it provides better
 * error messages.
 */
var OpaqueToken = (function () {
    function OpaqueToken(_desc) {
        this._desc = _desc;
    }
    Object.defineProperty(OpaqueToken.prototype, "desc", {
        get: function () { return this._desc; },
        enumerable: true,
        configurable: true
    });
    OpaqueToken.prototype.toString = function () { return "Token " + this._desc; };
    return OpaqueToken;
}());
exports.OpaqueToken = OpaqueToken;
//# sourceMappingURL=opaque_token.js.map

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var lang_1 = __webpack_require__(1);
var BaseException = (function (_super) {
    __extends(BaseException, _super);
    function BaseException(message) {
        if (message === void 0) { message = "--"; }
        var _this = _super.call(this, message) || this;
        _this.message = message;
        _this.stack = new Error(message).stack;
        return _this;
    }
    BaseException.prototype.toString = function () { return this.message; };
    return BaseException;
}(Error));
exports.BaseException = BaseException;
function getErrorMsg(typeOrFunc, msg) {
    return "\n      " + lang_1.getFuncName(typeOrFunc) + ":\n      ===========================\n      " + msg + "\n    ";
}
exports.getErrorMsg = getErrorMsg;
//# sourceMappingURL=exceptions.js.map

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

/**
 * @whatItDoes Represents a type that a Component or other object is instances of.
 *
 * @description
 *
 * An example of a `Type` is `MyCustomComponent` class, which in JavaScript is be represented by
 * the `MyCustomComponent` constructor function.
 *
 * @stable
 */
exports.Type = Function;
//# sourceMappingURL=type.js.map

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var decorators_1 = __webpack_require__(72);
var ChangeDetectorRef = ChangeDetectorRef_1 = (function () {
    function ChangeDetectorRef($scope) {
        this.$scope = $scope;
    }
    ChangeDetectorRef.create = function ($scope) {
        return new ChangeDetectorRef_1($scope);
    };
    /**
     * Marks all {@link ChangeDetectionStrategy#OnPush} ancestors as to be checked.
     *
     * <!-- TODO: Add a link to a chapter on OnPush components -->
     *
     * ### Example ([live demo](http://plnkr.co/edit/GC512b?p=preview))
     *
     * ```typescript
     * @Component({
     *   selector: 'cmp',
     *   changeDetection: ChangeDetectionStrategy.OnPush,
     *   template: `Number of ticks: {{numberOfTicks}}`
     * })
     * class Cmp {
     *   numberOfTicks = 0;
     *
     *   constructor(ref: ChangeDetectorRef) {
     *     setInterval(() => {
     *       this.numberOfTicks ++
     *       // the following is required, otherwise the view will not be updated
     *       this.ref.markForCheck();
     *     }, 1000);
     *   }
     * }
     *
     * @Component({
     *   selector: 'app',
     *   changeDetection: ChangeDetectionStrategy.OnPush,
     *   template: `
     *     <cmp><cmp>
     *   `,
     *   directives: [Cmp]
     * })
     * class App {
     * }
     *
     * bootstrap(App);
     * ```
     */
    ChangeDetectorRef.prototype.markForCheck = function () { this.$scope.$applyAsync(); };
    /**
     * Detaches the change detector from the change detector tree.
     *
     * The detached change detector will not be checked until it is reattached.
     *
     * This can also be used in combination with {@link ChangeDetectorRef#detectChanges} to implement
     * local change
     * detection checks.
     *
     * <!-- TODO: Add a link to a chapter on detach/reattach/local digest -->
     * <!-- TODO: Add a live demo once ref.detectChanges is merged into master -->
     *
     * ### Example
     *
     * The following example defines a component with a large list of readonly data.
     * Imagine the data changes constantly, many times per second. For performance reasons,
     * we want to check and update the list every five seconds. We can do that by detaching
     * the component's change detector and doing a local check every five seconds.
     *
     * ```typescript
     * class DataProvider {
     *   // in a real application the returned data will be different every time
     *   get data() {
     *     return [1,2,3,4,5];
     *   }
     * }
     *
     * @Component({
     *   selector: 'giant-list',
     *   template: `
     *     <li *ngFor="let d of dataProvider.data">Data {{d}}</lig>
     *   `,
     *   directives: [NgFor]
     * })
     * class GiantList {
     *   constructor(private ref: ChangeDetectorRef, private dataProvider:DataProvider) {
     *     ref.detach();
     *     setInterval(() => {
     *       this.ref.detectChanges();
     *     }, 5000);
     *   }
     * }
     *
     * @Component({
     *   selector: 'app',
     *   providers: [DataProvider],
     *   template: `
     *     <giant-list><giant-list>
     *   `,
     *   directives: [GiantList]
     * })
     * class App {
     * }
     *
     * bootstrap(App);
     * ```
     */
    ChangeDetectorRef.prototype.detach = function () { disconnectScope(this.$scope); };
    /**
     * Checks the change detector and its children.
     *
     * This can also be used in combination with {@link ChangeDetectorRef#detach} to implement local
     * change detection
     * checks.
     *
     * <!-- TODO: Add a link to a chapter on detach/reattach/local digest -->
     * <!-- TODO: Add a live demo once ref.detectChanges is merged into master -->
     *
     * ### Example
     *
     * The following example defines a component with a large list of readonly data.
     * Imagine, the data changes constantly, many times per second. For performance reasons,
     * we want to check and update the list every five seconds.
     *
     * We can do that by detaching the component's change detector and doing a local change detection
     * check
     * every five seconds.
     *
     * See {@link ChangeDetectorRef#detach} for more information.
     */
    ChangeDetectorRef.prototype.detectChanges = function () { this.$scope.$digest(); };
    /**
     * Checks the change detector and its children, and throws if any changes are detected.
     *
     * This is used in development mode to verify that running change detection doesn't introduce
     * other changes.
     */
    ChangeDetectorRef.prototype.checkNoChanges = function () { };
    /**
     * Reattach the change detector to the change detector tree.
     *
     * This also marks OnPush ancestors as to be checked. This reattached change detector will be
     * checked during the next change detection run.
     *
     * <!-- TODO: Add a link to a chapter on detach/reattach/local digest -->
     *
     * ### Example ([live demo](http://plnkr.co/edit/aUhZha?p=preview))
     *
     * The following example creates a component displaying `live` data. The component will detach
     * its change detector from the main change detector tree when the component's live property
     * is set to false.
     *
     * ```typescript
     * class DataProvider {
     *   data = 1;
     *
     *   constructor() {
     *     setInterval(() => {
     *       this.data = this.data * 2;
     *     }, 500);
     *   }
     * }
     *
     * @Component({
     *   selector: 'live-data',
     *   inputs: ['live'],
     *   template: `Data: {{dataProvider.data}}`
     * })
     * class LiveData {
     *   constructor(private ref: ChangeDetectorRef, private dataProvider:DataProvider) {}
     *
     *   set live(value) {
     *     if (value)
     *       this.ref.reattach();
     *     else
     *       this.ref.detach();
     *   }
     * }
     *
     * @Component({
     *   selector: 'app',
     *   providers: [DataProvider],
     *   template: `
     *     Live Update: <input type="checkbox" [(ngModel)]="live">
     *     <live-data [live]="live"><live-data>
     *   `,
     *   directives: [LiveData, FORM_DIRECTIVES]
     * })
     * class App {
     *   live = true;
     * }
     *
     * bootstrap(App);
     * ```
     */
    ChangeDetectorRef.prototype.reattach = function () { reconnectScope(this.$scope); };
    return ChangeDetectorRef;
}());
ChangeDetectorRef = ChangeDetectorRef_1 = __decorate([
    decorators_1.Injectable('changeDetectorRef'),
    __metadata("design:paramtypes", [Object])
], ChangeDetectorRef);
exports.ChangeDetectorRef = ChangeDetectorRef;
// Stop watchers and events from firing on a scope without destroying it,
// by disconnecting it from its parent and its siblings' linked lists.
function disconnectScope(scope) {
    if (!scope)
        return;
    // we can't destroy the root scope or a scope that has been already destroyed
    if (scope.$root === scope)
        return;
    if (scope.$$destroyed)
        return;
    var parent = scope.$parent;
    scope.$$disconnected = true;
    // See Scope.$destroy
    if (parent.$$childHead === scope)
        parent.$$childHead = scope.$$nextSibling;
    if (parent.$$childTail === scope)
        parent.$$childTail = scope.$$prevSibling;
    if (scope.$$prevSibling)
        scope.$$prevSibling.$$nextSibling = scope.$$nextSibling;
    if (scope.$$nextSibling)
        scope.$$nextSibling.$$prevSibling = scope.$$prevSibling;
    scope.$$nextSibling = scope.$$prevSibling = null;
}
// Undo the effects of disconnectScope above.
function reconnectScope(scope) {
    if (!scope)
        return;
    // we can't disconnect the root node or scope already disconnected
    if (scope.$root === scope)
        return;
    if (!scope.$$disconnected)
        return;
    var child = scope;
    var parent = child.$parent;
    child.$$disconnected = false;
    // See Scope.$new for this logic...
    child.$$prevSibling = parent.$$childTail;
    if (parent.$$childHead) {
        parent.$$childTail.$$nextSibling = child;
        parent.$$childTail = child;
    }
    else {
        parent.$$childHead = parent.$$childTail = child;
    }
}
var ChangeDetectorRef_1;
//# sourceMappingURL=change_detector_ref.js.map

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var UninitializedValue = (function () {
    function UninitializedValue() {
    }
    return UninitializedValue;
}());
exports.UninitializedValue = UninitializedValue;
var uninitialized = new UninitializedValue();
/**
 * Represents a basic change from a previous to a new value.
 */
var SimpleChange = (function () {
    function SimpleChange(previousValue, currentValue) {
        this.previousValue = previousValue;
        this.currentValue = currentValue;
    }
    /**
     * Check whether the new value is the first value assigned.
     */
    SimpleChange.prototype.isFirstChange = function () { return this.previousValue === uninitialized; };
    return SimpleChange;
}());
exports.SimpleChange = SimpleChange;
var ChangeDetectionUtil = (function () {
    function ChangeDetectionUtil() {
    }
    ChangeDetectionUtil.simpleChange = function (previousValue, currentValue) {
        return new SimpleChange(previousValue, currentValue);
    };
    ChangeDetectionUtil.isOnPushChangeDetectionStrategy = function (changeDetectionStrategy) {
        return lang_1.isPresent(changeDetectionStrategy) && changeDetectionStrategy === 0 /* OnPush */;
    };
    return ChangeDetectionUtil;
}());
ChangeDetectionUtil.uninitialized = uninitialized;
exports.ChangeDetectionUtil = ChangeDetectionUtil;
//# sourceMappingURL=change_detection_util.js.map

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var lang_1 = __webpack_require__(1);
var Subject_1 = __webpack_require__(212);
/**
 * Use by directives and components to emit custom Events.
 *
 * ### Examples
 *
 * In the following example, `ZippyComponent` alternatively emits `open` and `close` events when its
 * title gets clicked:
 *
 * ```
 * @Component({
 *   selector: 'zippy',
 *   template: `
 *   <div class="zippy">
 *     <div ng-click="$ctrl.toggle()">Toggle</div>
 *     <div ng-hide="!$ctrl.visible">
 *       <ng-transclude></ng-transclude>
 *     </div>
 *  </div>`
 * })
 * export class ZippyComponent {
 *   visible: boolean = true;
 *
 *   @Output() open = new EventEmitter<boolean>();
 *   @Output() close = new EventEmitter<boolean>();
 *
 *   toggle() {
 *     this.visible = !this.visible;
 *     if (this.visible) {
 *       this.open.emit( this.visible );
 *     } else {
 *       this.close.emit( this.visible );
 *     }
 *   }
 * }
 * ```
 *
 * Use Rx.Observable but provides an adapter to make it work as specified here:
 * https://github.com/jhusain/observable-spec
 *
 * Once a reference implementation of the spec is available, switch to it.
 */
var EventEmitter = (function (_super) {
    __extends(EventEmitter, _super);
    /**
     * Creates an instance of [EventEmitter], which depending on [isAsync],
     * delivers events synchronously or asynchronously.
     */
    function EventEmitter(isAsync) {
        if (isAsync === void 0) { isAsync = false; }
        var _this = _super.call(this) || this;
        /** @internal */
        _this._ngExpressionBindingCb = lang_1.noop;
        _this.__isAsync = isAsync;
        return _this;
    }
    /** @internal */
    EventEmitter.prototype.wrapNgExpBindingToEmitter = function (cb) {
        //used in reassignBindingsAndCreteEventEmitters to attach the original @Output binding to the instance new EventEmitter
        this._ngExpressionBindingCb = cb;
        // this could create memory leaks because the subscription would be never terminated
        // super.subscribe((newValue)=>this._ngExpressionBindingCb({$event:newValue}));
    };
    EventEmitter.prototype.emit = function (value) {
        var payload = { $event: value };
        // push just the value
        _super.prototype.next.call(this, value);
        // our & binding needs to be called via { $event: value } because Angular 1 locals
        this._ngExpressionBindingCb(payload);
    };
    EventEmitter.prototype.subscribe = function (generatorOrNext, error, complete) {
        var schedulerFn /** TODO #9100 */;
        var errorFn = function (err) { return null; };
        var completeFn = function () { return null; };
        if (generatorOrNext && typeof generatorOrNext === 'object') {
            schedulerFn = this.__isAsync
                ? function (value /** TODO #9100 */) { setTimeout(function () { return generatorOrNext.next(value); }); }
                : function (value /** TODO #9100 */) { generatorOrNext.next(value); };
            if (generatorOrNext.error) {
                errorFn = this.__isAsync
                    ? function (err) { setTimeout(function () { return generatorOrNext.error(err); }); }
                    : function (err) { generatorOrNext.error(err); };
            }
            if (generatorOrNext.complete) {
                completeFn = this.__isAsync
                    ? function () { setTimeout(function () { return generatorOrNext.complete(); }); }
                    : function () { generatorOrNext.complete(); };
            }
        }
        else {
            schedulerFn = this.__isAsync
                ? function (value /** TODO #9100 */) { setTimeout(function () { return generatorOrNext(value); }); }
                : function (value /** TODO #9100 */) { generatorOrNext(value); };
            if (error) {
                errorFn = this.__isAsync ? function (err) { setTimeout(function () { return error(err); }); } : function (err) { error(err); };
            }
            if (complete) {
                completeFn = this.__isAsync ? function () { setTimeout(function () { return complete(); }); } : function () { complete(); };
            }
        }
        return _super.prototype.subscribe.call(this, schedulerFn, errorFn, completeFn);
    };
    return EventEmitter;
}(Subject_1.Subject));
exports.EventEmitter = EventEmitter;
//# sourceMappingURL=async.js.map

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var isFunction_1 = __webpack_require__(81);
var Subscription_1 = __webpack_require__(49);
var Observer_1 = __webpack_require__(83);
var rxSubscriber_1 = __webpack_require__(50);
/**
 * Implements the {@link Observer} interface and extends the
 * {@link Subscription} class. While the {@link Observer} is the public API for
 * consuming the values of an {@link Observable}, all Observers get converted to
 * a Subscriber, in order to provide Subscription-like capabilities such as
 * `unsubscribe`. Subscriber is a common type in RxJS, and crucial for
 * implementing operators, but it is rarely used as a public API.
 *
 * @class Subscriber<T>
 */
var Subscriber = (function (_super) {
    __extends(Subscriber, _super);
    /**
     * @param {Observer|function(value: T): void} [destinationOrNext] A partially
     * defined Observer or a `next` callback function.
     * @param {function(e: ?any): void} [error] The `error` callback of an
     * Observer.
     * @param {function(): void} [complete] The `complete` callback of an
     * Observer.
     */
    function Subscriber(destinationOrNext, error, complete) {
        _super.call(this);
        this.syncErrorValue = null;
        this.syncErrorThrown = false;
        this.syncErrorThrowable = false;
        this.isStopped = false;
        switch (arguments.length) {
            case 0:
                this.destination = Observer_1.empty;
                break;
            case 1:
                if (!destinationOrNext) {
                    this.destination = Observer_1.empty;
                    break;
                }
                if (typeof destinationOrNext === 'object') {
                    if (destinationOrNext instanceof Subscriber) {
                        this.destination = destinationOrNext;
                        this.destination.add(this);
                    }
                    else {
                        this.syncErrorThrowable = true;
                        this.destination = new SafeSubscriber(this, destinationOrNext);
                    }
                    break;
                }
            default:
                this.syncErrorThrowable = true;
                this.destination = new SafeSubscriber(this, destinationOrNext, error, complete);
                break;
        }
    }
    Subscriber.prototype[rxSubscriber_1.$$rxSubscriber] = function () { return this; };
    /**
     * A static factory for a Subscriber, given a (potentially partial) definition
     * of an Observer.
     * @param {function(x: ?T): void} [next] The `next` callback of an Observer.
     * @param {function(e: ?any): void} [error] The `error` callback of an
     * Observer.
     * @param {function(): void} [complete] The `complete` callback of an
     * Observer.
     * @return {Subscriber<T>} A Subscriber wrapping the (partially defined)
     * Observer represented by the given arguments.
     */
    Subscriber.create = function (next, error, complete) {
        var subscriber = new Subscriber(next, error, complete);
        subscriber.syncErrorThrowable = false;
        return subscriber;
    };
    /**
     * The {@link Observer} callback to receive notifications of type `next` from
     * the Observable, with a value. The Observable may call this method 0 or more
     * times.
     * @param {T} [value] The `next` value.
     * @return {void}
     */
    Subscriber.prototype.next = function (value) {
        if (!this.isStopped) {
            this._next(value);
        }
    };
    /**
     * The {@link Observer} callback to receive notifications of type `error` from
     * the Observable, with an attached {@link Error}. Notifies the Observer that
     * the Observable has experienced an error condition.
     * @param {any} [err] The `error` exception.
     * @return {void}
     */
    Subscriber.prototype.error = function (err) {
        if (!this.isStopped) {
            this.isStopped = true;
            this._error(err);
        }
    };
    /**
     * The {@link Observer} callback to receive a valueless notification of type
     * `complete` from the Observable. Notifies the Observer that the Observable
     * has finished sending push-based notifications.
     * @return {void}
     */
    Subscriber.prototype.complete = function () {
        if (!this.isStopped) {
            this.isStopped = true;
            this._complete();
        }
    };
    Subscriber.prototype.unsubscribe = function () {
        if (this.closed) {
            return;
        }
        this.isStopped = true;
        _super.prototype.unsubscribe.call(this);
    };
    Subscriber.prototype._next = function (value) {
        this.destination.next(value);
    };
    Subscriber.prototype._error = function (err) {
        this.destination.error(err);
        this.unsubscribe();
    };
    Subscriber.prototype._complete = function () {
        this.destination.complete();
        this.unsubscribe();
    };
    Subscriber.prototype._unsubscribeAndRecycle = function () {
        var _a = this, _parent = _a._parent, _parents = _a._parents;
        this._parent = null;
        this._parents = null;
        this.unsubscribe();
        this.closed = false;
        this.isStopped = false;
        this._parent = _parent;
        this._parents = _parents;
        return this;
    };
    return Subscriber;
}(Subscription_1.Subscription));
exports.Subscriber = Subscriber;
/**
 * We need this JSDoc comment for affecting ESDoc.
 * @ignore
 * @extends {Ignored}
 */
var SafeSubscriber = (function (_super) {
    __extends(SafeSubscriber, _super);
    function SafeSubscriber(_parentSubscriber, observerOrNext, error, complete) {
        _super.call(this);
        this._parentSubscriber = _parentSubscriber;
        var next;
        var context = this;
        if (isFunction_1.isFunction(observerOrNext)) {
            next = observerOrNext;
        }
        else if (observerOrNext) {
            context = observerOrNext;
            next = observerOrNext.next;
            error = observerOrNext.error;
            complete = observerOrNext.complete;
            if (isFunction_1.isFunction(context.unsubscribe)) {
                this.add(context.unsubscribe.bind(context));
            }
            context.unsubscribe = this.unsubscribe.bind(this);
        }
        this._context = context;
        this._next = next;
        this._error = error;
        this._complete = complete;
    }
    SafeSubscriber.prototype.next = function (value) {
        if (!this.isStopped && this._next) {
            var _parentSubscriber = this._parentSubscriber;
            if (!_parentSubscriber.syncErrorThrowable) {
                this.__tryOrUnsub(this._next, value);
            }
            else if (this.__tryOrSetError(_parentSubscriber, this._next, value)) {
                this.unsubscribe();
            }
        }
    };
    SafeSubscriber.prototype.error = function (err) {
        if (!this.isStopped) {
            var _parentSubscriber = this._parentSubscriber;
            if (this._error) {
                if (!_parentSubscriber.syncErrorThrowable) {
                    this.__tryOrUnsub(this._error, err);
                    this.unsubscribe();
                }
                else {
                    this.__tryOrSetError(_parentSubscriber, this._error, err);
                    this.unsubscribe();
                }
            }
            else if (!_parentSubscriber.syncErrorThrowable) {
                this.unsubscribe();
                throw err;
            }
            else {
                _parentSubscriber.syncErrorValue = err;
                _parentSubscriber.syncErrorThrown = true;
                this.unsubscribe();
            }
        }
    };
    SafeSubscriber.prototype.complete = function () {
        if (!this.isStopped) {
            var _parentSubscriber = this._parentSubscriber;
            if (this._complete) {
                if (!_parentSubscriber.syncErrorThrowable) {
                    this.__tryOrUnsub(this._complete);
                    this.unsubscribe();
                }
                else {
                    this.__tryOrSetError(_parentSubscriber, this._complete);
                    this.unsubscribe();
                }
            }
            else {
                this.unsubscribe();
            }
        }
    };
    SafeSubscriber.prototype.__tryOrUnsub = function (fn, value) {
        try {
            fn.call(this._context, value);
        }
        catch (err) {
            this.unsubscribe();
            throw err;
        }
    };
    SafeSubscriber.prototype.__tryOrSetError = function (parent, fn, value) {
        try {
            fn.call(this._context, value);
        }
        catch (err) {
            parent.syncErrorValue = err;
            parent.syncErrorThrown = true;
            return true;
        }
        return false;
    };
    SafeSubscriber.prototype._unsubscribe = function () {
        var _parentSubscriber = this._parentSubscriber;
        this._context = null;
        this._parentSubscriber = null;
        _parentSubscriber.unsubscribe();
    };
    return SafeSubscriber;
}(Subscriber));
//# sourceMappingURL=Subscriber.js.map

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function isFunction(x) {
    return typeof x === 'function';
}
exports.isFunction = isFunction;
//# sourceMappingURL=isFunction.js.map

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// typeof any so that it we don't have to cast when comparing a result to the error object
exports.errorObject = { e: {} };
//# sourceMappingURL=errorObject.js.map

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.empty = {
    closed: true,
    next: function (value) { },
    error: function (err) { throw err; },
    complete: function () { }
};
//# sourceMappingURL=Observer.js.map

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.INPUT_MODE_REGEX = /^(<|=|@)?(\??)(\w*)$/;
exports.BINDING_MODE = Object.freeze({
    oneWay: '<',
    twoWay: '=',
    output: '&',
    attr: '@',
    optional: '?'
});
//# sourceMappingURL=constants.js.map

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var _kebabCase = _caseTransformerFactory('-');
var _snakeCase = _caseTransformerFactory('_');
var StringWrapper = (function () {
    function StringWrapper() {
    }
    StringWrapper.fromCharCode = function (code) { return String.fromCharCode(code); };
    StringWrapper.charCodeAt = function (s, index) { return s.charCodeAt(index); };
    StringWrapper.split = function (s, regExp) { return s.split(regExp); };
    StringWrapper.equals = function (s, s2) { return s === s2; };
    StringWrapper.stripLeft = function (s, charVal) {
        if (s && s.length) {
            var pos = 0;
            for (var i = 0; i < s.length; i++) {
                if (s[i] != charVal)
                    break;
                pos++;
            }
            s = s.substring(pos);
        }
        return s;
    };
    StringWrapper.stripRight = function (s, charVal) {
        if (s && s.length) {
            var pos = s.length;
            for (var i = s.length - 1; i >= 0; i--) {
                if (s[i] != charVal)
                    break;
                pos--;
            }
            s = s.substring(0, pos);
        }
        return s;
    };
    StringWrapper.replace = function (s, from, replace) {
        return s.replace(from, replace);
    };
    StringWrapper.replaceAll = function (s, from, replace) {
        return s.replace(from, replace);
    };
    StringWrapper.slice = function (s, from, to) {
        if (from === void 0) { from = 0; }
        if (to === void 0) { to = null; }
        return s.slice(from, to === null
            ? undefined
            : to);
    };
    StringWrapper.replaceAllMapped = function (s, from, cb) {
        return s.replace(from, function () {
            var matches = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                matches[_i] = arguments[_i];
            }
            // Remove offset & string from the result array
            matches.splice(-2, 2);
            // The callback receives match, p1, ..., pn
            return cb(matches);
        });
    };
    StringWrapper.compare = function (a, b) {
        if (a < b) {
            return -1;
        }
        else if (a > b) {
            return 1;
        }
        else {
            return 0;
        }
    };
    StringWrapper.includes = function (str, searchString, position) {
        if (position === void 0) { position = 0; }
        if (String.prototype.includes) {
            return str.includes(searchString, position);
        }
        return str.indexOf(searchString, position) === position;
    };
    StringWrapper.startsWith = function (str, searchString, position) {
        if (position === void 0) { position = 0; }
        if (String.prototype.startsWith) {
            return str.startsWith(searchString, position);
        }
        return str.indexOf(searchString, position) === position;
    };
    StringWrapper.endsWith = function (str, searchString, position) {
        if (String.prototype.endsWith) {
            return str.endsWith(searchString, position);
        }
        var subjectString = str.toString();
        if (!lang_1.isNumber(position) || !isFinite(position)
            || Math.floor(position) !== position || position > subjectString.length) {
            position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.indexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
    };
    StringWrapper.kebabCase = function (name) {
        return _kebabCase(name);
    };
    StringWrapper.snakeCase = function (name) {
        return _snakeCase(name);
    };
    return StringWrapper;
}());
exports.StringWrapper = StringWrapper;
function _caseTransformerFactory(separator) {
    var SNAKE_CASE_REGEXP = /[A-Z]/g;
    return _caseTransform;
    function _caseTransform(name) {
        return name.replace(SNAKE_CASE_REGEXP, function (match, offset) {
            return (offset
                ? separator
                : '') + match.toLowerCase();
        });
    }
}
//# sourceMappingURL=primitives.js.map

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var decorators_1 = __webpack_require__(29);
var metadata_1 = __webpack_require__(45);
exports.Pipe = decorators_1.makeDecorator(metadata_1.PipeMetadata);
//# sourceMappingURL=decorators.js.map

/***/ }),
/* 87 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 88 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 89 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var decorators_1 = __webpack_require__(31);
var NgForm = (function () {
    function NgForm() {
    }
    NgForm.prototype.$addControl = function (control) { };
    NgForm.prototype.$removeControl = function (control) { };
    NgForm.prototype.$setValidity = function (validationErrorKey, isValid, control) { };
    NgForm.prototype.$setDirty = function () { };
    NgForm.prototype.$setPristine = function () { };
    NgForm.prototype.$commitViewValue = function () { };
    NgForm.prototype.$rollbackViewValue = function () { };
    NgForm.prototype.$setSubmitted = function () { };
    NgForm.prototype.$setUntouched = function () { };
    return NgForm;
}());
NgForm = __decorate([
    decorators_1.Directive({ selector: 'form' }),
    __metadata("design:paramtypes", [])
], NgForm);
exports.NgForm = NgForm;
//# sourceMappingURL=ng_form.js.map

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var decorators_1 = __webpack_require__(31);
var NgModel = (function () {
    function NgModel() {
    }
    NgModel.prototype.$render = function () {
    };
    NgModel.prototype.$setValidity = function (validationErrorKey, isValid) {
    };
    NgModel.prototype.$setViewValue = function (value, trigger) {
    };
    NgModel.prototype.$setPristine = function () {
    };
    NgModel.prototype.$setDirty = function () {
    };
    NgModel.prototype.$validate = function () {
    };
    NgModel.prototype.$setTouched = function () {
    };
    NgModel.prototype.$setUntouched = function () {
    };
    NgModel.prototype.$rollbackViewValue = function () {
    };
    NgModel.prototype.$commitViewValue = function () {
    };
    NgModel.prototype.$isEmpty = function (value) {
        return undefined;
    };
    return NgModel;
}());
NgModel = __decorate([
    decorators_1.Directive({ selector: '[ng-model]' }),
    __metadata("design:paramtypes", [])
], NgModel);
exports.NgModel = NgModel;
//# sourceMappingURL=ng_model.js.map

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var decorators_1 = __webpack_require__(31);
var NgSelect = (function () {
    function NgSelect() {
    }
    NgSelect.prototype.renderUnknownOption = function (val) { };
    NgSelect.prototype.removeUnknownOption = function () { };
    // Write the value to the select control, the implementation of this changes depending
    // upon whether the select can have multiple values and whether ngOptions is at work.
    NgSelect.prototype.writeValue = function (value) { };
    // Tell the select control that an option, with the given value, has been added
    NgSelect.prototype.addOption = function (value, element) { };
    // Tell the select control that an option, with the given value, has been removed
    NgSelect.prototype.removeOption = function (value) { };
    NgSelect.prototype.registerOption = function (optionScope, optionElement, optionAttrs, interpolateValueFn, interpolateTextFn) { };
    return NgSelect;
}());
NgSelect = __decorate([
    decorators_1.Directive({ selector: 'select' }),
    __metadata("design:paramtypes", [])
], NgSelect);
exports.NgSelect = NgSelect;
//# sourceMappingURL=ng_select.js.map

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var decorators_1 = __webpack_require__(86);
var lang_1 = __webpack_require__(1);
/**
 * Based on @cvuorinen angular1-async-filter implementation
 * @link https://github.com/cvuorinen/angular1-async-filter
 *
 * The `async` pipe subscribes to an `Observable` or `Promise` and returns the latest value it has emitted.
 * When a new value is emitted, the `async` pipe marks the component to be checked for changes. ( runs $scope.$digest() )
 * When the component gets destroyed, the `async` pipe unsubscribes automatically to avoid potential memory leaks.
 *
 * ## Usage
 *
 *  object | async // for non observable
 *  object | async:this // for observable
 *
 * where:
 *  - `object` is one of type `Observable`, `Promise`, 'ng.IPromise' or 'ng.IHttpPromise'
 *  - `this` is pipe parameter ( in angular 1 reference to local $Scope ( we need for Observable disposal )
 *
 *  If you are using async with observables nad you don't provide scope we will throw Error to let you know that you forgot `this`, #perfmatters baby!
 *
 * ## Examples
 *
 * This example binds a `Promise` to the view. Clicking the `Resolve` button resolves the promise.
 *
 * {@example core/pipes/ts/async_pipe/async_pipe_example.ts region='AsyncPipePromise'}
 *
 * It's also possible to use `async` with Observables. The example below binds the `time` Observable
 * to the view. Every 500ms, the `time` Observable updates the view with the current time.
 *
 * {@example core/pipes/ts/async_pipe/async_pipe_example.ts region='AsyncPipeObservable'}
 */
var AsyncPipe = AsyncPipe_1 = (function () {
    function AsyncPipe() {
    }
    AsyncPipe._objectId = function (obj) {
        if (!obj.hasOwnProperty(AsyncPipe_1.TRACK_PROP_NAME)) {
            obj[AsyncPipe_1.TRACK_PROP_NAME] = ++AsyncPipe_1.nextObjectID;
        }
        return obj[AsyncPipe_1.TRACK_PROP_NAME];
    };
    AsyncPipe._getSubscriptionStrategy = function (input) {
        return input.subscribe && input.subscribe.bind(input)
            || input.success && input.success.bind(input) // To make it work with HttpPromise
            || input.then.bind(input); // To make it work with Promise
    };
    AsyncPipe._markForCheck = function (scope) {
        if (lang_1.isScope(scope)) {
            // #perfmatters
            // wait till event loop is free and run just local digest so we don't get in conflict with other local $digest
            setTimeout(function () { return scope.$digest(); });
        }
    };
    AsyncPipe._dispose = function (inputId) {
        if (lang_1.isSubscription(AsyncPipe_1.subscriptions[inputId])) {
            AsyncPipe_1.subscriptions[inputId].unsubscribe();
        }
        delete AsyncPipe_1.subscriptions[inputId];
        delete AsyncPipe_1.values[inputId];
    };
    AsyncPipe.prototype.transform = function (input, scope) {
        if (!lang_1.isPromiseOrObservable(input)) {
            return input;
        }
        if (lang_1.isObservable(input) && !lang_1.isScope(scope)) {
            throw new Error('AsyncPipe: you have to specify "this" as parameter so we can unsubscribe on scope.$destroy!');
        }
        var inputId = AsyncPipe_1._objectId(input);
        // return cached immediately
        if (inputId in AsyncPipe_1.subscriptions) {
            return AsyncPipe_1.values[inputId];
        }
        var subscriptionStrategy = AsyncPipe_1._getSubscriptionStrategy(input);
        AsyncPipe_1.subscriptions[inputId] = subscriptionStrategy(_setSubscriptionValue);
        if (lang_1.isScope(scope)) {
            // Clean up subscription and its last value when the scope is destroyed.
            scope.$on('$destroy', function () { AsyncPipe_1._dispose(inputId); });
        }
        function _setSubscriptionValue(value) {
            AsyncPipe_1.values[inputId] = value;
            // this is needed only for Observables
            AsyncPipe_1._markForCheck(scope);
        }
    };
    return AsyncPipe;
}());
// Need a way to tell the input objects apart from each other (so we only subscribe to them once)
AsyncPipe.nextObjectID = 0;
AsyncPipe.values = {};
AsyncPipe.subscriptions = {};
AsyncPipe.TRACK_PROP_NAME = '__asyncFilterObjectID__';
AsyncPipe = AsyncPipe_1 = __decorate([
    decorators_1.Pipe({ name: 'async' /*, pure: false*/ }),
    __metadata("design:paramtypes", [])
], AsyncPipe);
exports.AsyncPipe = AsyncPipe;
var AsyncPipe_1;
//# sourceMappingURL=async_pipe.js.map

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var whoExclusionList_service_1 = __webpack_require__(54);
var EnableWhoDetectionDialog = /** @class */ (function () {
    function EnableWhoDetectionDialog(getTextService, swDemoService, scmWhoExclusionListService, xuiToastService, scmModalService, scmLocationService) {
        var _this = this;
        this.getTextService = getTextService;
        this.swDemoService = swDemoService;
        this.scmWhoExclusionListService = scmWhoExclusionListService;
        this.xuiToastService = xuiToastService;
        this.scmModalService = scmModalService;
        this.scmLocationService = scmLocationService;
        this.whoDetectionEnabled = new core_1.EventEmitter();
        this._t = getTextService;
        this.scmLocationService.getHelpLink("OrionSCMEnableRealTimeFileMonitoring").then(function (value) {
            return _this.helpLink = value;
        });
    }
    /**
     * Register callbacks when Enable Who Detection is approved - used for refreshing multiple widgets
     */
    EnableWhoDetectionDialog.prototype.onWhoDetectionEnabled = function (callback) {
        this.whoDetectionEnabled.subscribe(callback);
    };
    EnableWhoDetectionDialog.prototype.showConfirmDialog = function (nodeId) {
        var _this = this;
        return this.scmModalService.showModal({
            template: __webpack_require__(136),
            size: "md",
        }, {
            title: this._t("Enable 'who made the change' detection?"),
            cancelButtonText: this._t("Cancel"),
            viewModel: { helpLink: this.helpLink },
            buttons: [
                {
                    name: "node-who-detection-enable",
                    text: this._t("Enable Who Detection"),
                    isPrimary: true,
                    action: function (dialog) {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                            return;
                        }
                        if (dialog.$valid) {
                            _this.scmWhoExclusionListService.removeFromExclusionList([nodeId]).then(function () {
                                var toastMsg = _this._t("'Who made the change' detection was enabled for this node.");
                                _this.xuiToastService.success(toastMsg);
                                _this.whoDetectionEnabled.emit(true);
                            });
                            return true;
                        }
                        return false;
                    },
                    isHidden: false
                }
            ]
        }).then(function (result) {
            if (result !== "cancel") {
                return true;
            }
        });
    };
    EnableWhoDetectionDialog = __decorate([
        core_1.Injectable("scmEnableWhoDetectionDialog"),
        __param(0, core_1.Inject("getTextService")),
        __param(1, core_1.Inject("swDemoService")),
        __param(2, core_1.Inject(whoExclusionList_service_1.WhoExclusionListService)),
        __param(3, core_1.Inject("xuiToastService")),
        __param(4, core_1.Inject("scmModalService")),
        __param(5, core_1.Inject("scmLocationService"))
    ], EnableWhoDetectionDialog);
    return EnableWhoDetectionDialog;
}());
exports.EnableWhoDetectionDialog = EnableWhoDetectionDialog;


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(264));
__export(__webpack_require__(265));
__export(__webpack_require__(266));


/***/ }),
/* 96 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 97 */
/***/ (function(module, exports) {

module.exports = "<div class=scm-content-message-popover-text> <div ng-if=\"::contentMessagePopover.contentCollectionState === 'collectcontentdisabled'\" _t> Line-by-line comparison is not available for this configuration item because content downloading is disabled. You can change this setting in the profile definition. </div> <div ng-if=\"::contentMessagePopover.contentCollectionState === 'sizelimitexceeded'\" _t=\"['{{::contentMessagePopover.contentSizeLimit}}']\"> Line-by-line comparison is not available for this configuration item because it exceeds {0} MBs. </div> </div> ";

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ProfileService = /** @class */ (function () {
    /** @ngInject */
    ProfileService.$inject = ["scmApi"];
    function ProfileService(scmApi) {
        this.scmApi = scmApi;
    }
    ProfileService.prototype.getAll = function () {
        return this.scmApi
            .all("profiles")
            .getList();
    };
    ProfileService.prototype.getServerConfigurationIdsForProfile = function (profileId) {
        return this.scmApi
            .one("profiles", profileId)
            .all("nodeIds")
            .getList();
    };
    ProfileService.prototype.assignToNodes = function (assignments) {
        return this.scmApi
            .all("profiles/assignToNodes")
            .post(assignments);
    };
    ProfileService.prototype.unassignFromNodes = function (deletion) {
        return this.scmApi
            .all("profiles/unassignFromNodes")
            .post(deletion);
    };
    ProfileService.prototype.unassignAllFromNodes = function (nodesToUnassign) {
        return this.scmApi
            .all("profiles/unassignAllFromNodes")
            .post(nodesToUnassign);
    };
    /** it has swAlertOnError because it may return application custom error codes */
    ProfileService.prototype.createProfile = function (profile) {
        return this.scmApi
            .all("profiles/addProfile")
            .post(profile, {
            swAlertOnError: false
        });
    };
    ProfileService.prototype.createProfiles = function (profiles) {
        return this.scmApi
            .all("profiles/addProfiles")
            .post(profiles, {
            swAlertOnError: false
        });
    };
    ProfileService.prototype.saveProfile = function (profile) {
        return this.scmApi
            .one("profiles", profile.profile.id)
            .customPUT(profile, undefined, {
            swAlertOnError: false
        });
    };
    ProfileService.prototype.deleteProfiles = function (ids) {
        return this.scmApi
            .all("profiles/deleteProfiles")
            .post(ids);
    };
    ProfileService.prototype.exportProfiles = function (profileIds) {
        window.location.href = "/api2/scm/profiles/export/" + profileIds.join(",") + "/date/" + moment(new Date).format("YYYYMMDDhhmmss");
    };
    ProfileService.prototype.importProfile = function (profileFiles) {
        var formData = new FormData();
        for (var i = 0; i < profileFiles.length; i++) {
            formData.append(profileFiles[i].name, profileFiles[i]);
        }
        return this.scmApi
            .one("profiles/import")
            .customPOST(formData, "", undefined, {
            "Content-Type": undefined
        });
    };
    return ProfileService;
}());
exports.ProfileService = ProfileService;


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ProfileElementService = /** @class */ (function () {
    /** @ngInject */
    ProfileElementService.$inject = ["scmApi"];
    function ProfileElementService(scmApi) {
        this.scmApi = scmApi;
    }
    ProfileElementService.prototype.getForProfile = function (profileId) {
        return this.scmApi
            .one("profiles", profileId)
            .all("profileElements")
            .getList();
    };
    ProfileElementService.prototype.getConnectionStrings = function () {
        return this.scmApi
            .all("profiles")
            .all("connectionStrings")
            .getList();
    };
    ProfileElementService.prototype.getForInvalidConfigurationIssues = function (profileId, nodeId) {
        return this.scmApi
            .one("profiles", profileId)
            .one("nodeId", nodeId)
            .one("incompatibleConfigurationIssues")
            .get();
    };
    ProfileElementService.prototype.getForUnableToFindCredentialsIssues = function (profileId, nodeId) {
        return this.scmApi
            .one("profiles", profileId)
            .one("nodeId", nodeId)
            .one("missingCredentialsIssues")
            .get();
    };
    return ProfileElementService;
}());
exports.ProfileElementService = ProfileElementService;


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var PolledElementService = /** @class */ (function () {
    /** @ngInject */
    PolledElementService.$inject = ["scmApi"];
    function PolledElementService(scmApi) {
        this.scmApi = scmApi;
    }
    PolledElementService.prototype.getPolledElement = function (polledElementId) {
        return this.scmApi
            .one("polledElement", polledElementId)
            .get();
    };
    PolledElementService.prototype.updatePolledElementEncoding = function (polledElementId, encoding) {
        return this.scmApi
            .one("polledElement", polledElementId)
            .post(null, null, { encoding: encoding });
    };
    return PolledElementService;
}());
exports.PolledElementService = PolledElementService;


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionNodeService = /** @class */ (function () {
    /** @ngInject */
    OrionNodeService.$inject = ["swApi", "scmApi"];
    function OrionNodeService(swApi, scmApi) {
        this.swApi = swApi;
        this.scmApi = scmApi;
    }
    OrionNodeService.prototype.getAll = function () {
        return this.scmApi
            .one("nodes/orion")
            .get();
    };
    OrionNodeService.prototype.getProfiles = function (nodeId) {
        return this.scmApi
            .one("nodes", nodeId)
            .all("profiles")
            .getList();
    };
    OrionNodeService.prototype.getProfileHeaders = function (nodeId) {
        return this.scmApi
            .one("nodes", nodeId)
            .all("profileHeaders")
            .getList();
    };
    OrionNodeService.prototype.enableAi = function (nodeIds) {
        return this.invokeSwisVerb("Orion.AssetInventory.Polling", "EnablePollingForNodes", [nodeIds.join(";")]);
    };
    OrionNodeService.prototype.invokeScmPollNow = function (nodeIds) {
        return this.invokeSwisVerb("Orion.SCM.ServerConfiguration", "PollNow", [nodeIds.join(";")]);
    };
    OrionNodeService.prototype.invokeSwisVerb = function (entity, verb, parameters) {
        // for reference of parameter passing see:
        // https://bitbucket.solarwinds.com/projects/ORION/repos/core/browse/Src/Web/Orion/Services/Information.asmx#307
        var payload = { entity: entity, verb: verb, param: parameters };
        return this.swApi.ws.one("Information.asmx").post("Invoke", payload, undefined, undefined);
    };
    OrionNodeService.prototype.getNodeNames = function (nodeIds) {
        return this.scmApi
            .one("nodes/nodenames")
            .get({
            "nodeIds": nodeIds
        });
    };
    OrionNodeService.prototype.getNode = function (nodeId) {
        return this.scmApi
            .one("nodes", nodeId)
            .get();
    };
    return OrionNodeService;
}());
exports.OrionNodeService = OrionNodeService;


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var CandidatesForMonitoringService = /** @class */ (function () {
    /** @ngInject */
    CandidatesForMonitoringService.$inject = ["scmApi"];
    function CandidatesForMonitoringService(scmApi) {
        this.scmApi = scmApi;
    }
    CandidatesForMonitoringService.prototype.getData = function () {
        return this.scmApi
            .one("candidatesForMonitoring")
            .get();
    };
    CandidatesForMonitoringService.prototype.dismiss = function (candidatesForMonitoring) {
        return this.scmApi
            .all("candidatesForMonitoring/dismiss")
            .post(candidatesForMonitoring);
    };
    return CandidatesForMonitoringService;
}());
exports.CandidatesForMonitoringService = CandidatesForMonitoringService;


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var CompareConfigService = /** @class */ (function () {
    /** @ngInject */
    CompareConfigService.$inject = ["scmApi"];
    function CompareConfigService(scmApi) {
        this.scmApi = scmApi;
    }
    CompareConfigService.prototype.getConfigDiff = function (node, leftTime, rightTime) {
        return this.scmApi
            .one("server-config-diff")
            .one("src-node", node)
            .one("dst-node", node)
            .get({
            sourceTime: leftTime.toISOString(),
            destinationTime: rightTime.toISOString()
        });
    };
    return CompareConfigService;
}());
exports.CompareConfigService = CompareConfigService;


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ProfileTreeSessionService = /** @class */ (function () {
    function ProfileTreeSessionService() {
    }
    ProfileTreeSessionService.prototype.saveSession = function (nodeId, state) {
        for (var property in state) {
            if (state.hasOwnProperty(property)) {
                sessionStorage.setItem("scm-node-" + nodeId + "-config-" + property, angular.toJson(state[property]));
            }
        }
    };
    ProfileTreeSessionService.prototype.loadSession = function (nodeId, properties) {
        if (!properties) {
            // return all
            return {
                profiles: angular.fromJson(sessionStorage.getItem("scm-node-" + nodeId + "-config-profiles")),
                filters: angular.fromJson(sessionStorage.getItem("scm-node-" + nodeId + "-config-filters"))
            };
        }
        else if (typeof properties === "string") {
            // return only one demanded property
            return _a = {},
                _a[properties] = angular.fromJson(sessionStorage.getItem("scm-node-" + nodeId + "-config-" + properties)),
                _a;
        }
        else if (angular.isArray(properties)) {
            // return session with multiple properties
            return _(properties).reduce(function (session, property) {
                return _.assign(session, (_a = {},
                    _a[property] = angular.fromJson(sessionStorage.getItem("scm-node-" + nodeId + "-config-" + property)),
                    _a));
                var _a;
            }, {});
        }
        throw new Error("loadSession was called with unknown parameters");
        var _a;
    };
    ProfileTreeSessionService.prototype.updateProfileTreeExpansion = function (target, source) {
        _(target).each(function (profile) {
            var savedProfile = _(source).find(function (item) { return item.profileId === profile.profileId; });
            if (savedProfile) {
                profile.expanded = savedProfile.expanded;
            }
        });
    };
    return ProfileTreeSessionService;
}());
exports.ProfileTreeSessionService = ProfileTreeSessionService;


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(22);
/**
 * This class disable reload by removing "data-resource-loading-mode" from all resources
 * according to https://cp.solarwinds.com/display/OrionPlatformRD/Asynchronous+resources
 * "The view will switch to async mode ONLY IF all the resources on the view are not Synchronous" - and in sync mode there is reloaded whole page
 * So we must find all those attributes, temporary set them "Disabled" value and enable return original value back
 *
 * Orion Core sources where this is handled: https://bitbucket.solarwinds.com/projects/ORION/repos/core/browse/Src/Web/Orion/js/AsyncView.js#61
 */
var ModalService = /** @class */ (function () {
    /** @ngInject */
    ModalService.$inject = ["xuiDialogService", "xuiWizardDialogService"];
    function ModalService(xuiDialogService, xuiWizardDialogService) {
        this.xuiDialogService = xuiDialogService;
        this.xuiWizardDialogService = xuiWizardDialogService;
    }
    ModalService.prototype.showModal = function (customSettings, dialogOptions) {
        return this.handleXuiDialog(this.xuiDialogService.showModal(customSettings, dialogOptions));
    };
    ModalService.prototype.showMessage = function (dialogText) {
        return this.handleXuiDialog(this.xuiDialogService.showMessage(dialogText));
    };
    ModalService.prototype.showWarning = function (dialogText, actionButton) {
        return this.handleXuiDialog(this.xuiDialogService.showWarning(dialogText, actionButton));
    };
    ModalService.prototype.showError = function (dialogText) {
        return this.handleXuiDialog(this.xuiDialogService.showError(dialogText));
    };
    ModalService.prototype.showModalWizardDialog = function (options) {
        return this.handleXuiDialog(this.xuiWizardDialogService.showModal(options));
    };
    ModalService.prototype.handleXuiDialog = function (showPromise) {
        this.disableReload();
        return showPromise
            .catch(function (reason) {
            if (reason === constants_1.default.EscapeKeyPress) {
                return "cancel";
            }
            else {
                throw reason;
            }
            ;
        })
            .finally(this.enableReload);
    };
    ModalService.prototype.disableReload = function () {
        $("[data-resource-loading-mode]").each(function () {
            var originalValue = $(this).attr("data-resource-loading-mode");
            $(this).attr("data-resource-loading-mode", "Disabled");
            $(this).attr("data-resource-loading-mode-temp", originalValue);
        });
    };
    ModalService.prototype.enableReload = function () {
        $("[data-resource-loading-mode='Disabled']").each(function () {
            var originalValue = $(this).attr("data-resource-loading-mode-temp");
            $(this).attr("data-resource-loading-mode", originalValue);
            $(this).removeAttr("data-resource-loading-mode-temp");
        });
    };
    return ModalService;
}());
exports.ModalService = ModalService;


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var BaselineService = /** @class */ (function () {
    /** @ngInject */
    BaselineService.$inject = ["scmApi", "getTextService", "xuiToastService", "scmModalService", "scmLocationService", "swUtil", "scmOrionNodeService", "scmDatetimeFilter", "scmApiHelperService"];
    function BaselineService(scmApi, getTextService, xuiToastService, scmModalService, scmLocationService, swUtil, scmOrionNodeService, scmDatetimeFilter, scmApiHelperService) {
        this.scmApi = scmApi;
        this.getTextService = getTextService;
        this.xuiToastService = xuiToastService;
        this.scmModalService = scmModalService;
        this.scmLocationService = scmLocationService;
        this.swUtil = swUtil;
        this.scmOrionNodeService = scmOrionNodeService;
        this.scmDatetimeFilter = scmDatetimeFilter;
        this.scmApiHelperService = scmApiHelperService;
        this._t = getTextService;
    }
    BaselineService.prototype.getBaseline = function (nodeId) {
        return this.scmApi
            .one("baseline/nodeId", nodeId)
            .get();
    };
    BaselineService.prototype.saveBaseline = function (nodeId, time) {
        var _this = this;
        var baseline = {
            id: 0,
            nodeId: nodeId,
            timestamp: time
        };
        return this.scmApi
            .all("baseline/setBaseline")
            .post(baseline)
            .then(function (value) { return _this.scmApiHelperService.handleNumericValue(value); });
    };
    BaselineService.prototype.setBaseline = function (nodeId, date) {
        var _this = this;
        return this.saveBaseline(nodeId, date).then(function (baselineId) {
            var baseline = {
                id: baselineId,
                nodeId: nodeId,
                timestamp: date
            };
            _this.xuiToastService.success(_this.swUtil.formatString(_this._t("Baseline successfully defined to {0}."), _this.scmDatetimeFilter(baseline.timestamp, "long")));
            return baseline;
        }).catch(function (error) {
            if (error.data.message === "There are not related poll data at given time") {
                _this.xuiToastService.error(_this._t("Baseline was not defined because no configuration items have been polled yet."));
            }
            else {
                _this.xuiToastService.error(_this._t("Baseline was not defined."));
            }
            return null;
        });
    };
    BaselineService.prototype.saveUpdatedBaseline = function (baseline, time) {
        var newBaseline = {
            id: baseline.id,
            nodeId: baseline.nodeId,
            timestamp: time
        };
        return this.scmApi
            .all("baseline/updateBaseline")
            .post(newBaseline);
    };
    BaselineService.prototype.updateBaseline = function (baseline, date) {
        var _this = this;
        return this.saveUpdatedBaseline(baseline, date).then(function () {
            _this.xuiToastService.success(_this.swUtil.formatString(_this._t("Baseline successfully re-defined to {0}."), _this.scmDatetimeFilter(date, "long")));
            return true;
        }).catch(function () {
            return false;
        });
    };
    BaselineService.prototype.showUpdateBaselineDialog = function (date, baseline) {
        var _this = this;
        return this.scmOrionNodeService.getNodeNames([baseline.nodeId]).then(function (_a) {
            var nodeName = _a[0];
            return _this.scmModalService.showWarning({
                title: _this._t("Redefine baseline?"),
                message: _this.swUtil.formatString(_this._t("\n                The current baseline configuration for node {0} is the configuration from\n                <span class=\"scm-redefine-baseline-link\"><a href=\"{2}\" target=\"_blank\" rel=\"noopener noreferrer\">{1}</a></span>.<br />\n                <b>Do you want to redefine the existing server configuration baseline?</b>"), nodeName.caption, _this.scmDatetimeFilter(baseline.timestamp, "long"), _this.scmLocationService.getCompareUrl(baseline.nodeId, baseline.timestamp, new Date())),
            }, {
                name: _this._t("Redefine baseline"),
                text: _this._t("Redefine baseline"),
                isPrimary: true,
                action: function () {
                    return _this.updateBaseline(baseline, date);
                }
            });
        });
    };
    BaselineService.prototype.bulkUpdateBaselines = function (nodeIds, date) {
        return this.scmApi
            .all("baseline/updateBaselines")
            .post({ nodeIds: nodeIds, timeStamp: date });
    };
    BaselineService.prototype.isBaseline = function (nodeId, polledElementId, timeStamp) {
        return this.scmApi
            .one("baseline/nodeId", nodeId)
            .one("polledElementId", polledElementId)
            .get({
            timeStamp: timeStamp.toISOString()
        });
    };
    BaselineService.prototype.computeBaselineStatus = function (nodeId, timeStamp) {
        return this.scmApi
            .one("baseline/nodeId", nodeId)
            .get({
            timeStamp: timeStamp.toISOString()
        });
    };
    BaselineService.prototype.deleteBaselines = function (nodeIds) {
        return this.scmApi
            .all("baseline/deleteBaselines")
            .post(nodeIds);
    };
    return BaselineService;
}());
exports.BaselineService = BaselineService;


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    module.service("scmElementDisplayService", ElementDisplayService);
};
var ElementDisplayService = /** @class */ (function () {
    /** @ngInject */
    ElementDisplayService.$inject = ["getTextService"];
    function ElementDisplayService(getTextService) {
        this.getTextService = getTextService;
        this.MAX_LINES_IN_TOOLTIP = 5;
        this._t = this.getTextService;
    }
    ElementDisplayService.prototype.getElementName = function (item) {
        if (!item) {
            return undefined;
        }
        if (this.isTypePathBased(item.type)) {
            if (item.displayName) {
                return this.getFileNameFromFilePath(item.displayName);
            }
            return this.getFileNameFromFilePath(this.getElementSettingsPath(item.settings));
        }
        else {
            return item.displayName ? item.displayName : item.displayAlias;
        }
    };
    ElementDisplayService.prototype.getElementNameInfo = function (item) {
        if (!item) {
            return undefined;
        }
        if (this.isTypePathBased(item.type)) {
            if (item.displayName) {
                return this.getPathFromFilePath(item.displayName);
            }
            return this.getPathFromFilePath(this.getElementSettingsPath(item.settings));
        }
        else {
            return null;
        }
    };
    ElementDisplayService.prototype.getElementTooltip = function (item) {
        if (!item) {
            return undefined;
        }
        var messages = [];
        if (this.isTypePathBased(item.type)) {
            if (item.description) {
                messages.push(item.description);
            }
            else {
                messages.push(item.displayName);
            }
        }
        else {
            if (item.description) {
                messages.push(item.description + "\n");
            }
            if (item.settings) {
                var commandLine = item.settings.pairs["commandline" /* CommandLine */];
                if (commandLine) {
                    messages.push(this._t("Command line:"));
                    messages.push(commandLine + "\n");
                }
                var path = item.settings.pairs["path" /* Path */];
                if (path) {
                    var split = path.split("\n");
                    var pathMessage = split.length > this.MAX_LINES_IN_TOOLTIP
                        ? _(split).take(this.MAX_LINES_IN_TOOLTIP).concat("…").join("\n")
                        : path;
                    var pathMessageHeaders = (_a = {},
                        _a["script" /* Script */] = this._t("Script-file:"),
                        _a["powershell" /* PowerShell */] = this._t("Script-file:"),
                        _a["swisQuery" /* SwisQuery */] = this._t("Internal query:"),
                        _a["database" /* Database */] = this._t("Database query:"),
                        _a);
                    messages.push(pathMessageHeaders[item.type]);
                    messages.push(pathMessage);
                }
            }
        }
        return messages.join("\n");
        var _a;
    };
    ElementDisplayService.prototype.getContentCollectionStateMessagePopover = function (contentCollectionState) {
        return "<scm-content-message-popover content-collection-state=\"" + contentCollectionState + "\"></scm-content-message-popover>";
    };
    ElementDisplayService.prototype.shouldShowContentCollectionStateMessage = function (type, state) {
        return !this.isTypeWithMetadata(type)
            && !(state === "contentnotpresent" /* ContentNotPresent */ || state === "contentpresent" /* ContentPresent */);
    };
    ElementDisplayService.prototype.isTypeWithMetadata = function (type) {
        return type === "file" /* File */
            || type === "parsedFile" /* ParsedFile */;
    };
    ElementDisplayService.prototype.isTypePathBased = function (type) {
        return type === "file" /* File */
            || type === "parsedFile" /* ParsedFile */
            || type === "registry" /* Registry */;
    };
    ElementDisplayService.prototype.getFileNameFromFilePath = function (path) {
        var pathStartsWithChar = path.charAt(0);
        path = _.trimEnd(path, "\\");
        return (pathStartsWithChar === "/" || pathStartsWithChar === "$")
            ? path.substring(path.lastIndexOf("/") + 1)
            : path.substring(path.lastIndexOf("\\") + 1);
    };
    ElementDisplayService.prototype.getPathFromFilePath = function (path) {
        var pathStartsWithChar = path.charAt(0);
        path = _.trimEnd(path, "\\");
        return (pathStartsWithChar === "/" || pathStartsWithChar === "$")
            ? path.substring(0, path.lastIndexOf("/") + 1)
            : path.substring(0, path.lastIndexOf("\\") + 1);
    };
    ElementDisplayService.prototype.getElementSettingsPath = function (settings) {
        if (settings) {
            return settings.pairs["path" /* Path */] || undefined;
        }
        return undefined;
    };
    return ElementDisplayService;
}());
exports.ElementDisplayService = ElementDisplayService;


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var CentralizedSettingsService = /** @class */ (function () {
    /** @ngInject */
    CentralizedSettingsService.$inject = ["scmApi"];
    function CentralizedSettingsService(scmApi) {
        this.scmApi = scmApi;
        this.cachedSettings = {};
    }
    CentralizedSettingsService.prototype.getSetting = function (settingName) {
        return this.scmApi
            .one("centralizedsettings", settingName)
            .get();
    };
    CentralizedSettingsService.prototype.getCachedSetting = function (settingName) {
        if (this.cachedSettings[settingName]) {
            return this.cachedSettings[settingName];
        }
        var setting = this.getSetting(settingName);
        this.cachedSettings[settingName] = setting;
        return setting;
    };
    CentralizedSettingsService.prototype.saveSetting = function (settingName, value) {
        return this.scmApi
            .all("centralizedsettings/" + settingName)
            .post(value);
    };
    return CentralizedSettingsService;
}());
exports.CentralizedSettingsService = CentralizedSettingsService;


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var FilteredListService = /** @class */ (function () {
    /** @ngInject */
    FilteredListService.$inject = ["scmApi", "scmUserSettingsService", "$log", "$q"];
    function FilteredListService(scmApi, scmUserSettingsService, $log, $q) {
        this.scmApi = scmApi;
        this.scmUserSettingsService = scmUserSettingsService;
        this.$log = $log;
        this.$q = $q;
    }
    FilteredListService.prototype.getFilterProperties = function (entityNames) {
        return this.scmApi
            .all("filteredList/availableFilterPropertiesForWizard")
            .post(entityNames);
    };
    FilteredListService.prototype.getFilterPropertiesForScmNodes = function (entityNames) {
        return this.scmApi
            .all("filteredList/availableFilterPropertiesForList")
            .post(entityNames);
    };
    FilteredListService.prototype.getFilteredNodes = function (dataSourceParameters) {
        return this.getFilteredNodesFromRoute("filteredList/orionForWizard", dataSourceParameters);
    };
    FilteredListService.prototype.getFilteredScmNodes = function (dataSourceParameters) {
        return this.getFilteredNodesFromRoute("filteredList/orionForList", dataSourceParameters);
    };
    FilteredListService.prototype.getFilteredScmNodesForNodesWidget = function (dataSourceParameters) {
        return this.getFilteredNodesFromRoute("filteredList/widgetNodes", dataSourceParameters);
    };
    FilteredListService.prototype.getFilteredWhoExcludedNodes = function (dataSourceParameters) {
        return this.getFilteredNodesFromRoute("filteredList/whoExcludedNodes", dataSourceParameters);
    };
    FilteredListService.prototype.getFilteredAddToWhoExclusionListNodes = function (dataSourceParameters) {
        return this.getFilteredNodesFromRoute("filteredList/addToWhoExclusionListDialog", dataSourceParameters);
    };
    FilteredListService.prototype.getFilteredIsNodeWhoExcluded = function (nodeId) {
        return this.scmApi.one("filteredList/isNodeWhoExcluded", nodeId).get();
    };
    FilteredListService.prototype.getFilteredNodesFromRoute = function (route, dataSourceParameters) {
        return this.scmApi
            .all(route)
            .post(dataSourceParameters);
    };
    FilteredListService.prototype.getScmNodesByProfileId = function (profileId) {
        return this.scmApi
            .one("filteredList/profile", profileId)
            .get();
    };
    FilteredListService.prototype.getAvailableFilterProperties = function () {
        return this.getFilterProperties(["Orion.Nodes", "Orion.AgentManagement.Agent"]);
    };
    FilteredListService.prototype.getAvailableFilterPropertiesForScmNodes = function () {
        return this.getFilterPropertiesForScmNodes(["Orion.Nodes", "Orion.AgentManagement.Agent", "Orion.SCM.Profiles", "Orion.SCM.ServerConfiguration"]);
    };
    FilteredListService.prototype.getVisibleFilterPropertiesDetailsFromRoute = function (route, filterIds) {
        if (_.isNil(filterIds) || filterIds.length === 0) {
            return this.$q.resolve([]);
        }
        else {
            return this.scmApi
                .all(route)
                .post(filterIds);
        }
    };
    FilteredListService.prototype.getVisibleFilterPropertiesDetails = function (params) {
        return this.getVisibleFilterPropertiesDetailsFromRoute("filteredList/visibleFilterPropertiesDetailsForWizard", params.filterIds);
    };
    FilteredListService.prototype.getVisibleFilterPropertiesDetailsForScmNodes = function (params) {
        return this.getVisibleFilterPropertiesDetailsFromRoute("filteredList/visibleFilterPropertiesDetailsForList", params.filterIds);
    };
    FilteredListService.prototype.getVisibleFilterPropertiesDetailsForWhoExclusionList = function (params) {
        return this.getVisibleFilterPropertiesDetailsFromRoute("filteredList/visibleFilterPropertiesDetailsForWhoExclusionList", params.filterIds);
    };
    FilteredListService.prototype.getVisibleFilterPropertiesDetailsForAddToWhoExclusionList = function (params) {
        return this.getVisibleFilterPropertiesDetailsFromRoute("filteredList/visibleFilterPropertiesDetailsForAddToWhoExclusionList", params.filterIds);
    };
    FilteredListService.prototype.getNodeListDataSourceParameters = function (sorting, pagination, searchPhrase) {
        return {
            searchPhrase: searchPhrase,
            sorting: {
                sortBy: sorting.sortBy.id, sortDirection: sorting.direction
            },
            pagination: {
                pageNumber: pagination.page - 1,
                pageSize: pagination.pageSize
            },
        };
    };
    FilteredListService.prototype.getNodeListDataSourceParametersByFiltersAndSearch = function (nodeIdFilter, blacklist, filters, search) {
        return {
            searchPhrase: search,
            filterPropertyValues: this.getFilterPropertyValuesFromIFilteredListItemSourceParams(filters),
            nodeIdFilter: nodeIdFilter,
            isFilterBlacklist: blacklist
        };
    };
    FilteredListService.prototype.getNodeListDataSourceParametersFromIFilteredListItemSourceParams = function (params) {
        var dataSourceParameters = this.getNodeListDataSourceParameters(params.sorting, params.pagination, params.search);
        dataSourceParameters.filterPropertyValues = this.getFilterPropertyValuesFromIFilteredListItemSourceParams(params.filters);
        return dataSourceParameters;
    };
    FilteredListService.prototype.getFilterPropertyValuesFromIFilteredListItemSourceParams = function (filters) {
        var selectedFilters = [];
        _(filters).forOwn(function (filter, filterKey) {
            _(filter.checkboxes).forOwn(function (checkbox, checkboxKey) {
                if (checkbox.checked) {
                    if (!_.isNil(checkbox.model) && !_.isNil(checkbox.model.startDatetime) &&
                        !_.isNil(checkbox.model.endDatetime)) {
                        selectedFilters.push({
                            name: filterKey,
                            value: checkboxKey,
                            startDate: checkbox.model.startDatetime,
                            endDate: checkbox.model.endDatetime
                        });
                    }
                    else {
                        selectedFilters.push({
                            name: filterKey,
                            value: checkboxKey
                        });
                    }
                }
            });
        });
        return selectedFilters;
    };
    FilteredListService.prototype.setDefaultFilters = function (userSettingName, nodeStateFilterPath, scope, nodeDispatcher, defaultFiltersToShow) {
        var _this = this;
        return this.scmUserSettingsService.getUserSettings(userSettingName)
            .then(function (userSettings) {
            var nodeStateFilters = _(scope).get(nodeStateFilterPath);
            var appliedSettings = {
                visibleFilters: defaultFiltersToShow,
                expandedFilters: []
            };
            if (!_.isNil(userSettings) && Array.isArray(userSettings.visibleFilters)) {
                appliedSettings.visibleFilters = userSettings.visibleFilters;
            }
            if (!_.isNil(userSettings) && Array.isArray(userSettings.expandedFilters)) {
                appliedSettings.expandedFilters = userSettings.expandedFilters;
            }
            var availableIds = _.map(nodeStateFilters.filterProperties, function (selectedProperty) { return selectedProperty.id; });
            appliedSettings.visibleFilters = _.intersection(appliedSettings.visibleFilters, availableIds);
            appliedSettings.expandedFilters = _.intersection(appliedSettings.expandedFilters, availableIds);
            nodeDispatcher.onVisibleFilterPropertiesChange(appliedSettings.visibleFilters, true);
            nodeStateFilters = _(scope).get(nodeStateFilterPath);
            _(appliedSettings.expandedFilters).forEach(function (filterId) {
                if (!_.isNil(nodeStateFilters.filterValues[filterId])) {
                    nodeStateFilters.filterValues[filterId].expanded = true;
                }
            });
        })
            .catch(function (error) {
            _this.$log.error("Unable to load user-preferred filters:", error);
        });
    };
    FilteredListService.prototype.startMonitoringAndSavingFilterChanges = function (userSettingName, nodeStateFilterPath, scope) {
        var _this = this;
        scope.$watch(nodeStateFilterPath, function (newNodeStateFilters, oldNodeStateFilters) {
            if (!_.isEqual(newNodeStateFilters.filterValues, oldNodeStateFilters.filterValues)) {
                _this.updateUserSettings(userSettingName, newNodeStateFilters);
            }
        }, true);
    };
    FilteredListService.prototype.updateUserSettings = function (userSettingName, nodeStateFilters) {
        var _this = this;
        this.$q.resolve().then(function () {
            var expandedFilters = _.reduce(nodeStateFilters.filterValues, function (filterValues, value, key) {
                if (value.expanded) {
                    filterValues.push(key);
                }
                return filterValues;
            }, []);
            var newUserSettings = {
                visibleFilters: nodeStateFilters.visibleFilterPropertyIds,
                expandedFilters: expandedFilters
            };
            return _this.scmUserSettingsService.saveUserSettingsIfChanged(userSettingName, newUserSettings);
        }).catch(function (error) {
            _this.$log.error("Unable to save user-preferred filters:", error);
        });
    };
    FilteredListService.prototype.fixPagination = function (currentItemCount, state) {
        var capacityOfPreviousPages = state.pagination.page * state.pagination.pageSize;
        var capacityHigherThanCurrent = Math.max(0, capacityOfPreviousPages - currentItemCount);
        var decrementPages = Math.floor(capacityHigherThanCurrent / state.pagination.pageSize);
        if (state.pagination.page > 1 && decrementPages > 0) {
            state.pagination.page = Math.max(1, state.pagination.page - decrementPages);
            return true;
        }
        return false;
    };
    FilteredListService.prototype.getSingleNodeData = function (nodeId) {
        var params = {
            nodeIdFilter: [nodeId],
            isFilterBlacklist: false,
            includeProfileHeaders: false
        };
        return this.getFilteredNodes(params).then((function (x) { return x.items[0]; }));
    };
    return FilteredListService;
}());
exports.FilteredListService = FilteredListService;


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../ref.d.ts" />
var UserSettingsService = /** @class */ (function () {
    /** @ngInject */
    UserSettingsService.$inject = ["webAdminService"];
    function UserSettingsService(webAdminService) {
        this.webAdminService = webAdminService;
    }
    UserSettingsService.prototype.getUserSettings = function (settingName) {
        return this.getUserSettingsString(settingName)
            .then(function (settingString) { return angular.fromJson(settingString); });
    };
    ;
    UserSettingsService.prototype.getUserSettingsString = function (settingName) {
        return this.webAdminService.getUserSetting(settingName);
    };
    ;
    UserSettingsService.prototype.saveUserSettingsIfChanged = function (settingName, settings) {
        var _this = this;
        return this.getUserSettingsString(settingName)
            .then(function (oldUserSettings) {
            var newUserSettings = angular.toJson(settings);
            if (oldUserSettings !== newUserSettings) {
                return _this.webAdminService.saveUserSetting(settingName, newUserSettings);
            }
            else {
                return true;
            }
        });
    };
    return UserSettingsService;
}());
exports.UserSettingsService = UserSettingsService;


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var RetentionService = /** @class */ (function () {
    /** @ngInject */
    RetentionService.$inject = ["scmApi"];
    function RetentionService(scmApi) {
        this.scmApi = scmApi;
    }
    RetentionService.prototype.get = function () {
        return this.scmApi
            .one("retention")
            .get();
    };
    RetentionService.prototype.update = function (setting) {
        return this.scmApi
            .all("retention")
            .post(setting);
    };
    RetentionService.prototype.getPolicyEngine = function () {
        return this.scmApi
            .one("retention")
            .one("policy")
            .get();
    };
    RetentionService.prototype.updatePolicyEngine = function (setting) {
        return this.scmApi
            .one("retention")
            .all("policy")
            .post(setting);
    };
    return RetentionService;
}());
exports.RetentionService = RetentionService;


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorHandlingService = /** @class */ (function () {
    /** @ngInject */
    ErrorHandlingService.$inject = ["scmLocationService", "xuiToastService", "scmRedirectService", "getTextService", "swUtil"];
    function ErrorHandlingService(scmLocationService, xuiToastService, scmRedirectService, getTextService, swUtil) {
        this.scmLocationService = scmLocationService;
        this.xuiToastService = xuiToastService;
        this.scmRedirectService = scmRedirectService;
        this.getTextService = getTextService;
        this.swUtil = swUtil;
        this._t = getTextService;
    }
    ;
    ErrorHandlingService.prototype.handleNodeRelatedErrorCodes = function (nodeId, statusCode) {
        var handledError = true;
        if (statusCode === 403) {
            var url = this.scmLocationService.getToNodeDetailsUrl(nodeId);
            this.scmRedirectService.redirectTo(url);
        }
        else if (statusCode === 404) {
            this.xuiToastService.error(this._t("The node does not exist in database."));
        }
        else {
            handledError = false;
        }
        return handledError;
    };
    ErrorHandlingService.prototype.handleElementRelatedErrorCodes = function (nodeId, elementId, statusCode) {
        var handledError = true;
        if (statusCode === 404) {
            this.xuiToastService.error(this.swUtil.formatString(this._t("The element {0} does not exist on node {1}."), elementId, nodeId));
        }
        else {
            handledError = false;
        }
        return handledError;
    };
    return ErrorHandlingService;
}());
exports.ErrorHandlingService = ErrorHandlingService;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../ref.d.ts" />
var constants_1 = __webpack_require__(22);
var AssetInventoryService = /** @class */ (function () {
    /** @ngInject */
    AssetInventoryService.$inject = ["xuiDialogService", "scmOrionNodeService", "getTextService", "scmLocationService", "swDemoService"];
    function AssetInventoryService(xuiDialogService, scmOrionNodeService, getTextService, scmLocationService, swDemoService) {
        var _this = this;
        this.xuiDialogService = xuiDialogService;
        this.scmOrionNodeService = scmOrionNodeService;
        this.getTextService = getTextService;
        this.scmLocationService = scmLocationService;
        this.swDemoService = swDemoService;
        this._t = getTextService;
        this.scmLocationService.getHelpLink("OrionSCMAssetInventory").then(function (helpLink) { return _this.prereqsHelpLink = helpLink; });
    }
    AssetInventoryService.prototype.isSwInventory = function (profile) {
        return profile.uniqueId && profile.uniqueId.toLowerCase() === constants_1.default.SwInventoryProfileUniqueId.toLowerCase();
    };
    AssetInventoryService.prototype.isHwInventory = function (profile) {
        return profile.uniqueId && profile.uniqueId.toLowerCase() === constants_1.default.HwInventoryProfileUniqueId.toLowerCase();
    };
    AssetInventoryService.prototype.needsAssetInventory = function (profile) {
        return this.isSwInventory(profile) || this.isHwInventory(profile);
    };
    AssetInventoryService.prototype.showConfirmationDialog = function (nodes) {
        var _this = this;
        var nodeNames = nodes.map(function (n) { return n.name; }).join(", ");
        return this.xuiDialogService.showModal({
            size: "md",
            template: __webpack_require__(162),
        }, {
            hideTopCancel: false,
            title: this._t("Enable Asset Inventory"),
            viewModel: {
                ctrl: this,
                nodeCount: nodes.length,
                nodeNames: nodeNames
            },
            cancelButtonText: this._t("Close"),
            buttons: [{
                    name: "enableAi",
                    text: this._t("Enable Asset Inventory"),
                    isPrimary: true,
                    action: function () {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                            return;
                        }
                        return _this.scmOrionNodeService.enableAi(nodes.map(function (node) { return node.nodeId; })).then(function () { return true; });
                    }
                }],
        });
    };
    return AssetInventoryService;
}());
exports.AssetInventoryService = AssetInventoryService;


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var filePathType_1 = __webpack_require__(275);
var ParsedFilePathParts = /** @class */ (function () {
    function ParsedFilePathParts() {
        this.error = false;
    }
    return ParsedFilePathParts;
}());
var ValidationService = /** @class */ (function () {
    /** @ngInject */
    ValidationService.$inject = ["scmApi", "$q"];
    function ValidationService(scmApi, $q) {
        this.scmApi = scmApi;
        this.$q = $q;
    }
    ValidationService.prototype.isSettingPathValid = function (profileElement) {
        var settingPath = profileElement.settings.pairs["path" /* Path */];
        switch (profileElement.type) {
            case "file" /* File */:
            case "parsedFile" /* ParsedFile */:
                return this.$q.resolve(this.isFilePathValid(settingPath));
            case "registry" /* Registry */:
                return this.$q.resolve(this.isRegistryValid(settingPath));
            case "swisQuery" /* SwisQuery */:
                return this.IsSwisQueryValid(settingPath);
            case "database" /* Database */:
                return this.$q.resolve(true);
            case "powershell" /* PowerShell */:
                return this.$q.resolve(true);
            case "script" /* Script */:
                return this.$q.resolve(this.isScriptValid(profileElement));
            case "unknown" /* Unknown */:
            default:
                break;
        }
        return this.$q.resolve(false);
    };
    ValidationService.prototype.isFilePathValid = function (filePath) {
        var pathType = this.getPathType(filePath);
        switch (pathType) {
            case filePathType_1.FilePathType.linuxPath:
                return this.getIsValidLinuxPath(filePath);
            case filePathType_1.FilePathType.windowsPath:
                return this.getIsValidWindowsPath(filePath);
        }
        return false;
    };
    ValidationService.prototype.validateOsCompatibility = function (assignments) {
        return this.scmApi
            .all("validator/validateOsCompatibility")
            .post(assignments);
    };
    ValidationService.prototype.getPathType = function (filePath) {
        var pathStartsWithChar = filePath.charAt(0);
        return pathStartsWithChar === "/" || pathStartsWithChar === "$" ? filePathType_1.FilePathType.linuxPath : filePathType_1.FilePathType.windowsPath;
    };
    ValidationService.prototype.getIsValidLinuxPath = function (filePath) {
        if (filePath === "/") {
            return false;
        }
        if (filePath.indexOf("\x00") >= 0) {
            return false;
        }
        if (!this.isLinuxFilePathLengthOk(filePath)) {
            return false;
        }
        var parsedFilePath = this.getParsedLinuxPath(filePath);
        if (parsedFilePath.error) {
            return false;
        }
        if (!this.getIsValidDoubleAsterisk(parsedFilePath.directories)) {
            return false;
        }
        // can not be as last character in file part
        var asLastCharacter = new RegExp(/[\/]$/);
        if (asLastCharacter.test(parsedFilePath.file)) {
            return false;
        }
        if (!this.getIsValidMultipleAsterisk(parsedFilePath.file)) {
            return false;
        }
        return true;
    };
    ValidationService.prototype.getIsValidWindowsPath = function (filePath) {
        if (!this.isWindowsFilePathLengthOk(filePath)) {
            return false;
        }
        var parsedFilePath = this.getParsedWindowsPath(filePath);
        if (parsedFilePath.error) {
            return false;
        }
        var rootDirectoryValidCharacters = new RegExp(/^[^*<>"/|\x00-\x1F]+$/);
        if (!rootDirectoryValidCharacters.test(parsedFilePath.rootDirectory)) {
            return false;
        }
        var directoriesAndFileValidCharacters = new RegExp(/^[^<>"/|:?\x00-\x1F]+$/);
        if (!directoriesAndFileValidCharacters.test(parsedFilePath.directories + parsedFilePath.file)) {
            return false;
        }
        if (!this.getIsValidDoubleAsterisk(parsedFilePath.directories)) {
            return false;
        }
        // directory can not be only white spaces
        var onlyWhitespaces = new RegExp(/\\[\s]+\\/);
        if (onlyWhitespaces.test("\\" + parsedFilePath.directories + "\\")) {
            return false;
        }
        // can not be as last character in file part
        var asLastCharacter = new RegExp(/[.\\ ]$/);
        if (asLastCharacter.test(parsedFilePath.file)) {
            return false;
        }
        if (!this.getIsValidMultipleAsterisk(parsedFilePath.file)) {
            return false;
        }
        return true;
    };
    ValidationService.prototype.getIsValidDoubleAsterisk = function (directoriesPath) {
        // directories can contain ** only once
        var countOfStars = directoriesPath.match(/([\*]+)/g);
        if (countOfStars) {
            if (countOfStars.length > 1) {
                return false;
            }
            if (countOfStars[0] !== "**") {
                return false;
            }
        }
        return true;
    };
    ValidationService.prototype.getIsValidMultipleAsterisk = function (filePath) {
        // file can contain only one * but multiple times
        var starsInFile = filePath.match(/([\*]+)/g);
        if (starsInFile) {
            for (var i = 0; i < starsInFile.length; i++) {
                if (starsInFile[i] !== "*") {
                    return false;
                }
            }
        }
        return true;
    };
    // example:         C:\directory1\directory2\file.txt
    // rootDirectory:   C:\
    // directories:        directory1\directory2
    // file:                                    \file.txt
    ValidationService.prototype.getParsedWindowsPath = function (filePath) {
        var parseFilePathParts = new ParsedFilePathParts();
        var pathWithoutRootDirectory;
        // -- root directory -----------------------------------
        // ^[a-zA-Z]:\\\\            -> e.g. C:\\
        // ^[a-zA-Z]:\\              -> e.g. C:\
        // ^%.+%\\                   -> e.g. %WINDIR%\
        // ^\\\\                     ->      \\
        var rootDirectory = new RegExp(/(^[a-zA-Z]:\\\\|^[a-zA-Z]:\\|^%.+%\\|^\\\\[^<>"/|:?\x00-\x1F\\]+\\[^<>"/|:?\x00-\x1F\\]+\\)(.*)/);
        var rootDirectoryMatch = filePath.match(rootDirectory);
        if (rootDirectoryMatch) {
            parseFilePathParts.rootDirectory = rootDirectoryMatch[1];
            pathWithoutRootDirectory = rootDirectoryMatch[2];
        }
        else {
            parseFilePathParts.error = true;
            return parseFilePathParts;
        }
        // -----------------------------------------------------
        // -- directories and file -----------------------------
        var directories = new RegExp(/(.*)(\\.*)/);
        var directoriesMatch = pathWithoutRootDirectory.match(directories);
        if (directoriesMatch) {
            parseFilePathParts.directories = directoriesMatch[1];
            parseFilePathParts.file = directoriesMatch[2];
        }
        else {
            parseFilePathParts.directories = "";
            parseFilePathParts.file = pathWithoutRootDirectory;
        }
        return parseFilePathParts;
    };
    ValidationService.prototype.getParsedLinuxPath = function (filePath) {
        var parsedFilePathParts = new ParsedFilePathParts();
        parsedFilePathParts.rootDirectory = "/";
        var pathWithoutRootDirectory = filePath.substr(1);
        var directoriesRegExp = new RegExp(/(.*)(\/.*)/);
        var directoriesMatch = pathWithoutRootDirectory.match(directoriesRegExp);
        if (directoriesMatch) {
            parsedFilePathParts.directories = directoriesMatch[1];
            parsedFilePathParts.file = directoriesMatch[2];
        }
        else {
            parsedFilePathParts.directories = "";
            parsedFilePathParts.file = pathWithoutRootDirectory;
        }
        return parsedFilePathParts;
    };
    ValidationService.prototype.isRegistryValid = function (registryPath) {
        if (!registryPath) {
            return false;
        }
        var beginsWithHKEY_prefix = new RegExp(/^HKEY_/);
        if (!beginsWithHKEY_prefix.test(registryPath)) {
            return false;
        }
        var twoBackslashes = new RegExp(/\\\\/);
        if (twoBackslashes.test(registryPath)) {
            return false;
        }
        var nonPrintableCharacters = new RegExp(/^[^\x00-\x1F%]+(\\[^\x00-\x1F%]+)*$/);
        if (!nonPrintableCharacters.test(registryPath)) {
            return false;
        }
        return true;
    };
    ValidationService.prototype.IsSwisQueryValid = function (swisQuery) {
        var readySwisQuery = swisQuery;
        if (this.ContainsNodeId(swisQuery)) {
            readySwisQuery = swisQuery.replace(new RegExp(/\${NodeId}/ig), "42");
        }
        return this.isSwisQueryCompliant(readySwisQuery);
    };
    ValidationService.prototype.ContainsNodeId = function (swisQuery) {
        var nodeIdVariable = new RegExp(/\${NodeId}/im);
        return nodeIdVariable.test(swisQuery);
    };
    ValidationService.prototype.isSwisQueryCompliant = function (query) {
        return this.scmApi
            .all("validator/queryExecutedWithoutException")
            .post([query]);
    };
    ValidationService.prototype.isWindowsFilePathLengthOk = function (filePath) {
        return filePath.length <= 258;
    };
    ValidationService.prototype.isLinuxFilePathLengthOk = function (filePath) {
        var maximumPathLengthIsOk = filePath.length <= 4096;
        var maximumFileNameLengthIsOk = _(filePath.split("/")).every(function (pathComponent) { return pathComponent.length <= 255; });
        return maximumPathLengthIsOk && maximumFileNameLengthIsOk;
    };
    ValidationService.prototype.isSettingPathLengthOk = function (profileElement) {
        if (profileElement.type !== "file" /* File */
            && profileElement.type !== "parsedFile" /* ParsedFile */) {
            return true;
        }
        return this.isWindowsFilePathLengthOk(profileElement.settings.pairs["path" /* Path */]);
    };
    ValidationService.prototype.isDisplayAliasLengthOk = function (profileElement) {
        if (profileElement.displayAlias == null) {
            return true;
        }
        return profileElement.displayAlias.length <= 256;
    };
    ValidationService.prototype.isDescriptionLengthOk = function (profileElement) {
        if (profileElement.description == null) {
            return true;
        }
        return profileElement.description.length <= 1e6;
    };
    ValidationService.prototype.isProfileNameLengthOk = function (profile) {
        return profile.name.length <= 200;
    };
    ValidationService.prototype.isProfileDescriptionLengthOk = function (profile) {
        return profile.description.length <= 1e6;
    };
    ValidationService.prototype.isScriptValid = function (profileElement) {
        var scriptRequired = this.isScriptRequired(profileElement);
        if (scriptRequired) {
            var script = profileElement.settings.pairs["path" /* Path */];
            return script && script.length > 0;
        }
        return true;
    };
    ValidationService.prototype.warnScriptNotUsed = function (profileElement) {
        var commandLine = profileElement.settings.pairs["commandline" /* CommandLine */];
        var script = profileElement.settings.pairs["path" /* Path */];
        var scriptRequired = this.isScriptRequired(profileElement);
        return !!commandLine && !!script && !scriptRequired;
    };
    ValidationService.prototype.isConnectionStringValid = function (profileElement, requireUsernamePassword) {
        var connectionString = profileElement.settings.pairs["connectionstring" /* ConnectionString */];
        var scriptFileMacro = /(?=.*\$\{(NodeIP|NodeHostname)\})/gi;
        if (requireUsernamePassword) {
            scriptFileMacro = /(?=.*\$\{(NodeIP|NodeHostname)\})(?=.*\$\{Username\})(?=.*\$\{Password\})/gi;
        }
        return scriptFileMacro.test(connectionString);
    };
    ValidationService.prototype.isScriptRequired = function (profileElement) {
        var ScriptFileMacro = /\$\{Script-file\}/gi;
        var commandLine = profileElement.settings.pairs["commandline" /* CommandLine */];
        return ScriptFileMacro.test(commandLine);
    };
    ValidationService.prototype.isCommandLineValid = function (commandLine) {
        var re = /\$\{Script-file\}/gi;
        var m;
        var count = 0;
        do {
            m = re.exec(commandLine);
            if (m) {
                count++;
            }
        } while (m);
        return count <= 1;
    };
    return ValidationService;
}());
exports.ValidationService = ValidationService;


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AuthorizationService = /** @class */ (function () {
    /** @ngInject */
    AuthorizationService.$inject = ["scmApi"];
    function AuthorizationService(scmApi) {
        this.scmApi = scmApi;
    }
    // (SCM Admin && nodemanagement) || Demo
    AuthorizationService.prototype.getIsScmUserAuthorized = function () {
        return this.scmApi
            .one("authorization/scmadminordemo")
            .get();
    };
    // Orion Admin || Demo
    AuthorizationService.prototype.getIsUserAuthorized = function () {
        return this.scmApi
            .one("authorization/adminordemo")
            .get();
    };
    AuthorizationService.prototype.getIsAllowSetBaseline = function () {
        return this.scmApi
            .one("authorization/allowsetbaseline")
            .get();
    };
    AuthorizationService.prototype.getIsAllowNodeManagement = function () {
        return this.scmApi
            .one("authorization/allownodemanagement")
            .get();
    };
    return AuthorizationService;
}());
exports.AuthorizationService = AuthorizationService;


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var RedirectService = /** @class */ (function () {
    /** @ngInject */
    RedirectService.$inject = ["$window"];
    function RedirectService($window) {
        var _this = this;
        this.$window = $window;
        this.redirectTo = function (absoluteUrlPath) {
            var targetUrl = _this.$window.location.origin + absoluteUrlPath;
            _this.$window.location.href = targetUrl;
        };
    }
    return RedirectService;
}());
exports.RedirectService = RedirectService;


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ApiHelperService = /** @class */ (function () {
    function ApiHelperService() {
    }
    ApiHelperService.prototype.handleNumericValue = function (value) {
        return value === undefined ? 0 : value;
    };
    return ApiHelperService;
}());
exports.ApiHelperService = ApiHelperService;


/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var TestJobService = /** @class */ (function () {
    /** @ngInject */
    TestJobService.$inject = ["$q", "$window", "$timeout", "scmApi", "scmSelectTestNodeDialogService", "scmLocationService"];
    function TestJobService($q, $window, $timeout, scmApi, scmSelectTestNodeDialogService, scmLocationService) {
        this.$q = $q;
        this.$window = $window;
        this.$timeout = $timeout;
        this.scmApi = scmApi;
        this.scmSelectTestNodeDialogService = scmSelectTestNodeDialogService;
        this.scmLocationService = scmLocationService;
        this.testJobRouteUri = "testJob";
        this.cancelationTokens = {};
        this.lastTempElementIdStorageKey = "scmLastTempElementId";
    }
    Object.defineProperty(TestJobService.prototype, "selectedTestJobNode", {
        get: function () {
            return this.scmSelectTestNodeDialogService.selectedTestJobNode;
        },
        enumerable: true,
        configurable: true
    });
    TestJobService.prototype.runElementTest = function (element, forceNodeSelection) {
        var _this = this;
        var tempElementId;
        return this.scmSelectTestNodeDialogService.showSelectTestNodeDialog(forceNodeSelection)
            .then(function (selectedTestJobNode) {
            if (!selectedTestJobNode) {
                return _this.$q.reject({
                    dialogCancelled: true
                });
            }
            return _this.scheduleTestJob(selectedTestJobNode.nodeId, element);
        })
            .then(function (tempId) {
            tempElementId = tempId;
            var previousTempElementId = JSON.parse(sessionStorage.getItem(_this.lastTempElementIdStorageKey));
            if (previousTempElementId) {
                return _this.removeTestJobElementResult(previousTempElementId);
            }
        })
            .then(function () {
            sessionStorage.setItem(_this.lastTempElementIdStorageKey, JSON.stringify(tempElementId));
            var url = _this.scmLocationService.getTestJobResultUrl(tempElementId);
            _this.$window.open(url, "scmTestResultsTab");
            return tempElementId;
        });
    };
    TestJobService.prototype.scheduleTestJob = function (nodeId, element) {
        var requestPayLoad = {
            nodeId: nodeId,
            profileElement: element,
            timeoutInMilliseconds: 2 * 60 * 1000
        };
        return this.scmApi
            .all(this.testJobRouteUri + "/scheduleTestJob")
            .post(requestPayLoad);
    };
    TestJobService.prototype.getTestJobResult = function (tempElementId) {
        return this.scmApi
            .one(this.testJobRouteUri + "/getResult", tempElementId)
            .get();
    };
    TestJobService.prototype.waitForTestJobResults = function (tempElementId) {
        var _this = this;
        var waitForResult = function (timeout) {
            return _this.getTestJobResult(tempElementId)
                .then(function (result) {
                if (_this.isFinishedState(result.state)) {
                    delete _this.cancelationTokens[tempElementId];
                    return result;
                }
                if (angular.isUndefined(_this.cancelationTokens[tempElementId])) {
                    return;
                }
                var timeoutNext = timeout + 1;
                if (timeoutNext > 10) {
                    timeoutNext = 10;
                }
                return _this.cancelationTokens[tempElementId] = _this.$timeout(function () { return waitForResult(timeoutNext); }, timeout * 1000);
            });
        };
        return this.cancelationTokens[tempElementId] = this.$timeout(function () { return waitForResult(1); });
    };
    TestJobService.prototype.isFinishedState = function (state) {
        return state !== "inprogress" /* InProgress */;
    };
    TestJobService.prototype.getTestJobElementContent = function (tempElementId, contentFilePath, offset, count, encodingName) {
        var requestPayload = {
            tempElementId: tempElementId,
            contentFilePath: contentFilePath,
            offset: offset,
            count: count,
            encodingName: encodingName
        };
        return this.scmApi
            .all(this.testJobRouteUri + "/getContent")
            .post(requestPayload)
            .then(function (result) {
            if (!result) {
                return;
            }
            var testJobResultContent = {
                id: contentFilePath,
                lines: result.lines.map(function (line, index) {
                    return {
                        lineNumber: index + offset + 1,
                        rawLine: line
                    };
                }),
                remainingLines: result.remainingLines
            };
            return testJobResultContent;
        });
    };
    TestJobService.prototype.removeTestJobElementResult = function (tempElementId) {
        this.$timeout.cancel(this.cancelationTokens[tempElementId]);
        delete this.cancelationTokens[tempElementId];
        return this.scmApi
            .one(this.testJobRouteUri + "/remove", tempElementId)
            .get();
    };
    return TestJobService;
}());
exports.TestJobService = TestJobService;


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var WhoService = /** @class */ (function () {
    /** @ngInject */
    WhoService.$inject = ["scmApi"];
    function WhoService(scmApi) {
        this.scmApi = scmApi;
    }
    WhoService.prototype.getWhoForAggregatedElement = function (nodeId, polledElementId, versionId) {
        var payload = {
            nodeId: nodeId,
            polledElementId: polledElementId,
            versionId: versionId,
        };
        return this.scmApi
            .all("who/aggregated")
            .post(payload);
    };
    return WhoService;
}());
exports.WhoService = WhoService;


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var CredentialService = /** @class */ (function () {
    /** @ngInject */
    CredentialService.$inject = ["scmApi"];
    function CredentialService(scmApi) {
        this.scmApi = scmApi;
    }
    CredentialService.prototype.getAll = function () {
        return this.scmApi
            .all("credential/all")
            .getList();
    };
    CredentialService.prototype.addCredential = function (credential) {
        return this.scmApi
            .all("credential/addCredential")
            .post(credential);
    };
    return CredentialService;
}());
exports.CredentialService = CredentialService;


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ScmContentMessagePopoverController = /** @class */ (function () {
    /** @ngInject */
    ScmContentMessagePopoverController.$inject = ["$templateCache", "scmCentralizedSettingsService"];
    function ScmContentMessagePopoverController($templateCache, scmCentralizedSettingsService) {
        this.$templateCache = $templateCache;
        this.scmCentralizedSettingsService = scmCentralizedSettingsService;
        this.contentSizeLimit = 10; // default value
    }
    ScmContentMessagePopoverController.prototype.$onInit = function () {
        var _this = this;
        if (this.contentCollectionState === "sizelimitexceeded" /* SizeLimitExceeded */) {
            this.scmCentralizedSettingsService.getCachedSetting("MaximumSizeOfContentInMegabytes").then(function (setting) {
                return _this.contentSizeLimit = Math.min(setting, 1024);
            });
        }
        this.$templateCache.put("scmContentMessagePopover", __webpack_require__(97));
    };
    return ScmContentMessagePopoverController;
}());
exports.default = ScmContentMessagePopoverController;


/***/ }),
/* 122 */
/***/ (function(module, exports) {

module.exports = "<scm-element-name [element]=item> <div class=\"scm-list-item xui-padding-smr\"> <span ellipsis class=\"id-settings item-part xui-text-r\"> <span class=\"id-filename xui-text-r\"> {{ $element.name }} </span> <span class=\"id-path xui-text-s\"> {{ $element.info }} </span> </span> <span class=\"id-new item-part xui-tag\" ng-if=!item.elementId _t> new </span> <span text-nowrap class=\"id-type item-part\"> {{ item.typeText }} </span> </div> <div class=\"scm-list-item scm-list-item--continuation xui-padding-smr\"> <span class=\"id-description item-part xui-text-dscrn\" ng-if=item.description> {{ item.description }} </span> </div> </scm-element-name> ";

/***/ }),
/* 123 */
/***/ (function(module, exports) {

module.exports = "<div class=id-multiedit-element-popover ng-repeat=\"element in dialogViewModel.currentlyEditedElement\"> <scm-element-name class=id-element [element]=element> <span class=\"id-filename xui-text-r\"> {{:: $element.name }} </span> <span class=\"id-path xui-text-s\"> {{:: $element.info }} </span> </scm-element-name> </div>";

/***/ }),
/* 124 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <scm-assign-profile-wizard sw-entity-popover-loader class=id-assign-profile-wizard initial-settings=$parent.vm.dialogOptions.viewModel.initialSettings> </scm-assign-profile-wizard> </xui-dialog> ";

/***/ }),
/* 125 */
/***/ (function(module, exports) {

module.exports = "<scm-grid class=scm-lineup-with-select> <scm-row align-items-start> <scm-col scm-col-auto> <xui-icon css-class=id-status-icon class=id-status-icon icon-size=small icon=unknownnode status=\"{{::vm.node.nodeStatus | swStatus}}\"/> </scm-col> <scm-col> <scm-row> <scm-col no-grow> <a ng-if=\":: vm.openLinksOnNewTab\" ellipsis class=\"id-node-name item-part scm-node-name xui-text-l xui-text-a\" ng-href={{vm.node.detailsUrl}} target=_blank rel=\"noopener noreferrer\"> {{::vm.node.name}} </a> <a ng-if=\":: !vm.openLinksOnNewTab\" ellipsis class=\"id-node-name item-part scm-node-name xui-text-l xui-text-a\" ng-href={{vm.node.detailsUrl}}> {{::vm.node.name}} </a> </scm-col> </scm-row> <scm-row wrap class=xui-text-dscrn> <scm-col scm-col-auto> <span class=\"id-ip-address xui-margin-smr\" uib-tooltip=\"_t(IP Address)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta>{{::vm.node.ipAddress}}</span> </scm-col> <scm-col scm-col-auto> <span class=id-vendor-icon uib-tooltip=_t(Vendor) tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <sw-vendor-icon ng-if=vm.node.vendorIcon vendor={{::vm.node.vendorIcon}} title={{::vm.node.vendor}}></sw-vendor-icon> </span> <span class=id-machine-type uib-tooltip=\"_t(Machine type)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta>{{::vm.node.machineType}}</span> </scm-col> <scm-col class=id-polling-method-with-icon scm-col-auto justify-content-around> <scm-polling-method-with-icon [node]=vm.node /> </scm-col> <scm-col scm-col-auto> <span class=\"id-excluded-from-who xui-margin-sml\" ng-if=\"vm.node.excludedFromWho && vm.isFimEnabledGlobally\"> <who-is-not-available [node-id]=vm.node.id /> </span> </scm-col> </scm-row> <scm-row ng-show=\"vm.node.profileCount > 0\"> <scm-profile-assigned-to-node full-width class=id-profile-assigned-to-node [node]=vm.node [profile-with-errors]=vm.node.profilesWithErrors [show-last-change]=false [show-profiles-expanded]=true /> </scm-row> <scm-row ng-show=\"vm.node.policyCount > 0\"> <scm-policy-profile-indications full-width class=id-policy-profile-indications [node]=vm.node [show-policy-profiles-expanded]=true /> </scm-row> </scm-col> <scm-col pull-right scm-col-auto scm-col-max-50 ng-if=vm.node.profiles> <scm-row> <scm-profile-indications full-width class=id-profile-indications [node]=vm.node [profile-with-errors]=vm.node.profilesWithErrors [show-last-change]=false [show-profiles-expanded]=true /> </scm-row> <scm-row class=xui-text-dscrn ng-show=vm.showMonitoredSince> <scm-col scm-col-auto pull-right> <span class=\"id-assigned xui-margin-mdl\" ng-if=vm.node.assigned _t=\"['{{:: vm.node.assigned | scmDatetime:&quot;calendar&quot;:&quot;N/A&quot; }}']\">Monitored since {0}</span> </scm-col> </scm-row> </scm-col> </scm-row> </scm-grid> ";

/***/ }),
/* 126 */
/***/ (function(module, exports) {

module.exports = "<scm-grid> <scm-row> <xui-icon icon=\"{{'Orion.Nodes' | swEntityIcon}}\" icon-size=small status=\"{{ vm.showStatus ? (vm.node.nodeStatus | swStatus) : ''}}\" text-alignment=right> </xui-icon> <a class=id-node-name ng-href=\"{{ vm.node.detailsUrl }}\" target=_blank rel=\"noopener noreferrer\" ellipsis> {{vm.node.name}} </a> </scm-row> </scm-grid> ";

/***/ }),
/* 127 */
/***/ (function(module, exports) {

module.exports = "<span _t>{{vm.missingWhoPopoverDescription}}</span> <a ng-href={{vm.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more</a> ";

/***/ }),
/* 128 */
/***/ (function(module, exports) {

module.exports = "<scm-grid class=\"id-who who\" scm-stop-click-propagation ng-if=vm.lastChange.lastChangeVisible> <scm-row ng-if=!vm.isUserInfoAvailable> <scm-col no-margin> <xui-popover class=id-missing-who-popover xui-popover-content=vm.missingWhoPopoverContent xui-popover-trigger=mouseenter xui-popover-is-modal=false xui-popover-title=\"_t(Who made the change?)\" _ta> <xui-icon class=id-who-icon css-class=id-who-icon icon-size=small tooltip-append-to-body=false status={{vm.whoStatusOverlay}} icon=user /> </xui-popover> </scm-col> </scm-row> <scm-row ng-attr-reverse=\"{{ vm.iconPlacement === 'left' ? true : undefined }}\" ng-if=vm.isUserInfoAvailable uib-tooltip=\"{{:: vm.getEllipsisText() }}\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip> <scm-col no-margin ellipsis> <span class=id-who-username ng-bind-html=\"vm.lastChange.lastModifiedBy | scmWindowsUser:'user' | escapeHtml | xuiHighlight:vm.search\"> </span> </scm-col> <scm-col scm-col-auto/> <scm-col scm-col-auto no-margin> <xui-icon class=id-who-icon css-class=id-who-icon icon-size=small tooltip-append-to-body=false icon=user /> </scm-col> </scm-row> </scm-grid> ";

/***/ }),
/* 129 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=vm.lastChange text-nowrap class=\"timer xui-text-dscrn id-last-change-date\" uib-tooltip={{::vm.tooltipMsg}} tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <span ng-if=vm.isDailyAggregated>{{::vm.lastChange | scmDatetime:'calendar':'N/A' }} - {{::vm.originalTimeStamp | scmDatetime:'calendar':'N/A' }}</span> <span ng-if=vm.isHourlyAggregated>{{::vm.lastChange | scmDatetime:'calendar':'N/A' }} - {{::vm.originalTimeStamp | scmDatetime:'timeOnly':'N/A' }}</span> <span ng-if=\"!vm.isDailyAggregated && !vm.isHourlyAggregated\">{{::vm.lastChange | scmDatetime:'calendar':'N/A' }}</span> </div> ";

/***/ }),
/* 130 */
/***/ (function(module, exports) {

module.exports = "<div xui-scrollbar class=scm-multiline-text-scroll-container> <div ng-transclude class=scm-multiline-text-scroll-content> {{ vm.text }} </div> </div> ";

/***/ }),
/* 131 */
/***/ (function(module, exports) {

module.exports = "<span class=id-polling-method uib-tooltip=\"_t(Polling method)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon css-class=\"item-part xui-margin-sml\" icon={{::vm.pollingMethodIcon()}} icon-size=small /> <span class=id-node-polling-method ng-if=vm.node.pollingMethod>{{::vm.node.pollingMethod}}</span> <span class=id-agent-deployed ng-show=vm.showAgentDeployed() _t>+ Agent deployed</span> </span>";

/***/ }),
/* 132 */
/***/ (function(module, exports) {

module.exports = "<span ng-if=!vm.node.licensed> <span class=node-properties> <span> <xui-icon icon-size=small icon=status_critical text-alignment=right /> </span> <span class=\"xui-text-critical id-scm-license\" _t>No SCM license available</span> </span> </span> <scm-grid ng-if=vm.node.licensed> <scm-row wrap> <scm-col scm-col-auto ng-if=\"vm.node.errorCount > 0\"> <span class=\"issues id-issues plus3digits xui-margin-smr\" uib-tooltip=\"_t(Number of polling issues)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon icon-size=small icon=status_warning /> <span class=xui-text-warning> {{vm.node.errorCount}} </span> </span> </scm-col> <scm-col scm-col-auto ng-if=vm.showLastChange> <xui-icon icon-size=xsmall tooltip-append-to-body=false text-alignment=left-right icon-color=gray icon=edit /> <span class=\"xui-text-dscrn id-last-change xui-margin-smr\" uib-tooltip=\"_t(Last change detected)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> {{vm.node.lastChange | scmDatetime:\"calendar\":\"N/A\" }} </span> </scm-col> <scm-col scm-col-auto class=\"plus3digits id-baseline-indication\"> <span ng-if=\"vm.node.baselineInfo.baselineStatus === 'NoBaselineSet'\" class=id-NoBaselineSet uib-tooltip=\"_t(No baseline set)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon icon-size=small icon=baseline icon-color=disabled-gray></xui-icon> <span _t>N/A</span> </span> <span ng-if=\"vm.node.baselineInfo.baselineStatus === 'IsBaseline'\" class=id-IsBaseline uib-tooltip=\"_t(Configuration is the baseline)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon icon-size=small icon-color=ok-green icon=baseline></xui-icon> <xui-icon icon-size=small icon-color=ok-green icon=checkmark></xui-icon> </span> <span ng-if=\"vm.node.baselineInfo.baselineStatus === 'MatchesBaseline'\" class=id-MatchesBaseline uib-tooltip=\"_t(Configuration matches the baseline)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon icon-size=small icon-color=ok-green icon=baseline></xui-icon> <xui-icon icon-size=small icon-color=ok-green icon=checkmark></xui-icon> </span> <span ng-if=\"vm.node.baselineInfo.baselineStatus === 'DiffersFromBaseline'\" class=\"xui-text-warning id-DiffersFromBaseline\" uib-tooltip=\"_t(# of baseline mismatches)\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon icon-size=small icon=baseline-mismatch icon-color=critical-red></xui-icon> <span> {{vm.getMismatchesCount(vm.node.baselineInfo)}} </span> </span> </scm-col> <scm-col scm-col-auto ng-if=::!vm.showProfilesExpanded> <scm-profiles-count class=\"profiles plus3digits\" node-id=vm.node.nodeId profiles-count=vm.node.profileCount /> </scm-col> </scm-row> </scm-grid> ";

/***/ }),
/* 133 */
/***/ (function(module, exports) {

module.exports = "<scm-grid ng-if=vm.node.licensed> <scm-row wrap> <scm-col scm-col-auto ng-if=::vm.showProfilesExpanded> <span class=id-assigned-profiles-icon uib-tooltip=\"_t(Assigned server configuration profile(s))\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon css-class=item-part icon=config-profile icon-size=small /> </span> </scm-col> <scm-col scm-col-auto ng-if=::vm.showProfilesExpanded ng-repeat=\"profile in vm.node.profiles\" class=xui-tag-container> <xui-tag no-max-width=false class=id-profile-name text={{::profile.name}} color={{vm.getProfileStatus(profile)}} icon={{vm.getProfileStatusIcon(profile)}}></xui-tag> </scm-col> </scm-row> </scm-grid> ";

/***/ }),
/* 134 */
/***/ (function(module, exports) {

module.exports = "<scm-grid ng-if=vm.node.licensed> <scm-row wrap> <scm-col scm-col-auto ng-if=::vm.showPolicyProfilesExpanded> <span class=id-assigned-policy-profiles-icon uib-tooltip=\"_t(Assigned policie(s))\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> <xui-icon css-class=item-part icon=policy icon-size=small /> </span> </scm-col> <scm-col scm-col-auto ng-if=::vm.showPolicyProfilesExpanded ng-repeat=\"profile in vm.node.policyProfiles\" class=xui-tag-container> <xui-tag no-max-width=false class=id-profile-name text={{::profile.name}} color=unknown></xui-tag> </scm-col> </scm-row> </scm-grid> ";

/***/ }),
/* 135 */
/***/ (function(module, exports) {

module.exports = "<div class=xui-tag__border ng-class=vm.getBorderClasses()> <div class=xui-tag__container ng-if=::vm.text> <div ng-if=\":: vm.icon\" class=\"xui-tag__item xui-tag__icon-container\" ng-class=\"vm.iconPosition === 'right' ? 'xui-tag__icon-right' : 'xui-tag__icon-left'\" role=\"{{ vm.onIconClick ? 'button' : 'none' }}\" ng-click=vm.clickIcon()> <xui-icon icon-size=small icon-color=\"{{ :: vm.iconColor }}\" icon=\"{{ :: vm.icon }}\"/> </div> <div class=\"xui-tag__text xui-tag__item\" xui-ellipsis append-to-body=true ellipsis-options=\":: {tooltipText: vm.text}\"> <span class=xui-tag__transcluded-text>{{ ::vm.text }}</span> </div> <div ng-if=vm.showRemoveIcon class=\"xui-tag__remove-icon xui-tag__item xui-tag__icon-container\" role=button ng-click=vm.clickRemoveIcon()> <xui-icon icon-size=small icon-color=\"{{ :: vm.removeIconColor }}\" icon=remove /> </div> </div> </div>";

/***/ }),
/* 136 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=scm-enable-who-detection-dialog> <div class=scm-who-detection-dialog-message _t> <div>This node is explicitly excluded from 'who made the change' detection, even though the feature is globally enabled. Therefore the 'who' value is not available for files and registry entries watched on this node via Orion Agent. </div> <div>When 'who made the change' detection is enabled the File Integrity Monitor (FIM) will automatically be deployed to this node. <a ng-href={{::vm.dialogOptions.viewModel.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more</a></div> </div> </xui-dialog>";

/***/ }),
/* 137 */
/***/ (function(module, exports) {

module.exports = "<span _t>The 'who' value is not available because this node is explicitly excluded from 'who made the change' detection.</span> <a ng-href={{vm.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more</a> <xui-divider></xui-divider> <a href=# class=id-enable-who-link ng-click=vm.enableWhoDetectionForNode() _t>Enable 'who' detection for this node</a>";

/***/ }),
/* 138 */
/***/ (function(module, exports) {

module.exports = "<xui-popover class=id-who-popover xui-popover-content=vm.whoPopoverContent xui-popover-trigger=mouseenter xui-popover-is-modal=false xui-popover-title=\"_t(Who made the change?)\" _ta> <xui-icon class=id-who-icon css-class=id-who-icon icon-size=small tooltip-append-to-body=false status=unreachable icon=user /> </xui-popover>";

/***/ }),
/* 139 */
/***/ (function(module, exports) {

module.exports = "<div uib-tooltip={{$element.tooltipPath}} ellipsis tooltip-append-to-body=true tooltip-class=\"ellipsis-tooltip element-path-tooltip\" tooltip-placement=\"auto top\"> </div> ";

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var core_1 = __webpack_require__(0);
var nodeConfig_issue_service_1 = __webpack_require__(52);
var constants_1 = __webpack_require__(22);
var NodeConfigService = /** @class */ (function () {
    function NodeConfigService(scmApi, aiService, issueService, userSettingsService) {
        this.scmApi = scmApi;
        this.aiService = aiService;
        this.issueService = issueService;
        this.userSettingsService = userSettingsService;
    }
    NodeConfigService.prototype.isWhoDisplayed = function () {
        return this.userSettingsService.getUserSettings(constants_1.default.DisplayWhoUserSettingName).then(function (setting) {
            return setting === null ? true : setting;
        });
    };
    NodeConfigService.prototype.saveUserSetting = function (value) {
        return this.userSettingsService.saveUserSettingsIfChanged(constants_1.default.DisplayWhoUserSettingName, value);
    };
    NodeConfigService.prototype.getNodeConfigTree = function (nodeId) {
        return this.scmApi
            .one("server-config")
            .one("node", nodeId)
            .get();
    };
    NodeConfigService.prototype.fillProfileIssuesAndMismatches = function (profiles) {
        _(profiles).each(function (profile) {
            profile.itemIssues = profile.elements
                .filter(function (element) { return element.pollingErrorType; })
                .map(function (element) { return ({
                profile: profile,
                element: element,
                type: element.pollingErrorType,
                timestamp: element.errorTimeStamp,
                message: element.errorMessage
            }); });
            profile.elementIssues = profile.elementErrors
                .map(function (element) { return ({
                profile: profile,
                element: element,
                type: element.elementErrorType,
                timestamp: element.elementErrorTimeStamp,
                message: element.elementErrorMessage
            }); });
            profile.nodeIssues = [];
            profile.mismatches = profile.elements
                .filter(function (element) { return !element.matchesBaseline; })
                .length;
        });
    };
    NodeConfigService.prototype.getNodeCompatibility = function (nodeId) {
        return this.scmApi
            .one("server-config")
            .one("nodeCompatibility", nodeId)
            .get();
    };
    NodeConfigService.prototype.getProfilesWithErrors = function (nodeIds) {
        return this.scmApi
            .one("server-config")
            .post("profilesWithErrors", nodeIds);
    };
    NodeConfigService.prototype.fillServerIssues = function (issues, profiles) {
        var _this = this;
        _(issues).each(function (issue) {
            if (_this.issueService.isAssetInventoryIssue(issue)) {
                _(profiles)
                    .filter(function (profile) { return _this.aiService.needsAssetInventory(profile); })
                    .each(function (profile) {
                    profile.nodeIssues.push(issue);
                });
            }
            else if (_this.issueService.isAgentIssue(issue)) {
                _(profiles)
                    .filter(function (profile) { return _this.needsAgent(profile); })
                    .each(function (profile) {
                    profile.nodeIssues.push(issue);
                });
            }
            else if (_this.issueService.isFimIssue(issue)) {
                _(profiles)
                    .filter(function (profile) { return _this.needsFim(profile); })
                    .each(function (profile) {
                    profile.nodeIssues.push(issue);
                });
            }
        });
    };
    NodeConfigService.prototype.needsFim = function (profile) {
        var types = [
            "file" /* File */,
            "parsedFile" /* ParsedFile */,
            "registry" /* Registry */
        ];
        return profile.elementsTypes.some(function (elementType) { return _.includes(types, elementType); });
    };
    NodeConfigService.prototype.needsAgent = function (profile) {
        var types = [
            "file" /* File */,
            "parsedFile" /* ParsedFile */,
            "powershell" /* PowerShell */,
            "script" /* Script */,
            "registry" /* Registry */
        ];
        return profile.elementsTypes.some(function (elementType) { return _.includes(types, elementType); });
    };
    NodeConfigService = __decorate([
        core_1.Injectable("scmNodeConfigService"),
        __param(0, core_1.Inject("scmApi")),
        __param(1, core_1.Inject("scmAssetInventoryService")),
        __param(2, core_1.Inject(nodeConfig_issue_service_1.NodeConfigIssueService)),
        __param(3, core_1.Inject("scmUserSettingsService"))
    ], NodeConfigService);
    return NodeConfigService;
}());
exports.NodeConfigService = NodeConfigService;


/***/ }),
/* 141 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <scm-grid id=polling-error-dialog-content> <scm-row padding-bottom-xs> <scm-col> <span class=xui-text-l _t=\"['{{:: vm.dialogOptions.viewModel.dateTime }}', '{{:: vm.dialogOptions.viewModel.type }}']\"> Error occurred at {0} while polling {1} element: </span> </scm-col> </scm-row> <scm-row padding-bottom-lg> <scm-col> <span class=\"xui-text-normal id-elementName\" uib-tooltip=\"{{:: vm.dialogOptions.viewModel.name }}\" tooltip-ellipsis tooltip-append-to-body=true> {{:: vm.dialogOptions.viewModel.name }} </span> <span class=\"xui-text-s id-elementPath\" ng-if=vm.dialogOptions.viewModel.path uib-tooltip=\"{{:: vm.dialogOptions.viewModel.path }}\" tooltip-ellipsis tooltip-append-to-body=true> {{:: vm.dialogOptions.viewModel.path }} </span> </scm-col> </scm-row> <scm-row padding-bottom-xs> <scm-col> <span class=xui-text-l _t> Configuration profile </span> </scm-col> </scm-row> <scm-row padding-bottom-lg> <scm-col> <span class=\"xui-text-normal id-profileName\" uib-tooltip=\"{{:: vm.dialogOptions.viewModel.profile }}\" tooltip-ellipsis tooltip-append-to-body=true> {{:: vm.dialogOptions.viewModel.profile }} </span> </scm-col> </scm-row> <scm-row padding-bottom-xs> <scm-col> <span class=xui-text-l _t> Details </span> </scm-col> </scm-row> <scm-row padding-bottom-lg> <scm-col> <xui-scroll-shadows> <div class=\"id-content xui-text-monospace scm-scroll-shadows\">{{:: vm.dialogOptions.viewModel.message }}</div> </xui-scroll-shadows> </scm-col> </scm-row> <scm-row> <scm-col> <a href={{::vm.dialogOptions.viewModel.helpLink}} target=_blank rel=\"noopener noreferrer\" _t> Learn more </a> </scm-col> </scm-row> </scm-grid> </xui-dialog> ";

/***/ }),
/* 142 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div id=polling-error-dialog-content> <div _t> Error occurred: </div> <div class=scm-dialog-section-padding> <div class=xui-text-s _t> Details </div> <div class=\"xui-text-r id-errorMessage\"> {{:: vm.dialogOptions.viewModel.message }} </div> </div> <div class=scm-dialog-section-padding> <a ng-href={{::vm.dialogOptions.viewModel.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more</a> </div> </div> </xui-dialog> ";

/***/ }),
/* 143 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <scm-grid id=polling-error-dialog-content> <scm-row padding-bottom-md> <scm-col> <div class=xui-text-s> {{ ::vm.dialogOptions.viewModel.message }} </div> </scm-col> </scm-row> <scm-row padding-bottom-md> <scm-col> <div class=xui-text-r> <ul class=squared-list> <li ng-repeat=\"element in vm.dialogOptions.viewModel.incompatibleProfileElements track by $index\"> <span>{{::element}}</span> </li> </ul> </div> </scm-col> </scm-row> <scm-row> <scm-col> <a ng-href={{::vm.dialogOptions.viewModel.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more</a> </scm-col> </scm-row> </scm-grid> </xui-dialog> ";

/***/ }),
/* 144 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <scm-grid id=polling-error-dialog-content> <scm-row padding-bottom-md> <scm-col> <div class=xui-text-s> {{ ::vm.dialogOptions.viewModel.message }} </div> </scm-col> </scm-row> <scm-row padding-bottom-md> <scm-col> <div class=xui-text-r> <ul class=squared-list> <li ng-repeat=\"element in vm.dialogOptions.viewModel.elementsWithoutCredentials track by $index\"> <span>{{::element}}</span> </li> </ul> </div> </scm-col> </scm-row> <scm-row> <scm-col> <a ng-if=vm.dialogOptions.viewModel.isAuthorized ng-href={{::vm.dialogOptions.viewModel.editProfileLink}} target=_blank rel=\"noopener noreferrer\" _t>Set credentials for assigned elements of '{{ ::vm.dialogOptions.viewModel.profileName }}'</a> </scm-col> </scm-row> <scm-row> <scm-col> <a ng-href={{::vm.dialogOptions.viewModel.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more</a> </scm-col> </scm-row> </scm-grid> </xui-dialog> ";

/***/ }),
/* 145 */
/***/ (function(module, exports) {

module.exports = "<div class=id-test-success-icon ng-if=!vm.result> <xui-icon icon=checkmark icon-size=small icon-color=ok-green tool-tip=\"Successful test\" tooltip-append-to-body=true></xui-icon> </div> <div class=id-test-error-icon ng-if=vm.result> <xui-icon icon=severity_warning icon-size=small tool-tip=\"Failed test\" tooltip-append-to-body=true></xui-icon> </div> ";

/***/ }),
/* 146 */
/***/ (function(module, exports) {

module.exports = "<span class=xui-text-s _t>Initial poll <xui-icon icon-size=xsmall tooltip-append-to-body=false icon-color=gray text-alignment=left icon=scan /> </span> ";

/***/ }),
/* 147 */
/***/ (function(module, exports) {

module.exports = "<div class=xui-margin-mdb _t=\"['{{ ::vm.lastChange.changesCount }}']\"> {0} versions were aggregated together according to data retention settings. </div> <div class=xui-margin-mdb> {{ ::vm.aggregatedTimeFrame }} </div> <div ng-if=::vm.shouldShowWhoOnPopover> {{ ::vm.knownContributors }} {{ ::vm.unknownContributors }} </div> ";

/***/ }),
/* 148 */
/***/ (function(module, exports) {

module.exports = " <scm-who class=\"id-modified-by xui-text-dscrn scm-who--constrained\" ng-if=\":: !vm.isDataAggregated && !vm.lastChange.isInitialPoll && vm.shouldShowWho\" icon-placement=right [has-issue]=vm.hasIssue [last-change]=vm.lastChange [search]=vm.whoSearchPhrase /> <xui-popover class=\"id-aggregated-changes-popover xui-text-dscrn\" ng-if=vm.isDataAggregated xui-popover-content=vm.aggregatedChangesPopoverContent xui-popover-trigger=mouseenter xui-popover-is-modal=false xui-popover-title=\"_t(Aggregated change details)\" xui-popover-on-show=vm.getWhoContributors() _ta> <scm-grid> <scm-row> <scm-col class=id-aggregated-changes-count no-margin ellipsis _t=\"['{{ vm.lastChange.changesCount }}']\"> {0} aggregated </scm-col> <scm-col scm-col-auto/> <scm-col scm-col-auto no-margin> <xui-icon class=id-aggregated-icon css-class=id-aggregated-icon icon-size=xsmall icon-color=gray tooltip-append-to-body=false icon=clock /> </scm-col> </scm-row> </scm-grid> </xui-popover> <scm-initial-poll-icon class=id-initial-poll-icon ng-if=vm.lastChange.isInitialPoll /> ";

/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var MismatchesController = /** @class */ (function () {
    /** @ngInject */
    MismatchesController.$inject = ["scmLocationService", "$templateCache", "getTextService", "swUtil"];
    function MismatchesController(scmLocationService, $templateCache, getTextService, swUtil) {
        this.scmLocationService = scmLocationService;
        this.$templateCache = $templateCache;
        this.getTextService = getTextService;
        this.swUtil = swUtil;
        this._t = getTextService;
        this.loadedAt = new Date();
    }
    MismatchesController.prototype.$onInit = function () {
        this.$templateCache.put("scmMismatchesPopoverContent", __webpack_require__(280));
    };
    MismatchesController.prototype.getMismatchesMsg = function () {
        var mismatchesCount = this.getMismatchesCount();
        if (mismatchesCount === 1) {
            return this.swUtil.formatString(this._t("{0} mismatch"), mismatchesCount);
        }
        return this.swUtil.formatString(this._t("{0} mismatches"), mismatchesCount);
    };
    MismatchesController.prototype.getPopoverTemplate = function () {
        return this.$templateCache.get("scmMismatchesPopoverContent");
    };
    MismatchesController.prototype.getCompareConfigurationsPage = function () {
        return this.scmLocationService.getCompareUrl(this.baselineInfo.baseline.nodeId, this.baselineInfo.baseline.timestamp, this.loadedAt);
    };
    MismatchesController.prototype.getMismatchesCount = function () {
        if (_.isNil(this.baselineInfo.mismatches)) {
            return 0;
        }
        return this.baselineInfo.mismatches.addedCount +
            this.baselineInfo.mismatches.changedCount +
            this.baselineInfo.mismatches.deletedCount;
    };
    return MismatchesController;
}());
exports.MismatchesController = MismatchesController;


/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AssignProfileWizardStepName_1 = __webpack_require__(51);
var ProfileListController = /** @class */ (function () {
    /** @ngInject */
    ProfileListController.$inject = ["$scope", "$log", "$q", "xuiFilteredListService", "xuiArraySourceFactoryService", "xuiToastService", "getTextService", "scmProfileService", "scmProfileElementService", "$templateCache", "scmManageProfileDialogService", "scmModalService", "$timeout", "scmAssignProfileWizardDialogService", "swUtil", "swDemoService", "scmFilteredListService", "$stateParams", "$location"];
    function ProfileListController($scope, $log, $q, xuiFilteredListService, xuiArraySourceFactoryService, xuiToastService, getTextService, scmProfileService, scmProfileElementService, $templateCache, scmManageProfileDialogService, scmModalService, $timeout, scmAssignProfileWizardDialogService, swUtil, swDemoService, scmFilteredListService, $stateParams, $location) {
        this.$scope = $scope;
        this.$log = $log;
        this.$q = $q;
        this.xuiFilteredListService = xuiFilteredListService;
        this.xuiArraySourceFactoryService = xuiArraySourceFactoryService;
        this.xuiToastService = xuiToastService;
        this.getTextService = getTextService;
        this.scmProfileService = scmProfileService;
        this.scmProfileElementService = scmProfileElementService;
        this.$templateCache = $templateCache;
        this.scmManageProfileDialogService = scmManageProfileDialogService;
        this.scmModalService = scmModalService;
        this.$timeout = $timeout;
        this.scmAssignProfileWizardDialogService = scmAssignProfileWizardDialogService;
        this.swUtil = swUtil;
        this.swDemoService = swDemoService;
        this.scmFilteredListService = scmFilteredListService;
        this.$stateParams = $stateParams;
        this.$location = $location;
        this._t = getTextService;
    }
    ProfileListController.prototype.$onInit = function () {
        var _this = this;
        this.createProfileState();
        this.createProfileDispatcher();
        this.loadAllProfiles()
            .then(this.preselectProfiles.bind(this))
            .then(this.triggerOnLoaded.bind(this))
            .then(function () {
            if (_this.$stateParams.addProfile) {
                _this.addProfile().finally(function () {
                    _this.$location.search({});
                    _this.$location.replace();
                });
            }
            else if (_this.$stateParams.editProfile) {
                _this.editProfile(_this.$stateParams.editProfile);
                _this.$location.search({});
                _this.$location.replace();
            }
        });
        this.$templateCache.put("profileList-item", __webpack_require__(282));
    };
    ProfileListController.prototype.triggerOnLoaded = function () {
        if (angular.isFunction(this.$scope.onLoaded)) {
            this.$timeout(this.$scope.onLoaded);
        }
    };
    ProfileListController.prototype.loadAllProfiles = function () {
        var _this = this;
        this.profileLoadingRequest = this.scmProfileService.getAll().then(function (profiles) {
            _this.$scope.profiles = profiles;
            return profiles;
        });
        return this.profileLoadingRequest;
    };
    ProfileListController.prototype.preselectProfiles = function () {
        if (!Array.isArray(this.$scope.profilesToPreselect)) {
            return;
        }
        var newSelection = _.intersection(this.$scope.profilesToPreselect, this.$scope.profiles.map(function (profile) { return profile.id; }));
        this.$scope.profileState.selection = { items: newSelection };
    };
    ProfileListController.prototype.addProfile = function () {
        var _this = this;
        return this.scmManageProfileDialogService.showAddDialog().finally(function () {
            _this.refreshList();
        });
    };
    ProfileListController.prototype.importProfile = function () {
        var _this = this;
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return;
        }
        var fileSelector = document.createElement("input");
        fileSelector.id = "scm-import-profile-input-id";
        fileSelector.type = "file";
        fileSelector.accept = ".scm-profile";
        fileSelector.multiple = true;
        fileSelector.onchange = function () {
            _this.isBusyMessage = _this._t("Importing profiles, please wait...");
            _this.isBusy = true;
            _this.scmProfileService.importProfile(fileSelector.files)
                .then(function (importedProfiles) {
                _this.scmManageProfileDialogService.showImportedProfilesToast(importedProfiles);
            })
                .catch(function (response) {
                var data = response.data;
                var duplicateProfileIds = data.duplicateProfileIds;
                var importedProfilesNames = data.importedProfilesNames;
                if (response.status === 400 || response.status === 409) {
                    return _this.scmManageProfileDialogService.showImportErrorDialog(fileSelector.files, duplicateProfileIds, data.validationErrors, data.notValidFiles, importedProfilesNames, _this.$scope.profiles)
                        .then(function () {
                        if (importedProfilesNames.length > 0) {
                            _this.scmManageProfileDialogService.showImportedProfilesToast(importedProfilesNames);
                        }
                    });
                }
                else {
                    _this.xuiToastService.error(_this._t("Something went wrong during the import process, please try again."));
                }
            })
                .finally(function () {
                _this.refreshList();
                _this.isBusy = false;
            });
        };
        fileSelector.click();
    };
    ProfileListController.prototype.editProfile = function (id) {
        var _this = this;
        var selectedIds = id ? [id] : this.getSelectedItems();
        if (selectedIds.length !== 1) {
            return;
        }
        var profileToEdit = _(this.$scope.profiles).filter(function (p) { return p.id === selectedIds[0]; }).first();
        this.isBusyMessage = this._t("Loading profile...");
        this.isBusy = true;
        this.scmProfileElementService.getForProfile(profileToEdit.id).then(function (data) {
            _this.scmManageProfileDialogService.showEditDialog(profileToEdit, data).finally(function () {
                _this.refreshList();
            });
        }).finally(function () {
            _this.isBusy = false;
        });
    };
    ProfileListController.prototype.copyProfile = function () {
        var _this = this;
        var selectedIds = this.getSelectedItems();
        if (selectedIds.length !== 1) {
            return;
        }
        var profileToCopy = _(this.$scope.profiles).filter(function (p) { return p.id === selectedIds[0]; }).first();
        this.isBusyMessage = this._t("Loading profile...");
        this.isBusy = true;
        this.scmProfileElementService.getForProfile(profileToCopy.id).then(function (data) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var element = data_1[_i];
                element.elementId = undefined;
                element.profileId = undefined;
            }
            _this.scmManageProfileDialogService.showCopyDialog(profileToCopy, data, _this.$scope.profiles, 1 /* Copy */).finally(function () {
                _this.refreshList();
            });
        }).finally(function () {
            _this.isBusy = false;
        });
    };
    ProfileListController.prototype.viewProfile = function (id) {
        var _this = this;
        var selectedIds = id ? [id] : this.getSelectedItems();
        if (selectedIds.length !== 1) {
            return;
        }
        var profileToView = _(this.$scope.profiles).filter(function (p) { return p.id === selectedIds[0]; }).first();
        this.isBusyMessage = this._t("Loading profile...");
        this.isBusy = true;
        this.scmProfileElementService.getForProfile(profileToView.id).then(function (data) {
            _this.scmManageProfileDialogService.showViewDialog(profileToView, data).finally(function () {
                _this.refreshList();
            });
        }).finally(function () {
            _this.isBusy = false;
        });
    };
    ProfileListController.prototype.assignProfile = function () {
        var _this = this;
        var selectedIds = this.getSelectedItems();
        if (selectedIds.length === 0) {
            return;
        }
        var settings = {
            selectedNodes: [],
            selectedProfiles: selectedIds,
            wizardStep: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Nodes
        };
        this.scmAssignProfileWizardDialogService.showModal(settings).finally(function () {
            _this.refreshList();
        });
    };
    ProfileListController.prototype.exportProfile = function () {
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return;
        }
        var selectedIds = this.getSelectedItems();
        this.scmProfileService.exportProfiles(selectedIds);
        this.xuiToastService.info(this._t("Export has been initiated..."));
        this.refreshList();
    };
    ProfileListController.prototype.refreshList = function () {
        var _this = this;
        this.loadAllProfiles().then(function (profiles) {
            _this.scmFilteredListService.fixPagination(profiles.length, _this.$scope.profileState);
            _this.$scope.profileState.selection.items = [];
            _this.$scope.profileState.selection.blacklist = false;
            _this.listControl.refreshListData();
        });
    };
    ProfileListController.prototype.selectedItemIsBuiltIn = function () {
        var selectedIds = this.getSelectedItems();
        if (selectedIds.length !== 1) {
            return;
        }
        var profileToEdit = _(this.$scope.profiles).filter(function (p) { return p.id === selectedIds[0]; }).first();
        return profileToEdit.builtIn;
    };
    ProfileListController.prototype.showDeleteConfirmationDialog = function () {
        var _this = this;
        this.loadAllProfiles();
        this.listControl.refreshListData();
        this.profileLoadingRequest.then(function () {
            _this.hiddenProfilesDueToLimitations = _this.getHiddenProfilesDueToLimitations();
            var selectedItems = _this.getSelectedItems();
            _this.profilesCountTotal = selectedItems.length;
            var profilesToDelete = selectedItems;
            var profileListController = _this;
            _this.calculateTotalNodesCount().then(function (nodeCount) {
                _this.nodeCount = nodeCount;
                _this.scmModalService.showModal({
                    template: __webpack_require__(283),
                    size: "md",
                }, {
                    title: _this.getConfirmationDialogTitle(_this.profilesCountTotal),
                    status: "warning",
                    actionButtonText: profileListController._t("Delete"),
                    viewModel: _this,
                    cancelButtonText: profileListController._t("Close"),
                    buttons: [{
                            name: "delete",
                            displayStyle: "tertiary",
                            text: profileListController._t("Delete"),
                            action: function () {
                                if (_this.swDemoService.isDemoMode()) {
                                    _this.swDemoService.showDemoErrorToast();
                                    return true;
                                }
                                var postData = {
                                    profilesIds: profilesToDelete
                                };
                                profileListController.scmProfileService.deleteProfiles(postData).then(function () {
                                    profileListController.xuiToastService.success(profileListController._t("Profiles were successfully deleted."));
                                    _this.refreshList();
                                });
                                return true;
                            }
                        }]
                });
            });
        });
    };
    ProfileListController.prototype.onProfileClick = function (profile) {
        if (!this.$scope.displayToolbar) {
            return;
        }
        if (profile.builtIn) {
            this.viewProfile(profile.id);
        }
        else {
            this.editProfile(profile.id);
        }
    };
    ProfileListController.prototype.getHiddenProfilesDueToLimitations = function () {
        var _this = this;
        return _(this.$scope.profiles)
            .filter(function (profile) { return profile.assignedNodesCount < profile.totalAssignedNodesCount &&
            _(_this.$scope.profileState.selection.items).find(function (selectedProfileId) { return selectedProfileId === profile.id; }); })
            .map(function (profile) {
            return profile.name;
        })
            .value();
    };
    ProfileListController.prototype.shouldShowDeleteButton = function () {
        if (!(this.$scope.displayToolbar
            && this.$scope.profileState.items.length > 0
            && !this.isAnyOfSelectedProfilesBuiltIn())) {
            return false;
        }
        return this.isMoreThanZeroProfilesSelected();
    };
    ProfileListController.prototype.shouldShowEditButton = function () {
        return this.isExactlyOneProfileSelected() && !this.selectedItemIsBuiltIn();
    };
    ProfileListController.prototype.shouldShowDetailsButton = function () {
        return this.isExactlyOneProfileSelected();
    };
    ProfileListController.prototype.shouldShowCopyButton = function () {
        return this.isExactlyOneProfileSelected();
    };
    ProfileListController.prototype.shouldShowAddButton = function () {
        if (!this.isSelectionDefined()) {
            return;
        }
        return this.$scope.profileState.selection.blacklist
            ? this.$scope.profileState.selection.items.length === this.$scope.profiles.length
            : this.$scope.profileState.selection.items.length === 0;
    };
    ProfileListController.prototype.shouldShowAssignButton = function () {
        return this.isMoreThanZeroProfilesSelected();
    };
    ProfileListController.prototype.shouldShowExportButton = function () {
        return this.isMoreThanZeroProfilesSelected();
    };
    ProfileListController.prototype.isSelectionDefined = function () {
        return this.$scope.profileState.selection !== undefined && this.$scope.profileState.selection.items !== undefined;
    };
    ProfileListController.prototype.isExactlyOneProfileSelected = function () {
        if (!this.isSelectionDefined()) {
            return false;
        }
        var selectedItemCount = this.$scope.profileState.selection.blacklist
            ? this.$scope.profiles.length - this.$scope.profileState.selection.items.length
            : this.$scope.profileState.selection.items.length;
        return selectedItemCount === 1;
    };
    ProfileListController.prototype.isMoreThanZeroProfilesSelected = function () {
        if (!this.isSelectionDefined()) {
            return false;
        }
        return this.$scope.profileState.selection.blacklist
            ? this.$scope.profileState.selection.items.length < this.$scope.profiles.length
            : this.$scope.profileState.selection.items.length > 0;
    };
    ProfileListController.prototype.getConfirmationDialogTitle = function (profilesCountTotal) {
        if (profilesCountTotal > 1) {
            return this._t("Delete profiles");
        }
        else {
            return this._t("Delete profile");
        }
    };
    ProfileListController.prototype.getSelectedItems = function () {
        var _this = this;
        if (this.$scope.profileState.selection.blacklist) {
            return this.$scope.filteredProfilesIds
                .filter(function (profileElementId) { return !_(_this.$scope.profileState.selection.items).includes(profileElementId); });
        }
        return this.$scope.profileState.selection.items;
    };
    ProfileListController.prototype.createProfileState = function () {
        this.$scope.profileState = {
            sorting: {
                sortableColumns: [
                    { id: "orderedName", label: this._t("Name") }
                ],
                sortBy: {
                    id: "orderedName",
                    label: this._t("Name")
                },
                direction: "asc"
            },
            pagination: {
                page: 1,
                pageSize: 10,
                total: 200
            },
            filters: {
                filterProperties: [
                    {
                        id: "builtIn",
                        title: this._t("Type"),
                        checkboxes: [
                            {
                                id: "true",
                                title: this._t("Out-of-the-box"),
                                count: undefined
                            },
                            {
                                id: "false",
                                title: this._t("Custom"),
                                count: undefined
                            }
                        ],
                    }
                ],
                visibleFilterPropertyIds: ["builtIn"],
                filterValues: {
                    "builtIn": {
                        checkboxes: {
                            "true": {
                                checked: false,
                                $visible: true
                            },
                            "false": {
                                checked: false,
                                $visible: true
                            }
                        },
                        visibleValues: [],
                        expanded: true
                    }
                }
            },
            options: {
                layout: "contained",
                stripe: false,
                selectionMode: "multi",
                selectionProperty: "id",
                trackBy: "id",
                templateUrl: "profileList-item",
                rowPadding: "narrow",
                allowSelectAllPages: true,
                busyMessage: this._t("Loading profiles, please wait..."),
                showAddRemoveFilterProperty: false
            }
        };
    };
    ProfileListController.prototype.isAnyOfSelectedProfilesBuiltIn = function () {
        var selectedProfiles = this.getSelectedItems();
        var allProfiles = this.$scope.profiles;
        var _loop_1 = function (i) {
            var profileId = selectedProfiles[i];
            var result = _(allProfiles).filter(function (x) { return x.id === profileId; }).first();
            if (result.builtIn === true) {
                return { value: true };
            }
        };
        for (var i = 0; i < selectedProfiles.length; i++) {
            var state_1 = _loop_1(i);
            if (typeof state_1 === "object")
                return state_1.value;
        }
        return false;
    };
    ProfileListController.prototype.calculateTotalNodesCount = function () {
        var selectedProfiles = this.getSelectedItems();
        var nodeIds = [];
        for (var i = 0; i < selectedProfiles.length; i++) {
            var profileId = selectedProfiles[i];
            nodeIds.push(this.scmProfileService.getServerConfigurationIdsForProfile(profileId));
        }
        return this.$q.all(nodeIds).then(function (x) { return _(x).flatten().uniq().value().length; });
    };
    ProfileListController.prototype.formatNodesCount = function (nodesCount) {
        return nodesCount + " " + this._t(nodesCount === 1 ? "node" : "nodes");
    };
    ProfileListController.prototype.formatSureMessage = function () {
        var profilesToDeleteCount = this.$scope.profileState.selection.items.length;
        if (profilesToDeleteCount === 1) {
            return this._t("Are you sure you want to delete selected profile?");
        }
        return this._t("Are you sure you want to delete selected profiles?");
    };
    ProfileListController.prototype.formatWarningMessageStart = function () {
        var enumeratedProfiles = _(this.hiddenProfilesDueToLimitations)
            .map(function (x) { return x = " '" + x + "'"; })
            .join(",")
            .concat(".");
        if (this.profilesCountTotal === 1) {
            return this.swUtil.formatString(this._t("The following profile is also assigned to nodes you cannot see due to account limitations:{0}"), enumeratedProfiles);
        }
        return this.swUtil.formatString(this._t("The following profiles are also assigned to nodes you cannot see due to account limitations:{0}"), enumeratedProfiles);
    };
    ProfileListController.prototype.formatWarningMessageEnd = function () {
        return this._t("Keep in mind that deleting profiles will affect all assigned nodes.\n        Contact your Orion administrator for more information.");
    };
    ProfileListController.prototype.formatConfigurationWarning = function () {
        if (this.nodeCount === 0) {
            return "";
        }
        if (this.nodeCount === 1) {
            return this._t("Any profile configuration data collected for assigned node will no longer be available.");
        }
        return this._t("Any profile configuration data collected for assigned nodes will no longer be available.");
    };
    ProfileListController.prototype.formatSelectedProfiles = function () {
        if (this.nodeCount === 0 && this.profilesCountTotal === 1) {
            return this.swUtil.formatString(this._t("The selected {0} profile is currently not assigned to any nodes."), this.profilesCountTotal);
        }
        if (this.nodeCount === 0 && this.profilesCountTotal > 1) {
            return this.swUtil.formatString(this._t("The selected {0} profiles are currently not assigned to any nodes."), this.profilesCountTotal);
        }
        if (this.profilesCountTotal === 1) {
            if (this.nodeCount === 1) {
                return this.swUtil.formatString(this._t("The selected {0} profile is currently assigned to 1 node."), this.profilesCountTotal);
            }
            if (this.nodeCount > 1) {
                return this.swUtil.formatString(this._t("The selected {0} profiles are currently assigned to {1} nodes."), this.profilesCountTotal, this.nodeCount);
            }
        }
        if (this.nodeCount === 1) {
            return this.swUtil.formatString(this._t("The selected {0} profiles are currently assigned to 1 node."), this.profilesCountTotal);
        }
        if (this.nodeCount > 1) {
            return this.swUtil.formatString(this._t("The selected {0} profiles are currently assigned to {1} nodes."), this.profilesCountTotal, this.nodeCount);
        }
    };
    ProfileListController.prototype.createProfileDispatcher = function () {
        this.profileDispatcher = this.xuiFilteredListService.getDispatcherInstance(this.xuiFilteredListService.getModel(this.$scope, "profileState"), {
            dataSource: {
                getItems: this.getProfileSources.bind(this)
            }
        });
    };
    ProfileListController.prototype.getProfileSources = function (params) {
        var _this = this;
        this.$log.debug("Called function with params", params);
        return this.profileLoadingRequest.then(function (data) {
            _this.$scope.profileState.pagination.total = data.length;
            var result = _(data).map(function (x) {
                return {
                    id: x.id,
                    name: x.name,
                    builtIn: x.builtIn,
                    description: x.description,
                    nodesCount: _this.formatNodesCount(x.assignedNodesCount),
                    orderedName: x.name.toLocaleLowerCase()
                };
            }).value();
            var profiles = _this.xuiArraySourceFactoryService.createItemSource(result, {
                propertiesToSearch: ["name"]
            });
            var paramsWithoutPagination = {
                pagination: null,
                search: params.search,
                sorting: params.sorting,
                filters: params.filters,
                reset: params.reset
            };
            var filteredProfilesWithoutPagination = profiles(paramsWithoutPagination);
            filteredProfilesWithoutPagination.then(function (filteredProfiles) {
                _this.$scope.filteredProfilesIds = filteredProfiles.items.map(function (items) { return items.id; });
            });
            return profiles(params);
        })
            .catch(function (error) {
            _this.$log.error(error);
            _this.xuiToastService.error(_this._t("Unable to load profiles from database. Try again later."));
        });
    };
    return ProfileListController;
}());
exports.default = ProfileListController;


/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var AssignProfileWizardStepName_1 = __webpack_require__(51);
var MonitoredNodeListController = /** @class */ (function () {
    /** @ngInject */
    MonitoredNodeListController.$inject = ["$scope", "$log", "xuiFilteredListService", "xuiToastService", "getTextService", "$templateCache", "$timeout", "scmAssignProfileWizardDialogService", "scmUnassignProfilesDialogService", "scmFilteredListService", "scmModalService", "swUtil", "scmProfileService", "scmKeepDataDialogService", "scmDeleteBaselinesDialogService", "scmBulkUpdateBaselinesDialogService", "scmBaselineService", "scmDatetimeFilter", "$q", "swDemoService", "scmOrionNodeService", "scmCentralizedSettingsService", "scmEnableWhoDetectionDialog", "scmNodeConfigService", "scmIFrameDialogService"];
    function MonitoredNodeListController($scope, $log, xuiFilteredListService, xuiToastService, getTextService, $templateCache, $timeout, scmAssignProfileWizardDialogService, scmUnassignProfilesDialogService, scmFilteredListService, scmModalService, swUtil, scmProfileService, scmKeepDataDialogService, scmDeleteBaselinesDialogService, scmBulkUpdateBaselinesDialogService, scmBaselineService, scmDatetimeFilter, $q, swDemoService, scmOrionNodeService, scmCentralizedSettingsService, scmEnableWhoDetectionDialog, scmNodeConfigService, scmIFrameDialogService) {
        this.$scope = $scope;
        this.$log = $log;
        this.xuiFilteredListService = xuiFilteredListService;
        this.xuiToastService = xuiToastService;
        this.getTextService = getTextService;
        this.$templateCache = $templateCache;
        this.$timeout = $timeout;
        this.scmAssignProfileWizardDialogService = scmAssignProfileWizardDialogService;
        this.scmUnassignProfilesDialogService = scmUnassignProfilesDialogService;
        this.scmFilteredListService = scmFilteredListService;
        this.scmModalService = scmModalService;
        this.swUtil = swUtil;
        this.scmProfileService = scmProfileService;
        this.scmKeepDataDialogService = scmKeepDataDialogService;
        this.scmDeleteBaselinesDialogService = scmDeleteBaselinesDialogService;
        this.scmBulkUpdateBaselinesDialogService = scmBulkUpdateBaselinesDialogService;
        this.scmBaselineService = scmBaselineService;
        this.scmDatetimeFilter = scmDatetimeFilter;
        this.$q = $q;
        this.swDemoService = swDemoService;
        this.scmOrionNodeService = scmOrionNodeService;
        this.scmCentralizedSettingsService = scmCentralizedSettingsService;
        this.scmEnableWhoDetectionDialog = scmEnableWhoDetectionDialog;
        this.scmNodeConfigService = scmNodeConfigService;
        this.scmIFrameDialogService = scmIFrameDialogService;
        this.isBusy = false;
        this.isBusyMessage = "Please wait...";
        this.userSettingName = "ScmNodeListUserSettings";
        this._t = getTextService;
    }
    MonitoredNodeListController.prototype.$onInit = function () {
        var _this = this;
        this.availableFilterPropertiesRequest = this.scmFilteredListService.getAvailableFilterPropertiesForScmNodes();
        this.createNodeState();
        this.createNodeDispatcher();
        this.getFimEnabledGloballySetting();
        this.scmEnableWhoDetectionDialog.onWhoDetectionEnabled(function () { return _this.refreshList(); });
        this.$scope.$watch("monitoredNodeList.nodeState.pagination", function () {
            if (_this.scmFilteredListService.fixPagination(_this.nodeState.pagination.total, _this.nodeState)) {
                _this.refreshList();
            }
        });
        this.$templateCache.put("scmMonitoredNodeList-item", "<scm-node-detail [node]=\"item\" [show-monitored-since]=\"vm.isMonitoredSinceChecked()\"\n            [is-fim-enabled-globally]=\"vm.isFimEnabledGlobally\"></scm-node-detail>");
    };
    MonitoredNodeListController.prototype.triggerOnLoaded = function () {
        if (angular.isFunction(this.$scope.onLoaded)) {
            this.$timeout(this.$scope.onLoaded);
        }
    };
    MonitoredNodeListController.prototype.refreshList = function () {
        var _this = this;
        this.nodeState.selection.items = [];
        this.nodeState.selection.blacklist = false;
        this.$timeout(function () {
            _this.listControl.refreshListData();
        });
    };
    MonitoredNodeListController.prototype.createNodeState = function () {
        this.nodeState = {
            sorting: {
                sortableColumns: [
                    { id: "Orion.Nodes|name", label: this._t("Name") },
                    { id: "Orion.Nodes|nodeStatus", label: this._t("Status") },
                    { id: "Orion.Nodes|machineType", label: this._t("Platform") },
                    { id: "Orion.SCM.ServerConfiguration|assigned", label: this._t("Monitored Since") }
                ],
                sortBy: {
                    id: "Orion.Nodes|name",
                    label: this._t("Name")
                },
                direction: "asc"
            },
            filters: {
                filterProperties: [],
                visibleFilterPropertyIds: []
            },
            pagination: {
                page: 1,
                pageSize: 10,
                total: 200
            },
            options: {
                layout: "contained",
                stripe: false,
                selectionMode: "multi",
                selectionProperty: "id",
                trackBy: "id",
                templateUrl: "scmMonitoredNodeList-item",
                rowPadding: "narrow",
                allowSelectAllPages: true,
                showEmptyFilterProperties: true,
                categorizedFilterPanel: true,
                showMorePropertyValuesThreshold: 10
            }
        };
    };
    MonitoredNodeListController.prototype.createNodeDispatcher = function () {
        var _this = this;
        this.nodeDispatcher = this.xuiFilteredListService.getDispatcherInstance(this.xuiFilteredListService.getModel(this, "nodeState"), {
            dataSource: {
                getItems: this.createGetItemsSource(),
                getAvailableFilters: function () {
                    return _this.availableFilterPropertiesRequest;
                },
                getFilters: function (params) {
                    return _this.scmFilteredListService.getVisibleFilterPropertiesDetailsForScmNodes(params);
                }
            }
        });
    };
    MonitoredNodeListController.prototype.createGetItemsSource = function () {
        var _this = this;
        return function (params) {
            return _this.availableFilterPropertiesRequest
                .then(function () { return _this.setDefaultFiltersInTree.bind(_this)(); })
                .then(function () { return _this.getNodesSources.bind(_this, params)(); });
        };
    };
    MonitoredNodeListController.prototype.getNodesSources = function (params) {
        var _this = this;
        var dataSourceParameters = this.scmFilteredListService.getNodeListDataSourceParametersFromIFilteredListItemSourceParams(params);
        dataSourceParameters.includeProfileHeaders = true;
        return this.scmFilteredListService.getFilteredScmNodes(dataSourceParameters)
            .then(function (loaded) {
            var nodeIds = _(loaded.items).map(function (x) { return x.nodeId; }).value();
            return _this.$q.all([_this.scmNodeConfigService.getProfilesWithErrors(nodeIds), loaded]);
        })
            .then(function (allData) {
            var data = allData[1];
            _this.nodes = data.items;
            _this.triggerOnLoaded.bind(_this);
            var result = _(data.items).map(function (x) {
                var profilesWithErrorsForNode = _(allData[0]).filter(function (y) { return y.nodeId === x.nodeId; }).value();
                return {
                    id: x.nodeId,
                    name: x.name,
                    ipAddress: x.ipAddress,
                    machineType: x.machineType,
                    vendorIcon: x.vendorIcon,
                    nodeStatus: x.nodeStatus,
                    detailsUrl: x.detailsUrl,
                    isServer: x.isServer,
                    pollingMethod: x.pollingMethod,
                    isAgentDeployed: x.isAgentDeployed,
                    profiles: x.profiles,
                    policyProfiles: x.policyProfiles,
                    assigned: x.assigned,
                    licensed: x.licensed,
                    errorCount: x.errorCount,
                    baselineInfo: x.baselineInfo,
                    profileCount: x.profileCount,
                    policyCount: x.policyCount,
                    excludedFromWho: x.excludedFromWho,
                    profilesWithErrors: profilesWithErrorsForNode
                };
            }).value();
            return { items: result, total: data.totalCount };
        }).catch(function (error) {
            _this.$log.error(error);
            _this.xuiToastService.error(_this._t("Unable to load nodes from database. Try again later."));
        });
    };
    MonitoredNodeListController.prototype.shouldShowSetupButton = function () {
        if (!this.isSelectionDefined()) {
            return true;
        }
        return this.nodeState.selection.blacklist
            ? this.nodeState.pagination.total === this.nodeState.selection.items.length
            : this.nodeState.selection.items.length === 0;
    };
    MonitoredNodeListController.prototype.shouldShowAssignButton = function () {
        if (!this.isSelectionDefined()) {
            return false;
        }
        return this.nodeState.selection.blacklist
            ? this.nodeState.pagination.total - this.nodeState.selection.items.length > 0
            : this.nodeState.selection.items.length > 0;
    };
    MonitoredNodeListController.prototype.shouldShowBaselineButtons = function () {
        return this.shouldShowAssignButton();
    };
    MonitoredNodeListController.prototype.isSelectionDefined = function () {
        return this.nodeState.selection !== undefined && this.nodeState.selection.items !== undefined;
    };
    MonitoredNodeListController.prototype.setup = function () {
        var _this = this;
        var settings = {
            selectedNodes: [],
            selectedProfiles: [],
            wizardStep: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Profiles
        };
        this.scmAssignProfileWizardDialogService.showModal(settings).then(function () {
            _this.refreshList();
        });
    };
    MonitoredNodeListController.prototype.assign = function () {
        var _this = this;
        this.getSelectedScmNodeIds().then(function (data) {
            var settings = {
                selectedNodes: data,
                selectedProfiles: [],
                wizardStep: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Profiles
            };
            return _this.scmAssignProfileWizardDialogService.showModal(settings);
        }).then(function () { return _this.refreshList(); });
    };
    MonitoredNodeListController.prototype.unassign = function () {
        var _this = this;
        this.getSelectedScmNodeIds().then(function (nodeIds) {
            return _this.scmUnassignProfilesDialogService.showModal(nodeIds, function () {
                _this.refreshList();
            });
        });
    };
    MonitoredNodeListController.prototype.unassignAll = function () {
        var _this = this;
        this.getSelectedScmNodeIds().then(function (nodeIds) {
            return _this.scmModalService.showModal({
                size: "md",
            }, {
                title: _this._t("Really unassign all profiles?"),
                message: _this.swUtil.formatString(_this._t("All configuration profiles will be unassigned from the selected {0} node{1}. " +
                    "Collected configuration data will no longer be available once the profiles are unassigned. " +
                    "Are you sure you want to stop monitoring server configuration on the selected nodes?"), nodeIds.length, nodeIds.length > 1 ? "s" : ""),
                status: "warning",
                viewModel: _this,
                cancelButtonText: _this._t("Close"),
                buttons: [{
                        name: "unassignAll",
                        displayStyle: "tertiary",
                        text: _this._t("Unassign all"),
                        action: function () {
                            if (_this.swDemoService.isDemoMode()) {
                                _this.swDemoService.showDemoErrorToast();
                                return true;
                            }
                            _this.scmKeepDataDialogService.showKeepDataModal(function (keepData) {
                                var postData = {
                                    nodeIds: nodeIds,
                                    keepHistory: keepData
                                };
                                _this.scmProfileService.unassignAllFromNodes(postData).then(function () {
                                    _this.xuiToastService.success(_this._t("All profiles have been successfully unassigned from selected nodes."));
                                    _this.refreshList();
                                })
                                    .catch(function (error) {
                                    _this.$log.error(error);
                                    _this.xuiToastService.error(_this._t("Unassign all profiles process has failed. Please try again later."));
                                });
                            })
                                .then(function (resolution) {
                                if (resolution === "cancel") {
                                    _this.unassignAll();
                                }
                            });
                            return true;
                        }
                    }]
            });
        });
    };
    MonitoredNodeListController.prototype.getFailedNodes = function (nodeBaselines, nodeNames) {
        var nodes = _.filter(nodeBaselines, function (nodeBaseline) { return nodeBaseline.baselineCreationStatus !== "Ok" /* Ok */; });
        var result = nodes.map(function (node) {
            var nodeName = _.find(nodeNames, function (name) { return name.nodeId === node.nodeId; });
            return __assign({}, node, { caption: nodeName.caption });
        });
        return result;
    };
    MonitoredNodeListController.prototype.bulkUpdateBaselines = function () {
        var _this = this;
        this.isBusy = true;
        this.isBusyMessage = this._t("Redefining baselines, please wait...");
        var nodeIds;
        this.getSelectedScmNodes()
            .then(function (selectedNodes) {
            nodeIds = selectedNodes.items.map(function (i) { return i.nodeId; });
            return _this.scmBulkUpdateBaselinesDialogService.showModal(selectedNodes.items);
        })
            .then(function (dialogOutput) {
            if (dialogOutput === "cancel" || _this.swDemoService.isDemoMode()) {
                return;
            }
            var currentTimeStamp = new Date();
            return _this.$q.all([_this.scmBaselineService.bulkUpdateBaselines(nodeIds, currentTimeStamp), _this.scmOrionNodeService.getNodeNames(nodeIds)])
                .then(function (_a) {
                var nodeBaselines = _a[0], nodeNames = _a[1];
                var failedNodes = _this.getFailedNodes(nodeBaselines, nodeNames);
                if (failedNodes.length === 0) {
                    _this.xuiToastService.success(_this.swUtil.formatString(_this._t("Baselines for the {0} nodes were successfully redefined to {1}."), nodeBaselines.length, _this.scmDatetimeFilter(currentTimeStamp, "long")));
                }
                else {
                    _this.scmIFrameDialogService.showFusionModal("baseline-error", { size: "md" }, { failedNodes: failedNodes });
                }
                _this.refreshList();
            })
                .catch(function () {
                _this.xuiToastService.error(_this._t("Baseline was not defined."));
            });
        })
            .finally(function () {
            _this.isBusy = false;
        });
    };
    MonitoredNodeListController.prototype.getSelectedScmNodes = function () {
        var nodeListDataSourceParameters = this.scmFilteredListService.getNodeListDataSourceParametersByFiltersAndSearch(this.nodeState.selection.items, this.nodeState.selection.blacklist, this.nodeState.filters.filterValues, this.nodeState.search);
        return this.scmFilteredListService.getFilteredScmNodes(nodeListDataSourceParameters);
    };
    MonitoredNodeListController.prototype.getSelectedScmNodeIds = function () {
        return this.getSelectedScmNodes().then(function (data) {
            return data.items.map(function (item) { return item.nodeId; });
        });
    };
    MonitoredNodeListController.prototype.setDefaultFiltersInTree = function () {
        var _this = this;
        if (_.isNil(this.defaultFiltersInTreePromise)) {
            var nodeStateFilterPath_1 = "monitoredNodeList.nodeState.filters";
            var filtersToshow = [
                "Orion.Nodes|AgentDeployed",
                "Orion.Nodes|IsServer",
                "Orion.Nodes|ObjectSubType",
                "Orion.Nodes|Status",
                "Orion.SCM.ServerConfiguration|PollingIssues",
                "Orion.SCM.ServerConfiguration|MonitoredSince",
                "Orion.SCM.Profiles|Name",
                "Orion.SCM.Profiles|BuiltIn",
                "Orion.SCM.ServerConfiguration|NodesExcludedFromWho",
                "Orion.Nodes|Platform",
                "Orion.Nodes|Vendor"
            ];
            this.defaultFiltersInTreePromise = this.scmFilteredListService.setDefaultFilters(this.userSettingName, nodeStateFilterPath_1, this.$scope, this.nodeDispatcher, filtersToshow)
                .then(function () {
                _this.adjustFilters(nodeStateFilterPath_1);
            })
                .then(function () { return _this.scmFilteredListService.startMonitoringAndSavingFilterChanges(_this.userSettingName, nodeStateFilterPath_1, _this.$scope); });
        }
        return this.defaultFiltersInTreePromise;
    };
    MonitoredNodeListController.prototype.adjustFilters = function (nodeStateFilterPath) {
        var nodeStateFilters = _(this.$scope).get(nodeStateFilterPath);
        if (!_.isNil(nodeStateFilters)) {
            var endDate = new Date();
            var startDate = moment(endDate).add("days", -7);
            var filter = nodeStateFilters.filterValues["Orion.SCM.ServerConfiguration|MonitoredSince"];
            if (!_.isNil(filter) && !_.isNil(filter.checkboxes["MonitoredSince"])) {
                var checkboxModel = filter.checkboxes["MonitoredSince"].model;
                checkboxModel.startDatetime = new Date(startDate.toString());
                checkboxModel.endDatetime = endDate;
                checkboxModel.selectedPresetId = "last7Days";
            }
        }
    };
    MonitoredNodeListController.prototype.isMonitoredSinceChecked = function () {
        if (this.nodeState.sorting.sortBy.id === "Orion.SCM.ServerConfiguration|assigned") {
            return true;
        }
        var nodeStateFilterPath = "monitoredNodeList.nodeState.filters";
        var nodeStateFilters = _(this.$scope).get(nodeStateFilterPath);
        if (!_.isNil(nodeStateFilters)) {
            var filter = nodeStateFilters.filterValues["Orion.SCM.ServerConfiguration|MonitoredSince"];
            if (!_.isNil(filter) && !_.isNil(filter.checkboxes["MonitoredSince"])) {
                return filter.checkboxes["MonitoredSince"].checked;
            }
        }
        return false;
    };
    MonitoredNodeListController.prototype.getSelectedScmNodeIdsWithBaseline = function () {
        return this.getSelectedScmNodes().then(function (data) {
            return _(data.items).filter(function (i) { return !_.isNil(i.baselineInfo) && !_.isNil(i.baselineInfo.baselineStatus)
                && i.baselineInfo.baselineStatus !== "NoBaselineSet" /* NoBaselineSet */; })
                .map(function (x) { return x.nodeId; })
                .value();
        });
    };
    MonitoredNodeListController.prototype.deleteBaselines = function () {
        var _this = this;
        this.isBusy = true;
        this.isBusyMessage = this._t("Deleting baselines, please wait...");
        this.getSelectedScmNodeIdsWithBaseline().then(function (nodeIds) {
            _this.scmDeleteBaselinesDialogService.showDeleteBaselinesModal(nodeIds).then(function (dialogOutput) {
                if (dialogOutput !== "cancel" && !_this.swDemoService.isDemoMode()) {
                    _this.printSuccessToast(nodeIds);
                }
            })
                .finally(function () {
                _this.isBusy = false;
                _this.refreshList();
            });
        });
    };
    MonitoredNodeListController.prototype.printSuccessToast = function (nodeIds) {
        if (nodeIds.length === 1) {
            this.xuiToastService.success(this.swUtil.formatString(this._t("Baseline for the {0} node was successfully deleted."), nodeIds.length));
        }
        else {
            this.xuiToastService.success(this.swUtil.formatString(this._t("Baselines for the {0} nodes were successfully deleted."), nodeIds.length));
        }
    };
    MonitoredNodeListController.prototype.getFimEnabledGloballySetting = function () {
        var _this = this;
        this.scmCentralizedSettingsService.getSetting("FimDriverWatchingEnabled")
            .then(function (fimEnabled) {
            _this.isFimEnabledGlobally = fimEnabled;
        });
    };
    return MonitoredNodeListController;
}());
exports.MonitoredNodeListController = MonitoredNodeListController;


/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var mappings_1 = __webpack_require__(95);
var profileElementSettingsHelper_1 = __webpack_require__(153);
var DialogMode;
(function (DialogMode) {
    DialogMode[DialogMode["Add"] = 0] = "Add";
    DialogMode[DialogMode["ReadOnly"] = 1] = "ReadOnly";
    DialogMode[DialogMode["FullEdit"] = 2] = "FullEdit";
    DialogMode[DialogMode["PartialEdit"] = 3] = "PartialEdit";
    DialogMode[DialogMode["Copy"] = 4] = "Copy";
})(DialogMode = exports.DialogMode || (exports.DialogMode = {}));
var DropdownOrionDefaultCredential = -1;
var DropdownAddNewCredential = -2;
var ManageProfileDialogController = /** @class */ (function () {
    /** @ngInject */
    ManageProfileDialogController.$inject = ["isReadOnly", "isConnectionStringRequired", "profile", "xuiFilteredListService", "xuiArraySourceFactoryService", "xuiToastService", "getTextService", "$timeout", "$q", "scmProfileService", "scmNodesListDialogService", "scmModalService", "scmElementDisplayService", "scmCentralizedSettingsService", "scmValidationService", "scmTestJobService", "swUtil", "swDemoService", "scmFilteredListService", "scmAuthorizationService", "scmCredentialService", "$templateCache", "scmAddCredentialDialogService"];
    function ManageProfileDialogController(isReadOnly, isConnectionStringRequired, profile, xuiFilteredListService, xuiArraySourceFactoryService, xuiToastService, getTextService, $timeout, $q, scmProfileService, scmNodesListDialogService, scmModalService, scmElementDisplayService, scmCentralizedSettingsService, scmValidationService, scmTestJobService, swUtil, swDemoService, scmFilteredListService, scmAuthorizationService, scmCredentialService, $templateCache, scmAddCredentialDialogService) {
        this.isReadOnly = isReadOnly;
        this.isConnectionStringRequired = isConnectionStringRequired;
        this.profile = profile;
        this.xuiFilteredListService = xuiFilteredListService;
        this.xuiArraySourceFactoryService = xuiArraySourceFactoryService;
        this.xuiToastService = xuiToastService;
        this.getTextService = getTextService;
        this.$timeout = $timeout;
        this.$q = $q;
        this.scmProfileService = scmProfileService;
        this.scmNodesListDialogService = scmNodesListDialogService;
        this.scmModalService = scmModalService;
        this.scmElementDisplayService = scmElementDisplayService;
        this.scmCentralizedSettingsService = scmCentralizedSettingsService;
        this.scmValidationService = scmValidationService;
        this.scmTestJobService = scmTestJobService;
        this.swUtil = swUtil;
        this.swDemoService = swDemoService;
        this.scmFilteredListService = scmFilteredListService;
        this.scmAuthorizationService = scmAuthorizationService;
        this.scmCredentialService = scmCredentialService;
        this.$templateCache = $templateCache;
        this.scmAddCredentialDialogService = scmAddCredentialDialogService;
        this.connectionStringPlaceholder = "";
        this.filterProfileElementTypes = [
            "file" /* File */,
            "registry" /* Registry */,
            "swisQuery" /* SwisQuery */,
            "parsedFile" /* ParsedFile */,
            "powershell" /* PowerShell */,
            "script" /* Script */,
            "database" /* Database */
        ];
        this._t = getTextService;
    }
    ManageProfileDialogController.prototype.$onInit = function () {
        var _this = this;
        this.createProfileElementsState();
        this.createProfileElementsDispatcher();
        this.importTemplates();
        this.credentialPicker = {
            isDialogOpen: false,
            credentialDropdown: {
                items: this.generateCredentialDropdownItems([]),
                selectedItem: null
            }
        };
        this.setDefaultMultiEditCheckboxes();
        this.scmAuthorizationService.getIsScmUserAuthorized().then(function (response) {
            _this.isUserAdmin = response;
        });
        this.scmCentralizedSettingsService.getSetting("EnableElementTypeSwisQuery").then(function (response) {
            _this.isSwisQueryElementTypeEnabled = response;
        });
        this.scmCentralizedSettingsService.getSetting("EnableElementTypePowerShellScript").then(function (response) {
            _this.isPowerShellElementTypeEnabled = response;
        });
        this.scmCentralizedSettingsService.getSetting("EnableElementTypeScript").then(function (response) {
            _this.isScriptElementTypeEnabled = response;
        });
        this.scmCentralizedSettingsService.getSetting("EnableElementTypeParsedFile").then(function (response) {
            _this.isParsedFileElementTypeEnabled = response;
        });
        this.scmCentralizedSettingsService.getSetting("EnableElementTypeDatabaseQuery").then(function (response) {
            _this.isDatabaseQueryElementTypeEnabled = response;
        });
    };
    ManageProfileDialogController.prototype.onChangeCredentialDropdown = function (newValue) {
        var _this = this;
        this.profileElementForm.CredentialDropdown.$setValidity("credentialEmpty", true, this.profileElementForm);
        if (newValue.id === DropdownAddNewCredential) {
            if (!this.credentialPicker.isDialogOpen) {
                this.credentialPicker.isDialogOpen = true;
                this.scmAddCredentialDialogService.showAddCredentialModal().then(function (credentialId) {
                    if (!_.isNil(credentialId)) {
                        _this.currentProfileElement.credentialId = credentialId;
                    }
                    _this.setCredentialsPicker();
                    _this.credentialPicker.isDialogOpen = false;
                });
            }
        }
        else {
            this.currentProfileElement.credentialId = newValue.id;
        }
        this.isConnectionStringValidAndSetValidity();
    };
    ManageProfileDialogController.prototype.isConnectionStringValidAndSetValidity = function () {
        var requireUsernamePassword = !_.isNil(this.currentProfileElement.credentialId)
            && this.currentProfileElement.credentialId !== DropdownOrionDefaultCredential;
        if (this.currentProfileElement.type === "database" /* Database */ && (!this.isMultiEdit ||
            (this.isMultiEdit && this.multiEditCheckbox.connectionString))) {
            this.profileElementForm.ConnectionString.$setTouched();
            if (!this.scmValidationService.isConnectionStringValid(this.currentProfileElement, requireUsernamePassword)) {
                this.profileElementForm.ConnectionString.$setValidity("connectionStringMacroRequired", false, this.profileElementForm);
                return false;
            }
            else {
                this.profileElementForm.ConnectionString.$setValidity("connectionStringMacroRequired", true, this.profileElementForm);
            }
        }
        return true;
    };
    ManageProfileDialogController.prototype.displayCredentialPicker = function () {
        return this.currentProfileElement.type !== "swisQuery" /* SwisQuery */;
    };
    ManageProfileDialogController.prototype.generateCredentialDropdownItems = function (items) {
        var addNewCredsItem = { name: this._t("Add new credentials"), id: DropdownAddNewCredential };
        var defaultCredsItem = { name: this._t("Use Orion default credentials"), id: DropdownOrionDefaultCredential };
        var credItems = [];
        credItems.push(defaultCredsItem);
        credItems.push({ separator: true });
        if (items.length > 0) {
            credItems = credItems.concat(items);
            credItems.push({ separator: true });
        }
        credItems.push(addNewCredsItem);
        return credItems;
    };
    ManageProfileDialogController.prototype.setDefaultMultiEditCheckboxes = function () {
        this.multiEditCheckbox = {
            connectionString: false,
            credential: false
        };
    };
    ManageProfileDialogController.prototype.setCredentialsPicker = function () {
        var _this = this;
        this.scmCredentialService.getAll().then(function (response) {
            _this.credentialPicker.credentialDropdown.items = _this.generateCredentialDropdownItems(response);
            if (_.isNil(_this.currentProfileElement.credentialId)) {
                _this.currentProfileElement.credentialId = null;
                _this.credentialPicker.credentialDropdown.selectedItem = null;
            }
            else if (_this.currentProfileElement.credentialId === DropdownOrionDefaultCredential) {
                _this.credentialPicker.credentialDropdown.selectedItem = _this.getSelectedItemFromDropdown(DropdownOrionDefaultCredential);
            }
            else {
                _this.credentialPicker.credentialDropdown.selectedItem = _this.getSelectedItemFromDropdown(_this.currentProfileElement.credentialId);
            }
            _this.isCredentialRequired = _.isNil(_this.credentialPicker.credentialDropdown.selectedItem);
        });
    };
    ManageProfileDialogController.prototype.getSelectedItemFromDropdown = function (id) {
        return _(this.credentialPicker.credentialDropdown.items)
            .filter(function (c) { return c.id === id; }).first();
    };
    Object.defineProperty(ManageProfileDialogController.prototype, "selectedProfileElementType", {
        get: function () {
            return this.selectedProfileElementTypeInternal;
        },
        set: function (value) {
            this.selectedProfileElementTypeInternal = value;
            this.currentProfileElement.type = value.name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ManageProfileDialogController.prototype, "elementDisplay", {
        get: function () { return this.scmElementDisplayService; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ManageProfileDialogController.prototype, "testJobNode", {
        get: function () {
            return this.scmTestJobService.selectedTestJobNode;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ManageProfileDialogController.prototype, "testButton", {
        get: function () {
            var _this = this;
            return {
                name: "config-element-dialog-test",
                text: this._t("Test"),
                isPrimary: false,
                isHidden: !this.isUserAdmin,
                action: function (dialog) {
                    if (_this.swDemoService.isDemoMode()) {
                        _this.swDemoService.showDemoErrorToast();
                        return;
                    }
                    _this.setInvalidFieldsDirty();
                    return _this.inputIsOk({ skipDuplicityCheck: true, skipCheckNodeIsInQuery: true }).then(function (ok) {
                        if (dialog.$valid && ok) {
                            _this.runElementTest();
                        }
                        return false;
                    });
                }
            };
        },
        enumerable: true,
        configurable: true
    });
    ;
    ManageProfileDialogController.prototype.fillProfileElementTypes = function (isReadOnly) {
        this.profileElementTypes = [];
        this.profileElementTypes.push({
            name: "file" /* File */,
            value: this._t(mappings_1.ProfileElementTypeFullText["file" /* File */])
        });
        this.profileElementTypes.push({
            name: "registry" /* Registry */,
            value: this._t(mappings_1.ProfileElementTypeFullText["registry" /* Registry */])
        });
        if (this.isSwisQueryElementTypeEnabled || isReadOnly) {
            this.profileElementTypes.push({
                name: "swisQuery" /* SwisQuery */,
                value: this._t(mappings_1.ProfileElementTypeFullText["swisQuery" /* SwisQuery */])
            });
        }
        if (this.isPowerShellElementTypeEnabled || isReadOnly) {
            this.profileElementTypes.push({
                name: "powershell" /* PowerShell */,
                value: this._t(mappings_1.ProfileElementTypeFullText["powershell" /* PowerShell */])
            });
        }
        if (this.isScriptElementTypeEnabled || isReadOnly) {
            this.profileElementTypes.push({
                name: "script" /* Script */,
                value: this._t(mappings_1.ProfileElementTypeFullText["script" /* Script */])
            });
        }
        if (this.isParsedFileElementTypeEnabled || isReadOnly) {
            this.profileElementTypes.push({
                name: "parsedFile" /* ParsedFile */,
                value: this._t(mappings_1.ProfileElementTypeFullText["parsedFile" /* ParsedFile */])
            });
        }
        if (this.isDatabaseQueryElementTypeEnabled || isReadOnly) {
            this.profileElementTypes.push({
                name: "database" /* Database */,
                value: this._t(mappings_1.ProfileElementTypeFullText["database" /* Database */])
            });
        }
    };
    ManageProfileDialogController.prototype.editProfileElement = function (isMultiEdit) {
        var _this = this;
        this.setDefaultMultiEditCheckboxes();
        var title = this._t("Edit configuration element");
        if (isMultiEdit) {
            this.fillMultipleProfileElementDialogWithSelectedProfileElement();
            title = this._t("Edit configuration elements");
        }
        else {
            this.fillProfileElementDialogWithSelectedProfileElement();
        }
        this.setCredentialsPicker();
        var mode = this.profile.profile.assignedNodesCount === 0 || !this.areSelectedElementsSaved()
            ? DialogMode.FullEdit : DialogMode.PartialEdit;
        var buttons = [
            {
                name: "config-element-edit-dialog-save",
                text: this._t("Save"),
                isPrimary: true,
                action: function (dialog) {
                    if (_this.swDemoService.isDemoMode()) {
                        _this.swDemoService.showDemoErrorToast();
                        return;
                    }
                    return _this.inputIsOk().then(function (ok) {
                        if (dialog.$valid && ok) {
                            _this.editElementToProfile();
                            return true;
                        }
                        return false;
                    });
                },
                isHidden: false
            }
        ];
        if (!this.isMultiEdit) {
            buttons.push(this.testButton);
        }
        return this.showProfileElementDialog({
            title: title,
            actionButtonText: this._t("Save"),
            cancelButtonText: this._t("Cancel"),
            viewModel: this,
            buttons: buttons
        }, mode).finally(function () {
            _this.connectionStringPlaceholder = "";
        });
    };
    ManageProfileDialogController.prototype.validateCredential = function () {
        var isCredentialValid = this.isCredentialValid();
        this.setCredentialDropDownValidity(isCredentialValid);
        return isCredentialValid;
    };
    ManageProfileDialogController.prototype.isCredentialValid = function () {
        if (this.currentProfileElement.type !== "swisQuery" /* SwisQuery */ && this.manageProfileElementDialogCredentialEnabled) {
            if (this.credentialHasExistingId(this.credentialPicker.credentialDropdown.selectedItem)) {
                return true;
            }
            else {
                return this.tryApplyCredentialsByInput();
            }
        }
        else {
            return true;
        }
    };
    ManageProfileDialogController.prototype.credentialHasExistingId = function (selectedCredentialInDropdown) {
        if (this.credentialInDropdownHasId(selectedCredentialInDropdown)) {
            if (selectedCredentialInDropdown.id !== DropdownOrionDefaultCredential) {
                return true;
            }
        }
        return false;
    };
    ManageProfileDialogController.prototype.credentialInDropdownHasId = function (selectedCredentialInDropdown) {
        return _.has(selectedCredentialInDropdown, "id");
    };
    ManageProfileDialogController.prototype.tryApplyCredentialsByInput = function () {
        var credentialsDropdownInput = this.profileElementForm.CredentialDropdown.$viewValue;
        var creds = this.credentialPicker.credentialDropdown.items.filter(function (x) { return x.name === credentialsDropdownInput; })[0];
        if (creds === undefined) {
            return false;
        }
        else {
            this.currentProfileElement.credentialId = creds.id;
            return true;
        }
    };
    ManageProfileDialogController.prototype.setCredentialDropDownValidity = function (isValid) {
        if (this.profileElementForm.CredentialDropdown === undefined) {
            return;
        }
        this.profileElementForm.CredentialDropdown.$setTouched();
        if (this.credentialDropdownViewValueIsEmpty()) {
            this.profileElementForm.CredentialDropdown.$setValidity("credentialEmpty", isValid, this.profileElementForm);
        }
        else {
            this.profileElementForm.CredentialDropdown.$setValidity("required", isValid, this.profileElementForm);
        }
    };
    ManageProfileDialogController.prototype.credentialDropdownViewValueIsEmpty = function () {
        if (this.profileElementForm.CredentialDropdown !== undefined) {
            return this.profileElementForm.CredentialDropdown.$viewValue === "";
        }
    };
    ManageProfileDialogController.prototype.copyProfileElement = function () {
        var _this = this;
        this.setDefaultMultiEditCheckboxes();
        this.fillProfileElementDialogWithSelectedProfileElement();
        delete this.currentProfileElement.elementId;
        if (this.currentProfileElement.displayAlias) {
            this.currentProfileElement.displayAlias = "Copy of " + this.currentProfileElement.displayAlias;
        }
        this.setCredentialsPicker();
        return this.showProfileElementDialog({
            title: this._t("Copy configuration element"),
            actionButtonText: this._t("Add"),
            cancelButtonText: this._t("Cancel"),
            viewModel: this,
            buttons: [
                {
                    name: "config-element-add-dialog-save",
                    text: this._t("Add"),
                    isPrimary: true,
                    action: function (dialog) {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                            return;
                        }
                        _this.setInvalidFieldsDirty();
                        return _this.inputIsOk().then(function (ok) {
                            if (dialog.$valid && ok) {
                                _this.addElementToProfile();
                                return true;
                            }
                            return false;
                        });
                    },
                    isHidden: false
                },
                this.testButton
            ]
        }, DialogMode.Copy);
    };
    ManageProfileDialogController.prototype.fillProfileElementDialogWithSelectedProfileElement = function () {
        var selectedIds = this.getSelectedItems();
        if (selectedIds.length !== 1) {
            return;
        }
        this.fillProfileElementTypes(true);
        this.currentlyEditedElement = [this.profile.profileElements[selectedIds[0]]];
        this.currentProfileElement = _.cloneDeep(this.currentlyEditedElement[0]);
        this.selectProfileElementType(this.currentProfileElement.type);
        this.isMultiEdit = false;
        this.oldProfileElementSettingsPath = this.currentProfileElement.settings.pairs["path" /* Path */];
        this.oldProfileElementName = this.currentProfileElement.displayAlias;
    };
    ManageProfileDialogController.prototype.fillMultipleProfileElementDialogWithSelectedProfileElement = function () {
        var _this = this;
        var selectedIds = this.getSelectedItems();
        if (selectedIds === undefined) {
            return;
        }
        if (selectedIds.length === 0) {
            return;
        }
        this.fillProfileElementTypes(true);
        this.currentlyEditedElement = selectedIds.map(function (i) { return _this.profile.profileElements[i]; });
        this.isMultiEdit = true;
        this.currentProfileElement = _.cloneDeep(this.currentlyEditedElement[0]);
        var selectedItemCredentialIds = selectedIds.map(function (i) { return _this.profile.profileElements[i].credentialId; });
        if (this.isNotUniform(selectedItemCredentialIds)) {
            this.currentProfileElement.credentialId = null;
        }
        if (this.currentlyEditedElement[0].type === "database" /* Database */) {
            var selectedConnectionStrings = selectedIds.map(function (i) { return _this.profile.profileElements[i].settings.pairs["connectionstring" /* ConnectionString */]; });
            if (this.isNotUniform(selectedConnectionStrings)) {
                this.currentProfileElement.settings.pairs["connectionstring" /* ConnectionString */] = "";
                this.connectionStringPlaceholder = this._t("Values are different among selected elements");
            }
        }
        this.selectProfileElementType(this.currentProfileElement.type);
    };
    ManageProfileDialogController.prototype.viewProfileElement = function () {
        var selectedIds = this.getSelectedItems();
        if (selectedIds.length !== 1) {
            return;
        }
        this.setCredentialsPicker();
        this.fillProfileElementTypes(true);
        this.currentProfileElement = this.profile.profileElements[selectedIds[0]];
        this.selectProfileElementType(this.currentProfileElement.type);
        return this.showProfileElementDialog({
            title: this._t("Configuration element details"),
            cancelButtonText: this._t("Close"),
            viewModel: this,
            buttons: [this.testButton]
        }, DialogMode.ReadOnly);
    };
    ManageProfileDialogController.prototype.editElementToProfile = function () {
        var _this = this;
        if (this.isMultiEdit) {
            this.currentlyEditedElement.forEach(function (element) {
                if (_this.multiEditCheckbox.connectionString) {
                    element.settings.pairs["connectionstring" /* ConnectionString */] =
                        _this.currentProfileElement.settings.pairs["connectionstring" /* ConnectionString */];
                }
                if (_this.multiEditCheckbox.credential) {
                    element.credentialId = _this.currentProfileElement.credentialId;
                }
            });
        }
        else {
            this.currentlyEditedElement[0].settings = this.currentProfileElement.settings;
            this.currentlyEditedElement[0].displayAlias = this.currentProfileElement.displayAlias;
            this.currentlyEditedElement[0].description = this.currentProfileElement.description;
            this.currentlyEditedElement[0].credentialId = this.currentProfileElement.credentialId;
            this.currentlyEditedElement[0].displayName = this.getElementHasDisplayName()
                ? this.currentProfileElement.displayAlias
                : this.currentProfileElement.settings.pairs["path" /* Path */];
        }
        this.listControl.refreshListData();
    };
    ManageProfileDialogController.prototype.selectProfileElementType = function (profileElementType) {
        this.selectedProfileElementType = _(this.profileElementTypes)
            .filter(function (x) { return x.name === profileElementType; })
            .first();
    };
    ManageProfileDialogController.prototype.addProfileElement = function () {
        var _this = this;
        this.currentProfileElement = {
            type: "file" /* File */,
            credentialId: DropdownOrionDefaultCredential
        };
        this.currentProfileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.currentProfileElement.settings);
        this.currentProfileElement.settings.pairs["collectcontent" /* CollectContent */] = "true";
        this.currentProfileElement.settings.pairs["recursive" /* Recursive */] = "false";
        this.currentProfileElement.settings.pairs["pollingfrequency" /* PollingFrequency */] = "0.0:5:0.0"; // default is 5 min
        this.currentProfileElement.settings.pairs["pollingtimeout" /* PollingTimeout */] = "0.0:1:0.0"; // default is 1 min
        this.fillProfileElementTypes(false);
        this.selectProfileElementType("file" /* File */);
        this.setCredentialsPicker();
        this.isMultiEdit = false;
        return this.showProfileElementDialog({
            title: this._t("Add configuration element"),
            actionButtonText: this._t("Add"),
            cancelButtonText: this._t("Cancel"),
            viewModel: this,
            buttons: [
                {
                    name: "config-element-add-dialog-save",
                    text: this._t("Add"),
                    isPrimary: true,
                    action: function (dialog) {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                            return;
                        }
                        _this.setInvalidFieldsDirty();
                        return _this.inputIsOk().then(function (ok) {
                            if (dialog.$valid && ok) {
                                _this.addElementToProfile();
                                return true;
                            }
                            return false;
                        });
                    },
                    isHidden: false
                },
                this.testButton
            ]
        }, DialogMode.Add);
    };
    ManageProfileDialogController.prototype.setInvalidFieldsDirty = function () {
        if (this.profileElementForm.$valid) {
            return;
        }
        _(this.profileElementForm.$error).forEach(function (controls) {
            _(controls).forEach(function (control) {
                control.$setDirty();
                if (control.$name === "CredentialDropdown") {
                    control.$setTouched();
                }
            });
        });
    };
    ManageProfileDialogController.prototype.showProfileElementDialog = function (dialogOptions, mode) {
        var _this = this;
        var dialogSettings;
        switch (mode) {
            case DialogMode.Copy:
            case DialogMode.FullEdit:
                this.manageProfileElementDialogDescriptionEnabled = true;
                this.manageProfileElementDialogElementTypeEnabled = false;
                this.manageProfileElementDialogShowPopover = false;
                this.manageProfileElementDialogCredentialEnabled = (this.multiEditCheckbox.credential && this.isMultiEdit) || !this.isMultiEdit;
                break;
            case DialogMode.PartialEdit:
                this.manageProfileElementDialogDescriptionEnabled = true;
                this.manageProfileElementDialogElementTypeEnabled = false;
                this.manageProfileElementDialogShowPopover = true;
                this.manageProfileElementDialogCredentialEnabled = (this.multiEditCheckbox.credential && this.isMultiEdit) || !this.isMultiEdit;
                break;
            case DialogMode.ReadOnly:
                this.manageProfileElementDialogDescriptionEnabled = false;
                this.manageProfileElementDialogElementTypeEnabled = false;
                this.manageProfileElementDialogShowPopover = false;
                this.manageProfileElementDialogCredentialEnabled = false;
                break;
            case DialogMode.Add:
            default:
                this.manageProfileElementDialogDescriptionEnabled = true;
                this.manageProfileElementDialogElementTypeEnabled = true;
                this.manageProfileElementDialogShowPopover = false;
                this.manageProfileElementDialogCredentialEnabled = true;
                break;
        }
        this.manageProfileElementDialogElementSettingsMode = mode;
        dialogSettings = {
            template: __webpack_require__(286),
            size: "md"
        };
        return this.scmModalService.showModal(dialogSettings, dialogOptions)
            .then(function () {
            if (_this.lastTestJobTempElementId) {
                _this.scmTestJobService.removeTestJobElementResult(_this.lastTestJobTempElementId);
            }
        });
    };
    ManageProfileDialogController.prototype.setManageProfileElementDialogCredentialEnabled = function () {
        var _this = this;
        this.$timeout(function () {
            _this.manageProfileElementDialogCredentialEnabled = (_this.multiEditCheckbox.credential && _this.isMultiEdit) || !_this.isMultiEdit;
        });
    };
    ManageProfileDialogController.prototype.changeTestNode = function () {
        var _this = this;
        this.setInvalidFieldsDirty();
        this.inputIsOk({ skipDuplicityCheck: true, skipCheckNodeIsInQuery: true }).then(function (ok) {
            if (_this.profileElementForm.$valid && ok) {
                _this.runElementTest(true);
            }
        });
    };
    ManageProfileDialogController.prototype.runElementTest = function (forceNodeSelection) {
        var _this = this;
        if (forceNodeSelection === void 0) { forceNodeSelection = false; }
        this.currentProfileElement.displayName = this.getElementHasDisplayName()
            ? this.currentProfileElement.displayAlias
            : this.currentProfileElement.settings.pairs["path" /* Path */];
        return this.scmTestJobService.runElementTest(this.currentProfileElement, forceNodeSelection)
            .then(function (tempElementId) {
            _this.lastTestJobTempElementId = tempElementId;
            return true;
        })
            .catch(function (reason) {
            return false;
        });
    };
    ManageProfileDialogController.prototype.profileElementPathValidation = function () {
        var _this = this;
        var isDuplicate = false;
        if (_([DialogMode.Add, DialogMode.Copy]).includes(this.manageProfileElementDialogElementSettingsMode)) {
            isDuplicate = _(this.profile.profileElements).some(function (el) {
                return el.settings.pairs["path" /* Path */] === _this.currentProfileElement.settings.pairs["path" /* Path */];
            });
        }
        else if (_([DialogMode.FullEdit, DialogMode.PartialEdit]).includes(this.manageProfileElementDialogElementSettingsMode)) {
            isDuplicate = _(this.profile.profileElements).some(function (el) {
                return el.settings.pairs["path" /* Path */] === _this.currentProfileElement.settings.pairs["path" /* Path */]
                    && el.elementId !== _this.currentProfileElement.elementId;
            });
        }
        if (isDuplicate) {
            this.profileElementForm.ElementPath.$setValidity("duplicateProfileElement", false, this.profileElementForm);
        }
        return isDuplicate;
    };
    ManageProfileDialogController.prototype.profileElementNameValidation = function () {
        var _this = this;
        var isDuplicate = false;
        if (_([DialogMode.Add, DialogMode.Copy]).includes(this.manageProfileElementDialogElementSettingsMode)) {
            isDuplicate = _(this.profile.profileElements).some(function (el) {
                return el.displayAlias === _this.currentProfileElement.displayAlias;
            });
        }
        else if (_([DialogMode.FullEdit, DialogMode.PartialEdit]).includes(this.manageProfileElementDialogElementSettingsMode)) {
            isDuplicate = _(this.profile.profileElements).some(function (el) {
                return el.displayAlias === _this.currentProfileElement.displayAlias
                    && el.elementId !== _this.currentProfileElement.elementId;
            });
        }
        if (isDuplicate) {
            this.profileElementForm.ElementDisplayAlias.$setValidity("duplicateProfileElement", false, this.profileElementForm);
        }
        return isDuplicate;
    };
    ManageProfileDialogController.prototype.addElementToProfile = function () {
        this.currentProfileElement.displayName = this.getElementHasDisplayName()
            ? this.currentProfileElement.displayAlias
            : this.currentProfileElement.settings.pairs["path" /* Path */];
        this.profile.profileElements.push(this.currentProfileElement);
        this.listControl.refreshListData();
    };
    ManageProfileDialogController.prototype.inputIsOk = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        return this.scmValidationService.isSettingPathValid(this.currentProfileElement).then(function (res) {
            if (!_this.isMultiEdit) {
                _this.profileElementForm.ElementPath.$setDirty();
                if (!_this.scmValidationService.isSettingPathLengthOk(_this.currentProfileElement)) {
                    _this.profileElementForm.ElementPath.$setValidity("settingPathLengthError", false, _this.profileElementForm);
                    return false;
                }
                if (!res) {
                    _this.profileElementForm.ElementPath.$setValidity("notValidProfileElementPathSetting", false, _this.profileElementForm);
                    return false;
                }
                if (!options.skipDuplicityCheck) {
                    var isDuplicate = false;
                    if (_this.currentProfileElement.type === "script" /* Script */
                        || _this.currentProfileElement.type === "powershell" /* PowerShell */
                        || _this.currentProfileElement.type === "swisQuery" /* SwisQuery */
                        || _this.currentProfileElement.type === "database" /* Database */) {
                        isDuplicate = _this.profileElementNameValidation();
                    }
                    if (_this.currentProfileElement.type === "file" /* File */
                        || _this.currentProfileElement.type === "parsedFile" /* ParsedFile */
                        || _this.currentProfileElement.type === "registry" /* Registry */) {
                        isDuplicate = _this.profileElementPathValidation();
                    }
                    if (isDuplicate) {
                        return false;
                    }
                }
                if (!_this.scmValidationService.isDisplayAliasLengthOk(_this.currentProfileElement)) {
                    _this.profileElementForm.ElementDisplayAlias.$setValidity("displayAliasLengthError", false, _this.profileElementForm);
                    return false;
                }
                if (!_this.validateCredential()) {
                    return false;
                }
                if (!_this.isConnectionStringValidAndSetValidity()) {
                    return false;
                }
                if (!_this.scmValidationService.isDescriptionLengthOk(_this.currentProfileElement)) {
                    _this.profileElementForm.ElementDescription.$setValidity("descriptionLengthError", false, _this.profileElementForm);
                    return false;
                }
                if (!options.skipCheckNodeIsInQuery) {
                    if (_this.currentProfileElement.type === "swisQuery" /* SwisQuery */ &&
                        !_this.scmValidationService.ContainsNodeId(_this.currentProfileElement.settings.pairs["path" /* Path */])) {
                        return _this.showSwisElementConfirmationDialog().then(function (result) {
                            return result;
                        });
                    }
                }
            }
            else {
                if (!_this.validateCredential()) {
                    return false;
                }
                if (_this.multiEditCheckbox.connectionString && !_this.isConnectionStringValidAndSetValidity()) {
                    return false;
                }
            }
            return true;
        });
    };
    ManageProfileDialogController.prototype.showSwisElementConfirmationDialog = function () {
        return this.scmModalService.showModal({
            template: __webpack_require__(287),
            size: "md",
        }, {
            title: this._t("Really add a generic query?"),
            actionButtonText: this._t("Add generic query"),
            cancelButtonText: this._t("Cancel"),
            viewModel: {},
        }).then(function (res) {
            if (res !== "cancel") {
                return true;
            }
            return false;
        });
    };
    ManageProfileDialogController.prototype.deleteElementFromProfile = function () {
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return;
        }
        var selectedIds = this.getSelectedItems();
        if (selectedIds.length === 0) {
            return true;
        }
        _.remove(this.profile.profileElements, function (x) { return _.includes(selectedIds, x.id); });
        this.profileElementsState.selection.blacklist = false;
        this.profileElementsState.selection.items = [];
        this.scmFilteredListService.fixPagination(this.profile.profileElements.length, this.profileElementsState);
        this.listControl.refreshListData();
        return true;
    };
    ManageProfileDialogController.prototype.getSelectedItems = function () {
        var _this = this;
        if (this.profileElementsState.selection.blacklist) {
            return this.profile.profileElements
                .filter(function (profileElement) {
                return _(_this.filteredProfileElementIds).includes(profileElement.id) && !_(_this.profileElementsState.selection.items).includes(profileElement.id);
            })
                .map(function (profileElement) { return profileElement.id; });
        }
        return this.profileElementsState.selection.items;
    };
    ManageProfileDialogController.prototype.createProfileElementsDispatcher = function () {
        var _this = this;
        this.profileElementsDispatcher = this.xuiFilteredListService.getDispatcherInstance(this.xuiFilteredListService.getModel(this, "profileElementsState"), {
            dataSource: {
                getItems: function (params) {
                    var elementIndexer = 0;
                    var result = _(_this.profile.profileElements)
                        .map(function (e) {
                        e.id = elementIndexer++;
                        e.typeText = _this._t(mappings_1.ProfileElementTypeFullText[e.type]);
                        // sortName property exists only because of sorting and searching, toLower it is because the sorting should be case insensitive
                        e.sortPath = _.toLower(e.displayName);
                        e.sortName = _.toLower(_this.elementDisplay.getElementNameInfo(e) + " " + _this.elementDisplay.getElementName(e));
                        return e;
                    })
                        .orderBy(["sortName"])
                        .value();
                    var itemSource = _this.xuiArraySourceFactoryService.createItemSource(result, {
                        propertiesToSearch: ["displayName"]
                    });
                    var resultWithoutPagination = itemSource(_.omit(params, "pagination"));
                    resultWithoutPagination.then(function (filteredElements) {
                        _this.filteredProfileElementIds = filteredElements.items.map(function (item) { return item.id; });
                    });
                    return itemSource(params);
                }
            }
        });
    };
    ManageProfileDialogController.prototype.createProfileElementsState = function () {
        var elementTypes = this.getElementTypeCheckboxItems();
        this.profileElementsState = {
            sorting: {
                sortableColumns: [
                    { id: "typeText", label: this._t("Type") },
                    { id: "sortName", label: this._t("Name") },
                    { id: "sortPath", label: this._t("Path") },
                ],
                sortBy: {
                    id: "typeText",
                    label: this._t("Type")
                },
                direction: "asc"
            },
            sidebarCollapsed: true,
            filters: {
                filterProperties: [
                    {
                        id: "type",
                        title: this._t("Element type"),
                        checkboxes: elementTypes
                    }
                ],
                visibleFilterPropertyIds: ["type"]
            },
            options: {
                layout: "contained",
                stripe: false,
                selectionMode: this.isReadOnly ? "radio" : "multi",
                selectionProperty: "id",
                trackBy: "id",
                templateUrl: "manageProfileDialog-item",
                rowPadding: "none",
                pageSize: 10,
                showAddRemoveFilterProperty: false,
                allowSelectAllPages: true,
                emptyData: { description: this._t("No configuration elements added yet") },
                showMorePropertyValuesThreshold: elementTypes.length
            }
        };
    };
    ManageProfileDialogController.prototype.getElementTypeCheckboxItems = function () {
        var checkboxes = [];
        for (var _i = 0, _a = this.filterProfileElementTypes; _i < _a.length; _i++) {
            var type = _a[_i];
            checkboxes.push({
                id: type,
                title: this._t(mappings_1.ProfileElementTypeFullText[type]),
                count: undefined
            });
        }
        return checkboxes;
    };
    ManageProfileDialogController.prototype.isProfileInputOk = function () {
        var result = true;
        if (!this.scmValidationService.isProfileNameLengthOk(this.profile.profile)) {
            this.profileForm.ProfileName.$setValidity("nameLengthError", false, this.profileForm);
            result = false;
        }
        if (!this.scmValidationService.isProfileDescriptionLengthOk(this.profile.profile)) {
            this.profileForm.Description.$setValidity("descriptionLengthError", false, this.profileForm);
            result = false;
        }
        return result;
    };
    ManageProfileDialogController.prototype.getElementHasDisplayName = function () {
        return !(this.currentProfileElement.type === "file" /* File */
            || this.currentProfileElement.type === "parsedFile" /* ParsedFile */
            || this.currentProfileElement.type === "registry" /* Registry */);
    };
    ManageProfileDialogController.prototype.add = function () {
        var _this = this;
        var result = this.$q.defer();
        if (!this.isProfileInputOk()) {
            result.resolve(false);
            return result.promise;
        }
        this.scmProfileService.createProfile(this.profile)
            .then(function (_) { return _this.resolveAndShowAddMessage(result); })
            .catch(function (_) {
            if (_.status === 409) {
                _this.profileForm.ProfileName.$setValidity("duplicateProfile", false, _this.profileForm);
            }
            else if (_.status === 400 && _.data.validationErrors) {
                var validationErrorMessages = [];
                for (var _i = 0, _a = _.data.validationErrors; _i < _a.length; _i++) {
                    var validationError = _a[_i];
                    validationErrorMessages.push("Profile '" + validationError.profileId + "': " + validationError.message);
                }
                var message = _this.swUtil.formatString(_this._t("The profile is not valid. Following validation errors were detected: {0}"), validationErrorMessages.join(", "));
                _this.scmModalService.showError({ message: message });
            }
            else {
                _this.xuiToastService.error(_this._t("We are having difficulty communicating with the Orion server. Try again later."));
            }
            result.resolve(false);
        });
        return result.promise;
    };
    ManageProfileDialogController.prototype.save = function () {
        var _this = this;
        var result = this.$q.defer();
        if (!this.isProfileInputOk()) {
            result.resolve(false);
            return result.promise;
        }
        this.scmProfileService.saveProfile(this.profile)
            .then(function (_) { return _this.resolveAndShowSaveMessage(result); })
            .catch(function (_) {
            if (_.status === 409) {
                _this.profileForm.ProfileName.$setValidity("duplicateProfile", false, _this.profileForm);
            }
            else {
                _this.xuiToastService.error(_this._t("We are having difficulty communicating with the Orion server. Try again later."));
            }
            result.resolve(false);
        });
        return result.promise;
    };
    ManageProfileDialogController.prototype.resolveAndShowAddMessage = function (result) {
        result.resolve(true);
        this.xuiToastService.success(this._t("Profile was successfully added."));
    };
    ManageProfileDialogController.prototype.resolveAndShowSaveMessage = function (result) {
        result.resolve(true);
        this.xuiToastService.success(this._t("Profile was successfully saved."));
    };
    ManageProfileDialogController.prototype.shouldShowAddButton = function () {
        if (!this.isSelectionDefined()) {
            return false;
        }
        return this.profileElementsState.selection.blacklist
            ? this.profileElementsState.selection.items.length === this.profile.profileElements.length
            : this.profileElementsState.selection.items.length === 0;
    };
    ManageProfileDialogController.prototype.shouldShowEditButton = function () {
        return !this.isReadOnly && this.isElementSelected(1);
    };
    ManageProfileDialogController.prototype.shouldShowMultiEditButton = function () {
        return !this.isReadOnly && this.areMoreElementsSelected(2) && this.areSelectedElementsValid();
    };
    ManageProfileDialogController.prototype.shouldShowCopyButton = function () {
        return this.isElementSelected(1);
    };
    ManageProfileDialogController.prototype.shouldShowViewButton = function () {
        return this.isElementSelected(1) && this.isReadOnly && this.areSelectedElementsSaved();
    };
    ManageProfileDialogController.prototype.shouldShowDeleteButton = function () {
        if (!this.isSelectionDefined()) {
            return false;
        }
        return this.profileElementsState.selection.blacklist
            ? this.profileElementsState.selection.items.length < this.profile.profileElements.length
            : this.profileElementsState.selection.items.length > 0;
    };
    ManageProfileDialogController.prototype.selectedItemCount = function () {
        return this.profileElementsState.selection.blacklist
            ? this.profile.profileElements.length - this.profileElementsState.selection.items.length
            : this.profileElementsState.selection.items.length;
    };
    ManageProfileDialogController.prototype.areSelectedElementsValid = function () {
        var _this = this;
        var selectedIds = this.getSelectedItems();
        if (selectedIds === undefined) {
            return false;
        }
        if (selectedIds.length === 0) {
            return false;
        }
        var selectedItems = selectedIds.map(function (i) { return _this.profile.profileElements[i]; });
        return selectedItems.some(function (x) { return x.type !== "swisQuery" /* SwisQuery */; }) && !this.isNotUniform(selectedItems.map(function (x) { return x.type; }));
    };
    ManageProfileDialogController.prototype.isNotUniform = function (array) {
        var item = array[0];
        for (var index = 1; index < array.length; index++) {
            if (item !== array[index]) {
                return true;
            }
        }
        return false;
    };
    ManageProfileDialogController.prototype.areMoreElementsSelected = function (minCount) {
        if (!this.isSelectionDefined()) {
            return false;
        }
        return this.selectedItemCount() >= minCount;
    };
    ManageProfileDialogController.prototype.isElementSelected = function (count) {
        if (!this.isSelectionDefined()) {
            return false;
        }
        return this.selectedItemCount() === count;
    };
    ManageProfileDialogController.prototype.isSelectionDefined = function () {
        return this.profileElementsState.selection !== undefined && this.profileElementsState.selection.items !== undefined;
    };
    ManageProfileDialogController.prototype.areSelectedElementsSaved = function () {
        var hasUnsavedElement = _(this.profile.profileElements).find(function (x) { return x.elementId === undefined; });
        return !hasUnsavedElement;
    };
    ManageProfileDialogController.prototype.openViewAssignedNodesDialog = function () {
        this.scmNodesListDialogService.showModal(this.profile.profile);
    };
    ManageProfileDialogController.prototype.viewAlreadyAssignedNodesInfoBox = function () {
        return this.profile.profile.assignedNodesCount > 0;
    };
    ManageProfileDialogController.prototype.profileIsAssignedToHiddenNodes = function () {
        return this.profile.profile.assignedNodesCount < this.profile.profile.totalAssignedNodesCount;
    };
    ManageProfileDialogController.prototype.importTemplates = function () {
        this.$templateCache.put("scm-credential-dropdown-template", __webpack_require__(288));
    };
    return ManageProfileDialogController;
}());
exports.default = ManageProfileDialogController;


/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ProfileElementSettingsHelper = /** @class */ (function () {
    function ProfileElementSettingsHelper() {
    }
    ProfileElementSettingsHelper.ensureDictionaryInSettings = function (settings) {
        if (settings) {
            return settings;
        }
        var dictionary = {};
        return { pairs: dictionary };
    };
    ProfileElementSettingsHelper.convertBoolValue = function (inputString) {
        if (!inputString) {
            return false;
        }
        switch (inputString.toLowerCase().trim()) {
            case "false":
                return false;
            case "true":
                return true;
            default:
                throw new Error("Value '" + inputString + "' is not supported by this method.");
        }
    };
    return ProfileElementSettingsHelper;
}());
exports.ProfileElementSettingsHelper = ProfileElementSettingsHelper;


/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var parsedFileElementType_1 = __webpack_require__(337);
var profileElementSettingsHelper_1 = __webpack_require__(153);
var manageProfileDialog_controller_1 = __webpack_require__(152);
var ElementSettingsController = /** @class */ (function () {
    /** @ngInject */
    ElementSettingsController.$inject = ["getTextService", "$timeout", "scmLocationService", "scmCentralizedSettingsService", "scmValidationService", "scmProfileElementService"];
    function ElementSettingsController(getTextService, $timeout, scmLocationService, scmCentralizedSettingsService, scmValidationService, scmProfileElementService) {
        this.getTextService = getTextService;
        this.$timeout = $timeout;
        this.scmLocationService = scmLocationService;
        this.scmCentralizedSettingsService = scmCentralizedSettingsService;
        this.scmValidationService = scmValidationService;
        this.scmProfileElementService = scmProfileElementService;
        this.connectionStrings = [];
        this.settingsPathValueByType = {};
        this._t = getTextService;
    }
    ElementSettingsController.prototype.$onInit = function () {
        var _this = this;
        var topics = [
            "orionSCMElementTypeFile",
            "orionSCMElementTypeRegistry",
            "orionSCMElementTypeSWIS",
            "orionSCMPowerShellScript",
            "orionSCMElementTypeParsedFile",
            "orionSCMLinuxScriptElements",
            "orionSCMDatabaseQueries"
        ];
        this.scmLocationService.getHelpLinks(topics).then(function (data) {
            _this.helpLinks = data;
        });
        var settings = {
            minPollingFrequency: "MinAllowedPollingFrequency",
            maxPollingFrequency: "MaxAllowedPollingFrequency",
            minPollingTimeout: "MinAllowedPollingTimeout",
            maxPollingTimeout: "MaxAllowedPollingTimeout"
        };
        _.each(settings, function (setting, key) {
            _this.scmCentralizedSettingsService
                .getCachedSetting(setting)
                .then(function (value) { return (_this[key] = value); });
        });
        this.fileParsingTypes = [
            {
                name: "IisWebConfig" /* IisWebConfig */,
                value: this._t(parsedFileElementType_1.FileParsingElementTypeText["IisWebConfig" /* IisWebConfig */])
            }
        ];
        switch (this.mode) {
            case manageProfileDialog_controller_1.DialogMode.PartialEdit:
                this.settingsEnabled = false;
                this.collectContentEnabled = true;
                this.recursiveEnabled = true;
                break;
            case manageProfileDialog_controller_1.DialogMode.ReadOnly:
                this.settingsEnabled = false;
                this.collectContentEnabled = false;
                this.recursiveEnabled = false;
                break;
            case manageProfileDialog_controller_1.DialogMode.FullEdit:
                this.settingsEnabled =
                    (this.multiEditCheckbox.connectionString && this.isMultiEdit) ||
                        !this.isMultiEdit;
                this.collectContentEnabled = true;
                this.recursiveEnabled = true;
                break;
            case manageProfileDialog_controller_1.DialogMode.Add:
                this.isConnectionStringRequired = true;
                this.recursiveEnabled = true;
            case manageProfileDialog_controller_1.DialogMode.Copy:
            default:
                this.settingsEnabled = true;
                this.collectContentEnabled = true;
                this.recursiveEnabled = true;
                break;
        }
        if (!this.isMultiEdit) {
            this.connectionStringPlaceholder = this._t("Select an existing connection string or add new one ...");
        }
        this.loadConnectionStrings();
    };
    ElementSettingsController.prototype.setSettingConnectionStringEnabled = function () {
        var _this = this;
        this.$timeout(function () {
            _this.settingsEnabled =
                (_this.multiEditCheckbox.connectionString && _this.isMultiEdit) ||
                    !_this.isMultiEdit;
        });
    };
    ElementSettingsController.prototype.$onDestroy = function () {
        switch (this.profileElement.type) {
            case "file" /* File */:
                this.profileElement.settings.pairs = this.filterAllowedKeys([
                    "path" /* Path */,
                    "collectcontent" /* CollectContent */
                ]);
                break;
            case "registry" /* Registry */:
                this.profileElement.settings.pairs = this.filterAllowedKeys([
                    "path" /* Path */,
                    "collectcontent" /* CollectContent */,
                    "recursive" /* Recursive */
                ]);
                break;
            case "parsedFile" /* ParsedFile */:
                this.profileElement.settings.pairs = this.filterAllowedKeys([
                    "path" /* Path */,
                    "fileparsingtype" /* FileParsingType */
                ]);
                break;
            case "powershell" /* PowerShell */:
                this.profileElement.settings.pairs = this.filterAllowedKeys([
                    "path" /* Path */,
                    "pollingtimeout" /* PollingTimeout */,
                    "pollingfrequency" /* PollingFrequency */
                ]);
                break;
            case "script" /* Script */:
                this.profileElement.settings.pairs = this.filterAllowedKeys([
                    "commandline" /* CommandLine */,
                    "workingdirectory" /* WorkingDirectory */,
                    "path" /* Path */,
                    "pollingtimeout" /* PollingTimeout */,
                    "pollingfrequency" /* PollingFrequency */
                ]);
                break;
            case "database" /* Database */:
                this.profileElement.settings.pairs = this.filterAllowedKeys([
                    "connectionstring" /* ConnectionString */,
                    "path" /* Path */,
                    "pollingtimeout" /* PollingTimeout */,
                    "pollingfrequency" /* PollingFrequency */,
                    "username" /* Username */,
                    "password" /* Password */
                ]);
                break;
            default:
                this.profileElement.settings.pairs = this.filterAllowedKeys([
                    "path" /* Path */
                ]);
        }
    };
    ElementSettingsController.prototype.timeoutValidators = function () {
        // xuiTextbox's `validators` are resolved once during compilation
        // but we might not have the settings yet as it is independant API call
        // so we pass interpolation into the `validators`
        // but the interpolation is evaluated in textCtrl scope
        // so we need to jump up the scope hierarchy using $parent
        return ("scm-duration-validator" +
            ",min={{:: $parent.elementSettings.minPollingTimeout }}" +
            ",max={{:: $parent.elementSettings.maxPollingTimeout }}");
    };
    ElementSettingsController.prototype.frequencyValidators = function () {
        return ("scm-duration-validator" +
            ",min={{:: $parent.elementSettings.minPollingFrequency }}" +
            ",max={{:: $parent.elementSettings.maxPollingFrequency }}");
    };
    ElementSettingsController.prototype.filterAllowedKeys = function (allowedKeys) {
        var output = {};
        for (var _i = 0, _a = Object.keys(this.profileElement.settings.pairs); _i < _a.length; _i++) {
            var key = _a[_i];
            if (_(allowedKeys).includes(key)) {
                output[key] = this.profileElement.settings.pairs[key];
            }
        }
        return output;
    };
    ElementSettingsController.prototype.isScriptFileContentRequired = function () {
        return this.scmValidationService.isScriptRequired(this.profileElement);
    };
    ElementSettingsController.prototype.warnScriptNotUsed = function () {
        return this.scmValidationService.warnScriptNotUsed(this.profileElement);
    };
    Object.defineProperty(ElementSettingsController.prototype, "settingsPath", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            if (_.isEmpty(this.settingsPathValueByType)) {
                this.settingsPathValueByType[this.profileElement.type] = this.profileElement.settings.pairs["path" /* Path */];
            }
            this.settingsPath = this.settingsPathValueByType[this.profileElement.type];
            return this.profileElement.settings.pairs["path" /* Path */];
        },
        set: function (path) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.settingsPathValueByType[this.profileElement.type] = path;
            this.profileElement.settings.pairs["path" /* Path */] = path;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsCollectContent", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            return profileElementSettingsHelper_1.ProfileElementSettingsHelper.convertBoolValue(this.profileElement.settings.pairs["collectcontent" /* CollectContent */]);
        },
        set: function (collectContent) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["collectcontent" /* CollectContent */] = collectContent.toString();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsRecursive", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            return profileElementSettingsHelper_1.ProfileElementSettingsHelper.convertBoolValue(this.profileElement.settings.pairs["recursive" /* Recursive */]);
        },
        set: function (recursive) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["recursive" /* Recursive */] = recursive.toString();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsParsingType", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            var fileParsingType = this.profileElement.settings.pairs["fileparsingtype" /* FileParsingType */];
            if (!fileParsingType) {
                fileParsingType = this.fileParsingTypes[0].name;
                this.profileElement.settings.pairs["fileparsingtype" /* FileParsingType */] = fileParsingType;
            }
            return _(this.fileParsingTypes).find(function (type) { return type.name === fileParsingType; });
        },
        set: function (newParsingType) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["fileparsingtype" /* FileParsingType */] =
                newParsingType.name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsPollingFrequency", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            return this.profileElement.settings.pairs["pollingfrequency" /* PollingFrequency */];
        },
        set: function (value) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["pollingfrequency" /* PollingFrequency */] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsPollingTimeout", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            return this.profileElement.settings.pairs["pollingtimeout" /* PollingTimeout */];
        },
        set: function (value) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["pollingtimeout" /* PollingTimeout */] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsCommandLine", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            return this.profileElement.settings.pairs["commandline" /* CommandLine */];
        },
        set: function (value) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["commandline" /* CommandLine */] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsWorkingDirectory", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            return this.profileElement.settings.pairs["workingdirectory" /* WorkingDirectory */];
        },
        set: function (value) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["workingdirectory" /* WorkingDirectory */] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementSettingsController.prototype, "settingsConnectionString", {
        get: function () {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            return this.profileElement.settings.pairs["connectionstring" /* ConnectionString */];
        },
        set: function (value) {
            this.profileElement.settings = profileElementSettingsHelper_1.ProfileElementSettingsHelper.ensureDictionaryInSettings(this.profileElement.settings);
            this.profileElement.settings.pairs["connectionstring" /* ConnectionString */] = value;
        },
        enumerable: true,
        configurable: true
    });
    ElementSettingsController.prototype.isTimeoutOverPolling = function () {
        var timeout = this.profileElement.settings.pairs["pollingtimeout" /* PollingTimeout */];
        var interval = this.profileElement.settings.pairs["pollingfrequency" /* PollingFrequency */];
        return (timeout &&
            interval &&
            moment.duration(timeout) > moment.duration(interval));
    };
    ElementSettingsController.prototype.loadConnectionStrings = function () {
        var _this = this;
        this.scmProfileElementService.getConnectionStrings().then(function (data) {
            _this.connectionStrings = _.chain(_this.profile.profileElements)
                .map(function (e) { return e.settings.pairs["connectionstring" /* ConnectionString */]; })
                .filter(function (cs) { return !!cs; })
                .concat(data)
                .uniq()
                .sort()
                .value();
        });
    };
    return ElementSettingsController;
}());
exports.default = ElementSettingsController;


/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var DatetimePickerPopoverController = /** @class */ (function () {
    /** @ngInject */
    DatetimePickerPopoverController.$inject = ["$scope", "$log", "$timeout", "$templateCache", "getTextService"];
    function DatetimePickerPopoverController($scope, $log, $timeout, $templateCache, getTextService) {
        this.$scope = $scope;
        this.$log = $log;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.getTextService = getTextService;
        this._t = this.getTextService;
    }
    DatetimePickerPopoverController.prototype.$onInit = function () {
        var _this = this;
        if (!this.caption) {
            this.caption = this._t("Select date/time");
        }
        this.innerModel = this.model;
        this.areButtonsDisplayed = false;
        this.$scope.$watch("ctrl.innerModel", function (newVal, oldVal) {
            _this.areButtonsDisplayed = newVal !== _this.model;
        });
        this.isTooltipEnabled = true;
    };
    DatetimePickerPopoverController.prototype.printModel = function () {
        return moment(this.model).calendar(null, {
            lastWeek: "lll",
            sameElse: "lll"
        });
    };
    DatetimePickerPopoverController.prototype.getTooltipDate = function () {
        return moment(this.model).format("YYYY-MM-DD HH:mm:ss.SSS");
    };
    DatetimePickerPopoverController.prototype.getPopoverTemplate = function () {
        return this.$templateCache.get("scmDatetimePickerPopover");
    };
    DatetimePickerPopoverController.prototype.onShow = function () {
        this.innerModel = this.model;
        this.areButtonsDisplayed = false;
        this.isTooltipEnabled = false;
        this.isTooltipOpen = false;
    };
    DatetimePickerPopoverController.prototype.onHide = function () {
        this.innerModel = this.model;
        this.areButtonsDisplayed = false;
        this.isTooltipEnabled = true;
        this.isTooltipOpen = false;
    };
    DatetimePickerPopoverController.prototype.onUseButtonClick = function () {
        var _this = this;
        if (angular.isFunction(this.onChange)) {
            if (this.model !== this.innerModel) {
                this.$timeout(function () { return _this.onChange(); });
            }
        }
        this.model = this.innerModel;
        this.isDisplayed = false;
    };
    DatetimePickerPopoverController.prototype.onCancelButtonClick = function () {
        this.isDisplayed = false;
    };
    DatetimePickerPopoverController.prototype.hasBaseline = function () {
        return this.baselineDate !== null && this.baselineDate !== undefined;
    };
    DatetimePickerPopoverController.prototype.onBaselineClick = function () {
        var _this = this;
        if (angular.isFunction(this.onChange)) {
            this.$timeout(function () { return _this.onChange(); });
        }
        this.model = this.baselineDate;
        this.isDisplayed = false;
    };
    DatetimePickerPopoverController.prototype.getBaselineDate = function () {
        return moment(this.baselineDate).format("lll");
    };
    return DatetimePickerPopoverController;
}());
exports.default = DatetimePickerPopoverController;


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AssignProfileWizardStepName_1 = __webpack_require__(51);
var AssignProfileWizardController = /** @class */ (function () {
    /** @ngInject */
    AssignProfileWizardController.$inject = ["$q", "$log", "$scope", "$timeout", "$templateCache", "xuiToastService", "getTextService", "scmProfileService", "scmFilteredListService", "swDemoService", "scmIFrameEventsService", "swUtil", "scmNodesListDialogService"];
    function AssignProfileWizardController($q, $log, $scope, $timeout, $templateCache, xuiToastService, getTextService, scmProfileService, scmFilteredListService, swDemoService, scmIFrameEventsService, swUtil, scmNodesListDialogService) {
        var _this = this;
        this.$q = $q;
        this.$log = $log;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.xuiToastService = xuiToastService;
        this.getTextService = getTextService;
        this.scmProfileService = scmProfileService;
        this.scmFilteredListService = scmFilteredListService;
        this.swDemoService = swDemoService;
        this.scmIFrameEventsService = scmIFrameEventsService;
        this.swUtil = swUtil;
        this.scmNodesListDialogService = scmNodesListDialogService;
        this._t = this.getTextService;
        this.currentStepIndex = 1;
        this.isBusy = false;
        this.errors = {};
        this.profilesFinishedLoadingPromise = this.$q.defer();
        this.nodesFinishedLoadingPromise = this.$q.defer();
        this.summaryProfiles = [];
        this.summaryNodes = [];
        this.summaryRows = [];
        this.assignedElementsByTypes = [];
        this.credentialsChecked = false;
        this.credentialsStepIsDisplayed = false;
        this.wizardSteps = [
            {
                label: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Profiles,
                title: this._t("Select configuration profiles"),
                shortTitle: this._t("Configuration profiles"),
                src: "assignProfileWizard-step1"
            },
            {
                label: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Nodes,
                title: this._t("Select nodes to monitor"),
                shortTitle: this._t("Nodes to monitor"),
                src: "assignProfileWizard-step2"
            },
            {
                label: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Credentials,
                title: this._t("Set credentials"),
                shortTitle: this._t("Credentials"),
                src: "assignProfileWizard-step3"
            },
            {
                label: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Confirm,
                title: this._t("Review assignments"),
                shortTitle: this._t("Summary"),
                src: "assignProfileWizard-step4"
            }
        ];
        this.credentialsStepOnInit = function () {
            _this.credentialsStepIsDisplayed = true;
            _this.initCredentialsStep();
        };
        this.profileStepOnInit = function (profiles) {
            _this.profiles = profiles;
            _this.profilesFinishedLoadingCallback();
        };
        this.selectedProfilesChanged = function (selectedProfiles) {
            _this.selectedProfiles = selectedProfiles;
            if (_this.profilesToPreselect
                && _this.selectedProfiles.length === _this.profilesToPreselect.length) {
                _this.profilesFinishedLoadingPromise.resolve();
            }
        };
        this.openNodesListDialog = function (profile) {
            _this.scmNodesListDialogService.showModal(profile);
        };
        this.setCredentialsStepTitle = function (containsDatabaseElement) {
            _this.$scope.$apply(function () {
                if (containsDatabaseElement) {
                    _this.wizardSteps[2].title = _this._t("Set credentials");
                }
                else {
                    _this.wizardSteps[2].title = _this._t("Set credentials (optional)");
                }
            });
        };
        this.setElementsToAssignByType = function (assignedElementsByTypes) {
            _this.assignedElementsByTypes = assignedElementsByTypes.filter(function (a) {
                return a.credentialId !== null || a.type === "database" /* Database */;
            });
            _this.credentialsChecked = true;
            _this.goToWizardStep(AssignProfileWizardStepName_1.AssignProfileWizardStepName.Confirm);
        };
        scmIFrameEventsService.subscribe("apollo.assignElementsByType", this.setElementsToAssignByType);
        scmIFrameEventsService.subscribe("fusion.credentialsStep.onInit", this.credentialsStepOnInit);
        scmIFrameEventsService.subscribe("apollo.credentialsStep.setTitle", this.setCredentialsStepTitle);
        scmIFrameEventsService.subscribe("fusion.profileStep.onInit", this.profileStepOnInit);
        scmIFrameEventsService.subscribe("fusion.profileStep.selectionChanged", this.selectedProfilesChanged);
        scmIFrameEventsService.subscribe("fusion.profileListItem.openNodesListDialog", this.openNodesListDialog);
        $scope.$on("$destroy", function () {
            _this.scmIFrameEventsService.unsubscribe("apollo.assignElementsByType", _this.setElementsToAssignByType);
            _this.scmIFrameEventsService.unsubscribe("fusion.credentialsStep.onInit", _this.credentialsStepOnInit);
            _this.scmIFrameEventsService.unsubscribe("apollo.credentialsStep.setTitle", _this.setCredentialsStepTitle);
            _this.scmIFrameEventsService.unsubscribe("fusion.profileStep.onInit", _this.profileStepOnInit);
            _this.scmIFrameEventsService.unsubscribe("fusion.profileStep.selectionChanged", _this.selectedProfilesChanged);
            _this.scmIFrameEventsService.unsubscribe("fusion.profileListItem.openNodesListDialog", _this.openNodesListDialog);
        });
    }
    AssignProfileWizardController.prototype.$onInit = function () {
        var _this = this;
        this.$templateCache.put("assignProfileWizard-step1", __webpack_require__(295));
        this.$templateCache.put("assignProfileWizard-step2", __webpack_require__(296));
        this.$templateCache.put("assignProfileWizard-step3", __webpack_require__(297));
        this.$templateCache.put("assignProfileWizard-step4", __webpack_require__(298));
        this.applyAssignProfileWizardSettings().finally(function () {
            _this.isBusy = false;
        });
    };
    AssignProfileWizardController.prototype.onLeave = function () {
        if (angular.isFunction(this.$scope.onLeave)) {
            this.$scope.onLeave();
        }
    };
    AssignProfileWizardController.prototype.onEnterStep = function (step) {
        this.errors = {};
        switch (step.label) {
            case AssignProfileWizardStepName_1.AssignProfileWizardStepName.Confirm:
                this.credentialsChecked = false;
            case AssignProfileWizardStepName_1.AssignProfileWizardStepName.Credentials:
                if (!this.credentialsChecked) {
                    this.loadSummaryData();
                    this.credentialsChecked = false;
                }
        }
        return true;
    };
    AssignProfileWizardController.prototype.onNextStep = function (from, to) {
        var _this = this;
        var go = function () {
            return _this.$q.resolve(true);
        };
        var stayWithError = function (error) {
            _this.errors = (_a = {}, _a[error] = true, _a);
            return _this.$q.resolve(false);
            var _a;
        };
        if (this.isBusy) {
            return stayWithError();
        }
        var checkProfiles = from.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Profiles || to.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Credentials
            || to.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Confirm;
        var checkNodes = from.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Nodes || to.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Credentials
            || to.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Confirm;
        var checkCredentials = this.credentialsStepIsDisplayed &&
            (from.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Credentials || to.label === AssignProfileWizardStepName_1.AssignProfileWizardStepName.Confirm);
        if (checkCredentials && !this.credentialsChecked) {
            this.scmIFrameEventsService.publish("fusion.checkCredentials");
            return this.$q.resolve(false);
        }
        if (checkProfiles) {
            if (!(this.selectedProfiles)) {
                return stayWithError("profiles");
            }
            var anyProfile = this.selectedProfiles.length > 0;
            if (!anyProfile) {
                return stayWithError("profiles");
            }
        }
        if (checkNodes) {
            if (!(this.nodesState.selection && this.nodesState.selection.items)) {
                return stayWithError("nodes");
            }
            var anyNodes = this.nodesState.selection.blacklist
                ? this.nodesState.selection.items.length < this.nodesState.pagination.total
                : this.nodesState.selection.items.length > 0;
            if (!anyNodes) {
                return stayWithError("nodes");
            }
        }
        return go();
    };
    AssignProfileWizardController.prototype.sendAssignments = function () {
        var _this = this;
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return true;
        }
        var assignments = this.summaryRows.map(function (row) { return ({
            nodeId: row.node.nodeId,
            profileIds: row.profiles.filter(function (p) { return p.assignState === "assigning"; }).map(function (p) { return p.id; }),
            assignedElementsByTypes: _this.assignedElementsByTypes
        }); });
        if (_(assignments).flatMap(function (a) { return a.profileIds; }).size() === 0) {
            this.xuiToastService.warning(this._t("There was no new assignment."));
            return false;
        }
        return this.scmProfileService.assignToNodes(assignments)
            .then(function () {
            var nodeCount = assignments.map(function (x) { return x.profileIds; }).filter(function (x) { return x.length > 0; }).length;
            var toastMessage = _this.swUtil.formatString(_this._t("Profile was successfully assigned to {0} node(s)."), nodeCount);
            var allProfileIds = _.uniq(_.flatten(assignments.map(function (x) { return x.profileIds; })));
            if (allProfileIds.length > 1) {
                toastMessage = _this.swUtil.formatString(_this._t("{0} profiles were successfully assigned to {1} node(s)."), allProfileIds.length, nodeCount);
            }
            var assignedElementsByType = _.flatten(assignments.map(function (x) { return x.assignedElementsByTypes; }));
            if (assignedElementsByType.length > 0) {
                toastMessage += " " + _this.swUtil.formatString(_this._t("If you need more granular credentials settings, set them on the Assigned elements page."));
            }
            _this.xuiToastService.success(toastMessage);
            _this.onLeave();
            return true;
        }).catch(function (reason) {
            _this.$log.error(reason);
            _this.xuiToastService.error(_this._t("Unable to assign profiles to nodes."));
            return false;
        });
    };
    AssignProfileWizardController.prototype.applyAssignProfileWizardSettings = function () {
        var _this = this;
        var settings = this.$scope.initialSettings;
        if (!settings) {
            return this.$q.resolve();
        }
        switch (settings.wizardStep) {
            case AssignProfileWizardStepName_1.AssignProfileWizardStepName.Confirm:
                if (!(Array.isArray(settings.selectedNodes) && settings.selectedNodes.length > 0
                    && Array.isArray(settings.selectedProfiles) && settings.selectedProfiles.length > 0)) {
                    this.$log.error("Invalid wizard's preselected nodes or profiles");
                    return this.$q.resolve();
                }
                break;
            case AssignProfileWizardStepName_1.AssignProfileWizardStepName.Nodes:
                if (!(Array.isArray(settings.selectedProfiles) && settings.selectedProfiles.length > 0)) {
                    this.$log.error("Invalid wizard's preselected profiles");
                    return this.$q.resolve();
                }
                break;
            case AssignProfileWizardStepName_1.AssignProfileWizardStepName.Profiles:
                if (Array.isArray(settings.selectedNodes) && settings.selectedNodes.length > 0) {
                    this.swapWizardSteps();
                }
                // can start on profiles step with or without selected nodes/profiles
                break;
            case AssignProfileWizardStepName_1.AssignProfileWizardStepName.Credentials:
                if (!(Array.isArray(settings.selectedNodes) && settings.selectedNodes.length > 0
                    && Array.isArray(settings.selectedProfiles) && settings.selectedProfiles.length > 0)) {
                    this.$log.error("Invalid wizard's preselected nodes or profiles");
                    return this.$q.resolve();
                }
                break;
            default:
                this.$log.error("Invalid initial wizard step");
                return this.$q.resolve();
        }
        this.hideWizardIfSomethingPreselected(settings);
        this.nodesToPreselect = settings.selectedNodes;
        return this.preselectProfiles(settings.selectedProfiles)
            .then(function () { return _this.nodesFinishedLoadingPromise.promise; })
            .then(function () { return _this.goToWizardStep(settings.wizardStep); })
            .finally(function () {
            _this.isLoadingPreselected = false;
        });
    };
    AssignProfileWizardController.prototype.hideWizardIfSomethingPreselected = function (settings) {
        this.isLoadingPreselected = (!_.isNil(settings.selectedProfiles) && settings.selectedProfiles.length > 0) ||
            (!_.isNil(settings.selectedNodes) && settings.selectedNodes.length > 0);
    };
    AssignProfileWizardController.prototype.profilesFinishedLoadingCallback = function () {
        var _this = this;
        if (this.profilesToPreselect) {
            var items = this.profiles.filter(function (p) { return _this.profilesToPreselect.some(function (s) { return s === p.id; }); });
            this.scmIFrameEventsService.publish("fusion.profileStep.preselectProfiles", items);
        }
    };
    AssignProfileWizardController.prototype.nodesFinishedLoadingCallback = function () {
        this.nodesFinishedLoadingPromise.resolve();
    };
    AssignProfileWizardController.prototype.preselectProfiles = function (selectedProfiles) {
        this.profilesToPreselect = selectedProfiles;
        return this.profilesFinishedLoadingPromise.promise;
    };
    AssignProfileWizardController.prototype.swapWizardSteps = function () {
        this.wizardSteps = [this.wizardSteps[1], this.wizardSteps[0], this.wizardSteps[2], this.wizardSteps[3]];
    };
    AssignProfileWizardController.prototype.goToWizardStep = function (wizardStep) {
        var _this = this;
        var wizardStepNumber = _.findIndex(this.wizardSteps, { label: wizardStep });
        var promises = [];
        var _loop_1 = function (stepIndex) {
            promises.push(this_1.$timeout(function () {
                _this.currentStepIndex = stepIndex;
            }));
        };
        var this_1 = this;
        for (var stepIndex = 0; stepIndex <= wizardStepNumber; stepIndex++) {
            _loop_1(stepIndex);
        }
        return this.$q.all(promises);
    };
    AssignProfileWizardController.prototype.loadSummaryData = function () {
        var _this = this;
        this.summaryRows = [];
        var nodeListDataSourceParameters = this.scmFilteredListService.getNodeListDataSourceParametersByFiltersAndSearch(this.nodesState.selection.items, this.nodesState.selection.blacklist, this.nodesState.filters.filterValues, this.nodesState.search);
        nodeListDataSourceParameters.includeProfileHeaders = true;
        this.isBusy = true;
        return this.scmFilteredListService.getFilteredNodes(nodeListDataSourceParameters)
            .then(function (nodes) {
            var selectedProfileIds = _this.selectedProfiles;
            _this.summaryProfiles = _this.profiles.filter(function (p) { return _.includes(selectedProfileIds, p.id); });
            _this.summaryNodes = nodes.items;
            _this.initCredentialsStep();
        }).finally(function () {
            _this.isBusy = false;
        });
    };
    AssignProfileWizardController.prototype.initCredentialsStep = function () {
        if (this.summaryNodes.length > 0) {
            this.scmIFrameEventsService.publish("setProfiles", { profileIds: this.selectedProfiles, nodeIds: this.summaryNodes.map(function (n) { return n.nodeId; }) });
        }
    };
    return AssignProfileWizardController;
}());
exports.AssignProfileWizardController = AssignProfileWizardController;


/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var UnassignProfilesDialogController = /** @class */ (function () {
    /** @ngInject */
    UnassignProfilesDialogController.$inject = ["$templateCache", "$scope", "scmUnassignProfilesControllerState"];
    function UnassignProfilesDialogController($templateCache, $scope, scmUnassignProfilesControllerState) {
        this.$templateCache = $templateCache;
        this.$scope = $scope;
        this.scmUnassignProfilesControllerState = scmUnassignProfilesControllerState;
        this.isPopoverDisplayed = true;
        this.gridPagination = { page: 1, pageSize: 10 };
        this.sorting = {
            sortableColumns: [{
                    id: "name",
                    label: "Name"
                }],
            sortBy: {
                id: "name",
                label: "Name"
            },
            direction: "asc"
        };
        this.options = {
            searchableColumns: ["name"]
        };
    }
    UnassignProfilesDialogController.prototype.$onInit = function () {
        this.$templateCache.put("scm-unassign-profiles-list-item-template", __webpack_require__(304));
        var nodeIds = this.$scope.nodeIds;
        this.scmUnassignProfilesControllerState.loadProfiles(nodeIds);
    };
    return UnassignProfilesDialogController;
}());
exports.UnassignProfilesDialogController = UnassignProfilesDialogController;


/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ScmProfilesCountController = /** @class */ (function () {
    /** @ngInject */
    ScmProfilesCountController.$inject = ["$templateCache", "scmOrionNodeService", "scmProfileElementService", "scmManageProfileDialogService", "scmAuthorizationService"];
    function ScmProfilesCountController($templateCache, scmOrionNodeService, scmProfileElementService, scmManageProfileDialogService, scmAuthorizationService) {
        this.$templateCache = $templateCache;
        this.scmOrionNodeService = scmOrionNodeService;
        this.scmProfileElementService = scmProfileElementService;
        this.scmManageProfileDialogService = scmManageProfileDialogService;
        this.scmAuthorizationService = scmAuthorizationService;
        this.popoverIsDisplayed = false;
        this.showProfileManagementLink = false;
    }
    ScmProfilesCountController.prototype.$onInit = function () {
        var _this = this;
        this.scmAuthorizationService.getIsScmUserAuthorized().then(function (isUserAuthorized) {
            if (isUserAuthorized) {
                _this.showProfileManagementLink = true;
            }
        });
        this.$templateCache.put("scmProfilesCountPopover", __webpack_require__(307));
    };
    ScmProfilesCountController.prototype.popoverOnShow = function () {
        var _this = this;
        this.isLoaded = false;
        this.scmOrionNodeService.getProfiles(this.nodeId).then(function (data) {
            _this.profiles = data;
        }).finally(function () {
            _this.isLoaded = true;
        });
    };
    ScmProfilesCountController.prototype.showProfile = function (profileToView) {
        var _this = this;
        this.popoverIsDisplayed = false;
        if (!this.showProfileManagementLink) {
            return;
        }
        this.scmProfileElementService.getForProfile(profileToView.id).then(function (data) {
            _this.scmManageProfileDialogService.showViewDialog(profileToView, data);
        });
    };
    ScmProfilesCountController.prototype.getProfilesCount = function () {
        if (_.isNil(this.profiles)) {
            return this.profilesCount;
        }
        return this.profiles.length;
    };
    ScmProfilesCountController.prototype.getPopoverTemplate = function () {
        return this.$templateCache.get("scmProfilesCountPopover");
    };
    return ScmProfilesCountController;
}());
exports.default = ScmProfilesCountController;


/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ScmNodesCountController = /** @class */ (function () {
    /** @ngInject */
    ScmNodesCountController.$inject = ["$templateCache", "scmNodesListDialogService", "getTextService", "swUtil"];
    function ScmNodesCountController($templateCache, scmNodesListDialogService, getTextService, swUtil) {
        this.$templateCache = $templateCache;
        this.scmNodesListDialogService = scmNodesListDialogService;
        this.getTextService = getTextService;
        this.swUtil = swUtil;
        this.MaxVisibleItems = 10;
        this.isPopoverDisplayed = true;
        this._t = getTextService;
    }
    ScmNodesCountController.prototype.$onInit = function () {
        this.$templateCache.put("scmNodesCountPopover", __webpack_require__(309));
    };
    ScmNodesCountController.prototype.popoverOnShow = function () {
        this.visibleNodes = this.nodes.slice(0, this.MaxVisibleItems);
    };
    ScmNodesCountController.prototype.getNodesCount = function () {
        if (_.isNil(this.nodes)) {
            return 0;
        }
        return this.nodes.length;
    };
    ScmNodesCountController.prototype.getItemsString = function () {
        var cnt = this.nodes.length;
        var baseText = this.swUtil.formatString("{0} {1}", "{0}", cnt === 1 ? "node" : "nodes");
        return this.swUtil.formatString(this._t(baseText), cnt);
    };
    ScmNodesCountController.prototype.getMoreItemsString = function () {
        return this.swUtil.formatString(this._t("+ {0} more"), this.nodes.length - this.MaxVisibleItems);
    };
    ScmNodesCountController.prototype.openNodesList = function () {
        this.isPopoverDisplayed = false;
        this.scmNodesListDialogService.showModal(null, this.nodes, this.nodesDialogTitle);
    };
    ScmNodesCountController.prototype.getPopoverTemplate = function () {
        return this.$templateCache.get("scmNodesCountPopover");
    };
    return ScmNodesCountController;
}());
exports.default = ScmNodesCountController;


/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SuitableNodeListController = /** @class */ (function () {
    /** @ngInject */
    SuitableNodeListController.$inject = ["$q", "$log", "$scope", "$timeout", "$templateCache", "xuiFilteredListService", "xuiToastService", "getTextService", "scmFilteredListService"];
    function SuitableNodeListController($q, $log, $scope, $timeout, $templateCache, xuiFilteredListService, xuiToastService, getTextService, scmFilteredListService) {
        this.$q = $q;
        this.$log = $log;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.xuiFilteredListService = xuiFilteredListService;
        this.xuiToastService = xuiToastService;
        this.getTextService = getTextService;
        this.scmFilteredListService = scmFilteredListService;
        this.userSettingName = "ScmSuitableNodeListSetting";
        this._t = getTextService;
    }
    SuitableNodeListController.prototype.$onInit = function () {
        this.selectionMode = this.selectionMode || "multi";
        this.availableFilterPropertiesRequest = this.scmFilteredListService.getAvailableFilterProperties();
        this.createNodesState();
        this.createNodesDispatcher();
        this.$templateCache.put("suitableNodeList-item", "<scm-node-detail [node]=\"item\" [open-links-on-new-tab]=\"true\"></scm-node-detail>");
    };
    SuitableNodeListController.prototype.createNodesDispatcher = function () {
        var _this = this;
        this.nodesDispatcher = this.xuiFilteredListService.getDispatcherInstance(this.xuiFilteredListService.getModel(this, "nodesState"), {
            dataSource: {
                getItems: this.createGetItemsSource(),
                getAvailableFilters: function () {
                    return _this.availableFilterPropertiesRequest;
                },
                getFilters: function (params) {
                    return _this.scmFilteredListService.getVisibleFilterPropertiesDetails(params);
                }
            }
        });
    };
    SuitableNodeListController.prototype.createGetItemsSource = function () {
        var _this = this;
        return function (params) {
            var itemSourcePromise = _this.availableFilterPropertiesRequest
                .then(function () { return _this.setDefaultFiltersInTree.bind(_this, params)(); })
                .then(function () { return _this.getNodesSources.bind(_this, params)(); });
            itemSourcePromise
                .then(function () { return _this.$timeout(function () { return _this.preselectNodes(_this.nodesToPreselect); }); })
                .finally(function () {
                if (_this.onLoaded) {
                    _this.$timeout(function () { return _this.onLoaded(); });
                }
            });
            return itemSourcePromise;
        };
    };
    SuitableNodeListController.prototype.setDefaultFiltersInTree = function (params) {
        var _this = this;
        if (_.isNil(this.defaultFiltersInTreePromise)) {
            var nodeStateFilterPath_1 = "suitableNodeList.nodesState.filters";
            var filtersToshow = [
                "Orion.Nodes|AgentDeployed",
                "Orion.Nodes|IsServer",
                "Orion.Nodes|ObjectSubType",
                "Orion.Nodes|Status",
                "Orion.Nodes|Platform",
                "Orion.Nodes|Vendor"
            ];
            this.defaultFiltersInTreePromise = this.scmFilteredListService.setDefaultFilters(this.userSettingName, nodeStateFilterPath_1, this.$scope, this.nodesDispatcher, filtersToshow)
                .then(function () {
                var nodeStateFilters = _(_this.$scope).get(nodeStateFilterPath_1);
                if (_this.nodesToPreselect && _this.nodesToPreselect.length > 0) {
                    return;
                }
                if (_(nodeStateFilters).has("filterValues[\"Orion.Nodes|IsServer\"].checkboxes.True")) {
                    nodeStateFilters.filterValues["Orion.Nodes|IsServer"].checkboxes.True.checked = true;
                    nodeStateFilters.filterValues["Orion.Nodes|IsServer"].expanded = true;
                    params.filters = nodeStateFilters.filterValues;
                }
                if (_(nodeStateFilters).has("filterValues[\"Orion.Nodes|Platform\"].checkboxes.Windows")) {
                    nodeStateFilters.filterValues["Orion.Nodes|Platform"].checkboxes.Windows.checked = true;
                    nodeStateFilters.filterValues["Orion.Nodes|Platform"].expanded = true;
                    params.filters = nodeStateFilters.filterValues;
                }
                if (_(nodeStateFilters).has("filterValues[\"Orion.Nodes|Platform\"].checkboxes.Linux")) {
                    nodeStateFilters.filterValues["Orion.Nodes|Platform"].checkboxes.Linux.checked = true;
                    nodeStateFilters.filterValues["Orion.Nodes|Platform"].expanded = true;
                    params.filters = nodeStateFilters.filterValues;
                }
            })
                .then(function () { return _this.scmFilteredListService.startMonitoringAndSavingFilterChanges(_this.userSettingName, nodeStateFilterPath_1, _this.$scope); });
        }
        return this.defaultFiltersInTreePromise;
    };
    SuitableNodeListController.prototype.getNodesSources = function (params) {
        var _this = this;
        this.$log.debug("Called nodes sources with params:", params);
        var dataSourceParameters = this.scmFilteredListService.getNodeListDataSourceParametersFromIFilteredListItemSourceParams(params);
        return this.scmFilteredListService.getFilteredNodes(dataSourceParameters)
            .then(function (result) {
            return { items: result.items, total: result.totalCount };
        })
            .catch(function (error) {
            _this.$log.error(error);
            _this.xuiToastService.error(_this._t("Unable to load nodes from database. Try again later."));
        });
    };
    SuitableNodeListController.prototype.preselectNodes = function (nodesToPreselect) {
        if (!Array.isArray(nodesToPreselect)) {
            return;
        }
        this.nodesState.selection = { items: nodesToPreselect };
        return this.$q.resolve();
    };
    SuitableNodeListController.prototype.createNodesState = function () {
        this.nodesState = {
            sorting: {
                sortableColumns: [
                    { id: "Orion.Nodes|name", label: this._t("Name") }
                ],
                sortBy: {
                    id: "Orion.Nodes|name",
                    label: this._t("Name")
                },
                direction: "asc"
            },
            pagination: {
                page: 1,
                pageSize: 10,
                total: 200
            },
            filters: {
                filterProperties: [],
                visibleFilterPropertyIds: []
            },
            options: {
                layout: "contained",
                stripe: false,
                selectionMode: this.selectionMode,
                selectionProperty: "nodeId",
                trackBy: "nodeId",
                templateUrl: "suitableNodeList-item",
                rowPadding: "narrow",
                showEmptyFilterProperties: true,
                categorizedFilterPanel: true,
                showMorePropertyValuesThreshold: 10,
                allowSelectAllPages: true
            }
        };
    };
    return SuitableNodeListController;
}());
exports.SuitableNodeListController = SuitableNodeListController;


/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ContentController = /** @class */ (function () {
    /** @ngInject */
    ContentController.$inject = ["$scope", "$timeout", "getTextService", "scmCentralizedSettingsService", "swUtil"];
    function ContentController($scope, $timeout, getTextService, scmCentralizedSettingsService, swUtil) {
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.getTextService = getTextService;
        this.scmCentralizedSettingsService = scmCentralizedSettingsService;
        this.swUtil = swUtil;
        this._t = this.getTextService;
    }
    ContentController.prototype.$onInit = function () {
        var _this = this;
        this.$scope.$watch("contentCtrl.content", function (newValue, oldValue) {
            if (!newValue || !oldValue) {
                return;
            }
            if (newValue.id !== oldValue.id) {
                _this.resetContentTracking();
            }
        });
        this.scmCentralizedSettingsService.getCachedSetting("MaximumSizeOfContentInMegabytes").then(function (setting) {
            return _this.contentSizeLimit = Math.min(setting, 1024);
        });
    };
    // This removes the line repeater from DOM and forces refresh of the content.
    // We need to use track by, because of adding more lines without redrawing already loaded lines
    // but at the same time, we want the content to properly refresh, when switching to different content
    ContentController.prototype.resetContentTracking = function () {
        var _this = this;
        this.hideContent = true;
        this.$timeout(function () {
            _this.hideContent = false;
            _this.$scope.$apply();
        });
    };
    ContentController.prototype.getNoContentMessage = function () {
        if (!this.content || this.content.lines.length) {
            return null;
        }
        switch (this.contentState) {
            case "contentnotpresent" /* ContentNotPresent */:
                return this._t("This configuration item was not monitored or did not exist at this time.");
            case "collectcontentdisabled" /* CollectContentDisabled */:
                return this._t("The content was not downloaded because content downloading was disabled for this configuration element.");
            case "collectcontenterror" /* CollectContentError */:
                return this._t("The content for this configuration item is not available.");
            case "sizelimitexceeded" /* SizeLimitExceeded */:
                return this.swUtil.formatString(this._t("The content was not downloaded because it exceeds the maximum size of {0} MBs."), this.contentSizeLimit);
            case "binarycontent" /* BinaryContent */:
                return this._t("The content was not downloaded because it is a binary file.");
            default:
                return this._t("Element tested successfully, but no content has been polled.");
        }
    };
    return ContentController;
}());
exports.ContentController = ContentController;


/***/ }),
/* 162 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=\"\"> <span _t> SCM needs Asset Inventory enabled to monitor servers for hardware and software changes. When you click Enable Asset Inventory, SCM automatically enables Asset Inventory for <b>{{vm.dialogOptions.viewModel.nodeNames}}</b> node<span ng-if=\"vm.dialogOptions.viewModel.nodeCount > 1\">s</span> if it meets specific requirements. This may take a couple of minutes. </span> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.prereqsHelpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn More</a> </xui-dialog>";

/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var toMetaKey = metadata.key;
var ordinaryDefineOwnMetadata = metadata.set;

metadata.exp({ defineMetadata: function defineMetadata(metadataKey, metadataValue, target, targetKey) {
  ordinaryDefineOwnMetadata(metadataKey, metadataValue, anObject(target), toMetaKey(targetKey));
} });


/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var strong = __webpack_require__(58);
var validate = __webpack_require__(14);
var MAP = 'Map';

// 23.1 Map Objects
module.exports = __webpack_require__(44)(MAP, function (get) {
  return function Map() { return get(this, arguments.length > 0 ? arguments[0] : undefined); };
}, {
  // 23.1.3.6 Map.prototype.get(key)
  get: function get(key) {
    var entry = strong.getEntry(validate(this, MAP), key);
    return entry && entry.v;
  },
  // 23.1.3.9 Map.prototype.set(key, value)
  set: function set(key, value) {
    return strong.def(validate(this, MAP), key === 0 ? 0 : key, value);
  }
}, strong, true);


/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(13);
var anObject = __webpack_require__(2);
var getKeys = __webpack_require__(63);

module.exports = __webpack_require__(8) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(10);
var toIObject = __webpack_require__(32);
var arrayIndexOf = __webpack_require__(167)(false);
var IE_PROTO = __webpack_require__(36)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(32);
var toLength = __webpack_require__(35);
var toAbsoluteIndex = __webpack_require__(168);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(65);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(6).document;
module.exports = document && document.documentElement;


/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(23)('native-function-to-string', Function.toString);


/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__(2);
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__(40);
var ITERATOR = __webpack_require__(7)('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__(174);
var ITERATOR = __webpack_require__(7)('iterator');
var Iterators = __webpack_require__(40);
module.exports = __webpack_require__(17).getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__(34);
var TAG = __webpack_require__(7)('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(66);
var $export = __webpack_require__(41);
var redefine = __webpack_require__(18);
var hide = __webpack_require__(25);
var Iterators = __webpack_require__(40);
var $iterCreate = __webpack_require__(176);
var setToStringTag = __webpack_require__(42);
var getPrototypeOf = __webpack_require__(27);
var ITERATOR = __webpack_require__(7)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(62);
var descriptor = __webpack_require__(38);
var setToStringTag = __webpack_require__(42);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(25)(IteratorPrototype, __webpack_require__(7)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 177 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__(6);
var dP = __webpack_require__(13);
var DESCRIPTORS = __webpack_require__(8);
var SPECIES = __webpack_require__(7)('species');

module.exports = function (KEY) {
  var C = global[KEY];
  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
    configurable: true,
    get: function () { return this; }
  });
};


/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__(7)('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(4);
var setPrototypeOf = __webpack_require__(181).set;
module.exports = function (that, target, C) {
  var S = target.constructor;
  var P;
  if (S !== C && typeof S == 'function' && (P = S.prototype) !== C.prototype && isObject(P) && setPrototypeOf) {
    setPrototypeOf(that, P);
  } return that;
};


/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(4);
var anObject = __webpack_require__(2);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(19)(Function.call, __webpack_require__(182).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(69);
var createDesc = __webpack_require__(38);
var toIObject = __webpack_require__(32);
var toPrimitive = __webpack_require__(61);
var has = __webpack_require__(10);
var IE8_DOM_DEFINE = __webpack_require__(59);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(8) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__(6);
var each = __webpack_require__(70)(0);
var redefine = __webpack_require__(18);
var meta = __webpack_require__(28);
var assign = __webpack_require__(187);
var weak = __webpack_require__(189);
var isObject = __webpack_require__(4);
var validate = __webpack_require__(14);
var NATIVE_WEAK_MAP = __webpack_require__(14);
var IS_IE11 = !global.ActiveXObject && 'ActiveXObject' in global;
var WEAK_MAP = 'WeakMap';
var getWeak = meta.getWeak;
var isExtensible = Object.isExtensible;
var uncaughtFrozenStore = weak.ufstore;
var InternalMap;

var wrapper = function (get) {
  return function WeakMap() {
    return get(this, arguments.length > 0 ? arguments[0] : undefined);
  };
};

var methods = {
  // 23.3.3.3 WeakMap.prototype.get(key)
  get: function get(key) {
    if (isObject(key)) {
      var data = getWeak(key);
      if (data === true) return uncaughtFrozenStore(validate(this, WEAK_MAP)).get(key);
      return data ? data[this._i] : undefined;
    }
  },
  // 23.3.3.5 WeakMap.prototype.set(key, value)
  set: function set(key, value) {
    return weak.def(validate(this, WEAK_MAP), key, value);
  }
};

// 23.3 WeakMap Objects
var $WeakMap = module.exports = __webpack_require__(44)(WEAK_MAP, wrapper, methods, weak, true, true);

// IE11 WeakMap frozen keys fix
if (NATIVE_WEAK_MAP && IS_IE11) {
  InternalMap = weak.getConstructor(wrapper, WEAK_MAP);
  assign(InternalMap.prototype, methods);
  meta.NEED = true;
  each(['delete', 'has', 'get', 'set'], function (key) {
    var proto = $WeakMap.prototype;
    var method = proto[key];
    redefine(proto, key, function (a, b) {
      // store frozen objects on internal weakmap shim
      if (isObject(a) && !isExtensible(a)) {
        if (!this._f) this._f = new InternalMap();
        var result = this._f[key](a, b);
        return key == 'set' ? this : result;
      // store all the rest on native weakmap
      } return method.call(this, a, b);
    });
  });
}


/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

// 9.4.2.3 ArraySpeciesCreate(originalArray, length)
var speciesConstructor = __webpack_require__(185);

module.exports = function (original, length) {
  return new (speciesConstructor(original))(length);
};


/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(4);
var isArray = __webpack_require__(186);
var SPECIES = __webpack_require__(7)('species');

module.exports = function (original) {
  var C;
  if (isArray(original)) {
    C = original.constructor;
    // cross-realm fallback
    if (typeof C == 'function' && (C === Array || isArray(C.prototype))) C = undefined;
    if (isObject(C)) {
      C = C[SPECIES];
      if (C === null) C = undefined;
    }
  } return C === undefined ? Array : C;
};


/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(34);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var DESCRIPTORS = __webpack_require__(8);
var getKeys = __webpack_require__(63);
var gOPS = __webpack_require__(188);
var pIE = __webpack_require__(69);
var toObject = __webpack_require__(43);
var IObject = __webpack_require__(33);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(16)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) {
      key = keys[j++];
      if (!DESCRIPTORS || isEnum.call(S, key)) T[key] = S[key];
    }
  } return T;
} : $assign;


/***/ }),
/* 188 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 189 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var redefineAll = __webpack_require__(37);
var getWeak = __webpack_require__(28).getWeak;
var anObject = __webpack_require__(2);
var isObject = __webpack_require__(4);
var anInstance = __webpack_require__(39);
var forOf = __webpack_require__(26);
var createArrayMethod = __webpack_require__(70);
var $has = __webpack_require__(10);
var validate = __webpack_require__(14);
var arrayFind = createArrayMethod(5);
var arrayFindIndex = createArrayMethod(6);
var id = 0;

// fallback for uncaught frozen keys
var uncaughtFrozenStore = function (that) {
  return that._l || (that._l = new UncaughtFrozenStore());
};
var UncaughtFrozenStore = function () {
  this.a = [];
};
var findUncaughtFrozen = function (store, key) {
  return arrayFind(store.a, function (it) {
    return it[0] === key;
  });
};
UncaughtFrozenStore.prototype = {
  get: function (key) {
    var entry = findUncaughtFrozen(this, key);
    if (entry) return entry[1];
  },
  has: function (key) {
    return !!findUncaughtFrozen(this, key);
  },
  set: function (key, value) {
    var entry = findUncaughtFrozen(this, key);
    if (entry) entry[1] = value;
    else this.a.push([key, value]);
  },
  'delete': function (key) {
    var index = arrayFindIndex(this.a, function (it) {
      return it[0] === key;
    });
    if (~index) this.a.splice(index, 1);
    return !!~index;
  }
};

module.exports = {
  getConstructor: function (wrapper, NAME, IS_MAP, ADDER) {
    var C = wrapper(function (that, iterable) {
      anInstance(that, C, NAME, '_i');
      that._t = NAME;      // collection type
      that._i = id++;      // collection id
      that._l = undefined; // leak store for uncaught frozen objects
      if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
    });
    redefineAll(C.prototype, {
      // 23.3.3.2 WeakMap.prototype.delete(key)
      // 23.4.3.3 WeakSet.prototype.delete(value)
      'delete': function (key) {
        if (!isObject(key)) return false;
        var data = getWeak(key);
        if (data === true) return uncaughtFrozenStore(validate(this, NAME))['delete'](key);
        return data && $has(data, this._i) && delete data[this._i];
      },
      // 23.3.3.4 WeakMap.prototype.has(key)
      // 23.4.3.4 WeakSet.prototype.has(value)
      has: function has(key) {
        if (!isObject(key)) return false;
        var data = getWeak(key);
        if (data === true) return uncaughtFrozenStore(validate(this, NAME)).has(key);
        return data && $has(data, this._i);
      }
    });
    return C;
  },
  def: function (that, key, value) {
    var data = getWeak(anObject(key), true);
    if (data === true) uncaughtFrozenStore(that).set(key, value);
    else data[that._i] = value;
    return that;
  },
  ufstore: uncaughtFrozenStore
};


/***/ }),
/* 190 */
/***/ (function(module, exports, __webpack_require__) {

var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var toMetaKey = metadata.key;
var getOrCreateMetadataMap = metadata.map;
var store = metadata.store;

metadata.exp({ deleteMetadata: function deleteMetadata(metadataKey, target /* , targetKey */) {
  var targetKey = arguments.length < 3 ? undefined : toMetaKey(arguments[2]);
  var metadataMap = getOrCreateMetadataMap(anObject(target), targetKey, false);
  if (metadataMap === undefined || !metadataMap['delete'](metadataKey)) return false;
  if (metadataMap.size) return true;
  var targetMetadata = store.get(target);
  targetMetadata['delete'](targetKey);
  return !!targetMetadata.size || store['delete'](target);
} });


/***/ }),
/* 191 */
/***/ (function(module, exports, __webpack_require__) {

var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var getPrototypeOf = __webpack_require__(27);
var ordinaryHasOwnMetadata = metadata.has;
var ordinaryGetOwnMetadata = metadata.get;
var toMetaKey = metadata.key;

var ordinaryGetMetadata = function (MetadataKey, O, P) {
  var hasOwn = ordinaryHasOwnMetadata(MetadataKey, O, P);
  if (hasOwn) return ordinaryGetOwnMetadata(MetadataKey, O, P);
  var parent = getPrototypeOf(O);
  return parent !== null ? ordinaryGetMetadata(MetadataKey, parent, P) : undefined;
};

metadata.exp({ getMetadata: function getMetadata(metadataKey, target /* , targetKey */) {
  return ordinaryGetMetadata(metadataKey, anObject(target), arguments.length < 3 ? undefined : toMetaKey(arguments[2]));
} });


/***/ }),
/* 192 */
/***/ (function(module, exports, __webpack_require__) {

var Set = __webpack_require__(193);
var from = __webpack_require__(194);
var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var getPrototypeOf = __webpack_require__(27);
var ordinaryOwnMetadataKeys = metadata.keys;
var toMetaKey = metadata.key;

var ordinaryMetadataKeys = function (O, P) {
  var oKeys = ordinaryOwnMetadataKeys(O, P);
  var parent = getPrototypeOf(O);
  if (parent === null) return oKeys;
  var pKeys = ordinaryMetadataKeys(parent, P);
  return pKeys.length ? oKeys.length ? from(new Set(oKeys.concat(pKeys))) : pKeys : oKeys;
};

metadata.exp({ getMetadataKeys: function getMetadataKeys(target /* , targetKey */) {
  return ordinaryMetadataKeys(anObject(target), arguments.length < 2 ? undefined : toMetaKey(arguments[1]));
} });


/***/ }),
/* 193 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var strong = __webpack_require__(58);
var validate = __webpack_require__(14);
var SET = 'Set';

// 23.2 Set Objects
module.exports = __webpack_require__(44)(SET, function (get) {
  return function Set() { return get(this, arguments.length > 0 ? arguments[0] : undefined); };
}, {
  // 23.2.3.1 Set.prototype.add(value)
  add: function add(value) {
    return strong.def(validate(this, SET), value = value === 0 ? 0 : value, value);
  }
}, strong);


/***/ }),
/* 194 */
/***/ (function(module, exports, __webpack_require__) {

var forOf = __webpack_require__(26);

module.exports = function (iter, ITERATOR) {
  var result = [];
  forOf(iter, false, result.push, result, ITERATOR);
  return result;
};


/***/ }),
/* 195 */
/***/ (function(module, exports, __webpack_require__) {

var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var ordinaryGetOwnMetadata = metadata.get;
var toMetaKey = metadata.key;

metadata.exp({ getOwnMetadata: function getOwnMetadata(metadataKey, target /* , targetKey */) {
  return ordinaryGetOwnMetadata(metadataKey, anObject(target)
    , arguments.length < 3 ? undefined : toMetaKey(arguments[2]));
} });


/***/ }),
/* 196 */
/***/ (function(module, exports, __webpack_require__) {

var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var ordinaryOwnMetadataKeys = metadata.keys;
var toMetaKey = metadata.key;

metadata.exp({ getOwnMetadataKeys: function getOwnMetadataKeys(target /* , targetKey */) {
  return ordinaryOwnMetadataKeys(anObject(target), arguments.length < 2 ? undefined : toMetaKey(arguments[1]));
} });


/***/ }),
/* 197 */
/***/ (function(module, exports, __webpack_require__) {

var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var getPrototypeOf = __webpack_require__(27);
var ordinaryHasOwnMetadata = metadata.has;
var toMetaKey = metadata.key;

var ordinaryHasMetadata = function (MetadataKey, O, P) {
  var hasOwn = ordinaryHasOwnMetadata(MetadataKey, O, P);
  if (hasOwn) return true;
  var parent = getPrototypeOf(O);
  return parent !== null ? ordinaryHasMetadata(MetadataKey, parent, P) : false;
};

metadata.exp({ hasMetadata: function hasMetadata(metadataKey, target /* , targetKey */) {
  return ordinaryHasMetadata(metadataKey, anObject(target), arguments.length < 3 ? undefined : toMetaKey(arguments[2]));
} });


/***/ }),
/* 198 */
/***/ (function(module, exports, __webpack_require__) {

var metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var ordinaryHasOwnMetadata = metadata.has;
var toMetaKey = metadata.key;

metadata.exp({ hasOwnMetadata: function hasOwnMetadata(metadataKey, target /* , targetKey */) {
  return ordinaryHasOwnMetadata(metadataKey, anObject(target)
    , arguments.length < 3 ? undefined : toMetaKey(arguments[2]));
} });


/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

var $metadata = __webpack_require__(5);
var anObject = __webpack_require__(2);
var aFunction = __webpack_require__(68);
var toMetaKey = $metadata.key;
var ordinaryDefineOwnMetadata = $metadata.set;

$metadata.exp({ metadata: function metadata(metadataKey, metadataValue) {
  return function decorator(target, targetKey) {
    ordinaryDefineOwnMetadata(
      metadataKey, metadataValue,
      (targetKey !== undefined ? anObject : aFunction)(target),
      toMetaKey(targetKey)
    );
  };
} });


/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var reflector_reader_1 = __webpack_require__(201);
/**
 * Reflective information about a symbol, including annotations, interfaces, and other metadata.
 */
var ReflectionInfo = (function () {
    function ReflectionInfo(annotations, parameters, factory, interfaces, propMetadata) {
        this.annotations = annotations;
        this.parameters = parameters;
        this.factory = factory;
        this.interfaces = interfaces;
        this.propMetadata = propMetadata;
    }
    return ReflectionInfo;
}());
exports.ReflectionInfo = ReflectionInfo;
/**
 * Provides access to reflection data about symbols. Used internally by Angular
 * to power dependency injection and compilation.
 */
var Reflector = (function (_super) {
    __extends(Reflector, _super);
    function Reflector(reflectionCapabilities) {
        var _this = _super.call(this) || this;
        // this._usedKeys = null;
        _this.reflectionCapabilities = reflectionCapabilities;
        return _this;
    }
    Reflector.prototype.isReflectionEnabled = function () { return this.reflectionCapabilities.isReflectionEnabled(); };
    Reflector.prototype.parameters = function (typeOrFunc) {
        // // get cached
        // if (this._injectableInfo.has(typeOrFunc)) {
        //   var res = this._getReflectionInfo(typeOrFunc).parameters;
        //   return isPresent(res) ? res : [];
        // } else {
        return this.reflectionCapabilities.parameters(typeOrFunc);
        // }
    };
    Reflector.prototype.rawParameters = function (typeOrFunc) {
        return this.reflectionCapabilities.rawParameters(typeOrFunc);
    };
    Reflector.prototype.registerParameters = function (parameters, typeOrFunc) {
        this.reflectionCapabilities.registerParameters(parameters, typeOrFunc);
    };
    Reflector.prototype.annotations = function (typeOrFunc) {
        // // get cached
        // if (this._injectableInfo.has(typeOrFunc)) {
        //   var res = this._getReflectionInfo(typeOrFunc).annotations;
        //   return isPresent(res) ? res : [];
        // } else {
        return this.reflectionCapabilities.annotations(typeOrFunc);
        // }
    };
    Reflector.prototype.ownAnnotations = function (typeOrFunc) {
        return this.reflectionCapabilities.ownAnnotations(typeOrFunc);
    };
    Reflector.prototype.registerAnnotations = function (parameters, typeOrFunc) {
        this.reflectionCapabilities.registerAnnotations(parameters, typeOrFunc);
    };
    Reflector.prototype.propMetadata = function (typeOrFunc) {
        // // get cached
        // if (this._injectableInfo.has(typeOrFunc)) {
        //   var res = this._getReflectionInfo(typeOrFunc).propMetadata;
        //   return isPresent(res) ? res : {};
        // } else {
        return this.reflectionCapabilities.propMetadata(typeOrFunc);
        // }
    };
    Reflector.prototype.ownPropMetadata = function (typeOrFunc) {
        return this.reflectionCapabilities.ownPropMetadata(typeOrFunc);
    };
    Reflector.prototype.registerPropMetadata = function (parameters, typeOrFunc) {
        this.reflectionCapabilities.registerPropMetadata(parameters, typeOrFunc);
    };
    Reflector.prototype.registerDowngradedNg2ComponentName = function (componentName, typeOrFunc) {
        this.reflectionCapabilities.registerDowngradedNg2ComponentName(componentName, typeOrFunc);
    };
    Reflector.prototype.downgradedNg2ComponentName = function (typeOrFunc) {
        return this.reflectionCapabilities.downgradedNg2ComponentName(typeOrFunc);
    };
    /** @internal */
    Reflector.prototype._getReflectionInfo = function (typeOrFunc) {
        /*if (isPresent(this._usedKeys)) {
         this._usedKeys.add(typeOrFunc);
         }
         return this._injectableInfo.get(typeOrFunc);*/
    };
    /** @internal */
    Reflector.prototype._containsReflectionInfo = function (typeOrFunc) { };
    return Reflector;
}(reflector_reader_1.ReflectorReader));
exports.Reflector = Reflector;
//# sourceMappingURL=reflector.js.map

/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Provides read-only access to reflection data about symbols. Used internally by Angular
 * to power dependency injection and compilation.
 */
var ReflectorReader = (function () {
    function ReflectorReader() {
    }
    return ReflectorReader;
}());
exports.ReflectorReader = ReflectorReader;
//# sourceMappingURL=reflector_reader.js.map

/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
// import {BaseException} from 'angular2/src/facade/exceptions';
// This will be needed when we will used Reflect APIs
var Reflect = lang_1.global.Reflect;
if (!isReflectMetadata(Reflect)) {
    throw "\n    Reflect.*metadata shim is required when using class decorators.\n    You can use one of: \n    - \"reflect-metadata\" (https://www.npmjs.com/package/reflect-metadata) \n    - \"core-js/es7/reflect\" (https://github.com/zloirock/core-js)\n  ";
}
/**
 * @internal
 */
exports.CLASS_META_KEY = 'annotations';
/**
 * @internal
 */
exports.PARAM_META_KEY = 'parameters';
/**
 * @internal
 */
exports.PARAM_REFLECT_META_KEY = 'design:paramtypes';
/**
 * @internal
 */
exports.PROP_META_KEY = 'propMetadata';
/**
 * @internal
 */
exports.DOWNGRADED_COMPONENT_NAME_KEY = 'downgradeComponentName';
function isReflectMetadata(reflect) {
    return lang_1.isPresent(reflect) && lang_1.isPresent(reflect.getMetadata);
}
var ReflectionCapabilities = (function () {
    function ReflectionCapabilities(reflect) {
        if (reflect === void 0) { reflect = lang_1.global.Reflect; }
        this._reflect = reflect;
    }
    ReflectionCapabilities.prototype.isReflectionEnabled = function () { return true; };
    ReflectionCapabilities.prototype.factory = function (t) {
        switch (t.length) {
            case 0:
                return function () { return new t(); };
            case 1:
                return function (a1) { return new t(a1); };
            case 2:
                return function (a1, a2) { return new t(a1, a2); };
            case 3:
                return function (a1, a2, a3) { return new t(a1, a2, a3); };
            case 4:
                return function (a1, a2, a3, a4) { return new t(a1, a2, a3, a4); };
            case 5:
                return function (a1, a2, a3, a4, a5) { return new t(a1, a2, a3, a4, a5); };
            case 6:
                return function (a1, a2, a3, a4, a5, a6) {
                    return new t(a1, a2, a3, a4, a5, a6);
                };
            case 7:
                return function (a1, a2, a3, a4, a5, a6, a7) {
                    return new t(a1, a2, a3, a4, a5, a6, a7);
                };
            case 8:
                return function (a1, a2, a3, a4, a5, a6, a7, a8) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8);
                };
            case 9:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9);
                };
            case 10:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) { return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10); };
            case 11:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) { return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11); };
            case 12:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12);
                };
            case 13:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13);
                };
            case 14:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14);
                };
            case 15:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15);
                };
            case 16:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16);
                };
            case 17:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17) {
                    return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17);
                };
            case 18:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18) { return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18); };
            case 19:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19) { return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19); };
            case 20:
                return function (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20) { return new t(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20); };
        }
        throw new Error("Cannot create a factory for '" + lang_1.stringify(t) + "' because its constructor has more than 20 arguments");
    };
    /** @internal */
    ReflectionCapabilities.prototype._zipTypesAndAnnotations = function (paramTypes, paramAnnotations) {
        var result;
        if (typeof paramTypes === 'undefined') {
            result = new Array(paramAnnotations.length);
        }
        else {
            result = new Array(paramTypes.length);
        }
        for (var i = 0; i < result.length; i++) {
            // TS outputs Object for parameters without types, while Traceur omits
            // the annotations. For now we preserve the Traceur behavior to aid
            // migration, but this can be revisited.
            if (typeof paramTypes === 'undefined') {
                result[i] = [];
            }
            else if (paramTypes[i] != Object) {
                result[i] = [paramTypes[i]];
            }
            else {
                result[i] = [];
            }
            if (lang_1.isPresent(paramAnnotations) && lang_1.isPresent(paramAnnotations[i])) {
                result[i] = result[i].concat(paramAnnotations[i]);
            }
        }
        return result;
    };
    ReflectionCapabilities.prototype.parameters = function (typeOrFunc) {
        // // Prefer the direct API.
        // if (isPresent((<any>typeOrFunc).parameters)) {
        //   return (<any>typeOrFunc).parameters;
        // }
        if (isReflectMetadata(this._reflect)) {
            // get parameter created with @Inject()
            var paramAnnotations = this._reflect.getMetadata(exports.PARAM_META_KEY, typeOrFunc);
            // get parameter created via TS type annotations
            var paramTypes = this._reflect.getMetadata(exports.PARAM_REFLECT_META_KEY, typeOrFunc);
            if (lang_1.isPresent(paramTypes) || lang_1.isPresent(paramAnnotations)) {
                return this._zipTypesAndAnnotations(paramTypes, paramAnnotations);
            }
        }
        // The array has to be filled with `undefined` because holes would be skipped by `some`
        var parameters = new Array(typeOrFunc.length);
        collections_1.ListWrapper.fill(parameters, undefined);
        // parameters.fill(undefined);
        return parameters;
    };
    ReflectionCapabilities.prototype.rawParameters = function (typeOrFunc) {
        return this._reflect.getMetadata(exports.PARAM_META_KEY, typeOrFunc);
    };
    ReflectionCapabilities.prototype.registerParameters = function (parameters, type) {
        this._reflect.defineMetadata(exports.PARAM_META_KEY, parameters, type);
    };
    ReflectionCapabilities.prototype.annotations = function (typeOrFunc) {
        // // Prefer the direct API.
        // if (isPresent((<any>typeOrFunc).annotations)) {
        //   var annotations = (<any>typeOrFunc).annotations;
        //   if (isFunction(annotations) && annotations.annotations) {
        //     annotations = annotations.annotations;
        //   }
        //   return annotations;
        // }
        if (isReflectMetadata(this._reflect)) {
            var annotations = this._reflect.getMetadata(exports.CLASS_META_KEY, typeOrFunc);
            if (lang_1.isPresent(annotations))
                return annotations;
        }
        return [];
    };
    ReflectionCapabilities.prototype.ownAnnotations = function (typeOrFunc) {
        return this._reflect.getOwnMetadata(exports.CLASS_META_KEY, typeOrFunc);
    };
    ReflectionCapabilities.prototype.registerAnnotations = function (annotations, typeOrFunc) {
        this._reflect.defineMetadata(exports.CLASS_META_KEY, annotations, typeOrFunc);
    };
    ReflectionCapabilities.prototype.propMetadata = function (typeOrFunc) {
        // // Prefer the direct API.
        // if (isPresent((<any>typeOrFunc).propMetadata)) {
        //   var propMetadata = (<any>typeOrFunc).propMetadata;
        //   if (isFunction(propMetadata) && propMetadata.propMetadata) {
        //     propMetadata = propMetadata.propMetadata;
        //   }
        //   return propMetadata;
        // }
        if (isReflectMetadata(this._reflect)) {
            var propMetadata = this._reflect.getMetadata(exports.PROP_META_KEY, typeOrFunc);
            if (lang_1.isPresent(propMetadata))
                return propMetadata;
        }
        return {};
    };
    ReflectionCapabilities.prototype.ownPropMetadata = function (typeOrFunc) {
        return this._reflect.getOwnMetadata(exports.PROP_META_KEY, typeOrFunc);
    };
    ReflectionCapabilities.prototype.registerPropMetadata = function (propMetadata, typeOrFunc) {
        this._reflect.defineMetadata(exports.PROP_META_KEY, propMetadata, typeOrFunc);
    };
    ReflectionCapabilities.prototype.registerDowngradedNg2ComponentName = function (componentName, typeOrFunc) {
        this._reflect.defineMetadata(exports.DOWNGRADED_COMPONENT_NAME_KEY, componentName, typeOrFunc);
    };
    ReflectionCapabilities.prototype.downgradedNg2ComponentName = function (typeOrFunc) {
        return this._reflect.getOwnMetadata(exports.DOWNGRADED_COMPONENT_NAME_KEY, typeOrFunc);
    };
    ReflectionCapabilities.prototype.interfaces = function (type) {
        // throw new BaseException("JavaScript does not support interfaces");
        throw new Error('JavaScript does not support interfaces');
    };
    ReflectionCapabilities.prototype.getter = function (name) { return new Function('o', 'return o.' + name + ';'); };
    ReflectionCapabilities.prototype.setter = function (name) {
        return new Function('o', 'v', 'return o.' + name + ' = v;');
    };
    ReflectionCapabilities.prototype.method = function (name) {
        var functionBody = "if (!o." + name + ") throw new Error('\"" + name + "\" is undefined');\n        return o." + name + ".apply(o, args);";
        return new Function('o', 'args', functionBody);
    };
    return ReflectionCapabilities;
}());
exports.ReflectionCapabilities = ReflectionCapabilities;
//# sourceMappingURL=reflection_capabilities.js.map

/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
var lang_2 = __webpack_require__(1);
var lang_3 = __webpack_require__(1);
/**
 * @TODO
 * this module is not used, we can leverage the Key creation for
 * caching @Inject token to string names for performance
 */
/**
 * A unique object used for retrieving items from the {@link Injector}.
 *
 * Keys have:
 * - a system-wide unique `id`.
 * - a `token`.
 *
 * `Key` is used internally by {@link Injector} because its system-wide unique `id` allows the
 * injector to store created objects in a more efficient way.
 *
 * `Key` should not be created directly. {@link Injector} creates keys automatically when resolving
 * providers.
 */
//export class Key {
//  /**
//   * Private
//   */
//  constructor( public token: Object, public id: number ) {
//    if ( isBlank( token ) ) {
//      throw new Error( 'Token must be defined!' );
//    }
//  }
//
//  /**
//   * Returns a stringified token.
//   */
//  get displayName(): string { return stringify( this.token ); }
//
//  /**
//   * Retrieves a `Key` for a token.
//   */
//  static get( token: Object ): Key { return _globalKeyRegistry.get( resolveForwardRef( token ) ); }
//
//  /**
//   * @returns the number of keys registered in the system.
//   */
//  static get numberOfKeys(): number { return _globalKeyRegistry.numberOfKeys; }
//}
/**
 * @internal
 */
var KeyRegistry = (function () {
    function KeyRegistry() {
        this._allKeys = collections_1.ListWrapper.create();
        this._idCounter = 0;
    }
    //get( token: string | OpaqueToken | Type ): string {
    //  // Return it if it is already a string like `'$http'` or `'$state'`
    //  if(isString(token)) {
    //    return token;
    //  }
    //  if(token instanceof OpaqueToken){
    //    return token.desc;
    //  }
    //
    //  const tokenString = stringify( token );
    //  const hasToken = StringMapWrapper.contains( this._allKeys, tokenString );
    //
    //  if ( hasToken ) {
    //    return tokenString;
    //  }
    //
    //  const newKey = `${ tokenString }${ this._uniqueId() }`;
    //  StringMapWrapper.set( this._allKeys, newKey, token );
    //  return newKey;
    //}
    /**
     *
     * @param token
     * @returns {*}
     */
    KeyRegistry.prototype.get = function (token) {
        if (!lang_2.isType(token)) {
            throw new Error("KeyRegistry#get:\n                        ================\n                        you'v tried to create a key for `" + token + "`\n                        creating and getting key tokens is avaialable only for Type");
        }
        var newKey = "" + lang_3.getTypeName(token) + KeyRegistry._suffix + this._uniqueId();
        this._allKeys.push(newKey);
        return newKey;
    };
    Object.defineProperty(KeyRegistry.prototype, "numberOfKeys", {
        get: function () { return collections_1.ListWrapper.size(this._allKeys); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(KeyRegistry.prototype, "allKeys", {
        get: function () { return collections_1.ListWrapper.clone(this._allKeys); },
        enumerable: true,
        configurable: true
    });
    /**
     * just for testing purposes
     * @private
     * @internal
     */
    KeyRegistry.prototype._reset = function () {
        collections_1.ListWrapper.clear(this._allKeys);
        this._idCounter = 0;
    };
    /**
     * Generates a unique ID. If `prefix` is provided the ID is appended to it.
     *
     * @param {string} [prefix] The value to prefix the ID with.
     * @returns {string} Returns the unique ID.
     * @example
     *
     * _uniqueId('contact_');
     * // => 'contact_104'
     *
     * _uniqueId();
     * // => '105'
     */
    KeyRegistry.prototype._uniqueId = function (prefix) {
        var id = ++this._idCounter;
        return "" + lang_1.baseToString(prefix) + id;
    };
    return KeyRegistry;
}());
KeyRegistry._suffix = "#";
exports.KeyRegistry = KeyRegistry;
exports.globalKeyRegistry = new KeyRegistry();
//# sourceMappingURL=key.js.map

/***/ }),
/* 204 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var pipe_resolver_1 = __webpack_require__(205);
/**
 * @internal
 */
var PipeProvider = (function () {
    function PipeProvider(pipeResolver) {
        this.pipeResolver = pipeResolver;
    }
    PipeProvider.prototype.createFromType = function (type) {
        var metadata = this.pipeResolver.resolve(type);
        if (!lang_1.isFunction(type.prototype.transform)) {
            throw new Error("@Pipe: must implement '#transform' method");
        }
        filterFactory.$inject = ['$injector'];
        function filterFactory($injector) {
            var pipeInstance = $injector.instantiate(type);
            // return angular 1 filter function
            var filterFn = pipeInstance.transform.bind(pipeInstance);
            if (metadata.pure === false) {
                filterFn.$stateful = true;
            }
            return filterFn;
        }
        return [
            metadata.name,
            filterFactory
        ];
    };
    return PipeProvider;
}());
exports.PipeProvider = PipeProvider;
/** @internal */
exports.pipeProvider = new PipeProvider(new pipe_resolver_1.PipeResolver());
//# sourceMappingURL=pipe_provider.js.map

/***/ }),
/* 205 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
var metadata_1 = __webpack_require__(45);
var reflection_1 = __webpack_require__(11);
var forward_ref_1 = __webpack_require__(20);
function _isPipeMetadata(type) {
    return type instanceof metadata_1.PipeMetadata;
}
/**
 * Resolve a `Type` for {@link PipeMetadata}.
 *
 */
var PipeResolver = (function () {
    function PipeResolver() {
    }
    /**
     * Return {@link PipeMetadata} for a given `Type`.
     */
    PipeResolver.prototype.resolve = function (type) {
        var metas = reflection_1.reflector.annotations(forward_ref_1.resolveForwardRef(type));
        if (lang_1.isPresent(metas)) {
            var annotation = collections_1.ListWrapper.find(metas, _isPipeMetadata);
            if (lang_1.isPresent(annotation)) {
                return annotation;
            }
        }
        throw new Error("No Pipe decorator found on " + lang_1.stringify(type));
    };
    return PipeResolver;
}());
exports.PipeResolver = PipeResolver;
//# sourceMappingURL=pipe_resolver.js.map

/***/ }),
/* 206 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var directive_resolver_1 = __webpack_require__(207);
var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
var directive_lifecycles_reflector_1 = __webpack_require__(208);
var directive_lifecycle_interfaces_1 = __webpack_require__(46);
var metadata_directives_1 = __webpack_require__(12);
var controller_factory_1 = __webpack_require__(209);
var children_resolver_1 = __webpack_require__(224);
var host_parser_1 = __webpack_require__(225);
var host_resolver_1 = __webpack_require__(226);
var directives_utils_1 = __webpack_require__(47);
/**
 * @internal
 */
var DirectiveProvider = (function () {
    function DirectiveProvider(directiveResolver) {
        this.directiveResolver = directiveResolver;
    }
    /**
     * creates directiveName and DirectiveFactory for angularJS container
     *
     * it produces directive for classes decorated with @Directive with following DDO:
     * ```
     * {
     * require: ['directiveName'],
     * controller: ClassDirective,
     * link: postLinkFn
     * }
     * ```
     *
     * it produces component for classes decorated with @Component with following DDO:
     * ```
     * {
     * require: ['directiveName'],
     * controller: ClassDirective,
     * controllerAs: '$ctrl',
     * template: 'component template string',
     * scope:{},
     * bindToController:{},
     * transclude: false,
     * link: postLinkFn
     * }
     * ```
     * @param type
     * @returns {string|function(): ng.IDirective[]}
     */
    DirectiveProvider.prototype.createFromType = function (type) {
        var metadata = this.directiveResolver.resolve(type);
        var directiveName = lang_1.resolveDirectiveNameFromSelector(metadata.selector);
        var requireMap = this.directiveResolver.getRequiredDirectivesMap(type);
        var lfHooks = directive_lifecycles_reflector_1.resolveImplementedLifeCycleHooks(type);
        var _ddo = {
            restrict: 'A',
            controller: _controller,
            link: {
                pre: function () { _ddo._ngOnInitBound(); },
                post: this._createLink(type, metadata, lfHooks)
            },
            // @TODO this will be removed after @Query handling is moved to directiveControllerFactory
            require: this._createRequires(requireMap, directiveName),
            _ngOnInitBound: lang_1.noop
        };
        // Component controllers must be created from a factory. Checkout out
        // util/directive-controller.js for more information about what's going on here
        _controller.$inject = ['$scope', '$element', '$attrs', '$transclude', '$injector'];
        function _controller($scope, $element, $attrs, $transclude, $injector) {
            var locals = { $scope: $scope, $element: $element, $attrs: $attrs, $transclude: $transclude };
            return controller_factory_1.directiveControllerFactory(this, type, $injector, locals, requireMap, _ddo, metadata);
        }
        // specific DDO augmentation for @Component
        if (metadata instanceof metadata_directives_1.ComponentMetadata) {
            var assetsPath = this.directiveResolver.parseAssetUrl(metadata);
            var componentSpecificDDO = {
                restrict: 'E',
                scope: {},
                bindToController: {},
                controllerAs: DirectiveProvider._controllerAs,
                transclude: DirectiveProvider._transclude
            };
            if (metadata.template && metadata.templateUrl) {
                throw new Error('cannot have both template and templateUrl');
            }
            if (metadata.template) {
                componentSpecificDDO.template = metadata.template;
            }
            if (metadata.templateUrl) {
                componentSpecificDDO.templateUrl = "" + assetsPath + metadata.templateUrl;
            }
            collections_1.StringMapWrapper.assign(_ddo, componentSpecificDDO);
        }
        // allow compile defined as static method on Type
        if (lang_1.isFunction(type.compile)) {
            _ddo.compile = function compile(tElement, tAttrs) {
                var linkFn = type.compile(tElement, tAttrs);
                // if user custom compile fn returns link use that one instead use generated
                return lang_1.isJsObject(linkFn)
                    ? linkFn
                    : this.link;
            };
        }
        // allow link defined as static method on Type override the created one
        // you should not use this very often
        // Note: if you use this any @Host property decorators or lifeCycle hooks wont work
        if (lang_1.isFunction(type.link)) {
            _ddo.link = type.link;
        }
        // legacy property overrides all generated DDO stuff
        var ddo = this._createDDO(_ddo, metadata.legacy);
        function directiveFactory() { return ddo; }
        // ==========================
        // ngComponentRouter Support:
        // ==========================
        // @TODO(pete) remove the following `forEach` before we release 1.6.0
        // The component-router@0.2.0 looks for the annotations on the controller constructor
        // Nothing in Angular looks for annotations on the factory function but we can't remove
        // it from 1.5.x yet.
        // Copy any annotation properties (starting with $) over to the factory and controller constructor functions
        // These could be used by libraries such as the new component router
        collections_1.StringMapWrapper.forEach(ddo, function (val, key) {
            if (key.charAt(0) === '$') {
                directiveFactory[key] = val;
                // Don't try to copy over annotations to named controller
                if (lang_1.isFunction(ddo.controller)) {
                    ddo.controller[key] = val;
                }
            }
        });
        // support componentRouter $canActivate lc hook as static instead of defined within legacy object
        // componentRouter reads all lc hooks from directiveFactory ¯\_(ツ)_/¯
        // @TODO update this when new component router will be available for Angular 1 ( 1.6 release probably )
        if (lang_1.isFunction(type.$canActivate)) {
            directiveFactory.$canActivate = type.$canActivate;
        }
        return [directiveName, directiveFactory];
    };
    DirectiveProvider.prototype._createDDO = function (ddo, legacyDDO) {
        return lang_1.assign({}, DirectiveProvider._ddoShell, ddo, legacyDDO);
    };
    /**
     *
     * @param requireMap
     * @param directiveName
     * @returns {Array}
     * @private
     * @internal
     */
    DirectiveProvider.prototype._createRequires = function (requireMap, directiveName) {
        return [directiveName].concat(collections_1.StringMapWrapper.values(requireMap));
    };
    /**
     * Directive lifeCycles:
     * - ngOnInit from preLink (all children compiled and DOM ready)
     * - ngAfterContentInit from postLink ( DOM in children ready )
     * - ngOnDestroy from postLink
     *
     * Component lifeCycles:
     * - ngOnInit from preLink (controller require ready)
     * - ngAfterViewInit from postLink ( all children in view+content compiled and DOM ready )
     * - ngAfterContentInit from postLink ( same as ngAfterViewInit )
     * - ngOnDestroy from postLink
     * @param type
     * @param metadata
     * @param lfHooks
     * @private
     * @internal
     */
    DirectiveProvider.prototype._createLink = function (type, metadata, lfHooks) {
        if ((lfHooks.ngAfterContentChecked || lfHooks.ngAfterViewChecked) && collections_1.StringMapWrapper.size(metadata.queries) === 0) {
            throw new Error("\n              Hooks Impl for " + lang_1.stringify(type) + ":\n              ===================================\n              You've implement AfterContentChecked/AfterViewChecked lifecycle, but @ViewChild(ren)/@ContentChild(ren) decorators are not used.\n              we cannot invoke After(Content|View)Checked without provided @Query decorators\n              ");
        }
        if (metadata instanceof metadata_directives_1.ComponentMetadata) {
            if ((lfHooks.ngAfterContentInit || lfHooks.ngAfterContentChecked) && !collections_1.StringMapWrapper.getValueFromPath(metadata, 'legacy.transclude')) {
                throw new Error("\n              Hooks Impl for " + lang_1.stringify(type) + ":\n              ===================================\n              You cannot implement AfterContentInit lifecycle, without allowed transclusion.\n              turn transclusion on within decorator like this: @Component({legacy:{transclude:true}})\n              ");
            }
        }
        // we need to implement this if query are present on class, because during postLink _ngOnChildrenChanged is not yet
        // implemented on controller instance
        if (collections_1.StringMapWrapper.size(metadata.queries)) {
            type.prototype._ngOnChildrenChanged = lang_1.noop;
        }
        var hostProcessed = host_parser_1._parseHost(metadata.host);
        return postLink;
        function postLink(scope, element, attrs, controller, transclude) {
            var _watchers = [];
            var ctrl = controller[0], requiredCtrls = controller.slice(1);
            host_resolver_1._setHostStaticAttributes(element, hostProcessed.hostStatic);
            // setup @HostBindings
            _watchers.push.apply(_watchers, host_resolver_1._setHostBindings(scope, element, ctrl, hostProcessed.hostBindings));
            // setup @HostListeners
            host_resolver_1._setHostListeners(scope, element, ctrl, hostProcessed.hostListeners);
            // @ContentChild/@ContentChildren/@ViewChild/@ViewChildren related logic
            var parentCheckedNotifiers = children_resolver_1._getParentCheckNotifiers(ctrl, requiredCtrls);
            _watchers.push.apply(_watchers, parentCheckedNotifiers);
            children_resolver_1._setupQuery(scope, element, ctrl, metadata.queries);
            // AfterContentInit/AfterViewInit Hooks
            // if there are query defined schedule $evalAsync semaphore
            if (collections_1.StringMapWrapper.size(metadata.queries)) {
                ctrl._ngOnChildrenChanged(directive_lifecycle_interfaces_1.ChildrenChangeHook.FromView, [
                    parentCheckedNotifiers.forEach(function (cb) { return cb(); }),
                    ctrl.ngAfterViewInit && ctrl.ngAfterViewInit.bind(ctrl),
                    ctrl.ngAfterViewChecked && ctrl.ngAfterViewChecked.bind(ctrl),
                ]);
                ctrl._ngOnChildrenChanged(directive_lifecycle_interfaces_1.ChildrenChangeHook.FromContent, [
                    parentCheckedNotifiers.forEach(function (cb) { return cb(); }),
                    ctrl.ngAfterContentInit && ctrl.ngAfterContentInit.bind(ctrl),
                    ctrl.ngAfterContentChecked && ctrl.ngAfterContentChecked.bind(ctrl),
                ]);
            }
            else {
                // no @ContentChild/@ViewChild(ref) decorators exist, call just controller init method
                parentCheckedNotifiers.forEach(function (cb) { return cb(); });
                ctrl.ngAfterViewInit && ctrl.ngAfterViewInit();
                ctrl.ngAfterContentInit && ctrl.ngAfterContentInit();
            }
            directives_utils_1._setupDestroyHandler(scope, element, ctrl, lfHooks.ngOnDestroy, _watchers);
        }
    };
    return DirectiveProvider;
}());
DirectiveProvider._ddoShell = {
    require: [],
    controller: lang_1.noop,
    link: { pre: lang_1.noop, post: lang_1.noop }
};
DirectiveProvider._controllerAs = "$ctrl";
DirectiveProvider._transclude = false;
exports.DirectiveProvider = DirectiveProvider;
exports.directiveProvider = new DirectiveProvider(new directive_resolver_1.DirectiveResolver());
//# sourceMappingURL=directive_provider.js.map

/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
var reflection_1 = __webpack_require__(11);
var metadata_directives_1 = __webpack_require__(12);
var metadata_di_1 = __webpack_require__(30);
var metadata_1 = __webpack_require__(9);
var provider_1 = __webpack_require__(15);
var forward_ref_1 = __webpack_require__(20);
var exceptions_1 = __webpack_require__(75);
// asset:<package-name>/<realm>/<path-to-module>
// var _ASSET_URL_RE = /asset:([^\/]+)\/([^\/]+)\/(.+)/g;
// <path-to-module>/filename.js
var ASSET_URL_RE = /^(.+)\/.+\.js$/;
function _isDirectiveMetadata(type) {
    return type instanceof metadata_directives_1.DirectiveMetadata;
}
/**
 * return required string map for provided local DI
 * ```typescript
 * // for
 * constructor(@Inject('ngModel') @Self() @Optional() ngModel){}
 * // it returns:
 * { ngModel: '?ngModel' }
 *
 * // when MyComponent is
 * @Component({ selector: 'myCoolCmp', template:`hello`})
 * class MyComponent{}
 * // for
 * constructor(@Host() @Optional() myCmp: MyComponent){}
 * // it returns:
 * { myCmp: '^myCoolCmp' }
 * ```
 * @param paramsMeta
 * @param idx
 * @param typeOrFunc
 * @returns {{[directiveName]:string}}
 * @private
 */
function _transformInjectedDirectivesMeta(paramsMeta, idx, typeOrFunc) {
    if (!_isInjectableParamsDirective(paramsMeta)) {
        return;
    }
    // @TODO unite this with _extractToken from provider.ts
    var injectInst = collections_1.ListWrapper.find(paramsMeta, function (param) { return param instanceof metadata_1.InjectMetadata; });
    var injectType = collections_1.ListWrapper.find(paramsMeta, lang_1.isType);
    var _a = (injectInst || { token: injectType }).token, token = _a === void 0 ? undefined : _a;
    // we need to decrement param count if user uses both @Inject() and :MyType
    var paramsMetaLength = (injectInst && injectType)
        ? paramsMeta.length - 1
        : paramsMeta.length;
    if (!token) {
        throw new Error(exceptions_1.getErrorMsg(typeOrFunc, "no Directive instance name provided within @Inject() or :DirectiveClass annotation missing"));
    }
    var isHost = collections_1.ListWrapper.findIndex(paramsMeta, function (param) { return param instanceof metadata_1.HostMetadata; }) !== -1;
    var isOptional = collections_1.ListWrapper.findIndex(paramsMeta, function (param) { return param instanceof metadata_1.OptionalMetadata; }) !== -1;
    var isSelf = collections_1.ListWrapper.findIndex(paramsMeta, function (param) { return param instanceof metadata_1.SelfMetadata; }) !== -1;
    var isSkipSelf = collections_1.ListWrapper.findIndex(paramsMeta, function (param) { return param instanceof metadata_1.SkipSelfMetadata; }) !== -1;
    if (isOptional && paramsMetaLength !== 3) {
        throw new Error(exceptions_1.getErrorMsg(typeOrFunc, "you cannot use @Optional() without related decorator for injecting Directives. use one of @Host|@Self()|@SkipSelf() + @Optional()"));
    }
    if (isSelf && isSkipSelf) {
        throw new Error(exceptions_1.getErrorMsg(typeOrFunc, "you cannot provide both @Self() and @SkipSelf() with @Inject(" + lang_1.getFuncName(token) + ") for Directive Injection"));
    }
    if ((isHost && isSelf) || (isHost && isSkipSelf)) {
        throw new Error(exceptions_1.getErrorMsg(typeOrFunc, "you cannot provide both @Host(),@SkipSelf() or @Host(),@Self() with @Inject(" + lang_1.getFuncName(token) + ") for Directive Injections"));
    }
    var locateType = _getLocateTypeSymbol();
    var optionalType = isOptional ? '?' : '';
    var requireExpressionPrefix = "" + optionalType + locateType;
    var directiveName = _getDirectiveName(token);
    // we need to generate unique names because if we require same directive controllers,
    // with different locale decorators it would merge to one which is wrong
    return _b = {},
        _b[directiveName + "#" + idx] = "" + requireExpressionPrefix + directiveName,
        _b;
    function _getDirectiveName(token) {
        return lang_1.isType(forward_ref_1.resolveForwardRef(token))
            ? provider_1.getInjectableName(forward_ref_1.resolveForwardRef(token))
            : token;
    }
    function _getLocateTypeSymbol() {
        if (isSelf) {
            return '';
        }
        if (isHost) {
            return '^';
        }
        if (isSkipSelf) {
            return '^^';
        }
    }
    // exit if user uses both @Inject() and :Type for DI because this is not directive injection
    function _isInjectableParamsDirective(paramsMeta) {
        // if there is just @Inject or Type from design:paramtypes return
        if (paramsMeta.length < 2) {
            return false;
        }
        if (paramsMeta.length === 2) {
            var injectableParamCount = paramsMeta.filter(function (inj) { return inj instanceof metadata_1.InjectMetadata || lang_1.isType(inj); }).length;
            if (injectableParamCount === 2) {
                return false;
            }
        }
        return true;
    }
    var _b;
}
/**
 * Resolve a `Type` for {@link DirectiveMetadata}.
 */
var DirectiveResolver = (function () {
    function DirectiveResolver() {
    }
    /**
     * Return {@link DirectiveMetadata} for a given `Type`.
     */
    DirectiveResolver.prototype.resolve = function (type) {
        var metadata = this._getDirectiveMeta(type);
        var propertyMetadata = reflection_1.reflector.propMetadata(type);
        return this._mergeWithPropertyMetadata(metadata, propertyMetadata);
    };
    /**
     * transform parameter annotations to required directives map so we can use it
     * for DDO creation
     *
     * map consist of :
     *  - key == name of directive
     *  - value == Angular 1 require expression
     *  ```js
     *  {
     *    ngModel: 'ngModel',
     *    form: '^^form',
     *    foo: '^foo',
     *    moo: '?^foo',
     *  }
     *  ```
     *
     * @param {Type} type
     * @returns {StringMap}
     */
    DirectiveResolver.prototype.getRequiredDirectivesMap = function (type) {
        var metadata = this._getDirectiveMeta(type);
        var paramMetadata = reflection_1.reflector.parameters(type);
        if (lang_1.isPresent(paramMetadata)) {
            return paramMetadata
                .reduce(function (acc, paramMetaArr, idx) {
                var requireExp = _transformInjectedDirectivesMeta(paramMetaArr, idx, type);
                if (lang_1.isPresent(requireExp)) {
                    lang_1.assign(acc, requireExp);
                }
                return acc;
            }, {});
        }
        return {};
    };
    DirectiveResolver.prototype.parseAssetUrl = function (cmpMetadata) {
        if (lang_1.isBlank(cmpMetadata.moduleId)) {
            return '';
        }
        var moduleId = cmpMetadata.moduleId;
        var _a = moduleId.match(ASSET_URL_RE) || [], _b = _a[1], urlPathMatch = _b === void 0 ? '' : _b;
        return urlPathMatch + "/";
    };
    /**
     *
     * @param type
     * @returns {DirectiveMetadata}
     * @throws Error
     * @private
     */
    DirectiveResolver.prototype._getDirectiveMeta = function (type) {
        var typeMetadata = reflection_1.reflector.annotations(forward_ref_1.resolveForwardRef(type));
        if (lang_1.isPresent(typeMetadata)) {
            var metadata = collections_1.ListWrapper.find(typeMetadata, _isDirectiveMetadata);
            if (lang_1.isPresent(metadata)) {
                return metadata;
            }
        }
        throw new Error("No Directive annotation found on " + lang_1.stringify(type));
    };
    DirectiveResolver.prototype._mergeWithPropertyMetadata = function (directiveMetadata, propertyMetadata) {
        var inputs = [];
        var attrs = [];
        var outputs = [];
        var host = {};
        var queries = {};
        collections_1.StringMapWrapper.forEach(propertyMetadata, function (metadata, propName) {
            metadata.forEach(function (propMetaInst) {
                if (propMetaInst instanceof metadata_directives_1.InputMetadata) {
                    if (lang_1.isPresent(propMetaInst.bindingPropertyName)) {
                        inputs.push(propName + ": " + propMetaInst.bindingPropertyName);
                    }
                    else {
                        inputs.push(propName);
                    }
                }
                if (propMetaInst instanceof metadata_directives_1.AttrMetadata) {
                    if (lang_1.isPresent(propMetaInst.bindingPropertyName)) {
                        attrs.push(propName + ": " + propMetaInst.bindingPropertyName);
                    }
                    else {
                        attrs.push(propName);
                    }
                }
                if (propMetaInst instanceof metadata_directives_1.OutputMetadata) {
                    if (lang_1.isPresent(propMetaInst.bindingPropertyName)) {
                        outputs.push(propName + ": " + propMetaInst.bindingPropertyName);
                    }
                    else {
                        outputs.push(propName);
                    }
                }
                if (propMetaInst instanceof metadata_directives_1.HostBindingMetadata) {
                    if (lang_1.isPresent(propMetaInst.hostPropertyName)) {
                        host["[" + propMetaInst.hostPropertyName + "]"] = propName;
                    }
                    else {
                        host["[" + propName + "]"] = propName;
                    }
                }
                if (propMetaInst instanceof metadata_directives_1.HostListenerMetadata) {
                    var args = lang_1.isPresent(propMetaInst.args)
                        ? propMetaInst.args.join(', ')
                        : '';
                    host["(" + propMetaInst.eventName + ")"] = propName + "(" + args + ")";
                }
                if (propMetaInst instanceof metadata_di_1.ContentChildrenMetadata) {
                    queries[propName] = propMetaInst;
                }
                if (propMetaInst instanceof metadata_di_1.ViewChildrenMetadata) {
                    queries[propName] = propMetaInst;
                }
                if (propMetaInst instanceof metadata_di_1.ContentChildMetadata) {
                    queries[propName] = propMetaInst;
                }
                if (propMetaInst instanceof metadata_di_1.ViewChildMetadata) {
                    queries[propName] = propMetaInst;
                }
            });
        });
        return this._merge(directiveMetadata, inputs, attrs, outputs, host, queries);
    };
    DirectiveResolver.prototype._merge = function (dm, inputs, attrs, outputs, host, queries) {
        var mergedInputs = lang_1.isPresent(dm.inputs)
            ? collections_1.ListWrapper.concat(dm.inputs, inputs)
            : inputs;
        var mergedAttrs = lang_1.isPresent(dm.attrs)
            ? collections_1.ListWrapper.concat(dm.attrs, attrs)
            : attrs;
        var mergedOutputs = lang_1.isPresent(dm.outputs)
            ? collections_1.ListWrapper.concat(dm.outputs, outputs)
            : outputs;
        var mergedHost = lang_1.isPresent(dm.host)
            ? collections_1.StringMapWrapper.merge(dm.host, host)
            : host;
        var mergedQueries = lang_1.isPresent(dm.queries)
            ? collections_1.StringMapWrapper.merge(dm.queries, queries)
            : queries;
        var directiveSettings = {
            selector: dm.selector,
            inputs: mergedInputs,
            attrs: mergedAttrs,
            outputs: mergedOutputs,
            host: mergedHost,
            queries: mergedQueries,
            legacy: dm.legacy
        };
        if (dm instanceof metadata_directives_1.ComponentMetadata) {
            var componentSettings = collections_1.StringMapWrapper.assign({}, directiveSettings, {
                moduleId: dm.moduleId,
                template: dm.template,
                templateUrl: dm.templateUrl,
                changeDetection: lang_1.isPresent(dm.changeDetection) ? dm.changeDetection : 1 /* Default */
            });
            return new metadata_directives_1.ComponentMetadata(componentSettings);
        }
        else {
            return new metadata_directives_1.DirectiveMetadata(directiveSettings);
        }
    };
    return DirectiveResolver;
}());
exports.DirectiveResolver = DirectiveResolver;
//# sourceMappingURL=directive_resolver.js.map

/***/ }),
/* 208 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var type_1 = __webpack_require__(76);
var directive_lifecycle_interfaces_1 = __webpack_require__(46);
function hasLifecycleHook(lcInterface, token) {
    if (!(token instanceof type_1.Type))
        return false;
    var proto = token.prototype;
    switch (lcInterface) {
        case directive_lifecycle_interfaces_1.LifecycleHooks.AfterContentInit:
            return !!proto.ngAfterContentInit;
        case directive_lifecycle_interfaces_1.LifecycleHooks.AfterContentChecked:
            return !!proto.ngAfterContentChecked;
        case directive_lifecycle_interfaces_1.LifecycleHooks.AfterViewInit:
            return !!proto.ngAfterViewInit;
        case directive_lifecycle_interfaces_1.LifecycleHooks.AfterViewChecked:
            return !!proto.ngAfterViewChecked;
        case directive_lifecycle_interfaces_1.LifecycleHooks.OnDestroy:
            return !!proto.ngOnDestroy;
        case directive_lifecycle_interfaces_1.LifecycleHooks.OnInit:
            return !!proto.ngOnInit;
        case directive_lifecycle_interfaces_1.LifecycleHooks.OnChanges:
            return !!proto.ngOnChanges;
        case directive_lifecycle_interfaces_1.LifecycleHooks.DoCheck:
            return !!proto.ngDoCheck;
        case directive_lifecycle_interfaces_1.LifecycleHooks._OnChildrenChanged:
            return !!proto._ngOnChildrenChanged;
        default:
            return false;
    }
}
exports.hasLifecycleHook = hasLifecycleHook;
/**
 * @internal
 */
function resolveImplementedLifeCycleHooks(type) {
    return {
        ngOnInit: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.OnInit, type),
        ngOnChanges: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.OnChanges, type),
        ngDoCheck: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.DoCheck, type),
        ngAfterContentInit: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.AfterContentInit, type),
        ngAfterContentChecked: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.AfterContentChecked, type),
        ngAfterViewInit: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.AfterViewInit, type),
        ngAfterViewChecked: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.AfterViewChecked, type),
        ngOnDestroy: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks.OnDestroy, type),
        _ngOnChildrenChanged: hasLifecycleHook(directive_lifecycle_interfaces_1.LifecycleHooks._OnChildrenChanged, type)
    };
}
exports.resolveImplementedLifeCycleHooks = resolveImplementedLifeCycleHooks;
//# sourceMappingURL=directive_lifecycles_reflector.js.map

/***/ }),
/* 209 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var collections_1 = __webpack_require__(3);
var change_detector_ref_1 = __webpack_require__(77);
var binding_factory_1 = __webpack_require__(210);
var directives_utils_1 = __webpack_require__(47);
var lang_1 = __webpack_require__(1);
var primitives_1 = __webpack_require__(85);
var constants_1 = __webpack_require__(223);
function directiveControllerFactory(caller, controller, $injector, locals, requireMap, _ddo, metadata) {
    var $scope = locals.$scope, $element = locals.$element, $attrs = locals.$attrs;
    var _services = {
        $parse: $injector.get('$parse'),
        $interpolate: $injector.get('$interpolate'),
        $rootScope: $injector.get('$rootScope')
    };
    var _localServices = {
        changeDetectorRef: change_detector_ref_1.ChangeDetectorRef.create($scope)
    };
    // Create an instance of the controller without calling its constructor
    var instance = Object.create(controller.prototype);
    // NOTE: this is not needed because we are creating bindings manually because of
    // angular behaviour https://github.com/ngParty/ng-metadata/issues/53
    // ===================================================================
    // Remember, angular has already set those bindings on the `caller`
    // argument. Now we need to extend them onto our `instance`. It is important
    // to extend after defining the properties. That way we fire the setters.
    //
    // StringMapWrapper.assign( instance, caller );
    // setup @Input/@Output/@Attrs for @Component/@Directive
    var _a = binding_factory_1._createDirectiveBindings(!directives_utils_1.isAttrDirective(metadata), $scope, $attrs, instance, metadata, _services), removeWatches = _a.removeWatches, initialChanges = _a.initialChanges;
    cleanUpWatchers(removeWatches);
    // change injectables to proper inject directives
    // we wanna do this only if we inject some locals/directives
    if (collections_1.StringMapWrapper.size(requireMap)) {
        controller.$inject = createNewInjectablesToMatchLocalDi(controller.$inject, requireMap);
    }
    var $requires = getEmptyRequiredControllers(requireMap);
    // $injector.invoke will delete any @Input/@Attr/@Output which were resolved within _createDirectiveBindings
    // and which have set default values in constructor. We need to store them and reassign after this invoke
    var initialInstanceBindingValues = getInitialBindings(instance);
    // Finally, invoke the constructor using the injection array and the captured locals
    $injector.invoke(controller, instance, collections_1.StringMapWrapper.assign(locals, _localServices, $requires));
    // reassign back the initial binding values, just in case if we used default values
    collections_1.StringMapWrapper.assign(instance, initialInstanceBindingValues);
    /*if ( isFunction( instance.ngOnDestroy ) ) {
     $scope.$on( '$destroy', instance.ngOnDestroy.bind( instance ) );
     }*/
    /*if (typeof instance.ngAfterViewInit === 'function') {
     ddo.ngAfterViewInitBound = instance.ngAfterViewInit.bind(instance);
     }*/
    // https://github.com/angular/angular.js/commit/0ad2b70862d49ecc4355a16d767c0ca9358ecc3e
    // onChanges is called before onInit
    if (lang_1.isFunction(instance.ngOnChanges)) {
        instance.ngOnChanges(initialChanges);
    }
    _ddo._ngOnInitBound = function _ngOnInitBound() {
        // invoke again only if there are any directive requires
        // #perf
        if (collections_1.StringMapWrapper.size(requireMap)) {
            var $requires_1 = getRequiredControllers(requireMap, $element, controller);
            // $injector.invoke will delete any @Input/@Attr/@Output which were resolved within _createDirectiveBindings
            // and which have set default values in constructor. We need to store them and reassign after this invoke
            var initialInstanceBindingValues_1 = getInitialBindings(instance);
            $injector.invoke(controller, instance, collections_1.StringMapWrapper.assign(locals, _localServices, $requires_1));
            // reassign back the initial binding values, just in case if we used default values
            collections_1.StringMapWrapper.assign(instance, initialInstanceBindingValues_1);
        }
        if (lang_1.isFunction(instance.ngOnInit)) {
            instance.ngOnInit();
        }
        // DoCheck is called after OnChanges and OnInit
        if (lang_1.isFunction(instance.ngDoCheck)) {
            var removeDoCheckWatcher = $postDigestWatch($scope, function () { return instance.ngDoCheck(); });
            cleanUpWatchers(removeDoCheckWatcher);
        }
    };
    // Return the controller instance
    return instance;
    function cleanUpWatchers(cb) {
        $scope.$on('$destroy', function () { return cb; });
    }
}
exports.directiveControllerFactory = directiveControllerFactory;
/**
 * Note: $$postDigest will not trigger another digest cycle.
 * So any modification to $scope inside $$postDigest will not get reflected in the DOM
 */
function $postDigestWatch(scope, cb) {
    var hasRegistered = false;
    var removeDoCheckWatcher = scope.$watch(function () {
        if (hasRegistered) {
            return;
        }
        hasRegistered = true;
        scope.$$postDigest(function () {
            hasRegistered = false;
            cb();
        });
    });
    return removeDoCheckWatcher;
}
function getInitialBindings(instance) {
    var initialBindingValues = {};
    collections_1.StringMapWrapper.forEach(instance, function (value, propName) {
        if (instance[propName]) {
            initialBindingValues[propName] = value;
        }
    });
    return initialBindingValues;
}
/**
 * Angular 1 copy of how to require other directives
 * @param require
 * @param $element
 * @param directive
 * @returns {any|null}
 */
function getRequiredControllers(require, $element, directive) {
    var value;
    if (lang_1.isString(require)) {
        var match = require.match(constants_1.REQUIRE_PREFIX_REGEXP);
        var name = require.substring(match[0].length);
        var inheritType = match[1] || match[3];
        var optional = match[2] === '?';
        //If only parents then start at the parent element
        if (inheritType === '^^') {
            $element = $element.parent();
        }
        if (!value) {
            var dataName = "$" + name + "Controller";
            value = inheritType ? $element.inheritedData(dataName) : $element.data(dataName);
        }
        if (!value && !optional) {
            throw new Error("Directive/Controller '" + name + "', required by directive '" + lang_1.getFuncName(directive) + "', can't be found!");
        }
    }
    else if (lang_1.isArray(require)) {
        value = [];
        for (var i = 0, ii = require.length; i < ii; i++) {
            value[i] = getRequiredControllers(require[i], $element, directive);
        }
    }
    else if (lang_1.isJsObject(require)) {
        value = {};
        collections_1.StringMapWrapper.forEach(require, function (controller, property) {
            value[property] = getRequiredControllers(controller, $element, directive);
        });
    }
    return value || null;
}
exports.getRequiredControllers = getRequiredControllers;
function getEmptyRequiredControllers(requireMap) {
    return collections_1.StringMapWrapper.keys(requireMap).reduce(function (acc, keyName) {
        acc[keyName] = undefined;
        return acc;
    }, {});
}
exports.getEmptyRequiredControllers = getEmptyRequiredControllers;
function createNewInjectablesToMatchLocalDi(originalInjectables, requireMap) {
    var requireKeys = collections_1.StringMapWrapper.keys(requireMap);
    return originalInjectables.slice()
        .map(function (injectable) {
        var replaceInjName = requireKeys
            .filter(function (keyName) { return primitives_1.StringWrapper.startsWith(keyName, injectable); })[0];
        // if found remove that key so we won't assign the same
        if (replaceInjName) {
            var idx = requireKeys.indexOf(replaceInjName);
            requireKeys.splice(idx, 1);
        }
        return replaceInjName || injectable;
    });
}
exports.createNewInjectablesToMatchLocalDi = createNewInjectablesToMatchLocalDi;
/**
 * @deprecated
 * @TODO remove?
 */
function injectionArgs(fn, locals, serviceName, injects) {
    var args = [], 
    // $inject = createInjector.$$annotate(fn, strictDi, serviceName);
    $inject = injects;
    for (var i = 0, length = $inject.length; i < length; i++) {
        var key = $inject[i];
        if (typeof key !== 'string') {
            throw new Error("itkn, Incorrect injection token! Expected service name as string, got " + key);
        }
        /*    args.push( locals && locals.hasOwnProperty( key )
         ? locals[ key ]
         : getService( key, serviceName ) );*/
        args.push(locals[key]);
    }
    return args;
}
//# sourceMappingURL=controller_factory.js.map

/***/ }),
/* 210 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var directives_utils_1 = __webpack_require__(47);
var change_detection_util_1 = __webpack_require__(78);
var changes_queue_1 = __webpack_require__(211);
var collections_1 = __webpack_require__(3);
var lang_1 = __webpack_require__(1);
var async_1 = __webpack_require__(79);
var constants_1 = __webpack_require__(84);
var binding_parser_1 = __webpack_require__(222);
/**
 * Create Bindings manually for both Directive/Component
 * @returns {{watchers: Array, observers: Array}}
 * @internal
 * @private
 */
function _createDirectiveBindings(hasIsolateScope, ngScope, ngAttrs, ctrl, metadata, _a) {
    /*  let BOOLEAN_ATTR = {};
     'multiple,selected,checked,disabled,readOnly,required,open'
     .split(',')
     .forEach(function(value) {
     BOOLEAN_ATTR[value.toLocaleLowerCase()] = value;
     });*/
    var $interpolate = _a.$interpolate, $parse = _a.$parse, $rootScope = _a.$rootScope;
    var isBindingImmutable = directives_utils_1.isComponentDirective(metadata) && change_detection_util_1.ChangeDetectionUtil.isOnPushChangeDetectionStrategy(metadata.changeDetection);
    var scope = hasIsolateScope
        ? ngScope.$parent
        : ngScope;
    var _b = metadata.inputs, inputs = _b === void 0 ? [] : _b, _c = metadata.outputs, outputs = _c === void 0 ? [] : _c;
    var parsedBindings = binding_parser_1.setupFields(ngAttrs, inputs, outputs);
    var _internalWatchers = [];
    var _internalObservers = [];
    // onChanges tmp vars
    var initialChanges = {};
    var changes;
    // this will create flush queue internally only once
    // we need to call this here because we need $rootScope service
    changes_queue_1.changesQueueService.buildFlushOnChanges($rootScope);
    // setup @Inputs '<' or '='
    collections_1.StringMapWrapper.forEach(parsedBindings.inputs, function (config, propName) {
        var exp = config.exp, attrName = config.attrName, mode = config.mode;
        // support for TWO_WAY only for components
        var hasTwoWayBinding = hasIsolateScope && mode === constants_1.BINDING_MODE.twoWay;
        var removeWatch = hasTwoWayBinding
            ? _createTwoWayBinding(propName, attrName, exp)
            : _createOneWayBinding(propName, attrName, exp, isBindingImmutable);
        _internalWatchers.push(removeWatch);
    });
    // setup @Input('@')
    collections_1.StringMapWrapper.forEach(parsedBindings.attrs, function (config, propName) {
        var attrName = config.attrName, exp = config.exp, mode = config.mode;
        var removeObserver = _createAttrBinding(propName, attrName, exp);
        _internalObservers.push(removeObserver);
    });
    // setup @Outputs
    collections_1.StringMapWrapper.forEach(parsedBindings.outputs, function (config, propName) {
        var exp = config.exp, attrName = config.attrName, mode = config.mode;
        _createOutputBinding(propName, attrName, exp);
    });
    function _createOneWayBinding(propName, attrName, exp, isImmutable) {
        if (isImmutable === void 0) { isImmutable = false; }
        if (!exp)
            return;
        var parentGet = $parse(exp);
        var initialValue = ctrl[propName] = parentGet(scope);
        initialChanges[propName] = change_detection_util_1.ChangeDetectionUtil.simpleChange(change_detection_util_1.ChangeDetectionUtil.uninitialized, ctrl[propName]);
        return scope.$watch(parentGet, function parentValueWatchAction(newValue, oldValue) {
            // https://github.com/angular/angular.js/commit/d9448dcb9f901ceb04deda1d5f3d5aac8442a718
            // https://github.com/angular/angular.js/commit/304796471292f9805b9cf77e51aacc9cfbb09921
            if (oldValue === newValue) {
                if (oldValue === initialValue)
                    return;
                oldValue = initialValue;
            }
            recordChanges(propName, newValue, oldValue);
            ctrl[propName] = isImmutable ? lang_1.global.angular.copy(newValue) : newValue;
        }, parentGet.literal);
    }
    function _createTwoWayBinding(propName, attrName, exp) {
        if (!exp)
            return;
        var lastValue;
        var parentGet = $parse(exp);
        var parentSet = parentGet.assign || function () {
            // reset the change, or we will throw this exception on every $digest
            lastValue = ctrl[propName] = parentGet(scope);
            throw new Error("nonassign,\n          Expression '" + ngAttrs[attrName] + "' in attribute '" + attrName + "' used with directive '{2}' is non-assignable!");
        };
        var compare = parentGet.literal ? lang_1.global.angular.equals : simpleCompare;
        var parentValueWatch = function parentValueWatch(parentValue) {
            if (!compare(parentValue, ctrl[propName])) {
                // we are out of sync and need to copy
                if (!compare(parentValue, lastValue)) {
                    // parent changed and it has precedence
                    ctrl[propName] = parentValue;
                }
                else {
                    // if the parent can be assigned then do so
                    parentSet(scope, parentValue = ctrl[propName]);
                }
            }
            return lastValue = parentValue;
        };
        parentValueWatch.$stateful = true;
        lastValue = ctrl[propName] = parentGet(scope);
        // NOTE: we don't support collection watch, it's not good for performance
        // if (definition.collection) {
        //   removeWatch = scope.$watchCollection(attributes[attrName], parentValueWatch);
        // } else {
        //   removeWatch = scope.$watch($parse(attributes[attrName], parentValueWatch), null, parentGet.literal);
        // }
        // removeWatchCollection.push(removeWatch);
        return scope.$watch(
        // $parse( ngAttrs[ attrName ], parentValueWatch ),
        $parse(exp, parentValueWatch), null, parentGet.literal);
        function simpleCompare(a, b) { return a === b || (a !== a && b !== b); }
    }
    function _createOutputBinding(propName, attrName, exp) {
        // Don't assign Object.prototype method to scope
        var parentGet = exp
            ? $parse(exp)
            : lang_1.noop;
        // Don't assign noop to ctrl if expression is not valid
        if (parentGet === lang_1.noop)
            return;
        // here we assign property to EventEmitter instance directly
        var emitter = new async_1.EventEmitter();
        emitter.wrapNgExpBindingToEmitter(function _exprBindingCb(locals) {
            return parentGet(scope, locals);
        });
        ctrl[propName] = emitter;
    }
    function _createAttrBinding(propName, attrName, exp) {
        var lastValue = exp;
        // register watchers for further changes
        // The observer function will be invoked once during the next $digest following compilation.
        // The observer is then invoked whenever the interpolated value changes.
        var _disposeObserver = ngAttrs.$observe(attrName, function (value) {
            // https://github.com/angular/angular.js/commit/499e1b2adf27f32d671123f8dceadb3df2ad84a9
            if (lang_1.isString(value) || lang_1.isBoolean(value)) {
                var oldValue = ctrl[propName];
                recordChanges(propName, value, oldValue);
                ctrl[propName] = value;
            }
        });
        ngAttrs.$$observers[attrName].$$scope = scope;
        if (lang_1.isString(lastValue)) {
            // If the attribute has been provided then we trigger an interpolation to ensure
            // the value is there for use in the link fn
            ctrl[propName] = $interpolate(lastValue)(scope);
        }
        else if (lang_1.isBoolean(lastValue)) {
            // If the attributes is one of the BOOLEAN_ATTR then Angular will have converted
            // the value to boolean rather than a string, so we special case this situation
            ctrl[propName] = lastValue;
        }
        initialChanges[propName] = change_detection_util_1.ChangeDetectionUtil.simpleChange(change_detection_util_1.ChangeDetectionUtil.uninitialized, ctrl[propName]);
        return _disposeObserver;
    }
    function recordChanges(key, currentValue, previousValue) {
        if (lang_1.isFunction(ctrl.ngOnChanges) && currentValue !== previousValue) {
            // If we have not already scheduled the top level onChangesQueue handler then do so now
            if (!changes_queue_1.changesQueueService.onChangesQueue) {
                scope.$$postDigest(changes_queue_1.changesQueueService.flushOnChangesQueue);
                changes_queue_1.changesQueueService.onChangesQueue = [];
            }
            // If we have not already queued a trigger of onChanges for this controller then do so now
            if (!changes) {
                changes = {};
                changes_queue_1.changesQueueService.onChangesQueue.push(triggerOnChangesHook);
            }
            // If the has been a change on this property already then we need to reuse the previous value
            if (changes[key]) {
                previousValue = changes[key].previousValue;
            }
            // Store this change
            changes[key] = change_detection_util_1.ChangeDetectionUtil.simpleChange(previousValue, currentValue);
        }
    }
    function triggerOnChangesHook() {
        ctrl.ngOnChanges(changes);
        // Now clear the changes so that we schedule onChanges when more changes arrive
        changes = undefined;
    }
    function removeWatches() {
        var removeWatchCollection = _internalWatchers.concat(_internalObservers);
        for (var i = 0, ii = removeWatchCollection.length; i < ii; ++i) {
            if (removeWatchCollection[i] && lang_1.isFunction(removeWatchCollection[i])) {
                removeWatchCollection[i]();
            }
        }
    }
    return {
        initialChanges: initialChanges,
        removeWatches: removeWatches,
        _watchers: { watchers: _internalWatchers, observers: _internalObservers }
    };
}
exports._createDirectiveBindings = _createDirectiveBindings;
//# sourceMappingURL=binding_factory.js.map

/***/ }),
/* 211 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// @TODO this needs to be in a singleton
var lang_1 = __webpack_require__(1);
var TTL = 10;
var onChangesTtl = TTL;
var ChangesQueue = (function () {
    function ChangesQueue() {
    }
    ChangesQueue.prototype.buildFlushOnChanges = function ($rootScope) {
        var _context = this;
        buildFlushOnChangesCb($rootScope);
        function buildFlushOnChangesCb($rootScope) {
            if (lang_1.isFunction(_context.flushOnChangesQueue)) {
                return _context.flushOnChangesQueue;
            }
            _context.flushOnChangesQueue = getFlushOnChangesQueueCb($rootScope);
            return _context.flushOnChangesQueue;
        }
        function getFlushOnChangesQueueCb($rootScope) {
            // This function is called in a $$postDigest to trigger all the onChanges hooks in a single digest
            return function _flushOnChangesQueue() {
                try {
                    if (!(--onChangesTtl)) {
                        // We have hit the TTL limit so reset everything
                        _context.onChangesQueue = undefined;
                        throw new Error("infchng, " + TTL + " ngOnChanges() iterations reached. Aborting!\n");
                    }
                    // We must run this hook in an apply since the $$postDigest runs outside apply
                    $rootScope.$apply(function () {
                        for (var i = 0, ii = _context.onChangesQueue.length; i < ii; ++i) {
                            _context.onChangesQueue[i]();
                        }
                        // Reset the queue to trigger a new schedule next time there is a change
                        _context.onChangesQueue = undefined;
                    });
                }
                finally {
                    onChangesTtl++;
                }
            };
        }
    };
    return ChangesQueue;
}());
exports.ChangesQueue = ChangesQueue;
exports.changesQueueService = new ChangesQueue();
//# sourceMappingURL=changes_queue.js.map

/***/ }),
/* 212 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Observable_1 = __webpack_require__(213);
var Subscriber_1 = __webpack_require__(80);
var Subscription_1 = __webpack_require__(49);
var ObjectUnsubscribedError_1 = __webpack_require__(220);
var SubjectSubscription_1 = __webpack_require__(221);
var rxSubscriber_1 = __webpack_require__(50);
/**
 * @class SubjectSubscriber<T>
 */
var SubjectSubscriber = (function (_super) {
    __extends(SubjectSubscriber, _super);
    function SubjectSubscriber(destination) {
        _super.call(this, destination);
        this.destination = destination;
    }
    return SubjectSubscriber;
}(Subscriber_1.Subscriber));
exports.SubjectSubscriber = SubjectSubscriber;
/**
 * @class Subject<T>
 */
var Subject = (function (_super) {
    __extends(Subject, _super);
    function Subject() {
        _super.call(this);
        this.observers = [];
        this.closed = false;
        this.isStopped = false;
        this.hasError = false;
        this.thrownError = null;
    }
    Subject.prototype[rxSubscriber_1.$$rxSubscriber] = function () {
        return new SubjectSubscriber(this);
    };
    Subject.prototype.lift = function (operator) {
        var subject = new AnonymousSubject(this, this);
        subject.operator = operator;
        return subject;
    };
    Subject.prototype.next = function (value) {
        if (this.closed) {
            throw new ObjectUnsubscribedError_1.ObjectUnsubscribedError();
        }
        if (!this.isStopped) {
            var observers = this.observers;
            var len = observers.length;
            var copy = observers.slice();
            for (var i = 0; i < len; i++) {
                copy[i].next(value);
            }
        }
    };
    Subject.prototype.error = function (err) {
        if (this.closed) {
            throw new ObjectUnsubscribedError_1.ObjectUnsubscribedError();
        }
        this.hasError = true;
        this.thrownError = err;
        this.isStopped = true;
        var observers = this.observers;
        var len = observers.length;
        var copy = observers.slice();
        for (var i = 0; i < len; i++) {
            copy[i].error(err);
        }
        this.observers.length = 0;
    };
    Subject.prototype.complete = function () {
        if (this.closed) {
            throw new ObjectUnsubscribedError_1.ObjectUnsubscribedError();
        }
        this.isStopped = true;
        var observers = this.observers;
        var len = observers.length;
        var copy = observers.slice();
        for (var i = 0; i < len; i++) {
            copy[i].complete();
        }
        this.observers.length = 0;
    };
    Subject.prototype.unsubscribe = function () {
        this.isStopped = true;
        this.closed = true;
        this.observers = null;
    };
    Subject.prototype._trySubscribe = function (subscriber) {
        if (this.closed) {
            throw new ObjectUnsubscribedError_1.ObjectUnsubscribedError();
        }
        else {
            return _super.prototype._trySubscribe.call(this, subscriber);
        }
    };
    Subject.prototype._subscribe = function (subscriber) {
        if (this.closed) {
            throw new ObjectUnsubscribedError_1.ObjectUnsubscribedError();
        }
        else if (this.hasError) {
            subscriber.error(this.thrownError);
            return Subscription_1.Subscription.EMPTY;
        }
        else if (this.isStopped) {
            subscriber.complete();
            return Subscription_1.Subscription.EMPTY;
        }
        else {
            this.observers.push(subscriber);
            return new SubjectSubscription_1.SubjectSubscription(this, subscriber);
        }
    };
    Subject.prototype.asObservable = function () {
        var observable = new Observable_1.Observable();
        observable.source = this;
        return observable;
    };
    Subject.create = function (destination, source) {
        return new AnonymousSubject(destination, source);
    };
    return Subject;
}(Observable_1.Observable));
exports.Subject = Subject;
/**
 * @class AnonymousSubject<T>
 */
var AnonymousSubject = (function (_super) {
    __extends(AnonymousSubject, _super);
    function AnonymousSubject(destination, source) {
        _super.call(this);
        this.destination = destination;
        this.source = source;
    }
    AnonymousSubject.prototype.next = function (value) {
        var destination = this.destination;
        if (destination && destination.next) {
            destination.next(value);
        }
    };
    AnonymousSubject.prototype.error = function (err) {
        var destination = this.destination;
        if (destination && destination.error) {
            this.destination.error(err);
        }
    };
    AnonymousSubject.prototype.complete = function () {
        var destination = this.destination;
        if (destination && destination.complete) {
            this.destination.complete();
        }
    };
    AnonymousSubject.prototype._subscribe = function (subscriber) {
        var source = this.source;
        if (source) {
            return this.source.subscribe(subscriber);
        }
        else {
            return Subscription_1.Subscription.EMPTY;
        }
    };
    return AnonymousSubject;
}(Subject));
exports.AnonymousSubject = AnonymousSubject;
//# sourceMappingURL=Subject.js.map

/***/ }),
/* 213 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var root_1 = __webpack_require__(48);
var toSubscriber_1 = __webpack_require__(214);
var observable_1 = __webpack_require__(219);
/**
 * A representation of any set of values over any amount of time. This the most basic building block
 * of RxJS.
 *
 * @class Observable<T>
 */
var Observable = (function () {
    /**
     * @constructor
     * @param {Function} subscribe the function that is  called when the Observable is
     * initially subscribed to. This function is given a Subscriber, to which new values
     * can be `next`ed, or an `error` method can be called to raise an error, or
     * `complete` can be called to notify of a successful completion.
     */
    function Observable(subscribe) {
        this._isScalar = false;
        if (subscribe) {
            this._subscribe = subscribe;
        }
    }
    /**
     * Creates a new Observable, with this Observable as the source, and the passed
     * operator defined as the new observable's operator.
     * @method lift
     * @param {Operator} operator the operator defining the operation to take on the observable
     * @return {Observable} a new observable with the Operator applied
     */
    Observable.prototype.lift = function (operator) {
        var observable = new Observable();
        observable.source = this;
        observable.operator = operator;
        return observable;
    };
    Observable.prototype.subscribe = function (observerOrNext, error, complete) {
        var operator = this.operator;
        var sink = toSubscriber_1.toSubscriber(observerOrNext, error, complete);
        if (operator) {
            operator.call(sink, this.source);
        }
        else {
            sink.add(this._trySubscribe(sink));
        }
        if (sink.syncErrorThrowable) {
            sink.syncErrorThrowable = false;
            if (sink.syncErrorThrown) {
                throw sink.syncErrorValue;
            }
        }
        return sink;
    };
    Observable.prototype._trySubscribe = function (sink) {
        try {
            return this._subscribe(sink);
        }
        catch (err) {
            sink.syncErrorThrown = true;
            sink.syncErrorValue = err;
            sink.error(err);
        }
    };
    /**
     * @method forEach
     * @param {Function} next a handler for each value emitted by the observable
     * @param {PromiseConstructor} [PromiseCtor] a constructor function used to instantiate the Promise
     * @return {Promise} a promise that either resolves on observable completion or
     *  rejects with the handled error
     */
    Observable.prototype.forEach = function (next, PromiseCtor) {
        var _this = this;
        if (!PromiseCtor) {
            if (root_1.root.Rx && root_1.root.Rx.config && root_1.root.Rx.config.Promise) {
                PromiseCtor = root_1.root.Rx.config.Promise;
            }
            else if (root_1.root.Promise) {
                PromiseCtor = root_1.root.Promise;
            }
        }
        if (!PromiseCtor) {
            throw new Error('no Promise impl found');
        }
        return new PromiseCtor(function (resolve, reject) {
            var subscription = _this.subscribe(function (value) {
                if (subscription) {
                    // if there is a subscription, then we can surmise
                    // the next handling is asynchronous. Any errors thrown
                    // need to be rejected explicitly and unsubscribe must be
                    // called manually
                    try {
                        next(value);
                    }
                    catch (err) {
                        reject(err);
                        subscription.unsubscribe();
                    }
                }
                else {
                    // if there is NO subscription, then we're getting a nexted
                    // value synchronously during subscription. We can just call it.
                    // If it errors, Observable's `subscribe` will ensure the
                    // unsubscription logic is called, then synchronously rethrow the error.
                    // After that, Promise will trap the error and send it
                    // down the rejection path.
                    next(value);
                }
            }, reject, resolve);
        });
    };
    Observable.prototype._subscribe = function (subscriber) {
        return this.source.subscribe(subscriber);
    };
    /**
     * An interop point defined by the es7-observable spec https://github.com/zenparsing/es-observable
     * @method Symbol.observable
     * @return {Observable} this instance of the observable
     */
    Observable.prototype[observable_1.$$observable] = function () {
        return this;
    };
    // HACK: Since TypeScript inherits static properties too, we have to
    // fight against TypeScript here so Subject can have a different static create signature
    /**
     * Creates a new cold Observable by calling the Observable constructor
     * @static true
     * @owner Observable
     * @method create
     * @param {Function} subscribe? the subscriber function to be passed to the Observable constructor
     * @return {Observable} a new cold observable
     */
    Observable.create = function (subscribe) {
        return new Observable(subscribe);
    };
    return Observable;
}());
exports.Observable = Observable;
//# sourceMappingURL=Observable.js.map

/***/ }),
/* 214 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Subscriber_1 = __webpack_require__(80);
var rxSubscriber_1 = __webpack_require__(50);
var Observer_1 = __webpack_require__(83);
function toSubscriber(nextOrObserver, error, complete) {
    if (nextOrObserver) {
        if (nextOrObserver instanceof Subscriber_1.Subscriber) {
            return nextOrObserver;
        }
        if (nextOrObserver[rxSubscriber_1.$$rxSubscriber]) {
            return nextOrObserver[rxSubscriber_1.$$rxSubscriber]();
        }
    }
    if (!nextOrObserver && !error && !complete) {
        return new Subscriber_1.Subscriber(Observer_1.empty);
    }
    return new Subscriber_1.Subscriber(nextOrObserver, error, complete);
}
exports.toSubscriber = toSubscriber;
//# sourceMappingURL=toSubscriber.js.map

/***/ }),
/* 215 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.isArray = Array.isArray || (function (x) { return x && typeof x.length === 'number'; });
//# sourceMappingURL=isArray.js.map

/***/ }),
/* 216 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function isObject(x) {
    return x != null && typeof x === 'object';
}
exports.isObject = isObject;
//# sourceMappingURL=isObject.js.map

/***/ }),
/* 217 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var errorObject_1 = __webpack_require__(82);
var tryCatchTarget;
function tryCatcher() {
    try {
        return tryCatchTarget.apply(this, arguments);
    }
    catch (e) {
        errorObject_1.errorObject.e = e;
        return errorObject_1.errorObject;
    }
}
function tryCatch(fn) {
    tryCatchTarget = fn;
    return tryCatcher;
}
exports.tryCatch = tryCatch;
;
//# sourceMappingURL=tryCatch.js.map

/***/ }),
/* 218 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * An error thrown when one or more errors have occurred during the
 * `unsubscribe` of a {@link Subscription}.
 */
var UnsubscriptionError = (function (_super) {
    __extends(UnsubscriptionError, _super);
    function UnsubscriptionError(errors) {
        _super.call(this);
        this.errors = errors;
        var err = Error.call(this, errors ?
            errors.length + " errors occurred during unsubscription:\n  " + errors.map(function (err, i) { return ((i + 1) + ") " + err.toString()); }).join('\n  ') : '');
        this.name = err.name = 'UnsubscriptionError';
        this.stack = err.stack;
        this.message = err.message;
    }
    return UnsubscriptionError;
}(Error));
exports.UnsubscriptionError = UnsubscriptionError;
//# sourceMappingURL=UnsubscriptionError.js.map

/***/ }),
/* 219 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var root_1 = __webpack_require__(48);
function getSymbolObservable(context) {
    var $$observable;
    var Symbol = context.Symbol;
    if (typeof Symbol === 'function') {
        if (Symbol.observable) {
            $$observable = Symbol.observable;
        }
        else {
            $$observable = Symbol('observable');
            Symbol.observable = $$observable;
        }
    }
    else {
        $$observable = '@@observable';
    }
    return $$observable;
}
exports.getSymbolObservable = getSymbolObservable;
exports.$$observable = getSymbolObservable(root_1.root);
//# sourceMappingURL=observable.js.map

/***/ }),
/* 220 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * An error thrown when an action is invalid because the object has been
 * unsubscribed.
 *
 * @see {@link Subject}
 * @see {@link BehaviorSubject}
 *
 * @class ObjectUnsubscribedError
 */
var ObjectUnsubscribedError = (function (_super) {
    __extends(ObjectUnsubscribedError, _super);
    function ObjectUnsubscribedError() {
        var err = _super.call(this, 'object unsubscribed');
        this.name = err.name = 'ObjectUnsubscribedError';
        this.stack = err.stack;
        this.message = err.message;
    }
    return ObjectUnsubscribedError;
}(Error));
exports.ObjectUnsubscribedError = ObjectUnsubscribedError;
//# sourceMappingURL=ObjectUnsubscribedError.js.map

/***/ }),
/* 221 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Subscription_1 = __webpack_require__(49);
/**
 * We need this JSDoc comment for affecting ESDoc.
 * @ignore
 * @extends {Ignored}
 */
var SubjectSubscription = (function (_super) {
    __extends(SubjectSubscription, _super);
    function SubjectSubscription(subject, subscriber) {
        _super.call(this);
        this.subject = subject;
        this.subscriber = subscriber;
        this.closed = false;
    }
    SubjectSubscription.prototype.unsubscribe = function () {
        if (this.closed) {
            return;
        }
        this.closed = true;
        var subject = this.subject;
        var observers = subject.observers;
        this.subject = null;
        if (!observers || observers.length === 0 || subject.isStopped || subject.closed) {
            return;
        }
        var subscriberIndex = observers.indexOf(this.subscriber);
        if (subscriberIndex !== -1) {
            observers.splice(subscriberIndex, 1);
        }
    };
    return SubjectSubscription;
}(Subscription_1.Subscription));
exports.SubjectSubscription = SubjectSubscription;
//# sourceMappingURL=SubjectSubscription.js.map

/***/ }),
/* 222 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var collections_1 = __webpack_require__(3);
var constants_1 = __webpack_require__(84);
function setupFields(ngAttrs, rawInputs, rawOutputs) {
    if (rawInputs === void 0) { rawInputs = []; }
    if (rawOutputs === void 0) { rawOutputs = []; }
    var _a = _setupInputs(_parseFields(rawInputs), ngAttrs), inputs = _a.inputs, attrs = _a.attrs;
    var outputs = _setupOutputs(_parseFields(rawOutputs), ngAttrs).outputs;
    return {
        inputs: inputs,
        attrs: attrs,
        outputs: outputs
    };
}
exports.setupFields = setupFields;
function _parseFields(names) {
    var attrProps = [];
    if (!names) {
        return attrProps;
    }
    for (var _i = 0, names_1 = names; _i < names_1.length; _i++) {
        var name_1 = names_1[_i];
        var parts = name_1.split(':');
        var prop = parts[0].trim();
        var attr = (parts[1] || parts[0]).trim();
        var isTypeByDeclaration = _isTypeByDeclaration(attr);
        var attrName = _getNormalizedAttrName(attr, prop, isTypeByDeclaration);
        var type = _getBindingType(attr, isTypeByDeclaration);
        attrProps.push({
            prop: prop,
            attr: attrName,
            bracketAttr: "[" + attrName + "]",
            parenAttr: "(" + attrName + ")",
            bracketParenAttr: "[(" + attrName + ")]",
            type: type,
            typeByTemplate: !isTypeByDeclaration
        });
    }
    return attrProps;
}
exports._parseFields = _parseFields;
function _getBindingType(originalAttr, isTypeByDeclaration) {
    return isTypeByDeclaration
        ? originalAttr.charAt(0)
        : '';
}
function _isTypeByDeclaration(attr) {
    return collections_1.ListWrapper.contains([
        constants_1.BINDING_MODE.attr, constants_1.BINDING_MODE.oneWay, constants_1.BINDING_MODE.twoWay
    ], attr.charAt(0));
}
function _getNormalizedAttrName(originalAttr, propName, isTypeByDeclaration) {
    if (!isTypeByDeclaration) {
        return originalAttr;
    }
    return originalAttr.length > 1
        ? originalAttr.substring(1)
        : propName;
}
function _setupInputs(inputs, ngAttrs) {
    var parsedAttrs = {};
    var parsedInputs = {};
    for (var _i = 0, inputs_1 = inputs; _i < inputs_1.length; _i++) {
        var input = inputs_1[_i];
        if (input.typeByTemplate) {
            if (ngAttrs.hasOwnProperty(input.attr)) {
                // @
                parsedAttrs[input.prop] = {
                    mode: constants_1.BINDING_MODE.attr,
                    exp: ngAttrs[input.attr],
                    attrName: input.attr
                };
            }
            else if (ngAttrs.hasOwnProperty(input.bracketAttr)) {
                // <
                parsedInputs[input.prop] = {
                    mode: constants_1.BINDING_MODE.oneWay,
                    exp: ngAttrs[input.bracketAttr],
                    attrName: input.bracketAttr
                };
            }
            else if (ngAttrs.hasOwnProperty(input.bracketParenAttr)) {
                // =
                parsedInputs[input.prop] = {
                    mode: constants_1.BINDING_MODE.twoWay,
                    exp: ngAttrs[input.bracketParenAttr],
                    attrName: input.bracketParenAttr
                };
            }
        }
        else {
            if (ngAttrs.hasOwnProperty(input.attr)) {
                var attrMetadata = { mode: input.type, exp: ngAttrs[input.attr], attrName: input.attr };
                if (input.type === constants_1.BINDING_MODE.attr) {
                    parsedAttrs[input.prop] = attrMetadata;
                }
                else {
                    parsedInputs[input.prop] = attrMetadata;
                }
            }
        }
    }
    return {
        inputs: parsedInputs,
        attrs: parsedAttrs
    };
}
exports._setupInputs = _setupInputs;
function _setupOutputs(outputs, ngAttrs) {
    var parsedOutputs = {};
    for (var i = 0; i < outputs.length; i = i + 1) {
        var output = outputs[i];
        var baseParsedAttrField = { mode: constants_1.BINDING_MODE.output, exp: undefined, attrName: '' };
        // & via event
        if (ngAttrs.hasOwnProperty(output.attr)) {
            parsedOutputs[output.prop] = collections_1.StringMapWrapper.assign({}, baseParsedAttrField, { exp: ngAttrs[output.attr], attrName: output.attr });
        }
        else if (ngAttrs.hasOwnProperty(output.parenAttr)) {
            parsedOutputs[output.prop] = collections_1.StringMapWrapper.assign({}, baseParsedAttrField, { exp: ngAttrs[output.parenAttr], attrName: output.parenAttr });
        }
    }
    return { outputs: parsedOutputs };
}
exports._setupOutputs = _setupOutputs;
/**
 * parses input/output/attrs string arrays from metadata fro further processing
 * @param inputs
 * @param outputs
 * @param attrs
 * @returns {{inputs: ParsedBindingsMap, outputs: ParsedBindingsMap, attrs: ParsedBindingsMap}}
 * @private
 * @deprecated
 */
function _parseBindings(_a) {
    var _b = _a.inputs, inputs = _b === void 0 ? [] : _b, _c = _a.outputs, outputs = _c === void 0 ? [] : _c, _d = _a.attrs, attrs = _d === void 0 ? [] : _d;
    var SPLIT_BY = ':';
    return {
        inputs: _parseByMode(inputs, constants_1.BINDING_MODE.twoWay, [constants_1.BINDING_MODE.attr]),
        outputs: _parseByMode(outputs, constants_1.BINDING_MODE.output),
        attrs: collections_1.StringMapWrapper.merge(
        // this will be removed in 2.0
        _parseByMode(attrs, constants_1.BINDING_MODE.attr), 
        // attrs build from @Input('@')
        _parseByMode(inputs, constants_1.BINDING_MODE.twoWay, [constants_1.BINDING_MODE.oneWay, constants_1.BINDING_MODE.twoWay]))
    };
    function _parseByMode(bindingArr, defaultMode, excludeMode) {
        if (excludeMode === void 0) { excludeMode = []; }
        return bindingArr.reduce(function (acc, binding) {
            var _a = binding.split(SPLIT_BY).map(function (part) { return part.trim(); }), name = _a[0], _b = _a[1], modeConfigAndAlias = _b === void 0 ? '' : _b;
            var parsedConfigAndAlias = modeConfigAndAlias.match(constants_1.INPUT_MODE_REGEX);
            var _c = parsedConfigAndAlias || [], _d = _c[1], mode = _d === void 0 ? defaultMode : _d, _e = _c[2], optional = _e === void 0 ? '' : _e, _f = _c[3], alias = _f === void 0 ? '' : _f;
            // exit early if processed mode is not allowed
            // for example if we are parsing Input('@') which produces attr binding instead of one or two way
            if (excludeMode.indexOf(mode) !== -1) {
                return acc;
            }
            // @TODO remove this in next version where template parsing will be implemented
            if ((defaultMode !== constants_1.BINDING_MODE.output) && !(parsedConfigAndAlias && parsedConfigAndAlias[1])) {
                console.warn("\n        You need to explicitly provide type of binding(=,<,@) within your <code>'@Input() " + name + ";</code>.\n        Next version 2.0 will create binding by parsing template if you provide '@Input()' without binding type.\n        ");
            }
            acc[name] = {
                mode: mode,
                alias: alias,
                optional: optional === constants_1.BINDING_MODE.optional || true
            };
            return acc;
        }, {});
    }
}
exports._parseBindings = _parseBindings;
//# sourceMappingURL=binding_parser.js.map

/***/ }),
/* 223 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.REQUIRE_PREFIX_REGEXP = /^(?:(\^\^?)?(\?)?(\^\^?)?)?/;
//# sourceMappingURL=constants.js.map

/***/ }),
/* 224 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
var directive_lifecycle_interfaces_1 = __webpack_require__(46);
var reflection_1 = __webpack_require__(11);
var provider_1 = __webpack_require__(15);
var metadata_directives_1 = __webpack_require__(12);
var metadata_di_1 = __webpack_require__(30);
/**
 * setup watchers for children component/directives provided by @Query decorators
 * - setup @ContentChild/@ContentChildren/@ViewChild/@ViewChildren
 * @param scope
 * @param element
 * @param ctrl
 * @param queries
 * @private
 */
function _setupQuery(scope, element, ctrl, queries) {
    var SEMAPHORE_PROP_NAMES = Object.freeze({
        view: '__readViewChildrenOrderScheduled',
        content: '__readContentChildrenOrderScheduled'
    });
    var DOM_RESOLVER_TYPES = Object.freeze({
        view: 'view',
        content: 'content'
    });
    if (collections_1.StringMapWrapper.size(queries) === 0) {
        return;
    }
    var onChildrenChangedCb = _getOnChildrenResolvers(element, ctrl, queries);
    ctrl.__readContentChildrenOrderScheduled = false;
    ctrl.__readViewChildrenOrderScheduled = false;
    // this is our created _ngOnChildrenChanged which will be called by children directives
    var _ngOnChildrenChanged = function (type, onFirstChangeDoneCb, domResolverCb) {
        if (onFirstChangeDoneCb === void 0) { onFirstChangeDoneCb = []; }
        if (domResolverCb === void 0) { domResolverCb = onChildrenChangedCb; }
        var orderScheduledSemaphorePropName = '';
        var domResolverCbType = '';
        if (type === directive_lifecycle_interfaces_1.ChildrenChangeHook.FromView) {
            orderScheduledSemaphorePropName = SEMAPHORE_PROP_NAMES.view;
            domResolverCbType = DOM_RESOLVER_TYPES.view;
        }
        else if (type === directive_lifecycle_interfaces_1.ChildrenChangeHook.FromContent) {
            domResolverCbType = DOM_RESOLVER_TYPES.content;
            orderScheduledSemaphorePropName = SEMAPHORE_PROP_NAMES.content;
        }
        else {
            throw new Error("_ngOnChildrenChanged: queryType(" + type + ") must be one of FromView|FromContent");
        }
        if (ctrl[orderScheduledSemaphorePropName]) {
            return;
        }
        ctrl[orderScheduledSemaphorePropName] = true;
        // we execute callback within $evalAsync to extend $digest loop count, which will not trigger another
        // $rootScope.$digest === #perfmatters
        scope.$evalAsync(function () {
            // turn semaphore On back again
            ctrl[orderScheduledSemaphorePropName] = false;
            // query DOM and assign instances/jqLite to controller properties
            domResolverCb[domResolverCbType].forEach(function (cb) { return cb(); });
            // when DOM is queried we can execute DirectiveComponent life cycles which have been registered
            // AfterViewInit | AfterContentInit
            onFirstChangeDoneCb.forEach(function (cb) { lang_1.isFunction(cb) && cb(); });
        });
    };
    // this method needs to be called from children which are we querying
    // if they are rendered dynamically/async
    ctrl._ngOnChildrenChanged = _ngOnChildrenChanged.bind(ctrl);
    /**
     * get all callbacks which will be executed withing $scope.$evalAsync,
     * which are querying for DOM elements and gets controller instances from host element children
     * @param element
     * @param ctrl
     * @param queries
     * @returns {view: Function[], content: Function[]}
     * @private
     */
    function _getOnChildrenResolvers(element, ctrl, queries) {
        var _onChildrenChangedCbMap = (_a = {},
            _a[DOM_RESOLVER_TYPES.view] = [],
            _a[DOM_RESOLVER_TYPES.content] = [],
            _a);
        collections_1.StringMapWrapper.forEach(queries, function (meta, key) {
            if (meta instanceof metadata_di_1.ViewChildMetadata) {
                _onChildrenChangedCbMap[DOM_RESOLVER_TYPES.view].push(_resolveViewChild(element, ctrl, key, meta));
            }
            if (meta instanceof metadata_di_1.ViewChildrenMetadata) {
                _onChildrenChangedCbMap[DOM_RESOLVER_TYPES.view].push(_resolveViewChildren(element, ctrl, key, meta));
            }
            if (meta instanceof metadata_di_1.ContentChildMetadata) {
                _onChildrenChangedCbMap[DOM_RESOLVER_TYPES.content].push(_resolveContentChild(element, ctrl, key, meta));
            }
            if (meta instanceof metadata_di_1.ContentChildrenMetadata) {
                _onChildrenChangedCbMap[DOM_RESOLVER_TYPES.content].push(_resolveContentChildren(element, ctrl, key, meta));
            }
        });
        return _onChildrenChangedCbMap;
        function _resolveViewChild(element, ctrl, key, meta) {
            return _resolveChildrenFactory(element, ctrl, key, meta.selector, DOM_RESOLVER_TYPES.view, true);
        }
        function _resolveContentChild(element, ctrl, key, meta) {
            return _resolveChildrenFactory(element, ctrl, key, meta.selector, DOM_RESOLVER_TYPES.content, true);
        }
        function _resolveViewChildren(element, ctrl, key, meta) {
            return _resolveChildrenFactory(element, ctrl, key, meta.selector, DOM_RESOLVER_TYPES.view);
        }
        function _resolveContentChildren(element, ctrl, key, meta) {
            return _resolveChildrenFactory(element, ctrl, key, meta.selector, DOM_RESOLVER_TYPES.content);
        }
        var _a;
    }
}
exports._setupQuery = _setupQuery;
/**
 * resolving DOM instances by provided @ContentChild(ref)/@ViewChild(ref)
 * - if querying for string, we handle it as a selector and return jqLite instances
 * - if querying for Type( directive | component ) we get proper selector and controller from
 * provided Type reference, query the DOM and return that controller instance if exists, otherwise null
 * @param element
 * @param ctrl
 * @param key
 * @param cssSelector
 * @param type
 * @param firstOnly
 * @returns {function(): void}
 * @private
 */
function _resolveChildrenFactory(element, ctrl, key, cssSelector, type, firstOnly) {
    if (firstOnly === void 0) { firstOnly = false; }
    var _a = _getSelectorAndCtrlName(cssSelector), selector = _a.selector, childCtrlName = _a.childCtrlName;
    return _childResolver;
    function _childResolver() {
        if (firstOnly) {
            ctrl[key] = null;
            var child = _getChildElements(element, selector, type, firstOnly);
            var childInstance = lang_1.isString(cssSelector)
                ? child
                : getControllerOnElement(child, childCtrlName);
            ctrl[key] = childInstance;
        }
        else {
            ctrl[key] = [];
            var children = _getChildElements(element, selector, type);
            if (lang_1.isString(cssSelector)) {
                ctrl[key] = children;
                return;
            }
            for (var i = 0; i < children.length; i++) {
                ctrl[key].push(getControllerOnElement(children.eq(i), childCtrlName));
            }
        }
    }
}
exports._resolveChildrenFactory = _resolveChildrenFactory;
/**
 * query View/Content DOM for particular child elements/attributes selector
 * @param $element
 * @param selector
 * @param type
 * @param firstOnly
 * @returns {IAugmentedJQuery}
 * @private
 */
function _getChildElements($element, selector, type, firstOnly) {
    if (firstOnly === void 0) { firstOnly = false; }
    var querySelector = '';
    if (type === 'view') {
        // Note: we are guarding only for first nested child inside ng-transclude
        // this would be to complicated and DOM heavy to select only selectors outside ng-transclude
        // - it should be author responsibility to not include Component view directive within <ng-transclude> and querying for them
        querySelector = ":not(ng-transclude):not([ng-transclude]) > " + selector;
    }
    if (type === 'content') {
        querySelector = "ng-transclude " + selector + ", [ng-transclude] " + selector;
    }
    var queryMethod = firstOnly
        ? 'querySelector'
        : 'querySelectorAll';
    return lang_1.global.angular.element($element[0][queryMethod](querySelector));
}
exports._getChildElements = _getChildElements;
function _getSelectorAndCtrlName(childSelector) {
    var selector = _getSelector(childSelector);
    var childCtrlName = provider_1.getInjectableName(childSelector);
    return { selector: selector, childCtrlName: childCtrlName };
}
exports._getSelectorAndCtrlName = _getSelectorAndCtrlName;
/**
 * get CSS selector from Component/Directive decorated class metadata
 * @param selector
 * @returns {string}
 * @private
 */
function _getSelector(selector) {
    if (lang_1.isString(selector)) {
        return selector;
    }
    if (lang_1.isType(selector)) {
        var annotation = reflection_1.reflector.annotations(selector)[0];
        if (annotation instanceof metadata_directives_1.DirectiveMetadata) {
            return annotation.selector;
        }
    }
    throw new Error("cannot query for non Directive/Component type " + lang_1.getFuncName(selector));
}
exports._getSelector = _getSelector;
/**
 * creates functions which will be called from parent component which is querying this component
 * - component which queries needs to be injected to child,
 * here child creates special callbacks by type of @Query which will be called from postLink and on scope destroy so
 * we clean up GC
 * @param ctrl
 * @param requiredCtrls
 * @returns {Object|Array|T|function()[]}
 * @private
 */
function _getParentCheckNotifiers(ctrl, requiredCtrls) {
    var parentCheckedNotifiers = requiredCtrls.reduce(function (acc, requiredCtrl) {
        if (!lang_1.isJsObject(requiredCtrl)) {
            return acc;
        }
        var Ctor = requiredCtrl.constructor;
        if (!lang_1.isType(Ctor)) {
            return acc;
        }
        var propMeta = reflection_1.reflector.propMetadata(Ctor);
        if (!collections_1.StringMapWrapper.size(propMeta)) {
            return acc;
        }
        var _parentCheckedNotifiers = [];
        collections_1.StringMapWrapper.forEach(propMeta, function (propMetaPropArr) {
            propMetaPropArr
                .filter(function (propMetaInstance) {
                // check if propMeta is one of @Query types and that it queries for Directive/Component ( typeof selector == function )
                if (!((propMetaInstance instanceof metadata_di_1.QueryMetadata) && lang_1.isType(propMetaInstance.selector))) {
                    return false;
                }
                // check if current child is really queried from its parent
                return ctrl instanceof propMetaInstance.selector;
            })
                .forEach(function (propMetaInstance) {
                // parent queried for this child with one from @ContentChild/@ContentChildren
                if (!propMetaInstance.isViewQuery) {
                    _parentCheckedNotifiers.push(function () { return requiredCtrl._ngOnChildrenChanged(directive_lifecycle_interfaces_1.ChildrenChangeHook.FromContent, [requiredCtrl.ngAfterContentChecked.bind(requiredCtrl)]); });
                }
                // parent queried for this child with one from @ViewChild/@ViewChildren
                if (propMetaInstance.isViewQuery) {
                    _parentCheckedNotifiers.push(function () { return requiredCtrl._ngOnChildrenChanged(directive_lifecycle_interfaces_1.ChildrenChangeHook.FromView, [requiredCtrl.ngAfterViewChecked.bind(requiredCtrl)]); });
                }
            });
        });
        return acc.concat(_parentCheckedNotifiers);
    }, []);
    return collections_1.ListWrapper.size(parentCheckedNotifiers)
        ? parentCheckedNotifiers
        : [lang_1.noop];
}
exports._getParentCheckNotifiers = _getParentCheckNotifiers;
function getControllerOnElement($element, ctrlName) {
    if (!$element) {
        return null;
    }
    return $element.controller(ctrlName);
}
exports.getControllerOnElement = getControllerOnElement;
//# sourceMappingURL=children_resolver.js.map

/***/ }),
/* 225 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
var HOST_BINDING_KEY_REGEX = /^\[.*\]$/;
var HOST_LISTENER_KEY_REGEX = /^\(.*\)$/;
var HAS_CLASS_REGEX = /^class./;
var HAS_ATTR_REGEX = /^attr./;
function _parseHost(host) {
    if (!lang_1.isPresent(host)) {
        return;
    }
    var hostStatic = {};
    var hostBindingsRaw = [];
    var hostListeners = {};
    collections_1.StringMapWrapper.forEach(host, function (hostValue, hostKey) {
        var hostMap = (_a = {}, _a[stripBindingOrListenerBrackets(hostKey)] = hostValue, _a);
        if (isStaticHost(hostKey)) {
            lang_1.assign(hostStatic, hostMap);
            return;
        }
        if (isHostBinding(hostKey)) {
            hostBindingsRaw.push(hostMap);
            return;
        }
        if (isHostListener(hostKey)) {
            lang_1.assign(hostListeners, processHostListenerCallback(hostMap));
        }
        var _a;
    });
    var hostBindings = hostBindingsRaw
        .reduce(function (acc, hostBindingObj) {
        var hostObjKey = Object.keys(hostBindingObj)[0];
        var hostObjValue = hostBindingObj[hostObjKey];
        if (HAS_CLASS_REGEX.test(hostObjKey)) {
            acc.classes[hostObjKey.replace(HAS_CLASS_REGEX, '')] = hostObjValue;
            return acc;
        }
        if (HAS_ATTR_REGEX.test(hostObjKey)) {
            acc.attributes[hostObjKey.replace(HAS_ATTR_REGEX, '')] = hostObjValue;
            return acc;
        }
        lang_1.assign(acc.properties, hostBindingObj);
        return acc;
    }, {
        classes: {},
        attributes: {},
        properties: {}
    });
    return {
        hostStatic: hostStatic,
        hostBindings: hostBindings,
        hostListeners: hostListeners
    };
    function isHostBinding(hostKey) {
        return HOST_BINDING_KEY_REGEX.test(hostKey);
    }
    function isHostListener(hostKey) {
        return HOST_LISTENER_KEY_REGEX.test(hostKey);
    }
    function isStaticHost(hostKey) {
        return !(isHostBinding(hostKey) || isHostListener(hostKey));
    }
    function stripBindingOrListenerBrackets(hostKey) {
        return hostKey.replace(/\[|\]|\(|\)/g, '');
    }
    function processHostListenerCallback(hostListener) {
        // eventKey is 'click' or 'document: click' etc
        var eventKey = Object.keys(hostListener)[0];
        // cbString is just value 'onMove($event.target)' or 'onMove()'
        var cbString = hostListener[eventKey];
        // here we parse out callback method and its argument to separate strings
        // - for instance we got from 'onMove($event.target)' --> 'onMove','$event.target'
        var _a = /^(\w+)\(([$\w.\s,]*)\)$/.exec(cbString), cbMethodName = _a[1], cbMethodArgs = _a[2];
        var eventValue = [
            cbMethodName
        ].concat(cbMethodArgs.split(',').filter(function (argument) { return Boolean(argument); }).map(function (argument) { return argument.trim(); }));
        return _b = {},
            _b[eventKey.replace(/\s/g, '')] = eventValue,
            _b;
        var _b;
    }
}
exports._parseHost = _parseHost;
//# sourceMappingURL=host_parser.js.map

/***/ }),
/* 226 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var collections_1 = __webpack_require__(3);
var primitives_1 = __webpack_require__(85);
/**
 *
 * @param element
 * @param staticAttributes
 * @private
 */
function _setHostStaticAttributes(element, staticAttributes) {
    element.attr(staticAttributes);
}
exports._setHostStaticAttributes = _setHostStaticAttributes;
/**
 *
 * @param scope
 * @param element
 * @param ctrl
 * @param hostBindings
 * @returns {Array}
 * @internal
 * @private
 */
function _setHostBindings(scope, element, ctrl, hostBindings) {
    // setup @HostBindings
    return _createWatcherByType('classes', hostBindings).concat(_createWatcherByType('attributes', hostBindings), _createWatcherByType('properties', hostBindings));
    /**
     * registers $scope.$watch for appropriate hostBinding
     * the watcher watches property on controller instance
     * @param type
     * @param hostBinding
     * @returns {Array}
     * @private
     */
    function _createWatcherByType(type, hostBinding) {
        var _watchersByType = [];
        collections_1.StringMapWrapper.forEach(hostBinding[type], function (watchPropName, keyToSet) {
            _watchersByType.push(scope.$watch(function () { return ctrl[watchPropName]; }, function (newValue) {
                if (type === 'classes') {
                    element.toggleClass(keyToSet, newValue);
                }
                if (type === 'attributes') {
                    element.attr(keyToSet, newValue);
                }
                if (type === 'properties') {
                    collections_1.StringMapWrapper.setValueInPath(element[0], keyToSet, newValue);
                }
            }));
        });
        return _watchersByType;
    }
}
exports._setHostBindings = _setHostBindings;
/**
 *
 * @param scope
 * @param element
 * @param ctrl
 * @param hostListeners
 * @internal
 * @private
 */
function _setHostListeners(scope, element, ctrl, hostListeners) {
    collections_1.StringMapWrapper.forEach(hostListeners, _registerHostListener);
    function _registerHostListener(cbArray, eventKey) {
        var methodName = cbArray[0], methodParams = cbArray.slice(1);
        var _a = _getTargetAndEvent(eventKey, element), event = _a.event, target = _a.target;
        // console.log( event );
        target.on(event, eventHandler);
        // global event
        if (target !== element) {
            scope.$on('$destroy', function () { return target.off(event, eventHandler); });
        }
        function eventHandler(evt) {
            var cbParams = _getHostListenerCbParams(evt, methodParams);
            scope.$apply(function () {
                var noPreventDefault = ctrl[methodName].apply(ctrl, cbParams);
                // HostListener event.preventDefault if method returns false
                if (noPreventDefault === false) {
                    evt.preventDefault();
                }
            });
        }
    }
}
exports._setHostListeners = _setHostListeners;
/**
 * return $event or it's property if found via path
 * @param event
 * @param eventParams
 * @returns {Array}
 * @private
 */
function _getHostListenerCbParams(event, eventParams) {
    var ALLOWED_EVENT_NAME = '$event';
    return eventParams.reduce(function (acc, eventPath) {
        if (!primitives_1.StringWrapper.startsWith(eventPath, ALLOWED_EVENT_NAME)) {
            throw new Error("\n              only $event.* is supported. Please provide correct listener parameter @example: $event,$event.target\n              ");
        }
        if (eventPath === ALLOWED_EVENT_NAME) {
            return acc.concat([event]);
        }
        return acc.concat([collections_1.StringMapWrapper.getValueFromPath(event, eventPath.replace(ALLOWED_EVENT_NAME, ''))]);
    }, []);
}
exports._getHostListenerCbParams = _getHostListenerCbParams;
function _getGlobalTargetReference($injector, targetName) {
    var globalEventTargets = ['document', 'window', 'body'];
    var $document = $injector.get("$document");
    if (targetName === 'document') {
        return $document;
    }
    if (targetName === 'window') {
        return lang_1.global.angular.element($injector.get("$" + targetName));
    }
    if (targetName === 'body') {
        return lang_1.global.angular.element($document[0][targetName]);
    }
    throw new Error("unsupported global target '" + targetName + "', only '" + globalEventTargets + "' are supported");
}
/**
 *
 * @param definedHostEvent this will be just simple 'event' string name or 'globalTarget:event'
 * @param hostElement
 * @returns {any}
 * @private
 */
function _getTargetAndEvent(definedHostEvent, hostElement) {
    // global target
    var eventWithGlobalTarget = definedHostEvent.split(/\s*:\s*/);
    if (eventWithGlobalTarget.length === 2) {
        var globalTarget = eventWithGlobalTarget[0], eventOnTarget = eventWithGlobalTarget[1];
        return {
            event: eventOnTarget,
            target: _getGlobalTargetReference(hostElement.injector(), globalTarget)
        };
    }
    return {
        event: definedHostEvent,
        target: hostElement
    };
}
//# sourceMappingURL=host_resolver.js.map

/***/ }),
/* 227 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__(29));
var bundler_1 = __webpack_require__(228);
exports.bundle = bundler_1.bundle;
//# sourceMappingURL=util.js.map

/***/ }),
/* 228 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var reflection_1 = __webpack_require__(11);
var provider_1 = __webpack_require__(15);
var provider_util_1 = __webpack_require__(21);
var reflective_provider_1 = __webpack_require__(229);
var collections_1 = __webpack_require__(3);
function _bundleComponent(ComponentClass, otherProviders, existingAngular1Module) {
    if (otherProviders === void 0) { otherProviders = []; }
    // Support registering downgraded ng2 components directly
    var downgradedNgComponentName = reflection_1.reflector.downgradedNg2ComponentName(ComponentClass);
    if (downgradedNgComponentName) {
        var angular1Module_1 = existingAngular1Module || lang_1.global.angular.module(downgradedNgComponentName, []);
        angular1Module_1.directive(downgradedNgComponentName, ComponentClass);
        return angular1Module_1;
    }
    var angular1ModuleName = provider_1.getInjectableName(ComponentClass);
    var angular1Module = existingAngular1Module || lang_1.global.angular.module(angular1ModuleName, []);
    var annotations = reflection_1.reflector.annotations(ComponentClass);
    var cmpAnnotation = annotations[0];
    var _a = cmpAnnotation.providers, providers = _a === void 0 ? [] : _a, _b = cmpAnnotation.viewProviders, viewProviders = _b === void 0 ? [] : _b;
    // process component
    var _c = provider_1.provide(ComponentClass), cmpName = _c[0], cmpFactoryFn = _c[1];
    var _d = reflective_provider_1._getAngular1ModuleMetadataByType(ComponentClass), providerName = _d.providerName, providerMethod = _d.providerMethod, moduleMethod = _d.moduleMethod;
    if (reflective_provider_1._isTypeRegistered(cmpName, angular1Module, providerName, providerMethod)) {
        return angular1Module;
    }
    // @TODO register via this once requires are resolved for 3 types of attr directive from template
    // _registerTypeProvider( angular1Module, ComponentClass, { moduleMethod, name: cmpName, value: cmpFactoryFn } );
    angular1Module[moduleMethod](cmpName, cmpFactoryFn);
    // 1. process component/directive decorator providers/viewProviders
    reflective_provider_1._normalizeProviders(angular1Module, providers);
    reflective_provider_1._normalizeProviders(angular1Module, viewProviders);
    // 2. process otherProviders argument
    // - providers can be string(angular1Module reference), Type, StringMap(providerLiteral)
    // - directives can't be registered as via global providers only @Injectable,@Pipe,{provide:any,use*:any}
    // registerProviders(angular1Module, otherProviders);
    reflective_provider_1._normalizeProviders(angular1Module, otherProviders);
    return angular1Module;
}
function bundle(NgModuleClass, otherProviders, existingAngular1Module) {
    if (otherProviders === void 0) { otherProviders = []; }
    var angular1ModuleName = provider_1.getInjectableName(NgModuleClass);
    var angular1Module = existingAngular1Module || lang_1.global.angular.module(angular1ModuleName, []);
    var annotations = reflection_1.reflector.annotations(NgModuleClass);
    var ngModuleAnnotation = annotations[0];
    if (!provider_util_1.isNgModule(ngModuleAnnotation)) {
        throw new Error("bundle() requires a decorated NgModule as its first argument");
    }
    var _a = ngModuleAnnotation.declarations, declarations = _a === void 0 ? [] : _a, _b = ngModuleAnnotation.providers, providers = _b === void 0 ? [] : _b, _c = ngModuleAnnotation.imports, imports = _c === void 0 ? [] : _c;
    /**
     * Process `declarations`
     */
    collections_1.ListWrapper.flattenDeep(declarations).forEach(function (directiveType) {
        _bundleComponent(directiveType, [], angular1Module);
    });
    /**
     * Process `providers`
     */
    reflective_provider_1._normalizeProviders(angular1Module, providers);
    /**
     * Process `imports`
     */
    // 1. imports which are not NgModules
    var nonNgModuleImports = imports.filter(function (imported) {
        if (!lang_1.isFunction(imported)) {
            return true;
        }
        var annotations = reflection_1.reflector.annotations(imported);
        return !provider_util_1.isNgModule(ngModuleAnnotation);
    });
    reflective_provider_1._normalizeProviders(angular1Module, nonNgModuleImports);
    // 2.imports which are NgModules
    var NgModuleImports = imports.filter(function (imported) {
        if (!lang_1.isFunction(imported)) {
            return false;
        }
        var annotations = reflection_1.reflector.annotations(imported);
        return provider_util_1.isNgModule(ngModuleAnnotation);
    });
    NgModuleImports.forEach(function (importedNgModule) {
        bundle(importedNgModule, [], angular1Module);
    });
    /**
     * Process `otherProviders`
     */
    reflective_provider_1._normalizeProviders(angular1Module, otherProviders);
    return angular1Module;
}
exports.bundle = bundle;
//# sourceMappingURL=bundler.js.map

/***/ }),
/* 229 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
var reflection_1 = __webpack_require__(11);
var provider_1 = __webpack_require__(15);
var provider_util_1 = __webpack_require__(21);
/**
 * process provider literals and return map for angular1Module consumption
 * @param provider
 * @returns {{method: string, name: string, value: any}}
 */
function resolveReflectiveProvider(provider) {
    var token = provider.token;
    if (lang_1.isPresent(provider.useValue)) {
        var _a = provider_1.provide(token, { useValue: provider.useValue }), name_1 = _a[0], value = _a[1];
        var method = 'value';
        return { method: method, name: name_1, value: value };
    }
    if (provider.useFactory) {
        var _b = provider_1.provide(token, { useFactory: provider.useFactory, deps: provider.dependencies }), name_2 = _b[0], value = _b[1];
        var method = 'factory';
        return { method: method, name: name_2, value: value };
    }
    if (provider.useClass) {
        var _c = provider_1.provide(token, { useClass: provider.useClass }), name_3 = _c[0], value = _c[1];
        var method = 'service';
        return { method: method, name: name_3, value: value };
    }
    if (provider.useExisting) {
        var _d = provider_1.provide(provider.useExisting), name_4 = _d[0], value = _d[1];
        var method = 'factory';
        throw new Error('useExisting is unimplemented');
    }
    throw new Error('invalid provider type! please specify one of: [useValue,useFactory,useClass]');
}
exports.resolveReflectiveProvider = resolveReflectiveProvider;
/**
 * returns StringMap of values needed for angular1Module registration and duplicity checks
 * @param injectable
 * @returns {any}
 * @private
 */
function _getAngular1ModuleMetadataByType(injectable) {
    // only the first class annotations is injectable
    var annotation = reflection_1.reflector.annotations(injectable)[0];
    if (lang_1.isBlank(annotation)) {
        // support configPhase ( function or pure class )
        if (lang_1.isType(injectable)) {
            return {
                providerName: '$injector',
                providerMethod: 'invoke',
                moduleMethod: 'config'
            };
        }
        throw new Error("\n        cannot get injectable name token from none decorated class " + lang_1.getFuncName(injectable) + "\n        Only decorated classes by one of [ @Injectable,@Directive,@Component,@Pipe ], can be injected by reference\n      ");
    }
    if (provider_util_1.isPipe(annotation)) {
        return {
            providerName: '$filterProvider',
            providerMethod: 'register',
            moduleMethod: 'filter'
        };
    }
    if (provider_util_1.isDirectiveLike(annotation)) {
        return {
            providerName: '$compileProvider',
            providerMethod: 'directive',
            moduleMethod: 'directive'
        };
    }
    if (provider_util_1.isService(annotation)) {
        return {
            providerName: '$provide',
            providerMethod: 'service',
            moduleMethod: 'service'
        };
    }
}
exports._getAngular1ModuleMetadataByType = _getAngular1ModuleMetadataByType;
/**
 * run through Component tree and register everything that is registered via Metadata
 * - works for nested arrays like angular 2 does ;)
 * @param angular1Module
 * @param providers
 * @returns {ng.IModule}
 * @private
 */
function _normalizeProviders(angular1Module, providers) {
    providers.forEach(function (providerType) {
        // this is for legacy Angular 1 module
        if (lang_1.isString(providerType)) {
            angular1Module.requires.push(providerType);
            return;
        }
        // this works only for value,factory,aliased services
        // you cannot register directive/pipe within provider literal
        if (provider_util_1.isProviderLiteral(providerType)) {
            var provider = provider_util_1.createProvider(providerType);
            var _a = resolveReflectiveProvider(provider), method = _a.method, name_5 = _a.name, value = _a.value;
            if (!_isTypeRegistered(name_5, angular1Module, '$provide', method)) {
                angular1Module[method](name_5, value);
            }
            return;
        }
        // this is for pipes/directives/services
        if (lang_1.isType(providerType)) {
            // const provider = createProvider( {provide:b, useClass:b} );
            // const { method, name, value } = resolveReflectiveProvider( provider );
            var _b = provider_1.provide(providerType), name_6 = _b[0], value = _b[1];
            var _c = _getAngular1ModuleMetadataByType(providerType), providerName = _c.providerName, providerMethod = _c.providerMethod, moduleMethod = _c.moduleMethod;
            // config phase support
            if (lang_1.isType(name_6)) {
                angular1Module.config(name_6);
                return;
            }
            if (!_isTypeRegistered(name_6, angular1Module, providerName, providerMethod)) {
                // @TODO register via this once requires are resolved for 3 types of attr directive from template
                // _registerTypeProvider( angular1Module, providerType, { moduleMethod, name, value } );
                angular1Module[moduleMethod](name_6, value);
            }
            return;
        }
        // un flattened array, unwrap and parse next array level of providers
        if (lang_1.isArray(providerType)) {
            _normalizeProviders(angular1Module, providerType);
        }
        else {
            throw new Error("InvalidProviderError(" + providerType + ")");
        }
    });
    // return res;
    return angular1Module;
}
exports._normalizeProviders = _normalizeProviders;
/**
 * check if `findRegisteredType` is registered within angular1Module, so we don't have duplicates
 * @param findRegisteredType
 * @param angular1Module
 * @param instanceType
 * @param methodName
 * @returns {boolean}
 * @private
 */
function _isTypeRegistered(findRegisteredType, angular1Module, instanceType, methodName) {
    var invokeQueue = angular1Module._invokeQueue;
    var types = invokeQueue
        .filter(function (_a) {
        var type = _a[0], fnName = _a[1];
        return type === instanceType && fnName === methodName;
    })
        .map(function (_a) {
        var type = _a[0], fnName = _a[1], registeredProvider = _a[2];
        return registeredProvider;
    });
    return types.some(function (_a) {
        var typeName = _a[0], typeFn = _a[1];
        return findRegisteredType === typeName;
    });
}
exports._isTypeRegistered = _isTypeRegistered;
/**
 * we need to register 3 types of attribute directives, if we are registering directive,
 * because we need to allow all 3 types of binding on the defined directive [name],(name),name
 * @private
 */
function _registerTypeProvider(angular1Module, provider, _a) {
    var moduleMethod = _a.moduleMethod, name = _a.name, value = _a.value;
    // only the first class annotations is injectable
    var annotation = reflection_1.reflector.annotations(provider)[0];
    if (lang_1.isBlank(annotation)) {
        return;
    }
    // we need to register attr directives for all possible binding types
    if (provider_util_1.isDirective(annotation)) {
        angular1Module[moduleMethod](name, value);
        angular1Module[moduleMethod]("[" + name + "]", value);
        angular1Module[moduleMethod]("(" + name + ")", value);
    }
    else {
        angular1Module[moduleMethod](name, value);
    }
}
exports._registerTypeProvider = _registerTypeProvider;
//# sourceMappingURL=reflective_provider.js.map

/***/ }),
/* 230 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__(31));
__export(__webpack_require__(30));
__export(__webpack_require__(12));
//# sourceMappingURL=directives.js.map

/***/ }),
/* 231 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var decorators_1 = __webpack_require__(86);
exports.Pipe = decorators_1.Pipe;
//# sourceMappingURL=pipes.js.map

/***/ }),
/* 232 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var change_detection_util_1 = __webpack_require__(78);
exports.SimpleChange = change_detection_util_1.SimpleChange;
var constants_1 = __webpack_require__(233);
exports.ChangeDetectionStrategy = constants_1.ChangeDetectionStrategy;
var change_detector_ref_1 = __webpack_require__(77);
exports.ChangeDetectorRef = change_detector_ref_1.ChangeDetectorRef;
//# sourceMappingURL=change_detection.js.map

/***/ }),
/* 233 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var lang_1 = __webpack_require__(1);
/**
 * Describes the current state of the change detector.
 */
var ChangeDetectorState;
(function (ChangeDetectorState) {
    /**
     * `NeverChecked` means that the change detector has not been checked yet, and
     * initialization methods should be called during detection.
     */
    ChangeDetectorState[ChangeDetectorState["NeverChecked"] = 0] = "NeverChecked";
    /**
     * `CheckedBefore` means that the change detector has successfully completed at least
     * one detection previously.
     */
    ChangeDetectorState[ChangeDetectorState["CheckedBefore"] = 1] = "CheckedBefore";
    /**
     * `Errored` means that the change detector encountered an error checking a binding
     * or calling a directive lifecycle method and is now in an inconsistent state. Change
     * detectors in this state will no longer detect changes.
     */
    ChangeDetectorState[ChangeDetectorState["Errored"] = 2] = "Errored";
})(ChangeDetectorState = exports.ChangeDetectorState || (exports.ChangeDetectorState = {}));
/**
 * Describes within the change detector which strategy will be used the next time change
 * detection is triggered.
 */
var ChangeDetectionStrategy;
(function (ChangeDetectionStrategy) {
    /**
     * `CheckedOnce` means that after calling detectChanges the mode of the change detector
     * will become `Checked`.
     */
    // CheckOnce,
    /**
     * `Checked` means that the change detector should be skipped until its mode changes to
     * `CheckOnce`.
     */
    // Checked,
    /**
     * `CheckAlways` means that after calling detectChanges the mode of the change detector
     * will remain `CheckAlways`.
     */
    // CheckAlways,
    /**
     * `Detached` means that the change detector sub tree is not a part of the main tree and
     * should be skipped.
     */
    // Detached,
    /**
     * `OnPush` means that the change detector's mode will be set to `CheckOnce` during hydration.
     */
    ChangeDetectionStrategy[ChangeDetectionStrategy["OnPush"] = 0] = "OnPush";
    /**
     * `Default` means that the change detector's mode will be set to `CheckAlways` during hydration.
     */
    ChangeDetectionStrategy[ChangeDetectionStrategy["Default"] = 1] = "Default";
})(ChangeDetectionStrategy = exports.ChangeDetectionStrategy || (exports.ChangeDetectionStrategy = {}));
/**
 * List of possible {@link ChangeDetectionStrategy} values.
 */
exports.CHANGE_DETECTION_STRATEGY_VALUES = [
    // ChangeDetectionStrategy.CheckOnce,
    // ChangeDetectionStrategy.Checked,
    // ChangeDetectionStrategy.CheckAlways,
    // ChangeDetectionStrategy.Detached,
    0 /* OnPush */,
    1 /* Default */
];
/**
 * List of possible {@link ChangeDetectorState} values.
 */
exports.CHANGE_DETECTOR_STATE_VALUES = [
    0 /* NeverChecked */,
    1 /* CheckedBefore */,
    2 /* Errored */
];
function isDefaultChangeDetectionStrategy(changeDetectionStrategy) {
    return lang_1.isBlank(changeDetectionStrategy) || changeDetectionStrategy === 1 /* Default */;
}
exports.isDefaultChangeDetectionStrategy = isDefaultChangeDetectionStrategy;
//# sourceMappingURL=constants.js.map

/***/ }),
/* 234 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Public API for Facade
// None :)

var type_1 = __webpack_require__(76);
exports.Type = type_1.Type;
var async_1 = __webpack_require__(79);
exports.EventEmitter = async_1.EventEmitter;
//# sourceMappingURL=facade.js.map

/***/ }),
/* 235 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(55);
var core_1 = __webpack_require__(0);
var scm_stop_click_propagation_1 = __webpack_require__(236);
var scm_stop_enter_propagation_1 = __webpack_require__(237);
var scmNodeDetail_component_1 = __webpack_require__(238);
var scmNodeLink_component_1 = __webpack_require__(239);
var windowsUser_pipe_1 = __webpack_require__(240);
var scm_set_tabs_href_1 = __webpack_require__(241);
var scmWho_component_1 = __webpack_require__(242);
var scmLastChangeDate_component_1 = __webpack_require__(243);
var datetime_pipe_1 = __webpack_require__(244);
var scmApi_service_provider_1 = __webpack_require__(245);
var durationValidator_1 = __webpack_require__(246);
var scmMultilineText_component_1 = __webpack_require__(247);
var common_1 = __webpack_require__(248);
var scmPollingMethodWithIcon_component_1 = __webpack_require__(253);
var scmProfileIndications_component_1 = __webpack_require__(254);
var scmProfileAssignedToNode_component_1 = __webpack_require__(255);
var scmPolicyProfileIndications_component_1 = __webpack_require__(256);
var xuiTag_component_1 = __webpack_require__(257);
var xui_expander_remove_content_directive_1 = __webpack_require__(259);
var scmEnableWhoDetectionDialog_component_1 = __webpack_require__(94);
var whoExclusionList_service_1 = __webpack_require__(54);
var whoIsNotAvailable_component_1 = __webpack_require__(260);
var tooltipEllipsis_directive_1 = __webpack_require__(261);
var elementName_component_1 = __webpack_require__(263);
var nodeConfig_service_1 = __webpack_require__(140);
var nodeConfig_issue_service_1 = __webpack_require__(52);
var commandLineValidator_1 = __webpack_require__(267);
var scmTestJobResultIcon_component_1 = __webpack_require__(268);
var scmInitialPollIcon_component_1 = __webpack_require__(269);
var scmVersionInfo_component_1 = __webpack_require__(270);
var scmIFrameV2_component_1 = __webpack_require__(271);
var UtilsModule = /** @class */ (function () {
    function UtilsModule() {
    }
    UtilsModule_1 = UtilsModule;
    // allows multiple widgets to use the utils module
    // if there are multiple widgets one one page they use same angular/`apolloisolate` environment
    //    so it causes problems when same module is registered multiple times
    // the workaround is to create global variable indicating that this module was already defined
    // better solution will be implemented as part of SCM-2680
    UtilsModule.register = function () {
        if (!angular.utilsModuleLoaded) {
            var Ng1NgMetadataModule = core_1.bundle(UtilsModule_1, []).name;
            Xui.registerModule("widgets").app().requires.push(Ng1NgMetadataModule);
            angular.utilsModuleLoaded = true;
        }
    };
    UtilsModule = UtilsModule_1 = __decorate([
        core_1.NgModule({
            declarations: [
                // components registrations
                scmNodeDetail_component_1.NodeDetailComponent,
                scmNodeLink_component_1.NodeLinkComponent,
                scmWho_component_1.WhoComponent,
                scmLastChangeDate_component_1.LastChangeDateComponent,
                scmMultilineText_component_1.MultilineTextComponent,
                scmPollingMethodWithIcon_component_1.PollingMethodWithIconComponent,
                scmProfileIndications_component_1.ProfileIndicationsComponent,
                scmProfileAssignedToNode_component_1.ProfileAssignedToNodeComponent,
                scmPolicyProfileIndications_component_1.PolicyProfileIndicationsComponent,
                xuiTag_component_1.TagComponent,
                whoIsNotAvailable_component_1.WhoIsNotAvailableComponent,
                elementName_component_1.ElementNameComponent,
                scmTestJobResultIcon_component_1.TestJobResultIconComponent,
                scmInitialPollIcon_component_1.InitialPollIconComponent,
                scmVersionInfo_component_1.VersionInfoComponent,
                scmIFrameV2_component_1.IFrameComponentV2,
                // directives registration
                scm_stop_click_propagation_1.StopClickPropagationDirective,
                scm_stop_enter_propagation_1.StopEnterPropagationDirective,
                scm_set_tabs_href_1.SetTabsHrefDirective,
                durationValidator_1.DurationValidator,
                commandLineValidator_1.CommandLineValidator,
                xui_expander_remove_content_directive_1.XuiExpanderRemoveContent,
                tooltipEllipsis_directive_1.TooltipEllipsis,
                // pipes registration
                windowsUser_pipe_1.WindowsUserPipe,
                datetime_pipe_1.DatetimeFilter,
                common_1.AsyncPipe
            ],
            providers: [
                scmApi_service_provider_1.ScmApiProvider,
                scmEnableWhoDetectionDialog_component_1.EnableWhoDetectionDialog,
                whoExclusionList_service_1.WhoExclusionListService,
                nodeConfig_service_1.NodeConfigService,
                nodeConfig_issue_service_1.NodeConfigIssueService
            ]
        })
    ], UtilsModule);
    return UtilsModule;
    var UtilsModule_1;
}());
exports.UtilsModule = UtilsModule;


/***/ }),
/* 236 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
 * @ngdoc directive
 * @name scmStopClickPropagation
 *
 * @description
 *
 * In case there is component which accept "Click" (like button in another clickable area)
 * This directive prevents from invoking event on parent elements
 *
 * @usage
 *
 * <button scm-stop-click-propagation="true" ... />
 * <button scm-stop-click-propagation ... />
 **/
var StopClickPropagationDirective = /** @class */ (function () {
    function StopClickPropagationDirective() {
    }
    StopClickPropagationDirective.prototype.onClick = function (event) {
        if ((!_.isNil(this.scmStopClickPropagation) && this.scmStopClickPropagation) ||
            _.isNil(this.scmStopClickPropagation)) {
            event.preventDefault();
        }
    };
    __decorate([
        core_1.Input("<")
    ], StopClickPropagationDirective.prototype, "scmStopClickPropagation", void 0);
    __decorate([
        core_1.HostListener("click", ["$event"])
    ], StopClickPropagationDirective.prototype, "onClick", null);
    StopClickPropagationDirective = __decorate([
        core_1.Directive({
            selector: "[scmStopClickPropagation]",
        })
    ], StopClickPropagationDirective);
    return StopClickPropagationDirective;
}());
exports.StopClickPropagationDirective = StopClickPropagationDirective;


/***/ }),
/* 237 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
 * @ngdoc directive
 * @name scmStopEnterPropagation
 *
 * @description
 *
 * In case there is component which accept "Enter" (like xuiSearch or xuiGrid) this directive prevent from submitting parent form
 * this is must-have on widgets (on any component that has search text), since they can be opened in detached mode - where is Form
 *
 * @usage
 *
 * <xui-search scm-stop-enter-propagation ... />
 **/
var StopEnterPropagationDirective = /** @class */ (function () {
    function StopEnterPropagationDirective($element) {
        this.$element = $element;
    }
    StopEnterPropagationDirective.prototype.ngAfterContentInit = function () {
        this.$element.bind("keydown", function (event) {
            if (event.which === 13) {
                event.preventDefault();
            }
        });
    };
    StopEnterPropagationDirective = __decorate([
        core_1.Directive({
            selector: "[scmStopEnterPropagation]"
        }),
        __param(0, core_1.Inject("$element"))
    ], StopEnterPropagationDirective);
    return StopEnterPropagationDirective;
}());
exports.StopEnterPropagationDirective = StopEnterPropagationDirective;


/***/ }),
/* 238 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var NodeDetailComponent = /** @class */ (function () {
    function NodeDetailComponent() {
    }
    __decorate([
        core_1.Input()
    ], NodeDetailComponent.prototype, "node", void 0);
    __decorate([
        core_1.Input()
    ], NodeDetailComponent.prototype, "showMonitoredSince", void 0);
    __decorate([
        core_1.Input()
    ], NodeDetailComponent.prototype, "isFimEnabledGlobally", void 0);
    __decorate([
        core_1.Input()
    ], NodeDetailComponent.prototype, "openLinksOnNewTab", void 0);
    NodeDetailComponent = __decorate([
        core_1.Component({
            selector: "scm-node-detail",
            template: __webpack_require__(125),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], NodeDetailComponent);
    return NodeDetailComponent;
}());
exports.NodeDetailComponent = NodeDetailComponent;


/***/ }),
/* 239 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var NodeLinkComponent = /** @class */ (function () {
    function NodeLinkComponent() {
    }
    NodeLinkComponent.prototype.ngOnInit = function () {
        if (this.showStatus === undefined) {
            this.showStatus = true;
        }
    };
    __decorate([
        core_1.Input()
    ], NodeLinkComponent.prototype, "node", void 0);
    __decorate([
        core_1.Input()
    ], NodeLinkComponent.prototype, "showStatus", void 0);
    NodeLinkComponent = __decorate([
        core_1.Component({
            selector: "scm-node-link",
            template: __webpack_require__(126),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], NodeLinkComponent);
    return NodeLinkComponent;
}());
exports.NodeLinkComponent = NodeLinkComponent;


/***/ }),
/* 240 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var WindowsUserPipe = /** @class */ (function () {
    function WindowsUserPipe() {
    }
    WindowsUserPipe.prototype.transform = function (input, format) {
        if (typeof input !== "string") {
            return input;
        }
        var split = input.split("\\");
        var domain = split[0], user = split[1];
        if (split.length !== 2 || !domain || !user) {
            return input;
        }
        switch (format) {
            case "domain":
                return domain;
            case "user":
                return user;
            case "upn":
                return user + "@" + domain;
            case "legacy":
                return domain + "\\" + user;
            default:
                return input;
        }
    };
    WindowsUserPipe = __decorate([
        core_1.Pipe({
            name: "scmWindowsUser"
        })
    ], WindowsUserPipe);
    return WindowsUserPipe;
}());
exports.WindowsUserPipe = WindowsUserPipe;


/***/ }),
/* 241 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
 * @ngdoc directive
 * @name scmSetTabsHref
 *
 * @usage
 *
 * <xui-tab scm-set-tabs-href ... />
 **/
var SetTabsHrefDirective = /** @class */ (function () {
    function SetTabsHrefDirective($element, $timeout, $state) {
        this.$element = $element;
        this.$timeout = $timeout;
        this.$state = $state;
    }
    SetTabsHrefDirective.prototype.ngAfterContentInit = function () {
        var _this = this;
        var allHrefs = this.extractHrefsFromTabState();
        if (allHrefs.length > 0) {
            // timeout to execute this after everything else is rendered
            this.$timeout(function () {
                var el = _this.$element[0].querySelectorAll(".xui-tabs__header-item-title");
                for (var i = 0; i < allHrefs.length; i++) {
                    el[i].removeAttribute("href");
                    el[i].setAttribute("href", allHrefs[i]);
                }
            });
        }
    };
    SetTabsHrefDirective.prototype.extractHrefsFromTabState = function () {
        var allHrefs = [];
        var tabs = this.$element[0].querySelectorAll("xui-tab");
        for (var i = 0; i < tabs.length; i++) {
            var attribute = tabs[i].getAttribute("tab-id");
            if (attribute) {
                var urlParts = attribute.split(".");
                var joinedIdParts = [];
                var finalUrls = [];
                for (var _i = 0, urlParts_1 = urlParts; _i < urlParts_1.length; _i++) {
                    var part = urlParts_1[_i];
                    joinedIdParts.push(part);
                    var state = this.$state.get(joinedIdParts.join("."));
                    finalUrls.push(state.url);
                }
                var fullUrl = this.removeFirstCharacter(finalUrls.join(""));
                var urlWithoutOptionalParameterAfterQuestionMark = fullUrl.match(/^([^\?]*)/)[0];
                allHrefs.push(urlWithoutOptionalParameterAfterQuestionMark);
            }
            else {
                allHrefs = []; // if some tab has no 'tab-id' attribute do nothing to not corrupt page even more
                break;
            }
        }
        return allHrefs;
    };
    SetTabsHrefDirective.prototype.removeFirstCharacter = function (input) {
        return input.substring(1);
    };
    SetTabsHrefDirective = __decorate([
        core_1.Directive({
            selector: "[scmSetTabsHref]",
            legacy: {
                priority: 10 // this directive must get compiled before tabs are transcluded
            }
        }),
        __param(0, core_1.Inject("$element")),
        __param(1, core_1.Inject("$timeout")),
        __param(2, core_1.Inject("$state"))
    ], SetTabsHrefDirective);
    return SetTabsHrefDirective;
}());
exports.SetTabsHrefDirective = SetTabsHrefDirective;


/***/ }),
/* 242 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(87);

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
__webpack_require__(87);
var WhoComponent = /** @class */ (function () {
    function WhoComponent(_t, swUtil, locationService) {
        this._t = _t;
        this.swUtil = swUtil;
        this.locationService = locationService;
        this.missingWhoPopoverContent = __webpack_require__(127);
    }
    WhoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.locationService.getHelpLink("OrionSCMMonitorWhoMadeServerConfigurationChanges").then(function (value) {
            return _this.helpLink = value;
        });
        this.whoStatusOverlay = this.whoStatusOverlayIconName();
    };
    WhoComponent.prototype.getEllipsisText = function () {
        return this.swUtil.formatString(this._t("Changed by: {0}"), this.lastChange.lastModifiedBy);
    };
    Object.defineProperty(WhoComponent.prototype, "isUserInfoAvailable", {
        get: function () {
            return this.lastChange.lastModifiedByState === "valuedetected" /* ValueDetected */ && this.lastChange.changesCount <= 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WhoComponent.prototype, "missingWhoPopoverDescription", {
        get: function () {
            return this.hasIssue
                ? this._t("The 'who' value is not available due to polling errors.")
                : this.lastChange.lastModifiedByStateDescription;
        },
        enumerable: true,
        configurable: true
    });
    WhoComponent.prototype.whoStatusOverlayIconName = function () {
        if (_.isNil(this.lastChange.lastModifiedBy) || this.lastChange.lastModifiedByState !== "valuedetected" /* ValueDetected */) {
            return "unknown";
        }
        return "";
    };
    __decorate([
        core_1.Input()
    ], WhoComponent.prototype, "lastChange", void 0);
    __decorate([
        core_1.Input()
    ], WhoComponent.prototype, "hasIssue", void 0);
    __decorate([
        core_1.Input()
    ], WhoComponent.prototype, "search", void 0);
    __decorate([
        core_1.Input("@")
    ], WhoComponent.prototype, "iconPlacement", void 0);
    __decorate([
        core_1.Input()
    ], WhoComponent.prototype, "tooltipPosition", void 0);
    WhoComponent = __decorate([
        core_1.Component({
            selector: "scm-who",
            template: __webpack_require__(128),
            legacy: {
                controllerAs: "vm"
            }
        }),
        __param(0, core_1.Inject("getTextService")),
        __param(1, core_1.Inject("swUtil")),
        __param(2, core_1.Inject("scmLocationService"))
    ], WhoComponent);
    return WhoComponent;
}());
exports.WhoComponent = WhoComponent;


/***/ }),
/* 243 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(88);

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
__webpack_require__(88);
var LastChangeDateComponent = /** @class */ (function () {
    function LastChangeDateComponent(_t) {
        this._t = _t;
        this.isInitialPoll = false;
    }
    LastChangeDateComponent.prototype.ngOnInit = function () {
        this.tooltipMsg = this._t("Last change detected");
        if (this.isInitialPoll) {
            this.tooltipMsg = this._t("Monitored since");
        }
    };
    Object.defineProperty(LastChangeDateComponent.prototype, "isHourlyAggregated", {
        get: function () {
            return this.aggregationState === "hourly" /* Hourly */;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LastChangeDateComponent.prototype, "isDailyAggregated", {
        get: function () {
            return this.aggregationState === "archive" /* Archive */
                || this.aggregationState === "daily" /* Daily */;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input()
    ], LastChangeDateComponent.prototype, "lastChange", void 0);
    __decorate([
        core_1.Input()
    ], LastChangeDateComponent.prototype, "originalTimeStamp", void 0);
    __decorate([
        core_1.Input()
    ], LastChangeDateComponent.prototype, "aggregationState", void 0);
    __decorate([
        core_1.Input()
    ], LastChangeDateComponent.prototype, "isInitialPoll", void 0);
    LastChangeDateComponent = __decorate([
        core_1.Component({
            selector: "scm-last-change-date",
            template: __webpack_require__(129),
            legacy: {
                controllerAs: "vm"
            }
        }),
        __param(0, core_1.Inject("getTextService"))
    ], LastChangeDateComponent);
    return LastChangeDateComponent;
}());
exports.LastChangeDateComponent = LastChangeDateComponent;


/***/ }),
/* 244 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var DatetimeFilter = /** @class */ (function () {
    function DatetimeFilter(_t) {
        this._t = _t;
    }
    DatetimeFilter.prototype.transform = function (input, format, ifFalsy) {
        if (_.isNil(input)) {
            return undefined;
        }
        if (!input && ifFalsy) {
            return this._t(ifFalsy);
        }
        var momented = moment(new Date(input));
        if (typeof input === "string" || !momented.isValid()) {
            return input;
        }
        switch (format) {
            case "calendar":
                return momented.calendar(null, {
                    lastWeek: "lll",
                    sameElse: "lll"
                });
            case "long":
                return momented.format("lll");
            case "timeOnly":
                return momented.format("LT");
            default:
                return momented.format(input);
        }
    };
    DatetimeFilter = __decorate([
        core_1.Pipe({
            name: "scmDatetime"
        }),
        __param(0, core_1.Inject("getTextService"))
    ], DatetimeFilter);
    return DatetimeFilter;
}());
exports.DatetimeFilter = DatetimeFilter;


/***/ }),
/* 245 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ScmApiService = /** @class */ (function () {
    function ScmApiService() {
    }
    ScmApiService.prototype.visitObject = function (object) {
        for (var key in object) {
            if (typeof object[key] === "object") {
                this.visitObject(object[key]);
            }
            else if (typeof object[key] === "string") {
                if (/\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(Z|(\+|\-)\d\d:\d\d)/.test(object[key])) {
                    var date = new Date(object[key]);
                    if (moment(date).isValid()) {
                        object[key] = date;
                    }
                }
            }
        }
    };
    ScmApiService.prototype.scmApiFactory = function () {
        var _this = this;
        return function (swApi) {
            return swApi.api(false)
                .withConfig(function (provider) {
                provider.addResponseInterceptor(function (data) {
                    _this.visitObject(data);
                    return data;
                });
            })
                .one("scm");
        };
    };
    ScmApiService.prototype.getProvider = function () {
        return { provide: "scmApi", useFactory: this.scmApiFactory(), deps: ["swApi"] };
    };
    return ScmApiService;
}());
exports.ScmApiService = ScmApiService;
exports.ScmApiProvider = new ScmApiService().getProvider();


/***/ }),
/* 246 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
 * This directive serves as a validator, parser and formatter for .NET `TimeSpan`s.
 * It uses `moment.Duration` for parsing multiple formats like "1.01:12:32.22" or ISO "P1DT1H12M32.22S"
 * but it always use format "hh:mm:ss" for displaying.
 * You can define min or max values through attributes to be checked.
 *
 * You can use it directly on input:
 *  `<input ng-model="..." scm-duration-validator>`
 * or indirectly through xuiTextbox's validators:
 * `<xui-textbox ng-model="..." validators="scm-duration-validator">`
 *
 * @example
```
 <xui-textbox ng-model="$ctrl.duration" validators="scm-duration-validator,min=00:11:00,max=24:00:00">
   <div ng-message="min">Lower than minimum</div>
   <div ng-message="max">Higher than maximum</div>
   <div ng-message="parse">Invalid format</div>
 </xui-textbox>
```
 */
var DurationValidator = /** @class */ (function () {
    function DurationValidator(ngModel, $element, $attributes) {
        this.ngModel = ngModel;
        this.$element = $element;
        this.$attributes = $attributes;
    }
    DurationValidator_1 = DurationValidator;
    DurationValidator.prototype.ngOnInit = function () {
        var _this = this;
        this.ngModel.$formatters.push(function (modelValue) {
            var duration = moment.duration(modelValue);
            return DurationValidator_1.durationToString(duration);
        });
        this.ngModel.$parsers.push(function (viewValue) {
            if (_this.ngModel.$isEmpty(viewValue)) {
                return viewValue;
            }
            // taken from sources of moment.duration
            // because moment.duration returns 00:00 for invalid format
            var aspNetRegex = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?\d*)?$/;
            var duration = moment.duration(viewValue);
            if (aspNetRegex.test(viewValue)) {
                return DurationValidator_1.durationToTimeSpanCompatibleString(duration);
            }
            else {
                return undefined;
            }
        });
        if (this.$element.attr("min")) {
            this.ngModel.$validators["min"] = function (modelValue, viewValue) {
                var min = _this.$element.attr("min");
                if (!min) {
                    return true;
                }
                return moment.duration(modelValue) >= moment.duration(min);
            };
            this.$attributes.$observe("min", function () { return _this.ngModel.$validate(); });
        }
        if (this.$element.attr("max")) {
            this.ngModel.$validators["max"] = function (modelValue, viewValue) {
                var max = _this.$element.attr("max");
                if (!max || moment.duration(max).valueOf() === 0) {
                    return true;
                }
                return moment.duration(modelValue) <= moment.duration(max);
            };
            this.$attributes.$observe("max", function () { return _this.ngModel.$validate(); });
        }
        // Because of some design decision in xuiTextbox, an error is not shown, unless the textbox was $touched.
        // So if we load invalid data, no error is shown but form cannot be submitted.
        this.ngModel.$setTouched();
    };
    DurationValidator.durationToString = function (duration) {
        var pad = function (string, length) {
            if (length === void 0) { length = 2; }
            return _.padStart(string.toString(), length, "0");
        };
        var sign = duration.asMilliseconds() < 0 ? "-" : "";
        return sign + pad(Math.floor(Math.abs(duration.asHours())))
            + ":" + pad(Math.abs(duration.minutes()))
            + ":" + pad(Math.abs(duration.seconds()));
    };
    DurationValidator.durationToTimeSpanCompatibleString = function (duration) {
        var sign = duration.asMilliseconds() < 0 ? "-" : "";
        return sign + Math.floor(Math.abs(duration.asDays()))
            + "." + Math.abs(duration.hours())
            + ":" + Math.abs(duration.minutes())
            + ":" + Math.abs(duration.seconds())
            + "." + Math.abs(duration.milliseconds());
    };
    DurationValidator = DurationValidator_1 = __decorate([
        core_1.Directive({
            selector: "[scm-duration-validator]",
        }),
        __param(0, core_1.Inject("ngModel")), __param(0, core_1.Host()),
        __param(1, core_1.Inject("$element")),
        __param(2, core_1.Inject("$attrs"))
    ], DurationValidator);
    return DurationValidator;
    var DurationValidator_1;
}());
exports.DurationValidator = DurationValidator;


/***/ }),
/* 247 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(89);

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
__webpack_require__(89);
var MultilineTextComponent = /** @class */ (function () {
    function MultilineTextComponent() {
    }
    __decorate([
        core_1.Input()
    ], MultilineTextComponent.prototype, "text", void 0);
    MultilineTextComponent = __decorate([
        core_1.Component({
            selector: "scm-multiline-text",
            template: __webpack_require__(130),
            legacy: {
                controllerAs: "vm",
                transclude: true
            }
        })
    ], MultilineTextComponent);
    return MultilineTextComponent;
}());
exports.MultilineTextComponent = MultilineTextComponent;


/***/ }),
/* 248 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
//import {} from './src/common/services';

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__(249));
__export(__webpack_require__(251));
//# sourceMappingURL=common.js.map

/***/ }),
/* 249 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ng_form_1 = __webpack_require__(90);
exports.NgForm = ng_form_1.NgForm;
var ng_model_1 = __webpack_require__(91);
exports.NgModel = ng_model_1.NgModel;
var ng_select_1 = __webpack_require__(92);
exports.NgSelect = ng_select_1.NgSelect;
var core_directives_1 = __webpack_require__(250);
exports.CORE_DIRECTIVES = core_directives_1.CORE_DIRECTIVES;
//# sourceMappingURL=directives.js.map

/***/ }),
/* 250 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ng_form_1 = __webpack_require__(90);
var ng_model_1 = __webpack_require__(91);
var ng_select_1 = __webpack_require__(92);
/**
 * A collection of Angular core directives that are likely to be used in each and every Angular
 * application.
 *
 * This collection can be used to quickly enumerate all the built-in directives in the `directives`
 * property of the `@Component` annotation.
 *
 * ### Example ([live demo](http://plnkr.co/edit/yakGwpCdUkg0qfzX5m8g?p=preview))
 *
 * Instead of writing:
 *
 * ```typescript
 * import {NgClass, NgIf, NgFor, NgSwitch, NgSwitchWhen, NgSwitchDefault} from 'angular2/common';
 * import {OtherDirective} from './myDirectives';
 *
 * @Component({
 *   selector: 'my-component',
 *   templateUrl: 'myComponent.html',
 *   directives: [NgClass, NgIf, NgFor, NgSwitch, NgSwitchWhen, NgSwitchDefault, OtherDirective]
 * })
 * export class MyComponent {
 *   ...
 * }
 * ```
 * one could import all the core directives at once:
 *
 * ```typescript
 * import {CORE_DIRECTIVES} from 'angular2/common';
 * import {OtherDirective} from './myDirectives';
 *
 * @Component({
 *   selector: 'my-component',
 *   templateUrl: 'myComponent.html',
 *   directives: [CORE_DIRECTIVES, OtherDirective]
 * })
 * export class MyComponent {
 *   ...
 * }
 * ```
 */
exports.CORE_DIRECTIVES = [
    ng_form_1.NgForm,
    ng_model_1.NgModel,
    ng_select_1.NgSelect
];
//# sourceMappingURL=core_directives.js.map

/***/ }),
/* 251 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @module
 * @description
 * This module provides a set of common Pipes.
 */

var async_pipe_1 = __webpack_require__(93);
exports.AsyncPipe = async_pipe_1.AsyncPipe;
var common_pipes_1 = __webpack_require__(252);
exports.COMMON_PIPES = common_pipes_1.COMMON_PIPES;
//# sourceMappingURL=pipes.js.map

/***/ }),
/* 252 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * @module
 * @description
 * This module provides a set of common Pipes.
 */
var async_pipe_1 = __webpack_require__(93);
/**
 * A collection of Angular core pipes that are likely to be used in each and every
 * application.
 *
 * This collection can be used to quickly enumerate all the built-in pipes in the `pipes`
 * property of the `@Component` decorator.
 *
 * @experimental Contains i18n pipes which are experimental
 */
exports.COMMON_PIPES = [
    async_pipe_1.AsyncPipe
];
//# sourceMappingURL=common_pipes.js.map

/***/ }),
/* 253 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var PollingMethodWithIconComponent = /** @class */ (function () {
    function PollingMethodWithIconComponent() {
    }
    PollingMethodWithIconComponent.prototype.pollingMethodIcon = function () {
        var pollingMethodIconCode;
        if (!_.isNil(this.node.pollingMethod)) {
            if (this.node.pollingMethod === "WMI") {
                pollingMethodIconCode = "wminode";
            }
            else if (this.node.pollingMethod === "SNMP") {
                pollingMethodIconCode = "snmpnode";
            }
            else if (this.node.pollingMethod === "Agent") {
                pollingMethodIconCode = "orion-agent";
            }
            else {
                pollingMethodIconCode = "icmpnode";
            }
        }
        else {
            pollingMethodIconCode = "unknownnode";
        }
        return pollingMethodIconCode;
    };
    PollingMethodWithIconComponent.prototype.showAgentDeployed = function () {
        if (this.node.pollingMethod !== "Agent") {
            return this.node.isAgentDeployed;
        }
        // we will not show the text Agent deployed for Agent nodes because its redundant info
        return false;
    };
    __decorate([
        core_1.Input()
    ], PollingMethodWithIconComponent.prototype, "node", void 0);
    PollingMethodWithIconComponent = __decorate([
        core_1.Component({
            selector: "scm-polling-method-with-icon",
            template: __webpack_require__(131),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], PollingMethodWithIconComponent);
    return PollingMethodWithIconComponent;
}());
exports.PollingMethodWithIconComponent = PollingMethodWithIconComponent;


/***/ }),
/* 254 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var ProfileIndicationsComponent = /** @class */ (function () {
    function ProfileIndicationsComponent() {
    }
    ProfileIndicationsComponent.prototype.getMismatchesCount = function (item) {
        if (_.isNil(item.mismatches)) {
            return 0;
        }
        return item.mismatches.addedCount + item.mismatches.changedCount + item.mismatches.deletedCount;
    };
    __decorate([
        core_1.Input()
    ], ProfileIndicationsComponent.prototype, "node", void 0);
    __decorate([
        core_1.Input()
    ], ProfileIndicationsComponent.prototype, "showLastChange", void 0);
    __decorate([
        core_1.Input()
    ], ProfileIndicationsComponent.prototype, "showProfilesExpanded", void 0);
    __decorate([
        core_1.Input()
    ], ProfileIndicationsComponent.prototype, "profileWithErrors", void 0);
    ProfileIndicationsComponent = __decorate([
        core_1.Component({
            selector: "scm-profile-indications",
            template: __webpack_require__(132),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], ProfileIndicationsComponent);
    return ProfileIndicationsComponent;
}());
exports.ProfileIndicationsComponent = ProfileIndicationsComponent;


/***/ }),
/* 255 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var ProfileAssignedToNodeComponent = /** @class */ (function () {
    function ProfileAssignedToNodeComponent() {
    }
    ProfileAssignedToNodeComponent.prototype.getProfileStatusIcon = function (profile) {
        var res = this.getProfileStatus(profile);
        if (res !== "") {
            return "status_" + res;
        }
        return "";
    };
    ProfileAssignedToNodeComponent.prototype.getProfileStatus = function (profile) {
        var res = _(this.profileWithErrors).find(function (x) { return x.profileId === profile.id; });
        if (res != null) {
            return "warning";
        }
        return "";
    };
    __decorate([
        core_1.Input()
    ], ProfileAssignedToNodeComponent.prototype, "node", void 0);
    __decorate([
        core_1.Input()
    ], ProfileAssignedToNodeComponent.prototype, "showProfilesExpanded", void 0);
    __decorate([
        core_1.Input()
    ], ProfileAssignedToNodeComponent.prototype, "profileWithErrors", void 0);
    ProfileAssignedToNodeComponent = __decorate([
        core_1.Component({
            selector: "scm-profile-assigned-to-node",
            template: __webpack_require__(133),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], ProfileAssignedToNodeComponent);
    return ProfileAssignedToNodeComponent;
}());
exports.ProfileAssignedToNodeComponent = ProfileAssignedToNodeComponent;


/***/ }),
/* 256 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var PolicyProfileIndicationsComponent = /** @class */ (function () {
    function PolicyProfileIndicationsComponent() {
    }
    __decorate([
        core_1.Input()
    ], PolicyProfileIndicationsComponent.prototype, "node", void 0);
    __decorate([
        core_1.Input()
    ], PolicyProfileIndicationsComponent.prototype, "showPolicyProfilesExpanded", void 0);
    PolicyProfileIndicationsComponent = __decorate([
        core_1.Component({
            selector: "scm-policy-profile-indications",
            template: __webpack_require__(134),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], PolicyProfileIndicationsComponent);
    return PolicyProfileIndicationsComponent;
}());
exports.PolicyProfileIndicationsComponent = PolicyProfileIndicationsComponent;


/***/ }),
/* 257 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
__webpack_require__(258);
var TagComponent = /** @class */ (function () {
    function TagComponent(swUtil, $scope, $element, $attrs) {
        this.swUtil = swUtil;
        this.$scope = $scope;
        this.$element = $element;
        this.$attrs = $attrs;
        this.onIconClick = new core_1.EventEmitter();
        this.onRemoveIconClick = new core_1.EventEmitter();
        this.MaxAllowedWidth = 150;
        // those constants are useful for calculation of max width
        this.ConstantIncrementWithTwoIcons = 48; // delete icon present, so count with smaller padding right
        this.ConstantIncrementWithOneIcon = 40;
        this.ConstantIncrementWithDeleteOnly = 32; // delete icon present, so count with smaller padding right
        this.ConstantIncrementWithNoIcon = 24;
    }
    TagComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var border = this.$element.find(".xui-tag__border");
        var ellipsisInner = this.$element.find(".xui-ellipsis-inner__text");
        var watchDeregistration = this.$scope.$watch(function () {
            if (ellipsisInner.length > 0) {
                return ellipsisInner[0].getBoundingClientRect().width;
            }
            else {
                return null;
            }
        }, function (newTextWidth) {
            _this.innerTextWidth = newTextWidth;
            if (newTextWidth && !_this.noMaxWidth) {
                if (border.length > 0) {
                    var newMaxWidth = _this.getMaxWidth(newTextWidth);
                    border[0].style.maxWidth = newMaxWidth + "px";
                    // for IE11 where flex-basis=0 collapse whole element to 0 width we must set also minWidth
                    if (_this.swUtil.Browser.isIE()) {
                        _this.$element[0].style.minWidth = newMaxWidth + "px";
                    }
                }
            }
        });
        this.$element.addClass(this.getIconClass());
        this.$scope.$on("$destroy", function () {
            watchDeregistration();
        });
    };
    TagComponent.prototype.getMaxWidth = function (newValue) {
        var maxWidth = newValue + this.getWidthIncrementedToText();
        return maxWidth >= this.MaxAllowedWidth ? this.MaxAllowedWidth : maxWidth;
    };
    TagComponent.prototype.getBorderClasses = function () {
        var classes = [];
        if (this.$attrs["color"]) {
            classes.push("xui-tag__color-" + this.$attrs["color"]);
        }
        if (this.showRemoveIcon) {
            classes.push("xui-tag__has-remove-button");
        }
        if (this.noMaxWidth) {
            classes.push("xui-tag__no-max-width");
        }
        return classes;
    };
    TagComponent.prototype.clickRemoveIcon = function () {
        this.onRemoveIconClick.emit(undefined);
    };
    TagComponent.prototype.clickIcon = function () {
        this.onIconClick.emit(undefined);
    };
    TagComponent.prototype.getWidthIncrementedToText = function () {
        if (this.hasTwoIcons()) {
            return this.ConstantIncrementWithTwoIcons; // two icons + borders
        }
        else if (this.showRemoveIcon) {
            return this.ConstantIncrementWithDeleteOnly; // one icon + borders
        }
        else if (this.hasOneIcon()) {
            return this.ConstantIncrementWithOneIcon; // one icon + borders
        }
        else {
            return this.ConstantIncrementWithNoIcon; // no icons
        }
    };
    TagComponent.prototype.hasOneIcon = function () {
        return this.showRemoveIcon || this.icon;
    };
    TagComponent.prototype.hasTwoIcons = function () {
        return this.showRemoveIcon && this.icon;
    };
    TagComponent.prototype.getIconClass = function () {
        if (this.hasTwoIcons()) {
            return "xui-tag__two-icons";
        }
        else if (this.hasOneIcon()) {
            return "xui-tag__one-icon";
        }
        else {
            return "xui-tag__no-icon";
        }
    };
    __decorate([
        core_1.Input("@")
    ], TagComponent.prototype, "text", void 0);
    __decorate([
        core_1.Input("<")
    ], TagComponent.prototype, "showRemoveIcon", void 0);
    __decorate([
        core_1.Input("@")
    ], TagComponent.prototype, "icon", void 0);
    __decorate([
        core_1.Input("@")
    ], TagComponent.prototype, "iconColor", void 0);
    __decorate([
        core_1.Input("@")
    ], TagComponent.prototype, "removeIconColor", void 0);
    __decorate([
        core_1.Input("@")
    ], TagComponent.prototype, "iconPosition", void 0);
    __decorate([
        core_1.Input("<")
    ], TagComponent.prototype, "noMaxWidth", void 0);
    __decorate([
        core_1.Output()
    ], TagComponent.prototype, "onIconClick", void 0);
    __decorate([
        core_1.Output()
    ], TagComponent.prototype, "onRemoveIconClick", void 0);
    TagComponent = __decorate([
        core_1.Component({
            selector: "xui-tag",
            template: __webpack_require__(135),
            legacy: {
                controllerAs: "vm"
            }
        }),
        __param(0, core_1.Inject("swUtil")),
        __param(1, core_1.Inject("$scope")),
        __param(2, core_1.Inject("$element")),
        __param(3, core_1.Inject("$attrs"))
    ], TagComponent);
    return TagComponent;
}());
exports.TagComponent = TagComponent;


/***/ }),
/* 258 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 259 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
 * `xui-expander` uses `ng-if` to remove its content when not expanded. This is not desirable for complex content like
 * filtered list, which would discard its whole state every time it was hidden. So this directive, when applied to the
 * `xui-expander` blocks the controlling variable for `ng-if`.
 *
 * @usage
 *  <xui-expander xui-expander-remove-content="false">
 *      <xui-expander-heading>
 *          Header
 *      </xui-expander-heading>
 *      <xui-filtered-list-v2
 *          state="vm.state"
 *          dispatcher="vm.dispatcher" />
 *  </xui-expander>
 **/
var XuiExpanderRemoveContent = /** @class */ (function () {
    function XuiExpanderRemoveContent(expander) {
        this.expander = expander;
    }
    XuiExpanderRemoveContent.prototype.ngOnInit = function () {
        if (this.value === "false") {
            // xuiExpanderController::removeContent is used in `ng-if` at `ng-transclude`
            // so we replace it by a property that always returns `false`
            Object.defineProperty(this.expander, "removeContent", { get: function () { return false; }, set: angular.noop });
        }
    };
    __decorate([
        core_1.Input("xuiExpanderRemoveContent")
    ], XuiExpanderRemoveContent.prototype, "value", void 0);
    XuiExpanderRemoveContent = __decorate([
        core_1.Directive({
            selector: "[xuiExpanderRemoveContent]",
        }),
        __param(0, core_1.Self()), __param(0, core_1.Inject("xuiExpander"))
    ], XuiExpanderRemoveContent);
    return XuiExpanderRemoveContent;
}());
exports.XuiExpanderRemoveContent = XuiExpanderRemoveContent;


/***/ }),
/* 260 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var scmEnableWhoDetectionDialog_component_1 = __webpack_require__(94);
var WhoIsNotAvailableComponent = /** @class */ (function () {
    function WhoIsNotAvailableComponent(locationService, scmEnableWhoDetectionDialog) {
        this.locationService = locationService;
        this.scmEnableWhoDetectionDialog = scmEnableWhoDetectionDialog;
        this.whoPopoverContent = __webpack_require__(137);
    }
    WhoIsNotAvailableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.locationService.getHelpLink("OrionSCMMonitorWhoMadeServerConfigurationChanges").then(function (value) {
            return _this.helpLink = value;
        });
    };
    WhoIsNotAvailableComponent.prototype.enableWhoDetectionForNode = function () {
        this.scmEnableWhoDetectionDialog.showConfirmDialog(this.nodeId);
    };
    __decorate([
        core_1.Input()
    ], WhoIsNotAvailableComponent.prototype, "nodeId", void 0);
    WhoIsNotAvailableComponent = __decorate([
        core_1.Component({
            selector: "who-is-not-available",
            template: __webpack_require__(138),
            legacy: {
                controllerAs: "vm"
            }
        }),
        __param(0, core_1.Inject("scmLocationService")),
        __param(1, core_1.Inject(scmEnableWhoDetectionDialog_component_1.EnableWhoDetectionDialog))
    ], WhoIsNotAvailableComponent);
    return WhoIsNotAvailableComponent;
}());
exports.WhoIsNotAvailableComponent = WhoIsNotAvailableComponent;


/***/ }),
/* 261 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
__webpack_require__(262);
/**
 * Atribute directive enhancing `uib-tooltip`.
 *
 * This is a lightweight implementation of ellipsis in css with tooltips showing only if the content is wider than its
 * container. It only enhance `uib-tooltip` which should be defined on the same element. Right before showing the tooltip
 * on `mouseenter` it disables the tooltip if there is no overflowing content.
 *
 * @example
 *  <scm-col
 *      uib-tooltip="{{::item.profile}}"
 *      tooltip-ellipsis
 *      tooltip-append-to-body="true">
 *      {{ item.profile }}
 *  </scm-col>
 */
var TooltipEllipsis = /** @class */ (function () {
    function TooltipEllipsis() {
    }
    TooltipEllipsis = __decorate([
        core_1.Directive({
            selector: "[tooltip-ellipsis]",
            legacy: {
                compile: function ($element, $attrs) {
                    if (angular.isUndefined($attrs["tooltipClass"])) {
                        $attrs.$set("tooltipClass", "ellipsis-tooltip");
                    }
                    $attrs.$set("tooltipEnable", "false");
                    return {
                        // using pre-link because we need to register `mouseenter` before `uib-tooltip`
                        pre: function ($scope, element, attrs) {
                            function recheck() {
                                // from definition on MDN:
                                //   scrollWidth (is) width of an element's content, including content not visible on the screen due to overflow
                                //   If the element's content can fit without a need for horizontal scrollbar, its scrollWidth is equal to clientWidth
                                var value = element[0].scrollWidth > element[0].clientWidth + 1; // +1 is for rounding in Edge
                                attrs.$set("tooltipEnable", value.toString());
                            }
                            ;
                            element.on("mouseenter.ellipsis", recheck);
                            $scope.$on("$destroy", function () {
                                return element.off("mouseenter.ellipsis");
                            });
                        }
                    };
                }
            }
        })
    ], TooltipEllipsis);
    return TooltipEllipsis;
}());
exports.TooltipEllipsis = TooltipEllipsis;


/***/ }),
/* 262 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 263 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var ElementNameComponent = /** @class */ (function () {
    function ElementNameComponent(displayService, $transclude, $element, $scope, _t) {
        this.displayService = displayService;
        this.$transclude = $transclude;
        this.$element = $element;
        this.$scope = $scope;
        this._t = _t;
    }
    Object.defineProperty(ElementNameComponent.prototype, "name", {
        get: function () {
            return this.element && this.displayService.getElementName(this.element);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementNameComponent.prototype, "info", {
        get: function () {
            return this.element && this.displayService.getElementNameInfo(this.element);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementNameComponent.prototype, "displayName", {
        get: function () {
            return this.element && this.element.displayName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ElementNameComponent.prototype, "description", {
        get: function () {
            return this.element && this.element.description;
        },
        enumerable: true,
        configurable: true
    });
    ElementNameComponent.prototype.ngOnInit = function () {
        var _this = this;
        var newScope = this.$scope.$parent.$new();
        newScope.$element = this;
        newScope.$watchGroup([
            function () { return _this.element && _this.element.displayName; },
            function () { return _this.element && _this.element.type; },
            function () { return _this.element && _this.element.description; }
        ], function () { return _this.refreshTooltip(); });
        this.$transclude(newScope, function (clone) { return _this.$element.children().append(clone); });
    };
    ElementNameComponent.prototype.refreshTooltip = function () {
        this.tooltipPath = this.displayService.getElementTooltip(this.element);
    };
    __decorate([
        core_1.Input()
    ], ElementNameComponent.prototype, "element", void 0);
    ElementNameComponent = __decorate([
        core_1.Component({
            selector: "scm-element-name",
            template: __webpack_require__(139),
            legacy: {
                controllerAs: "$element",
                transclude: true
            }
        }),
        __param(0, core_1.Inject("scmElementDisplayService")),
        __param(1, core_1.Inject("$transclude")),
        __param(2, core_1.Inject("$element")),
        __param(3, core_1.Inject("$scope")),
        __param(4, core_1.Inject("getTextService"))
    ], ElementNameComponent);
    return ElementNameComponent;
}());
exports.ElementNameComponent = ElementNameComponent;


/***/ }),
/* 264 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiffTypeText = (_a = {},
    _a["added" /* Added */] = "Added",
    _a["deleted" /* Deleted */] = "Removed",
    _a["modified" /* Modified */] = "Modified",
    _a["unchanged" /* Unchanged */] = "Unchanged",
    _a);
var _a;


/***/ }),
/* 265 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileElementTypeText = (_a = {},
    _a["unknown" /* Unknown */] = "?",
    _a["file" /* File */] = "file",
    _a["registry" /* Registry */] = "reg",
    _a["parsedFile" /* ParsedFile */] = "parsedFile",
    _a["swisQuery" /* SwisQuery */] = "SWIS",
    _a["powershell" /* PowerShell */] = "powershell",
    _a["database" /* Database */] = "database",
    _a["script" /* Script */] = "script",
    _a);
exports.ProfileElementTypeFullText = (_b = {},
    _b["unknown" /* Unknown */] = "Unknown",
    _b["file" /* File */] = "File",
    _b["registry" /* Registry */] = "Registry",
    _b["parsedFile" /* ParsedFile */] = "Parsed file",
    _b["swisQuery" /* SwisQuery */] = "Internal query",
    _b["powershell" /* PowerShell */] = "PowerShell script",
    _b["database" /* Database */] = "Database query",
    _b["script" /* Script */] = "Linux script",
    _b);
var _a, _b;


/***/ }),
/* 266 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebApiChangeTypeIcons = (_a = {},
    _a["unknown" /* Unknown */] = "plus",
    _a["add" /* Add */] = "plus",
    _a["update" /* Update */] = "edit",
    _a["remove" /* Remove */] = "minus",
    _a);
exports.WebApiChangeTypeTooltip = (_b = {},
    _b["unknown" /* Unknown */] = "Item discovered",
    _b["add" /* Add */] = "Item added",
    _b["update" /* Update */] = "Item updated",
    _b["remove" /* Remove */] = "Item removed",
    _b);
var _a, _b;


/***/ }),
/* 267 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
 * This directive serves as a validator for commandline - script element property.
 * It checks whether ScriptFile macro is included only once or never
 */
var CommandLineValidator = /** @class */ (function () {
    function CommandLineValidator(ngModel, $element, $attributes, scmValidationService) {
        this.ngModel = ngModel;
        this.$element = $element;
        this.$attributes = $attributes;
        this.scmValidationService = scmValidationService;
    }
    CommandLineValidator.prototype.ngOnInit = function () {
        var _this = this;
        this.ngModel.$parsers.push(function (viewValue) {
            var isValid = _this.scmValidationService.isCommandLineValid(viewValue);
            _this.ngModel.$setValidity("onlyonemacro", isValid);
            return viewValue;
        });
        // Because of some design decision in xuiTextbox, an error is not shown, unless the textbox was $touched.
        // So if we load invalid data, no error is shown but form cannot be submitted.
        this.ngModel.$setTouched();
    };
    CommandLineValidator = __decorate([
        core_1.Directive({
            selector: "[scm-commandline-validator]",
        }),
        __param(0, core_1.Inject("ngModel")), __param(0, core_1.Host()),
        __param(1, core_1.Inject("$element")),
        __param(2, core_1.Inject("$attrs")),
        __param(3, core_1.Inject("scmValidationService"))
    ], CommandLineValidator);
    return CommandLineValidator;
}());
exports.CommandLineValidator = CommandLineValidator;


/***/ }),
/* 268 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var TestJobResultIconComponent = /** @class */ (function () {
    function TestJobResultIconComponent() {
    }
    __decorate([
        core_1.Input()
    ], TestJobResultIconComponent.prototype, "result", void 0);
    TestJobResultIconComponent = __decorate([
        core_1.Component({
            selector: "scm-test-job-result-icon",
            template: __webpack_require__(145),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], TestJobResultIconComponent);
    return TestJobResultIconComponent;
}());
exports.TestJobResultIconComponent = TestJobResultIconComponent;


/***/ }),
/* 269 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var InitialPollIconComponent = /** @class */ (function () {
    function InitialPollIconComponent() {
    }
    InitialPollIconComponent = __decorate([
        core_1.Component({
            selector: "scm-initial-poll-icon",
            template: __webpack_require__(146),
            legacy: {
                controllerAs: "vm"
            }
        })
    ], InitialPollIconComponent);
    return InitialPollIconComponent;
}());
exports.InitialPollIconComponent = InitialPollIconComponent;


/***/ }),
/* 270 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(96);

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
__webpack_require__(96);
var VersionInfoComponent = /** @class */ (function () {
    function VersionInfoComponent(displayService, centralizedSettings, whoService, _t, swUtil) {
        this.displayService = displayService;
        this.centralizedSettings = centralizedSettings;
        this.whoService = whoService;
        this._t = _t;
        this.swUtil = swUtil;
        this.aggregatedChangesPopoverContent = __webpack_require__(147);
    }
    VersionInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.centralizedSettings.getCachedSetting("FimDriverWatchingEnabled")
            .then(function (fimEnabled) {
            _this.isFimEnabledGlobally = fimEnabled || false;
        });
    };
    Object.defineProperty(VersionInfoComponent.prototype, "aggregatedTimeFrame", {
        get: function () {
            var part1 = moment(this.lastChange.lastChange).format("lll");
            var part2 = this.getAggregatedTimeFramePart2();
            return this.swUtil.formatString(this._t("Detected between {0} - {1} ({2})"), part1, part2, moment(this.lastChange.lastChange).fromNow());
        },
        enumerable: true,
        configurable: true
    });
    VersionInfoComponent.prototype.getAggregatedTimeFramePart2 = function () {
        switch (this.lastChange.aggregationState) {
            case "hourly" /* Hourly */:
                return moment(this.lastChange.originalTimeStamp).format("LT");
            case "daily" /* Daily */:
            case "archive" /* Archive */:
                return moment(this.lastChange.originalTimeStamp).format("lll");
        }
    };
    Object.defineProperty(VersionInfoComponent.prototype, "isDataAggregated", {
        get: function () {
            return this.lastChange.aggregationState === "hourly" /* Hourly */
                || this.lastChange.aggregationState === "daily" /* Daily */
                || this.lastChange.aggregationState === "archive" /* Archive */;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VersionInfoComponent.prototype, "shouldShowWho", {
        get: function () {
            return !!this.lastChange.lastModifiedBy || this.isFimEnabledGlobally && this.displayService.isTypePathBased(this.elementType);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VersionInfoComponent.prototype, "shouldShowWhoOnPopover", {
        get: function () {
            if (this.knownContributorsCount === undefined) {
                return;
            }
            return this.knownContributorsCount > 0 || this.isFimEnabledGlobally && this.displayService.isTypePathBased(this.elementType);
        },
        enumerable: true,
        configurable: true
    });
    VersionInfoComponent.prototype.getWhoContributors = function () {
        var _this = this;
        this.whoService.getWhoForAggregatedElement(this.nodeId, this.elementId, this.versionId)
            .then(function (detectedUsers) {
            var unknownContributorsCount = _this.lastChange.changesCount - detectedUsers.length;
            var uniqueUsers = _(detectedUsers).uniq();
            _this.knownContributorsCount = uniqueUsers.size();
            _this.knownContributors = _this.getContributorsMessage(uniqueUsers.value(), _this.knownContributorsCount);
            _this.unknownContributors = _this.getUnknownContributorsMessage(_this.knownContributorsCount, unknownContributorsCount);
        });
    };
    VersionInfoComponent.prototype.getContributorsMessage = function (users, count) {
        if (count === 0) {
            return null;
        }
        if (count === 1) {
            return this.swUtil.formatString(this._t("{0} contributor: {1}"), count, users[0]);
        }
        if (count > 1) {
            return this.swUtil.formatString(this._t("{0} contributors: {1}"), count, users.join(", "));
        }
    };
    VersionInfoComponent.prototype.getUnknownContributorsMessage = function (knownUsersCount, unknownUsersCount) {
        if (knownUsersCount === 0) {
            return this._t("One or more unknown users contributed to the changes");
        }
        if (unknownUsersCount === 0) {
            return null;
        }
        if (unknownUsersCount === 1) {
            return this._t("and one unknown user");
        }
        if (unknownUsersCount > 1) {
            return this._t("and one or more unknown users");
        }
    };
    __decorate([
        core_1.Input()
    ], VersionInfoComponent.prototype, "nodeId", void 0);
    __decorate([
        core_1.Input()
    ], VersionInfoComponent.prototype, "elementId", void 0);
    __decorate([
        core_1.Input()
    ], VersionInfoComponent.prototype, "versionId", void 0);
    __decorate([
        core_1.Input()
    ], VersionInfoComponent.prototype, "lastChange", void 0);
    __decorate([
        core_1.Input()
    ], VersionInfoComponent.prototype, "elementType", void 0);
    __decorate([
        core_1.Input()
    ], VersionInfoComponent.prototype, "hasIssue", void 0);
    __decorate([
        core_1.Input()
    ], VersionInfoComponent.prototype, "whoSearchPhrase", void 0);
    VersionInfoComponent = __decorate([
        core_1.Component({
            selector: "scm-version-info",
            template: __webpack_require__(148),
            legacy: {
                controllerAs: "vm"
            }
        }),
        __param(0, core_1.Inject("scmElementDisplayService")),
        __param(1, core_1.Inject("scmCentralizedSettingsService")),
        __param(2, core_1.Inject("scmWhoService")),
        __param(3, core_1.Inject("getTextService")),
        __param(4, core_1.Inject("swUtil"))
    ], VersionInfoComponent);
    return VersionInfoComponent;
}());
exports.VersionInfoComponent = VersionInfoComponent;


/***/ }),
/* 271 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var di_1 = __webpack_require__(71);
var IFrameComponentV2 = /** @class */ (function () {
    /** @ngInject */
    IFrameComponentV2.$inject = ["$scope", "iframeEventsService"];
    function IFrameComponentV2($scope, iframeEventsService) {
        this.$scope = $scope;
        this.iframeEventsService = iframeEventsService;
        this.busy = true;
        this.subscriptions = [];
    }
    Object.defineProperty(IFrameComponentV2.prototype, "src", {
        get: function () {
            return "/apps/scm/iframe/" + this.route + "?id=" + this.id + "#noredirect";
        },
        enumerable: true,
        configurable: true
    });
    IFrameComponentV2.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.iframe.on("load.iframeDialog", function () {
            _this.$scope.$apply(function () { return _this.busy = false; });
            if (_this.dialog) {
                _this.sendCreate();
                _this.subscriptions.push(_this.subscribeInputs());
                _this.subscriptions.push(_this.subscribeOutputs());
                _this.subscriptions.push(_this.subscribeDialogClose());
                _this.subscriptions.push(_this.subscribeDialogDismiss());
            }
        });
        this.subscriptions.push(function () { return _this.iframe.off("load.iframeDialog"); });
    };
    IFrameComponentV2.prototype.sendCreate = function () {
        // first batch of inputs cannot wait for its own message because it needs to catch ngOnInit of the component
        this.iframeEventsService.publish("fusion.dialog.open.createComponent#" + this.id, __assign({}, this.dialog.data, { inputs: this.dialog.inputQueue }));
        this.dialog.inputQueue.length = 0;
    };
    IFrameComponentV2.prototype.subscribeInputs = function () {
        var _this = this;
        return this.dialog.$scope.$watchCollection(function () { return _this.dialog.inputQueue; }, function (n, o) {
            while (_this.dialog.inputQueue.length) {
                _this.iframeEventsService.publish("fusion.dialog.input#" + _this.id, _this.dialog.inputQueue.pop());
            }
        });
    };
    IFrameComponentV2.prototype.subscribeOutputs = function () {
        var _this = this;
        this.iframeEventsService.subscribe("apollo.dialog.output#" + this.id, function (data) {
            _this.iframeEventsService.publish("fusion.dialog.output#" + _this.id, data);
        });
        return function () { return _this.iframeEventsService.unsubscribe("apollo.dialog.output#" + _this.id); };
    };
    IFrameComponentV2.prototype.subscribeDialogClose = function () {
        var _this = this;
        this.iframeEventsService.subscribe("apollo.dialog.close#" + this.id, function (data) {
            _this.dialog.close(data.result);
            _this.iframeEventsService.publish("fusion.dialog.close#" + _this.id, data);
        });
        return function () { return _this.iframeEventsService.unsubscribe("apollo.dialog.close#" + _this.id); };
    };
    IFrameComponentV2.prototype.subscribeDialogDismiss = function () {
        var _this = this;
        this.iframeEventsService.subscribe("apollo.dialog.dismiss#" + this.id, function (data) {
            _this.dialog.cancel();
            _this.iframeEventsService.publish("fusion.dialog.dismiss#" + _this.id, data);
        });
        return function () { return _this.iframeEventsService.unsubscribe("apollo.dialog.dismiss#" + _this.id); };
    };
    IFrameComponentV2.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var unsubscribe = _a[_i];
            unsubscribe();
        }
    };
    __decorate([
        core_1.Input()
    ], IFrameComponentV2.prototype, "id", void 0);
    __decorate([
        core_1.Input()
    ], IFrameComponentV2.prototype, "route", void 0);
    __decorate([
        core_1.Input()
    ], IFrameComponentV2.prototype, "dialog", void 0);
    __decorate([
        core_1.ViewChild("iframe")
    ], IFrameComponentV2.prototype, "iframe", void 0);
    IFrameComponentV2 = __decorate([
        core_1.Component({
            selector: "scm-iframe-v2",
            template: "\n        <div xui-busy=\"$ctrl.busy\" style=\"height: 100%\">\n            <iframe ng-src=\"{{:: $ctrl.src }}\" width=\"100%\" height=\"100%\"></iframe>\n        </div>",
        }),
        __param(0, di_1.Inject("$scope")),
        __param(1, di_1.Inject("scmIFrameEventsService"))
    ], IFrameComponentV2);
    return IFrameComponentV2;
}());
exports.IFrameComponentV2 = IFrameComponentV2;


/***/ }),
/* 272 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var profileService_1 = __webpack_require__(98);
var profileElementService_1 = __webpack_require__(99);
var polledElementService_1 = __webpack_require__(100);
var orionNodeService_1 = __webpack_require__(101);
var candidatesForMonitoringService_1 = __webpack_require__(102);
var compareConfigService_1 = __webpack_require__(103);
var assignProfileWizardDialogService_1 = __webpack_require__(273);
var locationService_1 = __webpack_require__(56);
var nodesListDialogService_1 = __webpack_require__(274);
var profileTreeSessionService_1 = __webpack_require__(104);
var modalService_1 = __webpack_require__(105);
var baselineService_1 = __webpack_require__(106);
var elementDisplayService_1 = __webpack_require__(107);
var centralizedSettingsService_1 = __webpack_require__(108);
var filteredListService_1 = __webpack_require__(109);
var userSettingsService_1 = __webpack_require__(110);
var retentionService_1 = __webpack_require__(111);
var errorHandlingService_1 = __webpack_require__(112);
var assetInventoryService_1 = __webpack_require__(113);
var validationService_1 = __webpack_require__(114);
var authorizationService_1 = __webpack_require__(115);
var redirectService_1 = __webpack_require__(116);
var widgetsService_1 = __webpack_require__(57);
var scmApiHelperService_1 = __webpack_require__(117);
var testJobService_1 = __webpack_require__(118);
var whoService_1 = __webpack_require__(119);
var credentialService_1 = __webpack_require__(120);
var iframeEventsService_1 = __webpack_require__(276);
var iframeDialogService_1 = __webpack_require__(277);
var profileService_2 = __webpack_require__(98);
exports.ProfileService = profileService_2.ProfileService;
var profileElementService_2 = __webpack_require__(99);
exports.ProfileElementService = profileElementService_2.ProfileElementService;
var polledElementService_2 = __webpack_require__(100);
exports.PolledElementService = polledElementService_2.PolledElementService;
var orionNodeService_2 = __webpack_require__(101);
exports.OrionNodeService = orionNodeService_2.OrionNodeService;
var candidatesForMonitoringService_2 = __webpack_require__(102);
exports.CandidatesForMonitoringService = candidatesForMonitoringService_2.CandidatesForMonitoringService;
var compareConfigService_2 = __webpack_require__(103);
exports.CompareConfigService = compareConfigService_2.CompareConfigService;
var redirectService_2 = __webpack_require__(116);
exports.RedirectService = redirectService_2.RedirectService;
var widgetsService_2 = __webpack_require__(57);
exports.WidgetsService = widgetsService_2.WidgetsService;
var locationService_2 = __webpack_require__(56);
exports.LocationService = locationService_2.LocationService;
var profileTreeSessionService_2 = __webpack_require__(104);
exports.ProfileTreeSessionService = profileTreeSessionService_2.ProfileTreeSessionService;
var modalService_2 = __webpack_require__(105);
exports.ModalService = modalService_2.ModalService;
var baselineService_2 = __webpack_require__(106);
exports.BaselineService = baselineService_2.BaselineService;
var elementDisplayService_2 = __webpack_require__(107);
exports.ElementDisplayService = elementDisplayService_2.ElementDisplayService;
var centralizedSettingsService_2 = __webpack_require__(108);
exports.CentralizedSettingsService = centralizedSettingsService_2.CentralizedSettingsService;
var filteredListService_2 = __webpack_require__(109);
exports.FilteredListService = filteredListService_2.FilteredListService;
var userSettingsService_2 = __webpack_require__(110);
exports.UserSettingsService = userSettingsService_2.UserSettingsService;
var retentionService_2 = __webpack_require__(111);
exports.RetentionService = retentionService_2.RetentionService;
var errorHandlingService_2 = __webpack_require__(112);
exports.ErrorHandlingService = errorHandlingService_2.ErrorHandlingService;
var assetInventoryService_2 = __webpack_require__(113);
exports.AssetInventoryService = assetInventoryService_2.AssetInventoryService;
var validationService_2 = __webpack_require__(114);
exports.ValidationService = validationService_2.ValidationService;
var authorizationService_2 = __webpack_require__(115);
exports.AuthorizationService = authorizationService_2.AuthorizationService;
var scmApiHelperService_2 = __webpack_require__(117);
exports.ApiHelperService = scmApiHelperService_2.ApiHelperService;
var testJobService_2 = __webpack_require__(118);
exports.TestJobService = testJobService_2.TestJobService;
var whoService_2 = __webpack_require__(119);
exports.WhoService = whoService_2.WhoService;
var credentialService_2 = __webpack_require__(120);
exports.CredentialService = credentialService_2.CredentialService;
exports.default = function (module) {
    // register services
    module.service("scmProfileService", profileService_1.ProfileService);
    module.service("scmProfileElementService", profileElementService_1.ProfileElementService);
    module.service("scmPolledElementService", polledElementService_1.PolledElementService);
    module.service("scmOrionNodeService", orionNodeService_1.OrionNodeService);
    module.service("scmCandidatesForMonitoringService", candidatesForMonitoringService_1.CandidatesForMonitoringService);
    module.service("scmCompareConfigService", compareConfigService_1.CompareConfigService);
    module.service("scmAssignProfileWizardDialogService", assignProfileWizardDialogService_1.AssignProfileWizardDialogService);
    module.service("scmLocationService", locationService_1.LocationService);
    module.service("scmNodesListDialogService", nodesListDialogService_1.NodesListDialogService);
    module.service("scmProfileTreeSessionService", profileTreeSessionService_1.ProfileTreeSessionService);
    module.service("scmModalService", modalService_1.ModalService);
    module.service("scmBaselineService", baselineService_1.BaselineService);
    module.service("scmElementDisplayService", elementDisplayService_1.ElementDisplayService);
    module.service("scmCentralizedSettingsService", centralizedSettingsService_1.CentralizedSettingsService);
    module.service("scmFilteredListService", filteredListService_1.FilteredListService);
    module.service("scmUserSettingsService", userSettingsService_1.UserSettingsService);
    module.service("scmRetentionService", retentionService_1.RetentionService);
    module.service("scmErrorHandlingService", errorHandlingService_1.ErrorHandlingService);
    module.service("scmAssetInventoryService", assetInventoryService_1.AssetInventoryService);
    module.service("scmValidationService", validationService_1.ValidationService);
    module.service("scmAuthorizationService", authorizationService_1.AuthorizationService);
    module.service("scmRedirectService", redirectService_1.RedirectService);
    module.service("scmWidgetsService", widgetsService_1.WidgetsService);
    module.service("scmApiHelperService", scmApiHelperService_1.ApiHelperService);
    module.service("scmTestJobService", testJobService_1.TestJobService);
    module.service("scmWhoService", whoService_1.WhoService);
    module.service("scmCredentialService", credentialService_1.CredentialService);
    module.service("scmIFrameEventsService", iframeEventsService_1.IFrameEventsService);
    module.service("scmIFrameDialogService", iframeDialogService_1.IFrameDialogService);
};


/***/ }),
/* 273 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AssignProfileWizardDialogService = /** @class */ (function () {
    /** @ngInject */
    AssignProfileWizardDialogService.$inject = ["$templateCache", "getTextService", "scmModalService"];
    function AssignProfileWizardDialogService($templateCache, getTextService, scmModalService) {
        this.$templateCache = $templateCache;
        this.getTextService = getTextService;
        this.scmModalService = scmModalService;
        this._t = getTextService;
        this.$templateCache.put("scm-assign-profile-wizard-dialog", __webpack_require__(124));
    }
    AssignProfileWizardDialogService.prototype.showModal = function (settings) {
        var wizardOptions = {
            title: this._t("Assign configuration profiles"),
            templateUrl: "scm-assign-profile-wizard-dialog",
            size: "lg",
            viewModel: { initialSettings: settings }
        };
        return this.scmModalService.showModalWizardDialog(wizardOptions);
    };
    return AssignProfileWizardDialogService;
}());
exports.AssignProfileWizardDialogService = AssignProfileWizardDialogService;


/***/ }),
/* 274 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NodesListDialogService = /** @class */ (function () {
    /** @ngInject */
    NodesListDialogService.$inject = ["getTextService", "swUtil", "scmModalService"];
    function NodesListDialogService(getTextService, swUtil, scmModalService) {
        this.getTextService = getTextService;
        this.swUtil = swUtil;
        this.scmModalService = scmModalService;
        this.template = "<xui-dialog>\n    <scm-nodes-assigned-to-profile-list\n        profile=\"vm.dialogOptions.viewModel.profile\"\n        nodes=\"vm.dialogOptions.viewModel.nodes\">\n    </scm-nodes-assigned-to-profile-list>\n</xui-dialog>";
        this._t = getTextService;
    }
    NodesListDialogService.prototype.showModal = function (profile, nodes, title) {
        if (nodes === void 0) { nodes = null; }
        if (title === void 0) { title = null; }
        var dialogueTitle = profile === null
            ? this._t("Selected nodes")
            : this.swUtil.formatString(this._t("Nodes assigned to profile {0}"), profile.name);
        if (title !== null) {
            dialogueTitle = title;
        }
        return this.scmModalService.showModal({
            template: this.template,
            size: "lg",
        }, {
            title: dialogueTitle,
            cancelButtonText: this._t("Cancel"),
            viewModel: { profile: profile, nodes: nodes }
        });
    };
    return NodesListDialogService;
}());
exports.NodesListDialogService = NodesListDialogService;


/***/ }),
/* 275 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilePathType;
(function (FilePathType) {
    FilePathType[FilePathType["windowsPath"] = 0] = "windowsPath";
    FilePathType[FilePathType["linuxPath"] = 1] = "linuxPath";
})(FilePathType = exports.FilePathType || (exports.FilePathType = {}));


/***/ }),
/* 276 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IFrameEventsService = /** @class */ (function () {
    /** @ngInject */
    IFrameEventsService.$inject = ["$log", "$window"];
    function IFrameEventsService($log, $window) {
        var _this = this;
        this.$log = $log;
        this.$window = $window;
        this.channels = {};
        this.handler = function (event) {
            if (!_this.validateEvent(event)) {
                return;
            }
            var subscriptions = _this.channels[event.data.topic];
            if (subscriptions) {
                _this.$log.debug("Apollo: Received subscribed message from Fusion IFrame", event.data);
                subscriptions.forEach(function (handler) { return handler(event.data.data); });
            }
        };
        this.$window.addEventListener("message", this.handler);
    }
    IFrameEventsService.prototype.publish = function (topic, data) {
        if (data === void 0) { data = null; }
        var message = {
            topic: topic,
            data: data,
        };
        this.$log.debug("Apollo: Sending message to all Fusion IFrames", message);
        var iframes = this.$window.document.getElementsByTagName("iframe");
        for (var i = 0; i < iframes.length; i++) {
            iframes[i].contentWindow.postMessage(message, this.$window.location.origin);
        }
    };
    IFrameEventsService.prototype.validateEvent = function (event) {
        var iframes = this.$window.document.getElementsByTagName("iframe");
        var validSource = false;
        for (var i = 0; i < iframes.length; i++) {
            validSource = validSource || iframes[i].contentWindow === event.source;
        }
        var validOrigin = event.origin === this.$window.location.origin;
        var validData = !!(event.data && event.data.topic);
        return validSource && validOrigin && validData;
    };
    IFrameEventsService.prototype.subscribe = function (topic) {
        var _this = this;
        var handlers = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            handlers[_i - 1] = arguments[_i];
        }
        if (!this.channels[topic]) {
            this.channels[topic] = [];
        }
        this.$log.debug("Apollo: Subscribing to " + topic);
        handlers.forEach(function (handler) {
            _this.channels[topic].push(handler);
        });
    };
    IFrameEventsService.prototype.unsubscribe = function (topic, handler) {
        if (handler === void 0) { handler = null; }
        this.$log.debug("Apollo: Unsubscribing from " + topic);
        var t = this.channels[topic];
        if (!t) {
            // Wasn't found, wasn't removed
            return false;
        }
        if (!handler) {
            // Remove all handlers for this topic
            delete this.channels[topic];
            return true;
        }
        // We need to find and remove a specific handler
        var i = t.indexOf(handler);
        if (i < 0) {
            // Wasn't found, wasn't removed
            return false;
        }
        t.splice(i, 1);
        // If the channel is empty now, remove it from the channel map
        if (!t.length) {
            delete this.channels[topic];
        }
        return true;
    };
    return IFrameEventsService;
}());
exports.IFrameEventsService = IFrameEventsService;


/***/ }),
/* 277 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
// taken from @angular/core:application_tokens.ts
function randomChar() {
    return String.fromCharCode(97 + Math.floor(Math.random() * 25));
}
var IFrameDialogService = /** @class */ (function () {
    /** @ngInject */
    IFrameDialogService.$inject = ["$log", "$rootScope", "xuiToastService", "scmModalService", "scmIFrameEventsService", "swDemoService"];
    function IFrameDialogService($log, $rootScope, xuiToastService, scmModalService, scmIFrameEventsService, swDemoService) {
        this.$log = $log;
        this.$rootScope = $rootScope;
        this.xuiToastService = xuiToastService;
        this.scmModalService = scmModalService;
        this.scmIFrameEventsService = scmIFrameEventsService;
        this.swDemoService = swDemoService;
        this.subscribeCreateModal();
        this.subscribeCreateToast();
    }
    /**
     * This method subscribes to a `dialog.open` message from Fusion iframes.
     * In Apollo the handler creates a new modal with an iframe and options from the message,
     * sends inside initial configuration and setups routing of close/dismiss events.
     * At most one controller/directive must be subscribed at any given time.
     *
     * @returns an unsubscriber
     */
    IFrameDialogService.prototype.subscribeCreateModal = function () {
        var _this = this;
        var handler = function (data) {
            if (!/^[a-zA-Z_.-]+$/.test(data.id)) {
                _this.$log.error("Received request for a modal with malformed id");
                return;
            }
            var scope = _.assign(_this.$rootScope.$new(true), { data: data, inputQueue: [] });
            _this.scmIFrameEventsService.subscribe("apollo.dialog.input#" + data.id, function (input) {
                scope.$apply(function () { return scope.inputQueue.push(input); });
            });
            _this.scmModalService.showModal(__assign({}, data.options, { template: "<scm-iframe-v2 id=\"" + data.id + "\" route=\"dialog/" + data.route + "\" [dialog]=\"vm\"></scm-iframe-v2>", scope: scope }), {});
        };
        this.scmIFrameEventsService.subscribe("apollo.dialog.open.createModal", handler);
        return function () { return _this.scmIFrameEventsService.unsubscribe("apollo.dialog.open.createModal", handler); };
    };
    ;
    /**
     * This method subscribes to a `toast.open` message from Fusion iframes.
     * The toast definition in Nova is the same as in NovaJS, so we only pass the data from the message.
     *
     * @returns an unsubscriber
     */
    IFrameDialogService.prototype.subscribeCreateToast = function () {
        var _this = this;
        var openHandler = function (data) {
            if (!_.includes(["info", "success", "warning", "error", "demo"], data.type)) {
                _this.$log.error("Received request for unknown toast type");
            }
            if (data.type === "demo") {
                _this.swDemoService.showDemoErrorToast();
            }
            else {
                _this.xuiToastService[data.type](data.message, data.title, data.options, data.itemsToHightlight);
            }
        };
        this.scmIFrameEventsService.subscribe("apollo.toast.open", openHandler);
        var clearHandler = function () {
            _this.xuiToastService.clear();
        };
        this.scmIFrameEventsService.subscribe("apollo.toast.clear", clearHandler);
        return function () {
            _this.scmIFrameEventsService.unsubscribe("apollo.toast.open", openHandler);
            _this.scmIFrameEventsService.unsubscribe("apollo.toast.clear", clearHandler);
        };
    };
    IFrameDialogService.prototype.showFusionModal = function (route, customSettings, data) {
        // 6 random chars should be unique enough for a modal id
        var id = "_" + randomChar() + randomChar() + randomChar() + randomChar() + randomChar() + randomChar() + "_";
        return this.scmModalService.showModal(__assign({}, customSettings, { template: "<scm-iframe-v2 id=\"" + id + "\" route=\"dialog/" + route + "\" [dialog]=\"vm\"></scm-iframe-v2>", scope: _.assign(this.$rootScope.$new(true), { data: { id: id, route: route, data: data }, inputQueue: [] }) }), {});
    };
    ;
    return IFrameDialogService;
}());
exports.IFrameDialogService = IFrameDialogService;


/***/ }),
/* 278 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmContentMessagePopover_controller_1 = __webpack_require__(121);
var ScmContentMessagePopoverDirective = /** @class */ (function () {
    function ScmContentMessagePopoverDirective() {
        this.scope = {};
        this.restrict = "E";
        this.template = __webpack_require__(97);
        this.transclude = false;
        this.controller = scmContentMessagePopover_controller_1.default;
        this.controllerAs = "contentMessagePopover";
        this.bindToController = {
            contentCollectionState: "@?"
        };
    }
    return ScmContentMessagePopoverDirective;
}());
exports.default = ScmContentMessagePopoverDirective;


/***/ }),
/* 279 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var EscapeHtmlFilter = /** @class */ (function () {
    function EscapeHtmlFilter() {
    }
    EscapeHtmlFilter.factory = function () {
        var factoryFunction = function () {
            // inspired by https://github.com/angular/angular.js/issues/1703#issuecomment-18309081
            return function (text) {
                if (text) {
                    return text.
                        replace(/&/g, "&amp;").
                        replace(/</g, "&lt;").
                        replace(/>/g, "&gt;").
                        replace(/'/g, "&#39;").
                        replace(/"/g, "&quot;");
                }
                return "";
            };
        };
        factoryFunction.$inject = [];
        return factoryFunction;
    };
    return EscapeHtmlFilter;
}());
exports.EscapeHtmlFilter = EscapeHtmlFilter;


/***/ }),
/* 280 */
/***/ (function(module, exports) {

module.exports = "<div class=id-mismatches-popover-content> <div class=\"row-flex item add\" ng-if=\"vm.baselineInfo.mismatches.addedCount !== 0\"> <span class=col-flex> <xui-icon icon-size=xsmall icon=plus icon-color=gray></xui-icon> <span class=count>{{vm.baselineInfo.mismatches.addedCount}}</span> <span _t>Added item(s)</span> </span> </div> <div class=\"row-flex item update\" ng-if=\"vm.baselineInfo.mismatches.changedCount !== 0\"> <span class=col-flex> <xui-icon icon-size=xsmall icon=edit icon-color=gray></xui-icon> <span class=count>{{vm.baselineInfo.mismatches.changedCount}}</span> <span _t>Modified item(s)</span> </span> </div> <div class=\"row-flex item remove\" ng-if=\"vm.baselineInfo.mismatches.deletedCount !== 0\"> <span class=col-flex> <xui-icon icon-size=xsmall icon=minus icon-color=gray></xui-icon> <span class=count>{{vm.baselineInfo.mismatches.deletedCount}}</span> <span _t>Removed item(s)</span> </span> </div> <div class=\"row-flex comparison-link\"> <span class=col-flex> <a ng-href={{vm.getCompareConfigurationsPage()}} target=_blank rel=\"noopener noreferrer\" _t>Show comparison to baseline</a> </span> </div> </div> ";

/***/ }),
/* 281 */
/***/ (function(module, exports) {

module.exports = "<span> <span ng-if=\"vm.baselineInfo.baselineStatus === 'NoBaselineSet'\" class=id-NoBaselineSet _t> <xui-icon icon-size=small icon=baseline icon-color=disabled-gray></xui-icon> <span _t>No baseline set</span> </span> <span ng-if=\"vm.baselineInfo.baselineStatus === 'IsBaseline'\" class=id-IsBaseline _t> <xui-icon icon-size=small icon-color=ok-green icon=baseline></xui-icon> <span _t>Baseline</span> </span> <span ng-if=\"vm.baselineInfo.baselineStatus === 'MatchesBaseline'\" class=id-MatchesBaseline _t> <xui-icon icon-size=small icon-color=ok-green icon=baseline></xui-icon> <xui-icon icon-size=small icon-color=ok-green icon=checkmark></xui-icon> Matches the baseline </span> <span ng-if=\"vm.baselineInfo.baselineStatus === 'DiffersFromBaseline'\" class=\"id-DiffersFromBaseline xui-text-warning\"> <xui-popover class=id-mismatches-popover xui-popover-content=vm.getPopoverTemplate() xui-popover-trigger=mouseenter> <xui-icon icon-size=small icon-color=critical-red icon=baseline-mismatch></xui-icon> {{vm.getMismatchesMsg()}} </xui-popover> </span> </span> ";

/***/ }),
/* 282 */
/***/ (function(module, exports) {

module.exports = "<div class=scm-list-item ng-controller=\"scmProfileListItemController as ctrl\"> <xui-icon css-class=item-part icon=config-profile icon-size=small /> <div class=\"item-part scm-profile-name\"> <span class=\"id-profile-name scm-profile-name\" ng-class=\"{link: vm.$scope.displayToolbar}\" uib-tooltip=\"{{:: item.name }}\" tooltip-ellipsis tooltip-append-to-body=true ng-click=vm.onProfileClick(item)> {{item.name}} </span> </div> <div class=\"id-builtin item-part xui-tag\" ng-if=item.builtIn _t>out-of-the-box</div> <div class=\"id-node-count item-part scm-node-count\" ng-click=ctrl.openAssignedNodesDialog(item)>{{item.nodesCount}}</div> </div> ";

/***/ }),
/* 283 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=id-delete-dialog-content> <div> <b> {{vm.dialogOptions.viewModel.formatSelectedProfiles()}} </b> {{vm.dialogOptions.viewModel.formatConfigurationWarning()}} </div> <xui-message class=xui-margin-lgt type=warning ng-if=\"vm.dialogOptions.viewModel.hiddenProfilesDueToLimitations.length > 0\"> {{vm.dialogOptions.viewModel.formatWarningMessageStart()}} <br> {{vm.dialogOptions.viewModel.formatWarningMessageEnd()}} </xui-message> <br> {{vm.dialogOptions.viewModel.formatSureMessage()}} </div> </xui-dialog> ";

/***/ }),
/* 284 */
/***/ (function(module, exports) {

module.exports = "<div xui-busy=profileList.isBusy xui-busy-message=\"{{ profileList.isBusyMessage }}\" _ta> <xui-filtered-list-v2 class=\"id-profileManagement scm-list scm-list--bordered-items\" state=profileState dispatcher=profileList.profileDispatcher remote-control=profileList.listControl controller=profileList> <xui-if-show visible=displayToolbar> <sw-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button ng-if=profileList.shouldShowAddButton() class=id-add icon=add display-style=tertiary ng-click=profileList.addProfile() _t> Add </xui-button> <xui-button ng-if=profileList.shouldShowAddButton() class=id-import icon=import display-style=tertiary ng-click=profileList.importProfile() _t> Import </xui-button> <xui-button ng-if=profileList.shouldShowEditButton() class=id-edit icon=edit display-style=tertiary ng-click=profileList.editProfile() _t> Edit </xui-button> <xui-button ng-if=profileList.shouldShowCopyButton() class=id-copy icon=copy display-style=tertiary ng-click=profileList.copyProfile() _t> Copy </xui-button> <xui-button ng-if=profileList.shouldShowDetailsButton() class=id-view-details icon=browse display-style=tertiary ng-click=profileList.viewProfile() _t> View details </xui-button> <xui-button ng-if=profileList.shouldShowDeleteButton() class=id-delete-details icon=delete display-style=tertiary ng-click=profileList.showDeleteConfirmationDialog() _t> Delete </xui-button> <xui-button ng-if=profileList.shouldShowAssignButton() class=id-assign-to icon=assign display-style=tertiary ng-click=profileList.assignProfile() _t> Assign to </xui-button> <xui-button ng-if=profileList.shouldShowExportButton() class=id-export icon=export display-style=tertiary ng-click=profileList.exportProfile() _t> Export </xui-button> </xui-toolbar-item> </xui-toolbar> </sw-grid-toolbar-container> </xui-if-show> </xui-filtered-list-v2> </div> ";

/***/ }),
/* 285 */
/***/ (function(module, exports) {

module.exports = "<div xui-busy=monitoredNodeList.isBusy xui-busy-message={{monitoredNodeList.isBusyMessage}}> <xui-filtered-list-v2 class=\"id-monitoredNodes scm-list scm-list--bordered-items\" state=monitoredNodeList.nodeState dispatcher=monitoredNodeList.nodeDispatcher remote-control=monitoredNodeList.listControl controller=monitoredNodeList> <sw-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button ng-if=monitoredNodeList.shouldShowSetupButton() class=id-setup-configuration-monitoring icon=add display-style=tertiary ng-click=monitoredNodeList.setup() _t> Set up Configuration Monitoring </xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button ng-if=monitoredNodeList.shouldShowAssignButton() class=id-assign-profiles icon=assign display-style=tertiary ng-click=monitoredNodeList.assign() _t> Assign Profiles </xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button ng-if=monitoredNodeList.shouldShowAssignButton() class=id-unassign-profiles icon=unassign display-style=tertiary ng-click=monitoredNodeList.unassign() _t> Unassign Profiles </xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button ng-if=monitoredNodeList.shouldShowAssignButton() class=id-unassign-all icon=unassign display-style=tertiary ng-click=monitoredNodeList.unassignAll() _t> Unassign All </xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button ng-if=monitoredNodeList.shouldShowBaselineButtons() class=id-redefine-baseline icon=baseline icon-color=primary-blue display-style=tertiary ng-click=monitoredNodeList.bulkUpdateBaselines() _t> Redefine Baselines </xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button ng-if=monitoredNodeList.shouldShowBaselineButtons() class=id-delete-baseline icon=delete display-style=tertiary ng-click=monitoredNodeList.deleteBaselines() _t> Delete Baselines </xui-button> </xui-toolbar-item> </xui-toolbar> </sw-grid-toolbar-container> </xui-filtered-list-v2> </div> ";

/***/ }),
/* 286 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=scm-element-dialog ng-init=\"dialogViewModel = vm.dialogOptions.viewModel\"> <div sw-entity-popover-loader> <ng-form name=dialogViewModel.profileElementForm> <div class=id-multi-edit-message-window ng-if=dialogViewModel.isMultiEdit> <xui-message name=MultiEditMessage type=info> <span class=\"id-multi-edit-message header-content-underline xui-text-warning\" xui-popover xui-popover-content=\"{url: 'multiedit-element-popover-content'}\" _t>{{ ::dialogViewModel.currentlyEditedElement.length }} configuration elements</span> <span _t> selected. To edit values for all of them at once, select a property and enter a value to be used for all elements.</span> </xui-message> </div> <scm-grid> <scm-row> <scm-col class=scm-edit-element-checkbox-col scm-col-auto ng-if=elementSettings.isMultiEdit></scm-col> <scm-col> <xui-dropdown class=\"id-type xui-dropdown--justified\" is-disabled=::!dialogViewModel.manageProfileElementDialogElementTypeEnabled name=ElementType caption=\"_t(Element type)\" ng-model=dialogViewModel.selectedProfileElementType items-source=dialogViewModel.profileElementTypes display-value=value _ta/> <xui-textbox class=\"id-displayAlias no-pointer\" ng-if=\"dialogViewModel.getElementHasDisplayName() && !dialogViewModel.isMultiEdit\" is-read-only=\"{{ ::!dialogViewModel.manageProfileElementDialogDescriptionEnabled }}\" ng-model=dialogViewModel.currentProfileElement.displayAlias scm-reset-validation-on-change=\"displayAliasLengthError, duplicateProfileElement\" is-required=true xui-set-focus=dialogViewModel.selectedProfileElementType name=ElementDisplayAlias caption=\"_t(Element name)\" help-text=\"_t(This is the name used when referring to this configuration element.)\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required.</div> <div ng-message=duplicateProfileElement class=id-error-profile-exists _t>Element name already exists in this profile.</div> <div ng-message=displayAliasLengthError class=id-displayAlias-length-error _t>Element name is too long. Max length is 256 characters.</div> </xui-textbox> </scm-col> </scm-row> <scm-element-settings profile=dialogViewModel.profile profile-element=dialogViewModel.currentProfileElement mode=::dialogViewModel.manageProfileElementDialogElementSettingsMode show-popover=::dialogViewModel.manageProfileElementDialogShowPopover is-connection-string-required=dialogViewModel.isConnectionStringRequired is-multi-edit=dialogViewModel.isMultiEdit multi-edit-checkbox=dialogViewModel.multiEditCheckbox connection-string-placeholder=dialogViewModel.connectionStringPlaceholder /> <scm-row> <scm-col class=scm-edit-element-checkbox-col scm-col-auto ng-if=elementSettings.isMultiEdit></scm-col> <scm-col> <xui-textbox class=\"id-description no-pointer\" is-read-only=\"{{ ::!dialogViewModel.manageProfileElementDialogDescriptionEnabled }}\" ng-model=dialogViewModel.currentProfileElement.description scm-reset-validation-on-change=descriptionLengthError name=ElementDescription rows=3 caption=_t(Description) ng-if=!dialogViewModel.isMultiEdit _ta> <div ng-message=descriptionLengthError class=id-description-lenght-error _t>Description is too long. Max length is 1M characters.</div> </xui-textbox> </scm-col> </scm-row> <scm-row> <scm-col class=scm-edit-element-checkbox-col scm-col-auto align-self-start ng-if=elementSettings.isMultiEdit> <xui-checkbox class=id-credential-checkbox ng-model=dialogViewModel.multiEditCheckbox.credential ng-if=dialogViewModel.isMultiEdit ng-click=dialogViewModel.setManageProfileElementDialogCredentialEnabled()></xui-checkbox> </scm-col> <scm-col> <div class=\"scm-credential-picker xui-dropdown--justified\" ng-if=dialogViewModel.displayCredentialPicker();> <div class=xui-text-l _t>Access authentication</div> <div class=scm-form-help-hint _t> Configuration element may not be able to poll necessary information without proper access rights for specific server or database etc. Add necessary credentials if needed. </div> <xui-dropdown class=id-credential-combobox name=CredentialDropdown is-editable=true items-source=dialogViewModel.credentialPicker.credentialDropdown.items ng-model=dialogViewModel.credentialPicker.credentialDropdown.selectedItem display-value=name is-required=dialogViewModel.isCredentialRequired on-changed=dialogViewModel.onChangeCredentialDropdown(newValue) item-template-url=scm-credential-dropdown-template placeholder=\"_t(Select an existing credential or add new one...)\" is-disabled=!dialogViewModel.manageProfileElementDialogCredentialEnabled _ta> <div ng-message=credentialEmpty class=id-error-field-credential-missing _t>Credentials missing</div> <div ng-message=required class=id-error-field-credential-required _t>Select existing credentials</div> </xui-dropdown> </div> </scm-col> </scm-row> </scm-grid> </ng-form> <div ng-if=dialogViewModel.testJobNode> <xui-divider></xui-divider> <scm-grid class=xui-text-l _t> <scm-row> <scm-col scm-col-auto> Selected test node: </scm-col> <scm-col no-grow> <scm-node-link class=id-test-job-node [node]=dialogViewModel.testJobNode></scm-node-link> </scm-col> <scm-col> <span class=id-change-test-node> <xui-button size=small is-empty=true display-style=link icon=edit tool-tip=\"Change test node\" tooltip-append-to-body=true ng-click=dialogViewModel.changeTestNode()> </xui-button> </span> </scm-col> </scm-row> </scm-grid> </div> </div> </xui-dialog> ";

/***/ }),
/* 287 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=id-add-swis-query-dialog _t> This internal query will return results for more than one node, as it does not contain the ${NodeId} variable in the WHERE clause. The query result may detect changes from any Orion node. <br/><br/>Are you sure you want to monitor data unrelated to the Orion node the profile is assigned to? </div> </xui-dialog>";

/***/ }),
/* 288 */
/***/ (function(module, exports) {

module.exports = "<span class=credential-dropdown-item ng-if=\"item.id >= -1\">{{::item.name}}</span> <span class=\"xui-text-a credential-dropdown-item\" ng-if=\"item.id === -2\">{{::item.name}}</span> ";

/***/ }),
/* 289 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=scm-profile-dialog> <xui-textbox class=id-profile-name ng-model=vm.dialogOptions.viewModel.profile.profile.name name=ProfileName caption=\"_t(Profile name)\" is-required=true validators=required is-read-only=true _ta> </xui-textbox> <xui-textbox class=id-profile-desc ng-model=vm.dialogOptions.viewModel.profile.profile.description caption=_t(Description) rows=3 is-read-only=true _ta> </xui-textbox> <label _t>Configuration elements</label> <xui-filtered-list-v2 class=\"id-configuration-elements scm-list scm-list--bordered scm-list--bordered-items\" state=vm.dialogOptions.viewModel.profileElementsState dispatcher=vm.dialogOptions.viewModel.profileElementsDispatcher remote-control=vm.dialogOptions.viewModel.listControl controller=vm.dialogOptions.viewModel> <sw-grid-toolbar-container> <xui-toolbar ng-if=\"vm.dialogOptions.viewModel.profileElementsState.selection.items.length === 1\"> <xui-toolbar-item> <xui-button ng-if=\"vm.dialogOptions.viewModel.profileElementsState.selection.items.length === 1\" class=id-view-configuration-element name=ViewConfigurationElementButton icon=browse display-style=tertiary ng-click=vm.dialogOptions.viewModel.viewProfileElement() _t> View details </xui-button> </xui-toolbar-item> </xui-toolbar> </sw-grid-toolbar-container> </xui-filtered-list-v2> </xui-dialog> ";

/***/ }),
/* 290 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=scm-profile-dialog> <xui-message class=id-profile-is-already-assigned-info-message type=info ng-if=vm.dialogOptions.viewModel.viewAlreadyAssignedNodesInfoBox()> <b _t>This profile is already assigned to <a class=id-profile-is-already-assigned-button role=button ng-click=vm.dialogOptions.viewModel.openViewAssignedNodesDialog()><span _t=\"['{{ vm.dialogOptions.viewModel.profile.profile.assignedNodesCount }}', vm.dialogOptions.viewModel.profile.profile.assignedNodesCount > 1 ? 'nodes' : 'node']\">{0} {1}</span></a>. </b> <span _t> Keep in mind that changes made to this profile will affect all those nodes. </span> <div _t> To avoid conflicts with the configuration data already collected, it is no longer possible to edit configuration elements. Instead, create a new element with the desired settings and remove the old one. </div> </xui-message> <xui-message class=id-assigned-to-hidden-nodes-warning-message type=warning ng-if=vm.dialogOptions.viewModel.profileIsAssignedToHiddenNodes()> <span _t>This profile is assigned to nodes you cannot see due to account limitations. Keep in mind that editing this profile or its elements will affect all assigned nodes. Contact your Orion administrator for more information.</span> </xui-message> <ng-form name=vm.dialogOptions.viewModel.profileForm> <xui-textbox class=id-profile-name ng-model=vm.dialogOptions.viewModel.profile.profile.name scm-reset-validation-on-change=duplicateProfile xui-set-focus=true name=ProfileName caption=\"_t(Profile name)\" is-required=true validators=required is-read-only=!vm.dialogOptions.viewModel.profile.profile.name _ta> <div ng-message=nameLengthError class=id-name-length-error _t>Name is too long. Maximum length is 200 characters.</div> <div ng-message=required class=id-error-field-required _t>This field is required</div> <div ng-message=duplicateProfile class=id-error-profile-exists _t>Profile name already exists.</div> </xui-textbox> <xui-textbox class=id-profile-desc ng-model=vm.dialogOptions.viewModel.profile.profile.description name=Description caption=_t(Description) rows=3 is-disabled=vm.dialogOptions.viewModel.isReadOnly _ta> <div ng-message=descriptionLengthError class=id-description-length-error _t>Description is too long. Maximum length is 1400 characters.</div> </xui-textbox> <label _t>Configuration elements</label> <xui-filtered-list-v2 class=\"id-configuration-elements scm-list scm-list--bordered scm-list--bordered-items\" state=vm.dialogOptions.viewModel.profileElementsState dispatcher=vm.dialogOptions.viewModel.profileElementsDispatcher remote-control=vm.dialogOptions.viewModel.listControl controller=vm.dialogOptions.viewModel> <sw-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button ng-if=vm.dialogOptions.viewModel.shouldShowAddButton() class=id-add-configuration-element name=AddConfigurationElementButton icon=add display-style=tertiary ng-click=vm.dialogOptions.viewModel.addProfileElement() _t> Add Element </xui-button> <xui-button ng-if=vm.dialogOptions.viewModel.shouldShowEditButton() class=id-edit-configuration-element name=EditConfigurationElementButton icon=edit display-style=tertiary ng-click=vm.dialogOptions.viewModel.editProfileElement(false) _t> Edit </xui-button> <xui-button ng-if=vm.dialogOptions.viewModel.shouldShowCopyButton() class=id-copy-configuration-element name=CopyConfigurationElementButton icon=copy display-style=tertiary ng-click=vm.dialogOptions.viewModel.copyProfileElement() _t> Copy </xui-button> <xui-button ng-if=vm.dialogOptions.viewModel.shouldShowViewButton() class=id-view-configuration-element name=ViewConfigurationElementButton1 icon=browse display-style=tertiary ng-click=vm.dialogOptions.viewModel.viewProfileElement() _t> View details </xui-button> <xui-button ng-if=vm.dialogOptions.viewModel.shouldShowMultiEditButton() class=id-edit-multiple-configuration-element name=EditMultipleConfigurationElementButton icon=edit display-style=tertiary ng-click=vm.dialogOptions.viewModel.editProfileElement(true) _t> Edit </xui-button> <xui-button ng-if=vm.dialogOptions.viewModel.shouldShowDeleteButton() class=id-delete-configuration-element name=DeleteConfigurationElementButton icon=delete display-style=tertiary ng-click=vm.dialogOptions.viewModel.deleteElementFromProfile() _t> Delete </xui-button> </xui-toolbar-item> </xui-toolbar> </sw-grid-toolbar-container> </xui-filtered-list-v2> </ng-form> </xui-dialog> ";

/***/ }),
/* 291 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=xui-margin-mdv ng-if=\"vm.dialogOptions.viewModel.importedProfilesNames.length > 0\"> <div class=xui-text-h3 _t>Successfully imported profiles</div> <span class=xui-text-r ng-repeat=\"importedProfileName in vm.dialogOptions.viewModel.importedProfilesNames\"> {{ importedProfileName }}{{ $last ? '' : ','}} </span> </div> <div class=xui-margin-mdv ng-if=\"vm.dialogOptions.viewModel.notValidFiles.length > 0 || vm.dialogOptions.viewModel.validationErrors.length > 0\"> <div class=xui-text-h3 _t>Validation errors</div> <scm-grid> <scm-row ng-repeat=\"invalidFile in vm.dialogOptions.viewModel.notValidFiles\"> <scm-col no-grow class=xui-text-r>{{ invalidFile }}</scm-col><scm-col class=xui-text-dscrn _t>This file is not a valid SCM profile</scm-col> </scm-row> <scm-row ng-repeat=\"validationError in vm.dialogOptions.viewModel.validationErrors\"> <scm-col no-grow class=xui-text-r>{{ validationError.profileId }}</scm-col><scm-col class=xui-text-dscrn>{{ validationError.message }}</scm-col> </scm-row> </scm-grid> <div class=xui-text-critical _t>These profiles were not imported.</div> </div> <div class=xui-margin-mdv ng-if=\"vm.dialogOptions.viewModel.duplicateProfiles.length > 0\"> <div class=xui-text-h3 _t>Duplicate profiles</div> <span class=xui-text-r ng-repeat=\"duplicateProfile in vm.dialogOptions.viewModel.duplicateProfiles\"> {{ duplicateProfile.name }}{{ $last ? '' : ','}} </span> <div class=xui-text-warning _t>Do you want to create a copy of these profiles?</div> </div> </xui-dialog> ";

/***/ }),
/* 292 */
/***/ (function(module, exports) {

module.exports = " <xui-popover class=no-pointer ng-if=\"elementSettings.profileElement.type === 'file' && !elementSettings.isMultiEdit\" xui-popover-content=\"'This configuration element has already been polled. It is no longer possible to edit its \\'Path\\'. Instead, create a new element with the desired settings and remove this one.'\" xui-popover-trigger=\"{{::elementSettings.showPopover ? 'mouseenter' : 'none'}}\"> <xui-textbox scm-reset-validation-on-change=\"duplicateProfileElement, notValidProfileElementPathSetting, settingPathLengthError\" ng-model=elementSettings.settingsPath class=id-path name=ElementPath caption=_t(Path) is-required=true xui-set-focus=true is-read-only=\"{{ ::!elementSettings.settingsEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required</div> <div ng-message=duplicateProfileElement class=id-error-profile-exists _t>Path already exists in this profile.</div> <div ng-message=notValidProfileElementPathSetting class=id-error-not-valid-element _t>Enter a valid file path.</div> <div ng-message=settingPathLengthError class=id-settingPath-length-error _t>Path is too long. Max length is 260 characters.</div> </xui-textbox> <div class=scm-form-help-hint _t> Path to a file. Can use the wildcard character * to match part of a file name, ** to match any subdirectory, and Windows system variables such as %WINDIR% or Linux system variables such as ${HOME}. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMElementTypeFile']}}\" target=_blank rel=\"noopener noreferrer\">Learn more</a> </div> </xui-popover> <xui-popover class=no-pointer ng-if=\"elementSettings.profileElement.type === 'registry' && !elementSettings.isMultiEdit\" xui-popover-content=\"'This configuration element has already been polled. It is no longer possible to edit its \\'Registry key\\'. Instead, create a new element with the desired settings and remove this one.'\" xui-popover-trigger=\"{{::elementSettings.showPopover ? 'mouseenter' : 'none'}}\"> <xui-textbox scm-reset-validation-on-change=\"duplicateProfileElement, notValidProfileElementPathSetting\" ng-model=elementSettings.settingsPath class=id-path name=ElementPath caption=\"_t(Registry key)\" is-required=true rows=2 xui-set-focus=true is-read-only=\"{{ ::!elementSettings.settingsEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required</div> <div ng-message=duplicateProfileElement class=id-error-profile-exists _t>Registry key already exists in this profile.</div> <div ng-message=notValidProfileElementPathSetting class=id-error-not-valid-element _t>Enter a valid registry key.</div> </xui-textbox> <div class=scm-form-help-hint _t> Registry key path (e.g. HKEY_LOCAL_MACHINE\\SOFTWARE\\SolarWinds). No wildcard characters are allowed. All subtrees and value changes will be monitored. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMElementTypeRegistry']}}\" target=_blank rel=\"noopener noreferrer\">Learn more</a> </div> </xui-popover> <xui-popover class=no-pointer ng-if=\"elementSettings.profileElement.type === 'swisQuery' && !elementSettings.isMultiEdit\" xui-popover-content=\"'This configuration element has already been polled. It is no longer possible to edit its \\'Internal query\\'. Instead, create a new element with the desired settings and remove this one.'\" xui-popover-trigger=\"{{::elementSettings.showPopover ? 'mouseenter' : 'none'}}\"> <xui-textbox scm-reset-validation-on-change=\"duplicateProfileElement, notValidProfileElementPathSetting, nodeIdVariableNotPresent\" ng-model=elementSettings.settingsPath class=id-path name=ElementPath caption=\"_t(Internal query)\" is-required=true rows=4 is-read-only=\"{{ ::!elementSettings.settingsEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required</div> <div ng-message=duplicateProfileElement class=id-error-profile-exists _t>Internal query already exists in this profile.</div> <div ng-message=notValidProfileElementPathSetting class=id-error-not-valid-element _t>Enter a valid Internal query.</div> </xui-textbox> <div class=scm-form-help-hint _t> An internal SWIS query, which should include ${NodeId}. <a href=\"http://www.solarwinds.com/documentation/kbloader.aspx?kb=MT10260\" target=_blank rel=\"noopener noreferrer\">What is SWIS?</a> <a ng-href=\"{{::elementSettings.helpLinks['orionSCMElementTypeSWIS']}}\" target=_blank rel=\"noopener noreferrer\">Learn more </a> </div> </xui-popover> <scm-row> <scm-col class=scm-edit-element-checkbox-col scm-col-auto align-self-start ng-if=elementSettings.isMultiEdit> <xui-checkbox class=id-connection-string-checkbox ng-model=elementSettings.multiEditCheckbox.connectionString ng-if=\"elementSettings.isMultiEdit && elementSettings.profileElement.type === 'database' && elementSettings.mode === 2\" ng-click=elementSettings.setSettingConnectionStringEnabled()></xui-checkbox> </scm-col> <scm-col> <xui-popover class=no-pointer ng-if=\"elementSettings.profileElement.type === 'database'\" xui-popover-content=\"'This configuration element has already been polled. It is no longer possible to edit its \\'Database query\\'. Instead, create a new element with the desired settings and remove this one.'\" xui-popover-trigger=\"{{::elementSettings.showPopover ? 'mouseenter' : 'none'}}\"> <xui-dropdown class=\"xui-dropdown--justified id-connectionString\" name=ConnectionString caption=\"Connection string\" scm-reset-validation-on-change=connectionStringMacroRequired is-editable=true is-required=elementSettings.isConnectionStringRequired is-disabled=!elementSettings.settingsEnabled items-source=elementSettings.connectionStrings ng-model=elementSettings.settingsConnectionString placeholder=\"{{ ::elementSettings.connectionStringPlaceholder }}\"> <div ng-message=required class=id-error-field-required-connection-string _t>This field is required</div> <div ng-message=connectionStringMacroRequired class=id-error-field-required-connection-string-macro _t> This field requires a ${NodeIP} or ${NodeHostname} macro as well as ${Username} and ${Password} in the connection string. </div> </xui-dropdown> <div class=scm-form-help-hint _t> Pick the connection string that matches the database type you want to monitor. You can use the following macros: ${NodeIP}, ${NodeHostname}, ${Username}, and ${Password}. These macros will be substituted with the appropriate values during execution. Windows authentication is not supported, so you must provide database account credentials. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMDatabaseQueries']}}\" target=_blank rel=\"noopener noreferrer\"> Learn more about database queries </a> </div> <div ng-if=!elementSettings.isMultiEdit> <xui-textbox ng-model=elementSettings.settingsPath class=id-path name=ElementPath caption=\"_t(Database query)\" is-required=true rows=4 is-read-only=\"{{ ::!elementSettings.settingsEnabled }}\" _ta> <div ng-message=required class=id-error-field-required-database-query _t>This field is required</div> </xui-textbox> <div class=scm-form-help-hint _t> Write a SQL SELECT statement to retrieve the data from database. The returned data set is being tracked, and changes to it are detected. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMDatabaseQueries']}}\" target=_blank rel=\"noopener noreferrer\"> Learn more about database queries </a> </div> <scm-grid> <scm-row align-items-start> <scm-col margin-right-lg> <xui-textbox ng-model=elementSettings.settingsPollingFrequency class=id-polling-interval name=PollingFrequency caption=\"_t(Polling frequency [hh:mm:ss])\" is-required=true validators=\"{{:: elementSettings.frequencyValidators() }}\" is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required.</div> <div ng-message=min class=id-error-min _t=\"[ elementSettings.minPollingFrequency ]\">Value must be {0} or higher.</div> <div ng-message=max class=id-error-max _t=\"[ elementSettings.maxPollingFrequency ]\">Value must be {0} or lower.</div> <div ng-message=parse class=id-error-format _t>Invalid format.</div> </xui-textbox> </scm-col> <scm-col> <xui-textbox ng-model=elementSettings.settingsPollingTimeout class=id-timeout caption=\"_t(Polling timeout [hh:mm:ss])\" name=PollingTimeout is-required=true validators=\"{{:: elementSettings.timeoutValidators() }}\" is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required.</div> <div ng-message=min class=id-error-min _t=\"[ elementSettings.minPollingTimeout ]\">Value must be {0} or higher.</div> <div ng-message=max class=id-error-max _t=\"[ elementSettings.maxPollingTimeout ]\">Value must be {0} or lower.</div> <div ng-message=parse class=id-error-format _t>Invalid format.</div> </xui-textbox> </scm-col> </scm-row> <scm-row> <xui-message ng-show=\"elementSettings.isTimeoutOverPolling() && elementSettings.collectContentEnabled\" type=warning class=id-timeout-over-frequency-message _t> Polling timeout exceeds the polling frequency. Data from long-running queries may not be collected. </xui-message> </scm-row> </scm-grid> </div> </xui-popover> </scm-col> </scm-row> <div ng-if=\"elementSettings.profileElement.type === 'powershell' && !elementSettings.isMultiEdit\"> <xui-message class=id-already-polled-message ng-if=::elementSettings.showPopover type=warning _t> <b>This element has already been polled.</b> Changing the script output will trigger detection of a configuration change on this element for each of {{ elementSettings.profile.profile.assignedNodesCount }} assigned nodes. </xui-message> <xui-textbox scm-reset-validation-on-change=duplicateProfileElement ng-model=elementSettings.settingsPath class=id-path name=ElementPath caption=\"_t(PowerShell script)\" is-required=true rows=8 is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required-ps-script _t>This field is required</div> <div ng-message=duplicateProfileElement class=id-error-profile-exists _t>Script already exists in this profile.</div> </xui-textbox> <div class=scm-form-help-hint _t> Write your own PowerShell script to be executed on a monitored node. The script output is being tracked, and changes to it are detected. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMPowerShellScript']}}\" target=_blank rel=\"noopener noreferrer\">More about PowerShell scripts.</a> </div> <scm-grid> <scm-row align-items-start> <scm-col margin-right-lg> <xui-textbox ng-model=elementSettings.settingsPollingFrequency class=id-polling-interval name=PollingFrequency caption=\"_t(Polling frequency [hh:mm:ss])\" is-required=true validators=\"{{:: elementSettings.frequencyValidators() }}\" is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required.</div> <div ng-message=min class=id-error-min _t=\"[ elementSettings.minPollingFrequency ]\">Value must be {0} or higher.</div> <div ng-message=max class=id-error-max _t=\"[ elementSettings.maxPollingFrequency ]\">Value must be {0} or lower.</div> <div ng-message=parse class=id-error-format _t>Invalid format.</div> </xui-textbox> </scm-col> <scm-col> <xui-textbox ng-model=elementSettings.settingsPollingTimeout class=id-timeout caption=\"_t(Polling timeout [hh:mm:ss])\" name=PollingTimeout is-required=true validators=\"{{:: elementSettings.timeoutValidators() }}\" is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required.</div> <div ng-message=min class=id-error-min _t=\"[ elementSettings.minPollingTimeout ]\">Value must be {0} or higher.</div> <div ng-message=max class=id-error-max _t=\"[ elementSettings.maxPollingTimeout ]\">Value must be {0} or lower.</div> <div ng-message=parse class=id-error-format _t>Invalid format.</div> </xui-textbox> </scm-col> </scm-row> <scm-row> <xui-message ng-show=\"elementSettings.isTimeoutOverPolling() && elementSettings.collectContentEnabled\" type=warning class=id-timeout-over-frequency-message _t> Polling timeout exceeds the polling frequency. Data from long-running scripts may not be collected. </xui-message> </scm-row> </scm-grid> </div> <div ng-if=\"elementSettings.profileElement.type === 'script' && !elementSettings.isMultiEdit\"> <xui-message class=id-already-polled-message ng-if=::elementSettings.showPopover type=warning _t> <b>This element has already been polled.</b> Changing the script output will trigger detection of a configuration change on this element for each of {{ elementSettings.profile.assignedNodesCount }} assigned nodes. </xui-message> <xui-textbox ng-model=elementSettings.settingsCommandLine class=id-command-line name=CommandLine caption=\"_t(Command line)\" is-required=true is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required-command-line _t>This field is required</div> <div ng-message=onlyonemacro class=id-error-onlyonemacro-command-line _t>Only one macro ${Script-file} is allowed</div> </xui-textbox> <xui-message class=id-not-used-script ng-if=elementSettings.warnScriptNotUsed() type=warning _t> To run the script, use a ${Script-file} macro in the command line. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMLinuxScriptElements']}}\" target=_blank rel=\"noopener noreferrer\">Learn more</a> </xui-message> <div class=scm-form-help-hint _t> e.g. \"python ${Script-file}\" where ${Script-file} macro is referring to the generated script file with content defined below. </div> <xui-textbox ng-model=elementSettings.settingsPath class=\"id-path no-pointer\" name=ElementPath caption=\"_t(Script file content)\" is-required=elementSettings.isScriptFileContentRequired() is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" rows=8 _ta> <div ng-message=required class=id-error-field-required-ps-script _t>This field is required</div> </xui-textbox> <div class=scm-form-help-hint _t> Write your own script to be executed on a monitored node. The script output is being tracked, and changes to it are detected. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMLinuxScriptElements']}}\" target=_blank rel=\"noopener noreferrer\">More about scripts</a> </div> <xui-textbox ng-model=elementSettings.settingsWorkingDirectory class=\"id-working-directory no-pointer\" name=WorkingDirectory caption=\"_t(Working directory for script)\" is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" is-required=false _ta> </xui-textbox> <scm-grid> <scm-row align-items-start> <scm-col margin-right-lg> <xui-textbox ng-model=elementSettings.settingsPollingFrequency class=id-polling-interval name=PollingFrequency caption=\"_t(Polling frequency [hh:mm:ss])\" is-required=true validators=\"{{:: elementSettings.frequencyValidators() }}\" is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required.</div> <div ng-message=min class=id-error-min _t=\"[ elementSettings.minPollingFrequency ]\">Value must be {0} or higher.</div> <div ng-message=max class=id-error-max _t=\"[ elementSettings.maxPollingFrequency ]\">Value must be {0} or lower.</div> <div ng-message=parse class=id-error-format _t>Invalid format.</div> </xui-textbox> </scm-col> <scm-col> <xui-textbox ng-model=elementSettings.settingsPollingTimeout class=id-timeout caption=\"_t(Polling timeout [hh:mm:ss])\" name=PollingTimeout is-required=true validators=\"{{:: elementSettings.timeoutValidators() }}\" is-read-only=\"{{ ::!elementSettings.collectContentEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required.</div> <div ng-message=min class=id-error-min _t=\"[ elementSettings.minPollingTimeout ]\">Value must be {0} or higher.</div> <div ng-message=max class=id-error-max _t=\"[ elementSettings.maxPollingTimeout ]\">Value must be {0} or lower.</div> <div ng-message=parse class=id-error-format _t>Invalid format.</div> </xui-textbox> </scm-col> </scm-row> <scm-row> <xui-message ng-show=\"elementSettings.isTimeoutOverPolling() && elementSettings.collectContentEnabled\" type=warning class=id-timeout-over-frequency-message _t> Polling timeout exceeds the polling frequency. Data from long-running scripts may not be collected. </xui-message> </scm-row> </scm-grid> </div> <xui-popover class=no-pointer ng-if=\"elementSettings.profileElement.type === 'parsedFile' && !elementSettings.isMultiEdit\" xui-popover-content=\"'This configuration element has already been polled. It is no longer possible to edit its \\'Path to parsed file\\' or \\'Parser\\'. Instead, create a new element with the desired settings and remove this one'\" xui-popover-trigger=\"{{::elementSettings.showPopover ? 'mouseenter' : 'none'}}\"> <div> <xui-textbox ng-model=elementSettings.settingsPath scm-reset-validation-on-change=\"duplicateProfileElement, notValidProfileElementPathSetting, settingPathLengthError\" class=id-path name=ElementPath caption=\"_t(Path to parsed file)\" is-required=true xui-set-focus=true is-read-only=\"{{ ::!elementSettings.settingsEnabled }}\" _ta> <div ng-message=required class=id-error-field-required _t>This field is required</div> <div ng-message=duplicateProfileElement class=id-error-profile-exists _t>Path already exists in this profile.</div> <div ng-message=notValidProfileElementPathSetting class=id-error-not-valid-element _t>Enter a valid file path.</div> <div ng-message=settingPathLengthError class=id-settingPath-length-error _t>Path is too long. Max length is 260 characters.</div> </xui-textbox> <div class=scm-form-help-hint _t> This file will be processed by the chosen file parser to find paths to more configuration items. The path can use the wildcard character * to match part of a file name, ** to match any subdirectory, and system variables like %WINDIR%. <a ng-href=\"{{::elementSettings.helpLinks['orionSCMElementTypeParsedFile']}}\" target=_blank rel=\"noopener noreferrer\">Learn more</a> </div> <xui-dropdown class=id-parser name=Parser caption=_t(Parser) ng-model=elementSettings.settingsParsingType items-source=elementSettings.fileParsingTypes display-value=value is-disabled=::!elementSettings.settingsEnabled _ta> </xui-dropdown> <div class=scm-form-help-hint ng-if=\"elementSettings.settingsParsingType.name === 'IisWebConfig'\" _t> Parsing the file to search for distributed configuration via web.config files specific for a particular IIS site, application or virtual directory and located within its directory. </div> </div> </xui-popover> <div ng-if=\"elementSettings.profileElement.type === 'registry' && !elementSettings.isMultiEdit\"> <xui-switch name=Recursive ng-model=elementSettings.settingsRecursive ng-disabled=::!elementSettings.recursiveEnabled _t> Include subkeys </xui-switch> <div class=xui-text-dscrn> <span class=no-pointer _t> If enabled, download and monitor changes in a registry key and its subkeys. If disabled, download and monitor changes only in a specific registry key, not its subkeys. </span> </div> </div> <div class=xui-margin-smv ng-if=\"(elementSettings.profileElement.type === 'file' || elementSettings.profileElement.type === 'registry') && !elementSettings.isMultiEdit\"> <xui-switch name=CollectContent ng-model=elementSettings.settingsCollectContent ng-disabled=::!elementSettings.collectContentEnabled _t> Download content </xui-switch> <div class=xui-text-dscrn> <span ng-if=\"elementSettings.profileElement.type === 'file'\" class=no-pointer _t> If a file is very large, you may want to disable content downloading. Downloaded content is used for line-by-line content comparison. Without it, you will still be notified of a change, but won't be able to see the line-by-line comparison. </span> <span ng-if=\"elementSettings.profileElement.type === 'registry'\" class=no-pointer _t> If a registry tree content is very large, you may want to disable content downloading. Downloaded content is used for line-by-line content comparison. Without it, you will still be notified of a change, but won't be able to see the line-by-line content comparison. </span> </div> </div> ";

/***/ }),
/* 293 */
/***/ (function(module, exports) {

module.exports = "<a class=xui-text-a uib-tooltip={{ctrl.getTooltipDate()}} tooltip-append-to-body=true tooltip-class=ellipsis-tooltip tooltip-enable={{ctrl.isTooltipEnabled}} tooltip-is-open=ctrl.isTooltipOpen> <xui-popover class=id-popover xui-popover-content=ctrl.getPopoverTemplate() xui-popover-placement=right xui-popover-trigger=click xui-popover-id={{::ctrl.id}} xui-popover-is-modal=true xui-popover-on-show=ctrl.onShow() xui-popover-on-hide=ctrl.onHide() xui-popover-is-displayed=ctrl.isDisplayed> {{ctrl.printModel()}} </xui-popover> </a> ";

/***/ }),
/* 294 */
/***/ (function(module, exports) {

module.exports = "<div class=\"xui-margin-md-neg datetime-picker-popover\"> <div class=datetime-picker-popover-picker-panel> <div class=\"datetime-picker-popover-baseline xui-padding-lg\" ng-if=ctrl.hasBaseline()> <h5 _t>Baseline</h5> <a href=\"\" ng-click=ctrl.onBaselineClick()>{{ ctrl.getBaselineDate() }}</a> </div> <div class=\"datetime-picker-popover-specify-date xui-padding-lg\"> <h5 _t>Specific date</h5> <xui-datetime-picker class=datetime-picker-popover ng-model=ctrl.innerModel caption={{::ctrl.caption}} is-required=ctrl.isRequired is-disabled=ctrl.isDisabled min-date=ctrl.minDate max-date=ctrl.maxDate /> </div> </div> <div ng-show=ctrl.areButtonsDisplayed class=\"datetime-picker-popover-confirmation-panel xui-padding-lg\"> <xui-button ng-click=ctrl.onUseButtonClick() display-style=primary _t>Use</xui-button> <xui-button ng-click=ctrl.onCancelButtonClick() display-style=tertiary _t>Cancel</xui-button> </div> </div> ";

/***/ }),
/* 295 */
/***/ (function(module, exports) {

module.exports = "<scm-iframe-v2 route=assignProfileWizard/configurationProfiles></scm-iframe-v2> ";

/***/ }),
/* 296 */
/***/ (function(module, exports) {

module.exports = "<xui-progress id=step-2-progress-bar show=vm.isBusy show-progress=false allow-cancel=false message=\"_t(Loading data, please wait...)\" _ta> </xui-progress> <scm-suitable-node-list class=id-node-list nodes-state=vm.nodesState nodes-to-preselect=vm.nodesToPreselect on-loaded=vm.nodesFinishedLoadingCallback()> </scm-suitable-node-list> ";

/***/ }),
/* 297 */
/***/ (function(module, exports) {

module.exports = "<scm-iframe-v2 id=set-credentials-step route=assignProfileWizard/credentials></scm-iframe-v2> ";

/***/ }),
/* 298 */
/***/ (function(module, exports) {

module.exports = "<xui-progress id=step-3-progress-bar show=vm.isBusy show-progress=false allow-cancel=false message=\"_t(Loading data, please wait...)\" _ta> </xui-progress> <scm-assign-profile-summary class=id-summary nodes=vm.summaryNodes profiles=vm.summaryProfiles items=vm.summaryRows override-credentials=\"vm.assignedElementsByTypes.length > 0\" is-busy=vm.isBusy> </scm-assign-profile-summary> ";

/***/ }),
/* 299 */
/***/ (function(module, exports) {

module.exports = "<div class=node-wizard-wrapper> <xui-progress id=loading-preselected-progress show=vm.isLoadingPreselected show-progress=false allow-cancel=false message=\"_t(Loading selected data, please wait...)\" _ta> </xui-progress> <xui-wizard ng-hide=vm.isLoadingPreselected id=wizard current-step-index=vm.currentStepIndex on-cancel=vm.onLeave() on-enter-step=vm.onEnterStep(step) on-next-step=\"vm.onNextStep(from, to)\" on-finish=vm.sendAssignments() hide-header=false finish-text=_t(Confirm) _ta> <div ng-messages=vm.errors class=xui-margin-mdt> <ng-message when=profiles> <xui-message type=error _t><b>No configuration profile is selected.</b> Please select one or more configuration profiles.</xui-message> </ng-message> <ng-message when=nodes> <xui-message type=error _t><b>No node is selected.</b> Please select one or more nodes to monitor.</xui-message> </ng-message> </div> <xui-wizard-step ng-repeat=\"step in vm.wizardSteps\" class=id-wizard-step label=\"{{ step.label }}\" title=\"{{ step.title }}\" short-title=\"{{ step.shortTitle }}\"> <ng-include class=id-step src=step.src /> </xui-wizard-step> </xui-wizard> </div> ";

/***/ }),
/* 300 */
/***/ (function(module, exports) {

module.exports = "<scm-grid class=scm-summary-row> <scm-row> <scm-col no-grow align-self-baseline> <scm-node-link class=id-node [node]=item.node [show-status]=false> </scm-node-link> </scm-col> <scm-col class=\"xui-tag-container profile-container\"> <xui-icon icon=config-profile icon-size=small uib-tooltip=\"_t(Assigned server configuration profile(s))\" tooltip-append-to-body=true tooltip-class=ellipsis-tooltip _ta> </xui-icon> <span ng-repeat=\"profile in item.profiles\"> <xui-tag ng-show=\"profile.assignState !== 'skipping'\" class=\"id-tag summary-profile\" text=\"{{ profile.name }}\" icon=\"{{ vm.profileStatusIcon(profile) }}\" color=\"{{ vm.profileStatusColor(profile) }}\" show-remove-icon=\"profile.assignState === 'assigning'\" on-remove-icon-click=\"vm.removeProfile(item, profile)\"> </xui-tag> </span> </scm-col> </scm-row> <scm-row> <xui-message class=\"id-message summary-issues\" type=warning ng-if=item.issues.$count ng-messages=item.issues ng-messages-multiple> <ng-message when=needAgent class=summary-issue _t> To collect {{ vm.humanJoin(item.issues.needAgent.affected) }} configuration, deploy an agent on this node through <a ng-click=vm.scmLocationService.deployAgent(item.node.nodeId) role=button>Deploy agent wizard</a>. </ng-message> <ng-message when=needUpgrade class=summary-issue _t> Collecting {{ vm.humanJoin(item.issues.needUpgrade.affected) }} configuration is not supported on this operating system. </ng-message> <ng-message when=needAi class=summary-issue _t> To collect {{ vm.humanJoin(item.issues.needAi.affected) }} configuration on this node <a class=id-enable-ai role=button ng-click=vm.enableAssetInventory([item.node])>enable Asset Inventory</a> automatically or manually via <a ng-href=\"{{ vm.scmLocationService.getListResourcesUrl(item.node.nodeId) }}\" target=_blank rel=\"noopener noreferrer\">List Resources</a>. </ng-message> <ng-message when=windowsIncompatible class=summary-issue> <span ng-if=\"item.issues.windowsIncompatible.affected.length === 1\" _t> Profile {{ vm.humanJoin(item.issues.windowsIncompatible.affected) }} contains element types that cannot be polled on Windows nodes. </span> <span ng-if=\"item.issues.windowsIncompatible.affected.length > 1\" _t> Profiles {{ vm.humanJoin(item.issues.windowsIncompatible.affected) }} contain element types that cannot be polled on Windows nodes. </span> <span _t>{{ vm.humanJoin(item.issues.windowsIncompatible.affectedTypes) }} cannot be collected on Windows nodes.</span> </ng-message> <ng-message when=linuxIncompatible class=summary-issue> <span ng-if=\"item.issues.linuxIncompatible.affected.length === 1\" _t> Profile {{ vm.humanJoin(item.issues.linuxIncompatible.affected) }} contains element types that cannot be polled on Linux nodes. </span> <span ng-if=\"item.issues.linuxIncompatible.affected.length > 1\" _t> Profiles {{ vm.humanJoin(item.issues.linuxIncompatible.affected) }} contain element types that cannot be polled on Linux nodes. </span> <span _t>{{ vm.humanJoin(item.issues.linuxIncompatible.affectedTypes) }} cannot be collected on Linux nodes.</span> </ng-message> <ng-message when=aiInProgress class=summary-issue> <xui-spinner show=true /> <b _t>Asset Inventory discovery for this node is in progress...</b> </ng-message> <span class=summary-issue> <a ng-href=\"{{ vm.prereqsHelpLink }}\" target=_blank rel=\"noopener noreferrer\" _t>Learn more</a> </span> </xui-message> </scm-row> </scm-grid> ";

/***/ }),
/* 301 */
/***/ (function(module, exports) {

module.exports = "<div class=scm-summary-step> <div class=summary-legend> <span class=\"xui-text-sml xui-margin-lgr\"> <xui-icon icon=step icon-size=small text-alignment=right></xui-icon> <span _t>Already assigned</span> </span> <span class=\"xui-text-sml xui-margin-lgr\"> <xui-icon icon=severity_info icon-size=small icon-color=info-blue text-alignment=right></xui-icon> <span _t>Newly assigned</span> </span> <span class=\"xui-text-sml xui-margin-lgr\" ng-if=vm.totalIssueCount> <xui-icon icon=status_warning icon-size=small text-alignment=right></xui-icon> <b ng-if=\"vm.totalIssueCount === 1\" class=id-total-issue _t>{{ vm.totalIssueCount }} issue</b> <b ng-if=\"vm.totalIssueCount > 1\" class=id-total-issue _t>{{ vm.totalIssueCount }} issues</b> </span> </div> </div> <xui-message class=id-new-credentials-message ng-if=vm.overrideCredentials type=info _t> <b>New credentials to be set.</b> Credentials will be set for elements in newly assigned profiles. If needed, you can override existing credentials and apply new ones for all assignments on the Assigned elements page in Server Configuration Monitor Settings. </xui-message> <div class=\"id-bulk-ai-in-progress xui-margin-lg\" ng-if=vm.bulkAiDiscoveryInProgress> <span> <xui-spinner show=true /> <b class=xui-text-l _t>Bulk Asset Inventory discovery in progress...</b> <a role=button ng-click=vm.reload() _t> Re-check</a> </span></div> <xui-grid class=\"id-summary-grid scm-summary-step scm-list scm-list--bordered scm-list--bordered-items\" items-source=vm.items template-url=scmAssignProfileSummary-item row-padding=\"'narrow'\" stripe=false smart-mode=true options=vm.gridOptions sorting-data=vm.sorting pagination-data=vm.pagination controller=vm> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button class=id-recheck icon=reload display-style=tertiary ng-click=vm.reload() _t> Re-check problems </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-if=vm.deployAllAgentsIds> <a ng-click=vm.scmLocationService.deployAgent(vm.deployAllAgentsIds) sw-auth sw-auth-permissions=nodeManagement> <xui-button class=id-deploy-agents icon=orion-agent display-style=tertiary _t> Bulk-deploy agents </xui-button> </a> </xui-toolbar-item> <xui-toolbar-item ng-if=\"vm.getAiCandidates().length > 0\"> <xui-button class=id-bulk-enable-ai icon=enable display-style=tertiary ng-click=vm.enableBulkAssetInventory(vm.getAiCandidates()) _t> Bulk-enable asset inventory </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-grid> ";

/***/ }),
/* 302 */
/***/ (function(module, exports) {

module.exports = "<scm-grid> <scm-row> <scm-col> <scm-node-detail class=id-node-detail-item [node]=item [open-links-on-new-tab]=true></scm-node-detail> </scm-col> <scm-col scm-col-auto pull-right> <scm-profiles-count class=\"id-profiles-count item-part scm-profile-count\" node-id=item.id profiles-count=item.profileCount /> </scm-col> </scm-row> </scm-grid> ";

/***/ }),
/* 303 */
/***/ (function(module, exports) {

module.exports = "<div sw-entity-popover-loader> <xui-filtered-list-v2 class=\"id-nodes-assigned-to-profile-list scm-list scm-list--stretched scm-list--bordered-items\" state=nodesAssignedToProfileListController.nodesState dispatcher=nodesAssignedToProfileListController.nodesDispatcher controller=nodesAssignedToProfileListController> </xui-filtered-list-v2> </div> ";

/***/ }),
/* 304 */
/***/ (function(module, exports) {

module.exports = "<div class=scm-list-item> <xui-icon css-class=item-part icon-size=small icon=config-profile /> <div class=\"item-part scm-profile-name\"> <div class=id-profile-name uib-tooltip=\"{{:: item.name }}\" tooltip-ellipsis tooltip-append-to-body=true> {{item.name}} </div> </div> <div class=\"id-builtin item-part xui-tag\" ng-if=item.builtIn _t>out-of-the-box</div> </div> ";

/***/ }),
/* 305 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <scm-unassign-profile-dialog sw-entity-popover-loader class=id-unassign-profile-dialog node-ids=$parent.vm.dialogOptions.viewModel.nodeIds> </scm-unassign-profile-dialog> </xui-dialog> ";

/***/ }),
/* 306 */
/***/ (function(module, exports) {

module.exports = "<div> <div class=xui-padding-lgb _t> Select configuration profiles you want to unassign from <scm-nodes-count class=id-nodes-count nodes=vm.scmUnassignProfilesControllerState.nodes />. </div> <xui-busy xui-busy=vm.scmUnassignProfilesControllerState.isBusy> <xui-grid class=\"id-assigned-profiles-list scm-list scm-list--bordered scm-list--bordered-items scm-list--stretched\" display-toolbar=false id=assigned-profiles-list controller=vm items-source=vm.scmUnassignProfilesControllerState.profiles pagination-data=vm.gridPagination selection-mode=multi selection=vm.scmUnassignProfilesControllerState.selection selection-property=id show-selector=true sorting-data=vm.sorting template-url=scm-unassign-profiles-list-item-template smart-mode=true row-padding=\"'none'\" options=vm.options stripe=false allow-select-all-pages=true> </xui-grid> </xui-busy> </div>";

/***/ }),
/* 307 */
/***/ (function(module, exports) {

module.exports = "<div class=id-popover-content> <span class=xui-text-l _t=\"['{{::ctrl.getProfilesCount()}}']\">{0} configuration profiles assigned</span> <div xui-busy=!ctrl.isLoaded xui-busy-show-progress=false xui-allow-cancel=false> <div ng-repeat=\"profile in ctrl.profiles\"> <div class=xui-margin-smt ng-click=ctrl.showProfile(profile)> <span uib-tooltip=\"{{:: profile.name }}\" tooltip-ellipsis tooltip-append-to-body=true> <xui-icon icon-size=medium icon=config-profile /> <a class=id-profile-name ng-if=ctrl.showProfileManagementLink>{{::profile.name}}</a> <span class=xui-text-l ng-if=!ctrl.showProfileManagementLink>{{::profile.name}}</span> <span class=xui-tag ng-if=profile.builtIn _t>out-of-the-box</span> </span> </div> </div> </div> </div> ";

/***/ }),
/* 308 */
/***/ (function(module, exports) {

module.exports = "<span class=profiles-count-popover> <span class=xui-text-a role=button> <xui-popover class=id-profiles-popover xui-popover-is-displayed=ctrl.popoverIsDisplayed xui-popover-content=ctrl.getPopoverTemplate() xui-popover-trigger=mouseenter xui-popover-on-show=ctrl.popoverOnShow()> <xui-icon icon-size=small icon=config-profile /> <span class=id-profiles-count>{{ctrl.getProfilesCount()}}</span> </xui-popover> </span> </span> ";

/***/ }),
/* 309 */
/***/ (function(module, exports) {

module.exports = "<scm-grid class=id-popover-content> <scm-row ng-repeat=\"node in ctrl.visibleNodes\"> <scm-col scm-col-auto class=\"id-node xui-margin-smb\"> <xui-icon class=id-status-icon css-class=id-status-icon icon-size=small icon=unknownnode status=\"{{::node.nodeStatus | swStatus}}\"/> </scm-col> <scm-col no-grow> <a ellipsis class=\"id-node-name scm-node-name\" ng-href={{node.detailsUrl}}>{{::node.name}}</a> </scm-col> </scm-row> <scm-row class=\"id-more-nodes xui-text-a\" ng-if=\"ctrl.nodes.length > ctrl.MaxVisibleItems\" ng-click=ctrl.openNodesList()> {{ ctrl.getMoreItemsString() }} </scm-row> </scm-grid> ";

/***/ }),
/* 310 */
/***/ (function(module, exports) {

module.exports = "<span class=nodes-count-popover> <span> <xui-popover class=\"id-nodes-popover header-content-underline\" xui-popover-is-displayed=ctrl.isPopoverDisplayed xui-popover-content=ctrl.getPopoverTemplate() xui-popover-trigger=mouseenter xui-popover-on-show=ctrl.popoverOnShow()> <span>{{ctrl.getItemsString()}}</span> </xui-popover> </span> </span> ";

/***/ }),
/* 311 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=id-keep-history-dialog _t> <b>Do you want to keep the previously collected data for the selected profile(s) after unassignment?</b> If you keep the data, they will be visible when comparing historical server configurations even though the profile is no longer assigned. </xui-dialog>";

/***/ }),
/* 312 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div id=delete-baseline-dialog-content> <span _t=\"['{{::vm.dialogOptions.viewModel.nodesCount}}','{{::vm.dialogOptions.viewModel.nodeWord}}']\"> <b>You are about to delete the server configuration baseline on {0} selected {1}.</b> All current baseline mismatches for these nodes will disappear. </span> <a ng-href={{::vm.dialogOptions.viewModel.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more about baselines</a> </div> </xui-dialog>";

/***/ }),
/* 313 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div id=delete-baseline-dialog-content> <span _t>None of selected nodes have a defined baseline.</span> <a ng-href=\"{{ vm.dialogOptions.viewModel.helplink}}\" target=_blank rel=\"noopener noreferrer\" _t>Learn more about baselines</a> </div> </xui-dialog>";

/***/ }),
/* 314 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <span _t=\"['{{::vm.dialogOptions.viewModel.nodesCount}}','{{::vm.dialogOptions.viewModel.nodeWord}}']\"> <b>You are about to update the server configuration baseline on {0} selected {1}.</b> All current baseline mismatches for these nodes will disappear. </span> <a ng-href={{::vm.dialogOptions.viewModel.helpLink}} target=_blank rel=\"noopener noreferrer\" _t>Learn more about baselines</a> <div id=summary-of-baseline-changes> <br/> <b _t>Summary of baseline changes:</b> <span> <div ng-if=\"vm.dialogOptions.viewModel.nodesWithoutBaseline.length > 0\"> <xui-icon icon-size=small text-alignment=left-right icon-color=disabled-gray icon=baseline /> <scm-nodes-count class=scm-nodes-count nodesDialogTitle=\"_t(All nodes with baselines to be created)\" nodes=vm.dialogOptions.viewModel.nodesWithoutBaseline _ta/> <span _t>have no baseline defined and new baseline will be created.</span> </div> <div ng-if=\"vm.dialogOptions.viewModel.nodesMatchingBaseline.length > 0\"> <xui-icon icon-size=small text-alignment=left-right icon-color=ok-green icon=baseline /> <scm-nodes-count class=scm-nodes-count nodesDialogTitle=\"_t(All nodes with baselines to be updated)\" nodes=vm.dialogOptions.viewModel.nodesMatchingBaseline _ta/> <span _t>have no baseline mismatches and baselines will be updated.</span> </div> <div ng-if=\"vm.dialogOptions.viewModel.nodesMismatchingBaseline.length > 0\"> <xui-icon icon-size=small text-alignment=left-right icon-color=critical-red icon=baseline-mismatch /> <scm-nodes-count class=scm-nodes-count nodesDialogTitle=\"_t(All nodes with baseline mismatches to be updated)\" nodes=vm.dialogOptions.viewModel.nodesMismatchingBaseline _ta/> <span _t>have baseline mismatches and all baselines will be updated.</span> </div> </span> </div> </xui-dialog>";

/***/ }),
/* 315 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog ng-init=\"dialogViewModel = vm.dialogOptions.viewModel\"> <scm-suitable-node-list class=id-suitable-node-list nodes-state=dialogViewModel.nodesState selection-mode=radio> </scm-suitable-node-list> </xui-dialog> ";

/***/ }),
/* 316 */
/***/ (function(module, exports) {

module.exports = "<xui-filtered-list-v2 class=\"id-nodes scm-list scm-list--bordered scm-list--bordered-items\" state=suitableNodeList.nodesState dispatcher=suitableNodeList.nodesDispatcher controller=suitableNodeList> </xui-filtered-list-v2> ";

/***/ }),
/* 317 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=\"contentCtrl.content.lines.length > 0\"> <div ng-repeat=\"line in contentCtrl.content.lines track by line.lineNumber\" ng-if=!contentCtrl.hideContent> <sw-text-diff-item diff-status=1 line=::line is-original=true> </sw-text-diff-item> </div> </div> <xui-message class=id-no-content-message type=info ng-if=contentCtrl.getNoContentMessage()> {{ contentCtrl.getNoContentMessage() }} </xui-message> ";

/***/ }),
/* 318 */
/***/ (function(module, exports) {

module.exports = "<div class=\"scm-getting-started-popover xui-text-l\"> <div _t> You can always access the onboarding and education materials from here </div> <div text-right padding-top> <a class=id-got-it role=button ng-click=gettingStartedWithScm.gotItClicked()> <span class=text-caps _t>OK, Got it</span> </a> </div> </div>";

/***/ }),
/* 319 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=scm-getting-started-dialog> <div> <div class=\"xui-text-dscrn text-caps\" _t>SCM onboarding and education</div> <div class=xui-margin-mdt> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getGuideUrl()}} target=_blank rel=\"noopener noreferrer\"> <xui-icon icon=wizard icon-size=small text-alignment=right /> <span _t>Getting started guide</span> </a> </div> <div class=xui-margin-mdt> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getSuccessUrl()}} target=_blank rel=\"noopener noreferrer\"> <xui-icon icon=help icon-size=small text-alignment=right /> <span _t>Customer success center</span> </a> </div> <div class=xui-margin-mdt> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getReleaseNotesUrl()}} target=_blank rel=\"noopener noreferrer\"> <xui-icon icon=star-empty icon-size=small text-alignment=right /> <span _t>What's new and release notes</span> </a> </div> <div class=xui-margin-mdt> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getThwackUrl()}} target=_blank rel=\"noopener noreferrer\"> <xui-icon icon=thwack icon-size=small text-alignment=right /> <span _t>Thwack community forum</span> </a> </div> </div> <div ng-if=vm.dialogOptions.viewModel.ctrl.isAuthorized> <div class=small-divider> <xui-divider></xui-divider> </div> <div class=\"xui-text-dscrn text-caps\" _t>Configuration management</div> <div class=xui-margin-mdt> <a role=button ng-click=vm.dialogOptions.viewModel.ctrl.openWizard()> <xui-icon icon=assign icon-size=small text-alignment=right /> <span _t>Assign configuration profile to node</span> </a> </div> <div class=xui-margin-mdt> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getManageProfilesUrl()}} target=_self> <xui-icon icon=gear icon-size=small text-alignment=right /> <span _t>SCM settings</span> </a> </div> <div class=xui-margin-mdt> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getManageProfilesUrl(true)}} target=_self> <xui-icon icon=add icon-size=small text-alignment=right /> <span _t>Add new configuration profile</span> </a> </div> </div> <div> <div class=small-divider> <xui-divider></xui-divider> </div> <div class=\"xui-text-dscrn text-caps\" _t>Policy compliance</div> <div class=xui-margin-mdt> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getComplianceSummaryUrl()}} target=_blank rel=\"noopener noreferrer\"> <xui-icon icon=policy icon-size=small text-alignment=right /> <span _t>Overall compliance summary</span> </a> </div> <div class=xui-margin-mdt ng-if=vm.dialogOptions.viewModel.ctrl.isAuthorized> <a ng-href={{::vm.dialogOptions.viewModel.ctrl.scmLocationService.getAssignPoliciesUrl()}} target=_blank rel=\"noopener noreferrer\"> <xui-icon icon=assign icon-size=small text-alignment=right /> <span _t>Assign policy to node</span> </a> </div> </div> <div ng-if=vm.dialogOptions.viewModel.ctrl.isAuthorized> <div class=small-divider> <xui-divider></xui-divider> </div> <div class=\"xui-text-dscrn text-caps\" _t>Settings</div> <div class=xui-margin-mdt> <xui-switch name=GettingStartedSettings ng-model=vm.dialogOptions.viewModel.ctrl.gettingStartedSwitch ng-change=vm.dialogOptions.viewModel.ctrl.onSwitchChange()> <span class=xui-text-normal _t>Show getting started link on SCM Summary</span> </xui-switch> </div> </div> </xui-dialog> ";

/***/ }),
/* 320 */
/***/ (function(module, exports) {

module.exports = "<span ng-if=gettingStartedWithScm.showLink> <span ng-if=\"gettingStartedWithScm.showPopover === true\"> <xui-popover xui-popover-content=gettingStartedWithScm.popoverContent xui-popover-is-displayed=gettingStartedWithScm.isPopoverDisplayed xui-popover-trigger=none xui-popover-id=scm-getting-started-popover> <a role=button class=scm-getting-started-link ng-click=gettingStartedWithScm.openDialog()> <xui-icon icon=wizard icon-size=small /> <span _t>Getting started with SCM</span> </a> </xui-popover> </span> <span ng-if=\"gettingStartedWithScm.showPopover === false\"> <a role=button class=scm-getting-started-link ng-click=gettingStartedWithScm.openDialog()> <xui-icon icon=wizard icon-size=small /> <span _t>Getting started with SCM</span> </a> </span> </span> ";

/***/ }),
/* 321 */
/***/ (function(module, exports) {

module.exports = " <xui-dialog class=id-add-credential-dialog _t> <ng-form name=vm.dialogOptions.viewModel.credentialForm> <xui-textbox class=id-credential-name ng-model=vm.dialogOptions.viewModel.credentialName is-required=true scm-reset-validation-on-change=duplicateCredentials name=CredentialName caption=\"_t(Credential Name)\" _ta> <div ng-message=required class=id-error-field-required-credential-name _t>This field is required</div> <div ng-message=duplicateCredentials class=id-error-field-duplicate-credential _t>Credential Name already exists</div> </xui-textbox> <xui-divider/> <xui-textbox class=id-user-name ng-model=vm.dialogOptions.viewModel.userName is-required=true name=UserName caption=\"_t(User Name)\" help-text=\"_t(Enter Domain\\Username or Username@Domain)\" _ta> <div ng-message=required class=id-error-field-required-user-name _t>This field is required</div> </xui-textbox> <xui-textbox class=id-password ng-model=vm.dialogOptions.viewModel.password is-required=true name=Password scm-reset-validation-on-change=ConfirmPassword:passwordMatch type=password caption=_t(Password) validators=\"ng-trim=false\" _ta> <div ng-message=required class=id-error-field-required-password _t>This field is required</div> </xui-textbox> <xui-textbox class=id-confirm-password ng-model=vm.dialogOptions.viewModel.confirmPassword is-required=true scm-reset-validation-on-change=passwordMatch name=ConfirmPassword type=password caption=\"_t(Confirm Password)\" validators=\"ng-trim=false\" _ta> <div ng-message=required class=id-error-field-required-confirm-password _t>This field is required</div> <div ng-message=passwordMatch class=id-error-field-passwords-are-not-same _t>Passwords don't match</div> </xui-textbox> </ng-form> </xui-dialog> ";

/***/ }),
/* 322 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmMismatches_1 = __webpack_require__(323);
var scmProfileList_1 = __webpack_require__(326);
var scmMonitoredNodeList_1 = __webpack_require__(330);
var manageProfileDialog_1 = __webpack_require__(333);
var elementSettings_1 = __webpack_require__(336);
var datetimePickerPopover_1 = __webpack_require__(339);
var scmAssignProfileWizard_1 = __webpack_require__(342);
var nodesAssignedToProfile_1 = __webpack_require__(348);
var unassignProfilesDialog_1 = __webpack_require__(352);
var scmContentMessagePopover_1 = __webpack_require__(357);
var scmProfilesCount_1 = __webpack_require__(359);
var scmNodesCount_1 = __webpack_require__(361);
var keepDataDialog_1 = __webpack_require__(363);
var deleteBaselinesDialog_1 = __webpack_require__(365);
var bulkUpdateBaselinesDialog_1 = __webpack_require__(367);
var selectTestNodeDialog_1 = __webpack_require__(369);
var scmSuitableNodeList_1 = __webpack_require__(371);
var scmContent_1 = __webpack_require__(374);
var gettingStartedWithScm_1 = __webpack_require__(377);
var addCredentialDialog_1 = __webpack_require__(381);
exports.default = function (module) {
    scmMismatches_1.default(module);
    scmProfileList_1.default(module);
    scmMonitoredNodeList_1.default(module);
    manageProfileDialog_1.default(module);
    datetimePickerPopover_1.default(module);
    scmAssignProfileWizard_1.default(module);
    nodesAssignedToProfile_1.default(module);
    unassignProfilesDialog_1.default(module);
    elementSettings_1.default(module);
    scmContentMessagePopover_1.default(module);
    scmProfilesCount_1.default(module);
    scmNodesCount_1.default(module);
    keepDataDialog_1.default(module);
    bulkUpdateBaselinesDialog_1.default(module);
    deleteBaselinesDialog_1.default(module);
    selectTestNodeDialog_1.default(module);
    scmSuitableNodeList_1.default(module);
    scmContent_1.default(module);
    gettingStartedWithScm_1.default(module);
    addCredentialDialog_1.default(module);
};


/***/ }),
/* 323 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmMismatches_controller_1 = __webpack_require__(149);
var scmMismatches_directive_1 = __webpack_require__(324);
__webpack_require__(325);
exports.default = function (module) {
    module.component("scmMismatches", scmMismatches_directive_1.MismatchesDirective);
    module.controller("scmMismatchesController", scmMismatches_controller_1.MismatchesController);
};


/***/ }),
/* 324 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmMismatches_controller_1 = __webpack_require__(149);
var MismatchesDirective = /** @class */ (function () {
    function MismatchesDirective() {
        this.scope = {};
        this.restrict = "E";
        this.template = __webpack_require__(281);
        this.replace = true;
        this.transclude = false;
        this.controller = scmMismatches_controller_1.MismatchesController;
        this.controllerAs = "vm";
        this.bindToController = {
            baselineInfo: "=?"
        };
    }
    return MismatchesDirective;
}());
exports.MismatchesDirective = MismatchesDirective;


/***/ }),
/* 325 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 326 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmProfileList_controller_1 = __webpack_require__(150);
var scmProfileListItem_controller_1 = __webpack_require__(327);
var scmProfileList_directive_1 = __webpack_require__(328);
__webpack_require__(329);
exports.default = function (module) {
    module.component("scmProfileList", scmProfileList_directive_1.default);
    module.controller("scmProfileListController", scmProfileList_controller_1.default);
    module.controller("scmProfileListItemController", scmProfileListItem_controller_1.default);
};


/***/ }),
/* 327 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ScmProfileListItemController = /** @class */ (function () {
    /** @ngInject */
    ScmProfileListItemController.$inject = ["scmNodesListDialogService"];
    function ScmProfileListItemController(scmNodesListDialogService) {
        this.scmNodesListDialogService = scmNodesListDialogService;
    }
    ScmProfileListItemController.prototype.openAssignedNodesDialog = function (profile) {
        this.scmNodesListDialogService.showModal(profile);
    };
    return ScmProfileListItemController;
}());
exports.default = ScmProfileListItemController;


/***/ }),
/* 328 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmProfileList_controller_1 = __webpack_require__(150);
var ProfileListDirective = /** @class */ (function () {
    function ProfileListDirective() {
        this.template = __webpack_require__(284);
        this.transclude = false;
        this.controller = scmProfileList_controller_1.default;
        this.controllerAs = "profileList";
        this.scope = {
            profiles: "=?",
            filteredProfilesIds: "=?",
            profileState: "=?",
            profilesToPreselect: "<?",
            displayToolbar: "=",
            onLoaded: "&?"
        };
    }
    return ProfileListDirective;
}());
exports.default = ProfileListDirective;


/***/ }),
/* 329 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 330 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmMonitoredNodeList_controller_1 = __webpack_require__(151);
var scmMonitoredNodeList_directive_1 = __webpack_require__(331);
__webpack_require__(332);
exports.default = function (module) {
    module.component("scmMonitoredNodeList", scmMonitoredNodeList_directive_1.MonitoredNodeListDirective);
    module.controller("scmMonitoredNodeListController", scmMonitoredNodeList_controller_1.MonitoredNodeListController);
};


/***/ }),
/* 331 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmMonitoredNodeList_controller_1 = __webpack_require__(151);
var MonitoredNodeListDirective = /** @class */ (function () {
    function MonitoredNodeListDirective() {
        this.template = __webpack_require__(285);
        this.transclude = false;
        this.controller = scmMonitoredNodeList_controller_1.MonitoredNodeListController;
        this.controllerAs = "monitoredNodeList";
        this.scope = {
            nodes: "=?",
            nodeState: "=?",
            nodesToPreselect: "<?",
            displayToolbar: "=",
            onLoaded: "&?"
        };
    }
    return MonitoredNodeListDirective;
}());
exports.MonitoredNodeListDirective = MonitoredNodeListDirective;


/***/ }),
/* 332 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 333 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var manageProfileDialog_controller_1 = __webpack_require__(152);
var manageProfileDialogService_1 = __webpack_require__(334);
var widgets_1 = __webpack_require__(53);
__webpack_require__(335);
exports.default = function (module) {
    templates.$inject = ["$templateCache"];
    module.service("scmManageProfileDialogService", manageProfileDialogService_1.ManageProfileDialogService);
    module.controller("scmManageProfileDialogController", manageProfileDialog_controller_1.default);
    if (widgets_1.TemplateLateLoader) {
        widgets_1.TemplateLateLoader.load({
            "manageProfileDialog-item": __webpack_require__(122),
            "multiedit-element-popover-content": __webpack_require__(123)
        });
    }
    /* @ngInject */
    function templates($templateCache) {
        $templateCache.put("manageProfileDialog-item", __webpack_require__(122));
        $templateCache.put("multiedit-element-popover-content", __webpack_require__(123));
    }
    module.app().run(templates);
};


/***/ }),
/* 334 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ManageProfileDialogService = /** @class */ (function () {
    /** @ngInject */
    ManageProfileDialogService.$inject = ["getTextService", "scmModalService", "$controller", "xuiToastService", "swUtil", "$q", "swDemoService", "scmProfileService"];
    function ManageProfileDialogService(getTextService, scmModalService, $controller, xuiToastService, swUtil, $q, swDemoService, scmProfileService) {
        this.getTextService = getTextService;
        this.scmModalService = scmModalService;
        this.$controller = $controller;
        this.xuiToastService = xuiToastService;
        this.swUtil = swUtil;
        this.$q = $q;
        this.swDemoService = swDemoService;
        this.scmProfileService = scmProfileService;
        this._t = getTextService;
    }
    ManageProfileDialogService.prototype.showAddDialog = function () {
        this.profile = {
            profile: {
                id: 0,
                name: "",
                description: "",
                builtIn: false,
                templateMappingRules: ""
            },
            profileElements: []
        };
        var dialogController = this.createDialogController(false, true);
        return this.showAddDialogInternal(dialogController);
    };
    ManageProfileDialogService.prototype.showAddDialogInternal = function (dialogController) {
        var _this = this;
        return this.showDialog({
            title: this._t("Add configuration profile"),
            actionButtonText: this._t("Add"),
            cancelButtonText: this._t("Cancel"),
            viewModel: dialogController,
            buttons: [
                {
                    name: "add-configuration-profile",
                    text: this._t("Add"),
                    isPrimary: true,
                    action: function () {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                            return;
                        }
                        return dialogController.add();
                    }
                }
            ],
        });
    };
    ManageProfileDialogService.prototype.createDialogController = function (isReadOnly, isConnectionStringRequired) {
        var dialogCtrl = this.$controller("scmManageProfileDialogController", { profile: this.profile, isReadOnly: isReadOnly, isConnectionStringRequired: isConnectionStringRequired });
        dialogCtrl.$onInit();
        return dialogCtrl;
    };
    ManageProfileDialogService.prototype.showDialog = function (dialogOptions, isReadOnly) {
        if (isReadOnly === void 0) { isReadOnly = false; }
        var dialogSettings = isReadOnly
            ? {
                template: __webpack_require__(289),
                size: "lg",
            } : {
            template: __webpack_require__(290),
            size: "lg",
        };
        return this.scmModalService.showModal(dialogSettings, dialogOptions);
    };
    ManageProfileDialogService.prototype.showEditDialog = function (profileToEdit, profileElements) {
        var _this = this;
        this.profile = {
            profile: profileToEdit,
            profileElements: profileElements
        };
        var dialogController = this.createDialogController(false, false);
        return this.showDialog({
            title: this._t("Edit configuration profile"),
            actionButtonText: this._t("Save"),
            cancelButtonText: this._t("Cancel"),
            viewModel: dialogController,
            buttons: [
                {
                    name: "save-configuration-profile",
                    text: this._t("Save"),
                    isPrimary: true,
                    action: function () {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                            return;
                        }
                        return dialogController.save();
                    }
                }
            ]
        });
    };
    ManageProfileDialogService.prototype.escapeRegExp = function (text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    };
    ManageProfileDialogService.prototype.createCopyProfileName = function (profileName, profiles) {
        var prefix = this._t("Copy of");
        var re = new RegExp("^" + this.escapeRegExp(prefix) + " " + this.escapeRegExp(profileName) + "( \\((\\d+)\\))?$", "i");
        var dupliciteCounter = -1;
        for (var _i = 0, profiles_1 = profiles; _i < profiles_1.length; _i++) {
            var p = profiles_1[_i];
            var found = re.exec(p.name);
            if (found) {
                if (found[2] && dupliciteCounter < Number(found[2])) {
                    dupliciteCounter = Number(found[2]);
                    continue;
                }
                if (dupliciteCounter < 0) {
                    dupliciteCounter = 0;
                }
            }
        }
        if (dupliciteCounter < 0) {
            return prefix + " " + profileName;
        }
        return prefix + " " + profileName + " (" + ++dupliciteCounter + ")";
    };
    ManageProfileDialogService.prototype.showCopyDialog = function (profileToCopy, profileElements, profiles, profileOrigin) {
        profileToCopy.name = this.createCopyProfileName(profileToCopy.name, profiles);
        this.profile = this.copyProfile(profileToCopy, profileElements, profileOrigin);
        var dialogController = this.createDialogController(false, false);
        return this.showAddDialogInternal(dialogController);
    };
    ManageProfileDialogService.prototype.copyProfile = function (profileToCopy, profileElements, profileOrigin) {
        var copiedProfile = _.cloneDeep(profileToCopy);
        copiedProfile.id = -1;
        copiedProfile.uniqueId = undefined;
        copiedProfile.assignedNodesCount = 0;
        copiedProfile.totalAssignedNodesCount = 0;
        copiedProfile.profileOrigin = profileOrigin;
        copiedProfile.originalProfileUniqueId = profileToCopy.uniqueId;
        return {
            profile: copiedProfile,
            profileElements: profileElements
        };
    };
    ManageProfileDialogService.prototype.showViewDialog = function (profileToEdit, profileElements) {
        this.profile = {
            profile: profileToEdit,
            profileElements: profileElements
        };
        var dialogController = this.createDialogController(true, false);
        return this.showDialog({
            title: this._t("Configuration profile details"),
            cancelButtonText: this._t("Close"),
            viewModel: dialogController,
            buttons: [],
        }, true);
    };
    ManageProfileDialogService.prototype.showImportErrorDialog = function (profileFile, duplicateProfileIds, validationErrors, notValidFiles, importedProfilesNames, profiles) {
        var _this = this;
        var defer = this.$q.defer();
        this.readFiles(profileFile).then(function (profilesToImport) {
            var duplicateProfiles = profilesToImport.filter(function (p) {
                if (!p) {
                    return false;
                }
                return duplicateProfileIds.some(function (id) { return id.toLowerCase() === (p.uniqueId ? p.uniqueId.toLowerCase() : p.uniqueId); });
            });
            _this.scmModalService.showModal({
                template: __webpack_require__(291),
                size: "md"
            }, {
                title: _this._t("Import errors"),
                status: "error",
                actionButtonText: _this._t("Import as a copy"),
                cancelButtonText: _this._t("Close"),
                viewModel: {
                    duplicateProfiles: duplicateProfiles,
                    importedProfilesNames: importedProfilesNames,
                    notValidFiles: notValidFiles,
                    validationErrors: validationErrors
                },
                buttons: [{
                        name: "copy",
                        isPrimary: true,
                        isHidden: duplicateProfiles.length === 0,
                        text: _this._t("Import as a copy"),
                        actionText: _this._t("Loading data, please wait..."),
                        action: function () {
                            if (duplicateProfileIds.length === 1) {
                                var profileToImport = duplicateProfiles[0];
                                var profileElements = _this.getProfileElementWebModels(profileToImport);
                                var profile = {
                                    name: profileToImport.name,
                                    description: profileToImport.description,
                                    templateMappingRules: profileToImport.templateMappingRules,
                                    uniqueId: profileToImport.uniqueId,
                                    profileOrigin: 2 /* Imported */
                                };
                                _this.showCopyDialog(profile, profileElements, profiles, 2 /* Imported */).finally(defer.resolve);
                            }
                            else {
                                var profilesToCreate_1 = [];
                                duplicateProfiles.forEach(function (p) {
                                    var profileToCopy = {
                                        name: _this.createCopyProfileName(p.name, profiles),
                                        description: p.description,
                                        templateMappingRules: p.templateMappingRules,
                                        originalProfileUniqueId: p.uniqueId,
                                        profileOrigin: 2 /* Imported */,
                                    };
                                    var profileElements = _this.getProfileElementWebModels(p);
                                    profilesToCreate_1.push(_this.copyProfile(profileToCopy, profileElements, 2 /* Imported */));
                                });
                                _this.scmProfileService.createProfiles(profilesToCreate_1)
                                    .then(function () {
                                    var importedCopyProfilesNames = profilesToCreate_1.map(function (p) { return p.profile.name; });
                                    _this.showImportedProfilesToast(importedCopyProfilesNames);
                                }).finally(defer.resolve);
                            }
                            return true;
                        }
                    }]
            }).then(function (res) {
                if (!res || res === "cancel") {
                    defer.resolve();
                }
            });
        });
        return defer.promise;
    };
    ManageProfileDialogService.prototype.readFiles = function (profileFiles) {
        var promises = [];
        for (var i = 0; i < profileFiles.length; i++) {
            promises.push(this.readSingleFile(profileFiles[i]));
        }
        return this.$q.all(promises);
    };
    ManageProfileDialogService.prototype.readSingleFile = function (profileFile) {
        var _this = this;
        var defer = this.$q.defer();
        var fReader = new FileReader();
        fReader.readAsText(profileFile, "utf-16");
        fReader.onload = function (e) {
            defer.resolve(_this.tryParseImportedProfile(e.target));
        };
        return defer.promise;
    };
    ManageProfileDialogService.prototype.tryParseImportedProfile = function (param) {
        try {
            return JSON.parse(param.result);
        }
        catch (_a) {
            return null;
        }
    };
    ManageProfileDialogService.prototype.getProfileElementWebModels = function (profile) {
        return _(profile.profileElements).map(function (x) {
            return {
                type: x.type,
                displayAlias: x.displayAlias,
                description: x.description,
                settings: {
                    pairs: JSON.parse(x.settings)
                },
                credentialId: -1
            };
        }).value();
    };
    ManageProfileDialogService.prototype.showImportedProfilesToast = function (importedProfiles) {
        if (importedProfiles.length === 1) {
            this.xuiToastService.success(this.swUtil.formatString(this._t("{0} profile imported: {1}"), importedProfiles.length, importedProfiles.join(", ")));
        }
        else {
            this.xuiToastService.success(this.swUtil.formatString(this._t("{0} profiles imported: {1}"), importedProfiles.length, importedProfiles.join(", ")));
        }
    };
    return ManageProfileDialogService;
}());
exports.ManageProfileDialogService = ManageProfileDialogService;


/***/ }),
/* 335 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 336 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var elementSettings_controller_1 = __webpack_require__(154);
var elementSettings_directive_1 = __webpack_require__(338);
exports.default = function (module) {
    module.component("scmElementSettings", elementSettings_directive_1.default);
    module.controller("scmElementSettingsController", elementSettings_controller_1.default);
};


/***/ }),
/* 337 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileParsingElementTypeText = (_a = {},
    _a["IisWebConfig" /* IisWebConfig */] = "IIS Web Configuration Parser",
    _a);
var _a;


/***/ }),
/* 338 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var elementSettings_controller_1 = __webpack_require__(154);
var ElementSettingsDirective = /** @class */ (function () {
    function ElementSettingsDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(292);
        this.transclude = false;
        this.controller = elementSettings_controller_1.default;
        this.controllerAs = "elementSettings";
        this.bindToController = {
            profile: "<",
            profileElement: "=",
            mode: "<",
            isConnectionStringRequired: "<",
            showPopover: "<",
            isMultiEdit: "<",
            multiEditCheckbox: "=",
            connectionStringPlaceholder: "<"
        };
    }
    return ElementSettingsDirective;
}());
exports.default = ElementSettingsDirective;


/***/ }),
/* 339 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var datetimePickerPopover_controller_1 = __webpack_require__(155);
var datetimePickerPopover_directive_1 = __webpack_require__(340);
__webpack_require__(341);
exports.default = function (module) {
    templates.$inject = ["$templateCache"];
    module.component("scmDatetimePickerPopover", datetimePickerPopover_directive_1.default);
    module.controller("scmDatetimePickerPopoverController", datetimePickerPopover_controller_1.default);
    /* @ngInject */
    function templates($templateCache) {
        $templateCache.put("scmDatetimePickerPopover", __webpack_require__(294));
    }
    module.app().run(templates);
};


/***/ }),
/* 340 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var datetimePickerPopover_controller_1 = __webpack_require__(155);
var DatetimePickerPopoverDirective = /** @class */ (function () {
    function DatetimePickerPopoverDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(293);
        this.replace = true;
        this.transclude = false;
        this.controller = datetimePickerPopover_controller_1.default;
        this.controllerAs = "ctrl";
        this.scope = {};
        this.bindToController = {
            id: "@popoverId",
            model: "=ngModel",
            onChange: "&?",
            caption: "@?",
            name: "@",
            isRequired: "<?",
            isDisabled: "<?",
            maxDate: "<?",
            minDate: "<?",
            baselineDate: "<?"
        };
    }
    return DatetimePickerPopoverDirective;
}());
exports.default = DatetimePickerPopoverDirective;


/***/ }),
/* 341 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 342 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmAssignProfileWizard_controller_1 = __webpack_require__(156);
var scmAssignProfileWizard_directive_1 = __webpack_require__(343);
var summary_1 = __webpack_require__(344);
exports.default = function (module) {
    summary_1.default(module);
    module.component("scmAssignProfileWizard", scmAssignProfileWizard_directive_1.default);
    module.controller("scmAssignProfileWizardController", scmAssignProfileWizard_controller_1.AssignProfileWizardController);
};


/***/ }),
/* 343 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmAssignProfileWizard_controller_1 = __webpack_require__(156);
var AssignProfileWizardDirective = /** @class */ (function () {
    function AssignProfileWizardDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(299);
        this.replace = true;
        this.transclude = false;
        this.controller = scmAssignProfileWizard_controller_1.AssignProfileWizardController;
        this.controllerAs = "vm";
        this.scope = {
            onLeave: "&?",
            initialSettings: "<?"
        };
    }
    return AssignProfileWizardDirective;
}());
exports.default = AssignProfileWizardDirective;


/***/ }),
/* 344 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var assignProfileSummary_directive_1 = __webpack_require__(345);
__webpack_require__(347);
exports.default = function (module) {
    module.component("scmAssignProfileSummary", assignProfileSummary_directive_1.default);
};


/***/ }),
/* 345 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var assignProfileSummary_controller_1 = __webpack_require__(346);
var AssignProfileSummary = /** @class */ (function () {
    function AssignProfileSummary() {
        this.restrict = "E";
        this.template = __webpack_require__(301);
        this.controller = assignProfileSummary_controller_1.AssignProfileSummaryController;
        this.controllerAs = "vm";
        this.scope = {};
        this.bindToController = {
            nodes: "<",
            profiles: "<",
            items: "=",
            overrideCredentials: "<",
            isBusy: "=",
        };
    }
    return AssignProfileSummary;
}());
exports.default = AssignProfileSummary;


/***/ }),
/* 346 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var AssignProfileSummaryController = /** @class */ (function () {
    /** @ngInject */
    AssignProfileSummaryController.$inject = ["$scope", "$templateCache", "swUtil", "getTextService", "scmAssetInventoryService", "scmLocationService", "scmFilteredListService", "scmValidationService", "scmOrionNodeService"];
    function AssignProfileSummaryController($scope, $templateCache, swUtil, getTextService, scmAssetInventoryService, scmLocationService, scmFilteredListService, scmValidationService, scmOrionNodeService) {
        this.$scope = $scope;
        this.$templateCache = $templateCache;
        this.swUtil = swUtil;
        this.getTextService = getTextService;
        this.scmAssetInventoryService = scmAssetInventoryService;
        this.scmLocationService = scmLocationService;
        this.scmFilteredListService = scmFilteredListService;
        this.scmValidationService = scmValidationService;
        this.scmOrionNodeService = scmOrionNodeService;
        this._t = this.getTextService;
        this.items = [];
        this.gridOptions = {
            searchableColumns: ["$search"],
            hidePagination: true
        };
        this.sorting = {
            sortableColumns: [{
                    id: "$sortIssues",
                    label: this._t("Issue"),
                }, {
                    id: "node.name",
                    label: this._t("Name"),
                }],
            sortBy: {
                id: "$sortIssues",
                label: this._t("Issue"),
            },
            direction: "desc"
        };
        this.pagination = {
            page: 1,
            pageSize: Number.MAX_VALUE
        };
    }
    AssignProfileSummaryController.prototype.$onInit = function () {
        var _this = this;
        this.$templateCache.put("scmAssignProfileSummary-item", __webpack_require__(300));
        this.scmLocationService.getHelpLink("OrionSCMExplanationsOfErrorMessages")
            .then(function (helpLink) { return _this.prereqsHelpLink = helpLink; });
        this.$scope.$watchCollection("vm.nodes.concat(vm.profiles)", function () {
            if (_this.nodes && _this.profiles) {
                _this.revalidate();
            }
        });
    };
    AssignProfileSummaryController.prototype.reload = function () {
        var _this = this;
        var nodeListDataSourceParameters = this.scmFilteredListService.getNodeListDataSourceParametersByFiltersAndSearch(this.nodes.map(function (n) { return n.nodeId; }), false, undefined, undefined);
        nodeListDataSourceParameters.includeProfileHeaders = true;
        this.isBusy = true;
        return this.scmFilteredListService.getFilteredNodes(nodeListDataSourceParameters)
            .then(function (nodes) {
            _this.nodes = nodes.items;
        })
            .finally(function () {
            _this.isBusy = false;
        });
    };
    AssignProfileSummaryController.prototype.revalidate = function () {
        var _this = this;
        this.isBusy = true;
        return this.scmValidationService.validateOsCompatibility(this.nodes.map(function (node) { return ({
            nodeId: node.nodeId,
            profileIds: _this.profiles.map(function (p) { return p.id; }),
        }); }))
            .then(function (incompatibilities) {
            _this.incompatibilities = incompatibilities;
            _this.refresh();
        })
            .finally(function () {
            _this.isBusy = false;
        });
    };
    AssignProfileSummaryController.prototype.refresh = function () {
        this.refreshItems();
        this.refreshDeployAllAgents();
        this.refreshTotalIssues();
    };
    AssignProfileSummaryController.prototype.refreshItems = function () {
        var _this = this;
        this.items = this.nodes.map(function (node) {
            var oldItem = _.find(_this.items, function (row) { return row.node.nodeId === node.nodeId; });
            var assignedProfiles = node.profiles.map(function (profile) { return (__assign({}, profile, { assignState: "assigned" })); });
            var assigningProfiles = _this.profiles.map(function (profile) {
                var oldProfile = oldItem ? _.find(oldItem.profiles, function (p) { return p.id === profile.id; }) : undefined;
                return __assign({}, profile, { assignState: oldProfile ? oldProfile.assignState : "assigning" });
            });
            var profiles = _.chain(assignedProfiles).concat(assigningProfiles).uniqBy(function (p) { return p.id; }).sortBy(function (p) { return p.name; }).value();
            var incompatibility = _.find(_this.incompatibilities, function (inc) { return inc.nodeId === node.nodeId; });
            var issues = _this.computeSummaryIssues(node, profiles.filter(function (p) { return p.assignState === "assigning"; }), incompatibility);
            return {
                node: node,
                profiles: profiles,
                issues: issues,
                $search: _this.createSearchString(node, profiles),
                $sortIssues: _this.createSortString(node, issues),
            };
        });
        if (_.chain(this.items).reduce(function (acc, item) { return acc + ("aiInProgress" in item.issues ? 1 : 0); }, 0).value() === 0) {
            this.bulkAiDiscoveryInProgress = false;
        }
    };
    AssignProfileSummaryController.prototype.refreshDeployAllAgents = function () {
        var nodes = this.items
            .filter(function (row) { return "needAgent" in row.issues; })
            .map(function (row) { return row.node.nodeId; });
        this.deployAllAgentsIds = nodes.length === 0
            ? null
            : nodes;
    };
    AssignProfileSummaryController.prototype.refreshTotalIssues = function () {
        this.totalIssueCount = _.reduce(this.items, function (acc, row) { return acc + row.issues.$count; }, 0);
    };
    AssignProfileSummaryController.prototype.computeSummaryIssues = function (node, profiles, incompatibility) {
        var issues = {};
        this.fillNeedAi(issues, node, profiles);
        this.fillNeedAgent(issues, node, profiles);
        this.fillNeedUpgrade(issues, node, profiles);
        this.fillIncompatibles(issues, incompatibility, profiles);
        this.fillAiInProgress(issues, node, profiles);
        issues.$count = _.chain(issues).reduce(function (acc, issue) { return acc + issue.affected.length; }, 0).value();
        return issues;
    };
    AssignProfileSummaryController.prototype.fillNeedAi = function (errors, node, profiles) {
        var _this = this;
        if (node.assetInventoryStatus === "notEnabled" /* NotEnabled */) {
            var affected = profiles.filter(function (p) { return _this.scmAssetInventoryService.needsAssetInventory(p); });
            if (affected.length > 0) {
                errors.needAi = {
                    affected: affected.map(function (p) { return "'" + p.name + "'"; }),
                };
                _.each(affected, function (p) { return p.hasIssue = true; });
            }
        }
    };
    AssignProfileSummaryController.prototype.fillNeedAgent = function (errors, node, profiles) {
        if (!node.isAgentDeployed) {
            var affected = profiles.filter(function (p) { return p.needsAgent; });
            if (affected.length > 0) {
                errors.needAgent = {
                    affected: affected.map(function (p) { return "'" + p.name + "'"; }),
                };
                _.each(affected, function (p) { return p.hasIssue = true; });
            }
        }
    };
    AssignProfileSummaryController.prototype.fillNeedUpgrade = function (errors, node, profiles) {
        if (node.isAgentSupportedOs === false) {
            var affected = profiles.filter(function (p) { return p.needsAgent; });
            if (affected.length > 0) {
                errors.needUpgrade = {
                    affected: affected.map(function (p) { return "'" + p.name + "'"; }),
                };
                // Need Upgrade is stronger than Need Agent
                delete errors.needAgent;
                _.each(affected, function (p) { return p.hasIssue = true; });
            }
        }
    };
    AssignProfileSummaryController.prototype.fillAiInProgress = function (errors, node, profiles) {
        var _this = this;
        if (node.assetInventoryStatus === "discoveryInProgress" /* DiscoveryInProgress */) {
            var affected = profiles.filter(function (p) { return _this.scmAssetInventoryService.needsAssetInventory(p); });
            if (affected.length > 0) {
                errors.aiInProgress = {
                    affected: affected.map(function (p) { return "'" + p.name + "'"; }),
                };
                _.each(affected, function (p) { return p.hasIssue = true; });
            }
        }
    };
    AssignProfileSummaryController.prototype.fillIncompatibles = function (errors, incompatibility, profiles) {
        var _this = this;
        if (incompatibility) {
            var affected = profiles.filter(function (p) { return _.some(incompatibility.profiles, function (ip) { return ip.profileId === p.id; }); });
            if (affected.length > 0) {
                var affectedTypes = _(incompatibility.profiles)
                    .flatMap(function (p) { return p.elementTypes; }).uniq()
                    .map(function (t) { return _this.readableIncompatibilityElementType(t); })
                    .map(function (str, i) { return i === 0 ? _.upperFirst(str) : str; }) // make first letter of first type uppercase
                    .value();
                var error = {
                    affected: affected.map(function (p) { return "'" + p.name + "'"; }),
                    affectedTypes: affectedTypes,
                };
                switch (incompatibility.osType) {
                    case "Windows" /* Windows */:
                        errors.windowsIncompatible = error;
                        break;
                    case "Linux" /* Linux */:
                        errors.linuxIncompatible = error;
                        break;
                    default:
                        // For unknown OS we don't show any incompatibility messages, we will let it fail in polling
                        break;
                }
                // Node/Profile incompatibility is stronger than Need Upgrade
                delete errors.needUpgrade;
                _.each(affected, function (p) { return p.hasIssue = true; });
            }
        }
    };
    AssignProfileSummaryController.prototype.readableIncompatibilityElementType = function (type) {
        switch (type) {
            case "fileWithLinuxFilePath" /* FileWithLinuxFilePath */: return this._t("files with Linux filepaths");
            case "fileWithWindowsFilePath" /* FileWithWindowsFilePath */: return this._t("files with Windows filepaths");
            case "registry" /* Registry */: return this._t("registry entries");
            case "script" /* Script */: return this._t("Linux script outputs");
            case "powershell" /* Powershell */: return this._t("PowerShell outputs");
        }
    };
    AssignProfileSummaryController.prototype.createSearchString = function (node, profiles) {
        return [
            node.name,
            node.ipAddress
        ].concat(profiles.map(function (p) { return p.name; })).join("\0");
    };
    AssignProfileSummaryController.prototype.createSortString = function (node, issues) {
        return [
            _.padStart(issues.$count.toString(), 6, "0"),
            node.name,
        ].join("\0");
    };
    AssignProfileSummaryController.prototype.getAiCandidates = function () {
        var nodes = this.items
            .filter(function (row) { return "needAi" in row.issues; })
            .map(function (row) { return row.node; });
        return nodes;
    };
    AssignProfileSummaryController.prototype.enableBulkAssetInventory = function (nodes) {
        this.bulkAiDiscoveryInProgress = true;
        this.enableAssetInventory(nodes);
    };
    AssignProfileSummaryController.prototype.enableAssetInventory = function (nodes) {
        var _this = this;
        this.scmAssetInventoryService.showConfirmationDialog(nodes).then(function () {
            return _this.reload();
        });
    };
    AssignProfileSummaryController.prototype.humanJoin = function (array) {
        var _this = this;
        var join = function (list, item) { return _this.swUtil.formatString(_this._t("{0}, {1}"), list, item); };
        var joinLast = function (list, item) { return _this.swUtil.formatString(_this._t("{0} and {1}"), list, item); };
        switch (array.length) {
            case 0: return "";
            case 1: return _.first(array);
            default: return [
                _.chain(array).initial().reduce(join).value(),
                _.last(array)
            ].reduce(joinLast);
        }
    };
    AssignProfileSummaryController.prototype.removeProfile = function (row, profile) {
        // 'Smart' grid creates a copy of all items it's showing
        // so we need to change both actual 'smart' item to hide it
        // and original item to recount issues correctly
        _(this.items)
            .filter(function (r) { return r.node.nodeId === row.node.nodeId; })
            .flatMap(function (r) { return r.profiles; })
            .filter(function (p) { return p.id === profile.id; })
            .first()
            .assignState = "skipping";
        profile.assignState = "skipping";
        this.refresh();
    };
    AssignProfileSummaryController.prototype.profileStatusColor = function (profile) {
        if (profile.hasIssue) {
            return "warning";
        }
        if (profile.assignState === "assigning") {
            return "info";
        }
        return "";
    };
    AssignProfileSummaryController.prototype.profileStatusIcon = function (profile) {
        if (profile.hasIssue) {
            return "status_warning";
        }
        return "";
    };
    return AssignProfileSummaryController;
}());
exports.AssignProfileSummaryController = AssignProfileSummaryController;


/***/ }),
/* 347 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 348 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var nodesAssignedToProfileList_directive_1 = __webpack_require__(349);
__webpack_require__(351);
exports.default = function (module) {
    module.component("scmNodesAssignedToProfileList", nodesAssignedToProfileList_directive_1.default);
};


/***/ }),
/* 349 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var nodesAssignedToProfileList_controller_1 = __webpack_require__(350);
var NodesAssignedToProfileListDirective = /** @class */ (function () {
    function NodesAssignedToProfileListDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(303);
        this.transclude = false;
        this.controller = nodesAssignedToProfileList_controller_1.default;
        this.controllerAs = "nodesAssignedToProfileListController";
        this.scope = {
            profile: "=?",
            nodes: "=?"
        };
    }
    return NodesAssignedToProfileListDirective;
}());
exports.default = NodesAssignedToProfileListDirective;


/***/ }),
/* 350 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NodesAssignedToProfileListController = /** @class */ (function () {
    /** @ngInject */
    NodesAssignedToProfileListController.$inject = ["$q", "$scope", "getTextService", "xuiFilteredListService", "xuiArraySourceFactoryService", "scmFilteredListService", "$templateCache", "$log"];
    function NodesAssignedToProfileListController($q, $scope, getTextService, xuiFilteredListService, xuiArraySourceFactoryService, scmFilteredListService, $templateCache, $log) {
        this.$q = $q;
        this.$scope = $scope;
        this.getTextService = getTextService;
        this.xuiFilteredListService = xuiFilteredListService;
        this.xuiArraySourceFactoryService = xuiArraySourceFactoryService;
        this.scmFilteredListService = scmFilteredListService;
        this.$templateCache = $templateCache;
        this.$log = $log;
        this._t = getTextService;
    }
    NodesAssignedToProfileListController.prototype.$onInit = function () {
        this.profile = this.$scope.profile;
        this.nodes = this.$scope.nodes;
        this.importTemplates();
        this.createNodesState();
        this.createNodesDispatcher();
        this.loadAllNodes();
    };
    NodesAssignedToProfileListController.prototype.createNodesState = function () {
        this.nodesState = {
            sorting: {
                sortableColumns: [
                    { id: "name", label: this._t("Name") },
                    { id: "nodeStatus", label: this._t("Status") },
                ],
                sortBy: {
                    id: "name",
                    label: this._t("Name")
                },
                direction: "asc"
            },
            pagination: {
                page: 1,
                pageSize: 10,
                total: 200
            },
            options: {
                layout: "contained",
                stripe: false,
                selectionProperty: "id",
                trackBy: "id",
                templateUrl: "scm-nodes-assigned-to-profile-list-item",
                rowPadding: "narrow",
                showAddRemoveFilterProperty: false,
            },
            sidebarCollapsed: true,
            filters: {
                filterProperties: [
                    {
                        id: "nodeStatus",
                        title: this._t("Status"),
                        checkboxes: [
                            {
                                id: "1",
                                title: this._t("Up"),
                                count: undefined
                            },
                            {
                                id: "2",
                                title: this._t("Down"),
                                count: undefined
                            },
                            {
                                id: "3",
                                title: this._t("Warning"),
                                count: undefined
                            },
                            {
                                id: "14",
                                title: this._t("Critical"),
                                count: undefined
                            },
                            {
                                id: "9",
                                title: this._t("Unmanaged"),
                                count: undefined
                            }
                        ],
                    }
                ],
            },
        };
    };
    NodesAssignedToProfileListController.prototype.createNodesDispatcher = function () {
        this.nodesDispatcher = this.xuiFilteredListService.getDispatcherInstance(this.xuiFilteredListService.getModel(this, "nodesState"), {
            dataSource: {
                getItems: this.getNodesSources.bind(this)
            }
        });
    };
    NodesAssignedToProfileListController.prototype.getNodesSources = function (params) {
        var _this = this;
        return this.nodeLoadingRequest.then(function (data) {
            _this.nodesState.pagination.total = data.length;
            var result = _(data).map(function (x) {
                return {
                    id: x.nodeId,
                    name: x.name,
                    ipAddress: x.ipAddress,
                    machineType: x.machineType,
                    vendorIcon: x.vendorIcon,
                    nodeStatus: x.nodeStatus,
                    detailsUrl: x.detailsUrl,
                    isServer: x.isServer,
                    pollingMethod: x.pollingMethod,
                    isAgentDeployed: x.isAgentDeployed,
                    profileCount: x.profileCount
                };
            }).value();
            var nodesSource = _this.xuiArraySourceFactoryService.createItemSource(result, {
                propertiesToSearch: ["name"]
            });
            return nodesSource(params);
        })
            .catch(function (error) {
            _this.$log.error(error);
        });
    };
    NodesAssignedToProfileListController.prototype.loadAllNodes = function () {
        var _this = this;
        if (this.profile === null) {
            this.nodeLoadingRequest = this.$q.resolve(this.nodes);
        }
        else {
            this.nodeLoadingRequest = this.scmFilteredListService.getScmNodesByProfileId(this.profile.id)
                .then(function (nodes) {
                _this.nodes = nodes;
                return nodes;
            });
        }
    };
    NodesAssignedToProfileListController.prototype.importTemplates = function () {
        this.$templateCache.put("scm-nodes-assigned-to-profile-list-item", __webpack_require__(302));
    };
    return NodesAssignedToProfileListController;
}());
exports.default = NodesAssignedToProfileListController;


/***/ }),
/* 351 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 352 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(353);
var scmUnassignProfilesDialog_controller_1 = __webpack_require__(157);
var scmUnassignProfilesDialogService_1 = __webpack_require__(354);
var scmUnassignProfileDialog_directive_1 = __webpack_require__(355);
var scmUnassignProfilesControllerState_1 = __webpack_require__(356);
exports.default = function (module) {
    module.service("scmUnassignProfilesDialogService", scmUnassignProfilesDialogService_1.UnassignProfilesDialogService);
    module.service("scmUnassignProfilesControllerState", scmUnassignProfilesControllerState_1.UnassignProfilesControllerState);
    module.component("scmUnassignProfileDialog", scmUnassignProfileDialog_directive_1.default);
    module.controller("scmUnassignProfilesDialogController", scmUnassignProfilesDialog_controller_1.UnassignProfilesDialogController);
};


/***/ }),
/* 353 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 354 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var UnassignProfilesDialogService = /** @class */ (function () {
    /** @ngInject */
    UnassignProfilesDialogService.$inject = ["$rootScope", "getTextService", "scmModalService", "swDemoService", "scmKeepDataDialogService", "xuiToastService", "scmUnassignProfilesControllerState"];
    function UnassignProfilesDialogService($rootScope, getTextService, scmModalService, swDemoService, scmKeepDataDialogService, xuiToastService, scmUnassignProfilesControllerState) {
        this.$rootScope = $rootScope;
        this.getTextService = getTextService;
        this.scmModalService = scmModalService;
        this.swDemoService = swDemoService;
        this.scmKeepDataDialogService = scmKeepDataDialogService;
        this.xuiToastService = xuiToastService;
        this.scmUnassignProfilesControllerState = scmUnassignProfilesControllerState;
        this._t = getTextService;
    }
    UnassignProfilesDialogService.prototype.showModal = function (nodeIds, refreshAction) {
        return this.showUnassignedModal(nodeIds, refreshAction);
    };
    UnassignProfilesDialogService.prototype.showUnassignedModal = function (nodeIds, refreshAction) {
        var _this = this;
        var $scope = this.$rootScope.$new();
        $scope.nodeIds = nodeIds;
        var unassignButton = {
            name: "unassign-profiles",
            text: this._t("Unassign"),
            isPrimary: true,
            action: function () {
                if (_this.swDemoService.isDemoMode()) {
                    _this.swDemoService.showDemoErrorToast();
                    return;
                }
                return _this.scmKeepDataDialogService.showKeepDataModal(function (keepData) {
                    _this.scmUnassignProfilesControllerState.unassign(keepData)
                        .then(function () {
                        _this.xuiToastService.success(_this._t("SCM Profiles successfully unassigned."));
                        refreshAction();
                    })
                        .catch(function () {
                        _this.xuiToastService.error(_this._t("Unable to unassign profiles from nodes."));
                    });
                })
                    .then(function (result) { return result !== "cancel"; });
            },
            isDisabled: false
        };
        $scope.$watch(function () { return _this.scmUnassignProfilesControllerState.hasSelectedItems; }, function (newValue) {
            unassignButton.isDisabled = !newValue;
        });
        var dialogOptions = {
            title: this._t("Unassign profiles"),
            actionButtonText: this._t("Unassign"),
            cancelButtonText: this._t("Cancel"),
            viewModel: $scope,
            buttons: [unassignButton]
        };
        var dialogSettings = {
            template: __webpack_require__(305),
            size: "md",
        };
        return this.scmModalService.showModal(dialogSettings, dialogOptions);
    };
    return UnassignProfilesDialogService;
}());
exports.UnassignProfilesDialogService = UnassignProfilesDialogService;


/***/ }),
/* 355 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmUnassignProfilesDialog_controller_1 = __webpack_require__(157);
var UnassignProfileDialogDirective = /** @class */ (function () {
    function UnassignProfileDialogDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(306);
        this.replace = true;
        this.transclude = false;
        this.controller = scmUnassignProfilesDialog_controller_1.UnassignProfilesDialogController;
        this.controllerAs = "vm";
        this.scope = {
            onLeave: "&?",
            nodeIds: "<?"
        };
    }
    return UnassignProfileDialogDirective;
}());
exports.default = UnassignProfileDialogDirective;


/***/ }),
/* 356 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var UnassignProfilesControllerState = /** @class */ (function () {
    /** @ngInject */
    UnassignProfilesControllerState.$inject = ["$q", "$log", "scmProfileService", "scmFilteredListService", "swDemoService"];
    function UnassignProfilesControllerState($q, $log, scmProfileService, scmFilteredListService, swDemoService) {
        this.$q = $q;
        this.$log = $log;
        this.scmProfileService = scmProfileService;
        this.scmFilteredListService = scmFilteredListService;
        this.swDemoService = swDemoService;
        this.nodes = [];
        this.nodeIds = [];
        this.profiles = [];
    }
    Object.defineProperty(UnassignProfilesControllerState.prototype, "hasSelectedItems", {
        get: function () {
            if (this.selection && this.selection.items) {
                return this.selection.items.length !== 0;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    UnassignProfilesControllerState.prototype.loadProfiles = function (nodeIds) {
        var _this = this;
        this.nodeIds = nodeIds;
        this.isBusy = true;
        this.scmFilteredListService.getFilteredScmNodes({ nodeIdFilter: this.nodeIds, includeProfileHeaders: true })
            .then(function (nodeList) {
            // items should be filtered in the backend, but lets make sure the data are correct
            _this.nodes = nodeList.items.filter(function (node) {
                return _(_this.nodeIds).indexOf(node.nodeId) >= 0;
            });
            _this.profiles = _this.filterAssignedProfiles(_this.nodes);
            _this.selection.items = [];
            _this.selection.blacklist = false;
            _this.isBusy = false;
        });
    };
    UnassignProfilesControllerState.prototype.filterAssignedProfiles = function (nodes) {
        var result = [];
        for (var _i = 0, nodes_1 = nodes; _i < nodes_1.length; _i++) {
            var node = nodes_1[_i];
            var _loop_1 = function (profile) {
                if (!_(result).some(function (item) { return item.id === profile.id; })) {
                    result.push(profile);
                }
            };
            for (var _a = 0, _b = node.profiles; _a < _b.length; _a++) {
                var profile = _b[_a];
                _loop_1(profile);
            }
        }
        ;
        return result;
    };
    UnassignProfilesControllerState.prototype.unassign = function (keepHistory) {
        var _this = this;
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return this.$q.resolve(false);
        }
        if (!this.hasSelectedItems) {
            return this.$q.resolve(false);
        }
        var profilesToUnassign = this.selection.blacklist
            ? this.profiles.filter(function (profile) { return !_(_this.selection.items).includes(profile.id); }).map(function (p) { return p.id; })
            : this.selection.items;
        var deletion = {
            nodesProfilesAssignments: this.nodes.map(function (n) { return ({
                nodeId: n.nodeId,
                profileIds: profilesToUnassign
            }); }),
            keepHistory: keepHistory
        };
        this.isBusy = true;
        return this.scmProfileService.unassignFromNodes(deletion)
            .then(function () {
            return true;
        }).catch(function (reason) {
            _this.$log.error(reason);
            return false;
        }).finally(function () {
            _this.isBusy = false;
        });
    };
    return UnassignProfilesControllerState;
}());
exports.UnassignProfilesControllerState = UnassignProfilesControllerState;


/***/ }),
/* 357 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(358);
var scmContentMessagePopover_controller_1 = __webpack_require__(121);
var scmContentMessagePopover_directive_1 = __webpack_require__(278);
exports.default = function (module) {
    module.component("scmContentMessagePopover", scmContentMessagePopover_directive_1.default);
    module.controller("scmContentMessagePopoverController", scmContentMessagePopover_controller_1.default);
};


/***/ }),
/* 358 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 359 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmProfilesCount_controller_1 = __webpack_require__(158);
var scmProfilesCount_directive_1 = __webpack_require__(360);
exports.default = function (module) {
    module.component("scmProfilesCount", scmProfilesCount_directive_1.default);
    module.controller("scmProfilesCountController", scmProfilesCount_controller_1.default);
};


/***/ }),
/* 360 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var scmProfilesCount_controller_1 = __webpack_require__(158);
var ScmProfilesCountDirective = /** @class */ (function () {
    function ScmProfilesCountDirective() {
        this.scope = {};
        this.restrict = "E";
        this.template = __webpack_require__(308);
        this.replace = true;
        this.transclude = false;
        this.controller = scmProfilesCount_controller_1.default;
        this.controllerAs = "ctrl";
        this.bindToController = {
            nodeId: "<",
            profilesCount: "<"
        };
    }
    return ScmProfilesCountDirective;
}());
exports.default = ScmProfilesCountDirective;


/***/ }),
/* 361 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmNodesCount_controller_1 = __webpack_require__(159);
var scmNodesCount_directive_1 = __webpack_require__(362);
exports.default = function (module) {
    module.component("scmNodesCount", scmNodesCount_directive_1.default);
    module.controller("scmNodesCountController", scmNodesCount_controller_1.default);
};


/***/ }),
/* 362 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var scmNodesCount_controller_1 = __webpack_require__(159);
var ScmNodesCountDirective = /** @class */ (function () {
    function ScmNodesCountDirective() {
        this.scope = {};
        this.restrict = "E";
        this.template = __webpack_require__(310);
        this.replace = true;
        this.transclude = false;
        this.controller = scmNodesCount_controller_1.default;
        this.controllerAs = "ctrl";
        this.bindToController = {
            nodes: "<",
            nodesDialogTitle: "@nodesDialogTitle"
        };
    }
    return ScmNodesCountDirective;
}());
exports.default = ScmNodesCountDirective;


/***/ }),
/* 363 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var keepDataDialogService_1 = __webpack_require__(364);
exports.default = function (module) {
    module.service("scmKeepDataDialogService", keepDataDialogService_1.KeepDataDialogService);
};


/***/ }),
/* 364 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var KeepDataDialogService = /** @class */ (function () {
    /** @ngInject */
    KeepDataDialogService.$inject = ["$rootScope", "getTextService", "scmModalService"];
    function KeepDataDialogService($rootScope, getTextService, scmModalService) {
        this.$rootScope = $rootScope;
        this.getTextService = getTextService;
        this.scmModalService = scmModalService;
        this._t = getTextService;
    }
    KeepDataDialogService.prototype.showKeepDataModal = function (action) {
        var dialogOptions = {
            title: this._t("Keep historical data?"),
            cancelButtonText: this._t("Cancel"),
            buttons: [
                {
                    name: "keep-data",
                    text: this._t("Keep data"),
                    isPrimary: false,
                    displayStyle: "secondary",
                    action: function () { return action(true); }
                },
                {
                    name: "delete-data",
                    text: this._t("Delete data"),
                    isPrimary: false,
                    displayStyle: "secondary",
                    action: function () { return action(false); }
                }
            ]
        };
        var dialogSettings = {
            template: __webpack_require__(311),
            size: "md",
        };
        return this.scmModalService.showModal(dialogSettings, dialogOptions);
    };
    return KeepDataDialogService;
}());
exports.KeepDataDialogService = KeepDataDialogService;


/***/ }),
/* 365 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var deleteBaselinesDialogService_1 = __webpack_require__(366);
exports.default = function (module) {
    module.service("scmDeleteBaselinesDialogService", deleteBaselinesDialogService_1.DeleteBaselinesDialogService);
};


/***/ }),
/* 366 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DeleteBaselinesDialogService = /** @class */ (function () {
    /** @ngInject */
    DeleteBaselinesDialogService.$inject = ["getTextService", "scmModalService", "scmBaselineService", "scmLocationService", "swDemoService"];
    function DeleteBaselinesDialogService(getTextService, scmModalService, scmBaselineService, scmLocationService, swDemoService) {
        var _this = this;
        this.getTextService = getTextService;
        this.scmModalService = scmModalService;
        this.scmBaselineService = scmBaselineService;
        this.scmLocationService = scmLocationService;
        this.swDemoService = swDemoService;
        this._t = getTextService;
        this.scmLocationService.getHelpLink("OrionSCMDefineaBaseline")
            .then(function (helpLink) { return _this.deleteBaselineHelplink = helpLink; });
    }
    DeleteBaselinesDialogService.prototype.showDeleteBaselinesModal = function (nodeIds) {
        var _this = this;
        var dialogOptions;
        var dialogSettings;
        if (nodeIds.length > 0) {
            dialogOptions = {
                title: this._t("Delete baselines?"),
                status: "warning",
                viewModel: {
                    nodesCount: nodeIds.length,
                    nodeWord: nodeIds.length === 1 ? this._t("node") : this._t("nodes"),
                    helpLink: this.deleteBaselineHelplink
                },
                cancelButtonText: this._t("Cancel"),
                buttons: [
                    {
                        name: "delete-baselines",
                        text: this._t("Delete baselines"),
                        isPrimary: false,
                        displayStyle: "tertiary",
                        action: function () {
                            if (_this.swDemoService.isDemoMode()) {
                                _this.swDemoService.showDemoErrorToast();
                                return true;
                            }
                            return _this.scmBaselineService.deleteBaselines(nodeIds)
                                .then(function () { return true; })
                                .catch(function () { return false; });
                        }
                    }
                ]
            };
            dialogSettings = {
                template: __webpack_require__(312),
                size: "md",
            };
        }
        else {
            dialogOptions = {
                title: this._t("No baselines to delete"),
                status: "warning",
                viewModel: {
                    helplink: this.deleteBaselineHelplink
                },
                cancelButtonText: this._t("Close")
            };
            dialogSettings = {
                template: __webpack_require__(313),
                size: "md",
            };
        }
        return this.scmModalService.showModal(dialogSettings, dialogOptions);
    };
    return DeleteBaselinesDialogService;
}());
exports.DeleteBaselinesDialogService = DeleteBaselinesDialogService;


/***/ }),
/* 367 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var bulkUpdateBaselinesDialogService_1 = __webpack_require__(368);
exports.default = function (module) {
    module.service("scmBulkUpdateBaselinesDialogService", bulkUpdateBaselinesDialogService_1.BulkUpdateBaselinesDialogService);
};


/***/ }),
/* 368 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var BulkUpdateBaselinesDialogService = /** @class */ (function () {
    /** @ngInject */
    BulkUpdateBaselinesDialogService.$inject = ["getTextService", "scmModalService", "scmLocationService", "swDemoService"];
    function BulkUpdateBaselinesDialogService(getTextService, scmModalService, scmLocationService, swDemoService) {
        var _this = this;
        this.getTextService = getTextService;
        this.scmModalService = scmModalService;
        this.scmLocationService = scmLocationService;
        this.swDemoService = swDemoService;
        this._t = getTextService;
        this.scmLocationService.getHelpLink("OrionSCMDefineaBaseline")
            .then(function (helpLink) { return _this.redefineBaselineHelplink = helpLink; });
    }
    BulkUpdateBaselinesDialogService.prototype.showModal = function (nodes) {
        var _this = this;
        var baselineStatuses = _(nodes).groupBy(function (n) { return n.baselineInfo.baselineStatus; })
            .defaults((_a = {},
            _a["IsBaseline" /* IsBaseline */] = [],
            _a["NoBaselineSet" /* NoBaselineSet */] = [],
            _a["MatchesBaseline" /* MatchesBaseline */] = [],
            _a["DiffersFromBaseline" /* DiffersFromBaseline */] = [],
            _a))
            .value();
        var dialogOptions = {
            title: this._t("Redefine baselines?"),
            status: "warning",
            viewModel: {
                nodesCount: nodes.length,
                nodeWord: nodes.length === 1 ? this._t("node") : this._t("nodes"),
                nodesWithoutBaseline: baselineStatuses["NoBaselineSet" /* NoBaselineSet */],
                nodesMismatchingBaseline: baselineStatuses["DiffersFromBaseline" /* DiffersFromBaseline */],
                nodesMatchingBaseline: _.merge(baselineStatuses["MatchesBaseline" /* MatchesBaseline */], baselineStatuses["IsBaseline" /* IsBaseline */]),
                helpLink: this.redefineBaselineHelplink
            },
            cancelButtonText: this._t("Cancel"),
            buttons: [
                {
                    name: "bulk-update-baselines",
                    text: this._t("Redefine baselines"),
                    isPrimary: false,
                    displayStyle: "tertiary",
                    action: function () {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                        }
                        return true;
                    }
                }
            ]
        };
        var dialogSettings = {
            template: __webpack_require__(314),
            size: "md",
        };
        return this.scmModalService.showModal(dialogSettings, dialogOptions);
        var _a;
    };
    return BulkUpdateBaselinesDialogService;
}());
exports.BulkUpdateBaselinesDialogService = BulkUpdateBaselinesDialogService;


/***/ }),
/* 369 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var selectTestNodeDialogService_1 = __webpack_require__(370);
exports.default = function (module) {
    module.service("scmSelectTestNodeDialogService", selectTestNodeDialogService_1.SelectTestNodeDialogService);
};


/***/ }),
/* 370 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SelectTestNodeDialogService = /** @class */ (function () {
    /** @ngInject */
    SelectTestNodeDialogService.$inject = ["$q", "$rootScope", "getTextService", "scmModalService", "scmFilteredListService"];
    function SelectTestNodeDialogService($q, $rootScope, getTextService, scmModalService, scmFilteredListService) {
        var _this = this;
        this.$q = $q;
        this.$rootScope = $rootScope;
        this.getTextService = getTextService;
        this.scmModalService = scmModalService;
        this.scmFilteredListService = scmFilteredListService;
        this.testJobNodeSettingName = "scmTestNodeSetting";
        this._t = getTextService;
        this.loadTestJobNodeDataFromStorage().then(function (node) {
            _this.testJobNode = node;
        });
    }
    Object.defineProperty(SelectTestNodeDialogService.prototype, "selectedTestJobNode", {
        get: function () {
            return this.testJobNode;
        },
        enumerable: true,
        configurable: true
    });
    SelectTestNodeDialogService.prototype.showSelectTestNodeDialog = function (forceNodeSelection) {
        var _this = this;
        var $scope = this.$rootScope.$new();
        var selectButton = {
            name: "select-and-test",
            text: this._t("Select & Test"),
            isPrimary: true,
            action: function () { return true; },
            isDisabled: false
        };
        $scope.$watch("nodesState.selection.items", function () {
            if (!$scope.nodesState || !$scope.nodesState.selection) {
                return;
            }
            selectButton.isDisabled = $scope.nodesState.selection.items.length === 0;
        });
        var dialogOptions = {
            title: this._t("Select test node"),
            actionButtonText: this._t("Select & Test"),
            cancelButtonText: this._t("Cancel"),
            buttons: [selectButton],
            viewModel: $scope
        };
        var dialogSettings = {
            template: __webpack_require__(315),
            size: "md",
        };
        if (!forceNodeSelection && this.testJobNode) {
            return this.$q.resolve(this.testJobNode);
        }
        return this.scmModalService.showModal(dialogSettings, dialogOptions)
            .then(function (result) {
            if (result === "cancel") {
                return null;
            }
            var selectedTestJobNodeId = $scope.nodesState.selection.items[0];
            localStorage.setItem(_this.testJobNodeSettingName, JSON.stringify(selectedTestJobNodeId));
            return _this.scmFilteredListService.getSingleNodeData(selectedTestJobNodeId);
        })
            .then(function (node) {
            if (node) {
                _this.testJobNode = node;
            }
            return node;
        });
    };
    SelectTestNodeDialogService.prototype.loadTestJobNodeDataFromStorage = function () {
        var testJobNodeId = JSON.parse(localStorage.getItem(this.testJobNodeSettingName));
        if (!testJobNodeId) {
            return this.$q.resolve(null);
        }
        return this.scmFilteredListService.getSingleNodeData(testJobNodeId);
    };
    return SelectTestNodeDialogService;
}());
exports.SelectTestNodeDialogService = SelectTestNodeDialogService;


/***/ }),
/* 371 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmSuitableNodeList_controller_1 = __webpack_require__(160);
var scmSuitableNodeList_directive_1 = __webpack_require__(372);
__webpack_require__(373);
exports.default = function (module) {
    module.component("scmSuitableNodeList", scmSuitableNodeList_directive_1.SuitableNodeListDirective);
    module.controller("scmSuitableNodeListController", scmSuitableNodeList_controller_1.SuitableNodeListController);
};


/***/ }),
/* 372 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmSuitableNodeList_controller_1 = __webpack_require__(160);
var SuitableNodeListDirective = /** @class */ (function () {
    function SuitableNodeListDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(316);
        this.transclude = false;
        this.controller = scmSuitableNodeList_controller_1.SuitableNodeListController;
        this.controllerAs = "suitableNodeList";
        this.scope = {};
        this.bindToController = {
            nodesState: "=?",
            nodesToPreselect: "<?",
            onLoaded: "&?",
            selectionMode: "@"
        };
    }
    return SuitableNodeListDirective;
}());
exports.SuitableNodeListDirective = SuitableNodeListDirective;


/***/ }),
/* 373 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 374 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmContent_directive_1 = __webpack_require__(375);
var scmContent_controller_1 = __webpack_require__(161);
__webpack_require__(376);
exports.default = function (module) {
    module.component("scmContent", scmContent_directive_1.ContentDirective);
    module.controller("scmContentController", scmContent_controller_1.ContentController);
};


/***/ }),
/* 375 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scmContent_controller_1 = __webpack_require__(161);
var ContentDirective = /** @class */ (function () {
    function ContentDirective() {
        this.template = __webpack_require__(317);
        this.transclude = false;
        this.controller = scmContent_controller_1.ContentController;
        this.controllerAs = "contentCtrl";
        this.scope = {};
        this.bindToController = {
            content: "<",
            contentState: "<"
        };
    }
    return ContentDirective;
}());
exports.ContentDirective = ContentDirective;


/***/ }),
/* 376 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 377 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var gettingStartedWithScm_directive_1 = __webpack_require__(378);
__webpack_require__(380);
exports.default = function (module) {
    module.component("scmGettingStartedWithScm", gettingStartedWithScm_directive_1.GettingStartedWithScmDirective);
};


/***/ }),
/* 378 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var gettingStartedWithScm_controller_1 = __webpack_require__(379);
var GettingStartedWithScmDirective = /** @class */ (function () {
    function GettingStartedWithScmDirective() {
        this.template = __webpack_require__(320);
        this.transclude = false;
        this.controller = gettingStartedWithScm_controller_1.default;
        this.controllerAs = "gettingStartedWithScm";
        this.scope = {};
        this.bindToController = {
            isScmSummaryPage: "<",
        };
    }
    return GettingStartedWithScmDirective;
}());
exports.GettingStartedWithScmDirective = GettingStartedWithScmDirective;


/***/ }),
/* 379 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AssignProfileWizardStepName_1 = __webpack_require__(51);
var constants_1 = __webpack_require__(22);
var GettingStartedWithScmController = /** @class */ (function () {
    /** @ngInject */
    GettingStartedWithScmController.$inject = ["scmLocationService", "xuiDialogService", "getTextService", "scmAuthorizationService", "scmAssignProfileWizardDialogService", "scmUserSettingsService", "$timeout", "swDemoService"];
    function GettingStartedWithScmController(scmLocationService, xuiDialogService, getTextService, scmAuthorizationService, scmAssignProfileWizardDialogService, scmUserSettingsService, $timeout, swDemoService) {
        this.scmLocationService = scmLocationService;
        this.xuiDialogService = xuiDialogService;
        this.getTextService = getTextService;
        this.scmAuthorizationService = scmAuthorizationService;
        this.scmAssignProfileWizardDialogService = scmAssignProfileWizardDialogService;
        this.scmUserSettingsService = scmUserSettingsService;
        this.$timeout = $timeout;
        this.swDemoService = swDemoService;
        this.popoverContent = __webpack_require__(318);
        this._t = getTextService;
    }
    GettingStartedWithScmController.prototype.$onInit = function () {
        this.handleUserAuthorization();
        this.handleGettingStartedSwitch();
    };
    GettingStartedWithScmController.prototype.handleUserAuthorization = function () {
        var _this = this;
        this.scmAuthorizationService.getIsScmUserAuthorized().then(function (isAuthorized) {
            _this.isAuthorized = isAuthorized;
        });
    };
    GettingStartedWithScmController.prototype.handleGettingStartedSwitch = function () {
        var _this = this;
        this.scmUserSettingsService.getUserSettings(constants_1.default.ScmSummaryShowGettingStartedLink).then(function (value) {
            _this.gettingStartedSwitch = (value === null) ? true : value;
            _this.showLink = _this.isScmSummaryPage ? _this.gettingStartedSwitch : true;
            if (_this.showLink) {
                _this.handleGettingStartedPopover();
            }
        });
    };
    GettingStartedWithScmController.prototype.handleGettingStartedPopover = function () {
        var _this = this;
        if (this.isScmSummaryPage) {
            this.scmUserSettingsService.getUserSettings(constants_1.default.ScmHideGettingStartedPopover).then(function (hideGettingStartedPopover) {
                if (hideGettingStartedPopover === null || hideGettingStartedPopover === false) {
                    _this.showPopover = true;
                    _this.$timeout(function () {
                        _this.isPopoverDisplayed = true;
                    });
                }
                else {
                    _this.showPopover = false;
                }
            });
        }
        else {
            this.showPopover = false;
        }
    };
    GettingStartedWithScmController.prototype.openDialog = function () {
        this.xuiDialogService.showModal({
            size: "md",
            template: __webpack_require__(319)
        }, {
            hideTopCancel: false,
            title: this._t("Getting started with SCM"),
            viewModel: { ctrl: this },
            cancelButtonText: this._t("Close"),
        });
    };
    GettingStartedWithScmController.prototype.openWizard = function (item) {
        var settings = {
            selectedNodes: [],
            selectedProfiles: [],
            wizardStep: AssignProfileWizardStepName_1.AssignProfileWizardStepName.Profiles
        };
        this.scmAssignProfileWizardDialogService.showModal(settings);
    };
    GettingStartedWithScmController.prototype.gotItClicked = function () {
        this.scmUserSettingsService.saveUserSettingsIfChanged(constants_1.default.ScmHideGettingStartedPopover, true);
        this.showPopover = false;
        this.isPopoverDisplayed = false;
    };
    GettingStartedWithScmController.prototype.onSwitchChange = function () {
        var _this = this;
        if (this.swDemoService.isDemoMode()) {
            this.gettingStartedSwitch = true;
            this.swDemoService.showDemoErrorToast();
            return;
        }
        this.scmUserSettingsService.saveUserSettingsIfChanged(constants_1.default.ScmSummaryShowGettingStartedLink, this.gettingStartedSwitch);
        if (this.isScmSummaryPage) {
            this.showLink = this.gettingStartedSwitch;
        }
        this.scmUserSettingsService.getUserSettings(constants_1.default.ScmHideGettingStartedPopover).then(function (hideGettingStartedPopover) {
            if (hideGettingStartedPopover === null || hideGettingStartedPopover === false) {
                _this.showPopover = true;
                _this.$timeout(function () {
                    _this.isPopoverDisplayed = true;
                });
            }
        });
    };
    return GettingStartedWithScmController;
}());
exports.default = GettingStartedWithScmController;


/***/ }),
/* 380 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 381 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var addCredentialDialogService_1 = __webpack_require__(382);
var addCredentialDialog_controller_1 = __webpack_require__(383);
exports.default = function (module) {
    module.service("scmAddCredentialDialogService", addCredentialDialogService_1.AddCredentialDialogService);
    module.controller("scmAddCredentialDialogController", addCredentialDialog_controller_1.default);
};


/***/ }),
/* 382 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AddCredentialDialogService = /** @class */ (function () {
    /** @ngInject */
    AddCredentialDialogService.$inject = ["scmModalService", "getTextService", "$controller", "scmCredentialService", "swDemoService", "scmIFrameEventsService"];
    function AddCredentialDialogService(scmModalService, getTextService, $controller, scmCredentialService, swDemoService, scmIFrameEventsService) {
        this.scmModalService = scmModalService;
        this.getTextService = getTextService;
        this.$controller = $controller;
        this.scmCredentialService = scmCredentialService;
        this.swDemoService = swDemoService;
        this.scmIFrameEventsService = scmIFrameEventsService;
        this._t = getTextService;
        this.dialogController = this.createDialogController();
    }
    AddCredentialDialogService.prototype.showAddCredentialModal = function () {
        var _this = this;
        var credentialId;
        var dialogOptions = {
            viewModel: this.dialogController,
            title: this._t("Add new credential"),
            cancelButtonText: this._t("Cancel"),
            buttons: [
                {
                    name: "add",
                    text: this._t("Add"),
                    isPrimary: true,
                    displayStyle: "primary",
                    action: function () {
                        if (_this.swDemoService.isDemoMode()) {
                            _this.swDemoService.showDemoErrorToast();
                            return;
                        }
                        if (_this.dialogController.isCredentialDialogValid()) {
                            return _this.saveCredential().then(function (response) {
                                credentialId = response;
                                return true;
                            }).catch(function () {
                                _this.dialogController.setCredentialDuplicityValidation();
                                return false;
                            });
                        }
                        else {
                            return false;
                        }
                    }
                }
            ]
        };
        var dialogSettings = {
            template: __webpack_require__(321),
            size: "sm",
        };
        this.dialogController.clearFields();
        return this.scmModalService.showModal(dialogSettings, dialogOptions).then(function () { return credentialId; });
    };
    AddCredentialDialogService.prototype.createDialogController = function () {
        var dialogCtrl = this.$controller("scmAddCredentialDialogController");
        dialogCtrl.$onInit();
        return dialogCtrl;
    };
    AddCredentialDialogService.prototype.saveCredential = function () {
        var credential = this.dialogController.getCurrentCredentials();
        return this.scmCredentialService.addCredential(credential);
    };
    return AddCredentialDialogService;
}());
exports.AddCredentialDialogService = AddCredentialDialogService;


/***/ }),
/* 383 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AddCredentialDialogController = /** @class */ (function () {
    function AddCredentialDialogController() {
    }
    AddCredentialDialogController.prototype.$onInit = function () {
        this.clearFields();
    };
    AddCredentialDialogController.prototype.clearFields = function () {
        this.credentialName = "";
        this.userName = "";
        this.password = "";
        this.confirmPassword = "";
    };
    AddCredentialDialogController.prototype.isCredentialDialogValid = function () {
        this.setInvalidFieldsDirty();
        return this.credentialForm.$valid &&
            this.validatePasswordsAreSame() &&
            this.validateFieldsAreNotEmpty();
    };
    AddCredentialDialogController.prototype.validateFieldsAreNotEmpty = function () {
        return this.credentialName !== "" &&
            this.userName !== "" &&
            this.password !== "" &&
            this.confirmPassword !== "";
    };
    AddCredentialDialogController.prototype.validatePasswordsAreSame = function () {
        var pwdAreSame = this.password === this.confirmPassword;
        if (!pwdAreSame) {
            this.credentialForm.ConfirmPassword.$setValidity("passwordMatch", false, this.credentialForm);
        }
        return pwdAreSame;
    };
    AddCredentialDialogController.prototype.setCredentialDuplicityValidation = function () {
        this.credentialForm.CredentialName.$setValidity("duplicateCredentials", false, this.credentialForm);
    };
    AddCredentialDialogController.prototype.setInvalidFieldsDirty = function () {
        if (this.credentialForm.$valid) {
            return;
        }
        _(this.credentialForm.$error).forEach(function (controls) {
            _(controls).forEach(function (control) { return control.$setDirty(); });
        });
    };
    AddCredentialDialogController.prototype.getCurrentCredentials = function () {
        var creds = {
            id: null,
            name: this.credentialName,
            username: this.userName,
            password: this.password,
            owner: "SCM"
        };
        return creds;
    };
    return AddCredentialDialogController;
}());
exports.default = AddCredentialDialogController;


/***/ }),
/* 384 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var escapeHtml_1 = __webpack_require__(279);
exports.default = function (module) {
    module.app().filter("escapeHtml", escapeHtml_1.EscapeHtmlFilter.factory());
};


/***/ }),
/* 385 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(22);
exports.default = function (module) {
    module.service("scmConstants", constants_1.default);
};


/***/ }),
/* 386 */,
/* 387 */,
/* 388 */,
/* 389 */,
/* 390 */,
/* 391 */,
/* 392 */,
/* 393 */,
/* 394 */,
/* 395 */,
/* 396 */,
/* 397 */,
/* 398 */,
/* 399 */,
/* 400 */,
/* 401 */,
/* 402 */,
/* 403 */,
/* 404 */
/***/ (function(module, exports) {

module.exports = "<xui-grid class=scm-nodelist controller=vm hide-toolbar=true id=node-list items-source=vm.items on-pagination-change=\"vm.onPaginationChange(page, pageSize, total)\" on-sorting-change=\"vm.onSortingChange(newValue, oldValue)\" on-search=\"vm.onSearch(item, cancellation)\" on-clear=vm.onClear() options=vm.options pagination-data=vm.pagination row-padding=\"'none'\" sorting-data=vm.sorting stripe=false template-url=scm-node-list-item-template scm-stop-enter-propagation/> ";

/***/ }),
/* 405 */
/***/ (function(module, exports) {

module.exports = "<scm-grid class=scm-nodelist-item-wrapper> <scm-row> <scm-col scm-col-auto> <xui-icon css-class=\"node-status id-status-icon\" class=id-status-icon icon=unknownnode icon-size=small status=\"{{::item.nodeStatus | swStatus}}\"/> </scm-col> <scm-col no-grow> <a ellipsis class=id-node-name ng-href={{::item.detailsUrl}}> {{::item.name}} </a> </scm-col> <scm-profile-indications class=\"id-profile-indications col-flex node-properties\" [node]=item [profile-with-errors]=item.profilesWithErrors [show-last-change]=true [show-profiles-expanded]=false /> </scm-row> </scm-grid> ";

/***/ }),
/* 406 */
/***/ (function(module, exports) {

module.exports = "<a ng-click=ctrl.openWizard() ng-if=ctrl.showAssignToProfileDialogLink class=\"EditResourceButton sw-btn-resource sw-btn\"> <span class=sw-btn-c> <span class=sw-btn-t _t>Assign configuration profiles</span> </span> </a> ";

/***/ }),
/* 407 */,
/* 408 */,
/* 409 */,
/* 410 */,
/* 411 */,
/* 412 */,
/* 413 */,
/* 414 */,
/* 415 */,
/* 416 */,
/* 417 */,
/* 418 */,
/* 419 */,
/* 420 */,
/* 421 */,
/* 422 */,
/* 423 */,
/* 424 */,
/* 425 */,
/* 426 */,
/* 427 */,
/* 428 */,
/* 429 */,
/* 430 */,
/* 431 */,
/* 432 */,
/* 433 */,
/* 434 */,
/* 435 */,
/* 436 */,
/* 437 */,
/* 438 */,
/* 439 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(53);
exports.WidgetSettingsName = "scmNodeListWidgetSettings";
var NodeListController = /** @class */ (function () {
    /** @ngInject */
    NodeListController.$inject = ["$log", "$templateCache", "$timeout", "getTextService", "scmFilteredListService"];
    function NodeListController($log, $templateCache, $timeout, getTextService, scmFilteredListService) {
        this.$log = $log;
        this.$templateCache = $templateCache;
        this.$timeout = $timeout;
        this.getTextService = getTextService;
        this.scmFilteredListService = scmFilteredListService;
        this.items = [];
        this.pagination = { page: 1, pageSize: 10 };
        this._t = getTextService;
    }
    NodeListController.prototype.$onInit = function () {
        this.sorting = {
            sortableColumns: [{
                    id: "Orion.Nodes|name",
                    label: this._t("Name")
                }, {
                    id: "Orion.Nodes|nodeStatus",
                    label: this._t("Node status")
                }, {
                    id: "Orion.SCM.ServerConfiguration|errorCount",
                    label: this._t("Polling issues")
                }, {
                    id: "Orion.SCM.ServerConfiguration|lastChange",
                    label: this._t("Last change")
                }, {
                    id: "Orion.SCM.ServerConfiguration|profileCount",
                    label: this._t("Assigned profiles")
                }],
            sortBy: {
                id: "Orion.SCM.ServerConfiguration|lastChange",
                label: this._t("Last Change")
            },
            direction: "desc"
        };
        this.$templateCache.put("scm-node-list-item-template", __webpack_require__(405));
    };
    NodeListController.prototype.onPaginationChange = function (page, pageSize, total) {
        if (this.pagination.pageSize !== pageSize) {
            this.config.saveUserProperties({ pagesize: pageSize });
        }
        this.$timeout(this.reload.bind(this));
    };
    NodeListController.prototype.onSortingChange = function (newValue, oldValue) {
        if (newValue.sortBy.id !== oldValue.sortBy.id
            || newValue.direction !== oldValue.direction) {
            this.config.saveUserProperties({
                sortby: newValue.sortBy.id,
                sortdirection: newValue.direction
            });
        }
        this.reload();
    };
    NodeListController.prototype.onLoad = function (config) {
        this.config = config;
    };
    NodeListController.prototype.onSettingsChanged = function (settings, afterSave) {
        if (afterSave) {
            return;
        }
        if (settings.userSettings.sortby) {
            this.sorting.sortBy = _(this.sorting.sortableColumns).find(function (item) { return item.id === settings.userSettings.sortby; });
            if (!this.sorting.sortBy) {
                this.$log.error("Monitored Nodes List: not recognized sort column in database settings");
                this.sorting.sortBy = this.sorting.sortableColumns[0];
                this.config.saveUserProperties({ sortby: this.sorting.sortBy.id });
            }
        }
        if (settings.userSettings.sortdirection) {
            this.sorting.direction = settings.userSettings.sortdirection;
        }
        if (settings.userSettings.pagesize) {
            this.pagination.pageSize = settings.userSettings.pagesize;
        }
        this.reload();
    };
    NodeListController.prototype.onSearch = function (item, cancellation) {
        this.searchPhrase = item;
        this.reload();
    };
    NodeListController.prototype.onClear = function () {
        this.searchPhrase = null;
        this.reload();
    };
    NodeListController.prototype.hasBaseline = function (item) {
        return !_.isNil(item.baseline);
    };
    NodeListController.prototype.getMismatchesCount = function (item) {
        if (_.isNil(item.mismatches)) {
            return 0;
        }
        return item.mismatches.addedCount + item.mismatches.changedCount + item.mismatches.deletedCount;
    };
    NodeListController.prototype.reload = function () {
        var _this = this;
        this.config.isBusy(true);
        var dataSourceParameters = this.scmFilteredListService.getNodeListDataSourceParameters(this.sorting, this.pagination, this.searchPhrase);
        this.scmFilteredListService.getFilteredScmNodesForNodesWidget(dataSourceParameters)
            .then(function (result) {
            _this.items = result.items;
            _this.pagination.total = result.totalCount;
        }).catch(function (e) {
            _this.$log.error("Error encountered during parsing monitored nodes list:", e);
            _this.items = [];
        }).finally(function () {
            _this.config.isBusy(false);
        });
    };
    NodeListController = __decorate([
        widgets_1.Widget({
            selector: "scmNodeListWidget",
            template: __webpack_require__(404),
            controllerAs: "vm",
            settingName: exports.WidgetSettingsName,
            partialTemplates: [{ placeholderClass: "sw-widget__header__custom", directiveName: "scmNodeListWidgetHeader" }]
        })
    ], NodeListController);
    return NodeListController;
}());
exports.NodeListController = NodeListController;


/***/ }),
/* 440 */,
/* 441 */,
/* 442 */,
/* 443 */,
/* 444 */,
/* 445 */,
/* 446 */,
/* 447 */,
/* 448 */,
/* 449 */,
/* 450 */,
/* 451 */,
/* 452 */,
/* 453 */,
/* 454 */,
/* 455 */,
/* 456 */,
/* 457 */,
/* 458 */,
/* 459 */,
/* 460 */,
/* 461 */,
/* 462 */,
/* 463 */,
/* 464 */,
/* 465 */,
/* 466 */,
/* 467 */,
/* 468 */,
/* 469 */,
/* 470 */,
/* 471 */,
/* 472 */,
/* 473 */,
/* 474 */,
/* 475 */,
/* 476 */,
/* 477 */,
/* 478 */,
/* 479 */,
/* 480 */,
/* 481 */,
/* 482 */,
/* 483 */,
/* 484 */,
/* 485 */,
/* 486 */,
/* 487 */,
/* 488 */,
/* 489 */,
/* 490 */,
/* 491 */,
/* 492 */,
/* 493 */,
/* 494 */,
/* 495 */,
/* 496 */,
/* 497 */,
/* 498 */,
/* 499 */,
/* 500 */,
/* 501 */,
/* 502 */,
/* 503 */,
/* 504 */,
/* 505 */,
/* 506 */,
/* 507 */,
/* 508 */,
/* 509 */,
/* 510 */,
/* 511 */,
/* 512 */,
/* 513 */,
/* 514 */,
/* 515 */,
/* 516 */,
/* 517 */,
/* 518 */,
/* 519 */,
/* 520 */,
/* 521 */,
/* 522 */,
/* 523 */,
/* 524 */,
/* 525 */,
/* 526 */,
/* 527 */,
/* 528 */,
/* 529 */,
/* 530 */,
/* 531 */,
/* 532 */,
/* 533 */,
/* 534 */,
/* 535 */,
/* 536 */,
/* 537 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(538);


/***/ }),
/* 538 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var components_1 = __webpack_require__(322);
var services_1 = __webpack_require__(272);
var module_1 = __webpack_require__(385);
var filters_1 = __webpack_require__(384);
var nodeList_controller_1 = __webpack_require__(439);
var nodeList_edit_controller_1 = __webpack_require__(539);
__webpack_require__(540);
__webpack_require__(541);
__webpack_require__(55);
var utilsModule_1 = __webpack_require__(235);
utilsModule_1.UtilsModule.register();
components_1.default(Xui.module);
services_1.default(Xui.module);
module_1.default(Xui.module);
filters_1.default(Xui.module);
var init = function (module) {
    module.controller("scmNodeListController", nodeList_controller_1.NodeListController);
    module.controller("scmNodeListSettingsController", nodeList_edit_controller_1.NodeListSettingsController);
};
init(Xui.registerModule("widgets"));
exports.default = init;


/***/ }),
/* 539 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(53);
var nodeList_controller_1 = __webpack_require__(439);
var NodeListSettingsController = /** @class */ (function () {
    /** @ngInject */
    NodeListSettingsController.$inject = ["$log"];
    function NodeListSettingsController($log) {
        this.$log = $log;
    }
    NodeListSettingsController = __decorate([
        widgets_1.WidgetSettings({
            name: nodeList_controller_1.WidgetSettingsName,
            template: "",
            controllerAs: "vm"
        })
    ], NodeListSettingsController);
    return NodeListSettingsController;
}());
exports.NodeListSettingsController = NodeListSettingsController;


/***/ }),
/* 540 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(53);
var NodeListWidgetHeaderController = /** @class */ (function () {
    /** @ngInject */
    NodeListWidgetHeaderController.$inject = ["scmAssignProfileWizardDialogService", "scmAuthorizationService"];
    function NodeListWidgetHeaderController(scmAssignProfileWizardDialogService, scmAuthorizationService) {
        this.scmAssignProfileWizardDialogService = scmAssignProfileWizardDialogService;
        this.scmAuthorizationService = scmAuthorizationService;
    }
    NodeListWidgetHeaderController.prototype.$onInit = function () {
        var _this = this;
        this.scmAuthorizationService.getIsScmUserAuthorized().then(function (isUserAuthorized) {
            if (isUserAuthorized) {
                _this.showAssignToProfileDialogLink = true;
            }
        });
    };
    NodeListWidgetHeaderController.prototype.openWizard = function (item) {
        var _this = this;
        this.scmAssignProfileWizardDialogService.showModal(null)
            .then(function (data) {
            _this.widget.reload();
        });
    };
    NodeListWidgetHeaderController = __decorate([
        widgets_1.WidgetPartialTemplate({
            name: "scmNodeListWidgetHeader",
            template: __webpack_require__(406),
            controllerAs: "ctrl"
        })
    ], NodeListWidgetHeaderController);
    return NodeListWidgetHeaderController;
}());
exports.NodeListWidgetHeaderController = NodeListWidgetHeaderController;


/***/ }),
/* 541 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=nodeList.js.map