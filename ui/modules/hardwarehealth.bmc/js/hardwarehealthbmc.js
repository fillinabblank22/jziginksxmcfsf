/*!
 * @solarwinds/hardwarehealthbmc 2020.2.5-118
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog id=id-manage-as-node-dialog> <div> <table> <tr> <td style=width:40px> <img src=/Orion/Images/Info-Notification.gif alt=\"\"/> </td> <td> <span _t><b>Which interface of {{vm.customOptions.viewModel.entityName}} do you want to monitor with Orion?</b></span> </td> </tr> <tr> <td colspan=2> <br/> <input type=radio ng-model=vm.interface ng-value=vm.Interfaces.management /> <span _t> Management </span><br/> <input type=radio ng-model=vm.interface ng-value=vm.Interfaces.os /> <span _t> Operating System </span><br/><br/> </td> </tr> </table> <table class=sw-suggestion-info style=margin-top:20px;width:100%;background-color:#e6eff3> <tr> <td style=padding:5px> <p _t><b>Management Interface: </b> Use the Management Interface to monitor the server. When you add the node, the IP address automatically populates with information captured from the parent BMC controller during SNMP or ICMP polling. </p> <p _t><b>Operating System Interface: </b> Use this method to monitor the server via its built-in operating system that returns a broader range of data. To add the node, you'll need to provide its IP address because polling occurs in a layer of the OSI model that does not capture IP addresses. </p> </td> </tr> </table> </div> </xui-dialog> ";

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
module.exports = __webpack_require__(3);


/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="./ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(4);
var config_1 = __webpack_require__(5);
var index_1 = __webpack_require__(6);
var index_2 = __webpack_require__(8);
var templates_1 = __webpack_require__(13);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("hardwarehealthbmc.services", []);
angular.module("hardwarehealthbmc.templates", []);
angular.module("hardwarehealthbmc.components", []);
angular.module("hardwarehealthbmc.filters", []);
angular.module("hardwarehealthbmc.providers", []);
angular.module("hardwarehealthbmc", [
    "orion",
    "hardwarehealthbmc.services",
    "hardwarehealthbmc.templates",
    "hardwarehealthbmc.components",
    "hardwarehealthbmc.filters",
    "hardwarehealthbmc.providers"
]);
// create and register Xui (Orion) module wrapper
var servicegroups = Xui.registerModule("hardwarehealthbmc");
exports.default = servicegroups;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: hardwarehealthbmc");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(7);
exports.default = function (module) {
    module.service("hardwarehealthbmcConstants", constants_1.default);
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(9);
var index_2 = __webpack_require__(11);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var manageAsNodeDialog_controller_1 = __webpack_require__(10);
exports.default = function (module) {
    module.controller("manageAsNodeDialogController", manageAsNodeDialog_controller_1.default);
};


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ManageAsNodeDialogController = /** @class */ (function () {
    /** @ngInject */
    ManageAsNodeDialogController.$inject = ["getTextService", "customOptions", "$uibModalStack"];
    function ManageAsNodeDialogController(getTextService, customOptions, $uibModalStack) {
        var _this = this;
        this.getTextService = getTextService;
        this.customOptions = customOptions;
        this.$uibModalStack = $uibModalStack;
        this.Interfaces = {
            management: 1,
            os: 2
        };
        this.interface = this.Interfaces.management;
        this._t = this.getTextService;
        // Properties required to properly run xui-dialog
        this.isValid = true;
        this.isBusy = false;
        this.useCustomBusy = false;
        this.hasStatus = function () { return false; };
        this.getStatusAttributes = function () { return { icon: "", style: "modal-header" }; };
        this.dialogOptions = {
            title: this._t("Manage Blade As Node"),
            cancelButtonText: this._t("Cancel"),
            buttons: [{
                    name: "manage",
                    isPrimary: true,
                    text: this._t("Manage")
                }]
        };
        this.cancel = function () {
            _this.close("cancel");
        };
        this.close = function (dialogResult) {
            _this.$uibModalStack.dismissAll(dialogResult);
        };
        this.execute = function () {
            if (_this.interface === _this.Interfaces.management) {
                window.location.href = "/Orion/Nodes/Add/Default.aspx?IPAddress=" + _this.customOptions.viewModel.ipAddress + "&restart=false";
            }
            if (_this.interface === _this.Interfaces.os) {
                window.location.href = "/Orion/Nodes/Add/Default.aspx?restart=false";
            }
            return true;
        };
    }
    return ManageAsNodeDialogController;
}());
exports.default = ManageAsNodeDialogController;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dialog_controller_1 = __webpack_require__(12);
exports.default = function (module) {
    module.controller("dialogController", dialog_controller_1.default);
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DialogController = /** @class */ (function () {
    /** @ngInject */
    DialogController.$inject = ["xuiDialogService"];
    function DialogController(xuiDialogService) {
        var _this = this;
        this.xuiDialogService = xuiDialogService;
        this.openManageAsNodeDialog = function (ipAddress, entityName) {
            var dialogOptions = {
                viewModel: {
                    entityName: entityName,
                    ipAddress: ipAddress
                }
            };
            var customSettings = {
                template: __webpack_require__(0),
                controller: "manageAsNodeDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                    customOptions: function () { return dialogOptions; }
                }
            };
            _this.xuiDialogService.showModal(customSettings, dialogOptions);
        };
    }
    return DialogController;
}());
exports.default = DialogController;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(14);
    req.keys().forEach(function (r) {
        var key = "hardwarehealth" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/manageAsNodeDialog/manageAsNodeDialog.html": 0,
	"./rowTemplates/bladesWidgetRow.html": 15,
	"./rowTemplates/fansWidgetRow.html": 16,
	"./rowTemplates/hardwareHealthOverviewRow.html": 17,
	"./rowTemplates/psusWidgetRow.html": 18
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 14;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"dialogController as ctrl\" style=display:flex;justify-content:space-between> <div class=pull-left style=display:flex;width:50%> <div ng-if=item.nodeId style=margin-right:5px> <xui-icon icon=\"{{Orion.Nodes | swEntityIcon}}\" is-dynamic=true status={{item.status}}></xui-icon> </div> <div class=text_wrap ng-if=item.nodeId> <a href={{item.url}}>{{item.name}}</a> </div> <div class=text_wrap ng-if=\"!item.nodeId && !(item.ipAddress && ($root.SW.user.AllowNodeManagement || $root.SW.environment.demoMode))\"> {{item.name}} </div> <div class=text_wrap ng-if=\"!item.nodeId && (item.ipAddress && ($root.SW.user.AllowNodeManagement || $root.SW.environment.demoMode))\"> <a ng-if=!$root.SW.environment.demoMode ng-click=\"ctrl.openManageAsNodeDialog(item.ipAddress, item.name)\" name={{item.name}} style=cursor:pointer>{{item.name}}</a> <a ng-if=$root.SW.environment.demoMode onclick='return demoAction(\"HWHBMC_Overview_ManageEntity\")' name={{item.name}} href=\"\">{{item.name}}</a> </div> </div> <div class=pull-right ng-if=\"!(item.responseTime===null || item.responseTime===undefined || item.status === 'External')\" style=width:30%> <div class=\"pull-right text_wrap\" style=width:100%;font-weight:700;text-align:right> {{item.responseTime >= 0 ? item.responseTime : 0}} <span _t>ms</span> </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Current Response Time </div> </div> <div class=pull-right ng-if=\"!(item.percentLoss===null || item.responseTime===undefined  || item.status === 'External')\" style=margin-left:20px;width:20%> <div class=\"pull-right text_wrap\" style=width:100%;font-weight:700;text-align:right> {{item.percentLoss}} <span _t>%</span> </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Percent Loss </div> </div> </div> ";

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = "<div style=display:flex;justify-content:space-between> <div class=pull-left style=margin-top:3px;min-width:32px> <xui-icon icon=fan icon-size=medium text-alignment=right></xui-icon> </div> <div class=pull-left> <div class=text_wrap> {{item.name}} </div> <div class=text_wrap> <span _t>on Chassis Module</span> {{item.module}} </div> </div> <div class=pull-right style=width:100%> <div class=\"pull-right text_wrap\" style=font-weight:700> {{item.status}} </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Status </div> </div> </div>";

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<div class=hho-row style=display:flex> <div class=pull-left style=width:62%;display:flex> <div style=min-width:32px;margin-top:3px> <xui-icon icon={{::item.categoryXuiIconName}} status={{::item.statusIconPostfix}} style=margin:auto></xui-icon> </div> <div style=white-space:nowrap;text-overflow:ellipsis;overflow:hidden;display:block> <a href={{::item.hwhDetailsUrl}}> {{::item.sensorDisplayName}} </a> <div class=category-name style=font-size:11px;color:#a5abb6> {{::item.categoryName}} </div> </div> </div> <div class=pull-right style=width:20%;display:flex;margin-top:auto;margin-bottom:auto;justify-content:flex-end> <xui-icon icon={{::item.sourceStatusXuiIconName}} icon-size=small ng-if=\"item.sourceType == 1\" style=margin-right:4px;margin-top:1px></xui-icon> <a ng-if=item.sourceDetailsUrl style=white-space:nowrap;text-overflow:ellipsis;overflow:hidden;display:block href={{::item.sourceDetailsUrl}}><span class=source-name>{{::item.sourceName}}</span></a> <span class=source-name ng-if=!item.sourceDetailsUrl style=white-space:nowrap;text-overflow:ellipsis;overflow:hidden;display:block>{{::item.sourceName}}</span> </div> <div class=pull-right style=width:18%;position:relative> <div class=\"pull-right text_wrap\" style=text-align:right;line-height:40px;min-height:40px;padding-right:3px;width:100%;position:absolute;left:10px;top:-4px ng-if=\"item.sensorValue != null\" ng-style=\"item.isValueCritical && {'background-color':'#FDCCC8','font-weight':'bold'} || item.isValueWarning && {'background-color':'#FDE8BF','font-weight':'bold'}\"> <span class=sensor-display-value ng-bind-html=item.sensorDisplayValue></span> </div> </div> </div> ";

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "<div style=display:flex;justify-content:space-between> <div class=\"display: flex; justify-content: flex-start height:100%;\"> <div class=pull-left style=margin-top:3px;min-width:32px> <xui-icon icon=psu icon-size=medium text-alignment=right></xui-icon> </div> <div class=pull-left> <div class=text_wrap> {{item.name}} </div> </div> </div> <div class=pull-right> <div style=display:flex;justify-content:space-between> <div class=pull-right style=width:100%> <div class=\"pull-right text_wrap\" style=font-weight:700> {{item.power}} </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Power </div> </div> <div class=pull-right style=width:20px> </div> <div class=pull-right style=width:100%> <div class=\"pull-right text_wrap\" style=font-weight:700> {{item.status}} </div> <br/> <div class=\"pull-right text_wrap\" style=width:100%;text-align:right _t> Status </div> </div> </div> </div> </div>";

/***/ })
/******/ ]);
//# sourceMappingURL=hardwarehealthbmc.js.map