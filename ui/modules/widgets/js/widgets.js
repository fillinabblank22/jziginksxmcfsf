/*!
 * @solarwinds/widgets 1.13.0-5093
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("ChartsD3"));
	else if(typeof define === 'function' && define.amd)
		define(["ChartsD3"], factory);
	else if(typeof exports === 'object')
		exports["Widgets"] = factory(require("ChartsD3"));
	else
		root["Widgets"] = factory(root["ChartsD3"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_6__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 92);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetConfig = /** @class */ (function () {
    function WidgetConfig(container, widget) {
        var _this = this;
        this.container = container;
        this.widget = widget;
        this.saveSettings = function () { return _this.container.saveSettings(_this.settings); };
        this.title = function (newValue) {
            if (angular.isDefined(newValue)) {
                _this.container.setTitle(newValue);
            }
            return _this.container.title;
        };
        this.subtitle = function (newValue) {
            if (angular.isDefined(newValue)) {
                _this.container.setSubtitle(newValue);
            }
            return _this.container.subtitle;
        };
        this.isBusy = function (newValue, message) {
            if (angular.isDefined(newValue)) {
                _this.widget.isBusy = newValue;
            }
            _this.widget.busyMessage = message;
            return _this.widget.isBusy;
        };
        this.setOverlayType = function (type) {
            _this.widget.overlayType = type;
        };
        this.saveUserProperties = function (settings) {
            return _this.container.saveUserProperties(settings);
        };
        var id = container.id, settings = container.settings, title = container.title, subtitle = container.subtitle;
        this.id = id;
        this.settings = _.assign(settings, { title: title, subtitle: subtitle });
    }
    return WidgetConfig;
}());
exports.WidgetConfig = WidgetConfig;
/**
 * @ngdoc service
 * @name widgets.service.WidgetDecorator
 *
 * @description
 * @param {IWidgetDecoratorInputs} inputs Configuration of widget. Currently available options are:
 * @param {string} inputs.selector The unique directive name of this widget.
 * @param {string} inputs.controllerAs How the controller will be referenced from the template
 * @param {string=} inputs.template The actual template for the widget.
 * @param {string=} inputs.templateUrl A reference to the template for the widget
 * @param {string} inputs.settingName Unique name for the widget settings object
 *
 * @example
 *    <example module="widgets">
  *        <file src="src/decorators/docs/widget-decorator-examples.html" name="app.html"></file>
 *    </example>
 */
function Widget(inputs) {
    return function (t) {
        // The only required config is a selector. If one wasn't passed, throw immediately
        if (!inputs.selector) {
            throw new Error("Widget Decorator Error in \"" + t.name + "\": Widget selector must be provided");
        }
        var templateFn = function () {
            var ngIncludeMarkup = "ng-include=\"'" + inputs.templateUrl + "'\"";
            return "\n<div\n    xui-watch-counter\n    xui-busy=\"isBusy\"\n    xui-busy-message=\"{{ busyMessage }}\">\n    <div ng-switch=\"overlayType\">\n        <div ng-switch-when=\"dataNotAvailable\">\n            <xui-empty empty-data=\"{preset: 'dataNotAvailable'}\"></xui-empty>\n        </div>\n        <div ng-switch-when=\"serverUnreachable\">\n            <xui-empty empty-data=\"{preset: 'serverUnreachable'}\"></xui-empty>\n        </div>\n        <div ng-switch-when=\"dataNotApplicable\">\n            <xui-empty empty-data=\"{preset: 'dataNotApplicable'}\"></xui-empty>\n        </div>\n        <div ng-switch-when=\"emptyDataset\">\n            <xui-empty empty-data=\"{preset: 'emptyDataset'}\"></xui-empty>\n        </div>\n        <div ng-switch-default>\n            <div ng-if=\"" + !inputs.template + "\" " + (inputs.templateUrl ? ngIncludeMarkup : null) + "></div>\n            <div ng-if=\"" + !!inputs.template + "\">\n                " + inputs.template + "\n            </div>\n        </div>\n    </div>\n</div>";
        };
        var DecoratedWidget = /** @class */ (function () {
            function DecoratedWidget() {
                this.require = [inputs.selector, "^swWidgetContainer"];
                this.restrict = "E";
                this.template = templateFn;
                this.controller = t;
                this.controllerAs = inputs.controllerAs;
                this.scope = {};
                this.link = function (scope, element, attrs, _a) {
                    var widget = _a[0], container = _a[1];
                    container.settingName = inputs.settingName;
                    var config = new WidgetConfig(container, scope);
                    var showWidget = function () {
                        widget.onLoad(config);
                        container.showWidget();
                        container.widgetLoaded(widget);
                        notifyWhenWidgetIsNotBusy();
                    };
                    container.widget = widget;
                    if (inputs.partialTemplates) {
                        container.partialTemplates = inputs.partialTemplates;
                    }
                    scope.$on("$destroy", function () {
                        widget.onUnload();
                    });
                    container.hideWidget();
                    container.isVisibleDeferred.promise.then(function (isVisible) {
                        if (isVisible) {
                            showWidget();
                        }
                    });
                    container.scriptsLoaded.promise.then(function (isLoaded) {
                        if (isLoaded) {
                            config.scriptsLoaded = true;
                        }
                    });
                    function notifyWhenWidgetIsNotBusy() {
                        var unregisterWatch = scope.$watch(function () { return config.isBusy(); }, function (isBusy) {
                            if (!isBusy) {
                                fireLoadingCompletedNotification(unregisterWatch);
                            }
                        });
                        if (!config.isBusy()) {
                            fireLoadingCompletedNotification(unregisterWatch);
                        }
                    }
                    function fireLoadingCompletedNotification(unregisterWatchFunction) {
                        unregisterWatchFunction();
                        SW.Core.VisibilityObserver.loadingCompleted(config.id);
                    }
                };
            }
            return DecoratedWidget;
        }());
        Xui.registerModule("widgets").component(inputs.selector, DecoratedWidget);
    };
}
exports.Widget = Widget;
exports.default = WidgetConfig;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


//     This code was generated by a Reinforced.Typings tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
Object.defineProperty(exports, "__esModule", { value: true });
var AuthenticationAlgorithm;
(function (AuthenticationAlgorithm) {
    AuthenticationAlgorithm[AuthenticationAlgorithm["Unknown"] = 0] = "Unknown";
    AuthenticationAlgorithm[AuthenticationAlgorithm["None"] = 1] = "None";
    AuthenticationAlgorithm[AuthenticationAlgorithm["Other"] = 2] = "Other";
    AuthenticationAlgorithm[AuthenticationAlgorithm["HmacMd5"] = 3] = "HmacMd5";
    AuthenticationAlgorithm[AuthenticationAlgorithm["HmacSHa"] = 4] = "HmacSHa";
})(AuthenticationAlgorithm = exports.AuthenticationAlgorithm || (exports.AuthenticationAlgorithm = {}));
var EncryptionAlgorithm;
(function (EncryptionAlgorithm) {
    EncryptionAlgorithm[EncryptionAlgorithm["None"] = 1] = "None";
    EncryptionAlgorithm[EncryptionAlgorithm["Des"] = 2] = "Des";
    EncryptionAlgorithm[EncryptionAlgorithm["Des3"] = 3] = "Des3";
    EncryptionAlgorithm[EncryptionAlgorithm["Rc4"] = 4] = "Rc4";
    EncryptionAlgorithm[EncryptionAlgorithm["Rc5"] = 5] = "Rc5";
    EncryptionAlgorithm[EncryptionAlgorithm["Idea"] = 6] = "Idea";
    EncryptionAlgorithm[EncryptionAlgorithm["Cast"] = 7] = "Cast";
    EncryptionAlgorithm[EncryptionAlgorithm["Blowfish"] = 8] = "Blowfish";
    EncryptionAlgorithm[EncryptionAlgorithm["Aes"] = 9] = "Aes";
})(EncryptionAlgorithm = exports.EncryptionAlgorithm || (exports.EncryptionAlgorithm = {}));
var RasProtocol;
(function (RasProtocol) {
    RasProtocol[RasProtocol["Other"] = 1] = "Other";
    RasProtocol[RasProtocol["IPSec"] = 2] = "IPSec";
    RasProtocol[RasProtocol["I2tp"] = 3] = "I2tp";
    RasProtocol[RasProtocol["I2tpOverIpSec"] = 4] = "I2tpOverIpSec";
    RasProtocol[RasProtocol["Pptp"] = 5] = "Pptp";
    RasProtocol[RasProtocol["I2F"] = 6] = "I2F";
    RasProtocol[RasProtocol["Ssl"] = 7] = "Ssl";
})(RasProtocol = exports.RasProtocol || (exports.RasProtocol = {}));
var TunnelState;
(function (TunnelState) {
    TunnelState[TunnelState["Unknown"] = 0] = "Unknown";
    TunnelState[TunnelState["Connected"] = 1] = "Connected";
    TunnelState[TunnelState["Disconnected"] = 2] = "Disconnected";
    TunnelState[TunnelState["Failed"] = 3] = "Failed";
    TunnelState[TunnelState["Dropped"] = 4] = "Dropped";
})(TunnelState = exports.TunnelState || (exports.TunnelState = {}));
var RemoteAccessTunnelType;
(function (RemoteAccessTunnelType) {
    RemoteAccessTunnelType[RemoteAccessTunnelType["Unknown"] = 0] = "Unknown";
    RemoteAccessTunnelType[RemoteAccessTunnelType["Clientless"] = 1] = "Clientless";
    RemoteAccessTunnelType[RemoteAccessTunnelType["IpSec"] = 2] = "IpSec";
    RemoteAccessTunnelType[RemoteAccessTunnelType["Ssl"] = 3] = "Ssl";
})(RemoteAccessTunnelType = exports.RemoteAccessTunnelType || (exports.RemoteAccessTunnelType = {}));
var HighAvailabilityState;
(function (HighAvailabilityState) {
    HighAvailabilityState[HighAvailabilityState["Unknown"] = 0] = "Unknown";
    HighAvailabilityState[HighAvailabilityState["Up"] = 1] = "Up";
    HighAvailabilityState[HighAvailabilityState["Down"] = 2] = "Down";
})(HighAvailabilityState = exports.HighAvailabilityState || (exports.HighAvailabilityState = {}));
var HighAvailabilityType;
(function (HighAvailabilityType) {
    HighAvailabilityType[HighAvailabilityType["Unknown"] = 0] = "Unknown";
    HighAvailabilityType[HighAvailabilityType["None"] = 1] = "None";
    HighAvailabilityType[HighAvailabilityType["ActiveActive"] = 2] = "ActiveActive";
    HighAvailabilityType[HighAvailabilityType["ActiveStandby"] = 3] = "ActiveStandby";
})(HighAvailabilityType = exports.HighAvailabilityType || (exports.HighAvailabilityType = {}));
var HighAvailabilityMode;
(function (HighAvailabilityMode) {
    HighAvailabilityMode[HighAvailabilityMode["Unknown"] = 0] = "Unknown";
    HighAvailabilityMode[HighAvailabilityMode["NotConfigured"] = 1] = "NotConfigured";
    HighAvailabilityMode[HighAvailabilityMode["Active"] = 2] = "Active";
    HighAvailabilityMode[HighAvailabilityMode["Standby"] = 3] = "Standby";
})(HighAvailabilityMode = exports.HighAvailabilityMode || (exports.HighAvailabilityMode = {}));
var HighAvailabilityRole;
(function (HighAvailabilityRole) {
    HighAvailabilityRole[HighAvailabilityRole["Unknown"] = 0] = "Unknown";
    HighAvailabilityRole[HighAvailabilityRole["Primary"] = 1] = "Primary";
    HighAvailabilityRole[HighAvailabilityRole["Secondary"] = 2] = "Secondary";
})(HighAvailabilityRole = exports.HighAvailabilityRole || (exports.HighAvailabilityRole = {}));
var NodeStatus;
(function (NodeStatus) {
    NodeStatus[NodeStatus["Unknown"] = 0] = "Unknown";
    NodeStatus[NodeStatus["Up"] = 1] = "Up";
    NodeStatus[NodeStatus["Down"] = 2] = "Down";
    NodeStatus[NodeStatus["Warning"] = 3] = "Warning";
    NodeStatus[NodeStatus["Shutdown"] = 4] = "Shutdown";
    NodeStatus[NodeStatus["Testing"] = 5] = "Testing";
    NodeStatus[NodeStatus["Dormant"] = 6] = "Dormant";
    NodeStatus[NodeStatus["Notpresent"] = 7] = "Notpresent";
    NodeStatus[NodeStatus["Lowerlayerdown"] = 8] = "Lowerlayerdown";
    NodeStatus[NodeStatus["Unmanaged"] = 9] = "Unmanaged";
    NodeStatus[NodeStatus["Unplugged"] = 10] = "Unplugged";
    NodeStatus[NodeStatus["External"] = 11] = "External";
    NodeStatus[NodeStatus["Unreachable"] = 12] = "Unreachable";
    NodeStatus[NodeStatus["Critical"] = 14] = "Critical";
    NodeStatus[NodeStatus["PartlyAvailable"] = 15] = "PartlyAvailable";
    NodeStatus[NodeStatus["Misconfigured"] = 16] = "Misconfigured";
    NodeStatus[NodeStatus["Undefined"] = 17] = "Undefined";
    NodeStatus[NodeStatus["Unconfirmed"] = 19] = "Unconfirmed";
    NodeStatus[NodeStatus["Active"] = 22] = "Active";
    NodeStatus[NodeStatus["Inactive"] = 24] = "Inactive";
    NodeStatus[NodeStatus["Expired"] = 25] = "Expired";
    NodeStatus[NodeStatus["MonitoringDisabled"] = 26] = "MonitoringDisabled";
    NodeStatus[NodeStatus["Disabled"] = 27] = "Disabled";
    NodeStatus[NodeStatus["NotLicensed"] = 28] = "NotLicensed";
})(NodeStatus = exports.NodeStatus || (exports.NodeStatus = {}));


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @ngdoc service
 * @name widgets.service.WidgetSettingsDecorator
 *
 * @description
 * @param {IWidgetSettingsDecoratorInputs} inputs Configuration of widget settings. Currently available options are:
 * @param {string} inputs.name The name of the settings- this should match the value in the widget.
 * @param {string} inputs.controllerAs How the controller will be referenced from the template.
 * @param {string=} inputs.template The actual template for the widget settings.
 * @param {string=} inputs.templateUrl A reference to the template for the widget settings.
 *
 * @example
 *    <example module="widgets">
 *        <file src="src/decorators/docs/widgetSettings-decorator-examples.html" name="app.html"></file>
 *    </example>
 */
function WidgetSettings(inputs) {
    return function (t) {
        // The only required config is a name. If one wasn't passed, throw immediately
        if (!inputs.name) {
            throw new Error("WidgetSettings Decorator Error in \"" + inputs.name + "\": WidgetSettings name must be provided");
        }
        var WidgetSettings = /** @class */ (function () {
            function WidgetSettings() {
                this.restrict = "E";
                this.transclude = false;
                this.template = inputs.template;
                this.templateUrl = inputs.templateUrl;
                this.controller = t;
                this.controllerAs = inputs.controllerAs;
            }
            WidgetSettings.prototype.link = function (scope, element, attrs, controller) {
                // Pull in the viewModel from the WidgetContainer.onEdit() dialog.
                var viewModel = scope.$parent.$parent.vm.dialogOptions.viewModel;
                if (controller.onInit) {
                    controller.onInit(viewModel);
                }
                else {
                    // deprecated
                    controller.settings = viewModel.settings;
                }
                // Set the viewModel commands to the respective controller commands.
                viewModel.onSave = controller.onSave;
                viewModel.onCancel = controller.onCancel;
            };
            return WidgetSettings;
        }());
        Xui.registerModule("widgets").component(inputs.name, WidgetSettings);
    };
}
exports.WidgetSettings = WidgetSettings;


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(a, b) {
  return a < b ? -1 : a > b ? 1 : a >= b ? 0 : NaN;
});


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
function Inject(dependency) {
    return function (prototype, method, argumentPosition) {
        prototype.$inject = prototype.$inject || [];
        prototype.$inject[argumentPosition] = dependency;
    };
}
exports.Inject = Inject;
exports.default = Inject;


/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(x) {
  return x === null ? NaN : +x;
});


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function WidgetPartialTemplate(inputs) {
    return function (t) {
        if (!inputs.name) {
            throw new Error("WidgetPartialTemplate decorator error, parameter 'name' must be provided");
        }
        var WidgetPartialTemplate = /** @class */ (function () {
            function WidgetPartialTemplate() {
                this.require = [inputs.name, "widget"];
                this.restrict = "E";
                this.template = inputs.template;
                this.templateUrl = inputs.templateUrl;
                this.controller = t;
                this.controllerAs = inputs.controllerAs;
                this.scope = {};
                this.link = function (scope, element, attrs, controllers) {
                    var header = controllers[0];
                    header.widget = controllers[1];
                };
            }
            return WidgetPartialTemplate;
        }());
        Xui.registerModule("widgets").component(inputs.name, WidgetPartialTemplate);
    };
}
exports.WidgetPartialTemplate = WidgetPartialTemplate;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilteringConverter = /** @class */ (function () {
    function FilteringConverter() {
    }
    FilteringConverter.prototype.getExcludedFiltersFromStateReset = function () { return []; };
    ;
    FilteringConverter.prototype.shouldResetState = function (changedFilters) {
        return _.difference(changedFilters, this.getExcludedFiltersFromStateReset()).length > 0;
    };
    FilteringConverter.prototype.resetState = function (state) {
        return state;
    };
    return FilteringConverter;
}());
exports.FilteringConverter = FilteringConverter;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    Constants.visibilityDefaultUrl = "widgets/visibility/core/configuration/";
    Constants.plotSeriesDefaultLimit = 10;
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var charts_d3_1 = __webpack_require__(6);
var inject_1 = __webpack_require__(4);
var PerfstackSeriesType;
(function (PerfstackSeriesType) {
    PerfstackSeriesType[PerfstackSeriesType["undefined"] = 0] = "undefined";
    PerfstackSeriesType[PerfstackSeriesType["gauge"] = 1] = "gauge";
    PerfstackSeriesType[PerfstackSeriesType["counter"] = 2] = "counter";
    PerfstackSeriesType[PerfstackSeriesType["event"] = 3] = "event";
    PerfstackSeriesType[PerfstackSeriesType["alert"] = 4] = "alert";
    PerfstackSeriesType[PerfstackSeriesType["state"] = 5] = "state";
    PerfstackSeriesType[PerfstackSeriesType["multi"] = 6] = "multi";
    PerfstackSeriesType[PerfstackSeriesType["dpaWaitTime"] = 7] = "dpaWaitTime";
})(PerfstackSeriesType = exports.PerfstackSeriesType || (exports.PerfstackSeriesType = {}));
var Orion2Charts = [
    charts_d3_1.SeriesType.undefined,
    charts_d3_1.SeriesType.gauge,
    charts_d3_1.SeriesType.counter,
    charts_d3_1.SeriesType.event,
    charts_d3_1.SeriesType.alert,
    charts_d3_1.SeriesType.state,
    charts_d3_1.SeriesType.multi,
];
var Charts2Perfstack = [
    PerfstackSeriesType.undefined,
    PerfstackSeriesType.gauge,
    PerfstackSeriesType.counter,
    PerfstackSeriesType.event,
    PerfstackSeriesType.alert,
    PerfstackSeriesType.state,
    PerfstackSeriesType.multi,
];
var ChartMetricsService = /** @class */ (function () {
    /** @ngInject */
    ChartMetricsService.$inject = ["_t", "timeService", "$q", "$log", "xuiUnitConversionService", "xuiUnitConversionConstants", "metricService"];
    function ChartMetricsService(_t, timeService, $q, $log, xuiUnitConversionService, xuiUnitConversionConstants, metricService) {
        var _this = this;
        this._t = _t;
        this.timeService = timeService;
        this.$q = $q;
        this.$log = $log;
        this.xuiUnitConversionService = xuiUnitConversionService;
        this.xuiUnitConversionConstants = xuiUnitConversionConstants;
        this.metricService = metricService;
        this.defaultTimeFrame = function () {
            var now = moment();
            return _this.timeService.create().start(moment(now).subtract(1, "h")).end(now);
        };
        this.customTimeFrame = function (start, end) {
            return _this.timeService.current().start(start).end(end);
        };
        this.convertTimeFrame = function (timeframe) {
            return timeframe && _this.customTimeFrame(moment(timeframe.startDatetime), moment(timeframe.endDatetime));
        };
        this.updateTimeFrame = function (timeframe) {
            return _this.timeService.current(_this.convertTimeFrame(timeframe)).current();
        };
        // create model and delay render callback
        this.createModel = function (data, initialTimeframe, renderCallback) {
            var model = _.extend({
                id: null,
                series: null
            }, data);
            if (!model.id) {
                model.id = _.uniqueId();
            }
            if (renderCallback) {
                var defer_1 = _this.$q.defer();
                model.onRendered = function () { return defer_1.resolve(); };
                var promises = [defer_1.promise];
                if (initialTimeframe) {
                    promises.push(initialTimeframe.promise);
                }
                _this.$q.all(promises).then(function () { return renderCallback(model); });
            }
            return model;
        };
        // metric to time series helpers
        this.mapPerfstackStates = function (state) {
            switch (state) {
                case 0:
                    return charts_d3_1.ContinuousEventType.unknown;
                case 1:
                    return charts_d3_1.ContinuousEventType.ok;
                case 2:
                    return charts_d3_1.ContinuousEventType.down;
                case 3:
                    return charts_d3_1.ContinuousEventType.warning;
                case 4:
                    return charts_d3_1.ContinuousEventType.shutdown;
                case 9:
                    return charts_d3_1.ContinuousEventType.unmanaged;
                case 10:
                    return charts_d3_1.ContinuousEventType.unplugged;
                case 11:
                    return charts_d3_1.ContinuousEventType.external;
                case 12:
                    return charts_d3_1.ContinuousEventType.unrecheable;
                case 14:
                    return charts_d3_1.ContinuousEventType.error;
                case 27:
                    return charts_d3_1.ContinuousEventType.disabled;
                default:
                    return charts_d3_1.ContinuousEventType.unknown;
            }
        };
        this.getUnitType = function (unit) {
            switch (unit) {
                case "%":
                    return charts_d3_1.DomainType.percent;
                case "ms":
                    return charts_d3_1.DomainType.time;
                case "B":
                    return charts_d3_1.DomainType.byte;
                case "bps":
                    return charts_d3_1.DomainType.bitsPerSecond;
                default:
                    return charts_d3_1.DomainType.number;
            }
        };
        this.guessMetric = function (unitType) {
            switch (unitType) {
                case charts_d3_1.DomainType.byte:
                    return _this._t("Memory");
                case charts_d3_1.DomainType.bitsPerSecond:
                    return _this._t("Bandwidth");
                case charts_d3_1.DomainType.percent:
                    return _this._t("Utilization");
                default:
                    return "";
            }
        };
        this.mapOrionThresholds = function (levels) {
            if (!levels) {
                return;
            }
            return _.map(levels, function (l) { return ({
                start: l.start,
                end: l.end,
                type: (l.type === 2) ? 2 : ((l.type === 1) ? 1 : 0)
            }); });
        };
        this.metricDataToTimeSeriesData = function (data, metricType) {
            if (metricType === PerfstackSeriesType.state) {
                return _.map(data, function (d) { return ({
                    data: moment(d.dateTimeStamp).toDate(),
                    value: _this.mapPerfstackStates(d.value)
                }); });
            }
            return _.map(data, function (d) { return ({
                data: moment(d.dateTimeStamp).toDate(),
                value: d.value
            }); });
        };
        /**
         * converts provided metric to simple series
         * @param {IMetricDto} metric metric
         * @returns {ISimpleSeries} chart compatible series
         */
        this.metricDtoToMetricSeries = function (metric) {
            var unitType = _this.getUnitType(metric.units);
            var result = {
                id: metric.id,
                type: _this.orionToCharts(metric.type),
                displayName: metric.displayName,
                ancestorDisplayNames: metric.ancestorDisplayNames,
                sourceDisplayName: metric.entityDisplayName,
                isPercentile: (metric.units === "%"),
                unitType: unitType,
                unitLabel: metric.units,
                opacity: 1,
                visible: true,
                data: _this.metricDataToTimeSeriesData(metric.measurements, _this.orionToPerfstack(metric.type)),
                subSeries: [],
                xAxisLabel: "",
                yAxisLabel: _this.guessMetric(unitType),
                thresholdLevels: _this.mapOrionThresholds(metric.statistics && metric.statistics.thresholdLevels),
                statistics: {
                    percentile: metric.statistics && metric.statistics.percentile95
                }
            };
            return result;
        };
        // range series helpers
        this.metricsToRange = function (avgMetricData, minMetricData, maxMetricData) {
            return _.map(avgMetricData, function (d, index) { return ({
                data: moment(d.dateTimeStamp).toDate(),
                value: {
                    y: d.value,
                    yMin: minMetricData[index].value,
                    yMax: maxMetricData[index].value
                }
            }); });
        };
        /**
         * Converts 3 provided perfstack metrics into charts-d3 areaRange/columnRange-compatible series
         * @param {IMetricDto[]} metrics Array of 3 metrics with id's that represent Min, Max and Avg aggregation methods
         * @returns {ISimpleSeries} areaRange/columnRange-compatible series
         */
        this.metricDtoToRangeSeries = function (metrics) {
            if (metrics.length !== 3) {
                _this.$log.warn("To create Area Range or Bar Range series exactly 3 metrics are needed");
                return;
            }
            var avgMetric = _.find(metrics, function (m) { return _.startsWith(m.metricId, "Avg"); });
            var maxMetric = _.find(metrics, function (m) { return _.startsWith(m.metricId, "Max"); });
            var minMetric = _.find(metrics, function (m) { return _.startsWith(m.metricId, "Min"); });
            if (!(avgMetric && maxMetric && minMetric)) {
                _this.$log.warn("To create Area Range or Bar Range series all Avg, Max and Min metrics are needed");
                return;
            }
            // Since avg metric is kind of main one
            var dataLength = avgMetric.measurements.length;
            if (maxMetric.measurements.length !== dataLength || minMetric.measurements.length !== dataLength) {
                var info = "avg: " + avgMetric.measurements.length
                    + (", min: " + minMetric.measurements.length)
                    + (", max: " + maxMetric.measurements.length);
                _this.$log.warn("Number of measurements should be the same for all metrics (" + info + ")");
            }
            var unitType = _this.getUnitType(avgMetric.units);
            var result = {
                id: avgMetric.id,
                type: _this.orionToCharts(avgMetric.type),
                displayName: avgMetric.displayName,
                ancestorDisplayNames: avgMetric.ancestorDisplayNames,
                sourceDisplayName: avgMetric.entityDisplayName,
                isPercentile: (avgMetric.units === "%"),
                unitType: unitType,
                unitLabel: avgMetric.units,
                opacity: 1,
                visible: true,
                data: _this.metricsToRange(avgMetric.measurements, minMetric.measurements, maxMetric.measurements),
                subSeries: [],
                // (for instance cpu load may have sub series per CPU but we do not show them)
                xAxisLabel: "",
                yAxisLabel: _this.guessMetric(unitType),
                thresholdLevels: _this.mapOrionThresholds(avgMetric.statistics && avgMetric.statistics.thresholdLevels),
                statistics: {
                    percentile: avgMetric.statistics && avgMetric.statistics.percentile95
                }
            };
            return result;
        };
        this.orionToCharts = function (type) {
            return Orion2Charts[type] || charts_d3_1.SeriesType.undefined;
        };
        this.chartsToPerfstack = function (type) {
            return Charts2Perfstack[type] || PerfstackSeriesType.undefined;
        };
        this.orionToPerfstack = function (type) {
            return _this.chartsToPerfstack(_this.orionToCharts(type));
        };
        // *** normalize metrics
        this.absoluteMax = function (data, getter) {
            return _.reduce(data, function (max, item) { return Math.max(max, Math.abs(getter(item))); }, 0);
        };
        this.findMaxByteMetric = function (metrics, plotType) {
            var getter = (plotType === charts_d3_1.PlotType.range)
                ? function (item) { return item.value.yMax; }
                : function (item) { return item.value; };
            return _this.absoluteMax(metrics, function (metric) {
                if (metric.statistics && metric.statistics.max) {
                    return metric.statistics.max;
                }
                return _this.absoluteMax(metric.data, getter);
            });
        };
        /**
         * normalize metric data
         */
        this.normalizeMetrics = function (metrics, plotType) {
            var metricSets = [
                {
                    unitType: charts_d3_1.DomainType.byte,
                    units: _this.xuiUnitConversionConstants.bytes,
                    base: 1024
                },
                {
                    unitType: charts_d3_1.DomainType.bitsPerSecond,
                    units: _this.xuiUnitConversionConstants.bitsPerSecond,
                    base: 1000
                }
            ];
            _.forEach(metricSets, function (_a) {
                var unitType = _a.unitType, units = _a.units, base = _a.base;
                var filteredMetrics = _.filter(metrics, function (m) { return m.unitType === unitType; });
                var order = _this.xuiUnitConversionService.convert(_this.findMaxByteMetric(filteredMetrics, plotType), base).order;
                var factor = Math.pow(base, order);
                _.forEach(filteredMetrics, function (m) {
                    m.unitLabel = units[order];
                    _.forEach(m.data, function (seriesData) {
                        if (plotType === charts_d3_1.PlotType.range) {
                            seriesData.value.y = seriesData.value.y / factor;
                            seriesData.value.yMin = seriesData.value.yMin / factor;
                            seriesData.value.yMax = seriesData.value.yMax / factor;
                        }
                        else {
                            seriesData.value = seriesData.value / factor;
                        }
                    });
                    m.statistics.percentile /= factor;
                });
            });
            return metrics;
        };
        this.getMetricsByPlotType = function (perfstackMetrics, chartModelToUpdate, childrenIds, config) {
            if (chartModelToUpdate.plotType === charts_d3_1.PlotType.range) {
                return [_this.metricDtoToRangeSeries(perfstackMetrics)];
            }
            if (chartModelToUpdate.plotType === charts_d3_1.PlotType.proportional
                && perfstackMetrics.length === 1 && !_.isEmpty(perfstackMetrics[0].subMetrics)) {
                return _this.getSubMetrics(perfstackMetrics);
            }
            if ((chartModelToUpdate.plotType === charts_d3_1.PlotType.axis) &&
                (_this.orionToCharts(perfstackMetrics[0].type) === charts_d3_1.SeriesType.multi)) {
                return _this.getSubMetrics(perfstackMetrics);
            }
            // Group by is supported returning sub metrics if only one metric returned
            if (chartModelToUpdate.plotType === charts_d3_1.PlotType.axis && perfstackMetrics.length === 1 && !_.isEmpty(perfstackMetrics[0].subMetrics)) {
                return _this.subMetricToMetricSeries(perfstackMetrics[0]);
            }
            return _.map(perfstackMetrics, function (metric) {
                var series = _this.metricDtoToMetricSeries(metric);
                if (_.includes(childrenIds, series.id)) {
                    series.legendLinkURL = metric.entityUrl;
                    _this.formatDisplayName(series, config);
                }
                return series;
            });
        };
    }
    // retrieve metrics
    ChartMetricsService.prototype.getMetrics = function (metricQueries, timeframe, resolution, limitationIds) {
        var _this = this;
        if (resolution === void 0) { resolution = 1; }
        var options = {
            timeFrame: timeframe,
            resolution: this.timeService.getResolution() * resolution
        };
        return this.metricService.getMetrics(metricQueries, options, this.getSingleLimitationId(limitationIds))
            .then(function (metrics) { return _.filter(metrics, function (m) { return !!(m && m.id); }); })
            .then(function (metrics) {
            if (!metrics || !metrics.length) {
                _this.$log.warn("No metrics for [" + JSON.stringify(metricQueries) + "] available.");
                return [];
            }
            return metrics;
        });
    };
    ChartMetricsService.prototype.getRealTimeMetrics = function (metricQueries, timeframe, resolution, count) {
        var _this = this;
        if (resolution === void 0) { resolution = 1; }
        if (count === void 0) { count = 0; }
        var options = {
            timeFrame: timeframe,
            resolution: this.timeService.getResolution() * resolution,
            count: count
        };
        return this.metricService.getRealTimeMetrics(metricQueries, options)
            .then(function (metrics) { return _.filter(metrics, function (m) { return !!(m && m.id); }); })
            .then(function (metrics) {
            if (!metrics || !metrics.length) {
                _this.$log.warn("No real time metrics for [" + JSON.stringify(metricQueries) + "] available.");
                return [];
            }
            return metrics;
        });
    };
    ChartMetricsService.prototype.getEntityMetricsMetadata = function (opid) {
        var _this = this;
        return this.metricService.getEntityMetricsMetadata(opid)
            .then(function (metrics) { return _.filter(metrics, function (m) { return !!(m && m.id); }); })
            .then(function (metrics) {
            if (!metrics || !metrics.length) {
                _this.$log.warn("No metrics metadata for [" + opid + "] available.");
                return [];
            }
            return metrics;
        });
    };
    ChartMetricsService.prototype.getSingleLimitationId = function (limitationIds) {
        if (typeof limitationIds === "number") {
            return limitationIds;
        }
        var parseResult = parseInt(limitationIds, 10);
        return isNaN(parseResult) ? null : parseResult;
    };
    ChartMetricsService.prototype.formatDisplayName = function (series, config) {
        if (!config || !config.excludeSourceFromLabel) {
            if (!series.originalDisplayName) {
                series.originalDisplayName = series.displayName;
            }
            var displayName = series.sourceDisplayName + " : " + series.originalDisplayName;
            series.displayName = displayName;
        }
    };
    /**
     * Converts provided perfstack event metrics into charts-d3 event bar stacked series
     * @param {IMetricDto} metrics Array of perfstack event metrics
     * @returns {ISimpleSeries} areaRange/columnRange-compatible series
     */
    ChartMetricsService.prototype.metricDtoToEventSeries = function (metric) {
        var sourceMetrics = metric.subMetrics || [metric];
        function prepareValue() {
            return sourceMetrics.reduce(function (value, subMetric) {
                value[subMetric.displayName] = 0;
                return value;
            }, {});
        }
        function getColor(key) {
            return _.find(sourceMetrics, function (m) { return m.displayName === key; }).colorType;
        }
        var dataDict = {};
        _.forEach(sourceMetrics, function (subMetric) {
            _.forEach(subMetric.measurements, function (event) {
                var value = dataDict[event.dateTimeStamp] || (dataDict[event.dateTimeStamp] = prepareValue());
                if (event.value) {
                    value[subMetric.displayName] += event.value;
                }
            });
        });
        var data = [];
        _.forOwn(dataDict, function (value, key) {
            var event = {
                data: moment(key).toDate(),
                value: []
            };
            _.forOwn(value, function (v, k) {
                event.value.push({
                    cssClass: "xui-chart-alert-event-" + getColor(k),
                    type: k,
                    quantity: v
                });
            });
            data.push(event);
        });
        var result = {
            id: metric.entityId + "-Orion.PerfStack.Events",
            type: 3,
            displayName: metric.displayName,
            sourceDisplayName: metric.entityDisplayName,
            isPercentile: false,
            unitType: charts_d3_1.DomainType.number,
            opacity: 1,
            visible: true,
            data: data,
            subSeries: [],
            xAxisLabel: "",
            yAxisLabel: metric.displayName,
            precision: 0
        };
        return result;
    };
    ChartMetricsService.prototype.getSubMetrics = function (perfstackMetrics) {
        if (perfstackMetrics.length !== 1) {
            this.$log.warn("Unable to draw more than 1 metric for proportional type of plot");
            return [];
        }
        var metric = perfstackMetrics[0];
        if (_.isEmpty(metric.subMetrics)) {
            this.$log.warn("To draw proportional bar/area chart at least one subSeries should exist");
            // We need to push at least something since plot needs to be created
            // this can be done better
            return [this.metricDtoToMetricSeries(metric)];
        }
        return this.subMetricToMetricSeries(metric);
    };
    ChartMetricsService.prototype.subMetricToMetricSeries = function (perfstackMetric) {
        var _this = this;
        var metricType = this.orionToCharts(perfstackMetric.type);
        return _.map(perfstackMetric.subMetrics, function (metric) { return _.assignIn(_this.metricDtoToMetricSeries(metric), {
            type: metricType,
        }); });
    };
    ChartMetricsService = __decorate([
        __param(0, inject_1.Inject("getTextService")),
        __param(1, inject_1.Inject("swTimeService")),
        __param(2, inject_1.Inject("$q")),
        __param(3, inject_1.Inject("$log")),
        __param(4, inject_1.Inject("xuiUnitConversionService")),
        __param(5, inject_1.Inject("xuiUnitConversionConstants")),
        __param(6, inject_1.Inject("swPerfstackMetricService")),
        __metadata("design:paramtypes", [Function, Object, Function, Object, Object, Object, Object])
    ], ChartMetricsService);
    return ChartMetricsService;
}());
exports.ChartMetricsService = ChartMetricsService;
exports.default = ChartMetricsService;


/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__number__ = __webpack_require__(5);


/* harmony default export */ __webpack_exports__["a"] = (function(values, p, valueof) {
  if (valueof == null) valueof = __WEBPACK_IMPORTED_MODULE_0__number__["a" /* default */];
  if (!(n = values.length)) return;
  if ((p = +p) <= 0 || n < 2) return +valueof(values[0], 0, values);
  if (p >= 1) return +valueof(values[n - 1], n - 1, values);
  var n,
      i = (n - 1) * p,
      i0 = Math.floor(i),
      value0 = +valueof(values[i0], i0, values),
      value1 = +valueof(values[i0 + 1], i0 + 1, values);
  return value0 + (value1 - value0) * (i - i0);
});


/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<div class=asa-high-availability-chart xui-busy=chart.isBusy> <div class=xui-padding-mdv> <span class=xui-text-s _t>HA type:</span> <span class=\"xui-text-r ha-type\">{{chart.haType}}</span>; <span class=xui-text-s _t>Last failover:</span> <span class=\"xui-text-r last-failover-text\">{{chart.lastFailoverText}}</span> </div> <div class=graph-container> <div class=graph> <div class=\"node peer {{chart.haInfo.peer.status | orionStatusToXuiIcon}}\"> <div class=node-icon> <xui-icon icon=firewall></xui-icon> </div> </div> <div class=\"link {{chart.haInfo.linkInfo.status | orionStatusToXuiIcon}}\"></div> <div class=\"node current {{chart.haInfo.current.status | orionStatusToXuiIcon}}\"> <div class=node-icon> <xui-icon icon=firewall></xui-icon> </div> </div> </div> <div class=\"graph labels\"> <div class=\"peer node-labels\"> <div class=\"node-name xui-text-a\"> <a ng-if=chart.haInfo.peer.url target=_blank href={{chart.haInfo.peer.url}}>{{chart.haInfo.peer.name}}</a> <div ng-if=!chart.haInfo.peer.url>{{chart.haInfo.peer.name}}</div> </div> <div class=ha-mode ng-if=chart.haInfo.peer.mode>{{chart.peerModeLocalized}}</div> <div class=ha-role ng-if=chart.haInfo.peer.role>{{chart.peerRoleLocalized}}</div> </div> <div class=link-labels> <div class=content> <div ng-repeat=\"linkInfo in chart.linkStateInfos track by $index\" class=\"xui-padding-sm {{chart.enum.haStateType[linkInfo.haStateType]}}\"> <div> <div class=media> <div class=\"media-left media-top\"> <xui-icon is-dynamic=true icon=\"{{linkInfo.haState | nodeStatusToXuiStyle:'severity'}}\"></xui-icon> </div> <div class=media-body> <div class=\"link-title {{linkInfo.haState | nodeStatusToXuiStyle:'text'}}\">{{linkInfo.title}}</div> <div ng-if=\"linkInfo.haState == chart.enum.haState.Down\"> <div class=xui-text-dscrn _t>{{linkInfo.description}}</div> </div> </div> </div> </div> <script type=text/ng-template id=link-info-template-{{$index}}> <div class=\"media-body\">\n                                <div ng-if=\"chart.haInfo.linkInfo.syncState == chart.enum.haState.Up\">\n                                    <div _t>{{linkInfo.description}}</div>\n                                </div>\n                            </div> </script> </div> </div> </div> <div class=\"current node-labels\"> <div class=node-name>{{chart.haInfo.current.name}}</div> <div class=ha-mode ng-if=chart.haInfo.current.mode>{{chart.currentModeLocalized}}</div> <div class=ha-role ng-if=chart.haInfo.current.role>{{chart.currentRoleLocalized}}</div> </div> </div> </div> </div> ";

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Orion-object-status enum
 *
 * @enum
 */
var OrionObjectStatus;
(function (OrionObjectStatus) {
    OrionObjectStatus[OrionObjectStatus["Unknown"] = 0] = "Unknown";
    OrionObjectStatus[OrionObjectStatus["Up"] = 1] = "Up";
    OrionObjectStatus[OrionObjectStatus["Down"] = 2] = "Down";
    OrionObjectStatus[OrionObjectStatus["Warning"] = 3] = "Warning";
    OrionObjectStatus[OrionObjectStatus["Shutdown"] = 4] = "Shutdown";
    OrionObjectStatus[OrionObjectStatus["Testing"] = 5] = "Testing";
    OrionObjectStatus[OrionObjectStatus["Dormant"] = 6] = "Dormant";
    OrionObjectStatus[OrionObjectStatus["NotPresent"] = 7] = "NotPresent";
    OrionObjectStatus[OrionObjectStatus["LowerLayerDown"] = 8] = "LowerLayerDown";
    OrionObjectStatus[OrionObjectStatus["Unmanaged"] = 9] = "Unmanaged";
    OrionObjectStatus[OrionObjectStatus["Unplugged"] = 10] = "Unplugged";
    OrionObjectStatus[OrionObjectStatus["External"] = 11] = "External";
    OrionObjectStatus[OrionObjectStatus["Unreachable"] = 12] = "Unreachable";
    OrionObjectStatus[OrionObjectStatus["Critical"] = 14] = "Critical";
    OrionObjectStatus[OrionObjectStatus["Inactive"] = 24] = "Inactive";
    OrionObjectStatus[OrionObjectStatus["NotRunning"] = 30] = "NotRunning";
})(OrionObjectStatus = exports.OrionObjectStatus || (exports.OrionObjectStatus = {}));


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var charts_d3_1 = __webpack_require__(6);
var PerfstackWidgetService = /** @class */ (function () {
    /** @ngInject */
    PerfstackWidgetService.$inject = ["$q", "swPerfstackMetricService"];
    function PerfstackWidgetService($q, swPerfstackMetricService) {
        this.$q = $q;
        this.swPerfstackMetricService = swPerfstackMetricService;
        this.metricTypeToPlotDefinition = {};
        this.metricsMetadataCache = {};
        this.initMap();
    }
    PerfstackWidgetService.prototype.getPlotDefinitionForPerfstackMetricId = function (opidMetricId) {
        var _this = this;
        var _a = this.parseOpidMetricId(opidMetricId), opid = _a.opid, metricId = _a.metricId;
        return this.getMetricsMetadata(opid).then(function (metrics) {
            var metric = _.find(metrics, function (m) { return m.id === opidMetricId; });
            return metric ?
                _this.metricTypeToPlotDefinition[metric.type]
                : undefined;
        });
    };
    PerfstackWidgetService.prototype.initMap = function () {
        this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.gauge] = {
            config: {
                plotType: charts_d3_1.PlotType.axis,
                markType: charts_d3_1.MarkType.line
            },
            stackable: true
        };
        this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.counter] = this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.gauge];
        this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.state] = {
            config: {
                plotType: charts_d3_1.PlotType.stacked,
                markType: charts_d3_1.MarkType.state
            },
            stackable: false
        };
        this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.multi] = {
            config: {
                plotType: charts_d3_1.PlotType.proportional,
                markType: charts_d3_1.MarkType.areaStacked
            },
            stackable: false
        };
        this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.event] = {
            config: {
                plotType: charts_d3_1.PlotType.event,
                markType: charts_d3_1.MarkType.barStacked
            },
            stackable: false
        };
        this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.simple] = {
            config: {
                plotType: charts_d3_1.PlotType.proportional,
                markType: charts_d3_1.MarkType.barStacked
            },
            stackable: false
        };
        this.metricTypeToPlotDefinition[charts_d3_1.SeriesType.alert] = {
            config: {
                plotType: charts_d3_1.PlotType.proportional,
                markType: charts_d3_1.MarkType.barStacked
            },
            stackable: true
        };
    };
    PerfstackWidgetService.prototype.getMetricsMetadata = function (opid) {
        var _this = this;
        if (opid in this.metricsMetadataCache) {
            return this.$q.resolve(this.metricsMetadataCache[opid]);
        }
        return this.swPerfstackMetricService
            .getEntityMetricsMetadata(opid)
            .then(function (metrics) { return _this.metricsMetadataCache[opid] = metrics; });
    };
    PerfstackWidgetService.prototype.parseOpidMetricId = function (opidMetricId) {
        var parts = opidMetricId.split("-");
        return {
            opid: parts[0],
            metricId: parts[1]
        };
    };
    return PerfstackWidgetService;
}());
exports.PerfstackWidgetService = PerfstackWidgetService;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var UserService = /** @class */ (function () {
    function UserService() {
    }
    UserService.prototype.getCurrentUser = function () {
        return window.SW.user;
    };
    UserService.prototype.userHasRole = function (role) {
        var user = this.getCurrentUser();
        if (!user) {
            return false;
        }
        return user[role] === true;
    };
    return UserService;
}());
exports.UserService = UserService;


/***/ }),
/* 16 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-datalist-widget> <xui-strip-layout ng-if=dataList.showSearchSorterValue class=sw-datalist-widget__top-container> <xui-sorter items-source=dataList.sortColumns on-change=\"dataList.onSortChange(newValue, oldValue)\" selected-item=dataList.sortSelection.sortBy display-value=name class=sw-datalist-widget__top-container-sort sort-direction=dataList.sortSelection.direction uib-tooltip=\"Sort by\" tooltip-append-to-body=true tooltip-class=custom-tooltip> </xui-sorter> <xui-search on-search=\"dataList.onSearchTriggered(value, cancellation)\" class=sw-datalist-widget__top-container-search> </xui-search> </xui-strip-layout> <xui-listview ng-hide=dataList.emptySearch class=sw-datalist-widget__list stripe=true row-padding=narrow track-by=dataList.trackBy items-source=dataList.itemsSource template-url=\"{{ dataList.itemTemplateUrl }}\"> </xui-listview> <xui-empty ng-show=dataList.emptySearch empty-data=dataList.noSearchResults> </xui-empty> <xui-pager class=small page=dataList.currentPage page-size=dataList.pageSize total=dataList.totalItems adjacent=0 dots=... scroll-top=false hide-if-empty=false show-prev-next=true pager-action=\"dataList.onPageChanged(page, pageSize, total)\"></xui-pager> </div> ";

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "swAllAlertsWidgetSettings";
var AllAlertsController = /** @class */ (function () {
    /** @ngInject */
    AllAlertsController.$inject = ["$log", "$scope", "$location", "swAlertsService", "swDemoService", "pollingService", "getTextService", "siteFilterService", "swOrionViewService"];
    function AllAlertsController($log, $scope, $location, swAlertsService, swDemoService, pollingService, getTextService, siteFilterService, swOrionViewService) {
        var _this = this;
        this.$log = $log;
        this.$scope = $scope;
        this.$location = $location;
        this.swAlertsService = swAlertsService;
        this.swDemoService = swDemoService;
        this.pollingService = pollingService;
        this.getTextService = getTextService;
        this.siteFilterService = siteFilterService;
        this.swOrionViewService = swOrionViewService;
        this.$onInit = function () {
            _this.initProperties();
            _this.$scope.$watch(function () { return _this.swAlertsService.acknowledging; }, function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    _this.onChange();
                }
            });
            _this.handleDemoMode();
            _this.$scope.$on("acknowledgeAlertEvent", function (event, alertId) {
                _this.acknowledgeAlertEventCallback(alertId);
            });
            _this.viewInfo = _this.swOrionViewService.activeViewInfo;
        };
        this.siteFilterChanged = function () {
            _this.onChange(true);
        };
        this._t = this.getTextService;
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.removeSeverityFilters = function (filterName) {
            _.remove(_this.config.settings.activefilters, function (af) { return af === filterName; });
            _this.config.saveSettings();
        };
        this.onLoad = function (config) {
            _this.config = config;
            _this.settings = config.settings;
            _.assign(_this.sortingInitialData, _this.settings.sorting || _this.sortingInitialData);
            _this.checkForToolsPanelToggle();
            _this.paginationData.pageSize = _this.settings.rowsperpage;
            if (_.isEmpty(_this.settings.acknowledgestate)) {
                _this.settings.acknowledgestate = _this.settings.acknowledgestate ||
                    { name: _this._t("Not Acknowledged Only"), value: false, id: 1 };
                _this.config.saveSettings();
            }
            _this.pollingService.register(_this.onBackgroundUpdate);
            _this.config.isBusy(true, _this._t("Updating..."));
            _this.siteFilterService.onSiteFilterChanged(_this.siteFilterChanged);
        };
        this.onUnload = function () {
            _this.pollingService.unregister(_this.onBackgroundUpdate);
        };
        this.onSettingsChanged = function (settings) {
            _this.settings = settings;
            _this.checkForToolsPanelToggle();
            _this.config.settings = _this.settings;
            _this.onChange();
        };
        this.getTotal = function () {
            if (_this.items && _this.items.total) {
                return _this.items.total;
            }
            return 0;
        };
        this.mergeWidgetAndFilterSites = function () {
            var sitesToDisplay = [];
            var widgetSites = _this.settings.sitesenabled;
            var filterSites = _this.siteFilterService.getSelectedSites();
            sitesToDisplay = _.intersection(filterSites, widgetSites);
            if (sitesToDisplay.length === 0) {
                sitesToDisplay.push(-1);
            }
            return sitesToDisplay;
        };
        this.getSitesToDisplay = function () {
            var filterSites = _this.siteFilterService.getSelectedSites();
            if (filterSites && filterSites.length > 0) {
                if (_this.settings.sitestate && _this.settings.sitestate.name === _this.allSitesValue) {
                    return filterSites;
                }
                if (_this.settings.sitesenabled && _this.settings.sitestate.name !== _this.allSitesValue) {
                    return _this.mergeWidgetAndFilterSites();
                }
                return filterSites;
            }
            if (_this.settings.sitesenabled) {
                return _this.settings.sitesenabled;
            }
            return [];
        };
        this.initializeAlerts = function (result, sitesToDisplay) {
            _this.sortingInitialData.sortableColumns = _.sortBy(_this.swAlertsService.sortColumns, "id");
            var currentSitesToDisplay = _this.getSitesToDisplay();
            var filterChanged = !((sitesToDisplay.length === currentSitesToDisplay.length) &&
                sitesToDisplay.every(function (element, index) {
                    return element === currentSitesToDisplay[index];
                }));
            if (filterChanged) {
                return;
            }
            _this.items = result;
            _this.totalCounts = result.total;
            if (_this.items.rows.length > 0) {
                _this.getNodeStatus(_this.items.rows);
                _this.nothingFound = false;
                _.each(_this.items.rows, function (item) { return item.searchValue = _this.searchValue; });
            }
            else {
                if (!_.isEmpty(_this.searchValue)) {
                    _this.nothingFound = true;
                    return;
                }
            }
        };
        this.onChange = function (showBusy, searchValue) {
            if (showBusy === void 0) { showBusy = true; }
            if (searchValue === void 0) { searchValue = _this.searchValue; }
            if (_this.siteFilterService.filterSettingsLoaded === true ||
                _this.siteFilterService.filterMode === "single") {
                _this.config.isBusy(showBusy, _this._t("Updating..."));
                var sitesToDisplay_1 = _this.getSitesToDisplay();
                var triggeringObjectEntityNames_1 = _this.settings.entitytypes;
                var triggeringObjectEntityUris_1 = null;
                if (_this.viewInfo.dynamicInfo
                    && triggeringObjectEntityNames_1
                    && triggeringObjectEntityNames_1.length > 0) {
                    if (_this.viewInfo.dynamicInfo.uri) {
                        triggeringObjectEntityUris_1 = [_this.viewInfo.dynamicInfo.uri];
                    }
                    else if (_this.viewInfo.dynamicInfo.nodeUri) {
                        triggeringObjectEntityUris_1 = [_this.viewInfo.dynamicInfo.nodeUri];
                    }
                }
                else if (_this.settings.objecturi) {
                    triggeringObjectEntityUris_1 = [_this.settings.objecturi];
                }
                _this.$scope.$evalAsync(function () {
                    _this.swAlertsService
                        .getAlerts(_this.settings.showacknowledgedalerts, _this.paginationData.page, _this.settings.rowsperpage, { column: _this.sortingInitialData.sortBy.id, direction: _this.sortingInitialData.direction }, _this.settings.activefilters, searchValue, _this.settings.showonlyacknowledgedalerts, sitesToDisplay_1, _this.settings.limitations ? JSON.parse("[" + _this.settings.limitations + "]") : null, "", 0, triggeringObjectEntityNames_1, triggeringObjectEntityUris_1)
                        .then(function (result) {
                        _this.initializeAlerts(result, sitesToDisplay_1);
                    })
                        .finally(function () {
                        if (_this.totalCounts > 0) {
                            _this.config.title(_this.settings.title + " (" + _this.totalCounts + ")");
                        }
                        _this.config.isBusy(false);
                        _this.loaded = true;
                        _.assign(_this.paginationData, { pageSize: _this.settings.rowsperpage, total: _this.getTotal() });
                    });
                });
            }
        };
        this.onSortChange = function (oldValue, newValue) {
            _this.settings.sorting = newValue;
            _.assign(_this.sortingData, newValue);
            if (_this.swDemoService.isDemoMode()) {
                _this.onChange();
            }
            else {
                _this.config.saveSettings();
            }
        };
        this.onPageChange = function (page, pageSize, total) {
            if (!_.isEqual(_this.paginationData.pageSize, pageSize)) {
                _this.settings.rowsperpage = pageSize;
                if (!_this.swDemoService.isDemoMode()) {
                    _this.config.saveSettings();
                }
            }
            if (!_.isEqual(_this.paginationData.page, page)) {
                _this.paginationData.page = page;
                _this.onChange();
            }
        };
        this.filterSaveSettings = function () {
            if (_this.settings.activefilters.length || _this.settings.activefilters.length === 0) {
                _this.goToFirstPage();
                if (_this.swDemoService.isDemoMode()) {
                    _this.onChange();
                }
                else {
                    _this.config.saveSettings();
                }
            }
        };
        this.getNodeStatus = function (rows) {
            _this.swAlertsService.getNodeStatus(rows);
        };
        this.updateEmptyState = function () {
            _this.noAlertsState = {
                compactMode: true,
                description: _this._t("There are no active alerts. Great job."),
                image: "no-alerts"
            };
        };
        this.cancelSearch = function () {
            _this.updateEmptyState();
            _this.goToFirstPage();
            _this.searchValue = "";
            _this.onChange();
        };
        this.goToFirstPage = function () {
            _this.paginationData.page = 1;
        };
        this.checkForToolsPanelToggle = function () {
            _this.gridOptions.hideSearch = !_this.settings.showsearchandsorterbar;
            _this.sortingData = _this.settings.showsearchandsorterbar ? _this.sortingInitialData : {};
        };
        // prevent sorting and filtering in demo mode
        // TODO: rewrite into directive link function after all-alerts decoupling
        this.handleDemoMode = function () {
            if (_this.swDemoService.isDemoMode()) {
                var allAlertsWidgets_1 = $("sw-all-alerts-widget");
                var blockChildElem_1 = function (e) {
                    var elemsToBlock = allAlertsWidgets_1.find(".sw-all-alerts__item-menu-actions___item");
                    if (elemsToBlock.length > 0) {
                        elemsToBlock.each(function (idx, elem) {
                            if ($.contains(elem, $(e.target)[0])) {
                                e.stopPropagation();
                                _this.swDemoService.showDemoErrorToast();
                            }
                        });
                    }
                };
                if (allAlertsWidgets_1.length > 0) {
                    allAlertsWidgets_1.each(function (idx, widget) {
                        widget.addEventListener("click", blockChildElem_1, true);
                    });
                }
            }
        };
    }
    AllAlertsController.prototype.initProperties = function () {
        this.noSearchResults = {
            image: "no-search-results",
            description: this._t("Try searching for something else, use the name of an object or a swql query.")
        };
        this.items = {
            rows: [],
            metadata: {
                filters: [],
                totalCounts: 0,
            },
            total: 0
        };
        this.totalCounts = 0;
        this.showEmpty = false;
        this.gridPaddingSize = "none";
        this.searchValue = "";
        this.style = "sw-all-alerts";
        this.allSitesValue = "All";
        this.nothingFound = false;
        this.loaded = false;
        this.noAlertsState = {
            compactMode: true,
            description: this._t("There are no active alerts. Great job."),
            image: "no-alerts"
        };
        this.paginationData = {
            page: 1,
            pageSize: 5,
            total: 0
        };
        this.sortingInitialData = {
            sortableColumns: [],
            sortBy: this.swAlertsService.sortColumns[7],
            direction: "desc"
        };
        this.gridOptions = {
            hideSearch: false,
            pagerAdjacent: 0
        };
        this.severities = {
            "0": "severity_info",
            "1": "severity_warning",
            "2": "severity_critical",
            "3": "severity_critical",
            "4": "severity_info"
        };
    };
    AllAlertsController.prototype.acknowledgeAlertEventCallback = function (alertId) {
        var alert = _.find(this.items.rows, function (row) { return row.AlertId === alertId; });
        var sameSeverityAlerts = this.items.rows.filter(function (row) { return row.severityIcon === alert.severityIcon; });
        if (sameSeverityAlerts.length <= 1) {
            this.removeSeverityFilters(this.severities[alert.Severity]);
        }
    };
    AllAlertsController.prototype.onSearch = function (item, cancellation) {
        if (_.isUndefined(item)) {
            this.cancelSearch();
            return;
        }
        this.goToFirstPage();
        this.searchValue = item;
        this.noAlertsState = _.assign(this.noSearchResults, {
            title: "No result found for \"" + this.searchValue + "\""
        });
        this.onChange();
    };
    AllAlertsController = __decorate([
        widget_decorator_1.Widget({
            selector: "swAllAlertsWidget",
            templateUrl: "[widgets]:/components/tabular/allAlerts/allAlerts-widget.html",
            controllerAs: "alerts",
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object, Function, Object, Object])
    ], AllAlertsController);
    return AllAlertsController;
}());
exports.AllAlertsController = AllAlertsController;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../../ref.d.ts"/>
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var charts_d3_1 = __webpack_require__(6);
var widget_decorator_1 = __webpack_require__(0);
var chartMetrics_service_1 = __webpack_require__(10);
var inject_1 = __webpack_require__(4);
var constants_1 = __webpack_require__(9);
var d3_array_1 = __webpack_require__(116);
exports.WidgetSettingsName = "swChartWidgetSettings";
var ChartWidgetController = /** @class */ (function () {
    /** @ngInject */
    ChartWidgetController.$inject = ["$log", "$q", "$timeout", "$rootScope", "pollingService", "novaChartManager", "swApi", "swChartMetricsService", "swisService", "swWidgetService", "swNetObjectOpidConverterService", "swOrionSettingsService", "viewService", "viewInfo", "_t", "$interval", "timeService", "constants", "swDemoService"];
    function ChartWidgetController($log, $q, $timeout, $rootScope, pollingService, novaChartManager, swApi, swChartMetricsService, swisService, swWidgetService, swNetObjectOpidConverterService, swOrionSettingsService, viewService, viewInfo, _t, $interval, timeService, constants, swDemoService) {
        var _this = this;
        this.$log = $log;
        this.$q = $q;
        this.$timeout = $timeout;
        this.$rootScope = $rootScope;
        this.pollingService = pollingService;
        this.novaChartManager = novaChartManager;
        this.swApi = swApi;
        this.swChartMetricsService = swChartMetricsService;
        this.swisService = swisService;
        this.swWidgetService = swWidgetService;
        this.swNetObjectOpidConverterService = swNetObjectOpidConverterService;
        this.swOrionSettingsService = swOrionSettingsService;
        this.viewService = viewService;
        this.viewInfo = viewInfo;
        this._t = _t;
        this.$interval = $interval;
        this.timeService = timeService;
        this.constants = constants;
        this.swDemoService = swDemoService;
        this.$onInit = function () {
            _this.currentChartsModel = [];
            _this.currentTimeframe = _this.swChartMetricsService.defaultTimeFrame();
            _this.chartsModelMap = {};
            _this.exportMetrics = [];
            _this.chartModelIsReady = false;
            _this.radioList = [];
            _this.selectedModelisReady = false;
        };
        // chart series limit, dafault value is 10
        this.plotSeriesLimit = constants_1.default.plotSeriesDefaultLimit;
        this.initialTimeFrame = this.$q.defer();
        this.realTimeUpdatePeriod = 1000;
        this.realTimeRangeInMinutes = 10;
        this.onLoad = function (config) {
            _this.config = config;
            _this.widgetId = _this.config.id;
            var chartsConfig = _this.settings.chartconfiguration;
            if (!chartsConfig) {
                return;
            }
            _this.setupWidgetLinks();
            _this.setupSeriesLimit();
            _this.setupTimeframePicker();
            var firstChart = chartsConfig.charts[0];
            // Default tab needs to be set before initial model is set.
            _this.defaultTabId = (chartsConfig.tabsView && chartsConfig.tabsView.dafaultTabId)
                || firstChart.tabId;
            if (chartsConfig.showEntityInfo) {
                _this.getEntityInfo();
            }
            if (chartsConfig.realTime) {
                if (_this.constants.environment.RealTimeChartsUpdateInterval > 0) {
                    _this.realTimeUpdatePeriod = _this.constants.environment.RealTimeChartsUpdateInterval * 1000;
                }
                _this.updateRealTimeSeriesInterval = _this.$interval(_this.updateRealTimeSeries, _this.realTimeUpdatePeriod);
                _this.updateChartsInterval = _this.$interval(_this.updateCharts, _this.realTimeUpdatePeriod);
                return;
            }
            if (chartsConfig.listView) {
                //we make user set only one element in charts array
                _this.getRelatedMetrics(firstChart.relatedMetrics)
                    .then(function (children) {
                    // Here we always have IListItemObject[]
                    _.each(children, function (el, index) {
                        var item = _this.radioList[index] || (_this.radioList[index] = {});
                        _.assign(item, el);
                    });
                    _this.selectedSeriesIds = !_.isEmpty(_this.selectedSeriesIds)
                        ? _this.selectedSeriesIds
                        : [_this.radioList[0].id];
                    _this.$timeout(function () {
                        _this.selectedModelisReady = true;
                    });
                })
                    .catch(function (err) {
                    _this.$log.warn("Promise for getRelatedMetrics rejected: ", err);
                });
            }
            _this.pollingService.register(_this.updateHistoricalSeries, false);
        };
        this.onUnload = function () {
            if (_this.settings.chartconfiguration.realTime) {
                _this.$interval.cancel(_this.updateRealTimeSeriesInterval);
                _this.$interval.cancel(_this.updateChartsInterval);
            }
            else {
                _this.pollingService.unregister(_this.updateHistoricalSeries);
            }
        };
        this.onSettingsChanged = function (settings, afterSave) {
            _this.config.settings = settings;
            _this.updateChartModel();
            if (afterSave && !_this.settings.chartconfiguration.realTime) {
                _this.updateHistoricalSeries();
            }
            // we need access to 'custom' timeframe presets which are loaded by child component
            // For tabs view: chartModelReady flag should be set to true AFTER models in tabList are set
            _this.$timeout(function () {
                if (_this.timeframeSettings && _this.timeframeSettings.presetId) {
                    _this.timeframePresetId = _this.timeframeSettings.presetId;
                }
                _this.chartModelIsReady = true;
            });
        };
        this.onTimeframeChanged = function (timeframe) {
            if (!timeframe) {
                return;
            }
            _this.currentTimeframe = angular.copy(_this.swChartMetricsService.updateTimeFrame(timeframe));
            if (!_this.initialTimeFrame && !_this.settings.chartconfiguration.realTime) {
                // on all subsequent timerange changes we are updating all series and storing preset if needed
                _this.updateHistoricalSeries();
                if (timeframe.selectedPresetId) {
                    _this.updateTimeframeSettings(timeframe);
                }
            }
            if (_this.initialTimeFrame &&
                (_this.settings.chartconfiguration.timePickerMode !== "day" || timeframe.selectedPresetId === null)) {
                // on the initial timerange change we are resolving promise, which fires addSeries,
                // which is passed to onRender in chart model, which guarantees the correct timerange
                _this.initialTimeFrame.resolve();
                _this.initialTimeFrame = undefined;
            }
        };
        this.isSimpleView = function () {
            var config = _.has(_this, "config") ? _this.settings.chartconfiguration : false;
            return config && !(config.tabsView && config.tabsView.visible) && !config.listView;
        };
        this.isTabsView = function () {
            var config = _.has(_this, "config") ? _this.settings.chartconfiguration : false;
            return config && config.tabsView && config.tabsView.visible && _this.chartModelIsReady;
        };
        this.isListView = function () {
            var config = _.has(_this, "config") ? _this.settings.chartconfiguration : false;
            return config && config.listView;
        };
        this.templateFn = function () {
            return __webpack_require__(32);
        };
        this.onSelectChanged = function () {
            _this.updateHistoricalSeries();
        };
        this.setupWidgetLinks = function () {
            _this.customLinks = [
                {
                    href: _this.getPerfStackUrl(_this.timeframeSettings.presetId),
                    caption: _this._t("Open In PerfStack"),
                    customCss: "sw-btn-export_" + _this.widgetId,
                    position: 100,
                    target: "_blank",
                }
            ];
            if (_this.settings.chartconfiguration.customLinks) {
                _this.settings.chartconfiguration.customLinks.forEach(function (element) {
                    var linkId = _this.widgetId + "_" + _.uniqueId();
                    var customLink = {
                        id: linkId,
                        caption: element.caption,
                        href: element.href,
                        position: element.position,
                        customCss: "sw-btn-custom_" + linkId + " " + element.customCss || "",
                        target: element.target || "_self",
                    };
                    _this.customLinks.push(customLink);
                    _this.viewService.parseMacros(customLink, _this.viewInfo.activeViewInfo.netObject)
                        .then(function (response) {
                        var button = angular.element(".sw-btn-custom_" + response.id);
                        button.attr("href", response.href);
                    });
                });
            }
        };
        this.startUpdatingCharts = function () {
            _this.chartsUpdated = 0;
            _this.config.isBusy(true);
        };
        this.stopUpdatingCharts = function (chartId) {
            // for each chart model
            // HACK: Fix for width changes when scrollbars appear for drag and disappear after drag state
            _this.redrawChart(chartId);
            _this.chartsUpdated++;
            // after all chart models are ready
            if (_this.chartsUpdated === _this.currentChartsModel.length) {
                _this.updateExportButton(_this.timeframeSettings.presetId);
                _this.config.isBusy(false);
            }
        };
        this.defineSeriesConfigType = function (type) {
            switch (type) {
                case "typicalDay":
                    return charts_d3_1.SeriesConfigType.typicalDay;
                case "isStatusThreshold":
                    return charts_d3_1.SeriesConfigType.isStatusThreshold;
                default:
                    return charts_d3_1.SeriesConfigType.regular;
            }
        };
        this.getListViewMetrics = function () {
            var currentRadioItem = _.find(_this.radioList, function (metric) { return metric.id === _this.selectedSeriesIds[0]; });
            if (!currentRadioItem) {
                return [];
            }
            if (currentRadioItem.extendedConfig) {
                return _.map(currentRadioItem.extendedConfig, function (m) { return ({ id: m.id }); });
            }
            return [{ id: currentRadioItem.id }];
        };
        this.addSeries = function (chartConfig) {
            var config = _this.settings.chartconfiguration;
            if (config.realTime) {
                _this.realTimeStartTime = moment();
                if (_.isEmpty(chartConfig.metrics)) {
                    _this.stopUpdatingCharts(chartConfig.id);
                    return;
                }
                var startTime = moment(_this.timeService.current().start());
                var realTimeMetricQueries = _this.getMetricQueriesFromConfig(chartConfig.metrics);
                return _this.getRealTimeMetrics(realTimeMetricQueries, startTime, 0)
                    .then(function (perfstackMetrics) { return _this.updateChartWithMetricsData(perfstackMetrics, chartConfig); })
                    .finally(function () { return _this.stopUpdatingCharts(chartConfig.id); })
                    .catch(function (err) { return _this.$log.warn("GetRealTimeMetrics Promise rejected: ", err); });
            }
            else {
                // in case of a listview there is no need to fetch related metrics: it is done onLoad
                var relatedMetricsList = config.listView ? [] : chartConfig.relatedMetrics;
                return _this.getRelatedMetrics(relatedMetricsList)
                    .then(function (childrenIds) {
                    var historicalMetricQueries = config.listView
                        ? _this.getListViewMetrics()
                        : _this.getMetricQueriesFromConfig(chartConfig.metrics);
                    var allMetricQueries = _.concat(historicalMetricQueries, _.map(childrenIds, function (c) { return ({ id: c }); }));
                    _this.exportMetrics = _.map(allMetricQueries, function (metricQuery) { return metricQuery.id; });
                    _this.getHistoricalMetrics(allMetricQueries)
                        .then(function (perfstackMetrics) { return _this.updateChartWithMetricsData(perfstackMetrics, chartConfig, childrenIds); })
                        .finally(function () { return _this.stopUpdatingCharts(chartConfig.id); })
                        .catch(function (err) { return _this.$log.warn("GetHistoricalMetrics Promise rejected: ", err); });
                })
                    .catch(function (err) { return _this.$log.warn("GetRelatedMetrics Promise rejected: ", err); });
            }
        };
        this.updateCharts = function () {
            _this.timeService.updateToWindow(true);
            _this.novaChartManager.redrawAll();
        };
        this.encodeSeriesId = function (id) {
            //We need to replace dots to another symbol in seried.id. More details here - NUI-1572
            return id ? id.replace(/\./g, "_DOT_") : id;
        };
        this.decodeSeriesId = function (id) {
            return id ? id.replace(/_DOT_/g, ".") : id;
        };
        this.updateHistoricalSeries = function () {
            if (!(_this.currentChartsModel && _this.currentChartsModel.length)) {
                return;
            }
            var config = _this.settings.chartconfiguration;
            _this.startUpdatingCharts();
            _.forEach(_this.currentChartsModel, function (chartModel) {
                var chartInstance = _this.novaChartManager.get(chartModel.id);
                if (chartInstance) {
                    var seriesDictionary_1 = chartInstance.series();
                    var chartConfig_1 = _.find(config.charts, function (c) { return c.id === chartModel.id; });
                    var metricQueries_1 = _this.extractMetricQueries(chartConfig_1, seriesDictionary_1, config, chartModel);
                    _this.getHistoricalMetrics(metricQueries_1)
                        .then(function (perfstackMetrics) {
                        var isEmptyChart = true;
                        if (perfstackMetrics && perfstackMetrics.length) {
                            var childrenIds = _.map(metricQueries_1, function (metricQuery) { return metricQuery.id; });
                            var metrics_1 = _this.mapMetrics(perfstackMetrics, chartModel, chartConfig_1, childrenIds);
                            var metricsType = _this.swChartMetricsService.chartsToPerfstack(metrics_1[0].type);
                            var properMarkType = _this.getMarkType(metricsType, chartConfig_1);
                            // here we are just removing everything and adding new series
                            // a bit more actions, but less code -> less errors.
                            var plot_1 = chartInstance.getPlotManager().getPlots()[0];
                            var plotConfig = _this.getPlotConfig(chartConfig_1, chartInstance.getPlotManager(), properMarkType, metricsType, plot_1.config());
                            // This is needed to update plot config after chart is edited in the dialog
                            // And also to apply some state (like trend visibility) that is stored in plot config
                            _this.applyPlotConfig(plot_1, plotConfig);
                            _.forEach(seriesDictionary_1, function (series) {
                                // before removal preserve visibility
                                var newMetric = _.find(metrics_1, { "id": _this.decodeSeriesId(series.id) });
                                if (newMetric) {
                                    newMetric.visible = series.visible;
                                }
                                plot_1.removeSeries(series.id);
                            });
                            metrics_1.forEach(function (metric) {
                                metric.id = _this.encodeSeriesId(metric.id);
                                plot_1.addSeries(metric, metric.config);
                                if (!_.isEmpty(metric.data)) {
                                    isEmptyChart = false;
                                }
                            });
                        }
                        // If no metrics at all - chart is considered to be empty
                        chartModel.isEmpty = isEmptyChart;
                    })
                        .finally(function () { return _this.stopUpdatingCharts(chartModel.id); })
                        .catch(function (err) {
                        _this.$log.warn("GetMetrics Promise rejected: ", err);
                    });
                }
            });
        };
        this.updateRealTimeSeries = function () {
            if (!(_this.currentChartsModel && _this.currentChartsModel.length)) {
                return;
            }
            var config = _this.settings.chartconfiguration;
            var bisectDate = d3_array_1.bisector(function (d) { return d.data; }).left;
            _.forEach(_this.currentChartsModel, function (chartModel) {
                var chartInstance = _this.novaChartManager.get(chartModel.id);
                if (chartInstance) {
                    var seriesDictionary = chartInstance.series();
                    if (_.isEmpty(seriesDictionary)) {
                        return;
                    }
                    var chartConfig_2 = _.find(config.charts, function (c) { return c.id === chartModel.id; });
                    var metricQueries_2 = _this.extractMetricQueries(chartConfig_2, seriesDictionary, config, chartModel);
                    var count = _.max([moment().diff(_this.realTimeStartTime, "seconds"), 1]) * 2;
                    _this.getRealTimeMetrics(metricQueries_2, null, count)
                        .then(function (perfstackMetrics) {
                        var isEmptyChart = true;
                        if (perfstackMetrics && perfstackMetrics.length) {
                            var childrenIds = _.map(metricQueries_2, function (metricQuery) { return metricQuery.id; });
                            var metrics = _this.mapMetrics(perfstackMetrics, chartModel, chartConfig_2, childrenIds);
                            var plot_2 = chartInstance.getPlotManager().getPlots()[0];
                            metrics.forEach(function (metric) {
                                metric.id = _this.encodeSeriesId(metric.id);
                                var series = plot_2.getSeries(metric.id);
                                var startIndex = bisectDate(series.data, metric.data[0].data);
                                series.data.splice(startIndex, series.data.length - startIndex);
                                series.data.push.apply(series.data, metric.data);
                                startIndex = bisectDate(series.data, moment().subtract(_this.realTimeRangeInMinutes, "minutes").toDate());
                                series.data.splice(0, startIndex);
                                chartInstance.interactionPubSub().seriesUpdated(plot_2.id(), "update", series.id);
                                if (!_.isEmpty(metric.data)) {
                                    isEmptyChart = false;
                                }
                            });
                            chartModel.isEmpty = isEmptyChart;
                        }
                    })
                        .catch(function (err) {
                        _this.$log.warn("GetMetrics Promise rejected: ", err);
                    });
                }
            });
            _this.realTimeStartTime = moment();
        };
        this.createModel = function (chartConfig, index) {
            _.defaults(chartConfig, { id: _this.widgetId ? _this.widgetId + "_" + index : _.uniqueId() });
            return _this.swChartMetricsService.createModel({
                id: chartConfig.id,
                displayName: chartConfig.title,
                emptyData: chartConfig.emptyData,
                legendOrientation: chartConfig.legendOrientation,
                legendType: chartConfig.legendType,
                isEmpty: true,
            }, _this.settings.chartconfiguration.realTime ? undefined : _this.initialTimeFrame, function () { return _this.addSeries(chartConfig); });
        };
        this.updateGlobalModel = function () {
            _this.startUpdatingCharts();
            var config = _this.settings.chartconfiguration;
            _this.currentChartsModel = _.map(config.listView ? config.charts.slice(0, 1) : config.charts, _this.createModel);
        };
        this.getTab = function (tabId) {
            return _.find(_this.settings.chartconfiguration.tabsView.tabsList, function (tab) { return tab.id === tabId; });
        };
        this.getPlotType = function (metricType, chartConfig) {
            // types that can be defined easily based on chartConfig's plotType only
            if (chartConfig.plotType === "barRange" || chartConfig.plotType === "areaRange") {
                return charts_d3_1.PlotType.range;
            }
            if (chartConfig.plotType === "barStacked"
                || chartConfig.plotType === "barStackedNormalized"
                || chartConfig.plotType === "areaStacked") {
                return charts_d3_1.PlotType.proportional;
            }
            switch (metricType) {
                case chartMetrics_service_1.PerfstackSeriesType.gauge:
                    return chartConfig.isSpark ? charts_d3_1.PlotType.stacked : charts_d3_1.PlotType.axis;
                case chartMetrics_service_1.PerfstackSeriesType.state:
                    return charts_d3_1.PlotType.stacked;
                default:
                    // Axis plot seems to be a valid default value
                    return charts_d3_1.PlotType.axis;
            }
        };
        // TODO: divide it into 2 methods: getChildrenForListview, getChildrenIds
        this.getChildMetrics = function (relatedMetricConfig, relationships) {
            function getMetricId(relationId) {
                return relationId + "-" + relatedMetricConfig.id;
            }
            var getMetricInfo = function (relation) {
                var newExtendedConfig;
                if (relatedMetricConfig.extendedConfig) {
                    newExtendedConfig = relatedMetricConfig.extendedConfig.map(function (ec) { return _.assign({}, ec); });
                    newExtendedConfig.forEach(function (el) { return el.id = relation.id + "-" + el.id; });
                }
                var metricInfo = _this.mapEntityDataToListItemObject(relation);
                metricInfo.id = relatedMetricConfig.extendedConfig && getMetricId(relation.id);
                metricInfo.extendedConfig = newExtendedConfig;
                return metricInfo;
            };
            var filteredRelationData = _.filter(relationships, function (relation) { return relation.instanceType === relatedMetricConfig.entity; });
            return _this.settings.chartconfiguration.listView
                ? _.map(filteredRelationData, getMetricInfo)
                : _.map(filteredRelationData, function (relation) { return getMetricId(relation.id); });
        };
        this.getMetricQueriesFromConfig = function (metrics) {
            return _.map(metrics, function (metric) { return ({
                id: _this.settings.opid + "-" + metric.id,
                displayNameSource: metric.displayNameSource,
                groupBy: metric.groupBy
            }); });
        };
    }
    Object.defineProperty(ChartWidgetController.prototype, "settings", {
        get: function () {
            return this.config.settings;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ChartWidgetController.prototype, "timeframeSettings", {
        get: function () {
            var userSettings = this.settings.userSettings || (this.settings.userSettings = {});
            var settings = userSettings.chartTimeframe || (userSettings.chartTimeframe = {});
            if ((!settings.presetId) && (this.settings.chartconfiguration.presetTime)) {
                settings.presetId = this.settings.chartconfiguration.presetTime;
            }
            return settings;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ChartWidgetController.prototype, "selectedTabId", {
        get: function () {
            return this._selectedTabId;
        },
        set: function (value) {
            this._selectedTabId = value;
            // After selecting another tab, global model needs to be updated.
            this.updateChartModel();
        },
        enumerable: true,
        configurable: true
    });
    ChartWidgetController.prototype.setupTimeframePicker = function () {
        var chartsConfig = this.settings.chartconfiguration;
        if (chartsConfig.realTime) {
            this.timeframePickerConfig = {
                isHidden: true,
            };
            var now = moment();
            this.currentTimeframe = this.swChartMetricsService.customTimeFrame(moment(now).subtract(this.realTimeRangeInMinutes, "minutes"), moment(now));
            this.timeframePickerRange = {
                selectedPresetId: undefined,
                startDatetime: this.currentTimeframe.start(),
                endDatetime: this.currentTimeframe.end()
            };
        }
        else if (chartsConfig.timePickerMode === "day") {
            this.timeframePickerConfig = { mode: "day" };
        }
    };
    ChartWidgetController.prototype.getTabChartsModel = function (tab) {
        return this.chartsModelMap[tab.id] || (this.chartsModelMap[tab.id] = []);
    };
    ChartWidgetController.prototype.updateTimeframeSettings = function (timeframe) {
        this.timeframeSettings.presetId = timeframe.selectedPresetId;
        // block sending post request for saving settings to db in demo mode
        if (this.swDemoService.isDemoMode()) {
            return;
        }
        if (this.settings.chartconfiguration.timePickerMode !== "day") {
            this.config.saveUserProperties(this.settings.userSettings);
        }
    };
    ChartWidgetController.prototype.updateExportButton = function (presetId) {
        return __awaiter(this, void 0, void 0, function () {
            var buttonSelector, button, retryCount;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        buttonSelector = ".sw-btn-export_" + this.widgetId;
                        button = angular.element(buttonSelector);
                        if (!(button.length === 0)) return [3 /*break*/, 3];
                        retryCount = 0;
                        _a.label = 1;
                    case 1:
                        if (!(button.length === 0 && retryCount < 10)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.$timeout(500)];
                    case 2:
                        _a.sent();
                        button = angular.element(buttonSelector);
                        retryCount++;
                        return [3 /*break*/, 1];
                    case 3:
                        button.attr("href", this.getPerfStackUrl(presetId));
                        return [2 /*return*/];
                }
            });
        });
    };
    ChartWidgetController.prototype.redrawChart = function (chartId) {
        var _this = this;
        this.$timeout(function () {
            if (_this.novaChartManager.get(chartId)) {
                _this.novaChartManager.get(chartId).draw();
            }
        });
    };
    ChartWidgetController.prototype.getOpids = function (query) {
        var _this = this;
        return this.swisService
            .query(query)
            .then(function (result) { return result.rows; })
            .then(function (result) { return result.map(function (opid) { return _this.swNetObjectOpidConverterService.formatOpid({
            instanceSiteId: opid.InstanceSiteId,
            entityType: opid.EntityType,
            entityIds: [opid.EntityId],
        }); }); });
    };
    ChartWidgetController.prototype.replaceQueryOpid = function (query, opid) {
        var copy = _.clone(query);
        copy.id = copy.id.replace(/^[^-]+/, opid);
        return copy;
    };
    ChartWidgetController.prototype.mapQueriesToEntities = function (metricQueries, opids) {
        var _this = this;
        return _.reduce(opids, function (allQueries, opid) {
            return _.concat(allQueries, _.map(metricQueries, function (query) { return _this.replaceQueryOpid(query, opid); }));
        }, []);
    };
    ChartWidgetController.prototype.getHistoricalMetrics = function (metricQueries) {
        var _this = this;
        var metricsPromise;
        var config = this.settings.chartconfiguration;
        if (config.customEntitiesQuery) {
            metricsPromise = this.getOpids(config.customEntitiesQuery)
                .then(function (opids) { return _this.mapQueriesToEntities(metricQueries, opids); })
                .then(function (mappedQueries) { return _this.swChartMetricsService.getMetrics(mappedQueries, _this.currentTimeframe, null, _.has(_this.$rootScope, "SW.Core.View.LimitationID")
                ? _this.$rootScope.SW.Core.View.LimitationID
                : null); });
        }
        else {
            metricsPromise = this.swChartMetricsService.getMetrics(metricQueries, this.currentTimeframe, null, _.has(this.$rootScope, "SW.Core.View.LimitationID")
                ? this.$rootScope.SW.Core.View.LimitationID
                : null);
        }
        // In case of AVG aggregation, we append the latest detail measurement at the end of metrics array,
        // so that the legends of the charts show non aggregated actual values.
        return metricsPromise.then(function (perfstackMetrics) { return _this.appendLatestDetailMeasurement(perfstackMetrics); });
    };
    ChartWidgetController.prototype.getRealTimeMetrics = function (metricQueries, startTime, count) {
        if (startTime === void 0) { startTime = null; }
        if (count === void 0) { count = 0; }
        var timeFrame = this.timeService.create().start(startTime);
        return this.swChartMetricsService.getRealTimeMetrics(metricQueries, timeFrame, null, count);
    };
    ChartWidgetController.prototype.matchingSeries = function (series, m) {
        var metricId = this.settings.opid + "-" + m.id;
        var seriesId = series && series.id || "";
        return (seriesId.indexOf(m.id) > 0)
            || (seriesId.indexOf(metricId) === 0);
        // fix for multiseries when we have postfix for seriesId
        // for example: seriesId "0_Orion.Nodes_1-Orion.CPUMultiLoad.AvgLoad-CPU3" and metricId "0_Orion.Nodes_1-Orion.CPUMultiLoad.AvgLoad"
    };
    ChartWidgetController.prototype.getExtendedConfig = function (chartConfig) {
        var extendedConfigs = [];
        if (this.settings.chartconfiguration.listView) {
            var selectedId_1 = this.selectedSeriesIds[0];
            var properRadioList = _.find(this.radioList, function (el) { return el.id === selectedId_1; });
            var extendedConfig = properRadioList && properRadioList.extendedConfig;
            if (extendedConfig) {
                return extendedConfig;
            }
        }
        if (chartConfig.relatedMetrics) {
            if (_.isArray(chartConfig.relatedMetrics)) {
                chartConfig.relatedMetrics
                    .forEach(function (relatedMetric) {
                    if (relatedMetric.extendedConfig) {
                        extendedConfigs = extendedConfigs.concat(relatedMetric.extendedConfig);
                    }
                });
            }
            else {
                if (chartConfig.relatedMetrics.extendedConfig) {
                    extendedConfigs = extendedConfigs.concat(chartConfig.relatedMetrics.extendedConfig);
                }
            }
        }
        return (extendedConfigs.length > 0)
            ? extendedConfigs
            : null;
    };
    ChartWidgetController.prototype.mapMetrics = function (perfstackMetrics, chartModelToUpdate, chartConfig, childrenIds) {
        var _this = this;
        var metricsType = this.swChartMetricsService.orionToPerfstack(perfstackMetrics[0].type);
        chartModelToUpdate.plotType = this.getPlotType(metricsType, chartConfig);
        var metrics = this.swChartMetricsService.getMetricsByPlotType(perfstackMetrics, chartModelToUpdate, childrenIds, this.settings.chartconfiguration);
        if (!metrics || !metrics.length) {
            chartModelToUpdate.isEmpty = true;
            this.$log.warn("Metrics for chart '" + chartConfig.title + "' are not accessible.");
            return;
        }
        this.swChartMetricsService.normalizeMetrics(metrics, chartModelToUpdate.plotType);
        // metrics[0].type looks bad but for now we do have an assumption that
        // only one metric type can be in one chart
        var properMarkType = this.getMarkType(metricsType, chartConfig);
        var chart = this.novaChartManager.get(chartConfig.id);
        var plotManager = chart.getPlotManager();
        var metricConfigForRelated = this.getExtendedConfig(chartConfig);
        var activeMetricsConfig = metricConfigForRelated || chartConfig.metrics;
        _.forEach(metrics, function (series) {
            if (chart && !chart.getPlotManager().getPlots().length) {
                var metricType = _this.swChartMetricsService.chartsToPerfstack(series.type);
                var plotConfig = _this.getPlotConfig(chartConfig, plotManager, properMarkType, metricType);
                plotManager.addPlot(plotConfig);
            }
            _.each(activeMetricsConfig, function (m) {
                if (_this.matchingSeries(series, m)) {
                    series.unitLabel = !m.hideUnit ? series.unitLabel : "";
                    series.precision = m.precision || m.precision === 0 ? m.precision : 1;
                    if (m.axisLabels && m.axisLabels.yAxis) {
                        series.yAxisLabel = m.axisLabels.yAxis;
                    }
                    if (m.axisLabels && m.axisLabels.xAxis) {
                        series.xAxisLabel = m.axisLabels.xAxis;
                    }
                    series.config = {
                        type: _this.defineSeriesConfigType(m.type),
                        showNow: m.showNow
                    };
                }
            });
        });
        var otherSeriesIndex;
        // Other series metric should be the last. Comparison with "All others" is a hack
        metrics.forEach(function (metric, index) {
            if (_.includes(metric.displayName, "All others")) {
                metric.config = { type: charts_d3_1.SeriesConfigType.other };
                otherSeriesIndex = index;
            }
        });
        if (otherSeriesIndex) {
            metrics.push.apply(metrics, metrics.splice(otherSeriesIndex, 1));
        }
        return metrics;
    };
    ChartWidgetController.prototype.updateChartWithMetricsData = function (perfstackMetrics, chartConfig, childrenIds) {
        var _this = this;
        if (childrenIds === void 0) { childrenIds = []; }
        var chartModelToUpdate = _.find(this.currentChartsModel, function (m) { return m.id === chartConfig.id; });
        var isEmptyChart = true;
        if (perfstackMetrics && perfstackMetrics[0]) {
            var metrics = this.mapMetrics(perfstackMetrics, chartModelToUpdate, chartConfig, childrenIds);
            var metricsType_1 = this.swChartMetricsService.chartsToPerfstack(metrics[0].type);
            _.forEach(metrics, function (series) {
                if (!_.isEmpty(series.data)) {
                    isEmptyChart = false;
                }
                series.id = _this.encodeSeriesId(series.id);
                _this.novaChartManager.get(chartConfig.id)
                    .addSeries(series, _this.getMarkType(metricsType_1, chartConfig), series.config);
            });
        }
        chartModelToUpdate.isEmpty = isEmptyChart;
    };
    ChartWidgetController.prototype.extractMetricQueries = function (chartConfig, seriesDictionary, config, chartModel) {
        var _this = this;
        var metricQueries;
        if (config.listView) {
            metricQueries = this.getListViewMetrics();
        }
        else {
            var metrics = chartConfig.metrics;
            switch (chartModel.plotType) {
                case charts_d3_1.PlotType.range:
                case charts_d3_1.PlotType.proportional:
                    // basic metrics for range or proportional plot differs from those that are added to chart
                    metricQueries = this.getMetricQueriesFromConfig(metrics);
                    break;
                case charts_d3_1.PlotType.axis:
                    // for axis plot with related metrics we need to restore seriesIds using default strategy
                    if (!chartConfig.relatedMetrics) {
                        metricQueries = this.getMetricQueriesFromConfig(metrics);
                        break;
                    }
                default:
                    metricQueries = _.map(seriesDictionary, function (series) { return ({ id: _this.decodeSeriesId(series.id) }); });
            }
        }
        return metricQueries;
    };
    ChartWidgetController.prototype.applyPlotConfig = function (plot, newConfig) {
        var oldConfig = plot.config();
        var trendsVisibility = oldConfig && oldConfig.trendsConfig && oldConfig.trendsConfig.visible;
        // Applying current visibility state for trends
        if (newConfig.trendsConfig) {
            newConfig.trendsConfig.visible = trendsVisibility;
        }
        plot.config(newConfig);
    };
    ChartWidgetController.prototype.updateChartModel = function () {
        var config = this.settings.chartconfiguration;
        if (!(config.tabsView && config.tabsView.visible)) {
            if (_.isEmpty(this.currentChartsModel)) {
                this.updateGlobalModel();
            }
        }
        else {
            this.updateGlobalModelForTabsView();
        }
    };
    ChartWidgetController.prototype.updateAllTabsModels = function () {
        var _this = this;
        this._selectedTabId = this.defaultTabId;
        var config = this.settings.chartconfiguration;
        _.each(config.charts, function (chartConfig, index) {
            var model = _this.createModel(chartConfig, index);
            var tab = _this.getTab(chartConfig.tabId);
            _this.getTabChartsModel(tab).push(model);
        });
    };
    ChartWidgetController.prototype.updateGlobalModelForTabsView = function () {
        // Initial model update is setting models for each tab. Later, only global model should be updated,
        // to be able to call updateAllSeries method for correct model.
        this.currentChartsModel = [];
        if (!this._selectedTabId) {
            this.updateAllTabsModels();
        }
        var selectedTab = this.getTab(this._selectedTabId);
        // Cloning is not needed, since we change model's `isReady` field,
        // which should be change in model inside the Tab
        this.currentChartsModel = this.getTabChartsModel(selectedTab);
    };
    ChartWidgetController.prototype.getRelatedMetrics = function (relatedMetrics) {
        var _this = this;
        if (relatedMetrics) {
            return this.swWidgetService.getEntityRelationships(this.settings.opid)
                .then(function (relationships) {
                var metrics = _.isArray(relatedMetrics)
                    ? relatedMetrics
                    : [relatedMetrics];
                var childMetricIds = [];
                _.each(metrics, function (childNode) {
                    var childIds = _this.getChildMetrics(childNode, relationships);
                    childMetricIds.push.apply(childMetricIds, childIds);
                });
                return childMetricIds;
            })
                .catch(function (err) {
                _this.$log.warn("Promise rejected: ", err);
            });
        }
        return this.$q.resolve([]);
    };
    ChartWidgetController.prototype.getEntityInfo = function () {
        var _this = this;
        return this.swApi.api(false).one("perfstack")
            .one("entities/" + this.settings.opid + "/")
            .get()
            .then(function (res) {
            var data = res.plain().data;
            var entityData = data && data[0];
            _this.entityDetails = _this.mapEntityDataToListItemObject(entityData);
        })
            .catch(function (err) {
            _this.$log.warn("Promise rejected: ", err);
        });
    };
    ChartWidgetController.prototype.mapEntityDataToListItemObject = function (entityData) {
        if (entityData) {
            return {
                id: entityData.id,
                status: "" + entityData.status,
                statusIconPostfix: entityData.statusIconPostfix,
                description: entityData.description,
                iconName: this.relationInstanceToIconMapper(entityData),
                name: entityData.displayName,
                detailsUrl: entityData.detailsUrl
            };
        }
    };
    ChartWidgetController.prototype.getMarkType = function (metricType, chartConfig) {
        // types that can be defined easily based on chartConfig's plotType only
        // it is much better to define mark type explicitly and not guess it
        switch (chartConfig.plotType) {
            case "barRange":
                return charts_d3_1.MarkType.barRange;
            case "areaRange":
                return charts_d3_1.MarkType.areaRange;
            case "line":
                return charts_d3_1.MarkType.line;
            case "area":
                return charts_d3_1.MarkType.area;
            case "barStacked":
            case "barStackedNormalized":
                return charts_d3_1.MarkType.barStacked;
            case "areaStacked":
                return charts_d3_1.MarkType.areaStacked;
        }
        switch (metricType) {
            case chartMetrics_service_1.PerfstackSeriesType.gauge:
                return charts_d3_1.MarkType.line;
            case chartMetrics_service_1.PerfstackSeriesType.state:
                return charts_d3_1.MarkType.state;
            default:
                this.$log.warn("Can not detect plot type based on metric. Please specify plotType explicitly");
                return charts_d3_1.MarkType.area; // complete hack for DPA, to be removed
        }
    };
    ChartWidgetController.prototype.setupSeriesLimit = function () {
        var _this = this;
        this.swOrionSettingsService
            .getSetting("Web-MaximalNumberOfSeriesInChart", constants_1.default.plotSeriesDefaultLimit)
            .then(function (result) {
            _this.plotSeriesLimit = result;
        });
    };
    ChartWidgetController.prototype.getPlotConfig = function (chartConfig, plotManager, properMarkType, seriesType, plotConfigToExtend) {
        var config = this.settings.chartconfiguration;
        var plotType = this.getPlotType(seriesType, chartConfig);
        var plotConfig = plotConfigToExtend || plotManager.getConfigByType(plotType, properMarkType);
        plotConfig.plotSeriesLimit = this.plotSeriesLimit;
        if (chartConfig.adjustToTimeframe) {
            plotConfig.adjustToTimeframe = chartConfig.adjustToTimeframe;
        }
        var showThresholds = _.get(config, "statistics.thresholds")
            && _.get(chartConfig, "statistics.thresholds") !== false;
        plotConfig.thresholds = {
            visible: showThresholds,
            combined: showThresholds,
            showBottomThreshold: showThresholds,
        };
        plotConfig.trendsConfig = {
            visible: _.get(config, "statistics.trends") && _.get(chartConfig, "statistics.trends") !== false
        };
        plotConfig.percentiles.visible = _.get(config, "statistics.percentiles")
            && _.get(chartConfig, "statistics.percentiles") !== false;
        if (plotConfig.percentiles.visible) {
            plotConfig.percentiles.percentileLevel = config.statistics.percentileValue;
        }
        if (chartConfig.plotType === "barStackedNormalized") {
            plotConfig.percentageProportions = true;
        }
        if (chartConfig.axisLabelVisibility &&
            (_.has(chartConfig.axisLabelVisibility, "yAxis") || _.has(chartConfig.axisLabelVisibility, "xAxis"))) {
            plotConfig.grid.axis.bottom.isLabeled = chartConfig.axisLabelVisibility.xAxis;
            // this won't break anything, since we already do similar stuff by default inside charts-d3
            plotConfig.grid.axis.left.isLabeled
                = plotConfig.grid.axis.right.isLabeled
                    = chartConfig.axisLabelVisibility.yAxis;
        }
        return plotConfig;
    };
    ChartWidgetController.prototype.relationInstanceToIconMapper = function (item) {
        switch (item.instanceType) {
            case "Orion.NPM.Interfaces":
                return "network-interface";
            case "Orion.DPA.DatabaseInstance":
                return "dpa-database";
            default:
                // TODO: think about better defaults
                return "unknownnode";
        }
    };
    ChartWidgetController.prototype.getRelated = function (chart) {
        if (chart && chart.relatedMetrics) {
            return _.isArray(chart.relatedMetrics)
                ? chart.relatedMetrics
                : [chart.relatedMetrics];
        }
        return [];
    };
    ChartWidgetController.prototype.getPerfStackUrl = function (presetId) {
        var _this = this;
        if (!presetId) {
            presetId = "lastHour";
        }
        var urlParts = ["/ui/perfstack/?presetTime=" + presetId];
        var chartsConfig = this.settings.chartconfiguration.charts;
        if (chartsConfig) {
            var opid_1 = this.settings.opid;
            var charts = _.map(chartsConfig, function (chart) { return _this.exportMetrics.length
                ? _.join(_this.exportMetrics)
                : _.join(_.concat(_.map(chart.metrics, function (metric) { return opid_1 + "-" + metric.id; }), _.map(_this.getRelated(chart), function (node) { return opid_1 + "-" + node.id; })), ","); });
            urlParts.push("charts=" + _.join(charts, ";"));
        }
        if (this.settings.chartconfiguration.realTime) {
            urlParts.push("activateFastPolling=true");
        }
        return _.join(urlParts, "&");
    };
    ChartWidgetController.prototype.appendLatestDetailMeasurement = function (perfstackMetrics) {
        var _this = this;
        _.forEach(perfstackMetrics, function (metric) {
            if (metric.latestDetailMeasurement) {
                metric.measurements.push(metric.latestDetailMeasurement);
            }
            if (!_.isEmpty(metric.subMetrics)) {
                _this.appendLatestDetailMeasurement(metric.subMetrics);
            }
        });
        return perfstackMetrics;
    };
    ChartWidgetController = __decorate([
        widget_decorator_1.Widget({
            selector: "swChartWidget",
            template: __webpack_require__(33),
            controllerAs: "chartCtrl",
            settingName: exports.WidgetSettingsName,
            partialTemplates: [{ placeholderClass: "sw-widget__header__custom", directiveName: "swChartWidgetHeader" }],
        }),
        __param(12, inject_1.Inject("viewService")),
        __param(13, inject_1.Inject("swOrionViewService")),
        __param(14, inject_1.Inject("getTextService")),
        __param(16, inject_1.Inject("swTimeService")),
        __param(17, inject_1.Inject("constants")),
        __param(18, inject_1.Inject("swDemoService")),
        __metadata("design:paramtypes", [Object, Function, Function, Object, Object, Object, Object, chartMetrics_service_1.ChartMetricsService, Object, Object, Object, Object, Object, Object, Function, Function, Object, Object, Object])
    ], ChartWidgetController);
    return ChartWidgetController;
}());
exports.ChartWidgetController = ChartWidgetController;


/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return bisectRight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return bisectLeft; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ascending__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__bisector__ = __webpack_require__(21);



var ascendingBisect = Object(__WEBPACK_IMPORTED_MODULE_1__bisector__["a" /* default */])(__WEBPACK_IMPORTED_MODULE_0__ascending__["a" /* default */]);
var bisectRight = ascendingBisect.right;
var bisectLeft = ascendingBisect.left;
/* harmony default export */ __webpack_exports__["c"] = (bisectRight);


/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ascending__ = __webpack_require__(3);


/* harmony default export */ __webpack_exports__["a"] = (function(compare) {
  if (compare.length === 1) compare = ascendingComparator(compare);
  return {
    left: function(a, x, lo, hi) {
      if (lo == null) lo = 0;
      if (hi == null) hi = a.length;
      while (lo < hi) {
        var mid = lo + hi >>> 1;
        if (compare(a[mid], x) < 0) lo = mid + 1;
        else hi = mid;
      }
      return lo;
    },
    right: function(a, x, lo, hi) {
      if (lo == null) lo = 0;
      if (hi == null) hi = a.length;
      while (lo < hi) {
        var mid = lo + hi >>> 1;
        if (compare(a[mid], x) > 0) hi = mid;
        else lo = mid + 1;
      }
      return lo;
    }
  };
});

function ascendingComparator(f) {
  return function(d, x) {
    return Object(__WEBPACK_IMPORTED_MODULE_0__ascending__["a" /* default */])(f(d), x);
  };
}


/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["b"] = pair;
/* harmony default export */ __webpack_exports__["a"] = (function(array, f) {
  if (f == null) f = pair;
  var i = 0, n = array.length - 1, p = array[0], pairs = new Array(n < 0 ? 0 : n);
  while (i < n) pairs[i] = f(p, p = array[++i]);
  return pairs;
});

function pair(a, b) {
  return [a, b];
}


/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__variance__ = __webpack_require__(24);


/* harmony default export */ __webpack_exports__["a"] = (function(array, f) {
  var v = Object(__WEBPACK_IMPORTED_MODULE_0__variance__["a" /* default */])(array, f);
  return v ? Math.sqrt(v) : v;
});


/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__number__ = __webpack_require__(5);


/* harmony default export */ __webpack_exports__["a"] = (function(values, valueof) {
  var n = values.length,
      m = 0,
      i = -1,
      mean = 0,
      value,
      delta,
      sum = 0;

  if (valueof == null) {
    while (++i < n) {
      if (!isNaN(value = Object(__WEBPACK_IMPORTED_MODULE_0__number__["a" /* default */])(values[i]))) {
        delta = value - mean;
        mean += delta / ++m;
        sum += delta * (value - mean);
      }
    }
  }

  else {
    while (++i < n) {
      if (!isNaN(value = Object(__WEBPACK_IMPORTED_MODULE_0__number__["a" /* default */])(valueof(values[i], i, values)))) {
        delta = value - mean;
        mean += delta / ++m;
        sum += delta * (value - mean);
      }
    }
  }

  if (m > 1) return sum / (m - 1);
});


/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(values, valueof) {
  var n = values.length,
      i = -1,
      value,
      min,
      max;

  if (valueof == null) {
    while (++i < n) { // Find the first comparable value.
      if ((value = values[i]) != null && value >= value) {
        min = max = value;
        while (++i < n) { // Compare the remaining values.
          if ((value = values[i]) != null) {
            if (min > value) min = value;
            if (max < value) max = value;
          }
        }
      }
    }
  }

  else {
    while (++i < n) { // Find the first comparable value.
      if ((value = valueof(values[i], i, values)) != null && value >= value) {
        min = max = value;
        while (++i < n) { // Compare the remaining values.
          if ((value = valueof(values[i], i, values)) != null) {
            if (min > value) min = value;
            if (max < value) max = value;
          }
        }
      }
    }
  }

  return [min, max];
});


/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return slice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return map; });
var array = Array.prototype;

var slice = array.slice;
var map = array.map;


/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(start, stop, step) {
  start = +start, stop = +stop, step = (n = arguments.length) < 2 ? (stop = start, start = 0, 1) : n < 3 ? 1 : +step;

  var i = -1,
      n = Math.max(0, Math.ceil((stop - start) / step)) | 0,
      range = new Array(n);

  while (++i < n) {
    range[i] = start + i * step;
  }

  return range;
});


/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["b"] = tickIncrement;
/* harmony export (immutable) */ __webpack_exports__["c"] = tickStep;
var e10 = Math.sqrt(50),
    e5 = Math.sqrt(10),
    e2 = Math.sqrt(2);

/* harmony default export */ __webpack_exports__["a"] = (function(start, stop, count) {
  var reverse,
      i = -1,
      n,
      ticks,
      step;

  stop = +stop, start = +start, count = +count;
  if (start === stop && count > 0) return [start];
  if (reverse = stop < start) n = start, start = stop, stop = n;
  if ((step = tickIncrement(start, stop, count)) === 0 || !isFinite(step)) return [];

  if (step > 0) {
    start = Math.ceil(start / step);
    stop = Math.floor(stop / step);
    ticks = new Array(n = Math.ceil(stop - start + 1));
    while (++i < n) ticks[i] = (start + i) * step;
  } else {
    start = Math.floor(start * step);
    stop = Math.ceil(stop * step);
    ticks = new Array(n = Math.ceil(start - stop + 1));
    while (++i < n) ticks[i] = (start - i) / step;
  }

  if (reverse) ticks.reverse();

  return ticks;
});

function tickIncrement(start, stop, count) {
  var step = (stop - start) / Math.max(0, count),
      power = Math.floor(Math.log(step) / Math.LN10),
      error = step / Math.pow(10, power);
  return power >= 0
      ? (error >= e10 ? 10 : error >= e5 ? 5 : error >= e2 ? 2 : 1) * Math.pow(10, power)
      : -Math.pow(10, -power) / (error >= e10 ? 10 : error >= e5 ? 5 : error >= e2 ? 2 : 1);
}

function tickStep(start, stop, count) {
  var step0 = Math.abs(stop - start) / Math.max(0, count),
      step1 = Math.pow(10, Math.floor(Math.log(step0) / Math.LN10)),
      error = step0 / step1;
  if (error >= e10) step1 *= 10;
  else if (error >= e5) step1 *= 5;
  else if (error >= e2) step1 *= 2;
  return stop < start ? -step1 : step1;
}


/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(values) {
  return Math.ceil(Math.log(values.length) / Math.LN2) + 1;
});


/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(values, valueof) {
  var n = values.length,
      i = -1,
      value,
      min;

  if (valueof == null) {
    while (++i < n) { // Find the first comparable value.
      if ((value = values[i]) != null && value >= value) {
        min = value;
        while (++i < n) { // Compare the remaining values.
          if ((value = values[i]) != null && min > value) {
            min = value;
          }
        }
      }
    }
  }

  else {
    while (++i < n) { // Find the first comparable value.
      if ((value = valueof(values[i], i, values)) != null && value >= value) {
        min = value;
        while (++i < n) { // Compare the remaining values.
          if ((value = valueof(values[i], i, values)) != null && min > value) {
            min = value;
          }
        }
      }
    }
  }

  return min;
});


/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__min__ = __webpack_require__(30);


/* harmony default export */ __webpack_exports__["a"] = (function(matrix) {
  if (!(n = matrix.length)) return [];
  for (var i = -1, m = Object(__WEBPACK_IMPORTED_MODULE_0__min__["a" /* default */])(matrix, length), transpose = new Array(m); ++i < m;) {
    for (var j = -1, n, row = transpose[i] = new Array(n); ++j < n;) {
      row[j] = matrix[j][i];
    }
  }
  return transpose;
});

function length(d) {
  return d.length;
}


/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-chart-entity-view-item> <div class=sw-chart-entity-view-item_icon> <xui-icon icon={{::item.iconName}} status={{::item.statusIconPostfix}}></xui-icon> </div> <div class=sw-chart-entity-view-item_description> <a class=sw-chart-entity-view-item_title ng-click=$event.stopPropagation() ng-href={{::item.detailsUrl}}> {{::item.name}} </a> <span class=\"sw-chart-entity-view-item_subtitle xui-text-dscrn\">{{::item.description}}</span> </div> </div> ";

/***/ }),
/* 33 */
/***/ (function(module, exports) {

module.exports = "<div class=entity-view-container ng-if=chartCtrl.entityDetails> <div class=sw-chart-entity-view-item> <div class=sw-chart-entity-view-item_icon> <xui-icon icon={{::chartCtrl.entityDetails.iconName}} status={{::chartCtrl.entityDetails.statusIconPostfix}}></xui-icon> </div> <div class=sw-chart-entity-view-item_description> <span class=\"sw-chart-entity-view-item_title sw-chart-entity-view-item_title--bold\"> {{::chartCtrl.entityDetails.name}} </span> <span class=\"sw-chart-entity-view-item_subtitle xui-text-dscrn\">{{::chartCtrl.entityDetails.description}}</span> </div> </div> </div> <xui-tabs selected-tab-id=chartCtrl.selectedTabId class=sw-chart ng-if=chartCtrl.isTabsView()> <xui-tab ng-repeat=\"tab in chartCtrl.settings.chartconfiguration.tabsView.tabsList\" tab-id={{tab.id}} tab-title={{tab.title}}> <xui-chart-time model=chartCtrl.getTabChartsModel(tab) legend=bigNumber legend-orientation=right preset-id=chartCtrl.timeframePresetId on-timeframe-changed=chartCtrl.onTimeframeChanged(timeframe) chart-group-id=chartCtrl.widgetId should-fit-legend=true time-frame-picker-config=chartCtrl.timeframePickerConfig ng-class=\"{'sw-chart__graph--day-mode': chartCtrl.timeframePickerConfig.mode === 'day'}\"> </xui-chart-time> </xui-tab> </xui-tabs> <div class=sw-chart ng-if=chartCtrl.isSimpleView()> <div class=sw-chart__graph ng-if=chartCtrl.chartModelIsReady> <xui-chart-time model=chartCtrl.currentChartsModel legend=bigNumber legend-orientation=right preset-id=chartCtrl.timeframePresetId on-timeframe-changed=chartCtrl.onTimeframeChanged(timeframe) chart-group-id=chartCtrl.widgetId should-fit-legend=true time-frame-picker-config=chartCtrl.timeframePickerConfig time-frame-picker-range=chartCtrl.timeframePickerRange ng-class=\"{'sw-chart__graph--day-mode': chartCtrl.timeframePickerConfig.mode === 'day'}\"> </xui-chart-time> </div> </div> <div class=\"entity-view-container sw-chart\" ng-if=chartCtrl.isListView()> <xui-listview class=sw-chart-entity-view ng-click=chartCtrl.onSelectChanged() items-source=chartCtrl.radioList template-fn=chartCtrl.templateFn stripe=false row-padding=compact selection-mode=radio selection-property=id selection=chartCtrl.selectedSeriesIds></xui-listview> <div class=sw-chart__graph ng-if=chartCtrl.selectedModelisReady> <xui-chart-time model=chartCtrl.currentChartsModel legend=bigNumber legend-orientation=right preset-id=chartCtrl.timeframePresetId on-timeframe-changed=chartCtrl.onTimeframeChanged(timeframe) chart-group-id=chartCtrl.widgetId should-fit-legend=true time-frame-picker-config=chartCtrl.timeframePickerConfig ng-class=\"{'sw-chart__graph--day-mode': chartCtrl.timeframePickerConfig.mode === 'day'}\"> </xui-chart-time> </div> </div> ";

/***/ }),
/* 34 */
/***/ (function(module, exports) {

module.exports = "<div class=\"xui-flex-container xui-flex--nowrap edit-popover-container\"> <div class=\"edit-popover-container__presets xui-padding-lg\"> <h5 _t>quick picks</h5> <ul class=edit-popover-container__presets-list> <li class=\"xui-padding-sm edit-popover-container__presets-list-item\" ng-repeat=\"pick in chart.returnQuickPics()\" ng-click=chart.setPercentileValue(pick)> <a class=edit-popover-container__list-item-href>{{pick}}</a> <span ng-show=$first _t>(default)</span> </li> </ul> </div> <div class=\"edit-popover-specific-num xui-flex-item-grow xui-padding-lg\"> <h5 _t>custom</h5> <xui-textbox ng-model=chart.settings.chartconfiguration.statistics.percentileValue type=number></xui-textbox> </div> </div> ";

/***/ }),
/* 35 */
/***/ (function(module, exports) {

module.exports = "<div class=edit-dialog> <div class=edit-dialog-statistic ng-show=chart.allowStatistic ng-class=\"{'section-disabled': !chart.statisticGroupVisible}\"> <div class=edit-dialog-statistic__component> <h5 class=edit-dialog-statistic__component-title _t>Statistical lines</h5> <span class=edit-dialog-statistic__component-switch> <xui-switch ng-model=chart.statisticGroupVisible ng-change=chart.onStatisticValueChange(chart.settings.chartconfiguration.statistics)> </xui-switch> </span> </div> <div class=edit-dialog-statistic__group ng-show=chart.statisticGroupVisible> <xui-switch ng-show=chart.allowThresholds ng-model=chart.settings.chartconfiguration.statistics.thresholds _t>Thresholds</xui-switch> <xui-switch ng-model=chart.settings.chartconfiguration.statistics.trends _t>Trend</xui-switch> <xui-switch ng-model=chart.settings.chartconfiguration.statistics.percentiles> <xui-popover xui-popover-trigger=click xui-popover-placemaent=right xui-popover-content=chart.getPercentilePopoverTemplate()> <a _t>95th Percentile</a> </xui-popover> </xui-switch> </div> </div> <xui-divider></xui-divider> </div> ";

/***/ }),
/* 36 */
/***/ (function(module, exports) {

module.exports = "<a ng-repeat=\"link in ctrl.customLinks() | orderBy: 'position' | reverse \" ng-href={{link.href}} target={{link.target}} class=\"EditResourceButton sw-btn-resource sw-btn {{link.customCss}}\"> <span class=sw-btn-c> <span class=sw-btn-t>{{link.caption}}</span> </span> </a>";

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var charts_d3_1 = __webpack_require__(6);
var widget_decorator_1 = __webpack_require__(0);
var perfstackWidget_service_1 = __webpack_require__(14);
var chartMetrics_service_1 = __webpack_require__(10);
exports.WidgetSettingsName = "swPerfstackWidgetSettings";
var PerfstackWidgetController = /** @class */ (function () {
    /** @ngInject */
    PerfstackWidgetController.$inject = ["$log", "$q", "$timeout", "pollingService", "novaChartManager", "swPerfstackDialogService", "swPerfstackProjectService", "getTextService", "swWidgetService", "$element", "swAuthService", "swPerfstackWidgetService", "swChartMetricsService", "xuiTimeframeService", "swDemoService"];
    function PerfstackWidgetController($log, $q, $timeout, pollingService, novaChartManager, swPerfstackDialogService, swPerfstackProjectService, getTextService, swWidgetService, $element, swAuthService, swPerfstackWidgetService, swChartMetricsService, xuiTimeframeService, swDemoService) {
        var _this = this;
        this.$log = $log;
        this.$q = $q;
        this.$timeout = $timeout;
        this.pollingService = pollingService;
        this.novaChartManager = novaChartManager;
        this.swPerfstackDialogService = swPerfstackDialogService;
        this.swPerfstackProjectService = swPerfstackProjectService;
        this.getTextService = getTextService;
        this.swWidgetService = swWidgetService;
        this.$element = $element;
        this.swAuthService = swAuthService;
        this.swPerfstackWidgetService = swPerfstackWidgetService;
        this.swChartMetricsService = swChartMetricsService;
        this.xuiTimeframeService = xuiTimeframeService;
        this.swDemoService = swDemoService;
        this.$onInit = function () {
            _this.widgetId = _.uniqueId();
            _this.timeframePickerConfig = {};
            _this.isTimeFrameChanged = false;
            _this.legendSettings = {
                showSourceName: true
            };
            _this.loggedUser = _this.swAuthService.getUser();
            _this.initialTimeframe = _this.xuiTimeframeService.getTimeframeByPresetId("last7Days");
        };
        this.customPresets = {
            last10Minutes: {
                name: "Last 10 Minutes",
                startDatetimePattern: { minutes: -10 },
                endDatetimePattern: {}
            },
            lastHour: this.xuiTimeframeService.getDefaultPresets().lastHour,
            last2Hours: {
                name: "Last 2 Hours",
                startDatetimePattern: { hours: -2 },
                endDatetimePattern: {}
            },
            last12Hours: this.xuiTimeframeService.getDefaultPresets().last12Hours,
            last24Hours: this.xuiTimeframeService.getDefaultPresets().last24Hours,
            last5Days: this.xuiTimeframeService.getDefaultPresets().last5Days,
            last7Days: this.xuiTimeframeService.getDefaultPresets().last7Days,
            last30Days: this.xuiTimeframeService.getDefaultPresets().last30Days,
            last90Days: {
                name: "Last 90 Days",
                startDatetimePattern: { days: -90 },
                endDatetimePattern: {}
            }
        };
        this._t = this.getTextService;
        this.initialTimeframeDeferred = this.$q.defer();
        this.onLoad = function (config) {
            _this.config = config;
            _this.config.isBusy(true);
            // Extend presets, add several from PerfStack
            if (_this.isReport()) {
                _this.timeframePickerConfig.customPresets = _this.customPresets;
                _this.xuiTimeframeService.extendCurrentPresets(_this.customPresets);
            }
            _this.pollingService.register(_this.reloadProject);
        };
        this.onUnload = function () {
            _this.pollingService.unregister(_this.reloadProject);
        };
        this.onSettingsChanged = function (settings, afterSave) {
            _this.config.settings = settings;
            _this.$timeout(function () {
                if (!_this.timeframePickerConfig.defaultRange) {
                    // Request for a PerfStack project presetTime
                    _this.getProject(_this.settings.projectid).then(function (project) {
                        _this.settings.perfStackPresetTime = project.presetTime;
                        _this.timeframePickerConfig.defaultRange = _this.currentTimeframe;
                        _this.reloadProject();
                    }).catch(function (reason) {
                        _this.showEmptyWidget();
                        _this.config.isBusy(false);
                        _this.$log.warn("Could not display project with id: ", _this.settings.projectid, reason);
                    });
                }
                else {
                    _this.reloadProject();
                }
            });
        };
        this.onTimeframeChanged = function (timeframe) {
            if (!timeframe) {
                return;
            }
            _this.userSettings.timeframe = timeframe;
            if (_this.initialTimeframeDeferred) {
                _this.initialTimeframeDeferred.resolve();
                _this.initialTimeframeDeferred = undefined;
            }
            else {
                _this.updateAllSeries();
                // show info message if presets differs
                _this.isTimeFrameChanged = timeframe.selectedPresetId === _this.currentTimeframe.selectedPresetId ? false : true;
                // block sending post request saving settings to DB in reports
                if (_this.isReport()) {
                    return;
                }
                // block sending post request saving settings to DB in demo mode
                if (_this.swDemoService.isDemoMode()) {
                    return;
                }
                _this.config.saveUserProperties(_this.userSettings);
            }
        };
        // Check whether Perfstack project is on a report
        this.isReport = function () {
            return parseInt(_this.config.id, 10) < 0;
        };
        // Show info message to adjust time frame directly from PerfStack project (on a report)
        this.showPerfStackInfoMessage = function () {
            return _this.isReport() && _this.isTimeFrameChanged && typeof (_this.model) !== "undefined";
        };
        this.userHasRightsForWidget = function () {
            return _.includes(_this.loggedUser.roles, "admin") ||
                _.includes(_this.loggedUser.permissions, "customize");
        };
        this.reloadProject = function () {
            var model = {};
            _this.config.isBusy(true);
            _this.getProject(_this.settings.projectid).then(function (project) {
                _this.settings.perfStackPresetTime = project.presetTime;
                model.project = project;
            }).then(function () {
                return _this.prepareChartModels(model);
            }).then(function () {
                if (!model.charts.length) {
                    throw "Project doesn't contain any charts supported by PerfStack widget";
                }
                return _this.prepareInitialMetrics(model);
            }).then(function () {
                if (!model.charts.length) {
                    _this.hideWidget();
                    throw "Project doesn't contain any metrics for nodes permitted for user/view";
                }
                return _this.setModel(model);
            }).catch(function (reason) {
                _this.showEmptyWidget();
                _this.$log.warn("Could not display project with id: ", _this.settings.projectid, reason);
            }).finally(function () {
                _this.config.isBusy(false);
            });
        };
        this.hideWidget = function () {
            return _this.swWidgetService.hideParentWidget(_this.$element);
        };
        this.showEmptyWidget = function () {
            if (!_this.userHasRightsForWidget()) {
                _this.hideWidget();
            }
            else {
                _this.model = undefined;
                _this.config.title(_this._t("PerfStack Widget"));
            }
            _this.model = undefined;
            _this.config.title(_this._t("PerfStack Widget"));
        };
        this.processMetrics = function (metrics) {
            var results = _.map(_.filter(metrics), _this.processMetric);
            return _this.swChartMetricsService.normalizeMetrics(_.flatten(results));
        };
        this.processMetric = function (metric) {
            // make the perfstack widget consistent with charts v3
            _this.addLatestResultToMeasurements(metric);
            if (metric.type === 3) {
                return [_this.swChartMetricsService.metricDtoToEventSeries(metric)];
            }
            else if (!metric.subMetrics || !metric.subMetrics.length) {
                return [_this.swChartMetricsService.metricDtoToMetricSeries(metric)];
            }
            var metricType = _this.swChartMetricsService.orionToCharts(metric.type);
            var subSeries = _.map(metric.subMetrics, function (subMetric) {
                _this.addLatestResultToMeasurements(subMetric);
                var sub = _this.swChartMetricsService.metricDtoToMetricSeries(subMetric);
                sub.type = metricType;
                return sub;
            });
            return subSeries;
        };
        this.createChartModel = function (chartDef) {
            var metricQueries = _.map(chartDef.metricDefinitions, function (metricDef) { return metricDef.query; });
            var plotConfig = chartDef.metricDefinitions[0].plotDefinition.config;
            return _this.swChartMetricsService.createModel({
                isEmpty: false,
                legendOrientation: "right",
                legendType: "bigNumber",
                metricQueries: metricQueries,
                plotConfig: plotConfig
            }, _this.initialTimeframeDeferred, _this.addSeries);
        };
        this.updateAllSeries = function () {
            if (!_this.model || !_this.model.charts) {
                return;
            }
            _.forEach(_this.model.charts, function (chart) {
                var chartInstance = _this.novaChartManager.get(chart.id);
                if (!chartInstance) {
                    return;
                }
                var series = chartInstance.series();
                if (!_.values(series).length) {
                    return;
                }
                _this.getMetrics(chart.metricQueries)
                    .then(function (metrics) {
                    if (!metrics.length) {
                        chart.isEmpty = true;
                        return;
                    }
                    _.forEach(metrics, function (metric) {
                        series[metric.id].data = metric.data;
                    });
                    chart.isEmpty = _.every(metrics, function (metric) { return _.isEmpty(metric.data); });
                })
                    .then(function () { return _this.draw(chart.id); });
            });
        };
        this.draw = function (chartId) {
            return _this.$timeout(function () {
                var chart = _this.novaChartManager.get(chartId);
                if (chart) {
                    chart.draw();
                }
            });
        };
        this.getMarkType = function (metricType) {
            switch (metricType) {
                case chartMetrics_service_1.PerfstackSeriesType.gauge:
                    return charts_d3_1.MarkType.line;
                case chartMetrics_service_1.PerfstackSeriesType.state:
                    return charts_d3_1.MarkType.state;
                case chartMetrics_service_1.PerfstackSeriesType.multi:
                    return charts_d3_1.MarkType.barStacked;
                case chartMetrics_service_1.PerfstackSeriesType.dpaWaitTime:
                    return charts_d3_1.MarkType.barStacked;
                default:
                    _this.$log.warn("Metric type " + metricType + " is not supported.");
                    return;
            }
        };
        this.addSeries = function (chartModel) {
            var metrics = chartModel.initialSeries;
            var chart = _this.novaChartManager.get(chartModel.id);
            _.forEach(metrics, function (series) {
                var plots = chart.getPlotManager().getPlots();
                if (!plots.length) {
                    var plot = chart
                        .getPlotManager()
                        .getConfigByType(chartModel.plotConfig.plotType, chartModel.plotConfig.markType);
                    plot.adjustToTimeframe = true;
                    plot.plotSeriesLimit = PerfstackWidgetController_1.MaxSeriesOnChart;
                    chart.getPlotManager().addPlot(plot);
                }
                chart.addSeries(series, chartModel.plotConfig.markType);
            });
            _this.draw(chartModel.id);
        };
        this.addLatestResultToMeasurements = function (metric) {
            if (metric.latestDetailMeasurement) {
                metric.measurements.push(metric.latestDetailMeasurement);
            }
        };
    }
    PerfstackWidgetController_1 = PerfstackWidgetController;
    Object.defineProperty(PerfstackWidgetController.prototype, "settings", {
        get: function () {
            return this.config && this.config.settings;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PerfstackWidgetController.prototype, "userSettings", {
        get: function () {
            return this.settings.userSettings || {};
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PerfstackWidgetController.prototype, "currentTimeframe", {
        get: function () {
            this.preprocessSettings();
            var perfStackPresetTime = this.settings.perfStackPresetTime;
            var userSettingsTimeFrame = this.userSettings.timeframe;
            // Use PerfStack timeFrame for reports
            if (this.isReport()) {
                if (typeof perfStackPresetTime !== "undefined") {
                    var perfStackTimeFrame = this.xuiTimeframeService.getTimeframeByPresetId(perfStackPresetTime);
                    return perfStackTimeFrame;
                }
                return this.initialTimeframe;
            }
            return userSettingsTimeFrame || this.initialTimeframe;
        },
        enumerable: true,
        configurable: true
    });
    PerfstackWidgetController.prototype.showSelectProjectDialog = function () {
        var _this = this;
        this.swPerfstackDialogService
            .showProjectSelectDialog()
            .then(function (project) {
            if (!project) {
                return;
            }
            _this.settings.projectid = project.id;
            _this.settings.title = project.name;
            _this.config.saveSettings();
        });
    };
    PerfstackWidgetController.prototype.isEmptyVisible = function () {
        return !this.config.isBusy() && !this.model;
    };
    PerfstackWidgetController.prototype.onDismissInfoMessage = function () {
        this.isTimeFrameChanged = false;
    };
    PerfstackWidgetController.prototype.preprocessSettings = function () {
        var timeframe = this.userSettings.timeframe;
        if (timeframe && !moment.isDate(timeframe.startDatetime)) {
            if (timeframe.selectedPresetId) {
                var preset = this.xuiTimeframeService.getTimeframeByPresetId(timeframe.selectedPresetId);
                timeframe.startDatetime = preset.startDatetime;
                timeframe.endDatetime = preset.endDatetime;
            }
            else {
                timeframe.startDatetime = new Date(timeframe.startDatetime.toString());
                timeframe.endDatetime = new Date(timeframe.endDatetime.toString());
            }
        }
    };
    PerfstackWidgetController.prototype.getProject = function (projectId) {
        if (!projectId) {
            return this.$q.reject("No projectId selected");
        }
        return this.swPerfstackProjectService.getProjects(projectId).then(function (result) {
            if (result.items.length) {
                return result.items[0];
            }
            throw "Got empty result from PerfStack API";
        });
    };
    PerfstackWidgetController.prototype.prepareChartDefinitions = function (model) {
        model.chartsDefinition = _.map(model.project.charts, function (metrics) {
            var definition = {
                metricDefinitions: _.map(metrics, function (metric) { return ({
                    query: { id: metric }
                }); })
            };
            return definition;
        });
    };
    PerfstackWidgetController.prototype.preparePlotDefinitions = function (model) {
        var _this = this;
        var promises = [];
        _.forEach(model.chartsDefinition, function (definition) {
            _.forEach(definition.metricDefinitions, function (metric) {
                var promise = _this.swPerfstackWidgetService
                    .getPlotDefinitionForPerfstackMetricId(metric.query.id)
                    .then(function (plotDefinition) {
                    if (plotDefinition) {
                        metric.plotDefinition = plotDefinition;
                    }
                });
                promises.push(promise);
            });
        });
        return this.$q.all(promises);
    };
    PerfstackWidgetController.prototype.removeChartsWithoutPlotDefinition = function (model) {
        var _this = this;
        var chartDefs = [];
        _.forEach(model.chartsDefinition, function (chart) {
            chart.metricDefinitions = _.filter(chart.metricDefinitions, function (metric) {
                if (!metric.plotDefinition) {
                    _this.$log.debug("Metric id " + metric.query.id + " is not supported by perfstack widget"
                        + ", metric will not be displayed.");
                    return false;
                }
                return true;
            });
        });
        model.chartsDefinition = _.filter(model.chartsDefinition, function (chart) { return !!chart.metricDefinitions.length; });
    };
    PerfstackWidgetController.prototype.unstackCharts = function (model) {
        var unstacked = [];
        var addDefinition = function (metrics) {
            unstacked.push({
                metricDefinitions: metrics
            });
        };
        _.forEach(model.chartsDefinition, function (chartDef) {
            var byPlotType = _.groupBy(chartDef.metricDefinitions, function (metricDef) { return metricDef.plotDefinition.config.plotType; });
            _.forOwn(byPlotType, function (metricDefs) {
                var plotDefinition = metricDefs[0].plotDefinition;
                if (!plotDefinition.stackable) {
                    _.forEach(metricDefs, function (metricDef) { return addDefinition([metricDef]); });
                }
                else {
                    var chunks = _.chunk(metricDefs, PerfstackWidgetController_1.MaxSeriesOnChart);
                    _.forEach(chunks, function (chunk) { return addDefinition(chunk); });
                }
            });
        });
        model.chartsDefinition = unstacked;
    };
    PerfstackWidgetController.prototype.prepareChartModels = function (model) {
        var _this = this;
        this.prepareChartDefinitions(model);
        return this.preparePlotDefinitions(model).then(function () {
            _this.removeChartsWithoutPlotDefinition(model);
            _this.unstackCharts(model);
            _this.createChartModels(model);
        });
    };
    PerfstackWidgetController.prototype.createChartModels = function (model) {
        var charts = _.map(model.chartsDefinition, this.createChartModel);
        model.charts = charts;
    };
    PerfstackWidgetController.prototype.prepareInitialMetrics = function (model) {
        var _this = this;
        var setInitialMetric = function (chart) {
            return _this.getMetrics(chart.metricQueries).then(function (metrics) {
                chart.initialSeries = _.filter(metrics);
            });
        };
        var promises = _.map(model.charts, setInitialMetric);
        return this.$q.all(promises).then(function () {
            model.charts = _.filter(model.charts, function (chart) { return !!chart.initialSeries.length; });
        });
    };
    /**
     * Set model to undefined and schedule model change in the next digest,
     * forcing angular to re-create the xui-chart-time component with the new model.
     */
    PerfstackWidgetController.prototype.setModel = function (model) {
        var _this = this;
        this.model = undefined;
        return this.$timeout(function () {
            _this.model = model;
        });
    };
    PerfstackWidgetController.prototype.getMetrics = function (metricQueries) {
        var timeframe = this.swChartMetricsService.convertTimeFrame(this.currentTimeframe);
        return this.swChartMetricsService
            .getMetrics(metricQueries, timeframe, 1, this.config.settings.limitations)
            .then(this.processMetrics);
    };
    PerfstackWidgetController.MaxSeriesOnChart = 17;
    PerfstackWidgetController = PerfstackWidgetController_1 = __decorate([
        widget_decorator_1.Widget({
            selector: "swPerfstackWidget",
            template: __webpack_require__(38),
            controllerAs: "ctrl",
            settingName: exports.WidgetSettingsName,
            partialTemplates: [{ placeholderClass: "sw-widget__header__custom", directiveName: "swPerfstackHeader" }]
        }),
        __metadata("design:paramtypes", [Object, Function, Function, Object, Object, Object, Object, Function, Object, Object, Object, perfstackWidget_service_1.PerfstackWidgetService,
            chartMetrics_service_1.ChartMetricsService, Object, Object])
    ], PerfstackWidgetController);
    return PerfstackWidgetController;
    var PerfstackWidgetController_1;
}());
exports.PerfstackWidgetController = PerfstackWidgetController;


/***/ }),
/* 38 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-perfstack-widget> <sw-perfstack-empty ng-if=ctrl.isEmptyVisible() on-click=ctrl.showSelectProjectDialog()> </sw-perfstack-empty> <xui-message ng-if=ctrl.showPerfStackInfoMessage() type=info allow-dismiss=true on-dismiss=ctrl.onDismissInfoMessage()> <div style=margin-top:1.45px> <span _t>Changes were not saved. To adjust the time frame, modify</span> <a ng-href=/ui/perfstack/{{ctrl.model.project.id}} class=sw-link style=text-decoration:none target=_blank> '{{ctrl.model.project.name}}' </a> <span _t>PerfStack project.</span> </div> </xui-message> <xui-chart-time ng-if=ctrl.model.charts.length model=ctrl.model.charts legend=bigNumber legend-settings=ctrl.legendSettings legend-orientation=right on-timeframe-changed=ctrl.onTimeframeChanged(timeframe) time-frame-picker-config=ctrl.timeframePickerConfig chart-group-id=ctrl.widgetId should-fit-legend=true> </xui-chart-time> </div> ";

/***/ }),
/* 39 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-perfstack-header> <a ng-if=ctrl.projectId() ng-href=/ui/perfstack/{{ctrl.projectId()}} class=\"sw-perfstack-header__open sw-btn-resource sw-btn\"> <span class=sw-btn-c> <span class=sw-btn-t _t>Open in PerfStack</span> </span> </a> </div> ";

/***/ }),
/* 40 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-perfstack-project-selector> <xui-grid template-url=[widgets]:/components/perfstack/perfstackProjectSelector/perfstackProjectSelector-item.html items-source=ctrl.items selection-mode=radio selection=ctrl.selection pagination-data=ctrl.pagination sorting-data=ctrl.sorting on-search=ctrl.onSearch(item) on-pagination-change=ctrl.onSearch() on-sorting-change=ctrl.onSearch() on-clear=ctrl.onClear()> </xui-grid> </div> ";

/***/ }),
/* 41 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-perfstack-empty> <xui-button display-style=primary tool-tip=\"_t(Load PerfStack project for this widget)\" _ta ng-click=onClick() _t> Load PerfStack project </xui-button> </div> ";

/***/ }),
/* 42 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-perfstack-widget-settings> <sw-edit-dialog-section-button title=\"_t(PerfStack Project)\" subtitle=\"_t(Choose a PerfStack project to display)\" icon=performance click=vmPerfstackSettings.onChooseProject() _ta>({{vmPerfstackSettings.settings.title}})</sw-edit-dialog-section-button> </div> ";

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "swListWidgetSettings";
var ListController = /** @class */ (function () {
    /** @ngInject */
    ListController.$inject = ["$log", "pollingService"];
    function ListController($log, pollingService) {
        var _this = this;
        this.$log = $log;
        this.pollingService = pollingService;
        this.$onInit = function () {
            _this.listData = [{ name: "foo" }, { name: "jeff" }, { name: "bar" }];
            _this.filterData = [{ value: 1, icon: "status_down" },
                { value: 1, icon: "status_up" },
                { value: 1, icon: "status_unknown" }];
            _this.paginationData = {
                page: 1,
                pageSize: 5,
                total: 3
            };
            _this.sortingData = {
                sortableColumns: _this.sortColumns,
                sortBy: _this.sortColumns[0],
                direction: "desc"
            };
            _this.gridOptions = {
                hideSearch: false,
                pagerAdjacent: 0
            };
            _this.noItemState = {
                compactMode: true,
                description: "There are no data to show. Great job.",
                image: "no-data-to-show"
            };
            _this.searchValue = "";
            _this.style = "sw-list";
            _this.loaded = true;
        };
        ////////mock data
        this.sortColumns = [
            { label: "Status", id: "[1]" },
            { label: "Related Node", id: "[2]" }
        ];
        this.mockData = [{ name: "foo", icon: "status_down" },
            { name: "jeff", icon: "status_up" },
            { name: "bar", icon: "status_unknown" }];
        this.getTotal = function () {
            return _this.mockData.length;
        };
        ////////mock data
        this.onLoad = function (config) {
            _this.config = config;
            _this.settings = config.settings;
            _this.pollingService.register(_this.refresh, true);
        };
        this.refresh = function () {
            _this.listData = _this.mockData;
        };
        this.onSettingsChanged = function (settings) {
            _this.settings = settings;
        };
    }
    ListController = __decorate([
        widget_decorator_1.Widget({
            selector: "swListWidget",
            template: __webpack_require__(44),
            controllerAs: "listCtrl",
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], ListController);
    return ListController;
}());
exports.ListController = ListController;


/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-list> <sw-all-alerts items-source=listCtrl.listData filter-source=listCtrl.filterData active-filters=listCtrl.activeFilters class-css=listCtrl.style show-filters=true filter-change=\"listCtrl.onChange('filter')\" template-url=[widgets]:/components/list/list-item-template.html pagination-data=listCtrl.paginationData sorting-data=listCtrl.sortingData on-pagination-change=\"listCtrl.onPageChange(page, pageSize, total)\" on-sorting-change=\"listCtrl.onSortChange(oldValue, newValue)\" on-search=\"listCtrl.onSearch(item, cancellation)\" on-clear=listCtrl.cancelSearch() options=listCtrl.gridOptions track-by=name row-padding=listCtrl.padding loaded=listCtrl.loaded empty-data=listCtrl.noItemState> </sw-all-alerts> </div> ";

/***/ }),
/* 45 */
/***/ (function(module, exports) {

module.exports = "<xui-switch ng-model=list.settings.showfilters _t>Severity Summary</xui-switch> <div xui-bind-html-compile=list.pluginText()></div> ";

/***/ }),
/* 46 */
/***/ (function(module, exports) {

module.exports = "<div> <xui-textbox ng-model=list.settings.swql caption=SWQL> </xui-textbox> </div> ";

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "swTileWidgetSettings";
var TileController = /** @class */ (function () {
    /** @ngInject */
    TileController.$inject = ["$log", "$scope", "$interval", "getTextService", "swTileService", "statusInfoService", "siteFilterService"];
    function TileController($log, $scope, $interval, getTextService, swTileService, statusInfoService, siteFilterService) {
        var _this = this;
        this.$log = $log;
        this.$scope = $scope;
        this.$interval = $interval;
        this.getTextService = getTextService;
        this.swTileService = swTileService;
        this.statusInfoService = statusInfoService;
        this.siteFilterService = siteFilterService;
        this._t = this.getTextService;
        this.statusInfo = [];
        this.showAlerts = true;
        this.busyMessage = this._t("Loading data, please wait...");
        this.isRefreshPaused = false;
        this.onLoad = function (config) {
            var tileRefreshInterval = 30;
            _this.config = config;
            _this.settings = config.settings;
            _this.config.isBusy(true, _this.busyMessage);
            _this.initStatusInfo();
            _this.siteFilterService.onSiteFilterChanged(_this.siteFilterChanged);
            _this.swTileService
                .getRefreshInterval().then(function (interval) {
                if (interval) {
                    tileRefreshInterval = interval;
                }
                _this.refreshTask = _this.$interval(function () {
                    if (!_this.isRefreshPaused) {
                        try {
                            _this.onChange(false);
                        }
                        catch (exception) {
                            _this.$log.error(exception);
                        }
                    }
                }, tileRefreshInterval * 1000);
                return true;
            });
        };
        this.initStatusInfo = function () {
            _this.statusInfoService.getStatusData()
                .then(function (statusInfo) {
                _this.statusInfo = statusInfo;
                _this.onChange(true);
            })
                .catch(function (error) {
                _this.$log.error(error);
            });
        };
        this.siteFilterChanged = function () {
            _this.onChange(true);
        };
        this.isGroupFilterApplied = function () {
            return _this.settings.filter && _this.settings.filter.groupFilter.length > 0
                && _this.settings.tiletype === "CustomBasic";
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            if (_this.siteFilterService.filterSettingsLoaded === true ||
                _this.siteFilterService.filterMode === "single") {
                _this.isRefreshPaused = true;
                _this.config.isBusy(showBusy, _this.busyMessage);
                _this.$scope.$evalAsync(function () {
                    var tile = {
                        swql: _this.settings.swql,
                        otherIssuesSwql: _this.settings.otherissuesswql,
                        hasListSwql: _this.settings.listswql ? true : false,
                        tileType: _this.settings.tiletype,
                    };
                    if (_this.isGroupFilterApplied()) {
                        tile.basicFilter = _this.settings.basicfilter;
                        tile.entity = _this.settings.entity;
                        tile.sites = _this.settings.filter.siteFilter;
                        tile.groups = _this.settings.filter.groupFilter;
                    }
                    var originallistSwql = _this.settings.listswql;
                    var selectedSites = _this.siteFilterService.getSelectedSites();
                    _this.swTileService
                        .loadTileInfo(_this.siteFilterService.getSelectedSites(), tile)
                        .then(function (data) {
                        if (_this.isGroupFilterApplied() && originallistSwql !== data.tile.listSwql) {
                            _this.settings.listswql = data.tile.listSwql;
                            _this.settings.listtemplate = data.tile.listtemplate;
                            _this.config.saveSettings();
                        }
                        //handle case when filter was changed but previous request was not finished
                        var skipInit = _this.swTileService.tileFilterChanged(selectedSites, _this.siteFilterService.getSelectedSites());
                        if (!skipInit) {
                            _this.initializeTile(data.tile);
                        }
                    }).finally(function () {
                        _this.$log.info("finally");
                        _this.config.isBusy(false);
                        _this.isRefreshPaused = false;
                    });
                });
            }
        };
        this.initializeTile = function (tileData) {
            var siteIDs = _this.getSelectedSitesForUrl();
            _this.initializeEmptyTile();
            _this.activeAlerts = [];
            if (tileData.issues) {
                _this.activeAlerts = tileData.issues;
            }
            if (_this.settings.showalerts !== null && _this.settings.showalerts !== undefined) {
                _this.showAlerts = _this.settings.showalerts;
            }
            _this.redirectEnabled = tileData.hasListSwql;
            _this.redirectUrl = tileData.assetExplorerPath + (_this.config.id + "?" + siteIDs);
            _this.otherStatuses = [];
            if (tileData.statuses) {
                _this.entitiesCount = _.reduce(tileData.statuses, function (sum, n) {
                    return sum + n.count;
                }, 0);
                _this.otherStatuses = _this.statusInfoService
                    .getMetaStatusCollection(tileData.statuses, _this.statusInfo);
                _.each(_this.otherStatuses, function (detail) {
                    detail.iconName = _this.statusInfoService.getMetaStatusIconName(detail.name);
                    var siteFilter = "";
                    if (siteIDs) {
                        siteFilter = siteIDs + "&";
                    }
                    detail.redirectUrl = tileData.assetExplorerPath +
                        (_this.config.id + "?" + siteFilter + "status=" + detail.name);
                });
                _this.mainStatus = _this.getMainStatus(_this.otherStatuses);
            }
        };
        this.determineGroupStatus = function (otherStatuses) {
            return _this.statusInfoService.getMetaStatusRollup(otherStatuses).toLowerCase();
        };
        this.initializeEmptyTile = function () {
            _this.mainStatus = {
                name: "none",
                count: 0,
                iconName: "none",
                displayName: "None",
                redirectUrl: "#"
            };
            _this.redirectEnabled = false;
            _this.showAlerts = true;
        };
        this.getMainStatus = function (details) {
            var status;
            if (details.length > 0) {
                status = details[0];
                status.name = _this.determineGroupStatus(details);
                details.shift();
            }
            return status;
        };
        this.getSelectedSitesForUrl = function () {
            return _.join(_.map(_this.siteFilterService.getSelectedSites(), function (siteId) { return "siteIDs=" + siteId; }), "&");
        };
        this.onUnload = function () {
            if (_this.refreshTask) {
                _this.$interval.cancel(_this.refreshTask);
            }
        };
        this.onSettingsChanged = function (settings) {
            _this.settings = settings;
            _this.config.settings = _this.settings;
            _this.onChange();
        };
    }
    TileController = __decorate([
        widget_decorator_1.Widget({
            selector: "swTileWidget",
            templateUrl: "[widgets]:/components/tile/tile-widget.html",
            controllerAs: "vm",
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object, Object, Function, Function, Object, Object, Object])
    ], TileController);
    return TileController;
}());
exports.TileController = TileController;


/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = "﻿<xui-dialog class=basic-filter-dialog> <div class=entity-container xui-scrollbar> <div class=\"scroll-content xui-edge-definer\"> <ul ng-if=\"vm.dialogOptions.viewModel.selectedEntities.length > 0\"> <li ng-repeat=\"selectedEntity in vm.dialogOptions.viewModel.selectedEntities track by $index\" class=entity-container-selected> <div class=\"row entity-container-conditions-row\"> <div class=\"col-md-11 entity-container-title\"> <b _t>Entity</b> </div> <div class=\"col-md-1 pull-left entity-container-remove-entity\"> <xui-button size=large ng-click=vm.dialogOptions.viewModel.onEntityRemoved($index) display-style=tertiary icon=close></xui-button> </div> </div> <div class=entity-container-filter> <div class=xui-dropdown--justified xui-busy=selectedEntity.isBusy xui-busy-message=\"Loading data, please wait...\" _t> <xui-dropdown on-changed=\"vm.dialogOptions.viewModel.onEntityChanged(newValue, oldValue, $index)\" display-value=entityDisplayName display-format={0} ng-model=selectedEntity ng-click=vm.dialogOptions.viewModel.adjustMenuPosition($event) items-source=vm.dialogOptions.viewModel.availableEntities></xui-dropdown> </div> <ul ng-if=\"selectedEntity.conditions.length > 0\" class=entity-container-conditions> <li ng-repeat=\"condition in selectedEntity.conditions track by $index\"> <h4 ng-if=\"$index > 0\" class=entity-container-operation> AND </h4> <div class=row> <div class=\"col-md-4 xui-dropdown--justified entity-container-available-fields\"> <xui-dropdown ng-model=condition.field is-editable=false display-value=displayName display-format={0} items-source=selectedEntity.availableFields ng-click=vm.dialogOptions.viewModel.adjustMenuPosition($event) is-required=true on-changed=\"vm.dialogOptions.viewModel.onFilteredFieldChanged(newValue, oldValue, $index, selectedEntity)\"> <div ng-message=required _t>This field is required</div> </xui-dropdown> </div> <div class=\"col-md-3 xui-dropdown--justified entity-container-available-values\"> <xui-dropdown ng-model=condition.operation is-editable=false display-value=name display-format={0} items-source=vm.dialogOptions.viewModel.availableOperations ng-click=vm.dialogOptions.viewModel.adjustMenuPosition($event) is-required=true> <div ng-message=required _t>This field is required</div> </xui-dropdown> </div> <div class=\"col-md-4 xui-dropdown--justified entity-container-available-values\" xui-busy=condition.isBusy ng-if=condition.operation.supportValues xui-busy-message=\"Loading available values, please wait...\" _t> <xui-dropdown ng-model=condition.value is-editable=true display-value=displayValue display-format={0} items-source=condition.availableValues ng-click=vm.dialogOptions.viewModel.adjustMenuPosition($event) is-required=false></xui-dropdown> </div> <div class=\"col-md-1 pull-right entity-container-remove-condition\"> <xui-button size=large ng-click=\"vm.dialogOptions.viewModel.onFilteredRemoved(selectedEntity, $index)\" display-style=tertiary icon=close></xui-button> </div> </div> </li> </ul> </div> <xui-button size=large ng-click=vm.dialogOptions.viewModel.addFilterCondition(selectedEntity) display-style=tertiary icon=add class=entity-container-add-button _t>Add Condition</xui-button> </li> </ul> <xui-button size=large ng-click=vm.dialogOptions.viewModel.onEntityAdded(selectedEntity) display-style=tertiary icon=add _t>Entity</xui-button> </div> </div> </xui-dialog>";

/***/ }),
/* 49 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <xui-expander is-open=true> <xui-expander-heading _t> <h3 _t> SWQL Status Summary Query</h3> </xui-expander-heading> <textarea ng-model=vm.dialogOptions.viewModel.swqlTileQuery ng-change=vm.dialogOptions.viewModel.onSWQLChange() class=sw-widget-settings-tile-query></textarea> <div ng-if=vm.dialogOptions.viewModel.tileQueryError> <xui-message type=error allow-dismiss=true> {{vm.dialogOptions.viewModel.tileQueryError}} </xui-message> </div> </xui-expander> <xui-expander is-open=true> <xui-expander-heading _t> <h3 _t> SWQL Asset Explorer Query</h3> </xui-expander-heading> <textarea ng-model=vm.dialogOptions.viewModel.swqlAssetQuery ng-change=vm.dialogOptions.viewModel.onSWQLChange() class=sw-widget-settings-tile-query></textarea> <div ng-if=vm.dialogOptions.viewModel.assetExplorerQueryError> <xui-message type=error allow-dismiss=true> {{vm.dialogOptions.viewModel.assetExplorerQueryError}} </xui-message> </div> </xui-expander> <xui-expander is-open=true> <xui-expander-heading _t> <h3 _t> SWQL Alert Query</h3> </xui-expander-heading> <textarea ng-model=vm.dialogOptions.viewModel.swqlAlertQuery ng-change=vm.dialogOptions.viewModel.onSWQLChange() class=sw-widget-settings-tile-query></textarea> <div ng-if=vm.dialogOptions.viewModel.alertQueryError> <xui-message type=error allow-dismiss=true> {{vm.dialogOptions.viewModel.alertQueryError}} </xui-message> </div> </xui-expander> <div class=row> <div ng-if=!vm.dialogOptions.viewModel.isTileOrAlertQueryDefined> <div class=col-md-8> <xui-message type=error allow-dismiss=true _t> Please define Status Summary or Alert Query </xui-message> </div> </div> </div> </xui-dialog> ";

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Observable = /** @class */ (function () {
    function Observable(value) {
        this.value = value;
        this.listeners = [];
    }
    Observable.prototype.update = function (newValue) {
        if (typeof (newValue) !== "undefined" && newValue !== this.value) {
            this.value = newValue;
            this.notify(newValue);
        }
        return this.value;
    };
    Observable.prototype.subscribe = function (listener) {
        this.listeners.push(listener);
    };
    Observable.prototype.notify = function (value) {
        this.listeners.forEach(function (listener) { return listener(value); });
    };
    Observable.prototype.valueOf = function () {
        return this.value;
    };
    return Observable;
}());
exports.Observable = Observable;


/***/ }),
/* 51 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=!ctrl.emptyData.viewModel.filtersApplied> <xui-empty empty-data=ctrl.emptyData.viewModel.noDataToShow></xui-empty> </div> <div ng-if=ctrl.emptyData.viewModel.filtersApplied> <xui-empty empty-data=ctrl.emptyData.viewModel.noFilteredResults></xui-empty> </div>";

/***/ }),
/* 52 */
/***/ (function(module, exports) {

module.exports = "<div class=row_container> <div ng-if=item.entityType class=\"icon_column pull-left\"> <div class=icon_wrapper> <xui-icon icon=\"{{item.entityType | swEntityIcon}}\" is-dynamic=true status=\"{{item.status | swStatus:item.substatus}}\"></xui-icon> </div> </div> <div class=\"pull-left list_member\"> <div class=text_wrap opid={{item.opid}} ng-if=item.url> <a href={{item.url}}>{{item.name}}</a> </div> <div class=text_wrap opid={{item.opid}} ng-if=!item.url> {{item.name}} </div> <div ng-if=item.parentName> <div class=text_wrap opid={{item.parentOpid}} ng-if=item.parentUrl _t> on <xui-icon icon-size=small icon=\"{{item.parentEntityType | swEntityIcon}}\" is-dynamic=true status=\"{{item.parentStatus | swStatus:item.parentSubstatus}}\"></xui-icon> <a href={{item.parentUrl}}>{{item.parentName}}</a> </div> <div class=text_wrap opid={{item.parentOpid}} ng-if=!item.parentUrl _t> on <xui-icon icon-size=small icon=\"{{item.parentEntityType | swEntityIcon}}\" is-dynamic=true status=\"{{item.parentStatus | swStatus:item.parentSubstatus}}\"></xui-icon> {{item.parentName}} </div> </div> </div> <div ng-if=\"item.alerts && item.alerts.length > 0\" class=\"alerts pull-right\"> <div class=\"pull-right data\" ng-repeat=\"alert in item.alerts | orderBy:'alertType'\"> <span> <xui-icon icon-size=small icon=severity_{{alert.severity}}></xui-icon> </span> <span> {{alert.numberOfAlerts}} </span> </div><br/> <div title=\"Active Alerts\" class=\"pull-right text_wrap description\" _t> Active Alerts </div> </div> <div ng-if=\"item.availability !== undefined\" class=\"availability pull-right\"> <div class=\"pull-right data\"> {{item.availability}}% </div><br/> <div title=Availability class=\"pull-right text_wrap description\" _t> Availability </div> </div> </div>";

/***/ }),
/* 53 */
/***/ (function(module, exports) {

module.exports = "<div class=generic-list ng-if=vm.initialized> <div class=timeFrameWrapper ng-if=vm.filteringSession.state.timeFrame.__isLoaded> <xui-chart-time-frame-picker range=vm.filteringSession.state.timeFrame.range is-reset-visible=vm.filteringSession.state.timeFrame.isResetVisible() config=vm.filteringSession.state.timeFrame.config on-zoom-reset=vm.filteringSession.state.timeFrame.onZoomReset(); on-range-shift=vm.filteringSession.state.timeFrame.onRangeShift(timerange) on-time-selection=vm.filteringSession.state.timeFrame.onTimeSelection(timeframe)> </xui-chart-time-frame-picker> </div> <div class=filterBarsWrapper ng-repeat=\"filterBar in vm.filteringSession.state.filterBars\"><xui-status-filter active-filters=filterBar.activeFilters filter-source=filterBar.filterSource filter-change=\"vm.filteringSession.onComponentUpdate('filterBars')\" ng-if=\"filterBar.filterSource.length > 0\"> </xui-status-filter></div> <div xui-busy=vm.filteringSession.isBusy> <xui-grid ng-if=vm.filteringSession.state.list items-source=vm.filteringSession.state.list.items hide-toolbar=true pagination-data=vm.filteringSession.state.list.pagination sorting-data=vm.filteringSession.state.list.sorting searching-data=vm.filteringSession.state.list.searching on-pagination-change=\"vm.filteringSession.onComponentUpdate('list')\" on-sorting-change=\"vm.filteringSession.onComponentUpdate('list')\" on-search=\"vm.filteringSession.onComponentUpdate('list')\" options=vm.filteringSession.state.list.options template-fn=vm.getTemplate empty-data=vm.emptyData row-padding=vm.filteringSession.state.list.custom.rowPadding stripe=vm.filteringSession.state.list.custom.striped header-template-url={{vm.headerTemplateId}} controller=vm /> </div> </div> ";

/***/ }),
/* 54 */
/***/ (function(module, exports) {

module.exports = "<a ng-repeat=\"link in ctrl.customLinks\" ng-href={{link.href}} class=\"EditResourceButton sw-btn-resource sw-btn\"> <span class=sw-btn-c> <span class=sw-btn-t _t>{{link.caption}}</span> </span> </a> ";

/***/ }),
/* 55 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-widget-drawer ng-class=\"{ 'sw-widget-drawer__filter-panel--open' : $widgetDrawer.isFilterOpen }\"> <div class=sw-widget-drawer__backdrop> <div class=sw-widget-drawer__drag-tip> <div class=sw-widget-drawer__drag-tip-outer> <div class=sw-widget-drawer__drag-tip-inner _t>Drag widgets here</div> </div> </div> </div> <div class=sw-widget-drawer__panel ng-class=\"{ 'sw-widget-drawer__panel--search-active' : $widgetDrawer.isSearching }\"> <div class=sw-widget-drawer__filter-panel> <div class=sw-widget-drawer__filter-heading> <div class=sw-widget-drawer__title _t>Group By</div> <div class=sw-widget-drawer__filter-heading-icon> <xui-icon icon=filter></xui-icon> </div> </div> <div class=\"sw-widget-drawer__filter-content xui-dropdown--justified\"> <xui-dropdown class=sw-widget-drawer__category-groups items-source=$widgetDrawer.categoryGroups display-value=name ng-model=$widgetDrawer.selectedCategoryGroup on-changed=$widgetDrawer.onCategoryGroupChanged(newValue)> </xui-dropdown> <div class=sw-widget-drawer__filter-categories xui-scrollbar> <xui-listview class=sw-widget-drawer__filter-categories-list controller=$widgetDrawer stripe=false template-url=[widgets]:/directives/widget/widgetDrawer/widgetCategory-item.html items-source=$widgetDrawer.categories row-padding=none selection=$widgetDrawer.selectedCategories selection-mode=single> </xui-listview> </div> </div> <div class=sw-widget-drawer__filter-panel-collapsed ng-click=$widgetDrawer.toggleFilter()> <div class=sw-widget-drawer__filter-panel-icon> <xui-icon icon=filter></xui-icon> </div> </div> </div> <div class=sw-widget-drawer__list-container> <xui-button class=sw-widget-drawer__filter-heading-btn display-style=tertiary icon=\"{{$widgetDrawer.isFilterOpen ? 'double-caret-right' : 'double-caret-left'}}\" ng-click=$widgetDrawer.toggleFilter() is-empty=true> </xui-button> <div class=sw-widget-drawer__title _t>Available Widgets</div> <div class=sw-widget-drawer__list-header> <div class=sw-widget-drawer__list-sorter> <xui-sorter items-source=$widgetDrawer.sortColumns on-change=\"$widgetDrawer.onSortChange(newValue, oldValue)\" selected-item=$widgetDrawer.sortSelection display-value=name sort-direction=$widgetDrawer.sortDirection> </xui-sorter> </div> <div class=sw-widget-drawer__list-search> <xui-search placeholder=_t(Search...) value=$widgetDrawer.searchTerm on-search=\"$widgetDrawer.onSearch(value, cancellation)\" on-clear=$widgetDrawer.onClear() _ta> </xui-search> </div> </div> <div class=sw-widget-drawer__scroll-container> <xui-scroll-shadows class=\"\"> <div class=sw-widget-drawer__scroller> <div class=sw-widget-drawer__list xui-busy=$widgetDrawer.isBusy xui-busy-message=\"Loading widgets...\"> <xui-listview controller=$widgetDrawer stripe=false items-source=$widgetDrawer.items track-by=Path row-padding=none empty-text=\"_t(Nothing to show)\" template-url=[widgets]:/directives/widget/widgetDrawer/widgetDrawer-item.html _ta></xui-listview> </div> <div class=sw-widget-drawer__pager> <xui-pager class=small page=$widgetDrawer.pageNumber page-size=$widgetDrawer.pageSize total=$widgetDrawer.totalItems adjacent=0 dots=... scroll-top=false hide-if-empty=false show-prev-next=true pager-action=\"$widgetDrawer.onPageChanged(page, pageSize, total)\"> </xui-pager> </div> </div> </xui-scroll-shadows> </div> </div> </div> </div> ";

/***/ }),
/* 56 */
/***/ (function(module, exports) {

module.exports = "<div class=\"xui ResourceWrapper sw-widget-drawer__load-placeholder\"> <div class=sw-widget-overlay__header> <div class=sw-widget-overlay__title> <span class=sw-widget-drawer__load-indicator-title title={{::widgetTitle}}>{{::widgetTitle}}</span> </div> </div> <xui-message class=sw-widget-drawer__load-indicator-error type=critical> <span _t> <b>Widget cannot be loaded from the server.</b> Please try again later.&nbsp; </span> <a href class=sw-widget-drawer__load-indicator-reload ng-click=reload() _t>Reload Now</a> </xui-message> </div> ";

/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = "<div class=ResourceWrapperPanel> <div class=\"xui ResourceWrapper sw-widget-drawer__load-indicator sw-widget-drawer__load-placeholder\"> <div class=sw-widget-overlay__header> <div class=sw-widget-overlay__title> <span class=sw-widget-drawer__load-indicator-title title={{::widgetTitle}}>{{::widgetTitle}}</span> </div> </div> <xui-progress class=sw-widget-drawer__load-indicator-progress show=true message=\"_t(Loading, please wait...)\" _ta> </xui-progress> </div> </div> ";

/***/ }),
/* 58 */
/***/ (function(module, exports) {

module.exports = "<div class=\"xui sw-widget-drawer__placeholder\"> <div class=sw-widget-drawer__circle> <xui-icon icon=drop-active></xui-icon> </div> </div> ";

/***/ }),
/* 59 */
/***/ (function(module, exports) {

module.exports = "<div class=asa-interfaces ng-init=asaInterfacesController.init()> <cli-credentials-notification> <span _t><b>CLI Credentials are missing.</b> To get the information about security levels, we need CLI Credentials.&nbsp;<asa-node-properties-link>Add CLI credentials</asa-node-properties-link></span> </cli-credentials-notification> <xui-grid name=asa-interfaces-list items-source=asaInterfacesController.items template-url=asa-interfaces-template options=asaInterfacesController.options hide-toolbar=true smart-mode=true sorting-data=asaInterfacesController.sorting pagination-data=asaInterfacesController.paging controller=asaInterfacesController> </xui-grid> <script id=asa-interfaces-template type=text/ng-template> <div class=\"row\">\n            <div class=\"col-md-4\" name=\"columnName\">\n                <div class=\"media\">\n                    <div class=\"media-left media-top\">\n                        <xui-icon css-class=\"widget-icon asa-status-icon\" icon=\"network-interface\" status=\"{{item.status | orionStatusToXuiIcon}}\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <div>\n                            <a href={{item.detailsUrl}}>\n                                <span class=\"asa-clickable-capitals asa-interface-caption\">{{item.caption}}</span>\n                            </a>\n                        </div>\n                        <div>\n                            <xui-help-hint ng-if=\"item.standbyIp && item.ipAddress\">\n                                <span class=\"asa-interface-ip-address\"><b>{{item.ipAddress}},</b> </span>\n                                <span class=\"asa-interface-standby-ip\" _t=\"['{{item.standbyIp}}']\">{0} (standby)</span>\n                            </xui-help-hint>\n                            <xui-help-hint ng-if=\"!item.standbyIp && item.ipAddress\">\n                                <span class=\"asa-interface-ip-address\"><b>{{item.ipAddress}}</b></span>\n                            </xui-help-hint>\n                            <xui-help-hint ng-if=\"item.standbyIp && !item.ipAddress\">\n                                <span class=\"asa-interface-standby-ip\" _t=\"['{{item.standbyIp}}']\">{0} (standby)</span>\n                            </xui-help-hint>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-1 asa-text-right\" name=\"columnSecurityLevel\" ng-switch=\"item.securityLevel == null\">\n                <span class=\"asa-interface-security-level\" ng-switch-when=\"true\" _t>N/A</span>\n                <span class=\"asa-interface-security-level\" ng-switch-default _t>{{ item.securityLevel }}</span>\n                <xui-help-hint _t>sec. level</xui-help-hint>\n            </div>\n            <div class=\"col-md-2 asa-text-right\" name=\"columnErrorRate\" ng-switch=\"item.inErrorsInLastHour == null && item.outErrorsInLastHour == null\">\n                <span class=\"asa-interface-errors-in-last-hour\" ng-switch-when=\"true\" _t>N/A</span>\n                <span class=\"asa-interface-errors-in-last-hour\" ng-switch-default _t>{{item.totalErrorRate | number}}</span>\n                <xui-help-hint _t>errors in last hour</xui-help-hint>\n            </div>\n            <div class=\"col-md-2 asa-text-right\" name=\"columnInBps\">\n                <span class=\"asa-interface-in-bits-per-second\">{{item.inBitsPerSecond | asaMetric: 'bps' : 2}}</span>\n                <xui-help-hint _t>in</xui-help-hint>\n            </div>\n            <div class=\"col-md-2 asa-text-right\" name=\"columnOutBps\">\n                <span class=\"asa-interface-out-bits-per-second\">{{item.outBitsPerSecond | asaMetric: 'bps' : 2}}</span>\n                <xui-help-hint _t>out</xui-help-hint>\n            </div>\n            <div class=\"col-md-1 asa-interfaces-favoriteIcon text-center\" name=\"columnIsFavorite\">\n                <xui-icon icon-size=\"small\" icon=\"{{item.isFavorite ? 'star-full' : 'star-empty'}}\" is-dynamic=\"true\" ng-click=\"vm.switchIsFavoriteValue(item)\"></xui-icon>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 60 */
/***/ (function(module, exports) {

module.exports = "<div class=asa-cli-credentials-notification ng-show=vm.isDisplayed> <xui-message type=info> <span ng-transclude></span> </xui-message> </div>";

/***/ }),
/* 61 */
/***/ (function(module, exports) {

module.exports = "<div class=\"asa-widget asa-context-list\"> <xui-grid name=asa-context-list items-source=contextList.items template-url=asa-contexts-template options=contextList.options hide-toolbar=true smart-mode=true pagination-data=contextList.paging controller=contextList> </xui-grid> <script id=asa-contexts-template type=text/ng-template> <div class=\"row\">\n            <div class=\"col-md-12\" name=\"columnName\">\n                <div class=\"media\">\n                    <div class=\"media-left media-top\">\n                        <xui-icon css-class=\"widget-icon asa-status-icon\" icon=\"firewall\" status=\"{{item.status | orionStatusToXuiIcon}}\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <div ng-if=\"item.isOrionNode\">\n                            <div class=\"full-name\">\n                                <a href={{item.detailsUrl}} ng-if=\"!item.isThisContext\">\n                                    <span class=\"xui-text-h3 context-name\">{{item.contextName}}</span>\n                                </a>\n                                <i ng-if=\"item.isThisContext\" _t>this node</i>\n                            </div>\n                            <div class=\"host-name\">\n                                {{item.hostName}}\n                            </div>\n                        </div>\n                        <div ng-if=\"!item.isOrionNode\">\n                            <div class=\"full-name\">\n                                <span class=\"xui-text-h3 context-name\">{{item.contextName}}</span>\n                            </div>\n                            <a class=\"monitor-node\" href=\"/Orion/Nodes/Add/Default.aspx?&restart=false\" _t>\n                                Monitor Node\n                            </a>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 62 */
/***/ (function(module, exports) {

module.exports = "<a class=asa-node-properties-link sw-auth sw-auth-permissions=nodeManagement ng-click=vm.onClick() href=# ng-transclude></a>";

/***/ }),
/* 63 */
/***/ (function(module, exports) {

module.exports = "<div class=\"asa-widget asa-platform-details\"> <div class=media xui-busy=platform.isBusy> <div class=media-left> <xui-icon icon-size=xlarge css-class=widget-icon icon=firewall status=\"{{platform.details.status | orionStatusToXuiIcon}}\"></xui-icon> </div> <div class=media-body> <div class=display-data> <span class=\"display-name xui-text-h1\">{{platform.details.caption}}</span> <span ng-if=platform.details.haState>, </span> <span ng-if=platform.details.haState class=\"ha-state xui-text-bg xui-text-h2\">{{platform.details.haState}}</span> </div> <div> <span ng-if=platform.details.asaContext class=\"asa-context xui-text-h3\">{{platform.details.asaContext}}</span> <span class=\"context-separator xui-text-h3\" ng-bind-html=platform.details.contextSeparator></span> <span class=\"machine-type xui-text-bg\">{{platform.details.machineType}}</span> </div> <div ng-if=platform.details.iosVersion> <span class=\"xui-text-dscrn xui-text-sml\" _t>running</span> <span class=\"ios-version xui-text-l\">{{platform.details.iosVersion}}</span> </div> <div ng-if=platform.details.mode> <span class=\"mode xui-text-l\">{{platform.details.mode}}</span> <span class=\"xui-text-dscrn xui-text-sml\" _t>mode</span> </div> <div ng-if=platform.details.ipAddress> <span class=\"xui-text-dscrn xui-text-sml\" _t>Polled at</span> <span class=\"ip-address xui-text-l\">{{platform.details.ipAddress}}</span> </div> </div> </div> <cli-credentials-notification> <span _t><b>CLI Credentials are missing.</b> To get the information about contexts, security levels and inactive site-to-site tunnels, we need CLI Credentials.&nbsp;<asa-node-properties-link>Add CLI credentials</asa-node-properties-link></span> </cli-credentials-notification> <xui-expander heading=\"_t(Other details)\" _ta css-class=other-details ng-if=platform.details.hasOtherDetails> <xui-listview class=asa-other-details row-padding=narrow items-source=platform.details.vpnDetails template-url=[widgets]:/orionModules/asa/components/platformDetails/platformDetailsOther.html> </xui-listview> <div ng-if=\"platform.details.orionDetails.length>0\" class=\"xui-text-h3 margin-md-top\" ng-if=\"platform.details.orionDetails.length>0\" _t>Orion Details</div> <xui-listview ng-if=\"platform.details.orionDetails.length>0\" class=orion-other-details row-padding=narrow items-source=platform.details.orionDetails template-url=[widgets]:/orionModules/asa/components/platformDetails/platformDetailsOther.html> </xui-listview> </xui-expander> <div></div> </div> ";

/***/ }),
/* 64 */
/***/ (function(module, exports) {

module.exports = "<div class=other-details-item> <div class=item-description>{{::item.description}}</div> <div class=\"item-label xui-text-dscrn xui-text-sml\">{{::item.label}}</div> </div> ";

/***/ }),
/* 65 */
/***/ (function(module, exports) {

module.exports = "<xui-filtered-list items-source=serverSideFilteredListController.queryResults.items on-refresh=\"serverSideFilteredListController.getFilteredData(filters, filterModels, pagination, sorting, searching)\" options=serverSideFilteredListController.options filter-values=serverSideFilteredListController.initialFilterValues filter-properties-fn=serverSideFilteredListController.getFilterProperties() sorting=serverSideFilteredListController.sortingSettings controller=serverSideFilteredListController.parentController pagination-data=serverSideFilteredListController.pagination> </xui-filtered-list> ";

/***/ }),
/* 66 */
/***/ (function(module, exports) {

module.exports = " <asa-server-side-filtered-list parent-controller=sessionListController options=sessionListController.options getdata=\"sessionListController.getSessions(filters, filterModels, pagination, sorting, searching)\"> </asa-server-side-filtered-list> ";

/***/ }),
/* 67 */
/***/ (function(module, exports) {

module.exports = " <div class=row ng-if=\"item.isConnected() || item.isDisconnected()\"> <div class=col-md-4> <div class=media> <div class=\"media-left media-top\"> <xui-icon icon=\"{{item.state | sessionStateToApolloState}}\" is-dynamic=true title=\"{{item.state | sessionStateToText}}\"></xui-icon> </div> <div class=media-body> <div class=user-name>{{item.userName}}</div> <xui-help-hint ng-if=item.isConnected() _t> Connected for {{item.minutesConnected | humanizedDuration}} </xui-help-hint> <xui-help-hint _t=\"['{{item.state | sessionStateToText }}']\" ng-if=\"item.state == 2\"> {0} </xui-help-hint> </div> </div> </div> <div class=col-md-2> <div ng-if=item.inTotalBytes> {{item.inTotalBytes | swBytes}} <xui-help-hint _t>downloaded</xui-help-hint> </div> </div> <div class=col-md-2> <div ng-if=item.outTotalBytes> {{item.outTotalBytes | swBytes}} <xui-help-hint _t>uploaded</xui-help-hint> </div> </div> <div class=col-md-4> <div class=pull-right ng-if=item.isConnected()> {{item.connected | humanizedDateTime}} <xui-help-hint _t>since</xui-help-hint> </div> <div class=pull-right ng-if=item.isDisconnected()> <div class=align-right>{{ item.minutesConnected | humanizedDuration }}</div> <xui-help-hint _t>from {{item.connected | humanizedDateTime}} to {{item.disconnected | humanizedDateTime}}</xui-help-hint> </div> </div> </div> <div class=row ng-if=item.connectionFailed()> <div class=col-md-6> <div class=media> <div class=\"media-left media-top\"> <xui-icon icon=\"{{item.state | sessionStateToApolloState}}\" is-dynamic=true status=\"{{item.state | sessionStateToApolloState}}\"></xui-icon> </div> <div class=media-body> <div>{{item.userName}}</div> <xui-help-hint ng-switch=item.state> <div class=text-danger> <span _t>Connection Failed</span>, <span class=text-normal>{{item.statusMessage}}</span> </div> </xui-help-hint> </div> </div> </div> <div class=col-md-2> <div> {{item.clientIpAddress}} <xui-help-hint _t>{{item.remoteAccessTunnelType | remoteAccessTunnelTypeToText }}, {{item.clientVersion}}</xui-help-hint> </div> </div> <div class=col-md-4> <div class=pull-right> <div class=align-right>{{item.connected | humanizedDateTime }}</div> <xui-help-hint _t>occurred at</xui-help-hint> </div> </div> </div> <div class=row ng-if=item.connectionDropped()> <div class=col-md-4> <div class=media> <div class=\"media-left media-top\"> <xui-icon icon=\"{{item.state | sessionStateToApolloState}}\" is-dynamic=true status=\"{{item.state | sessionStateToApolloState}}\"></xui-icon> </div> <div class=media-body> <div>{{item.userName}}</div> <xui-help-hint> <b ng-if=!item.isConnected() _t>Not Connected</b> <i ng-if=item.isConnected()>(Connected Successfully Since {{item.connected | humanizedDateTime}}, {{item.connected | date:'MMM d'}})</i>, {{item.statusMessage}} </xui-help-hint> </div> </div> </div> <div class=col-md-2> <div ng-if=item.inTotalBytes> {{item.inTotalBytes | swBytes}} <xui-help-hint _t>downloaded</xui-help-hint> </div> </div> <div class=col-md-2> <div ng-if=item.outTotalBytes> {{item.outTotalBytes | swBytes}} <xui-help-hint _t>uploaded</xui-help-hint> </div> </div> <div class=col-md-4> <div class=pull-right> <div class=align-right>{{item.connected | humanizedDateTime }}</div> <xui-help-hint _t>occurred at</xui-help-hint> </div> </div> </div> ";

/***/ }),
/* 68 */
/***/ (function(module, exports) {

module.exports = "<div class=vpn-site-to-site-tunnels> <cli-credentials-notification> <span _t><b>CLI Credentials are missing.</b> To get the information about inactive site-to-site tunnels, we need CLI Credentials.&nbsp;<asa-node-properties-link>Add CLI credentials</asa-node-properties-link></span> </cli-credentials-notification> <asa-server-side-filtered-list parent-controller=filteredTunnelsController options=filteredTunnelsController.options getdata=\"filteredTunnelsController.getSessions(filters, filterModels, pagination, sorting, searching)\"> </asa-server-side-filtered-list> </div> ";

/***/ }),
/* 69 */
/***/ (function(module, exports) {

module.exports = "<div class=row> <div class=\"col-md-5 media\"> <div class=\"media-left media-middle\"> <xui-icon icon-size=small icon=vpn-tunnel-sitetosite status=\"{{item.status | siteToSiteVpnTunnelOrionStatusFilter}}\" is-dynamic=true></xui-icon> </div> <div class=media-body> <span class=site-to-site-tunnel-target-ip-address>&#8594; {{item.hostName}}</span> <xui-help-hint ng-if=\"item.status == vm.orionObjectStatus.Up\"> <span class=\"site-to-site-tunnel-status {{item.status | siteToSiteVpnTunnelOrionStatusTextCSSFilter}}\">{{item.statusText}}</span> <span _t>Status</span>, <b class=site-to-site-tunnel-encryptionAlgorithm>{{item.encryptionAlgorithmPhase1 | asaEncryptAlgorithm}}</b> <span _t>encryption</span>, <b class=site-to-site-tunnel-hashAlgorithm>{{item.hashAlgorithmPhase1 | asaHashAlgorithm}}</b> <span _t>hashing</span> </xui-help-hint> <xui-help-hint ng-if=\"item.status == vm.orionObjectStatus.Down\"> <b class=\"site-to-site-tunnel-status {{item.status | siteToSiteVpnTunnelOrionStatusTextCSSFilter}}\">{{item.statusText}}</b> <span _t>Status</span>, <span class=\"{{item.status | siteToSiteVpnTunnelOrionStatusTextCSSFilter}}\" _t>Failing at <b class=site-to-site-tunnel-failPhase>Phase {{item.failPhase}}</b></span> </xui-help-hint> <xui-help-hint ng-if=\"item.status == vm.orionObjectStatus.Inactive\"> <span class=\"site-to-site-tunnel-status {{item.status | siteToSiteVpnTunnelOrionStatusTextCSSFilter}}\">{{item.statusText}}</span> <span _t>Status</span> </xui-help-hint> </div> </div> <div class=\"col-md-2 asa-text-right\"> <div ng-if=\"item.status == vm.orionObjectStatus.Up\"> <span class=site-to-site-tunnel-inBitsPerSecond>{{item.inBitsPerSecond | asaMetric: 'bps' : 2}}</span> <xui-help-hint _t>in</xui-help-hint> </div> </div> <div class=\"col-md-2 asa-text-right\"> <div ng-if=\"item.status == vm.orionObjectStatus.Up\"> <span class=site-to-site-tunnel-outBitsPerSecond>{{item.outBitsPerSecond | asaMetric: 'bps' : 2}}</span> <xui-help-hint _t>out</xui-help-hint> </div> </div> <div class=\"col-md-2 asa-text-right\"> <xui-help-hint ng-if=\"item.startTime!=null\"> <span class=site-to-site-tunnel-upTime>{{item.uptime | asaShortTimeSpan}}</span> <xui-help-hint class=site-to-site-tunnel-startTime _t=\"['{{item.startTime | asaShortDateOrTime}}']\">since {0}</xui-help-hint> </xui-help-hint> <xui-help-hint ng-if=\"item.startTime==null\"> <span class=site-to-site-tunnel-startTime _t>---</span> </xui-help-hint> </div> <div class=\"col-md-1 site-to-site-tunnel-favoriteIcon text-center\"> <xui-icon icon-size=small icon=\"{{item.isFavorite ? 'star-full' : 'star-empty'}}\" is-dynamic=true ng-click=vm.switchIsFavoriteValue(item)></xui-icon> </div> </div> ";

/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports = "<div class=\"vpn-widget asa-interfaces-widget\"> <asa-interfaces></asa-interfaces> </div>";

/***/ }),
/* 71 */
/***/ (function(module, exports) {

module.exports = "<div class=\"asa-widget asa-context-list-widget\"> <asa-context-list></asa-context-list> </div> ";

/***/ }),
/* 72 */
/***/ (function(module, exports) {

module.exports = "<div class=\"asa-widget asa-high-availability-chart-widget\"> <asa-high-availability-chart></asa-high-availability-chart> </div>";

/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = "<div class=\"asa-widget asa-platform-details-widget\"> <asa-platform-details></asa-platform-details> </div>";

/***/ }),
/* 74 */
/***/ (function(module, exports) {

module.exports = "<div class=vpn-remote-access-tunnels> <asa-session-list></asa-session-list> </div> ";

/***/ }),
/* 75 */
/***/ (function(module, exports) {

module.exports = "<div class=vpn-site-to-site-tunnels-widget> <asa-site-to-site-filtered-tunnels></asa-site-to-site-filtered-tunnels> </div> ";

/***/ }),
/* 76 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-access-list ng-init=ctrlAccessList.activate()> <div xui-busy=ctrlAccessList.isLoading xui-busy-message=Loading> <div class=acl-compare-button-container> <a href={{ctrlAccessList.getCompareAclUrl()}}> <xui-button icon=compare class=id-acl-compare-button is-disabled=ctrlAccessList.navigateToCompareAclDisabled() display-style=link> <span _t>Compare 2 ACLs</span> </xui-button> </a> </div> <xui-divider class=acl-divider> </xui-divider> <xui-grid items-source=ctrlAccessList.itemsSource class=id-acl-grid-parent hide-toolbar=true pagination-data=ctrlAccessList.gridPagination options=ctrlAccessList.gridOptions sorting-data=ctrlAccessList.sorting smart-mode=true template-url=[widgets]:/orionModules/ncm/components/accessList/accessList-item-template-level1.html on-search=\"ctrlAccessList.onSearch(item, cancellation)\" on-pagination-change=ctrlAccessList.restoreSelectedOfItemsSource() empty-data=ctrlAccessList.emptyData show-empty=!ctrlAccessList.isLoading> </xui-grid> </div> </div> ";

/***/ }),
/* 77 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=controller.activate() class=col-srd-desc> <div ng-if=\"controller.isLoading() && controller.canShowSrdIcon()\"> <xui-icon icon=busy-cube class=id-acl-srd-busy-icon icon-size=medium tool-tip=\"Rules state is being updated\"></xui-icon> </div> <xui-popover ng-if=\"!controller.isLoading() && controller.canShowSrdIcon()\" class=id-acl-srd-popover xui-popover-trigger=mouseenter xui-popover-placement=left xui-popover-content=controller.getSrdIconPopoverTemplate xui-popover-title=\"Overlapping Rules\"> <span class=srd-icon> <xui-icon icon=status_warning class=id-acl-srd-icon con-size=medium></xui-icon> </span> <span class=\"srd-counter id-acl-srd-counter\">{{controller.getTotalSrdCount()}}</span> </xui-popover> <script type=text/ng-template id=ncm-acl-srd-popover-template> <div class=\"id-acl-srd-icon-popover-dsc\">\n            <div ng-repeat=\"desc in controller.popoverDescriptions\">\n                {{desc}}\n            </div>\n        </div> </script> </div>";

/***/ }),
/* 78 */
/***/ (function(module, exports) {

module.exports = "<div class=asa-acllists> <access-list node-id=accessListsWdgCntl.nodeId></access-list> </div>";

/***/ }),
/* 79 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 80 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var switchIsFavoriteErrors_1 = __webpack_require__(246);
var FavoriteEntityController = /** @class */ (function () {
    /** @ngInject */
    FavoriteEntityController.$inject = ["$log", "getTextService", "xuiToastService"];
    function FavoriteEntityController($log, getTextService, xuiToastService) {
        this.$log = $log;
        this.getTextService = getTextService;
        this.xuiToastService = xuiToastService;
        this._t = this.getTextService;
    }
    FavoriteEntityController.prototype.processSwitchIsFavoriteResult = function (promise, favoriteEntity) {
        var _this = this;
        promise.then(
        /* success */
        function (success) {
            var errorCode = success ? success.errorCode : null;
            var maxFavoritesCount = success ? success.maxFavoritesCount : 0;
            switch (errorCode) {
                case switchIsFavoriteErrors_1.SwitchIsFavoriteErrors.ItemNotFound:
                    _this.xuiToastService.warning(_this._t("This item no longer exists."));
                    break;
                case switchIsFavoriteErrors_1.SwitchIsFavoriteErrors.MaxFavoritesPerNodeExceeded:
                    _this.xuiToastService.warning(_this._t("Maximum number of favorites ({0}) reached. To add this item, \
remove one or more items from favorites.").replace("{0}", maxFavoritesCount.toString()));
                    break;
                case switchIsFavoriteErrors_1.SwitchIsFavoriteErrors.AccessToVerbDenied:
                    _this.xuiToastService.warning(_this._t("You cannot change favorites because you don't have Node Management rights."));
                    break;
                default:
                    favoriteEntity.isFavorite = !favoriteEntity.isFavorite;
                    break;
            }
        }, 
        /* error */
        function (result) {
            _this.$log.error("switchIsFavoriteValue() failed", result);
        });
    };
    ;
    return FavoriteEntityController;
}());
exports.FavoriteEntityController = FavoriteEntityController;


/***/ }),
/* 82 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var HaStateType;
(function (HaStateType) {
    HaStateType[HaStateType["Unknown"] = 0] = "Unknown";
    HaStateType[HaStateType["Config"] = 1] = "Config";
    HaStateType[HaStateType["Connection"] = 2] = "Connection";
    HaStateType[HaStateType["Standby"] = 3] = "Standby";
})(HaStateType = exports.HaStateType || (exports.HaStateType = {}));


/***/ }),
/* 84 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 85 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 86 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 87 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var accessListGrid_service_1 = __webpack_require__(89);
var AccessListController = /** @class */ (function () {
    /** @ngInject */
    AccessListController.$inject = ["$log", "ncmWidgetsAccessListService", "ncmWidgetsNavigationService", "ncmWidgetsAccessListGridService", "$sce"];
    function AccessListController($log, ncmWidgetsAccessListService, ncmWidgetsNavigationService, ncmWidgetsAccessListGridService, $sce) {
        var _this = this;
        this.$log = $log;
        this.ncmWidgetsAccessListService = ncmWidgetsAccessListService;
        this.ncmWidgetsNavigationService = ncmWidgetsNavigationService;
        this.ncmWidgetsAccessListGridService = ncmWidgetsAccessListGridService;
        this.$sce = $sce;
        this.itemsSource = [];
        this.$onInit = function () {
            _this.gridPagination = {
                page: 1,
                pageSize: 10
            };
            _this.innerGridPagination = {
                page: 1,
                pageSize: 1000
            };
            _this.gridOptions = {
                hideSearch: false,
                searchableColumns: ["name"]
            };
            _this.innerGridOptions = {
                hideSearch: true,
                hidePagination: true
            };
            _this.sorting = {
                sortableColumns: [{
                        id: "name",
                        label: "Name"
                    }],
                sortBy: {
                    id: "name",
                    label: "Name"
                },
                direction: "asc"
            };
            _this.emptyData = {
                image: "no-search-results",
                title: "No Access Lists were found",
                description: "Try searching for something else"
            };
        };
        this.getAclDetailsUrl = function (gridItem) {
            var params = {
                nodeId: gridItem.nodeId,
                configId: gridItem.configId,
                aclName: gridItem.name
            };
            return _this.ncmWidgetsNavigationService.getAclDetailsUrl(params);
        };
        this.activate = function () {
            _this.ncmWidgetsAccessListGridService.reset();
            _this.fetchAccessLists();
            _this.locateAsaIfTabUrl();
        };
        this.fetchAccessLists = function () {
            _this.isLoading = true;
            _this.ncmWidgetsAccessListService.getAccessControlLists(_this.nodeId).then(function (result) {
                _this.aclHistory = result.accessLists;
                _this.ncmWidgetsAccessListGridService.itemsSource = _this.getAclHistoryGridItemsSource();
                _this.ncmWidgetsAccessListGridService.ifUrlsMapping = result.interfacesUrl;
                _this.itemsSource = angular.copy(_this.ncmWidgetsAccessListGridService.itemsSource);
            }).finally(function () {
                _this.isLoading = false;
            });
        };
        this.restoreSelectedOfItemsSource = function () {
            _this.ncmWidgetsAccessListGridService.restoreSelectedOfItems(_this.itemsSource);
        };
        this.onStatusChangedOnExpander = function (isOpen, gridItem) {
            if (isOpen) {
                // innerItemsSource is passed into child grid component,
                // we need to have updated 'selected' of given elements
                _this.ncmWidgetsAccessListGridService.restoreSelectedOfItems(gridItem.innerItemsSource);
            }
        };
        this.getAclHistoryGridItemsSource = function () {
            var itemsSource = new Array();
            if (_this.aclHistory === null) {
                return itemsSource;
            }
            _this.aclHistory.forEach(function (row) {
                var gridItem = new accessListGrid_service_1.GridItem();
                var currentAcl = row.history[0];
                gridItem.name = row.name;
                gridItem.nodeId = _this.nodeId;
                gridItem.configId = currentAcl.configId;
                gridItem.interfaces = currentAcl.interfaces;
                gridItem.modificationTime = currentAcl.modificationTime;
                gridItem.modificationTimeFormatted = currentAcl.modificationTimeFormatted;
                gridItem.selected = false;
                gridItem.innerItemsSource = _this.getAclHistoryGridInnerItemsSource(row.name, row.history.slice(1));
                gridItem.srdStatistics = currentAcl.srdStatistics;
                itemsSource.push(gridItem);
            });
            return itemsSource;
        };
        this.getAclHistoryGridInnerItemsSource = function (name, history) {
            var itemsSource = new Array();
            history.forEach(function (row) {
                var gridItem = new accessListGrid_service_1.GridItem();
                gridItem.name = name;
                gridItem.nodeId = _this.nodeId;
                gridItem.configId = row.configId;
                gridItem.interfaces = row.interfaces;
                gridItem.modificationTime = row.modificationTime;
                gridItem.modificationTimeFormatted = row.modificationTimeFormatted;
                gridItem.selected = false;
                gridItem.srdStatistics = row.srdStatistics;
                itemsSource.push(gridItem);
            });
            return itemsSource;
        };
        this.locateAsaIfTabUrl = function () {
            _this.ncmWidgetsNavigationService.getAsaIfSubviewId()
                .then(function (id) {
                if (id) {
                    _this.ncmWidgetsAccessListGridService.asaIfTabUrl =
                        "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + _this.nodeId + "&ViewID=" + id;
                }
            });
        };
        this.onCheckboxClick = function (gridItem) {
            _this.ncmWidgetsAccessListGridService.selectItem(gridItem);
        };
        this.isCheckboxDisabled = function (gridItem) {
            return _this.ncmWidgetsAccessListGridService.selection.length === 2 &&
                !_this.ncmWidgetsAccessListGridService.isItemAlredySelected(gridItem);
        };
        this.navigateToCompareAclDisabled = function () {
            return _this.ncmWidgetsAccessListGridService.selection.length !== 2;
        };
        this.navigateToAsaIfDetails = function (ifName) {
            var url = _this.getInterfaceUrl(ifName);
            if (url) {
                _this.ncmWidgetsNavigationService.navigateToUrl(url);
            }
        };
        this.navigateToInterfacesTab = function () {
            _this.ncmWidgetsNavigationService.navigateToUrl(_this.ncmWidgetsAccessListGridService.asaIfTabUrl);
        };
        this.isInterfaceAvailable = function (ifName) {
            return _this.getInterfaceUrl(ifName) !== undefined;
        };
        this.areInterfacesEnabled = function () {
            return !_.isEmpty(_this.ncmWidgetsAccessListGridService.ifUrlsMapping);
        };
        this.getInterfaceUrl = function (ifName) {
            return _this.ncmWidgetsAccessListGridService.ifUrlsMapping[ifName.toLowerCase()];
        };
        this.getCompareAclUrl = function (gridItem) {
            if (_this.ncmWidgetsAccessListGridService.selection.length >= 2) {
                var params = {
                    leftNodeId: _this.ncmWidgetsAccessListGridService.selection[1].nodeId,
                    leftConfigId: _this.ncmWidgetsAccessListGridService.selection[1].configId,
                    leftAclName: _this.ncmWidgetsAccessListGridService.selection[1].name,
                    rightNodeId: _this.ncmWidgetsAccessListGridService.selection[0].nodeId,
                    rightConfigId: _this.ncmWidgetsAccessListGridService.selection[0].configId,
                    rightAclName: _this.ncmWidgetsAccessListGridService.selection[0].name
                };
                return _this.ncmWidgetsNavigationService.getCompareAclUrl(params);
            }
        };
        this.onSearch = function (item, cancellation) {
            _this.ncmWidgetsAccessListGridService.searchTerm = item;
        };
        this.highlightSearch = function (name) {
            if (_this.ncmWidgetsAccessListGridService.searchTerm) {
                var pattern = new RegExp("(" + _this.ncmWidgetsAccessListGridService.searchTerm + ")", "gi");
                var text = name.replace(pattern, "<span class=\"acl-search-highlight\">$1</span>");
                return _this.$sce.trustAsHtml(text);
            }
            return name;
        };
    }
    Object.defineProperty(AccessListController.prototype, "getAclHistory", {
        get: function () {
            return this.aclHistory;
        },
        enumerable: true,
        configurable: true
    });
    return AccessListController;
}());
exports.AccessListController = AccessListController;


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var GridItem = /** @class */ (function () {
    function GridItem() {
    }
    return GridItem;
}());
exports.GridItem = GridItem;
var NcmWidgetsAccessListGridService = /** @class */ (function () {
    function NcmWidgetsAccessListGridService() {
        var _this = this;
        this.itemsAreEqual = function (item1, item2) {
            return item1.name === item2.name &&
                item1.nodeId === item2.nodeId &&
                item1.configId === item2.configId;
        };
        this.ifUrlsMapping = {};
        this.selectItem = function (item) {
            if (item) {
                item.selected ? _this.addItem(item) : _this.removeItem(item);
                _this.updateSelectedParent();
                _this.itemsSource
                    .reduce(function (acc, itemSource) {
                    return acc.concat(itemSource.innerItemsSource);
                }, [])
                    .filter(function (elem) {
                    return _this.itemsAreEqual(elem, item);
                })
                    .forEach(function (innerItemSource) {
                    innerItemSource.selected = item.selected;
                });
            }
            ;
        };
        this.restoreSelectedOfItems = function (items) {
            items.forEach(function (item) {
                item.selected = _this.isItemAlredySelected(item);
            });
        };
        this.reset = function () {
            _this.searchTerm = "";
            _this.selectedItems.length = 0;
        };
        this.isItemAlredySelected = function (item) {
            return _.some(_this.selectedItems, function (selectedItem) {
                return _this.itemsAreEqual(item, selectedItem);
            });
        };
        this.addItem = function (item) {
            _this.selectedItems.push(item);
        };
        this.removeItem = function (item) {
            _.remove(_this.selectedItems, function (currentItem) {
                return _this.itemsAreEqual(item, currentItem);
            });
        };
        this.updateSelectedParent = function () {
            _.forEach(_this._itemsSource, function (item) {
                item.selected = _this.isItemAlredySelected(item);
                if (item.innerItemsSource) {
                    _this.updateSelectedChild(item.innerItemsSource);
                }
            });
        };
        this.updateSelectedChild = function (items) {
            _.forEach(items, function (child) {
                child.selected = _this.isItemAlredySelected(child);
            });
        };
        this.selectedItems = new Array();
    }
    Object.defineProperty(NcmWidgetsAccessListGridService.prototype, "itemsSource", {
        get: function () {
            return this._itemsSource;
        },
        set: function (items) {
            this._itemsSource = items;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(NcmWidgetsAccessListGridService.prototype, "searchTerm", {
        get: function () {
            return this._searchTerm;
        },
        set: function (item) {
            this._searchTerm = item;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(NcmWidgetsAccessListGridService.prototype, "selection", {
        get: function () {
            return this.selectedItems;
        },
        enumerable: true,
        configurable: true
    });
    ;
    return NcmWidgetsAccessListGridService;
}());
exports.NcmWidgetsAccessListGridService = NcmWidgetsAccessListGridService;


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AclSrdIconController = /** @class */ (function () {
    /** @ngInject */
    AclSrdIconController.$inject = ["$scope", "$log"];
    function AclSrdIconController($scope, $log) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.activate = function () {
            _this.$log.debug("AclSrdIconController:Activate() for acl " + _this.model.name);
            _this.popoverDescriptions = _this.getPopoverDescriptions();
        };
        this.getSrdIconPopoverTemplate = { url: "ncm-acl-srd-popover-template" };
        this.isLoading = function () {
            return _this.model.srdStatistics == null;
        };
        this.canShowSrdIcon = function () {
            var stats = _this.model.srdStatistics;
            var result = stats == null ||
                stats.fullyRedundantCount > 0 ||
                stats.fullyShadowedCount > 0 ||
                stats.partiallyRedundantCount > 0 ||
                stats.partiallyShadowedCount > 0;
            return result;
        };
        this.getTotalSrdCount = function () {
            var stats = _this.model.srdStatistics;
            return stats.fullyRedundantCount + stats.fullyShadowedCount +
                stats.partiallyRedundantCount + stats.partiallyShadowedCount;
        };
        this.buildDescriptionViewModel = function (count, baseDescription, descriptions) {
            if (count > 0) {
                var description = count.toString() + " " + baseDescription;
                if (count > 1) {
                    description += "s";
                }
                descriptions.push(description);
            }
        };
        this.getPopoverDescriptions = function () {
            var stats = _this.model.srdStatistics;
            var descs = new Array();
            if (stats != null) {
                _this.buildDescriptionViewModel(stats.fullyShadowedCount, "fully shadowed rule", descs);
                _this.buildDescriptionViewModel(stats.partiallyShadowedCount, "partially shadowed rule", descs);
                _this.buildDescriptionViewModel(stats.fullyRedundantCount, "fully redundant rule", descs);
                _this.buildDescriptionViewModel(stats.partiallyRedundantCount, "partially redundant rule", descs);
            }
            return descs;
        };
        this.model = $scope.acl;
    }
    return AclSrdIconController;
}());
exports.AclSrdIconController = AclSrdIconController;


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Restrict;
(function (Restrict) {
    Restrict.element = "E";
    Restrict.attribute = "A";
    Restrict.className = "C";
    Restrict.comment = "M";
})(Restrict = exports.Restrict || (exports.Restrict = {}));
var BindingScope;
(function (BindingScope) {
    BindingScope.method = "&";
    BindingScope.twoWay = "=";
    BindingScope.oneWay = "@";
    BindingScope.optional = "?";
    BindingScope.oneWayOptional = BindingScope.oneWay + BindingScope.optional;
    BindingScope.twoWayOptional = BindingScope.twoWay + BindingScope.optional;
    BindingScope.methodOptional = BindingScope.method + BindingScope.optional;
})(BindingScope = exports.BindingScope || (exports.BindingScope = {}));


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(93);
module.exports = __webpack_require__(94);


/***/ }),
/* 93 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
window.SW = window.SW || {};
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
// include Xui repository module
var app_1 = __webpack_require__(95);
// include configuration file for angular module
var config_1 = __webpack_require__(96);
// include other sources
var index_1 = __webpack_require__(97);
var services_1 = __webpack_require__(98);
var decorators_1 = __webpack_require__(105);
var components_1 = __webpack_require__(106);
var directives_1 = __webpack_require__(182);
var templates_1 = __webpack_require__(200);
var orionModules_1 = __webpack_require__(219);
// execute imported functions and pass a module as parameter
config_1.default(app_1.default);
index_1.default(app_1.default);
components_1.default(app_1.default);
directives_1.default(app_1.default);
services_1.default(app_1.default);
decorators_1.default(app_1.default);
templates_1.default(app_1.default);
orionModules_1.default(app_1.default);
__export(__webpack_require__(0));
__export(__webpack_require__(2));
__export(__webpack_require__(7));
__export(__webpack_require__(309));


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("widgets.services", []);
angular.module("widgets.templates", []);
angular.module("widgets.components", [
    "i18n-gettext-tools",
    "filtered-list",
    "xui",
    "charts-d3",
    "orion-ui-components"
]);
angular.module("widgets.filters", []);
angular.module("widgets.providers", ["oc.lazyLoad"]);
angular.module("widgets", [
    "xui",
    "widgets.services",
    "widgets.templates",
    "widgets.components",
    "widgets.filters",
    "widgets.providers",
    "ngSanitize"
]);
// create and register Xui (Orion) module wrapper
var widgets = Xui.registerModule("widgets");
exports.default = widgets;


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run library: widgets");
}
exports.default = function (module) {
    module.app()
        .run(run);
};


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(9);
exports.default = function (module) {
    module.service("Constants", constants_1.default);
};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widget_service_1 = __webpack_require__(99);
var chartMetrics_service_1 = __webpack_require__(10);
var perfstackProject_service_1 = __webpack_require__(100);
var perfstackWidget_service_1 = __webpack_require__(14);
var perfstackDialog_service_1 = __webpack_require__(101);
var orionSettings_service_1 = __webpack_require__(104);
var user_service_1 = __webpack_require__(15);
exports.default = function (module) {
    module.service("swWidgetService", widget_service_1.WidgetService);
    module.service("swChartMetricsService", chartMetrics_service_1.ChartMetricsService);
    module.service("swPerfstackProjectService", perfstackProject_service_1.PerfstackProjectService);
    module.service("swPerfstackWidgetService", perfstackWidget_service_1.PerfstackWidgetService);
    module.service("swOrionSettingsService", orionSettings_service_1.OrionSettingsService);
    module.service("swUserService", user_service_1.UserService);
    perfstackDialog_service_1.default(module);
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetService = /** @class */ (function () {
    /** @ngInject */
    WidgetService.$inject = ["$log", "$timeout", "swApi", "swDemoService", "$q"];
    function WidgetService($log, $timeout, swApi, swDemoService, $q) {
        var _this = this;
        this.$log = $log;
        this.$timeout = $timeout;
        this.swApi = swApi;
        this.swDemoService = swDemoService;
        this.$q = $q;
        this.getWidgets = function (viewType, categoryName, searchTerm, groupName, pageNumber, pageSize, sortBy, sortDirection) {
            if (searchTerm === void 0) { searchTerm = ""; }
            if (groupName === void 0) { groupName = ""; }
            if (pageNumber === void 0) { pageNumber = 1; }
            if (pageSize === void 0) { pageSize = 20; }
            if (sortBy === void 0) { sortBy = ""; }
            if (sortDirection === void 0) { sortDirection = "ASC"; }
            var params = {
                request: {
                    SearchTerm: searchTerm,
                    GroupName: groupName,
                    CategoryName: categoryName,
                    ViewType: viewType
                }
            };
            var rowStart = (pageNumber - 1) * pageSize;
            return _this.swApi.ws.one("ResourcePickerWebServices.asmx")
                .post("GetListResources?start=" + rowStart + "&limit=" + pageSize + "&sort=" + sortBy + "&dir=" + sortDirection, params)
                .then(function (results) {
                var dataTable = _this.swApi.transformDataTable(results.d.DataTable, results.d.TotalRows);
                var groupTable = dataTable;
                groupTable.groupCounts = results.d.Metadata;
                return groupTable;
            });
        };
        this.getGroupCategories = function (groupName, viewType, sortBy, sortDirection) {
            if (sortBy === void 0) { sortBy = ""; }
            if (sortDirection === void 0) { sortDirection = "ASC"; }
            var params = {
                request: {
                    GroupName: groupName,
                    ViewType: viewType
                }
            };
            return _this.swApi.ws.one("ResourcePickerWebServices.asmx")
                .post("GetGroupcategories?sort=" + sortBy + "&dir=" + sortDirection, params)
                .then(function (results) {
                return _this.swApi.transformDataTable(results.d.DataTable, results.d.TotalRows);
            });
        };
        this.toggleFavorite = function (widget) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve(true);
            }
            var params = {
                request: {
                    IsFavorite: !widget.IsFavorite,
                    ResourcePath: widget.Path
                }
            };
            return _this.swApi.ws.one("ResourcePickerWebServices.asmx")
                .post("ToggleFavorite", params)
                .then(function () { return widget.IsFavorite = !widget.IsFavorite; });
        };
        this.findParentWidget = function (instanceElement) {
            if (instanceElement == null) {
                return null;
            }
            var wrapper = instanceElement.closest(".ResourceWrapper");
            if (wrapper.length === 0) {
                return null;
            }
            return wrapper;
        };
        this.hideParentWidget = function (instanceElement) {
            var widget = _this.findParentWidget(instanceElement);
            if (widget == null) {
                return false;
            }
            widget.addClass("HiddenResourceWrapper");
            return true;
        };
        this.isRequiredToCallRelationshipApi = function (opid, caller) {
            if (WidgetService.EntityCacheQueue[opid] === undefined) {
                WidgetService.EntityCacheQueue[opid] = [caller];
                return true;
            }
            else {
                WidgetService.EntityCacheQueue[opid].push(caller);
                return false;
            }
        };
        this.getEntityRelationships = function (opid) {
            var result = _this.$q.defer();
            var cachedItem = WidgetService.EntityCache[opid];
            if (cachedItem === undefined) {
                if (_this.isRequiredToCallRelationshipApi(opid, result)) {
                    _this.swApi.api(false).one("perfstack")
                        .one("entities/" + opid + "/relationships/")
                        .get()
                        .then(function (res) {
                        var ps = res.plain();
                        WidgetService.EntityCache[opid] = ps;
                        WidgetService.EntityCacheQueue[opid].forEach(function (v) {
                            v.resolve(ps);
                        });
                    })
                        .catch(function (err) {
                        _this.$log.warn("Promise rejected: ", err);
                        WidgetService.EntityCacheQueue[opid].forEach(function (v) {
                            v.reject(err);
                        });
                    })
                        .finally(function () {
                        WidgetService.EntityCacheQueue[opid] = undefined;
                    });
                }
            }
            else {
                result.resolve(cachedItem);
            }
            return result.promise;
        };
        this.showParentWidget = function (instanceElement) {
            var widget = _this.findParentWidget(instanceElement);
            if (widget == null) {
                return false;
            }
            widget.removeClass("HiddenResourceWrapper");
            return true;
        };
    }
    WidgetService.EntityCacheQueue = {};
    WidgetService.EntityCache = {};
    return WidgetService;
}());
exports.WidgetService = WidgetService;
exports.default = WidgetService;


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PerfstackProjectService = /** @class */ (function () {
    /** @ngInject */
    PerfstackProjectService.$inject = ["swApi"];
    function PerfstackProjectService(swApi) {
        this.swApi = swApi;
        this.endpoint = swApi.api(false).one("perfstack/projects/");
    }
    PerfstackProjectService.prototype.getProjects = function (projectId, parameters) {
        var _this = this;
        var endpoint = (projectId)
            ? this.endpoint.one(projectId)
            : this.endpoint;
        return endpoint
            .get(parameters)
            .then(function (value) { return _this.parseResponse(value.plain()); });
    };
    PerfstackProjectService.prototype.parseResponse = function (response) {
        return {
            items: _.map(response.data, this.mapProject),
            total: response.query.total
        };
    };
    PerfstackProjectService.prototype.mapProject = function (project) {
        var perfstackProject = {
            id: project.id,
            name: project.displayName,
            description: project.description,
            owner: project.owner,
            updated: moment(project.updateDateTime),
            charts: [],
            presetTime: null,
        };
        var innerData = angular.fromJson(project.data);
        // Get presetTime from the Data column
        if (innerData.presetTime) {
            perfstackProject.presetTime = innerData.presetTime;
        }
        if (innerData.charts) {
            perfstackProject.charts = _.map(_.filter(innerData.charts.split(";")), function (chart) { return chart.split(","); });
        }
        return perfstackProject;
    };
    return PerfstackProjectService;
}());
exports.PerfstackProjectService = PerfstackProjectService;


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var perfstackDialog_service_1 = __webpack_require__(102);
__webpack_require__(103);
exports.default = function (module) {
    module.service("swPerfstackDialogService", perfstackDialog_service_1.PerfstackDialogService);
};


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
var PerfstackDialogService = /** @class */ (function () {
    function PerfstackDialogService(xuiDialogService, _t) {
        this.xuiDialogService = xuiDialogService;
        this._t = _t;
    }
    PerfstackDialogService.prototype.showProjectSelectDialog = function () {
        var viewModel = {
            selection: { items: [] }
        };
        var selectedProject;
        var settings = {
            templateUrl: "[widgets]:/services/perfstackDialog-service/perfstackSelect-dialog.html"
        };
        var options = {
            title: this._t("Edit Widget: PerfStack Widget"),
            buttons: [
                {
                    name: "save",
                    text: this._t("Select"),
                    actionText: this._t("Saving widget settings..."),
                    isPrimary: true,
                    action: function () {
                        if (viewModel.selection.items.length) {
                            selectedProject = viewModel.selection.items[0];
                        }
                        return true;
                    }
                },
                {
                    name: "cancel",
                    text: this._t("Back"),
                    displayStyle: "tertiary",
                    icon: "caret-left",
                    pullLeft: true,
                    action: function () { return true; }
                }
            ],
            viewModel: viewModel
        };
        return this.xuiDialogService.showModal(settings, options).then(function () { return selectedProject; });
    };
    PerfstackDialogService.$inject = ["xuiDialogService", "getTextService"];
    return PerfstackDialogService;
}());
exports.PerfstackDialogService = PerfstackDialogService;


/***/ }),
/* 103 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var OrionSettingsService = /** @class */ (function () {
    /** @ngInject */
    OrionSettingsService.$inject = ["swApi"];
    function OrionSettingsService(swApi) {
        this.swApi = swApi;
    }
    OrionSettingsService.prototype.getSetting = function (settingId, defaultValue) {
        return this.swApi.api(true).one("orionsettings")
            .one("/" + settingId)
            .get()
            .then(function (result) {
            return result;
        }).catch(function () {
            return defaultValue;
        });
    };
    return OrionSettingsService;
}());
exports.OrionSettingsService = OrionSettingsService;
exports.default = OrionSettingsService;


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.default = function (module) {
    module.controller("swWidgetConfig", widget_decorator_1.WidgetConfig);
};


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var datalist_1 = __webpack_require__(107);
var tabular_1 = __webpack_require__(110);
var chart_1 = __webpack_require__(115);
var perfstack_1 = __webpack_require__(138);
var list_1 = __webpack_require__(153);
var tile_1 = __webpack_require__(157);
var widgetContainer_1 = __webpack_require__(165);
var genericList_1 = __webpack_require__(169);
__webpack_require__(181);
exports.default = function (module) {
    widgetContainer_1.default(module);
    datalist_1.default(module);
    tabular_1.default(module);
    chart_1.default(module);
    list_1.default(module);
    tile_1.default(module);
    perfstack_1.default(module);
    genericList_1.default(module);
};


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(16);
var datalist_widget_1 = __webpack_require__(108);
exports.default = function (module) {
    module.component("swDatalistWidget", datalist_widget_1.default);
};


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(16);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var datalist_widget_controller_1 = __webpack_require__(109);
var DatalistWidget = /** @class */ (function () {
    function DatalistWidget() {
        this.transclude = false;
        this.template = __webpack_require__(17);
        this.restrict = "E";
        this.scope = {};
        this.bindToController = {
            itemsSource: "<",
            trackBy: "@?",
            currentPage: "=?",
            pageSize: "=?",
            totalItems: "<?",
            sortColumns: "<?",
            sortSelection: "=?",
            itemTemplateUrl: "@?",
            showSearchSorterValue: "=?",
            emptySearch: "<?",
            onChange: "&?",
            onSearch: "&?",
            configSettings: "=?"
        };
        this.controller = datalist_widget_controller_1.default;
        this.controllerAs = "dataList";
    }
    return DatalistWidget;
}());
exports.default = DatalistWidget;


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var DatalistController = /** @class */ (function () {
    function DatalistController($log, $timeout, getTextService, $q) {
        var _this = this;
        this.$log = $log;
        this.$timeout = $timeout;
        this.getTextService = getTextService;
        this.$q = $q;
        this.searchValue = "";
        this.noSearchResults = {
            image: "no-search-results",
            description: "Try searching for something else, use the name of an object or a swql query."
        };
        this.onSortChange = function (newValue, oldValue) {
            _this.configSettings.settings.sorting = newValue;
            _this.configSettings.saveSettings();
        };
        this.onPageChanged = function (page, pageSize, total) {
            if (!_.isEqual(_this.pageSize, pageSize)) {
                _this.configSettings.settings.rowsperpage = pageSize;
                _this.configSettings.saveSettings();
            }
            _this.pageSize = pageSize;
            _this.totalItems = total;
            if (!_.isEqual(_this.currentPage, page)) {
                _this.currentPage = page;
                _this.reload();
            }
        };
        this.onSearchTriggered = function (value, cancellation) {
            if (!angular.isFunction(_this.onSearch)) {
                return _this.$q.resolve();
            }
            _this.searchValue = value;
            _this.updateNoSearchResultData(value);
            return _this.onSearch({ value: value, cancellation: cancellation });
        };
        this.reload = function () {
            if (!angular.isFunction(_this.onChange)) {
                return;
            }
            _this.$timeout(_this.onChange);
        };
        this.updateNoSearchResultData = function (searchValue) {
            _this.noSearchResults = _.assign(_this.noSearchResults, {
                title: "No result found for \"" + searchValue + "\""
            });
        };
        console.warn("DataList component is deprecated. For now it's not used anywhere.");
    }
    DatalistController.prototype.translate = function (text) {
        return this.getTextService(text);
    };
    DatalistController.$inject = ["$log", "$timeout", "getTextService"];
    return DatalistController;
}());
exports.default = DatalistController;


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var allAlerts_1 = __webpack_require__(111);
exports.default = function (module) {
    allAlerts_1.default(module);
};


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var alerts_service_1 = __webpack_require__(112);
var allAlerts_controller_1 = __webpack_require__(18);
var allAlerts_edit_controller_1 = __webpack_require__(113);
__webpack_require__(114);
exports.default = function (module) {
    module.service("swAlertsService", alerts_service_1.default);
    module.controller("swAllAlertsController", allAlerts_controller_1.AllAlertsController);
    module.controller("swAllAlertSettingsController", allAlerts_edit_controller_1.AllAlertSettingsController);
};


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AlertsService = /** @class */ (function () {
    function AlertsService($q, $rootScope, _t, swApi, swUtil, dialogService, toast, swDemoService) {
        var _this = this;
        this.$q = $q;
        this.$rootScope = $rootScope;
        this._t = _t;
        this.swApi = swApi;
        this.swUtil = swUtil;
        this.dialogService = dialogService;
        this.toast = toast;
        this.swDemoService = swDemoService;
        this.acknowledging = false;
        this.isCheckedEOC = true;
        this.severityVocabulary = {
            "0": { icon: "severity_info", color: "xui-color-info-border", order: 3, isFilter: true, label: this._t("Info") },
            "1": { icon: "severity_warning", color: "xui-color-warning-border", order: 2, isFilter: true, label: this._t("Warning") },
            "2": { icon: "severity_critical", color: "xui-color-critical-border", order: 1, isFilter: true, label: this._t("Critical") },
            "3": { icon: "severity_critical", color: "xui-color-critical-border", order: 1, isFilter: false, label: this._t("Serious") },
            "4": { icon: "severity_info", color: "xui-color-info-border", order: 3, isFilter: false, label: this._t("Notice") }
        };
        this.sortColumns = [
            { id: "[AlertName]", label: this._t("Alert Name") },
            { id: "[AlertMessage]", label: this._t("Alert Message") },
            { id: "[TriggeringObject]", label: this._t("Triggering Object") },
            { id: "[ActiveTime]", label: this._t("Active Time") },
            { id: "[Notes]", label: this._t("Notes") },
            { id: "[AcknowledgedByFullName]", label: this._t("Acknowledged By") },
            { id: "[RelatedNode]", label: this._t("Related Node") },
            { id: "[SeverityOrder]", label: this._t("Severity") }
        ];
        this.getAlerts = function (showAcknowledged, page, pageSize, orderBy, activeFilters, searchValue, showOnlyAcknowledged, instanceSiteIds, limitationsIds, relatedNodeEntityUri, relatedNodeId, triggeringObjectEntityNames, triggeringObjectEntityUris) {
            var params = [
                {
                    CurrentPageIndex: page - 1,
                    LimitationIds: limitationsIds ? limitationsIds : [],
                    OrderByClause: orderBy && orderBy.column ? orderBy.column + " " + orderBy.direction : "",
                    PageSize: pageSize,
                    RelatedNodeEntityUri: relatedNodeEntityUri,
                    RelatedNodeId: relatedNodeId,
                    ShowAcknowledgedAlerts: showAcknowledged,
                    ShowOnlyAcknowledgedAlerts: showOnlyAcknowledged,
                    TriggeringObjectEntityNames: triggeringObjectEntityNames,
                    TriggeringObjectEntityUris: triggeringObjectEntityUris,
                    Severity: _this.unMapSeverities(activeFilters),
                    FederationEnabled: _this.federationEnabled,
                    Search: "%" + searchValue + "%"
                },
                {
                    FederationEnabled: _this.federationEnabled,
                    WithoutAcknowledgedAlerts: !showAcknowledged,
                    ShowAcknowledgedAlerts: showAcknowledged,
                    ShowOnlyAcknowledgedAlerts: showOnlyAcknowledged,
                    property: "Severity",
                    LimitationIds: limitationsIds ? limitationsIds : [],
                    SearchValue: "%" + searchValue + "%",
                    TriggeringObjectEntityNames: triggeringObjectEntityNames,
                    TriggeringObjectEntityUris: triggeringObjectEntityUris
                }
            ];
            if (!_.isUndefined(_this.federationEnabled)) {
                _this.expandFederationParams(params, instanceSiteIds);
                return _this.makeAlertsRequest(params);
            }
            return _this.getFederationStatus()
                .then(function (features) {
                if (_.isArray(features)) {
                    _this.federationEnabled = !_.isEqual(features.indexOf("FederationEnabled"), -1);
                    _this.expandFederationParams(params, instanceSiteIds, true);
                    return _this.makeAlertsRequest(params);
                }
            });
        };
        this.acknowledgeAlert = function (alertId, note, siteId) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve(false);
            }
            var ackParams = {
                AlertObjectID: alertId,
                Note: note
            };
            if (_.isNumber(siteId)) {
                ackParams.SiteID = siteId;
            }
            return _this.swApi.api(true).one("ActiveAlertsGrid")
                .post("AcknowledgeActiveAlerts", [ackParams])
                .then(function (results) {
                _this.$rootScope.$broadcast("acknowledgeAlertEvent", alertId);
                _this.toast.success(_this._t("Alert Acknowledged"));
                _this.acknowledging = true;
                return true;
            });
        };
        this.getNodeStatus = function (rows) {
            var apiPayload = {
                uris: _.uniq(_.filter(_.map(rows, function (alert) { return alert.RelatedNodeEntityUri; })))
            };
            if (apiPayload.uris.length === 0) {
                return _this.$q.resolve([]);
            }
            return _this.swApi.api(true).one("AlertSuppression")
                .post("GetAlertSuppressionState", apiPayload)
                .then(function (result) {
                _.forEach(rows, function (row) {
                    _.forEach(result, function (item) {
                        if (_.isEqual(item.EntityUri, row.RelatedNodeEntityUri)) {
                            _.assign(row, { nodeStatus: item.SuppressionMode });
                        }
                    });
                });
                return rows;
            });
        };
        this.getFederationStatus = function () {
            return _this.swApi.api(false).one("features").getList();
        };
        this.getAvailableSites = function () {
            return _this.swApi.api(false).one("eoc/sites/").get();
        };
        this.makeAlertsRequest = function (params) {
            var dataTable = {
                rows: [],
                metadata: {
                    filters: [],
                    totalCounts: 0,
                },
                total: 0
            };
            return _this.$q.all([
                _this.swApi.api(true)
                    .one("ActiveAlertsOnThisEntity")
                    .post("GetActiveAlerts", params[0]),
                _this.swApi.api(true)
                    .one("ActiveAlertsGrid")
                    .post("GetAlertGroupValues", params[1])
            ]).then(function (results) {
                if (results[0].TotalRows > 0) {
                    dataTable = _this.transformDataTable(results[0].DataTable, results[1].DataTable.Rows, results[0].TotalRows);
                    _this.updateData(dataTable);
                }
                _this.data = dataTable;
                return _this.data;
            });
        };
        this.expandFederationParams = function (params, instanceSiteIds, firstCall) {
            if (instanceSiteIds === void 0) { instanceSiteIds = []; }
            if (firstCall === void 0) { firstCall = false; }
            if (_.isEqual(_this.federationEnabled, true)) {
                _.assign(params[0], { InstanceSiteIds: instanceSiteIds });
                _.assign(params[1], { InstanceSiteIds: instanceSiteIds });
                if (firstCall) {
                    if (_.isUndefined(_.find(_this.sortColumns, "[SiteName]"))) {
                        if (_this.isCheckedEOC) {
                            _this.sortColumns.push({ label: _this._t("Site Name"), id: "[SiteName]" });
                            _this.isCheckedEOC = false;
                        }
                    }
                }
            }
            if (firstCall) {
                _.forEach(params, function (param) {
                    _.assign(param, { FederationEnabled: _this.federationEnabled });
                });
            }
        };
        this.updateData = function (dataTable) {
            _.forEach(dataTable.rows, function (entity) {
                entity.severityLabel = _this.severityVocabulary[entity.Severity].label;
                entity.severityIcon = _this.severityVocabulary[entity.Severity].icon;
                entity.alertDetailsUrl = _this.getAlertDetailsUrl(entity);
                entity.options = {
                    tooltipText: entity.AlertName,
                    htmlBindTemplate: "item.AlertName | xuiHighlight:item.searchValue"
                };
                entity.optionsTrigger = {
                    tooltipText: entity.TriggeringObject,
                    htmlBindTemplate: "item.TriggeringObject | xuiHighlight:item.searchValue"
                };
                entity.optionsNode = {
                    tooltipText: entity.RelatedNode,
                    htmlBindTemplate: "item.RelatedNode | xuiHighlight:item.searchValue"
                };
                entity.acked = entity.AcknowledgedByFullName.length > 0;
                entity.openAckDialog = function () { return _this.openAckDialog(entity.AlertId, entity.SiteID); };
                entity.filterAlertMessage = function () { return _this.filterAlertMessage(entity.AlertMessage); };
                entity.federationEnabled = _this.federationEnabled;
                entity.getFullActiveDate = function () { return _this.getFullActiveDate(entity.LastTriggeredDateTime); };
                entity.muteNode = function () { return _this.changeNodeMute(entity, true); };
                entity.unmuteNode = function () { return _this.changeNodeMute(entity, false); };
                entity.isMuteVisible = function () {
                    return entity.RelatedNodeEntityUri && !entity.nodeStatus && (!entity.SiteID || entity.SiteID === 0);
                };
                entity.isUnmuteVisible = function () {
                    return entity.RelatedNodeEntityUri && entity.nodeStatus && (!entity.SiteID || entity.SiteID === 0);
                };
            });
        };
        this.transformDataTable = function (alerts, rows, totalRows) {
            var entities = _this.getEntities(alerts);
            var infoFilter = _this.getFilter(rows, ["0", "4"]);
            var warningFilter = _this.getFilter(rows, ["1"]);
            var criticalFilter = _this.getFilter(rows, ["2", "3"]);
            var filters = [infoFilter, warningFilter, criticalFilter]
                .filter(function (s) { return s.value !== 0; })
                .sort(_this.severitySorter);
            var totalCounts = _.sum(filters.map(function (af) { return af.value; }));
            return {
                rows: entities,
                metadata: { filters: filters, totalCounts: totalCounts },
                total: totalRows
            };
        };
        this.getElementCount = function (elements, keys) {
            return elements.filter(function (e) { return keys.indexOf(e[0]) !== -1; }).map(function (e) { return Number(e[1]); });
        };
        this.getEntities = function (dataTable) {
            var columns = dataTable.Columns;
            return _.map(dataTable.Rows, function (row) {
                var entity = {};
                for (var index = 0; index < columns.length; index++) {
                    entity[columns[index]] = row[index];
                }
                return entity;
            });
        };
        this.severitySorter = function (filter1, filter2) {
            var mapIndex = function (filter) {
                return _.findKey(_this.severityVocabulary, function (item) { return _.isEqual(item.icon, filter.name); });
            };
            return _this.severityVocabulary[mapIndex(filter1)].order - _this.severityVocabulary[mapIndex(filter2)].order;
        };
        this.filterAlertMessage = function (alertMessage) {
            return _.replace(alertMessage, / was triggered./g, "");
        };
        this.getFullActiveDate = function (lastTriggeredDateTime) {
            return moment(lastTriggeredDateTime).format("ddd D MMMM, YYYY, H:mm A");
        };
        this.changeNodeMute = function (alert, mute) {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return _this.$q.resolve();
            }
            var endpoint;
            var successMessage;
            var apiPaylod = {
                uris: [alert.RelatedNodeEntityUri]
            };
            if (mute) {
                endpoint = "SuppressAlerts";
                successMessage = _this.swUtil.formatString(_this._t("{0} has been successfully muted"), alert.RelatedNode);
            }
            else {
                endpoint = "ResumeAlerts";
                successMessage = _this.swUtil.formatString(_this._t("{0} has been successfully unmuted"), alert.RelatedNode);
            }
            return _this.swApi.api(true).one("AlertSuppression")
                .post(endpoint, apiPaylod)
                .then(function (results) {
                if (results) {
                    _this.toast.success(successMessage);
                    _this.getNodeStatus(_this.data.rows);
                }
            });
        };
        this.openAckDialog = function (alertId, siteId) {
            _this.acknowledging = false;
            var viewModel = { noteText: "" };
            var dialogOptions = {
                title: _this._t("Acknowledge Alert"),
                hideCancel: false,
                buttons: [{
                        name: "ack",
                        isPrimary: true,
                        text: _this._t("Acknowledge"),
                        actionText: _this._t("Acknowledging alert..."),
                        action: function () {
                            return _this.acknowledgeAlert(alertId, viewModel.noteText, siteId);
                        }
                    }],
                viewModel: viewModel
            };
            var customSettings = {
                templateUrl: "[widgets]:/components/tabular/allAlerts/allAlerts-ack-dialog.html"
            };
            return _this.dialogService
                .showModal(customSettings, dialogOptions);
        };
        this.unMapSeverities = function (activeFilters) {
            if (activeFilters && activeFilters.length) {
                var filterArray_1 = [];
                _.forEach(activeFilters, function (filter) {
                    if (_.isEqual(filter, "severity_critical")) {
                        filterArray_1.push("2", "3");
                    }
                    if (_.isEqual(filter, "severity_info")) {
                        filterArray_1.push("0", "4");
                    }
                    else {
                        filterArray_1.push(_.findKey(_this.severityVocabulary, function (item) { return _.isEqual(item.icon, filter); }));
                    }
                });
                return filterArray_1;
            }
        };
    }
    Object.defineProperty(AlertsService.prototype, "data", {
        get: function () {
            return this.dataTable;
        },
        set: function (dataTable) {
            this.dataTable = dataTable;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    AlertsService.prototype.getAlertDetailsUrl = function (item) {
        var prefix = this.federationEnabled
            ? "/Server/" + item.SiteID
            : "";
        return prefix + "/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:" + item.AlertId;
    };
    AlertsService.prototype.getFilter = function (elements, keys) {
        var sameCategoryElements = this.getElementCount(elements, keys);
        var sameCategoryElementCount = _.sum(sameCategoryElements);
        var firstGroup = this.severityVocabulary[keys[0]];
        return {
            name: firstGroup.icon,
            value: sameCategoryElementCount,
            emphasize: firstGroup.color,
            icon: firstGroup.icon
        };
    };
    AlertsService.$inject = [
        "$q", "$rootScope", "getTextService", "swApi", "swUtil", "xuiDialogService",
        "xuiToastService", "swDemoService"
    ];
    return AlertsService;
}());
exports.default = AlertsService;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetSettings_decorator_1 = __webpack_require__(2);
var allAlerts_controller_1 = __webpack_require__(18);
var inject_1 = __webpack_require__(4);
var AllAlertSettingsController = /** @class */ (function () {
    /** @ngInject */
    AllAlertSettingsController.$inject = ["_t", "$scope", "swAlertsService", "xuiToastService"];
    function AllAlertSettingsController(_t, $scope, swAlertsService, xuiToastService) {
        var _this = this;
        this._t = _t;
        this.$scope = $scope;
        this.swAlertsService = swAlertsService;
        this.xuiToastService = xuiToastService;
        this.$onInit = function () {
            _this.dataAcknowledgeList = [
                { name: _this._t("All"), value: true, id: 0 },
                { name: _this._t("Not Acknowledged Only"), value: false, id: 1 },
                { name: _this._t("Acknowledged Only"), value: true, id: 2 }
            ];
            var unbindWatch = _this.$scope.$watch(function () { return _this.settings; }, function () {
                if (_this.settings && _this.swAlertsService.federationEnabled) {
                    _this.initEocSettings();
                    unbindWatch();
                }
            });
        };
        this.onSave = function () {
            _this.xuiToastService.success(_this._t("Saved!"));
            if (!_this.settings.datafilteringenabled) {
                _this.settings.showacknowledgedalerts = _this.dataAcknowledgeList[1].value;
                _this.settings.showonlyacknowledgedalerts = false;
            }
            if (_.isEqual(_this.swAlertsService.federationEnabled, true)) {
                _this.saveEnabledSites();
            }
            return true;
        };
        this.onChange = function (newValue) {
            if (newValue.name === _this.dataAcknowledgeList[2].name) {
                _this.settings.showonlyacknowledgedalerts = newValue.value;
                _this.settings.showacknowledgedalerts = false;
            }
            else {
                _this.settings.showacknowledgedalerts = newValue.value;
                _this.settings.showonlyacknowledgedalerts = false;
            }
            _this.settings.acknowledgestate = newValue;
        };
        this.initEocSettings = function () {
            _this.eoc = {
                sitesList: [
                    { name: _this._t("All"), id: 0 },
                    { name: _this._t("Only Specific Site"), id: 1 },
                ],
                sitesAvailable: []
            };
            _this.getAvailableSites();
        };
        this.getAvailableSites = function () {
            return _this.swAlertsService.getAvailableSites().then(function (result) {
                _this.eoc.sitesAvailable = result;
                _.each(_this.eoc.sitesAvailable, function (site) {
                    site.enabled = false;
                    _.each(_this.settings.sitesenabled, function (siteId) {
                        if (_.isEqual(siteId, site.siteID)) {
                            site.enabled = true;
                        }
                    });
                });
                if (_.isUndefined(_this.settings.sitestate)) {
                    _this.settings.sitestate = _this.eoc.sitesList[0];
                }
            });
        };
        this.saveEnabledSites = function () {
            _this.settings.sitesenabled = [];
            if (_.isEqual(_this.settings.sitestate, _this.eoc.sitesList[0])) {
                return;
            }
            else if (_.isEqual(_this.settings.datafilteringenabled, false)) {
                _this.settings.sitestate = _this.eoc.sitesList[0];
            }
            _.each(_this.eoc.sitesAvailable, function (site) {
                if (_.isEqual(site.enabled, true)) {
                    _this.settings.sitesenabled.push(site.siteID);
                }
            });
            if (_.isEmpty(_this.settings.sitesenabled)) {
                _this.settings.sitestate = _this.eoc.sitesList[0];
            }
        };
    }
    AllAlertSettingsController = __decorate([
        widgetSettings_decorator_1.WidgetSettings({
            name: allAlerts_controller_1.WidgetSettingsName,
            templateUrl: "[widgets]:/components/tabular/allAlerts/allAlerts-edit-dialog.html",
            controllerAs: "alerts"
        }),
        __param(0, inject_1.default("getTextService")),
        __metadata("design:paramtypes", [Function, Object, Object, Object])
    ], AllAlertSettingsController);
    return AllAlertSettingsController;
}());
exports.AllAlertSettingsController = AllAlertSettingsController;


/***/ }),
/* 114 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var chartWidget_controller_1 = __webpack_require__(19);
var chartWidgetSettings_controller_1 = __webpack_require__(133);
__webpack_require__(134);
__webpack_require__(135);
var header_1 = __webpack_require__(136);
exports.default = function (module) {
    module.controller("swChartWidget", chartWidget_controller_1.ChartWidgetController);
    module.controller("swChartWidgetSettings", chartWidgetSettings_controller_1.ChartWidgetSettingsController);
    header_1.default(module);
};


/***/ }),
/* 116 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bisect__ = __webpack_require__(20);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "bisect", function() { return __WEBPACK_IMPORTED_MODULE_0__bisect__["c"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "bisectRight", function() { return __WEBPACK_IMPORTED_MODULE_0__bisect__["b"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "bisectLeft", function() { return __WEBPACK_IMPORTED_MODULE_0__bisect__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ascending__ = __webpack_require__(3);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ascending", function() { return __WEBPACK_IMPORTED_MODULE_1__ascending__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bisector__ = __webpack_require__(21);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "bisector", function() { return __WEBPACK_IMPORTED_MODULE_2__bisector__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cross__ = __webpack_require__(117);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "cross", function() { return __WEBPACK_IMPORTED_MODULE_3__cross__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__descending__ = __webpack_require__(118);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "descending", function() { return __WEBPACK_IMPORTED_MODULE_4__descending__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__deviation__ = __webpack_require__(23);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "deviation", function() { return __WEBPACK_IMPORTED_MODULE_5__deviation__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__extent__ = __webpack_require__(25);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "extent", function() { return __WEBPACK_IMPORTED_MODULE_6__extent__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__histogram__ = __webpack_require__(119);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "histogram", function() { return __WEBPACK_IMPORTED_MODULE_7__histogram__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__threshold_freedmanDiaconis__ = __webpack_require__(122);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "thresholdFreedmanDiaconis", function() { return __WEBPACK_IMPORTED_MODULE_8__threshold_freedmanDiaconis__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__threshold_scott__ = __webpack_require__(123);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "thresholdScott", function() { return __WEBPACK_IMPORTED_MODULE_9__threshold_scott__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__threshold_sturges__ = __webpack_require__(29);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "thresholdSturges", function() { return __WEBPACK_IMPORTED_MODULE_10__threshold_sturges__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__max__ = __webpack_require__(124);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "max", function() { return __WEBPACK_IMPORTED_MODULE_11__max__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__mean__ = __webpack_require__(125);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "mean", function() { return __WEBPACK_IMPORTED_MODULE_12__mean__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__median__ = __webpack_require__(126);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "median", function() { return __WEBPACK_IMPORTED_MODULE_13__median__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__merge__ = __webpack_require__(127);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "merge", function() { return __WEBPACK_IMPORTED_MODULE_14__merge__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__min__ = __webpack_require__(30);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "min", function() { return __WEBPACK_IMPORTED_MODULE_15__min__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pairs__ = __webpack_require__(22);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "pairs", function() { return __WEBPACK_IMPORTED_MODULE_16__pairs__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__permute__ = __webpack_require__(128);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "permute", function() { return __WEBPACK_IMPORTED_MODULE_17__permute__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__quantile__ = __webpack_require__(11);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "quantile", function() { return __WEBPACK_IMPORTED_MODULE_18__quantile__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__range__ = __webpack_require__(27);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "range", function() { return __WEBPACK_IMPORTED_MODULE_19__range__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__scan__ = __webpack_require__(129);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "scan", function() { return __WEBPACK_IMPORTED_MODULE_20__scan__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__shuffle__ = __webpack_require__(130);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "shuffle", function() { return __WEBPACK_IMPORTED_MODULE_21__shuffle__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__sum__ = __webpack_require__(131);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "sum", function() { return __WEBPACK_IMPORTED_MODULE_22__sum__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ticks__ = __webpack_require__(28);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "ticks", function() { return __WEBPACK_IMPORTED_MODULE_23__ticks__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "tickIncrement", function() { return __WEBPACK_IMPORTED_MODULE_23__ticks__["b"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "tickStep", function() { return __WEBPACK_IMPORTED_MODULE_23__ticks__["c"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__transpose__ = __webpack_require__(31);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "transpose", function() { return __WEBPACK_IMPORTED_MODULE_24__transpose__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__variance__ = __webpack_require__(24);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "variance", function() { return __WEBPACK_IMPORTED_MODULE_25__variance__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__zip__ = __webpack_require__(132);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "zip", function() { return __WEBPACK_IMPORTED_MODULE_26__zip__["a"]; });





























/***/ }),
/* 117 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pairs__ = __webpack_require__(22);


/* harmony default export */ __webpack_exports__["a"] = (function(values0, values1, reduce) {
  var n0 = values0.length,
      n1 = values1.length,
      values = new Array(n0 * n1),
      i0,
      i1,
      i,
      value0;

  if (reduce == null) reduce = __WEBPACK_IMPORTED_MODULE_0__pairs__["b" /* pair */];

  for (i0 = i = 0; i0 < n0; ++i0) {
    for (value0 = values0[i0], i1 = 0; i1 < n1; ++i1, ++i) {
      values[i] = reduce(value0, values1[i1]);
    }
  }

  return values;
});


/***/ }),
/* 118 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(a, b) {
  return b < a ? -1 : b > a ? 1 : b >= a ? 0 : NaN;
});


/***/ }),
/* 119 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__array__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__bisect__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constant__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__extent__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__identity__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__range__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ticks__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__threshold_sturges__ = __webpack_require__(29);









/* harmony default export */ __webpack_exports__["a"] = (function() {
  var value = __WEBPACK_IMPORTED_MODULE_4__identity__["a" /* default */],
      domain = __WEBPACK_IMPORTED_MODULE_3__extent__["a" /* default */],
      threshold = __WEBPACK_IMPORTED_MODULE_7__threshold_sturges__["a" /* default */];

  function histogram(data) {
    var i,
        n = data.length,
        x,
        values = new Array(n);

    for (i = 0; i < n; ++i) {
      values[i] = value(data[i], i, data);
    }

    var xz = domain(values),
        x0 = xz[0],
        x1 = xz[1],
        tz = threshold(values, x0, x1);

    // Convert number of thresholds into uniform thresholds.
    if (!Array.isArray(tz)) {
      tz = Object(__WEBPACK_IMPORTED_MODULE_6__ticks__["c" /* tickStep */])(x0, x1, tz);
      tz = Object(__WEBPACK_IMPORTED_MODULE_5__range__["a" /* default */])(Math.ceil(x0 / tz) * tz, x1, tz); // exclusive
    }

    // Remove any thresholds outside the domain.
    var m = tz.length;
    while (tz[0] <= x0) tz.shift(), --m;
    while (tz[m - 1] > x1) tz.pop(), --m;

    var bins = new Array(m + 1),
        bin;

    // Initialize bins.
    for (i = 0; i <= m; ++i) {
      bin = bins[i] = [];
      bin.x0 = i > 0 ? tz[i - 1] : x0;
      bin.x1 = i < m ? tz[i] : x1;
    }

    // Assign data to bins by value, ignoring any outside the domain.
    for (i = 0; i < n; ++i) {
      x = values[i];
      if (x0 <= x && x <= x1) {
        bins[Object(__WEBPACK_IMPORTED_MODULE_1__bisect__["c" /* default */])(tz, x, 0, m)].push(data[i]);
      }
    }

    return bins;
  }

  histogram.value = function(_) {
    return arguments.length ? (value = typeof _ === "function" ? _ : Object(__WEBPACK_IMPORTED_MODULE_2__constant__["a" /* default */])(_), histogram) : value;
  };

  histogram.domain = function(_) {
    return arguments.length ? (domain = typeof _ === "function" ? _ : Object(__WEBPACK_IMPORTED_MODULE_2__constant__["a" /* default */])([_[0], _[1]]), histogram) : domain;
  };

  histogram.thresholds = function(_) {
    return arguments.length ? (threshold = typeof _ === "function" ? _ : Array.isArray(_) ? Object(__WEBPACK_IMPORTED_MODULE_2__constant__["a" /* default */])(__WEBPACK_IMPORTED_MODULE_0__array__["b" /* slice */].call(_)) : Object(__WEBPACK_IMPORTED_MODULE_2__constant__["a" /* default */])(_), histogram) : threshold;
  };

  return histogram;
});


/***/ }),
/* 120 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(x) {
  return function() {
    return x;
  };
});


/***/ }),
/* 121 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(x) {
  return x;
});


/***/ }),
/* 122 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__array__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ascending__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__number__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__quantile__ = __webpack_require__(11);





/* harmony default export */ __webpack_exports__["a"] = (function(values, min, max) {
  values = __WEBPACK_IMPORTED_MODULE_0__array__["a" /* map */].call(values, __WEBPACK_IMPORTED_MODULE_2__number__["a" /* default */]).sort(__WEBPACK_IMPORTED_MODULE_1__ascending__["a" /* default */]);
  return Math.ceil((max - min) / (2 * (Object(__WEBPACK_IMPORTED_MODULE_3__quantile__["a" /* default */])(values, 0.75) - Object(__WEBPACK_IMPORTED_MODULE_3__quantile__["a" /* default */])(values, 0.25)) * Math.pow(values.length, -1 / 3)));
});


/***/ }),
/* 123 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__deviation__ = __webpack_require__(23);


/* harmony default export */ __webpack_exports__["a"] = (function(values, min, max) {
  return Math.ceil((max - min) / (3.5 * Object(__WEBPACK_IMPORTED_MODULE_0__deviation__["a" /* default */])(values) * Math.pow(values.length, -1 / 3)));
});


/***/ }),
/* 124 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(values, valueof) {
  var n = values.length,
      i = -1,
      value,
      max;

  if (valueof == null) {
    while (++i < n) { // Find the first comparable value.
      if ((value = values[i]) != null && value >= value) {
        max = value;
        while (++i < n) { // Compare the remaining values.
          if ((value = values[i]) != null && value > max) {
            max = value;
          }
        }
      }
    }
  }

  else {
    while (++i < n) { // Find the first comparable value.
      if ((value = valueof(values[i], i, values)) != null && value >= value) {
        max = value;
        while (++i < n) { // Compare the remaining values.
          if ((value = valueof(values[i], i, values)) != null && value > max) {
            max = value;
          }
        }
      }
    }
  }

  return max;
});


/***/ }),
/* 125 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__number__ = __webpack_require__(5);


/* harmony default export */ __webpack_exports__["a"] = (function(values, valueof) {
  var n = values.length,
      m = n,
      i = -1,
      value,
      sum = 0;

  if (valueof == null) {
    while (++i < n) {
      if (!isNaN(value = Object(__WEBPACK_IMPORTED_MODULE_0__number__["a" /* default */])(values[i]))) sum += value;
      else --m;
    }
  }

  else {
    while (++i < n) {
      if (!isNaN(value = Object(__WEBPACK_IMPORTED_MODULE_0__number__["a" /* default */])(valueof(values[i], i, values)))) sum += value;
      else --m;
    }
  }

  if (m) return sum / m;
});


/***/ }),
/* 126 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ascending__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__number__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__quantile__ = __webpack_require__(11);




/* harmony default export */ __webpack_exports__["a"] = (function(values, valueof) {
  var n = values.length,
      i = -1,
      value,
      numbers = [];

  if (valueof == null) {
    while (++i < n) {
      if (!isNaN(value = Object(__WEBPACK_IMPORTED_MODULE_1__number__["a" /* default */])(values[i]))) {
        numbers.push(value);
      }
    }
  }

  else {
    while (++i < n) {
      if (!isNaN(value = Object(__WEBPACK_IMPORTED_MODULE_1__number__["a" /* default */])(valueof(values[i], i, values)))) {
        numbers.push(value);
      }
    }
  }

  return Object(__WEBPACK_IMPORTED_MODULE_2__quantile__["a" /* default */])(numbers.sort(__WEBPACK_IMPORTED_MODULE_0__ascending__["a" /* default */]), 0.5);
});


/***/ }),
/* 127 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(arrays) {
  var n = arrays.length,
      m,
      i = -1,
      j = 0,
      merged,
      array;

  while (++i < n) j += arrays[i].length;
  merged = new Array(j);

  while (--n >= 0) {
    array = arrays[n];
    m = array.length;
    while (--m >= 0) {
      merged[--j] = array[m];
    }
  }

  return merged;
});


/***/ }),
/* 128 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(array, indexes) {
  var i = indexes.length, permutes = new Array(i);
  while (i--) permutes[i] = array[indexes[i]];
  return permutes;
});


/***/ }),
/* 129 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ascending__ = __webpack_require__(3);


/* harmony default export */ __webpack_exports__["a"] = (function(values, compare) {
  if (!(n = values.length)) return;
  var n,
      i = 0,
      j = 0,
      xi,
      xj = values[j];

  if (compare == null) compare = __WEBPACK_IMPORTED_MODULE_0__ascending__["a" /* default */];

  while (++i < n) {
    if (compare(xi = values[i], xj) < 0 || compare(xj, xj) !== 0) {
      xj = xi, j = i;
    }
  }

  if (compare(xj, xj) === 0) return j;
});


/***/ }),
/* 130 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(array, i0, i1) {
  var m = (i1 == null ? array.length : i1) - (i0 = i0 == null ? 0 : +i0),
      t,
      i;

  while (m) {
    i = Math.random() * m-- | 0;
    t = array[m + i0];
    array[m + i0] = array[i + i0];
    array[i + i0] = t;
  }

  return array;
});


/***/ }),
/* 131 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function(values, valueof) {
  var n = values.length,
      i = -1,
      value,
      sum = 0;

  if (valueof == null) {
    while (++i < n) {
      if (value = +values[i]) sum += value; // Note: zero and null are equivalent.
    }
  }

  else {
    while (++i < n) {
      if (value = +valueof(values[i], i, values)) sum += value;
    }
  }

  return sum;
});


/***/ }),
/* 132 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__transpose__ = __webpack_require__(31);


/* harmony default export */ __webpack_exports__["a"] = (function() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__transpose__["a" /* default */])(arguments);
});


/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetSettings_decorator_1 = __webpack_require__(2);
var chartWidget_controller_1 = __webpack_require__(19);
var ChartWidgetSettingsController = /** @class */ (function () {
    /** @ngInject */
    ChartWidgetSettingsController.$inject = ["$log", "$timeout"];
    function ChartWidgetSettingsController($log, $timeout) {
        var _this = this;
        this.$log = $log;
        this.$timeout = $timeout;
        this.$onInit = function () {
            _this.$timeout(function () {
                var config = _this.settings.chartconfiguration;
                var stats = config.statistics;
                if (stats) {
                    stats.percentileValue = stats.percentileValue || 95;
                }
                _this.statisticGroupVisible = stats && (stats.trends || stats.percentiles || stats.thresholds);
                _this.allowStatistic = _.filter(config.charts, function (c) { return c.isSpark; }).length === 0;
                _this.allowThresholds = config.allowThresholds;
            });
        };
        this.onStatisticValueChange = function (newConfig) {
            var config = _this.settings.chartconfiguration;
            var stats = config.statistics || (config.statistics = {});
            stats.trends = stats.percentiles = _this.statisticGroupVisible;
            stats.thresholds = (_this.allowThresholds === true) && _this.statisticGroupVisible;
        };
        this.onInit = function (viewModel) {
            _this.settings = viewModel.settings;
        };
        this.setPercentileValue = function (percentile) {
            _this.settings.chartconfiguration.statistics.percentileValue = percentile;
        };
        this.getPercentilePopoverTemplate = function () { return __webpack_require__(34); };
        this.returnQuickPics = function () {
            return [95, 90, 85, 75];
        };
    }
    ChartWidgetSettingsController = __decorate([
        widgetSettings_decorator_1.WidgetSettings({
            name: chartWidget_controller_1.WidgetSettingsName,
            template: __webpack_require__(35),
            controllerAs: "chart"
        }),
        __metadata("design:paramtypes", [Object, Function])
    ], ChartWidgetSettingsController);
    return ChartWidgetSettingsController;
}());
exports.ChartWidgetSettingsController = ChartWidgetSettingsController;


/***/ }),
/* 134 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 135 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var chartHeader_controller_1 = __webpack_require__(137);
exports.default = function (module) {
    module.controller("swChartWidgetHeader", chartHeader_controller_1.ChartHeaderController);
};


/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetPartialTemplate_decorator_1 = __webpack_require__(7);
var ChartHeaderController = /** @class */ (function () {
    function ChartHeaderController() {
    }
    ChartHeaderController.prototype.customLinks = function () {
        return this.widget.customLinks;
    };
    ChartHeaderController = __decorate([
        widgetPartialTemplate_decorator_1.WidgetPartialTemplate({
            name: "swChartWidgetHeader",
            template: __webpack_require__(36),
            controllerAs: "ctrl"
        })
    ], ChartHeaderController);
    return ChartHeaderController;
}());
exports.ChartHeaderController = ChartHeaderController;


/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var perfstackWidget_controller_1 = __webpack_require__(37);
var perfstackHeader_1 = __webpack_require__(139);
var perfstackProjectSelector_1 = __webpack_require__(142);
var perfstackEmpty_1 = __webpack_require__(147);
var perfstackWidgetSettings_1 = __webpack_require__(150);
__webpack_require__(152);
exports.default = function (module) {
    module.controller("swPerfstackWidget", perfstackWidget_controller_1.PerfstackWidgetController);
    perfstackHeader_1.default(module);
    perfstackProjectSelector_1.default(module);
    perfstackEmpty_1.default(module);
    perfstackWidgetSettings_1.default(module);
};


/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var perfstackHeader_controller_1 = __webpack_require__(140);
__webpack_require__(141);
exports.default = function (module) {
    module.controller("swPerfstackHeader", perfstackHeader_controller_1.PerfstackHeaderController);
};


/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetPartialTemplate_decorator_1 = __webpack_require__(7);
var PerfstackHeaderController = /** @class */ (function () {
    function PerfstackHeaderController() {
    }
    PerfstackHeaderController.prototype.projectId = function () {
        if (this.widget.model && this.widget.model.project) {
            return this.widget.model.project.id;
        }
    };
    PerfstackHeaderController = __decorate([
        widgetPartialTemplate_decorator_1.WidgetPartialTemplate({
            name: "swPerfstackHeader",
            template: __webpack_require__(39),
            controllerAs: "ctrl"
        })
    ], PerfstackHeaderController);
    return PerfstackHeaderController;
}());
exports.PerfstackHeaderController = PerfstackHeaderController;


/***/ }),
/* 141 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var perfstackProjectSelector_1 = __webpack_require__(143);
__webpack_require__(146);
exports.default = function (module) {
    module.component("swPerfstackProjectSelector", perfstackProjectSelector_1.PerfstackProjectSelector);
};


/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(144);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var perfstackProjectSelector_controller_1 = __webpack_require__(145);
var PerfstackProjectSelector = /** @class */ (function () {
    function PerfstackProjectSelector() {
        this.controller = perfstackProjectSelector_controller_1.PerfstackProjectSelectorController;
        this.controllerAs = "ctrl";
        this.template = __webpack_require__(40);
        this.restrict = "E";
        this.scope = {};
        this.bindToController = {
            selection: "=?"
        };
    }
    return PerfstackProjectSelector;
}());
exports.PerfstackProjectSelector = PerfstackProjectSelector;


/***/ }),
/* 144 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PerfstackProjectSelectorController = /** @class */ (function () {
    function PerfstackProjectSelectorController(swPerfstackProjectService, $timeout, _t) {
        var _this = this;
        this.swPerfstackProjectService = swPerfstackProjectService;
        this.$timeout = $timeout;
        this._t = _t;
        this.$onInit = function () {
            _this.pagination = {
                page: 1,
                pageSize: 5,
                total: 0
            };
            var columns = [
                { id: "displayName", label: _this._t("Name") },
                { id: "updateDateTime", label: _this._t("Updated") }
            ];
            _this.sorting = {
                sortableColumns: columns,
                sortBy: columns[1],
                direction: "desc"
            };
            _this.items = [];
            _this.requestItems();
        };
    }
    PerfstackProjectSelectorController.prototype.onSearch = function (term) {
        var _this = this;
        this.searchTerm = term || this.searchTerm;
        this.$timeout(function () { return _this.requestItems(); });
    };
    PerfstackProjectSelectorController.prototype.onClear = function () {
        this.searchTerm = undefined;
        this.onSearch();
    };
    PerfstackProjectSelectorController.prototype.requestItems = function (overrides) {
        var _this = this;
        var params = this.buildParameters(overrides);
        this.swPerfstackProjectService
            .getProjects(undefined, params)
            .then(function (result) {
            _this.items = result.items;
            _this.pagination.total = result.total;
        });
    };
    PerfstackProjectSelectorController.prototype.buildParameters = function (overrides) {
        var params = {
            length: this.pagination.pageSize,
            offset: (this.pagination.page - 1) * this.pagination.pageSize,
            displayName: this.searchTerm,
            orderBy: this.sorting.sortBy.id,
            sort: this.sorting.direction
        };
        return _.assign({}, params, overrides);
    };
    PerfstackProjectSelectorController.$inject = ["swPerfstackProjectService", "$timeout", "getTextService"];
    return PerfstackProjectSelectorController;
}());
exports.PerfstackProjectSelectorController = PerfstackProjectSelectorController;


/***/ }),
/* 146 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var perfstackEmpty_1 = __webpack_require__(148);
exports.default = function (module) {
    module.component("swPerfstackEmpty", perfstackEmpty_1.PerfstackEmpty);
};


/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(149);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PerfstackEmpty = /** @class */ (function () {
    function PerfstackEmpty() {
        this.template = __webpack_require__(41);
        this.restrict = "E";
        this.scope = {
            onClick: "&"
        };
    }
    return PerfstackEmpty;
}());
exports.PerfstackEmpty = PerfstackEmpty;


/***/ }),
/* 149 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var perfstackWidgetSettings_controller_1 = __webpack_require__(151);
exports.default = function (module) {
    module.controller("swPerfstackWidgetSettings", perfstackWidgetSettings_controller_1.PerfstackWidgetSettingsController);
};


/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetSettings_decorator_1 = __webpack_require__(2);
var inject_1 = __webpack_require__(4);
var perfstackWidget_controller_1 = __webpack_require__(37);
var PerfstackWidgetSettingsController = /** @class */ (function () {
    function PerfstackWidgetSettingsController($log, swPerfstackDialogService) {
        var _this = this;
        this.$log = $log;
        this.swPerfstackDialogService = swPerfstackDialogService;
        this.onInit = function (viewModel) {
            _this.settings = viewModel.settings;
            viewModel.options.titleReadOnly = true;
        };
    }
    PerfstackWidgetSettingsController.prototype.onChooseProject = function () {
        var _this = this;
        return this.swPerfstackDialogService
            .showProjectSelectDialog()
            .then(function (project) {
            if (project) {
                _this.settings.projectid = project.id;
                _this.settings.title = project.name;
            }
        });
    };
    PerfstackWidgetSettingsController = __decorate([
        widgetSettings_decorator_1.WidgetSettings({
            name: perfstackWidget_controller_1.WidgetSettingsName,
            template: __webpack_require__(42),
            controllerAs: "vmPerfstackSettings"
        }),
        __param(0, inject_1.Inject("$log")),
        __param(1, inject_1.Inject("swPerfstackDialogService")),
        __metadata("design:paramtypes", [Object, Object])
    ], PerfstackWidgetSettingsController);
    return PerfstackWidgetSettingsController;
}());
exports.PerfstackWidgetSettingsController = PerfstackWidgetSettingsController;


/***/ }),
/* 152 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var list_controller_1 = __webpack_require__(43);
var list_edit_controller_1 = __webpack_require__(154);
var plugins_1 = __webpack_require__(155);
exports.default = function (module) {
    module.controller("swListWidget", list_controller_1.ListController);
    module.controller("swListWidgetSettings", list_edit_controller_1.ListSettingsController);
    plugins_1.default(module);
};


/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetSettings_decorator_1 = __webpack_require__(2);
var list_controller_1 = __webpack_require__(43);
var ListSettingsController = /** @class */ (function () {
    /** @ngInject */
    ListSettingsController.$inject = ["$log"];
    function ListSettingsController($log) {
        var _this = this;
        this.$log = $log;
        this.pluginText = function () {
            var type = _this.settings.listconfiguration.pluginType;
            return "<sw-list-plugin-" + type + "></sw-list-plugin-" + type + ">";
        };
        this.onInit = function (viewModel) {
            _this.settings = viewModel.settings;
        };
        this.onSave = function () {
            return true;
        };
    }
    ListSettingsController = __decorate([
        widgetSettings_decorator_1.WidgetSettings({
            name: list_controller_1.WidgetSettingsName,
            template: __webpack_require__(45),
            controllerAs: "list"
        }),
        __metadata("design:paramtypes", [Object])
    ], ListSettingsController);
    return ListSettingsController;
}());
exports.ListSettingsController = ListSettingsController;


/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SWQL_directive_1 = __webpack_require__(156);
exports.default = function (module) {
    module.component("swListPluginSwql", SWQL_directive_1.SWQLPlugin);
};


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SWQLPlugin = /** @class */ (function () {
    /** @ngInject */
    SWQLPlugin.$inject = ["$log", "pollingService"];
    function SWQLPlugin($log, pollingService) {
        this.$log = $log;
        this.pollingService = pollingService;
        this.template = __webpack_require__(46);
        this.restrict = "E";
    }
    return SWQLPlugin;
}());
exports.SWQLPlugin = SWQLPlugin;


/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var tile_controller_1 = __webpack_require__(47);
__webpack_require__(158);
__webpack_require__(159);
var tile_service_1 = __webpack_require__(160);
var tile_edit_controller_1 = __webpack_require__(161);
var basic_filter_dialog_service_1 = __webpack_require__(162);
var advanced_filter_dialog_service_1 = __webpack_require__(163);
var main_filter_dialog_service_1 = __webpack_require__(164);
exports.default = function (module) {
    module.controller("swTileController", tile_controller_1.TileController);
    module.controller("swTileWidgetSettings", tile_edit_controller_1.default);
    module.service("swTileService", tile_service_1.default);
    module.service("swMainFilterDialogService", main_filter_dialog_service_1.MainFilterDialogService);
    module.service("swBasicFilterDialogService", basic_filter_dialog_service_1.BasicFilterDialogService);
    module.service("swAdvancedFilterDialogService", advanced_filter_dialog_service_1.AdvancedFilterDialogService);
};


/***/ }),
/* 158 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 159 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var TileService = /** @class */ (function () {
    /** @ngInject */
    TileService.$inject = ["$log", "swApi"];
    function TileService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.loadTileInfo = function (siteIDs, tile) {
            _this.$log.info("TileService.getTiles called");
            return _this.swApi
                .api(false)
                .one("widgets/tile")
                .post("loadtileinfo", { siteIds: siteIDs, tile: tile });
        };
        this.getManagedEntities = function () {
            _this.$log.info("TileService.managedentities called");
            return _this.swApi
                .api(false)
                .one("widgets/tile/managedentities")
                .get();
        };
        this.getAvailableFields = function (entity) {
            _this.$log.info("TileService.getAvailableFields called");
            return _this.swApi
                .api(false)
                .one("widgets/tile/availablefields")
                .get({ entity: entity });
        };
        this.getAvailableFieldValues = function (entity, field) {
            _this.$log.info("TileService.getAvailableFields called");
            return _this.swApi
                .api(false)
                .one("widgets/tile/availablefieldvalues")
                .get({ entity: entity, field: field });
        };
        this.getGroups = function () {
            _this.$log.info("TileService.getGroups called");
            return _this.swApi
                .api(false)
                .one("widgets/tile/groups")
                .get();
        };
        this.getFederationEnabled = function () {
            _this.$log.info("TileService.getFederationEnabled called");
            return _this.swApi
                .api(false)
                .one("widgets/tile/isFederationEnabled")
                .get();
        };
        this.getRefreshInterval = function () {
            _this.$log.info("TileService.getRefreshInterval called");
            return _this.swApi
                .api(false)
                .one("widgets/tile/refreshInterval")
                .get();
        };
        this.getAllSitesDetails = function (siteIDs) {
            _this.$log.info("ManageSitesService.getAllSites called");
            return _this.swApi.api(false)
                .one("eoc/managesites/")
                .post("statussites", siteIDs);
        };
        this.getFilterOperations = function () {
            _this.$log.info("TileService.getFilterOperations called");
            return _this.swApi.api(false).one("widgets/tile/filteroperations").get();
        };
        this.getSites = function () {
            _this.$log.info("TileService.getSites called");
            return _this.swApi.api(false).one("eoc/sites/").get();
        };
        this.buildTile = function (tile) {
            _this.$log.info("TileService.addTile called");
            return _this.swApi
                .api(false)
                .one("widgets/tile")
                .post("buildtile", tile);
        };
        this.buildAdvancedTileQueries = function (tile) {
            _this.$log.info("TileService.buildAdvancedTileQueries called");
            return _this.swApi
                .api(false)
                .one("widgets/tile")
                .post("advancedqueries", tile);
        };
        this.validateTileQueries = function (swql, listSwql, otherIssuesSwql) {
            _this.$log.info("TileService.validateTileQueries called");
            return _this.swApi
                .api(false)
                .one("widgets/tile")
                .post("validatequery", { swql: swql, listSwql: listSwql, otherIssuesSwql: otherIssuesSwql });
        };
        this.tileFilterChanged = function (existedSites, selectedSites) {
            if (!selectedSites) {
                return false;
            }
            var val = !((selectedSites.length === existedSites.length) &&
                _.isEqual(existedSites.sort(), selectedSites.sort()));
            return val;
        };
    }
    return TileService;
}());
exports.TileService = TileService;
exports.default = TileService;


/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetSettings_decorator_1 = __webpack_require__(2);
var tile_controller_1 = __webpack_require__(47);
var TileSettingsController = /** @class */ (function () {
    /** @ngInject */
    TileSettingsController.$inject = ["$scope", "$log", "$q", "swMainFilterDialogService", "swTileService", "swBasicFilterDialogService", "swAdvancedFilterDialogService"];
    function TileSettingsController($scope, $log, $q, swMainFilterDialogService, swTileService, swBasicFilterDialogService, swAdvancedFilterDialogService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$q = $q;
        this.swMainFilterDialogService = swMainFilterDialogService;
        this.swTileService = swTileService;
        this.swBasicFilterDialogService = swBasicFilterDialogService;
        this.swAdvancedFilterDialogService = swAdvancedFilterDialogService;
        this.$onInit = function () {
            var unbindWatch = _this.$scope.$watch(function () { return _this.settings; }, function () {
                if (_this.settings) {
                    _this.swMainFilterDialogService.initializeDialog(_this.settings);
                    unbindWatch();
                }
            });
        };
        this.openBasicFilterDialog = function () {
            _this.syncSelectedEntities();
            var vm = _this.swMainFilterDialogService.viewModel;
            if (vm.hasUpdatedFilter) {
                _this.settings.basicfilter = _this.swMainFilterDialogService.viewModel.basicFilter;
                vm.hasUpdatedFilter = false;
            }
            var entities = _this.settings.basicfilter && _this.settings.basicfilter.length > 0 ?
                _this.settings.basicfilter : _this.swMainFilterDialogService.viewModel.selectedEntities;
            var originalEntities = angular.copy(entities);
            _this.swBasicFilterDialogService.showDialog(originalEntities, _this.swMainFilterDialogService.viewModel.availableEntities);
        };
        this.openAdvancedFilterDialog = function () {
            var tile = _this.swMainFilterDialogService.getTileFromUi();
            _this.swAdvancedFilterDialogService.showDialog(tile);
        };
        this.onSave = function () {
            var vm = _this.swMainFilterDialogService.viewModel;
            vm.firstTileLoad = false;
            if (!_this.swMainFilterDialogService.isDialogValid()) {
                return false;
            }
            if (vm.isAdvancedMode) {
                return _this.saveAdvancedModeTile();
            }
            _this.syncEntitiesAndBasicFilter();
            var tile = _this.swMainFilterDialogService.getTileFromUi();
            return _this.saveTile(tile);
        };
        this.saveTile = function (tile) {
            return _this.swTileService
                .buildTile(tile)
                .then(function (data) {
                if (data) {
                    _this.settings.entity = data.entity;
                    _this.settings.swql = data.swql;
                    _this.settings.listswql = data.listSwql;
                    _this.settings.otherissuesswql = data.otherIssuesSwql;
                    _this.settings.listtemplate = data.listTemplate;
                    _this.settings.filter = data.filter;
                    _this.settings.basicfilter = tile.basicFilter;
                    _this.settings.tiletype = tile.tileType;
                    _this.settings.showalerts = tile.showAlerts;
                }
                return true;
            });
        };
        this.syncEntitiesAndBasicFilter = function () {
            var vm = _this.swMainFilterDialogService.viewModel;
            var selectedEntities = vm.selectedEntities;
            if (vm.basicFilter && vm.basicFilter.length > 0) {
                var newEntities_1 = [];
                var selectedEntitiesNames = _(selectedEntities
                    .map(function (entity) { return entity.entityName; })).uniq().value();
                _.each(selectedEntitiesNames, function (entityName) {
                    var existingEntities = _.filter(vm.basicFilter, function (property) {
                        return property.entityName === entityName;
                    });
                    if (existingEntities.length > 0) {
                        newEntities_1.push.apply(newEntities_1, existingEntities);
                    }
                });
                vm.basicFilter = newEntities_1;
            }
        };
        this.syncSelectedEntities = function () {
            var selectedEntities = _this.swMainFilterDialogService.viewModel.selectedEntities;
            var selectedEntitiesNames = _(selectedEntities
                .map(function (entity) { return entity.entityName; })).uniq().value();
            if (_this.swMainFilterDialogService.viewModel.basicFilter &&
                _this.swMainFilterDialogService.viewModel.basicFilter.length > 0) {
                var newEntities_2 = [];
                _.each(selectedEntitiesNames, function (selectedEntitiesName) {
                    var existingEntities = _.filter(_this.swMainFilterDialogService.viewModel.basicFilter, function (property) {
                        return property.entityName === selectedEntitiesName;
                    });
                    if (existingEntities.length > 0) {
                        newEntities_2.push.apply(newEntities_2, existingEntities);
                    }
                    else {
                        var existingEntity = _.find(selectedEntities, function (property) {
                            return property.entityName === selectedEntitiesName;
                        });
                        newEntities_2.push(existingEntity);
                    }
                });
                _this.settings.basicfilter = newEntities_2;
                _this.swMainFilterDialogService.viewModel.basicFilter = newEntities_2;
            }
        };
        this.saveAdvancedModeTile = function () {
            var tile = _this.swMainFilterDialogService.getTileFromUi();
            tile.basicFilter = [];
            var vm = _this.swMainFilterDialogService.viewModel;
            return _this.swTileService.validateTileQueries(vm.swqlTileQuery, vm.swqlAssetQuery, vm.swqlAlertQuery).then(function (result) {
                vm.tileQueryError = result.swqlError;
                vm.assetExplorerQueryError = result.listSwqlError;
                vm.alertQueryError = result.otherIssuesSwqlError;
                if (result.swqlError || result.otherIssuesSwqlError || result.listSwqlError) {
                    return false;
                }
                return _this.saveTile(tile);
            });
        };
    }
    TileSettingsController = __decorate([
        widgetSettings_decorator_1.WidgetSettings({
            name: tile_controller_1.WidgetSettingsName,
            templateUrl: "[widgets]:/components/tile/dialogs/mainFilter/main-filter-dialog.html",
            controllerAs: "tile"
        }),
        __metadata("design:paramtypes", [Object, Object, Function, Object, Object, Object, Object])
    ], TileSettingsController);
    return TileSettingsController;
}());
exports.default = TileSettingsController;


/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
;
;
;
;
var BasicFilterDialogService = /** @class */ (function () {
    /** @ngInject */
    BasicFilterDialogService.$inject = ["getTextService", "xuiDialogService", "swTileService", "swMainFilterDialogService", "xuiEdgeDetectionService"];
    function BasicFilterDialogService(getTextService, xuiDialogService, swTileService, swMainFilterDialogService, xuiEdgeDetectionService) {
        var _this = this;
        this.getTextService = getTextService;
        this.xuiDialogService = xuiDialogService;
        this.swTileService = swTileService;
        this.swMainFilterDialogService = swMainFilterDialogService;
        this.xuiEdgeDetectionService = xuiEdgeDetectionService;
        this._t = this.getTextService;
        this.onFilteredFieldChanged = function (newField, oldField, entityIndex, selectedEntity, value) {
            var condition = selectedEntity.conditions[entityIndex];
            if (!condition) {
                return;
            }
            condition.isBusy = true;
            _this.swTileService.getAvailableFieldValues(newField.entity, newField.name).then(function (result) {
                condition.availableValues = result.Result;
                if (value) {
                    condition.value = value;
                }
                else {
                    condition.value = {};
                }
                condition.isBusy = false;
            });
        };
        this.onEntityAdded = function () {
            var vm = _this.dialogOptions.viewModel;
            var entity = {
                entityDisplayName: vm.availableEntities[0].entityDisplayName,
                entityName: vm.availableEntities[0].entityName
            };
            vm.selectedEntities.push(entity);
            _this.onEntityChanged(entity, null, vm.selectedEntities.length - 1);
        };
        this.onEntityRemoved = function (entityIndex) {
            _this.dialogOptions.viewModel.selectedEntities.splice(entityIndex, 1);
        };
        this.onEntityChanged = function (newField, oldField, entityIndex) {
            newField.isBusy = true;
            var fields = [];
            var conditions = [];
            var entity = {
                entityDisplayName: newField.entityDisplayName,
                entityName: newField.entityName,
                isBusy: true,
                conditions: conditions,
                availableFields: fields
            };
            _this.dialogOptions.viewModel.selectedEntities[entityIndex] = entity;
            _this.swTileService.getAvailableFields(newField.entityName).then(function (result) {
                entity.availableFields = result.Result;
                entity.isBusy = false;
            });
        };
        this.onFilteredRemoved = function (selectedEntity, entityIndex) {
            selectedEntity.conditions.splice(entityIndex, 1);
        };
        this.adjustMenuPosition = function (event) {
            var element = $(event.currentTarget);
            var menu = angular.element(element.find(".xui-dropdown__menu"));
            var button = angular.element(element.find(".xui-dropdown__toggle-button"));
            //when widget is opened in separate page need to temporary remove xui-edge-definer from sw-widget-container
            //because of multiple elements with xui-edge-definer class
            angular.element(".sw-widget-container.xui-edge-definer").removeClass("xui-edge-definer");
            var canBe = _this.xuiEdgeDetectionService.canBe(button, menu);
            if (!canBe.placed.bottom && canBe.placed.top) {
                element.find(".dropdown").addClass("dropup").removeClass("dropdown");
            }
            else {
                element.find(".dropup").addClass("dropdown").removeClass("dropup");
            }
            //adding xui-edge-definer back
            angular.element(".sw-widget-container").addClass("xui-edge-definer");
        };
        this.addFilterCondition = function (selectedEntity) {
            if (!selectedEntity.conditions) {
                selectedEntity.conditions = [];
            }
            var values = [];
            var condition = {
                field: selectedEntity.availableFields[0],
                operation: _this.dialogOptions.viewModel.availableOperations[0],
                availableValues: values
            };
            selectedEntity.conditions.push(condition);
            _this.onFilteredFieldChanged(condition.field, null, selectedEntity.conditions.length - 1, selectedEntity, null);
        };
        this.initDialogState = function (selectedEntities) {
            var that = _this;
            _.each(selectedEntities, function (entity) {
                entity.isBusy = true;
                entity.availableFields = [];
                _.each(entity.conditions, function (conditionEntity, key) {
                    conditionEntity.isBusy = false;
                    that.onFilteredFieldChanged(conditionEntity.field, null, key, entity, conditionEntity.value);
                    conditionEntity.availableValues = [];
                });
                that.swTileService.getAvailableFields(entity.entityName).then(function (result) {
                    entity.availableFields = result.Result;
                    entity.isBusy = false;
                });
            });
            that.swTileService.getFilterOperations().then(function (result) {
                that.dialogOptions.viewModel.availableOperations = result;
            });
        };
        this.showDialog = function (selectedEntities, availableEntities) {
            _this.initDialogState(selectedEntities);
            var that = _this;
            _this.dialogOptions = {
                hideCancel: true,
                hideTopCancel: true,
                title: _this._t("Advanced Entity Filter"),
                buttons: [{
                        icon: "caret-left",
                        name: "cancel",
                        isPrimary: false,
                        pullLeft: true,
                        text: _this._t("back"),
                        action: function () {
                            return true;
                        }
                    },
                    {
                        name: "start",
                        isPrimary: false,
                        text: _this._t("SAVE AND CLOSE"),
                        action: function () {
                            var vm = that.dialogOptions.viewModel;
                            _.each(vm.selectedEntities, function (entity) {
                                _.each(entity.conditions, function (conditionEntity, key) {
                                    var conditionValue = conditionEntity.value;
                                    if (!angular.isObject(conditionValue)) {
                                        conditionEntity.value = { value: conditionValue, displayValue: conditionValue };
                                    }
                                    else {
                                        if (conditionValue.value === undefined) {
                                            conditionValue = { value: "", displayValue: "" };
                                        }
                                    }
                                });
                            });
                            that.swMainFilterDialogService.setBasicFilter(that.dialogOptions.viewModel.selectedEntities);
                            return true;
                        }
                    }],
                viewModel: {
                    wasStarted: false,
                    isBusy: false,
                    wasCanceled: false,
                    addFilterCondition: _this.addFilterCondition,
                    onFilteredFieldChanged: _this.onFilteredFieldChanged,
                    onFilteredRemoved: _this.onFilteredRemoved,
                    selectedEntities: selectedEntities,
                    availableEntities: availableEntities,
                    availableOperations: [],
                    onEntityChanged: _this.onEntityChanged,
                    onEntityAdded: _this.onEntityAdded,
                    onEntityRemoved: _this.onEntityRemoved,
                    adjustMenuPosition: _this.adjustMenuPosition
                }
            };
            _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(48),
            }, _this.dialogOptions);
        };
    }
    return BasicFilterDialogService;
}());
exports.BasicFilterDialogService = BasicFilterDialogService;


/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AdvancedFilterDialogService = /** @class */ (function () {
    /** @ngInject */
    AdvancedFilterDialogService.$inject = ["getTextService", "xuiDialogService", "swTileService", "swMainFilterDialogService"];
    function AdvancedFilterDialogService(getTextService, xuiDialogService, swTileService, swMainFilterDialogService) {
        var _this = this;
        this.getTextService = getTextService;
        this.xuiDialogService = xuiDialogService;
        this.swTileService = swTileService;
        this.swMainFilterDialogService = swMainFilterDialogService;
        this._t = this.getTextService;
        this.onSwqlChange = function () {
            var vm = _this.dialogOptions.viewModel;
            if (!vm) {
                return;
            }
            if (!vm.swqlTileQuery) {
                vm.tileQueryError = null;
            }
            if (!vm.swqlAssetQuery) {
                vm.assetExplorerQueryError = null;
            }
            if (!vm.swqlAlertQuery) {
                vm.alertQueryError = null;
            }
            vm.isTileOrAlertQueryDefined =
                (vm.swqlTileQuery || vm.swqlAlertQuery) ? true : false;
        };
        this.showRedirectWarning = function () {
            return _this.xuiDialogService.showWarning({
                title: _this._t("Switching to basic mode"),
                cancelButtonText: _this._t("Cancel"),
                message: (_this._t("Switching to Basic mode will discard all changes made in Advanced mode. \
Are you sure you want to proceed?"))
            }, {
                name: "proceed",
                text: _this._t("ok"),
                action: function () {
                    var vm = _this.dialogOptions.viewModel;
                    vm.swqlTileQuery = "";
                    vm.swqlAssetQuery = "";
                    vm.swqlAlertQuery = "";
                    return true;
                }
            });
        };
        this.initializeTileFilters = function (tile) {
            var vm = _this.dialogOptions.viewModel;
            if (!tile.swql && !tile.listSwql && !tile.otherIssuesSwql) {
                _this.swTileService.buildAdvancedTileQueries(tile).then(function (result) {
                    vm.swqlTileQuery = result.swql;
                    vm.swqlAssetQuery = result.listSwql;
                    vm.swqlAlertQuery = result.otherIssuesSwql;
                });
            }
            else {
                vm.swqlTileQuery = tile.swql;
                vm.swqlAssetQuery = tile.listSwql;
                vm.swqlAlertQuery = tile.otherIssuesSwql;
            }
        };
        this.validateTileQueries = function () {
            var vm = _this.dialogOptions.viewModel;
            return _this.swTileService.validateTileQueries(vm.swqlTileQuery, vm.swqlAssetQuery, vm.swqlAlertQuery).then(function (result) {
                vm.tileQueryError = result.swqlError;
                vm.assetExplorerQueryError = result.listSwqlError;
                vm.alertQueryError = result.otherIssuesSwqlError;
                if (result.swqlError || result.otherIssuesSwqlError || result.listSwqlError) {
                    return false;
                }
                else {
                    _this.swMainFilterDialogService.
                        setAdvancedFilter(_this.dialogOptions.viewModel);
                    return true;
                }
            });
        };
        this.showDialog = function (tile) {
            var that = _this;
            _this.dialogOptions = {
                hideCancel: true,
                hideTopCancel: true,
                title: _this._t("Advanced SWQL Filter"),
                actionButtonText: _this._t("SAVE AND CLOSE"),
                cancelButtonText: _this._t("BACK"),
                buttons: [{
                        icon: "caret-left",
                        name: "cancel",
                        isPrimary: false,
                        pullLeft: true,
                        text: _this._t("BACK"),
                        action: function (dialogResult) {
                            return that.showRedirectWarning().then(function (result) {
                                return result === "cancel" ? false : true;
                            });
                        }
                    },
                    {
                        name: "start",
                        isPrimary: false,
                        text: _this._t("SAVE AND CLOSE"),
                        action: function (dialogResult) {
                            var vm = that.dialogOptions.viewModel;
                            vm.isTileOrAlertQueryDefined = (vm.swqlTileQuery || vm.swqlAlertQuery) ? true : false;
                            if (!vm.isTileOrAlertQueryDefined) {
                                return false;
                            }
                            return that.validateTileQueries();
                        }
                    }],
                viewModel: {
                    swqlTileQuery: "",
                    swqlAssetQuery: "",
                    swqlAlertQuery: "",
                    tileQueryError: null,
                    assetExplorerQueryError: null,
                    alertQueryError: null,
                    onSWQLChange: _this.onSwqlChange,
                    isTileOrAlertQueryDefined: true
                }
            };
            _this.initializeTileFilters(tile);
            _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(49),
            }, _this.dialogOptions);
        };
    }
    return AdvancedFilterDialogService;
}());
exports.AdvancedFilterDialogService = AdvancedFilterDialogService;


/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
;
;
;
var MainFilterDialogService = /** @class */ (function () {
    /** @ngInject */
    MainFilterDialogService.$inject = ["$q", "$interval", "getTextService", "$timeout", "xuiDialogService", "swTileService"];
    function MainFilterDialogService($q, $interval, getTextService, $timeout, xuiDialogService, swTileService) {
        var _this = this;
        this.$q = $q;
        this.$interval = $interval;
        this.getTextService = getTextService;
        this.$timeout = $timeout;
        this.xuiDialogService = xuiDialogService;
        this.swTileService = swTileService;
        this.federationEnabled = false;
        this._t = this.getTextService;
        this.initializeFilters = function () {
            _this.viewModel.isBusy = true;
            _this.swTileService.getFederationEnabled().then(function (_federationEnabled) {
                _this.viewModel.federationEnabled = _federationEnabled;
                if (_this.viewModel.federationEnabled === true) {
                    _this.swTileService.getSites().then(function (result) {
                        _this.viewModel.availableSites = result;
                        if (_this.viewModel.availableSites) {
                            angular.forEach(_this.viewModel.availableSites, function (entity) {
                                if (_this.tileForEdit.filter) {
                                    var sites = _.filter(_this.tileForEdit.filter.siteFilter, function (item) { return (item.siteID === entity.siteID); });
                                    if (sites.length > 0) {
                                        _this.viewModel.selectedSites.push(entity);
                                    }
                                }
                            });
                        }
                    });
                }
            });
            _this.swTileService.getManagedEntities().then(function (result) {
                _this.viewModel.availableEntities = result.managedEntities;
                _this.viewModel.isBusy = false;
                if (_this.tileForEdit.entity) {
                    _.each(_this.viewModel.availableEntities, function (entity) {
                        if (_this.tileForEdit.entity.indexOf(entity.entityName) >= 0) {
                            _this.viewModel.selectedEntities.push(entity);
                        }
                    });
                }
            });
            _this.swTileService.getGroups().then(function (result) {
                _this.viewModel.availableGroups = result.groups;
                if (_this.viewModel.availableGroups) {
                    angular.forEach(_this.viewModel.availableGroups, function (entity) {
                        if (_this.tileForEdit.filter) {
                            var groups = _.filter(_this.tileForEdit.filter.groupFilter, function (item) { return (item.siteID === entity.siteID) && (item.groupID === entity.id); });
                            if (groups.length > 0) {
                                _this.viewModel.selectedGroups.push(entity);
                            }
                        }
                    });
                }
            });
        };
        this.setAdvancedFilter = function (advancedFilter) {
            var vm = _this.viewModel;
            vm.isAdvancedMode = true;
            vm.swqlTileQuery = advancedFilter.swqlTileQuery;
            vm.swqlAssetQuery = advancedFilter.swqlAssetQuery;
            vm.swqlAlertQuery = advancedFilter.swqlAlertQuery;
            vm.basicFilter = [];
        };
        this.setBasicFilter = function (selectedEntities) {
            var vm = _this.viewModel;
            var that = _this;
            vm.isAdvancedMode = false;
            vm.swqlTileQuery = "";
            vm.swqlAssetQuery = "";
            vm.swqlAlertQuery = "";
            vm.isBusy = false;
            if (!vm.basicFilter) {
                vm.basicFilter = [];
            }
            vm.selectedEntities = [];
            vm.isEntityFilterValid = selectedEntities.length > 0;
            var newEntities = [];
            _.each(selectedEntities, function (entity) {
                var existingEntity = _.find(vm.basicFilter, function (property) {
                    if (!property.conditions) {
                        property.conditions = [];
                    }
                    if (!entity.conditions) {
                        entity.conditions = [];
                    }
                    if (property.entityName === entity.entityName &&
                        property.conditions.length === entity.conditions.length) {
                        return that.compareFilterConditions(property, entity);
                    }
                    return false;
                });
                if (existingEntity) {
                    newEntities.push(existingEntity);
                }
                else {
                    newEntities.push(entity);
                }
                _.each(vm.availableEntities, function (availableEntity) {
                    if (availableEntity.entityName === entity.entityName) {
                        vm.selectedEntities.push(availableEntity);
                    }
                });
            });
            vm.basicFilter = newEntities;
            vm.hasUpdatedFilter = true;
        };
        this.compareFilterConditions = function (property, entity) {
            var conditionExists = false;
            if (property.conditions.length === 0 && entity.conditions.length) {
                return true;
            }
            _.each(property.conditions, function (propertyСondition) {
                if (!conditionExists) {
                    _.each(entity.conditions, function (entityСondition) {
                        if (_this.compareConditions(propertyСondition, entityСondition)) {
                            conditionExists = true;
                        }
                        else {
                            conditionExists = false;
                            return false;
                        }
                    });
                }
            });
            return conditionExists;
        };
        this.compareConditions = function (oldCondition, newCondition) {
            return oldCondition.field.name === newCondition.field.name &&
                oldCondition.field.entity === newCondition.field.entity &&
                oldCondition.value.value === newCondition.value.value &&
                oldCondition.operation.value.toString() === newCondition.operation.value.toString();
        };
        this.getBasicFilterEntity = function (filter) {
            var entityFilter = {
                entityDisplayName: filter.entityDisplayName,
                entityName: filter.entityName,
                conditions: []
            };
            _.each(filter.conditions, function (condition) {
                entityFilter.conditions.push({
                    operation: condition.operation,
                    field: {
                        displayName: condition.field.displayName,
                        entity: condition.field.entity,
                        name: condition.field.name,
                        type: condition.field.type,
                        units: condition.field.units
                    },
                    value: condition.value
                });
            });
            return entityFilter;
        };
        this.getTileFromUi = function () {
            var tile = {
                sites: [],
                groups: [],
                managedEntities: [],
                tileType: null,
                swql: null,
                listSwql: null,
                otherIssuesSwql: null
            };
            tile.showAlerts = _this.viewModel.showAlerts;
            if (_this.viewModel.isAdvancedMode) {
                tile.swql = _this.viewModel.swqlTileQuery;
                tile.listSwql = _this.viewModel.swqlAssetQuery;
                tile.otherIssuesSwql = _this.viewModel.swqlAlertQuery;
                tile.tileType = "CustomAdvanced";
            }
            else {
                tile.sites = _this.viewModel.selectedSites;
                tile.groups = _this.viewModel.selectedGroups;
                tile.managedEntities = _this.viewModel.selectedEntities;
                tile.basicFilter = _this.viewModel.basicFilter;
                tile.tileType = "CustomBasic";
            }
            return tile;
        };
        this.initializeTileFilters = function (tile) {
            _this.tileForEdit = tile;
            _this.viewModel.isAdvancedMode = tile.tiletype === "CustomAdvanced";
            _this.viewModel.swqlTileQuery = tile.swql;
            _this.viewModel.swqlAssetQuery = tile.listswql;
            _this.viewModel.swqlAlertQuery = tile.otherissuesswql;
            _this.initializeFilters();
        };
        this.isNewTile = function () {
            var vm = _this.viewModel;
            if (!vm.swqlAlertQuery && !vm.swqlAssetQuery
                && !vm.swqlTileQuery && (!vm.basicFilter || vm.basicFilter.length === 0) && vm.firstTileLoad) {
                return true;
            }
            return false;
        };
        this.isDialogValid = function () {
            var vm = _this.viewModel;
            if (vm.isAdvancedMode || _this.isNewTile()) {
                return true;
            }
            else {
                if (!vm.selectedEntities || vm.selectedEntities.length === 0) {
                    vm.isEntityFilterValid = false;
                    return false;
                }
            }
            return true;
        };
        this.initializeDialog = function (tile) {
            var firstTileLoad = false;
            if (!tile.basicfilter) {
                firstTileLoad = true;
                tile.basicfilter = [];
            }
            _this.viewModel = {
                availableSites: [],
                availableGroups: _this.availableGroups,
                availableEntities: [],
                selectedEntities: [],
                selectedGroups: [],
                selectedSites: [],
                isEntityFilterValid: true,
                isAdvancedMode: false,
                swqlTileQuery: "",
                swqlAssetQuery: "",
                swqlAlertQuery: "",
                tileQueryError: null,
                assetExplorerQueryError: null,
                alertQueryError: null,
                basicFilter: tile.basicfilter,
                isBusy: false,
                firstTileLoad: firstTileLoad,
                hasUpdatedFilter: false,
                isDialogValid: _this.isDialogValid,
                federationEnabled: _this.federationEnabled,
                showAlerts: true
            };
            if (tile.showalerts !== null && tile.showalerts !== undefined) {
                _this.viewModel.showAlerts = tile.showalerts;
            }
            _this.initializeTileFilters(tile);
            if (tile.entity) {
                _this.viewModel.isEntityFilterValid = true;
            }
        };
    }
    return MainFilterDialogService;
}());
exports.MainFilterDialogService = MainFilterDialogService;


/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widgetContainer_1 = __webpack_require__(166);
exports.default = function (module) {
    module.component("swWidgetContainer", widgetContainer_1.default);
};


/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(167);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var widgetContainer_controller_1 = __webpack_require__(168);
var WidgetContainer = /** @class */ (function () {
    function WidgetContainer($log, $ocLazyLoad, $compile, $rootScope, revisionService) {
        var _this = this;
        this.$log = $log;
        this.$ocLazyLoad = $ocLazyLoad;
        this.$compile = $compile;
        this.$rootScope = $rootScope;
        this.revisionService = revisionService;
        this.restrict = "A";
        this.scope = {};
        this.controller = widgetContainer_controller_1.default;
        this.controllerAs = "widgetContainer";
        this.bindToController = {
            id: "@swWidgetContainer",
            title: "@?swWidgetTitle",
            subtitle: "@?swWidgetSubtitle",
            edittitle: "@?swWidgetEdittitle",
            editsubtitle: "@?swWidgetEditsubtitle",
            defaulttitle: "@?swWidgetDefaulttitle"
        };
        this.compile = function ($element) {
            // If the host widget is not an xui resource we can ignore it.
            if (!$element.hasClass("XuiResourceWrapper")) {
                return null;
            }
            $element.addClass("sw-widget-container xui-edge-definer");
            return {
                pre: _this.preLink,
                post: _this.postLink
            };
        };
        this.preLink = function (scope, element, attrs, container) {
            if (attrs["swWidgetProperties"]) {
                container.settings = _this.deserializeSettings(angular.fromJson(attrs["swWidgetProperties"]));
            }
            else {
                container.settings = {};
            }
            container.settings.userSettings = {};
            container.isVisible = false;
            container.getVisibilityConfiguration()
                .then(function (isVisible) {
                if (isVisible) {
                    container.getUserProperties();
                }
                container.isVisibleDeferred.resolve(isVisible);
            });
        };
        this.postLink = function (scope, element, attrs, container) {
            container.setTitle = function (newValue) {
                if (newValue || newValue === "") {
                    element.find(".sw-widget__title").text(newValue);
                }
            };
            container.setSubtitle = function (newValue) {
                if (newValue || newValue === "") {
                    element.find(".sw-widget__subtitle").text(newValue);
                }
            };
            element.find(".sw-widget__edit-command")
                .removeAttr("href")
                .click(function (eventArgs) {
                if (angular.isFunction(container.onEdit)) {
                    eventArgs.stopPropagation();
                    eventArgs.preventDefault();
                    container.onEdit();
                }
            });
            var $widgetBody = element.find(".ResourceContent");
            var resId = $widgetBody.parent().attr("resourceid");
            SW.Core.VisibilityObserver.registerByResourceId(resId, function () { return _this.initAndLoad(scope, element, container, $widgetBody, resId); }, true);
        };
        this.initAndLoad = function (scope, element, container, $widgetBody, resId) {
            // Add any styles to the DOM.
            if (container.settings.stylepaths) {
                var stylesToLoad = container.settings.stylepaths.split(",")
                    .map(function (pth) {
                    return {
                        type: "css",
                        path: _this.revisionService.getPathWithRevision(pth),
                    };
                });
                _this.$ocLazyLoad.load(stylesToLoad);
            }
            // Download, eval(), and compile.
            if (container.settings.scriptpaths) {
                var scriptsToLoad = container.settings.scriptpaths.split(",")
                    .map(function (pth) {
                    return {
                        type: "js",
                        path: _this.revisionService.getPathWithRevision(pth),
                    };
                });
                _this.$ocLazyLoad
                    .load(scriptsToLoad)
                    .then(function () {
                    // compile directive if not compiled yet
                    // first child of .xui is the directive element
                    var directive = $widgetBody.find(".xui :first");
                    // if empty then we consider it to be not compiled yet
                    var isCompiled = !directive.is(":empty");
                    if (!isCompiled) {
                        _this.$compile($widgetBody)(scope);
                    }
                    _this.processPartialTemplates(element, container);
                    container.scriptsLoaded.resolve(true);
                })
                    .catch(function (ex) {
                    _this.$log.error(ex);
                    $widgetBody.html("<div class=\"sw-widget-container__error\">" + ex.message + "</div>");
                });
            }
            else {
                _this.processPartialTemplates(element, container);
                container.scriptsLoaded.resolve(true);
            }
        };
        this.deserializeSettings = function (jsonSettings) {
            var settings = {};
            for (var prop in jsonSettings) {
                if (jsonSettings.hasOwnProperty(prop)) {
                    try {
                        settings[prop] = angular.fromJson(jsonSettings[prop]);
                    }
                    catch (e) {
                        // this is a hack for not deserializable items like "helplinkfragment"
                        settings[prop] = jsonSettings[prop];
                    }
                }
            }
            return settings;
        };
    }
    WidgetContainer.prototype.compilePartialTemplate = function (template, widget) {
        var directive = _.kebabCase(template.directiveName);
        var linkingFn = this.$compile("<" + directive + "></" + directive + ">");
        return linkingFn(this.$rootScope.$new(), undefined, {
            transcludeControllers: {
                widget: {
                    instance: widget
                }
            }
        });
    };
    WidgetContainer.prototype.processPartialTemplates = function (element, container) {
        var _this = this;
        container.partialTemplates.forEach(function (template) {
            var templateElement = _this.compilePartialTemplate(template, container.widget);
            element.find("." + template.placeholderClass).replaceWith(templateElement);
        });
    };
    WidgetContainer.$inject = ["$log", "$ocLazyLoad", "$compile", "$rootScope", "revisionService"];
    return WidgetContainer;
}());
exports.default = WidgetContainer;


/***/ }),
/* 167 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var inject_1 = __webpack_require__(4);
var constants_1 = __webpack_require__(9);
var WidgetContainerController = /** @class */ (function () {
    function WidgetContainerController(swApi, swWidgetService, swNetObjectOpidConverterService, viewService, dialogService, swUtil, _t, demoService, $q, $log, viewInfo, swAuthService) {
        var _this = this;
        this.swApi = swApi;
        this.swWidgetService = swWidgetService;
        this.swNetObjectOpidConverterService = swNetObjectOpidConverterService;
        this.viewService = viewService;
        this.dialogService = dialogService;
        this.swUtil = swUtil;
        this._t = _t;
        this.demoService = demoService;
        this.$q = $q;
        this.$log = $log;
        this.viewInfo = viewInfo;
        this.swAuthService = swAuthService;
        this.$onInit = function () {
            _this.isVisibleDeferred = _this.$q.defer();
            _this.getUserPropertiesDeferred = _this.$q.defer();
            _this.scriptsLoaded = _this.$q.defer();
            _this.loggedUser = _this.swAuthService.getUser();
            _this.partialTemplates = [];
            _this.widgetElement = angular.element("[resourceid=" + _this.id + "]");
        };
        this.widgetLoaded = function (widget) {
            _this.widget = widget;
            _this.getUserPropertiesDeferred.promise
                .then(function () { return _this.onSettingsChanged(); });
        };
        this.onEdit = function () {
            if (!_this.settingName) {
                throw new Error("Settings not defined for widget \"" + _this.id + "\".");
            }
            if (!_this.settings.edittitle) {
                _this.settings.edittitle = _this.settings.title;
            }
            if (!_this.settings.editsubtitle) {
                _this.settings.editsubtitle = _this.settings.subtitle;
            }
            var viewModel = {
                settings: _.extend({
                    title: _this.title,
                    edittitle: _this.edittitle,
                    subtitle: _this.subtitle,
                    editsubtitle: _this.editsubtitle,
                    defaulttitle: _this.defaulttitle,
                }, _this.settings),
                options: {
                    titleReadOnly: false
                }
            };
            var dialogAction = function (dialogResult) {
                var hasChanges = !_.isEqual(_this.settings, viewModel);
                if (!hasChanges) {
                    // No changes but we want the dialog close.
                    return true;
                }
                var saveResult = angular.isFunction(viewModel.onSave) ? viewModel.onSave() : true;
                if (saveResult && angular.isFunction(saveResult.then)) {
                    return saveResult.then(function (success) {
                        return (success === true) && _this.saveSettings(viewModel.settings);
                    });
                }
                else if (!angular.isDefined(saveResult) || saveResult === true) {
                    return _this.saveSettings(viewModel.settings);
                }
                return false;
            };
            // Convert to snake-case for html rendering (mySettingsComponent => my-settings-component).
            var componentName = _this.settingName.replace(/[A-Z]/g, function (char) { return "-" + char.toLowerCase(); });
            var customSettings = {
                template: "\n<xui-dialog>\n    <sw-widget-settings-form\n        class=\"xui-edge-definer\"\n        on-save=\"vm.dialogOptions.viewModel.onSave()\"\n        on-cancel=\"vm.dialogOptions.viewModel.onCancel()\"\n        sw-widget-settings-data=\"vm.dialogOptions.viewModel.settings\"\n        sw-widget-settings-options=\"vm.dialogOptions.viewModel.options\">\n        <" + componentName + "></" + componentName + ">\n    </sw-widget-settings-form>\n</xui-dialog>"
            };
            var dialogOptions = {
                title: _this.swUtil.formatString(_this._t("Edit Settings ({0})"), _this.settings.edittitle),
                cancelButtonText: _this._t("Cancel"),
                hideCancel: false,
                buttons: [{
                        name: "save",
                        isPrimary: true,
                        text: _this._t("Save"),
                        actionText: _this._t("Saving widget settings..."),
                        action: dialogAction
                    }],
                viewModel: viewModel
            };
            _this.dialogService
                .showModal(customSettings, dialogOptions)
                .then(function (dialogResult) {
                if (dialogResult === "cancel" && angular.isFunction(viewModel.onCancel)) {
                    viewModel.onCancel();
                }
            });
        };
        this.onSettingsChanged = function () {
            if (_this.widget && angular.isFunction(_this.widget.onSettingsChanged)) {
                _this.widget.onSettingsChanged(_this.settings);
            }
        };
        this.saveSettings = function (settings) {
            settings.title = (typeof settings.edittitle === "undefined")
                ? settings.title : settings.edittitle;
            settings.subtitle = (typeof settings.editsubtitle === "undefined")
                ? settings.subtitle : settings.editsubtitle;
            settings.title = (typeof settings.title === "string" && settings.title.length > 0)
                ? settings.title : settings.defaulttitle;
            var widgetNetObject = _this.viewInfo.activeViewInfo.netObject;
            // ensure required properties
            if (!settings.subtitle) {
                settings.subtitle = "";
            }
            var saveToDb = function () {
                // since we do not want to save userSettings to legacyUserSettings
                var saveData = _.assign({}, settings);
                delete saveData.userSettings;
                if (!_this.userHasRightsForWidget()) {
                    return _this.$q.resolve();
                }
                return _this.viewService.saveLegacyResourceUserSettings(_this.id, saveData);
            };
            var showDemo = function () {
                _this.demoService.showDemoErrorToast();
                return _this.$q.resolve();
            };
            var action = _this.demoService.isDemoMode() ? showDemo : saveToDb;
            return action()
                .then(function () {
                _.assign(_this.settings, settings);
                if (_this.widget && _this.widget.onSettingsChanged) {
                    _this.widget.onSettingsChanged(_this.settings, true);
                }
                var macros = {
                    title: settings.title,
                    subtitle: settings.subtitle
                };
                return _this.viewService.parseMacros(macros, widgetNetObject);
            })
                .then(function (response) {
                settings.title = response.title;
                settings.subtitle = response.subtitle;
                _this.setTitle(settings.title);
                _this.setSubtitle(settings.subtitle);
                return true;
            })
                .catch(function (err) {
                _this.$log.error(err);
                return false;
            });
        };
        this.saveUserProperties = function (settings) {
            return _this.viewService.saveUserProperties(_this.id, settings);
        };
        this.getUserProperties = function () {
            return _this.viewService.getUserProperties(_this.id)
                .then(function (response) {
                _this.settings.userSettings = response;
                _this.getUserPropertiesDeferred.resolve();
            });
        };
        this.userHasRightsForWidget = function () {
            return _this.swAuthService.isUserAdmin() ||
                _.includes(_this.loggedUser.permissions, _this.swAuthService.permissions.customize);
        };
    }
    WidgetContainerController.prototype.hideWidget = function () {
        return this.swWidgetService.hideParentWidget(this.widgetElement);
    };
    WidgetContainerController.prototype.showWidget = function () {
        return this.swWidgetService.showParentWidget(this.widgetElement);
    };
    WidgetContainerController.prototype.getVisibilityConfiguration = function () {
        var _this = this;
        return this.$q.when(this.getVisibilityConfigurationInternal())
            .then(function (isVisible) {
            _this.isVisible = isVisible === true;
            return _this.isVisible;
        })
            .catch(function (err) {
            // note: possible HTTP exceptions are already logged by Angular
            _this.isVisible = true;
            return _this.isVisible;
        });
    };
    WidgetContainerController.prototype.getVisibilityConfigurationInternal = function () {
        if (!_.has(this.settings, "visibilityconfiguration")) {
            return true;
        }
        var visibilityConf = this.settings.visibilityconfiguration;
        var opid = _.has(this.settings, "opid") ? this.swNetObjectOpidConverterService.parseOpid(this.settings.opid) : null;
        var useDefaultUrl = !(visibilityConf && visibilityConf.url);
        var useDefaultParams = useDefaultUrl || !visibilityConf.customParams;
        if (useDefaultUrl && opid == null) {
            console.warn("Opid must be provided when using default visibility configuration url.");
            return true;
        }
        var url = useDefaultUrl ? constants_1.default.visibilityDefaultUrl : visibilityConf.url;
        var requestData = useDefaultParams
            ? { visibilityContext: opid, visibilityConfiguration: visibilityConf.params }
            : Object.assign({}, visibilityConf.customParams, { opid: this.settings.opid });
        return this.swApi.api(false).one(url).post("", requestData);
    };
    WidgetContainerController = __decorate([
        __param(0, inject_1.Inject("swApi")),
        __param(1, inject_1.Inject("swWidgetService")),
        __param(2, inject_1.Inject("swNetObjectOpidConverterService")),
        __param(3, inject_1.Inject("viewService")),
        __param(4, inject_1.Inject("xuiDialogService")),
        __param(5, inject_1.Inject("swUtil")),
        __param(6, inject_1.Inject("getTextService")),
        __param(7, inject_1.Inject("swDemoService")),
        __param(8, inject_1.Inject("$q")),
        __param(9, inject_1.Inject("$log")),
        __param(10, inject_1.Inject("swOrionViewService")),
        __param(11, inject_1.Inject("swAuthService")),
        __metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object, Function, Object, Function, Object, Object, Object])
    ], WidgetContainerController);
    return WidgetContainerController;
}());
exports.WidgetContainerController = WidgetContainerController;
exports.default = WidgetContainerController;


/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var genericList_controller_1 = __webpack_require__(170);
var header_1 = __webpack_require__(178);
__webpack_require__(180);
exports.default = function (module) {
    module.controller("swGenericListWidget", genericList_controller_1.GenericListController);
    header_1.default(module);
};


/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
var filteringSession_1 = __webpack_require__(171);
var filteringSessionDataProvider_1 = __webpack_require__(173);
var gridConverter_1 = __webpack_require__(174);
var filterBarsConverter_1 = __webpack_require__(175);
var timeFrameConverter_1 = __webpack_require__(177);
exports.WidgetSettingsName = "swGenericListWidgetSettings";
var GenericListController = /** @class */ (function () {
    function GenericListController($scope, restangular, $timeout, $templateCache, $templateRequest, $q, _t, xuiTimeframeService, revisionService) {
        var _this = this;
        this.$scope = $scope;
        this.restangular = restangular;
        this.$timeout = $timeout;
        this.$templateCache = $templateCache;
        this.$templateRequest = $templateRequest;
        this.$q = $q;
        this._t = _t;
        this.xuiTimeframeService = xuiTimeframeService;
        this.revisionService = revisionService;
        this.rowTemplateId = "swGenericListWidget-row";
        this.headerTemplateId = null;
        this.initialized = false;
        this.$onInit = function () {
            _this.$templateCache.put("swGenericListWidget-emptyData", __webpack_require__(51));
            _this.$templateCache.put("swGenericListWidget-row", __webpack_require__(52));
            _this.emptyData = {
                templateUrl: "swGenericListWidget-emptyData",
                viewModel: {
                    filtersApplied: false,
                    noDataToShow: {
                        description: _this._t("No data to show"),
                        image: "no-data-to-show"
                    },
                    noFilteredResults: {
                        description: _this._t("Nothing found"),
                        image: "no-search-results",
                    }
                }
            };
        };
        this.getTemplate = function () {
            return _this.rowTemplateId;
        };
    }
    GenericListController.prototype.onLoad = function (config) {
        var _this = this;
        this.config = config;
        this.config.isBusy(true);
        var scriptsLoaded = this.$q.defer();
        this.$scope.$watch("vm.config.scriptsLoaded", function (newValue) {
            if (newValue) {
                scriptsLoaded.resolve();
            }
        });
        this.$q.all([
            this.loadTemplate(config.settings),
            this.loadHeaderTemplate(config.settings),
            scriptsLoaded.promise,
        ]).then(function () {
            _this.initFilteringSession(config.settings.endpoint, _this.config.settings.opid);
        });
    };
    GenericListController.prototype.loadTemplate = function (settings) {
        var _this = this;
        var templateLoaded = this.$q.defer();
        if (settings.template && settings.template.id && settings.template.url) {
            var url = this.revisionService.getPathWithRevision(settings.template.url);
            this.$templateRequest(url).then(function (html) {
                _this.rowTemplateId = settings.template.id;
                _this.$templateCache.put(_this.rowTemplateId, html);
                templateLoaded.resolve(true);
            });
        }
        else {
            if (settings.template.id) {
                this.rowTemplateId = settings.template.id;
            }
            templateLoaded.resolve(true);
        }
        return templateLoaded.promise;
    };
    GenericListController.prototype.loadHeaderTemplate = function (settings) {
        var _this = this;
        var templateLoaded = this.$q.defer();
        if (settings.header && settings.header.id && settings.header.url) {
            this.$templateRequest("" + settings.header.url).then(function (html) {
                _this.headerTemplateId = settings.header.id;
                _this.$templateCache.put(_this.headerTemplateId, html);
                templateLoaded.resolve(true);
            });
        }
        else {
            templateLoaded.resolve(false);
        }
        return templateLoaded.promise;
    };
    GenericListController.prototype.onUnload = function () {
        /* ignore */
    };
    GenericListController.prototype.onSettingsChanged = function (settings, afterSave) {
        /* ignore */
    };
    GenericListController.prototype.initFilteringSession = function (endpoint, opid) {
        var _this = this;
        var dataProvider = new filteringSessionDataProvider_1.FilteringSessionDataProvider(endpoint, this.restangular);
        this.filteringSession = new filteringSession_1.FilteringSession(dataProvider, this.$timeout, opid);
        this.filteringSession.register("list", new gridConverter_1.GridConverter());
        this.filteringSession.register("filterBars", new filterBarsConverter_1.FilterBarsConverter());
        var timeFrameConverter = new timeFrameConverter_1.TimeFrameConverter(this.xuiTimeframeService);
        timeFrameConverter.onChange.subscribe(function () { return _this.filteringSession.onComponentUpdate("timeFrame", undefined); });
        this.filteringSession.register("timeFrame", timeFrameConverter);
        this.filteringSession.initialize(function () {
            _this.config.isBusy(false);
            _this.initialized = true;
        });
        //Verify whether filters affecting data are applied or not.
        this.filteringSession.output.subscribe(function (value) {
            var keys = Object.keys(value.filters);
            _this.emptyData.viewModel.filtersApplied =
                (_.differenceWith(keys, ["sorting", "pagination"], _.isEqual).length > 0);
        });
    };
    GenericListController.$inject = [
        "$scope", "Restangular", "$timeout", "$templateCache", "$templateRequest", "$q", "getTextService", "xuiTimeframeService", "revisionService"
    ];
    GenericListController = __decorate([
        widget_decorator_1.Widget({
            selector: "swGenericListWidget",
            template: __webpack_require__(53),
            controllerAs: "vm",
            settingName: exports.WidgetSettingsName,
            partialTemplates: [{ placeholderClass: "sw-widget__header__custom", directiveName: "swGenericListWidgetHeader" }]
        }),
        __metadata("design:paramtypes", [Object, Object, Function, Object, Function, Function, Function, Object, Object])
    ], GenericListController);
    return GenericListController;
}());
exports.GenericListController = GenericListController;


/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var presentationSession_1 = __webpack_require__(172);
var FilteringSession = /** @class */ (function (_super) {
    __extends(FilteringSession, _super);
    function FilteringSession(dataProvider, $timeout, opid) {
        var _this = _super.call(this) || this;
        _this.dataProvider = dataProvider;
        _this.$timeout = $timeout;
        _this.opid = opid;
        _this.isBusy = false;
        _this.output.update({
            filters: {},
            components: {},
            custom: { "opid": opid }
        });
        _this.output.subscribe(function (filters) { return _this.getData(filters); });
        return _this;
    }
    FilteringSession.prototype.initialize = function (onFinishedCallback) {
        var _this = this;
        var filters = this.output.valueOf();
        var configurationParameters = {
            custom: filters.custom
        };
        this.dataProvider.getConfiguration(configurationParameters).then(function (definitions) {
            _this.onConfiguration(definitions);
            _this.buildAllFilters();
            onFinishedCallback();
        });
    };
    FilteringSession.prototype.refresh = function () {
        this.getData(this.output.valueOf());
    };
    FilteringSession.prototype.onComponentUpdate = function (componentKey, componentState) {
        var _this = this;
        //This timeout is necessary here, to let bindings from components
        //propagate properly before sending request.
        this.$timeout(function () {
            _super.prototype.onComponentUpdate.call(_this, componentKey, componentState);
        });
    };
    FilteringSession.prototype.merge = function (output, componentOutput, componentKey) {
        if (!componentOutput) {
            return output;
        }
        var componentFilters = [];
        var componentOutputs = {};
        var componentCustom = {};
        componentFilters.push(componentOutput.filters);
        if (componentOutput.components) {
            componentOutputs[componentKey] = componentOutput.components;
        }
        var changedFilters = this.getChangedFilters(output.filters, componentOutput.filters);
        for (var key in this.converters) {
            if (this.converters.hasOwnProperty(key)) {
                var converter = this.converters[key];
                if (converter.shouldResetState(changedFilters)) {
                    this.state[key] = this.converters[key].resetState(this.state[key]);
                    var newComponentFilters = this.converters[key].fromComponent(this.state[key]);
                    if (newComponentFilters.filters) {
                        componentFilters.push(newComponentFilters.filters);
                    }
                    if (newComponentFilters.components) {
                        componentOutputs[key] = newComponentFilters.components;
                    }
                    if (newComponentFilters.custom) {
                        componentCustom[key] = newComponentFilters.custom;
                    }
                }
            }
        }
        var result = {
            filters: _.assign.apply(_, [{}, output.filters].concat(componentFilters)),
            components: _.assign({}, output.components, componentOutputs),
            custom: _.assign({}, output.custom, componentCustom)
        };
        //Clear empty filters 
        this.clearEmptyFilters(result.filters);
        return result;
    };
    FilteringSession.prototype.getChangedFilters = function (outputFilters, componentFilters) {
        var difference = [];
        for (var filter in componentFilters) {
            if (componentFilters.hasOwnProperty(filter)) {
                if (outputFilters[filter]) {
                    var currentFiltersDiff = _.differenceWith(outputFilters[filter], componentFilters[filter], _.isEqual);
                    if (currentFiltersDiff.length > 0) {
                        difference.push(filter);
                    }
                }
                else {
                    difference.push(filter);
                }
            }
        }
        return difference;
    };
    FilteringSession.prototype.buildAllFilters = function () {
        var newOutput = this.output.valueOf();
        for (var _i = 0, _a = Object.keys(this.converters); _i < _a.length; _i++) {
            var key = _a[_i];
            var filters = this.converters[key].fromComponent(this.state[key]);
            newOutput = this.merge(newOutput, filters, key);
        }
        this.output.update(newOutput);
    };
    FilteringSession.prototype.getData = function (filters) {
        var _this = this;
        this.isBusy = true;
        this.dataProvider.getData(filters).then(function (result) {
            _this.onInput(result);
            _this.isBusy = false;
        });
    };
    FilteringSession.prototype.clearEmptyFilters = function (object) {
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                if (_.isEmpty(object[prop])) {
                    delete object[prop];
                }
            }
        }
    };
    return FilteringSession;
}(presentationSession_1.PresentationSession));
exports.FilteringSession = FilteringSession;


/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = __webpack_require__(50);
var PresentationSession = /** @class */ (function () {
    function PresentationSession() {
        this.converters = {};
        this.output = new observable_1.Observable();
        this.state = {};
    }
    PresentationSession.prototype.register = function (componentKey, converter) {
        this.converters[componentKey] = converter;
        this.state[componentKey] = converter.defaultState();
    };
    PresentationSession.prototype.onConfiguration = function (configuration) {
        for (var _i = 0, _a = Object.keys(configuration); _i < _a.length; _i++) {
            var key = _a[_i];
            if (!this.converters[key]) {
                continue;
            }
            this.state[key] = this.converters[key].fromConfiguration(configuration[key]);
        }
    };
    PresentationSession.prototype.onInput = function (data) {
        for (var _i = 0, _a = Object.keys(data); _i < _a.length; _i++) {
            var key = _a[_i];
            if (!this.converters[key]) {
                continue;
            }
            this.state[key] = this.converters[key].toComponent(data[key], this.state[key]);
        }
    };
    PresentationSession.prototype.onComponentUpdate = function (componentKey, componentState) {
        if (typeof componentState !== "undefined") {
            this.state[componentKey] = componentState;
        }
        var state = this.state[componentKey];
        var componentOutput = this.converters[componentKey].fromComponent(state);
        var mergedOutput = this.merge(this.output.valueOf(), componentOutput, componentKey);
        this.output.update(mergedOutput);
    };
    PresentationSession.prototype.merge = function (output, componentOutput, componentKey) {
        return _.assign({}, output, componentOutput);
    };
    return PresentationSession;
}());
exports.PresentationSession = PresentationSession;


/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var FilteringSessionDataProvider = /** @class */ (function () {
    function FilteringSessionDataProvider(endpoint, restangular) {
        this.endpoint = endpoint;
        this.restangular = restangular;
    }
    FilteringSessionDataProvider.prototype.getConfiguration = function (parameters) {
        return this.restangular.one(this.endpoint + "/configuration").post("", parameters);
    };
    FilteringSessionDataProvider.prototype.getData = function (parameters) {
        return this.restangular.one(this.endpoint + "/data").post("", parameters);
    };
    return FilteringSessionDataProvider;
}());
exports.FilteringSessionDataProvider = FilteringSessionDataProvider;


/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var filteringConverter_1 = __webpack_require__(8);
var GridConverter = /** @class */ (function (_super) {
    __extends(GridConverter, _super);
    function GridConverter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GridConverter.prototype.defaultState = function () {
        return {
            items: [],
            pagination: null,
            searching: {
                searchTerm: ""
            },
            sorting: null,
            options: {},
            custom: {
                striped: true,
                rowPadding: "regular"
            }
        };
    };
    GridConverter.prototype.fromConfiguration = function (configuration) {
        var state = this.defaultState();
        if (configuration.pagination) {
            state.pagination = {
                page: configuration.pagination.page,
                pageSize: configuration.pagination.pageSize
            };
        }
        else {
            state.pagination = {
                page: 1,
                pageSize: 5
            };
        }
        if (configuration.sorter && configuration.sorter.sortableColumns) {
            state.sorting = {
                direction: configuration.sorter.defaultSort.sortDirection,
                sortBy: _.find(configuration.sorter.sortableColumns, function (sc) { return sc.id === configuration.sorter.defaultSort.sortBy; }),
                sortableColumns: configuration.sorter.sortableColumns
            };
        }
        if (configuration.search) {
            state.searching = {
                searchTerm: configuration.search.searchTerm || ""
            };
            state.options.hideSearch = configuration.search.hideSearch;
        }
        if (configuration.grid) {
            state.custom.rowPadding = configuration.grid.rowPadding;
            state.custom.striped = configuration.grid.striped;
        }
        return state;
    };
    GridConverter.prototype.fromComponent = function (state) {
        var filters = {};
        if (state.pagination) {
            var from = (state.pagination.page - 1) * state.pagination.pageSize + 1;
            var to = from + state.pagination.pageSize - 1;
            filters["pagination"] = [{ start: from, end: to }];
        }
        if (state.sorting) {
            filters["sorting"] = [state.sorting.sortBy.id, state.sorting.direction];
        }
        if (state.searching) {
            filters["search"] = [state.searching.searchTerm];
        }
        return {
            filters: filters
        };
    };
    GridConverter.prototype.toComponent = function (data, state) {
        return _.assign({}, state, {
            items: data.items,
            pagination: _.assign({}, state.pagination, { total: data.total })
        });
    };
    GridConverter.prototype.getExcludedFiltersFromStateReset = function () {
        return ["sorting", "pagination"];
    };
    GridConverter.prototype.resetState = function (state) {
        var newState = _.assign({}, state, { pagination: { page: 1, pageSize: state.pagination.pageSize } });
        return newState;
    };
    return GridConverter;
}(filteringConverter_1.FilteringConverter));
exports.GridConverter = GridConverter;


/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var filteringConverter_1 = __webpack_require__(8);
var statusFilterConverter_1 = __webpack_require__(176);
var FilterBarsConverter = /** @class */ (function (_super) {
    __extends(FilterBarsConverter, _super);
    function FilterBarsConverter() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.converters = [];
        return _this;
    }
    FilterBarsConverter.prototype.defaultState = function () {
        return {};
    };
    FilterBarsConverter.prototype.fromConfiguration = function (configuration) {
        var state = this.defaultState();
        for (var _i = 0, configuration_1 = configuration; _i < configuration_1.length; _i++) {
            var filterBar = configuration_1[_i];
            var statusFilterConverter = new statusFilterConverter_1.StatusFilterConverter(filterBar.id);
            state[filterBar.id] = statusFilterConverter.fromConfiguration(filterBar.items);
            this.converters.push(statusFilterConverter);
        }
        return state;
    };
    FilterBarsConverter.prototype.fromComponent = function (state) {
        var filters = {};
        var components = {
            filterIds: []
        };
        for (var _i = 0, _a = this.converters; _i < _a.length; _i++) {
            var filterBar = _a[_i];
            var componentFilters = filterBar.fromComponent(state[filterBar.filterKey]);
            var appliedFilters = componentFilters.filters[filterBar.filterKey];
            filters[filterBar.filterKey] = appliedFilters;
            components.filterIds.push(filterBar.filterKey);
        }
        return {
            filters: filters,
            components: components
        };
    };
    FilterBarsConverter.prototype.toComponent = function (data, state) {
        var _loop_1 = function (filterBar) {
            var converter = _.find(this_1.converters, function (c) { return c.filterKey === filterBar.id; });
            state[filterBar.id] = converter.toComponent(filterBar.items, state[filterBar.id]);
        };
        var this_1 = this;
        for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
            var filterBar = data_1[_i];
            _loop_1(filterBar);
        }
        return state;
    };
    return FilterBarsConverter;
}(filteringConverter_1.FilteringConverter));
exports.FilterBarsConverter = FilterBarsConverter;
;
;


/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var filteringConverter_1 = __webpack_require__(8);
var StatusFilterConverter = /** @class */ (function (_super) {
    __extends(StatusFilterConverter, _super);
    function StatusFilterConverter(filterKey) {
        var _this = _super.call(this) || this;
        _this.filterKey = filterKey;
        _this.tryLoadActiveFilters = function (items, state) {
            var selectedFilters = _this.getSelectedFilters(items);
            if (!_.isEmpty(selectedFilters)) {
                state.activeFilters = selectedFilters;
            }
        };
        _this.buildFilterSource = function (items) {
            var transformedFilterSource = [];
            _.forEach(items, function (value) {
                var item = {
                    name: value.id,
                    tooltip: value.metadata.tooltip || null,
                    tooltipAppendToBody: true,
                    emphasize: value.metadata.emphasize || null,
                    icon: value.metadata.icon || null,
                    value: value.count
                };
                transformedFilterSource.push(item);
            });
            return transformedFilterSource;
        };
        _this.getSelectedFilters = function (items) {
            var selectedFilters = [];
            _.forEach(items, function (value) {
                if (value.selected === true) {
                    selectedFilters.push(value.id);
                }
            });
            return selectedFilters;
        };
        return _this;
    }
    StatusFilterConverter.prototype.defaultState = function () {
        return {
            activeFilters: [],
            filterSource: []
        };
    };
    StatusFilterConverter.prototype.fromConfiguration = function (configuration) {
        var state = this.defaultState();
        this.tryLoadActiveFilters(configuration, state);
        state.filterSource = this.buildFilterSource(configuration);
        return state;
    };
    StatusFilterConverter.prototype.fromComponent = function (state) {
        var filters = {};
        filters[this.filterKey] = _.assign([], state.activeFilters);
        return {
            filters: filters
        };
    };
    StatusFilterConverter.prototype.toComponent = function (data, state) {
        this.tryLoadActiveFilters(data, state);
        state.filterSource = this.buildFilterSource(data);
        return state;
    };
    return StatusFilterConverter;
}(filteringConverter_1.FilteringConverter));
exports.StatusFilterConverter = StatusFilterConverter;
;


/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var filteringConverter_1 = __webpack_require__(8);
var observable_1 = __webpack_require__(50);
var TimeFrameConverter = /** @class */ (function (_super) {
    __extends(TimeFrameConverter, _super);
    function TimeFrameConverter(xuiTimeframeService) {
        var _this = _super.call(this) || this;
        _this.xuiTimeframeService = xuiTimeframeService;
        _this._onChange = new observable_1.Observable();
        return _this;
    }
    Object.defineProperty(TimeFrameConverter.prototype, "onChange", {
        get: function () {
            return this._onChange;
        },
        enumerable: true,
        configurable: true
    });
    TimeFrameConverter.prototype.defaultState = function () {
        return {
            __isLoaded: false
        };
    };
    TimeFrameConverter.prototype.fromConfiguration = function (configuration) {
        var _this = this;
        var state = {
            __isLoaded: true
        };
        state.range = {
            selectedPresetId: configuration.range ? configuration.range.selectedPresetId : undefined,
            startDatetime: configuration.range ? this.readDateTime(configuration.range.startDatetime) : undefined,
            endDatetime: configuration.range ? this.readDateTime(configuration.range.endDatetime) : undefined
        };
        state.config = {
            mode: configuration.mode ? configuration.mode : undefined,
            minDate: this.readDateTime(configuration.minDate),
            maxDate: this.readDateTime(configuration.maxDate)
        };
        if (state.range && state.range.selectedPresetId) {
            var defaultPresetId_1 = state.range.selectedPresetId;
            state.isResetVisible = function () { return state.range.selectedPresetId !== defaultPresetId_1; };
            state.onZoomReset = function () {
                state.range.selectedPresetId = defaultPresetId_1;
                // get current value of timeframe since time has flown a bit since the beginning
                var currentTimeframe = _this.xuiTimeframeService.getTimeframeByPresetId(defaultPresetId_1);
                state.range.startDatetime = currentTimeframe.startDatetime;
                state.range.endDatetime = currentTimeframe.endDatetime;
                _this._onChange.update(_.clone(state.range));
            };
        }
        else {
            var defaultStart_1 = state.range.startDatetime;
            var defaultEnd_1 = state.range.endDatetime;
            state.isResetVisible = function () { return state.range.startDatetime !== defaultStart_1 || state.range.endDatetime !== defaultEnd_1; };
            state.onZoomReset = function () {
                state.range.selectedPresetId = undefined;
                state.range.startDatetime = defaultStart_1;
                state.range.endDatetime = defaultEnd_1;
                _this._onChange.update(state.range);
            };
        }
        state.onRangeShift = function (timeframe) {
            _this._onChange.update(_.clone(timeframe));
        };
        state.onTimeSelection = function (timeframe) {
            // ignored since this event is unnecessarily raised by xuiTimeFramePicker on startup
            if (state.range.startDatetime === timeframe.startDatetime && state.range.endDatetime === timeframe.endDatetime) {
                return;
            }
            _this._onChange.update(_.clone(timeframe));
        };
        return state;
    };
    TimeFrameConverter.prototype.fromComponent = function (state) {
        if (!state.__isLoaded) {
            return {
                filters: {}
            };
        }
        var start = moment(state.range.startDatetime).utc().toISOString();
        var end = moment(state.range.endDatetime).utc().toISOString();
        var filters = {
            "timeFrame": [{ start: start, end: end }]
        };
        return {
            filters: filters
        };
    };
    TimeFrameConverter.prototype.toComponent = function (data, state) {
        return state;
    };
    /**
     * Converts given datetime string into Date instance.
     * It also checks for validity of the datetime (e.g. happens when datetime with zeros is processed).
     */
    TimeFrameConverter.prototype.readDateTime = function (value) {
        if (!value) {
            return undefined;
        }
        var m = moment.utc(value);
        if (m.isValid() && m.unix() > 0) {
            return m.toDate();
        }
        return undefined;
    };
    return TimeFrameConverter;
}(filteringConverter_1.FilteringConverter));
exports.TimeFrameConverter = TimeFrameConverter;
;
;


/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var genericListHeader_controller_1 = __webpack_require__(179);
exports.default = function (module) {
    module.controller("swGenericListWidgetHeader", genericListHeader_controller_1.GenericListHeaderController);
};


/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgetPartialTemplate_decorator_1 = __webpack_require__(7);
var user_service_1 = __webpack_require__(15);
var GenericListHeaderController = /** @class */ (function () {
    /** @ngInject */
    GenericListHeaderController.$inject = ["swUserService", "swOrionViewService", "swDemoService"];
    function GenericListHeaderController(swUserService, swOrionViewService, swDemoService) {
        var _this = this;
        this.swUserService = swUserService;
        this.swOrionViewService = swOrionViewService;
        this.swDemoService = swDemoService;
        this.filterLink = function (link) {
            if (!link.requiredRoles) {
                return true;
            }
            var roles = link.requiredRoles.split(",");
            return roles.some(function (role) { return _this.swUserService.userHasRole(role); });
        };
        this.processLink = function (link) {
            var netObject = _this.swOrionViewService.activeViewInfo.netObject;
            var netObjectParts = (netObject || "").split(":");
            var netObjectPrefix = netObjectParts[0];
            var netObjectId = netObjectParts[1];
            var opid = _this.widget.config.settings.opid;
            return {
                caption: link.caption,
                href: link.href
                    .replace(/\[\[netobject\]\]/gi, netObject)
                    .replace(/\[\[netobjectprefix\]\]/gi, netObjectPrefix)
                    .replace(/\[\[netobjectid\]\]/gi, netObjectId)
                    .replace(/\[\[opid\]\]/gi, opid),
                requiredRoles: link.requiredRoles
            };
        };
    }
    Object.defineProperty(GenericListHeaderController.prototype, "customLinks", {
        get: function () { return this.customLinksLazy = this.customLinksLazy || this.getCustomLinks(); },
        enumerable: true,
        configurable: true
    });
    GenericListHeaderController.prototype.getCustomLinks = function () {
        if (!this.widget.config || !this.widget.config.settings) {
            // we need to wait till widget is completely ready
            return undefined;
        }
        var links = this.widget.config.settings.customlinks;
        if (!links) {
            return [];
        }
        if (!this.swDemoService.isDemoMode()) {
            links = links.filter(this.filterLink);
        }
        return links.map(this.processLink);
    };
    GenericListHeaderController = __decorate([
        widgetPartialTemplate_decorator_1.WidgetPartialTemplate({
            name: "swGenericListWidgetHeader",
            template: __webpack_require__(54),
            controllerAs: "ctrl"
        }),
        __metadata("design:paramtypes", [user_service_1.UserService, Object, Object])
    ], GenericListHeaderController);
    return GenericListHeaderController;
}());
exports.GenericListHeaderController = GenericListHeaderController;


/***/ }),
/* 180 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 181 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widget_1 = __webpack_require__(183);
var editDialogSectionButton_1 = __webpack_require__(197);
exports.default = function (module) {
    widget_1.default(module);
    editDialogSectionButton_1.default(module);
};


/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(184);
var index_2 = __webpack_require__(188);
var index_3 = __webpack_require__(191);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
};


/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widgetOverlay_1 = __webpack_require__(185);
exports.default = function (module) {
    module.component("swWidgetOverlay", widgetOverlay_1.default);
};


/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(186);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var widgetOverlay_controller_1 = __webpack_require__(187);
var WidgetOverlay = /** @class */ (function () {
    function WidgetOverlay() {
        this.require = ["^swWidgetContainer", "swWidgetOverlay"];
        this.restrict = "E";
        this.scope = {};
        this.controller = widgetOverlay_controller_1.WidgetOverlayController;
        this.controllerAs = "$widgetOverlay";
        this.bindToController = {};
        this.templateUrl = "[widgets]:/directives/widget/widgetOverlay/widgetOverlay.html";
        this.link = function (scope, element, attrs, ctrls) {
            var container = ctrls[0], overlay = ctrls[1];
            overlay.initialize(element, container);
        };
    }
    return WidgetOverlay;
}());
exports.default = WidgetOverlay;


/***/ }),
/* 186 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetOverlayController = /** @class */ (function () {
    /** @ngInject */
    WidgetOverlayController.$inject = ["$log", "$q", "swOrionViewService", "swApi", "swDemoService"];
    function WidgetOverlayController($log, $q, swOrionViewService, swApi, swDemoService) {
        var _this = this;
        this.$log = $log;
        this.$q = $q;
        this.swOrionViewService = swOrionViewService;
        this.swApi = swApi;
        this.swDemoService = swDemoService;
        this.$onInit = function () {
            _this._isBusy = false;
            _this._disableRemove = _this.swOrionViewService.activeViewInfo
                && _this.swOrionViewService.activeViewInfo.readOnly;
            _this.appendTo = _this.getAppendTarget();
            if (_this.disableRemove) {
                _this.remove = angular.noop;
            }
        };
        this.initialize = function (element, container) {
            _this.element = element;
            _this.containerId = container.id;
            _this.title = container.title;
        };
        this.remove = function () {
            if (_this.swDemoService.isDemoMode()) {
                _this.swDemoService.showDemoErrorToast();
                return;
            }
            _this._isBusy = true;
            _this.swApi.api(true).one("Resources")
                .post("RemoveResourceFromView", { resourceId: _this.containerId })
                .then(function (result) {
                if (result.ColumnDeleted) {
                    return _this.deleteItem(_this.element.closest(".ResourceColumn"), _this.updateColumnIds);
                }
                if (result.ResourceDeleted) {
                    return _this.deleteItem(_this.element.closest(".ResourceWrapperPanel"));
                }
                _this.$log.warn("Attempted to delete resource " + _this.containerId + ", but server did not delete it");
            })
                .finally(function () { return _this._isBusy = false; });
        };
        this.getAppendTarget = function () {
            var id = "sw-widget-append-target";
            var target = angular.element("#" + id);
            if (target.length) {
                return target;
            }
            return angular
                .element("<div id=\"" + id + "\" class='xui xui-menu-append-target'></div>")
                .appendTo(angular.element("body"));
        };
        this.deleteItem = function (item, callback) {
            if (!item || !item.length) {
                return _this.$q.resolve();
            }
            var defer = _this.$q.defer();
            item.animate({ "opacity": 0 }, 300, function () {
                item.remove();
                defer.resolve(callback && callback());
            });
            return defer.promise;
        };
        this.updateColumnIds = function () {
            //after removing the deleted column, update all the other ids
            angular.forEach(angular.element(".ResourceColumn:not(.ResourceColumn-New)"), function (value, index) {
                var column = angular.element(value);
                column.attr("columnid", index);
                var id = column.attr("id");
                if (_.includes(column.attr("id"), "sw-dynamicAddColumn")) {
                    column.attr("id", "sw-dynamicAddColumn" + (index + 1));
                }
            });
        };
    }
    Object.defineProperty(WidgetOverlayController.prototype, "isBusy", {
        get: function () {
            return this._isBusy;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetOverlayController.prototype, "disableRemove", {
        get: function () {
            return this._disableRemove;
        },
        enumerable: true,
        configurable: true
    });
    return WidgetOverlayController;
}());
exports.WidgetOverlayController = WidgetOverlayController;


/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widgetSettingsForm_1 = __webpack_require__(189);
exports.default = function (module) {
    module.component("swWidgetSettingsForm", widgetSettingsForm_1.WidgetSettingsForm);
    module.controller("swWidgetSettingsFormController", widgetSettingsForm_1.WidgetSettingsFormController);
};


/***/ }),
/* 189 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(190);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WidgetSettingsForm = /** @class */ (function () {
    function WidgetSettingsForm() {
        this.restrict = "E";
        this.transclude = true;
        this.templateUrl = "[widgets]:/directives/widget/widgetSettingsForm/widgetSettingsForm.html";
        this.controller = WidgetSettingsFormController;
        this.controllerAs = "vmForm";
        this.bindToController = {
            settings: "=swWidgetSettingsData",
            options: "=swWidgetSettingsOptions"
        };
    }
    return WidgetSettingsForm;
}());
exports.WidgetSettingsForm = WidgetSettingsForm;
var WidgetSettingsFormController = /** @class */ (function () {
    function WidgetSettingsFormController() {
    }
    return WidgetSettingsFormController;
}());
exports.WidgetSettingsFormController = WidgetSettingsFormController;


/***/ }),
/* 190 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 191 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widgetDrawer_1 = __webpack_require__(192);
var widgetDropZone_1 = __webpack_require__(195);
exports.default = function (module) {
    module.component("swWidgetDrawer", widgetDrawer_1.default);
    module.component("swWidgetDropZone", widgetDropZone_1.default);
};


/***/ }),
/* 192 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(193);

"use strict";
/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var widgetDrawer_controller_1 = __webpack_require__(194);
var WidgetDrawer = /** @class */ (function () {
    /** @ngInject */
    WidgetDrawer.$inject = ["$rootScope", "$timeout", "$window", "swDemoService"];
    function WidgetDrawer($rootScope, $timeout, $window, swDemoService) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;
        this.$window = $window;
        this.swDemoService = swDemoService;
        this.restrict = "E";
        this.template = __webpack_require__(55);
        this.controller = widgetDrawer_controller_1.default;
        this.controllerAs = "$widgetDrawer";
        this.bindToController = {
            isOpen: "=?",
        };
        this.scope = {};
        this.link = function (scope, element, attrs, ctrl) {
            element = element.find(".sw-widget-drawer");
            element.keypress(function (event) {
                //don't let enter bubble up to the form & trigger a submit UIF-3897
                if (event.which === 13) {
                    return false;
                }
            });
            var bottomLimitingElementSelector = "#footer";
            var newColumnPlaceholderSelector = "table.ResourceContainer .ResourceColumn-New .ResourceColumn-Center";
            var swWidgetScaleRatio = {
                initial: 1,
                drawerOpen: 0.6
            };
            ctrl.onDragStart = function (event) {
                _this.$rootScope.drawerDragging =
                    _this.$rootScope.drawerDropping =
                        ctrl.isDragging =
                            ctrl.isDropping = true;
                element.addClass("sw-widget-drawer--dragging");
                var updateNewColumnPlaceholderView = function () {
                    ctrl.setNewColumnPlaceholderPosition(newColumnPlaceholderSelector, swWidgetScaleRatio.drawerOpen);
                    ctrl.setNewColumnPlaceholderHeight(newColumnPlaceholderSelector, swWidgetScaleRatio.drawerOpen, bottomLimitingElementSelector);
                };
                updateNewColumnPlaceholderView();
                ctrl.registerScrollEvent(updateNewColumnPlaceholderView);
            };
            ctrl.onDragEnd = function () {
                _this.$rootScope.drawerDragging =
                    ctrl.isDragging = false;
                element.removeClass("sw-widget-drawer--dragging");
                ctrl.unregisterScrollEvent();
            };
            _this.$rootScope.$watch("drawerDragging", function (newValue, oldValue) {
                angular.element("body").toggleClass("sw-widget-drawer--dropping", newValue === true);
            });
            _this.$rootScope.$watch("editingDashboard", function (newValue, oldValue) {
                angular.element("body").toggleClass("sw-dashboard--editing", newValue === true);
            });
            element.find(".sw-widget-drawer__backdrop").mousedown(function (event) {
                if (!element.find(".sw-widget-drawer__panel")[0].contains(event.target) &&
                    element.hasClass("sw-widget-drawer--open")) {
                    scope.$apply(ctrl.close);
                }
            });
            var isLoaded = false;
            scope.$watch(function () { return ctrl.isOpen; }, function (newValue, oldValue) {
                element.toggleClass("sw-widget-drawer--open", newValue);
                var $body = angular.element("body");
                $body.css("overflow-y", newValue === true ? "hidden" : "");
                angular.element(".ui-resizable-handle.ui-resizable-e").css({ display: newValue ? "none" : "block" });
                var resourceContainerElem = angular.element("table.ResourceContainer");
                resourceContainerElem.css("transform-origin", "left " + $body.scrollTop() + "px");
                var scaleRatio = swWidgetScaleRatio.initial;
                if (newValue) {
                    $body.animate({
                        scrollTop: _this.$window.scrollY,
                        scrollLeft: _this.$window.scrollX + 520
                    }, 300);
                    scaleRatio = swWidgetScaleRatio.drawerOpen;
                    // It extends table height up to the very bottom of screen
                    // It makes footer to be always placed below and prevents unpredictable case.
                    // Case: a lot of space is between table and a footer, but the footer disappear
                    // when new widget dragging is started.
                    // (Empty space actually contains invisible placeholders on drag start)
                    // Enough space is for footer before a drag, too few on drag
                    ctrl.setRelativeViewHeight(resourceContainerElem, resourceContainerElem.offset().top);
                }
                else {
                    $body.animate({
                        scrollTop: _this.$window.scrollY,
                        scrollLeft: _this.$window.scrollX - 520
                    }, 300);
                    resourceContainerElem.css({
                        "height": ""
                    });
                }
                resourceContainerElem.css({
                    "transform": "scale(" + scaleRatio + ")"
                });
                if (!isLoaded && newValue === true) {
                    isLoaded = true;
                    // Delay a bit so we're not competing with the drawer animation.
                    _this.$timeout(function () {
                        ctrl.onCategoryGroupChanged(ctrl.selectedCategoryGroup);
                    }, 400);
                }
            });
        };
    }
    return WidgetDrawer;
}());
exports.default = WidgetDrawer;


/***/ }),
/* 193 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 194 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetDrawerController = /** @class */ (function () {
    function WidgetDrawerController($rootScope, $timeout, widgetService, _t, orionViewService) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;
        this.widgetService = widgetService;
        this._t = _t;
        this.orionViewService = orionViewService;
        this.$onInit = function () {
            _this.initFields();
            _this.viewInfo = _this.orionViewService.activeViewInfo;
            _this.$rootScope.$on("JQ_SORTABLE_START-OVER", function (unused, data) {
                var updateNewColumnPlaceholderView = function () {
                    _this.setNewColumnPlaceholderPosition(data.placeholderId, data.scaleRatio);
                    _this.setNewColumnPlaceholderHeight(data.placeholderId, data.scaleRatio, data.bottomLimitingElementId);
                };
                updateNewColumnPlaceholderView();
                _this.registerScrollEvent(updateNewColumnPlaceholderView);
            });
        };
        this.open = function () {
            _this.isOpen = true;
        };
        this.close = function () {
            _this.isOpen = false;
        };
        this.toggleFilter = function () {
            _this.isFilterOpen = !_this.isFilterOpen;
        };
        this.onClear = function () {
            _this.searchTerm = "";
            _this.pageNumber = 1;
            _this.isSearching = false;
            return _this.updateResults();
        };
        this.onSearch = function (value, cancellation) {
            if (value) {
                _this.isSearching = true;
                _this.searchCountUpdateNeeded = true;
                _this._selectedCategories = [_this.categories[0]];
                cancellation.then(function () {
                    _this.isSearching = false;
                    _this.pageNumber = 1;
                    return _this.updateResults();
                });
                _this.pageNumber = 1;
                return _this.updateResults();
            }
        };
        this.toggleFavorite = function (item) {
            _this.widgetService.toggleFavorite(item);
        };
        this.getIconTooltip = function (widget) {
            return _this.iconTooltip[_this.getIconName(widget)];
        };
        this.getIconName = function (widget) {
            return WidgetDrawerController.drawerItemMap[widget.DrawerIcon.toLowerCase()] || "widget_other";
        };
        this.onSortChange = function (newValue, oldValue) {
            _this.updateResults();
        };
        this.onPageChanged = function (page, pageSize, total) {
            _this.pageNumber = page;
            _this.pageSize = pageSize;
            _this.updateResults();
        };
        this.onCategoryChanged = function (newValue) {
            _this.pageNumber = 1;
            _this._selectedCategories = newValue;
            return _this.updateResults();
        };
        this.onCategoryGroupChanged = function (newValue) {
            _this.widgetService
                .getGroupCategories(newValue.value, _this.viewInfo.viewType || "Summary")
                .then(function (results) {
                _this.categories = results.rows;
                _this.categories.unshift({
                    GroupName: newValue.value,
                    CategoryName: "",
                    CategoryDisplayName: _this._t("All results"),
                    CategorySubname: "",
                    InstanceCount: 0,
                    IsAllResults: true
                });
                if (_this.isSearching) {
                    _this.searchCountUpdateNeeded = true;
                    return _this.onCategoryChanged([_this.categories[0]]);
                }
                else {
                    return _this.onCategoryChanged([_this.categories[1]]);
                }
            })
                .finally(function () {
                _this.isBusy = false;
            });
        };
        this.initFields = function () {
            _this.isBusy = true;
            _this.isOpen = false;
            _this.isFilterOpen = true;
            _this.totalItems = 0;
            _this.isDragging = false;
            _this.isDropping = false;
            _this.isSearching = false;
            _this.searchCountUpdateNeeded = false;
            _this.pageNumber = 1;
            _this.pageSize = 20;
            // Sort state
            _this.sortColumns = [
                { name: _this._t("Name"), value: "Title" },
                { name: _this._t("Category"), value: "Category" },
                { name: _this._t("Favorite"), value: "IsFavorite" }
            ];
            _this.sortSelection = _this.sortColumns[0];
            // Category group state
            _this.categoryGroups = [
                { name: _this._t("Type"), value: "Type" },
                { name: _this._t("Feature"), value: "Owner" },
                { name: _this._t("Classic"), value: "ClassicCategory" }
            ];
            _this.selectedCategoryGroup = _this.categoryGroups[0];
            _this.categories = [];
            _this._selectedCategories = [];
            _this.items = [];
            _this.iconTooltip = {
                "widget_table": _this._t("Table"),
                "widget_chart": _this._t("Chart"),
                "widget_pie-chart": _this._t("Pie Chart"),
                "widget_map": _this._t("Map"),
                "widget_gauge": _this._t("Gauge"),
                "widget_tree": _this._t("Tree"),
                "widget_summary": _this._t("Summary"),
                "widget_list": _this._t("List"),
                "widget_other": _this._t("Other")
            };
        };
        this.updateResults = function () {
            _this.isBusy = true;
            var searchCategory = "";
            var searchGroup = _this.selectedCategoryGroup.value;
            //If there is no selected category, you may be searching
            if (_this.selectedCategories && _this.selectedCategories.length > 0) {
                var category = _this.selectedCategories[0];
                searchCategory = category.CategoryName;
            }
            return _this.widgetService
                .getWidgets(_this.viewInfo.viewType || "Summary", searchCategory, _this.isSearching ? _this.searchTerm : "", searchGroup, _this.pageNumber, _this.pageSize, _this.sortSelection.value, _this.sortDirection)
                .then(function (results) {
                _this.items = results.rows;
                angular.forEach(_this.items, function (item) {
                    //allow the items to know about the search term, so they can highlight
                    item.searchValue = _this.isSearching ? _this.searchTerm : "";
                    item.viewInfo = _this.viewInfo;
                    if (item.ShortDescription) {
                        item.Tooltip = item.ShortDescription + ". " + item.Tooltip;
                    }
                });
                _this.totalItems = results.total;
                _this.selection = _this.items[0];
                if (_this.isSearching && _this.searchCountUpdateNeeded) {
                    angular.forEach(_this.categories, function (category) {
                        category.InstanceCount = 0;
                        if (results.groupCounts[category.CategoryName]) {
                            category.InstanceCount = results.groupCounts[category.CategoryName];
                        }
                        if (category.IsAllResults) {
                            category.InstanceCount = results.total;
                        }
                    });
                    _this.searchCountUpdateNeeded = false;
                }
            })
                .finally(function () {
                _this.isBusy = false;
            });
        };
        this.setNewColumnPlaceholderPosition = function (elementSelector, scaleRatio) {
            if (scaleRatio === void 0) { scaleRatio = 1; }
            var newColumnPlaceholder = angular.element(elementSelector);
            newColumnPlaceholder.css({
                "top": angular.element(window).scrollTop() / scaleRatio
            });
        };
        this.setNewColumnPlaceholderHeight = function (elementSelector, scaleRatio, bottomLimitingElementSelector) {
            if (scaleRatio === void 0) { scaleRatio = 1; }
            if (bottomLimitingElementSelector === void 0) { bottomLimitingElementSelector = ""; }
            var newColumnPlaceholderBottomSpace = 20;
            var newColumnPlaceholder = angular.element(elementSelector);
            if (newColumnPlaceholder.length) {
                var newColumnPlaceholderOffsetTop = newColumnPlaceholder.offset().top;
                var newColumnPlaceholderTop = newColumnPlaceholder.position().top;
                _this.setRelativeViewHeight(newColumnPlaceholder, newColumnPlaceholderOffsetTop - newColumnPlaceholderTop + newColumnPlaceholderBottomSpace, scaleRatio);
                newColumnPlaceholder.css({
                    "max-height": "none" // it's necessary to remove potential limitations
                });
                if (bottomLimitingElementSelector) {
                    var bottomLimitingElement = angular.element(bottomLimitingElementSelector);
                    if (bottomLimitingElement) {
                        var bottomLimitingElementOffsetTop = bottomLimitingElement.offset().top;
                        var newColumnPlaceholderMaxHeight = (bottomLimitingElementOffsetTop / scaleRatio) -
                            (newColumnPlaceholderOffsetTop / scaleRatio) - newColumnPlaceholderBottomSpace;
                        newColumnPlaceholder.css({
                            "max-height": newColumnPlaceholderMaxHeight
                        });
                    }
                }
            }
        };
        this.setRelativeViewHeight = function (element, additionalLimit, scaleRatio) {
            if (additionalLimit === void 0) { additionalLimit = 0; }
            if (scaleRatio === void 0) { scaleRatio = 1; }
            var heightExpr = "100vh - " + additionalLimit + "px";
            if (scaleRatio && scaleRatio !== 1) {
                heightExpr = "(" + heightExpr + ") / " + scaleRatio;
            }
            element.css({
                height: "calc(" + heightExpr + ")",
            });
        };
        this.registerScrollEvent = function (scrollHandler) {
            _this.unregisterScrollEvent(); // reset
            var wait = 10; // ms
            _this.registeredScrollHandler = _.throttle(scrollHandler, wait);
            angular.element(window).scroll(_this.registeredScrollHandler);
        };
        this.unregisterScrollEvent = function () {
            if (_this.isRegisteredScrollEvent()) {
                angular.element(window).off("scroll", _this.registeredScrollHandler);
                _this.registeredScrollHandler = undefined;
            }
        };
        this.isRegisteredScrollEvent = function () {
            return !!_this.registeredScrollHandler;
        };
    }
    Object.defineProperty(WidgetDrawerController.prototype, "selectedCategories", {
        get: function () {
            return this._selectedCategories;
        },
        set: function (values) {
            this.onCategoryChanged(values);
        },
        enumerable: true,
        configurable: true
    });
    WidgetDrawerController.$inject = ["$rootScope", "$timeout", "swWidgetService", "getTextService", "swOrionViewService"];
    WidgetDrawerController.drawerItemMap = {
        "table": "widget_table",
        "chart": "widget_chart",
        "piechart": "widget_pie-chart",
        "map": "widget_map",
        "gauges": "widget_gauge",
        "tree": "widget_tree",
        "summary": "widget_summary",
        "list": "widget_list",
        "other": "widget_other"
    };
    return WidgetDrawerController;
}());
exports.WidgetDrawerController = WidgetDrawerController;
exports.default = WidgetDrawerController;


/***/ }),
/* 195 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var widgetDropHandler_1 = __webpack_require__(196);
var WidgetDropZone = /** @class */ (function () {
    function WidgetDropZone($rootScope, $log, $timeout, $compile, $location, constants, $q, swDemo, swApi) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$log = $log;
        this.$timeout = $timeout;
        this.$compile = $compile;
        this.$location = $location;
        this.constants = constants;
        this.$q = $q;
        this.swDemo = swDemo;
        this.swApi = swApi;
        this.restrict = "A";
        this.require = ["xuiDroppable"];
        this.compile = function (element, attrs) {
            _this._placeholder = angular.element(".sw-widget-drawer__placeholder");
            if (_this._placeholder.length === 0) {
                _this._placeholder = angular
                    .element(__webpack_require__(58))
                    .appendTo("body");
            }
            return {
                post: _this.postLink
            };
        };
        this.postLink = function (scope, element, attrs, ctrls) {
            var droppable = ctrls[0];
            var wrapperPanels;
            var current;
            var addToTop;
            var isScaledValue = false;
            var newColumnClassOnDropOver = "sw-widget-drawer--drop-over";
            var cleanup = function (reset) {
                if (reset === void 0) { reset = false; }
                var dropHolderClass = "sw-widget-drawer__drop-holder sw-widget-drawer__drop-holder--top";
                if (current) {
                    current.removeClass(dropHolderClass);
                    current = undefined;
                }
                if (reset) {
                    _this._placeholder.css({ opacity: 0 });
                    element.find(".ResourceWrapperPanel").removeClass(dropHolderClass);
                }
            };
            var positionPlaceholder = function (widget, pageY) {
                if (!widget) {
                    return;
                }
                var top = widget.data("widgetTop");
                var bounds = widget[0].getBoundingClientRect();
                var vCenter = widget.offset().top + (bounds.height / 2);
                var positionTop = pageY < vCenter;
                _this._placeholder.css({
                    top: positionTop ? top : top + bounds.height + 6,
                    left: widget.offset().left,
                    width: bounds.width - 10,
                    opacity: 1
                });
                return positionTop;
            };
            var getNearestLocatedWidget = function (widgets, pageY) {
                var widget = null;
                widgets
                    .each(function (index, currentWidget) {
                    widget = currentWidget;
                    return currentWidget.getBoundingClientRect().top < pageY;
                });
                return jQuery(widget);
            };
            droppable.onDragEnter = function (params) {
                if (!_this.$rootScope.editingDashboard) {
                    return;
                }
                wrapperPanels = element.find(".ResourceWrapperPanel");
                wrapperPanels.each(function (index, el) {
                    var $element = jQuery(el);
                    $element.data("widgetTop", $element.offset().top);
                });
                var isNewColumn = element.hasClass("ResourceColumn-New");
                if (isNewColumn) {
                    element.addClass(newColumnClassOnDropOver);
                }
            };
            droppable.onDragOver = function (params) {
                if (!_this.$rootScope.editingDashboard) {
                    return;
                }
                var target = jQuery(params.event.target);
                var widget = target.closest(".ResourceWrapperPanel");
                var pageY = params.event.pageY;
                // We consider it's the same widget as previous one if event target coincides with it or is contained in.
                var targetIsCurrent = current &&
                    (current[0] === params.event.target || current[0].contains(params.event.target));
                // Try to get nearest (in terms if vertical distance) widget
                if (!targetIsCurrent && 0 === widget.length) {
                    var allWidgetsInColumn = target
                        .closest(".ResourceColumn")
                        .children(".ResourceWrapperPanel");
                    widget = getNearestLocatedWidget(allWidgetsInColumn, pageY);
                }
                // Are we over the current widget?
                if (!targetIsCurrent) {
                    cleanup();
                }
                // didn't find a widget
                if (0 === widget.length) {
                    return;
                }
                addToTop = positionPlaceholder(widget, pageY);
                widget
                    .addClass("sw-widget-drawer__drop-holder")
                    .toggleClass("sw-widget-drawer__drop-holder--top", addToTop);
                widget.prevAll(".ResourceWrapperPanel").removeClass("sw-widget-drawer__drop-holder--top");
                widget.nextAll(".ResourceWrapperPanel").addClass("sw-widget-drawer__drop-holder--top");
                current = widget;
            };
            droppable.onDragLeave = function (params) {
                cleanup(true);
                var isNewColumn = element.hasClass("ResourceColumn-New");
                if (isNewColumn) {
                    element.removeClass(newColumnClassOnDropOver);
                }
            };
            var dropHandler = new widgetDropHandler_1.WidgetDropHandler(element, cleanup, _this.$rootScope, _this.$log, _this.$timeout, _this.$compile, _this.$location, _this.constants, _this.$q, _this.swApi, _this.swDemo);
            droppable.onDrop = function (params) {
                if (!_this.$rootScope.drawerDropping) {
                    return;
                }
                if (_this.swDemo.isDemoMode()) {
                    cleanup(true);
                    var isNewColumn = element.hasClass("ResourceColumn-New");
                    if (isNewColumn) {
                        element.removeClass(newColumnClassOnDropOver);
                    }
                    _this.swDemo.showDemoErrorToast();
                    return;
                }
                var obj = params.transferObject;
                var target = params.event.currentTarget;
                return dropHandler.onDrop(obj, target, current, addToTop);
            };
        };
    }
    WidgetDropZone.$inject = [
        "$rootScope", "$log", "$timeout", "$compile", "$location", "constants", "$q", "swDemoService", "swApi"
    ];
    return WidgetDropZone;
}());
exports.default = WidgetDropZone;


/***/ }),
/* 196 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetDropHandler = /** @class */ (function () {
    function WidgetDropHandler(element, cleanup, $rootScope, $log, $timeout, $compile, $location, constants, $q, swApi, swDemo) {
        var _this = this;
        this.element = element;
        this.cleanup = cleanup;
        this.$rootScope = $rootScope;
        this.$log = $log;
        this.$timeout = $timeout;
        this.$compile = $compile;
        this.$location = $location;
        this.constants = constants;
        this.$q = $q;
        this.swApi = swApi;
        this.swDemo = swDemo;
        this.prependColumn = function (data) {
            var newColumn = _this.addColumn(_this.element, data.repositionData.viewId, data.repositionData.columnNumber);
            newColumn.prepend(data.draggingElement);
        };
        // handles JQuery UI events
        this.handleJQSortableEvents = function () {
            // handles stop event outside of angular context,
            // when legacy widget is moved to 'ResourceColumn-New'
            if (_this.element.hasClass("ResourceColumn-New")) {
                _this.$rootScope.$on("JQ_SORTABLE_STOP", function (unused, data) {
                    if (!_this.swDemo.isDemoMode()) {
                        _this.swApi.api(true).one("Resources")
                            .post("MoveResourceToNewColumn", data.repositionData)
                            .then(function () { return _this.prependColumn(data); });
                    }
                    else {
                        _this.prependColumn(data);
                    }
                });
            }
        };
        this.addColumn = function (dropLocation, viewId, columnId) {
            var columnTag = "sw-dynamicAddColumn" + columnId;
            var newColumn = angular.element("<td id=" + columnTag + " columnid=\"" + columnId + "\" xui-droppable=\"\" "
                + ("class=\"ResourceColumn ui-sortable ui-resizable\" sw-widget-drop-zone=\"\" viewid=\"" + viewId + "\">")
                + "<div class=\"ResourceColumn_drag-spacer\"></div></td>");
            _this.$compile(newColumn)(_this.$rootScope);
            dropLocation.before(newColumn);
            // Init jQuery sortable
            jQuery("#" + columnTag).sortable(_this.$rootScope.SW.Core.ResourceContainer.sortableDefinition);
            // Init jQuery Resize
            jQuery("#" + columnTag).resizable(_this.$rootScope.SW.Core.ResourceContainer.resizableDefinition);
            newColumn.children(".ui-resizable-handle.ui-resizable-e").css({ opacity: 0 }).hover(function () {
                jQuery(this).css("opacity", 1);
            }, function () {
                jQuery(this).css("opacity", 0);
            });
            return newColumn;
        };
        this.getParentColumn = function (widgetDroppedOn, dropLocation, viewId) {
            if (widgetDroppedOn) {
                return angular.element(widgetDroppedOn.parent(".ResourceColumn"));
            }
            //we probably landed on an empty column or the new column target
            var droppedColumn = angular.element(dropLocation);
            var isNewColumn = droppedColumn.hasClass("ResourceColumn-New");
            if (!isNewColumn) {
                return droppedColumn;
            }
            var existingColumns = jQuery(".ResourceColumn:not(.ResourceColumn-New)");
            var maxColumnID = +existingColumns.last()[0].getAttribute("ColumnID");
            var columnId = maxColumnID + 1;
            //don't let us create more columns than the db can handle
            if (columnId > _this.constants.dashboards.maxColumns) {
                throw new Error("Attempting to add more than 6 columns");
            }
            return _this.addColumn(droppedColumn, viewId, columnId);
        };
        this.populateWidgetPlaceholder = function (placeholder, obj, viewId, columnId, newPosition, childScope, enforcedWidth) {
            // some resources require this in the query string.
            var queryParams = {};
            if (obj.viewInfo.netObject) {
                queryParams.NetObject = obj.viewInfo.netObject;
            }
            var postParams = {
                resourcePaths: [obj.Path],
                viewId: viewId,
                columnNumber: columnId,
                position: newPosition,
                // Legacy params
                NetObject: obj.viewInfo.netObject,
                NamingContainer: "",
                isNOCView: "",
                currentUrl: btoa(_this.$location.absUrl()) //this does not seem to be used by the service
            };
            var deferred = _this.$q.defer();
            var onError = function () {
                WidgetDropHandler.requestsInFlight--;
                if (WidgetDropHandler.requestsInFlight <= 0) {
                    jQuery(".ResourceColumn").sortable("enable");
                }
                var insertItem = angular.element(__webpack_require__(56));
                childScope.reload = function () {
                    window.location.reload(true);
                };
                _this.$compile(insertItem)(childScope);
                //normally, the enforced width would come from the server.  Our fake widget needs to match it's siblings
                if (enforcedWidth) {
                    insertItem.width(enforcedWidth);
                }
                placeholder.empty().append(insertItem);
                deferred.reject(false);
            };
            var onComplete = function () {
                WidgetDropHandler.requestsInFlight--;
                if (WidgetDropHandler.requestsInFlight <= 0) {
                    jQuery(".ResourceColumn").sortable("enable");
                }
                placeholder.addClass("sw-widget-overlay--new");
                deferred.resolve(true);
                _this.$timeout(function () {
                    var location = placeholder.offset();
                    location.left -= 20;
                    location.top -= 100;
                    jQuery("body").animate({
                        scrollTop: location.top,
                        scrollLeft: location.left
                    }, "slow");
                    var overlay = placeholder.find(".sw-widget-overlay");
                    overlay.addClass("sw-widget-overlay--highlight");
                    _this.$timeout(function () { return overlay.removeClass("sw-widget-overlay--highlight"); }, 3000);
                }, 400);
            };
            //fade the progress indicator in- usually the load is very quick, and we want to avoid a content flash
            placeholder.children().animate({
                opacity: 1
            }, "slow");
            //put this in a timeout- if we replace the element too soon, it looks like the angular compile overwrites it
            _this.$timeout(function () {
                _this.$rootScope.SW.Core.Loader.Control(placeholder.children(), queryParams, postParams, "replace", onError, onComplete);
            });
            _this.$rootScope.drawerDropping = false;
            _this.$rootScope.editingDashboard = true;
            //tell the drop zone to clean up spacer classes
            _this.cleanup(true);
            //need to tell jquery ui that an item has been added to the column
            jQuery(".ResourceColumn").sortable("refresh");
            WidgetDropHandler.requestsInFlight++;
            jQuery(".ResourceColumn").sortable("disable");
            return deferred.promise;
        };
        /**
         * Return width of the first widget from column and null in case there are no widgets
         */
        this.getEnforcedWidth = function (parentColumn) {
            var firstWidgetInColumn = parentColumn.find(".ResourceWrapper:first");
            return firstWidgetInColumn.width();
        };
        this.onDrop = function (obj, target, current, addToTop) {
            var widgetDroppedOn = current, viewId = obj.viewInfo.viewId, parentColumn = _this.getParentColumn(widgetDroppedOn, target, viewId), columnId = +parentColumn[0].getAttribute("ColumnID");
            //need to get this value BEFORE placeholder is added
            var enforcedWidth = _this.getEnforcedWidth(parentColumn);
            var insertItem = angular.element(__webpack_require__(57));
            var childScope = _this.$rootScope.$new();
            childScope.widgetTitle = obj.Title;
            _this.$compile(insertItem)(childScope);
            if (enforcedWidth) {
                insertItem.find(".ResourceWrapper").width(enforcedWidth);
            }
            var newPosition = 1;
            if (widgetDroppedOn) {
                var indexOfExistingControl = parentColumn
                    .children("div.ResourceWrapperPanel")
                    .index(widgetDroppedOn);
                newPosition = indexOfExistingControl + 1;
                if (addToTop) {
                    widgetDroppedOn.before(insertItem);
                }
                else {
                    widgetDroppedOn.after(insertItem);
                    newPosition++;
                }
            }
            else {
                //first item in column
                parentColumn.prepend(insertItem);
            }
            return _this.populateWidgetPlaceholder(insertItem, obj, viewId, columnId, newPosition, childScope, enforcedWidth);
        };
        this.handleJQSortableEvents();
    }
    //this is static, so we can coordinate across columns
    WidgetDropHandler.requestsInFlight = 0;
    return WidgetDropHandler;
}());
exports.WidgetDropHandler = WidgetDropHandler;


/***/ }),
/* 197 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var editDialogSectionButton_1 = __webpack_require__(198);
exports.default = function (module) {
    module.component("swEditDialogSectionButton", editDialogSectionButton_1.EditDialogSectionButton);
};


/***/ }),
/* 198 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(199);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EditDialogSectionButton = /** @class */ (function () {
    function EditDialogSectionButton() {
        this.restrict = "E";
        this.transclude = true;
        this.templateUrl = "[widgets]:/directives/editDialogSectionButton/editDialogSectionButton.html";
        this.controller = EditDialogSectionButtonController;
        this.controllerAs = "vm";
        this.bindToController = {
            title: "@",
            subtitle: "@",
            comment: "@?",
            icon: "@",
            click: "&"
        };
    }
    return EditDialogSectionButton;
}());
exports.EditDialogSectionButton = EditDialogSectionButton;
var EditDialogSectionButtonController = /** @class */ (function () {
    function EditDialogSectionButtonController() {
    }
    return EditDialogSectionButtonController;
}());
exports.EditDialogSectionButtonController = EditDialogSectionButtonController;
exports.default = EditDialogSectionButton;


/***/ }),
/* 199 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the repo.
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(201);
    req.keys().forEach(function (r) {
        var key = "[widgets]:" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/chart/chartWidget-listview.html": 32,
	"./components/chart/chartWidget.html": 33,
	"./components/chart/chartWidgetSettings-dialog.html": 35,
	"./components/chart/chartWidgetSettings-percentile-popover.html": 34,
	"./components/chart/header/chartHeader.html": 36,
	"./components/datalist/datalist-widget.html": 17,
	"./components/genericList/genericList-widget.html": 53,
	"./components/genericList/header/genericListHeader.html": 54,
	"./components/genericList/templates/genericList-emptyData.html": 51,
	"./components/genericList/templates/rows/default/defaultGenericListRow.html": 52,
	"./components/list/list-edit-dialog.html": 45,
	"./components/list/list-item-template.html": 202,
	"./components/list/list-widget.html": 44,
	"./components/list/plugins/SWQL-directive.html": 46,
	"./components/perfstack/perfstackEmpty/perfstackEmpty.html": 41,
	"./components/perfstack/perfstackHeader/perfstackHeader.html": 39,
	"./components/perfstack/perfstackProjectSelector/perfstackProjectSelector-item.html": 203,
	"./components/perfstack/perfstackProjectSelector/perfstackProjectSelector.html": 40,
	"./components/perfstack/perfstackWidget.html": 38,
	"./components/perfstack/perfstackWidgetSettings/perfstackWidgetSettings.html": 42,
	"./components/tabular/allAlerts/allAlerts-ack-dialog.html": 204,
	"./components/tabular/allAlerts/allAlerts-directive-template.html": 205,
	"./components/tabular/allAlerts/allAlerts-edit-dialog.html": 206,
	"./components/tabular/allAlerts/allAlerts-widget.html": 207,
	"./components/tile/dialogs/advancedFilter/advanced-filter-dialog.html": 49,
	"./components/tile/dialogs/basicFilter/basic-filter-dialog.html": 48,
	"./components/tile/dialogs/mainFilter/main-filter-dialog.html": 208,
	"./components/tile/tile-widget.html": 209,
	"./directives/editDialogSectionButton/editDialogSectionButton.html": 210,
	"./directives/widget/widgetDrawer/widgetCategory-item.html": 211,
	"./directives/widget/widgetDrawer/widgetDrawer-item.html": 212,
	"./directives/widget/widgetDrawer/widgetDrawer.html": 55,
	"./directives/widget/widgetDrawer/widgetDropZone-placeholder.html": 58,
	"./directives/widget/widgetDrawer/widgetErrorTemplate.html": 56,
	"./directives/widget/widgetDrawer/widgetLoadingTemplate.html": 57,
	"./directives/widget/widgetOverlay/widgetOverlay.html": 213,
	"./directives/widget/widgetSettingsForm/widgetSettingsForm.html": 214,
	"./orionModules/asa/components/asaInterfaces/asaInterfaces.html": 59,
	"./orionModules/asa/components/cliCredentialsNotification/cliCredentialsNotification.html": 60,
	"./orionModules/asa/components/contextList/contextList.html": 61,
	"./orionModules/asa/components/highAvailabilityChart/highAvailabilityChart.html": 12,
	"./orionModules/asa/components/nodePropertiesLink/nodePropertiesLink.html": 62,
	"./orionModules/asa/components/platformDetails/platformDetails.html": 63,
	"./orionModules/asa/components/platformDetails/platformDetailsOther.html": 64,
	"./orionModules/asa/components/serverSideFilteredList/serverSideFilteredList.html": 65,
	"./orionModules/asa/components/sessionList/sessionList.html": 66,
	"./orionModules/asa/components/sessionList/sessionListItem.html": 67,
	"./orionModules/asa/components/siteToSiteFilteredTunnels/siteToSiteFilteredTunnels.html": 68,
	"./orionModules/asa/components/siteToSiteFilteredTunnels/siteToSiteFilteredTunnelsItem.html": 69,
	"./orionModules/asa/widgets/asaInterfaces/asaInterfacesWidget.html": 70,
	"./orionModules/asa/widgets/contextList/contextListWidget.html": 71,
	"./orionModules/asa/widgets/highAvailabilityChart/highAvailabilityChartWidget.html": 72,
	"./orionModules/asa/widgets/platformDetails/platformDetailsWidget.html": 73,
	"./orionModules/asa/widgets/remoteAccessVpnTunnel/remoteAccessVpnTunnelWidget.html": 74,
	"./orionModules/asa/widgets/siteToSiteTunnels/siteToSiteTunnels-widget.html": 75,
	"./orionModules/ncm/components/accessList/accessList-directive.html": 76,
	"./orionModules/ncm/components/accessList/accessList-item-grid-row.html": 215,
	"./orionModules/ncm/components/accessList/accessList-item-template-level1.html": 216,
	"./orionModules/ncm/components/accessList/accessList-item-template-level2.html": 217,
	"./orionModules/ncm/components/accessList/srdIcon/aclSrdIcon-directive.html": 77,
	"./orionModules/ncm/widgets/accessLists/accessListsWidget.html": 78,
	"./services/perfstackDialog-service/perfstackSelect-dialog.html": 218
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 201;

/***/ }),
/* 202 */
/***/ (function(module, exports) {

module.exports = "<div style=display:flex;justify-content:space-between> <span _t>Name = {{item.name}}</span> <xui-icon icon={{item.icon}}></xui-icon> </div> ";

/***/ }),
/* 203 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-perfstack-project-selector-item> <div class=\"xui-text-s sw-perfstack-project-selector-item__updated\" _t> Updated {{::item.updated.fromNow()}} </div> <span class=xui-text-l> {{::item.name}} </span> <span class=\"xui-text-s sw-perfstack-project-selector-item__owner\" _t> by {{::item.owner}} </span> </div> ";

/***/ }),
/* 204 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <xui-message type=info> <span _t>Acknowledging the alert prevents all associated actions (e.g. send email etc.) from firing. You can add a note that displays on the alert details page.</span> </xui-message> <xui-textbox rows=5 caption=_t(Notes) ng-model=vm.dialogOptions.viewModel.noteText _ta></xui-textbox> </xui-dialog>";

/***/ }),
/* 205 */
/***/ (function(module, exports) {

module.exports = "<div class=\"media sw-all-alerts__item\" ng-class=\"{'sw-all-alerts__item--unacked': !item.acked, 'sw-all-alerts__item--acked': item.acked}\"> <div class=\"media-left sw-all-alerts__item-icon sw-all-alerts__item-type-{{::item.severityIcon}}-inverse\"> <xui-icon icon={{::item.severityIcon}}-inverse tool-tip={{::item.severityLabel}} tooltip-append-to-body=true tooltip-class=custom-tooltip></xui-icon> </div> <div class=media-body> <div class=sw-all-alerts__item-alert-row-left> <a class=sw-all-alerts__item-alert-name href={{::item.alertDetailsUrl}} title={{::item.AlertMessage}}> <span xui-ellipsis ellipsis-options=item.options is-ellipsis-enabled=false> {{::item.AlertName}} </span> </a> <span class=sw-all-alerts__item-triggered-by ng-if=::item.TriggeringObject title={{::item.TriggeringObject}} _t>By <a class=\"sw-all-alerts__item-triggered-by-link NoTip\" tooltip=processed href={{::item.TriggeringObjectDetailsUrl}}> {{::item.TriggeringObject}} </a> </span> <span class=sw-all-alerts__item-related-node ng-if=::item.RelatedNode _t> On <a class=sw-all-alerts__item-related-node-link href={{::item.RelatedNodeDetailsUrl}}> {{::item.RelatedNode}} </a> </span> <span class=sw-all-alerts__item-site-name ng-if=::item.federationEnabled _t>In <a class=sw-all-alerts__item-site-name-link href=/Server/{{::item.SiteID}}> {{::item.SiteName}} </a> </span> </div> <div class=sw-all-alerts__item-alert-row-right> <div class=sw-all-alerts__item-alert-row-right__left ng-class=\"{'opacity-hidden': item.isMenuExpanded}\"> <span class=sw-all-alerts__item-active-time title={{::item.getFullActiveDate()}} ng-bind-html=\"item.ActiveTime | xuiHighlight:item.searchValue\"> </span> <span class=sw-all-alerts__item-ack-by ng-if=item.acked> <xui-popover ng-if=::item.Notes xui-popover-content=::item.Notes xui-popover-placement=right xui-popover-trigger=\"mouseenter click\" xui-popover-title=Notes> <span class=sw-all-alerts__item-ack-notes> <xui-icon icon=note icon-size=small></xui-icon> </span> </xui-popover> {{::item.AcknowledgedByFullName}} </span> </div> <div class=sw-all-alerts__item-menu ng-mouseleave=\"item.isMenuExpanded=false\"> <div class=sw-all-alerts__item-menu-actions ng-class=\"{expanded: item.isMenuExpanded}\"> <div class=sw-all-alerts__item-menu-actions-inner> <div class=\"sw-all-alerts__item-menu-actions___item sw-all-alerts__item-menu-icon\" ng-if=item.isMuteVisible() ng-click=item.muteNode()> <xui-icon icon=mute icon-color=primary-blue tool-tip=\"_t(Mute node. No new alerts will be triggered for this entity until you resume alerts.)\" tooltip-append-to-body=true tooltip-class=custom-tooltip _ta></xui-icon> </div> <div class=\"sw-all-alerts__item-menu-actions___item sw-all-alerts__item-menu-icon\" ng-if=item.isUnmuteVisible() ng-click=item.unmuteNode()> <xui-icon icon=sound-3 icon-color=primary-blue tool-tip=\"_t(Unmute node)\" tooltip-append-to-body=true tooltip-class=custom-tooltip _ta></xui-icon> </div> <div class=\"sw-all-alerts__item-menu-actions___item sw-all-alerts__item-menu-icon\" ng-if=::!item.acked ng-click=item.openAckDialog()> <xui-icon icon=acknowledge tool-tip=\"_t(Acknowledge alert)\" tooltip-append-to-body=true tooltip-class=custom-tooltip _ta></xui-icon> </div> </div> </div> <div class=\"sw-all-alerts__item-menu-btn sw-all-alerts__item-menu-icon\" ng-init=\"item.isMenuExpanded = false\" ng-mouseenter=\"item.isMenuExpanded=true\"> <xui-icon icon=menu></xui-icon> </div> </div> </div> </div> </div> ";

/***/ }),
/* 206 */
/***/ (function(module, exports) {

module.exports = "<div> <xui-switch ng-model=alerts.settings.showfilters _t>Severity Summary</xui-switch> <xui-switch ng-model=alerts.settings.showsearchandsorterbar _t>Sort &amp; Search</xui-switch> <xui-switch ng-model=alerts.settings.showemptyimage _t>Empty image</xui-switch> <xui-divider></xui-divider> <xui-switch class=data-filtering-switch ng-model=alerts.settings.datafilteringenabled _t>Data filtering </xui-switch> <div class=data-filtering-list ng-if=alerts.settings.datafilteringenabled> <xui-dropdown class=\"alert-state xui-dropdown--justified\" caption=\"_t(Alert State)\" display-value=name items-source=alerts.dataAcknowledgeList on-changed=\"alerts.onChange(newValue, oldValue)\" selected-item=alerts.settings.acknowledgestate _ta></xui-dropdown> <div ng-if=alerts.swAlertsService.federationEnabled> <xui-dropdown class=\"alert-state xui-dropdown--justified\" caption=\"_t(Orion Site)\" display-value=name items-source=alerts.eoc.sitesList selected-item=alerts.settings.sitestate _ta></xui-dropdown> <div class=alert-sites ng-if=\"alerts.settings.sitestate.id==1\"> <xui-checkbox ng-model=site.enabled ng-repeat=\"site in alerts.eoc.sitesAvailable\">{{::site.name}} </xui-checkbox> </div> </div> </div> </div> ";

/***/ }),
/* 207 */
/***/ (function(module, exports) {

module.exports = "<div sw-auth sw-auth-feature-toggle=SwAllAlertsWidget> <sw-all-alerts items-source=alerts.items.rows filter-source=alerts.items.metadata.filters active-filters=alerts.settings.activefilters loaded=alerts.loaded class-css=alerts.style show-filters=alerts.settings.showfilters filter-change=alerts.filterSaveSettings() empty-data=alerts.noAlertsState show-empty=alerts.settings.showemptyimage template-url=[widgets]:/components/tabular/allAlerts/allAlerts-directive-template.html pagination-data=alerts.paginationData sorting-data=alerts.sortingData on-pagination-change=\"alerts.onPageChange(page, pageSize, total)\" on-sorting-change=\"alerts.onSortChange(oldValue, newValue)\" on-search=\"alerts.onSearch(item, cancellation)\" on-clear=alerts.cancelSearch() options=alerts.gridOptions track-by=AlertId row-padding=alerts.gridPaddingSize> </sw-all-alerts> </div> ";

/***/ }),
/* 208 */
/***/ (function(module, exports) {

module.exports = "<div ng-if=\"tile.swMainFilterDialogService.viewModel.isAdvancedMode === false\" xui-busy=tile.swMainFilterDialogService.viewModel.isBusy xui-busy-message=\"Loading data, please wait...\" _t> <div class=row> <div class=col-md-6 style=padding-top:10px> <xui-switch ng-model=tile.swMainFilterDialogService.viewModel.showAlerts>Show Active Alerts</xui-switch> </div> </div> <xui-divider></xui-divider> <div class=row> <div class=col-md-4> <div ng-if=\"tile.swMainFilterDialogService.viewModel.federationEnabled === true\"> <h2 _t>Site Filter</h2> <xui-content> <xui-checkbox-group ng-model=tile.swMainFilterDialogService.viewModel.selectedSites> <xui-checkbox ng-repeat=\"site in tile.swMainFilterDialogService.viewModel.availableSites\" value=site _t> {{site.name}} </xui-checkbox> </xui-checkbox-group> </xui-content> </div> </div> <div class=col-md-4> <h2 _t>Group Filter</h2> <xui-content> <xui-checkbox-group ng-model=tile.swMainFilterDialogService.viewModel.selectedGroups> <xui-checkbox ng-repeat=\"group in tile.swMainFilterDialogService.viewModel.availableGroups\" value=group _t> {{group.name}} on <a target=_blank title=\"SolarWinds Site\" href=/Server/{{group.siteID}}>{{group.siteName}}</a> </xui-checkbox> </xui-checkbox-group> </xui-content> </div> <div class=col-md-4> <h2 _t>Entity Filter</h2> <xui-content> <xui-checkbox-group ng-model=tile.swMainFilterDialogService.viewModel.selectedEntities> <xui-checkbox ng-repeat=\"entitiy in tile.swMainFilterDialogService.viewModel.availableEntities\" value=entitiy _t> {{entitiy.entityDisplayName}} </xui-checkbox> </xui-checkbox-group> </xui-content> </div> </div> </div> <div ng-if=\"tile.swMainFilterDialogService.viewModel.isAdvancedMode === true\"> <h5 _t>SWQL Status Summary Query</h5> <xui-content size=small> <span id=xui-demo-content-small-size>{{tile.swMainFilterDialogService.viewModel.swqlTileQuery}}</span> </xui-content> <h5 _t>SWQL Asset Explorer Query</h5> <xui-content size=small> <span id=xui-demo-content-small-size>{{tile.swMainFilterDialogService.viewModel.swqlAssetQuery}}</span> </xui-content> <h5 _t>SWQL Alert Query</h5> <xui-content size=small> <span id=xui-demo-content-small-size>{{tile.swMainFilterDialogService.viewModel.swqlAlertQuery}}</span> </xui-content> </div> <div xui-busy=tile.swMainFilterDialogService.viewModel.isBusy xui-busy-message=\"Loading data, please wait...\" _t> <xui-divider></xui-divider> <div class=row> <div class=\"col-md-1 pull-left\" style=padding-top:10px> <xui-icon icon=filter></xui-icon> </div> <div class=\"col-md-10 pull-left\"> <div class=xui-expandable-property__heading> FILTERING </div> <div _t> <xui-button ng-click=tile.openBasicFilterDialog() display-style=link icon=caret-right class=entity-container-add-button>Advanced Entity Filter</xui-button> </div> <div _t> <xui-button ng-click=tile.openAdvancedFilterDialog() display-style=link icon=caret-right class=entity-container-add-button>Advanced SWQL Filter</xui-button> </div> </div> </div> </div> <div class=row> <div ng-if=\"!tile.swMainFilterDialogService.viewModel.isBusy\n         && !tile.swMainFilterDialogService.viewModel.isDialogValid()\"> <div class=col-md-8> <xui-message type=error allow-dismiss=true _t> Please select at least one entity filter </xui-message> </div> </div> </div> ";

/***/ }),
/* 209 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-environment-summary-resource> <sw-tile entities-count={{vm.entitiesCount}} redirect-url={{vm.redirectUrl}} main-status=vm.mainStatus other-statuses=vm.otherStatuses active-alerts=vm.activeAlerts redirect-enabled=vm.redirectEnabled alerts-hidden=!vm.showAlerts> </sw-tile> </div> ";

/***/ }),
/* 210 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-widget-settings-section-button> <xui-button display-style=tertiary ng-click=vm.click()> <div class=sw-widget-settings-section-button__row> <div class=sw-widget-settings-section-button__icon> <xui-icon ng-if=::vm.icon icon={{::vm.icon}}></xui-icon> </div> <div class=sw-widget-settings-section-button__body> <div class=sw-widget-settings-section-button__title>{{::vm.title}} <span ng-transclude></span></div> <div class=sw-widget-settings-section-button__subtitle>{{::vm.subtitle}}</div> </div> <div class=sw-widget-settings-section-button__caret> <xui-icon icon=caret-right></xui-icon> </div> </div> </xui-button> </div> ";

/***/ }),
/* 211 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-widget-drawer__filter-category ng-class=\"::{'sw-widget-drawer__filter-category-all': item.IsAllResults}\" data-hit-count={{item.InstanceCount}}> <div class=\"sw-widget-drawer__filter-category-hit-count xui-highlighted\">{{item.InstanceCount}}</div> <div ng-if=\"item.CategoryType === 'Favorites'\" class=sw-widget-drawer__category-favorite> <xui-icon icon=star-full icon-size=small></xui-icon> </div> <div>{{::item.CategoryDisplayName || item.CategoryName}}</div> </div> ";

/***/ }),
/* 212 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-widget-drawer__item xui-draggable xui-draggable-object=item on-drag-start=vm.onDragStart(event) on-drag-end=vm.onDragEnd() adorner-drag-class=\"sw-widget-drawer__drag-adorner xui-ignore-highlight\"> <div class=\"sw-widget-drawer__item-content media\"> <div class=\"sw-widget-drawer__item-icon media-left\"> <xui-icon title={{::vm.getIconTooltip(item)}} class=media-object icon={{::vm.getIconName(item)}}></xui-icon> </div> <div class=media-body> <div class=sw-widget-drawer__item-favorite title=\"_t(Click to toggle favorite)\" ng-click=vm.toggleFavorite(item) _ta> <xui-icon icon=\"{{ item.IsFavorite ? 'star-full' : 'star-empty' }}\" icon-size=small is-dynamic=true></xui-icon> </div> <div class=sw-widget-drawer__item-title ng-bind-html=\"item.Title | xuiHighlight:item.searchValue\"></div> <xui-help-hint><div ng-bind-html=\"item.Tooltip | xuiHighlight:item.searchValue\"></div></xui-help-hint> </div> </div> <div class=sw-widget-drawer__item-handle> <xui-icon icon=drag css-class=sw-widget-drawer__item-handle-glyph></xui-icon> </div> </div> ";

/***/ }),
/* 213 */
/***/ (function(module, exports) {

module.exports = "<div class=\"xui sw-widget-overlay sw-widget-drag-element\"> <div class=sw-widget-overlay__header> <div class=sw-widget-overlay__title> <div class=sw-widget-overlay__handle> <xui-icon icon=drag css-class=sw-widget-overlay__handle-glyph></xui-icon> </div> <span title={{::$widgetOverlay.title}}>{{::$widgetOverlay.title}}</span> </div> <div class=sw-widget-overlay__actions> <div class=sw-widget-overlay__new-indicator _t>Just Added</div> <xui-menu class=sw-widget-overlay__menu tooltip=Remove menu-align=left display-style=tertiary icon=remove is-empty=true is-disabled=$widgetOverlay.disableRemove menu-append-to=$widgetOverlay.appendTo> <xui-menu-action action=$widgetOverlay.remove() _t>Remove</xui-menu-action> </xui-menu> </div> </div> </div> ";

/***/ }),
/* 214 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-widget-settings> <xui-textbox caption=_t(Title) type=text ng-model=vmForm.settings.edittitle is-read-only={{vmForm.options.titleReadOnly}} _ta> </xui-textbox> <xui-textbox caption=_t(Subtitle) type=text ng-model=vmForm.settings.editsubtitle _ta> </xui-textbox> <div ng-transclude></div> </div> ";

/***/ }),
/* 215 */
/***/ (function(module, exports) {

module.exports = "<div class=grid-row> <div class=col-checkBox> <xui-checkbox ng-model=item.selected class=id-acl-item-checkbox-parent is-disabled=ctrlAccessList.isCheckboxDisabled(item) ng-change=ctrlAccessList.onCheckboxClick(item)></xui-checkbox> </div> <div class=col-icon> <xui-icon class=id-acl-item-icon-parent icon=config-file></xui-icon> </div> <div class=col-name> <a role=button href={{ctrlAccessList.getAclDetailsUrl(item)}} _t><span class=id-acl-item-name-parent ng-bind-html=::ctrlAccessList.highlightSearch(item.name)></span></a> <div class=\"text-muted id-acl-item-datetime-parent\">{{::item.modificationTimeFormatted}}</div> </div> <div class=\"col-interface id-acl-item-interface-parent\"> <div style=display:inline ng-if=\"::item.interfaces.length <= 2\" ng-repeat=\"aclInterface in ::item.interfaces\"> <span ng-if=ctrlAccessList.isInterfaceAvailable(aclInterface)> <a href={{ctrlAccessList.getInterfaceUrl(aclInterface)}}>{{aclInterface}}</a><span ng-if=\"$last == false\">,&nbsp;</span> </span> <span ng-if=!ctrlAccessList.isInterfaceAvailable(aclInterface)> <span>{{aclInterface}}</span><span ng-if=\"$last == false\">,&nbsp;</span> </span> </div> <div style=display:inline ng-if=\"::item.interfaces.length > 2\"> <span ng-if=!ctrlAccessList.areInterfacesEnabled()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </span> <span ng-if=ctrlAccessList.areInterfacesEnabled()> <a role=button ng-click=ctrlAccessList.navigateToInterfacesTab()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </a> </span> </div> </div> <acl-srd-icon acl=item></acl-srd-icon> </div> ";

/***/ }),
/* 216 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-acl-list__item_level1> <div ng-controller=\"accessListDirectiveController as ctrlAccessList\"> <div ng-if=\"item.innerItemsSource.length === 0\" class=expand-icon-offset> <div ng-include=\"'[widgets]:/orionModules/ncm/components/accessList/accessList-item-grid-row.html'\"></div> </div> <xui-expander ng-if=\"item.innerItemsSource.length !== 0\" class=id-acl-expander is-disabled=\"item.innerItemsSource.length === 0\" on-status-changed=\"ctrlAccessList.onStatusChangedOnExpander(isOpen, item)\"> <xui-expander-heading> <div> <div ng-include=\"'[widgets]:/orionModules/ncm/components/accessList/accessList-item-grid-row.html'\"></div> </div> </xui-expander-heading> <div class=row> <div class=col-md-12> <xui-grid items-source=item.innerItemsSource class=\"id-acl-grid-child acl-grid-child\" hide-toolbar=true pagination-data=ctrlAccessList.innerGridPagination options=ctrlAccessList.innerGridOptions smart-mode=true template-url=[widgets]:/orionModules/ncm/components/accessList/accessList-item-template-level2.html controller=ctrlAccessList> </xui-grid> </div> </div> </xui-expander> </div> </div> ";

/***/ }),
/* 217 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-acl-list__item_level2> <div ng-controller=\"accessListDirectiveController as ctrlAccessList\"> <div class=grid-row> <div class=col-checkBox> <xui-checkbox ng-model=item.selected class=id-acl-item-checkbox-child is-disabled=ctrlAccessList.isCheckboxDisabled(item) ng-change=ctrlAccessList.onCheckboxClick(item)></xui-checkbox> </div> <div class=col-icon> <xui-icon class=id-acl-item-icon-child icon=config-file></xui-icon> </div> <div class=col-name> <a role=button href={{ctrlAccessList.getAclDetailsUrl(item)}} _t><span class=id-acl-item-name-child ng-bind-html=::ctrlAccessList.highlightSearch(item.name)></span></a> <div class=\"text-muted id-acl-item-datetime-child\">{{::item.modificationTimeFormatted}}</div> </div> <div class=\"col-interface id-acl-item-interface-child\"> <div style=display:inline ng-if=\"::item.interfaces.length <= 2\" ng-repeat=\"aclInterface in ::item.interfaces\"> <span ng-if=ctrlAccessList.isInterfaceAvailable(aclInterface)> <a href={{ctrlAccessList.getInterfaceUrl(aclInterface)}}>{{aclInterface}}</a><span ng-if=\"$last == false\">,&nbsp;</span> </span> <span ng-if=!ctrlAccessList.isInterfaceAvailable(aclInterface)> <span>{{aclInterface}}</span><span ng-if=\"$last == false\">,&nbsp;</span> </span> </div> <div style=display:inline ng-if=\"::item.interfaces.length > 2\"> <span ng-if=!ctrlAccessList.areInterfacesEnabled()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </span> <span ng-if=ctrlAccessList.areInterfacesEnabled()> <a role=button ng-click=ctrlAccessList.navigateToInterfacesTab()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </a> </span> </div> </div> <acl-srd-icon acl=item></acl-srd-icon> </div> </div> </div> ";

/***/ }),
/* 218 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=\"sw-perfstack-select-dialog__title xui-text-h3\"> <span class=text-muted _t>Edit</span> <span class=text-muted>/</span> <span _t>PerfStack Project</span> </div> <sw-perfstack-project-selector selection=vm.dialogOptions.viewModel.selection></sw-perfstack-project-selector> </xui-dialog> ";

/***/ }),
/* 219 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(220);
var index_2 = __webpack_require__(294);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
};


/***/ }),
/* 220 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(221);
var components_1 = __webpack_require__(234);
var filters_1 = __webpack_require__(267);
var services_1 = __webpack_require__(282);
__webpack_require__(293);
exports.default = function (module) {
    widgets_1.default(module);
    components_1.default(module);
    filters_1.default(module);
    services_1.default(module);
    widgets_1.default(module);
};


/***/ }),
/* 221 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(222);
var index_2 = __webpack_require__(224);
var _1 = __webpack_require__(226);
var index_3 = __webpack_require__(228);
var _2 = __webpack_require__(230);
var _3 = __webpack_require__(232);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    _1.default(module);
    index_3.default(module);
    _2.default(module);
    _3.default(module);
};


/***/ }),
/* 222 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var siteToSiteTunnels_controller_1 = __webpack_require__(223);
exports.default = function (module) {
    module.controller("asaSiteToSiteTunnelsWidget", siteToSiteTunnels_controller_1.SiteToSiteTunnelsController);
};


/***/ }),
/* 223 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
var SiteToSiteTunnelsController = /** @class */ (function () {
    /** @ngInject */
    SiteToSiteTunnelsController.$inject = ["$log", "netObjectsService"];
    function SiteToSiteTunnelsController($log, netObjectsService) {
        var _this = this;
        this.$log = $log;
        this.netObjectsService = netObjectsService;
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.onLoad = function (config) {
            _this.config = config;
            _this.netObjectsService.setNetObject(config.settings.netobject);
        };
        this.onUnload = function () {
            //Nothing to unload
        };
        this.onSettingsChanged = function (settings) {
            _this.onChange();
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            //Nothing to handle
        };
    }
    SiteToSiteTunnelsController = __decorate([
        widget_decorator_1.Widget({
            selector: "vpnSiteToSiteTunnelsWidget",
            template: __webpack_require__(75),
            controllerAs: "siteToSiteTunnelsWdgCntl"
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], SiteToSiteTunnelsController);
    return SiteToSiteTunnelsController;
}());
exports.SiteToSiteTunnelsController = SiteToSiteTunnelsController;


/***/ }),
/* 224 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var remoteAccessVpnTunnelController_1 = __webpack_require__(225);
exports.default = function (module) {
    module.controller("vpnRemoteAccessTunnelsWidget", remoteAccessVpnTunnelController_1.RemoteAccessVpnTunnelController);
};


/***/ }),
/* 225 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "vpnRemoteAccessTunnelsWidget";
var RemoteAccessVpnTunnelController = /** @class */ (function () {
    /** @ngInject */
    RemoteAccessVpnTunnelController.$inject = ["$log", "netObjectsService"];
    function RemoteAccessVpnTunnelController($log, netObjectsService) {
        var _this = this;
        this.$log = $log;
        this.netObjectsService = netObjectsService;
        //public settings: IRemoteAccessVpnTunnelWidgetSettings;
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.onLoad = function (config) {
            _this.config = config;
            _this.netObjectsService.setNetObject(config.settings.netobject);
            //this.settings = config.settings;
        };
        //public onUnload = () => {
        //};
        this.onSettingsChanged = function (settings) {
            //this.settings = settings;
            _this.onChange();
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            //this.config.isBusy(showBusy, this._t("Updating..."));
            //this.config.isBusy(false);
        };
    }
    RemoteAccessVpnTunnelController = __decorate([
        widget_decorator_1.Widget({
            selector: "vpnRemoteAccessTunnelsWidget",
            template: __webpack_require__(74),
            controllerAs: "widgetController",
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], RemoteAccessVpnTunnelController);
    return RemoteAccessVpnTunnelController;
}());
exports.RemoteAccessVpnTunnelController = RemoteAccessVpnTunnelController;


/***/ }),
/* 226 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var platformDetailsWidgetController_1 = __webpack_require__(227);
exports.default = function (module) {
    module.controller("asaPlatformDetailsWidget", platformDetailsWidgetController_1.PlatformDetailsWidgetController);
};


/***/ }),
/* 227 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "platformDetailsWidget";
var PlatformDetailsWidgetController = /** @class */ (function () {
    /** @ngInject */
    PlatformDetailsWidgetController.$inject = ["$log", "netObjectsService"];
    function PlatformDetailsWidgetController($log, netObjectsService) {
        var _this = this;
        this.$log = $log;
        this.netObjectsService = netObjectsService;
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.onLoad = function (config) {
            _this.config = config;
            _this.netObjectsService.setNetObject(config.settings.netobject);
        };
        this.onSettingsChanged = function (settings) {
            _this.onChange();
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            //this.config.isBusy(showBusy, this._t("Updating..."));
            //this.config.isBusy(false);
        };
    }
    PlatformDetailsWidgetController = __decorate([
        widget_decorator_1.Widget({
            selector: "asaPlatformDetailsWidget",
            template: __webpack_require__(73),
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], PlatformDetailsWidgetController);
    return PlatformDetailsWidgetController;
}());
exports.PlatformDetailsWidgetController = PlatformDetailsWidgetController;


/***/ }),
/* 228 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asaInterfacesWidget_controller_1 = __webpack_require__(229);
exports.default = function (module) {
    module.controller("asaInterfacesWidget", asaInterfacesWidget_controller_1.AsaInterfacesWidgetController);
};


/***/ }),
/* 229 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
var AsaInterfacesWidgetController = /** @class */ (function () {
    /** @ngInject */
    AsaInterfacesWidgetController.$inject = ["$log", "netObjectsService"];
    function AsaInterfacesWidgetController($log, netObjectsService) {
        var _this = this;
        this.$log = $log;
        this.netObjectsService = netObjectsService;
        this.$onInit = function () {
            _this.nodeId = _this.getNodeIdFromUri();
        };
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.onLoad = function (config) {
            _this.config = config;
            _this.netObjectsService.setNetObject(config.settings.netobject);
        };
        // This is a temporary solution, NetObject should be provided by platform in config.settings.netObject (UIF-3925)
        this.getNodeIdFromUri = function () {
            var queryVariable = _this.getUrlParameter("NetObject");
            return queryVariable ? parseInt(queryVariable.split(":")[1], 10) : 0;
        };
        this.getUrlParameter = function (name) {
            name = name.replace(/[\[]/, "'\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
            var results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };
        this.onUnload = function () {
            //Nothing to unload
        };
        this.onSettingsChanged = function (settings) {
            _this.onChange();
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            //Nothing to handle
        };
    }
    AsaInterfacesWidgetController = __decorate([
        widget_decorator_1.Widget({
            selector: "asaInterfacesWidget",
            template: __webpack_require__(70),
            controllerAs: "asaInterfacesWidgetController"
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], AsaInterfacesWidgetController);
    return AsaInterfacesWidgetController;
}());
exports.AsaInterfacesWidgetController = AsaInterfacesWidgetController;


/***/ }),
/* 230 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var highAvailabilityChartWidgetController_1 = __webpack_require__(231);
exports.default = function (module) {
    module.controller("asaHighAvailabilityChartWidget", highAvailabilityChartWidgetController_1.HighAvailabilityChartWidgetController);
};


/***/ }),
/* 231 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "highAvailabilityChartWidget";
var HighAvailabilityChartWidgetController = /** @class */ (function () {
    /** @ngInject */
    HighAvailabilityChartWidgetController.$inject = ["$log", "netObjectsService"];
    function HighAvailabilityChartWidgetController($log, netObjectsService) {
        var _this = this;
        this.$log = $log;
        this.netObjectsService = netObjectsService;
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.onLoad = function (config) {
            _this.config = config;
            _this.netObjectsService.setNetObject(config.settings.netobject);
        };
        this.onSettingsChanged = function (settings) {
            _this.onChange();
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            //this.config.isBusy(showBusy, this._t("Updating..."));
            //this.config.isBusy(false);
        };
    }
    HighAvailabilityChartWidgetController = __decorate([
        widget_decorator_1.Widget({
            selector: "asaHighAvailabilityChartWidget",
            template: __webpack_require__(72),
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], HighAvailabilityChartWidgetController);
    return HighAvailabilityChartWidgetController;
}());
exports.HighAvailabilityChartWidgetController = HighAvailabilityChartWidgetController;


/***/ }),
/* 232 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var contextListWidgetController_1 = __webpack_require__(233);
exports.default = function (module) {
    module.controller("asaContextListWidget", contextListWidgetController_1.ContextListWidgetController);
};


/***/ }),
/* 233 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "contextListWidget";
var ContextListWidgetController = /** @class */ (function () {
    /** @ngInject */
    ContextListWidgetController.$inject = ["$log", "netObjectsService"];
    function ContextListWidgetController($log, netObjectsService) {
        var _this = this;
        this.$log = $log;
        this.netObjectsService = netObjectsService;
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.onLoad = function (config) {
            _this.config = config;
            _this.netObjectsService.setNetObject(config.settings.netobject);
        };
        this.onSettingsChanged = function (settings) {
            _this.onChange();
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            //this.config.isBusy(showBusy, this._t("Updating..."));
            //this.config.isBusy(false);
        };
    }
    ContextListWidgetController = __decorate([
        widget_decorator_1.Widget({
            selector: "asaContextListWidget",
            template: __webpack_require__(71),
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], ContextListWidgetController);
    return ContextListWidgetController;
}());
exports.ContextListWidgetController = ContextListWidgetController;


/***/ }),
/* 234 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var sessionList_1 = __webpack_require__(235);
var platformDetails_1 = __webpack_require__(238);
var index_1 = __webpack_require__(242);
var highAvailabilityChart_1 = __webpack_require__(247);
var serverSideFilteredList_1 = __webpack_require__(251);
var siteToSiteFilteredTunnels_1 = __webpack_require__(254);
var cliCredentialsNotification_1 = __webpack_require__(257);
var nodePropertiesLink_1 = __webpack_require__(260);
var contextList_1 = __webpack_require__(263);
exports.default = function (module) {
    sessionList_1.default(module);
    platformDetails_1.default(module);
    index_1.default(module);
    highAvailabilityChart_1.default(module);
    serverSideFilteredList_1.default(module);
    siteToSiteFilteredTunnels_1.default(module);
    cliCredentialsNotification_1.default(module);
    nodePropertiesLink_1.default(module);
    contextList_1.default(module);
};


/***/ }),
/* 235 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var sessionList_1 = __webpack_require__(236);
__webpack_require__(79);
__webpack_require__(67);
exports.default = function (module) {
    module.component("asaSessionList", sessionList_1.default);
};


/***/ }),
/* 236 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(79);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sessionListController_1 = __webpack_require__(237);
var SessionList = /** @class */ (function () {
    /** @ngInject */
    SessionList.$inject = ["$log"];
    function SessionList($log) {
        this.$log = $log;
        this.restrict = "E";
        this.transclude = false;
        this.template = __webpack_require__(66);
        this.scope = {};
        this.controller = sessionListController_1.default;
        this.controllerAs = "sessionListController";
        this.bindToController = {};
    }
    return SessionList;
}());
exports.default = SessionList;


/***/ }),
/* 237 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
var SessionListController = /** @class */ (function () {
    /** @ngInject */
    SessionListController.$inject = ["$log", "$q", "remoteTunnelsService", "netObjectsService"];
    function SessionListController($log, $q, remoteTunnelsService, netObjectsService) {
        this.$log = $log;
        this.$q = $q;
        this.remoteTunnelsService = remoteTunnelsService;
        this.netObjectsService = netObjectsService;
        // For use in templates
        this.enum = {
            state: asa.TunnelState,
            type: asa.RemoteAccessTunnelType
        };
        this.options = {
            smartMode: false,
            showSelector: false,
            hideSearch: false,
            triggerSearchOnChange: true,
            hidePagination: false,
            showAddRemoveFilterProperty: false,
            templateUrl: "[widgets]:/orionModules/asa/components/sessionList/sessionListItem.html"
        };
    }
    SessionListController.prototype.getSessions = function (filters, filterModels, pagination, sorting, search) {
        var _this = this;
        return this.netObjectsService.getNetObject()
            .then(function (netObject) {
            var filterParameters = {
                filterValues: filters,
                filterModels: filterModels,
                pagination: pagination,
                sorting: sorting,
                searching: search
            };
            return _this.remoteTunnelsService.getVpnTunnelSessions(netObject.nodeId, filterParameters);
        }, function (failure) {
            _this.$log.error("netObjectsService.getNetObject failed", failure);
            return null;
        });
    };
    return SessionListController;
}());
exports.default = SessionListController;


/***/ }),
/* 238 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var platformDetails_1 = __webpack_require__(239);
__webpack_require__(80);
__webpack_require__(64);
exports.default = function (module) {
    module.component("asaPlatformDetails", platformDetails_1.default);
};


/***/ }),
/* 239 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(80);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platformDetailsController_1 = __webpack_require__(240);
var PlatformDetails = /** @class */ (function () {
    /** @ngInject */
    PlatformDetails.$inject = ["$log"];
    function PlatformDetails($log) {
        var _this = this;
        this.$log = $log;
        this.restrict = "E";
        this.transclude = false;
        this.template = __webpack_require__(63);
        this.scope = {};
        this.controller = platformDetailsController_1.default;
        this.controllerAs = "platform";
        this.bindToController = {};
        this.link = function (scope, instanceElement, instanceAttributes, controller) {
            try {
                controller.initialize();
            }
            catch (e) {
                _this.$log.error(e);
            }
        };
    }
    return PlatformDetails;
}());
exports.default = PlatformDetails;


/***/ }),
/* 240 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asaPlatformDetails_1 = __webpack_require__(241);
var PlatformDetailsController = /** @class */ (function () {
    /** @ngInject */
    PlatformDetailsController.$inject = ["$log", "$q", "platformDetailsService", "netObjectsService"];
    function PlatformDetailsController($log, $q, platformDetailsService, netObjectsService) {
        var _this = this;
        this.$log = $log;
        this.$q = $q;
        this.platformDetailsService = platformDetailsService;
        this.netObjectsService = netObjectsService;
        this.$onInit = function () {
            _this.isBusy = true;
            _this.loadFailed = false;
        };
    }
    PlatformDetailsController.prototype.initialize = function () {
        var _this = this;
        try {
            this.details = new asaPlatformDetails_1.default();
            this.netObjectsService.getNetObject().then(function (netObject) {
                _this.netObject = netObject;
                _this.initializeDetails();
            });
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    PlatformDetailsController.prototype.initializeDetails = function () {
        var _this = this;
        try {
            this.platformDetailsService.getPlatformDetails(this.netObject.nodeId).then(function (success) {
                _this.details = new asaPlatformDetails_1.default(success);
                _this.isBusy = false;
            }, function (failure) {
                _this.$log.error(failure);
                _this.details = new asaPlatformDetails_1.default(null, true);
                // Awaiting story for failure handling - UIF-4201
                _this.loadFailed = true;
                _this.isBusy = false;
            });
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    return PlatformDetailsController;
}());
exports.default = PlatformDetailsController;


/***/ }),
/* 241 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
exports.emptyStringData = "";
var AsaPlatformDetails = /** @class */ (function () {
    function AsaPlatformDetails(dto, populateFields) {
        if (populateFields === void 0) { populateFields = false; }
        this.nodeId = 0;
        this.name = exports.emptyStringData;
        this.status = asa.NodeStatus.Unknown;
        this.haState = exports.emptyStringData;
        this.iosVersion = exports.emptyStringData;
        this.caption = exports.emptyStringData;
        this.description = exports.emptyStringData;
        this.machineType = exports.emptyStringData;
        this.ipAddress = exports.emptyStringData;
        this.asaContext = exports.emptyStringData;
        this.mode = exports.emptyStringData;
        this.otherDetails = [];
        $.extend(this, dto);
        if (this.asaContext.length > 0 && this.machineType.length > 0) {
            this.contextSeparator = ",&nbsp;";
        }
        this.vpnDetails = this.getVpnDetails();
        this.orionDetails = this.getOrionDetails();
        this.hasOtherDetails = this.orionDetails.length > 0 || this.vpnDetails.length > 0;
    }
    AsaPlatformDetails.prototype.getVpnDetails = function () {
        return _.filter(this.otherDetails, function (d) { return !d.isOrionDetail; });
    };
    AsaPlatformDetails.prototype.getOrionDetails = function () {
        return _.filter(this.otherDetails, function (d) { return d.isOrionDetail; });
    };
    return AsaPlatformDetails;
}());
exports.default = AsaPlatformDetails;


/***/ }),
/* 242 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asaInterfaces_directive_1 = __webpack_require__(243);
exports.default = function (module) {
    module.component("asaInterfaces", asaInterfaces_directive_1.default);
};


/***/ }),
/* 243 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var asaInterfaces_controller_1 = __webpack_require__(244);
var AsaInterfaces = /** @class */ (function () {
    function AsaInterfaces() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(59);
        this.scope = {};
        this.controller = asaInterfaces_controller_1.default;
        this.controllerAs = "asaInterfacesController";
        this.bindToController = {};
    }
    return AsaInterfaces;
}());
exports.default = AsaInterfaces;


/***/ }),
/* 244 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var asaInterface_1 = __webpack_require__(245);
var favoriteEntity_controller_1 = __webpack_require__(81);
var AsaInterfacesController = /** @class */ (function (_super) {
    __extends(AsaInterfacesController, _super);
    /** @ngInject */
    AsaInterfacesController.$inject = ["$log", "$q", "asaInterfacesService", "netObjectsService", "getTextService", "xuiToastService"];
    function AsaInterfacesController($log, $q, asaInterfacesService, netObjectsService, getTextService, xuiToastService) {
        var _this = _super.call(this, $log, getTextService, xuiToastService) || this;
        _this.$log = $log;
        _this.$q = $q;
        _this.asaInterfacesService = asaInterfacesService;
        _this.netObjectsService = netObjectsService;
        _this.getTextService = getTextService;
        _this.xuiToastService = xuiToastService;
        _this.$onInit = function () {
            _this.sortableColumns = [
                { id: "isFavorite", label: _this._t("Favorite") },
                { id: "status", label: _this._t("Status") },
                { id: "totalErrorRate", label: _this._t("Errors In Last Hour") },
                { id: "securityLevel", label: _this._t("Security Level") },
                { id: "inBitsPerSecond", label: _this._t("Bandwidth, In") },
                { id: "outBitsPerSecond", label: _this._t("Bandwidth, Out") },
                { id: "standbyIp", label: _this._t("Standby Ip") }
            ];
            _this.sorting = {
                sortableColumns: _this.sortableColumns,
                sortBy: _this.sortableColumns[1],
                direction: "desc"
            };
            _this.items = [];
            _this.paging = {
                page: 0,
                pageSize: 1000
            };
        };
        _this.init = function () {
            _this.$log.debug("Initializing AsaInterfacesController");
            _this.getInterfaces();
        };
        _this.options = {
            hidePagination: true,
            hideSearch: true
        };
        _this.getInterfaces = function () {
            _this.$log.debug("Getting list of interfaces");
            return _this.netObjectsService.getNetObject()
                .then(function (netObject) {
                return _this.asaInterfacesService.getInterfaces(netObject.nodeId)
                    .then(
                /* success */
                function (result) {
                    _this.$log.debug("Received list of interfaces");
                    var interfaces = new Array();
                    result.forEach(function (dto) { return interfaces
                        .push(new asaInterface_1.default(dto)); });
                    _this.items = interfaces;
                }, 
                /* error */
                function (result) {
                    _this.$log.error("Getting list of interfaces failed", result);
                });
            }, function (failure) {
                _this.$log.error("netObjectsService.getNetObject failed", failure);
                return null;
            });
        };
        _this.$log.debug("Creating AsaInterfacesController");
        _this._t = getTextService;
        return _this;
    }
    AsaInterfacesController.prototype.switchIsFavoriteValue = function (iface) {
        var switchIsFavoriteResult = this.asaInterfacesService.assignIsFavoriteValue(iface.interfaceID, !iface.isFavorite);
        _super.prototype.processSwitchIsFavoriteResult.call(this, switchIsFavoriteResult, iface);
    };
    ;
    return AsaInterfacesController;
}(favoriteEntity_controller_1.FavoriteEntityController));
exports.default = AsaInterfacesController;


/***/ }),
/* 245 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AsaInterface = /** @class */ (function () {
    function AsaInterface(interfaceDto) {
        jQuery.extend(this, interfaceDto);
        this.totalErrorRate = this.inErrorsInLastHour + this.outErrorsInLastHour;
    }
    return AsaInterface;
}());
exports.default = AsaInterface;


/***/ }),
/* 246 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Errors when switching isFavorite value.
 *
 * @enum
 */
var SwitchIsFavoriteErrors;
(function (SwitchIsFavoriteErrors) {
    SwitchIsFavoriteErrors[SwitchIsFavoriteErrors["ItemNotFound"] = 1] = "ItemNotFound";
    SwitchIsFavoriteErrors[SwitchIsFavoriteErrors["MaxFavoritesPerNodeExceeded"] = 2] = "MaxFavoritesPerNodeExceeded";
    SwitchIsFavoriteErrors[SwitchIsFavoriteErrors["AccessToVerbDenied"] = 3] = "AccessToVerbDenied";
})(SwitchIsFavoriteErrors = exports.SwitchIsFavoriteErrors || (exports.SwitchIsFavoriteErrors = {}));


/***/ }),
/* 247 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var highAvailabilityChart_1 = __webpack_require__(248);
__webpack_require__(82);
__webpack_require__(12);
exports.default = function (module) {
    module.component("asaHighAvailabilityChart", highAvailabilityChart_1.default);
};


/***/ }),
/* 248 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(82);

"use strict";
/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var highAvailabilityChartController_1 = __webpack_require__(249);
var HighAvailabilityChart = /** @class */ (function () {
    /** @ngInject */
    HighAvailabilityChart.$inject = ["$log"];
    function HighAvailabilityChart($log) {
        var _this = this;
        this.$log = $log;
        this.restrict = "E";
        this.transclude = false;
        this.template = __webpack_require__(12);
        this.scope = {};
        this.controller = highAvailabilityChartController_1.default;
        this.controllerAs = "chart";
        this.bindToController = {};
        this.link = function (scope, instanceElement, instanceAttributes, controller) {
            try {
                controller.initialize(instanceElement);
            }
            catch (e) {
                _this.$log.error(e);
            }
        };
    }
    return HighAvailabilityChart;
}());
exports.default = HighAvailabilityChart;


/***/ }),
/* 249 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SolarWinds_Orion_ASA_WebApi_1 = __webpack_require__(1);
var highAvailabilityLinkStateInfos_1 = __webpack_require__(250);
var ILinkStateInfo_1 = __webpack_require__(83);
var HighAvailabilityChartController = /** @class */ (function () {
    /** @ngInject */
    HighAvailabilityChartController.$inject = ["$log", "$q", "getTextService", "highAvailabilityService", "netObjectsService", "widgetHelperService"];
    function HighAvailabilityChartController($log, $q, getTextService, highAvailabilityService, netObjectsService, widgetHelperService) {
        var _this = this;
        this.$log = $log;
        this.$q = $q;
        this.getTextService = getTextService;
        this.highAvailabilityService = highAvailabilityService;
        this.netObjectsService = netObjectsService;
        this.widgetHelperService = widgetHelperService;
        // For use in templates
        this.enum = {
            mode: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityMode,
            role: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityRole,
            haState: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityState,
            haStateType: ILinkStateInfo_1.HaStateType
        };
        this.$onInit = function () {
            _this.isBusy = true;
        };
        this._t = getTextService;
    }
    HighAvailabilityChartController.prototype.initialize = function (instanceElement) {
        var _this = this;
        if (instanceElement === void 0) { instanceElement = null; }
        try {
            this.instanceElement = instanceElement;
            this.unknownText = this._t("Unknown");
            this.netObjectsService.getNetObject().then(function (netObject) {
                _this.netObject = netObject;
                _this.initializeDetails();
            });
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    HighAvailabilityChartController.prototype.initializeDetails = function () {
        var _this = this;
        try {
            this.highAvailabilityService.getHighAvailabilityInfo(this.netObject.nodeId).then(function (success) {
                try {
                    if (success == null && _this.widgetHelperService.hideParentWidget(_this.instanceElement)) {
                        return;
                    }
                    _this.setHaInfo(success || _this.getDefaultHaInfo());
                    _this.isBusy = false;
                }
                catch (successError) {
                    _this.$log.error(successError);
                }
            }, function (failure) {
                try {
                    _this.$log.error(failure);
                    // Awaiting story for failure handling - UIF-4201
                    _this.setHaInfo(_this.getDefaultHaInfo());
                    _this.loadFailed = true;
                    _this.isBusy = false;
                }
                catch (failureError) {
                    _this.$log.error(failureError);
                }
            });
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    HighAvailabilityChartController.prototype.setHaInfo = function (haInfo) {
        try {
            this.haInfo = haInfo;
            this.haType = this.localizeHaType(SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityType[this.haInfo.type]);
            this.currentModeLocalized = this.localizeMode(this.haInfo.current.mode);
            this.currentRoleLocalized = this.localizeRole(this.haInfo.current.role);
            this.peerModeLocalized = this.localizeMode(this.haInfo.peer.mode);
            this.peerRoleLocalized = this.localizeRole(this.haInfo.peer.role);
            this.lastFailoverText = (this.haInfo.lastFailover
                ? moment(this.haInfo.lastFailover).fromNow()
                : this.unknownText);
            this.populateLinkStateInfos();
            var thisNode = (this.haInfo.peer
                .nodeId ===
                this.netObject.nodeId
                ? this.haInfo.peer
                : this.haInfo.current);
            if (thisNode.name !== this.unknownText) {
                thisNode.name = this._t("this node");
            }
            if (this.haInfo.peer.nodeId === null) {
                this.haInfo.peer.url = "/Orion/Nodes/Add/Default.aspx?&restart=false&IPAddress=" + (this.haInfo.peer
                    .name ||
                    "");
                this.haInfo.peer.name = this._t("Add node as monitored");
            }
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    HighAvailabilityChartController.prototype.populateLinkStateInfos = function () {
        var _this = this;
        try {
            this.linkStateInfos = [];
            // Config Sync State
            this.linkStateInfos.push(highAvailabilityLinkStateInfos_1.highAvailabilityLinkStateInfos.filter(function (stateInfo, i) {
                return stateInfo.haStateType === ILinkStateInfo_1.HaStateType.Config &&
                    stateInfo.haState === _this.haInfo.linkInfo.configState;
            }).pop());
            // Connection State
            this.linkStateInfos.push(highAvailabilityLinkStateInfos_1.highAvailabilityLinkStateInfos.filter(function (stateInfo, i) {
                return stateInfo.haStateType === ILinkStateInfo_1.HaStateType.Connection &&
                    stateInfo.haState === _this.haInfo.linkInfo.syncState;
            }).pop());
            // Connection State
            this.linkStateInfos.push(highAvailabilityLinkStateInfos_1.highAvailabilityLinkStateInfos.filter(function (stateInfo, i) {
                return stateInfo.haStateType === ILinkStateInfo_1.HaStateType.Standby &&
                    stateInfo.haState === _this.haInfo.linkInfo.standByState;
            }).pop());
            this.linkStateInfos = _.sortBy(this.linkStateInfos, function (linkStateInfo) { return linkStateInfo.rank; });
            this.linkStateInfos.forEach(function (value) {
                value.title = _this.localizeLinkStateInfo(value.title);
                value.description = _this.localizeLinkStateInfo(value.description);
            });
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    HighAvailabilityChartController.prototype.getDefaultHaInfo = function () {
        return {
            name: this.unknownText,
            type: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityType.Unknown,
            current: {
                status: SolarWinds_Orion_ASA_WebApi_1.NodeStatus.Unknown,
                role: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityRole.Unknown,
                mode: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityMode.Unknown,
                name: this.unknownText,
                url: "#"
            },
            peer: {
                status: SolarWinds_Orion_ASA_WebApi_1.NodeStatus.Unknown,
                role: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityRole.Unknown,
                mode: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityMode.Unknown,
                name: this.unknownText,
                url: "#"
            },
            linkInfo: {
                status: SolarWinds_Orion_ASA_WebApi_1.NodeStatus.Unknown,
                standByState: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityState.Unknown,
                syncState: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityState.Unknown,
                configState: SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityState.Unknown
            },
            lastFailover: null
        };
    };
    HighAvailabilityChartController.prototype.localizeLinkStateInfo = function (text) {
        switch (text) {
            case "Standby state unknown":
                return this._t("Standby state unknown");
            case "Config sync unknown":
                return this._t("Config sync unknown");
            case "Connection state sync unknown":
                return this._t("Connection state sync unknown");
            case "Standby not ready":
                return this._t("Standby not ready");
            case "The standby is not ready to take over in the event of a failure!":
                return this._t("The standby is not ready to take over in the event of a failure!");
            case "Config not synced":
                return this._t("Config not synced");
            case "Config is not synchronized. Firewall behavior may change if a failover occurs.":
                return this._t("Config is not synchronized. Firewall behavior may change if a failover occurs.");
            case "Connection state not synced":
                return this._t("Connection state not synced");
            case "Connection state information is not in sync. If a failover occurs, \
            ongoing connections through this firewall will have to be reestablished.":
                return this._t("Connection state information is not in sync. If a failover occurs, \
                ongoing connections through this firewall will have to be reestablished.");
            case "Standby ready":
                return this._t("Standby ready");
            case "The standby is ready to take over in the event of a failure.":
                return this._t("The standby is ready to take over in the event of a failure.");
            case "Config synced":
                return this._t("Config synced");
            case "Config is in sync.":
                return this._t("Config is in sync.");
            case "Connection state synced":
                return this._t("Connection state synced");
            case "Connection state information is in sync.":
                return this._t("Connection state information is in sync.");
            default:
                return text;
        }
    };
    HighAvailabilityChartController.prototype.localizeHaType = function (text) {
        switch (text) {
            case "Unknown":
                return this._t("Unknown");
            case "None":
                return this._t("None");
            case "ActiveActive":
                return this._t("Active/Active");
            case "ActiveStandby":
                return this._t("Active/Standby");
            default:
                return text;
        }
    };
    HighAvailabilityChartController.prototype.localizeMode = function (mode) {
        switch (mode) {
            case SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityMode.Unknown:
                return this._t("Unknown");
            case SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityMode.NotConfigured:
                return this._t("Not Configured");
            case SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityMode.Active:
                return this._t("Active");
            case SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityMode.Standby:
                return this._t("Standby");
            default:
                return "";
        }
    };
    HighAvailabilityChartController.prototype.localizeRole = function (role) {
        switch (role) {
            case SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityRole.Unknown:
                return this._t("Unknown");
            case SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityRole.Primary:
                return this._t("Primary");
            case SolarWinds_Orion_ASA_WebApi_1.HighAvailabilityRole.Secondary:
                return this._t("Secondary");
            default:
                return "";
        }
    };
    return HighAvailabilityChartController;
}());
exports.default = HighAvailabilityChartController;


/***/ }),
/* 250 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ILinkStateInfo_1 = __webpack_require__(83);
var asa = __webpack_require__(1);
exports.highAvailabilityLinkStateInfos = [
    {
        title: "Standby state unknown",
        description: "",
        haState: asa.HighAvailabilityState.Unknown,
        haStateType: ILinkStateInfo_1.HaStateType.Standby,
        rank: 10
    },
    {
        title: "Config sync unknown",
        description: "",
        haState: asa.HighAvailabilityState.Unknown,
        haStateType: ILinkStateInfo_1.HaStateType.Config,
        rank: 20
    },
    {
        title: "Connection state sync unknown",
        description: "",
        haState: asa.HighAvailabilityState.Unknown,
        haStateType: ILinkStateInfo_1.HaStateType.Connection,
        rank: 30
    },
    {
        title: "Standby not ready",
        description: "The standby is not ready to take over in the event of a failure!",
        haState: asa.HighAvailabilityState.Down,
        haStateType: ILinkStateInfo_1.HaStateType.Standby,
        rank: 1
    },
    {
        title: "Config not synced",
        description: "Config is not synchronized. Firewall behavior may change if a failover occurs.",
        haState: asa.HighAvailabilityState.Down,
        haStateType: ILinkStateInfo_1.HaStateType.Config,
        rank: 2
    },
    {
        title: "Connection state not synced",
        description: "Connection state information is not in sync. If a failover occurs, \
ongoing connections through this firewall will have to be reestablished.",
        haState: asa.HighAvailabilityState.Down,
        haStateType: ILinkStateInfo_1.HaStateType.Connection,
        rank: 3
    },
    {
        title: "Standby ready",
        description: "The standby is ready to take over in the event of a failure.",
        haState: asa.HighAvailabilityState.Up,
        haStateType: ILinkStateInfo_1.HaStateType.Standby,
        rank: 100
    },
    {
        title: "Config synced",
        description: "Config is in sync.",
        haState: asa.HighAvailabilityState.Up,
        haStateType: ILinkStateInfo_1.HaStateType.Config,
        rank: 200
    },
    {
        title: "Connection state synced",
        description: "Connection state information is in sync.",
        haState: asa.HighAvailabilityState.Up,
        haStateType: ILinkStateInfo_1.HaStateType.Connection,
        rank: 300
    }
];


/***/ }),
/* 251 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var serverSideFilteredList_1 = __webpack_require__(252);
__webpack_require__(84);
exports.default = function (module) {
    module.component("asaServerSideFilteredList", serverSideFilteredList_1.default);
};


/***/ }),
/* 252 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(84);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var serverSideFilteredListController_1 = __webpack_require__(253);
var SessionList = /** @class */ (function () {
    /** @ngInject */
    SessionList.$inject = ["$log"];
    function SessionList($log) {
        this.$log = $log;
        this.restrict = "E";
        this.transclude = false;
        this.template = __webpack_require__(65);
        this.scope = {};
        this.controller = serverSideFilteredListController_1.default;
        this.controllerAs = "serverSideFilteredListController";
        this.bindToController = {
            options: "=?",
            getData: "&getdata",
            parentController: "="
        };
    }
    return SessionList;
}());
exports.default = SessionList;


/***/ }),
/* 253 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ServerSideFilteredListController = /** @class */ (function () {
    /** @ngInject */
    ServerSideFilteredListController.$inject = ["$log", "$q"];
    function ServerSideFilteredListController($log, $q) {
        this.$log = $log;
        this.$q = $q;
        // Default required until UIF-4647 is fixed
        this.queryResults = {
            items: []
        };
        this.pagination = {};
        this.sortingSettings = null;
        this.initialFilterValues = {};
    }
    ServerSideFilteredListController.prototype.getFilterProperties = function () {
        this.getFilterPropertiesDeferred = this.$q.defer();
        return this.getFilterPropertiesDeferred.promise;
    };
    ServerSideFilteredListController.prototype.getFilteredData = function (filters, filterModels, pagination, sorting, search) {
        var _this = this;
        var results = {
            items: [],
            total: 0
        };
        var filterParameters = {
            filters: filters,
            filterModels: filterModels,
            pagination: pagination,
            sorting: sorting,
            searching: search
        };
        if (!angular.isFunction(this.getData)) {
            return this.$q.when(results);
        }
        return this.getData(filterParameters).then(function (response) {
            try {
                if (response === null || response === undefined) {
                    return null;
                }
                results.items = response.items;
                results.total = response.total;
                // HACK!!!! Fixed by UIF-4653
                _this.pagination.total = results.total;
                _this.queryResults = response;
                if (Object.keys(_this.initialFilterValues).length === 0) {
                    _this.initialFilterValues = _this.queryResults.filterValues;
                }
                if (_this.sortingSettings != null && !_this.sortingSettings.sortBy) {
                    _this.sortingSettings = {
                        sortBy: response.sortableColumns[0],
                        direction: response.sortableColumns[0].direction,
                        sortableColumns: response.sortableColumns
                    };
                }
                _this.getFilterPropertiesDeferred.resolve(_this.queryResults.filterProperties);
            }
            catch (e) {
                _this.$log.error(e);
            }
            return results;
        }, function (failure) {
            return results;
        });
    };
    return ServerSideFilteredListController;
}());
exports.default = ServerSideFilteredListController;


/***/ }),
/* 254 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var siteToSiteFilteredTunnels_1 = __webpack_require__(255);
__webpack_require__(85);
__webpack_require__(69);
exports.default = function (module) {
    module.component("asaSiteToSiteFilteredTunnels", siteToSiteFilteredTunnels_1.default);
};


/***/ }),
/* 255 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(85);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var siteToSiteFilteredTunnelsController_1 = __webpack_require__(256);
var SiteToSiteFilteredTunnels = /** @class */ (function () {
    /** @ngInject */
    SiteToSiteFilteredTunnels.$inject = ["$log"];
    function SiteToSiteFilteredTunnels($log) {
        this.$log = $log;
        this.restrict = "E";
        this.transclude = false;
        this.template = __webpack_require__(68);
        this.scope = {};
        this.controller = siteToSiteFilteredTunnelsController_1.default;
        this.controllerAs = "filteredTunnelsController";
        this.bindToController = {};
    }
    return SiteToSiteFilteredTunnels;
}());
exports.default = SiteToSiteFilteredTunnels;


/***/ }),
/* 256 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var orionObjectStatus_1 = __webpack_require__(13);
var favoriteEntity_controller_1 = __webpack_require__(81);
var SiteToSiteFilteredTunnelsController = /** @class */ (function (_super) {
    __extends(SiteToSiteFilteredTunnelsController, _super);
    /** @ngInject */
    SiteToSiteFilteredTunnelsController.$inject = ["$log", "$q", "siteToSiteTunnelsService", "netObjectsService", "getTextService", "xuiToastService"];
    function SiteToSiteFilteredTunnelsController($log, $q, siteToSiteTunnelsService, netObjectsService, getTextService, xuiToastService) {
        var _this = _super.call(this, $log, getTextService, xuiToastService) || this;
        _this.$log = $log;
        _this.$q = $q;
        _this.siteToSiteTunnelsService = siteToSiteTunnelsService;
        _this.netObjectsService = netObjectsService;
        _this.getTextService = getTextService;
        _this.xuiToastService = xuiToastService;
        // For use in templates
        _this.orionObjectStatus = orionObjectStatus_1.OrionObjectStatus;
        _this.options = {
            smartMode: false,
            showSelector: false,
            hideSearch: false,
            triggerSearchOnChange: true,
            hidePagination: false,
            showAddRemoveFilterProperty: false,
            templateUrl: "[widgets]:/orionModules/asa/components/siteToSiteFilteredTunnels/siteToSiteFilteredTunnelsItem.html"
        };
        return _this;
    }
    SiteToSiteFilteredTunnelsController.prototype.getSessions = function (filters, filterModels, pagination, sorting, search) {
        var _this = this;
        return this.netObjectsService.getNetObject()
            .then(function (netObject) {
            var filterParameters = {
                filterValues: filters,
                filterModels: filterModels,
                pagination: pagination,
                sorting: sorting,
                searching: search
            };
            return _this.siteToSiteTunnelsService.getFilteredTunnels(netObject.nodeId, filterParameters);
        }, function (failure) {
            _this.$log.error("netObjectsService.getNetObject failed", failure);
            return null;
        });
    };
    SiteToSiteFilteredTunnelsController.prototype.switchIsFavoriteValue = function (tunnel) {
        var switchIsFavoriteResult = this.siteToSiteTunnelsService.assignIsFavoriteValue(tunnel.tunnelID, !tunnel.isFavorite);
        _super.prototype.processSwitchIsFavoriteResult.call(this, switchIsFavoriteResult, tunnel);
    };
    ;
    return SiteToSiteFilteredTunnelsController;
}(favoriteEntity_controller_1.FavoriteEntityController));
exports.default = SiteToSiteFilteredTunnelsController;


/***/ }),
/* 257 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var cliCredentialsNotification_1 = __webpack_require__(258);
exports.default = function (module) {
    module.component("cliCredentialsNotification", cliCredentialsNotification_1.default);
};


/***/ }),
/* 258 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var cliCredentialsNotificationController_1 = __webpack_require__(259);
var CliCredentialsNotification = /** @class */ (function () {
    function CliCredentialsNotification() {
        this.restrict = "E";
        this.transclude = true;
        this.replace = true;
        this.template = __webpack_require__(60);
        this.controller = cliCredentialsNotificationController_1.default;
        this.controllerAs = "vm";
        this.scope = {};
        this.bindToController = {};
    }
    return CliCredentialsNotification;
}());
exports.default = CliCredentialsNotification;


/***/ }),
/* 259 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var CliCredentialsNotificationController = /** @class */ (function () {
    /** @ngInject */
    CliCredentialsNotificationController.$inject = ["asaNodesService"];
    function CliCredentialsNotificationController(asaNodesService) {
        this.asaNodesService = asaNodesService;
        this.isDisplayed = false;
    }
    CliCredentialsNotificationController.prototype.$onInit = function () {
        var _this = this;
        this.asaNodesService.hasCliCredentials().then(function (hasCredentials) {
            _this.isDisplayed = !hasCredentials;
        });
    };
    return CliCredentialsNotificationController;
}());
exports.default = CliCredentialsNotificationController;


/***/ }),
/* 260 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var nodePropertiesLink_1 = __webpack_require__(261);
exports.default = function (module) {
    module.component("asaNodePropertiesLink", nodePropertiesLink_1.default);
};


/***/ }),
/* 261 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(86);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var nodePropertiesLinkController_1 = __webpack_require__(262);
__webpack_require__(86);
var NodePropertiesLink = /** @class */ (function () {
    function NodePropertiesLink() {
        this.restrict = "E";
        this.transclude = true;
        this.replace = true;
        this.template = __webpack_require__(62);
        this.controller = nodePropertiesLinkController_1.default;
        this.controllerAs = "vm";
        this.bindToController = {};
    }
    return NodePropertiesLink;
}());
exports.default = NodePropertiesLink;


/***/ }),
/* 262 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NodePropertiesLinkController = /** @class */ (function () {
    /** @ngInject */
    NodePropertiesLinkController.$inject = ["$window", "swDemoService", "netObjectsService"];
    function NodePropertiesLinkController($window, swDemoService, netObjectsService) {
        this.$window = $window;
        this.swDemoService = swDemoService;
        this.netObjectsService = netObjectsService;
    }
    NodePropertiesLinkController.prototype.$onInit = function () {
        var _this = this;
        this.netObjectsService.getNetObject().then(function (netObject) {
            _this.nodePropertiesUrl = "/Orion/Nodes/NodeProperties.aspx?Nodes=" + netObject.nodeId;
        });
    };
    NodePropertiesLinkController.prototype.onClick = function () {
        if (!this.swDemoService.isDemoMode()) {
            this.$window.location.href = this.nodePropertiesUrl;
        }
        else {
            this.swDemoService.showDemoErrorToast();
        }
    };
    return NodePropertiesLinkController;
}());
exports.default = NodePropertiesLinkController;


/***/ }),
/* 263 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var contextList_1 = __webpack_require__(264);
__webpack_require__(87);
exports.default = function (module) {
    module.component("asaContextList", contextList_1.default);
};


/***/ }),
/* 264 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(87);

"use strict";
/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var contextListController_1 = __webpack_require__(265);
var ContextList = /** @class */ (function () {
    /** @ngInject */
    ContextList.$inject = ["$log"];
    function ContextList($log) {
        var _this = this;
        this.$log = $log;
        this.restrict = "E";
        this.transclude = false;
        this.template = __webpack_require__(61);
        this.scope = {};
        this.controller = contextListController_1.default;
        this.controllerAs = "contextList";
        this.bindToController = {};
        this.link = function (scope, instanceElement, instanceAttributes, controller) {
            try {
                controller.initialize(instanceElement);
            }
            catch (e) {
                _this.$log.error(e);
            }
        };
    }
    return ContextList;
}());
exports.default = ContextList;


/***/ }),
/* 265 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var nodeContext_1 = __webpack_require__(266);
var ContextListController = /** @class */ (function () {
    /** @ngInject */
    ContextListController.$inject = ["$log", "$q", "contextListService", "netObjectsService", "widgetHelperService"];
    function ContextListController($log, $q, contextListService, netObjectsService, widgetHelperService) {
        var _this = this;
        this.$log = $log;
        this.$q = $q;
        this.contextListService = contextListService;
        this.netObjectsService = netObjectsService;
        this.widgetHelperService = widgetHelperService;
        this.items = [];
        this.$onInit = function () {
            _this.isBusy = true;
            _this.loadFailed = false;
            _this.options = {
                hidePagination: false,
                hideSearch: true,
            };
            _this.paging = {
                page: 0,
                pageSize: 5
            };
        };
    }
    ContextListController.prototype.initialize = function (instanceElement) {
        var _this = this;
        try {
            this.isBusy = true;
            this.instanceElement = instanceElement;
            this.items = [];
            this.netObjectsService.getNetObject().then(function (netObject) {
                _this.netObject = netObject;
                _this.initializeContexts();
            });
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    ContextListController.prototype.initializeContexts = function () {
        var _this = this;
        try {
            this.contextListService.getNodeContexts(this.netObject.nodeId).then(function (success) {
                if (success === null || success.length === 0) {
                    _this.widgetHelperService.hideParentWidget(_this.instanceElement);
                    _this.isBusy = false;
                    return;
                }
                var contexts = new Array();
                success.forEach(function (dto) { return contexts.push(new nodeContext_1.default(dto)); });
                _this.items = contexts;
                _this.isBusy = false;
            }, function (failure) {
                _this.$log.error(failure);
                _this.items = [];
                // Awaiting story for failure handling - UIF-4201
                _this.loadFailed = true;
                _this.isBusy = false;
                _this.widgetHelperService.hideParentWidget(_this.instanceElement);
            });
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    return ContextListController;
}());
exports.default = ContextListController;


/***/ }),
/* 266 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NodeContext = /** @class */ (function () {
    function NodeContext(dto) {
        jQuery.extend(this, dto);
        if (this.isAdminContext) {
            this.hostName = "Admin-Context";
        }
        this.isOrionNode = (this.nodeId != null);
    }
    return NodeContext;
}());
exports.default = NodeContext;


/***/ }),
/* 267 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var shortTimeSpan_filter_1 = __webpack_require__(268);
var shortDateOrTime_filter_1 = __webpack_require__(269);
var metric_filter_1 = __webpack_require__(270);
var encryptAlgorithm_filter_1 = __webpack_require__(271);
var hashAlgorithm_filter_1 = __webpack_require__(272);
var sessionStateToApolloStateFilter_1 = __webpack_require__(273);
var sessionStateToTextFilter_1 = __webpack_require__(274);
var humanizedDurationFilter_1 = __webpack_require__(275);
var humanizedDateTimeFilter_1 = __webpack_require__(276);
var siteToSiteVpnTunnelOrionStatus_filter_1 = __webpack_require__(277);
var siteToSiteVpnTunnelOrionStatusTextCSS_filter_1 = __webpack_require__(278);
var orionStatusToXuiIconFilter_1 = __webpack_require__(279);
var nodeStatusToXuiStyleFilter_1 = __webpack_require__(280);
var remoteAccessTunnelTypeToTextFilter_1 = __webpack_require__(281);
exports.default = function (module) {
    // register filters
    module.filter("asaShortTimeSpan", shortTimeSpan_filter_1.default);
    module.filter("asaShortDateOrTime", shortDateOrTime_filter_1.default);
    module.filter("asaMetric", metric_filter_1.default);
    module.filter("asaEncryptAlgorithm", encryptAlgorithm_filter_1.default);
    module.filter("asaHashAlgorithm", hashAlgorithm_filter_1.default);
    module.filter("sessionStateToApolloState", sessionStateToApolloStateFilter_1.default);
    module.filter("sessionStateToText", sessionStateToTextFilter_1.default);
    module.filter("humanizedDateTime", humanizedDateTimeFilter_1.default);
    module.filter("humanizedDuration", humanizedDurationFilter_1.default);
    module.filter("siteToSiteVpnTunnelOrionStatusFilter", siteToSiteVpnTunnelOrionStatus_filter_1.default);
    module.filter("siteToSiteVpnTunnelOrionStatusTextCSSFilter", siteToSiteVpnTunnelOrionStatusTextCSS_filter_1.default);
    module.filter("orionStatusToXuiIcon", orionStatusToXuiIconFilter_1.default);
    module.filter("nodeStatusToXuiStyle", nodeStatusToXuiStyleFilter_1.default);
    module.filter("remoteAccessTunnelTypeToText", remoteAccessTunnelTypeToTextFilter_1.default);
};


/***/ }),
/* 268 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Filter converts TimeSpan string value ("00:00:00" or "000.00:00.00" ~ days.hours:minutes:seconds)
 * to a textual value e.g. "20 days" or "50 minutes", whichever highest unit is present in the input
 *
 * Result is localized
 * Result when empty defaults to "-"
 */
function shortTimeSpanFilter(_t) {
    var units = [_t("days"), _t("hours"), _t("minutes"), _t("seconds")];
    var unitsSingular = [_t("day"), _t("hour"), _t("minute"), _t("second")];
    var timeSpanRegex = /(?:(\d+)\.)?0*(\d+):0*(\d+):0*(\d+)/;
    var nbsp = String.fromCharCode(160);
    return function (timeSpanString, emptyResult) {
        if (emptyResult === void 0) { emptyResult = _t("now"); }
        var timeSpanMatches = timeSpanRegex.exec(timeSpanString);
        if (timeSpanMatches == null) {
            return null;
        }
        timeSpanMatches.shift(); // start with individual matches
        for (var index = 0; index < units.length; index++) {
            if (timeSpanMatches[index] && timeSpanMatches[index] !== "0") {
                var unitsArray = timeSpanMatches[index] !== "1" ? units : unitsSingular;
                return timeSpanMatches[index] + nbsp + unitsArray[index];
            }
        }
        return emptyResult;
    };
}
shortTimeSpanFilter.$inject = ["getTextService"];
exports.default = shortTimeSpanFilter;


/***/ }),
/* 269 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function shortDateOrTimeFilter($filter) {
    return function (datetimeString) {
        var datetime = new Date(datetimeString);
        var datetimeFormat = new Date().toDateString() === datetime.toDateString() ? "shortTime" : "MMM d";
        return $filter("date")(datetime, datetimeFormat);
    };
}
shortDateOrTimeFilter.$inject = ["$filter"];
exports.default = shortDateOrTimeFilter;


/***/ }),
/* 270 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function metricFilter() {
    var metricPrefixes = ["y", "z", "a", "f", "p", "n", "μ", "m", "", "k", "M", "G", "T", "P", "E", "Z", "Y"];
    var zeroMetricIndex = 8;
    var defaultScale = 1000;
    var knownScales = {
        b: 1024,
        B: 1024
    };
    var nbsp = String.fromCharCode(160);
    return function (value, unit, precision, scale) {
        if (precision === void 0) { precision = 1; }
        if (!scale) {
            scale = unit in knownScales ? knownScales[unit] : defaultScale;
        }
        if (isNaN(parseFloat(value)) || !isFinite(value)) {
            return "---";
        }
        var magnitude = Math.floor(Math.log(value) / Math.log(scale));
        magnitude = Math.max(Math.min(magnitude + zeroMetricIndex, metricPrefixes.length), 0) - zeroMetricIndex;
        var metricIndex = magnitude + zeroMetricIndex;
        var finalNumber = (value / Math.pow(scale, Math.floor(magnitude))).toFixed(precision);
        if (parseFloat(finalNumber) === 0) {
            return "0" + nbsp + unit;
        }
        return finalNumber + nbsp + metricPrefixes[metricIndex] + unit;
    };
}
exports.default = metricFilter;


/***/ }),
/* 271 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// Transforms the EncryptAlgorithm specified as number into its textual representation.
function encryptAlgorithmFilter(_t, swUtil) {
    return function (encryptAlgorithmValue) {
        switch (encryptAlgorithmValue) {
            case 1:
                return _t("None");
            case 2:
                return _t("DES");
            case 3:
                return _t("DES3");
            case 4:
                return _t("AES-128");
            case 5:
                return _t("AES-192");
            case 6:
                return _t("AES-256");
            case 7:
                return _t("RC4");
            case 8:
                return _t("RC5");
            case 9:
                return _t("IDEA");
            case 10:
                return _t("CAST");
            case 11:
                return _t("Blowfish");
            case 12:
                return _t("AES");
            case 13:
                return _t("AES-128-CBC");
            case 14:
                return _t("AES-192-CBC");
            case 15:
                return _t("AES-256-CBC");
            case 16:
                return _t("AES-128-CCM");
            case 17:
                return _t("AES-128-GCM");
            case 18:
                return _t("AES-256-GCM");
            case 255:
                return _t("Various");
            default:
                return encryptAlgorithmValue || encryptAlgorithmValue === 0
                    ? swUtil.formatString(_t("Unknown ({0})"), encryptAlgorithmValue)
                    : _t("Unknown");
        }
        ;
    };
}
encryptAlgorithmFilter.$inject = ["getTextService", "swUtil"];
exports.default = encryptAlgorithmFilter;


/***/ }),
/* 272 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// Transforms the HashAlgorithm specified as number into its textual representation.
function hashAlgorithmFilter(_t, swUtil) {
    return function (hashAlgorithmValue) {
        switch (hashAlgorithmValue) {
            case 1:
                return _t("None");
            case 2:
                return _t("MD5");
            case 3:
                return _t("SHA");
            case 4:
                return _t("SHA1");
            case 5:
                return _t("SHA256");
            case 6:
                return _t("SHA384");
            case 7:
                return _t("SHA512");
            default:
                return hashAlgorithmValue || hashAlgorithmValue === 0
                    ? swUtil.formatString(_t("Unknown ({0})"), hashAlgorithmValue)
                    : _t("Unknown");
        }
        ;
    };
}
hashAlgorithmFilter.$inject = ["getTextService", "swUtil"];
exports.default = hashAlgorithmFilter;


/***/ }),
/* 273 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
function sessionStateToApolloStateFilter() {
    return function (state) {
        switch (state) {
            case asa.TunnelState.Connected:
                return "state_running";
            case asa.TunnelState.Dropped:
                return "status_used";
            case asa.TunnelState.Disconnected:
                return "status_testing";
            case asa.TunnelState.Failed:
                return "status_critical";
            default:
                return "status_unknown";
        }
        ;
    };
}
exports.default = sessionStateToApolloStateFilter;


/***/ }),
/* 274 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
function sessionStateToApolloStateFilter(_t) {
    return function (state) {
        switch (state) {
            case asa.TunnelState.Connected:
                return _t("Connected");
            case asa.TunnelState.Dropped:
                return _t("Not Connected");
            case asa.TunnelState.Disconnected:
                return _t("Connection Ended");
            case asa.TunnelState.Failed:
                return _t("Not Connected");
            default:
                return _t("Unknown");
        }
        ;
    };
}
sessionStateToApolloStateFilter.$inject = ["getTextService"];
exports.default = sessionStateToApolloStateFilter;


/***/ }),
/* 275 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
function humanizedDurationFilter() {
    return function (minutes) {
        return moment.duration(minutes, "minutes").humanize();
    };
}
exports.default = humanizedDurationFilter;


/***/ }),
/* 276 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function humanizedDateTimeFilter($filter) {
    return function (datetimeString) {
        var datetime = new Date(datetimeString);
        var datetimeFormat = new Date().toDateString() === datetime.toDateString() ? "shortTime" : "MMM d";
        return $filter("date")(datetime, datetimeFormat);
    };
}
humanizedDateTimeFilter.$inject = ["$filter"];
exports.default = humanizedDateTimeFilter;


/***/ }),
/* 277 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionObjectStatus_1 = __webpack_require__(13);
function siteToSiteVpnTunnelOrionStatusFilter() {
    return function (status) {
        switch (status) {
            case orionObjectStatus_1.OrionObjectStatus.Up:
                return "up";
            case orionObjectStatus_1.OrionObjectStatus.Down:
                return "down";
            case orionObjectStatus_1.OrionObjectStatus.Warning:
                return "warning";
            case orionObjectStatus_1.OrionObjectStatus.Inactive:
                return "inactive";
            default:
                return "unknown";
        }
        ;
    };
}
exports.default = siteToSiteVpnTunnelOrionStatusFilter;


/***/ }),
/* 278 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionObjectStatus_1 = __webpack_require__(13);
// Transforms the orion object status into CSS class for styling status text.
function siteToSiteVpnTunnelOrionStatusTextCSSFilter() {
    return function (status) {
        switch (status) {
            case orionObjectStatus_1.OrionObjectStatus.Up:
                return "orion-object-status-up-text";
            case orionObjectStatus_1.OrionObjectStatus.Down:
                return "orion-object-status-down-text";
            case orionObjectStatus_1.OrionObjectStatus.Warning:
                return "orion-object-status-warning-text";
            case orionObjectStatus_1.OrionObjectStatus.Inactive:
                return "orion-object-status-inactive-text";
            default:
                return "orion-object-status-unknown-text";
        }
        ;
    };
}
exports.default = siteToSiteVpnTunnelOrionStatusTextCSSFilter;


/***/ }),
/* 279 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
/**
 * General filter that transforms status to string corresponding to orion status icon.
 */
function orionStatusToXuiIconFilter() {
    return function (status) {
        switch (status) {
            case asa.NodeStatus.Up:
                return "up";
            case asa.NodeStatus.Warning:
                return "warning";
            case asa.NodeStatus.Down:
                return "down";
            case asa.NodeStatus.Critical:
                return "critical";
            case asa.NodeStatus.Unmanaged:
                return "unmanaged";
            case asa.NodeStatus.Shutdown:
                return "shutdown";
            case asa.NodeStatus.Unplugged:
                return "unplugged";
            case asa.NodeStatus.Unreachable:
                return "unreachable";
            default:
                return "unknown";
        }
        ;
    };
}
exports.default = orionStatusToXuiIconFilter;


/***/ }),
/* 280 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
var xuiStyle = {
    severity: "severity",
    state: "state",
    text: "text",
    health: "health"
};
;
;
var xuiStyleMap = (_a = {},
    _a[xuiStyle.severity] = (_b = {},
        _b[asa.NodeStatus.Up] = "severity_ok",
        _b[asa.NodeStatus.Active] = "severity_ok",
        _b[asa.NodeStatus.Warning] = "severity_warning",
        _b[asa.NodeStatus.Critical] = "severity_critical",
        _b[asa.NodeStatus.Down] = "severity_critical",
        _b[0] = "severity_unknown",
        _b),
    _a[xuiStyle.health] = (_c = {},
        _c[asa.NodeStatus.Up] = "up",
        _c[asa.NodeStatus.Active] = "up",
        _c[asa.NodeStatus.Warning] = "warning",
        _c[asa.NodeStatus.Critical] = "critical",
        _c[asa.NodeStatus.Unmanaged] = "unmanaged",
        _c[0] = "unknown",
        _c),
    _a[xuiStyle.text] = (_d = {},
        _d[asa.NodeStatus.Warning] = "xui-text-warning",
        _d[asa.NodeStatus.Down] = "xui-text-critical",
        _d[asa.NodeStatus.Critical] = "xui-text-critical",
        _d[0] = "xui-text-normal",
        _d),
    _a[xuiStyle.state] = (_e = {},
        _e[asa.NodeStatus.Active] = "state_ok",
        _e[asa.NodeStatus.Up] = "state_ok",
        _e[asa.NodeStatus.Unplugged] = "state_unplugged",
        _e[0] = "state_unknown",
        _e),
    _a);
/**
 * Maps from Orion NodeStatus to one of for xui styles, text, state,
 * severity, of health.
 * See http://apollo-docs.swdev.local/xui/master/ng1/sdk/api-docs/index.html#/api/xui.directive:xuiIcon
 * and http://apollo-docs.swdev.local/xui/master/ng1/sdk/api-docs/index.html#/api/xui.styles.typography:Typography
 * EXAMPLE:  myStatus | nodeStatusToXuiStyle:'severity'
 */
function nodeStatusToXuiStyleFilter() {
    return function (status, style) {
        if (style === void 0) { style = "health"; }
        return xuiStyleMap[style][status] || xuiStyleMap[style][0];
    };
}
exports.default = nodeStatusToXuiStyleFilter;
var _a, _b, _c, _d, _e;


/***/ }),
/* 281 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
function remoteAccessTunnelTypeToTextFilter() {
    return function (type) {
        return asa.RemoteAccessTunnelType[type];
    };
}
exports.default = remoteAccessTunnelTypeToTextFilter;


/***/ }),
/* 282 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var siteToSiteTunnels_service_1 = __webpack_require__(283);
var remoteTunnelsService_1 = __webpack_require__(284);
var netObjectsService_1 = __webpack_require__(286);
var platformDetailsService_1 = __webpack_require__(287);
var asaInterfaces_service_1 = __webpack_require__(288);
var highAvailabilityService_1 = __webpack_require__(289);
var asaNodesService_1 = __webpack_require__(290);
var contextListService_1 = __webpack_require__(291);
var widgetHelperService_1 = __webpack_require__(292);
exports.default = function (module) {
    module.service("remoteTunnelsService", remoteTunnelsService_1.default);
    module.service("netObjectsService", netObjectsService_1.default);
    module.service("siteToSiteTunnelsService", siteToSiteTunnels_service_1.default);
    module.service("platformDetailsService", platformDetailsService_1.default);
    module.service("asaInterfacesService", asaInterfaces_service_1.default);
    module.service("highAvailabilityService", highAvailabilityService_1.default);
    module.service("asaNodesService", asaNodesService_1.default);
    module.service("contextListService", contextListService_1.default);
    module.service("widgetHelperService", widgetHelperService_1.default);
};


/***/ }),
/* 283 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiTunnelsUrl = "vpn/l2ltunnels/tunnels/{nodeId}";
exports.swApiFilteredTunnelsUrl = "vpn/l2ltunnels/tunnels/{nodeId}/filtered";
exports.swApiSetFavoriteUrl = "vpn/l2ltunnels/setfavorite";
exports.swApiRemoveFavoriteUrl = "vpn/l2ltunnels/removefavorite";
var SiteToSiteTunnelsService = /** @class */ (function () {
    /** @ngInject */
    SiteToSiteTunnelsService.$inject = ["swApi", "$q"];
    function SiteToSiteTunnelsService(swApi, $q) {
        var _this = this;
        this.swApi = swApi;
        this.$q = $q;
        this.getTunnels = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiTunnelsUrl.replace("{nodeId}", nodeId.toString()))
                .get();
        };
        this.assignIsFavoriteValue = function (tunnelId, isFavorite) {
            return _this.swApi
                .api(false) //false for non-legacy ('api2')
                .one(isFavorite ? exports.swApiSetFavoriteUrl : exports.swApiRemoveFavoriteUrl)
                .post(tunnelId);
        };
    }
    SiteToSiteTunnelsService.prototype.getFilteredTunnels = function (nodeId, filterParameters) {
        return this.swApi
            .api(false)
            .one(exports.swApiFilteredTunnelsUrl.replace("{nodeId}", nodeId.toString()))
            .customPOST(filterParameters);
    };
    return SiteToSiteTunnelsService;
}());
exports.default = SiteToSiteTunnelsService;


/***/ }),
/* 284 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var remoteVpnTunnelSession_1 = __webpack_require__(285);
var RemoteTunnelsService = /** @class */ (function () {
    /** @ngInject */
    RemoteTunnelsService.$inject = ["swApi", "$log", "$location"];
    function RemoteTunnelsService(swApi, $log, $location) {
        this.swApi = swApi;
        this.$log = $log;
        this.$location = $location;
        this.apiBaseRoute = "vpn/remoteTunnel/";
    }
    RemoteTunnelsService.prototype.getVpnTunnelSessions = function (nodeId, filterParameters) {
        var _this = this;
        // Back-door for displaying faked tunnel data until real data is available
        if (this.$location.absUrl().indexOf("debug=fake") > 0) {
            nodeId = -nodeId;
        }
        return this.swApi
            .api(false)
            .one(this.apiBaseRoute + "GetVpnTunnelSessions/" + nodeId)
            .customPOST(filterParameters)
            .then(function (success) {
            try {
                var sessions_1 = [];
                if (success == null) {
                    return null;
                }
                // Replace with pleerock/class-transformer
                success.items.forEach(function (dto) {
                    sessions_1.push(new remoteVpnTunnelSession_1.default(dto));
                });
                success.items = sessions_1;
                return success;
            }
            catch (e) {
                _this.$log.log(e);
            }
            return null;
        }, function (error) {
            _this.$log.log(error);
            return null;
        });
    };
    return RemoteTunnelsService;
}());
exports.default = RemoteTunnelsService;


/***/ }),
/* 285 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var asa = __webpack_require__(1);
var RemoteVpnTunnelSession = /** @class */ (function () {
    function RemoteVpnTunnelSession(sessionDto) {
        jQuery.extend(this, sessionDto);
    }
    RemoteVpnTunnelSession.prototype.isConnected = function () {
        return this.state === asa.TunnelState.Connected;
    };
    RemoteVpnTunnelSession.prototype.isDisconnected = function () {
        return this.state === asa.TunnelState.Disconnected;
    };
    RemoteVpnTunnelSession.prototype.connectionDropped = function () {
        return this.state === asa.TunnelState.Dropped;
    };
    RemoteVpnTunnelSession.prototype.connectionFailed = function () {
        return this.state === asa.TunnelState.Failed;
    };
    return RemoteVpnTunnelSession;
}());
exports.default = RemoteVpnTunnelSession;


/***/ }),
/* 286 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiHasCliCredentialsUrlFragment = "asa/nodes/hasCliCredentials/{nodeId}";
var NetObjectsService = /** @class */ (function () {
    /** @ngInject */
    NetObjectsService.$inject = ["$log", "$location", "$q", "swApi"];
    function NetObjectsService($log, $location, $q, swApi) {
        this.$log = $log;
        this.$location = $location;
        this.$q = $q;
        this.swApi = swApi;
        this.netObject = null;
        this.deferredNetObject = this.$q.defer();
    }
    NetObjectsService.prototype.setNetObject = function (netObjectQueryString) {
        try {
            // RE-HACK!!! Gets around Apollo breaking change where netObjectID support was removed without notice
            if (!netObjectQueryString) {
                netObjectQueryString = decodeURIComponent(this.$location.absUrl()).match(/(N\:[0-9]+)/)
                    ? RegExp.$1
                    : "0";
            }
            var nodeId = parseInt(decodeURIComponent(netObjectQueryString).match(/N\:([0-9]+)/)
                ? RegExp.$1
                : "0", 10);
            this.netObject = {
                nodeId: nodeId
            };
            this.deferredNetObject.resolve(this.netObject);
            return true;
        }
        catch (e) {
            this.$log.error(e);
        }
        return false;
    };
    NetObjectsService.prototype.getNetObject = function () {
        return this.deferredNetObject.promise;
    };
    return NetObjectsService;
}());
exports.default = NetObjectsService;


/***/ }),
/* 287 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PlatformDetailsService = /** @class */ (function () {
    /** @ngInject */
    PlatformDetailsService.$inject = ["swApi", "$log"];
    function PlatformDetailsService(swApi, $log) {
        this.swApi = swApi;
        this.$log = $log;
        this.detailsUrl = "asa/platform/";
    }
    PlatformDetailsService.prototype.getPlatformDetails = function (nodeId) {
        return this.swApi
            .api(false)
            .one(this.detailsUrl + nodeId)
            .get();
    };
    return PlatformDetailsService;
}());
exports.default = PlatformDetailsService;


/***/ }),
/* 288 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiSetFavoriteUrl = "asa/interfaces/setfavorite";
exports.swApiRemoveFavoriteUrl = "asa/interfaces/removefavorite";
exports.swApiGetInterfacesUrlFragment = "asa/interfaces/{nodeId}";
var AsaInterfacesService = /** @class */ (function () {
    /** @ngInject */
    AsaInterfacesService.$inject = ["swApi", "$q"];
    function AsaInterfacesService(swApi, $q) {
        var _this = this;
        this.swApi = swApi;
        this.$q = $q;
        this.getInterfaces = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiGetInterfacesUrlFragment.replace("{nodeId}", nodeId.toString()))
                .get();
        };
        this.assignIsFavoriteValue = function (interfaceId, isFavorite) {
            return _this.swApi
                .api(false) //false for non-legacy ('api2')
                .one(isFavorite ? exports.swApiSetFavoriteUrl : exports.swApiRemoveFavoriteUrl)
                .post(interfaceId);
        };
    }
    return AsaInterfacesService;
}());
exports.default = AsaInterfacesService;


/***/ }),
/* 289 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var HighAvailabilityService = /** @class */ (function () {
    /** @ngInject */
    HighAvailabilityService.$inject = ["swApi", "$log"];
    function HighAvailabilityService(swApi, $log) {
        this.swApi = swApi;
        this.$log = $log;
        this.detailsUrl = "asa/ha/";
    }
    HighAvailabilityService.prototype.getHighAvailabilityInfo = function (nodeId) {
        return this.swApi
            .api(false)
            .one(this.detailsUrl + nodeId)
            .get();
    };
    return HighAvailabilityService;
}());
exports.default = HighAvailabilityService;


/***/ }),
/* 290 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiHasCliCredentialsUrlFragment = "asa/nodes/hasCliCredentials/{nodeId}";
var AsaNodesService = /** @class */ (function () {
    /** @ngInject */
    AsaNodesService.$inject = ["swApi", "netObjectsService"];
    function AsaNodesService(swApi, netObjectsService) {
        var _this = this;
        this.swApi = swApi;
        this.netObjectsService = netObjectsService;
        this.hasCliCredentials = function () {
            return _this.netObjectsService.getNetObject().then(function (netObject) {
                return _this.swApi
                    .api(false)
                    .one(exports.swApiHasCliCredentialsUrlFragment.replace("{nodeId}", netObject.nodeId.toString()))
                    .get()
                    .then(function (hasCredentials) {
                    return hasCredentials;
                });
            });
        };
    }
    return AsaNodesService;
}());
exports.default = AsaNodesService;


/***/ }),
/* 291 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ContextListService = /** @class */ (function () {
    /** @ngInject */
    ContextListService.$inject = ["swApi", "$log"];
    function ContextListService(swApi, $log) {
        this.swApi = swApi;
        this.$log = $log;
        this.detailsUrl = "asa/contexts/";
    }
    ContextListService.prototype.getNodeContexts = function (nodeId) {
        return this.swApi
            .api(false)
            .one(this.detailsUrl + nodeId)
            .get();
    };
    return ContextListService;
}());
exports.default = ContextListService;


/***/ }),
/* 292 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WidgetHelperService = /** @class */ (function () {
    /** @ngInject */
    function WidgetHelperService() {
        // Empty
    }
    /*
    Hides parent widget of instanceElement
    */
    WidgetHelperService.prototype.hideParentWidget = function (instanceElement) {
        // Temp hack until Apollo provides solution.
        if (instanceElement == null) {
            return false;
        }
        var wrapper = instanceElement.closest(".ResourceWrapper");
        if (wrapper.length === 0) {
            return false;
        }
        wrapper.addClass("HiddenResourceWrapper");
        return true;
    };
    return WidgetHelperService;
}());
exports.default = WidgetHelperService;


/***/ }),
/* 293 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 294 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(295);
var components_1 = __webpack_require__(298);
var services_1 = __webpack_require__(305);
__webpack_require__(308);
exports.default = function (module) {
    widgets_1.default(module);
    components_1.default(module);
    services_1.default(module);
    widgets_1.default(module);
};


/***/ }),
/* 295 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(296);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 296 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessListsWidgetController_1 = __webpack_require__(297);
exports.default = function (module) {
    module.controller("aclListsWidget", accessListsWidgetController_1.AccessListsWidgetController);
};


/***/ }),
/* 297 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widget_decorator_1 = __webpack_require__(0);
exports.WidgetSettingsName = "accessListsWidgetSettings";
var AccessListsWidgetController = /** @class */ (function () {
    /** @ngInject */
    AccessListsWidgetController.$inject = ["$log"];
    function AccessListsWidgetController($log) {
        var _this = this;
        this.$log = $log;
        this.$onInit = function () {
            _this.nodeId = _this.getNodeIdFromUri().toString();
        };
        this.onBackgroundUpdate = function () { return _this.onChange(false); };
        this.onLoad = function (config) {
            _this.config = config;
        };
        this.onSettingsChanged = function (settings) {
            _this.onChange();
        };
        this.onChange = function (showBusy) {
            if (showBusy === void 0) { showBusy = true; }
            //this.config.isBusy(showBusy, this._t("Updating..."));
            //this.config.isBusy(false);
        };
        // This is a temporary solution, NetObject should be provided by platform in config.settings.netObject (UIF-3925)
        this.getNodeIdFromUri = function () {
            var queryVariable = _this.getUrlParameter("NetObject");
            return queryVariable ? parseInt(queryVariable.split(":")[1], 10) : 0;
        };
        this.getUrlParameter = function (name) {
            name = name.replace(/[\[]/, "'\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
            var results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };
    }
    AccessListsWidgetController = __decorate([
        widget_decorator_1.Widget({
            selector: "accessListsWidget",
            template: __webpack_require__(78),
            controllerAs: "accessListsWdgCntl",
            settingName: exports.WidgetSettingsName
        }),
        __metadata("design:paramtypes", [Object])
    ], AccessListsWidgetController);
    return AccessListsWidgetController;
}());
exports.AccessListsWidgetController = AccessListsWidgetController;


/***/ }),
/* 298 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(299);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 299 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessList_directive_1 = __webpack_require__(300);
var accessList_directive_controller_1 = __webpack_require__(88);
var index_1 = __webpack_require__(302);
//import "../../styles/ncm-aclrule.less";
exports.default = function (module) {
    module.component("accessList", accessList_directive_1.AccessList);
    module.controller("accessListDirectiveController", accessList_directive_controller_1.AccessListController);
    index_1.default(module);
};


/***/ }),
/* 300 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(301);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var accessList_directive_controller_1 = __webpack_require__(88);
/**
 * Directive function for access control list component
 */
var AccessList = /** @class */ (function () {
    function AccessList() {
        this.restrict = "E";
        this.controller = accessList_directive_controller_1.AccessListController;
        this.controllerAs = "ctrlAccessList";
        this.template = __webpack_require__(76);
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            nodeId: "<"
        };
    }
    return AccessList;
}());
exports.AccessList = AccessList;


/***/ }),
/* 301 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 302 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var aclSrdIcon_directive_controller_1 = __webpack_require__(90);
var aclSrdIcon_directive_1 = __webpack_require__(303);
exports.default = function (module) {
    module.component("aclSrdIcon", aclSrdIcon_directive_1.AclSrdIcon);
    module.controller("aclSrdIconController", aclSrdIcon_directive_controller_1.AclSrdIconController);
};


/***/ }),
/* 303 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(304);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angularDirectiveUtils_1 = __webpack_require__(91);
var angularDirectiveUtils_2 = __webpack_require__(91);
var aclSrdIcon_directive_controller_1 = __webpack_require__(90);
var AclSrdIcon = /** @class */ (function () {
    function AclSrdIcon() {
        this.restrict = angularDirectiveUtils_1.Restrict.element;
        this.transclude = false;
        this.controller = aclSrdIcon_directive_controller_1.AclSrdIconController;
        this.controllerAs = "controller";
        this.template = __webpack_require__(77);
        this.replace = true;
        this.scope = {
            acl: angularDirectiveUtils_2.BindingScope.twoWay
        };
    }
    return AclSrdIcon;
}());
exports.AclSrdIcon = AclSrdIcon;


/***/ }),
/* 304 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 305 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessListGrid_service_1 = __webpack_require__(89);
var ncmNavigation_service_1 = __webpack_require__(306);
var accessList_service_1 = __webpack_require__(307);
exports.default = function (module) {
    module.service("ncmWidgetsAccessListService", accessList_service_1.NcmWidgetsAccessListService);
    module.service("ncmWidgetsNavigationService", ncmNavigation_service_1.NcmWidgetsNavigationService);
    module.service("ncmWidgetsAccessListGridService", accessListGrid_service_1.NcmWidgetsAccessListGridService);
};


/***/ }),
/* 306 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmWidgetsNavigationService = /** @class */ (function () {
    /** @ngInject */
    NcmWidgetsNavigationService.$inject = ["$window", "swisService"];
    function NcmWidgetsNavigationService($window, swisService) {
        var _this = this;
        this.$window = $window;
        this.swisService = swisService;
        this.swqlAsaSubViewId = "SELECT ViewID FROM Orion.Views WHERE ViewKey='ASA Interfaces Subview'";
        this.getAclDetailsUrl = function (params) {
            if (params) {
                var encodedAclName = encodeURIComponent(params.aclName);
                return "/ui/ncm/aclRules/" + params.nodeId + "/" + params.configId + "?aclName=" + encodedAclName;
            }
        };
        this.getCompareAclUrl = function (params) {
            if (params) {
                var urlParams = "?leftNodeId=" + params.leftNodeId +
                    ("&leftConfigId=" + params.leftConfigId) +
                    ("&leftAclName=" + encodeURIComponent(params.leftAclName)) +
                    ("&rightNodeId=" + params.rightNodeId) +
                    ("&rightConfigId=" + params.rightConfigId) +
                    ("&rightAclName=" + encodeURIComponent(params.rightAclName));
                return "/ui/ncm/aclRulesDiff/" + urlParams;
            }
        };
        this.navigateToUrl = function (url) {
            if (url) {
                _this.$window.open(url, "_blank");
            }
        };
        this.getAsaIfSubviewId = function () {
            return _this.swisService.query(_this.swqlAsaSubViewId)
                .then(function (results) {
                if (results.rows.length > 0) {
                    return results.rows[0]["ViewID"];
                }
                else {
                    return undefined;
                }
            });
        };
    }
    return NcmWidgetsNavigationService;
}());
exports.NcmWidgetsNavigationService = NcmWidgetsNavigationService;


/***/ }),
/* 307 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiUrl = "ncm/acl/history";
var NcmWidgetsAccessListService = /** @class */ (function () {
    /** @ngInject */
    NcmWidgetsAccessListService.$inject = ["$log", "swApi"];
    function NcmWidgetsAccessListService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.getAccessControlLists = function (nodeId) {
            _this.$log.info("AccessListService.getAccessControlLists called");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + nodeId)
                .get();
        };
    }
    return NcmWidgetsAccessListService;
}());
exports.NcmWidgetsAccessListService = NcmWidgetsAccessListService;


/***/ }),
/* 308 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 309 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var TemplateLateLoader = /** @class */ (function () {
    function TemplateLateLoader() {
    }
    TemplateLateLoader.load = function (templates) {
        if (angular.element(document.body) && angular.element(document.body).injector()) {
            angular.element(document.body).injector().invoke(["$templateCache", "$log",
                function ($templateCache, $log) {
                    angular.forEach(templates, function (value, key) {
                        if ($templateCache.get(key)) {
                            $log.warn("overwriting template cache value with key = " + key);
                        }
                        $templateCache.put(key, value);
                    });
                }]);
        }
    };
    return TemplateLateLoader;
}());
exports.TemplateLateLoader = TemplateLateLoader;


/***/ })
/******/ ]);
});
//# sourceMappingURL=widgets.js.map