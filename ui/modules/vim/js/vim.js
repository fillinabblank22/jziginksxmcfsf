/*!
 * @solarwinds/vim 8.8.0-1179
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 62);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Helper function for autogenerating of class's $inject property
 *
 * @param {string} dependency Name of a dependency under which injected component is registered (e.g. xuiToastService)
 * @example
 * import {Inject} from "decorators/di";
 * class MyClass { constructor(@Inject("xuiToastService") private svc: IXuiToastService){} };
 */
function Inject(dependency) {
    return function (prototype, method, argumentPosition) {
        prototype.$inject = prototype.$inject || [];
        prototype.$inject[argumentPosition] = dependency;
    };
}
exports.Inject = Inject;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(2);
exports.NgServiceNames = index_1.NgServiceNames;
exports.ApolloServiceNames = index_1.ApolloServiceNames;
var index_2 = __webpack_require__(2);
exports.MainServiceNames = index_2.ServiceNames;
/**
 * Service names used by capacity planning
 *
 * @class
 */
exports.ServiceNames = {
    licensingService: "vimCapacityPlanningLicensingService",
    routingRegistrationService: "vimCapacityPlanningRoutingRegistration",
    routingService: "vimCapacityPlanningRoutingService",
    reportDetailService: "vimCapacityPlanningReportDetailService",
    reportListService: "vimCapacityPlanningReportListService",
    reportWizardService: "vimCapacityPlanningReportWizardService",
    webUtilsService: "vimCapacityPlanningWebUtilsService"
};


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of services provided by Angular and related libraries
 */
exports.NgServiceNames = {
    ngWindowService: "$window",
    ngQService: "$q",
    ngScope: "$scope",
    ngState: "$state",
    ngStateParams: "$stateParams",
    ngProvideService: "$provide",
    ngControllerService: "$controller",
    ngRootScopeService: "$rootScope",
    ngHttpBackendService: "$httpBackend",
    ngInjectorService: "$injector",
    ngTimeoutService: "$timeout",
    ngSanitizeService: "$sanitize"
};
/**
 * Names of services provided by Apollo
 */
exports.ApolloServiceNames = {
    toastService: "xuiToastService",
    getTextService: "getTextService",
    swApiService: "swApi",
    swUtilService: "swUtil",
    swisService: "swisService",
    dateTimeService: "swDateTimeService",
    dialogService: "xuiDialogService",
    xuiFilteredListService: "xuiFilteredListService",
    wizardDialogService: "xuiWizardDialogService",
    helpLinkService: "helpLinkService"
};
/**
 * Service names used by VIM
 *
 * @class
 */
exports.ServiceNames = {
    eventsService: "vimEventsService",
    vmService: "vimVmService"
};


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(21));
__export(__webpack_require__(27));
__export(__webpack_require__(28));
__export(__webpack_require__(29));
__export(__webpack_require__(30));
__export(__webpack_require__(31));
__export(__webpack_require__(32));
__export(__webpack_require__(22));
// re-export VIM common
__export(__webpack_require__(7));
__export(__webpack_require__(8));
__export(__webpack_require__(4));


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Possible orion user roles
 */
exports.Roles = {
    admin: "admin",
    demo: "demo"
};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * VIM module names
 *
 * @const
 */
exports.ModuleNames = {
    main: "vim",
    providers: "vim.providers",
    services: "vim.services",
    filters: "vim.filters"
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(7));
__export(__webpack_require__(5));
__export(__webpack_require__(4));
__export(__webpack_require__(8));
__export(__webpack_require__(13));
__export(__webpack_require__(14));
__export(__webpack_require__(15));
__export(__webpack_require__(16));


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var roles_1 = __webpack_require__(4);
/**
 * Auth profiles used by Recommendations engine
 *
 * @const
 * @var
 */
exports.AuthProfiles = {
    guestProfile: {
        roles: [],
        permissions: []
    },
    adminProfile: {
        roles: [roles_1.Roles.admin],
        permissions: []
    },
    adminProfileDemoAllowed: {
        roles: [roles_1.Roles.admin, roles_1.Roles.demo],
        permissions: []
    }
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constants for net object prefixes managed by VIM
 *
 * @const
 */
exports.NetObjectPrefixes = {
    cluster: "VMC",
    host: "VH"
};


/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(35));
__export(__webpack_require__(17));
__export(__webpack_require__(25));
__export(__webpack_require__(26));
__export(__webpack_require__(34));


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Basic colors
 */
var Colors = /** @class */ (function () {
    function Colors() {
    }
    /* Primary blue */
    Colors.primaryBlue = "#297994";
    /* Light gray */
    Colors.lightGray = "#D5D5D5";
    /* Chart orange */
    Colors.chartOrange = "#ff7300";
    /* Chart warning background */
    Colors.chartWarningBackground = "#FCF8E0";
    /* Chart critical background */
    Colors.chartCriticalBackground = "#FFE4E0";
    /* Chart ultramarine */
    Colors.chartUltramarine = "#6666C0";
    /* Chart ultramarine light */
    Colors.chartUltramarineLight = "#a6a6db";
    return Colors;
}());
exports.Colors = Colors;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baseUrl = "vim/general";
var getLogEntryIdBuAlertIdUrlMethod = "get-logentryid-by-alertid";
var getTopLevelVMwareNodesNotLicensedInLM = "get-top-level-VMware-nodes-not-licensed-in-LM";
/**
 * Backend urls used by vim module
 *
 * @const
 */
exports.SwApiUrls = {
    baseUrl: baseUrl,
    getLogEntryIdBuAlertIdUrlMethod: getLogEntryIdBuAlertIdUrlMethod,
    getLogEntryIdBuAlertIdUrl: baseUrl + "/" + getLogEntryIdBuAlertIdUrlMethod,
    getTopLevelVMwareNodesNotLicensedInLM: baseUrl + "/" + getTopLevelVMwareNodesNotLicensedInLM,
};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * SWIS Entities names
 *
 * @const
 */
exports.SwisEntitiesNames = {
    vm: "Orion.VIM.VirtualMachines"
};


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Possible virtualization platforms
 */
var Platforms;
(function (Platforms) {
    Platforms[Platforms["VMware"] = 1] = "VMware";
    Platforms[Platforms["HyperV"] = 2] = "HyperV";
})(Platforms = exports.Platforms || (exports.Platforms = {}));
;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(37));
__export(__webpack_require__(38));


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(33));
__export(__webpack_require__(19));
__export(__webpack_require__(41));


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var BytesUnit;
(function (BytesUnit) {
    BytesUnit[BytesUnit["B"] = 0] = "B";
    BytesUnit[BytesUnit["kB"] = 1] = "kB";
    BytesUnit[BytesUnit["MB"] = 2] = "MB";
    BytesUnit[BytesUnit["GB"] = 3] = "GB";
    BytesUnit[BytesUnit["TB"] = 4] = "TB";
    BytesUnit[BytesUnit["PB"] = 5] = "PB";
})(BytesUnit = exports.BytesUnit || (exports.BytesUnit = {}));
;
var HertzUnit;
(function (HertzUnit) {
    HertzUnit[HertzUnit["Hz"] = 0] = "Hz";
    HertzUnit[HertzUnit["kHz"] = 1] = "kHz";
    HertzUnit[HertzUnit["MHz"] = 2] = "MHz";
    HertzUnit[HertzUnit["GHz"] = 3] = "GHz";
    HertzUnit[HertzUnit["THz"] = 4] = "THz";
})(HertzUnit = exports.HertzUnit || (exports.HertzUnit = {}));
;
/**
 * Helper for unit conversion
 */
var ConversionHelper = /** @class */ (function () {
    function ConversionHelper() {
    }
    /**
     * Converts value from defined unit to target unit, returns undefined if any of params is undefined
     */
    ConversionHelper.convertBytes = function (value, inputUnit, outputUnit) {
        if (_.isUndefined(inputUnit)) {
            throw new Error("InputUnit is not defined.");
        }
        if (_.isUndefined(outputUnit)) {
            throw new Error("OutputUnit is not defined.");
        }
        if (_.isUndefined(value) || _.isNull(value)) {
            return undefined;
        }
        if (inputUnit === outputUnit) {
            return value;
        }
        var exponent = inputUnit - outputUnit;
        return value * Math.pow(1024, Math.floor(exponent));
    };
    /**
     * Converts value from defined unit to target unit, returns undefined if any of params is undefined
     */
    ConversionHelper.convertHertz = function (value, inputUnit, outputUnit) {
        if (_.isUndefined(inputUnit)) {
            throw new Error("InputUnit is not defined.");
        }
        if (_.isUndefined(outputUnit)) {
            throw new Error("OutputUnit is not defined.");
        }
        if (_.isUndefined(value) || _.isNull(value)) {
            return undefined;
        }
        if (inputUnit === outputUnit) {
            return value;
        }
        var exponent = inputUnit - outputUnit;
        return value * Math.pow(1000, Math.floor(exponent));
    };
    return ConversionHelper;
}());
exports.ConversionHelper = ConversionHelper;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(24));


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * VIM module names
 *
 * @const
 */
exports.ModuleNames = {
    main: "vimCapacityPlanning",
    components: "vimCapacityPlanning.components",
    providers: "vimCapacityPlanning.providers",
    services: "vimCapacityPlanning.services"
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Determines what kind of profile should be used
 */
var VmProfileTypes;
(function (VmProfileTypes) {
    VmProfileTypes[VmProfileTypes["Existing"] = 1] = "Existing";
    VmProfileTypes[VmProfileTypes["Custom"] = 2] = "Custom";
})(VmProfileTypes = exports.VmProfileTypes || (exports.VmProfileTypes = {}));


/***/ }),
/* 23 */,
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var filterRegistrationSuffix = "Filter";
var ApolloFilterNames = /** @class */ (function () {
    function ApolloFilterNames() {
    }
    ApolloFilterNames.percentFilter = "swPercent";
    return ApolloFilterNames;
}());
exports.ApolloFilterNames = ApolloFilterNames;
;
var ApolloFilterFullNames = /** @class */ (function () {
    function ApolloFilterFullNames() {
    }
    ApolloFilterFullNames.percentFilter = ApolloFilterNames.percentFilter + filterRegistrationSuffix;
    return ApolloFilterFullNames;
}());
exports.ApolloFilterFullNames = ApolloFilterFullNames;
;
var FilterNames = /** @class */ (function () {
    function FilterNames() {
    }
    FilterNames.dateTimeFilter = "vimDateFormat";
    FilterNames.counterUnitFilter = "vimCounterFormat";
    FilterNames.bytesFormatFilter = "vimBytesFormat";
    FilterNames.numberFormatFilter = "vimNumberFormat";
    FilterNames.hertzFormatFilter = "vimHertzFormat";
    FilterNames.durationFormatFilter = "vimDurationFormat";
    return FilterNames;
}());
exports.FilterNames = FilterNames;
;
var FilterFullNames = /** @class */ (function () {
    function FilterFullNames() {
    }
    FilterFullNames.dateTimeFilter = FilterNames.dateTimeFilter + filterRegistrationSuffix;
    FilterFullNames.counterUnitFilter = FilterNames.counterUnitFilter + filterRegistrationSuffix;
    FilterFullNames.bytesFormatFilter = FilterNames.bytesFormatFilter + filterRegistrationSuffix;
    FilterFullNames.numberFormatFilter = FilterNames.numberFormatFilter + filterRegistrationSuffix;
    FilterFullNames.hertzFormatFilter = FilterNames.hertzFormatFilter + filterRegistrationSuffix;
    FilterFullNames.durationFormatFilter = FilterNames.durationFormatFilter + filterRegistrationSuffix;
    return FilterFullNames;
}());
exports.FilterFullNames = FilterFullNames;
;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(39));


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var ap = __webpack_require__(40);
var index_1 = __webpack_require__(3);
var helpers_1 = __webpack_require__(18);
/**
 * Default implementation of VM profile restrictions
 *
 * @const
 * @implements IVmProfileRestrictions
 */
exports.DefaultVmProfileRestrictions = {
    cpuCoreCountMin: 1,
    cpuCoreCountMax: 64,
    memoryCapacityMin: helpers_1.ConversionHelper.convertBytes(1, helpers_1.BytesUnit.GB, helpers_1.BytesUnit.B),
    memoryCapacityMax: helpers_1.ConversionHelper.convertBytes(128, helpers_1.BytesUnit.GB, helpers_1.BytesUnit.B),
    diskSpaceCapacityMin: helpers_1.ConversionHelper.convertBytes(1, helpers_1.BytesUnit.GB, helpers_1.BytesUnit.B),
    diskSpaceCapacityMax: helpers_1.ConversionHelper.convertBytes(10, helpers_1.BytesUnit.TB, helpers_1.BytesUnit.B),
    instanceCountMin: 0,
    instanceCountMax: 1000
};
/**
 * Implementation of VM profile interface to ensure proper value validation
 *
 * @class
 * @implements IVmProfileValidationWrapper
 */
var VmProfileValidationWrapper = /** @class */ (function () {
    function VmProfileValidationWrapper(profile, swUtil, _t, xuiToastService, counterUnitFilter, restrictions) {
        if (restrictions === void 0) { restrictions = exports.DefaultVmProfileRestrictions; }
        var _this = this;
        this.profile = profile;
        this.swUtil = swUtil;
        this._t = _t;
        this.xuiToastService = xuiToastService;
        this.counterUnitFilter = counterUnitFilter;
        this.restrictions = restrictions;
        this.onPropertyChanging = function (property, value) {
            if (property === "instanceCount") {
                return _this.ensureValidInteger(value, _this.restrictions.instanceCountMin, _this.swUtil.formatString(_this._t("Simulated instance value cannot be less than {0}."), _this.restrictions.instanceCountMin), _this.restrictions.instanceCountMax, _this.swUtil.formatString(_this._t("Simulated instance value cannot exceed {0}."), _this.restrictions.instanceCountMax));
            }
            return value;
        };
        this.onPropertyChanged = function (property, value) {
        };
        this.id = _.uniqueId();
        this.ensureValidInteger = function (value, min, minMessage, max, maxMessage) {
            var isInteger = _.isInteger(value);
            if (!isInteger || value < min) {
                _this.xuiToastService.warning(minMessage);
                return min;
            }
            else if (isInteger && value > max) {
                _this.xuiToastService.warning(maxMessage);
                return max;
            }
            return value;
        };
    }
    Object.defineProperty(VmProfileValidationWrapper.prototype, "name", {
        get: function () {
            return this.profile.name;
        },
        set: function (value) {
            this.profile.name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "cpuCores", {
        get: function () {
            return this.profile.counters[index_1.Counters.cpuCores].value;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.cpuCoreCountMin, this.swUtil.formatString(this._t("CPU core count value must be {0} or higher."), this.restrictions.cpuCoreCountMin), this.restrictions.cpuCoreCountMax, this.swUtil.formatString(this._t("CPU core count value cannot exceed {0}."), this.restrictions.cpuCoreCountMax));
            this.profile.counters[index_1.Counters.cpuCores].value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "memory", {
        get: function () {
            return this.profile.counters[index_1.Counters.memory].value;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.memoryCapacityMin, this.swUtil.formatString(this._t("Memory capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.memoryCapacityMin, "bytes")), this.restrictions.memoryCapacityMax, this.swUtil.formatString(this._t("Memory capacity value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.memoryCapacityMax, "bytes")));
            this.profile.counters[index_1.Counters.memory].value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "diskSpace", {
        get: function () {
            return this.profile.counters[index_1.Counters.disk].value;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.diskSpaceCapacityMin, this.swUtil.formatString(this._t("Disk space capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMin, "bytes")), this.restrictions.diskSpaceCapacityMax, this.swUtil.formatString(this._t("Disk space capacity value must be {0} or lower."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMax, "bytes")));
            this.profile.counters[index_1.Counters.disk].value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "virtualMachineId", {
        get: function () {
            return this.profile.virtualMachineId;
        },
        set: function (value) {
            this.profile.virtualMachineId = value;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        ap.AutoProperty("profile"),
        __metadata("design:type", Number)
    ], VmProfileValidationWrapper.prototype, "instanceCount", void 0);
    __decorate([
        ap.AutoProperty("profile"),
        __metadata("design:type", Object)
    ], VmProfileValidationWrapper.prototype, "counters", void 0);
    return VmProfileValidationWrapper;
}());
exports.VmProfileValidationWrapper = VmProfileValidationWrapper;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Route names used by capacity planning
 *
 * @const
 */
exports.RouteNames = {
    summaryStateName: "vimCapacityPlanningSummary",
    notLicensedErrorStateName: "vimCapacityPlanningNotLicensedError",
    reportWizardStateName: "vimCapacityPlanningReportWizard",
    reportDetailStateName: "vimCapacityPlanningReportDetail"
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Urls for routes used by VIM
 *
 * @const
 */
exports.RouteUrls = {
    summaryUrl: "/vim/capacity-planning",
    notLicensedUrl: "/vim/capacity-planning/not-licensed",
    reportWizardUrl: "/vim/capacity-planning/report-wizard/{id:[0-9]*}",
    reportDetailUrl: "/vim/capacity-planning/report-detail/{ids:[0-9]+(?:,[0-9]+)*}"
};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Report status
 *
 * @enum
 */
var ReportStatus;
(function (ReportStatus) {
    ReportStatus[ReportStatus["Created"] = 0] = "Created";
    ReportStatus[ReportStatus["Running"] = 1] = "Running";
    ReportStatus[ReportStatus["Finished"] = 2] = "Finished";
    ReportStatus[ReportStatus["Failed"] = 3] = "Failed";
    ReportStatus[ReportStatus["Aborted"] = 4] = "Aborted";
})(ReportStatus = exports.ReportStatus || (exports.ReportStatus = {}));
;
/**
 * Type of entity that is report created for
 *
 * @enum
 */
var ReportEntityType;
(function (ReportEntityType) {
    ReportEntityType[ReportEntityType["Cluster"] = 0] = "Cluster";
})(ReportEntityType = exports.ReportEntityType || (exports.ReportEntityType = {}));
;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Counter constants
 */
var Counters = /** @class */ (function () {
    function Counters() {
    }
    /**
     * Counter for virtual machines
     */
    Counters.virtualMachines = "virtualMachines";
    /**
     * Counter for hosts
     */
    Counters.hosts = "hosts";
    /**
     * Counter for CPU Cores
     */
    Counters.cpu = "cpu";
    /**
     * Counter for CPU Cores
     */
    Counters.cpuCores = "cpuCores";
    /**
     * Counter for CPU Clock
     */
    Counters.cpuClock = "cpuClock";
    /**
     * Counter for Memory
     */
    Counters.memory = "memory";
    /**
     * Counter for Disk
     */
    Counters.disk = "disk";
    return Counters;
}());
exports.Counters = Counters;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baseUrl = "vim/capacity-planning";
var isLicensedMethod = "is-licensed";
var getHelpUrlMethod = "get-help-url";
var deleteReportsMethod = "delete-reports";
/**
 * Backend urls used by capacity planning module
 *
 * @const
 */
exports.SwApiUrls = {
    baseUrl: baseUrl,
    isLicensedMethod: isLicensedMethod,
    isLicensedUrl: baseUrl + "/" + isLicensedMethod,
    getHelpUrl: baseUrl + "/" + getHelpUrlMethod,
    deleteReports: deleteReportsMethod
};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Determines typical resource load for counter
 *
 * @enum
 */
var ResourceLoad;
(function (ResourceLoad) {
    ResourceLoad[ResourceLoad["Low"] = 10] = "Low";
    ResourceLoad[ResourceLoad["Typical"] = 50] = "Typical";
    ResourceLoad[ResourceLoad["HighLoad"] = 85] = "HighLoad";
})(ResourceLoad = exports.ResourceLoad || (exports.ResourceLoad = {}));
;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Helper for working with json data format
 */
var JsonHelper = /** @class */ (function () {
    function JsonHelper() {
    }
    /**
     * Parses json string and creates object. If there are capitals, they are converted to lower case in order to keep typescript conventions
     */
    JsonHelper.fromJsonWithLowerCase = function (jsonString) {
        return JSON.parse(jsonString, function (key, value) {
            if (value && typeof value === "object") {
                for (var k in value) {
                    if (/^[A-Z]/.test(k) && Object.hasOwnProperty.call(value, k)) {
                        value[k.charAt(0).toLowerCase() + k.substring(1)] = value[k];
                        delete value[k];
                    }
                }
            }
            return value;
        });
    };
    return JsonHelper;
}());
exports.JsonHelper = JsonHelper;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var conversionHelper_1 = __webpack_require__(19);
;
/**
 * Default implementation of host profile restrictions
 *
 * @const
 * @implements IHostProfileRestrictions
 */
exports.DefaultHostProfileRestrictions = {
    cpuCoreCountMin: 1,
    cpuCoreCountMax: 64,
    /** 1 GHz */
    cpuCoreFrequencyMin: conversionHelper_1.ConversionHelper.convertHertz(1, conversionHelper_1.HertzUnit.GHz, conversionHelper_1.HertzUnit.Hz),
    /** 10 GHz */
    cpuCoreFrequencyMax: conversionHelper_1.ConversionHelper.convertHertz(10, conversionHelper_1.HertzUnit.GHz, conversionHelper_1.HertzUnit.Hz),
    /** 1 GB */
    memoryCapacityMin: conversionHelper_1.ConversionHelper.convertBytes(1, conversionHelper_1.BytesUnit.GB, conversionHelper_1.BytesUnit.B),
    /** 1 TB  */
    memoryCapacityMax: conversionHelper_1.ConversionHelper.convertBytes(1, conversionHelper_1.BytesUnit.TB, conversionHelper_1.BytesUnit.B),
    /** 1 TB */
    diskSpaceCapacityMin: conversionHelper_1.ConversionHelper.convertBytes(1, conversionHelper_1.BytesUnit.TB, conversionHelper_1.BytesUnit.B),
    /** 10 TB */
    diskSpaceCapacityMax: conversionHelper_1.ConversionHelper.convertBytes(10, conversionHelper_1.BytesUnit.TB, conversionHelper_1.BytesUnit.B),
    instanceCountMin: 0,
    instanceCountMax: 1000
};
;
/**
 * Implementation of host profile interface to ensure proper value validation
 *
 * @class
 * @implements IHostProfileValidationWrapper
 */
var HostProfileValidationWrapper = /** @class */ (function () {
    function HostProfileValidationWrapper(profile, swUtil, _t, xuiToastService, counterUnitFilter, restrictions) {
        if (restrictions === void 0) { restrictions = exports.DefaultHostProfileRestrictions; }
        var _this = this;
        this.profile = profile;
        this.swUtil = swUtil;
        this._t = _t;
        this.xuiToastService = xuiToastService;
        this.counterUnitFilter = counterUnitFilter;
        this.restrictions = restrictions;
        this.ensureValidInteger = function (value, min, minMessage, max, maxMessage) {
            if (_.isNumber(value) && _.round(value) !== value) {
                _this.xuiToastService.warning(_this._t("Decimal part is not allowed!"));
                return _.round(value);
            }
            return _this.checkMinMaxValues(value, min, minMessage, max, maxMessage);
        };
        this.ensureValidNumber = function (value, min, minMessage, max, maxMessage) {
            return _this.checkMinMaxValues(value, min, minMessage, max, maxMessage);
        };
        this.checkMinMaxValues = function (value, min, minMessage, max, maxMessage) {
            if (!_.isFinite(value)) {
                _this.xuiToastService.warning(_this._t("Number is not valid!"));
                return min;
            }
            else if (value < min) {
                _this.xuiToastService.warning(minMessage);
                return min;
            }
            else if (value > max) {
                _this.xuiToastService.warning(maxMessage);
                return max;
            }
            return value;
        };
    }
    Object.defineProperty(HostProfileValidationWrapper.prototype, "name", {
        get: function () {
            return this.profile.name;
        },
        set: function (value) {
            this.profile.name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "hostId", {
        get: function () {
            return this.profile.hostId;
        },
        set: function (value) {
            this.profile.hostId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "cpuCoreCount", {
        get: function () {
            return this.profile.cpuCoreCount;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.cpuCoreCountMin, this.swUtil.formatString(this._t("CPU core count value must be {0} or higher."), this.restrictions.cpuCoreCountMin), this.restrictions.cpuCoreCountMax, this.swUtil.formatString(this._t("CPU core count value cannot exceed {0}."), this.restrictions.cpuCoreCountMax));
            this.profile.cpuCoreCount = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "cpuCoreFrequency", {
        get: function () {
            return this.profile.cpuCoreFrequency;
        },
        set: function (value) {
            value = this.ensureValidNumber(value, this.restrictions.cpuCoreFrequencyMin, this.swUtil.formatString(this._t("CPU core frequency value must be {0} or higher."), this.counterUnitFilter(this.restrictions.cpuCoreFrequencyMax, "clock")), this.restrictions.cpuCoreFrequencyMax, this.swUtil.formatString(this._t("CPU core frequency value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.cpuCoreFrequencyMax, "clock")));
            this.profile.cpuCoreFrequency = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "memoryCapacity", {
        get: function () {
            return this.profile.memoryCapacity;
        },
        set: function (value) {
            value = this.ensureValidNumber(value, this.restrictions.memoryCapacityMin, this.swUtil.formatString(this._t("Memory capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.memoryCapacityMin, "bytes")), this.restrictions.memoryCapacityMax, this.swUtil.formatString(this._t("Memory capacity value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.memoryCapacityMax, "bytes")));
            this.profile.memoryCapacity = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "diskSpaceCapacity", {
        get: function () {
            return this.profile.diskSpaceCapacity;
        },
        set: function (value) {
            value = this.ensureValidNumber(value, this.restrictions.diskSpaceCapacityMin, this.swUtil.formatString(this._t("Disk space capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMin, "bytes")), this.restrictions.diskSpaceCapacityMax, this.swUtil.formatString(this._t("Disk space capacity value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMax, "bytes")));
            this.profile.diskSpaceCapacity = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "instanceCount", {
        get: function () {
            return this.profile.instanceCount;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.instanceCountMin, this.swUtil.formatString(this._t("Simulated instance value cannot be less than {0}."), this.restrictions.instanceCountMin), this.restrictions.instanceCountMax, this.swUtil.formatString(this._t("Simulated instance value cannot exceed {0}."), this.restrictions.instanceCountMax));
            this.profile.instanceCount = value;
        },
        enumerable: true,
        configurable: true
    });
    return HostProfileValidationWrapper;
}());
exports.HostProfileValidationWrapper = HostProfileValidationWrapper;
;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(36));


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
* Ids of filtering options
*/
var ReportsFilterPropertyNames = /** @class */ (function () {
    function ReportsFilterPropertyNames() {
    }
    ReportsFilterPropertyNames.entityPropertyName = "entity";
    ReportsFilterPropertyNames.parameterPropertyName = "parameter";
    return ReportsFilterPropertyNames;
}());
exports.ReportsFilterPropertyNames = ReportsFilterPropertyNames;
/**
* Ids used in report list filtering by parameters
*/
var ReportsFilterParameterNames = /** @class */ (function () {
    function ReportsFilterParameterNames() {
    }
    ReportsFilterParameterNames.simulatedResourcesEnabled = "simulatedResourcesEnabled";
    ReportsFilterParameterNames.simulatedWorkloadsEnabled = "simulatedWorkloadsEnabled";
    return ReportsFilterParameterNames;
}());
exports.ReportsFilterParameterNames = ReportsFilterParameterNames;
;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Default implementation for report definition interface
 *
 * @class
 * @implements IReportDefinition
 */
var ReportDefinition = /** @class */ (function () {
    function ReportDefinition() {
        this.computedEntities = [];
        this.simulatedVmProfiles = [];
        this.simulatedHostProfiles = [];
    }
    return ReportDefinition;
}());
exports.ReportDefinition = ReportDefinition;
;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Determines how much the utilization data is filtered
 *
 * @enum
 */
var ResourceAllocationType;
(function (ResourceAllocationType) {
    /**
     * No filtering used on utilization
     */
    ResourceAllocationType[ResourceAllocationType["Conservative"] = 0] = "Conservative";
    /**
     * 95th percentile is used to filter peaks
     */
    ResourceAllocationType[ResourceAllocationType["Balanced"] = 1] = "Balanced";
    /**
     * 75th percentile is used to filter peaks
     */
    ResourceAllocationType[ResourceAllocationType["HighlyOptimized"] = 2] = "HighlyOptimized";
})(ResourceAllocationType = exports.ResourceAllocationType || (exports.ResourceAllocationType = {}));


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Default implementation for report detail interface
 *
 * @class
 * @implements IReportDetail
 */
var ReportDetail = /** @class */ (function () {
    function ReportDetail() {
    }
    return ReportDetail;
}());
exports.ReportDetail = ReportDetail;
;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Generates get/set methods for a property
 * @param {string} [storageObject] Name of a backend property to store the real value
 */
function AutoProperty(storageObject) {
    return function (target, name) {
        var variableName = storageObject ? name : "_" + name;
        Object.defineProperty(target, name, {
            get: function () {
                if (storageObject) {
                    return this[storageObject][variableName];
                }
                return this[variableName];
            },
            set: function (value) {
                // on changing
                if (_.isFunction(this["onPropertyChanging"])) {
                    var handler = this["onPropertyChanging"];
                    value = handler(name, value);
                }
                if (storageObject) {
                    this[storageObject][variableName] = value;
                }
                else {
                    this[variableName] = value;
                }
                // on changed
                if (_.isFunction(this["onPropertyChanged"])) {
                    var handler = this["onPropertyChanged"];
                    handler(name, value);
                }
            },
            enumerable: true,
            configurable: true
        });
    };
}
exports.AutoProperty = AutoProperty;
;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
* Common Helper for entities
*/
var EntityHelper = /** @class */ (function () {
    function EntityHelper() {
    }
    /**
    * Extract numeric instance id from NetObjectId
    * @example "VC:1" => 1
    */
    EntityHelper.parseInstanceId = function (entityNetObjectId) {
        if (_.isUndefined(entityNetObjectId)) {
            throw new Error("EntityNetObjectId needs to be specified");
        }
        var fragments = _.split(entityNetObjectId, ":");
        if (fragments.length !== 2) {
            throw new Error("Invalid entityNetObjectId");
        }
        return _.parseInt(fragments[1]);
    };
    return EntityHelper;
}());
exports.EntityHelper = EntityHelper;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Holder for app-specific event names
 *
 * @class
 */
var EventNames = /** @class */ (function () {
    function EventNames() {
    }
    EventNames.prefix = "vim.cp";
    EventNames.reportResultChanged = EventNames.prefix + ".reportResultChanged";
    return EventNames;
}());
exports.EventNames = EventNames;


/***/ }),
/* 43 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 44 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of all registered providers
 *
 * @class
 */
var ProviderNames = /** @class */ (function () {
    function ProviderNames() {
    }
    ProviderNames.routingRegistrationProvider = "vimCapacityPlanningRoutingRegistrationProvider";
    return ProviderNames;
}());
exports.ProviderNames = ProviderNames;


/***/ }),
/* 46 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 47 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 48 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 49 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 50 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 51 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 52 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 53 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 54 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 55 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 56 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 57 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 58 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 59 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 60 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 61 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(44);
module.exports = __webpack_require__(63);


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(64);
__webpack_require__(65);
var index_1 = __webpack_require__(226);
var index_2 = __webpack_require__(235);
var index_3 = __webpack_require__(236);
var vimModule = app_1.default();
index_1.default(vimModule);
index_2.default(vimModule);
index_3.default(vimModule);
exports.default = vimModule;


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var moduleNames_1 = __webpack_require__(5);
var moduleNames_2 = __webpack_require__(21);
/**
 * Helper function indicating whether code is currently under test
 */
exports.isUnderTest = function () {
    return !!angular.mock;
};
/** @ngInject */
var run = function ($log) {
    $log.info("Run module: VIM");
};
run.$inject = ["$log"];
exports.default = function () {
    angular.module(moduleNames_1.ModuleNames.providers, []);
    angular.module(moduleNames_1.ModuleNames.services, []);
    angular.module(moduleNames_1.ModuleNames.filters, []);
    angular.module(moduleNames_1.ModuleNames.main, [
        "ui.router",
        "orion",
        "orion-ui-components",
        "sw-charts",
        moduleNames_1.ModuleNames.providers,
        moduleNames_1.ModuleNames.services,
        moduleNames_1.ModuleNames.filters,
        moduleNames_2.ModuleNames.main
    ]);
    var module = Xui.registerModule(moduleNames_1.ModuleNames.main);
    module.app().run(run);
    return module;
};


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(66);


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(67);
var index_1 = __webpack_require__(68);
var index_2 = __webpack_require__(70);
var index_3 = __webpack_require__(209);
var index_4 = __webpack_require__(219);
var vimModule = app_1.default();
index_1.default(vimModule);
index_2.default(vimModule);
index_3.default(vimModule);
index_4.default(vimModule);


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var moduleNames_1 = __webpack_require__(5);
var moduleNames_2 = __webpack_require__(21);
exports.default = function () {
    angular.module(moduleNames_2.ModuleNames.components, []);
    angular.module(moduleNames_2.ModuleNames.providers, []);
    angular.module(moduleNames_2.ModuleNames.services, []);
    angular.module(moduleNames_2.ModuleNames.main, [
        "ui.router",
        "orion",
        "orion-ui-components",
        "filtered-list",
        moduleNames_1.ModuleNames.filters,
        moduleNames_2.ModuleNames.components,
        moduleNames_2.ModuleNames.providers,
        moduleNames_2.ModuleNames.services,
    ]);
    var module = Xui.registerModule(moduleNames_2.ModuleNames.main);
    return module;
};


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var routes_1 = __webpack_require__(69);
exports.default = function (module) {
    routes_1.default(module);
};


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(45);
/**
 * Configures basic routing settings on config event
 *
 * @function
 */
var configureRouting = function (provider) {
    provider.registerRoutes();
};
exports.default = function (module) {
    module.app().config([index_1.ProviderNames.routingRegistrationProvider, configureRouting]);
};


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(71);
var index_2 = __webpack_require__(76);
var index_3 = __webpack_require__(154);
var index_4 = __webpack_require__(194);
var index_5 = __webpack_require__(198);
var index_6 = __webpack_require__(202);
var wizardStepToggle_1 = __webpack_require__(206);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
    wizardStepToggle_1.default(module);
};


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportList_directive_1 = __webpack_require__(72);
exports.default = function (module) {
    module.component("vimCpReportList", reportList_directive_1.ReportListDirective);
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(46);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var reportList_controller_1 = __webpack_require__(73);
/**
 * Directive which displays list of reports
 */
var ReportListDirective = /** @class */ (function () {
    function ReportListDirective() {
        this.restrict = "E";
        this.replace = false;
        this.transclude = false;
        this.template = __webpack_require__(75);
        this.controller = reportList_controller_1.ReportListController;
        this.controllerAs = "vm";
    }
    return ReportListDirective;
}());
exports.ReportListDirective = ReportListDirective;
;


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(24);
var index_2 = __webpack_require__(12);
var index_3 = __webpack_require__(1);
var icons_1 = __webpack_require__(74);
/**
 * Controller for summary report list
 *
 * @class
 */
var ReportListController = /** @class */ (function () {
    function ReportListController($scope, $timeout, $sanitize, _t, dateTimeService, dialogService, toastService, dateFilter, routingService, reportListService) {
        var _this = this;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.$sanitize = $sanitize;
        this._t = _t;
        this.dateTimeService = dateTimeService;
        this.dialogService = dialogService;
        this.toastService = toastService;
        this.dateFilter = dateFilter;
        this.routingService = routingService;
        this.reportListService = reportListService;
        this.clusterIcons = icons_1.ClusterIcons;
        this.remoteControl = {};
        this.openReportWizard = function () {
            _this.routingService.goToReportWizard();
        };
        this.openReportDetail = function () {
            if (_.isEmpty(_this.selectedReports.items)) {
                return;
            }
            var reportIds = _this.selectedReports.items.map(function (id) { return id; });
            if (reportIds) {
                _this.routingService.goToReportDetail(reportIds);
            }
        };
        this.openDeleteDialog = function () {
            return _this.dialogService.showModal({
                size: "sm"
            }, {
                title: _this._t("Delete report(s)"),
                hideCancel: false,
                message: _this._t("Are you sure you want to delete the selected report(s)?"),
                actionButtonText: _this._t("Delete"),
                cancelButtonText: _this._t("Cancel")
            })
                .then(function (dialogResult) {
                if (dialogResult && dialogResult !== "cancel") {
                    _this.deleteReports();
                }
            });
        };
        this.$onInit = function () {
            _this.initProperties();
            _this.clearSelectedReports();
            _this.$scope.$on("$destroy", function () {
                _this.reportListService.saveFilterValues(_this.filterValues);
            });
            _this.filterValues = _this.reportListService.getFilterValues();
        };
        this.isReportDetailButtonVisible = function () {
            return _this.selectedReports.items.length >= 1;
        };
        this.isReportDetailButtonDisabled = function () {
            if (!_this.isReportDetailButtonVisible()) {
                return true;
            }
            var _loop_1 = function (selectedReportId) {
                var report = _.find(_this.reports, function (r) { return r.id === selectedReportId; });
                if (!report) {
                    return "continue";
                }
                if (!_this.isReportFinished(report)) {
                    return { value: true };
                }
            };
            for (var _i = 0, _a = _this.selectedReports.items; _i < _a.length; _i++) {
                var selectedReportId = _a[_i];
                var state_1 = _loop_1(selectedReportId);
                if (typeof state_1 === "object")
                    return state_1.value;
            }
            return false;
        };
        this.isDeleteReportButtonVisible = function () {
            return _this.selectedReports.items.length >= 1;
        };
        this.isReportFinished = function (report) {
            return report && moment.isMoment(report.computationDateFinished);
        };
        /**
         * Custom function for getting current reports data
         */
        this.getData = function (filters, pagination, sorting, searching) {
            _this.searchTerm = searching;
            var filter = _this.getReportsFilter(filters, pagination, sorting, searching);
            return _this.reportListService.getReports(filter)
                .then(function (result) {
                // TODO: UIF-3718 
                $(".xui-grid__listview-panel").addClass("xui-edge-definer");
                _this.reports = result.rows;
                _this.pagination.total = result.total;
                // autorefresh if there is running report
                if (_.some(_this.reports, function (report) { return !_this.isReportFinished(report); })) {
                    _this.$timeout(_this.remoteControl.refreshListData, 5000);
                }
            });
        };
        this.getFilterProperties = function () {
            return _this.reportListService.getReportsFilterStats().then(function (stats) {
                var filter = [
                    _this.createClustersFilterProperty(stats),
                    _this.createParametersFilterProperty(stats)
                ];
                return filter;
            });
        };
        this.getComputationDateString = function (report) {
            return _this.dateFilter(report.computationDateStarted, "shortDate");
        };
        this.getProjectionDateString = function (report) {
            if (!report.computationDateStarted) {
                return _this.dateFilter(undefined, "shortDate");
            }
            var projectionDate = moment(report.computationDateStarted).add(report.runway, "day");
            return _this.dateFilter(projectionDate, "shortDate");
        };
        this.clearSelectedReports = function () {
            _this.selectedReports = {
                items: [],
                blacklist: false
            };
        };
        this.createClustersFilterProperty = function (filterStats) {
            var filterStatsGrouped = _.groupBy(filterStats, function (st) { return st.entityNetObjectId; });
            var filterValues = _.map(filterStatsGrouped, function (clusterStats, clusterNetObjectId) {
                return { id: clusterNetObjectId, title: clusterStats[0].entityCaption, count: clusterStats.length };
            });
            return { refId: index_2.ReportsFilterPropertyNames.entityPropertyName, title: _this._t("Cluster"), values: filterValues };
        };
        this.createParametersFilterProperty = function (filterStats) {
            var filterValues = [
                {
                    id: index_2.ReportsFilterParameterNames.simulatedResourcesEnabled,
                    title: _this._t("Simulated Hosts"),
                    count: _.filter(filterStats, function (stat) { return stat.simulatedResourcesEnabled; }).length
                },
                {
                    id: index_2.ReportsFilterParameterNames.simulatedWorkloadsEnabled,
                    title: _this._t("Simulated VMs"),
                    count: _.filter(filterStats, function (stat) { return stat.simulatedWorkloadsEnabled; }).length
                }
            ];
            return { refId: index_2.ReportsFilterPropertyNames.parameterPropertyName, title: _this._t("Parameters"), values: filterValues };
        };
        this.getReportsFilter = function (filters, pagination, sorting, search) {
            var filter = {
                filters: filters,
                pagination: pagination,
                sorting: sorting,
                search: search
            };
            return filter;
        };
        this.deleteReports = function () {
            if (_.isEmpty(_this.selectedReports.items)) {
                return;
            }
            _this.reportListService.deleteReports(_this.selectedReports.items)
                .then(function () {
                _this.toastService.success(_this._t("Report(s) deleted"));
                _this.remoteControl.refreshListData();
                _this.clearSelectedReports();
            });
        };
    }
    ReportListController.prototype.initProperties = function () {
        this.reports = [];
        this.sortByName = { id: "Name", label: this._t("Name") };
        this.sortByCluster = { id: "EntityCaption", label: this._t("Cluster") };
        this.sortByCreationTime = { id: "ComputationDateFinished", label: this._t("Creation Time") };
        this.options = {
            selectionMode: "multi",
            selectionProperty: "id",
            pageSize: 10,
            showAddRemoveFilterProperty: false,
            emptyText: this._t("No reports available."),
            showMorePropertyValuesThreshold: 10,
            hideSearch: false,
            triggerSearchOnChange: true,
            searchDebounce: 500,
            allowSelectAllPages: false,
            templateUrl: "report-template",
            sortableColumns: [this.sortByCreationTime, this.sortByName, this.sortByCluster]
        };
        this.sorting = {
            sortBy: this.sortByCreationTime,
            direction: "desc"
        };
    };
    ;
    ReportListController = __decorate([
        __param(0, di_1.Inject(index_3.NgServiceNames.ngScope)),
        __param(1, di_1.Inject(index_3.NgServiceNames.ngTimeoutService)),
        __param(2, di_1.Inject(index_3.NgServiceNames.ngSanitizeService)),
        __param(3, di_1.Inject(index_3.ApolloServiceNames.getTextService)),
        __param(4, di_1.Inject(index_3.ApolloServiceNames.dateTimeService)),
        __param(5, di_1.Inject(index_3.ApolloServiceNames.dialogService)),
        __param(6, di_1.Inject(index_3.ApolloServiceNames.toastService)),
        __param(7, di_1.Inject(index_1.FilterFullNames.dateTimeFilter)),
        __param(8, di_1.Inject(index_3.ServiceNames.routingService)),
        __param(9, di_1.Inject(index_3.ServiceNames.reportListService)),
        __metadata("design:paramtypes", [Object, Function, Function, Function, Object, Object, Object, Function, Object, Object])
    ], ReportListController);
    return ReportListController;
}());
exports.ReportListController = ReportListController;
;


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/**
 * Icon mapping
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClusterIcons = {
    "VMware": "vmwarecluster",
    "Hyper-V": "hypervcluster",
    "Unknown": "virtual-cluster"
};


/***/ }),
/* 75 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-list> <xui-filtered-list items-source=vm.reports on-refresh=\"vm.getData(filters, pagination, sorting, searching)\" options=vm.options filter-properties-fn=vm.getFilterProperties() filter-values=vm.filterValues selection=vm.selectedReports pagination-data=::vm.pagination sorting=::vm.sorting remote-control=vm.remoteControl controller=vm> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button class=vim-cp-report-list-add-btn icon=add display-style=tertiary ng-click=vm.openReportWizard() data-selector=add-btn _t> New </xui-button> <xui-button ng-if=vm.isReportDetailButtonVisible() is-disabled=vm.isReportDetailButtonDisabled() icon=details display-style=tertiary ng-click=vm.openReportDetail() data-selector=view-report-btn _t> View Report(s) </xui-button> <xui-button ng-if=vm.isDeleteReportButtonVisible() icon=delete display-style=tertiary ng-click=vm.openDeleteDialog() data-selector=delete-btn _t> Delete </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list> <script type=text/ng-template id=report-template> <div class=\"report\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 title\" data-selector=\"report-name\">\n                    <span ng-bind-html=\"vm.$sanitize(item.name) | xuiHighlight:vm.searchTerm\"></span>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-xs-12 subtitle\">\n                    <div class=\"col-xs-6\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-2 no-left-padding\">\n                                {{::item.computationDateFinished | vimDateFormat: 'shortDate'}}\n                            </div>\n                            <div class=\"col-xs-3\" data-selector=\"entity-name\">\n                                <xui-icon icon=\"{{::vm.clusterIcons[item.entityPlatform || 'Unknown']}}\" status=\"{{::item.entityStatus}}\" icon-size=\"small\"></xui-icon>\n                                <span ng-bind-html=\"vm.$sanitize(item.entityCaption) | xuiHighlight:vm.searchTerm\"></span>\n                            </div>\n                            <div class=\"col-xs-2\">\n                                <span ng-if=\"::item.simulatedResourcesEnabled\" data-selector=\"simulated-resources\" _t>+ Sim Hosts</span>\n                            </div>\n                            <div class=\"col-xs-2\">\n                                <span ng-if=\"::item.simulatedWorkloadsEnabled\" data-selector=\"simulated-workloads\" _t>+ Sim VMs</span>\n                            </div>\n                            <div class=\"col-xs-3\">\n                                <div>\n                                    <div _t=\"::[vm.getComputationDateString(item)]\"\n                                         data-selector=\"computation-date\">\n                                         Run: {0}\n                                    </div>\n                                    <div _t=\"::[vm.getProjectionDateString(item)]\"\n                                         data-selector=\"projection-date\">\n                                         Projection: {0}\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportWizard_directive_1 = __webpack_require__(77);
var index_1 = __webpack_require__(80);
exports.default = function (module) {
    module.component("vimCpReportWizard", reportWizard_directive_1.ReportWizardDirective);
    index_1.default(module);
};


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(47);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var reportWizard_controller_1 = __webpack_require__(78);
/**
 * Directive wrapper for report wizard
 */
var ReportWizardDirective = /** @class */ (function () {
    function ReportWizardDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(79);
        this.controller = reportWizard_controller_1.ReportWizardController;
        this.controllerAs = "vm";
        this.bindToController = {
            isNew: "<",
            report: "<"
        };
        this.scope = {};
    }
    return ReportWizardDirective;
}());
exports.ReportWizardDirective = ReportWizardDirective;
;


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
/**
 * Controller for whole wizard directive
 */
var ReportWizardController = /** @class */ (function () {
    function ReportWizardController($q, $scope, _t, toastService, reportWizardService, routingService) {
        var _this = this;
        this.$q = $q;
        this.$scope = $scope;
        this._t = _t;
        this.toastService = toastService;
        this.reportWizardService = reportWizardService;
        this.routingService = routingService;
        this.currentStepIndex = 0;
        this.taskSelectionStep = {
            label: "taskSelectionStep",
            shortTitle: this._t("Simulation Mode"),
            templateUrl: "report-task-selection-step"
        };
        this.timeFrameStep = {
            label: "timeFrameStep",
            shortTitle: this._t("Time Frame"),
            templateUrl: "report-time-frame-step"
        };
        this.entitySelectionStep = {
            label: "entitySelectionStep",
            shortTitle: this._t("Pick Cluster"),
            templateUrl: "report-entity-selection-step"
        };
        this.vmProfilesStep = {
            label: "vmProfilesStep",
            shortTitle: this._t("VM Profiles"),
            templateUrl: "report-vm-profiles-step"
        };
        this.hostProfileStep = {
            label: "hostProfileStep",
            shortTitle: this._t("Host Profile"),
            templateUrl: "report-host-profile-step"
        };
        this.pickResourcesStep = {
            label: "pickResourcesStep",
            shortTitle: this._t("Resources"),
            templateUrl: "report-pick-resources-step"
        };
        this.historicalAnalysisStep = {
            label: "historicalAnalysisStep",
            shortTitle: this._t("Usage History"),
            templateUrl: "report-historical-analysis-step"
        };
        this.advancedSettingsStep = {
            label: "advancedSettingsStep",
            shortTitle: this._t("Advanced"),
            templateUrl: "report-advanced-settings-step"
        };
        this.finishStep = {
            label: "finishStep",
            shortTitle: this._t("Summary"),
            templateUrl: "report-finish-step"
        };
        this.steps = [];
        this.$onInit = function () {
            // init remote control here because step directive cannot set whole instance of it
            _this.resetRemoteControl();
            _this.updateSteps();
        };
        this.updateSteps = function () {
            _this.steps = _this.getRelevantSteps();
        };
        this.getRelevantSteps = function () {
            var steps = [_this.taskSelectionStep, _this.timeFrameStep, _this.entitySelectionStep];
            if (_this.report.simulatedWorkloadsEnabled) {
                steps.push(_this.vmProfilesStep);
            }
            if (_this.report.simulatedResourcesEnabled) {
                steps.push(_this.hostProfileStep);
            }
            steps.push(_this.pickResourcesStep, _this.historicalAnalysisStep, _this.advancedSettingsStep, _this.finishStep);
            return steps;
        };
        this.validateOnNext = function () {
            return _this.$q.resolve(_this.validateCurrentStep());
        };
        this.canFinish = function () {
            return _this.isLastStep() && _this.validateCurrentStep();
        };
        this.onFinish = function () {
            if (!_this.validateCurrentStep()) {
                return _this.$q.resolve(false);
            }
            // clear all current toasts
            _this.toastService.clear();
            if (_this.isNew) {
                _this.report.dateCreated = moment().utc();
                var promises = [];
                for (var _i = 0, _a = _this.report.computedEntities; _i < _a.length; _i++) {
                    var computed = _a[_i];
                    var thisReport = _.cloneDeep(_this.report);
                    thisReport.counters = computed.counters;
                    thisReport.entityCaption = computed.entityCaption;
                    thisReport.entityNetObjectId = computed.entityNetObjectId;
                    thisReport.name = thisReport.name;
                    var promise = _this.reportWizardService.processNewReportDefinition(thisReport);
                    promises.push(promise);
                }
                return _this.$q.all(promises).then(function () {
                    _this.routingService.goToReportList();
                    return true;
                })
                    .catch(function () {
                    return false;
                });
            }
            // todo: implement update
            throw new Error("Not supported.");
        };
        this.onCancel = function () {
            _this.routingService.goToReportList();
            return true;
        };
        this.onExitStep = function () {
            if (_.isFunction(_this.currentStepRemoteControl.save)) {
                _this.currentStepRemoteControl.save();
            }
            _this.resetRemoteControl();
        };
        this.isLastStep = function () {
            return _this.currentStepIndex === (_this.steps.length - 1);
        };
        this.validateCurrentStep = function (showToast) {
            if (showToast === void 0) { showToast = false; }
            // check if supported
            if (!angular.isFunction(_this.currentStepRemoteControl.isValid)) {
                return true;
            }
            var validationResult = _this.currentStepRemoteControl.isValid();
            if (showToast && !validationResult.isValid) {
                var message = validationResult.errors.join("<br/>");
                _this.toastService.error(message);
            }
            return validationResult.isValid;
        };
        this.resetRemoteControl = function () {
            _this.currentStepRemoteControl = {
                isValid: undefined,
                save: undefined
            };
        };
    }
    Object.defineProperty(ReportWizardController.prototype, "simulatedWorkloadsEnabled", {
        get: function () {
            return this.report.simulatedWorkloadsEnabled;
        },
        set: function (value) {
            this.report.simulatedWorkloadsEnabled = value;
            this.updateSteps();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ReportWizardController.prototype, "simulatedResourcesEnabled", {
        get: function () {
            return this.report.simulatedResourcesEnabled;
        },
        set: function (value) {
            this.report.simulatedResourcesEnabled = value;
            this.updateSteps();
        },
        enumerable: true,
        configurable: true
    });
    ReportWizardController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.NgServiceNames.ngScope)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(3, di_1.Inject(index_1.ApolloServiceNames.toastService)),
        __param(4, di_1.Inject(index_1.ServiceNames.reportWizardService)),
        __param(5, di_1.Inject(index_1.ServiceNames.routingService)),
        __metadata("design:paramtypes", [Function, Object, Function, Object, Object, Object])
    ], ReportWizardController);
    return ReportWizardController;
}());
exports.ReportWizardController = ReportWizardController;
;


/***/ }),
/* 79 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard ng-form=reportWizard> <xui-wizard name=reportWizard ng-model=vm current-step-index=vm.currentStepIndex finish-text=\"_t(Generate report(s))\" on-next-step=vm.validateOnNext() on-finish=vm.onFinish() on-cancel=vm.onCancel() on-exit-step=vm.onExitStep() _ta> <xui-wizard-step ng-repeat=\"step in vm.steps track by step.label\" label={{::step.label}} short-title={{::step.shortTitle}} class=vim-cp-report-wizard-step> <ng-include src=::step.templateUrl></ng-include> </xui-wizard-step> </xui-wizard> <script type=text/ng-template id=report-task-selection-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-task-selection-step \n                simulated-workloads-enabled=\"vm.simulatedWorkloadsEnabled\"\n                simulated-resources-enabled=\"vm.simulatedResourcesEnabled\">\n            </vim-cp-report-wizard-task-selection-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-time-frame-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-time-frame-step \n                remote-control=\"vm.currentStepRemoteControl\"\n                runway=\"vm.report.runway\">\n            </vim-cp-report-wizard-time-frame-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-entity-selection-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-entity-selection-step \n                remote-control=\"vm.currentStepRemoteControl\"\n                computed-entities=\"vm.report.computedEntities\">\n            </vim-cp-report-wizard-entity-selection-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-vm-profiles-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-vm-profiles-step \n                vm-profiles=\"vm.report.simulatedVmProfiles\"\n                remote-control=\"vm.currentStepRemoteControl\"\n                computed-entities=\"vm.report.computedEntities\">\n            </vim-cp-report-wizard-vm-profiles-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-host-profile-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-host-profile-step \n                remote-control=\"vm.currentStepRemoteControl\"\n                host-profiles=\"vm.report.simulatedHostProfiles\"\n                computed-entities=\"vm.report.computedEntities\">\n            </vim-cp-report-wizard-host-profile-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-pick-resources-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-pick-resources-step \n                remote-control=\"vm.currentStepRemoteControl\"\n                counters=\"vm.report.counters\"\n                computed-entities=\"vm.report.computedEntities\">\n            </vim-cp-report-wizard-pick-resources-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-historical-analysis-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-historical-analysis-step \n                utilization-period-in-days=\"vm.report.utilizationPeriodInDays\">\n            </vim-cp-report-wizard-historical-analysis-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-advanced-settings-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-advanced-settings-step \n                remote-control=\"vm.currentStepRemoteControl\"\n                resource-allocation-type=\"vm.report.resourceAllocationType\"\n                failover-reservation=\"vm.report.failoverReservation\">\n            </vim-cp-report-wizard-advanced-settings-step>\n        </vim-cp-wizard-step-toggle> </script> <script type=text/ng-template id=report-finish-step> <vim-cp-wizard-step-toggle>\n            <vim-cp-report-wizard-finish-step \n                remote-control=\"vm.currentStepRemoteControl\"\n                report-name=\"vm.report.name\">\n            </vim-cp-report-wizard-finish-step>\n        </vim-cp-wizard-step-toggle> </script> </div> ";

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(81);
var index_2 = __webpack_require__(85);
var index_3 = __webpack_require__(89);
var index_4 = __webpack_require__(93);
var index_5 = __webpack_require__(109);
var index_6 = __webpack_require__(113);
var index_7 = __webpack_require__(117);
var index_8 = __webpack_require__(129);
var index_9 = __webpack_require__(133);
var index_10 = __webpack_require__(137);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_4.default(module);
    index_3.default(module);
    index_5.default(module);
    index_6.default(module);
    index_7.default(module);
    index_8.default(module);
    index_9.default(module);
    index_10.default(module);
};


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var taskSelectionStep_directive_1 = __webpack_require__(82);
exports.default = function (module) {
    module.component("vimCpReportWizardTaskSelectionStep", taskSelectionStep_directive_1.TaskSelectionStep);
};


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var taskSelectionStep_controller_1 = __webpack_require__(83);
/**
 * Directive wrapper for report wizard / start step
 */
var TaskSelectionStep = /** @class */ (function () {
    function TaskSelectionStep() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(84);
        this.controller = taskSelectionStep_controller_1.TaskSelectionStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            simulatedWorkloadsEnabled: "=",
            simulatedResourcesEnabled: "="
        };
        this.scope = {};
    }
    return TaskSelectionStep;
}());
exports.TaskSelectionStep = TaskSelectionStep;
;


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Determines if simulation is performed on current environment (CheckUp) or simulated resources/workloads are added
 */
var SimulationMode;
(function (SimulationMode) {
    SimulationMode[SimulationMode["CheckUp"] = 1] = "CheckUp";
    SimulationMode[SimulationMode["AddWorkloads"] = 2] = "AddWorkloads";
    SimulationMode[SimulationMode["AddResources"] = 3] = "AddResources";
    SimulationMode[SimulationMode["AddWorkloadsAndResources"] = 4] = "AddWorkloadsAndResources";
})(SimulationMode = exports.SimulationMode || (exports.SimulationMode = {}));
;
/**
 * Controller for selecting type of simulation
 */
var TaskSelectionStepController = /** @class */ (function () {
    function TaskSelectionStepController() {
        var _this = this;
        this.simulationModeEnum = SimulationMode;
        this.getSimulationModeFromFlags = function () {
            if (_this.simulatedResourcesEnabled && _this.simulatedWorkloadsEnabled) {
                return SimulationMode.AddWorkloadsAndResources;
            }
            else if (_this.simulatedResourcesEnabled) {
                return SimulationMode.AddResources;
            }
            else if (_this.simulatedWorkloadsEnabled) {
                return SimulationMode.AddWorkloads;
            }
            return SimulationMode.CheckUp;
        };
        this.updateFlags = function (simulationMode) {
            _this.simulatedResourcesEnabled = false;
            _this.simulatedWorkloadsEnabled = false;
            switch (simulationMode) {
                case SimulationMode.AddResources:
                    _this.simulatedResourcesEnabled = true;
                    break;
                case SimulationMode.AddWorkloads:
                    _this.simulatedWorkloadsEnabled = true;
                    break;
                case SimulationMode.AddWorkloadsAndResources:
                    _this.simulatedResourcesEnabled = true;
                    _this.simulatedWorkloadsEnabled = true;
                    break;
            }
            ;
        };
    }
    Object.defineProperty(TaskSelectionStepController.prototype, "simulationMode", {
        get: function () {
            return this.getSimulationModeFromFlags();
        },
        set: function (value) {
            this.updateFlags(value);
        },
        enumerable: true,
        configurable: true
    });
    return TaskSelectionStepController;
}());
exports.TaskSelectionStepController = TaskSelectionStepController;


/***/ }),
/* 84 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-task-selection-step> <p _t> The capacity planner models virtualization resource allocation and usage. Select a task for the planner. We'll walk you through the steps and produce a report explaining the predictions. </p> <div class=xui-margin-lg> <div class=xui-margin-lgv> <xui-radio data-selector=checkup-radio ng-model=vm.simulationMode ng-value=vm.simulationModeEnum.CheckUp help-text=\"_t(Examine the current state of one of your clusters. Predict when future capacity problems are likely to occur.)\" _ta> <span class=xui-text-l _t>Run a checkup</span> </xui-radio> </div> <div class=xui-margin-lgv> <xui-radio data-selector=workloads-radio ng-model=vm.simulationMode ng-value=vm.simulationModeEnum.AddWorkloads help-text=\"_t(See how adding additional virtual machines would impact the performance of a cluster. Estimate how many additional host computers would be required to maintain acceptable performance.)\" _ta> <span class=xui-text-l _t>Simulate adding extra VMs</span> </xui-radio> </div> <div class=xui-margin-lgv> <xui-radio data-selector=resources-radio ng-model=vm.simulationMode ng-value=vm.simulationModeEnum.AddResources help-text=\"_t(View the impact of adding more host computers to one of your clusters.)\" _ta> <span class=xui-text-l _t>Simulate adding extra host computers</span> </xui-radio> </div> <div class=xui-margin-lgv> <xui-radio data-selector=workloads-resources-radio ng-model=vm.simulationMode ng-value=vm.simulationModeEnum.AddWorkloadsAndResources help-text=\"_t(Simulate a complete expansion project. Find resource bottlenecks in your build-out, and predict future capacity problems.)\" _ta> <span class=xui-text-l _t>Simulate adding extra VMs and host computers</span> </xui-radio> </div> </div> </div> ";

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var entitySelectionStep_directive_1 = __webpack_require__(86);
exports.default = function (module) {
    module.component("vimCpReportWizardEntitySelectionStep", entitySelectionStep_directive_1.EntitySelectionStepDirective);
};


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(48);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var entitySelectionStep_controller_1 = __webpack_require__(87);
/**
 * Directive wrapper for report wizard / entity selection step
 */
var EntitySelectionStepDirective = /** @class */ (function () {
    function EntitySelectionStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(88);
        this.controller = entitySelectionStep_controller_1.EntitySelectionStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            computedEntities: "=",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return EntitySelectionStepDirective;
}());
exports.EntitySelectionStepDirective = EntitySelectionStepDirective;
;


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
;
/**
 * Controller for entity selection step of the wizard
 */
var EntitySelectionStepController = /** @class */ (function () {
    function EntitySelectionStepController($scope, _t, reportWizardService) {
        var _this = this;
        this.$scope = $scope;
        this._t = _t;
        this.reportWizardService = reportWizardService;
        this.$onInit = function () {
            _this.initProperties();
            if (!_.isEmpty(_this.computedEntities)) {
                _this.selectedEntities.items = _this.computedEntities;
            }
            _this.$scope.$watch(_this.getSelectedEntities, _this.handleGridSelectionChanged);
            // initial load
            return _this.loadEntities(_this.entityNetObjectId).then(_this.preSelectInitialEntities);
        };
        this.getSelectedEntities = function () {
            if (_.isEmpty(_this.selectedEntities.items)) {
                return undefined;
            }
            return _this.selectedEntities.items;
        };
        this.preSelectInitialEntities = function () {
            if (_.isEmpty(_this.computedEntities)) {
                return;
            }
            _this.selectedEntities = { items: [], blacklist: false };
            var _loop_1 = function (computed) {
                var entity = _.find(_this.entities, function (e) { return e.netObjectId === computed.entityNetObjectId; });
                if (entity) {
                    _this.selectedEntities.items.push(entity);
                }
            };
            for (var _i = 0, _a = _this.computedEntities; _i < _a.length; _i++) {
                var computed = _a[_i];
                _loop_1(computed);
            }
        };
        this.loadEntities = function (prioritizeEntity) {
            var filter = {
                search: _this.search,
                pagination: _this.pagination,
                prioritizedEntity: prioritizeEntity,
                sorting: _this.sorting
            };
            _this.isBusy = true;
            return _this.reportWizardService.getEntities(filter)
                .then(function (result) {
                _this.entities = result.rows;
                _this.pagination.total = result.total;
            })
                .finally(function () {
                _this.isBusy = false;
            });
        };
        this.getTemplateUrlFn = function () {
            return function (e) { return _this.templates[e.entityType]; };
        };
        this.onSearch = function (search) {
            _this.search = search;
            return _this.loadEntities();
        };
        this.onPaginationChange = function (page, pageSize) {
            _this.pagination.page = page;
            _this.pagination.pageSize = pageSize;
            return _this.loadEntities();
        };
        this.onSortingChange = function () {
            return _this.loadEntities();
        };
        this.handleGridSelectionChanged = function (newSelectedEntities, oldSelectedEntities) {
            // ignore initial callback
            if (angular.equals(newSelectedEntities, oldSelectedEntities)) {
                return;
            }
            _this.computedEntities = [];
            if (newSelectedEntities) {
                var selected = _this.getSelectedEntities();
                for (var _i = 0, selected_1 = selected; _i < selected_1.length; _i++) {
                    var entity = selected_1[_i];
                    var computedEntity = {
                        entityCaption: entity.name,
                        entityNetObjectId: entity.netObjectId,
                        counters: undefined
                    };
                    _this.computedEntities.push(computedEntity);
                }
            }
        };
        this.isValid = function () {
            _this.isErrorMessageVisible = (_this.computedEntities.length === 0);
            return {
                isValid: !_this.isErrorMessageVisible
            };
        };
    }
    EntitySelectionStepController.prototype.initProperties = function () {
        this.isBusy = false;
        this.search = undefined;
        this.entities = [];
        this.isErrorMessageVisible = false;
        this.selectedEntities = { items: [], blacklist: false };
        this.pagination = { page: 1, pageSize: 5, total: undefined };
        this.sortByClusterName = { id: "Name", label: this._t("Cluster") };
        this.templates = (_a = {},
            _a[index_1.ReportEntityType.Cluster] = "cluster-template",
            _a);
        this.options = {
            triggerSearchOnChange: true,
            searchDebounce: 500
        };
        this.sorting = {
            sortableColumns: [
                this.sortByClusterName,
                { id: "DatacenterName", label: this._t("Datacenter") }
            ],
            sortBy: this.sortByClusterName,
            direction: "asc"
        };
        this.remoteControl.isValid = this.isValid;
        var _a;
    };
    ;
    EntitySelectionStepController = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngScope)),
        __param(1, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_2.ServiceNames.reportWizardService)),
        __metadata("design:paramtypes", [Object, Function, Object])
    ], EntitySelectionStepController);
    return EntitySelectionStepController;
}());
exports.EntitySelectionStepController = EntitySelectionStepController;
;


/***/ }),
/* 88 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-entity-selection-step> <div class=row> <div class=col-xs-12> <h2 _t>SELECT A CLUSTER TO MODEL</h2> </div> <div class=col-xs-12> <p _t>The planner will work in the context of the selected cluster(s). For each cluster you select, the planner will examine the configuration of hosts and VMs in the cluster and make predictions based on the cluster’s past performance history. <br/><br/>A separate Capacity Planning report will be generated for each selected cluster.</p> </div> </div> <xui-divider></xui-divider> <div class=\"row entities-grid\"> <div class=\"col-xs-12 xui-edge-definer\" xui-busy=vm.isBusy> <xui-grid items-source=vm.entities track-by=netObjectId pagination-data=vm.pagination sorting-data=::vm.sorting selection-mode=multi selection=vm.selectedEntities show-selector=false empty-template-url=empty-list-template hide-toolbar=true options=vm.options controller=vm template-fn=vm.getTemplateUrlFn(item) on-search=vm.onSearch(item) on-pagination-change=\"vm.onPaginationChange(page, pageSize)\" on-sorting-change=vm.onSortingChange()> </xui-grid> <div ng-if=vm.isErrorMessageVisible class=\"sw-widget-error xui-margin-lgl\" _t> Select a cluster. </div> </div> </div> <script type=text/ng-template id=empty-list-template> <div class=\"xui-listview__empty\">\n            <span ng-if=\"vm.search\" _t>\n                No clusters matching the search criteria.\n            </span>\n            <span ng-if=\"!vm.search\" _t>\n                No clusters available.\n            </span>\n        </div> </script> <script type=text/ng-template id=cluster-template> <div class=\"row entity-item\" data-id=\"{{::item.netObjectId}}\">\n            <div class=\"col-xs-5\">\n                <strong class=\"entity-name\">\n                    {{::item.name}}\n                </strong>\n                <div class=\"datacenter-name\" \n                     ng-if=\"::item.datacenterName\">\n                    {{::item.datacenterName}}\n                </div>\n            </div>\n            <div class=\"col-xs-3 vms-count\" \n                 _t=\"['{{::item.vmsCount | number}}']\">\n                VMs: {0}\n            </div>\n            <div class=\"col-xs-2 hosts-count\" \n                 _t=\"['{{::item.hostsCount | number}}']\">\n                Hosts: {0}\n            </div>\n            <div class=\"col-xs-2 datastores-count\" \n                 _t=\"['{{::item.datastoresCount | number}}']\">\n                Datastores: {0}\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var timeFrameStep_directive_1 = __webpack_require__(90);
exports.default = function (module) {
    module.component("vimCpReportWizardTimeFrameStep", timeFrameStep_directive_1.TimeFrameStepDirective);
};


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(49);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var timeFrameStep_controller_1 = __webpack_require__(91);
/**
 * Directive wrapper for report wizard / time frame step
 */
var TimeFrameStepDirective = /** @class */ (function () {
    function TimeFrameStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(92);
        this.controller = timeFrameStep_controller_1.TimeFrameStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            runway: "=",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return TimeFrameStepDirective;
}());
exports.TimeFrameStepDirective = TimeFrameStepDirective;
;


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
/**
 * Controller for time frame step of the wizard
 */
var TimeFrameStepController = /** @class */ (function () {
    function TimeFrameStepController($scope, _t, dateTimeService) {
        var _this = this;
        this.$scope = $scope;
        this._t = _t;
        this.dateTimeService = dateTimeService;
        this.options = [];
        this.$onInit = function () {
            _this.isCustomDateErrorMessageVisible = false;
            _this.remoteControl.isValid = _this.isValid;
            _this.options = _this.getOptions();
            _this.initCustomDate();
            if (_.isUndefined(_this.runway)) {
                _this.selectDefaultOption();
            }
            else {
                _this.selectInitialOption();
            }
            _this.$scope.$watch(function () { return _this.selectedOptionIndex; }, _this.onSelectedOptionIndexChanged);
            // todo: UIF-4676
            _this.$scope.$watch(function () { return _this.customDate; }, _this.onCustomDateChanged);
        };
        this.selectDefaultOption = function () {
            var defaultOption = _.find(_this.options, function (o) { return o.isDefault; });
            _this.selectedOptionIndex = _.indexOf(_this.options, defaultOption);
            _this.runway = defaultOption.days;
        };
        this.selectInitialOption = function () {
            // try to find relevant non-custom option
            var initialNonCustomOption = _.find(_this.options, function (o) { return !o.isCustom && o.days === _this.runway; });
            if (initialNonCustomOption) {
                _this.selectedOptionIndex = _.indexOf(_this.options, initialNonCustomOption);
            }
            else {
                _this.selectInitialCustomOption();
            }
        };
        this.selectInitialCustomOption = function () {
            var customOption = _.find(_this.options, function (o) { return o.isCustom; });
            _this.selectedOptionIndex = _.indexOf(_this.options, customOption);
            var customDayMoment = _this.getOrionToday().add(_this.runway, "days");
            _this.customDate = _this.dateTimeService.convertMomentToOrionDate(customDayMoment);
        };
        this.getOptions = function () {
            return [
                _this.getRightNowOption(),
                _this.getNDaysFromNowOption(_this._t("90 days from now"), 90),
                _this.getEndOfYearOption(),
                _this.getNDaysFromNowOption(_this._t("365 days from now"), 365, true),
                _this.getCustomDateOption()
            ];
        };
        this.getRightNowOption = function () {
            return {
                title: _this._t("Right now"),
                description: _this._t("The planner will not perform a projection. It will just report on the current (or simulated) state of the environment."),
                days: 0,
                isDefault: false,
                isCustom: false
            };
        };
        this.getNDaysFromNowOption = function (title, daysToAdd, isDefault) {
            if (isDefault === void 0) { isDefault = false; }
            return {
                title: title,
                days: daysToAdd,
                isCustom: false,
                isDefault: isDefault
            };
        };
        this.getEndOfYearOption = function () {
            var today = _this.getOrionToday();
            var endOfYear = moment(today).endOf("year").startOf("day");
            var endOfYearDiff = endOfYear.diff(today);
            var days = Math.round(moment.duration(endOfYearDiff).asDays());
            return {
                title: _this._t("The end of the current year"),
                days: days,
                isDefault: false,
                isCustom: false
            };
        };
        this.getCustomDateOption = function () {
            return {
                title: _this._t("Select a specific date"),
                days: undefined,
                isDefault: false,
                isCustom: true
            };
        };
        this.initCustomDate = function () {
            var today = _this.getOrionToday();
            _this.customDate = _this.customDateMin = _this.dateTimeService.convertMomentToOrionDate(moment(today));
            _this.customDateMax = _this.dateTimeService.convertMomentToOrionDate(moment(today).add(1, "year"));
        };
        this.isCustomOptionSelected = function () {
            return _this.options[_this.selectedOptionIndex].isCustom;
        };
        this.isValid = function () {
            _this.isCustomDateErrorMessageVisible = _this.selectedOptionIndex === 4 && !_.isDate(_this.customDate);
            return {
                isValid: !_this.isCustomDateErrorMessageVisible
            };
        };
        this.onSelectedOptionIndexChanged = function (newValue, oldValue) {
            if (_.isEqual(newValue, oldValue)) {
                return;
            }
            var option = _this.options[newValue];
            if (option.isCustom) {
                _this.runway = _this.getNumberOfDaysToCustomDate();
            }
            else {
                _this.runway = option.days;
            }
        };
        this.onCustomDateChanged = function (newValue, oldValue) {
            if (_.isEqual(newValue, oldValue)) {
                return;
            }
            _this.runway = _this.getNumberOfDaysToCustomDate();
        };
        /**
         * Gets number of days remaining to selected custom date
         */
        this.getNumberOfDaysToCustomDate = function () {
            if (_.isUndefined(_this.customDate)) {
                return undefined;
            }
            var orionTodayDate = _this.dateTimeService.convertMomentToOrionDate(_this.getOrionToday());
            var diff = moment(_this.customDate).startOf("day").diff(orionTodayDate);
            return Math.round(moment.duration(diff).asDays());
        };
        this.getOrionToday = function () {
            return _this.dateTimeService.getOrionNowMoment().startOf("day");
        };
    }
    TimeFrameStepController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.dateTimeService)),
        __metadata("design:paramtypes", [Object, Function, Object])
    ], TimeFrameStepController);
    return TimeFrameStepController;
}());
exports.TimeFrameStepController = TimeFrameStepController;
;


/***/ }),
/* 92 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-time-frame-step> <div class=row> <div class=col-xs-12> <h2 _t>SELECT A MODELING WINDOW</h2> </div> </div> <div class=row> <div class=col-xs-12> <p _t>The planner predicts how usage will change over time.</p> <p _t>The planner always starts at the current date, but you can choose how far forward you want the planner to project.</p> </div> </div> <div class=row> <div class=col-xs-12> <p _t>What future date should the planner use?</p> </div> </div> <div class=row> <div class=\"col-xs-12 xui-margin-lgl\" data-selector=time-frame-options> <div ng-repeat=\"option in ::vm.options\"> <xui-radio ng-model=vm.selectedOptionIndex ng-value=::$index help-text={{::option.description}}> <div class=time-frame-option> <span class=xui-text-l _t> {{::option.title}} </span> </div> </xui-radio> <div class=xui-margin-lgl ng-if=::option.isCustom data-selector=custom-time-frame> <xui-date-picker min-date=::vm.customDateMin max-date=::vm.customDateMax preserve-insignificant=\"'false'\" is-disabled=!vm.isCustomOptionSelected() ng-model=vm.customDate> </xui-date-picker> <div ng-if=vm.isCustomDateErrorMessageVisible class=sw-widget-error _t> Invalid or missing date! </div> </div> </div> </div> </div> </div> ";

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var hostProfileStep_directive_1 = __webpack_require__(94);
var index_1 = __webpack_require__(97);
var index_2 = __webpack_require__(105);
exports.default = function (module) {
    module.component("vimCpReportWizardHostProfileStep", hostProfileStep_directive_1.HostProfileStepDirective);
    index_1.default(module);
    index_2.default(module);
};


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(50);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hostProfileStep_controller_1 = __webpack_require__(95);
/**
 * Directive wrapper for report wizard / host profile step
 */
var HostProfileStepDirective = /** @class */ (function () {
    function HostProfileStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(96);
        this.controller = hostProfileStep_controller_1.HostProfileStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            hostProfiles: "<",
            remoteControl: "<",
            computedEntities: "<"
        };
        this.scope = {};
    }
    return HostProfileStepDirective;
}());
exports.HostProfileStepDirective = HostProfileStepDirective;
;


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(12);
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var index_2 = __webpack_require__(20);
var SimulatedHostType;
(function (SimulatedHostType) {
    SimulatedHostType[SimulatedHostType["Existing"] = 0] = "Existing";
    SimulatedHostType[SimulatedHostType["Custom"] = 1] = "Custom";
})(SimulatedHostType = exports.SimulatedHostType || (exports.SimulatedHostType = {}));
;
/**
 * Controller for host profile step of the wizard
 */
var HostProfileStepController = /** @class */ (function () {
    function HostProfileStepController(xuiToastService, swUtil, _t, counterUnitFilter, $scope) {
        var _this = this;
        this.xuiToastService = xuiToastService;
        this.swUtil = swUtil;
        this._t = _t;
        this.counterUnitFilter = counterUnitFilter;
        this.$scope = $scope;
        this.hostTypeEnum = SimulatedHostType;
        this.$onInit = function () {
            _this.remoteControl.isValid = _this.isValid;
            _this.remoteControl.save = _this.save;
            _this.initializeValues();
        };
        this.save = function () {
            var selectedHostProfile = _this.getSelectedHostProfile();
            if (!selectedHostProfile) {
                return;
            }
            selectedHostProfile.instanceCount = _this.instanceCountValue;
            var unwrappedProfile = selectedHostProfile.profile;
            _this.hostProfiles.splice(0, 1, unwrappedProfile);
        };
        this.isValid = function () {
            _this.isErrorMessageVisible = _this.hostType === SimulatedHostType.Existing && (!_this.existingHostProfile || !_this.existingHostProfile.hostId);
            return {
                isValid: !_this.isErrorMessageVisible
            };
        };
        this.initializeValues = function () {
            if (_.isEmpty(_this.hostProfiles)) {
                _this.hostType = SimulatedHostType.Custom;
                _this.instanceCountValue = 0;
                return;
            }
            //currently we're using exactly one host profile
            var hostProfile = _this.hostProfiles[0];
            if (hostProfile.hostId) {
                //existing host profile
                _this.hostType = SimulatedHostType.Existing;
                _this.existingHostProfile = new models_1.HostProfileValidationWrapper(hostProfile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
            }
            else {
                _this.hostType = SimulatedHostType.Custom;
                _this.customHostProfile = new models_1.HostProfileValidationWrapper(hostProfile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
            }
            _this.instanceCountValue = hostProfile.instanceCount;
        };
        this.getSelectedHostProfile = function () {
            if (_this.hostType === SimulatedHostType.Custom) {
                return _this.customHostProfile;
            }
            else if (_this.hostType === SimulatedHostType.Existing) {
                return _this.existingHostProfile;
            }
            return null;
        };
    }
    Object.defineProperty(HostProfileStepController.prototype, "instanceCount", {
        get: function () {
            return this.instanceCountValue;
        },
        set: function (value) {
            var min = models_1.DefaultHostProfileRestrictions.instanceCountMin;
            var max = models_1.DefaultHostProfileRestrictions.instanceCountMax;
            if (value == null) {
                this.xuiToastService.warning(this._t("Number is not valid."));
                this.instanceCountValue = min;
                return;
            }
            if (!_.isInteger(value)) {
                this.xuiToastService.warning(this._t("Number is not valid."));
                this.instanceCountValue = _.round(value);
                return;
            }
            if (value < min) {
                this.xuiToastService.warning(this.swUtil.formatString(this._t("Simulated instance value cannot be less than {0}."), min));
                this.instanceCountValue = min;
                return;
            }
            else if (value > max) {
                this.xuiToastService.warning(this.swUtil.formatString(this._t("Simulated instance value cannot exceed {0}."), max));
                this.instanceCountValue = max;
                return;
            }
            this.instanceCountValue = value;
        },
        enumerable: true,
        configurable: true
    });
    HostProfileStepController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.toastService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.swUtilService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(3, di_1.Inject(index_2.FilterFullNames.counterUnitFilter)),
        __param(4, di_1.Inject(index_1.NgServiceNames.ngScope)),
        __metadata("design:paramtypes", [Object, Object, Function, Function, Object])
    ], HostProfileStepController);
    return HostProfileStepController;
}());
exports.HostProfileStepController = HostProfileStepController;
;


/***/ }),
/* 96 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-host-profile-step> <div class=row> <div class=col-xs-12> <h2 _t>HOST PROFILE</h2> </div> </div> <div class=row> <div class=col-xs-12> <span _t>The host profile represents a typical host computer in your environment. You can add extra hosts to simulate purchasing additional capacity.</span> </div> </div> <div class=row> <div class=col-xs-12> <h4 _t>Simulated Host Count:</h4> </div> </div> <div class=row> <div class=col-xs-2> <xui-textbox type=number ng-model=vm.instanceCount ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" data-selector=instance-count> </xui-textbox> </div> </div> <div class=row> <div class=col-xs-12> <xui-radio ng-model=vm.hostType ng-value=vm.hostTypeEnum.Custom data-selector=custom-host-radio _ta> <span class=xui-text-l _t>Define a custom host profile</span> <div ng-class=\"{'disabled': vm.hostType != vm.hostTypeEnum.Custom}\" data-selector=custom-host> <vim-cp-report-wizard-custom-host-profile profile=vm.customHostProfile> </vim-cp-report-wizard-custom-host-profile> </div> </xui-radio> <xui-radio ng-model=vm.hostType ng-value=vm.hostTypeEnum.Existing data-selector=existing-host-radio _ta> <span class=xui-text-l _t>Simulate an existing host computer</span> <div ng-class=\"{'disabled': vm.hostType != vm.hostTypeEnum.Existing}\" data-selector=existing-host> <div class=xui-margin-lg> <vim-cp-report-wizard-existing-host-profile profile=vm.existingHostProfile parent-entity-net-object-ids=vm.entityNetObjectIds computed-entities=vm.computedEntities> </vim-cp-report-wizard-existing-host-profile> </div> </div> </xui-radio> <div ng-if=vm.isErrorMessageVisible class=sw-widget-error _t> Select an existing host. </div> </div> </div> </div>";

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var existingHostProfile_directive_1 = __webpack_require__(98);
var index_1 = __webpack_require__(101);
exports.default = function (module) {
    module.component("vimCpReportWizardExistingHostProfile", existingHostProfile_directive_1.ExistingHostProfileDirective);
    index_1.default(module);
};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var existingHostProfile_controller_1 = __webpack_require__(99);
/**
 * Directive wrapper for existing host profile
 */
var ExistingHostProfileDirective = /** @class */ (function () {
    function ExistingHostProfileDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(100);
        this.controller = existingHostProfile_controller_1.ExistingHostProfileController;
        this.controllerAs = "vm";
        this.bindToController = {
            profile: "=",
            computedEntities: "<"
        };
        this.scope = {};
    }
    return ExistingHostProfileDirective;
}());
exports.ExistingHostProfileDirective = ExistingHostProfileDirective;
;


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(12);
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var index_2 = __webpack_require__(20);
;
/**
 * Controller for existing host profile
 */
var ExistingHostProfileController = /** @class */ (function () {
    function ExistingHostProfileController(xuiToastService, swUtil, _t, dialogService, counterUnitFilter) {
        var _this = this;
        this.xuiToastService = xuiToastService;
        this.swUtil = swUtil;
        this._t = _t;
        this.dialogService = dialogService;
        this.counterUnitFilter = counterUnitFilter;
        this.$onInit = function () {
            _this.isSelected = false;
            _this.isErrorMessageVisible = false;
            if (_this.profile) {
                _this.isSelected = true;
            }
            _this.toggleButtonText();
        };
        this.onSelect = function (selected) {
            if (_.isEmpty(selected) && !_this.isSelected) {
                _this.toggleButtonText();
                return;
            }
            var selectedHost = selected[0];
            var hostProfile = {
                cpuCoreCount: selectedHost.cpuCoreCount,
                cpuCoreFrequency: selectedHost.cpuCoreFrequency,
                diskSpaceCapacity: selectedHost.diskSpaceCapacity,
                instanceCount: undefined,
                memoryCapacity: selectedHost.memoryCapacity,
                name: selectedHost.hostName,
                hostId: selectedHost.hostId
            };
            _this.profile = new models_1.HostProfileValidationWrapper(hostProfile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
            _this.isSelected = true;
            _this.toggleButtonText();
        };
        this.toggleButtonText = function () {
            _this.buttonText = _this.isSelected ? _this._t("Change Host") : _this._t("Select Host");
        };
        this.selectExistingHost = function () {
            var ids = [];
            for (var _i = 0, _a = _this.computedEntities; _i < _a.length; _i++) {
                var computed = _a[_i];
                ids.push(computed.entityNetObjectId);
            }
            var viewModel = {
                profile: _this.profile,
                parentEntityNetObjectIds: ids
            };
            return _this.openExistingHostProfileWizardDialog(viewModel).then(function (res) {
                if (!res || res === "cancel" || !viewModel.profile) {
                    return;
                }
                _this.profile = new models_1.HostProfileValidationWrapper(viewModel.profile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
                _this.isSelected = true;
                _this.toggleButtonText();
            });
        };
        this.openExistingHostProfileWizardDialog = function (viewModel) {
            var wizardOptions = {
                title: _this._t("Select Existing Host Profile").toUpperCase(),
                viewModel: viewModel
            };
            return _this.dialogService.showModal({
                size: "lg",
                templateUrl: "existing-host-profile-template",
            }, wizardOptions);
        };
    }
    ExistingHostProfileController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.toastService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.swUtilService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(3, di_1.Inject(index_1.ApolloServiceNames.dialogService)),
        __param(4, di_1.Inject(index_2.FilterFullNames.counterUnitFilter)),
        __metadata("design:paramtypes", [Object, Object, Function, Object, Function])
    ], ExistingHostProfileController);
    return ExistingHostProfileController;
}());
exports.ExistingHostProfileController = ExistingHostProfileController;
;


/***/ }),
/* 100 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-existing-host-profile> <div class=xui-margin-lgv> <div class=\"row existing-host-profile-selected-item\" ng-if=vm.isSelected> <div class=col-xs-4> <div class=row> <div class=\"col-xs-12 xui-text-l\" _t> Typical Host </div> </div> <div class=row> <div class=col-xs-12> <dfn data-selector=item-hostname>{{vm.profile.name}}</dfn> </div> </div> </div> <div class=col-xs-2> <div class=media> <div class=media-left> <xui-icon icon=cpu icon-size=small></xui-icon> </div> <div class=media-body> <span data-selector=item-cpu>{{vm.profile.cpuCoreCount | vimCounterFormat: 'cores'}}</span> </div> </div> </div> <div class=col-xs-2> <div class=media> <div class=media-left> <xui-icon icon=memory icon-size=small></xui-icon> </div> <div class=media-body> <span data-selector=item-memory>{{vm.profile.memoryCapacity | vimCounterFormat: 'bytes'}}</span> </div> </div> </div> <div class=col-xs-2> <div class=media> <div class=media-left> <xui-icon icon=harddrive icon-size=small></xui-icon> </div> <div class=media-body> <span data-selector=item-harddrive>{{vm.profile.diskSpaceCapacity | vimCounterFormat: 'bytes'}}</span> </div> </div> </div> </div> </div> <div class=xui-margin-lgv> <div class=row> <div class=col-xs-12> <xui-button ng-click=vm.selectExistingHost()>{{vm.buttonText}} </xui-button> <div ng-if=vm.isErrorMessageVisible class=sw-widget-error _t> No host selected! </div> </div> </div> </div> <script type=text/ng-template id=existing-host-profile-template> <xui-dialog>\n            <vim-cp-report-existing-host-profile-dialog\n                view-model=\"vm.dialogOptions.viewModel\">\n            </vim-cp-report-existing-host-profile-dialog>\n        </xui-dialog> </script> </div>";

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var existingHostProfileDialog_directive_1 = __webpack_require__(102);
exports.default = function (module) {
    module.component("vimCpReportExistingHostProfileDialog", existingHostProfileDialog_directive_1.ExistingHostProfileDialogDirective);
};


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var existingHostProfileDialog_controller_1 = __webpack_require__(103);
/**
 * Directive wrapper for existing host profile
 */
var ExistingHostProfileDialogDirective = /** @class */ (function () {
    function ExistingHostProfileDialogDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(104);
        this.controller = existingHostProfileDialog_controller_1.ExistingHostProfileDialogController;
        this.controllerAs = "vm";
        this.bindToController = {
            viewModel: "<"
        };
        this.scope = {};
    }
    return ExistingHostProfileDialogDirective;
}());
exports.ExistingHostProfileDialogDirective = ExistingHostProfileDialogDirective;
;


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(6);
var di_1 = __webpack_require__(0);
var index_2 = __webpack_require__(1);
/**
 * Controller for existing host profile dialog
 */
var ExistingHostProfileDialogController = /** @class */ (function () {
    function ExistingHostProfileDialogController($scope, _t, reportWizardService, xuiFilteredListService) {
        var _this = this;
        this.$scope = $scope;
        this._t = _t;
        this.reportWizardService = reportWizardService;
        this.xuiFilteredListService = xuiFilteredListService;
        this.$onInit = function () {
            _this.state = {
                options: {
                    templateUrl: "host-item-template",
                    selectionMode: "single"
                }
            };
            _this.dispatcher = _this.xuiFilteredListService.getDispatcherInstance(_this.xuiFilteredListService.getModel(_this, "state"), {
                dataSource: {
                    getItems: _this.loadHosts
                }
            });
            _this.$scope.$watch(_this.getSelectedHostEntityId, _this.handleSelectedHostChange);
        };
        this.preselectHost = function (hosts) {
            if (!_this.viewModel.profile) {
                return;
            }
            var host = _.find(hosts, function (h) { return h.hostId === _this.viewModel.profile.hostId; });
            if (host) {
                _this.state.selection.items = [host];
            }
        };
        this.handleSelectedHostChange = function () {
            var hostEntity = _this.getSelectedHostEntity();
            if (!hostEntity) {
                return;
            }
            var hostProfile = {
                cpuCoreCount: hostEntity.cpuCoreCount,
                cpuCoreFrequency: hostEntity.cpuCoreFrequency,
                diskSpaceCapacity: hostEntity.diskSpaceCapacity,
                instanceCount: undefined,
                memoryCapacity: hostEntity.memoryCapacity,
                name: hostEntity.hostName,
                hostId: hostEntity.hostId
            };
            _this.viewModel.profile = hostProfile;
        };
        this.getSelectedHostEntityId = function () {
            var selected = _this.getSelectedHostEntity();
            return selected ? selected.hostId : undefined;
        };
        this.getSelectedHostEntity = function () {
            if (_.isEmpty(_this.state.selection.items)) {
                return undefined;
            }
            return _this.state.selection.items[0];
        };
        this.loadHosts = function (param) {
            var filter = {
                search: param.search,
                parentNetObjectIds: _this.viewModel.parentEntityNetObjectIds,
                pagination: param.pagination,
                sorting: {
                    sortBy: { id: "HostName" },
                    direction: "asc"
                },
                prioritizedEntity: _this.viewModel.profile ? index_1.NetObjectPrefixes.host + ":" + _this.viewModel.profile.hostId : undefined
            };
            return _this.reportWizardService.getExistingHosts(filter)
                .then(function (result) {
                _this.preselectHost(result.rows);
                return {
                    items: result.rows,
                    total: result.total
                };
            });
        };
    }
    ExistingHostProfileDialogController = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngScope)),
        __param(1, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_2.ServiceNames.reportWizardService)),
        __param(3, di_1.Inject(index_2.ApolloServiceNames.xuiFilteredListService)),
        __metadata("design:paramtypes", [Object, Function, Object, Object])
    ], ExistingHostProfileDialogController);
    return ExistingHostProfileDialogController;
}());
exports.ExistingHostProfileDialogController = ExistingHostProfileDialogController;
;


/***/ }),
/* 104 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-existing-host-profile-dialog> <xui-filtered-list-v2 state=vm.state dispatcher=vm.dispatcher> </xui-filtered-list-v2> <script type=text/ng-template id=host-item-template> <div class=\"row existing-host-profile-selector-item\">\n            <div class=\"col-xs-6\" data-selector=\"item-hostname\">\n                {{::item.hostName}}\n            </div>\n            <div class=\"col-xs-2\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"cpu\" icon-size=\"small\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span data-selector=\"item-cpu\">{{::item.cpuCoreCount | vimCounterFormat: 'cores'}}</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-2\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"memory\" icon-size=\"small\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span data-selector=\"item-memory\">{{::item.memoryCapacity | vimCounterFormat: 'bytes'}}</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-2\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"harddrive\" icon-size=\"small\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span data-selector=\"item-harddrive\">{{::item.diskSpaceCapacity | vimCounterFormat: 'bytes'}}</span>\n                    </div>\n                </div>\n            </div>\n        </div> </script> </div>";

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var customHostProfile_directive_1 = __webpack_require__(106);
exports.default = function (module) {
    module.component("vimCpReportWizardCustomHostProfile", customHostProfile_directive_1.CustomHostProfileDirective);
};


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(51);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customHostProfile_controller_1 = __webpack_require__(107);
/**
 * Directive wrapper for custom host profile
 */
var CustomHostProfileDirective = /** @class */ (function () {
    function CustomHostProfileDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(108);
        this.controller = customHostProfile_controller_1.CustomHostProfileController;
        this.controllerAs = "vm";
        this.bindToController = {
            profile: "=",
        };
        this.scope = {};
    }
    return CustomHostProfileDirective;
}());
exports.CustomHostProfileDirective = CustomHostProfileDirective;
;


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(12);
var conversionHelper_1 = __webpack_require__(19);
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var index_2 = __webpack_require__(20);
/**
 * Controller for custom host profile
 */
var CustomHostProfileController = /** @class */ (function () {
    function CustomHostProfileController(xuiToastService, swUtil, _t, counterUnitFilter, $scope) {
        var _this = this;
        this.xuiToastService = xuiToastService;
        this.swUtil = swUtil;
        this._t = _t;
        this.counterUnitFilter = counterUnitFilter;
        this.$scope = $scope;
        this.$onInit = function () {
            _this.initializeValues();
        };
        this.initializeValues = function () {
            //Host profile is empty
            if (_this.profile) {
                return;
            }
            var profile = {
                name: _this._t("Custom Host Profile")
            };
            _this.profile = new models_1.HostProfileValidationWrapper(profile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
            _this.cpuCoreCount = 8;
            _this.cpuCoreFrequency = 3.6; // 3.6 GHz
            _this.memoryCapacity = 64; // 64 GB
            _this.diskSpaceCapacity = 2; // 2 TB
        };
    }
    Object.defineProperty(CustomHostProfileController.prototype, "cpuCoreCount", {
        get: function () {
            return this.profile.cpuCoreCount;
        },
        set: function (value) {
            this.profile.cpuCoreCount = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CustomHostProfileController.prototype, "cpuCoreFrequency", {
        get: function () {
            return conversionHelper_1.ConversionHelper.convertHertz(this.profile.cpuCoreFrequency, conversionHelper_1.HertzUnit.Hz, conversionHelper_1.HertzUnit.GHz);
        },
        set: function (value) {
            this.profile.cpuCoreFrequency = conversionHelper_1.ConversionHelper.convertHertz(value, conversionHelper_1.HertzUnit.GHz, conversionHelper_1.HertzUnit.Hz);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CustomHostProfileController.prototype, "memoryCapacity", {
        get: function () {
            return conversionHelper_1.ConversionHelper.convertBytes(this.profile.memoryCapacity, conversionHelper_1.BytesUnit.B, conversionHelper_1.BytesUnit.GB);
        },
        set: function (value) {
            this.profile.memoryCapacity = conversionHelper_1.ConversionHelper.convertBytes(value, conversionHelper_1.BytesUnit.GB, conversionHelper_1.BytesUnit.B);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CustomHostProfileController.prototype, "diskSpaceCapacity", {
        get: function () {
            return conversionHelper_1.ConversionHelper.convertBytes(this.profile.diskSpaceCapacity, conversionHelper_1.BytesUnit.B, conversionHelper_1.BytesUnit.TB);
        },
        set: function (value) {
            this.profile.diskSpaceCapacity = conversionHelper_1.ConversionHelper.convertBytes(value, conversionHelper_1.BytesUnit.TB, conversionHelper_1.BytesUnit.B);
        },
        enumerable: true,
        configurable: true
    });
    CustomHostProfileController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.toastService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.swUtilService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(3, di_1.Inject(index_2.FilterFullNames.counterUnitFilter)),
        __param(4, di_1.Inject(index_1.NgServiceNames.ngScope)),
        __metadata("design:paramtypes", [Object, Object, Function, Function, Object])
    ], CustomHostProfileController);
    return CustomHostProfileController;
}());
exports.CustomHostProfileController = CustomHostProfileController;
;


/***/ }),
/* 108 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-custom-host-profile> <div class=\"col-xs-12 media params-row\"> <div class=\"media-left media-middle\"> <xui-icon icon=cpu icon-size=small></xui-icon> </div> <div class=\"media-left media-middle\"> <span _t>CPU</span> </div> <div class=\"media-body profile-value xui-margin-mdr\"> <xui-textbox ng-model=vm.cpuCoreCount ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" type=number data-selector=cpu-core-count> </xui-textbox> </div> <div class=\"media-left media-middle\"> <span _t>cores @</span> </div> <div class=\"media-body profile-value xui-margin-mdr\"> <xui-textbox ng-model=vm.cpuCoreFrequency ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" type=number data-selector=cpu-frequency> </xui-textbox> </div> <div class=\"media-left media-middle unit-margin\"> <span _t>GHz</span> </div> <div class=\"media-left media-middle\"> <xui-icon icon=memory icon-size=small></xui-icon> </div> <div class=\"media-left media-middle\"> <span _t>Memory</span> </div> <div class=\"media-body profile-value xui-margin-mdr\"> <xui-textbox ng-model=vm.memoryCapacity ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" type=number data-selector=memory-capacity> </xui-textbox> </div> <div class=\"media-left media-middle\"> <span _t>GB</span> </div> </div> <div class=\"col-xs-12 media params-row\"> <div class=\"media-left media-middle\"> <xui-icon icon=harddrive icon-size=small></xui-icon> </div> <div class=\"media-left media-middle\"> <span _t>Virtualization Disk Space</span> </div> <div class=\"media-body profile-value xui-margin-mdr\"> <xui-textbox ng-model=vm.diskSpaceCapacity ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" type=number data-selector=disk-space> </xui-textbox> </div> <div class=\"media-left media-middle unit-margin\"> <span _t>TB</span> </div> </div> </div>";

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var finishStep_directive_1 = __webpack_require__(110);
exports.default = function (module) {
    module.component("vimCpReportWizardFinishStep", finishStep_directive_1.FinishStepDirective);
};


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var finishStep_controller_1 = __webpack_require__(111);
/**
 * Directive wrapper for report wizard / finish step
 */
var FinishStepDirective = /** @class */ (function () {
    function FinishStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(112);
        this.controller = finishStep_controller_1.FinishStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            reportName: "=",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return FinishStepDirective;
}());
exports.FinishStepDirective = FinishStepDirective;
;


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Controller for last step of the wizard
 */
var FinishStepController = /** @class */ (function () {
    function FinishStepController() {
        var _this = this;
        this.$onInit = function () {
            _this.isErrorMessageVisible = false;
            _this.remoteControl.isValid = _this.isValid;
        };
        this.isValid = function () {
            _this.isErrorMessageVisible = !_this.reportName;
            return {
                isValid: !_this.isErrorMessageVisible
            };
        };
    }
    return FinishStepController;
}());
exports.FinishStepController = FinishStepController;
;


/***/ }),
/* 112 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-finish-step> <div class=row> <div class=col-xs-12> <h3 _t>READY TO CALCULATE</h3> </div> <div class=col-xs-12> <p _t>The planner has everything it needs to estimate your future usage.</p> </div> </div> <div class=row> <div class=col-xs-12> <h4 _t>Report Name</h4> </div> <div class=\"col-xs-5 report-name-input\"> <xui-textbox name=reportName ng-model=vm.reportName validators=required> </xui-textbox> <div ng-if=vm.isErrorMessageVisible class=sw-widget-error _t> Enter a report name. </div> </div> </div> <div class=row> <div class=col-xs-12 _t> If you have a large environment, or the planner is considering weeks or months of historical data, the report may take many hours to complete. When the report is ready, you can retrieve it from the Capacity Planning Reports page. </div> </div> </div> ";

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pickResourcesStep_directive_1 = __webpack_require__(114);
exports.default = function (module) {
    module.component("vimCpReportWizardPickResourcesStep", pickResourcesStep_directive_1.PickResourcesStepDirective);
};


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pickResourcesStep_controller_1 = __webpack_require__(115);
/**
 * Directive wrapper for report wizard / pick resources step
 */
var PickResourcesStepDirective = /** @class */ (function () {
    function PickResourcesStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(116);
        this.controller = pickResourcesStep_controller_1.PickResourcesStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            remoteControl: "<",
            counters: "=",
            computedEntities: "=",
        };
        this.scope = {};
    }
    return PickResourcesStepDirective;
}());
exports.PickResourcesStepDirective = PickResourcesStepDirective;
;


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(18);
var common_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
/**
 * Controller for pick resources step of the wizard
 */
var PickResourcesStepController = /** @class */ (function () {
    function PickResourcesStepController(_t, reportWizardService) {
        var _this = this;
        this._t = _t;
        this.reportWizardService = reportWizardService;
        this.countersEnum = common_1.Counters;
        this.$onInit = function () {
            _this.isBusy = false;
            _this.isErrorMessageVisible = false;
            _this.remoteControl.isValid = _this.isValid;
            _this.remoteControl.save = _this.save;
            if (_.isEmpty(_this.counters)) {
                _this.counters = (_a = {},
                    _a[common_1.Counters.cpu] = undefined,
                    _a[common_1.Counters.memory] = undefined,
                    _a[common_1.Counters.disk] = undefined,
                    _a);
            }
            _this.countersSelected = (_b = {},
                _b[common_1.Counters.cpu] = false,
                _b[common_1.Counters.memory] = false,
                _b[common_1.Counters.disk] = false,
                _b);
            _.each(Object.keys(_this.counters), function (counterName) {
                _this.countersSelected[counterName] = true;
            });
            // load counters info (thresholds)
            _this.isBusy = true;
            var _loop_1 = function (computed) {
                var clusterId = index_1.EntityHelper.parseInstanceId(computed.entityNetObjectId);
                _this.reportWizardService.getClusterCountersInfo(clusterId).then(function (result) {
                    computed.counters = result;
                });
            };
            for (var _i = 0, _c = _this.computedEntities; _i < _c.length; _i++) {
                var computed = _c[_i];
                _loop_1(computed);
            }
            _this.isBusy = false;
            var _a, _b;
        };
        this.isValid = function () {
            var isAtLeastOneSelected = _.some(_this.countersSelected, function (value) { return value === true; });
            _this.isErrorMessageVisible = !isAtLeastOneSelected;
            return {
                isValid: isAtLeastOneSelected
            };
        };
        this.save = function () {
            _.each(_this.countersSelected, function (value, key) {
                _this.mapCounter(value, key);
            });
        };
        this.mapCounter = function (counterEnabled, counterName) {
            if (!counterEnabled) {
                for (var i = 0; i < _this.computedEntities.length; i++) {
                    delete _this.computedEntities[i].counters[counterName];
                    delete _this.counters[counterName];
                }
                ;
            }
            ;
        };
    }
    PickResourcesStepController = __decorate([
        __param(0, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __param(1, di_1.Inject(index_2.ServiceNames.reportWizardService)),
        __metadata("design:paramtypes", [Function, Object])
    ], PickResourcesStepController);
    return PickResourcesStepController;
}());
exports.PickResourcesStepController = PickResourcesStepController;
;


/***/ }),
/* 116 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-pick-resources-step> <div class=row> <div class=col-xs-12> <h2 _t>RESOURCE EVALUATION</h2> </div> </div> <div class=row> <div class=col-xs-12> <p _t>The planner will consider the selected resources when allocating VMs:</p> </div> </div> <div class=row> <div class=\"col-xs-12 xui-margin-lgl\"> <xui-checkbox title=\"_t(CPU usage resource)\" ng-model=vm.countersSelected[vm.countersEnum.cpu] data-selector=cp-resources-cpu _ta> <span _t>CPU Usage</span> </xui-checkbox> <xui-checkbox title=\"_t(Memory usage resource)\" ng-model=vm.countersSelected[vm.countersEnum.memory] data-selector=cp-resources-memory _ta> <span _t>Memory usage</span> </xui-checkbox> <xui-checkbox title=\"_t(Disk space resource)\" ng-model=vm.countersSelected[vm.countersEnum.disk] data-selector=cp-resources-disk _ta> <span _t>Disk space</span> </xui-checkbox> <div ng-if=vm.isErrorMessageVisible class=sw-widget-error _t> Select a resource. </div> </div> </div> <div></div></div>";

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var advancedSettingsStep_directive_1 = __webpack_require__(118);
var index_1 = __webpack_require__(121);
var index_2 = __webpack_require__(125);
exports.default = function (module) {
    module.component("vimCpReportWizardAdvancedSettingsStep", advancedSettingsStep_directive_1.AdvancedSettingsStepDirective);
    index_2.default(module);
    index_1.default(module);
};


/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var advancedSettingsStep_controller_1 = __webpack_require__(119);
/**
 * Directive wrapper for report wizard / advanced settings step
 */
var AdvancedSettingsStepDirective = /** @class */ (function () {
    function AdvancedSettingsStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(120);
        this.controller = advancedSettingsStep_controller_1.AdvancedSettingsStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            remoteControl: "<",
            resourceAllocationType: "=",
            failoverReservation: "="
        };
        this.scope = {};
    }
    return AdvancedSettingsStepDirective;
}());
exports.AdvancedSettingsStepDirective = AdvancedSettingsStepDirective;
;


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var services_1 = __webpack_require__(2);
/**
 * Controller for advanced settings step of the wizard
 */
var AdvancedSettingsStepController = /** @class */ (function () {
    function AdvancedSettingsStepController(helpLinkService) {
        var _this = this;
        this.helpLinkService = helpLinkService;
        this.$onInit = function () {
            _this.childValidators = [];
            _this.remoteControl.isValid = _this.isValid;
            _this.helpUrl = _this.helpLinkService.getKbLink("MT99225");
        };
        this.addChildValidator = function (validator) {
            _this.childValidators.push(validator);
        };
        this.isValid = function () {
            var results = _.map(_this.childValidators, function (val) { return val(); });
            var isValid = _.every(results, function (res) { return res.isValid; });
            if (isValid) {
                return {
                    isValid: true
                };
            }
            return {
                isValid: false,
                errors: [].concat(_.map(results, function (res) { return res.errors; }))
            };
        };
    }
    AdvancedSettingsStepController.$inject = [services_1.ApolloServiceNames.helpLinkService];
    return AdvancedSettingsStepController;
}());
exports.AdvancedSettingsStepController = AdvancedSettingsStepController;
;


/***/ }),
/* 120 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-advanced-settings-step> <div class=row> <div class=col-xs-12> <xui-message type=info allow-dismiss=false> <div _t=\"['{{::vm.helpUrl}}']\"> <strong>Advanced Configuration.</strong> For most users, it's best to leave these options unchanged. <a target=_blank href={0}>&raquo; Learn More</a> </div> </xui-message> </div> </div> <div class=row> <div class=col-xs-8> <vim-cp-report-wizard-resource-allocation resource-allocation-type=vm.resourceAllocationType> </vim-cp-report-wizard-resource-allocation> </div> </div> <div class=row> <div class=col-xs-8> <vim-cp-report-wizard-failover-reservation failover-reservation=vm.failoverReservation> </vim-cp-report-wizard-failover-reservation> </div> </div> </div> ";

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var failoverReservation_directive_1 = __webpack_require__(122);
exports.default = function (module) {
    module.component("vimCpReportWizardFailoverReservation", failoverReservation_directive_1.FailoverReservationDirective);
};


/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var failoverReservation_controller_1 = __webpack_require__(123);
/**
 * Directive which displays failover reservation settings
 */
var FailoverReservationDirective = /** @class */ (function () {
    function FailoverReservationDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(124);
        this.controller = failoverReservation_controller_1.FailoverReservationController;
        this.controllerAs = "vm";
        this.bindToController = {
            failoverReservation: "="
        };
        this.scope = {};
        this.require = ["^^vimCpReportWizardAdvancedSettingsStep", "vimCpReportWizardFailoverReservation"];
        this.link = function ($scope, element, attrs, controllers) {
            var advCtl = controllers[0];
            var failoverCtl = controllers[1];
        };
    }
    return FailoverReservationDirective;
}());
exports.FailoverReservationDirective = FailoverReservationDirective;
;


/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(2);
/**
 * Controller for failover reservation settings
 *
 * @class
 */
var FailoverReservationController = /** @class */ (function () {
    function FailoverReservationController(xuiToastService) {
        var _this = this;
        this.xuiToastService = xuiToastService;
        this.$onInit = function () {
            _this.failoverReservation = _this._failoverReservation || 0;
        };
    }
    Object.defineProperty(FailoverReservationController.prototype, "failoverReservation", {
        get: function () {
            return this._failoverReservation;
        },
        set: function (value) {
            if (_.isUndefined(value)) {
                this._failoverReservation = 0;
                return;
            }
            if (!_.isInteger(value)) {
                if (this.xuiToastService) {
                    this.xuiToastService.warning("Invalid hosts reserved value!");
                }
            }
            else if (value >= 0) {
                this._failoverReservation = value;
            }
            else {
                if (this.xuiToastService) {
                    this.xuiToastService.warning("Hosts reserved value cannot be less than 0.");
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    FailoverReservationController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.toastService)),
        __metadata("design:paramtypes", [Object])
    ], FailoverReservationController);
    return FailoverReservationController;
}());
exports.FailoverReservationController = FailoverReservationController;


/***/ }),
/* 124 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-failover-reservation> <div class=row> <div class=col-xs-8> <h2 _t>Failover reservation<h2> </h2></h2></div> </div> <div class=row> <div class=col-xs-8 _t>The planner can ignore some number of hosts when calculating resource usage. Use this to estimate the performance impact of removing hosts from the resource pool. </div> </div> <div class=row> <div class=col-xs-8> <div class=\"xui-padding-lgl xui-padding-mdv\"> <xui-textbox type=number data-selector=failover-reservation-textbox ng-model=vm.failoverReservation ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" validators=\"required,min=0\" ng-change=vm.onChange() custom-box-width=130px suffix=\"_t(Hosts reserved)\" _ta> <div ng-message=min _t>Can't select negative value for failover hosts reservation.</div> </xui-textbox> </div> </div> </div> </div>";

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var resourceAllocation_directive_1 = __webpack_require__(126);
exports.default = function (module) {
    module.component("vimCpReportWizardResourceAllocation", resourceAllocation_directive_1.ResourceAllocationDirective);
};


/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var resourceAllocation_controller_1 = __webpack_require__(127);
/**
 * Directive wrapper for report wizard / advanced settings step
 */
var ResourceAllocationDirective = /** @class */ (function () {
    function ResourceAllocationDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(128);
        this.controller = resourceAllocation_controller_1.ResourceAllocationController;
        this.controllerAs = "vm";
        this.bindToController = {
            resourceAllocationType: "="
        };
        this.scope = {};
    }
    return ResourceAllocationDirective;
}());
exports.ResourceAllocationDirective = ResourceAllocationDirective;
;


/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(17);
/**
 * Controller for advanced settings step of the wizard
 */
var ResourceAllocationController = /** @class */ (function () {
    function ResourceAllocationController() {
        var _this = this;
        this.resourceAllocationTypeEnum = index_1.ResourceAllocationType;
        this.$onInit = function () {
            if (!_.isInteger(_this.resourceAllocationType)) {
                _this.resourceAllocationType = index_1.ResourceAllocationType.Balanced;
            }
        };
    }
    Object.defineProperty(ResourceAllocationController.prototype, "resourceAllocationTypeProxy", {
        // todo: UIF-6210
        get: function () {
            return this.resourceAllocationType.toString();
        },
        set: function (value) {
            this.resourceAllocationType = _.parseInt(value);
        },
        enumerable: true,
        configurable: true
    });
    return ResourceAllocationController;
}());
exports.ResourceAllocationController = ResourceAllocationController;
;


/***/ }),
/* 128 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-resource-allocation> <h2 _t>Resource allocation model</h2> <p _t>When estimating the footprint of VMs, the planner can use several different models.</p> <div class=xui-margin-lg> <div class=xui-margin-lgv data-selector=option-conservative> <xui-radio ng-model=vm.resourceAllocationTypeProxy value={{::vm.resourceAllocationTypeEnum.Conservative}} help-text=\"_t(Use the peak VM resource usage. Ensure there are sufficient resources for all VMs to operate at peak levels simultaneously, with no contention.)\" _ta> <span class=xui-text-l _t>Conservative</span> </xui-radio> </div> <div class=xui-margin-lgv data-selector=option-balanced> <xui-radio ng-model=vm.resourceAllocationTypeProxy value={{::vm.resourceAllocationTypeEnum.Balanced}} help-text=\"_t(Use the 95th percentile of VM resource usage measurements. This model balances VM performance and efficient resource allocation.)\" _ta> <span class=xui-text-l _t>Balanced</span> </xui-radio> </div> <div class=xui-margin-lgv data-selector=option-optimized> <xui-radio ng-model=vm.resourceAllocationTypeProxy value={{::vm.resourceAllocationTypeEnum.HighlyOptimized}} help-text=\"_t(Use the 75th percentile of VM resource usage measurements. This model maximizes resource allocation but may produce poor VM performance.)\" _ta> <span class=xui-text-l _t>Aggressively Optimized</span> </xui-radio> </div> </div> </div> ";

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var historicalAnalysisStep_directive_1 = __webpack_require__(130);
exports.default = function (module) {
    module.component("vimCpReportWizardHistoricalAnalysisStep", historicalAnalysisStep_directive_1.HistoricalAnalysisStep);
};


/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var historicalAnalysisStep_controller_1 = __webpack_require__(131);
/**
 * Directive wrapper for report wizard / start step
 */
var HistoricalAnalysisStep = /** @class */ (function () {
    function HistoricalAnalysisStep() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(132);
        this.controller = historicalAnalysisStep_controller_1.HistoricalAnalysisStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            utilizationPeriodInDays: "=",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return HistoricalAnalysisStep;
}());
exports.HistoricalAnalysisStep = HistoricalAnalysisStep;
;


/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
exports.UtilizationPeriodLimits = {
    upperLimit: 365,
    lowerLimit: 7,
    recommended: 30
};
var HistoricalAnalysisStepController = /** @class */ (function () {
    function HistoricalAnalysisStepController(xuiToastService, swUtil, _t) {
        var _this = this;
        this.xuiToastService = xuiToastService;
        this.swUtil = swUtil;
        this._t = _t;
        this.$onInit = function () {
            _this.utilizationPeriodLimits = exports.UtilizationPeriodLimits;
            _this.invalidPeriodMessage = _this._t("Entered Historical Period is out of range. Please specify a period between {0} and {1} days.");
            _this.invalidPeriodMessageFormatted = _this.swUtil.formatString(_this.invalidPeriodMessage, _this.utilizationPeriodLimits.lowerLimit, _this.utilizationPeriodLimits.upperLimit);
            _this.utilizationPeriodInDays = _this.utilizationPeriodInDays || _this.utilizationPeriodLimits.recommended;
        };
    }
    Object.defineProperty(HistoricalAnalysisStepController.prototype, "utilizationPeriodInDays", {
        get: function () {
            return this._utilizationPeriodInDays;
        },
        set: function (value) {
            if (!this.utilizationPeriodLimits) {
                this._utilizationPeriodInDays = value;
                return;
            }
            var showToast = false;
            if (!_.isInteger(value)) {
                value = this.utilizationPeriodLimits.recommended;
                showToast = true;
            }
            else if (value < this.utilizationPeriodLimits.lowerLimit) {
                value = this.utilizationPeriodLimits.lowerLimit;
                showToast = true;
            }
            else if (value > this.utilizationPeriodLimits.upperLimit) {
                value = this.utilizationPeriodLimits.upperLimit;
                showToast = true;
            }
            if (showToast) {
                this.xuiToastService.warning(this.invalidPeriodMessageFormatted);
            }
            this._utilizationPeriodInDays = value;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    HistoricalAnalysisStepController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.toastService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.swUtilService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Object, Function])
    ], HistoricalAnalysisStepController);
    return HistoricalAnalysisStepController;
}());
exports.HistoricalAnalysisStepController = HistoricalAnalysisStepController;


/***/ }),
/* 132 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-historical-analysis-step> <h2 _t>Sample period</h2> <p _t> The planner will examine usage trends on your real resources to predict future usage. As the planner learns more about your environment, it makes more accurate predictions. The planner requires at least 7 days. </p> <div> <xui-textbox type=number ng-model=vm.utilizationPeriodInDays ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" data-selector=utilization-period custom-box-width=160px help-text=\"_t(Recommended value is {{::vm.utilizationPeriodLimits.recommended}} days or more.)\" _ta> </xui-textbox> </div> </div>";

/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vmProfilesStep_directive_1 = __webpack_require__(134);
exports.default = function (module) {
    module.component("vimCpReportWizardVmProfilesStep", vmProfilesStep_directive_1.VmProfilesStepDirective);
};


/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(52);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vmProfilesStep_controller_1 = __webpack_require__(135);
/**
 * Directive wrapper for report wizard / vm profiles step
 */
var VmProfilesStepDirective = /** @class */ (function () {
    function VmProfilesStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(136);
        this.controller = vmProfilesStep_controller_1.VmProfilesStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            vmProfiles: "<",
            computedEntities: "<",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return VmProfilesStepDirective;
}());
exports.VmProfilesStepDirective = VmProfilesStepDirective;
;


/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(12);
var index_2 = __webpack_require__(2);
var di_1 = __webpack_require__(0);
var index_3 = __webpack_require__(18);
var index_4 = __webpack_require__(3);
var index_5 = __webpack_require__(20);
/**
 * Controller for vm profiles step of the wizard
 */
var VmProfilesStepController = /** @class */ (function () {
    function VmProfilesStepController($q, _t, wizardDialogService, xuiToastService, counterUnitFilter, swUtil) {
        var _this = this;
        this.$q = $q;
        this._t = _t;
        this.wizardDialogService = wizardDialogService;
        this.xuiToastService = xuiToastService;
        this.counterUnitFilter = counterUnitFilter;
        this.swUtil = swUtil;
        this.$onInit = function () {
            // wrap profiles with validation helper
            for (var i = 0; i < _this.vmProfiles.length; i++) {
                _this.vmProfiles[i] = new index_1.VmProfileValidationWrapper(_this.vmProfiles[i], _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
            }
            _this.remoteControl.save = _this.save;
        };
        this.isItemSelected = function () {
            return _.isEmpty(_this.selectedProfile);
        };
        this.save = function () {
            // convert VM profiles back to plain objects
            for (var i = 0; i < _this.vmProfiles.length; i++) {
                _this.vmProfiles[i] = _this.vmProfiles[i].profile;
            }
        };
        this.startEditVmProfileWizard = function (isNew) {
            var profile;
            var idx;
            if (isNew) {
                profile = {
                    name: _this._t("VM Profile"),
                    instanceCount: 0,
                    counters: (_a = {},
                        _a[index_4.Counters.cpuCores] = { value: 1, utilizationInPercent: index_4.ResourceLoad.Typical },
                        _a[index_4.Counters.memory] = { value: index_3.ConversionHelper.convertBytes(1, index_3.BytesUnit.GB, index_3.BytesUnit.B), utilizationInPercent: index_4.ResourceLoad.Typical },
                        _a[index_4.Counters.disk] = { value: index_3.ConversionHelper.convertBytes(1, index_3.BytesUnit.GB, index_3.BytesUnit.B), utilizationInPercent: index_4.ResourceLoad.Typical },
                        _a),
                    profileType: index_4.VmProfileTypes.Existing
                };
            }
            else {
                idx = _.findIndex(_this.vmProfiles, function (p) { return p.id === _this.selectedProfile[0].id; });
                profile = _this.vmProfiles[idx].profile;
                profile = _.cloneDeep(profile);
            }
            var ids = [];
            for (var _i = 0, _b = _this.computedEntities; _i < _b.length; _i++) {
                var computed = _b[_i];
                ids.push(computed.entityNetObjectId);
            }
            var viewModel = {
                profile: profile,
                parentEntityNetObjectIds: ids,
                profileType: isNew || !!profile.virtualMachineId ? index_4.VmProfileTypes.Existing : index_4.VmProfileTypes.Custom
            };
            return _this.openVmProfileWizardDialog(isNew, viewModel).then(function (res) {
                if (!viewModel.resultProfile) {
                    return;
                }
                if (isNew) {
                    _this.vmProfiles.push(new index_1.VmProfileValidationWrapper(viewModel.resultProfile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter));
                    var profileCreatedMessage = _this._t("Successfully created VM profile \"{0}\".");
                    _this.selectedProfile.splice(0, 1, _.last(_this.vmProfiles));
                    var profileCreatedMessageFormatted = _this.swUtil.formatString(profileCreatedMessage, viewModel.resultProfile.name);
                    _this.xuiToastService.success(_this._t(profileCreatedMessageFormatted));
                }
                else {
                    _this.vmProfiles[idx] = new index_1.VmProfileValidationWrapper(viewModel.resultProfile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
                    _this.selectedProfile.splice(0, 1, _this.vmProfiles[idx]);
                    var profileCreatedMessage = _this._t("Successfully modified VM profile \"{0}\".");
                    var profileCreatedMessageFormatted = _this.swUtil.formatString(profileCreatedMessage, viewModel.resultProfile.name);
                    _this.xuiToastService.success(_this._t(profileCreatedMessageFormatted));
                }
            });
            var _a;
        };
        this.openVmProfileWizardDialog = function (isNew, viewModel) {
            var wizardOptions = {
                title: isNew ? _this._t("Add VM Profile").toUpperCase() : _this._t("Edit VM Profile").toUpperCase(),
                templateUrl: "vm-profile-wizard-template",
                viewModel: viewModel
            };
            return _this.wizardDialogService.showModal(wizardOptions);
        };
    }
    VmProfilesStepController = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_2.ApolloServiceNames.wizardDialogService)),
        __param(3, di_1.Inject(index_2.ApolloServiceNames.toastService)),
        __param(4, di_1.Inject(index_5.FilterFullNames.counterUnitFilter)),
        __param(5, di_1.Inject(index_2.ApolloServiceNames.swUtilService)),
        __metadata("design:paramtypes", [Function, Function, Object, Object, Function, Object])
    ], VmProfilesStepController);
    return VmProfilesStepController;
}());
exports.VmProfilesStepController = VmProfilesStepController;


/***/ }),
/* 136 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-vm-profiles-step> <p _t> VM profiles are used to estimate remaining capacity. The planner will tell you how many of each type of VM you can add to the cluster. You can also simulate adding new VMs to the cluster by adding simulated instances of a profile. </p> <xui-divider></xui-divider> <div> <xui-button class=xui-padding-sm ng-click=vm.startEditVmProfileWizard(true) display-style=tertiary data-selector=add-vm-profile-button icon=add _t>ADD</xui-button> <xui-button class=xui-padding-sm ng-click=vm.startEditVmProfileWizard(false) display-style=tertiary is-disabled=vm.isItemSelected() data-selector=edit-vm-profile-button icon=edit _t>EDIT</xui-button> </div> <xui-divider></xui-divider> <xui-listview stripe=true items-source=vm.vmProfiles template-url=vm-profile-template selection=vm.selectedProfile selection-mode=single track-by=id> </xui-listview> <script type=text/ng-template id=vm-profile-template> <div class=\"row vm-profile\">\n            <!-- name -->\n            <div class=\"col-xs-4 name\" data-selector=\"name\">\n                {{item.name}}\n            </div>\n            <!-- counter parameters -->\n            <div class=\"col-xs-5\">\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <!-- cpu -->\n                        <div class=\"col-xs-4\">\n                            <div class=\"media\">\n                                <div class=\"media-left\">\n                                    <xui-icon icon=\"cpu\" icon-size=\"small\"></xui-icon>\n                                </div>\n                                <div class=\"media-body\" data-selector=\"cpu-core-count\">\n                                    {{item.counters[\"cpuCores\"].value | vimCounterFormat: \"cores\"}}\n                                </div>\n                            </div>\n                        </div>\n\n                        <!-- memory -->\n                        <div class=\"col-xs-4\">\n                            <div class=\"media\">\n                                <div class=\"media-left\">\n                                    <xui-icon icon=\"memory\" icon-size=\"small\"></xui-icon>\n                                </div>\n                                <div class=\"media-body\" data-selector=\"memory-capacity\">\n                                    {{item.counters[\"memory\"].value | vimCounterFormat: \"bytes\"}}\n                                </div>\n                            </div>\n                        </div>\n\n                        <!-- disk space -->\n                        <div class=\"col-xs-4\">\n                            <div class=\"media\">\n                                <div class=\"media-left\">\n                                    <xui-icon icon=\"harddrive\" icon-size=\"small\"></xui-icon>\n                                </div>\n                                <div class=\"media-body\" data-selector=\"disk-space-capacity\">\n                                    {{item.counters[\"disk\"].value | vimCounterFormat: \"bytes\"}}\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <!-- instance count -->\n            <div class=\"col-xs-3\">\n                <xui-textbox ng-model=\"item.instanceCount\"\n                             ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\"\n                             class=\"instance-count\"\n                             type=\"number\"\n                             help-text=\"_t(Simulated instances)\"\n                             _ta>\n                </xui-textbox>\n            </div>\n        </div> </script> <script type=text/ng-template id=vm-profile-wizard-template> <xui-dialog>\n            <vim-cp-report-wizard-vm-profile-wizard\n                view-model=\"vm.dialogOptions.viewModel\">\n            </vim-cp-report-wizard-vm-profile-wizard>\n        </xui-dialog> </script> </div> ";

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileWizard_directive_1 = __webpack_require__(138);
var index_1 = __webpack_require__(141);
exports.default = function (module) {
    module.component("vimCpReportWizardVmProfileWizard", vmProfileWizard_directive_1.VmProfileWizardDirective);
    index_1.default(module);
};


/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileWizard_controller_1 = __webpack_require__(139);
/**
 * Directive wrapper for VM profiles wizard dialog
 */
var VmProfileWizardDirective = /** @class */ (function () {
    function VmProfileWizardDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(140);
        this.controller = vmProfileWizard_controller_1.VmProfileWizardController;
        this.controllerAs = "vm";
        this.bindToController = {
            viewModel: "<"
        };
        this.scope = {};
    }
    return VmProfileWizardDirective;
}());
exports.VmProfileWizardDirective = VmProfileWizardDirective;


/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileTypes_1 = __webpack_require__(22);
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
/**
 * Controller for VM profiles wizard dialog directive.
 */
var VmProfileWizardController = /** @class */ (function () {
    function VmProfileWizardController($q, _t, reportWizardService, routingService) {
        var _this = this;
        this.$q = $q;
        this._t = _t;
        this.reportWizardService = reportWizardService;
        this.routingService = routingService;
        this.selectVmProfileTypeStep = {
            label: "selectVmProfileTypeStep",
            templateUrl: "vm-profile-type-step"
        };
        this.customVmProfileStep = {
            label: "customVmProfileStep",
            templateUrl: "custom-vm-profile-step"
        };
        this.existingVmProfileStep = {
            label: "existingVmProfileStep",
            templateUrl: "existing-vm-profile-step"
        };
        this.$onInit = function () {
            _this.entityNetObjectIds = [];
            _this.steps = [
                _this.selectVmProfileTypeStep
            ];
            _this.currentStepRemoteControl = { isValid: undefined, canFinish: undefined };
            _this.profileType = _this.viewModel.profileType || vmProfileTypes_1.VmProfileTypes.Existing;
            _this.profile = _.clone(_this.viewModel.profile);
            _this.entityNetObjectIds = _this.viewModel.parentEntityNetObjectIds;
        };
        this.getNextStep = function (profileType) {
            if (profileType === vmProfileTypes_1.VmProfileTypes.Custom) {
                return _this.customVmProfileStep;
            }
            return _this.existingVmProfileStep;
        };
        this.onFinish = function () {
            if (!_this.validateFinalStep()) {
                return _this.$q.resolve(false);
            }
            _this.viewModel.resultProfile = _this.profile;
            return _this.$q.resolve(true);
        };
        this.onCancel = function () {
            _this.viewModel.resultProfile = undefined;
            return true;
        };
        this.onNextStep = function () {
            return _this.$q.resolve(_this.validateCurrentStep());
        };
        this.validateFinalStep = function () {
            if (!angular.isFunction(_this.currentStepRemoteControl.canFinish)) {
                return true;
            }
            return _this.currentStepRemoteControl.canFinish();
        };
        this.validateCurrentStep = function () {
            if (!angular.isFunction(_this.currentStepRemoteControl.isValid)) {
                return true;
            }
            return _this.currentStepRemoteControl.isValid();
        };
    }
    Object.defineProperty(VmProfileWizardController.prototype, "profileType", {
        get: function () {
            return this._profileType;
        },
        set: function (value) {
            if (value === this._profileType) {
                return;
            }
            this.currentStepRemoteControl.canFinish = undefined;
            this.steps.splice(1, 1, this.getNextStep(value));
            this._profileType = value;
        },
        enumerable: true,
        configurable: true
    });
    VmProfileWizardController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_1.ServiceNames.reportWizardService)),
        __param(3, di_1.Inject(index_1.ServiceNames.routingService)),
        __metadata("design:paramtypes", [Function, Function, Object, Object])
    ], VmProfileWizardController);
    return VmProfileWizardController;
}());
exports.VmProfileWizardController = VmProfileWizardController;


/***/ }),
/* 140 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-vm-profile-wizard> <xui-wizard name=vmProfileWizard hide-header=true on-finish=vm.onFinish() on-cancel=vm.onCancel() on-next-step=vm.onNextStep() finish-text=_t(Save) ng-model=vm _ta> <xui-wizard-step ng-repeat=\"step in vm.steps track by step.label\" label={{::step.label}}> <ng-include src=::step.templateUrl></ng-include> </xui-wizard-step> </xui-wizard> <script type=text/ng-template id=vm-profile-type-step> <vim-cp-report-wizard-vm-profile-selection-step\n            remote-control=\"vm.currentStepRemoteControl\"\n            profile-type=\"vm.profileType\"\n            profile-name=\"vm.profile.name\">\n        </vim-cp-report-wizard-vm-profile-selection-step> </script> <script type=text/ng-template id=custom-vm-profile-step> <vim-cp-report-wizard-new-custom-vm-profile-step\n            remote-control=\"vm.currentStepRemoteControl\"\n            profile=\"vm.profile\">\n        </vim-cp-report-wizard-new-custom-vm-profile-step> </script> <script type=text/ng-template id=existing-vm-profile-step> <vim-cp-report-wizard-select-existing-vm-profile-step\n            profile=\"vm.profile\"\n            remote-control=\"vm.currentStepRemoteControl\"\n            entity-net-object-ids=\"vm.entityNetObjectIds\">\n        </vim-cp-report-wizard-select-existing-vm-profile-step> </script> </div>";

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(142);
var index_2 = __webpack_require__(146);
var index_3 = __webpack_require__(150);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
};


/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var newCustomProfileStep_directive_1 = __webpack_require__(143);
exports.default = function (module) {
    module.component("vimCpReportWizardNewCustomVmProfileStep", newCustomProfileStep_directive_1.NewCustomProfileStep);
};


/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(53);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var newCustomProfileStep_controller_1 = __webpack_require__(144);
/**
 * Directive for defining a custom VM spec
 */
var NewCustomProfileStep = /** @class */ (function () {
    function NewCustomProfileStep() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(145);
        this.controller = newCustomProfileStep_controller_1.NewCustomProfileStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            profile: "<",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return NewCustomProfileStep;
}());
exports.NewCustomProfileStep = NewCustomProfileStep;


/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(3);
var helpers_1 = __webpack_require__(18);
var models_1 = __webpack_require__(12);
var index_2 = __webpack_require__(1);
var di_1 = __webpack_require__(0);
var index_3 = __webpack_require__(20);
/**
 * Controller for custom VM profile creation
 *
 * @class
 */
var NewCustomProfileStepController = /** @class */ (function () {
    function NewCustomProfileStepController(_t, swUtil, xuiToastService, counterUnitFilter) {
        var _this = this;
        this._t = _t;
        this.swUtil = swUtil;
        this.xuiToastService = xuiToastService;
        this.counterUnitFilter = counterUnitFilter;
        this.memoryCapacityUnit = helpers_1.BytesUnit.GB;
        this.diskSpaceUnit = helpers_1.BytesUnit.GB;
        this.$onInit = function () {
            _this.profile.virtualMachineId = undefined;
            _this.profileWrapper = new models_1.VmProfileValidationWrapper(_this.profile, _this.swUtil, _this._t, _this.xuiToastService, _this.counterUnitFilter);
            _this.resourceLoadItems = [
                { resourceLoad: index_1.ResourceLoad[index_1.ResourceLoad.Low], header: _this._t("Low"), description: _this._t("Average load is less than 10% of configured resources.") },
                { resourceLoad: index_1.ResourceLoad[index_1.ResourceLoad.Typical], header: _this._t("Typical"), description: _this._t("Average load is around 50% of configured resources.") },
                { resourceLoad: index_1.ResourceLoad[index_1.ResourceLoad.HighLoad], header: _this._t("High load"), description: _this._t("Average load is greater than 85% of configured resources.") }
            ];
            _this.cpuResourceLoadSelected = _this.getDropDownItem(_this.profileWrapper.counters[index_1.Counters.cpuCores].utilizationInPercent);
            _this.memoryResourceLoadSelected = _this.getDropDownItem(_this.profileWrapper.counters[index_1.Counters.memory].utilizationInPercent);
            _this.diskSpaceResourceLoadSelected = _this.getDropDownItem(_this.profileWrapper.counters[index_1.Counters.disk].utilizationInPercent);
        };
        this.onCpuLoadChanged = function (newValue, oldValue) {
            _this.profileWrapper.counters[index_1.Counters.cpuCores].utilizationInPercent = index_1.ResourceLoad[newValue.resourceLoad];
        };
        this.onMemoryLoadChanged = function (newValue, oldValue) {
            _this.profileWrapper.counters[index_1.Counters.memory].utilizationInPercent = index_1.ResourceLoad[newValue.resourceLoad];
        };
        this.onDiskSpaceLoadChanged = function (newValue, oldValue) {
            _this.profileWrapper.counters[index_1.Counters.disk].utilizationInPercent = index_1.ResourceLoad[newValue.resourceLoad];
        };
    }
    Object.defineProperty(NewCustomProfileStepController.prototype, "cpuCoreCount", {
        get: function () {
            return this.profileWrapper.cpuCores;
        },
        set: function (value) {
            this.profileWrapper.cpuCores = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewCustomProfileStepController.prototype, "memoryCapacity", {
        get: function () {
            return helpers_1.ConversionHelper.convertBytes(this.profileWrapper.memory, helpers_1.BytesUnit.B, this.memoryCapacityUnit);
        },
        set: function (value) {
            this.profileWrapper.memory = helpers_1.ConversionHelper.convertBytes(value, this.memoryCapacityUnit, helpers_1.BytesUnit.B);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewCustomProfileStepController.prototype, "diskSpaceCapacity", {
        get: function () {
            return helpers_1.ConversionHelper.convertBytes(this.profileWrapper.diskSpace, helpers_1.BytesUnit.B, this.diskSpaceUnit);
        },
        set: function (value) {
            this.profileWrapper.diskSpace = helpers_1.ConversionHelper.convertBytes(value, this.diskSpaceUnit, helpers_1.BytesUnit.B);
        },
        enumerable: true,
        configurable: true
    });
    NewCustomProfileStepController.prototype.getDropDownItem = function (value) {
        if (_.isUndefined(value) || value == null) {
            return this.resourceLoadItems[1];
        }
        return _.find(this.resourceLoadItems, function (x) { return x.resourceLoad === index_1.ResourceLoad[value]; });
    };
    NewCustomProfileStepController = __decorate([
        __param(0, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __param(1, di_1.Inject(index_2.ApolloServiceNames.swUtilService)),
        __param(2, di_1.Inject(index_2.ApolloServiceNames.toastService)),
        __param(3, di_1.Inject(index_3.FilterFullNames.counterUnitFilter)),
        __metadata("design:paramtypes", [Function, Object, Object, Function])
    ], NewCustomProfileStepController);
    return NewCustomProfileStepController;
}());
exports.NewCustomProfileStepController = NewCustomProfileStepController;


/***/ }),
/* 145 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-new-custom-profile-step> <div class=row> <div class=col-xs-12 _t> Specify the hardware for the profile <strong>{{vm.profileWrapper.name}}</strong> </div> </div> <div class=row> <div class=col-xs-7> <div class=row> <div class=col-xs-12> <h4 _t>Configured virtual resources</h4> </div> </div> <div class=row> <div class=\"col-xs-5 xui-margin-smv\"> <xui-icon icon=cpu icon-size=small></xui-icon> <strong class=xui-padding-sml _t>CPU</strong> </div> <div class=col-xs-7> <xui-textbox type=number ng-model=vm.cpuCoreCount ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" data-selector=cpu-cores-textbox custom-box-width=100px suffix=_t(cores) _ta> </xui-textbox> </div> </div> <div class=row> <div class=\"col-xs-5 xui-margin-smv\"> <xui-icon icon=memory icon-size=small></xui-icon> <strong class=xui-padding-sml _t>Memory</strong> </div> <div class=col-xs-7> <xui-textbox type=number ng-model=vm.memoryCapacity ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" data-selector=memory-textbox custom-box-width=100px suffix=_t(GB) _ta> </xui-textbox> </div> </div> <div class=row> <div class=\"col-xs-5 xui-margin-smv\"> <xui-icon icon=harddrive icon-size=small></xui-icon> <strong class=xui-padding-sml _t>Disk Space</strong> </div> <div class=col-xs-7> <xui-textbox type=number ng-model=vm.diskSpaceCapacity ng-model-options=\"{ updateOn: 'blur', debounce: { 'blur': 0 } }\" data-selector=storage-textbox custom-box-width=100px suffix=_t(GB) _ta> </xui-textbox> </div> </div> </div> <div class=\"col-xs-5 xui-dropdown--justified\"> <div class=row> <div class=col-xs-12> <h4 _t>Typical resource load</h4> </div> </div> <div class=row> <div class=col-xs-12 data-selector=cpu-dropdown> <xui-dropdown selected-item=vm.cpuResourceLoadSelected items-source=vm.resourceLoadItems display-value=header on-changed=\"vm.onCpuLoadChanged(newValue, oldValue)\" item-template-url=custom-dropdown-template> </xui-dropdown> </div> </div> <div class=row> <div class=col-xs-12 data-selector=memory-dropdown> <xui-dropdown selected-item=vm.memoryResourceLoadSelected items-source=vm.resourceLoadItems display-value=header on-changed=\"vm.onMemoryLoadChanged(newValue, oldValue)\" item-template-url=custom-dropdown-template> </xui-dropdown> </div> </div> <div class=row> <div class=col-xs-12 data-selector=disk-dropdown> <xui-dropdown selected-item=vm.diskSpaceResourceLoadSelected items-source=vm.resourceLoadItems display-value=header on-changed=\"vm.onDiskSpaceLoadChanged(newValue, oldValue)\" item-template-url=custom-dropdown-template> </xui-dropdown> </div> </div> </div> </div> <script id=custom-dropdown-template type=text/ng-template> <div class=\"media\" data-selector=\"{{::item.resourceLoad}}\">\n            <div class=\"media-body\">\n                <h3 class=\"media-heading\">{{::item.header}}</h3>\n                <span>{{::item.description}}</span>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileSelectionStep_directive_1 = __webpack_require__(147);
exports.default = function (module) {
    module.component("vimCpReportWizardVmProfileSelectionStep", vmProfileSelectionStep_directive_1.VmProfileSelectionStepDirective);
};


/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileSelectionStep_controller_1 = __webpack_require__(148);
/**
 * Directive wrapper for edit VM profile / VM profile selection step
 */
var VmProfileSelectionStepDirective = /** @class */ (function () {
    function VmProfileSelectionStepDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(149);
        this.controller = vmProfileSelectionStep_controller_1.VmProfileSelectionStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            profileName: "=",
            profileType: "=",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return VmProfileSelectionStepDirective;
}());
exports.VmProfileSelectionStepDirective = VmProfileSelectionStepDirective;


/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileTypes_1 = __webpack_require__(22);
/**
 * Controller for VM profile selection step
 */
var VmProfileSelectionStepController = /** @class */ (function () {
    function VmProfileSelectionStepController() {
        var _this = this;
        this.profileTypes = vmProfileTypes_1.VmProfileTypes;
        this.$onInit = function () {
            _this.isErrorMessageVisible = false;
            _this.remoteControl.isValid = _this.isValid;
        };
        this.isValid = function () {
            _this.isErrorMessageVisible = _.isUndefined(_this.profileName) || _this.profileName.length === 0;
            return !_this.isErrorMessageVisible;
        };
    }
    return VmProfileSelectionStepController;
}());
exports.VmProfileSelectionStepController = VmProfileSelectionStepController;


/***/ }),
/* 149 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-vm-profile-selection-step> <div class=row> <div class=col-xs-12> <span _t>A VM profile describes a typical VM in your environment. The profile is used to calculate:</span> </div> <div class=col-xs-12> <div class=row> <div class=col-xs-12> <ul style=list-style-type:square> <li class=xui-text-sml _t>The impact of adding additional VMs to your environment</li> <li class=xui-text-sml _t>The number of additional VMs you could create while maintaining acceptable performance</li> </ul> </div> </div> <xui-divider></xui-divider> <div class=row> <div class=col-xs-12> <div class=row> <div class=col-xs-5> <span class=xui-text-l _t>Profile name:</span> </div> </div> <div class=row> <div class=col-xs-5> <xui-textbox ng-model=vm.profileName validators=required> </xui-textbox> <div ng-if=vm.isErrorMessageVisible class=sw-widget-error _t> Enter a profile name. </div> </div> </div> </div> </div> <div class=row> <div class=col-xs-12> <div class=xui-margin-lg> <div class=xui-margin-lgv> <xui-radio data-selector=existingvm-radio ng-model=vm.profileType ng-value=vm.profileTypes.Existing help-text=\"_t(Pick a virtual machine from your environment. The VM profile will use the virtual hardware configuration of that VM.)\" _ta> <span class=xui-text-l _t>Use an existing virtual machine</span> </xui-radio> </div> <div class=xui-margin-lgv> <xui-radio data-selector=customvm-radio ng-model=vm.profileType ng-value=vm.profileTypes.Custom help-text=\"_t(Manually specify the hardware configuration.)\" _ta> <span class=xui-text-l _t>Build a custom profile</span> </xui-radio> </div> </div> </div> </div> </div> </div> </div>";

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var selectExistingProfileStep_directive_1 = __webpack_require__(151);
exports.default = function (module) {
    module.component("vimCpReportWizardSelectExistingVmProfileStep", selectExistingProfileStep_directive_1.SelectExistingProfileStep);
};


/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var selectExistingProfileStep_controller_1 = __webpack_require__(152);
/**
 * Directive for defining an existing virtual machine to VM profile
 */
var SelectExistingProfileStep = /** @class */ (function () {
    function SelectExistingProfileStep() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(153);
        this.controller = selectExistingProfileStep_controller_1.SelectExistingProfileStepController;
        this.controllerAs = "vm";
        this.bindToController = {
            profile: "=",
            entityNetObjectIds: "<",
            remoteControl: "<"
        };
        this.scope = {};
    }
    return SelectExistingProfileStep;
}());
exports.SelectExistingProfileStep = SelectExistingProfileStep;


/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(1);
var index_2 = __webpack_require__(3);
var di_1 = __webpack_require__(0);
/**
 * Controller for existing VM profile selection
 *
 * @class
 */
var SelectExistingProfileStepController = /** @class */ (function () {
    function SelectExistingProfileStepController($scope, _t, reportWizardService) {
        var _this = this;
        this.$scope = $scope;
        this._t = _t;
        this.reportWizardService = reportWizardService;
        this.$onInit = function () {
            _this.initProperties();
            _this.remoteControl.canFinish = _this.canFinish;
            _this.$scope.$watch(_this.getSelectedVirtualMachineEntityId, _this.handleSelectedVirtualMachineChanged);
            // initial load
            return _this.loadEntities();
        };
        this.canFinish = function () {
            _this.isErrorMessageVisible = _.isUndefined(_this.getSelectedVirtualMachineEntity());
            return !_this.isErrorMessageVisible;
        };
        this.getSelectedVirtualMachineEntity = function () {
            if (_.isEmpty(_this.selectedEntities.items)) {
                return undefined;
            }
            return _this.selectedEntities.items[0];
        };
        this.getSelectedVirtualMachineEntityId = function () {
            var selected = _this.getSelectedVirtualMachineEntity();
            return selected ? selected.id : undefined;
        };
        this.loadEntities = function () {
            var filter = {
                search: _this.search,
                pagination: _this.pagination,
                sorting: _this.sorting
            };
            _this.isBusy = true;
            return _this.reportWizardService.getVirtualMachinesByCluster(_this.entityNetObjectIds, filter)
                .then(function (result) {
                _this.virtualMachines = result.rows;
                _this.pagination.total = result.total;
            })
                .finally(function () {
                _this.isBusy = false;
            });
        };
        this.handleSelectedVirtualMachineChanged = function (newId, oldId) {
            // ignore initial callback
            if (angular.equals(newId, oldId)) {
                return;
            }
            if (!newId) {
                return;
            }
            var vm = _this.getSelectedVirtualMachineEntity();
            _this.profile.virtualMachineId = newId;
            _this.profile.counters = (_a = {},
                _a[index_2.Counters.cpuCores] = { value: vm.cpuCoreCount, utilizationInPercent: index_2.ResourceLoad.Typical },
                _a[index_2.Counters.memory] = { value: vm.memoryCapacity, utilizationInPercent: index_2.ResourceLoad.Typical },
                _a[index_2.Counters.disk] = { value: vm.diskSpaceCapacity, utilizationInPercent: index_2.ResourceLoad.Typical },
                _a);
            var _a;
        };
        this.onPaginationChange = function (page, pageSize) {
            _this.pagination.page = page;
            _this.pagination.pageSize = pageSize;
            return _this.loadEntities();
        };
        this.onSortingChange = function () {
            return _this.loadEntities();
        };
        this.onSearch = function (search) {
            _this.emptyData.title = _.isEmpty(search)
                ? _this._t("No virtual machines available.")
                : _this._t("No virtual machines matching the search criteria.");
            _this.search = search;
            return _this.loadEntities();
        };
    }
    SelectExistingProfileStepController.prototype.initProperties = function () {
        this.isErrorMessageVisible = false;
        this.isBusy = false;
        this.selectedEntities = { items: [], blacklist: false };
        this.items = [];
        this.options = {
            triggerSearchOnChange: true,
            searchDebounce: 500
        };
        this.search = undefined;
        this.pagination = { page: 1, pageSize: 5, total: undefined };
        this.virtualMachines = [];
        this.emptyData = {
            title: this._t("No virtual machines available.")
        };
        this.sortByVirtualMachineName = { id: "Name", label: this._t("Name") };
        this.sorting = {
            sortableColumns: [
                this.sortByVirtualMachineName
            ],
            sortBy: this.sortByVirtualMachineName,
            direction: "asc"
        };
    };
    ;
    SelectExistingProfileStepController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_1.ServiceNames.reportWizardService)),
        __metadata("design:paramtypes", [Object, Function, Object])
    ], SelectExistingProfileStepController);
    return SelectExistingProfileStepController;
}());
exports.SelectExistingProfileStepController = SelectExistingProfileStepController;


/***/ }),
/* 153 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-wizard-select-existing-vm-profile-step> <div class=\"row entities-grid\"> <div class=\"col-xs-12 xui-edge-definer\" xui-busy=vm.isBusy> <xui-grid items-source=vm.virtualMachines controller=vm options=vm.options track-by=id selection-mode=single selection=vm.selectedEntities show-selector=false hide-toolbar=true smart-mode=false empty-data=vm.emptyData template-url=vm-item-template row-padding=none on-search=vm.onSearch(item) pagination-data=vm.pagination on-pagination-change=\"vm.onPaginationChange(page, pageSize)\" sorting-data=::vm.sorting on-sorting-change=vm.onSortingChange()> </xui-grid> <div ng-if=vm.isErrorMessageVisible class=sw-widget-error _t> No virtual machine selected! </div> </div> </div> <script type=text/ng-template id=vm-item-template> <div class=\"row\">\n            <div class=\"col-xs-12\">{{::item.name}}</div>\n        </div> </script> </div>";

/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportDetail_directive_1 = __webpack_require__(155);
var index_1 = __webpack_require__(158);
var index_2 = __webpack_require__(162);
var index_3 = __webpack_require__(166);
var index_4 = __webpack_require__(170);
var index_5 = __webpack_require__(186);
var index_6 = __webpack_require__(190);
exports.default = function (module) {
    module.component("vimCpReportDetail", reportDetail_directive_1.ReportDetailDirective);
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
};


/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportDetail_controller_1 = __webpack_require__(156);
/**
 * Directive which displays report detail
 */
var ReportDetailDirective = /** @class */ (function () {
    function ReportDetailDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(157);
        this.controller = reportDetail_controller_1.ReportDetailController;
        this.controllerAs = "vm";
        this.bindToController = {
            reportDetail: "<"
        };
        this.scope = {};
    }
    return ReportDetailDirective;
}());
exports.ReportDetailDirective = ReportDetailDirective;
;


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
/**
 * Controller for report detail
 *
 * @class
 */
var ReportDetailController = /** @class */ (function () {
    function ReportDetailController($q, _t, routingService, reportDetailService) {
        var _this = this;
        this.$q = $q;
        this._t = _t;
        this.routingService = routingService;
        this.reportDetailService = reportDetailService;
        this.$onInit = function () {
            _this.initProperties();
        };
        this.getCounterSummaryTitle = function (counter) {
            switch (counter) {
                case index_1.Counters.cpu:
                    return _this._t("CPU Usage");
                case index_1.Counters.memory:
                    return _this._t("Memory Usage");
                case index_1.Counters.disk:
                    return _this._t("Disk Usage");
                default:
                    return counter;
            }
        };
        this.getCounterSummaryKey = function (counter) {
            switch (counter) {
                case index_1.Counters.cpu:
                    return index_1.Counters.cpuClock;
                default:
                    return counter;
            }
        };
        this.getTypicalOrSimulatedHost = function () {
            if (_this.useSimulatedHostsForAdditionalHosts()) {
                return _this.reportDetail.simulatedHostProfiles[0];
            }
            return _this.reportDetail.simulationResult.typicalHost;
        };
        this.useSimulatedHostsForAdditionalHosts = function () {
            return _this.reportDetail.simulatedResourcesEnabled && _this.reportDetail.simulatedHostProfiles != null && _this.reportDetail.simulatedHostProfiles.length > 0;
        };
        this.availableCapacityDisplayed = function () {
            return _this.reportDetail && _this.reportDetail.simulationResult.availableVmsCapacity.length > 0;
        };
        this.missingHostsDisplayed = function () {
            return _this.reportDetail && _.isInteger(_this.reportDetail.simulationResult.missingHostsCount);
        };
    }
    ReportDetailController.prototype.initProperties = function () {
        this.workloadsTitle = this._t("Workload");
        this.workloadsSubtitle = this._t("Aggregated data of VM workload usages in the cluster.");
        this.resourcesTitle = this._t("Resources");
        this.resourcesSubtitle = this._t("Aggregated data of host resources in the cluster.");
    };
    ;
    ReportDetailController = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_2.ServiceNames.routingService)),
        __param(3, di_1.Inject(index_2.ServiceNames.reportDetailService)),
        __metadata("design:paramtypes", [Function, Function, Object, Object])
    ], ReportDetailController);
    return ReportDetailController;
}());
exports.ReportDetailController = ReportDetailController;


/***/ }),
/* 157 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-detail> <div class=row> <div class=\"col-md-7 report-detail-workloads\"> <vim-cp-environment-summary summary-title=vm.workloadsTitle summary-subtitle=vm.workloadsSubtitle environment-summary=vm.reportDetail.simulationResult.workloads show-cpu-clock=false> </vim-cp-environment-summary> </div> <div class=\"col-md-5 report-detail-vm-profiles\" ng-if=vm.reportDetail.simulatedWorkloadsEnabled> <vim-cp-vm-profile-summary summary-title=\"_t(Simulated VMs)\" vm-profiles=vm.reportDetail.simulatedVmProfiles _ta> </vim-cp-vm-profile-summary> </div> </div> <div class=row> <div class=\"col-md-7 report-detail-resources\"> <vim-cp-environment-summary summary-title=vm.resourcesTitle summary-subtitle=vm.resourcesSubtitle environment-summary=vm.reportDetail.simulationResult.resources show-cpu-clock=true> </vim-cp-environment-summary> </div> <div class=\"col-md-5 report-detail-host-profiles\" ng-if=vm.reportDetail.simulatedResourcesEnabled> <vim-cp-host-profile-summary summary-title=\"_t(Simulated Hosts)\" host-profiles=vm.reportDetail.simulatedHostProfiles _ta> </vim-cp-host-profile-summary> </div> </div> <xui-divider></xui-divider> <div class=row ng-repeat=\"(key, value) in vm.reportDetail.counters track by $index\"> <div class=\"col-md-12 report-detail-counter-summary-item\"> <vim-cp-counter-summary report-computation-date=vm.reportDetail.computationDateStarted report-utilization-period=vm.reportDetail.utilizationPeriodInDays report-runway=vm.reportDetail.runway counter-type=key counter-title=vm.getCounterSummaryTitle(key) counter-depletion=vm.reportDetail.simulationResult.counterDepletions[key] counter-info=value counter-summary-resources=vm.reportDetail.simulationResult.resources[vm.getCounterSummaryKey(key)] counter-summary-workloads=vm.reportDetail.simulationResult.workloads[vm.getCounterSummaryKey(key)] counter-utilization=vm.reportDetail.simulationResult.counterUtilizations[key]> </vim-cp-counter-summary> </div> </div> <div ng-if=vm.availableCapacityDisplayed()> <xui-divider></xui-divider> <div class=row> <vim-cp-available-capacity vm-profiles=vm.reportDetail.simulationResult.availableVmsCapacity> </vim-cp-available-capacity> </div> </div> <div ng-if=vm.missingHostsDisplayed()> <xui-divider></xui-divider> <div class=row> <vim-cp-additional-hosts host-profile=vm.getTypicalOrSimulatedHost() resource-simulation-enabled=vm.useSimulatedHostsForAdditionalHosts() report-runway=vm.reportDetail.runway required-host-count=vm.reportDetail.simulationResult.missingHostsCount computation-date-started=vm.reportDetail.computationDateStarted> </vim-cp-additional-hosts> </div> </div> </div>";

/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var environmentSummary_directive_1 = __webpack_require__(159);
exports.default = function (module) {
    module.component("vimCpEnvironmentSummary", environmentSummary_directive_1.EnvironmentSummaryDirective);
};


/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(54);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var environmentSummary_controller_1 = __webpack_require__(160);
/**
 * Directive which displays Workloads and Resources
 */
var EnvironmentSummaryDirective = /** @class */ (function () {
    function EnvironmentSummaryDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(161);
        this.controller = environmentSummary_controller_1.EnvironmentSummaryController;
        this.controllerAs = "vm";
        this.bindToController = {
            summaryTitle: "<",
            summarySubtitle: "<",
            environmentSummary: "<",
            showCpuClock: "<"
        };
        this.scope = {};
    }
    return EnvironmentSummaryDirective;
}());
exports.EnvironmentSummaryDirective = EnvironmentSummaryDirective;
;


/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
/**
 * Controller for environment summary
 *
 * @class
 */
var EnvironmentSummaryController = /** @class */ (function () {
    function EnvironmentSummaryController($q, _t) {
        var _this = this;
        this.$q = $q;
        this._t = _t;
        this.cpuCoresRendered = false;
        this.getIcon = function (counter) {
            switch (counter) {
                case index_1.Counters.virtualMachines:
                    return "vmwareguest";
                case index_1.Counters.hosts:
                    return "vmwarehost";
                case index_1.Counters.cpuCores:
                case index_1.Counters.cpuClock:
                    return "cpu";
                case index_1.Counters.memory:
                    return "memory";
                case index_1.Counters.disk:
                    return "harddrive";
                default:
                    return "vmwareguest";
            }
        };
        this.getLabel = function (counter) {
            switch (counter) {
                case index_1.Counters.virtualMachines:
                    return _this._t("VMs:");
                case index_1.Counters.hosts:
                    return _this._t("Hosts:");
                case index_1.Counters.cpuCores:
                    return _this._t("CPU:");
                case index_1.Counters.memory:
                    return _this._t("RAM:");
                case index_1.Counters.disk:
                    return _this._t("Disk:");
                default:
                    return "";
            }
        };
        this.showLabel = function (counter) {
            if (counter === index_1.Counters.cpuClock) {
                return false;
            }
            return true;
        };
        this.getClass = function (key, index) {
            if (index === 0) {
                _this.cpuCoresRendered = false;
            }
            var returnClass = "";
            if (!_this.cpuCoresRendered) {
                if (index % 2 === 0) {
                    returnClass = "stripe";
                }
                if (key === index_1.Counters.cpuCores && _this.environmentSummary[index_1.Counters.cpuClock]) {
                    _this.cpuCoresRendered = true;
                }
            }
            else {
                if (index % 2 === 1) {
                    returnClass = "stripe";
                }
            }
            return returnClass;
        };
        this.getFormat = function (counter) {
            switch (counter) {
                case index_1.Counters.virtualMachines:
                case index_1.Counters.hosts:
                    return "";
                case index_1.Counters.memory:
                case index_1.Counters.disk:
                    return "bytes";
                case index_1.Counters.cpuCores:
                    return "cores";
                case index_1.Counters.cpuClock:
                    return "clock";
                default:
                    return "";
            }
        };
        this.showRecord = function (counter) {
            return (counter !== index_1.Counters.cpuClock || _this.showCpuClock);
        };
    }
    EnvironmentSummaryController = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __metadata("design:paramtypes", [Function, Function])
    ], EnvironmentSummaryController);
    return EnvironmentSummaryController;
}());
exports.EnvironmentSummaryController = EnvironmentSummaryController;


/***/ }),
/* 161 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-environment-summary> <h4 class=environment-summary-title>{{vm.summaryTitle}}</h4> <h5 class=environment-summary-subtitle>{{vm.summarySubtitle}}</h5> <table class=table> <thead> <tr> <th></th> <th _t>Current</th> <th _t>Projected Growth</th> <th _t>Total</th> </tr> </thead> <tbody> <tr ng-repeat=\"(key, value) in vm.environmentSummary track by $index\" ng-if=vm.showRecord(key) ng-class=\"vm.getClass(key, $index)\" class=environment-summary-item> <td> <div ng-if=vm.showLabel(key) class=\"media item-label\"> <div class=media-left> <xui-icon icon={{vm.getIcon(key)}} icon-size=small></xui-icon> </div> <div class=media-body> {{vm.getLabel(key)}} </div> </div> </td> <td class=item-current>{{value.current | vimCounterFormat: vm.getFormat(key)}}</td> <td class=item-simulated-growth>{{value.simulatedGrowth | vimCounterFormat: vm.getFormat(key)}}</td> <td class=item-total>{{value.total | vimCounterFormat: vm.getFormat(key)}}</td> </tr> </tbody> </table> </div> ";

/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var additionalHosts_directive_1 = __webpack_require__(163);
exports.default = function (module) {
    module.component("vimCpAdditionalHosts", additionalHosts_directive_1.AdditionalHostsDirective);
};


/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var additionalHosts_controller_1 = __webpack_require__(164);
/**
 * Directive which displays Workloads and Resources
 */
var AdditionalHostsDirective = /** @class */ (function () {
    function AdditionalHostsDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(165);
        this.controller = additionalHosts_controller_1.AdditionalHostsController;
        this.controllerAs = "vm";
        this.bindToController = {
            hostProfile: "<",
            resourceSimulationEnabled: "<",
            reportRunway: "<",
            requiredHostCount: "<",
            computationDateStarted: "<"
        };
        this.scope = {};
    }
    return AdditionalHostsDirective;
}());
exports.AdditionalHostsDirective = AdditionalHostsDirective;
;


/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
/**
 * Controller for environment summary
 *
 * @class
 */
var AdditionalHostsController = /** @class */ (function () {
    function AdditionalHostsController(swUtil, _t) {
        var _this = this;
        this.swUtil = swUtil;
        this._t = _t;
        this.$onInit = function () {
            _this.hostTitle = _this.getHostTitle();
            _this.hostSubtitle = _this.getHostSubtitle();
            _this.requiredHostCountText = _this.getRequiredHostCountText();
            _this.reportProjection = _this.getReportProjection();
            _this.hosts = [_this.hostProfile];
        };
        this.getComputationStartDate = function () {
            return moment(_this.computationDateStarted).startOf("day");
        };
        this.getRequiredHostCountText = function () {
            return _this.swUtil.formatString(_this._t("{0} more"), _this.requiredHostCount);
        };
        this.getHostTitle = function () {
            if (_this.resourceSimulationEnabled) {
                return _this._t("Simulated Host");
            }
            return _this._t("Typical Host");
        };
        this.getHostSubtitle = function () {
            if (_this.resourceSimulationEnabled) {
                return _this.swUtil.formatString(_this._t("defined as {0}"), _this.hostProfile.name);
            }
            return _this.swUtil.formatString(_this._t("similar to {0}"), _this.hostProfile.name);
        };
        this.getReportProjection = function () {
            if (_this.reportRunway === 0) {
                return _this._t("in 0 days");
            }
            var compDate = _this.getComputationStartDate();
            return compDate.to(compDate.clone().add(_this.reportRunway, "day"));
        };
    }
    AdditionalHostsController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.swUtilService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Function])
    ], AdditionalHostsController);
    return AdditionalHostsController;
}());
exports.AdditionalHostsController = AdditionalHostsController;


/***/ }),
/* 165 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-additional-hosts> <div class=\"xui-padding-lgh xui-padding-mdv\"> <div class=row> <div class=col-xs-12> <h4 class=additional-hosts-title _t>Additional Hosts Recommended</h4> </div> </div> <div class=row> <div class=col-xs-9> <xui-listview items-source=vm.hosts template-url=additional-hosts-template controller=vm stripe=false> </xui-listview> </div> </div> <script type=text/ng-template id=additional-hosts-template> <div class=\"row\">\n                    <div class=\"col-xs-7 xui-margin-lgl\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-7\">\n                                <strong data-selector=\"host-title\">{{vm.hostTitle}}</strong>\n                            </div>\n                            <div class=\"col-xs-7\">\n                                <em data-selector=\"host-subtitle\">{{vm.hostSubtitle}}</em>\n                            </div>\n                        </div>\n                        <div class=\"row vim-cp-ah-host-summary\">\n                            <div class=\"col-xs-10 media\">\n                                <div class=\"media-left\">\n                                    <xui-icon icon=\"cpu\" icon-size=\"small\"></xui-icon>\n                                </div>\n                                <div class=\"media-body\" data-selector=\"host-cpu\">\n                                    {{vm.hostProfile.cpuCoreCount | vimCounterFormat: \"cores\"}}\n                                </div>\n                                <div class=\"media-left\">\n                                    <xui-icon icon=\"memory\" icon-size=\"small\"></xui-icon>\n                                </div>\n                                <div class=\"media-body\" data-selector=\"host-memory\">\n                                    {{vm.hostProfile.memoryCapacity | vimCounterFormat: \"bytes\"}}\n                                </div>\n                                <div class=\"media-left\">\n                                    <xui-icon icon=\"harddrive\" icon-size=\"small\"></xui-icon>\n                                </div>\n                                <div class=\"media-body\" data-selector=\"host-disk\">\n                                    {{::vm.hostProfile.diskSpaceCapacity | vimCounterFormat: \"bytes\"}}\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                     <div class=\"pull-right col-xs-3 col-xs-offset-1 xui-margin-lgv xui-margin-lgl xui-padding-lgl\">\n                         <div class=\"row\">\n                            <span _t>Required:</span> <strong data-selector=\"hosts-required\">{{vm.requiredHostCountText}}</strong>\n                        </div>\n                     <div class=\"row\" data-selector=\"projection\">\n                         {{vm.reportProjection}}\n                    </div>\n                </div>\n            </div> </script> </div> </div> ";

/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var availableCapacity_directive_1 = __webpack_require__(167);
exports.default = function (module) {
    module.component("vimCpAvailableCapacity", availableCapacity_directive_1.AvailableCapacityDirective);
};


/***/ }),
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var availableCapacity_controller_1 = __webpack_require__(168);
/**
 * Directive which displays available capacity for given VM profiles
 */
var AvailableCapacityDirective = /** @class */ (function () {
    function AvailableCapacityDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(169);
        this.controller = availableCapacity_controller_1.AvailableCapacityController;
        this.controllerAs = "vm";
        this.bindToController = {
            vmProfiles: "<"
        };
        this.scope = {};
    }
    return AvailableCapacityDirective;
}());
exports.AvailableCapacityDirective = AvailableCapacityDirective;
;


/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var constants_1 = __webpack_require__(30);
var index_1 = __webpack_require__(1);
var AvailableCapacityController = /** @class */ (function () {
    function AvailableCapacityController(_t) {
        var _this = this;
        this._t = _t;
        this.getCounterConstraintText = function (counter) {
            switch (counter.toLowerCase()) {
                case constants_1.Counters.cpu:
                    return _this._t("CPU");
                case constants_1.Counters.memory:
                    return _this._t("Memory");
                case constants_1.Counters.disk:
                    return _this._t("Disk Space");
                default:
                    return counter;
            }
        };
    }
    AvailableCapacityController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __metadata("design:paramtypes", [Function])
    ], AvailableCapacityController);
    return AvailableCapacityController;
}());
exports.AvailableCapacityController = AvailableCapacityController;
;


/***/ }),
/* 169 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-available-capacity> <div class=\"xui-padding-lgh xui-padding-mdv\"> <div class=row> <div class=col-xs-12> <h4 class=available-capacity-title _t>Available Virtual Capacity</h4> </div> </div> <div class=row> <div class=col-xs-9> <xui-listview items-source=vm.vmProfiles template-url=available-capacity-template controller=vm row-padding=compact> </xui-listview> <div> </div> <script type=text/ng-template id=available-capacity-template> <div class=\"row vm-available-capacity\">\n                <div class=\"col-xs-7 xui-margin-lgl\">\n                    <div class=\"row\">\n                        <div class=\"col-xs-7 xui-margin-smv\">\n                            <strong data-selector=\"vm-name\">{{item.name}}</strong>\n                        </div>\n                    </div>\n                    <div class=\"row available-capacity\">\n                        <div class=\"col-xs-10 media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"cpu\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"cpu-cores-count\">\n                                {{item.cpuCoreCount | vimCounterFormat: \"cores\"}}\n                            </div>\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"memory\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"memory-capacity\">\n                                {{item.memoryCapacity | vimCounterFormat: \"bytes\"}}\n                            </div>\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"harddrive\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"disk-capacity\">\n                                {{item.diskSpaceCapacity | vimCounterFormat: \"bytes\"}}\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"pull-right col-xs-3 col-xs-offset-1 xui-margin-lgv xui-margin-lgl xui-padding-lgl\">\n                    <div class=\"row xui-margin-lgr\">\n                        <span  _t>Space for:</span> <strong data-selector=\"capacity-for\" _t=\"['{{item.availableVmsCount | number}}']\">{0} more</strong>\n                    </div>\n                    <div class=\"row xui-margin-lgr\">\n                        <span _t>Constraint:</span> <span data-selector=\"constraint\">{{vm.getCounterConstraintText(item.firstDepletedCounter)}}</span>\n                    </div>\n                </div>\n            </div> </script> </div> </div> </div></div>";

/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var counterSummary_directive_1 = __webpack_require__(171);
var index_1 = __webpack_require__(174);
var index_2 = __webpack_require__(178);
var index_3 = __webpack_require__(182);
exports.default = function (module) {
    module.component("vimCpCounterSummary", counterSummary_directive_1.CounterSummaryDirective);
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
};


/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var counterSummary_controller_1 = __webpack_require__(172);
/**
 * Directive which displays Workloads and Resources
 */
var CounterSummaryDirective = /** @class */ (function () {
    function CounterSummaryDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(173);
        this.controller = counterSummary_controller_1.CounterSummaryController;
        this.controllerAs = "vm";
        this.bindToController = {
            reportComputationDate: "<",
            reportUtilizationPeriod: "<",
            reportRunway: "<",
            counterTitle: "<",
            counterType: "<",
            counterDepletion: "<",
            counterInfo: "<",
            counterSummaryResources: "<",
            counterSummaryWorkloads: "<",
            counterUtilization: "<"
        };
        this.scope = {};
    }
    return CounterSummaryDirective;
}());
exports.CounterSummaryDirective = CounterSummaryDirective;
;


/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
/**
 * Controller for report detail
 *
 * @class
 */
var CounterSummaryController = /** @class */ (function () {
    function CounterSummaryController($q, _t, routingService, reportDetailService) {
        this.$q = $q;
        this._t = _t;
        this.routingService = routingService;
        this.reportDetailService = reportDetailService;
    }
    CounterSummaryController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(3, di_1.Inject(index_1.ServiceNames.reportDetailService)),
        __metadata("design:paramtypes", [Function, Function, Object, Object])
    ], CounterSummaryController);
    return CounterSummaryController;
}());
exports.CounterSummaryController = CounterSummaryController;


/***/ }),
/* 173 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-counter-summary> <h4 data-selector=counter-title>{{vm.counterTitle}}</h4> <div class=row> <div class=col-md-3 data-selector=pie-chart> <vim-cp-usage-pie-chart counter-type=vm.counterType counter-summary-resources=vm.counterSummaryResources counter-summary-workloads=vm.counterSummaryWorkloads> </vim-cp-usage-pie-chart> </div> <div class=col-md-9> <div class=row> <div class=col-md-12> </div> </div> <div class=row> <div class=\"col-md-12 counter-summary-depletions-chart\" data-selector=depletions-chart> <vim-cp-depletions-chart report-computation-date=vm.reportComputationDate report-utilization-period=vm.reportUtilizationPeriod report-runway=vm.reportRunway counter-info=vm.counterInfo counter-summary-resources=vm.counterSummaryResources counter-utilization=vm.counterUtilization> </vim-cp-depletions-chart> </div> <div class=\"col-md-12 counter-summary-depletions\"> <vim-cp-depletions counter-depletion=vm.counterDepletion counter-info=vm.counterInfo> </vim-cp-depletions> </div> </div> </div> </div> </div>";

/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var depletions_directive_1 = __webpack_require__(175);
exports.default = function (module) {
    module.component("vimCpDepletions", depletions_directive_1.DepletionsDirective);
};


/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(55);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var depletions_controller_1 = __webpack_require__(176);
/**
 * Directive which displays Workloads and Resources
 */
var DepletionsDirective = /** @class */ (function () {
    function DepletionsDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(177);
        this.controller = depletions_controller_1.DepletionsController;
        this.controllerAs = "vm";
        this.bindToController = {
            counterDepletion: "<",
            counterInfo: "<"
        };
        this.scope = {};
    }
    return DepletionsDirective;
}());
exports.DepletionsDirective = DepletionsDirective;
;


/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
/**
 * Controller for depletions
 *
 * @class
 */
var DepletionsController = /** @class */ (function () {
    function DepletionsController($q, _t, routingService, reportDetailService) {
        var _this = this;
        this.$q = $q;
        this._t = _t;
        this.routingService = routingService;
        this.reportDetailService = reportDetailService;
        this.isDurationInPast = function (duration) {
            return duration && duration.asMilliseconds() <= 0;
        };
        this.showActualRow = function () {
            return !_this.counterDepletion.actual;
        };
        this.showSimulationRow = function () {
            return !_this.counterDepletion.simulation;
        };
    }
    DepletionsController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __param(2, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(3, di_1.Inject(index_1.ServiceNames.reportDetailService)),
        __metadata("design:paramtypes", [Function, Function, Object, Object])
    ], DepletionsController);
    return DepletionsController;
}());
exports.DepletionsController = DepletionsController;


/***/ }),
/* 177 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-depletion> <table class=table> <thead class=depletion-header> <tr class=depletion-row> <th _t>Trend</th> <th class=row-warning _t=\"['{{vm.counterInfo.warningThreshold}}']\">Reaches Warning ({0}%)</th> <th class=row-critical _t=\"['{{vm.counterInfo.criticalThreshold}}']\">Reaches Critical ({0}%)</th> <th class=row-capacity _t=\"['{{vm.counterInfo.exhaustedThreshold}}']\">At Capacity ({0}%)</th> </tr> </thead> <tbody class=depletion-body> <tr class=depletion-row ng-if=vm.showActualRow> <td _t> Actual </td> <td class=row-warning ng-class=\"{'warning': vm.isDurationInPast(vm.counterDepletion.actual.warningThresholdDepletion)}\" data-selector=actual-warning-depletion> {{vm.counterDepletion.actual.warningThresholdDepletion | vimDurationFormat}} </td> <td class=row-critical ng-class=\"{'danger': vm.isDurationInPast(vm.counterDepletion.actual.criticalThresholdDepletion)}\" data-selector=actual-critical-depletion> {{vm.counterDepletion.actual.criticalThresholdDepletion | vimDurationFormat}} </td> <td class=row-capacity ng-class=\"{'capacity': vm.isDurationInPast(vm.counterDepletion.actual.capacityHitDepletion)}\" data-selector=actual-capacity-depletion> {{vm.counterDepletion.actual.capacityHitDepletion | vimDurationFormat}} </td> </tr> <tr class=depletion-row ng-if=vm.showSimulationRow> <td _t> Simulation </td> <td class=row-warning ng-class=\"{'warning': vm.isDurationInPast(vm.counterDepletion.simulation.warningThresholdDepletion)}\" data-selector=simulation-warning-depletion> {{vm.counterDepletion.simulation.warningThresholdDepletion | vimDurationFormat}} </td> <td class=row-critical ng-class=\"{'danger': vm.isDurationInPast(vm.counterDepletion.simulation.criticalThresholdDepletion)}\" data-selector=simulation-critical-depletion> {{vm.counterDepletion.simulation.criticalThresholdDepletion | vimDurationFormat}} </td> <td class=row-capacity ng-class=\"{'capacity': vm.isDurationInPast(vm.counterDepletion.simulation.capacityHitDepletion)}\" data-selector=simulation-capacity-depletion> {{vm.counterDepletion.simulation.capacityHitDepletion | vimDurationFormat}} </td> </tr> </tbody> </table> </div>";

/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var depletionsChart_directive_1 = __webpack_require__(179);
exports.default = function (module) {
    module.component("vimCpDepletionsChart", depletionsChart_directive_1.DepletionsChartDirective);
};


/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(56);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var depletionsChart_controller_1 = __webpack_require__(180);
/**
 * Directive for displaying counter depletions as a line chart
 */
var DepletionsChartDirective = /** @class */ (function () {
    function DepletionsChartDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(181);
        this.controller = depletionsChart_controller_1.DepletionsChartController;
        this.controllerAs = "vm";
        this.bindToController = {
            reportComputationDate: "<",
            reportUtilizationPeriod: "<",
            reportRunway: "<",
            counterInfo: "<",
            counterSummaryResources: "<",
            counterUtilization: "<"
        };
        this.scope = {};
    }
    return DepletionsChartDirective;
}());
exports.DepletionsChartDirective = DepletionsChartDirective;
;


/***/ }),
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(20);
var index_2 = __webpack_require__(2);
var index_3 = __webpack_require__(6);
var eventNames_1 = __webpack_require__(42);
/**
 * Enum of series ids
 *
 * @const
 */
exports.SeriesIds = {
    utilization: "utilization",
    utilizationHistoryTrend: "utilizationHistoryTrend",
    utilizationFutureTrend: "utilizationFutureTrend",
    simulationTrend: "simulationTrend"
};
var Trends;
(function (Trends) {
    Trends[Trends["utilizationTrend"] = 0] = "utilizationTrend";
    Trends[Trends["actualTrendHistorical"] = 1] = "actualTrendHistorical";
    Trends[Trends["actualTrendFuture"] = 2] = "actualTrendFuture";
    Trends[Trends["simulatedUtilizationTrend"] = 3] = "simulatedUtilizationTrend";
})(Trends || (Trends = {}));
/**
 * Controller for depletions
 *
 * @class
 */
var DepletionsChartController = /** @class */ (function () {
    function DepletionsChartController($scope, $rootScope, _t, dateTimeService, percentFilter) {
        var _this = this;
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this._t = _t;
        this.dateTimeService = dateTimeService;
        this.percentFilter = percentFilter;
        this._actualUtilizationTrendEnabled = true;
        this._actualTrendEnabled = true;
        this._simulatedUtilizationTrendEnabled = true;
        this.$onInit = function () {
            _this.setupAndCompute();
            _this.$rootScope.$on(eventNames_1.EventNames.reportResultChanged, _this.setupAndCompute);
        };
        this.$onDestroy = function () {
            if (_this.chartInstance) {
                _this.chartInstance.destroy();
            }
        };
        this.setupAndCompute = function () {
            _this.reportComputationDateOrionTz = _this.dateTimeService.moveMomentToOrionTimezone(_this.reportComputationDate).startOf("day");
            _this.minDate = moment(_this.reportComputationDateOrionTz).add("day", -_this.reportUtilizationPeriod);
            _this.minDateFormatted = _this.dateTimeService.formatMoment(_this.minDate, "longDate");
            _this.maxDate = moment(_this.reportComputationDateOrionTz).add("day", _this.reportRunway);
            _this.maxDateFormatted = _this.dateTimeService.formatMoment(_this.maxDate, "longDate");
            _this.setupHighcharts();
            _this.setupChartWatch();
        };
        this.isDataPointEmpty = function (value) {
            return value === "NaN";
        };
        this.convertUtilizationDataToPercentageData = function (data, resources) {
            return _.map(data, function (dataPoint) {
                if (_this.isDataPointEmpty(dataPoint)) {
                    return null;
                }
                return Math.round((dataPoint / resources) * 100);
            });
        };
        this.computeLinearRegressionPercentageValue = function (x, total, regression) {
            var y = _this.computeLinearRegressionValue(x, regression);
            return Math.round(y / total * 100);
        };
        this.computeLinearRegressionValue = function (x, regression) {
            return regression.b * x + regression.a;
        };
        this.createUtilizationSeries = function (id, data, minTick, totalResources) {
            // convert absolute to relative (%)
            var percentageData = _this.convertUtilizationDataToPercentageData(data, totalResources);
            // create pairs of [tick position, value %]
            var tickedPercentageData = _.map(percentageData, function (value, index) {
                return [minTick + index, value];
            });
            return {
                id: id,
                type: "line",
                data: tickedPercentageData,
                color: index_3.Colors.chartOrange,
                enableMouseTracking: false
            };
        };
        this.createTrendSeries = function (id, firstX, lastX, firstTick, lastTick, totalResources, trend, color, zIndex) {
            var firstY = _this.computeLinearRegressionPercentageValue(firstX, totalResources, trend);
            var lastY = _this.computeLinearRegressionPercentageValue(lastX, totalResources, trend);
            return {
                id: id,
                type: "line",
                data: [[firstTick, firstY], [lastTick, lastY]],
                color: color,
                dashStyle: "ShortDash",
                enableMouseTracking: false,
                zIndex: zIndex
            };
        };
        this.createComputationDatePlotLine = function (value) {
            return {
                color: index_3.Colors.lightGray,
                width: 0.8,
                value: value,
                zIndex: 5
            };
        };
        this.createThresholdPlotBands = function (warningThreshold, criticalThreshold) {
            return [{
                    from: warningThreshold,
                    to: criticalThreshold,
                    color: index_3.Colors.chartWarningBackground
                }, {
                    from: criticalThreshold,
                    to: 100,
                    color: index_3.Colors.chartCriticalBackground
                }];
        };
        this.createHorizontalTickPositions = function (minTick, middleTick, maxTick, fragmentsCount) {
            if (fragmentsCount === void 0) { fragmentsCount = 4; }
            // there is same count of negative/positive fragments
            var negativeFragment = Math.round(Math.abs(Math.abs(middleTick) - Math.abs(minTick)) / fragmentsCount);
            var positiveFragment = Math.round(Math.abs(Math.abs(maxTick) - Math.abs(middleTick)) / fragmentsCount);
            // create array of ticks, minTick...middleTick...maxTick
            var positions = [];
            // add values minTick...(last tick < middleTick)
            for (var i = 0; i < fragmentsCount; i++) {
                positions.push(minTick + (i * negativeFragment));
            }
            // add values middleTick...(last tick < maxTick)
            for (var i = 0; i < fragmentsCount; i++) {
                positions.push(middleTick + (i * positiveFragment));
            }
            positions.push(maxTick);
            return positions;
        };
        this.createHorizontalTickLabels = function (tickPositions, computationTickPosition) {
            var orionToday = _this.dateTimeService.getOrionNowMoment().startOf("day");
            var monthOnlyFormat = "MMM";
            var monthDayFormat = "MMM D";
            var dateFormat = "ll";
            var labels = [];
            // history labels (min tick...computation-1)
            var displayOnlyMonthsInHistory = _this.reportComputationDateOrionTz.diff(_this.minDate, "month") >= 3;
            var historyTicksCount = _.filter(tickPositions, function (tickPosition) { return tickPosition < computationTickPosition; }).length;
            var historyTickOffset = Math.round(_this.reportUtilizationPeriod / historyTicksCount);
            for (var i = 0; i < historyTicksCount; i++) {
                // first is empty
                if (i === 0) {
                    labels.push("");
                    continue;
                }
                var date = moment(_this.minDate).add("day", i * historyTickOffset);
                if (displayOnlyMonthsInHistory) {
                    labels.push(date.format(monthOnlyFormat));
                }
                else {
                    labels.push(date.format(monthDayFormat));
                }
            }
            // computation date
            if (orionToday.valueOf() === _this.reportComputationDateOrionTz.valueOf()) {
                labels.push(_this._t("TODAY"));
            }
            else {
                labels.push(_this.reportComputationDateOrionTz.format(dateFormat));
            }
            // future labels (computation+1...last tick)
            var displayOnlyMonthsInFuture = _this.reportComputationDateOrionTz.diff(_this.maxDate, "month") <= -3;
            var futureTicksCount = _.filter(tickPositions, function (tick) { return tick > computationTickPosition; }).length;
            var futureTickOffset = Math.round(_this.reportRunway / futureTicksCount);
            for (var i = 1; i <= futureTicksCount; i++) {
                // last tick is empty
                if (i === futureTicksCount) {
                    labels.push("");
                }
                var date = moment(_this.reportComputationDateOrionTz).add("day", i * futureTickOffset);
                if (displayOnlyMonthsInFuture) {
                    labels.push(date.format(monthOnlyFormat));
                }
                else {
                    labels.push(date.format(dateFormat));
                }
            }
            return labels;
        };
        this.createVerticalTickPositions = function () {
            return [0, 25, 50, 75, 100];
        };
        this.createVerticalTickLabels = function (tickPositions) {
            return _.map(tickPositions, function (value) {
                if (value === 0) {
                    return "";
                }
                return _this.percentFilter(value, 0);
            });
        };
        this.createSeries = function (settings, includeUtilization, includeUtilizationTrend, includeSimulationTrend) {
            if (includeUtilization === void 0) { includeUtilization = true; }
            if (includeUtilizationTrend === void 0) { includeUtilizationTrend = true; }
            if (includeSimulationTrend === void 0) { includeSimulationTrend = true; }
            var series = [];
            if (includeUtilization) {
                var dataReversed = _.reverse(_this.counterUtilization.utilization.utilization.data);
                var utilizationSeries = _this.createUtilizationSeries(exports.SeriesIds.utilization, dataReversed, settings.minTick, _this.counterSummaryResources.current);
                series.push(utilizationSeries);
            }
            if (includeUtilizationTrend) {
                var utilizationHistoryTrendSeries = _this.createTrendSeries(exports.SeriesIds.utilizationHistoryTrend, (-settings.utilizationDataLength * settings.utilizationGranularity) + 1, 0, settings.minTick, settings.middleTick, _this.counterSummaryResources.current, _this.counterUtilization.utilizationTrend, index_3.Colors.chartUltramarine, 2);
                var utilizationFutureTrendSeries = _this.createTrendSeries(exports.SeriesIds.utilizationFutureTrend, 0, settings.simulationDataLength * settings.utilizationGranularity, settings.middleTick, settings.maxTick, _this.counterSummaryResources.current, _this.counterUtilization.utilizationTrend, index_3.Colors.chartUltramarine, 2);
                series.push(utilizationHistoryTrendSeries);
                series.push(utilizationFutureTrendSeries);
            }
            // simulation trend if available
            if (includeSimulationTrend && _this.simulatedUtilizationTrendAvailable) {
                var simulationTrendSeries = _this.createTrendSeries(exports.SeriesIds.simulationTrend, 0, settings.simulationDataLength * settings.utilizationGranularity, settings.middleTick, settings.maxTick, _this.counterSummaryResources.simulatedTotal, _this.counterUtilization.simulatedUtilizationTrend, index_3.Colors.chartUltramarineLight, 1);
                series.push(simulationTrendSeries);
            }
            return series;
        };
        this.createSettings = function () {
            var utilizationDataLength = _this.counterUtilization.utilization.utilization.data.length;
            // to make simulation trend proportional, we need to compute amount of simulation x points
            //    so ratio (data points/day) is preserved
            var simulationDataLength = Math.round((_this.reportRunway * utilizationDataLength) / _this.reportUtilizationPeriod);
            // +1/-1 done since data contains value for computation date (tick=0)
            var minTick = -utilizationDataLength + 1;
            var middleTick = 0;
            var maxTick = utilizationDataLength - 1;
            return {
                minTick: minTick,
                middleTick: middleTick,
                maxTick: maxTick,
                utilizationDataLength: utilizationDataLength,
                utilizationGranularity: _this.counterUtilization.utilization.utilizationGranularity,
                simulationDataLength: simulationDataLength
            };
        };
        this.setupHighcharts = function () {
            var settings = _this.createSettings();
            var series = _this.createSeries(settings);
            var xPlotLines = [].concat(_this.createComputationDatePlotLine(settings.middleTick));
            var yPlotBands = [].concat(_this.createThresholdPlotBands(_this.counterInfo.warningThreshold, _this.counterInfo.criticalThreshold));
            var xTickPositions = _this.createHorizontalTickPositions(settings.minTick, settings.middleTick, settings.maxTick);
            var xTickLabels = _this.createHorizontalTickLabels(xTickPositions, settings.middleTick);
            var yTickPositions = _this.createVerticalTickPositions();
            var yTickLabels = _this.createVerticalTickLabels(yTickPositions);
            _this.chartConfig = {
                tag: _.uniqueId("reportChart_"),
                type: "line",
                chart: {
                    plotBorderColor: index_3.Colors.lightGray,
                    plotBorderWidth: 2,
                    height: 260
                },
                legend: {
                    enabled: false
                },
                xAxis: [{
                        type: "linear",
                        tickPositions: xTickPositions,
                        startOnTick: true,
                        endOnTick: true,
                        plotLines: xPlotLines,
                        labels: {
                            y: 25,
                            style: {
                                whiteSpace: "nowrap"
                            },
                            formatter: function () {
                                var idx = _.indexOf(xTickPositions, this.value);
                                return xTickLabels[idx];
                            }
                        }
                    }],
                yAxis: {
                    type: "linear",
                    tickPositions: yTickPositions,
                    startOnTick: true,
                    endOnTick: true,
                    title: {
                        text: null
                    },
                    // disable label for "0%"
                    showFirstLabel: false,
                    labels: {
                        x: -3,
                        y: 10,
                        formatter: function () {
                            var idx = _.indexOf(yTickPositions, this.value);
                            return yTickLabels[idx];
                        }
                    },
                    plotBands: yPlotBands
                },
                series: series,
                plotOptions: {
                    series: {
                        marker: {
                            enabled: false
                        }
                    }
                }
            };
        };
        this.setupChartWatch = function () {
            var watch = _this.$scope.$watch(_this.findChart, function (chart) {
                if (!chart) {
                    return;
                }
                _this.chartInstance = chart;
                // remove watch
                watch();
            });
        };
        this.findChart = function () {
            return _.find(Highcharts.charts, function (x) {
                return x && x.options.tag === _this.chartConfig.tag;
            });
        };
    }
    Object.defineProperty(DepletionsChartController.prototype, "actualUtilizationTrendEnabled", {
        get: function () {
            return this._actualUtilizationTrendEnabled;
        },
        set: function (value) {
            this._actualUtilizationTrendEnabled = value;
            if (this.chartInstance) {
                this.chartInstance.series[Trends.utilizationTrend].setVisible(value, true);
            }
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(DepletionsChartController.prototype, "actualTrendEnabled", {
        get: function () {
            return this._actualTrendEnabled;
        },
        set: function (value) {
            this._actualTrendEnabled = value;
            if (this.chartInstance) {
                this.chartInstance.series[Trends.actualTrendHistorical].setVisible(value, true);
                this.chartInstance.series[Trends.actualTrendFuture].setVisible(value, true);
            }
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(DepletionsChartController.prototype, "simulatedUtilizationTrendAvailable", {
        get: function () {
            return !!this.counterUtilization.simulatedUtilizationTrend;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DepletionsChartController.prototype, "simulatedUtilizationTrendEnabled", {
        get: function () {
            return this._simulatedUtilizationTrendEnabled;
        },
        set: function (value) {
            this._simulatedUtilizationTrendEnabled = value;
            if (this.chartInstance && this.chartInstance.series[Trends.simulatedUtilizationTrend]) {
                this.chartInstance.series[Trends.simulatedUtilizationTrend].setVisible(value, true);
            }
        },
        enumerable: true,
        configurable: true
    });
    ;
    DepletionsChartController = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngScope)),
        __param(1, di_1.Inject(index_2.NgServiceNames.ngRootScopeService)),
        __param(2, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __param(3, di_1.Inject(index_2.ApolloServiceNames.dateTimeService)),
        __param(4, di_1.Inject(index_1.ApolloFilterFullNames.percentFilter)),
        __metadata("design:paramtypes", [Object, Object, Function, Object, Function])
    ], DepletionsChartController);
    return DepletionsChartController;
}());
exports.DepletionsChartController = DepletionsChartController;


/***/ }),
/* 181 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-depletions-chart> <div class=row> <div class=col-xs-12> <div class=title> <div class=title-start _t=[vm.minDateFormatted]> From {0} </div> <div class=title-end _t=[vm.maxDateFormatted]> To {0} </div> </div> </div> </div> <div class=row> <div class=col-xs-12> <sw-chart type=line config=vm.chartConfig></sw-chart> </div> </div> <div class=row> <div class=legend> <div class=\"col-xs-4 legend-item\"> <xui-checkbox title=\"_t(Actual Utilization)\" ng-model=vm.actualUtilizationTrendEnabled _ta> <span class=\"legend-color-box actual-utilization\"></span> <span _t>Actual Utilization</span> </xui-checkbox> </div> <div class=\"col-xs-4 legend-item\"> <xui-checkbox title=\"_t(Actual Trend)\" ng-model=vm.actualTrendEnabled _ta> <span class=\"legend-color-box actual-trend\"></span> <span _t>Actual Trend</span> </xui-checkbox> </div> <div ng-if=vm.simulatedUtilizationTrendAvailable class=\"col-xs-4 legend-item\"> <xui-checkbox title=\"_t(Simulated Utilization)\" ng-model=vm.simulatedUtilizationTrendEnabled _ta> <span class=\"legend-color-box simulated-utilization\"></span> <span _t>Simulated Utilization</span> </xui-checkbox> </div> </div> </div> </div>";

/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pieChart_directive_1 = __webpack_require__(183);
exports.default = function (module) {
    module.component("vimCpUsagePieChart", pieChart_directive_1.PieChartDirective);
};


/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(57);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pieChart_controller_1 = __webpack_require__(184);
/**
 * Directive which displays usage pie chart
 */
var PieChartDirective = /** @class */ (function () {
    function PieChartDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(185);
        this.controller = pieChart_controller_1.PieChartController;
        this.controllerAs = "vm";
        this.bindToController = {
            counterType: "<",
            counterSummaryResources: "<",
            counterSummaryWorkloads: "<"
        };
        this.scope = {};
    }
    return PieChartDirective;
}());
exports.PieChartDirective = PieChartDirective;
;


/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(6);
var index_2 = __webpack_require__(3);
var eventNames_1 = __webpack_require__(42);
var index_3 = __webpack_require__(1);
/**
 * Controller for usage pie chart
 *
 * @class
 */
var PieChartController = /** @class */ (function () {
    function PieChartController($q, $rootScope, _t, routingService) {
        var _this = this;
        this.$q = $q;
        this.$rootScope = $rootScope;
        this._t = _t;
        this.routingService = routingService;
        this.$onInit = function () {
            _this.setupAndCompute();
            _this.$rootScope.$on(eventNames_1.EventNames.reportResultChanged, _this.setupAndCompute);
        };
        this.getFormat = function () {
            switch (_this.counterType) {
                case index_2.Counters.memory:
                case index_2.Counters.disk:
                    return "bytes";
                case index_2.Counters.cpu:
                    return "clock";
                default:
                    return "";
            }
        };
        this.computeValues = function () {
            _this.total = _this.counterSummaryResources.current;
            _this.inUse = _this.counterSummaryWorkloads.current;
            _this.inUsePercentage = Math.round(_this.inUse / _this.total * 100);
            _this.remaining = _this.total - _this.inUse;
            if (_this.remaining < 0) {
                _this.remaining = 0;
            }
            _this.remainingPercentage = 100 - _this.inUsePercentage;
            if (_this.remainingPercentage < 0) {
                _this.remainingPercentage = 0;
            }
        };
        /**
         * Initializes highcharts configuration
         */
        this.setupHighcharts = function () {
            var self = _this;
            _this.chartConfig = {
                chart: {
                    type: "solidgauge",
                    margin: [0, 0, 0, 0],
                    height: 275
                },
                tooltip: {
                    enabled: false
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{
                            outerRadius: "85%",
                            innerRadius: "55%",
                            backgroundColor: index_1.Colors.lightGray,
                            borderWidth: 0
                        }]
                },
                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: []
                },
                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            enabled: true,
                            borderWidth: 0,
                            backgroundColor: "none",
                            shadow: false,
                            style: {
                                fontSize: "26px",
                            },
                            formatter: function () {
                                return this.y + " " + "%";
                            },
                            y: -20
                        },
                        linecap: "round",
                        stickyTracking: false,
                        rounded: true
                    }
                },
                series: [{
                        name: "Move",
                        data: [{
                                color: index_1.Colors.primaryBlue,
                                radius: 85,
                                innerRadius: 55,
                                y: _this.inUsePercentage
                            }]
                    }],
            };
        };
        this.setupAndCompute = function () {
            _this.computeValues();
            _this.setupHighcharts();
        };
    }
    PieChartController = __decorate([
        __param(0, di_1.Inject(index_3.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_3.NgServiceNames.ngRootScopeService)),
        __param(2, di_1.Inject(index_3.ApolloServiceNames.getTextService)),
        __param(3, di_1.Inject(index_3.ServiceNames.routingService)),
        __metadata("design:paramtypes", [Function, Object, Function, Object])
    ], PieChartController);
    return PieChartController;
}());
exports.PieChartController = PieChartController;


/***/ }),
/* 185 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-pie-chart> <div class=pie-chart-box> <div class=row> <div class=col-xs-12> <sw-chart type=solidgauge config=vm.chartConfig></sw-chart> </div> </div> <div class=stats> <div class=row> <div class=\"col-xs-5 text-right\" _t>Total</div> <div class=col-xs-7 data-selector=pie-chart-total> {{vm.total | vimCounterFormat: vm.getFormat()}} </div> </div> <div class=row> <div class=\"col-xs-5 text-right\" _t>In Use</div> <div class=col-xs-7 data-selector=pie-chart-in-use> {{vm.inUse | vimCounterFormat: vm.getFormat()}} ({{vm.inUsePercentage | vimNumberFormat: 0}}) </div> </div> <div class=row> <div class=\"col-xs-5 text-right\" _t>Remaining</div> <div class=col-xs-7 data-selector=pie-chart-remaining> {{vm.remaining | vimCounterFormat: vm.getFormat()}} ({{vm.remainingPercentage | vimNumberFormat: 0}}) </div> </div> </div> </div> </div>";

/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileSummary_directive_1 = __webpack_require__(187);
exports.default = function (module) {
    module.component("vimCpVmProfileSummary", vmProfileSummary_directive_1.VmProfileSummaryDirective);
};


/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(58);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vmProfileSummary_controller_1 = __webpack_require__(188);
var VmProfileSummaryDirective = /** @class */ (function () {
    function VmProfileSummaryDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(189);
        this.controller = vmProfileSummary_controller_1.VmProfileSummaryController;
        this.controllerAs = "vm";
        this.bindToController = {
            vmProfiles: "<",
            summaryTitle: "@"
        };
        this.scope = {};
    }
    return VmProfileSummaryDirective;
}());
exports.VmProfileSummaryDirective = VmProfileSummaryDirective;
;


/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VmProfileSummaryController = /** @class */ (function () {
    function VmProfileSummaryController() {
        var _this = this;
        this.$onInit = function () {
            _this.vmProfilesFiltered = _.filter(_this.vmProfiles, function (profile) { return profile.instanceCount > 0; });
        };
    }
    return VmProfileSummaryController;
}());
exports.VmProfileSummaryController = VmProfileSummaryController;
;


/***/ }),
/* 189 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-vm-profile-summary> <h4 class=summary-title>{{vm.summaryTitle}}</h4> <xui-message type=info allow-dismiss=false _t> Calculations for capacity planning simulations are based on resource utilization rather than assigned resources. </xui-message> <xui-listview items-source=vm.vmProfilesFiltered template-url=vm-profile-template> </xui-listview> <script type=text/ng-template id=vm-profile-template> <div class=\"row vm-profile\">\n            <div class=\"col-md-2 text-center instance-count\" data-selector=\"vm-instance-count\">\n                {{item.instanceCount}}\n            </div>\n            <div class=\"col-md-10\">\n                <div class=\"row\">\n                    <div class=\"col-md-12 name\"  data-selector=\"vm-name\">\n                        {{item.name}}\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <!-- cpu -->\n                    <div class=\"col-md-4\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"cpu\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"vm-cpu-core-count\">\n                                {{item.counters[\"cpuCores\"].value | vimCounterFormat: \"cores\"}}\n                            </div>\n                        </div>\n                    </div>\n\n                    <!-- memory -->\n                    <div class=\"col-md-4\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"memory\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"vm-memory-capacity\">\n                                {{item.counters[\"memory\"].value | vimCounterFormat: \"bytes\"}}\n                            </div>\n                        </div>\n                    </div>\n\n                    <!-- disk space -->\n                    <div class=\"col-md-4\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"harddrive\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"vm-disk-space-capacity\">\n                                {{item.counters[\"disk\"].value | vimCounterFormat: \"bytes\"}}\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 190 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var hostProfileSummary_directive_1 = __webpack_require__(191);
exports.default = function (module) {
    module.component("vimCpHostProfileSummary", hostProfileSummary_directive_1.HostProfileSummaryDirective);
};


/***/ }),
/* 191 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(59);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hostProfileSummary_controller_1 = __webpack_require__(192);
var HostProfileSummaryDirective = /** @class */ (function () {
    function HostProfileSummaryDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(193);
        this.controller = hostProfileSummary_controller_1.HostProfileSummaryController;
        this.controllerAs = "vm";
        this.bindToController = {
            hostProfiles: "<",
            summaryTitle: "@"
        };
        this.scope = {};
    }
    return HostProfileSummaryDirective;
}());
exports.HostProfileSummaryDirective = HostProfileSummaryDirective;
;


/***/ }),
/* 192 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var HostProfileSummaryController = /** @class */ (function () {
    function HostProfileSummaryController() {
    }
    return HostProfileSummaryController;
}());
exports.HostProfileSummaryController = HostProfileSummaryController;
;


/***/ }),
/* 193 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-host-profile-summary> <h4 class=summary-title>{{vm.summaryTitle}}</h4> <xui-listview items-source=vm.hostProfiles template-url=host-profile-template> </xui-listview> <script type=text/ng-template id=host-profile-template> <div class=\"row host-profile\">\n            <div class=\"col-md-2 text-center instance-count\" data-selector=\"host-instance-count\">\n                {{item.instanceCount}}\n            </div>\n            <div class=\"col-md-10\">\n                <div class=\"row\">\n                    <div class=\"col-md-12 name\" data-selector=\"host-name\">\n                        {{item.name}}\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <!-- cpu -->\n                    <div class=\"col-md-4\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"cpu\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"host-cpu-core-count\">\n                                {{item.cpuCoreCount | vimCounterFormat: \"cores\"}}\n                            </div>\n                        </div>\n                    </div>\n\n                    <!-- memory -->\n                    <div class=\"col-md-4\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"memory\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"host-memory-capacity\">\n                                {{item.memoryCapacity | vimCounterFormat: \"bytes\"}}\n                            </div>\n                        </div>\n                    </div>\n\n                    <!-- disk space -->\n                    <div class=\"col-md-4\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"harddrive\" icon-size=\"small\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" data-selector=\"host-disk-space-capacity\">\n                                {{item.diskSpaceCapacity | vimCounterFormat: \"bytes\"}}\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 194 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportHeader_directive_1 = __webpack_require__(195);
exports.default = function (module) {
    module.component("vimCpReportHeader", reportHeader_directive_1.ReportHeaderDirective);
};


/***/ }),
/* 195 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(60);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var reportHeader_controller_1 = __webpack_require__(196);
/**
 * Directive which displays report header
 */
var ReportHeaderDirective = /** @class */ (function () {
    function ReportHeaderDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(197);
        this.controller = reportHeader_controller_1.ReportHeaderController;
        this.controllerAs = "vm";
        this.bindToController = {
            name: "<",
            entityCaption: "<",
            computationDateStarted: "<",
            runway: "<"
        };
        this.scope = {};
    }
    return ReportHeaderDirective;
}());
exports.ReportHeaderDirective = ReportHeaderDirective;
;


/***/ }),
/* 196 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Controller for report header
 *
 * @class
 */
var ReportHeaderController = /** @class */ (function () {
    function ReportHeaderController() {
        var _this = this;
        this.getProjectionDate = function () {
            return moment(_this.computationDateStarted).startOf("day").add(_this.runway, "day");
        };
    }
    return ReportHeaderController;
}());
exports.ReportHeaderController = ReportHeaderController;


/***/ }),
/* 197 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-header> <div class=\"header-container xui-padding-lgh xui-padding-mdv\"> <div class=row> <div class=\"col-xs-12 title\" data-selector=title> {{vm.name}} </div> </div> <div class=row> <div class=\"col-xs-12 subtitle\" data-selector=subtitle> {{vm.entityCaption}} </div> </div> <div class=row> <div class=\"col-xs-6 computation-details\"> <span _t>Run:</span> <span data-selector=computation-date> {{vm.computationDateStarted | vimDateFormat: 'shortDate'}} </span> <span _t>Projection:</span> <span data-selector=projection-date> {{vm.getProjectionDate() | vimDateFormat: 'shortDate'}} </span> </div> </div> </div> </div>";

/***/ }),
/* 198 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportNavigation_directive_1 = __webpack_require__(199);
exports.default = function (module) {
    module.component("vimCpReportNavigation", reportNavigation_directive_1.ReportNavigationDirective);
};


/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportNavigation_controller_1 = __webpack_require__(200);
/**
 * Directive which displays report navigation
 */
var ReportNavigationDirective = /** @class */ (function () {
    function ReportNavigationDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(201);
        this.controller = reportNavigation_controller_1.ReportNavigationController;
        this.controllerAs = "vm";
        this.scope = {};
    }
    return ReportNavigationDirective;
}());
exports.ReportNavigationDirective = ReportNavigationDirective;
;


/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
/**
 * Controller for report detail
 *
 * @class
 */
var ReportNavigationController = /** @class */ (function () {
    function ReportNavigationController(routingService) {
        var _this = this;
        this.routingService = routingService;
        this.redirectBackToReports = function () {
            _this.routingService.goToReportList();
        };
        this.getRedirectBackHref = function () {
            return _this.routingService.getReportListHref();
        };
    }
    ReportNavigationController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.routingService)),
        __metadata("design:paramtypes", [Object])
    ], ReportNavigationController);
    return ReportNavigationController;
}());
exports.ReportNavigationController = ReportNavigationController;


/***/ }),
/* 201 */
/***/ (function(module, exports) {

module.exports = "<div class=\"vim-cp-report-navigation xui-margin-lg\"> <a href={{::vm.getRedirectBackHref()}} ng-click=vm.redirectBackToReports() _t>&lt; back to capacity planning reports</a> </div>";

/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var reportFooter_directive_1 = __webpack_require__(203);
exports.default = function (module) {
    module.component("vimCpReportFooter", reportFooter_directive_1.ReportFooterDirective);
};


/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(61);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var reportFooter_controller_1 = __webpack_require__(204);
/**
 * Directive which displays report footer
 */
var ReportFooterDirective = /** @class */ (function () {
    function ReportFooterDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(205);
        this.controller = reportFooter_controller_1.ReportFooterController;
        this.controllerAs = "vm";
        this.bindToController = {
            failoverReservation: "<",
            resourceAllocationType: "<",
            utilizationPeriodInDays: "<",
            counters: "="
        };
        this.scope = {};
    }
    return ReportFooterDirective;
}());
exports.ReportFooterDirective = ReportFooterDirective;
;


/***/ }),
/* 204 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(3);
/**
 * Controller for report footer
 *
 * @class
 */
var ReportFooterController = /** @class */ (function () {
    function ReportFooterController() {
        var _this = this;
        this.hasCpuCounter = function () {
            return _this.hasCounter(index_1.Counters.cpu);
        };
        this.hasMemoryCounter = function () {
            return _this.hasCounter(index_1.Counters.memory);
        };
        this.hasDiskCounter = function () {
            return _this.hasCounter(index_1.Counters.disk);
        };
        this.hasCounter = function (counterName) {
            return !_.isEmpty(_this.counters) && counterName in _this.counters;
        };
    }
    return ReportFooterController;
}());
exports.ReportFooterController = ReportFooterController;


/***/ }),
/* 205 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp-report-footer> <div class=\"footer-container xui-padding-lgh xui-padding-mdv\"> <div class=row> <div class=col-xs-12> <h4 _t>Advanced Settings<h4> </h4></h4></div> </div> <div class=\"row xui-margin-lg\"> <div class=col-sm-4> <h5 _t>Resource Modeling</h5> <ul class=\"adv-settings-list xui-margin-lgl xui-padding-lgl\"> <li ng-if=\"vm.resourceAllocationType == 0\" data-selector=resource-allocation-type _t>Conservative model</li> <li ng-if=\"vm.resourceAllocationType == 1\" data-selector=resource-allocation-type _t>Balanced model</li> <li ng-if=\"vm.resourceAllocationType == 2\" data-selector=resource-allocation-type _t>Aggressively Optimized model</li> <li ng-if=\"vm.failoverReservation == 0\" data-selector=failover-reservation _t>No failover reservation</li> <li ng-if=\"vm.failoverReservation == 1\" data-selector=failover-reservation _t>1 host reserved for failover</li> <li ng-if=\"vm.failoverReservation > 1\" data-selector=failover-reservation _t>{{vm.failoverReservation}} hosts reserved for failover</li> </ul> </div> <div class=col-sm-4> <h5 _t>Usage History</h5> <ul class=\"adv-settings-list xui-margin-lgl xui-padding-lgl\"> <li ng-if=\"vm.utilizationPeriodInDays == 1\" data-selector=utilization-period-in-days _t>Last day</li> <li ng-if=\"vm.utilizationPeriodInDays > 1\" data-selector=utilization-period-in-days _t>Last {{vm.utilizationPeriodInDays}} days</li> </ul> </div> <div class=col-sm-4> <h5 _t>Resources & Thresholds</h5> <ul class=\"adv-settings-list xui-margin-lgl xui-padding-lgl\"> <li ng-if=vm.hasCpuCounter() data-selector=cpu-counter _t>CPU Usage - included</li> <li ng-if=vm.hasMemoryCounter() data-selector=memory-counter _t>Memory Usage - included</li> <li ng-if=vm.hasDiskCounter() data-selector=disk-counter _t>Disk Space - included</li> </ul> </div> </div> </div> </div> ";

/***/ }),
/* 206 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var wizardStepToggle_directive_1 = __webpack_require__(207);
exports.default = function (module) {
    module.component("vimCpWizardStepToggle", wizardStepToggle_directive_1.WizardStepToggleDirective);
};


/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Directive for toggling wizard step visibility
 */
var WizardStepToggleDirective = /** @class */ (function () {
    function WizardStepToggleDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(208);
        this.require = ["^^xuiWizardStep"];
        this.scope = {};
        this.link = function (scope, instanceElement, instanceAttributes, controllers) {
            // keep step instance within current scope
            scope.step = controllers[0];
        };
    }
    return WizardStepToggleDirective;
}());
exports.WizardStepToggleDirective = WizardStepToggleDirective;


/***/ }),
/* 208 */
/***/ (function(module, exports) {

module.exports = "<div> <div ng-if=step.active ng-transclude=\"\"></div> </div>";

/***/ }),
/* 209 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(45);
var routingRegistration_provider_1 = __webpack_require__(210);
var replaceProviderKeyword = function (name) {
    return name.replace("Provider", "");
};
exports.default = function (module) {
    module.provider(replaceProviderKeyword(index_1.ProviderNames.routingRegistrationProvider), routingRegistration_provider_1.RoutingRegistrationProvider);
};


/***/ }),
/* 210 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(6);
var index_2 = __webpack_require__(211);
var index_3 = __webpack_require__(1);
var di_1 = __webpack_require__(0);
var routeUrls_1 = __webpack_require__(28);
var routeNames_1 = __webpack_require__(27);
var reportWizard_controller_1 = __webpack_require__(212);
var reportDetail_controller_1 = __webpack_require__(213);
var notLicensed_controller_1 = __webpack_require__(214);
var errorStateParams = {
    errorType: "auth",
    errorCode: "403",
    message: "Access Denied",
    location: null
};
var isLicensedName = "isLicensed";
/**
 * Checks if Capacity Planing is licensed.
*/
var checkIsCapacityPlanningLicensed = function (licensingService) {
    return licensingService.isLicensed();
};
checkIsCapacityPlanningLicensed.$inject = [index_3.ServiceNames.licensingService];
var onLicensedPageEnter = function (routingService, isLicensed) {
    if (!isLicensed) {
        routingService.goToNotLicensedPage();
    }
};
onLicensedPageEnter.$inject = [index_3.ServiceNames.routingService, isLicensedName];
/**
 * Provider for routing-related registration
 *
 * @class
 */
var RoutingRegistrationProvider = /** @class */ (function () {
    function RoutingRegistrationProvider($stateProvider, $urlRouterProvider) {
        var _this = this;
        this.$stateProvider = $stateProvider;
        this.$urlRouterProvider = $urlRouterProvider;
        this.registerRoutes = function () {
            _this.registerNotLicensedRoute();
            _this.registerReportListRoute();
            _this.registerReportWizardRoute();
            _this.registerReportDetailRoute();
        };
        this.registerNotLicensedRoute = function () {
            _this.registerInternalRoute(routeNames_1.RouteNames.notLicensedErrorStateName, routeUrls_1.RouteUrls.notLicensedUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Not Licensed – Capacity Planning)",
                template: __webpack_require__(215),
                controller: notLicensed_controller_1.NotLicensedController
            }, false);
        };
        this.registerReportListRoute = function () {
            _this.registerInternalRoute(routeNames_1.RouteNames.summaryStateName, routeUrls_1.RouteUrls.summaryUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Report List – Capacity Planning)",
                template: __webpack_require__(216)
            });
        };
        this.registerReportWizardRoute = function () {
            _this.registerInternalRoute(routeNames_1.RouteNames.reportWizardStateName, routeUrls_1.RouteUrls.reportWizardUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Report Wizard – Capacity Planning)",
                template: __webpack_require__(217),
                controller: reportWizard_controller_1.ReportWizardController
            });
        };
        this.registerReportDetailRoute = function () {
            _this.registerInternalRoute(routeNames_1.RouteNames.reportDetailStateName, routeUrls_1.RouteUrls.reportDetailUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Report Detail – Capacity Planning)",
                template: __webpack_require__(218),
                controller: reportDetail_controller_1.ReportDetailController
            });
        };
        this.registerInternalRoute = function (stateName, stateUrl, authProfile, configuration, checkLicense) {
            if (checkLicense === void 0) { checkLicense = true; }
            var routerState = {
                i18Title: configuration.i18title,
                url: stateUrl,
                template: configuration.template,
                params: angular.extend({}, errorStateParams, { location: stateUrl, authProfile: authProfile }),
                authProfile: authProfile,
                resolve: {}
            };
            if (checkLicense) {
                routerState.resolve[isLicensedName] = checkIsCapacityPlanningLicensed;
                routerState.onEnter = onLicensedPageEnter;
            }
            if (configuration.controller) {
                routerState.controller = configuration.controller;
                routerState.controllerAs = "vm";
            }
            _this.$stateProvider.state(stateName, routerState);
        };
        this.initGet = function () {
            _this.$get = [function () {
                    return {};
                }];
        };
        this.initGet();
    }
    RoutingRegistrationProvider = __decorate([
        __param(0, di_1.Inject(index_2.ProviderNames.ngStateProvider)),
        __param(1, di_1.Inject(index_2.ProviderNames.ngUrlRouterProvider)),
        __metadata("design:paramtypes", [Object, Object])
    ], RoutingRegistrationProvider);
    return RoutingRegistrationProvider;
}());
exports.RoutingRegistrationProvider = RoutingRegistrationProvider;
;


/***/ }),
/* 211 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of all registered providers
 *
 * @class
 */
var ProviderNames = /** @class */ (function () {
    function ProviderNames() {
    }
    ProviderNames.ngStateProvider = "$stateProvider";
    ProviderNames.ngUrlRouterProvider = "$urlRouterProvider";
    return ProviderNames;
}());
exports.ProviderNames = ProviderNames;


/***/ }),
/* 212 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(17);
var index_2 = __webpack_require__(1);
var index_3 = __webpack_require__(3);
var index_4 = __webpack_require__(18);
/**
 * Controller for wizard view
 */
var ReportWizardController = /** @class */ (function () {
    function ReportWizardController($q, $stateParams, routingService, reportWizardService, _t) {
        var _this = this;
        this.$q = $q;
        this.$stateParams = $stateParams;
        this.routingService = routingService;
        this.reportWizardService = reportWizardService;
        this._t = _t;
        /**
         * Initializes the route
         */
        this.init = function () {
            var id = _this.$stateParams["id"];
            if (id) {
                // edit
                return _this.reportWizardService.getReportDefinition(id).then(function (r) {
                    if (!r) {
                        _this.isNew = undefined;
                        _this.report = undefined;
                        _this.routingService.goToReportList();
                        return _this.$q.reject("Report definition not found.");
                    }
                    _this.isNew = false;
                    _this.report = r;
                    return _this.$q.resolve();
                });
            }
            else {
                // add
                var emptyReport = new index_1.ReportDefinition();
                _this.addPredefinedVmProfiles(emptyReport.simulatedVmProfiles);
                _this.isNew = true;
                _this.report = emptyReport;
                return _this.$q.resolve();
            }
        };
        /**
         * Add predefined VM profiles to vmProfiles array
         */
        this.addPredefinedVmProfiles = function (vmProfiles) {
            vmProfiles.push(_this.generatePredefinedProfile(_this._t("Small VM"), 1, 4, 10));
            vmProfiles.push(_this.generatePredefinedProfile(_this._t("Medium VM"), 2, 8, 20));
            vmProfiles.push(_this.generatePredefinedProfile(_this._t("Large VM"), 4, 12, 40));
        };
        this.generatePredefinedProfile = function (name, cpuCoreCount, memoryCapacity, diskSpaceCapacity) {
            return {
                name: name,
                instanceCount: 0,
                counters: (_a = {},
                    _a[index_3.Counters.cpuCores] = { value: cpuCoreCount, utilizationInPercent: index_3.ResourceLoad.Typical },
                    _a[index_3.Counters.memory] = { value: index_4.ConversionHelper.convertBytes(memoryCapacity, index_4.BytesUnit.GB, index_4.BytesUnit.B), utilizationInPercent: index_3.ResourceLoad.Typical },
                    _a[index_3.Counters.disk] = { value: index_4.ConversionHelper.convertBytes(diskSpaceCapacity, index_4.BytesUnit.GB, index_4.BytesUnit.B), utilizationInPercent: index_3.ResourceLoad.Typical },
                    _a)
            };
            var _a;
        };
    }
    ReportWizardController = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_2.NgServiceNames.ngStateParams)),
        __param(2, di_1.Inject(index_2.ServiceNames.routingService)),
        __param(3, di_1.Inject(index_2.ServiceNames.reportWizardService)),
        __param(4, di_1.Inject(index_2.ApolloServiceNames.getTextService)),
        __metadata("design:paramtypes", [Function, Object, Object, Object, Function])
    ], ReportWizardController);
    return ReportWizardController;
}());
exports.ReportWizardController = ReportWizardController;
;


/***/ }),
/* 213 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var eventNames_1 = __webpack_require__(42);
var enums_1 = __webpack_require__(29);
/**
 * Controller for wizard view
 */
var ReportDetailController = /** @class */ (function () {
    function ReportDetailController($q, $stateParams, $rootScope, $timeout, routingService, reportDetailService) {
        var _this = this;
        this.$q = $q;
        this.$stateParams = $stateParams;
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;
        this.routingService = routingService;
        this.reportDetailService = reportDetailService;
        this.reportResultItems = [];
        this.isBusy = false;
        this.reportStatus = enums_1.ReportStatus;
        /**
         * Initializes the route
         */
        this.init = function () {
            _this.isBusy = true;
            var ids;
            if (_this.$stateParams["ids"]) {
                ids = _this.$stateParams["ids"].split(",").map(_.parseInt);
            }
            if (!ids) {
                _this.reportDetail = undefined;
                _this.routingService.goToReportList();
                return _this.$q.reject("Invalid report result identifier.");
            }
            return _this.getReports(ids).then(function () { return _this.getReportDetail(ids[0]); });
        };
        this.getReports = function (ids) {
            return _this.reportDetailService.getReportsData(ids).then(function (reportsData) {
                if (!reportsData) {
                    _this.reportDetail = undefined;
                    _this.routingService.goToReportList();
                    return _this.$q.reject("Report result was not found.");
                }
                _this.reportResultItems = reportsData;
                for (var _i = 0, _a = _this.reportResultItems; _i < _a.length; _i++) {
                    var report = _a[_i];
                    report.combinedNameInDropdown = report.reportName + " \u2013 " + report.clusterName;
                }
                _this.reportResultSelected = _.find(reportsData, function (n) { return n.reportDefinitionId === ids[0]; });
            });
        };
        this.getReportDetail = function (reportId) {
            return _this.reportDetailService.getReportDetail(reportId)
                .then(function (r) {
                if (!r) {
                    _this.reportDetail = undefined;
                    _this.routingService.goToReportList();
                    return _this.$q.reject("Report result was not found.");
                }
                _this.reportDetail = r;
                return _this.$q.resolve();
            })
                .finally(function () {
                _this.isBusy = false;
            });
        };
        this.displayReportsDropdown = function () {
            return _this.reportResultItems.length > 1;
        };
        this.onReportResultChanged = function (newValue, oldValue) {
            if (angular.equals(newValue, oldValue)) {
                return;
            }
            if (newValue) {
                _this.getReportDetail(newValue.reportDefinitionId).finally(function () {
                    _this.$timeout(function () { return _this.$rootScope.$broadcast(eventNames_1.EventNames.reportResultChanged); }, 0);
                });
            }
        };
    }
    ReportDetailController = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.NgServiceNames.ngStateParams)),
        __param(2, di_1.Inject(index_1.NgServiceNames.ngRootScopeService)),
        __param(3, di_1.Inject(index_1.NgServiceNames.ngTimeoutService)),
        __param(4, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(5, di_1.Inject(index_1.ServiceNames.reportDetailService)),
        __metadata("design:paramtypes", [Function, Object, Object, Function, Object, Object])
    ], ReportDetailController);
    return ReportDetailController;
}());
exports.ReportDetailController = ReportDetailController;
;


/***/ }),
/* 214 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var NotLicensedController = /** @class */ (function () {
    function NotLicensedController(_t) {
        var _this = this;
        this._t = _t;
        this.init = function () {
            _this.errorParams = {
                errorCode: "403",
                errorType: "lic",
                location: "",
                message: _this._t("In order to access the Capacity Planning feature, you need to apply a valid VMAN license or integrate with a VMAN Appliance.")
            };
        };
    }
    NotLicensedController = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.getTextService)),
        __metadata("design:paramtypes", [Function])
    ], NotLicensedController);
    return NotLicensedController;
}());
exports.NotLicensedController = NotLicensedController;


/***/ }),
/* 215 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=vm.init()> <sw-orion-error error-report=::vm.errorParams></sw-orion-error> </div>";

/***/ }),
/* 216 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=\"xui-page-content--no-padding vim-cp\" page-layout=fullWidth page-title=\"_t(Capacity Planning Reports)\" _ta> <vim-cp-report-list></vim-cp-report-list> </xui-page-content> ";

/***/ }),
/* 217 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=vm.init()> <xui-page-content page-title=\"_t(Capacity Planning Wizard)\" page-layout=form _ta> <vim-cp-report-wizard is-new=vm.isNew report=vm.report> </vim-cp-report-wizard> </xui-page-content> </div> ";

/***/ }),
/* 218 */
/***/ (function(module, exports) {

module.exports = "<div class=vim-cp ng-init=vm.init() xui-busy=vm.isBusy> <xui-page-content page-layout=form _ta> <xui-page-header> <vim-cp-report-navigation></vim-cp-report-navigation> <div class=\"xui-padding-sm xui-dropdown-inline vim-cp-report-dropdown\" ng-if=vm.displayReportsDropdown() data-selector=vim-cp-report-dropdown> <xui-dropdown caption=\"&nbsp;Selected Report:\" selected-item=vm.reportResultSelected items-source=vm.reportResultItems display-value=combinedNameInDropdown on-changed=\"vm.onReportResultChanged(newValue, oldValue)\" item-template-url=vim-cp-report-dropdown-template> </xui-dropdown> </div> <xui-divider></xui-divider> <div ng-if=\"vm.reportDetail.reportStatus === vm.reportStatus.Finished\"> <vim-cp-report-header name=vm.reportDetail.name entity-caption=vm.reportDetail.entityCaption computation-date-started=vm.reportDetail.computationDateStarted runway=vm.reportDetail.runway> </vim-cp-report-header> <xui-divider></xui-divider> </div> </xui-page-header> <xui-message type=error ng-if=vm.reportDetail.errorMessage> {{::vm.reportDetail.errorMessage}} </xui-message> <div ng-if=\"vm.reportDetail.reportStatus === vm.reportStatus.Finished\"> <vim-cp-report-detail report-detail=vm.reportDetail></vim-cp-report-detail> <xui-page-footer> <vim-cp-report-footer failover-reservation=vm.reportDetail.failoverReservation resource-allocation-type=vm.reportDetail.resourceAllocationType utilization-period-in-days=vm.reportDetail.utilizationPeriodInDays counters=vm.reportDetail.counters> </vim-cp-report-footer> </xui-page-footer> </div> </xui-page-content> <script id=vim-cp-report-dropdown-template type=text/ng-template> <div class=\"media\" data-selector=\"{{::item.reportDefinitionId}}\">\n            <div class=\"media-body\">\n                <div class=\"media-heading\">{{::item.reportName}} – {{::item.clusterName}}</div>\n            </div>\n        </div> </script> </div>";

/***/ }),
/* 219 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(1);
var routing_service_1 = __webpack_require__(220);
var licensing_service_1 = __webpack_require__(221);
var reportList_service_1 = __webpack_require__(222);
var reportWizard_service_1 = __webpack_require__(223);
var reportDetail_service_1 = __webpack_require__(224);
var webUtils_service_1 = __webpack_require__(225);
exports.default = function (module) {
    module.service(index_1.ServiceNames.routingService, routing_service_1.RoutingService);
    module.service(index_1.ServiceNames.reportListService, reportList_service_1.ReportListService);
    module.service(index_1.ServiceNames.reportWizardService, reportWizard_service_1.ReportWizardService);
    module.service(index_1.ServiceNames.reportDetailService, reportDetail_service_1.ReportDetailService);
    module.service(index_1.ServiceNames.licensingService, licensing_service_1.LicensingService);
    module.service(index_1.ServiceNames.webUtilsService, webUtils_service_1.WebUtilsService);
};


/***/ }),
/* 220 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
/**
 * Routing service
 */
var RoutingService = /** @class */ (function () {
    function RoutingService($state) {
        var _this = this;
        this.$state = $state;
        this.goToReportList = function () {
            _this.$state.go(index_1.RouteNames.summaryStateName);
        };
        this.getReportListHref = function () {
            return _this.$state.href(index_1.RouteNames.summaryStateName);
        };
        this.goToReportWizard = function (id) {
            _this.$state.go(index_1.RouteNames.reportWizardStateName, { id: id || "" });
        };
        this.goToReportDetail = function (ids) {
            _this.$state.go(index_1.RouteNames.reportDetailStateName, { ids: ids });
        };
        this.goToNotLicensedPage = function () {
            _this.$state.go(index_1.RouteNames.notLicensedErrorStateName, undefined, { location: false });
        };
    }
    RoutingService = __decorate([
        __param(0, di_1.Inject(index_2.NgServiceNames.ngState)),
        __metadata("design:paramtypes", [Object])
    ], RoutingService);
    return RoutingService;
}());
exports.RoutingService = RoutingService;
;


/***/ }),
/* 221 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
/**
 * VIM Capacity Planning licensing service
 */
var LicensingService = /** @class */ (function () {
    function LicensingService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.isLicensed = function () {
            return _this.swApi.api(false).one(index_1.SwApiUrls.isLicensedUrl).get();
        };
    }
    LicensingService = __decorate([
        __param(0, di_1.Inject(index_2.ApolloServiceNames.swApiService)),
        __metadata("design:paramtypes", [Object])
    ], LicensingService);
    return LicensingService;
}());
exports.LicensingService = LicensingService;
;


/***/ }),
/* 222 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(1);
var di_1 = __webpack_require__(0);
var index_2 = __webpack_require__(12);
var swApiUrls_1 = __webpack_require__(31);
/**
 * Swis connection service
 *
 * @class
 * @implements ISwisService
 */
var ReportListService = /** @class */ (function () {
    function ReportListService(swis, dateTimeService, swApi) {
        var _this = this;
        this.swis = swis;
        this.dateTimeService = dateTimeService;
        this.swApi = swApi;
        this.filterValues = { entity: [], parameter: [] };
        this.getReports = function (filter) {
            var query = "SELECT \n                df.ID, \n                df.Name, \n                df.EntityCaption,\n                df.Runway,\n                df.SimulatedResourcesEnabled,\n                df.SimulatedWorkloadsEnabled,\n                df.Result.ComputationDateStarted,\n                df.Result.ComputationDateFinished as ComputationDateFinished,\n                pl.Name AS PlatformName,\n                si.IconPostfix AS EntityStatus,\n                df.Result.ID AS ResultID\n            FROM Orion.VIM.CapacityPlanning.ReportDefinitions df\n            LEFT JOIN (SELECT  CONCAT (OrionIdPrefix,ClusterID) as EntityNetObjectID, Status, PlatformId\n            FROM Orion.VIM.Clusters) cl\n            ON df.EntityNetObjectID = cl.EntityNetObjectID\n            JOIN Orion.StatusInfo si\n            ON si.StatusId = cl.status\n            JOIN Orion.VIM.Platform pl\n            ON pl.PlatformID = cl.PlatformId\n             " + _this.getFilterReportsQuery(filter);
            return _this.swis.runQuery(query).then(function (results) {
                var reports = _.map(results.rows, function (data) {
                    return _this.getReportFromData(data);
                });
                return { rows: reports, total: results.total || 0 };
            });
        };
        this.getReportsFilterStats = function () {
            var query = "SELECT EntityCaption, EntityNetObjectId, SimulatedResourcesEnabled, SimulatedWorkloadsEnabled\n             FROM Orion.VIM.CapacityPlanning.ReportDefinitions";
            return _this.swis.runQuery(query).then(function (results) {
                return _.map(results.rows, function (data) {
                    return {
                        entityCaption: data["EntityCaption"],
                        entityNetObjectId: data["EntityNetObjectId"],
                        simulatedResourcesEnabled: data["SimulatedResourcesEnabled"],
                        simulatedWorkloadsEnabled: data["SimulatedWorkloadsEnabled"]
                    };
                });
            });
        };
        this.getFilterValues = function () {
            return _this.filterValues;
        };
        this.saveFilterValues = function (filterValues) {
            _this.filterValues = filterValues ? _.cloneDeep(filterValues) : filterValues;
        };
        this.deleteReports = function (ids) {
            var request = {
                ReportDefinitionIds: ids
            };
            return _this.swApi.api(false).one(swApiUrls_1.SwApiUrls.baseUrl).post(swApiUrls_1.SwApiUrls.deleteReports, request);
        };
        this.getReportFromData = function (data) {
            var report = {
                id: data.ID,
                resultId: data.ResultID,
                name: data.Name,
                entityCaption: data.EntityCaption,
                entityPlatform: data.PlatformName,
                entityStatus: data.EntityStatus,
                runway: data.Runway,
                simulatedResourcesEnabled: data.SimulatedResourcesEnabled,
                simulatedWorkloadsEnabled: data.SimulatedWorkloadsEnabled,
                computationDateStarted: data.ComputationDateStarted ? moment.utc(data.ComputationDateStarted) : undefined,
                computationDateFinished: data.ComputationDateFinished ? moment.utc(data.ComputationDateFinished) : undefined
            };
            return report;
        };
        this.getFilterReportsQuery = function (filter) {
            var query = "";
            query += _this.getFilterWhereQuery(filter);
            query += _this.getFilterSortingQuery(filter);
            query += _this.getFilterPaginationQuery(filter);
            return query;
        };
        this.getFilterWhereQuery = function (filter) {
            var whereConditions = [];
            // search
            if (!_.isEmpty(filter.search)) {
                whereConditions.push(" EntityCaption LIKE '%" + filter.search + "%' OR Name LIKE '%" + filter.search + "%'");
            }
            // filter by cluster
            var entityFilter = filter.filters.entity;
            if (!_.isEmpty(entityFilter)) {
                var entityFilterValue = _.map(entityFilter, function (id) { return "'" + id + "'"; }).join(", ");
                whereConditions.push("EntityNetObjectID IN (" + entityFilterValue + ")");
            }
            // filter by parameters
            var parameterFilter = filter.filters.parameter;
            if (!_.isEmpty(parameterFilter)) {
                if (parameterFilter.indexOf(index_2.ReportsFilterParameterNames.simulatedResourcesEnabled) >= 0) {
                    whereConditions.push("SimulatedResourcesEnabled = 1");
                }
                if (parameterFilter.indexOf(index_2.ReportsFilterParameterNames.simulatedWorkloadsEnabled) >= 0) {
                    whereConditions.push("SimulatedWorkloadsEnabled = 1");
                }
            }
            if (whereConditions.length === 0) {
                return "";
            }
            var conditionsJoined = whereConditions.join(" AND ");
            return " WHERE " + conditionsJoined;
        };
        this.getFilterPaginationQuery = function (filter) {
            if (!filter.pagination) {
                return "";
            }
            var startRow = (filter.pagination.pageSize * (filter.pagination.page - 1)) + 1;
            var endRow = startRow + filter.pagination.pageSize - 1;
            return " WITH ROWS " + startRow + " TO " + endRow + " WITH TOTALROWS";
        };
        this.getFilterSortingQuery = function (filter) {
            var query = "";
            if (filter.sorting) {
                var sortBy = filter.sorting.sortBy.id;
                var sortDirection = filter.sorting.direction;
                query += " ORDER BY " + sortBy + " " + sortDirection;
                if (sortBy === "Name" || sortBy === "EntityCaption") {
                    query += ", ComputationDateFinished DESC";
                }
            }
            else {
                query += " ORDER BY ComputationDateFinished DESC";
            }
            return query;
        };
    }
    ReportListService = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.swisService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.dateTimeService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.swApiService)),
        __metadata("design:paramtypes", [Object, Object, Object])
    ], ReportListService);
    return ReportListService;
}());
exports.ReportListService = ReportListService;
;


/***/ }),
/* 223 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var conversionHelper_1 = __webpack_require__(19);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(17);
var index_3 = __webpack_require__(1);
var di_1 = __webpack_require__(0);
;
;
;
;
;
/**
 * Report wizard service implementation
 */
var ReportWizardService = /** @class */ (function () {
    function ReportWizardService(swApi, swis, dateTimeService) {
        var _this = this;
        this.swApi = swApi;
        this.swis = swis;
        this.dateTimeService = dateTimeService;
        this.getReportDefinition = function (id) {
            var swql = "SELECT ID, Name FROM Orion.VIM.CapacityPlanning.ReportDefinitions WHERE ID=" + id;
            return _this.swis.runQuery(swql)
                .then(function (result) {
                if (_.isEmpty(result.rows)) {
                    return null;
                }
                var row = result.rows[0];
                return _this.generateReportDefinition(row);
            });
        };
        this.getEntities = function (filter) {
            // ensure first page is used when priority sort is used
            if (filter.prioritizedEntity) {
                filter.pagination = filter.pagination || { page: 1, pageSize: 1000000 };
                filter.pagination.page = 1;
            }
            var swql = "SELECT Entities.EntityType, Entities.NetObjectId, Entities.Name, Entities.DatacenterName,\n                Entities.HostsCount, Entities.VmsCount, Entities.DatastoresCount, Entities.PrioritySort\n            FROM (SELECT\n                    (" + index_1.ReportEntityType.Cluster + ") AS EntityType,\n                    ('" + index_1.NetObjectPrefixes.cluster + ":' + TOSTRING(clusters.ClusterID)) AS NetObjectId,\n                    clusters.Name,\n                    clusters.DataCenter.Name AS DatacenterName,\n                    COUNT(DISTINCT clusters.Hosts.HostID) AS HostsCount,\n                    COUNT(DISTINCT clusters.Hosts.VirtualMachines.VirtualMachineID) AS VmsCount,\n                    COUNT(DISTINCT clusters.DataStores.DataStoreID) AS DatastoresCount,\n                    MIN(clusters.Hosts.PollingSource) AS IsCapacityPlanningSupported,\n                    (CASE \n                        WHEN ('" + index_1.NetObjectPrefixes.cluster + ":' + TOSTRING(clusters.ClusterID)) = '" + (filter.prioritizedEntity || "") + "' THEN 0\n                        ELSE 1\n                    END) AS PrioritySort\n                FROM Orion.VIM.Clusters clusters\n                GROUP BY ('" + index_1.NetObjectPrefixes.cluster + ":' + TOSTRING(clusters.ClusterID)), clusters.Name, clusters.Datacenter.Name\n            ) AS Entities\n        " + _this.getFilterQuery(filter);
            return _this.swis.runQuery(swql)
                .then(function (result) {
                var entities = _.map(result.rows, function (e) { return _this.generateEntity(e); });
                return {
                    rows: entities,
                    total: result.total
                };
            });
        };
        this.getExistingHosts = function (filter) {
            var swql = "SELECT HostID, DiskSpaceCapacity, HostName, ClusterID, CpuCoreCount, MemorySize, CpuMhz\n                        FROM\n                            (SELECT HostID, SUM(h.DataStores.Capacity) AS DiskSpaceCapacity, HostName, ClusterID, CpuCoreCount, MemorySize, CpuMhz,\n                                (CASE \n                                    WHEN ('" + index_1.NetObjectPrefixes.host + ":' + TOSTRING(HostID)) = '" + (filter.prioritizedEntity || "") + "' THEN 0\n                                ELSE 1\n                                END) AS PrioritySort\n                            FROM Orion.VIM.Hosts h \n                            " + _this.getHostsWhereClause(filter) + "\n                            GROUP BY HostID, HostName, ClusterID, CpuCoreCount, MemorySize, CpuMhz)\n                        " + _this.getHostsFilterQuery(filter);
            return _this.swis.runQuery(swql)
                .then(function (result) {
                var hosts = _.map(result.rows, function (e) { return _this.generateHostEntity(e); });
                return {
                    rows: hosts,
                    total: result.total
                };
            });
        };
        this.processNewReportDefinition = function (ReportDefinition) {
            var request = {
                reportDefinition: ReportDefinition
            };
            return _this.swApi.api(false).one("vim/capacity-planning").post("process-new-report-definition", request);
        };
        this.getVirtualMachinesByCluster = function (clusterNetObjectIds, filter) {
            var ids = [];
            for (var _i = 0, clusterNetObjectIds_1 = clusterNetObjectIds; _i < clusterNetObjectIds_1.length; _i++) {
                var clusterNetObjectId = clusterNetObjectIds_1[_i];
                ids.push("'" + clusterNetObjectId + "'");
            }
            var idsString = ids.join(",");
            var swql = "SELECT VirtualMachineID, Name, ProcessorCount, TotalStorageSize, MemoryConfigured\n                      FROM Orion.VIM.VirtualMachines vm\n                      WHERE '" + index_1.NetObjectPrefixes.cluster + ":' + TOSTRING(vm.Host.ClusterID) IN (" + idsString + ")\n                      " + _this.getVirtualMachineFilterQuery(filter);
            return _this.swis.runQuery(swql)
                .then(function (result) {
                if (_.isEmpty(result.rows)) {
                    return null;
                }
                var virtualMachines = _.map(result.rows, function (vm) { return _this.generateVirtualMachineEntity(vm); });
                return {
                    rows: virtualMachines,
                    total: result.total
                };
            });
        };
        this.getClusterCountersInfo = function (clusterId) {
            var countersInfo = {};
            // clusters don't have custom threshold for storage so we load global datastore threshold 
            var query = "SELECT Name, Level1Value AS WarningValue, Level2Value AS CriticalValue\n                       FROM Orion.VIM.ClusterThresholds\n                       WHERE InstanceId = " + clusterId + " AND (Name = 'VIM.Clusters.Stats.CPULoad' OR Name = 'VIM.Clusters.Stats.MemUsage')\n                       UNION ALL\n                       (\n                       SELECT Name, GlobalWarningValue AS WarningValue, GlobalCriticalValue AS CriticalValue\n                       FROM Orion.VIM.DatastoreThresholds\n                       WHERE InstanceId = 0 AND Name = 'VIM.Datastores.Stats.SpaceUsage'\n                       )";
            return _this.swis.runQuery(query)
                .then(function (result) {
                _.each(result.rows, function (threshold) {
                    var counterName = _this.getCounterNameByThreshold(threshold.Name);
                    countersInfo[counterName] = _this.generateCounterInfo(threshold);
                });
                return countersInfo;
            });
        };
        this.generateReportDefinition = function (data) {
            var rd = new index_2.ReportDefinition();
            rd.id = data.ID;
            rd.name = data.Name;
            return rd;
        };
        this.generateEntity = function (data) {
            if (data.EntityType === index_1.ReportEntityType.Cluster) {
                return _this.generateClusterEntity(data);
            }
            throw new Error("Entity type " + data.EntityType + " is not supported.");
        };
        this.generateHostEntity = function (data) {
            var entity = {};
            entity.hostId = data.HostID;
            entity.hostName = data.HostName;
            entity.cpuCoreCount = data.CpuCoreCount;
            entity.cpuCoreFrequency = conversionHelper_1.ConversionHelper.convertHertz(data.CpuMhz, conversionHelper_1.HertzUnit.MHz, conversionHelper_1.HertzUnit.Hz);
            entity.diskSpaceCapacity = data.DiskSpaceCapacity;
            entity.memoryCapacity = data.MemorySize;
            return entity;
        };
        this.generateClusterEntity = function (data) {
            var entity = {};
            entity.entityType = index_1.ReportEntityType.Cluster;
            entity.netObjectId = data.NetObjectId;
            entity.name = data.Name;
            entity.datacenterName = data.DatacenterName;
            entity.vmsCount = data.VmsCount;
            entity.hostsCount = data.HostsCount;
            entity.datastoresCount = data.DatastoresCount;
            return entity;
        };
        this.getFilterQuery = function (filter) {
            var query = "";
            query += _this.getEntitiesWhereClause(filter);
            query += _this.getFilterSortingQuery(filter.sorting);
            query += _this.getFilterPaginationQuery(filter.pagination);
            return query;
        };
        this.getVirtualMachineFilterQuery = function (filter) {
            var query = "";
            query += _this.getVirtualMachineWhereClause(filter);
            query += _this.getFilterSortingQuery(filter.sorting);
            query += _this.getFilterPaginationQuery(filter.pagination);
            return query;
        };
        this.getHostsFilterQuery = function (filter) {
            var query = "";
            query += _this.getPrioritizedFilterSortingQuery(filter.sorting);
            query += _this.getFilterPaginationQuery(filter.pagination);
            return query;
        };
        this.getEntitiesWhereClause = function (filter) {
            var conditions = [];
            conditions.push("IsCapacityPlanningSupported > 0");
            if (filter.search) {
                conditions.push("(Name LIKE '%" + filter.search + "%' OR DatacenterName LIKE '%" + filter.search + "%')");
            }
            var conditionsJoined = conditions.join(" AND ");
            return "WHERE " + conditionsJoined;
        };
        this.getVirtualMachineWhereClause = function (filter) {
            var conditions = [];
            if (filter.search) {
                conditions.push("Name LIKE '%" + filter.search + "%'");
            }
            if (_.isEmpty(conditions)) {
                return "";
            }
            var conditionsJoined = conditions.join(" AND ");
            return "AND " + conditionsJoined;
        };
        this.getHostsWhereClause = function (filter) {
            var conditions = [];
            var ids = [];
            for (var _i = 0, _a = filter.parentNetObjectIds; _i < _a.length; _i++) {
                var parentNetObjectId = _a[_i];
                ids.push("'" + parentNetObjectId + "'");
            }
            conditions.push("'" + index_1.NetObjectPrefixes.cluster + ":' + TOSTRING(ClusterID) IN (" + ids + ")");
            if (filter.search) {
                conditions.push("HostName LIKE '%" + filter.search + "%'");
            }
            var conditionsJoined = conditions.join(" AND ");
            return "WHERE " + conditionsJoined;
        };
        this.getFilterSortingQuery = function (sorting) {
            var query = "";
            if (sorting) {
                var sortBy = sorting.sortBy.id;
                var sortDirection = sorting.direction;
                query += " ORDER BY " + sortBy + " " + sortDirection;
            }
            else {
                query += " ORDER BY Name ASC";
            }
            return query;
        };
        this.getPrioritizedFilterSortingQuery = function (sorting) {
            var query = "";
            if (sorting) {
                var sortBy = sorting.sortBy.id;
                var sortDirection = sorting.direction;
                query += " ORDER BY PrioritySort, " + sortBy + " " + sortDirection;
            }
            else {
                query += " ORDER BY PrioritySort, Name ASC";
            }
            return query;
        };
        this.getFilterPaginationQuery = function (pagination) {
            if (!pagination) {
                return "";
            }
            var startRow = (pagination.pageSize * (pagination.page - 1)) + 1;
            var endRow = startRow + pagination.pageSize - 1;
            return " WITH ROWS " + startRow + " TO " + endRow + " WITH TOTALROWS";
        };
        this.generateVirtualMachineEntity = function (data) {
            var entity = {};
            entity.id = data.VirtualMachineID;
            entity.name = data.Name;
            entity.memoryCapacity = data.MemoryConfigured;
            entity.cpuCoreCount = data.ProcessorCount;
            entity.diskSpaceCapacity = data.TotalStorageSize;
            return entity;
        };
        this.generateCounterInfo = function (data) {
            var info = {};
            info.criticalThreshold = data.CriticalValue;
            info.warningThreshold = data.WarningValue;
            info.exhaustedThreshold = 100;
            return info;
        };
    }
    ReportWizardService.prototype.getCounterNameByThreshold = function (thresholdName) {
        switch (thresholdName) {
            case "VIM.Clusters.Stats.CPULoad":
                return index_1.Counters.cpu;
            case "VIM.Clusters.Stats.MemUsage":
                return index_1.Counters.memory;
            case "VIM.Datastores.Stats.SpaceUsage":
                return index_1.Counters.disk;
            default:
                throw new Error("Unsupported threshold.");
        }
    };
    ReportWizardService = __decorate([
        __param(0, di_1.Inject(index_3.ApolloServiceNames.swApiService)),
        __param(1, di_1.Inject(index_3.ApolloServiceNames.swisService)),
        __param(2, di_1.Inject(index_3.ApolloServiceNames.dateTimeService)),
        __metadata("design:paramtypes", [Object, Object, Object])
    ], ReportWizardService);
    return ReportWizardService;
}());
exports.ReportWizardService = ReportWizardService;
;


/***/ }),
/* 224 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
var index_3 = __webpack_require__(25);
var jsonHelper_1 = __webpack_require__(33);
/**
 * Report detail service
 *
 * @class
 * @implements IReportDetailService
 */
var ReportDetailService = /** @class */ (function () {
    function ReportDetailService(swis, $q) {
        var _this = this;
        this.swis = swis;
        this.$q = $q;
        this.getReportsData = function (ids) {
            var swql = "SELECT df.Result.ID AS ResultID, df.ID as DefinitionID, df.Name, df.EntityCaption\n                        FROM Orion.VIM.CapacityPlanning.ReportDefinitions df\n                        WHERE df.DefinitionID IN (" + ids.join(", ") + ")";
            return _this.swis.runQuery(swql)
                .then(function (result) {
                if (_.isEmpty(result.rows)) {
                    return null;
                }
                return result.rows.map(function (row) {
                    return { reportResultId: row.ResultID, reportDefinitionId: row.DefinitionID, reportName: row.Name, clusterName: row.EntityCaption };
                });
            });
        };
        this.getReportDetail = function (id) {
            var swql = "SELECT df.ID, df.EntityCaption, df.Name, df.Counters, df.Runway, df.SimulatedWorkloadsEnabled, df.Result.ReportStatus, df.Result.ErrorMessage, \n                        df.Result.SimulationResult, df.Result.ComputationDateStarted, df.Result.ComputationDateFinished, df.FailoverReservation, df.Result.ID AS ResultID,\n                        df.SimulatedResourcesEnabled, df.UtilizationPeriodInDays, df.ResourceAllocationType\n                        FROM Orion.VIM.CapacityPlanning.ReportDefinitions df\n                        WHERE df.ID=" + id;
            return _this.swis.runQuery(swql)
                .then(function (result) {
                if (_.isEmpty(result.rows)) {
                    return null;
                }
                var report = _this.generateReportDetail(result.rows[0]);
                var results = [];
                if (report.simulatedWorkloadsEnabled) {
                    results.push(_this.getSimulatedVmProfiles(report.reportDefinitionId));
                }
                if (report.simulatedResourcesEnabled) {
                    results.push(_this.getSimulatedHostProfiles(report.reportDefinitionId));
                }
                if (results.length === 0) {
                    return report;
                }
                return _this.$q.all(results).then(function (profiles) {
                    if (report.simulatedResourcesEnabled) {
                        report.simulatedHostProfiles = profiles.pop();
                    }
                    if (report.simulatedWorkloadsEnabled) {
                        report.simulatedVmProfiles = profiles.pop();
                    }
                    return report;
                });
            });
        };
        this.generateReportDetail = function (data) {
            var report = new index_3.ReportDetail();
            report.reportStatus = index_1.ReportStatus[data.ReportStatus];
            report.errorMessage = data.ErrorMessage;
            if (report.reportStatus === index_1.ReportStatus.Finished) {
                report.reportDefinitionId = data.ID;
                report.name = data.Name;
                report.entityCaption = data.EntityCaption;
                report.runway = data.Runway;
                report.utilizationPeriodInDays = data.UtilizationPeriodInDays;
                report.reportResultId = data.ResultID;
                report.computationDateStarted = moment.utc(data.ComputationDateStarted);
                report.computationDateFinished = moment.utc(data.ComputationDateFinished);
                report.simulationResult = jsonHelper_1.JsonHelper.fromJsonWithLowerCase(data.SimulationResult);
                report.counters = jsonHelper_1.JsonHelper.fromJsonWithLowerCase(data.Counters);
                report.simulatedResourcesEnabled = data.SimulatedResourcesEnabled;
                report.simulatedWorkloadsEnabled = data.SimulatedWorkloadsEnabled;
                report.simulatedResourcesEnabled = data.SimulatedResourcesEnabled;
                report.failoverReservation = data.FailoverReservation;
                report.resourceAllocationType = data.ResourceAllocationType;
                _.forEach(report.simulationResult.counterDepletions, function (value) {
                    _this.convertToDuration(value.actual);
                    _this.convertToDuration(value.simulation);
                });
            }
            return report;
        };
        this.convertToDuration = function (depletion) {
            if (!depletion) {
                return;
            }
            if (depletion.capacityHitDepletion) {
                depletion.capacityHitDepletion = moment.duration(depletion.capacityHitDepletion);
            }
            if (depletion.criticalThresholdDepletion) {
                depletion.criticalThresholdDepletion = moment.duration(depletion.criticalThresholdDepletion);
            }
            if (depletion.warningThresholdDepletion) {
                depletion.warningThresholdDepletion = moment.duration(depletion.warningThresholdDepletion);
            }
        };
        this.getSimulatedVmProfiles = function (reportId) {
            var swql = "SELECT Name, InstanceCount, Counters, VirtualMachineId\n                        FROM Orion.VIM.CapacityPlanning.VMProfiles\n                        WHERE ReportDefinitionID=" + reportId;
            return _this.swis.runQuery(swql)
                .then(function (result) {
                if (_.isEmpty(result.rows)) {
                    return null;
                }
                var profiles = _.map(result.rows, function (data) {
                    return _this.generateVmProfile(data);
                });
                return profiles;
            });
        };
        this.generateVmProfile = function (data) {
            var profile = {
                name: data.Name,
                instanceCount: data.InstanceCount,
                virtualMachineId: data.VirtualMachineId,
                counters: jsonHelper_1.JsonHelper.fromJsonWithLowerCase(data.Counters)
            };
            return profile;
        };
        this.getSimulatedHostProfiles = function (reportId) {
            var swql = "SELECT Name, InstanceCount, CpuCoreCount, CpuCoreFrequency, MemoryCapacity, DiskSpaceCapacity\n                        FROM Orion.VIM.CapacityPlanning.HostProfiles\n                        WHERE ReportDefinitionID=" + reportId;
            return _this.swis.runQuery(swql)
                .then(function (result) {
                if (_.isEmpty(result.rows)) {
                    return null;
                }
                var profiles = _.map(result.rows, function (data) {
                    return _this.generateHostProfile(data);
                });
                return profiles;
            });
        };
        this.generateHostProfile = function (data) {
            var profile = {
                name: data.Name,
                instanceCount: data.InstanceCount,
                cpuCoreCount: data.CpuCoreCount,
                cpuCoreFrequency: data.CpuCoreFrequency,
                memoryCapacity: data.MemoryCapacity,
                diskSpaceCapacity: data.DiskSpaceCapacity
            };
            return profile;
        };
    }
    ReportDetailService = __decorate([
        __param(0, di_1.Inject(index_2.ApolloServiceNames.swisService)),
        __param(1, di_1.Inject(index_2.NgServiceNames.ngQService)),
        __metadata("design:paramtypes", [Object, Function])
    ], ReportDetailService);
    return ReportDetailService;
}());
exports.ReportDetailService = ReportDetailService;


/***/ }),
/* 225 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(1);
var di_1 = __webpack_require__(0);
var index_2 = __webpack_require__(3);
/**
* VIM Capacity Planning Web Utils service
*/
var WebUtilsService = /** @class */ (function () {
    function WebUtilsService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.getHelpUrl = function (fragment) {
            var request = { Fragment: fragment };
            return _this.swApi.api(false).one(index_2.SwApiUrls.getHelpUrl).get(request);
        };
    }
    WebUtilsService = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.swApiService)),
        __metadata("design:paramtypes", [Object])
    ], WebUtilsService);
    return WebUtilsService;
}());
exports.WebUtilsService = WebUtilsService;
;


/***/ }),
/* 226 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = __webpack_require__(227);
__webpack_require__(228);
exports.default = function (module) {
    http_1.default(module);
};


/***/ }),
/* 227 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function configHttp($httpProvider) {
    // configure $http service to combine processing of multiple http responses received at around the same time 
    $httpProvider.useApplyAsync(true);
}
exports.configHttp = configHttp;
;
configHttp.$inject = ["$httpProvider"];
exports.default = function (module) {
    module.config(configHttp);
};


/***/ }),
/* 228 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */

/**
 * Requires dynamic context for .less files - i.e. loads all of them
 */
var req = __webpack_require__(229);


/***/ }),
/* 229 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./modules/capacityPlanning/directives/reportDetail/counterSummary/depletions/depletions-directive.less": 55,
	"./modules/capacityPlanning/directives/reportDetail/counterSummary/depletionsChart/depletionsChart-directive.less": 56,
	"./modules/capacityPlanning/directives/reportDetail/counterSummary/pieChart/pieChart-directive.less": 57,
	"./modules/capacityPlanning/directives/reportDetail/environmentSummary/environmentSummary-directive.less": 54,
	"./modules/capacityPlanning/directives/reportDetail/hostProfileSummary/hostProfileSummary-directive.less": 59,
	"./modules/capacityPlanning/directives/reportDetail/vmProfileSummary/vmProfileSummary-directive.less": 58,
	"./modules/capacityPlanning/directives/reportFooter/reportFooter-directive.less": 61,
	"./modules/capacityPlanning/directives/reportHeader/reportHeader-directive.less": 60,
	"./modules/capacityPlanning/directives/reportList/reportList-directive.less": 46,
	"./modules/capacityPlanning/directives/reportNavigation/reportNavigation.less": 230,
	"./modules/capacityPlanning/directives/reportWizard/reportWizard-directive.less": 47,
	"./modules/capacityPlanning/directives/reportWizard/steps/entitySelectionStep/entitySelectionStep-directive.less": 48,
	"./modules/capacityPlanning/directives/reportWizard/steps/hostProfileStep/customHostProfile/customHostProfile-directive.less": 51,
	"./modules/capacityPlanning/directives/reportWizard/steps/hostProfileStep/hostProfileStep-directive.less": 50,
	"./modules/capacityPlanning/directives/reportWizard/steps/timeFrameStep/timeFrameStep-directive.less": 49,
	"./modules/capacityPlanning/directives/reportWizard/steps/vmProfilesStep/vmProfilesStep-directive.less": 52,
	"./modules/capacityPlanning/directives/vmProfileWizard/steps/newCustomProfileStep/newCustomProfileStep-directive.less": 53,
	"./modules/capacityPlanning/styles.less": 231,
	"./modules/capacityPlanning/styles/styles.less": 232,
	"./modules/capacityPlanning/views/reportDetail/reportDetail.less": 233,
	"./styles.less": 44,
	"./widgets/eventDetailsWidget/eventDetailsWidget.less": 43,
	"./widgets/styles.less": 234
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 229;

/***/ }),
/* 230 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 231 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 232 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 233 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 234 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 235 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    // noop
};


/***/ }),
/* 236 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(24);
var dateTime_filter_1 = __webpack_require__(237);
var counterUnit_filter_1 = __webpack_require__(238);
var bytesFormat_filter_1 = __webpack_require__(239);
var numberFormat_filter_1 = __webpack_require__(240);
var hertzFormat_filter_1 = __webpack_require__(241);
var duration_filter_1 = __webpack_require__(242);
exports.default = function (module) {
    module.filter(index_1.FilterNames.dateTimeFilter, dateTime_filter_1.dateTimeFilterFactory);
    module.filter(index_1.FilterNames.counterUnitFilter, counterUnit_filter_1.counterUnitFilterFactory);
    module.filter(index_1.FilterNames.bytesFormatFilter, bytesFormat_filter_1.bytesFormatFilterFactory);
    module.filter(index_1.FilterNames.numberFormatFilter, numberFormat_filter_1.numberFormatFilterFactory);
    module.filter(index_1.FilterNames.hertzFormatFilter, hertzFormat_filter_1.hertzFormatFilterFactory);
    module.filter(index_1.FilterNames.durationFormatFilter, duration_filter_1.durationFilterFactory);
};


/***/ }),
/* 237 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(2);
exports.dateTimeFilterFactory = function (dateTimeService, _t) {
    return function (valueUtc, format) {
        if (!valueUtc || !valueUtc.isValid()) {
            return _t("N/A");
        }
        // move to Orion timezone
        var valueOrion = dateTimeService.moveMomentToOrionTimezone(valueUtc);
        // check if today
        var orionToday = dateTimeService.getOrionNowMoment().startOf("day");
        var valueOrionDate = moment(valueOrion).startOf("day");
        if (valueOrionDate.toISOString() === orionToday.toISOString()) {
            return _t("Today");
        }
        return dateTimeService.formatMoment(valueOrion, format);
    };
};
exports.dateTimeFilterFactory.$inject = [index_1.ApolloServiceNames.dateTimeService, index_1.ApolloServiceNames.getTextService];


/***/ }),
/* 238 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(24);
var index_2 = __webpack_require__(2);
exports.counterUnitFilterFactory = function (bytesFilter, numberFilter, hertzFilter, _t) {
    return function (input, format) {
        switch (format) {
            case "bytes":
                return bytesFilter(input, 1, false);
            case "clock":
                return hertzFilter(input, 0, false);
            case "cores":
                return numberFilter(input, 0, _t("cores"), _t("core"), false);
            default:
                return input.toString();
        }
    };
};
exports.counterUnitFilterFactory.$inject = [
    index_1.FilterNames.bytesFormatFilter + "Filter",
    index_1.FilterNames.numberFormatFilter + "Filter",
    index_1.FilterNames.hertzFormatFilter + "Filter",
    index_2.ApolloServiceNames.getTextService
];


/***/ }),
/* 239 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var kilo = 1024;
/**
 * Filter factory
 *
 * @function
 */
exports.bytesFormatFilterFactory = function () {
    return function (bytes, precision, plusSign) {
        if (!isFinite(bytes)) {
            return "---";
        }
        var units = ["B", "kB", "MB", "GB", "TB", "PB"];
        if (bytes === 0) {
            return bytes + " " + units[3];
        }
        precision = precision || 1;
        var prefix = plusSign && bytes > 0 ? "+" : "";
        var order = Math.floor(Math.log(Math.abs(bytes)) / Math.log(kilo));
        var value = bytes / Math.pow(kilo, Math.floor(order));
        // fix the precision edge case (we store bytes as float in the backend)
        var valueCeiled = Math.ceil(value);
        if (valueCeiled % kilo === 0) {
            value = valueCeiled / kilo;
            order += 1;
        }
        var strValue = (value).toFixed(precision);
        // remove trailing zeros
        var matches = strValue.match(/([0-9]+)\.0*$/);
        if (matches != null && matches.length === 2) {
            strValue = matches[1];
        }
        return prefix + strValue + " " + units[order];
    };
};


/***/ }),
/* 240 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Number format filter factory implemenatation
 *
 * @function
 */
exports.numberFormatFilterFactory = function () {
    return function (input, precision, suffix, singularSuffix, plusSign) {
        precision = angular.isNumber(precision) ? precision : 2;
        suffix = suffix || "%";
        var prefix = plusSign && input > 0 ? "+" : "";
        if (isNaN(input)) {
            return "---";
        }
        if (input === 1 && singularSuffix) {
            suffix = singularSuffix;
        }
        return prefix + Math.round(input * Math.pow(10, precision)) / Math.pow(10, precision) + " " + suffix;
    };
};


/***/ }),
/* 241 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var kilo = 1000;
/**
 * Filter factory
 *
 * @function
 */
exports.hertzFormatFilterFactory = function () {
    return function (hertz, precision, plusSign) {
        if (!isFinite(hertz)) {
            return "---";
        }
        var units = ["Hz", "kHz", "MHz", "GHz", "THz", "PHz"];
        if (hertz === 0) {
            return hertz + " " + units[2];
        }
        precision = precision || 1;
        var prefix = plusSign && hertz > 0 ? "+" : "";
        var order = Math.floor(Math.log(Math.abs(hertz)) / Math.log(kilo));
        var value = hertz / Math.pow(kilo, Math.floor(order));
        // fix the precision edge case (we store bytes as float in the backend)
        var valueCeiled = Math.ceil(value);
        if (valueCeiled % kilo === 0) {
            value = valueCeiled / kilo;
            order += 1;
        }
        var strValue = (value).toFixed(precision);
        // remove trailing zeros
        var matches = strValue.match(/([0-9]+)\.0*$/);
        if (matches != null && matches.length === 2) {
            strValue = matches[1];
        }
        return prefix + strValue + " " + units[order];
    };
};


/***/ }),
/* 242 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(2);
/**
 * Filter factory
 *
 * @function
 */
exports.durationFilterFactory = function (_t, util) {
    function getFormattedString(value, singularStr, pluralStr) {
        var base = value > 1 ? pluralStr : singularStr;
        return util.formatString(base, value);
    }
    return function (duration) {
        if (!duration) {
            return _t("N/A");
        }
        var years = duration.asYears();
        if (years > 0.9) {
            if (years > 20) {
                return _t(">20 years");
            }
            return getFormattedString(Math.round(years), _t("{0} year"), _t("{0} years"));
        }
        var months = Math.round(duration.asMonths());
        if (months > 0) {
            return getFormattedString(months, _t("{0} month"), _t("{0} months"));
        }
        var days = Math.round(duration.asDays());
        if (days > 0) {
            return getFormattedString(days, _t("{0} day"), _t("{0} days"));
        }
        var hours = Math.round(duration.asHours());
        if (hours > 0) {
            return getFormattedString(hours, _t("{0} hour"), _t("{0} hours"));
        }
        return _t("Now");
    };
};
exports.durationFilterFactory.$inject = [
    index_1.ApolloServiceNames.getTextService,
    index_1.ApolloServiceNames.swUtilService
];


/***/ })
/******/ ]);
//# sourceMappingURL=vim.js.map