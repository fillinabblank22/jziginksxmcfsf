/*!
 * @solarwinds/vim 8.8.0-1179
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 251);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Helper function for autogenerating of class's $inject property
 *
 * @param {string} dependency Name of a dependency under which injected component is registered (e.g. xuiToastService)
 * @example
 * import {Inject} from "decorators/di";
 * class MyClass { constructor(@Inject("xuiToastService") private svc: IXuiToastService){} };
 */
function Inject(dependency) {
    return function (prototype, method, argumentPosition) {
        prototype.$inject = prototype.$inject || [];
        prototype.$inject[argumentPosition] = dependency;
    };
}
exports.Inject = Inject;


/***/ }),

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(2);
var VmService = /** @class */ (function () {
    /** @ngInject */
    VmService.$inject = ["swis"];
    function VmService(swis) {
        var _this = this;
        this.swis = swis;
        /**
         * Returns status of vm with given ID
         */
        this.getVmStatus = function (vmId) {
            var logEntrySwql = "SELECT si.IconPostfix as status\n                              FROM Orion.VIM.VirtualMachines vm\n                              LEFT JOIN Orion.StatusInfo si ON vm.Status = si.StatusId\n                              WHERE vm.VirtualMachineID = @VmId";
            return _this.swis.runQuery(logEntrySwql, { "VmId": vmId })
                .then(function (eventResult) {
                if (_.isEmpty(eventResult.rows)) {
                    return null;
                }
                return eventResult.rows[0].status;
            });
        };
    }
    VmService = __decorate([
        __param(0, di_1.Inject(index_1.ApolloServiceNames.swisService)),
        __metadata("design:paramtypes", [Object])
    ], VmService);
    return VmService;
}());
exports.VmService = VmService;


/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var services_1 = __webpack_require__(2);
var events_service_1 = __webpack_require__(9);
var vm_service_1 = __webpack_require__(10);
var events_service_2 = __webpack_require__(9);
exports.EventsService = events_service_2.EventsService;
var vm_service_2 = __webpack_require__(10);
exports.VmService = vm_service_2.VmService;
exports.default = function (module) {
    module.service(services_1.ServiceNames.eventsService, events_service_1.EventsService);
    module.service(services_1.ServiceNames.vmService, vm_service_1.VmService);
};


/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Basic colors
 */
var Colors = /** @class */ (function () {
    function Colors() {
    }
    /* Primary blue */
    Colors.primaryBlue = "#297994";
    /* Light gray */
    Colors.lightGray = "#D5D5D5";
    /* Chart orange */
    Colors.chartOrange = "#ff7300";
    /* Chart warning background */
    Colors.chartWarningBackground = "#FCF8E0";
    /* Chart critical background */
    Colors.chartCriticalBackground = "#FFE4E0";
    /* Chart ultramarine */
    Colors.chartUltramarine = "#6666C0";
    /* Chart ultramarine light */
    Colors.chartUltramarineLight = "#a6a6db";
    return Colors;
}());
exports.Colors = Colors;


/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baseUrl = "vim/general";
var getLogEntryIdBuAlertIdUrlMethod = "get-logentryid-by-alertid";
var getTopLevelVMwareNodesNotLicensedInLM = "get-top-level-VMware-nodes-not-licensed-in-LM";
/**
 * Backend urls used by vim module
 *
 * @const
 */
exports.SwApiUrls = {
    baseUrl: baseUrl,
    getLogEntryIdBuAlertIdUrlMethod: getLogEntryIdBuAlertIdUrlMethod,
    getLogEntryIdBuAlertIdUrl: baseUrl + "/" + getLogEntryIdBuAlertIdUrlMethod,
    getTopLevelVMwareNodesNotLicensedInLM: baseUrl + "/" + getTopLevelVMwareNodesNotLicensedInLM,
};


/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * SWIS Entities names
 *
 * @const
 */
exports.SwisEntitiesNames = {
    vm: "Orion.VIM.VirtualMachines"
};


/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Possible virtualization platforms
 */
var Platforms;
(function (Platforms) {
    Platforms[Platforms["VMware"] = 1] = "VMware";
    Platforms[Platforms["HyperV"] = 2] = "HyperV";
})(Platforms = exports.Platforms || (exports.Platforms = {}));
;


/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of services provided by Angular and related libraries
 */
exports.NgServiceNames = {
    ngWindowService: "$window",
    ngQService: "$q",
    ngScope: "$scope",
    ngState: "$state",
    ngStateParams: "$stateParams",
    ngProvideService: "$provide",
    ngControllerService: "$controller",
    ngRootScopeService: "$rootScope",
    ngHttpBackendService: "$httpBackend",
    ngInjectorService: "$injector",
    ngTimeoutService: "$timeout",
    ngSanitizeService: "$sanitize"
};
/**
 * Names of services provided by Apollo
 */
exports.ApolloServiceNames = {
    toastService: "xuiToastService",
    getTextService: "getTextService",
    swApiService: "swApi",
    swUtilService: "swUtil",
    swisService: "swisService",
    dateTimeService: "swDateTimeService",
    dialogService: "xuiDialogService",
    xuiFilteredListService: "xuiFilteredListService",
    wizardDialogService: "xuiWizardDialogService",
    helpLinkService: "helpLinkService"
};
/**
 * Service names used by VIM
 *
 * @class
 */
exports.ServiceNames = {
    eventsService: "vimEventsService",
    vmService: "vimVmService"
};


/***/ }),

/***/ 251:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(11);


/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Possible orion user roles
 */
exports.Roles = {
    admin: "admin",
    demo: "demo"
};


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * VIM module names
 *
 * @const
 */
exports.ModuleNames = {
    main: "vim",
    providers: "vim.providers",
    services: "vim.services",
    filters: "vim.filters"
};


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(7));
__export(__webpack_require__(5));
__export(__webpack_require__(4));
__export(__webpack_require__(8));
__export(__webpack_require__(13));
__export(__webpack_require__(14));
__export(__webpack_require__(15));
__export(__webpack_require__(16));


/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var roles_1 = __webpack_require__(4);
/**
 * Auth profiles used by Recommendations engine
 *
 * @const
 * @var
 */
exports.AuthProfiles = {
    guestProfile: {
        roles: [],
        permissions: []
    },
    adminProfile: {
        roles: [roles_1.Roles.admin],
        permissions: []
    },
    adminProfileDemoAllowed: {
        roles: [roles_1.Roles.admin, roles_1.Roles.demo],
        permissions: []
    }
};


/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constants for net object prefixes managed by VIM
 *
 * @const
 */
exports.NetObjectPrefixes = {
    cluster: "VMC",
    host: "VH"
};


/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(0);
var index_1 = __webpack_require__(2);
var index_2 = __webpack_require__(6);
var EventsService = /** @class */ (function () {
    /** @ngInject */
    EventsService.$inject = ["$window", "swis", "swApi"];
    function EventsService($window, swis, swApi) {
        var _this = this;
        this.$window = $window;
        this.swis = swis;
        this.swApi = swApi;
        /**
         * Returns Details for Event with given ID
         */
        this.getEventDetail = function (logEntryId) {
            var logEntrySwql = "SELECT le.LogEntryID as logEntryId, le.Message as message, le.Level as level, le.DateTime as messageDateTime, sl.TextValue as user\n                              FROM Orion.OLM.LogEntry le\n                              LEFT JOIN \n                                  (SELECT lefv.LogEntryID, lefv.TextValue from Orion.OLM.LogEntryFieldValue lefv\n                                  WHERE lefv.Name='User' AND lefv.LogEntryID = @LogEntryId) sl\n                              ON le.LogEntryID = sl.LogEntryID \n                              WHERE le.LogEntryID = @LogEntryId";
            return _this.swis.runQuery(logEntrySwql, { "LogEntryId": logEntryId })
                .then(function (eventResult) {
                if (_.isEmpty(eventResult.rows)) {
                    return null;
                }
                return _this.getSecondarySources(logEntryId).then(function (secSourcesResult) {
                    var row = eventResult.rows[0];
                    return {
                        logEntryId: logEntryId,
                        message: row.message,
                        level: row.level,
                        messageDateTime: moment.utc(row.messageDateTime).local(),
                        user: row.user,
                        secondarySources: secSourcesResult
                    };
                });
            });
        };
        /**
         * Returns Details for Event related to alert with given ID
         */
        this.getEventRelatedToAlert = function (alertId) {
            return _this.getLogEntryIdByAlertId(alertId)
                .then(function (logEntryId) {
                if (_.isEmpty(logEntryId)) {
                    return undefined;
                }
                return _this.getEventDetail(logEntryId);
            });
        };
        this.getLogEntryIdByAlertId = function (alertId) {
            var request = { AlertObjectId: alertId };
            return _this.swApi.api(false).one(index_2.SwApiUrls.baseUrl).post(index_2.SwApiUrls.getLogEntryIdBuAlertIdUrlMethod, request);
        };
    }
    /**
     * Returns Log manager Entry ID from URL
     * Currently it is impossible to get it from OPID as Log Manager page does not provide it (CORE-11786)
     */
    EventsService.prototype.getLmeIdFromUrl = function () {
        var viewNetObject = this.getViewNetObject();
        if (viewNetObject && viewNetObject.prefix === "LME") {
            return viewNetObject.value;
        }
        return null;
    };
    /**
     * Returns info about OLM Nodes license usage.
     */
    EventsService.prototype.getOlmNodesLicenseUsage = function () {
        var query = "SELECT ElementCount AS used, MaxCount AS total\n        FROM Orion.LicenseSaturation WHERE ElementType = 'OLM.OLMNodes'";
        return this.swis.runQuery(query)
            .then(function (result) {
            if (_.isEmpty(result.rows)) {
                return null;
            }
            return result.rows[0];
        });
    };
    /**
     * Returns collection of top level VMware entities which are not covered by LM license.
     */
    EventsService.prototype.getTopLevelVmwareNodesNotLicensedInLM = function () {
        return this.swApi.api(false).one(index_2.SwApiUrls.getTopLevelVMwareNodesNotLicensedInLM).get()
            .then(function (response) {
            return response.plain().entities;
        });
    };
    EventsService.prototype.getSecondarySources = function (logEntryId) {
        var secondarySourcesSwql = "SELECT ss.Label as label, ss.Identity as identity\n                              FROM Orion.OLM.LogEntrySecondarySources as ss\n                              JOIN Orion.OLM.LogEntrySecondarySourceAssignment ssa ON ssa.LogEntrySecondarySourceID = ss.LogEntrySecondarySourceID\n                              WHERE ssa.LogEntryID = @LogEntryId";
        var secondarySourcesResult = [];
        return this.swis.runQuery(secondarySourcesSwql, { "LogEntryId": logEntryId })
            .then(function (secSourcesResult) {
            if (_.isEmpty(secSourcesResult.rows)) {
                return secondarySourcesResult;
            }
            for (var _i = 0, _a = secSourcesResult.rows; _i < _a.length; _i++) {
                var row = _a[_i];
                secondarySourcesResult.push(row);
            }
            ;
            return secondarySourcesResult;
        });
    };
    EventsService.prototype.getViewNetObject = function () {
        var netObject = decodeURIComponent(this.$window.location.search).match(/NetObject=([A-Z0-9]+):(.+?)(?:&|$)/i);
        if (netObject) {
            return { prefix: netObject[1], value: netObject[2] };
        }
        else {
            return null;
        }
    };
    EventsService = __decorate([
        __param(0, di_1.Inject(index_1.NgServiceNames.ngWindowService)),
        __param(1, di_1.Inject(index_1.ApolloServiceNames.swisService)),
        __param(2, di_1.Inject(index_1.ApolloServiceNames.swApiService)),
        __metadata("design:paramtypes", [Object, Object, Object])
    ], EventsService);
    return EventsService;
}());
exports.EventsService = EventsService;


/***/ })

/******/ });
//# sourceMappingURL=services.js.map