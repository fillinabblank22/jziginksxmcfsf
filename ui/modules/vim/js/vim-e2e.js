/*!
 * @solarwinds/vim 8.8.0-1179
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 256);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(2);
exports.NgServiceNames = index_1.NgServiceNames;
exports.ApolloServiceNames = index_1.ApolloServiceNames;
var index_2 = __webpack_require__(2);
exports.MainServiceNames = index_2.ServiceNames;
/**
 * Service names used by capacity planning
 *
 * @class
 */
exports.ServiceNames = {
    licensingService: "vimCapacityPlanningLicensingService",
    routingRegistrationService: "vimCapacityPlanningRoutingRegistration",
    routingService: "vimCapacityPlanningRoutingService",
    reportDetailService: "vimCapacityPlanningReportDetailService",
    reportListService: "vimCapacityPlanningReportListService",
    reportWizardService: "vimCapacityPlanningReportWizardService",
    webUtilsService: "vimCapacityPlanningWebUtilsService"
};


/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(35));
__export(__webpack_require__(17));
__export(__webpack_require__(25));
__export(__webpack_require__(26));
__export(__webpack_require__(34));


/***/ }),

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(37));
__export(__webpack_require__(38));


/***/ }),

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(33));
__export(__webpack_require__(19));
__export(__webpack_require__(41));


/***/ }),

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var BytesUnit;
(function (BytesUnit) {
    BytesUnit[BytesUnit["B"] = 0] = "B";
    BytesUnit[BytesUnit["kB"] = 1] = "kB";
    BytesUnit[BytesUnit["MB"] = 2] = "MB";
    BytesUnit[BytesUnit["GB"] = 3] = "GB";
    BytesUnit[BytesUnit["TB"] = 4] = "TB";
    BytesUnit[BytesUnit["PB"] = 5] = "PB";
})(BytesUnit = exports.BytesUnit || (exports.BytesUnit = {}));
;
var HertzUnit;
(function (HertzUnit) {
    HertzUnit[HertzUnit["Hz"] = 0] = "Hz";
    HertzUnit[HertzUnit["kHz"] = 1] = "kHz";
    HertzUnit[HertzUnit["MHz"] = 2] = "MHz";
    HertzUnit[HertzUnit["GHz"] = 3] = "GHz";
    HertzUnit[HertzUnit["THz"] = 4] = "THz";
})(HertzUnit = exports.HertzUnit || (exports.HertzUnit = {}));
;
/**
 * Helper for unit conversion
 */
var ConversionHelper = /** @class */ (function () {
    function ConversionHelper() {
    }
    /**
     * Converts value from defined unit to target unit, returns undefined if any of params is undefined
     */
    ConversionHelper.convertBytes = function (value, inputUnit, outputUnit) {
        if (_.isUndefined(inputUnit)) {
            throw new Error("InputUnit is not defined.");
        }
        if (_.isUndefined(outputUnit)) {
            throw new Error("OutputUnit is not defined.");
        }
        if (_.isUndefined(value) || _.isNull(value)) {
            return undefined;
        }
        if (inputUnit === outputUnit) {
            return value;
        }
        var exponent = inputUnit - outputUnit;
        return value * Math.pow(1024, Math.floor(exponent));
    };
    /**
     * Converts value from defined unit to target unit, returns undefined if any of params is undefined
     */
    ConversionHelper.convertHertz = function (value, inputUnit, outputUnit) {
        if (_.isUndefined(inputUnit)) {
            throw new Error("InputUnit is not defined.");
        }
        if (_.isUndefined(outputUnit)) {
            throw new Error("OutputUnit is not defined.");
        }
        if (_.isUndefined(value) || _.isNull(value)) {
            return undefined;
        }
        if (inputUnit === outputUnit) {
            return value;
        }
        var exponent = inputUnit - outputUnit;
        return value * Math.pow(1000, Math.floor(exponent));
    };
    return ConversionHelper;
}());
exports.ConversionHelper = ConversionHelper;


/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of services provided by Angular and related libraries
 */
exports.NgServiceNames = {
    ngWindowService: "$window",
    ngQService: "$q",
    ngScope: "$scope",
    ngState: "$state",
    ngStateParams: "$stateParams",
    ngProvideService: "$provide",
    ngControllerService: "$controller",
    ngRootScopeService: "$rootScope",
    ngHttpBackendService: "$httpBackend",
    ngInjectorService: "$injector",
    ngTimeoutService: "$timeout",
    ngSanitizeService: "$sanitize"
};
/**
 * Names of services provided by Apollo
 */
exports.ApolloServiceNames = {
    toastService: "xuiToastService",
    getTextService: "getTextService",
    swApiService: "swApi",
    swUtilService: "swUtil",
    swisService: "swisService",
    dateTimeService: "swDateTimeService",
    dialogService: "xuiDialogService",
    xuiFilteredListService: "xuiFilteredListService",
    wizardDialogService: "xuiWizardDialogService",
    helpLinkService: "helpLinkService"
};
/**
 * Service names used by VIM
 *
 * @class
 */
exports.ServiceNames = {
    eventsService: "vimEventsService",
    vmService: "vimVmService"
};


/***/ }),

/***/ 21:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * VIM module names
 *
 * @const
 */
exports.ModuleNames = {
    main: "vimCapacityPlanning",
    components: "vimCapacityPlanning.components",
    providers: "vimCapacityPlanning.providers",
    services: "vimCapacityPlanning.services"
};


/***/ }),

/***/ 22:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Determines what kind of profile should be used
 */
var VmProfileTypes;
(function (VmProfileTypes) {
    VmProfileTypes[VmProfileTypes["Existing"] = 1] = "Existing";
    VmProfileTypes[VmProfileTypes["Custom"] = 2] = "Custom";
})(VmProfileTypes = exports.VmProfileTypes || (exports.VmProfileTypes = {}));


/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(39));


/***/ }),

/***/ 256:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(2);
var moduleNames_1 = __webpack_require__(5);
var index_e2e_1 = __webpack_require__(257);
var setupApiCalls = function ($q, $httpBackend) {
    // blocks all swis queries
    $httpBackend.whenPOST(/Information\.asmx/).respond(function () { return $q.reject("Running E2E"); });
    $httpBackend.whenPOST(/api2\/swis\/query/).respond(function () { return $q.reject("Running E2E"); });
};
var setupOrionEnvironment = function () {
    // mock Orion timezone offset
    window.SW = window.SW || {};
    window.SW.environment = window.SW.environment || {};
    window.SW.environment.timezoneOffsetMinutes = 0;
};
var disableRouting = function ($rootScope) {
    // disable routing for component tests
    $rootScope.$on("$stateChangeStart", function (e) {
        e.preventDefault();
    });
};
var e2eModule = angular.module("vim.e2e", [moduleNames_1.ModuleNames.main, "ngMockE2E"])
    .run([index_1.NgServiceNames.ngQService, index_1.NgServiceNames.ngHttpBackendService, setupApiCalls])
    .run([setupOrionEnvironment])
    .run([index_1.NgServiceNames.ngRootScopeService, disableRouting]);
index_e2e_1.default(e2eModule);


/***/ }),

/***/ 257:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
var reportList_e2e_1 = __webpack_require__(258);
var reportWizard_e2e_1 = __webpack_require__(259);
var reportDetail_e2e_1 = __webpack_require__(260);
var reportHeader_e2e_1 = __webpack_require__(261);
var reportFooter_e2e_1 = __webpack_require__(262);
var mockBackendCalls = function ($q, $httpBackend) {
    var isLicnsedRegExp = new RegExp(_.escapeRegExp(index_1.SwApiUrls.isLicensedUrl), "i");
    $httpBackend.whenGET(isLicnsedRegExp).respond(function () { return $q.resolve(true); });
    var getHelpRexExp = new RegExp(_.escapeRegExp(index_1.SwApiUrls.getHelpUrl), "i");
    $httpBackend.whenGET(getHelpRexExp).respond(function () { return $q.resolve("helpLink"); });
};
exports.default = function (module) {
    module.run([index_2.NgServiceNames.ngQService, index_2.NgServiceNames.ngHttpBackendService, mockBackendCalls]);
    reportList_e2e_1.default(module);
    reportWizard_e2e_1.default(module);
    reportDetail_e2e_1.default(module);
    reportHeader_e2e_1.default(module);
    reportFooter_e2e_1.default(module);
};


/***/ }),

/***/ 258:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(1);
exports.default = function (module) {
    module.controller("E2E_CapacityPlanning_ReportListController", ReportListController);
};
var ReportListController = /** @class */ (function () {
    function ReportListController($q, reportListService) {
        // setup services
        reportListService.getReportsFilterStats = function () {
            // todo
            return $q.resolve([]);
        };
        reportListService.getReports = function (filter) {
            var result = {
                rows: [
                    {
                        id: 12,
                        resultId: 1122,
                        name: "Report 1",
                        entityCaption: "Cluster 1",
                        entityStatus: "up",
                        entityPlatform: "vmwarecluster",
                        computationDateFinished: moment.utc({ year: 2010, month: 11, day: 2 }),
                        runway: 0,
                        simulatedResourcesEnabled: true,
                        simulatedWorkloadsEnabled: true
                    },
                    {
                        id: 34,
                        resultId: 112234,
                        name: "Report 2",
                        entityCaption: "Cluster 2",
                        entityStatus: "up",
                        entityPlatform: "vmwarecluster",
                        computationDateStarted: moment.utc({ year: 2010, month: 11, day: 1 }),
                        computationDateFinished: moment.utc({ year: 2010, month: 11, day: 2 }),
                        runway: 20,
                        simulatedResourcesEnabled: false,
                        simulatedWorkloadsEnabled: true
                    }
                ],
                total: 2
            };
            return $q.resolve(result);
        };
    }
    ;
    ReportListController.$inject = [index_1.NgServiceNames.ngQService, index_1.ServiceNames.reportListService];
    return ReportListController;
}());
;


/***/ }),

/***/ 259:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var hostProfileValidationWrapper_1 = __webpack_require__(34);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(17);
var index_3 = __webpack_require__(1);
var index_4 = __webpack_require__(18);
var vmProfileValidationWrapper_1 = __webpack_require__(26);
exports.default = function (module) {
    module.controller("E2E_CapacityPlanning_ReportWizardController", ReportWizardController);
};
var ReportWizardController = /** @class */ (function () {
    function ReportWizardController($q, reportWizardService) {
        var _this = this;
        this.newReport = new index_2.ReportDefinition();
        this.insertOperationSubmitted = false;
        this.createDefaultCounterInfo = function () {
            return {
                criticalThreshold: 90,
                exhaustedThreshold: 100,
                warningThreshold: 80
            };
        };
        // setup services
        reportWizardService.getEntities = function (filter) {
            var result = {
                rows: [
                    {
                        entityType: index_1.ReportEntityType.Cluster,
                        netObjectId: "VMC:1",
                        name: "Cluster1",
                        datacenterName: "Datacenter1",
                        vmsCount: 100,
                        hostsCount: 10,
                        datastoresCount: 5
                    },
                    {
                        entityType: index_1.ReportEntityType.Cluster,
                        netObjectId: "VMC:2",
                        name: "Cluster2",
                        datacenterName: "Datacenter2",
                        vmsCount: 10,
                        hostsCount: 2,
                        datastoresCount: 1
                    },
                    {
                        entityType: index_1.ReportEntityType.Cluster,
                        netObjectId: "VMC:3",
                        name: "Cluster3",
                        datacenterName: "Datacenter1",
                        vmsCount: 50,
                        hostsCount: 15,
                        datastoresCount: 2
                    }
                ],
                total: 3
            };
            return $q.resolve(result);
        };
        // setup services
        reportWizardService.getExistingHosts = function (filter) {
            var result = {
                rows: [
                    {
                        hostName: "Host1",
                        hostId: 1,
                        cpuCoreCount: 4,
                        cpuCoreFrequency: index_4.ConversionHelper.convertHertz(3, index_4.HertzUnit.GHz, index_4.HertzUnit.Hz),
                        diskSpaceCapacity: index_4.ConversionHelper.convertBytes(2, index_4.BytesUnit.TB, index_4.BytesUnit.B),
                        memoryCapacity: index_4.ConversionHelper.convertBytes(4, index_4.BytesUnit.GB, index_4.BytesUnit.B),
                    },
                    {
                        hostName: "Host2",
                        hostId: 1,
                        cpuCoreCount: 4,
                        cpuCoreFrequency: index_4.ConversionHelper.convertHertz(3, index_4.HertzUnit.GHz, index_4.HertzUnit.Hz),
                        diskSpaceCapacity: index_4.ConversionHelper.convertBytes(2, index_4.BytesUnit.TB, index_4.BytesUnit.B),
                        memoryCapacity: index_4.ConversionHelper.convertBytes(4, index_4.BytesUnit.GB, index_4.BytesUnit.B),
                    },
                    {
                        hostName: "Host3",
                        hostId: 1,
                        cpuCoreCount: 4,
                        cpuCoreFrequency: index_4.ConversionHelper.convertHertz(3, index_4.HertzUnit.GHz, index_4.HertzUnit.Hz),
                        diskSpaceCapacity: index_4.ConversionHelper.convertBytes(2, index_4.BytesUnit.TB, index_4.BytesUnit.B),
                        memoryCapacity: index_4.ConversionHelper.convertBytes(4, index_4.BytesUnit.GB, index_4.BytesUnit.B),
                    }
                ],
                total: 3
            };
            return $q.resolve(result);
        };
        reportWizardService.processNewReportDefinition = function (report) {
            _this.insertOperationSubmitted = true;
            return $q.resolve();
        };
        reportWizardService.getVirtualMachinesByCluster = function (clusterNetObjectIds, filter) {
            var result = {
                rows: [
                    {
                        id: 1,
                        name: "Existing Profile 1",
                        cpuCoreCount: vmProfileValidationWrapper_1.DefaultVmProfileRestrictions.cpuCoreCountMax,
                        memoryCapacity: vmProfileValidationWrapper_1.DefaultVmProfileRestrictions.memoryCapacityMax,
                        diskSpaceCapacity: hostProfileValidationWrapper_1.DefaultHostProfileRestrictions.diskSpaceCapacityMax
                    },
                    {
                        id: 2,
                        name: "Existing Profile 2",
                        cpuCoreCount: vmProfileValidationWrapper_1.DefaultVmProfileRestrictions.cpuCoreCountMin,
                        memoryCapacity: vmProfileValidationWrapper_1.DefaultVmProfileRestrictions.memoryCapacityMin,
                        diskSpaceCapacity: hostProfileValidationWrapper_1.DefaultHostProfileRestrictions.diskSpaceCapacityMin
                    },
                ],
                total: 2
            };
            return $q.resolve(result);
        };
        reportWizardService.getClusterCountersInfo = function (clusterId) {
            var result = (_a = {},
                _a[index_1.Counters.cpu] = _this.createDefaultCounterInfo(),
                _a[index_1.Counters.memory] = _this.createDefaultCounterInfo(),
                _a[index_1.Counters.disk] = _this.createDefaultCounterInfo(),
                _a);
            return $q.resolve(result);
            var _a;
        };
    }
    ReportWizardController.$inject = [index_3.NgServiceNames.ngQService, index_3.ServiceNames.reportWizardService];
    return ReportWizardController;
}());


/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var ap = __webpack_require__(40);
var index_1 = __webpack_require__(3);
var helpers_1 = __webpack_require__(18);
/**
 * Default implementation of VM profile restrictions
 *
 * @const
 * @implements IVmProfileRestrictions
 */
exports.DefaultVmProfileRestrictions = {
    cpuCoreCountMin: 1,
    cpuCoreCountMax: 64,
    memoryCapacityMin: helpers_1.ConversionHelper.convertBytes(1, helpers_1.BytesUnit.GB, helpers_1.BytesUnit.B),
    memoryCapacityMax: helpers_1.ConversionHelper.convertBytes(128, helpers_1.BytesUnit.GB, helpers_1.BytesUnit.B),
    diskSpaceCapacityMin: helpers_1.ConversionHelper.convertBytes(1, helpers_1.BytesUnit.GB, helpers_1.BytesUnit.B),
    diskSpaceCapacityMax: helpers_1.ConversionHelper.convertBytes(10, helpers_1.BytesUnit.TB, helpers_1.BytesUnit.B),
    instanceCountMin: 0,
    instanceCountMax: 1000
};
/**
 * Implementation of VM profile interface to ensure proper value validation
 *
 * @class
 * @implements IVmProfileValidationWrapper
 */
var VmProfileValidationWrapper = /** @class */ (function () {
    function VmProfileValidationWrapper(profile, swUtil, _t, xuiToastService, counterUnitFilter, restrictions) {
        if (restrictions === void 0) { restrictions = exports.DefaultVmProfileRestrictions; }
        var _this = this;
        this.profile = profile;
        this.swUtil = swUtil;
        this._t = _t;
        this.xuiToastService = xuiToastService;
        this.counterUnitFilter = counterUnitFilter;
        this.restrictions = restrictions;
        this.onPropertyChanging = function (property, value) {
            if (property === "instanceCount") {
                return _this.ensureValidInteger(value, _this.restrictions.instanceCountMin, _this.swUtil.formatString(_this._t("Simulated instance value cannot be less than {0}."), _this.restrictions.instanceCountMin), _this.restrictions.instanceCountMax, _this.swUtil.formatString(_this._t("Simulated instance value cannot exceed {0}."), _this.restrictions.instanceCountMax));
            }
            return value;
        };
        this.onPropertyChanged = function (property, value) {
        };
        this.id = _.uniqueId();
        this.ensureValidInteger = function (value, min, minMessage, max, maxMessage) {
            var isInteger = _.isInteger(value);
            if (!isInteger || value < min) {
                _this.xuiToastService.warning(minMessage);
                return min;
            }
            else if (isInteger && value > max) {
                _this.xuiToastService.warning(maxMessage);
                return max;
            }
            return value;
        };
    }
    Object.defineProperty(VmProfileValidationWrapper.prototype, "name", {
        get: function () {
            return this.profile.name;
        },
        set: function (value) {
            this.profile.name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "cpuCores", {
        get: function () {
            return this.profile.counters[index_1.Counters.cpuCores].value;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.cpuCoreCountMin, this.swUtil.formatString(this._t("CPU core count value must be {0} or higher."), this.restrictions.cpuCoreCountMin), this.restrictions.cpuCoreCountMax, this.swUtil.formatString(this._t("CPU core count value cannot exceed {0}."), this.restrictions.cpuCoreCountMax));
            this.profile.counters[index_1.Counters.cpuCores].value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "memory", {
        get: function () {
            return this.profile.counters[index_1.Counters.memory].value;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.memoryCapacityMin, this.swUtil.formatString(this._t("Memory capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.memoryCapacityMin, "bytes")), this.restrictions.memoryCapacityMax, this.swUtil.formatString(this._t("Memory capacity value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.memoryCapacityMax, "bytes")));
            this.profile.counters[index_1.Counters.memory].value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "diskSpace", {
        get: function () {
            return this.profile.counters[index_1.Counters.disk].value;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.diskSpaceCapacityMin, this.swUtil.formatString(this._t("Disk space capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMin, "bytes")), this.restrictions.diskSpaceCapacityMax, this.swUtil.formatString(this._t("Disk space capacity value must be {0} or lower."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMax, "bytes")));
            this.profile.counters[index_1.Counters.disk].value = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VmProfileValidationWrapper.prototype, "virtualMachineId", {
        get: function () {
            return this.profile.virtualMachineId;
        },
        set: function (value) {
            this.profile.virtualMachineId = value;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        ap.AutoProperty("profile"),
        __metadata("design:type", Number)
    ], VmProfileValidationWrapper.prototype, "instanceCount", void 0);
    __decorate([
        ap.AutoProperty("profile"),
        __metadata("design:type", Object)
    ], VmProfileValidationWrapper.prototype, "counters", void 0);
    return VmProfileValidationWrapper;
}());
exports.VmProfileValidationWrapper = VmProfileValidationWrapper;


/***/ }),

/***/ 260:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = __webpack_require__(12);
var index_1 = __webpack_require__(3);
var index_2 = __webpack_require__(1);
var resourceLoad_1 = __webpack_require__(32);
exports.default = function (module) {
    module.controller("E2E_CapacityPlanning_ReportDetailController", ReportDetailController);
};
var ReportDetailController = /** @class */ (function () {
    function ReportDetailController() {
        this.reportDetail = {
            reportDefinitionId: 1,
            entityCaption: "caption",
            computationDateFinished: moment.utc({ year: 2010, month: 11, day: 1 }),
            computationDateStarted: moment.utc({ year: 2010, month: 11, day: 1 }),
            runway: 20,
            utilizationPeriodInDays: 30,
            errorMessage: "errorMessage",
            name: "Name",
            reportResultId: 2,
            reportStatus: index_1.ReportStatus.Finished,
            simulatedWorkloadsEnabled: true,
            simulatedResourcesEnabled: true,
            failoverReservation: 4,
            resourceAllocationType: models_1.ResourceAllocationType.Balanced,
            simulationResult: {
                workloads: {
                    virtualMachines: {
                        current: 10,
                        simulatedGrowth: 10,
                        total: 20,
                        simulatedTotal: 18
                    },
                    cpuCores: {
                        current: 20,
                        simulatedGrowth: 30,
                        total: 50,
                        simulatedTotal: 48
                    },
                    cpuClock: {
                        current: 100000000000,
                        simulatedGrowth: 200000000000,
                        total: 150000000000,
                        simulatedTotal: 140000000000
                    },
                    memory: {
                        current: 100000000000,
                        simulatedGrowth: 200000000000,
                        total: 300000000000,
                        simulatedTotal: 200000000000
                    }
                },
                resources: {
                    hosts: {
                        current: 10,
                        simulatedGrowth: 10,
                        total: 20,
                        simulatedTotal: 18
                    },
                    cpuCores: {
                        current: 1,
                        simulatedGrowth: 2,
                        total: 3,
                        simulatedTotal: 2
                    },
                    cpuClock: {
                        current: 100000000000,
                        simulatedGrowth: 200000000000,
                        total: 300000000000,
                        simulatedTotal: 200000000000
                    },
                    memory: {
                        current: 100000000000,
                        simulatedGrowth: 200000000000,
                        total: 300000000000,
                        simulatedTotal: 200000000000
                    }
                },
                counterDepletions: (_a = {},
                    _a[index_1.Counters.cpu] = {
                        actual: {
                            warningThresholdDepletion: moment.duration(2, "day"),
                            criticalThresholdDepletion: moment.duration(3, "day"),
                            capacityHitDepletion: moment.duration(2, "month"),
                        },
                        simulation: {
                            warningThresholdDepletion: moment.duration(-2, "day"),
                            criticalThresholdDepletion: moment.duration(3, "day"),
                            capacityHitDepletion: moment.duration(2, "year"),
                        }
                    },
                    _a[index_1.Counters.memory] = {
                        actual: {
                            warningThresholdDepletion: moment.duration(2, "day"),
                            criticalThresholdDepletion: moment.duration(3, "day"),
                            capacityHitDepletion: moment.duration(2, "month"),
                        },
                        simulation: {
                            warningThresholdDepletion: moment.duration(-2, "day"),
                            criticalThresholdDepletion: moment.duration(3, "day"),
                            capacityHitDepletion: moment.duration(2, "year"),
                        }
                    },
                    _a),
                counterUtilizations: (_b = {},
                    _b[index_1.Counters.cpu] = {
                        utilization: {
                            utilization: {
                                count: 5,
                                isConstant: false,
                                data: [20000000000, 30000000000, 40000000000, 50000000000, 50000000000]
                            },
                            utilizationGranularity: 1,
                            isEmpty: false
                        },
                        utilizationTrend: {
                            a: 2.28778793E10,
                            b: 0
                        },
                        simulatedUtilization: {
                            utilization: {
                                count: 5,
                                isConstant: false,
                                data: []
                            },
                            utilizationGranularity: 1,
                            isEmpty: false
                        },
                        simulatedUtilizationTrend: {
                            a: 6.28778793E10,
                            b: 5E10
                        }
                    },
                    _b[index_1.Counters.memory] = {
                        utilization: {
                            utilization: {
                                count: 0,
                                isConstant: false,
                                data: [20000000000, 20000000000, 1500000000, 25000000000, 30000000000]
                            },
                            utilizationGranularity: 1,
                            isEmpty: false
                        },
                        utilizationTrend: {
                            a: 1.68778793E10,
                            b: 0
                        },
                        simulatedUtilization: {
                            utilization: {
                                count: 0,
                                isConstant: false,
                                data: []
                            },
                            utilizationGranularity: 1,
                            isEmpty: false
                        },
                        simulatedUtilizationTrend: {
                            a: 4.28778793E10,
                            b: 4E10
                        }
                    },
                    _b),
                missingHostsCount: 1,
                typicalHost: {
                    name: "lab-esx-04",
                    cpuCoreCount: 16,
                    cpuCoreFrequency: 32000,
                    memoryCapacity: 100,
                    diskSpaceCapacity: 101
                },
                availableVmsCapacity: [
                    {
                        firstDepletedCounter: index_1.Counters.cpu,
                        availableVmsCount: 2,
                        name: "Small VMs",
                        cpuCoreCount: 1,
                        memoryCapacity: 100,
                        diskSpaceCapacity: 101
                    },
                    {
                        firstDepletedCounter: index_1.Counters.memory,
                        name: "Medium VM",
                        cpuCoreCount: 2,
                        memoryCapacity: 1000,
                        diskSpaceCapacity: 1000,
                        availableVmsCount: 3
                    }
                ]
            },
            counters: (_c = {},
                _c[index_1.Counters.cpu] = {
                    criticalThreshold: 90,
                    exhaustedThreshold: 100,
                    warningThreshold: 80
                },
                _c[index_1.Counters.memory] = {
                    criticalThreshold: 90,
                    exhaustedThreshold: 100,
                    warningThreshold: 80
                },
                _c),
            simulatedVmProfiles: [
                {
                    name: "Small VMs",
                    instanceCount: 2,
                    counters: (_d = {},
                        _d[index_1.Counters.cpuCores] = { value: 1, utilizationInPercent: resourceLoad_1.ResourceLoad.Low },
                        _d[index_1.Counters.memory] = { value: 100, utilizationInPercent: resourceLoad_1.ResourceLoad.HighLoad },
                        _d[index_1.Counters.disk] = { value: 101, utilizationInPercent: resourceLoad_1.ResourceLoad.Typical },
                        _d)
                },
                {
                    name: "Medium VM",
                    instanceCount: 1,
                    counters: (_e = {},
                        _e[index_1.Counters.cpuCores] = { value: 2, utilizationInPercent: resourceLoad_1.ResourceLoad.HighLoad },
                        _e[index_1.Counters.memory] = { value: 1000, utilizationInPercent: resourceLoad_1.ResourceLoad.Low },
                        _e[index_1.Counters.disk] = { value: 1000, utilizationInPercent: resourceLoad_1.ResourceLoad.Typical },
                        _e)
                }
            ],
            simulatedHostProfiles: [
                {
                    name: "Small Hosts",
                    instanceCount: 2,
                    cpuCoreCount: 1,
                    cpuCoreFrequency: 1000,
                    memoryCapacity: 100,
                    diskSpaceCapacity: 101
                },
                {
                    name: "Medium Host",
                    instanceCount: 1,
                    cpuCoreCount: 2,
                    cpuCoreFrequency: 2000,
                    memoryCapacity: 1000,
                    diskSpaceCapacity: 1000
                }
            ]
        };
        var _a, _b, _c, _d, _e;
    }
    ReportDetailController.$inject = [index_2.NgServiceNames.ngQService, index_2.ServiceNames.reportDetailService];
    return ReportDetailController;
}());


/***/ }),

/***/ 261:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    module.controller("E2E_CapacityPlanning_ReportHeaderController", ReportHeaderController);
};
var ReportHeaderController = /** @class */ (function () {
    function ReportHeaderController() {
        this.name = "testTitle";
        this.entityCaption = "testCaption123";
        this.computationDateStarted = moment.utc({ year: 2010, month: 11, day: 1 });
        this.runway = 10;
    }
    ReportHeaderController.$inject = [];
    return ReportHeaderController;
}());
;


/***/ }),

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(17);
var index_2 = __webpack_require__(3);
exports.default = function (module) {
    module.controller("E2E_CapacityPlanning_ReportFooterController", ReportFooterController);
};
var ReportFooterController = /** @class */ (function () {
    function ReportFooterController() {
        this.counterInfo = {
            criticalThreshold: 90,
            exhaustedThreshold: 100,
            warningThreshold: 80
        };
        this.failoverReservation = 0;
        this.resourceAllocationType = index_1.ResourceAllocationType.Balanced;
        this.utilizationPeriodInDays = 0;
        this.counters = undefined;
        this.cpuCounters = (_a = {}, _a[index_2.Counters.cpu] = this.counterInfo, _a);
        this.memoryCounters = (_b = {}, _b[index_2.Counters.memory] = this.counterInfo, _b);
        this.diskSpaceCounters = (_c = {}, _c[index_2.Counters.disk] = this.counterInfo, _c);
        this.cpuDiskCounters = (_d = {}, _d[index_2.Counters.cpu] = this.counterInfo, _d[index_2.Counters.disk] = this.counterInfo, _d);
        var _a, _b, _c, _d;
    }
    return ReportFooterController;
}());
;


/***/ }),

/***/ 27:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Route names used by capacity planning
 *
 * @const
 */
exports.RouteNames = {
    summaryStateName: "vimCapacityPlanningSummary",
    notLicensedErrorStateName: "vimCapacityPlanningNotLicensedError",
    reportWizardStateName: "vimCapacityPlanningReportWizard",
    reportDetailStateName: "vimCapacityPlanningReportDetail"
};


/***/ }),

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Urls for routes used by VIM
 *
 * @const
 */
exports.RouteUrls = {
    summaryUrl: "/vim/capacity-planning",
    notLicensedUrl: "/vim/capacity-planning/not-licensed",
    reportWizardUrl: "/vim/capacity-planning/report-wizard/{id:[0-9]*}",
    reportDetailUrl: "/vim/capacity-planning/report-detail/{ids:[0-9]+(?:,[0-9]+)*}"
};


/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Report status
 *
 * @enum
 */
var ReportStatus;
(function (ReportStatus) {
    ReportStatus[ReportStatus["Created"] = 0] = "Created";
    ReportStatus[ReportStatus["Running"] = 1] = "Running";
    ReportStatus[ReportStatus["Finished"] = 2] = "Finished";
    ReportStatus[ReportStatus["Failed"] = 3] = "Failed";
    ReportStatus[ReportStatus["Aborted"] = 4] = "Aborted";
})(ReportStatus = exports.ReportStatus || (exports.ReportStatus = {}));
;
/**
 * Type of entity that is report created for
 *
 * @enum
 */
var ReportEntityType;
(function (ReportEntityType) {
    ReportEntityType[ReportEntityType["Cluster"] = 0] = "Cluster";
})(ReportEntityType = exports.ReportEntityType || (exports.ReportEntityType = {}));
;


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(21));
__export(__webpack_require__(27));
__export(__webpack_require__(28));
__export(__webpack_require__(29));
__export(__webpack_require__(30));
__export(__webpack_require__(31));
__export(__webpack_require__(32));
__export(__webpack_require__(22));
// re-export VIM common
__export(__webpack_require__(7));
__export(__webpack_require__(8));
__export(__webpack_require__(4));


/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Counter constants
 */
var Counters = /** @class */ (function () {
    function Counters() {
    }
    /**
     * Counter for virtual machines
     */
    Counters.virtualMachines = "virtualMachines";
    /**
     * Counter for hosts
     */
    Counters.hosts = "hosts";
    /**
     * Counter for CPU Cores
     */
    Counters.cpu = "cpu";
    /**
     * Counter for CPU Cores
     */
    Counters.cpuCores = "cpuCores";
    /**
     * Counter for CPU Clock
     */
    Counters.cpuClock = "cpuClock";
    /**
     * Counter for Memory
     */
    Counters.memory = "memory";
    /**
     * Counter for Disk
     */
    Counters.disk = "disk";
    return Counters;
}());
exports.Counters = Counters;


/***/ }),

/***/ 31:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baseUrl = "vim/capacity-planning";
var isLicensedMethod = "is-licensed";
var getHelpUrlMethod = "get-help-url";
var deleteReportsMethod = "delete-reports";
/**
 * Backend urls used by capacity planning module
 *
 * @const
 */
exports.SwApiUrls = {
    baseUrl: baseUrl,
    isLicensedMethod: isLicensedMethod,
    isLicensedUrl: baseUrl + "/" + isLicensedMethod,
    getHelpUrl: baseUrl + "/" + getHelpUrlMethod,
    deleteReports: deleteReportsMethod
};


/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Determines typical resource load for counter
 *
 * @enum
 */
var ResourceLoad;
(function (ResourceLoad) {
    ResourceLoad[ResourceLoad["Low"] = 10] = "Low";
    ResourceLoad[ResourceLoad["Typical"] = 50] = "Typical";
    ResourceLoad[ResourceLoad["HighLoad"] = 85] = "HighLoad";
})(ResourceLoad = exports.ResourceLoad || (exports.ResourceLoad = {}));
;


/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Helper for working with json data format
 */
var JsonHelper = /** @class */ (function () {
    function JsonHelper() {
    }
    /**
     * Parses json string and creates object. If there are capitals, they are converted to lower case in order to keep typescript conventions
     */
    JsonHelper.fromJsonWithLowerCase = function (jsonString) {
        return JSON.parse(jsonString, function (key, value) {
            if (value && typeof value === "object") {
                for (var k in value) {
                    if (/^[A-Z]/.test(k) && Object.hasOwnProperty.call(value, k)) {
                        value[k.charAt(0).toLowerCase() + k.substring(1)] = value[k];
                        delete value[k];
                    }
                }
            }
            return value;
        });
    };
    return JsonHelper;
}());
exports.JsonHelper = JsonHelper;


/***/ }),

/***/ 34:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var conversionHelper_1 = __webpack_require__(19);
;
/**
 * Default implementation of host profile restrictions
 *
 * @const
 * @implements IHostProfileRestrictions
 */
exports.DefaultHostProfileRestrictions = {
    cpuCoreCountMin: 1,
    cpuCoreCountMax: 64,
    /** 1 GHz */
    cpuCoreFrequencyMin: conversionHelper_1.ConversionHelper.convertHertz(1, conversionHelper_1.HertzUnit.GHz, conversionHelper_1.HertzUnit.Hz),
    /** 10 GHz */
    cpuCoreFrequencyMax: conversionHelper_1.ConversionHelper.convertHertz(10, conversionHelper_1.HertzUnit.GHz, conversionHelper_1.HertzUnit.Hz),
    /** 1 GB */
    memoryCapacityMin: conversionHelper_1.ConversionHelper.convertBytes(1, conversionHelper_1.BytesUnit.GB, conversionHelper_1.BytesUnit.B),
    /** 1 TB  */
    memoryCapacityMax: conversionHelper_1.ConversionHelper.convertBytes(1, conversionHelper_1.BytesUnit.TB, conversionHelper_1.BytesUnit.B),
    /** 1 TB */
    diskSpaceCapacityMin: conversionHelper_1.ConversionHelper.convertBytes(1, conversionHelper_1.BytesUnit.TB, conversionHelper_1.BytesUnit.B),
    /** 10 TB */
    diskSpaceCapacityMax: conversionHelper_1.ConversionHelper.convertBytes(10, conversionHelper_1.BytesUnit.TB, conversionHelper_1.BytesUnit.B),
    instanceCountMin: 0,
    instanceCountMax: 1000
};
;
/**
 * Implementation of host profile interface to ensure proper value validation
 *
 * @class
 * @implements IHostProfileValidationWrapper
 */
var HostProfileValidationWrapper = /** @class */ (function () {
    function HostProfileValidationWrapper(profile, swUtil, _t, xuiToastService, counterUnitFilter, restrictions) {
        if (restrictions === void 0) { restrictions = exports.DefaultHostProfileRestrictions; }
        var _this = this;
        this.profile = profile;
        this.swUtil = swUtil;
        this._t = _t;
        this.xuiToastService = xuiToastService;
        this.counterUnitFilter = counterUnitFilter;
        this.restrictions = restrictions;
        this.ensureValidInteger = function (value, min, minMessage, max, maxMessage) {
            if (_.isNumber(value) && _.round(value) !== value) {
                _this.xuiToastService.warning(_this._t("Decimal part is not allowed!"));
                return _.round(value);
            }
            return _this.checkMinMaxValues(value, min, minMessage, max, maxMessage);
        };
        this.ensureValidNumber = function (value, min, minMessage, max, maxMessage) {
            return _this.checkMinMaxValues(value, min, minMessage, max, maxMessage);
        };
        this.checkMinMaxValues = function (value, min, minMessage, max, maxMessage) {
            if (!_.isFinite(value)) {
                _this.xuiToastService.warning(_this._t("Number is not valid!"));
                return min;
            }
            else if (value < min) {
                _this.xuiToastService.warning(minMessage);
                return min;
            }
            else if (value > max) {
                _this.xuiToastService.warning(maxMessage);
                return max;
            }
            return value;
        };
    }
    Object.defineProperty(HostProfileValidationWrapper.prototype, "name", {
        get: function () {
            return this.profile.name;
        },
        set: function (value) {
            this.profile.name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "hostId", {
        get: function () {
            return this.profile.hostId;
        },
        set: function (value) {
            this.profile.hostId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "cpuCoreCount", {
        get: function () {
            return this.profile.cpuCoreCount;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.cpuCoreCountMin, this.swUtil.formatString(this._t("CPU core count value must be {0} or higher."), this.restrictions.cpuCoreCountMin), this.restrictions.cpuCoreCountMax, this.swUtil.formatString(this._t("CPU core count value cannot exceed {0}."), this.restrictions.cpuCoreCountMax));
            this.profile.cpuCoreCount = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "cpuCoreFrequency", {
        get: function () {
            return this.profile.cpuCoreFrequency;
        },
        set: function (value) {
            value = this.ensureValidNumber(value, this.restrictions.cpuCoreFrequencyMin, this.swUtil.formatString(this._t("CPU core frequency value must be {0} or higher."), this.counterUnitFilter(this.restrictions.cpuCoreFrequencyMax, "clock")), this.restrictions.cpuCoreFrequencyMax, this.swUtil.formatString(this._t("CPU core frequency value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.cpuCoreFrequencyMax, "clock")));
            this.profile.cpuCoreFrequency = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "memoryCapacity", {
        get: function () {
            return this.profile.memoryCapacity;
        },
        set: function (value) {
            value = this.ensureValidNumber(value, this.restrictions.memoryCapacityMin, this.swUtil.formatString(this._t("Memory capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.memoryCapacityMin, "bytes")), this.restrictions.memoryCapacityMax, this.swUtil.formatString(this._t("Memory capacity value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.memoryCapacityMax, "bytes")));
            this.profile.memoryCapacity = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "diskSpaceCapacity", {
        get: function () {
            return this.profile.diskSpaceCapacity;
        },
        set: function (value) {
            value = this.ensureValidNumber(value, this.restrictions.diskSpaceCapacityMin, this.swUtil.formatString(this._t("Disk space capacity value must be {0} or higher."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMin, "bytes")), this.restrictions.diskSpaceCapacityMax, this.swUtil.formatString(this._t("Disk space capacity value cannot exceed {0}."), this.counterUnitFilter(this.restrictions.diskSpaceCapacityMax, "bytes")));
            this.profile.diskSpaceCapacity = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostProfileValidationWrapper.prototype, "instanceCount", {
        get: function () {
            return this.profile.instanceCount;
        },
        set: function (value) {
            value = this.ensureValidInteger(value, this.restrictions.instanceCountMin, this.swUtil.formatString(this._t("Simulated instance value cannot be less than {0}."), this.restrictions.instanceCountMin), this.restrictions.instanceCountMax, this.swUtil.formatString(this._t("Simulated instance value cannot exceed {0}."), this.restrictions.instanceCountMax));
            this.profile.instanceCount = value;
        },
        enumerable: true,
        configurable: true
    });
    return HostProfileValidationWrapper;
}());
exports.HostProfileValidationWrapper = HostProfileValidationWrapper;
;


/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(36));


/***/ }),

/***/ 36:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
* Ids of filtering options
*/
var ReportsFilterPropertyNames = /** @class */ (function () {
    function ReportsFilterPropertyNames() {
    }
    ReportsFilterPropertyNames.entityPropertyName = "entity";
    ReportsFilterPropertyNames.parameterPropertyName = "parameter";
    return ReportsFilterPropertyNames;
}());
exports.ReportsFilterPropertyNames = ReportsFilterPropertyNames;
/**
* Ids used in report list filtering by parameters
*/
var ReportsFilterParameterNames = /** @class */ (function () {
    function ReportsFilterParameterNames() {
    }
    ReportsFilterParameterNames.simulatedResourcesEnabled = "simulatedResourcesEnabled";
    ReportsFilterParameterNames.simulatedWorkloadsEnabled = "simulatedWorkloadsEnabled";
    return ReportsFilterParameterNames;
}());
exports.ReportsFilterParameterNames = ReportsFilterParameterNames;
;


/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Default implementation for report definition interface
 *
 * @class
 * @implements IReportDefinition
 */
var ReportDefinition = /** @class */ (function () {
    function ReportDefinition() {
        this.computedEntities = [];
        this.simulatedVmProfiles = [];
        this.simulatedHostProfiles = [];
    }
    return ReportDefinition;
}());
exports.ReportDefinition = ReportDefinition;
;


/***/ }),

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Determines how much the utilization data is filtered
 *
 * @enum
 */
var ResourceAllocationType;
(function (ResourceAllocationType) {
    /**
     * No filtering used on utilization
     */
    ResourceAllocationType[ResourceAllocationType["Conservative"] = 0] = "Conservative";
    /**
     * 95th percentile is used to filter peaks
     */
    ResourceAllocationType[ResourceAllocationType["Balanced"] = 1] = "Balanced";
    /**
     * 75th percentile is used to filter peaks
     */
    ResourceAllocationType[ResourceAllocationType["HighlyOptimized"] = 2] = "HighlyOptimized";
})(ResourceAllocationType = exports.ResourceAllocationType || (exports.ResourceAllocationType = {}));


/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Default implementation for report detail interface
 *
 * @class
 * @implements IReportDetail
 */
var ReportDetail = /** @class */ (function () {
    function ReportDetail() {
    }
    return ReportDetail;
}());
exports.ReportDetail = ReportDetail;
;


/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Possible orion user roles
 */
exports.Roles = {
    admin: "admin",
    demo: "demo"
};


/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Generates get/set methods for a property
 * @param {string} [storageObject] Name of a backend property to store the real value
 */
function AutoProperty(storageObject) {
    return function (target, name) {
        var variableName = storageObject ? name : "_" + name;
        Object.defineProperty(target, name, {
            get: function () {
                if (storageObject) {
                    return this[storageObject][variableName];
                }
                return this[variableName];
            },
            set: function (value) {
                // on changing
                if (_.isFunction(this["onPropertyChanging"])) {
                    var handler = this["onPropertyChanging"];
                    value = handler(name, value);
                }
                if (storageObject) {
                    this[storageObject][variableName] = value;
                }
                else {
                    this[variableName] = value;
                }
                // on changed
                if (_.isFunction(this["onPropertyChanged"])) {
                    var handler = this["onPropertyChanged"];
                    handler(name, value);
                }
            },
            enumerable: true,
            configurable: true
        });
    };
}
exports.AutoProperty = AutoProperty;
;


/***/ }),

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
* Common Helper for entities
*/
var EntityHelper = /** @class */ (function () {
    function EntityHelper() {
    }
    /**
    * Extract numeric instance id from NetObjectId
    * @example "VC:1" => 1
    */
    EntityHelper.parseInstanceId = function (entityNetObjectId) {
        if (_.isUndefined(entityNetObjectId)) {
            throw new Error("EntityNetObjectId needs to be specified");
        }
        var fragments = _.split(entityNetObjectId, ":");
        if (fragments.length !== 2) {
            throw new Error("Invalid entityNetObjectId");
        }
        return _.parseInt(fragments[1]);
    };
    return EntityHelper;
}());
exports.EntityHelper = EntityHelper;


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * VIM module names
 *
 * @const
 */
exports.ModuleNames = {
    main: "vim",
    providers: "vim.providers",
    services: "vim.services",
    filters: "vim.filters"
};


/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var roles_1 = __webpack_require__(4);
/**
 * Auth profiles used by Recommendations engine
 *
 * @const
 * @var
 */
exports.AuthProfiles = {
    guestProfile: {
        roles: [],
        permissions: []
    },
    adminProfile: {
        roles: [roles_1.Roles.admin],
        permissions: []
    },
    adminProfileDemoAllowed: {
        roles: [roles_1.Roles.admin, roles_1.Roles.demo],
        permissions: []
    }
};


/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constants for net object prefixes managed by VIM
 *
 * @const
 */
exports.NetObjectPrefixes = {
    cluster: "VMC",
    host: "VH"
};


/***/ })

/******/ });
//# sourceMappingURL=vim-e2e.js.map