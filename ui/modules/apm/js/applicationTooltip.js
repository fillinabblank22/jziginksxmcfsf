/*!
 * @solarwinds/apm 2020.2.5-302
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 24);
/******/ })
/************************************************************************/
/******/ ({

/***/ 24:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(25);
module.exports = __webpack_require__(26);


/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SamEntityManagementService = /** @class */ (function () {
    /** @ngInject */
    SamEntityManagementService.$inject = ["xuiToastService", "swApi", "getTextService", "swDemoService"];
    function SamEntityManagementService(xuiToastService, swApi, getTextService, swDemoService) {
        this.xuiToastService = xuiToastService;
        this.swApi = swApi;
        this.getTextService = getTextService;
        this.swDemoService = swDemoService;
        this.url = "/Orion/APM/Services/Applications.asmx/ManageApplications";
        /*prevent tslint error*/
    }
    SamEntityManagementService.prototype.Unmanage = function (parameters) {
        var _this = this;
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return;
        }
        if (!parameters) {
            return;
        }
        var payload = {
            appIds: [parameters.entityId],
            managed: false,
            unmanageFrom: parameters.unmanageFrom || null,
            unmanageUntil: parameters.unmanageUntil || null
        };
        return this.swApi.ws.allUrl("applications", this.url).post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextService("Selected entity is unmanaged. Refresh the page to view the changes."));
        }, function () {
            _this.xuiToastService.error(_this.getTextService("There was an error while unmanaging selected entity. Please contact your system admin."));
        });
    };
    SamEntityManagementService.prototype.Remanage = function (parameters) {
        var _this = this;
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return;
        }
        if (!parameters) {
            return;
        }
        var payload = {
            appIds: [parameters.entityId],
            managed: true,
            unmanageFrom: parameters.unmanageFrom || null,
            unmanageUntil: parameters.unmanageUntil || null
        };
        return this.swApi.ws.allUrl("applications", this.url).post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextService("Selected entity is managed. Refresh the page to view the changes."));
        }, function () {
            _this.xuiToastService.error(_this.getTextService("There was an error while remanaging selected entity. Please contact your system admin."));
        });
    };
    return SamEntityManagementService;
}());
exports.SamEntityManagementService = SamEntityManagementService;
angular.module("orion.components").service("samEntityManagementService", SamEntityManagementService);


/***/ }),

/***/ 26:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

/******/ });
//# sourceMappingURL=applicationTooltip.js.map