/*!
 * @solarwinds/apm 2020.2.5-302
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var OrionStatus;
(function (OrionStatus) {
    OrionStatus[OrionStatus["Unknown"] = 0] = "Unknown";
    OrionStatus[OrionStatus["Up"] = 1] = "Up";
    OrionStatus[OrionStatus["Critical"] = 14] = "Critical";
})(OrionStatus = exports.OrionStatus || (exports.OrionStatus = {}));


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<div class=aad-dc-roles ng-controller=domainControllerRolesController> <div class=aad-dc-roles__pill ng-repeat=\"role in aadDC.rolesList\"> <span title={{::aadDC.getTooltip(role)}} class=\"aad-dc-roles-tooltip xui-margin-sml\"> <svg class=aad-dc-roles__pill__ellipsis> <g> <rect x=0 y=1 rx=10 ry=50 width=50 height=18 /> <text text-anchor=middle x=26 y=14 class=xui-text-r>{{::role}}</text> </g> </svg> </span> </div> </div> ";

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = "<span class=xui-text-bg ng-if=\"vm.replicationData.status !== 0\" xui-popover xui-popover-content=vm.getAvailabilityPopoverTemplate()>{{vm.getAvailabilityDescription()}}</span> <span class=xui-text-bg ng-if=\"vm.replicationData.status === 0\" _t>Unknown</span> <script type=text/ng-template id=availability-popover> <div _t=\"['{{vm.replicationData.lastSuccessTime | apmDate}}']\">Last success: {0}</div>\n    <div ng-if=\"vm.replicationData.lastFailureTime\" _t=\"['{{vm.replicationData.lastFailureTime | apmDate}}']\">Last failure: {0}</div>\n    <div ng-if=\"vm.replicationData.consecutiveFailures > 1\" _t=\"['{{vm.replicationData.consecutiveFailures}}']\">{0} consecutive failures</div> </script> ";

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<replication-details-row replication-data=::item> </replication-details-row> ";

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = "<div> <xui-icon icon-size=small icon=\"{{'group' | swEntityIcon}}\" is-dynamic=true status=\"{{vm.replicationData.status | orionStatusIcon}}\"></xui-icon> <span class=xui-text-bg>{{vm.replicationData.name}}</span> <replication-details-availability style=float:right replication-data=vm.replicationData /> </div> <div class=xui-margin-smv> <span class=xui-text-s style=margin-left:24px _t>Replication</span> </div> ";

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = "<xui-search on-search=\"aadSite.onSearch(value, cancellation)\" on-clear=aadSite.cancelSearch() placeholder=_t(Search...) _ta> </xui-search> <xui-empty ng-if=\"aadSite.sites.length === 0\"></xui-empty> <div class=aad-site-tree-view> <xui-treeview class=xui-margin-mdv model=aadSite.sites options=aadSite.settings state={{aadSite.treeViewState}} node-identifier=id> <div class=aad-site-node> <div> <div ng-if=\"node.$model.type === 'domainControllers'\" class=\"aad-site-domain-controller xui-margin-smv\"> <xui-icon icon=unknownnode status={{node.$model.statusDescription}}></xui-icon> <div ng-if=node.$model.nodeDetailsUrl class=\"xui-margin-mdl aad-site-domain-controller-monitored\"> <a ng-href={{node.$model.nodeDetailsUrl}}>{{node.$model.name}}</a> </div> <div ng-if=!node.$model.nodeDetailsUrl class=xui-margin-mdl> <span>{{node.$model.name}}</span> <div> <a href=\"http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionSAMAGAppInsightforADDomainController\" target=_blank _t> » Learn how to monitor this node </a> </div> </div> </div> <span ng-if=\"node.$model.type !== 'domainControllers'\"> {{node.$model.label}} </span> <span ng-if=\"(node.$model.type === 'links-title' || node.$model.type === 'subnets-title' || node.$model.type === 'domainControllers-title')\"> ({{node.$model.children.length}}) </span> </div> <div ng-if=\"node.$model.type == 'site'\"> <span class=xui-margin-lgr _t=\"['{{node.$model.data.linksCount}}']\"> {0} Links </span> <span class=xui-margin-lgr _t=\"['{{node.$model.data.subnetsCount}}']\"> {0} Subnets </span> <span class=xui-margin-lgr _t=\"['{{node.$model.data.domainControllersCount}}']\"> {0} Replication Servers </span> </div> </div> </xui-treeview> </div> ";

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "<div class=\"id-trust-transitivity xui-text-s\"> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isNonTransitive></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isNonTransitive></xui-icon> <span _t>Non Transitive</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isUpLevelOnly></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isUpLevelOnly></xui-icon> <span _t>Up Level Only</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isQuarantinedDomain></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isQuarantinedDomain></xui-icon> <span _t>Quarantined Domain</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isForestTransitive></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isForestTransitive></xui-icon> <span _t>Forest Transitive</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isCrossOrganization></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isCrossOrganization></xui-icon> <span _t>Cross Organization</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isWithinForest></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isWithinForest></xui-icon> <span _t>Within Forest</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isTreatAsExternal></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isTreatAsExternal></xui-icon> <span _t>Treat As External</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isUsingRC4Encryption></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isUsingRC4Encryption></xui-icon> <span _t>Using RC4 Encryption</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isCrossOrganizationNoTgtDelegation></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isCrossOrganizationNoTgtDelegation></xui-icon> <span _t>Cross Organization No TGT Delegation</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isPIMTrust></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isPIMTrust></xui-icon> <span _t>PIM Trust</span> </p> </div> ";

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionStatusIconFilter_1 = __webpack_require__(8);
exports.default = function (module) {
    module.filter("orionStatusIcon", orionStatusIconFilter_1.orionStatusIconFilter);
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionStatus_1 = __webpack_require__(0);
/* @ngInject */
function orionStatusIconFilter() {
    return function (status) {
        if (status === orionStatus_1.OrionStatus.Unknown) {
            return "unknown";
        }
        else if (status === orionStatus_1.OrionStatus.Up) {
            return "up";
        }
        else if (status === orionStatus_1.OrionStatus.Critical) {
            return "critical";
        }
        else {
            return "unknown";
        }
    };
}
exports.orionStatusIconFilter = orionStatusIconFilter;


/***/ }),
/* 9 */,
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(11);
module.exports = __webpack_require__(12);


/***/ }),
/* 11 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(13);
var config_1 = __webpack_require__(14);
var index_1 = __webpack_require__(15);
var templates_1 = __webpack_require__(17);
var index_2 = __webpack_require__(7);
index_1.default(app_1.default);
config_1.default(app_1.default);
templates_1.default(app_1.default);
index_2.default(app_1.default);


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("apm.services", []);
angular.module("apm.templates", []);
angular.module("apm.components", []);
angular.module("apm.filters", []);
angular.module("apm.providers", []);
angular.module("apm", [
    "orion",
    "filtered-list",
    "apm.services",
    "apm.templates",
    "apm.components",
    "apm.filters",
    "apm.providers"
]);
// create and register Xui (Orion) module wrapper
var apm = Xui.registerModule("apm");
exports.default = apm;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: apm");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(16);
exports.default = function (module) {
    module.service("apmConstants", constants_1.default);
};


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(18);
    req.keys().forEach(function (r) {
        var key = "apm" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./appInsightForActiveDirectory/components/domainControllerRoles/domain-controller-roles.html": 1,
	"./appInsightForActiveDirectory/components/replicationDetailsAvailability/replication-details-availability.html": 2,
	"./appInsightForActiveDirectory/components/replicationDetailsExpander/swReplicationDetailsTemplate.html": 3,
	"./appInsightForActiveDirectory/components/replicationDetailsRow/replication-details-row.html": 4,
	"./appInsightForActiveDirectory/components/siteTreeView/site-tree-view.html": 5,
	"./appInsightForActiveDirectory/components/trustSummary/trust-summaryPopover.html": 6,
	"./components/tooltips/applicationTooltip-directive.html": 19,
	"./rowTemplates/domainControllerRowTemplate.html": 20,
	"./rowTemplates/orionApiPollerMonitoredValuesRowTemplate.html": 21,
	"./rowTemplates/replicationDetailsRowTemplate.html": 22,
	"./rowTemplates/trustSummaryRowTemplate.html": 23
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 18;

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-application-tooltip> <div ng-if=::popoverData.extensionData.container.members.length> <div class=sw-application-tooltip__title _t>Components with problems:</div> <sw-short-entity-list items=::popoverData.extensionData.container.members more-count=::popoverData.extensionData.container.moreMembers more-url=::popoverData.extensionData.container.detailUrl> </sw-short-entity-list> </div> <div ng-if=::!popoverData.extensionData.container.members.length> <div class=sw-application-tooltip__message _t>All components are up.</div> </div> </div> ";

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<div class=aad-dc-item> <div class=\"aad-dc-item__left-side aad-dc-center-content\"> <div class=aad-dc-domaincontroller> <div class=\"aad-dc-domaincontroller__icon pull-left\"> <xui-icon icon=\"{{'orion.nodes' | swEntityIcon}}\" is-dynamic=true status=\"{{::item.status | swStatus}}\"></xui-icon> </div> <div class=text_wrap> <a ng-if=item.nodeDetailsUrl ng-href={{::item.nodeDetailsUrl}} class=\"xui-text-l xui-text-a\">{{::item.name}}</a> <span ng-if=!item.nodeDetailsUrl class=xui-text-r>{{::item.name}}</span> </div> </div> <div class=\"xui-margin-smv xui-margin-mdl\"> <div class=\"xui-text-s text_wrap\" ng-if=\"item.nodeID !== null && item.siteName != null\"> <span _t>at</span>&nbsp;{{::item.siteName}} </div> <div class=\"xui-text-s text_wrap\" ng-if=\"item.nodeID !== null && item.siteName === null\"> <span _t>N/A</span> </div> <div class=text_wrap ng-if=\"item.nodeID === null\"> <a href=\"http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionSAMAGAppInsightforADDomainController\" target=_blank class=xui-text-a _t> » Learn how to monitor this node </a> </div> </div> </div> <div> <domain-controller-roles roles=::item.roles></domain-controller-roles> <div class=\"text_wrap xui-text-r\" ng-if=\"item.roles === 0\" _t> No Roles </div> </div> </div> ";

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = "<div class=\"id-oap-row oap-row xui-margin-mdv\"> <div class=\"id-oap-poller oap-poller oap-poller-name-content\"> <div class=\"id-oap-value-to-monitor oap-value-to-monitor\"> <div class=\"id-oap-value-to-monitor-icon oap-value-to-monitor-icon pull-left\"> <xui-icon ng-if=item.valueToMonitorStatus icon=\"status_{{::item.valueToMonitorStatus | swStatus}}\" is-dynamic=true></xui-icon> <xui-icon ng-if=!item.valueToMonitorStatus icon=status_unknown is-dynamic=true></xui-icon> </div> <div class=\"pull-left text_wrap oap-value-to-monitor-name\"> <a class=id-oap-value-to-monitor-link ng-href={{::item.valueToMonitorUrl}} xui-ellipsis ellipsis-options=\"{tooltipText: item.valueToMonitorName}\">{{::item.valueToMonitorName}}</a> </div> </div> <div class=\"oap-value-to-monitor-spreader pull-left\"></div> <div class=oap-poller-name xui-ellipsis ellipsis-options=\"{tooltipText: item.orionApiPollerName}\"> <span _t>on</span> <a class=id-oap-poller-link ng-href={{::item.apiPollerUrl}}>{{::item.orionApiPollerName}}</a> </div> </div> <div class=\"id-oap-value-to-monitor-metric oap-poller-value-content\"> <span xui-ellipsis ng-if=\"item.valueToMonitorMetric !== null && item.valueToMonitorMetric !== undefined\" ellipsis-options=\"{tooltipText: item.valueToMonitorMetric.toString()}\">{{::item.valueToMonitorMetric}}</span> </div> </div> ";

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"replicationDetailsExpanderController as vm\" class=aad-replication-row> <xui-expander on-status-changed=\"vm.onExpanderStatusChanged({\n                isOpen,\n                sourceDomainControllerFqdn: item.sourceDomainControllerFqdn,\n                destinationDomainControllerFqdn: item.destinationDomainControllerFqdn\n            })\"> <xui-expander-heading> <div class=\"id-replication-details-row aad-replication-details-row\"> <div class=\"id-replication-destination aad-replication-node aad-replication-center-content\"> <div class=\"id-domaincontroller aad-replication-domaincontroller\"> <div class=\"id-domaincontroller-icon aad-replication-domaincontroller-icon pull-left\"> <xui-icon icon=\"{{'orion.nodes' | swEntityIcon}}\" is-dynamic=true status=\"{{::item.destinationNodeStatus | swStatus}}\"></xui-icon> </div> <div class=\"id-domaincontroller-name aad-replication-domaincontroller-name pull-left text_wrap xui-text-l\"> <a class=id-node-name-link ng-href={{::item.destinationNodeUrl}} ng-if=\"item.destinationNodeName !== null\">{{::item.destinationNodeName}}</a> <span class=id-node-name ng-if=\"item.destinationNodeName === null\" xui-ellipsis append-to-body=true ellipsis-options=\"{tooltipText: item.destinationDomainControllerFqdn}\"> {{::item.destinationDomainControllerFqdn}} </span> </div> </div> <div class=\"aad-replication-spreader pull-left\"></div> <div class=\"id-destination-domain aad-replication-domain text_wrap\" ng-if=\"item.destinationDomainName !== null\"> <span _t>at</span> <xui-icon icon-size=small icon=\"{{'orion.nodes' | swEntityIcon}}\" is-dynamic=true status=\"{{::item.destinationApplicationStatus | swStatus}}\"> </xui-icon> <a class=id-destination-domain-name-link ng-href={{::item.destinationApplicationUrl}}>{{::item.destinationDomainName}}</a> </div> <div class=\"id-start-monitoring aad-replication-domain\"> <div class=text_wrap ng-if=\"item.destinationNodeName === null\"> <a href=\"http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionSAMAGAppInsightforADDomainController\" target=_blank class=xui-text-a xui-ellipsis ellipsis-options=vm.ellipsisOptions append-to-body=true _t> » Learn how to monitor this node </a> </div> </div> </div> <div class=\"id-replication-pill aad-replication-pill aad-replication-center-content\" ng-class=::vm.getPillStatus(item.status)> <div class=arrow> <div class=\"id-arrowhead aad-replication-arrowhead-left\"></div> <div class=line></div> </div> <svg class=replication-pill-svg> <g> <rect x=0 y=1 rx=10 ry=50 width=50 height=18 class=rectangle /> <text text-anchor=middle x=26 y=14 class=id-transport-type ng-if=\"item.transportType == 1\"> IP </text> <text text-anchor=middle x=26 y=14 class=id-transport-type ng-if=\"item.transportType == 2\"> SMTP </text> <text text-anchor=middle x=26 y=14 class=id-transport-type ng-if=\"item.transportType == 3\"> RPC </text> </g> </svg> </div> <div class=\"id-replication-source aad-replication-node aad-replication-center-content\"> <div class=\"id-domaincontroller aad-replication-domaincontroller\"> <div class=\"id-domaincontroller-icon aad-replication-domaincontroller-icon pull-left\"> <xui-icon icon=\"{{'orion.nodes' | swEntityIcon}}\" is-dynamic=true status=\"{{::item.sourceNodeStatus | swStatus}}\"></xui-icon> </div> <div class=text_wrap ng-class=\"{'xui-text-l' : (item.sourceNodeName !== null)}\"> <a class=id-node-name-link ng-href={{::item.sourceNodeUrl}} ng-if=\"item.sourceNodeName !== null\">{{::item.sourceNodeName}}</a> <span class=\"xui-text-s text_wrap\" ng-if=\"item.sourceNodeName === null\" xui-ellipsis append-to-body=true ellipsis-options=\"{tooltipText: item.sourceDomainControllerFqdn}\"> {{::item.sourceDomainControllerFqdn}} </span> </div> </div> <div class=\"aad-replication-spreader pull-left\"></div> <div class=\"id-source-domain aad-replication-domain text_wrap\" ng-if=\"item.sourceDomainName !== null\"> <span _t>at</span> <xui-icon icon-size=small class=id-source-application-status-icon icon=\"{{'orion.nodes' | swEntityIcon}}\" is-dynamic=true status=\"{{::item.sourceApplicationStatus | swStatus}}\"> </xui-icon> <a class=id-source-domain-name-link ng-href={{::item.sourceApplicationUrl}}> {{::item.sourceDomainName}} </a> </div> <div class=\"id-start-monitoring aad-replication-domain\"> <div class=text_wrap ng-if=\"item.sourceNodeName === null\"> <a href=\"http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionSAMAGAppInsightforADDomainController\" xui-ellipsis ellipsis-options=vm.ellipsisOptions append-to-body=true target=_blank class=xui-text-a _t> » Learn how to monitor this node </a> </div> </div> </div> </div> </xui-expander-heading> <xui-listview stripe=false row-padding=compact items-source=vm.namingContextReplications template-url=replication-details-template controller=vm> </xui-listview> </xui-expander> </div> ";

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"trustSummaryController as vm\" class=\"aad-trust-summary-row xui-margin-mdv\"> <div class=\"id-trusting-domain aad-trust-domain\"> <div class=\"id-trusting-domain-icon aad-trust-domain-icon pull-left\"> <xui-icon icon=network></xui-icon> </div> <div class=\"id-trusting-domain-name pull-left text_wrap\" xui-ellipsis append-to-body=true ellipsis-options=\"{tooltipText: item.trustingDomainName}\"> <span class=xui-text-bg> {{::item.trustingDomainName}} </span> </div> </div> <div class=\"id-trust-info aad-trust-info\"> <div class=\"id-trust-pill aad-trust-pill\" xui-popover xui-popover-content=vm.popOverTemplate() xui-popover-title=\"_t(Trust Attributes)\" _ta> <div class=arrow> <div class=\"id-arrowhead aad-trust-arrowhead-left\" ng-if=\"item.direction === 'InBound' || item.direction === 'Bidirectional'\"></div> <div class=line></div> </div> <svg class=trust-pill-svg> <g> <rect x=0 y=1 rx=10 ry=50 width=150 height=18 class=rectangle /> <text text-anchor=middle x=75 y=14 class=id-trust-type ng-if=\"item.type === 'DownLevel'\" _t> Down Level </text> <text text-anchor=middle x=75 y=14 class=id-trust-type ng-if=\"item.type === 'UpLevel'\" _t> Up Level </text> <text text-anchor=middle x=75 y=14 class=id-trust-type ng-if=\"item.type === 'MIT'\" _t> MIT </text> <text text-anchor=middle x=75 y=14 class=id-trust-type ng-if=\"item.type === 'DCE'\" _t> DCE </text> </g> </svg> <div class=arrow> <div class=line></div> <div class=\"id-arrowhead aad-trust-arrowhead-right\" ng-if=\"item.direction === 'OutBound' || item.direction === 'Bidirectional'\"></div> </div> </div> </div> <div class=\"id-trusted-domain aad-trust-domain\"> <div class=\"id-trusted-domain-icon aad-trust-domain-icon pull-left\"> <xui-icon icon=network></xui-icon> </div> <div class=\"id-trusted-domain-name text_wrap\" xui-ellipsis append-to-body=true ellipsis-options=\"{tooltipText: item.trustedDomainName}\"> <span class=xui-text-bg> {{::item.trustedDomainName}} </span> </div> </div> </div> ";

/***/ })
/******/ ]);
//# sourceMappingURL=apm.js.map