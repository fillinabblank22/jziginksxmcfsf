/*!
 * @solarwinds/apm 2020.2.5-302
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 27);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var OrionStatus;
(function (OrionStatus) {
    OrionStatus[OrionStatus["Unknown"] = 0] = "Unknown";
    OrionStatus[OrionStatus["Up"] = 1] = "Up";
    OrionStatus[OrionStatus["Critical"] = 14] = "Critical";
})(OrionStatus = exports.OrionStatus || (exports.OrionStatus = {}));


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<div class=aad-dc-roles ng-controller=domainControllerRolesController> <div class=aad-dc-roles__pill ng-repeat=\"role in aadDC.rolesList\"> <span title={{::aadDC.getTooltip(role)}} class=\"aad-dc-roles-tooltip xui-margin-sml\"> <svg class=aad-dc-roles__pill__ellipsis> <g> <rect x=0 y=1 rx=10 ry=50 width=50 height=18 /> <text text-anchor=middle x=26 y=14 class=xui-text-r>{{::role}}</text> </g> </svg> </span> </div> </div> ";

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = "<span class=xui-text-bg ng-if=\"vm.replicationData.status !== 0\" xui-popover xui-popover-content=vm.getAvailabilityPopoverTemplate()>{{vm.getAvailabilityDescription()}}</span> <span class=xui-text-bg ng-if=\"vm.replicationData.status === 0\" _t>Unknown</span> <script type=text/ng-template id=availability-popover> <div _t=\"['{{vm.replicationData.lastSuccessTime | apmDate}}']\">Last success: {0}</div>\n    <div ng-if=\"vm.replicationData.lastFailureTime\" _t=\"['{{vm.replicationData.lastFailureTime | apmDate}}']\">Last failure: {0}</div>\n    <div ng-if=\"vm.replicationData.consecutiveFailures > 1\" _t=\"['{{vm.replicationData.consecutiveFailures}}']\">{0} consecutive failures</div> </script> ";

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<replication-details-row replication-data=::item> </replication-details-row> ";

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = "<div> <xui-icon icon-size=small icon=\"{{'group' | swEntityIcon}}\" is-dynamic=true status=\"{{vm.replicationData.status | orionStatusIcon}}\"></xui-icon> <span class=xui-text-bg>{{vm.replicationData.name}}</span> <replication-details-availability style=float:right replication-data=vm.replicationData /> </div> <div class=xui-margin-smv> <span class=xui-text-s style=margin-left:24px _t>Replication</span> </div> ";

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = "<xui-search on-search=\"aadSite.onSearch(value, cancellation)\" on-clear=aadSite.cancelSearch() placeholder=_t(Search...) _ta> </xui-search> <xui-empty ng-if=\"aadSite.sites.length === 0\"></xui-empty> <div class=aad-site-tree-view> <xui-treeview class=xui-margin-mdv model=aadSite.sites options=aadSite.settings state={{aadSite.treeViewState}} node-identifier=id> <div class=aad-site-node> <div> <div ng-if=\"node.$model.type === 'domainControllers'\" class=\"aad-site-domain-controller xui-margin-smv\"> <xui-icon icon=unknownnode status={{node.$model.statusDescription}}></xui-icon> <div ng-if=node.$model.nodeDetailsUrl class=\"xui-margin-mdl aad-site-domain-controller-monitored\"> <a ng-href={{node.$model.nodeDetailsUrl}}>{{node.$model.name}}</a> </div> <div ng-if=!node.$model.nodeDetailsUrl class=xui-margin-mdl> <span>{{node.$model.name}}</span> <div> <a href=\"http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionSAMAGAppInsightforADDomainController\" target=_blank _t> » Learn how to monitor this node </a> </div> </div> </div> <span ng-if=\"node.$model.type !== 'domainControllers'\"> {{node.$model.label}} </span> <span ng-if=\"(node.$model.type === 'links-title' || node.$model.type === 'subnets-title' || node.$model.type === 'domainControllers-title')\"> ({{node.$model.children.length}}) </span> </div> <div ng-if=\"node.$model.type == 'site'\"> <span class=xui-margin-lgr _t=\"['{{node.$model.data.linksCount}}']\"> {0} Links </span> <span class=xui-margin-lgr _t=\"['{{node.$model.data.subnetsCount}}']\"> {0} Subnets </span> <span class=xui-margin-lgr _t=\"['{{node.$model.data.domainControllersCount}}']\"> {0} Replication Servers </span> </div> </div> </xui-treeview> </div> ";

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "<div class=\"id-trust-transitivity xui-text-s\"> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isNonTransitive></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isNonTransitive></xui-icon> <span _t>Non Transitive</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isUpLevelOnly></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isUpLevelOnly></xui-icon> <span _t>Up Level Only</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isQuarantinedDomain></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isQuarantinedDomain></xui-icon> <span _t>Quarantined Domain</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isForestTransitive></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isForestTransitive></xui-icon> <span _t>Forest Transitive</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isCrossOrganization></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isCrossOrganization></xui-icon> <span _t>Cross Organization</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isWithinForest></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isWithinForest></xui-icon> <span _t>Within Forest</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isTreatAsExternal></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isTreatAsExternal></xui-icon> <span _t>Treat As External</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isUsingRC4Encryption></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isUsingRC4Encryption></xui-icon> <span _t>Using RC4 Encryption</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isCrossOrganizationNoTgtDelegation></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isCrossOrganizationNoTgtDelegation></xui-icon> <span _t>Cross Organization No TGT Delegation</span> </p> <p> <xui-icon icon=step-complete icon-size=small ng-if=item.isPIMTrust></xui-icon> <xui-icon icon=close icon-size=small ng-if=!item.isPIMTrust></xui-icon> <span _t>PIM Trust</span> </p> </div> ";

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionStatusIconFilter_1 = __webpack_require__(8);
exports.default = function (module) {
    module.filter("orionStatusIcon", orionStatusIconFilter_1.orionStatusIconFilter);
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionStatus_1 = __webpack_require__(0);
/* @ngInject */
function orionStatusIconFilter() {
    return function (status) {
        if (status === orionStatus_1.OrionStatus.Unknown) {
            return "unknown";
        }
        else if (status === orionStatus_1.OrionStatus.Up) {
            return "up";
        }
        else if (status === orionStatus_1.OrionStatus.Critical) {
            return "critical";
        }
        else {
            return "unknown";
        }
    };
}
exports.orionStatusIconFilter = orionStatusIconFilter;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SiteDetailsService = /** @class */ (function () {
    /** @ngInject */
    SiteDetailsService.$inject = ["swApi"];
    function SiteDetailsService(swApi) {
        this.swApi = swApi;
    }
    SiteDetailsService.prototype.getSitesDetails = function (applicationId) {
        var result = this.swApi.api(false)
            .one("activedirectoryblackbox/sites/sites-with-related-entities/" + applicationId)
            .get();
        return result;
    };
    return SiteDetailsService;
}());
exports.SiteDetailsService = SiteDetailsService;


/***/ }),
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(28);


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(29);
var config_1 = __webpack_require__(30);
var index_1 = __webpack_require__(31);
var index_2 = __webpack_require__(47);
var index_3 = __webpack_require__(7);
config_1.default(app_1.default);
index_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("apm.appinsight-for-active-directory.components", []);
angular.module("apm.appinsight-for-active-directory.services", []);
angular.module("apm.appinsight-for-active-directory.filters", []);
angular.module("apm.appinsight-for-active-directory", [
    "apm.appinsight-for-active-directory.components",
    "apm.appinsight-for-active-directory.services",
    "apm.appinsight-for-active-directory.filters"
]);
// create and register Xui (Orion) module wrapper
var aad = Xui.registerModule("apm.appinsight-for-active-directory");
exports.default = aad;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: aad");
}
exports.default = function (module) {
    module.app()
        .run(run);
};


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(32);
var index_2 = __webpack_require__(34);
var index_3 = __webpack_require__(36);
var index_4 = __webpack_require__(38);
var index_5 = __webpack_require__(41);
var index_6 = __webpack_require__(45);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var replication_details_expander_controller_1 = __webpack_require__(33);
exports.default = function (module) {
    module.controller("replicationDetailsExpanderController", replication_details_expander_controller_1.ReplicationDetailsExpanderController);
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionStatus_1 = __webpack_require__(0);
var ReplicationDetailsExpanderController = /** @class */ (function () {
    /** @ngInject */
    ReplicationDetailsExpanderController.$inject = ["$templateCache", "namingContextReplicationService", "getTextService"];
    function ReplicationDetailsExpanderController($templateCache, namingContextReplicationService, getTextService) {
        this.$templateCache = $templateCache;
        this.namingContextReplicationService = namingContextReplicationService;
        this.getTextService = getTextService;
        this.ellipsisOptions = {
            tooltipText: this.getTextService("Learn how to monitor this node"),
            tooltipOptions: {}
        };
        this.$templateCache.put("replication-details-template", __webpack_require__(3));
    }
    ReplicationDetailsExpanderController.prototype.onExpanderStatusChanged = function (args) {
        var _this = this;
        if (args.isOpen) {
            this.namingContextReplicationService.getNamingContextReplication(args.sourceDomainControllerFqdn, args.destinationDomainControllerFqdn)
                .then(function (namingContextReplications) {
                _this.namingContextReplications = namingContextReplications;
            });
        }
    };
    ReplicationDetailsExpanderController.prototype.getPillStatus = function (replicationStatus) {
        if (replicationStatus === orionStatus_1.OrionStatus.Unknown) {
            return "aad-replication-status-unknown";
        }
        else if (replicationStatus === orionStatus_1.OrionStatus.Up) {
            return "aad-replication-status-available";
        }
        else if (replicationStatus === orionStatus_1.OrionStatus.Critical) {
            return "aad-replication-status-critical";
        }
        else {
            return "aad-replication-status-unknown";
        }
    };
    return ReplicationDetailsExpanderController;
}());
exports.ReplicationDetailsExpanderController = ReplicationDetailsExpanderController;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var replication_details_availability_controller_1 = __webpack_require__(35);
exports.default = function (module) {
    module.controller("replicationDetailsAvailabilityController", replication_details_availability_controller_1.ReplicationDetailsAvailability);
    module.app().component("replicationDetailsAvailability", {
        template: __webpack_require__(2),
        controller: "replicationDetailsAvailabilityController",
        controllerAs: "vm",
        bindings: {
            replicationData: "<"
        }
    });
};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionStatus_1 = __webpack_require__(0);
var ReplicationDetailsAvailability = /** @class */ (function () {
    /** @ngInject */
    ReplicationDetailsAvailability.$inject = ["getTextService"];
    function ReplicationDetailsAvailability(getTextService) {
        this.getTextService = getTextService;
        this._t = this.getTextService;
    }
    ReplicationDetailsAvailability.prototype.getAvailabilityPopoverTemplate = function () {
        return { url: "availability-popover" };
    };
    ReplicationDetailsAvailability.prototype.getAvailabilityDescription = function () {
        if (this.replicationData.status === orionStatus_1.OrionStatus.Up) {
            return this._t("Available");
        }
        else if (this.replicationData.status === orionStatus_1.OrionStatus.Critical) {
            return this._t("Unavailable");
        }
        else {
            return "";
        }
    };
    return ReplicationDetailsAvailability;
}());
exports.ReplicationDetailsAvailability = ReplicationDetailsAvailability;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var replication_details_row_controller_1 = __webpack_require__(37);
exports.default = function (module) {
    module.controller("replicationDetailsRowController", replication_details_row_controller_1.ReplicationDetailsRowController);
    module.app().component("replicationDetailsRow", {
        template: __webpack_require__(4),
        controller: "replicationDetailsRowController",
        controllerAs: "vm",
        bindings: {
            replicationData: "<"
        }
    });
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ReplicationDetailsRowController = /** @class */ (function () {
    function ReplicationDetailsRowController() {
    }
    return ReplicationDetailsRowController;
}());
exports.ReplicationDetailsRowController = ReplicationDetailsRowController;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var domain_controller_roles_controller_1 = __webpack_require__(39);
exports.default = function (module) {
    module.controller("domainControllerRolesController", domain_controller_roles_controller_1.DomainControllerRolesController);
    module.app().component("domainControllerRoles", {
        template: __webpack_require__(1),
        controller: "domainControllerRolesController",
        controllerAs: "aadDC",
        bindings: {
            roles: "<"
        }
    });
};


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var fsmoRoles_1 = __webpack_require__(40);
var SchemaMaster = fsmoRoles_1.FsmoRoles.SchemaMaster;
var DomainNamingMaster = fsmoRoles_1.FsmoRoles.DomainNamingMaster;
var InfrastructureMaster = fsmoRoles_1.FsmoRoles.InfrastructureMaster;
var RidMaster = fsmoRoles_1.FsmoRoles.RidMaster;
var PdcEmulator = fsmoRoles_1.FsmoRoles.PdcEmulator;
/* tslint:disable:no-bitwise */
var DomainControllerRolesController = /** @class */ (function () {
    /** @ngInject */
    DomainControllerRolesController.$inject = ["getTextService"];
    function DomainControllerRolesController(getTextService) {
        this.getTextService = getTextService;
        this._t = this.getTextService;
        this.tooltips = {
            "SM": this._t("Role: Schema Master"),
            "DN": this._t("Role: Domain Name"),
            "IM": this._t("Role: Infrastructure Manager"),
            "RID": this._t("Role: Relative Identifier Master"),
            "PDC": this._t("Role: Primary Domain Controller Emulator")
        };
    }
    Object.defineProperty(DomainControllerRolesController.prototype, "roles", {
        get: function () {
            return this._roles;
        },
        set: function (roles) {
            this._roles = roles;
            if (this.rolesList === undefined) {
                this.rolesList = [];
            }
            if ((this.roles & SchemaMaster) === fsmoRoles_1.FsmoRoles.SchemaMaster) {
                this.rolesList.push("SM");
            }
            if ((this.roles & DomainNamingMaster) === fsmoRoles_1.FsmoRoles.DomainNamingMaster) {
                this.rolesList.push("DN");
            }
            if ((this.roles & InfrastructureMaster) === fsmoRoles_1.FsmoRoles.InfrastructureMaster) {
                this.rolesList.push("IM");
            }
            if ((this.roles & RidMaster) === fsmoRoles_1.FsmoRoles.RidMaster) {
                this.rolesList.push("RID");
            }
            if ((this.roles & PdcEmulator) === fsmoRoles_1.FsmoRoles.PdcEmulator) {
                this.rolesList.push("PDC");
            }
        },
        enumerable: true,
        configurable: true
    });
    DomainControllerRolesController.prototype.getTooltip = function (role) {
        if (role in this.tooltips) {
            return this.tooltips[role];
        }
        return "";
    };
    return DomainControllerRolesController;
}());
exports.DomainControllerRolesController = DomainControllerRolesController;
/* tslint:enable:no-bitwise */


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable:no-bitwise */
var FsmoRoles;
(function (FsmoRoles) {
    FsmoRoles[FsmoRoles["None"] = 0] = "None";
    FsmoRoles[FsmoRoles["SchemaMaster"] = 1] = "SchemaMaster";
    FsmoRoles[FsmoRoles["DomainNamingMaster"] = 2] = "DomainNamingMaster";
    FsmoRoles[FsmoRoles["RidMaster"] = 4] = "RidMaster";
    FsmoRoles[FsmoRoles["InfrastructureMaster"] = 8] = "InfrastructureMaster";
    FsmoRoles[FsmoRoles["PdcEmulator"] = 16] = "PdcEmulator";
})(FsmoRoles = exports.FsmoRoles || (exports.FsmoRoles = {}));
/* tslint:enable:no-bitwise */


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var site_tree_view_controller_1 = __webpack_require__(42);
exports.default = function (module) {
    module.controller("siteTreeViewController", site_tree_view_controller_1.SiteTreeViewController);
};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var siteDetails_service_1 = __webpack_require__(9);
var _ = __webpack_require__(43);
var widgets_1 = __webpack_require__(44);
var SiteTreeViewController = /** @class */ (function () {
    /** @ngInject */
    SiteTreeViewController.$inject = ["siteDetailsService", "$timeout", "$location", "getTextService"];
    function SiteTreeViewController(siteDetailsService, $timeout, $location, getTextService) {
        this.siteDetailsService = siteDetailsService;
        this.$timeout = $timeout;
        this.$location = $location;
        this.getTextService = getTextService;
    }
    SiteTreeViewController.prototype.$onInit = function () {
        var _this = this;
        this.sites = [];
        this.allSites = [];
        this._t = this.getTextService;
        this.settings = { "limit": 10 };
        this.siteDetailsService.getSitesDetails(this.getApplicationIdFromUrl()).then(function (result) {
            _this.allSites = result.siteDetails;
            _this.convertToXuiTreeFormat(_this.allSites);
        });
    };
    SiteTreeViewController.prototype.onSearch = function (value) {
        var _this = this;
        var filteredSitesDetails = this.allSites;
        if (value !== undefined) {
            filteredSitesDetails = this.filterSitesDetailsByName(this.allSites, value);
        }
        this.treeViewState = [];
        this.convertToXuiTreeFormat(filteredSitesDetails);
        this.$timeout(function () {
            _this.expandAllChildren(_this.sites, [], value !== "" && value !== undefined);
        });
    };
    SiteTreeViewController.prototype.cancelSearch = function () {
        this.convertToXuiTreeFormat(this.allSites);
    };
    SiteTreeViewController.prototype.onLoad = function (params) {
        /* ignore */
    };
    SiteTreeViewController.prototype.onUnload = function () {
        /* ignore */
    };
    SiteTreeViewController.prototype.onSettingsChanged = function (settings, afterSave) {
        /* ignore */
    };
    SiteTreeViewController.prototype.getApplicationIdFromUrl = function () {
        return parseInt(decodeURIComponent(this.$location.absUrl()).match("NetObject=ABAA:([0-9]+)")[1], 10);
    };
    SiteTreeViewController.prototype.filterSitesDetailsByName = function (dataSource, search) {
        var _this = this;
        if (search === "") {
            return dataSource;
        }
        var siteWithFilteredChildren = _.map(dataSource, function (site) {
            var links = site.links, subnets = site.subnets, domainControllers = site.domainControllers;
            if (!_this.isStringContainsSubstring(site.name, search)) {
                links = _this.filterSiteChildren(site, "links", search);
                subnets = _this.filterSiteChildren(site, "subnets", search);
                domainControllers = _this.filterSiteChildren(site, "domainControllers", search);
            }
            return __assign({}, site, { links: links, subnets: subnets, domainControllers: domainControllers });
        });
        return this.filterSites(siteWithFilteredChildren, search);
    };
    SiteTreeViewController.prototype.filterSites = function (sites, search) {
        var _this = this;
        return sites.filter(function (site) {
            return _this.isStringContainsSubstring(site.name, search) ||
                _this.doesSiteHaveAnyChild(site);
        });
    };
    SiteTreeViewController.prototype.filterSiteChildren = function (site, type, search) {
        var _this = this;
        return _.filter(site[type], function (child) { return _this.isStringContainsSubstring(child.name, search); });
    };
    SiteTreeViewController.prototype.isStringContainsSubstring = function (text, subtext) {
        return text.toLowerCase().indexOf(subtext.toLowerCase()) !== -1;
    };
    SiteTreeViewController.prototype.convertToXuiTreeFormat = function (dataSource) {
        var _this = this;
        this.sites = _.map(dataSource, function (site) {
            var siteElement = _this.getSiteForXuiTree(site);
            if (_this.doesSiteHaveAnyChild(site)) {
                siteElement.children = [];
            }
            _this.appendChildrenToSite(siteElement, site.links, "links", _this._t("Links"));
            _this.appendChildrenToSite(siteElement, site.subnets, "subnets", _this._t("Subnets"));
            _this.appendChildrenToSite(siteElement, site.domainControllers, "domainControllers", _this._t("Replication servers"));
            return siteElement;
        });
    };
    SiteTreeViewController.prototype.doesSiteHaveAnyChild = function (site) {
        return site.links.length + site.subnets.length + site.domainControllers.length > 0;
    };
    SiteTreeViewController.prototype.getSiteForXuiTree = function (site) {
        return {
            id: "site-" + site.name,
            label: site.name,
            type: "site",
            icon: ("status_" + site.statusDescription).toLowerCase(),
            data: {
                linksCount: site.links.length,
                subnetsCount: site.subnets.length,
                domainControllersCount: site.domainControllers.length
            }
        };
    };
    SiteTreeViewController.prototype.appendChildrenToSite = function (site, dataSource, type, label) {
        if (dataSource.length === 0) {
            return;
        }
        site.children.push({
            id: type,
            label: label,
            type: type + "-title",
            children: _.map(dataSource, function (child) {
                return __assign({}, child, { label: child.name, id: type + "-" + child.name, type: type });
            })
        });
    };
    SiteTreeViewController.prototype.expandAllChildren = function (children, path, isExpanded) {
        var _this = this;
        _.forEach(children, function (child) {
            if (child.children) {
                _this.treeViewState.push({
                    id: child.id,
                    path: path,
                    expanded: isExpanded,
                    checked: false
                });
                _this.expandAllChildren(child.children, path.concat(child.id), isExpanded);
            }
        });
    };
    SiteTreeViewController = __decorate([
        widgets_1.Widget({
            selector: "siteTreeView",
            template: __webpack_require__(5),
            controllerAs: "aadSite",
            settingName: "swSiteTreeViewSettings"
        }),
        __metadata("design:paramtypes", [siteDetails_service_1.SiteDetailsService, Function, Object, Function])
    ], SiteTreeViewController);
    return SiteTreeViewController;
}());
exports.SiteTreeViewController = SiteTreeViewController;


/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = _;

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var trust_summary_controller_1 = __webpack_require__(46);
exports.default = function (module) {
    module.controller("trustSummaryController", trust_summary_controller_1.TrustSummaryController);
};


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var TrustSummaryController = /** @class */ (function () {
    /** @ngInject */
    TrustSummaryController.$inject = ["$templateCache"];
    function TrustSummaryController($templateCache) {
        this.$templateCache = $templateCache;
        var templateName = "trust-summary-template";
        this.$templateCache.put(templateName, __webpack_require__(6));
        this.popOverTemplate = function () { return { url: templateName }; };
    }
    return TrustSummaryController;
}());
exports.TrustSummaryController = TrustSummaryController;


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var namingContextReplication_service_1 = __webpack_require__(48);
var siteDetails_service_1 = __webpack_require__(9);
exports.default = function (module) {
    module.service("namingContextReplicationService", namingContextReplication_service_1.NamingContextReplicationService);
    module.service("siteDetailsService", siteDetails_service_1.SiteDetailsService);
};


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NamingContextReplicationService = /** @class */ (function () {
    /** @ngInject */
    NamingContextReplicationService.$inject = ["swApi"];
    function NamingContextReplicationService(swApi) {
        this.swApi = swApi;
    }
    NamingContextReplicationService.prototype.getNamingContextReplication = function (sourceDomainControllerFqdn, destinationDomainControllerFqdn) {
        var result = this.swApi.api(false)
            .one("activedirectoryblackbox/src/" + sourceDomainControllerFqdn + "/dest/" + destinationDomainControllerFqdn + "/replications")
            .get();
        return result;
    };
    return NamingContextReplicationService;
}());
exports.NamingContextReplicationService = NamingContextReplicationService;


/***/ })
/******/ ]);
//# sourceMappingURL=appInsightForActiveDirectory.js.map