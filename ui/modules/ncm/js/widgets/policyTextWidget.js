/*!
 * @solarwinds/ncm-apollo 2020.2.5-3053
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 221);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var securityPolicy_1 = __webpack_require__(2);
exports.swApiUrl = "ncm/securityPolicies";
var NcmSecurityPoliciesService = /** @class */ (function () {
    /** @ngInject */
    NcmSecurityPoliciesService.$inject = ["swApi", "_t"];
    function NcmSecurityPoliciesService(swApi, _t) {
        var _this = this;
        this.swApi = swApi;
        this._t = _t;
        this.policyOriginNamesMap = {};
        this.getSecurityPolicies = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicy = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyChanges = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/changeHistory/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyNodes = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/similarPolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyApplication = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/applicationPolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyAddresses = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/addresses/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyServices = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/servicePolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyOriginName = function (policyOrigin) {
            return _this.policyOriginNamesMap[policyOrigin];
        };
        this.initPolicyOriginNameDictionary = function () {
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.Local] = _this._t("Local");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.PostPanorama] = _this._t("Post policy (Panorama)");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.PrePanorama] = _this._t("Pre policy (Panorama)");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.Unknown] = _this._t("Unknown");
        };
        this.initPolicyOriginNameDictionary();
    }
    NcmSecurityPoliciesService.prototype.getSecurityPolicyText = function (nodeId, policyName) {
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + policyName + "/text/nodes/" + nodeId)
            .get();
    };
    return NcmSecurityPoliciesService;
}());
exports.NcmSecurityPoliciesService = NcmSecurityPoliciesService;


/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmPolicyWidgetsService = /** @class */ (function () {
    /** @ngInject */
    NcmPolicyWidgetsService.$inject = ["$window"];
    function NcmPolicyWidgetsService($window) {
        this.$window = $window;
    }
    NcmPolicyWidgetsService.prototype.getNodeIdFromUrl = function () {
        var regExpMatchArray = decodeURIComponent(this.$window.location.search).match(/NodeId=([0-9]+)(&|$)/);
        if (regExpMatchArray) {
            return Number(regExpMatchArray[1]);
        }
        return null;
    };
    ;
    NcmPolicyWidgetsService.prototype.getPolicyNameFromUrl = function () {
        var regExpMatchArray = decodeURIComponent(this.$window.location.search).match(/PolicyName=(.+?)(&|$)/);
        if (regExpMatchArray) {
            return atob(regExpMatchArray[1]);
        }
        return null;
    };
    ;
    return NcmPolicyWidgetsService;
}());
exports.NcmPolicyWidgetsService = NcmPolicyWidgetsService;


/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var SecurityPolicyAction;
(function (SecurityPolicyAction) {
    SecurityPolicyAction[SecurityPolicyAction["Allow"] = 0] = "Allow";
    SecurityPolicyAction[SecurityPolicyAction["Deny"] = 1] = "Deny";
    SecurityPolicyAction[SecurityPolicyAction["Drop"] = 2] = "Drop";
    SecurityPolicyAction[SecurityPolicyAction["ResetClient"] = 3] = "ResetClient";
    SecurityPolicyAction[SecurityPolicyAction["ResetServer"] = 4] = "ResetServer";
    SecurityPolicyAction[SecurityPolicyAction["ResetBothClientAndServer"] = 5] = "ResetBothClientAndServer";
})(SecurityPolicyAction = exports.SecurityPolicyAction || (exports.SecurityPolicyAction = {}));
var PolicyAddressType;
(function (PolicyAddressType) {
    PolicyAddressType[PolicyAddressType["Source"] = 0] = "Source";
    PolicyAddressType[PolicyAddressType["Destination"] = 1] = "Destination";
})(PolicyAddressType = exports.PolicyAddressType || (exports.PolicyAddressType = {}));
var PolicyType;
(function (PolicyType) {
    PolicyType[PolicyType["universal"] = 0] = "universal";
    PolicyType[PolicyType["intrazone"] = 1] = "intrazone";
    PolicyType[PolicyType["interzone"] = 2] = "interzone";
})(PolicyType = exports.PolicyType || (exports.PolicyType = {}));
var SecurityPolicyOrigin;
(function (SecurityPolicyOrigin) {
    SecurityPolicyOrigin[SecurityPolicyOrigin["Unknown"] = 0] = "Unknown";
    SecurityPolicyOrigin[SecurityPolicyOrigin["PrePanorama"] = 1] = "PrePanorama";
    SecurityPolicyOrigin[SecurityPolicyOrigin["Local"] = 2] = "Local";
    SecurityPolicyOrigin[SecurityPolicyOrigin["PostPanorama"] = 3] = "PostPanorama";
})(SecurityPolicyOrigin = exports.SecurityPolicyOrigin || (exports.SecurityPolicyOrigin = {}));
var SecurityPolicyChangeSource;
(function (SecurityPolicyChangeSource) {
    SecurityPolicyChangeSource[SecurityPolicyChangeSource["Local"] = 0] = "Local";
    SecurityPolicyChangeSource[SecurityPolicyChangeSource["Pushed"] = 1] = "Pushed";
})(SecurityPolicyChangeSource = exports.SecurityPolicyChangeSource || (exports.SecurityPolicyChangeSource = {}));
var SecurityPolicyLight = /** @class */ (function () {
    function SecurityPolicyLight() {
    }
    return SecurityPolicyLight;
}());
exports.SecurityPolicyLight = SecurityPolicyLight;
var SecurityPolicyFilter = /** @class */ (function () {
    function SecurityPolicyFilter() {
    }
    return SecurityPolicyFilter;
}());
exports.SecurityPolicyFilter = SecurityPolicyFilter;
var SecurityPolicy = /** @class */ (function (_super) {
    __extends(SecurityPolicy, _super);
    function SecurityPolicy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SecurityPolicy;
}(SecurityPolicyLight));
exports.SecurityPolicy = SecurityPolicy;
var SecurityPolicyChangeInfo = /** @class */ (function () {
    function SecurityPolicyChangeInfo() {
    }
    return SecurityPolicyChangeInfo;
}());
exports.SecurityPolicyChangeInfo = SecurityPolicyChangeInfo;
var SecurityPolicyChange = /** @class */ (function () {
    function SecurityPolicyChange() {
    }
    return SecurityPolicyChange;
}());
exports.SecurityPolicyChange = SecurityPolicyChange;
var SecurityPolicyChangesModel = /** @class */ (function () {
    function SecurityPolicyChangesModel() {
    }
    return SecurityPolicyChangesModel;
}());
exports.SecurityPolicyChangesModel = SecurityPolicyChangesModel;
var SecurityPolicyNode = /** @class */ (function () {
    function SecurityPolicyNode() {
    }
    return SecurityPolicyNode;
}());
exports.SecurityPolicyNode = SecurityPolicyNode;
var ApplicationPolicy = /** @class */ (function () {
    function ApplicationPolicy() {
    }
    return ApplicationPolicy;
}());
exports.ApplicationPolicy = ApplicationPolicy;
var ServicePolicy = /** @class */ (function () {
    function ServicePolicy() {
    }
    return ServicePolicy;
}());
exports.ServicePolicy = ServicePolicy;
var SecurityPolicyAddress = /** @class */ (function () {
    function SecurityPolicyAddress() {
    }
    return SecurityPolicyAddress;
}());
exports.SecurityPolicyAddress = SecurityPolicyAddress;
var TokenType;
(function (TokenType) {
    TokenType[TokenType["Unknown"] = 0] = "Unknown";
    TokenType[TokenType["Action"] = 1] = "Action";
    TokenType[TokenType["SourceZone"] = 2] = "SourceZone";
    TokenType[TokenType["DestinationZone"] = 3] = "DestinationZone";
    TokenType[TokenType["PolicyOrigin"] = 4] = "PolicyOrigin";
})(TokenType = exports.TokenType || (exports.TokenType = {}));
var Token = /** @class */ (function () {
    function Token() {
    }
    return Token;
}());
exports.Token = Token;


/***/ }),

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(222);


/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(223);
var ncmSecurityPolicies_service_1 = __webpack_require__(0);
var ncmPolicyWidgets_service_1 = __webpack_require__(1);
var policyTextWidget_controller_1 = __webpack_require__(224);
var _t_service_1 = __webpack_require__(3);
angular.module("widgets")
    .service("ncmSecurityPoliciesService", ncmSecurityPolicies_service_1.NcmSecurityPoliciesService)
    .service("ncmPolicyWidgetsService", ncmPolicyWidgets_service_1.NcmPolicyWidgetsService)
    .service("_t", _t_service_1.default)
    .controller("ncmPolicyTextWidget", policyTextWidget_controller_1.PolicyTextWidget);


/***/ }),

/***/ 223:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 224:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(4);
var ncmSecurityPolicies_service_1 = __webpack_require__(0);
var ncmPolicyWidgets_service_1 = __webpack_require__(1);
var PolicyTextWidget = /** @class */ (function () {
    /** @ngInject */
    PolicyTextWidget.$inject = ["$log", "ncmSecurityPoliciesService", "ncmPolicyWidgetsService"];
    function PolicyTextWidget($log, ncmSecurityPoliciesService, ncmPolicyWidgetsService) {
        this.$log = $log;
        this.ncmSecurityPoliciesService = ncmSecurityPoliciesService;
        this.ncmPolicyWidgetsService = ncmPolicyWidgetsService;
    }
    PolicyTextWidget.prototype.onLoad = function (config) {
        var _this = this;
        this.config = config;
        this.config.isBusy(true);
        var nodeId = this.ncmPolicyWidgetsService.getNodeIdFromUrl();
        var policyName = this.ncmPolicyWidgetsService.getPolicyNameFromUrl();
        this.ncmSecurityPoliciesService.getSecurityPolicyText(nodeId, policyName).then(function (result) {
            _this.policyText = result;
            _this.config.isBusy(false);
        });
    };
    ;
    PolicyTextWidget = __decorate([
        widgets_1.Widget({
            selector: "ncmPolicyTextWidget",
            template: __webpack_require__(225),
            controllerAs: "vm",
            settingName: "ncmPolicyTextWidgetSettings"
        }),
        __metadata("design:paramtypes", [Object, ncmSecurityPolicies_service_1.NcmSecurityPoliciesService,
            ncmPolicyWidgets_service_1.NcmPolicyWidgetsService])
    ], PolicyTextWidget);
    return PolicyTextWidget;
}());
exports.PolicyTextWidget = PolicyTextWidget;


/***/ }),

/***/ 225:
/***/ (function(module, exports) {

module.exports = "<div class=ncm-policy-text-widget id=policy-text> <p ng-repeat=\"singleLine in vm.policyText track by $index\">{{singleLine}}<br/></p> </div>";

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
_tService.$inject = ["getTextService"];
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable  */
/** @ngInject */
function _tService(getTextService) {
    return function (input) {
        return getTextService(input);
    };
}
exports.default = _tService;


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ })

/******/ });
//# sourceMappingURL=policyTextWidget.js.map