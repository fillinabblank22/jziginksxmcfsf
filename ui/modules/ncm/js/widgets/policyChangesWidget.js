/*!
 * @solarwinds/ncm-apollo 2020.2.5-3053
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 201);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var securityPolicy_1 = __webpack_require__(2);
exports.swApiUrl = "ncm/securityPolicies";
var NcmSecurityPoliciesService = /** @class */ (function () {
    /** @ngInject */
    NcmSecurityPoliciesService.$inject = ["swApi", "_t"];
    function NcmSecurityPoliciesService(swApi, _t) {
        var _this = this;
        this.swApi = swApi;
        this._t = _t;
        this.policyOriginNamesMap = {};
        this.getSecurityPolicies = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicy = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyChanges = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/changeHistory/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyNodes = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/similarPolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyApplication = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/applicationPolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyAddresses = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/addresses/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyServices = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/servicePolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyOriginName = function (policyOrigin) {
            return _this.policyOriginNamesMap[policyOrigin];
        };
        this.initPolicyOriginNameDictionary = function () {
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.Local] = _this._t("Local");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.PostPanorama] = _this._t("Post policy (Panorama)");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.PrePanorama] = _this._t("Pre policy (Panorama)");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.Unknown] = _this._t("Unknown");
        };
        this.initPolicyOriginNameDictionary();
    }
    NcmSecurityPoliciesService.prototype.getSecurityPolicyText = function (nodeId, policyName) {
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + policyName + "/text/nodes/" + nodeId)
            .get();
    };
    return NcmSecurityPoliciesService;
}());
exports.NcmSecurityPoliciesService = NcmSecurityPoliciesService;


/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmPolicyWidgetsService = /** @class */ (function () {
    /** @ngInject */
    NcmPolicyWidgetsService.$inject = ["$window"];
    function NcmPolicyWidgetsService($window) {
        this.$window = $window;
    }
    NcmPolicyWidgetsService.prototype.getNodeIdFromUrl = function () {
        var regExpMatchArray = decodeURIComponent(this.$window.location.search).match(/NodeId=([0-9]+)(&|$)/);
        if (regExpMatchArray) {
            return Number(regExpMatchArray[1]);
        }
        return null;
    };
    ;
    NcmPolicyWidgetsService.prototype.getPolicyNameFromUrl = function () {
        var regExpMatchArray = decodeURIComponent(this.$window.location.search).match(/PolicyName=(.+?)(&|$)/);
        if (regExpMatchArray) {
            return atob(regExpMatchArray[1]);
        }
        return null;
    };
    ;
    return NcmPolicyWidgetsService;
}());
exports.NcmPolicyWidgetsService = NcmPolicyWidgetsService;


/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmViewsService = /** @class */ (function () {
    /** @ngInject */
    NcmViewsService.$inject = ["swisService"];
    function NcmViewsService(swisService) {
        this.swisService = swisService;
    }
    NcmViewsService.prototype.getAccessListsSubviewId = function () {
        return this.getViewId("NCMAccessLists");
    };
    NcmViewsService.prototype.getAsaInterfacesSubviewId = function () {
        return this.getViewId("ASA Interfaces Subview");
    };
    NcmViewsService.prototype.getConfigsSubviewId = function () {
        return this.getViewId("Configs SubView");
    };
    NcmViewsService.prototype.getViewId = function (viewName) {
        return this.swisService.query("SELECT ViewID FROM Orion.Views WHERE ViewKey=@viewKey", { viewKey: viewName })
            .then(function (results) {
            if (results.rows.length > 0) {
                return results.rows[0].ViewID;
            }
            else {
                return null;
            }
        });
    };
    return NcmViewsService;
}());
exports.NcmViewsService = NcmViewsService;


/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ConfigDiffPageSettings;
(function (ConfigDiffPageSettings) {
    ConfigDiffPageSettings[ConfigDiffPageSettings["None"] = 0] = "None";
    ConfigDiffPageSettings[ConfigDiffPageSettings["Selection"] = 1] = "Selection";
    ConfigDiffPageSettings[ConfigDiffPageSettings["AdditionalCommands"] = 2] = "AdditionalCommands";
    ConfigDiffPageSettings[ConfigDiffPageSettings["Pushed"] = 4] = "Pushed";
})(ConfigDiffPageSettings = exports.ConfigDiffPageSettings || (exports.ConfigDiffPageSettings = {}));


/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var SecurityPolicyAction;
(function (SecurityPolicyAction) {
    SecurityPolicyAction[SecurityPolicyAction["Allow"] = 0] = "Allow";
    SecurityPolicyAction[SecurityPolicyAction["Deny"] = 1] = "Deny";
    SecurityPolicyAction[SecurityPolicyAction["Drop"] = 2] = "Drop";
    SecurityPolicyAction[SecurityPolicyAction["ResetClient"] = 3] = "ResetClient";
    SecurityPolicyAction[SecurityPolicyAction["ResetServer"] = 4] = "ResetServer";
    SecurityPolicyAction[SecurityPolicyAction["ResetBothClientAndServer"] = 5] = "ResetBothClientAndServer";
})(SecurityPolicyAction = exports.SecurityPolicyAction || (exports.SecurityPolicyAction = {}));
var PolicyAddressType;
(function (PolicyAddressType) {
    PolicyAddressType[PolicyAddressType["Source"] = 0] = "Source";
    PolicyAddressType[PolicyAddressType["Destination"] = 1] = "Destination";
})(PolicyAddressType = exports.PolicyAddressType || (exports.PolicyAddressType = {}));
var PolicyType;
(function (PolicyType) {
    PolicyType[PolicyType["universal"] = 0] = "universal";
    PolicyType[PolicyType["intrazone"] = 1] = "intrazone";
    PolicyType[PolicyType["interzone"] = 2] = "interzone";
})(PolicyType = exports.PolicyType || (exports.PolicyType = {}));
var SecurityPolicyOrigin;
(function (SecurityPolicyOrigin) {
    SecurityPolicyOrigin[SecurityPolicyOrigin["Unknown"] = 0] = "Unknown";
    SecurityPolicyOrigin[SecurityPolicyOrigin["PrePanorama"] = 1] = "PrePanorama";
    SecurityPolicyOrigin[SecurityPolicyOrigin["Local"] = 2] = "Local";
    SecurityPolicyOrigin[SecurityPolicyOrigin["PostPanorama"] = 3] = "PostPanorama";
})(SecurityPolicyOrigin = exports.SecurityPolicyOrigin || (exports.SecurityPolicyOrigin = {}));
var SecurityPolicyChangeSource;
(function (SecurityPolicyChangeSource) {
    SecurityPolicyChangeSource[SecurityPolicyChangeSource["Local"] = 0] = "Local";
    SecurityPolicyChangeSource[SecurityPolicyChangeSource["Pushed"] = 1] = "Pushed";
})(SecurityPolicyChangeSource = exports.SecurityPolicyChangeSource || (exports.SecurityPolicyChangeSource = {}));
var SecurityPolicyLight = /** @class */ (function () {
    function SecurityPolicyLight() {
    }
    return SecurityPolicyLight;
}());
exports.SecurityPolicyLight = SecurityPolicyLight;
var SecurityPolicyFilter = /** @class */ (function () {
    function SecurityPolicyFilter() {
    }
    return SecurityPolicyFilter;
}());
exports.SecurityPolicyFilter = SecurityPolicyFilter;
var SecurityPolicy = /** @class */ (function (_super) {
    __extends(SecurityPolicy, _super);
    function SecurityPolicy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SecurityPolicy;
}(SecurityPolicyLight));
exports.SecurityPolicy = SecurityPolicy;
var SecurityPolicyChangeInfo = /** @class */ (function () {
    function SecurityPolicyChangeInfo() {
    }
    return SecurityPolicyChangeInfo;
}());
exports.SecurityPolicyChangeInfo = SecurityPolicyChangeInfo;
var SecurityPolicyChange = /** @class */ (function () {
    function SecurityPolicyChange() {
    }
    return SecurityPolicyChange;
}());
exports.SecurityPolicyChange = SecurityPolicyChange;
var SecurityPolicyChangesModel = /** @class */ (function () {
    function SecurityPolicyChangesModel() {
    }
    return SecurityPolicyChangesModel;
}());
exports.SecurityPolicyChangesModel = SecurityPolicyChangesModel;
var SecurityPolicyNode = /** @class */ (function () {
    function SecurityPolicyNode() {
    }
    return SecurityPolicyNode;
}());
exports.SecurityPolicyNode = SecurityPolicyNode;
var ApplicationPolicy = /** @class */ (function () {
    function ApplicationPolicy() {
    }
    return ApplicationPolicy;
}());
exports.ApplicationPolicy = ApplicationPolicy;
var ServicePolicy = /** @class */ (function () {
    function ServicePolicy() {
    }
    return ServicePolicy;
}());
exports.ServicePolicy = ServicePolicy;
var SecurityPolicyAddress = /** @class */ (function () {
    function SecurityPolicyAddress() {
    }
    return SecurityPolicyAddress;
}());
exports.SecurityPolicyAddress = SecurityPolicyAddress;
var TokenType;
(function (TokenType) {
    TokenType[TokenType["Unknown"] = 0] = "Unknown";
    TokenType[TokenType["Action"] = 1] = "Action";
    TokenType[TokenType["SourceZone"] = 2] = "SourceZone";
    TokenType[TokenType["DestinationZone"] = 3] = "DestinationZone";
    TokenType[TokenType["PolicyOrigin"] = 4] = "PolicyOrigin";
})(TokenType = exports.TokenType || (exports.TokenType = {}));
var Token = /** @class */ (function () {
    function Token() {
    }
    return Token;
}());
exports.Token = Token;


/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(202);


/***/ }),

/***/ 202:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(203);
var ncmPolicyWidgets_service_1 = __webpack_require__(1);
var ncmSecurityPolicies_service_1 = __webpack_require__(0);
var policyChangesWidget_controller_1 = __webpack_require__(204);
var ncmNavigation_service_1 = __webpack_require__(8);
var ncmViews_service_1 = __webpack_require__(12);
var _t_service_1 = __webpack_require__(3);
angular.module("widgets")
    .service("ncmViewsService", ncmViews_service_1.NcmViewsService)
    .service("ncmNavigationService", ncmNavigation_service_1.NcmNavigationService)
    .service("ncmPolicyWidgetsService", ncmPolicyWidgets_service_1.NcmPolicyWidgetsService)
    .service("ncmSecurityPoliciesService", ncmSecurityPolicies_service_1.NcmSecurityPoliciesService)
    .service("_t", _t_service_1.default)
    .controller("ncmPolicyChangesWidget", policyChangesWidget_controller_1.PolicyChangesWidget);


/***/ }),

/***/ 203:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 204:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_1 = __webpack_require__(4);
var ncmPolicyWidgets_service_1 = __webpack_require__(1);
var ncmSecurityPolicies_service_1 = __webpack_require__(0);
var securityPolicy_1 = __webpack_require__(2);
var ncmNavigation_service_1 = __webpack_require__(8);
var configDiffPageSettings_1 = __webpack_require__(15);
var PolicyChangesWidget = /** @class */ (function () {
    /** @ngInject */
    PolicyChangesWidget.$inject = ["ncmPolicyWidgetsService", "ncmSecurityPoliciesService", "ncmNavigationService", "$filter", "_t"];
    function PolicyChangesWidget(ncmPolicyWidgetsService, ncmSecurityPoliciesService, ncmNavigationService, $filter, _t) {
        var _this = this;
        this.ncmPolicyWidgetsService = ncmPolicyWidgetsService;
        this.ncmSecurityPoliciesService = ncmSecurityPoliciesService;
        this.ncmNavigationService = ncmNavigationService;
        this.$filter = $filter;
        this._t = _t;
        this.viewDiffString = this._t("View diff");
        this.viewLocalDiffString = this._t("View local diff");
        this.viewPanoramaDiffString = this._t("View panorama diff");
        this.policyChanges = [];
        this.gridPagination = {
            page: 1,
            pageSize: 10
        };
        this.gridSorting = {
            sortableColumns: [
                { id: "changeTime", label: this._t("Date") }
            ],
            sortBy: {
                id: "changeTime",
                label: this._t("Date")
            },
            direction: "desc"
        };
        this.gridOptions = {
            searchableColumns: ["searchString"],
            searchPlaceholder: this._t("Search...")
        };
        this.additionalCommandKey = "";
        this.getYearDisplayFormat = function (changeTime) {
            var optionsYear = { year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" };
            return new Date(changeTime).toLocaleDateString(undefined, optionsYear);
        };
        this.displayTwoDiff = function (policyChange) {
            return policyChange.containsPolicyRawOrLocalObjectsChanges && policyChange.containsPanoramaObjectsChanges
                && policyChange.securityPolicyChangeSource === securityPolicy_1.SecurityPolicyChangeSource.Local;
        };
        this.getViewDiffUrl = function (policyChange) {
            if (!policyChange.containsPanoramaObjectsChanges && policyChange.securityPolicyChangeSource === securityPolicy_1.SecurityPolicyChangeSource.Local) {
                return _this.getLocalDiff(policyChange);
            }
            else if (policyChange.containsPanoramaObjectsChanges && policyChange.securityPolicyChangeSource === securityPolicy_1.SecurityPolicyChangeSource.Local) {
                return _this.getPanoramaDiff(policyChange);
            }
            else if (policyChange.securityPolicyChangeSource === securityPolicy_1.SecurityPolicyChangeSource.Pushed) {
                return _this.getPanoramaDiff(policyChange);
            }
        };
        this.getPanoramaDiff = function (policyChange) {
            /* tslint:disable:no-bitwise */
            return _this.ncmNavigationService.getConfigDiffUrl(policyChange.currentConfigId, policyChange.modifiedConfigId, configDiffPageSettings_1.ConfigDiffPageSettings.Pushed | configDiffPageSettings_1.ConfigDiffPageSettings.AdditionalCommands, _this.additionalCommandKey);
            /* tslint:enable:no-bitwise */
        };
        this.getLocalDiff = function (policyChange) {
            return _this.ncmNavigationService.getConfigDiffUrl(policyChange.currentConfigId, policyChange.modifiedConfigId, configDiffPageSettings_1.ConfigDiffPageSettings.None, _this.additionalCommandKey);
        };
    }
    PolicyChangesWidget.prototype.onLoad = function (config) {
        var _this = this;
        this.config = config;
        this.config.isBusy(true);
        var nodeId = this.ncmPolicyWidgetsService.getNodeIdFromUrl();
        var policyName = this.ncmPolicyWidgetsService.getPolicyNameFromUrl();
        this.ncmSecurityPoliciesService.getSecurityPolicyChanges(nodeId, policyName).then(function (model) {
            _this.policyChanges = model.changes;
            _this.additionalCommandKey = model.additionalCommandForPushed;
            _this.collectSearchData();
            _this.config.isBusy(false);
        });
    };
    ;
    PolicyChangesWidget.prototype.collectSearchData = function () {
        var concatenatedTitleString = "";
        this.policyChanges.forEach(function (change) {
            change.changesInfo.forEach(function (info) {
                concatenatedTitleString += info.title;
            });
            change.searchString = concatenatedTitleString;
            concatenatedTitleString = "";
        });
    };
    PolicyChangesWidget = __decorate([
        widgets_1.Widget({
            selector: "ncmPolicyChangesWidget",
            template: __webpack_require__(205),
            controllerAs: "vm",
            settingName: "ncmPolicyChangesWidgetSettings"
        }),
        __metadata("design:paramtypes", [ncmPolicyWidgets_service_1.NcmPolicyWidgetsService,
            ncmSecurityPolicies_service_1.NcmSecurityPoliciesService,
            ncmNavigation_service_1.NcmNavigationService, Function, Function])
    ], PolicyChangesWidget);
    return PolicyChangesWidget;
}());
exports.PolicyChangesWidget = PolicyChangesWidget;


/***/ }),

/***/ 205:
/***/ (function(module, exports) {

module.exports = "<div class=ncm-policy-changes-widget> <xui-grid items-source=vm.policyChanges smart-mode=true pagination-data=vm.gridPagination sorting-data=vm.gridSorting options=vm.gridOptions template-url=policy-change-template hide-toolbar=true controller=vm> </xui-grid> <script type=text/ng-template id=policy-change-template> <div class=\"row\">\n            <div class=\"col-md-4 policy-change-title\">\n                <div ng-repeat=\"change in item.changesInfo\" class=\"policy-change-row\">\n                    <b>{{change.title}}</b>\n                    <div class=\"xui-help-hint\" ng-if=\"change.subTitle !== null\">{{change.subTitle}}</div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                {{vm.getYearDisplayFormat(item.changeTime)}}\n            </div>\n            <div class=\"col-md-5\">\n                <a ng-if=!vm.displayTwoDiff(item) href=\"{{vm.getViewDiffUrl(item)}}\">{{vm.viewDiffString}} &#0187;</a>\n                    <div ng-if=vm.displayTwoDiff(item)>\n                        <a href=\"{{vm.getLocalDiff(item)}}\">{{vm.viewLocalDiffString}} &#0187;</a> \n                        <br><a href=\"{{vm.getPanoramaDiff(item)}}\">{{vm.viewPanoramaDiffString}} &#0187;</a>\n                    </div>\n            </div>\n        </div> </script> </div>";

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
_tService.$inject = ["getTextService"];
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable  */
/** @ngInject */
function _tService(getTextService) {
    return function (input) {
        return getTextService(input);
    };
}
exports.default = _tService;


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = Widgets;

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DiffConsts;
(function (DiffConsts) {
    DiffConsts.LEFT_CONFIG_ID_PARAM_NAME = "leftConfigId";
    DiffConsts.RIGHT_CONFIG_ID_PARAM_NAME = "rightConfigId";
    DiffConsts.SETTINGS_PARAM_NAME = "settings";
    DiffConsts.ADDITIONAL_COMMAND_KEY = "additional_command_key";
})(DiffConsts = exports.DiffConsts || (exports.DiffConsts = {}));


/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var diffConsts_1 = __webpack_require__(7);
var NcmNavigationService = /** @class */ (function () {
    /** @ngInject */
    NcmNavigationService.$inject = ["$state", "$window", "swApi", "ncmViewsService"];
    function NcmNavigationService($state, $window, swApi, ncmViewsService) {
        var _this = this;
        this.$state = $state;
        this.$window = $window;
        this.swApi = swApi;
        this.ncmViewsService = ncmViewsService;
        this.getHelpUrl = function (helpUrlFragment) {
            return _this.swApi
                .api(false)
                .one("ncm/help/" + helpUrlFragment)
                .get();
        };
        this.getAclDetailsUrl = function (params) {
            if (params) {
                var encodedAclName = encodeURIComponent(params.aclName);
                return "/ui/ncm/aclRules/" + params.nodeId + "/" + params.configId + "?aclName=" + encodedAclName;
            }
        };
        this.getCompareAclUrl = function (params) {
            if (params) {
                var urlParams = "?leftNodeId=" + params.leftNodeId +
                    ("&leftConfigId=" + params.leftConfigId) +
                    ("&leftAclName=" + encodeURIComponent(params.leftAclName)) +
                    ("&rightNodeId=" + params.rightNodeId) +
                    ("&rightConfigId=" + params.rightConfigId) +
                    ("&rightAclName=" + encodeURIComponent(params.rightAclName));
                return "/ui/ncm/aclRulesDiff/" + urlParams;
            }
        };
        this.getNodeDetailsUrl = function (nodeId, viewId) {
            if (viewId != null) {
                return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId + "&ViewID=" + viewId;
            }
            return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId;
        };
        this.getAccessListsUrl = function (nodeId) {
            return _this.ncmViewsService.getAccessListsSubviewId().then(function (viewId) {
                return "/ui/ncm/accessListsSubview?ViewID=" + viewId + "&NetObject=N:" + nodeId;
            });
        };
        this.getConfigsSubViewUrl = function (nodeId) {
            return _this.ncmViewsService.getConfigsSubviewId().then(function (viewId) {
                if (viewId != null) {
                    return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId + "&ViewID=" + viewId;
                }
                return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId;
            });
        };
        this.getManageNodesUrl = function () {
            return "/Orion/Nodes/Default.aspx";
        };
        this.getAddNodeToOrionUrl = function () {
            return "/Orion/Nodes/Add/Default.aspx";
        };
        this.navigateToUrl = function (url) {
            if (url) {
                _this.$window.open(url, "_blank");
            }
        };
        this.navigateToAddEditBaseline = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("editBaseline", params);
        };
        this.navigateToBaselineManagement = function () {
            _this.$state.go("baselineManagement");
        };
        this.navigateToAclDetails = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("aclRules", params);
        };
        this.navigateToCompareAcl = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("aclRulesDiff", params);
        };
        this.navigateToCompareGroups = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("groupObjDiff", params);
        };
        this.navigateToNodeDetails = function (nodeId) {
            _this.$window.open(_this.getNodeDetailsUrl(nodeId), "_self");
        };
        this.getComparisonCriteriaUrl = function () {
            return "/Orion/NCM/Admin/Settings/ComparisonCriteria.aspx";
        };
        this.getBaselineDiffUrl = function (baselineId, configId) {
            return "/ui/ncm/baselineDiff/" + baselineId + "/" + configId;
        };
        this.getConfigDiffUrl = function (leftConfigId, rightConfigId, settings, additionalCommandKey) {
            var result = "/ui/ncm/configDiff?" + diffConsts_1.DiffConsts.LEFT_CONFIG_ID_PARAM_NAME + "=" + leftConfigId + "&" + diffConsts_1.DiffConsts.RIGHT_CONFIG_ID_PARAM_NAME + "=" + rightConfigId;
            if (settings != null) {
                result += "&" + diffConsts_1.DiffConsts.SETTINGS_PARAM_NAME + "=" + settings;
            }
            if (additionalCommandKey != null) {
                result += "&" + diffConsts_1.DiffConsts.ADDITIONAL_COMMAND_KEY + "=" + additionalCommandKey;
            }
            return result;
        };
    }
    NcmNavigationService.prototype.getEditBaselineUrl = function (baselineId) {
        return "/ui/ncm/editBaseline?baselineId=" + baselineId;
    };
    NcmNavigationService.prototype.getSecurityPolicyDetailsUrl = function (nodeId, policyName) {
        return "/Orion/NCM/SecurityPolicyDetails.aspx?NodeId=" + nodeId + "&PolicyName=" + btoa(policyName);
    };
    return NcmNavigationService;
}());
exports.NcmNavigationService = NcmNavigationService;


/***/ })

/******/ });
//# sourceMappingURL=policyChangesWidget.js.map