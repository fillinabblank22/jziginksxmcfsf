/*!
 * @solarwinds/ncm-apollo 2020.2.5-3053
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 31);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var securityPolicy_1 = __webpack_require__(2);
exports.swApiUrl = "ncm/securityPolicies";
var NcmSecurityPoliciesService = /** @class */ (function () {
    /** @ngInject */
    NcmSecurityPoliciesService.$inject = ["swApi", "_t"];
    function NcmSecurityPoliciesService(swApi, _t) {
        var _this = this;
        this.swApi = swApi;
        this._t = _t;
        this.policyOriginNamesMap = {};
        this.getSecurityPolicies = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicy = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyChanges = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/changeHistory/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyNodes = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/similarPolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyApplication = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/applicationPolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyAddresses = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/addresses/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyServices = function (nodeId, policyName) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + policyName + "/servicePolicies/nodes/" + nodeId)
                .get();
        };
        this.getSecurityPolicyOriginName = function (policyOrigin) {
            return _this.policyOriginNamesMap[policyOrigin];
        };
        this.initPolicyOriginNameDictionary = function () {
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.Local] = _this._t("Local");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.PostPanorama] = _this._t("Post policy (Panorama)");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.PrePanorama] = _this._t("Pre policy (Panorama)");
            _this.policyOriginNamesMap[securityPolicy_1.SecurityPolicyOrigin.Unknown] = _this._t("Unknown");
        };
        this.initPolicyOriginNameDictionary();
    }
    NcmSecurityPoliciesService.prototype.getSecurityPolicyText = function (nodeId, policyName) {
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + policyName + "/text/nodes/" + nodeId)
            .get();
    };
    return NcmSecurityPoliciesService;
}());
exports.NcmSecurityPoliciesService = NcmSecurityPoliciesService;


/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var SecurityPolicyAction;
(function (SecurityPolicyAction) {
    SecurityPolicyAction[SecurityPolicyAction["Allow"] = 0] = "Allow";
    SecurityPolicyAction[SecurityPolicyAction["Deny"] = 1] = "Deny";
    SecurityPolicyAction[SecurityPolicyAction["Drop"] = 2] = "Drop";
    SecurityPolicyAction[SecurityPolicyAction["ResetClient"] = 3] = "ResetClient";
    SecurityPolicyAction[SecurityPolicyAction["ResetServer"] = 4] = "ResetServer";
    SecurityPolicyAction[SecurityPolicyAction["ResetBothClientAndServer"] = 5] = "ResetBothClientAndServer";
})(SecurityPolicyAction = exports.SecurityPolicyAction || (exports.SecurityPolicyAction = {}));
var PolicyAddressType;
(function (PolicyAddressType) {
    PolicyAddressType[PolicyAddressType["Source"] = 0] = "Source";
    PolicyAddressType[PolicyAddressType["Destination"] = 1] = "Destination";
})(PolicyAddressType = exports.PolicyAddressType || (exports.PolicyAddressType = {}));
var PolicyType;
(function (PolicyType) {
    PolicyType[PolicyType["universal"] = 0] = "universal";
    PolicyType[PolicyType["intrazone"] = 1] = "intrazone";
    PolicyType[PolicyType["interzone"] = 2] = "interzone";
})(PolicyType = exports.PolicyType || (exports.PolicyType = {}));
var SecurityPolicyOrigin;
(function (SecurityPolicyOrigin) {
    SecurityPolicyOrigin[SecurityPolicyOrigin["Unknown"] = 0] = "Unknown";
    SecurityPolicyOrigin[SecurityPolicyOrigin["PrePanorama"] = 1] = "PrePanorama";
    SecurityPolicyOrigin[SecurityPolicyOrigin["Local"] = 2] = "Local";
    SecurityPolicyOrigin[SecurityPolicyOrigin["PostPanorama"] = 3] = "PostPanorama";
})(SecurityPolicyOrigin = exports.SecurityPolicyOrigin || (exports.SecurityPolicyOrigin = {}));
var SecurityPolicyChangeSource;
(function (SecurityPolicyChangeSource) {
    SecurityPolicyChangeSource[SecurityPolicyChangeSource["Local"] = 0] = "Local";
    SecurityPolicyChangeSource[SecurityPolicyChangeSource["Pushed"] = 1] = "Pushed";
})(SecurityPolicyChangeSource = exports.SecurityPolicyChangeSource || (exports.SecurityPolicyChangeSource = {}));
var SecurityPolicyLight = /** @class */ (function () {
    function SecurityPolicyLight() {
    }
    return SecurityPolicyLight;
}());
exports.SecurityPolicyLight = SecurityPolicyLight;
var SecurityPolicyFilter = /** @class */ (function () {
    function SecurityPolicyFilter() {
    }
    return SecurityPolicyFilter;
}());
exports.SecurityPolicyFilter = SecurityPolicyFilter;
var SecurityPolicy = /** @class */ (function (_super) {
    __extends(SecurityPolicy, _super);
    function SecurityPolicy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SecurityPolicy;
}(SecurityPolicyLight));
exports.SecurityPolicy = SecurityPolicy;
var SecurityPolicyChangeInfo = /** @class */ (function () {
    function SecurityPolicyChangeInfo() {
    }
    return SecurityPolicyChangeInfo;
}());
exports.SecurityPolicyChangeInfo = SecurityPolicyChangeInfo;
var SecurityPolicyChange = /** @class */ (function () {
    function SecurityPolicyChange() {
    }
    return SecurityPolicyChange;
}());
exports.SecurityPolicyChange = SecurityPolicyChange;
var SecurityPolicyChangesModel = /** @class */ (function () {
    function SecurityPolicyChangesModel() {
    }
    return SecurityPolicyChangesModel;
}());
exports.SecurityPolicyChangesModel = SecurityPolicyChangesModel;
var SecurityPolicyNode = /** @class */ (function () {
    function SecurityPolicyNode() {
    }
    return SecurityPolicyNode;
}());
exports.SecurityPolicyNode = SecurityPolicyNode;
var ApplicationPolicy = /** @class */ (function () {
    function ApplicationPolicy() {
    }
    return ApplicationPolicy;
}());
exports.ApplicationPolicy = ApplicationPolicy;
var ServicePolicy = /** @class */ (function () {
    function ServicePolicy() {
    }
    return ServicePolicy;
}());
exports.ServicePolicy = ServicePolicy;
var SecurityPolicyAddress = /** @class */ (function () {
    function SecurityPolicyAddress() {
    }
    return SecurityPolicyAddress;
}());
exports.SecurityPolicyAddress = SecurityPolicyAddress;
var TokenType;
(function (TokenType) {
    TokenType[TokenType["Unknown"] = 0] = "Unknown";
    TokenType[TokenType["Action"] = 1] = "Action";
    TokenType[TokenType["SourceZone"] = 2] = "SourceZone";
    TokenType[TokenType["DestinationZone"] = 3] = "DestinationZone";
    TokenType[TokenType["PolicyOrigin"] = 4] = "PolicyOrigin";
})(TokenType = exports.TokenType || (exports.TokenType = {}));
var Token = /** @class */ (function () {
    function Token() {
    }
    return Token;
}());
exports.Token = Token;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
_tService.$inject = ["getTextService"];
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable  */
/** @ngInject */
function _tService(getTextService) {
    return function (input) {
        return getTextService(input);
    };
}
exports.default = _tService;


/***/ }),
/* 4 */,
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AccessControlListRule = /** @class */ (function () {
    function AccessControlListRule() {
    }
    return AccessControlListRule;
}());
exports.AccessControlListRule = AccessControlListRule;
var ShadowRuleDetectionResult = /** @class */ (function () {
    function ShadowRuleDetectionResult() {
    }
    return ShadowRuleDetectionResult;
}());
exports.ShadowRuleDetectionResult = ShadowRuleDetectionResult;
var RuleCheckDetectionType;
(function (RuleCheckDetectionType) {
    RuleCheckDetectionType[RuleCheckDetectionType["Unique"] = 0] = "Unique";
    RuleCheckDetectionType[RuleCheckDetectionType["Shadow"] = 1] = "Shadow";
    RuleCheckDetectionType[RuleCheckDetectionType["PartiallyShadow"] = 2] = "PartiallyShadow";
    RuleCheckDetectionType[RuleCheckDetectionType["Redundant"] = 3] = "Redundant";
    RuleCheckDetectionType[RuleCheckDetectionType["PartiallyRedundant"] = 4] = "PartiallyRedundant";
})(RuleCheckDetectionType = exports.RuleCheckDetectionType || (exports.RuleCheckDetectionType = {}));
var AceTokenType;
(function (AceTokenType) {
    AceTokenType[AceTokenType["Unknown"] = 0] = "Unknown";
    AceTokenType[AceTokenType["SourceAddress"] = 1] = "SourceAddress";
    AceTokenType[AceTokenType["DestinationAddress"] = 2] = "DestinationAddress";
    AceTokenType[AceTokenType["SourcePort"] = 4] = "SourcePort";
    AceTokenType[AceTokenType["DestinationPort"] = 8] = "DestinationPort";
    AceTokenType[AceTokenType["Object"] = 16] = "Object";
    AceTokenType[AceTokenType["ObjectGroup"] = 32] = "ObjectGroup";
    AceTokenType[AceTokenType["Interface"] = 64] = "Interface";
    AceTokenType[AceTokenType["Protocol"] = 128] = "Protocol";
    AceTokenType[AceTokenType["Keyword"] = 256] = "Keyword";
    AceTokenType[AceTokenType["TimeRange"] = 512] = "TimeRange";
    AceTokenType[AceTokenType["Action"] = 1024] = "Action";
    AceTokenType[AceTokenType["Url"] = 2048] = "Url";
    AceTokenType[AceTokenType["LogLevel"] = 4096] = "LogLevel";
    AceTokenType[AceTokenType["Interval"] = 8192] = "Interval";
})(AceTokenType = exports.AceTokenType || (exports.AceTokenType = {}));
var AceToken = /** @class */ (function () {
    function AceToken() {
    }
    return AceToken;
}());
exports.AceToken = AceToken;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.toLocalDateString = function (date) {
        var options;
        if (new Date().getFullYear() === new Date(date).getFullYear()) {
            options = { month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" };
        }
        else {
            options = { year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" };
        }
        return new Date(date).toLocaleDateString(undefined, options);
    };
    return Utils;
}());
exports.default = Utils;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DiffConsts;
(function (DiffConsts) {
    DiffConsts.LEFT_CONFIG_ID_PARAM_NAME = "leftConfigId";
    DiffConsts.RIGHT_CONFIG_ID_PARAM_NAME = "rightConfigId";
    DiffConsts.SETTINGS_PARAM_NAME = "settings";
    DiffConsts.ADDITIONAL_COMMAND_KEY = "additional_command_key";
})(DiffConsts = exports.DiffConsts || (exports.DiffConsts = {}));


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var diffConsts_1 = __webpack_require__(7);
var NcmNavigationService = /** @class */ (function () {
    /** @ngInject */
    NcmNavigationService.$inject = ["$state", "$window", "swApi", "ncmViewsService"];
    function NcmNavigationService($state, $window, swApi, ncmViewsService) {
        var _this = this;
        this.$state = $state;
        this.$window = $window;
        this.swApi = swApi;
        this.ncmViewsService = ncmViewsService;
        this.getHelpUrl = function (helpUrlFragment) {
            return _this.swApi
                .api(false)
                .one("ncm/help/" + helpUrlFragment)
                .get();
        };
        this.getAclDetailsUrl = function (params) {
            if (params) {
                var encodedAclName = encodeURIComponent(params.aclName);
                return "/ui/ncm/aclRules/" + params.nodeId + "/" + params.configId + "?aclName=" + encodedAclName;
            }
        };
        this.getCompareAclUrl = function (params) {
            if (params) {
                var urlParams = "?leftNodeId=" + params.leftNodeId +
                    ("&leftConfigId=" + params.leftConfigId) +
                    ("&leftAclName=" + encodeURIComponent(params.leftAclName)) +
                    ("&rightNodeId=" + params.rightNodeId) +
                    ("&rightConfigId=" + params.rightConfigId) +
                    ("&rightAclName=" + encodeURIComponent(params.rightAclName));
                return "/ui/ncm/aclRulesDiff/" + urlParams;
            }
        };
        this.getNodeDetailsUrl = function (nodeId, viewId) {
            if (viewId != null) {
                return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId + "&ViewID=" + viewId;
            }
            return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId;
        };
        this.getAccessListsUrl = function (nodeId) {
            return _this.ncmViewsService.getAccessListsSubviewId().then(function (viewId) {
                return "/ui/ncm/accessListsSubview?ViewID=" + viewId + "&NetObject=N:" + nodeId;
            });
        };
        this.getConfigsSubViewUrl = function (nodeId) {
            return _this.ncmViewsService.getConfigsSubviewId().then(function (viewId) {
                if (viewId != null) {
                    return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId + "&ViewID=" + viewId;
                }
                return "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + nodeId;
            });
        };
        this.getManageNodesUrl = function () {
            return "/Orion/Nodes/Default.aspx";
        };
        this.getAddNodeToOrionUrl = function () {
            return "/Orion/Nodes/Add/Default.aspx";
        };
        this.navigateToUrl = function (url) {
            if (url) {
                _this.$window.open(url, "_blank");
            }
        };
        this.navigateToAddEditBaseline = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("editBaseline", params);
        };
        this.navigateToBaselineManagement = function () {
            _this.$state.go("baselineManagement");
        };
        this.navigateToAclDetails = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("aclRules", params);
        };
        this.navigateToCompareAcl = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("aclRulesDiff", params);
        };
        this.navigateToCompareGroups = function (params) {
            if (params === void 0) { params = null; }
            _this.$state.go("groupObjDiff", params);
        };
        this.navigateToNodeDetails = function (nodeId) {
            _this.$window.open(_this.getNodeDetailsUrl(nodeId), "_self");
        };
        this.getComparisonCriteriaUrl = function () {
            return "/Orion/NCM/Admin/Settings/ComparisonCriteria.aspx";
        };
        this.getBaselineDiffUrl = function (baselineId, configId) {
            return "/ui/ncm/baselineDiff/" + baselineId + "/" + configId;
        };
        this.getConfigDiffUrl = function (leftConfigId, rightConfigId, settings, additionalCommandKey) {
            var result = "/ui/ncm/configDiff?" + diffConsts_1.DiffConsts.LEFT_CONFIG_ID_PARAM_NAME + "=" + leftConfigId + "&" + diffConsts_1.DiffConsts.RIGHT_CONFIG_ID_PARAM_NAME + "=" + rightConfigId;
            if (settings != null) {
                result += "&" + diffConsts_1.DiffConsts.SETTINGS_PARAM_NAME + "=" + settings;
            }
            if (additionalCommandKey != null) {
                result += "&" + diffConsts_1.DiffConsts.ADDITIONAL_COMMAND_KEY + "=" + additionalCommandKey;
            }
            return result;
        };
    }
    NcmNavigationService.prototype.getEditBaselineUrl = function (baselineId) {
        return "/ui/ncm/editBaseline?baselineId=" + baselineId;
    };
    NcmNavigationService.prototype.getSecurityPolicyDetailsUrl = function (nodeId, policyName) {
        return "/Orion/NCM/SecurityPolicyDetails.aspx?NodeId=" + nodeId + "&PolicyName=" + btoa(policyName);
    };
    return NcmNavigationService;
}());
exports.NcmNavigationService = NcmNavigationService;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Restrict;
(function (Restrict) {
    Restrict.element = "E";
    Restrict.attribute = "A";
    Restrict.className = "C";
    Restrict.comment = "M";
})(Restrict = exports.Restrict || (exports.Restrict = {}));
var BindingScope;
(function (BindingScope) {
    BindingScope.method = "&";
    BindingScope.twoWay = "=";
    BindingScope.oneWay = "@";
    BindingScope.optional = "?";
    BindingScope.oneWayOptional = BindingScope.oneWay + BindingScope.optional;
    BindingScope.twoWayOptional = BindingScope.twoWay + BindingScope.optional;
    BindingScope.methodOptional = BindingScope.method + BindingScope.optional;
})(BindingScope = exports.BindingScope || (exports.BindingScope = {}));


/***/ }),
/* 10 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var BaselineConfigLight = /** @class */ (function () {
    function BaselineConfigLight(baselineId, baselineName, baselineDescription, baselineViolationStatus, nodesCount, nodeName, nodeDetailsUrl, disabled) {
        this.baselineId = baselineId;
        this.baselineName = baselineName;
        this.baselineDescription = baselineDescription;
        this.nodesCount = nodesCount;
        this.nodeName = nodeName;
        this.nodeDetailsUrl = nodeDetailsUrl;
        this.disabled = disabled;
    }
    return BaselineConfigLight;
}());
exports.BaselineConfigLight = BaselineConfigLight;
var BaselineNodeAssigment = /** @class */ (function () {
    function BaselineNodeAssigment() {
    }
    return BaselineNodeAssigment;
}());
exports.BaselineNodeAssigment = BaselineNodeAssigment;
var BaselineConfigLine = /** @class */ (function () {
    function BaselineConfigLine() {
    }
    return BaselineConfigLine;
}());
exports.BaselineConfigLine = BaselineConfigLine;
var BaselineConfig = /** @class */ (function () {
    function BaselineConfig(baselineId, baselineName, baselineDescription, exactMatching, useComparisonCriterias, lines, selectedNodes, selectedConfigTypes, disabled) {
        if (baselineId === void 0) { baselineId = 0; }
        if (baselineName === void 0) { baselineName = ""; }
        if (baselineDescription === void 0) { baselineDescription = ""; }
        if (exactMatching === void 0) { exactMatching = false; }
        if (useComparisonCriterias === void 0) { useComparisonCriterias = false; }
        if (lines === void 0) { lines = []; }
        if (selectedNodes === void 0) { selectedNodes = []; }
        if (selectedConfigTypes === void 0) { selectedConfigTypes = []; }
        if (disabled === void 0) { disabled = false; }
        this.baselineId = baselineId;
        this.baselineName = baselineName;
        this.baselineDescription = baselineDescription;
        this.exactMatching = exactMatching;
        this.useComparisonCriterias = useComparisonCriterias;
        this.lines = lines;
        this.selectedNodes = selectedNodes;
        this.selectedConfigTypes = selectedConfigTypes;
        this.disabled = disabled;
    }
    return BaselineConfig;
}());
exports.BaselineConfig = BaselineConfig;
var BaselineConfigType = /** @class */ (function () {
    function BaselineConfigType() {
    }
    return BaselineConfigType;
}());
exports.BaselineConfigType = BaselineConfigType;
var BaselineViolation = /** @class */ (function () {
    function BaselineViolation() {
    }
    return BaselineViolation;
}());
exports.BaselineViolation = BaselineViolation;
var BaselineResult = /** @class */ (function () {
    function BaselineResult() {
    }
    return BaselineResult;
}());
exports.BaselineResult = BaselineResult;
var BaselineState = /** @class */ (function () {
    function BaselineState() {
    }
    return BaselineState;
}());
exports.BaselineState = BaselineState;
var BaselineViolationState;
(function (BaselineViolationState) {
    BaselineViolationState[BaselineViolationState["Unknown"] = 0] = "Unknown";
    BaselineViolationState[BaselineViolationState["NoIssues"] = 1] = "NoIssues";
    BaselineViolationState[BaselineViolationState["Mismatches"] = 2] = "Mismatches";
    BaselineViolationState[BaselineViolationState["Updating"] = 3] = "Updating";
    BaselineViolationState[BaselineViolationState["Error"] = 4] = "Error";
})(BaselineViolationState = exports.BaselineViolationState || (exports.BaselineViolationState = {}));


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmViewsService = /** @class */ (function () {
    /** @ngInject */
    NcmViewsService.$inject = ["swisService"];
    function NcmViewsService(swisService) {
        this.swisService = swisService;
    }
    NcmViewsService.prototype.getAccessListsSubviewId = function () {
        return this.getViewId("NCMAccessLists");
    };
    NcmViewsService.prototype.getAsaInterfacesSubviewId = function () {
        return this.getViewId("ASA Interfaces Subview");
    };
    NcmViewsService.prototype.getConfigsSubviewId = function () {
        return this.getViewId("Configs SubView");
    };
    NcmViewsService.prototype.getViewId = function (viewName) {
        return this.swisService.query("SELECT ViewID FROM Orion.Views WHERE ViewKey=@viewKey", { viewKey: viewName })
            .then(function (results) {
            if (results.rows.length > 0) {
                return results.rows[0].ViewID;
            }
            else {
                return null;
            }
        });
    };
    return NcmViewsService;
}());
exports.NcmViewsService = NcmViewsService;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ObjectDefinitionBase = /** @class */ (function () {
    function ObjectDefinitionBase() {
    }
    return ObjectDefinitionBase;
}());
exports.ObjectDefinitionBase = ObjectDefinitionBase;
var ObjectDefinitionReferenceType;
(function (ObjectDefinitionReferenceType) {
    ObjectDefinitionReferenceType[ObjectDefinitionReferenceType["ObjectGroup"] = 0] = "ObjectGroup";
    ObjectDefinitionReferenceType[ObjectDefinitionReferenceType["Object"] = 1] = "Object";
})(ObjectDefinitionReferenceType = exports.ObjectDefinitionReferenceType || (exports.ObjectDefinitionReferenceType = {}));
var ObjectGroupMember = /** @class */ (function () {
    function ObjectGroupMember() {
    }
    return ObjectGroupMember;
}());
exports.ObjectGroupMember = ObjectGroupMember;
var ObjectGroup = /** @class */ (function (_super) {
    __extends(ObjectGroup, _super);
    function ObjectGroup() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ObjectGroup;
}(ObjectDefinitionBase));
exports.ObjectGroup = ObjectGroup;
var ObjectDefinition = /** @class */ (function (_super) {
    __extends(ObjectDefinition, _super);
    function ObjectDefinition() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ObjectDefinition;
}(ObjectDefinitionBase));
exports.ObjectDefinition = ObjectDefinition;
var AclEntityProperties = /** @class */ (function () {
    function AclEntityProperties() {
    }
    return AclEntityProperties;
}());
exports.AclEntityProperties = AclEntityProperties;
var AclEntityHistory = /** @class */ (function () {
    function AclEntityHistory() {
    }
    return AclEntityHistory;
}());
exports.AclEntityHistory = AclEntityHistory;
var GroupDiff = /** @class */ (function () {
    function GroupDiff() {
    }
    return GroupDiff;
}());
exports.GroupDiff = GroupDiff;
var ObjectGroupDiff = /** @class */ (function () {
    function ObjectGroupDiff() {
    }
    return ObjectGroupDiff;
}());
exports.ObjectGroupDiff = ObjectGroupDiff;
var ObjectGroupDialogModel = /** @class */ (function () {
    function ObjectGroupDialogModel(left, right) {
        this.left = left;
        this.right = right;
    }
    return ObjectGroupDialogModel;
}());
exports.ObjectGroupDialogModel = ObjectGroupDialogModel;
var ObjectGroupDialogInfo = /** @class */ (function () {
    function ObjectGroupDialogInfo(itemsSource, selectedItem) {
        this.itemsSource = itemsSource;
        this.selectedItem = selectedItem;
    }
    return ObjectGroupDialogInfo;
}());
exports.ObjectGroupDialogInfo = ObjectGroupDialogInfo;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Require;
(function (Require) {
    function notNull(obj, paramName) {
        if (obj == null) {
            throw new ReferenceError("The parameter '" + paramName + "' is null or undefined.");
        }
    }
    Require.notNull = notNull;
    function notEmptyArray(array, paramName) {
        notNull(array, paramName);
        if (array.length === 0) {
            throw new RangeError("The array '" + paramName + "' doesn't have items.");
        }
    }
    Require.notEmptyArray = notEmptyArray;
    function notEmptyString(str, paramName) {
        notNull(str, paramName);
        if (str.trim() === "") {
            throw new RangeError("The parameter '" + paramName + "' is empty.");
        }
    }
    Require.notEmptyString = notEmptyString;
})(Require = exports.Require || (exports.Require = {}));


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ConfigDiffPageSettings;
(function (ConfigDiffPageSettings) {
    ConfigDiffPageSettings[ConfigDiffPageSettings["None"] = 0] = "None";
    ConfigDiffPageSettings[ConfigDiffPageSettings["Selection"] = 1] = "Selection";
    ConfigDiffPageSettings[ConfigDiffPageSettings["AdditionalCommands"] = 2] = "AdditionalCommands";
    ConfigDiffPageSettings[ConfigDiffPageSettings["Pushed"] = 4] = "Pushed";
})(ConfigDiffPageSettings = exports.ConfigDiffPageSettings || (exports.ConfigDiffPageSettings = {}));


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AceDiff = /** @class */ (function () {
    function AceDiff() {
    }
    return AceDiff;
}());
exports.AceDiff = AceDiff;
var AceLine = /** @class */ (function () {
    function AceLine() {
    }
    return AceLine;
}());
exports.AceLine = AceLine;
var DiffType;
(function (DiffType) {
    DiffType[DiffType["Unknown"] = 0] = "Unknown";
    DiffType[DiffType["Unchanged"] = 1] = "Unchanged";
    DiffType[DiffType["Changed"] = 2] = "Changed";
    DiffType[DiffType["Added"] = 3] = "Added";
    DiffType[DiffType["Deleted"] = 4] = "Deleted";
})(DiffType = exports.DiffType || (exports.DiffType = {}));
var AclDiffInfo = /** @class */ (function () {
    function AclDiffInfo() {
    }
    return AclDiffInfo;
}());
exports.AclDiffInfo = AclDiffInfo;
var AclInfo = /** @class */ (function () {
    function AclInfo() {
    }
    return AclInfo;
}());
exports.AclInfo = AclInfo;
var AclNode = /** @class */ (function () {
    function AclNode() {
    }
    return AclNode;
}());
exports.AclNode = AclNode;
//  Acl Diff Dialog section
var AclDiffDialogViewModel = /** @class */ (function () {
    function AclDiffDialogViewModel(aclDiffInfo) {
        this.aclDiffInfo = aclDiffInfo;
    }
    return AclDiffDialogViewModel;
}());
exports.AclDiffDialogViewModel = AclDiffDialogViewModel;
var AclInterface = /** @class */ (function () {
    function AclInterface() {
        this.acl = [];
    }
    return AclInterface;
}());
exports.AclInterface = AclInterface;
var AclItem = /** @class */ (function () {
    function AclItem() {
        this.history = [];
    }
    return AclItem;
}());
exports.AclItem = AclItem;
var AclItemHistory = /** @class */ (function () {
    function AclItemHistory() {
    }
    return AclItemHistory;
}());
exports.AclItemHistory = AclItemHistory;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var aclDiff_1 = __webpack_require__(16);
var utils_1 = __webpack_require__(6);
var AclPickerData = /** @class */ (function () {
    function AclPickerData() {
        this.nodes = [];
        this.interfaces = [];
        this.selectedNode = new aclDiff_1.AclNode();
        this.selectedInterface = new aclDiff_1.AclInterface();
        this.selectedAcl = new aclDiff_1.AclItem();
        this.selectedVersion = new aclDiff_1.AclItemHistory();
    }
    return AclPickerData;
}());
exports.AclPickerData = AclPickerData;
var AclPickerItemType;
(function (AclPickerItemType) {
    AclPickerItemType[AclPickerItemType["Node"] = 0] = "Node";
    AclPickerItemType[AclPickerItemType["Interface"] = 1] = "Interface";
    AclPickerItemType[AclPickerItemType["Acl"] = 2] = "Acl";
    AclPickerItemType[AclPickerItemType["Version"] = 3] = "Version";
})(AclPickerItemType = exports.AclPickerItemType || (exports.AclPickerItemType = {}));
var AclPickerController = /** @class */ (function () {
    /** @ngInject */
    AclPickerController.$inject = ["$scope", "$log", "_t"];
    function AclPickerController($scope, $log, _t) {
        this.$scope = $scope;
        this.$log = $log;
        this._t = _t;
        this.aclCaptionInterfaceString = this._t("Interface");
        this.aclCaptionAclString = this._t("Access List");
        this.aclCaptionVersionString = this._t("Version");
        this.aclSameNodeString = this._t("Same Node");
        this.aclSameInterfaceString = this._t("Same Interface");
        this.aclSameAclString = this._t("Same Acl");
        this.aclBaseNodeString = this._t("Base Node");
    }
    /*
    * Render functions
    */
    AclPickerController.prototype.renderInterface = function (item) {
        if (item.interfaceName === "") {
            return this._t("Not assigned");
        }
        else {
            return this.renderEllipsisIfNeeded(item.interfaceName);
        }
    };
    AclPickerController.prototype.renderVersion = function (item) {
        var versionTimeFormatted = utils_1.default.toLocalDateString(item.versionTime);
        return item.isLatest
            ? versionTimeFormatted + " " + this._t("Not assigned")
            : versionTimeFormatted;
    };
    AclPickerController.prototype.renderAclName = function (item) {
        if (item.aclName == null) {
            return "";
        }
        else {
            return this.renderEllipsisIfNeeded(item.aclName);
        }
    };
    AclPickerController.prototype.getSelectedInterfaceName = function () {
        if (this.itemsDataModel.selectedInterface.interfaceName === "") {
            return this._t("Not assigned");
        }
        else {
            return this.itemsDataModel.selectedInterface.interfaceName;
        }
    };
    /*
    * On Changed events
    */
    AclPickerController.prototype.onNodeChanged = function (newValue, oldValue) {
        if (_.isEqual(newValue, oldValue)) {
            return;
        }
        this.onItemTypeChanged(AclPickerItemType.Node);
        if (angular.isFunction(this.onChanged)) {
            this.onChanged({ sourceType: AclPickerItemType.Node });
        }
    };
    AclPickerController.prototype.onInterfaceChanged = function (newValue, oldValue) {
        if (_.isEqual(newValue, oldValue)) {
            return;
        }
        this.onItemTypeChanged(AclPickerItemType.Interface);
        if (angular.isFunction(this.onChanged)) {
            this.onChanged({ sourceType: AclPickerItemType.Interface });
        }
    };
    AclPickerController.prototype.onAclChanged = function (newValue, oldValue) {
        if (_.isEqual(newValue, oldValue)) {
            return;
        }
        this.onItemTypeChanged(AclPickerItemType.Acl);
        if (angular.isFunction(this.onChanged)) {
            this.onChanged({ sourceType: AclPickerItemType.Acl });
        }
    };
    AclPickerController.prototype.onVersionChanged = function (newValue, oldValue) {
        if (_.isEqual(newValue, oldValue)) {
            return;
        }
        if (angular.isFunction(this.onChanged)) {
            this.onChanged({ sourceType: AclPickerItemType.Version });
        }
    };
    AclPickerController.prototype.onItemTypeChanged = function (itemType) {
        this.adaptSelection(itemType);
        this.adjustSameValues(itemType);
    };
    AclPickerController.prototype.adaptSelection = function (itemType) {
        switch (itemType) {
            case AclPickerItemType.Interface: {
                this.itemsDataModel.selectedAcl = this.itemsDataModel.selectedInterface.acl[0];
                this.itemsDataModel.selectedVersion = this.itemsDataModel.selectedAcl.history[0];
                break;
            }
            case AclPickerItemType.Acl: {
                this.itemsDataModel.selectedVersion = this.itemsDataModel.selectedAcl.history[0];
                break;
            }
        }
    };
    AclPickerController.prototype.adjustSameValues = function (itemType) {
        switch (itemType) {
            case AclPickerItemType.Node: {
                this.itemsDataModel.isSameNode = false;
                this.itemsDataModel.isSameInterface = false;
                this.itemsDataModel.isSameAcl = false;
                break;
            }
            case AclPickerItemType.Interface: {
                this.itemsDataModel.isSameInterface = false;
                this.itemsDataModel.isSameAcl = false;
                break;
            }
            case AclPickerItemType.Acl: {
                this.itemsDataModel.isSameAcl = false;
                break;
            }
        }
    };
    AclPickerController.prototype.renderEllipsisIfNeeded = function (text) {
        if (text && text.length > 32) {
            return text.substr(0, 32) + "...";
        }
        return text;
    };
    return AclPickerController;
}());
exports.AclPickerController = AclPickerController;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VpcInterfaceLine = /** @class */ (function () {
    function VpcInterfaceLine(rawData, isInterfaceCommand, ordinalNumber) {
        this.rawData = rawData;
        this.isInterfaceCommand = isInterfaceCommand;
        this.ordinalNumber = ordinalNumber;
    }
    return VpcInterfaceLine;
}());
exports.VpcInterfaceLine = VpcInterfaceLine;
var VpcStatus;
(function (VpcStatus) {
    VpcStatus[VpcStatus["none"] = 0] = "none";
    VpcStatus[VpcStatus["ok"] = 1] = "ok";
    VpcStatus[VpcStatus["noConfig"] = 2] = "noConfig";
    VpcStatus[VpcStatus["notInNcm"] = 3] = "notInNcm";
    VpcStatus[VpcStatus["noVpcInformation"] = 4] = "noVpcInformation";
    VpcStatus[VpcStatus["notInOrion"] = 5] = "notInOrion";
})(VpcStatus = exports.VpcStatus || (exports.VpcStatus = {}));


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PasteFileDialogViewModel = /** @class */ (function () {
    function PasteFileDialogViewModel() {
    }
    return PasteFileDialogViewModel;
}());
exports.PasteFileDialogViewModel = PasteFileDialogViewModel;
var PasteFileDialogController = /** @class */ (function () {
    /** @ngInject */
    PasteFileDialogController.$inject = ["$scope", "$log", "_t"];
    function PasteFileDialogController($scope, $log, _t) {
        this.$scope = $scope;
        this.$log = $log;
        this._t = _t;
        this.pasteFileDialogText = this._t("Paste the config file you want to use as a baseline in the text box below.");
    }
    PasteFileDialogController.prototype.init = function (viewModel) {
        this.viewModel = viewModel;
        this.bindSubmitResultCallback(this.viewModel);
    };
    PasteFileDialogController.prototype.bindSubmitResultCallback = function (model) {
        var _this = this;
        model.getSubmittedResultCallback = function () {
            return _this.splitBaseline();
        };
    };
    PasteFileDialogController.prototype.splitBaseline = function () {
        if (this.baseline != null) {
            return this.baseline.split("\n");
        }
    };
    return PasteFileDialogController;
}());
exports.PasteFileDialogController = PasteFileDialogController;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SelectedNodesAndConfigTypes = /** @class */ (function () {
    function SelectedNodesAndConfigTypes() {
    }
    return SelectedNodesAndConfigTypes;
}());
exports.SelectedNodesAndConfigTypes = SelectedNodesAndConfigTypes;
var AssignNodesDialogModel = /** @class */ (function () {
    function AssignNodesDialogModel(selectedNodes, selectedConfigTypes, baselineName) {
        this.selectedNodes = selectedNodes;
        this.selectedConfigTypes = selectedConfigTypes;
        this.baselineName = baselineName;
    }
    return AssignNodesDialogModel;
}());
exports.AssignNodesDialogModel = AssignNodesDialogModel;


/***/ }),
/* 21 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 22 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ncmAccessListGrid_service_1 = __webpack_require__(24);
var utils_1 = __webpack_require__(6);
var AccessListController = /** @class */ (function () {
    /** @ngInject */
    AccessListController.$inject = ["$log", "accessListService", "ncmNavigationService", "ncmAccessListGridService", "ncmViewsService", "$sce", "$location", "$templateCache", "_t"];
    function AccessListController($log, accessListService, ncmNavigationService, ncmAccessListGridService, ncmViewsService, $sce, $location, $templateCache, _t) {
        var _this = this;
        this.$log = $log;
        this.accessListService = accessListService;
        this.ncmNavigationService = ncmNavigationService;
        this.ncmAccessListGridService = ncmAccessListGridService;
        this.ncmViewsService = ncmViewsService;
        this.$sce = $sce;
        this.$location = $location;
        this.$templateCache = $templateCache;
        this._t = _t;
        this.aclLoadingString = this._t("Loading...");
        this.itemsSource = [];
        this.isLoading = false;
        this.gridPagination = {
            page: 1,
            pageSize: 10
        };
        this.innerGridPagination = {
            page: 1,
            pageSize: 1000
        };
        this.gridOptions = {
            hideSearch: false,
            searchableColumns: ["name"],
            searchPlaceholder: this._t("Search...")
        };
        this.innerGridOptions = {
            hideSearch: true,
            hidePagination: true,
            searchPlaceholder: this._t("Search...")
        };
        this.sorting = {
            sortableColumns: [{
                    id: "name",
                    label: this._t("Name")
                }],
            sortBy: {
                id: "name",
                label: this._t("Name")
            },
            direction: "asc"
        };
        this.emptyData = {
            templateUrl: "empty-template"
        };
        this.getAclDetailsUrl = function (gridItem) {
            var params = {
                nodeId: gridItem.nodeId,
                configId: gridItem.configId,
                aclName: gridItem.name
            };
            return _this.ncmNavigationService.getAclDetailsUrl(params);
        };
        this.activate = function () {
            _this.ncmAccessListGridService.reset();
            _this.fetchAccessLists();
            _this.locateAsaIfTabUrl();
        };
        this.fetchAccessLists = function () {
            _this.isLoading = true;
            _this.accessListService.getAccessControlLists(_this.nodeId).then(function (result) {
                _this.aclHistory = result.accessLists;
                _this.ncmAccessListGridService.itemsSource = _this.getAclHistoryGridItemsSource();
                _this.ncmAccessListGridService.ifUrlsMapping = result.interfacesUrl;
                _this.itemsSource = angular.copy(_this.ncmAccessListGridService.itemsSource);
            }).finally(function () {
                _this.loadPaginationData();
                _this.isLoading = false;
            });
        };
        this.restoreSelectedOfItemsSource = function () {
            _this.ncmAccessListGridService.restoreSelectedOfItems(_this.itemsSource);
        };
        this.paginationChange = function (page, pageSize, total) {
            _this.savePaginationDataToUrl(page, pageSize);
            _this.restoreSelectedOfItemsSource();
        };
        this.savePaginationDataToUrl = function (page, pageSize) {
            _this.$location.search().pageNum = page;
            _this.$location.search().pageSize = pageSize;
            _this.$location.search(_this.$location.search());
        };
        this.loadPaginationData = function () {
            if (_this.$location.search().pageNum) {
                _this.gridPagination.page = Number(_this.$location.search().pageNum);
            }
            if (_this.$location.search().pageSize) {
                _this.gridPagination.pageSize = Number(_this.$location.search().pageSize);
            }
        };
        this.onStatusChangedOnExpander = function (isOpen, gridItem) {
            if (isOpen) {
                // innerItemsSource is passed into child grid component,
                // we need to have updated 'selected' of given elements
                _this.ncmAccessListGridService.restoreSelectedOfItems(gridItem.innerItemsSource);
            }
        };
        this.getAclHistoryGridItemsSource = function () {
            var itemsSource = new Array();
            if (_this.aclHistory === null) {
                return itemsSource;
            }
            _this.aclHistory.forEach(function (row) {
                var gridItem = new ncmAccessListGrid_service_1.GridItem();
                var currentAcl = row.history[0];
                gridItem.name = row.name;
                gridItem.nodeId = _this.nodeId;
                gridItem.configId = currentAcl.configId;
                gridItem.interfaces = currentAcl.interfaces;
                gridItem.modificationTime = currentAcl.modificationTime;
                gridItem.selected = false;
                gridItem.innerItemsSource = _this.getAclHistoryGridInnerItemsSource(row.name, row.history.slice(1));
                gridItem.srdStatistics = currentAcl.srdStatistics;
                itemsSource.push(gridItem);
            });
            return itemsSource;
        };
        this.getAclHistoryGridInnerItemsSource = function (name, history) {
            var itemsSource = new Array();
            history.forEach(function (row) {
                var gridItem = new ncmAccessListGrid_service_1.GridItem();
                gridItem.name = name;
                gridItem.nodeId = _this.nodeId;
                gridItem.configId = row.configId;
                gridItem.interfaces = row.interfaces;
                gridItem.modificationTime = row.modificationTime;
                gridItem.selected = false;
                gridItem.srdStatistics = row.srdStatistics;
                itemsSource.push(gridItem);
            });
            return itemsSource;
        };
        this.locateAsaIfTabUrl = function () {
            _this.ncmViewsService.getAsaInterfacesSubviewId()
                .then(function (id) {
                if (id != null) {
                    _this.ncmAccessListGridService.asaIfTabUrl =
                        "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:" + _this.nodeId + "&ViewID=" + id;
                }
            });
        };
        this.onCheckboxClick = function (gridItem) {
            _this.ncmAccessListGridService.selectItem(gridItem);
        };
        this.isCheckboxDisabled = function (gridItem) {
            return _this.ncmAccessListGridService.selection.length === 2 &&
                !_this.ncmAccessListGridService.isItemAlredySelected(gridItem);
        };
        this.navigateToCompareAclDisabled = function () {
            return _this.ncmAccessListGridService.selection.length !== 2;
        };
        this.navigateToAsaIfDetails = function (ifName) {
            var url = _this.getInterfaceUrl(ifName);
            if (url) {
                _this.ncmNavigationService.navigateToUrl(url);
            }
        };
        this.navigateToInterfacesTab = function () {
            _this.ncmNavigationService.navigateToUrl(_this.ncmAccessListGridService.asaIfTabUrl);
        };
        this.isInterfaceAvailable = function (ifName) {
            return _this.getInterfaceUrl(ifName) !== undefined;
        };
        this.areInterfacesEnabled = function () {
            return !_.isEmpty(_this.ncmAccessListGridService.ifUrlsMapping);
        };
        this.getInterfaceUrl = function (ifName) {
            return _this.ncmAccessListGridService.ifUrlsMapping[ifName.toLowerCase()];
        };
        this.getCompareAclUrl = function (gridItem) {
            if (_this.ncmAccessListGridService.selection.length >= 2) {
                var params = {
                    leftNodeId: _this.ncmAccessListGridService.selection[1].nodeId,
                    leftConfigId: _this.ncmAccessListGridService.selection[1].configId,
                    leftAclName: _this.ncmAccessListGridService.selection[1].name,
                    rightNodeId: _this.ncmAccessListGridService.selection[0].nodeId,
                    rightConfigId: _this.ncmAccessListGridService.selection[0].configId,
                    rightAclName: _this.ncmAccessListGridService.selection[0].name
                };
                return _this.ncmNavigationService.getCompareAclUrl(params);
            }
            return "#";
        };
        this.onSearch = function (item, cancellation) {
            _this.emptyData = {
                image: "no-search-results",
                title: _this._t("Nothing found"),
                description: _this._t("Try searching for something else")
            };
            _this.ncmAccessListGridService.searchTerm = item;
            _this.restoreSelectedOfItemsSource();
        };
        this.onSort = function () {
            _this.restoreSelectedOfItemsSource();
        };
        this.highlightSearch = function (name) {
            if (_this.ncmAccessListGridService.searchTerm) {
                var regex = _.escapeRegExp("(" + _this.ncmAccessListGridService.searchTerm + ")");
                var pattern = new RegExp(regex, "gi");
                var text = name.replace(pattern, "<span class=\"acl-search-highlight\">$1</span>");
                return _this.$sce.trustAsHtml(text);
            }
            return name;
        };
        this.$templateCache.put("empty-template", __webpack_require__(110));
    }
    Object.defineProperty(AccessListController.prototype, "getAclHistory", {
        get: function () {
            return this.aclHistory;
        },
        enumerable: true,
        configurable: true
    });
    AccessListController.prototype.toLocalDate = function (date) {
        return utils_1.default.toLocalDateString(date);
    };
    return AccessListController;
}());
exports.AccessListController = AccessListController;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var GridItem = /** @class */ (function () {
    function GridItem() {
    }
    return GridItem;
}());
exports.GridItem = GridItem;
var NcmAccessListGridService = /** @class */ (function () {
    function NcmAccessListGridService() {
        var _this = this;
        this.itemsAreEqual = function (item1, item2) {
            return item1.name === item2.name &&
                item1.nodeId === item2.nodeId &&
                item1.configId === item2.configId;
        };
        this.ifUrlsMapping = {};
        this.selectItem = function (item) {
            if (item) {
                item.selected ? _this.addItem(item) : _this.removeItem(item);
                _this.updateSelectedParent();
                _this.itemsSource
                    .reduce(function (acc, itemSource) {
                    return acc.concat(itemSource.innerItemsSource);
                }, [])
                    .filter(function (elem) {
                    return _this.itemsAreEqual(elem, item);
                })
                    .forEach(function (innerItemSource) {
                    innerItemSource.selected = item.selected;
                });
            }
            ;
        };
        this.restoreSelectedOfItems = function (items) {
            items.forEach(function (item) {
                item.selected = _this.isItemAlredySelected(item);
            });
        };
        this.reset = function () {
            _this.searchTerm = "";
            _this.selectedItems.length = 0;
        };
        this.isItemAlredySelected = function (item) {
            return _.some(_this.selectedItems, function (selectedItem) {
                return _this.itemsAreEqual(item, selectedItem);
            });
        };
        this.addItem = function (item) {
            _this.selectedItems.push(item);
        };
        this.removeItem = function (item) {
            _.remove(_this.selectedItems, function (currentItem) {
                return _this.itemsAreEqual(item, currentItem);
            });
        };
        this.updateSelectedParent = function () {
            _.forEach(_this._itemsSource, function (item) {
                item.selected = _this.isItemAlredySelected(item);
                if (item.innerItemsSource) {
                    _this.updateSelectedChild(item.innerItemsSource);
                }
            });
        };
        this.updateSelectedChild = function (items) {
            _.forEach(items, function (child) {
                child.selected = _this.isItemAlredySelected(child);
            });
        };
        this.clearSearch = function () {
            _this.searchTerm = "";
        };
        this.selectedItems = new Array();
    }
    Object.defineProperty(NcmAccessListGridService.prototype, "itemsSource", {
        get: function () {
            return this._itemsSource;
        },
        set: function (items) {
            this._itemsSource = items;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(NcmAccessListGridService.prototype, "searchTerm", {
        get: function () {
            return this._searchTerm;
        },
        set: function (item) {
            this._searchTerm = item;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(NcmAccessListGridService.prototype, "selection", {
        get: function () {
            return this.selectedItems;
        },
        enumerable: true,
        configurable: true
    });
    ;
    return NcmAccessListGridService;
}());
exports.NcmAccessListGridService = NcmAccessListGridService;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AclSrdIconController = /** @class */ (function () {
    /** @ngInject */
    AclSrdIconController.$inject = ["$scope", "$log", "_t"];
    function AclSrdIconController($scope, $log, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this._t = _t;
        this.overlappingRulesPlaceholder = "Overlapping Rules";
        this.activate = function () {
            _this.$log.debug("AclSrdIconController:Activate() for acl " + _this.model.name);
            _this.popoverDescriptions = _this.getPopoverDescriptions();
        };
        this.getSrdIconPopoverTemplate = { url: "ncm-acl-srd-popover-template" };
        this.isLoading = function () {
            return _this.model.srdStatistics == null;
        };
        this.canShowSrdIcon = function () {
            var stats = _this.model.srdStatistics;
            var result = stats == null ||
                stats.fullyRedundantCount > 0 ||
                stats.fullyShadowedCount > 0 ||
                stats.partiallyRedundantCount > 0 ||
                stats.partiallyShadowedCount > 0;
            return result;
        };
        this.getTotalSrdCount = function () {
            var stats = _this.model.srdStatistics;
            return stats.fullyRedundantCount + stats.fullyShadowedCount +
                stats.partiallyRedundantCount + stats.partiallyShadowedCount;
        };
        this.buildDescriptionViewModel = function (count, baseDescription, descriptions) {
            if (count > 0) {
                var description = count.toString() + " " + baseDescription;
                if (count > 1) {
                    description += "s";
                }
                descriptions.push(description);
            }
        };
        this.getPopoverDescriptions = function () {
            var stats = _this.model.srdStatistics;
            var descs = new Array();
            if (stats != null) {
                _this.buildDescriptionViewModel(stats.fullyShadowedCount, _this._t("fully shadowed rule"), descs);
                _this.buildDescriptionViewModel(stats.partiallyShadowedCount, _this._t("partially shadowed rule"), descs);
                _this.buildDescriptionViewModel(stats.fullyRedundantCount, _this._t("fully redundant rule"), descs);
                _this.buildDescriptionViewModel(stats.partiallyRedundantCount, _this._t("partially redundant rule"), descs);
            }
            return descs;
        };
        this.model = $scope.acl;
    }
    return AclSrdIconController;
}());
exports.AclSrdIconController = AclSrdIconController;


/***/ }),
/* 26 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcInterface_1 = __webpack_require__(18);
var utils_1 = __webpack_require__(6);
var VpcInterfaceInfoController = /** @class */ (function () {
    /** @ngInject */
    VpcInterfaceInfoController.$inject = ["$stateParams", "$log", "$scope", "ncmNavigationService", "ncmViewsService", "_t"];
    function VpcInterfaceInfoController($stateParams, $log, $scope, ncmNavigationService, ncmViewsService, _t) {
        this.$stateParams = $stateParams;
        this.$log = $log;
        this.$scope = $scope;
        this.ncmNavigationService = ncmNavigationService;
        this.ncmViewsService = ncmViewsService;
        this._t = _t;
        this.vpcConfiguration = [];
        this.memberInterfacesConfiguration = [];
        this.isActivated = false;
        this.vpcNotFoundData = {
            description: "",
            url: ""
        };
    }
    VpcInterfaceInfoController.prototype.activate = function () {
        this.$log.info("VpcInterfaceInfoDirectiveController: activate called");
        this.nodeName = this.$scope.nodeName;
        this.nodeId = this.$scope.nodeId;
        this.vpcStatus = this.$scope.vpcStatus;
        this.configChangedDate = utils_1.default.toLocalDateString(new Date(this.$scope.configChangedDate));
        this.vpcConfiguration = this.mapRawDataToLines(this.$scope.vpcConfiguration);
        this.memberInterfacesConfiguration = this.mapMultipleRawDataToLines(this.$scope.memberInterfacesConfiguration);
        this.refreshVpcNotFoundData();
        this.isActivated = true;
    };
    VpcInterfaceInfoController.prototype.getNodeDetailsUrl = function () {
        return this.ncmNavigationService.getNodeDetailsUrl(this.nodeId);
    };
    VpcInterfaceInfoController.prototype.getCssClass = function (index) {
        return index % 2 ? "stripe-odd-bg" : "stripe-even-bg";
    };
    VpcInterfaceInfoController.prototype.getEntityDescription = function () {
        switch (this.vpcStatus) {
            case vpcInterface_1.VpcStatus.ok:
                return this._t("Download time") + "&nbsp" + ("<span class='important'>" + this.configChangedDate + "</span>") + "&nbsp" + this._t("(Current)");
            case vpcInterface_1.VpcStatus.noConfig:
                return this._t("Configuration not yet downloaded");
            case vpcInterface_1.VpcStatus.notInNcm:
                return this._t("Node not monitored in NCM");
            case vpcInterface_1.VpcStatus.notInOrion:
                return this._t("Node not monitored in Orion");
            default:
                return this._t("vPC info could not be found in latest config");
        }
    };
    VpcInterfaceInfoController.prototype.refreshVpcNotFoundData = function () {
        var _this = this;
        switch (this.vpcStatus) {
            case vpcInterface_1.VpcStatus.notInOrion:
                this.vpcNotFoundData.description = this._t("Monitor in Orion");
                this.vpcNotFoundData.url = this.ncmNavigationService.getAddNodeToOrionUrl();
                break;
            case vpcInterface_1.VpcStatus.notInNcm:
                this.vpcNotFoundData.description = this._t("Manage Nodes");
                this.vpcNotFoundData.url = this.ncmNavigationService.getManageNodesUrl();
                break;
            case vpcInterface_1.VpcStatus.noConfig:
            case vpcInterface_1.VpcStatus.noVpcInformation:
                this.ncmViewsService.getConfigsSubviewId().then(function (viewId) {
                    _this.vpcNotFoundData.description = _this._t("Go to Configs page");
                    _this.vpcNotFoundData.url = _this.ncmNavigationService.getNodeDetailsUrl(_this.nodeId, viewId);
                });
                break;
        }
    };
    VpcInterfaceInfoController.prototype.mapRawDataToLines = function (rawData) {
        if (rawData == null) {
            return;
        }
        var counter = 0;
        var processedLines = [];
        rawData.forEach(function (line) {
            processedLines.push(new vpcInterface_1.VpcInterfaceLine(line, counter !== 0, counter++));
        });
        return processedLines;
    };
    VpcInterfaceInfoController.prototype.mapMultipleRawDataToLines = function (multipleRawData) {
        if (multipleRawData == null) {
            return;
        }
        var counter = 0;
        var processedLines = [];
        multipleRawData.forEach(function (rawData) {
            var internalCounter = 0;
            rawData.forEach(function (line) {
                processedLines.push(new vpcInterface_1.VpcInterfaceLine(line, internalCounter !== 0, counter++));
                internalCounter++;
            });
        });
        return processedLines;
    };
    VpcInterfaceInfoController.prototype.isStatusOk = function () {
        if (this.vpcStatus == null) {
            return false;
        }
        return this.vpcStatus === vpcInterface_1.VpcStatus.ok;
    };
    return VpcInterfaceInfoController;
}());
exports.VpcInterfaceInfoController = VpcInterfaceInfoController;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = __webpack_require__(6);
var AclConfigInfoController = /** @class */ (function () {
    /** @ngInject */
    AclConfigInfoController.$inject = ["$scope", "$log", "ncmNavigationService"];
    function AclConfigInfoController($scope, $log, ncmNavigationService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.ncmNavigationService = ncmNavigationService;
        this.aceLines = [];
        this.getCssClass = function (index) {
            return index % 2 ? "stripe-odd-bg" : "stripe-even-bg";
        };
        this.aceLines = $scope.aceLines;
        this.aclInfo = $scope.aclInfo;
        this.searchTerm = $scope.searchTerm;
        this.aclInfo.aclProperties.modificationTime = this.aclInfo.aclProperties.modificationTime;
        this.tokenClickHandler = $scope.tokenClickHandler;
        $scope.onReload = function () {
            _this.searchTerm = $scope.searchTerm;
            _this.aceLines = $scope.aceLines;
            _this.tokenClickHandler = $scope.tokenClickHandler;
        };
        this.$log.info("AclConfigInfoController:Activate1() (handler = " + this.tokenClickHandler + ")");
    }
    AclConfigInfoController.prototype.getAclDetailsUrl = function () {
        return this.ncmNavigationService.getNodeDetailsUrl(this.aclInfo.aclNode.coreNodeID);
    };
    ;
    AclConfigInfoController.prototype.toLocalDate = function (date) {
        return utils_1.default.toLocalDateString(date);
    };
    return AclConfigInfoController;
}());
exports.AclConfigInfoController = AclConfigInfoController;


/***/ }),
/* 29 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DiffDataEntry = /** @class */ (function () {
    function DiffDataEntry(nodeId, configId, entityName) {
        this.nodeId = nodeId;
        this.configId = configId;
        this.entityName = entityName;
    }
    return DiffDataEntry;
}());
exports.DiffDataEntry = DiffDataEntry;
var DiffData = /** @class */ (function () {
    function DiffData(leftNodeId, leftConfigId, leftObjectName, rightNodeId, rightConfigId, rightObjectName) {
        this.left = new DiffDataEntry(leftNodeId, leftConfigId, leftObjectName);
        this.right = new DiffDataEntry(rightNodeId, rightConfigId, rightObjectName);
    }
    return DiffData;
}());
exports.DiffData = DiffData;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(32);
module.exports = __webpack_require__(33);


/***/ }),
/* 32 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(34);
var config_1 = __webpack_require__(35);
var index_1 = __webpack_require__(36);
var index_2 = __webpack_require__(39);
var index_3 = __webpack_require__(107);
var index_4 = __webpack_require__(167);
var index_5 = __webpack_require__(188);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
index_5.default(app_1.default);


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("ncm.services", []);
angular.module("ncm.templates", []);
angular.module("ncm.components", []);
angular.module("ncm.filters", []);
angular.module("ncm.providers", []);
angular.module("ncm", [
    "orion",
    "filtered-list",
    "ncm.services",
    "ncm.templates",
    "ncm.components",
    "ncm.filters",
    "ncm.providers"
]);
// create and register Xui (Orion) module wrapper
var ncm = Xui.registerModule("ncm");
exports.default = ncm;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: ncm");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(37);
__webpack_require__(38);
exports.default = function (module) {
    module.service("Constants", constants_1.default);
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 38 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(40);
var index_2 = __webpack_require__(44);
var index_3 = __webpack_require__(49);
var index_4 = __webpack_require__(55);
var index_5 = __webpack_require__(61);
var index_6 = __webpack_require__(66);
var index_7 = __webpack_require__(86);
var index_8 = __webpack_require__(95);
var index_9 = __webpack_require__(101);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
    index_7.default(module);
    index_8.default(module);
    index_9.default(module);
};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessLists_controller_1 = __webpack_require__(41);
var accessLists_config_1 = __webpack_require__(42);
exports.default = function (module) {
    module.controller("AccessListsController", accessLists_controller_1.default);
    module.config(accessLists_config_1.default);
};


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AccessListsController = /** @class */ (function () {
    /** @ngInject */
    AccessListsController.$inject = ["$log", "swSubviewInfo"];
    function AccessListsController($log, swSubviewInfo) {
        this.$log = $log;
        this.swSubviewInfo = swSubviewInfo;
        this.nodeId = this.getNodeIdFromNetObject(this.swSubviewInfo.queryParams.netobject);
        this.$log.info("nodeId: " + this.nodeId);
    }
    Object.defineProperty(AccessListsController.prototype, "getNodeId", {
        get: function () {
            return this.nodeId;
        },
        enumerable: true,
        configurable: true
    });
    AccessListsController.prototype.getNodeIdFromNetObject = function (netObject) {
        if (_.startsWith(netObject, "N:")) {
            return Number(netObject.substring(2));
        }
        return null;
    };
    return AccessListsController;
}());
exports.default = AccessListsController;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
AccessListsConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function AccessListsConfig($stateProvider) {
    $stateProvider.state("accessLists", {
        parent: "subview",
        i18Title: "_t(Access Lists)",
        url: "/ncm/accessListsSubview",
        controller: "AccessListsController",
        controllerAs: "vm",
        template: __webpack_require__(43)
    });
}
exports.default = AccessListsConfig;
;


/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = "<div class=asa-acllists> <access-list-subview node-id=vm.nodeId></access-list-subview> </div> ";

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(45);
var aclRules_controller_1 = __webpack_require__(46);
var aclRules_config_1 = __webpack_require__(47);
exports.default = function (module) {
    module.controller("AclRulesController", aclRules_controller_1.default);
    module.config(aclRules_config_1.default);
};


/***/ }),
/* 45 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AclRulesController = /** @class */ (function () {
    /** @ngInject */
    AclRulesController.$inject = ["$scope", "$log", "$stateParams", "breadCrumbService", "_t"];
    function AclRulesController($scope, $log, $stateParams, breadCrumbService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.breadCrumbService = breadCrumbService;
        this._t = _t;
        this.activate = function () {
            _this.$log.info("AclRulesController:Activate()");
            _this.buildBreadCrumbs();
        };
        this.buildBreadCrumbs = function () {
            _this.breadCrumbService.buildBreadCrumbsForAclRulesView(_this.nodeId, _this.pageTitle).then(function (crumbs) {
                _this.crumbs = crumbs;
            });
        };
        this.nodeId = $stateParams["nodeId"];
        this.$log.info("nodeId: " + this.nodeId);
        this.aclName = $stateParams["aclName"];
        this.$log.info("aclName: " + this.aclName);
        this.configId = $stateParams["configId"];
        this.$log.info("configId: " + this.configId);
        this.pageTitle = this._t("Rules of") + (" " + this.aclName);
    }
    Object.defineProperty(AclRulesController.prototype, "getNodeId", {
        get: function () {
            return this.nodeId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AclRulesController.prototype, "getAclName", {
        get: function () {
            return this.aclName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AclRulesController.prototype, "getConfigId", {
        get: function () {
            return this.configId;
        },
        enumerable: true,
        configurable: true
    });
    return AclRulesController;
}());
exports.default = AclRulesController;


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
AclRulesConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function AclRulesConfig($stateProvider) {
    $stateProvider.state("aclRules", {
        i18Title: "_t(Access Control Lists)",
        url: "/ncm/aclRules/:nodeId/:configId?aclName",
        controller: "AclRulesController",
        controllerAs: "vm",
        template: __webpack_require__(48)
    });
}
exports.default = AclRulesConfig;


/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content ng-init=vm.activate() id=ncmAclRulesPage page-title={{vm.pageTitle}}> <xui-page-breadcrumbs> <xui-breadcrumb crumbs=vm.crumbs></xui-breadcrumb> </xui-page-breadcrumbs> <div class=ncm-aclRules-page> <div> <rule-list node-id=vm.nodeId acl-name=vm.aclName config-id=vm.configId></rule-list> </div> </div> </xui-page-content>";

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var aclRulesDiff_controller_1 = __webpack_require__(50);
var aclRulesDiff_config_1 = __webpack_require__(52);
var changeAclsComparedDialog_controller_1 = __webpack_require__(54);
__webpack_require__(10);
exports.default = function (module) {
    module.controller("AclRulesDiffController", aclRulesDiff_controller_1.default);
    module.controller("ChangeAclsComparedDialogController", changeAclsComparedDialog_controller_1.default);
    module.config(aclRulesDiff_config_1.default);
};


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var aclDiff_1 = __webpack_require__(16);
var accessControlListRule_1 = __webpack_require__(5);
var AclRulesDiffController = /** @class */ (function () {
    /** @ngInject */
    AclRulesDiffController.$inject = ["$scope", "$log", "$stateParams", "aclRulesDiffService", "ncmNavigationService", "xuiDialogService", "aclRuleHighlightingService", "$sce", "$window", "_t"];
    function AclRulesDiffController($scope, $log, $stateParams, aclRulesDiffService, ncmNavigationService, xuiDialogService, aclRuleHighlightingService, $sce, $window, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.aclRulesDiffService = aclRulesDiffService;
        this.ncmNavigationService = ncmNavigationService;
        this.xuiDialogService = xuiDialogService;
        this.aclRuleHighlightingService = aclRuleHighlightingService;
        this.$sce = $sce;
        this.$window = $window;
        this._t = _t;
        this.searchPlaceholderString = this._t("Search...");
        this.pageNameString = this._t("Access List Rules Diff");
        this.busyMassageString = this._t("Loading...");
        this.activate = function () {
            _this.$log.info("AclRulesDiffController:Activate()");
            _this.aclRulesDiffService.getAclDiffInfo(_this.leftNodeId, _this.leftConfigId, _this.leftAclName, _this.rightNodeId, _this.rightConfigId, _this.rightAclName)
                .then(function (result) {
                _this.aclDiffInfo = result;
                _this.getAclDiff();
                _this.onResize();
                _this.unbindEventHandler();
            });
        };
        this.getAclDiff = function () {
            _this.aclRulesDiffService.getAceDiff(_this.aclDiffInfo.left.aclNode.coreNodeID, _this.aclDiffInfo.left.aclProperties.configId, _this.aclDiffInfo.left.aclName, _this.aclDiffInfo.right.aclNode.coreNodeID, _this.aclDiffInfo.right.aclProperties.configId, _this.aclDiffInfo.right.aclName)
                .then(function (result) {
                _this.aceDiff = angular.copy(result);
                _this.filteredAceDiff = angular.copy(result);
                _this.isBusy = false;
                _this.invokeSizeCalc();
            });
        };
        this.getCssClass = function (index, rule, status) {
            if (rule == null || status === 0 || status === 1) {
                return index % 2 ? "stripe-odd-bg" : "stripe-even-bg";
            }
            else {
                return "status-" + status;
            }
        };
        this.sameInterfaces = function (leftInterfaces, rightInterfaces) {
            return _.isEqual(_.sortBy(leftInterfaces), _.sortBy(rightInterfaces));
        };
        this.compareDiffClick = function () {
            _this.$log.info("AclRulesDiffController:compareDiffClick()");
        };
        this.navigateToNodeDetails = function (nodeId) {
            _this.ncmNavigationService.navigateToNodeDetails(nodeId);
        };
        this.navigateToAclDetails = function (aclInfo) {
            _this.ncmNavigationService.navigateToAclDetails({
                "nodeId": aclInfo.aclNode.coreNodeID, "configId": aclInfo.aclProperties.configId, "aclName": aclInfo.aclName
            });
        };
        this.isReferenceType = function (token) {
            /* tslint:disable:no-bitwise */
            return (token.type & accessControlListRule_1.AceTokenType.ObjectGroup) === accessControlListRule_1.AceTokenType.ObjectGroup ||
                (token.type & accessControlListRule_1.AceTokenType.Object) === accessControlListRule_1.AceTokenType.Object;
            /* tslint:enable:no-bitwise */
        };
        this.resolveTokenCssClass = function (token) {
            return _this.aclRuleHighlightingService.resolveCssName(token);
        };
        this.openUrl = function (url) {
            _this.$window.open(url, "_blank");
        };
        this.isUrlType = function (token) {
            return token.type === accessControlListRule_1.AceTokenType.Url;
        };
        this.isInactive = function (tokens) {
            /* tslint:disable:no-bitwise */
            var res = _.find(tokens, function (token) {
                return (token.type & accessControlListRule_1.AceTokenType.Keyword) === accessControlListRule_1.AceTokenType.Keyword && token.value === "inactive";
            });
            /* tslint:enable:no-bitwise */
            return !_.isUndefined(res);
        };
        //Dialog section
        this.showChangeAclsComparedDialog = function () {
            var vm = new aclDiff_1.AclDiffDialogViewModel(_this.aclDiffInfo);
            var setupDialogOptions = {
                title: _this._t("Change ACLs Compared"),
                buttons: [
                    {
                        name: "changeButton",
                        isPrimary: true,
                        text: _this._t("change"),
                        action: function (response) {
                            var model = vm.getSubmittedResultCallback();
                            _this.ncmNavigationService.navigateToCompareAcl({
                                "leftNodeId": model.left.aclNode.coreNodeID,
                                "leftConfigId": model.left.aclProperties.configId,
                                "leftAclName": model.left.aclName,
                                "rightNodeId": model.right.aclNode.coreNodeID,
                                "rightConfigId": model.right.aclProperties.configId,
                                "rightAclName": model.right.aclName
                            });
                            return true;
                        }
                    },
                    {
                        name: "cancelButton",
                        isPrimary: false,
                        text: _this._t("cancel"),
                        action: function () {
                            return true;
                        }
                    }
                ],
                viewModel: vm
            };
            _this.xuiDialogService.showModal({
                template: __webpack_require__(51)
            }, setupDialogOptions);
        };
        this.onLeftTokenClickHandler = function (token) {
            _this.showSidePanel = true;
            _this.token = angular.copy(token);
            _this.tokenConfigId = angular.copy(_this.leftConfigId);
            _this.nodeId = angular.copy(_this.leftNodeId);
        };
        this.onRightTokenClickHandler = function (token) {
            _this.showSidePanel = true;
            _this.token = angular.copy(token);
            _this.tokenConfigId = angular.copy(_this.rightConfigId);
            _this.nodeId = angular.copy(_this.rightNodeId);
        };
        this.findMatchesInRule = function (searchTokens, pattern, rule) {
            var rawData = rule.tokens.map(function (x) { return x.value; }).join(" ");
            var match = pattern.exec(rawData);
            var matchesPosition = [];
            while (match != null) {
                var tokenPosition = rawData.substring(0, match.index).split(" ").length;
                matchesPosition.push(tokenPosition - 1);
                match = pattern.exec(rawData);
            }
            matchesPosition.forEach(function (position) {
                searchTokens.forEach(function (token, index) {
                    var escapedToken = _.escapeRegExp(token);
                    var regPattern;
                    if (searchTokens.length > 1) {
                        regPattern = index === 0 ? escapedToken + "(?!.*" + escapedToken + ")" : "^" + escapedToken;
                    }
                    else {
                        regPattern = escapedToken;
                    }
                    rule.tokens[position + index].toSelect = regPattern;
                });
            });
        };
        this.invokeSizeCalc = function () {
            var success;
            for (var i = 0; i < 30; i++) {
                setTimeout(function () {
                    success = _this.alignHeighOfAcl();
                }, 100);
                if (success) {
                    break;
                }
            }
        };
        this.alignHeighOfAcl = function () {
            var containerswithAces = document.getElementsByClassName("acl-configs-info");
            if (containerswithAces.length === 2) {
                var minQuantityAcl = Math.min(containerswithAces[0].getElementsByClassName("rule-row")
                    .length, containerswithAces[1].getElementsByClassName("rule-row").length);
                for (var i = 0; i < minQuantityAcl; i++) {
                    var comparedAcls = document.getElementsByName("rule-row-" + i);
                    var maxheight = Math.max(comparedAcls[0].clientHeight, comparedAcls[1].clientHeight).toString() + "px";
                    comparedAcls[0].style.height = maxheight;
                    comparedAcls[1].style.height = maxheight;
                }
                return true;
            }
            return false;
        };
        this.leftNodeId = $stateParams["leftNodeId"];
        this.$log.info("rightNodeId: " + this.leftNodeId);
        this.leftConfigId = $stateParams["leftConfigId"];
        this.$log.info("leftConfigId: " + this.leftConfigId);
        this.leftAclName = $stateParams["leftAclName"];
        this.$log.info("leftAclName: " + this.leftAclName);
        this.rightNodeId = $stateParams["rightNodeId"];
        this.$log.info("rightNodeId: " + this.rightNodeId);
        this.rightConfigId = $stateParams["rightConfigId"];
        this.$log.info("rightConfigId: " + this.rightConfigId);
        this.rightAclName = $stateParams["rightAclName"];
        this.$log.info("rightAclName: " + this.rightAclName);
        this.showSidePanel = false;
        this.isBusy = true;
    }
    AclRulesDiffController.prototype.onSearch = function (searchTerm, cancellation) {
        var _this = this;
        this.searchTerm = searchTerm;
        if (this.searchTerm) {
            this.filteredAceDiff = this.filterAceDiff(this.aceDiff);
        }
        else {
            this.filteredAceDiff = angular.copy(this.aceDiff);
        }
        cancellation.then(function () {
            _this.onSearchClear();
        });
    };
    AclRulesDiffController.prototype.onSearchClear = function () {
        this.filteredAceDiff = angular.copy(this.aceDiff);
        this.searchTerm = "";
    };
    AclRulesDiffController.prototype.filterAceDiff = function (diff) {
        var pattern = new RegExp(_.escapeRegExp(this.searchTerm), "i");
        var filteredDiff = {
            left: angular.copy(diff.left.filter(function (item) {
                return (item.accessControlListRule != null && pattern.test(item.accessControlListRule.rawData));
            })),
            right: angular.copy(diff.right.filter(function (item) {
                return (item.accessControlListRule != null && pattern.test(item.accessControlListRule.rawData));
            }))
        };
        this.findMatches(filteredDiff);
        return filteredDiff;
    };
    AclRulesDiffController.prototype.findMatches = function (diff) {
        var _this = this;
        var searchTokens = this.searchTerm.split(" ");
        var pattern = new RegExp(_.escapeRegExp(this.searchTerm), "gi");
        diff.left.forEach(function (item) {
            if (item != null) {
                _this.findMatchesInRule(searchTokens, pattern, item.accessControlListRule);
            }
        });
        diff.right.forEach(function (item) {
            if (item != null) {
                _this.findMatchesInRule(searchTokens, pattern, item.accessControlListRule);
            }
        });
    };
    AclRulesDiffController.prototype.onResize = function () {
        $(window).on("resize", function () {
            var containerswithAces = document.getElementsByClassName("acl-config-info");
            var maxQuantityAcl = Math.max(containerswithAces[0].getElementsByClassName("rule-row")
                .length, containerswithAces[1].getElementsByClassName("rule-row").length);
            for (var i = 0; i < maxQuantityAcl; i++) {
                var comparedAcls = document.getElementsByName("rule-row-" + i);
                comparedAcls[0].style.height = "auto";
                comparedAcls[1].style.height = "auto";
                var maxheight = Math.max(comparedAcls[0].clientHeight, comparedAcls[1].clientHeight)
                    .toString() + "px";
                comparedAcls[0].style.height = maxheight;
                comparedAcls[1].style.height = maxheight;
            }
        });
    };
    AclRulesDiffController.prototype.unbindEventHandler = function () {
        this.$scope.$on("$destroy", function () {
            $(window).off("resize");
        });
    };
    return AclRulesDiffController;
}());
exports.default = AclRulesDiffController;


/***/ }),
/* 51 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"ChangeAclsComparedDialogController as container\" ng-init=container.init(vm.dialogOptions.viewModel)> <xui-dialog class=ncm-aclRulesDiff-dialog xui-busy=container.isBusy xui-busy-message={{container.busyMassageString}} xui-allow-cancel=true xui-busy-on-cancel=vm.cancel()> <div class=row> <acl-picker node-caption={{container.baseNodeCaptionString}} items-data-model=container.leftCompModel on-changed=container.onLeftSelectionChanged(sourceType) class=col-md-6> </acl-picker> <acl-picker node-caption={{container.compareWithNodeCaption}} items-data-model=container.rightCompModel on-changed=container.onRightSelectionChanged(sourceType) class=\"col-md-6 vertical-divider\"> </acl-picker> </div> </xui-dialog> </div>";

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
AclRulesDiffConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function AclRulesDiffConfig($stateProvider) {
    $stateProvider.state("aclRulesDiff", {
        url: "/ncm/aclRulesDiff?leftNodeId&leftConfigId&leftAclName&rightNodeId&rightConfigId&rightAclName",
        i18Title: "_t(Access List Rules Diff)",
        controller: "AclRulesDiffController",
        controllerAs: "vm",
        template: __webpack_require__(53)
    });
}
exports.default = AclRulesDiffConfig;


/***/ }),
/* 53 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content ng-init=vm.activate() id=ncmAclRulesDiffPage class=ncm-aclRulesDiff-page page-title={{vm.pageNameString}} is-busy=vm.isBusy busy-message={{vm.busyMassageString}}> <div> <xui-button icon=edit ng-click=vm.showChangeAclsComparedDialog() display-style=link class=change-acls-compare-button> <span _t>CHANGE ACLs COMPARED</span> </xui-button> </div> <xui-divider> </xui-divider> <div class=row> <div class=\"{{vm.showSidePanel ? 'col-md-9': 'col-md-12'}}\"> <div class=\"row xui-padding-mdb\"> <div class=\"col-md-offset-8 col-md-4\"> <xui-search placeholder={{vm.searchPlaceholderString}} on-search=\"vm.onSearch(value, cancellation)\" on-clear=vm.onSearchClear() items-source=[]> </xui-search> </div> </div> <div class=aclConfigsInfo-details-container ng-if=!vm.isBusy> <acl-config-info class=acl-configs-info1 token-click-handler=vm.onLeftTokenClickHandler id=id-acl-configs-info1 ace-lines=vm.filteredAceDiff.left acl-info=vm.aclDiffInfo.left search-term=vm.searchTerm></acl-config-info> <acl-config-info class=acl-configs-info2 token-click-handler=vm.onRightTokenClickHandler id=id-acl-configs-info2 ace-lines=vm.filteredAceDiff.right acl-info=vm.aclDiffInfo.right search-term=vm.searchTerm></acl-config-info> </div> </div> <div class=\"col-md-3 sticky-object-inspector-container\" ng-if=\"vm.showSidePanel==true\"> <object-inspector token=vm.token node-id=vm.nodeId config-id=vm.tokenConfigId on-close=\"vm.showSidePanel = false;\"> </object-inspector> </div> </div> </xui-page-content>";

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var aclPicker_directive_controller_1 = __webpack_require__(17);
var ChangeAclsComparedDialogController = /** @class */ (function () {
    /** @ngInject */
    ChangeAclsComparedDialogController.$inject = ["$scope", "$log", "aclRulesDiffService", "_t"];
    function ChangeAclsComparedDialogController($scope, $log, aclRulesDiffService, _t) {
        this.$scope = $scope;
        this.$log = $log;
        this.aclRulesDiffService = aclRulesDiffService;
        this._t = _t;
        this.defaultInterfaceIndex = 0;
        this.notAssignedInterface = "";
        this.isLeftLoading = true;
        this.isRightLoading = true;
        this.busyMassageString = this._t("Loading...");
        this.baseNodeCaptionString = this._t("Base Node");
        this.compareWithNodeCaption = this._t("Compare with Node");
        this.leftCompModel = new aclPicker_directive_controller_1.AclPickerData();
        this.rightCompModel = new aclPicker_directive_controller_1.AclPickerData();
    }
    Object.defineProperty(ChangeAclsComparedDialogController.prototype, "isBusy", {
        get: function () {
            return this.isLeftLoading || this.isRightLoading;
        },
        enumerable: true,
        configurable: true
    });
    ChangeAclsComparedDialogController.prototype.init = function (viewModel) {
        this.viewModel = viewModel;
        this.bindNodesDataSources(this.viewModel.aclDiffInfo);
        this.bindAclInterfacesDataSources(this.viewModel.aclDiffInfo);
        this.bindIsSameValues(this.viewModel.aclDiffInfo);
        this.bindSubmitResultCallback(this.viewModel);
    };
    ChangeAclsComparedDialogController.prototype.onRightSelectionChanged = function (sourceType) {
        var _this = this;
        if (sourceType === aclPicker_directive_controller_1.AclPickerItemType.Node) {
            this.isRightLoading = true;
            this.fetchAclInterfaces(this.rightCompModel.selectedNode.coreNodeID).then(function (rResult) {
                _this.rightCompModel.interfaces = rResult;
                _this.bindSelectedItems(_this.rightCompModel, rResult);
                _this.isRightLoading = false;
            });
        }
    };
    ChangeAclsComparedDialogController.prototype.onLeftSelectionChanged = function (sourceType) {
        var _this = this;
        if (sourceType === aclPicker_directive_controller_1.AclPickerItemType.Node) {
            this.isLeftLoading = true;
            this.fetchAclInterfaces(this.leftCompModel.selectedNode.coreNodeID).then(function (lResult) {
                _this.leftCompModel.interfaces = lResult;
                _this.bindSelectedItems(_this.leftCompModel, lResult);
                _this.trySyncLeftSide(sourceType);
                _this.isLeftLoading = false;
            });
        }
        else if (sourceType === aclPicker_directive_controller_1.AclPickerItemType.Interface || sourceType === aclPicker_directive_controller_1.AclPickerItemType.Acl) {
            this.trySyncLeftSide(sourceType);
        }
    };
    ChangeAclsComparedDialogController.prototype.trySyncLeftSide = function (level) {
        if ((level === aclPicker_directive_controller_1.AclPickerItemType.Node && this.rightCompModel.isSameNode) ||
            (level === aclPicker_directive_controller_1.AclPickerItemType.Interface && this.rightCompModel.isSameInterface) ||
            (level === aclPicker_directive_controller_1.AclPickerItemType.Acl && this.rightCompModel.isSameAcl)) {
            this.rightCompModel.selectedNode = this.leftCompModel.selectedNode;
            this.rightCompModel.interfaces = this.leftCompModel.interfaces;
            this.rightCompModel.selectedInterface = this.leftCompModel.selectedInterface;
            this.rightCompModel.selectedAcl = this.leftCompModel.selectedAcl;
            this.rightCompModel.selectedVersion = this.leftCompModel.selectedAcl.history[this.defaultInterfaceIndex];
        }
    };
    //bind functions
    ChangeAclsComparedDialogController.prototype.bindSubmitResultCallback = function (model) {
        var _this = this;
        model.getSubmittedResultCallback = function () {
            model.aclDiffInfo.left.aclNode.coreNodeID = _this.leftCompModel.selectedNode.coreNodeID;
            model.aclDiffInfo.left.aclName = _this.leftCompModel.selectedAcl.aclName;
            model.aclDiffInfo.left.aclProperties.configId = _this.leftCompModel.selectedVersion.configId;
            model.aclDiffInfo.right.aclNode.coreNodeID = _this.rightCompModel.selectedNode.coreNodeID;
            model.aclDiffInfo.right.aclName = _this.rightCompModel.selectedAcl.aclName;
            model.aclDiffInfo.right.aclProperties.configId = _this.rightCompModel.selectedVersion.configId;
            return model.aclDiffInfo;
        };
    };
    ChangeAclsComparedDialogController.prototype.bindNodesDataSources = function (aclDiffInfo) {
        var _this = this;
        this.fetchNodes().then(function (result) {
            _this.leftCompModel.nodes = result;
            _this.rightCompModel.nodes = result;
            _this.leftCompModel.selectedNode = _this.getPreselectedNode(result, aclDiffInfo.left.aclNode.coreNodeID);
            _this.rightCompModel.selectedNode = _this.getPreselectedNode(result, aclDiffInfo.right.aclNode.coreNodeID);
        });
    };
    ChangeAclsComparedDialogController.prototype.bindAclInterfacesDataSources = function (aclDiffInfo) {
        var _this = this;
        this.fetchAclInterfaces(aclDiffInfo.left.aclNode.coreNodeID).then(function (lResult) {
            _this.leftCompModel.interfaces = lResult;
            _this.bindSelectedItems(_this.leftCompModel, lResult, aclDiffInfo.left);
            if (aclDiffInfo.left.aclNode.coreNodeID === aclDiffInfo.right.aclNode.coreNodeID) {
                _this.rightCompModel.interfaces = lResult;
                _this.bindSelectedItems(_this.rightCompModel, lResult, aclDiffInfo.right);
            }
            else {
                _this.fetchAclInterfaces(aclDiffInfo.right.aclNode.coreNodeID).then(function (rResult) {
                    _this.rightCompModel.interfaces = rResult;
                    _this.bindSelectedItems(_this.rightCompModel, rResult, aclDiffInfo.right);
                });
            }
            _this.isLeftLoading = false;
            _this.isRightLoading = false;
        });
    };
    ChangeAclsComparedDialogController.prototype.bindIsSameValues = function (aclDiffInfo) {
        this.rightCompModel.isSameNode =
            aclDiffInfo.left.aclNode.coreNodeID === aclDiffInfo.right.aclNode.coreNodeID;
        this.rightCompModel.isSameInterface =
            aclDiffInfo.left.aclProperties.interfaces[this.defaultInterfaceIndex]
                === aclDiffInfo.right.aclProperties.interfaces[this.defaultInterfaceIndex];
        this.rightCompModel.isSameAcl =
            aclDiffInfo.left.aclName === aclDiffInfo.right.aclName;
    };
    ChangeAclsComparedDialogController.prototype.bindSelectedItems = function (dataSource, result, preSelectedInfo) {
        if (preSelectedInfo) {
            dataSource.selectedInterface =
                this.getPreselectedInterface(result, preSelectedInfo.aclProperties.interfaces);
            dataSource.selectedAcl =
                this.getPreselectedAcl(dataSource.selectedInterface.acl, preSelectedInfo.aclName);
            dataSource.selectedVersion =
                this.getPreselectedVersion(dataSource.selectedAcl.history, preSelectedInfo.aclProperties.configId);
        }
        else {
            dataSource.selectedInterface = result[0];
            dataSource.selectedAcl = dataSource.selectedInterface.acl[0];
            dataSource.selectedVersion = dataSource.selectedAcl.history[0];
        }
    };
    //preSelected functions
    ChangeAclsComparedDialogController.prototype.getPreselectedNode = function (result, selectedNodeId) {
        return _.find(result, function (node) {
            return node.coreNodeID === selectedNodeId;
        });
    };
    ChangeAclsComparedDialogController.prototype.getPreselectedInterface = function (source, selectedInterfaces) {
        var _this = this;
        var preSelectedInterface;
        if (selectedInterfaces.length > 0) {
            preSelectedInterface = _.find(source, function (i) { return i.interfaceName === selectedInterfaces[_this.defaultInterfaceIndex]; });
        }
        else {
            preSelectedInterface = _.find(source, function (i) { return i.interfaceName === _this.notAssignedInterface; });
        }
        return preSelectedInterface;
    };
    ChangeAclsComparedDialogController.prototype.getPreselectedAcl = function (source, aclName) {
        return _.find(source, function (acl) {
            return acl.aclName === aclName;
        });
    };
    ChangeAclsComparedDialogController.prototype.getPreselectedVersion = function (source, selectedConfigId) {
        return _.find(source, function (ver) {
            return ver.configId === selectedConfigId;
        });
    };
    //fetch functions
    ChangeAclsComparedDialogController.prototype.fetchNodes = function () {
        return this.aclRulesDiffService.getAclNodes().then(function (result) {
            return result;
        });
    };
    ChangeAclsComparedDialogController.prototype.fetchAclInterfaces = function (nodeId) {
        return this.aclRulesDiffService.getAccessControlListsFilter(nodeId).then(function (result) {
            return result;
        });
    };
    return ChangeAclsComparedDialogController;
}());
exports.default = ChangeAclsComparedDialogController;


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var groupObjDiff_controller_1 = __webpack_require__(56);
var groupObjDiff_config_1 = __webpack_require__(58);
var changeGroupsComparedDialog_controller_1 = __webpack_require__(60);
__webpack_require__(10);
exports.default = function (module) {
    module.controller("GroupObjDiffController", groupObjDiff_controller_1.default);
    module.controller("ChangeGroupsComparedDialogController", changeGroupsComparedDialog_controller_1.default);
    module.config(groupObjDiff_config_1.default);
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var objectGroup_1 = __webpack_require__(13);
var objectGroup_2 = __webpack_require__(13);
var accessControlListRule_1 = __webpack_require__(5);
var GroupObjDiffController = /** @class */ (function () {
    /** @ngInject */
    GroupObjDiffController.$inject = ["$scope", "$log", "$stateParams", "objectDefinitionService", "objectGroupService", "dialogService", "ncmNavigationService", "aclRuleHighlightingService", "$sce", "_t"];
    function GroupObjDiffController($scope, $log, $stateParams, objectDefinitionService, objectGroupService, dialogService, ncmNavigationService, aclRuleHighlightingService, $sce, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.objectDefinitionService = objectDefinitionService;
        this.objectGroupService = objectGroupService;
        this.dialogService = dialogService;
        this.ncmNavigationService = ncmNavigationService;
        this.aclRuleHighlightingService = aclRuleHighlightingService;
        this.$sce = $sce;
        this._t = _t;
        this.groupDiff = [];
        this.groupDiffCopy = [];
        this.history = new Array();
        this.sameObjGroupMessageString = this._t("Same Object Group");
        this.sameObjMessageString = this._t("Same Object");
        this.searchPlaceholderString = this._t("Search...");
        this.busyMessageString = this._t("Loading...");
        this.activate = function () {
            _this.$log.info("GroupObjDiffController:Activate()");
            if (_this.isObjectItemObjectGroup) {
                _this.getObjectGroupHistory();
                _this.getObjectGroupDiff();
            }
            else {
                _this.getObjectDefinitionHistory();
                _this.getObjectDefinitionDiff();
            }
        };
        this.getObjectGroupDiff = function () {
            _this.objectGroupService.getObjectGroupDiff(_this.leftNodeId, _this.leftConfigId, _this.leftGroupName, _this.rightNodeId, _this.rightConfigId, _this.rightGroupName)
                .then(function (result) {
                _this.groupDiff = result.diff;
                _this.groupDiffCopy = angular.copy(_this.groupDiff);
                _this.isBusy = false;
            });
        };
        this.getObjectDefinitionDiff = function () {
            _this.objectDefinitionService.getObjectDefinitionDiff(_this.leftNodeId, _this.leftConfigId, _this.leftGroupName, _this.rightNodeId, _this.rightConfigId, _this.rightGroupName)
                .then(function (result) {
                result.diff.forEach(function (item) {
                    if (item.left != null) {
                        item.left.tokens = _this.tokenize(item.left.rawData, item.left);
                    }
                    if (item.right != null) {
                        item.right.tokens = _this.tokenize(item.right.rawData, item.right);
                    }
                });
                _this.groupDiff = result.diff;
                _this.groupDiffCopy = angular.copy(_this.groupDiff);
                _this.isBusy = false;
            });
        };
        this.isReferenceType = function (token) {
            /* tslint:disable:no-bitwise */
            return (token.type & accessControlListRule_1.AceTokenType.ObjectGroup) === accessControlListRule_1.AceTokenType.ObjectGroup ||
                (token.type & accessControlListRule_1.AceTokenType.Object) === accessControlListRule_1.AceTokenType.Object;
            /* tslint:enable:no-bitwise */
        };
        this.getCssClass = function (index, member, status) {
            if (member == null || status === 0 || status === 1) {
                return index % 2 ? "stripe-odd-bg" : "stripe-even-bg";
            }
            else {
                return "highlight-status-" + status;
            }
        };
        this.resolveTokenCssClass = function (token) {
            return _this.aclRuleHighlightingService.resolveCssName(token);
        };
        this.onTokenClickHandler = function (token, nodeId, configId) {
            _this.showSidePanel = true;
            _this.token = angular.copy(token);
            _this.nodeId = angular.copy(nodeId);
            _this.configId = angular.copy(configId);
        };
        this.showChangeGroupsComparedDialog = function () {
            _this.$log.info("GroupObjDiffController:showChangeGroupsComparedDialog()");
            var left = new objectGroup_1.ObjectGroupDialogInfo(_this.history, _.find(_this.history, function (v) { return v.configId === _this.leftConfigId; }));
            var right = new objectGroup_1.ObjectGroupDialogInfo(_this.history, _.find(_this.history, function (v) { return v.configId === _this.rightConfigId; }));
            var vm = new objectGroup_1.ObjectGroupDialogModel(left, right);
            var dialogOptions = {
                title: _this._t("Change Diff Values"),
                buttons: [
                    {
                        name: "changeButton",
                        isPrimary: true,
                        text: _this._t("change"),
                        action: function (response) {
                            _this.ncmNavigationService.navigateToCompareGroups({
                                "leftNodeId": _this.leftNodeId,
                                "leftConfigId": vm.left.selectedItem.configId,
                                "leftGroupName": _this.leftGroupName,
                                "rightNodeId": _this.rightNodeId,
                                "rightConfigId": vm.right.selectedItem.configId,
                                "rightGroupName": _this.rightGroupName,
                                "referenceType": _this.isObjectItemObjectGroup ?
                                    objectGroup_2.ObjectDefinitionReferenceType[objectGroup_2.ObjectDefinitionReferenceType.ObjectGroup] :
                                    objectGroup_2.ObjectDefinitionReferenceType[objectGroup_2.ObjectDefinitionReferenceType.Object]
                            });
                            return true;
                        }
                    },
                    {
                        name: "cancelButton",
                        isPrimary: false,
                        text: _this._t("cancel"),
                        action: function () {
                            return true;
                        }
                    }
                ],
                viewModel: vm
            };
            _this.dialogService.showModal({
                template: __webpack_require__(57)
            }, dialogOptions);
        };
        this.onSearch = function (value, cancellation) {
            _this.searchTerm = value;
            if (value) {
                var pattern_1 = new RegExp(_.escapeRegExp(value), "i");
                _this.groupDiff = angular.copy(_this.groupDiffCopy.filter(function (item) {
                    return (item.left != null && pattern_1.test(item.left.rawData))
                        || (item.right != null && pattern_1.test(item.right.rawData));
                }));
                _this.findMatches(_this.groupDiff);
            }
            else {
                _this.groupDiff = angular.copy(_this.groupDiffCopy);
            }
        };
        this.highlightSearch = function (token) {
            if (token.toSelect) {
                var regex = _.escapeRegExp("(" + token.toSelect + ")");
                var pattern = new RegExp(regex, "gi");
                var text = token.value.replace(pattern, "<span class=\"xui-highlighted\">$1</span>");
                return _this.$sce.trustAsHtml(text);
            }
            return token.value;
        };
        this.findMatches = function (diff) {
            var searchTokens = _this.searchTerm.split(" ");
            var pattern = new RegExp(_.escapeRegExp(_this.searchTerm), "gi");
            diff.forEach(function (item) {
                if (item.left != null) {
                    _this.findMatchesInObjectGroupMember(searchTokens, pattern, item.left);
                }
                if (item.right != null) {
                    _this.findMatchesInObjectGroupMember(searchTokens, pattern, item.right);
                }
            });
        };
        this.findMatchesInObjectGroupMember = function (searchTokens, pattern, member) {
            var rawData = member.tokens.map(function (x) { return x.value; }).join(" ");
            var match = pattern.exec(rawData);
            var matchesPosition = [];
            while (match != null) {
                var tokenPosition = rawData.substring(0, match.index).split(" ").length;
                matchesPosition.push(tokenPosition - 1);
                match = pattern.exec(rawData);
            }
            matchesPosition.forEach(function (position) {
                searchTokens.forEach(function (token, index) {
                    var escapedToken = _.escapeRegExp(token);
                    var regPattern;
                    if (searchTokens.length > 1) {
                        regPattern = index === 0 ? escapedToken + "(?!.*" + escapedToken + ")" : "^" + escapedToken;
                    }
                    else {
                        regPattern = escapedToken;
                    }
                    member.tokens[position + index].toSelect = regPattern;
                });
            });
        };
        this.leftNodeId = $stateParams["leftNodeId"];
        this.$log.info("rightNodeId: " + this.leftNodeId);
        this.leftConfigId = $stateParams["leftConfigId"];
        this.$log.info("leftConfigId: " + this.leftConfigId);
        this.leftGroupName = $stateParams["leftGroupName"];
        this.$log.info("leftGroupName: " + this.leftGroupName);
        this.rightNodeId = $stateParams["rightNodeId"];
        this.$log.info("rightNodeId: " + this.rightNodeId);
        this.rightConfigId = $stateParams["rightConfigId"];
        this.$log.info("rightConfigId: " + this.rightConfigId);
        this.rightGroupName = $stateParams["rightGroupName"];
        this.$log.info("rightGroupName: " + this.rightGroupName);
        this.referenceType = objectGroup_2.ObjectDefinitionReferenceType[$stateParams["referenceType"]];
        this.$log.info("referenceType: " + this.referenceType);
        this.isObjectItemObjectGroup = this.referenceType === objectGroup_2.ObjectDefinitionReferenceType.ObjectGroup;
        this.pageTitle = this.isObjectItemObjectGroup === true ?
            this._t("Object Groups Diff") :
            this._t("Objects Diff");
        this.isBusy = true;
        this.showSidePanel = false;
    }
    GroupObjDiffController.prototype.getObjectGroupHistory = function () {
        var _this = this;
        if (this.leftNodeId === this.rightNodeId && this.leftGroupName === this.rightGroupName) {
            this.objectGroupService.getObjectGroupHistory(this.leftNodeId, this.leftGroupName, null)
                .then(function (result) {
                _this.history = result.historyEntries;
                _this.leftGroupHistory = _.find(_this.history, function (h) { return h.configId === _this.leftConfigId; });
                _this.rightGroupHistory = _.find(_this.history, function (h) { return h.configId === _this.rightConfigId; });
            });
        }
    };
    ;
    GroupObjDiffController.prototype.getObjectDefinitionHistory = function () {
        var _this = this;
        if (this.leftNodeId === this.rightNodeId && this.leftGroupName === this.rightGroupName) {
            this.objectDefinitionService.getObjectDefinitionHistory(this.leftNodeId, this.leftGroupName, null)
                .then(function (result) {
                _this.history = result.historyEntries;
                _this.leftGroupHistory = _.find(_this.history, function (h) { return h.configId === _this.leftConfigId; });
                _this.rightGroupHistory = _.find(_this.history, function (h) { return h.configId === _this.rightConfigId; });
            });
        }
    };
    ;
    GroupObjDiffController.prototype.tokenize = function (line, member) {
        var arr = new Array();
        line.split(" ")
            .forEach(function (item) {
            var token = new accessControlListRule_1.AceToken();
            token.value = item;
            token.type = accessControlListRule_1.AceTokenType.Unknown;
            arr.push(token);
        });
        return arr;
    };
    ;
    return GroupObjDiffController;
}());
exports.default = GroupObjDiffController;


/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=ncm-groupObjDiff-dialog> <div class=row ng-controller=\"ChangeGroupsComparedDialogController as controller\"> <div class=col-md-6> <xui-dropdown caption={{controller.versionDropdownTitle}} display-value-fn=controller.renderDisplayName(item) items-source=vm.dialogOptions.viewModel.left.itemsSource selected-item=vm.dialogOptions.viewModel.left.selectedItem on-secondary-action=\"\"> </xui-dropdown> </div> <div class=\"col-md-6 vertical-divider\"> <xui-dropdown caption={{controller.versionDropdownTitle}} display-value-fn=controller.renderDisplayName(item) items-source=vm.dialogOptions.viewModel.right.itemsSource selected-item=vm.dialogOptions.viewModel.right.selectedItem on-secondary-action=\"\"> </xui-dropdown> </div> </div> </xui-dialog> ";

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
AclRulesDiffConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function AclRulesDiffConfig($stateProvider) {
    $stateProvider.state("groupObjDiff", {
        url: "/ncm/groupObjDiff/" +
            ":leftNodeId/:leftConfigId/:leftGroupName/" +
            ":rightNodeId/:rightConfigId/:rightGroupName/" +
            ":referenceType",
        i18Title: "_t(Object Groups Diff)",
        controller: "GroupObjDiffController",
        controllerAs: "vm",
        template: __webpack_require__(59)
    });
}
exports.default = AclRulesDiffConfig;


/***/ }),
/* 59 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content ng-init=vm.activate() id=ncmGroupObjDiffPage class=ncm-groupObjDiff-page page-title={{vm.pageTitle}} is-busy=vm.isBusy busy-message={{vm.busyMessageString}}> <div> <xui-button icon=edit ng-click=vm.showChangeGroupsComparedDialog() display-style=link> <span _t>CHANGE GROUPs COMPARED</span> </xui-button> </div> <xui-divider> </xui-divider> <div class=row> <div class=\"{{vm.showSidePanel ? 'col-md-9': 'col-md-12'}}\"> <div class=\"row xui-padding-mdb\"> <div class=\"col-md-offset-8 col-md-4\"> <xui-search placeholder={{vm.searchPlaceholderString}} on-search=\"vm.onSearch(value, cancellation)\" items-source=[]> </xui-search> </div> </div> <table class=diff-table> <thead> <tr> <td colspan=2 class=header-bg> <div> <strong class=entity-name>{{vm.leftGroupName}}</strong> </div> <div> <span ng-if=\"vm.isObjectItemObjectGroup==true\" _t>Object Group changed</span> <span ng-if=\"vm.isObjectItemObjectGroup==false\" _t>Object changed</span> <strong> {{vm.toLocateDate(vm.leftGroupHistory.downloadTime)}} </strong> <span ng-if=vm.leftGroupHistory.isLatest _t>(Current)</span> </div> </td> <td class=td-divider>&nbsp;</td> <td colspan=2 class=header-bg> <div> <strong ng-if=\"vm.isObjectItemObjectGroup==true\"> {{vm.rightGroupName === vm.leftGroupName ? vm.sameObjGroupMessageString : vm.rightGroupName}} </strong> <strong ng-if=\"vm.isObjectItemObjectGroup==false\"> {{vm.rightGroupName === vm.leftGroupName ? vm.sameObjMessageString : vm.rightGroupName}} </strong> </div> <div> <span ng-if=\"vm.isObjectItemObjectGroup==true\" _t>Object Group changed</span> <span ng-if=\"vm.isObjectItemObjectGroup==false\" _t>Object changed</span> <strong> {{vm.toLocateDate(vm.rightGroupHistory.downloadTime)}} </strong> <span ng-if=vm.rightGroupHistory.isLatest _t>(Current)</span> </div> </td> </tr> <tr ng-if=\"vm.searchTerm && vm.groupDiff.length === 0\"> <td colspan=5 class=xui-listview__empty _t> No results were found that match your query </td> </tr> </thead> <tbody> <tr ng-repeat=\"item in vm.groupDiff\"> <td class=\"td-ordinalNumber {{vm.getCssClass(item.index, item.left, item.status)}}\"> {{item.left.index}}</td> <td class=\"td-rawData {{vm.getCssClass(item.index)}}\"> <div ng-if=\"vm.isObjectItemObjectGroup==true\"> <span ng-repeat=\"token in item.left.tokens\"> <span ng-class=::vm.resolveTokenCssClass(token) ng-if=\"::vm.isReferenceType(token)==false\"> <span ng-bind-html=::vm.highlightSearch(token)></span> </span> <a role=button ng-click=\"::vm.onTokenClickHandler(token, vm.leftNodeId, vm.leftConfigId)\" ng-class=::vm.resolveTokenCssClass(token) ng-if=\"::vm.isReferenceType(token)==true\"> <span ng-bind-html=::vm.highlightSearch(token)></span> </a>&nbsp; </span> </div> <div ng-if=\"vm.isObjectItemObjectGroup==false\"> <span ng-repeat=\"token in item.left.tokens\"> <span ng-class=::vm.resolveTokenCssClass(token)> <span ng-bind-html=::vm.highlightSearch(token)></span>&nbsp; </span> </span> </div> </td> <td class=td-divider>&nbsp;</td> <td class=\"td-ordinalNumber {{vm.getCssClass(item.index, item.right, item.status)}}\"> {{item.right.index}}</td> <td class=\"td-rawData {{vm.getCssClass(item.index)}}\"> <div ng-if=\"vm.isObjectItemObjectGroup==true\"> <span ng-repeat=\"token in item.right.tokens\"> <span ng-class=::vm.resolveTokenCssClass(token) ng-if=\"::vm.isReferenceType(token)==false\"> <span ng-bind-html=::vm.highlightSearch(token)></span> </span> <a role=button ng-click=\"::vm.onTokenClickHandler(token, vm.rightNodeId, vm.rightConfigId)\" ng-class=::vm.resolveTokenCssClass(token) ng-if=\"::vm.isReferenceType(token)==true\"> <span ng-bind-html=::vm.highlightSearch(token)></span> </a>&nbsp; </span> </div> <div ng-if=\"vm.isObjectItemObjectGroup==false\"> <span ng-repeat=\"token in item.right.tokens\"> <span ng-class=::vm.resolveTokenCssClass(token)> <span ng-bind-html=::vm.highlightSearch(token)></span>&nbsp; </span> </span> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-3 sticky-object-inspector-container\" ng-if=\"vm.showSidePanel==true\"> <object-inspector token=vm.token node-id=vm.nodeId config-id=vm.configId on-close=\"vm.showSidePanel = false;\"> </object-inspector> </div> </div> </xui-page-content>";

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = __webpack_require__(6);
var ChangeGroupsComparedDialogController = /** @class */ (function () {
    /** @ngInject */
    ChangeGroupsComparedDialogController.$inject = ["$scope", "$log", "_t"];
    function ChangeGroupsComparedDialogController($scope, $log, _t) {
        this.$scope = $scope;
        this.$log = $log;
        this._t = _t;
        this.versionDropdownTitle = this._t("Version");
    }
    ChangeGroupsComparedDialogController.prototype.renderDisplayName = function (item) {
        var downloadTimeFormatted = utils_1.default.toLocalDateString(item.downloadTime);
        return item.isLatest ? downloadTimeFormatted + this._t("Current") : downloadTimeFormatted;
    };
    ;
    return ChangeGroupsComparedDialogController;
}());
exports.default = ChangeGroupsComparedDialogController;


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcSubpage_config_1 = __webpack_require__(62);
var vpcSubpage_controller_1 = __webpack_require__(64);
__webpack_require__(65);
exports.default = function (module) {
    module.controller("VpcSubpageController", vpcSubpage_controller_1.default);
    module.config(vpcSubpage_config_1.default);
};


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
VpcSubpageConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function VpcSubpageConfig($stateProvider) {
    $stateProvider.state("vpcSubpage", {
        url: "/ncm/vpcSubpage/:currentNodeId/:comparedNodeId/:vpcId",
        i18Title: "_t(vPC Configuration Details)",
        controller: "VpcSubpageController",
        controllerAs: "vm",
        template: __webpack_require__(63),
    });
}
exports.default = VpcSubpageConfig;
;


/***/ }),
/* 63 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content ng-init=vm.activate() id=vpcSubpage class=ncm-vpcSubpage> <xui-page-header> <div class=header-container> <div class=page-title _t> vPC Configuration Details </div> </div> </xui-page-header> <div class=busy xui-busy=!vm.isDataReady xui-busy-message={{vm.busyMessageString}}> <div class=vpc-details-container ng-if=vm.isDataReady> <vpc-interface-info class=vpc-interface-information-table id=vpc-interface-information-table-currentNode node-name=vm.currentNodeName vpc-status=vm.currentVpcStatus node-id=vm.currentNodeId config-changed-date=vm.currentConfigDownloadTime vpc-configuration=vm.currentVpcConfiguration member-interfaces-configuration=vm.currentMemberInterfacesConfiguration> </vpc-interface-info> <vpc-interface-info class=vpc-interface-information-table id=vpc-interface-information-table-comparedNode node-name=vm.comparedNodeName vpc-status=vm.comparedVpcStatus node-id=vm.comparedNodeId config-changed-date=vm.comparedConfigDownloadTime vpc-configuration=vm.comparedVpcConfiguration member-interfaces-configuration=vm.comparedMemberInterfacesConfiguration> </vpc-interface-info> </div> </div> </xui-page-content> ";

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var vpcInterface_1 = __webpack_require__(18);
var VpcSubpageController = /** @class */ (function () {
    /** @ngInject */
    VpcSubpageController.$inject = ["$scope", "$log", "$stateParams", "vpcSubpageService", "ncmNavigationService", "_t"];
    function VpcSubpageController($scope, $log, $stateParams, vpcSubpageService, ncmNavigationService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.vpcSubpageService = vpcSubpageService;
        this.ncmNavigationService = ncmNavigationService;
        this._t = _t;
        this.currentVpcConfiguration = [];
        this.currentMemberInterfacesConfiguration = [];
        this.comparedVpcConfiguration = [];
        this.comparedMemberInterfacesConfiguration = [];
        this.busyMessageString = this._t("Loading");
        this.activate = function () {
            _this.$log.info("VpcSubpageController:Activate()");
            _this.refreshSubpageData();
        };
        this.getNodeDetailsUrl = function (nodeId) {
            return _this.ncmNavigationService.getNodeDetailsUrl(nodeId);
        };
        this.getComparedNodeName = function () {
            if (_this.comparedNodeId < 0) {
                var msg = _this._t("Unknown");
                return Promise.resolve(msg);
            }
            return _this.vpcSubpageService
                .getVpcNodeName(_this.comparedNodeId);
        };
        this.currentNodeId = $stateParams["currentNodeId"];
        this.comparedNodeId = $stateParams["comparedNodeId"];
        this.vpcId = $stateParams["vpcId"];
    }
    VpcSubpageController.prototype.refreshSubpageData = function () {
        var _this = this;
        var timeStart = new Date().getTime();
        var promisedData = [
            this.vpcSubpageService.getVpcNodeName(this.currentNodeId),
            this.vpcSubpageService.getVpcInterfacesData(this.currentNodeId, this.vpcId),
            this.getComparedNodeName()
        ];
        Promise.all(promisedData).then(function (result) {
            _this.currentNodeName = result[0];
            _this.prepareVpcData(result[1]);
            _this.comparedNodeName = result[2];
            _this.$log.info("fetched vpc and node data in " + (new Date().getTime() - timeStart) + "ms");
            _this.$scope.$apply(function () { return _this.isDataReady = true; });
        });
    };
    VpcSubpageController.prototype.prepareVpcData = function (vpcInterfacesData) {
        this.prepareCurrentVpcData(vpcInterfacesData.localVpc);
        this.prepareComparedVpcData(vpcInterfacesData.remoteVpc);
    };
    VpcSubpageController.prototype.prepareCurrentVpcData = function (wholeInterface) {
        this.currentVpcStatus = wholeInterface.status;
        this.currentConfigDownloadTime = wholeInterface.configDownloadDateTime;
        if (this.currentVpcStatus !== vpcInterface_1.VpcStatus.ok) {
            return;
        }
        this.currentVpcConfiguration = wholeInterface.rawData;
        if (wholeInterface.relatedInterfaces == null) {
            return;
        }
        this.currentMemberInterfacesConfiguration = wholeInterface.relatedInterfaces.map(function (item) { return item.rawData; });
    };
    VpcSubpageController.prototype.prepareComparedVpcData = function (wholeInterface) {
        this.comparedVpcStatus = wholeInterface.status;
        this.comparedConfigDownloadTime = wholeInterface.configDownloadDateTime;
        if (this.comparedVpcStatus !== vpcInterface_1.VpcStatus.ok) {
            return;
        }
        this.comparedVpcConfiguration = wholeInterface.rawData;
        if (wholeInterface.relatedInterfaces == null) {
            return;
        }
        this.comparedMemberInterfacesConfiguration =
            wholeInterface.relatedInterfaces.map(function (item) { return item.rawData; });
    };
    return VpcSubpageController;
}());
exports.default = VpcSubpageController;


/***/ }),
/* 65 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(67);
var index_2 = __webpack_require__(73);
var index_3 = __webpack_require__(77);
var index_4 = __webpack_require__(82);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
};


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(68);
var editBaseline_controller_1 = __webpack_require__(69);
var editBaseline_config_1 = __webpack_require__(71);
var pasteFileDialog_controller_1 = __webpack_require__(19);
__webpack_require__(21);
exports.default = function (module) {
    module.controller("EditBaselineController", editBaseline_controller_1.default);
    module.controller("PasteFileDialogController", pasteFileDialog_controller_1.PasteFileDialogController);
    module.config(editBaseline_config_1.default);
};


/***/ }),
/* 68 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var baselineConfig_1 = __webpack_require__(11);
var pasteFileDialog_controller_1 = __webpack_require__(19);
var multiDeviceBaseline_1 = __webpack_require__(20);
var EditBaselineController = /** @class */ (function () {
    /** @ngInject */
    EditBaselineController.$inject = ["$scope", "$log", "$stateParams", "xuiDialogService", "baselineManagementService", "ncmNavigationService", "baselineAssignNodesDialogService", "ncmConfigComparisonService", "swDemoService", "_t"];
    function EditBaselineController($scope, $log, $stateParams, xuiDialogService, baselineManagementService, ncmNavigationService, baselineAssignNodesDialogService, ncmConfigComparisonService, swDemoService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.xuiDialogService = xuiDialogService;
        this.baselineManagementService = baselineManagementService;
        this.ncmNavigationService = ncmNavigationService;
        this.baselineAssignNodesDialogService = baselineAssignNodesDialogService;
        this.ncmConfigComparisonService = ncmConfigComparisonService;
        this.swDemoService = swDemoService;
        this._t = _t;
        this.baselineId = 0;
        this.baselineConfig = new baselineConfig_1.BaselineConfig();
        this.ignoredLines = [];
        this.pageTitle = this._t("New Baseline Config");
        this.baselines = [];
        this.applyBaselineToSourceNode = true;
        this.selectedNodes = [];
        this.selectedConfigTypes = [];
        this.baselineName = "";
        this.gridPagination = {
            page: 1,
            pageSize: 50
        };
        this.gridOptions = {
            hidePagination: false,
            hideSearch: false,
            pagerAdjacent: 2,
            searchableColumns: ["content"],
            searchPlaceholder: this._t("Search...")
        };
        this.ellipsisOptions = {
            tooltipText: "",
            tooltipOptions: {}
        };
        this.chooseLinesToIgnore = false;
        this.baselineContentIsEmpty = false;
        this.baselineNamePlaceholderText = this._t("Enter baseline name");
        this.baselineDescriptionPlaceholderText = this._t("Description");
        this.chooseLinesToIgnoreString = this._t("Ignoring lines such as interfaces/ports, hostnames, locations, timezones, vlans, will prevent false positives.");
        this.onSearch = function (item, cancellation) {
            _this.searchTerm = item;
            cancellation.then(function () {
                _this.onClear();
            });
        };
        this.onClear = function () {
            _this.searchTerm = "";
        };
        this.reset = function () {
            _this.baselineConfig.useComparisonCriterias = false;
            _this.chooseLinesToIgnore = false;
            _this.baselineConfig.lines = [];
            _this.ignoredLines = [];
        };
        this.baselineId = $stateParams["baselineId"];
        this.promotedConfigId = $stateParams["configId"];
        if (this.baselineId != null) {
            this.pageTitle = this._t("Edit Baseline Config");
        }
        else if (this.promotedConfigId != null) {
            this.isFromPromotedConfig = true;
        }
    }
    EditBaselineController.prototype.activate = function () {
        var _this = this;
        this.$log.info("EditBaselineController:Activate()");
        if (this.baselineId > 0) {
            this.baselineManagementService.getBaseline(this.baselineId).then(function (result) {
                _this.baselineConfig = result;
                _this.ignoredLines = _this.baselineConfig.lines.filter(function (line) { return line.isIgnored; });
                _this.chooseLinesToIgnore = _this.ignoredLines.length > 0;
                _this.selectedNodes = result.selectedNodes;
                _this.selectedConfigTypes = result.selectedConfigTypes;
            });
        }
        else if (this.isFromPromotedConfig) {
            this.baselineManagementService.getBaselineFromConfig(this.promotedConfigId).then(function (result) {
                _this.baselinePromotionDetails = result;
                _this.selectedNodes = [{
                        nodeId: result.nodeId,
                        nodeName: result.nodeName,
                        isSelected: true
                    }];
                _this.selectedConfigTypes = [result.configType];
                _this.baselineConfig.baselineName = result.nodeName + " " + result.configTitle;
                _this.$scope.$watch(function () { return _this.applyBaselineToSourceNode; }, function () {
                    return _this.onApplyBaselineToSourceNodeChanged();
                });
                _this.loadBaselineLines(result.config.split("\n"));
            });
        }
        this.$scope.$watch(function () { return _this.baselineConfig.useComparisonCriterias; }, function () {
            if (_this.baselineConfig.useComparisonCriterias) {
                _this.applyComparisonCriteria();
            }
            else {
                _this.resetComparisonCriteria();
            }
        });
    };
    EditBaselineController.prototype.checkDemo = function () {
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return true;
        }
        return false;
    };
    EditBaselineController.prototype.showApplyBaselineDialog = function () {
        var _this = this;
        var model = new multiDeviceBaseline_1.AssignNodesDialogModel(this.selectedNodes, this.selectedConfigTypes, this.baselineName);
        this.baselineAssignNodesDialogService.showDialog(model).then(function (result) {
            if (result) {
                var selection = model.getResultsCallback();
                _this.selectedNodes = selection.selectedNodes;
                _this.selectedConfigTypes = selection.selectedConfigTypes;
                if (_this.isFromPromotedConfig) {
                    _this.checkApplyBaselineToSourceNodeState();
                }
            }
        });
    };
    EditBaselineController.prototype.saveBaseline = function () {
        var _this = this;
        if (!this.isValid() || this.checkDemo()) {
            return;
        }
        var successCallback = function (baselineId) {
            _this.ncmNavigationService.navigateToBaselineManagement();
        };
        if (!this.chooseLinesToIgnore) {
            this.clearAllIgnoredLines();
        }
        this.baselineConfig.selectedNodes = this.selectedNodes;
        this.baselineConfig.selectedConfigTypes = this.selectedConfigTypes;
        if (this.baselineId > 0) {
            this.baselineManagementService.updateBaseline(this.baselineConfig).then(successCallback);
        }
        else {
            this.baselineManagementService.addBaseline(this.baselineConfig).then(successCallback);
        }
    };
    EditBaselineController.prototype.cancelBaseline = function () {
        this.ncmNavigationService.navigateToBaselineManagement();
    };
    EditBaselineController.prototype.goToComparisonCriteria = function () {
        if (this.checkDemo()) {
            return;
        }
        return this.ncmNavigationService.navigateToUrl(this.ncmNavigationService.getComparisonCriteriaUrl());
    };
    EditBaselineController.prototype.getComparisonCriteriaUrl = function () {
        return this.ncmNavigationService.getComparisonCriteriaUrl();
    };
    EditBaselineController.prototype.cbLineChange = function (item) {
        this.ignoreLine(item.index, item.isIgnored);
    };
    EditBaselineController.prototype.lineClick = function (item) {
        if (this.chooseLinesToIgnore) {
            this.ignoreLine(item.index, !item.isIgnored);
        }
    };
    EditBaselineController.prototype.removeLineFromIgnoredClick = function (item) {
        if (this.chooseLinesToIgnore) {
            this.ignoreLine(item.index, false);
        }
    };
    EditBaselineController.prototype.clearAllIgnoredLinesClick = function () {
        if (this.chooseLinesToIgnore) {
            this.clearAllIgnoredLines();
        }
    };
    EditBaselineController.prototype.escapeXmlTags = function (text) {
        if (text != null) {
            var regex = /</g;
            return text.replace(regex, "&lt;");
        }
    };
    EditBaselineController.prototype.clearAllIgnoredLines = function () {
        this.baselineConfig.lines.filter(function (line) { return line.isIgnored; }).forEach(function (line) { line.isIgnored = false; });
        this.ignoredLines = [];
    };
    EditBaselineController.prototype.ignoreLine = function (index, isIgnored) {
        _.find(this.baselineConfig.lines, function (line) { return line.index === index; }).isIgnored = isIgnored;
        this.ignoredLines = this.baselineConfig.lines.filter(function (line) { return line.isIgnored; });
    };
    EditBaselineController.prototype.isValid = function () {
        this.setDirtyOnFieldsWithError();
        this.baselineContentIsEmpty = this.baselineConfig.lines.length === 0;
        return this.baselineForm.$valid && !this.baselineContentIsEmpty;
    };
    EditBaselineController.prototype.setDirtyOnFieldsWithError = function () {
        angular.forEach(this.baselineForm.$error.required, function (field) {
            field.$setDirty();
        });
    };
    EditBaselineController.prototype.onFileChanged = function () {
        this.reset();
        if (this.file == null) {
            return;
        }
        var me = this;
        var fileReader = new FileReader();
        fileReader.readAsText(this.file);
        fileReader.onload = function () {
            var value = fileReader.result;
            me.loadBaselineLines(value.split("\n"));
            me.$scope.$apply();
        };
    };
    EditBaselineController.prototype.onApplyBaselineToSourceNodeChanged = function () {
        var _this = this;
        if (!this.applyBaselineToSourceNode) {
            this.selectedNodes = this.selectedNodes.filter(function (node) { return node.nodeId !== _this.baselinePromotionDetails.nodeId; });
            return;
        }
        if (!this.selectedNodes.some(function (node) { return node.nodeId === _this.baselinePromotionDetails.nodeId; })) {
            this.selectedNodes.push({
                nodeName: this.baselinePromotionDetails.nodeName,
                nodeId: this.baselinePromotionDetails.nodeId,
                isSelected: true
            });
        }
        if (!this.selectedConfigTypes.some(function (config) { return config === _this.baselinePromotionDetails.configType; })) {
            this.selectedConfigTypes.push(this.baselinePromotionDetails.configType);
        }
    };
    EditBaselineController.prototype.checkApplyBaselineToSourceNodeState = function () {
        var _this = this;
        this.applyBaselineToSourceNode =
            this.selectedNodes.some(function (node) { return node.nodeId === _this.baselinePromotionDetails.nodeId; })
                && this.selectedConfigTypes.some(function (type) { return type === _this.baselinePromotionDetails.configType; });
    };
    EditBaselineController.prototype.showPasteFileDialog = function () {
        var _this = this;
        var dialogViewModel = new pasteFileDialog_controller_1.PasteFileDialogViewModel();
        var setupDialogOptions = {
            title: this._t("Paste Baseline"),
            buttons: [
                {
                    name: "saveButton",
                    isPrimary: true,
                    text: this._t("SAVE"),
                    action: function (response) {
                        _this.reset();
                        var lines = dialogViewModel.getSubmittedResultCallback();
                        _this.loadBaselineLines(lines);
                    }
                },
                {
                    name: "cancelButton",
                    isPrimary: false,
                    text: this._t("CANCEL"),
                    action: function () {
                        return true;
                    }
                }
            ],
            viewModel: dialogViewModel
        };
        this.xuiDialogService.showModal({
            template: __webpack_require__(70)
        }, setupDialogOptions);
    };
    ;
    EditBaselineController.prototype.loadBaselineLines = function (lines) {
        var _this = this;
        if (lines == null) {
            this.baselineContentIsEmpty = true;
            return;
        }
        var counter = 0;
        lines.forEach(function (line) {
            _this.baselineConfig.lines[counter] = {
                index: counter++,
                content: line,
                isIgnored: false,
                isCaughtByComparisonCriteria: false
            };
        });
        this.baselineContentIsEmpty = this.baselineConfig.lines.length === 0;
    };
    EditBaselineController.prototype.applyComparisonCriteria = function () {
        var _this = this;
        this.ncmConfigComparisonService.getIgnoredLines(this.baselineConfig.lines.map(function (x) { return x.content; })).then(function (indexes) {
            indexes.forEach(function (index) {
                _this.baselineConfig.lines[index].isCaughtByComparisonCriteria = true;
            });
        });
    };
    EditBaselineController.prototype.resetComparisonCriteria = function () {
        this.baselineConfig.lines.forEach(function (line) { return line.isCaughtByComparisonCriteria = false; });
    };
    return EditBaselineController;
}());
exports.default = EditBaselineController;


/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"PasteFileDialogController as container\" ng-init=container.init(vm.dialogOptions.viewModel) class=ncm-paste-file-dialog> <xui-dialog> <div class=paste-file-dialog-content> <xui-textbox caption={{container.pasteFileDialogText}} ng-model=container.baseline rows=20 is-required=true></xui-textbox> </div> </xui-dialog> </div> ";

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
EditBaselineConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function EditBaselineConfig($stateProvider) {
    $stateProvider.state("editBaseline", {
        url: "/ncm/editBaseline?baselineId&configId",
        i18Title: "_t(Edit Baseline)",
        controller: "EditBaselineController",
        controllerAs: "vm",
        template: __webpack_require__(72)
    });
}
exports.default = EditBaselineConfig;


/***/ }),
/* 72 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content id=ncmEditBaselinePage class=edit-baseline-page ng-init=vm.activate() page-title={{vm.pageTitle}}> <div ng-form=vm.baselineForm> <div class=subsection> <span class=subtitle _t>Baseline Name</span> <xui-textbox id=baselineName class=textbox placeholder={{vm.baselineNamePlaceholderText}} ng-model=vm.baselineConfig.baselineName validators=\"required,maxlength=200\"> <div ng-message=required _t>Baseline name is required</div> </xui-textbox> </div> <div class=subsection> <span class=subtitle _t>Description</span> <xui-textbox id=baselineDescription class=textbox placeholder={{vm.baselineDescriptionPlaceholderText}} rows=3 ng-model=vm.baselineConfig.baselineDescription></xui-textbox> </div> <div class=subsection ng-if=vm.isFromPromotedConfig> <xui-checkbox id=applyBaselineToNodeCheckbox ng-model=vm.applyBaselineToSourceNode> <span _t>Apply to</span> <b>{{vm.baselinePromotionDetails.configTitle}}</b> <span _t>on</span> <b>{{vm.baselinePromotionDetails.nodeName}}</b>  </xui-checkbox></div> <div id=id-comparing-config-file-baseline class=subsection> <span class=subtitle _t>This baseline is:</span> <xui-radio id=id-radio-button-exact-match ng-model=vm.baselineConfig.exactMatching ng-value=true _t>A complete config file (should <b>exactly match</b> any configs it is compared to)</xui-radio> <xui-radio id=id-radio-button-contain ng-model=vm.baselineConfig.exactMatching ng-value=false _t>A snippet of a config file (compared configs should <b>contain</b> all of this snippet)</xui-radio> </div> <div class=subsection ng-if=!vm.isFromPromotedConfig> <span class=subtitle _t>File to use as baseline</span> <div class=button> <xui-button display-style=secondary class=baseline-paste-button ng-click=vm.showPasteFileDialog() _t>PASTE</xui-button> </div> <form novalidate> <div class=button> <xui-file-upload id=baselineFileUpload file=vm.file on-change=vm.onFileChanged()> </xui-file-upload> </div> </form> </div> <xui-divider></xui-divider> <div class=subsection> <span class=title _t>Configure</span> <xui-message type=error ng-if=vm.baselineContentIsEmpty> <b><span _t>Baseline content is required</span></b> </xui-message> <div class=selector-with-ignored-lines> <xui-grid id=baselineList class=multiline-selector items-source=vm.baselineConfig.lines searchPlaceholder=vm.gridOptions.searchPlaceholder stripe=false hide-toolbar=true row-padding=none smart-mode=true pagination-data=vm.gridPagination options=vm.gridOptions on-search=\"vm.onSearch(item, cancellation)\" on-clear=vm.onClear() template-url=mdb-item-template controller=vm> </xui-grid> <div class=ignored-lines> <div class=markup-baseline-section> <div class=title _t>Mark up baseline</div> <xui-checkbox id=cbComparisonCriteria ng-model=vm.baselineConfig.useComparisonCriterias> <span _t>Apply global comparison criteria</span> <div> <a href=# ng-click=vm.goToComparisonCriteria()>&#0187; <span _t>View/edit your comparison criteria</span></a> </div> </xui-checkbox> <xui-checkbox id=cbLinesToIgnore ng-model=vm.chooseLinesToIgnore help-text={{vm.chooseLinesToIgnoreString}} _t> Choose lines to ignore </xui-checkbox> </div> <div class=ignored-lines-section ng-if=\"vm.ignoredLines.length > 0\"> <div class=ignored-lines-toolbar> <div class=title> <span _t>Ignored lines: </span> ({{vm.ignoredLines.length}}) </div> <xui-button class=clear-all-button ng-click=vm.clearAllIgnoredLinesClick() display-style=link is-disabled=!vm.chooseLinesToIgnore _t>Clear all</xui-button> </div> <xui-listview class=ignored-lines-listview stripe=false row-padding=compact items-source=vm.ignoredLines template-url=ignored-line-item-template controller=vm> </xui-listview> </div> </div> </div> </div> <xui-divider></xui-divider> <div class=\"subsection align-right\"> <xui-button name=apply display-style=tertiary ng-click=vm.showApplyBaselineDialog()> <span _t>ASSIGN TO NODES </span> ({{vm.selectedNodes.length}}) </xui-button> <xui-button name=save display-style=primary ng-click=vm.saveBaseline() _t>SAVE</xui-button> <xui-button display-style=tertiary ng-click=vm.cancelBaseline() _t>CANCEL</xui-button> </div> </div> <script type=text/ng-template id=ignored-line-item-template> <div class=\"ignored-line\" ng-click=\"vm.removeLineFromIgnoredClick(item)\">\n            <div class=\"index\">{{item.index + 1}}</div>\n            <div xui-ellipsis ellipsis-options=\"vm.ellipsisOptions\" is-ellipsis-enabled=\"false\" class=\"content\">{{item.content}}</div>\n            <div class=\"icon-remove\">\n                <xui-icon icon-size=\"small\" icon=\"close\"></xui-icon>\n            </div>\n        </div> </script> <script type=text/ng-template id=mdb-item-template> <div class=\"line\">\n            <div class=\"checkbox\">\n                <xui-checkbox class=\"cbBaselineLine\" ng-model=\"item.isIgnored\" is-disabled=\"!vm.chooseLinesToIgnore\" ng-change=\"vm.cbLineChange(item)\"></xui-checkbox>\n            </div>\n            <div ng-class=\"{content: true, selected: item.isIgnored, caughtByComparisonCriteria: item.isCaughtByComparisonCriteria}\" ng-click=\"vm.lineClick(item)\">\n                <span ng-bind-html=\"vm.escapeXmlTags(item.content) | xuiHighlight:vm.escapeXmlTags(vm.searchTerm)\"></span>\n            </div>\n            <div class=\"index\" ng-click=\"vm.lineClick(item)\">{{item.index + 1}}</div>\n        </div> </script> </xui-page-content>";

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baselineDiff_controller_1 = __webpack_require__(74);
var baselineDiff_config_1 = __webpack_require__(75);
__webpack_require__(21);
exports.default = function (module) {
    module.controller("BaselineDiffController", baselineDiff_controller_1.default);
    module.config(baselineDiff_config_1.default);
};


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var BaselineDiffController = /** @class */ (function () {
    /** @ngInject */
    BaselineDiffController.$inject = ["$scope", "$log", "$stateParams", "$q", "baselineManagementService", "ncmConfigService", "ncmNavigationService", "_t"];
    function BaselineDiffController($scope, $log, $stateParams, $q, baselineManagementService, ncmConfigService, ncmNavigationService, _t) {
        this.$scope = $scope;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.$q = $q;
        this.baselineManagementService = baselineManagementService;
        this.ncmConfigService = ncmConfigService;
        this.ncmNavigationService = ncmNavigationService;
        this._t = _t;
        this.baselineId = 0;
        this.baselineDiffPageTitle = this._t("Baseline Diff");
        this.baselineId = $stateParams["baselineId"];
        this.configId = $stateParams["configId"];
        this.refreshBaselineInfo(this.baselineId, this.configId);
        this.refreshBaselineDiff(this.baselineId, this.configId);
    }
    BaselineDiffController.prototype.loadMoreLines = function (index, size) {
        var lines = this.allDiffLines.slice(index, index + size);
        return this.$q.resolve(lines);
    };
    BaselineDiffController.prototype.getNodeDetailsUrl = function () {
        return this.ncmNavigationService.getNodeDetailsUrl(this.nodeId);
    };
    BaselineDiffController.prototype.getBaselineDetailsUrl = function () {
        return this.ncmNavigationService.getEditBaselineUrl(this.baselineId);
    };
    BaselineDiffController.prototype.refreshBaselineInfo = function (baselineId, configId) {
        var _this = this;
        this.baselineManagementService.getBaselineName(baselineId).then(function (name) {
            return _this.baselineName = name;
        });
        this.ncmConfigService.getConfigDetails(configId).then(function (details) {
            _this.configName = details.configName;
            _this.nodeName = details.nodeName;
            _this.nodeId = details.nodeId;
        });
    };
    BaselineDiffController.prototype.refreshBaselineDiff = function (baselineId, configId) {
        var _this = this;
        this.baselineManagementService.getBaselineDiff(baselineId, configId)
            .then(function (diff) {
            _this.baselineDiff = {
                numberOfLines: diff.numberOfLines,
                hunks: []
            };
            _this.allDiffLines = [];
            diff.hunks.forEach(function (hunk) {
                if (hunk.show) {
                    _this.baselineDiff.hunks.push(hunk);
                }
                _this.allDiffLines = _this.allDiffLines.concat(hunk.lines);
            });
            _this.$log.info("Baseline diff between baseline " + baselineId + " and config " + configId + " is ready!");
        });
    };
    return BaselineDiffController;
}());
exports.default = BaselineDiffController;


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
BaselineDiffConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function BaselineDiffConfig($stateProvider) {
    $stateProvider.state("baselineDiff", {
        url: "/ncm/baselineDiff/:baselineId/:configId",
        i18Title: "_t(Baseline Diff)",
        controller: "BaselineDiffController",
        controllerAs: "vm",
        params: {
            baselineId: { squash: true, value: null },
            configId: { squash: true, value: null },
        },
        template: __webpack_require__(76)
    });
}
exports.default = BaselineDiffConfig;


/***/ }),
/* 76 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=baseline-diff-page ng-init=vm.activate() page-title={{vm.baselineDiffPageTitle}}> <sw-side-by-side-text-diff id=side-by-side-diff document-diff=vm.baselineDiff load-more-lines-fn=vm.loadMoreLines(index,size)> <sw-side-by-side-text-diff-left-header> <div class=\"side-title ncm-text-ellipsis\"><a href={{vm.getBaselineDetailsUrl()}}>{{vm.baselineName}}</a></div> <div class=side-description _t>baseline</div> </sw-side-by-side-text-diff-left-header> <sw-side-by-side-text-diff-right-header> <div class=side-title><a href={{vm.getNodeDetailsUrl()}}>{{vm.nodeName}}</a></div> <div class=side-description>{{vm.configName}}</div> </sw-side-by-side-text-diff-right-header> </sw-side-by-side-text-diff> </xui-page-content>";

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baselineManagement_controller_1 = __webpack_require__(78);
var baselineManagement_config_1 = __webpack_require__(80);
exports.default = function (module) {
    module.controller("BaselineManagementController", baselineManagement_controller_1.default);
    module.config(baselineManagement_config_1.default);
};


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var baselineConfig_1 = __webpack_require__(11);
var multiDeviceBaseline_1 = __webpack_require__(20);
var ncmAccountRole_1 = __webpack_require__(79);
var GridState = /** @class */ (function () {
    function GridState() {
    }
    return GridState;
}());
var NavigationTab = /** @class */ (function () {
    function NavigationTab() {
    }
    return NavigationTab;
}());
var BaselineManagementController = /** @class */ (function () {
    /** @ngInject */
    BaselineManagementController.$inject = ["$scope", "$log", "ncmNavigationService", "baselineManagementService", "ncmAccountsService", "xuiDialogService", "baselineAssignNodesDialogService", "$q", "$interval", "swDemoService", "_t"];
    function BaselineManagementController($scope, $log, ncmNavigationService, baselineManagementService, ncmAccountsService, xuiDialogService, baselineAssignNodesDialogService, $q, $interval, swDemoService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.ncmNavigationService = ncmNavigationService;
        this.baselineManagementService = baselineManagementService;
        this.ncmAccountsService = ncmAccountsService;
        this.xuiDialogService = xuiDialogService;
        this.baselineAssignNodesDialogService = baselineAssignNodesDialogService;
        this.$q = $q;
        this.$interval = $interval;
        this.swDemoService = swDemoService;
        this._t = _t;
        this.baselineTabId = "baselineMngmtTab";
        this.configMgmtTabId = "configMngmtTab";
        this.inventoryStatusTabId = "inventoryStatusTab";
        this.scriptMngmtTabId = "scriptMngmtTab";
        this.transferStatusTabId = "transferStatusTab";
        this.baselines = [];
        this.isBusy = true;
        this.searchTerm = "";
        this.baselineConfig = null;
        this.showManagementButtons = false;
        this.navigationTabs = [];
        this.tabsInitialized = false;
        this.gridOptions = {
            hidePagination: false,
            searchPlaceholder: this._t("Search..."),
        };
        this.sorting = {
            sortableColumns: [
                { id: "baselineName", label: this._t("Name") }
            ],
            sortBy: {
                id: "baselineName",
                label: this._t("Name")
            },
            direction: "asc"
        };
        this.pagination = {
            page: 1,
            pageSize: 25
        };
        this.sidebarSettings = {
            display: false,
            position: "right",
            shrink: false
        };
        this.encode = function (text) {
            return _.escape(text);
        };
        this.addBaseline = function () {
            _this.ncmNavigationService.navigateToAddEditBaseline();
        };
        this.editBaseline = function () {
            var params = {
                baselineId: _this.selection.items[0]
            };
            _this.ncmNavigationService.navigateToAddEditBaseline(params);
        };
        this.deleteBaselines = function () {
            if (_this.checkDemo()) {
                return;
            }
            var _baselinesIds = _this.selection.items;
            return _this.baselineManagementService.deleteBaselines(_baselinesIds);
        };
        this.enableBaselines = function () { return __awaiter(_this, void 0, void 0, function () {
            var _baselinesIds;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.checkDemo() || !this.hasItemsSelected()) {
                            return [2 /*return*/];
                        }
                        _baselinesIds = this.selection.items;
                        return [4 /*yield*/, this.baselineManagementService.enableBaselines(_baselinesIds)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.loadBaselines()];
                    case 2:
                        _a.sent();
                        this.requestUpdateBaselineCache();
                        return [2 /*return*/];
                }
            });
        }); };
        this.disableBaselines = function () { return __awaiter(_this, void 0, void 0, function () {
            var _baselinesIds;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.checkDemo() || !this.hasItemsSelected()) {
                            return [2 /*return*/];
                        }
                        _baselinesIds = this.selection.items;
                        return [4 /*yield*/, this.baselineManagementService.disableBaselines(_baselinesIds)];
                    case 1:
                        _a.sent();
                        this.refreshState();
                        return [2 /*return*/];
                }
            });
        }); };
        this.showDeleteBaselinesDialog = function () {
            if (_this.checkDemo()) {
                return;
            }
            _this.xuiDialogService.showModal(null, {
                title: _this._t("Delete Baseline(s)"),
                message: _this._t("Are you sure you want to delete selected baseline(s)?"),
                buttons: [
                    {
                        name: "delete",
                        isPrimary: true,
                        text: _this._t("DELETE"),
                        action: function (dialogResult) {
                            _this.deleteBaselines().then(function () {
                                _this.refreshState();
                            });
                            return true;
                        }
                    },
                    {
                        name: "cancel",
                        isPrimary: false,
                        text: _this._t("Cancel"),
                        action: function () {
                            return true;
                        }
                    }
                ]
            });
        };
        this.requestUpdateBaselineCache = function () {
            if (_this.checkDemo()) {
                return;
            }
            var baselineIds = _this.selection.items;
            _this.baselineManagementService.requestUpdateBaselineCache(baselineIds).then(function () {
                _this.refreshState();
            });
        };
        this.search = function (item, cancellation) {
            _this.gridState.searchTerm = item;
            _this.loadBaselines();
        };
        this.clearSearch = function () {
            _this.gridState.searchTerm = "";
            _this.loadBaselines();
        };
        this.pageChange = function (page, pageSize, total) {
            _this.gridState.page = page;
            _this.gridState.pageSize = pageSize;
            _this.loadBaselines();
        };
        this.sortingChange = function (oldValue, newValue) {
            _this.gridState.sortBy = newValue.sortBy.id;
            _this.gridState.sortDirection = newValue.direction;
            _this.loadBaselines();
        };
        this.loadBaselines = function () {
            _this.isBusy = true;
            return _this.baselineManagementService.getBaselines({
                currentPage: _this.gridState.page,
                pageSize: _this.gridState.pageSize,
                search: _this.gridState.searchTerm || "",
                sortByColumn: _this.gridState.sortBy,
                sortByDirection: _this.gridState.sortDirection,
                filterValues: null
            }).then(function (result) {
                _this.baselines = result.baselines;
                _this.pagination.total = result.totalCount;
                _this.isBusy = false;
                if (_this.baselineConfig) {
                    _this.baselineConfig = _.find(_this.baselines, function (x) { return x.baselineId === _this.baselineConfig.baselineId; });
                }
            });
        };
        this.InitUserPermissions = function () {
            _this.isBusy = true;
            _this.ncmAccountsService.getAccountNCMRole()
                .then(function (role) {
                _this.isBusy = false;
                _this.InitNavigationTabs(role);
                _this.InitManagementButtons(role);
            });
        };
        this.InitNavigationTabs = function (ncmRole) {
            var configMngmtTab = {
                tabId: _this.configMgmtTabId,
                tabTitle: _this._t("CONFIG MANAGEMENT")
            };
            var transferStatusTab = {
                tabId: _this.transferStatusTabId,
                tabTitle: _this._t("TRANSFER STATUS")
            };
            var inventoryStatusTab = {
                tabId: _this.inventoryStatusTabId,
                tabTitle: _this._t("INVENTORY STATUS")
            };
            var scriptMngmtTab = {
                tabId: _this.scriptMngmtTabId,
                tabTitle: _this._t("SCRIPT MANAGEMENT")
            };
            var baselineMngmtTab = {
                tabId: _this.baselineTabId,
                tabTitle: _this._t("BASELINE MANAGEMENT")
            };
            switch (ncmRole) {
                case ncmAccountRole_1.NCMAccountRole.WebViewer:
                    _this.navigationTabs.push(configMngmtTab, baselineMngmtTab);
                    break;
                case ncmAccountRole_1.NCMAccountRole.WebDownloader:
                    _this.navigationTabs.push(configMngmtTab, transferStatusTab, inventoryStatusTab, baselineMngmtTab);
                    break;
                case ncmAccountRole_1.NCMAccountRole.WebUploader:
                case ncmAccountRole_1.NCMAccountRole.Engineer:
                case ncmAccountRole_1.NCMAccountRole.Administrator:
                    _this.navigationTabs.push(configMngmtTab, transferStatusTab, inventoryStatusTab, scriptMngmtTab, baselineMngmtTab);
                    console.log(_this.navigationTabs.length);
                    break;
            }
            _this.tabsInitialized = true;
        };
        this.InitManagementButtons = function (ncmRole) {
            switch (ncmRole) {
                case ncmAccountRole_1.NCMAccountRole.None:
                case ncmAccountRole_1.NCMAccountRole.WebViewer:
                case ncmAccountRole_1.NCMAccountRole.WebDownloader:
                    _this.showManagementButtons = false;
                    break;
                case ncmAccountRole_1.NCMAccountRole.WebUploader:
                case ncmAccountRole_1.NCMAccountRole.Engineer:
                case ncmAccountRole_1.NCMAccountRole.Administrator:
                    _this.showManagementButtons = true;
                    break;
            }
        };
        this.inspectBaseline = function (item) {
            _this.baselineConfig = item;
            _this.sidebarSettings.display = true;
        };
        this.hasItemsSelected = function () {
            return _this.selection != null && _this.selection.items != null && _this.selection.items.length >= 1;
        };
        this.showBaselineViolationStatus = function (item) {
            return item.disabled === false && item.baselineViolationStatus !== baselineConfig_1.BaselineViolationState.Unknown;
        };
        this.initGridState = function () {
            _this.gridState = new GridState();
            _this.gridState.page = _this.pagination.page;
            _this.gridState.pageSize = _this.pagination.pageSize;
            _this.gridState.searchTerm = "";
            _this.gridState.sortBy = _this.sorting.sortBy.id;
            _this.gridState.sortDirection = _this.sorting.direction;
        };
        this.updateBaselinState = function () {
            var ids = _this.baselines.map(function (x) { return x.baselineId; });
            if (ids.length > 0) {
                _this.baselineManagementService.fetchBaselinesState(ids)
                    .then(function (states) {
                    states.forEach(function (x) {
                        _.find(_this.baselines, function (y) { return x.id === y.baselineId; }).baselineViolationStatus = x.state;
                    });
                });
            }
        };
    }
    BaselineManagementController.prototype.activate = function () {
        var _this = this;
        this.$log.info("BaselineManagementController:Activate()");
        this.InitUserPermissions();
        this.initGridState();
        this.loadBaselines();
        this.intervalPromise = this.$interval(function () { return _this.updateBaselinState(); }, 6 * 1000);
        this.$scope.$on("$destroy", function () {
            _this.$interval.cancel(_this.intervalPromise);
        });
    };
    BaselineManagementController.prototype.checkDemo = function () {
        if (this.swDemoService.isDemoMode()) {
            this.swDemoService.showDemoErrorToast();
            return true;
        }
        return false;
    };
    BaselineManagementController.prototype.refreshState = function () {
        if (this.selection !== undefined) {
            this.selection.items = [];
        }
        return this.loadBaselines();
    };
    BaselineManagementController.prototype.showAssignNodesDialog = function () {
        var _this = this;
        var baselineId = this.selection.items[0];
        var nodes = this.baselineManagementService.getBaselineNodes(baselineId);
        var configTypes = this.baselineManagementService.getBaselineConfigTypes(baselineId);
        var baselineName = this.baselineManagementService.getBaselineName(baselineId);
        this.$q.all([nodes, configTypes, baselineName]).then(function (result) {
            var model = new multiDeviceBaseline_1.AssignNodesDialogModel(result[0], result[1], result[2]);
            _this.baselineAssignNodesDialogService.showDialog(model)
                .then(function (saved) { return __awaiter(_this, void 0, void 0, function () {
                var selection;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(saved && !this.checkDemo())) return [3 /*break*/, 2];
                            selection = model.getResultsCallback();
                            return [4 /*yield*/, this.baselineManagementService.assignBaselineNodes(baselineId, selection.selectedNodes, selection.selectedConfigTypes)];
                        case 1:
                            _a.sent();
                            this.selection.items = [];
                            this.loadBaselines();
                            _a.label = 2;
                        case 2: return [2 /*return*/];
                    }
                });
            }); });
        });
    };
    return BaselineManagementController;
}());
exports.default = BaselineManagementController;


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NCMAccountRole;
(function (NCMAccountRole) {
    NCMAccountRole[NCMAccountRole["None"] = 0] = "None";
    NCMAccountRole[NCMAccountRole["WebViewer"] = 1] = "WebViewer";
    NCMAccountRole[NCMAccountRole["WebDownloader"] = 2] = "WebDownloader";
    NCMAccountRole[NCMAccountRole["WebUploader"] = 3] = "WebUploader";
    NCMAccountRole[NCMAccountRole["Engineer"] = 4] = "Engineer";
    NCMAccountRole[NCMAccountRole["Administrator"] = 5] = "Administrator";
})(NCMAccountRole = exports.NCMAccountRole || (exports.NCMAccountRole = {}));


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
BaselineManagementConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function BaselineManagementConfig($stateProvider) {
    $stateProvider.state("baselineManagement", {
        url: "/ncm/baselineManagement",
        i18Title: "_t(Baseline Management)",
        controller: "BaselineManagementController",
        controllerAs: "vm",
        template: __webpack_require__(81)
    });
    $stateProvider.state("baselineManagement.configMngmtTab", {
        onEnter: function () {
            window.location.href = "/Orion/NCM/ConfigurationManagement.aspx";
        }
    });
    $stateProvider.state("baselineManagement.transferStatusTab", {
        onEnter: function () {
            window.location.href = "/Orion/NCM/TransferStatus.aspx";
        }
    });
    $stateProvider.state("baselineManagement.inventoryStatusTab", {
        onEnter: function () {
            window.location.href = "/Orion/NCM/Resources/InventoryReports/InventoryStatus.aspx";
        }
    });
    $stateProvider.state("baselineManagement.scriptMngmtTab", {
        onEnter: function () {
            window.location.href = "/Orion/NCM/SnippetManagement.aspx";
        }
    });
    $stateProvider.state("baselineManagement.baselineMngmtTab", {
        url: "/baselineManagement"
    });
}
exports.default = BaselineManagementConfig;


/***/ }),
/* 81 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=baseline-management-page ng-init=vm.activate() page-layout=fill> <xui-page-header> <div class=xui-page-content__header> <h1 class=\"xui-h1 xui-page-content__header-title\" _t>Baseline Management</h1> <ncm-help-button help-url-fragment=OrionNCMBaselineManagement></ncm-help-button> </div> <xui-tabs id=navigationTab class=navigation-tabs selected-tab-id=vm.baselineTabId xui-tabs-router ng-if=vm.tabsInitialized> <xui-tab ng-repeat=\"tab in vm.navigationTabs\" tab-id={{tab.tabId}} tab-title={{tab.tabTitle}}></xui-tab> </xui-tabs> </xui-page-header> <xui-sidebar-container class=baseline-list-container layout=fill> <xui-grid id=mainGrid smart-mode=false items-source=vm.baselines template-url=baseline-list-template selection-property=baselineId options=vm.gridOptions selection-mode=multi show-selector=true sorting-data=vm.sorting pagination-data=vm.pagination selection=vm.selection busy=vm.isBusy on-search=\"vm.search(item, cancellation)\" on-clear=vm.clearSearch() on-pagination-change=\"vm.pageChange(page, pageSize, total)\" on-sorting-change=vm.sortingChange(oldValue,newValue) allow-select-all-pages=false layout=fill controller=vm> <xui-grid-toolbar-container> <xui-toolbar ng-if=vm.showManagementButtons> <xui-toolbar-item> <xui-button icon=add class=add-baseline-button display-style=tertiary ng-click=vm.addBaseline() _t>New baseline</xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button icon=edit display-style=tertiary ng-click=vm.editBaseline() is-disabled=\"vm.selection.items.length !== 1\" _t>Edit</xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button name=applyNodes icon=select-all display-style=tertiary ng-click=vm.showAssignNodesDialog() is-disabled=\"vm.selection.items.length !== 1\" _t>Apply/Remove</xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button name=enable icon=select-all display-style=tertiary ng-click=vm.enableBaselines() is-disabled=!vm.hasItemsSelected() _t>Enable</xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button name=disable icon=disable display-style=tertiary ng-click=vm.disableBaselines() is-disabled=!vm.hasItemsSelected() _t>Disable</xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button icon=reload name=update display-style=tertiary ng-click=vm.requestUpdateBaselineCache() is-disabled=\"vm.selection.items.length < 1\" _t>Update</xui-button> </xui-toolbar-item> <xui-toolbar-item> <xui-button class=delete-baselines-button icon=delete display-style=tertiary ng-click=vm.showDeleteBaselinesDialog() is-disabled=\"vm.selection.items.length < 1\" _t>Delete</xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-grid> <xui-sidebar settings=vm.sidebarSettings> <baseline-inspector id=baseline-inspector-id baseline-config=vm.baselineConfig on-close=\"vm.sidebarSettings.display = false;\"> </baseline-inspector> </xui-sidebar> </xui-sidebar-container> <script type=text/ng-template id=baseline-list-template> <div class=\"row\" style=\"margin-top:3px;\" ng-class=\"{'ncm-baseline-disabled': item.disabled}\">\n            <div class=\"col-md-3 ncm-text-ellipsis\">\n                <span class=\"ncm-baseline-name\" ng-bind-html=\"vm.encode(item.baselineName) | xuiHighlight:vm.searchTerm\"></span>\n            </div>\n            <div class=\"col-md-4 ncm-text-ellipsis\">\n                {{::item.baselineDescription}}\n            </div>\n            <div class=\"col-md-2\">\n                <div ng-if=\"vm.showBaselineViolationStatus(item)\">\n                    <xui-button \n                        class=\"ncm-baseline-violation-status\"\n                        icon=\"{{item.baselineViolationStatus | baselineViolationStatusIconFilter}}\"\n                        ng-click=\"vm.inspectBaseline(item)\"\n                        display-style=\"link\">{{item.baselineViolationStatus | baselineViolationStatusTextFilter}}\n                    </xui-button>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <a ng-if=\"item.nodesCount === 1\" target=\"__blank\" href=\"{{item.nodeDetailsUrl}}\">\n                    <span class=\"ncm-baseline-nodename\"> \n                        {{::item.nodeName}}\n                    </span> \n                </a>\n                <span ng-if=\"item.nodesCount > 1\">\n                    {{::item.nodesCount}} \n                    <span _t>nodes</span>\n                </span>\n                <div ng-if=\"item.nodesCount > 0\" class=\"hint\" _t>Applied to</div>\n            </div>\n            <div class=\"col-md-1 text-center\">\n                <xui-icon\n                    name=\"inspectBaseline\"\n                    icon=\"caret-right\" \n                    ng-click=\"vm.inspectBaseline(item)\" \n                    ng-if=\"item.nodesCount > 0 && !item.disabled\">\n                </xui-icon>\n            </div>\n        </div> </script> </xui-page-content> ";

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baselineAssignNodesDialog_controller_1 = __webpack_require__(83);
var baselineAssignNodesDialogService_1 = __webpack_require__(84);
exports.default = function (module) {
    module.service("baselineAssignNodesDialogService", baselineAssignNodesDialogService_1.BaselineAssignNodesDialogService);
    module.controller("baselineAssignNodesDialogController", baselineAssignNodesDialog_controller_1.BaselineAssignNodesDialogController);
};


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var BaselineAssignNodesDialogController = /** @class */ (function () {
    //** @ngInject */
    BaselineAssignNodesDialogController.$inject = ["assignedData"];
    function BaselineAssignNodesDialogController(assignedData) {
        var _this = this;
        this.assignedData = assignedData;
        this.isApplyMode = false;
        this.atLeastOneNodeSelected = true;
        this.atLeastOneConfigTypeSelected = true;
        this.validate = function () {
            _this.atLeastOneNodeSelected = true;
            _this.atLeastOneConfigTypeSelected = true;
            if (_this.isApplyMode) {
                return _this.validateApplyMode();
            }
            else {
                return _this.validateRemoveMode();
            }
        };
        this.validateApplyMode = function () {
            if (_this.assignedData.selectedNodes.length === 0) {
                _this.atLeastOneNodeSelected = false;
            }
            else if (_this.assignedData.selectedConfigTypes.length === 0) {
                _this.atLeastOneConfigTypeSelected = false;
            }
            return _this.atLeastOneNodeSelected && _this.atLeastOneConfigTypeSelected;
        };
        this.validateRemoveMode = function () {
            if (_this.assignedData.selectedNodes.length > 0 && _this.assignedData.selectedConfigTypes.length === 0) {
                _this.atLeastOneConfigTypeSelected = false;
            }
            return _this.atLeastOneConfigTypeSelected;
        };
    }
    ;
    BaselineAssignNodesDialogController.prototype.init = function () {
        this.isApplyMode = this.assignedData.selectedNodes.length === 0;
    };
    return BaselineAssignNodesDialogController;
}());
exports.BaselineAssignNodesDialogController = BaselineAssignNodesDialogController;


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var BaselineAssignNodesDialogService = /** @class */ (function () {
    /** @ngInject */
    BaselineAssignNodesDialogService.$inject = ["xuiDialogService", "$controller", "_t"];
    function BaselineAssignNodesDialogService(xuiDialogService, $controller, _t) {
        this.xuiDialogService = xuiDialogService;
        this.$controller = $controller;
        this._t = _t;
    }
    BaselineAssignNodesDialogService.prototype.showDialog = function (assignNodesModel) {
        var preselectedNodes = assignNodesModel.selectedNodes.map(function (x) { return x; });
        var preselectedConfigTypes = assignNodesModel.selectedConfigTypes.map(function (x) { return x; });
        var dialogData = {
            selectedNodes: preselectedNodes,
            selectedConfigTypes: preselectedConfigTypes
        };
        var dlgController = this.$controller("baselineAssignNodesDialogController", {
            assignedData: dialogData
        });
        var dlgSettings = {
            template: __webpack_require__(85),
            size: "assign-remove"
        };
        var dlgOptions = {
            title: this._t("APPLY/REMOVE"),
            message: assignNodesModel.baselineName,
            viewModel: dlgController,
            hideTopCancel: true,
            actionButtonText: this._t("SAVE"),
            cancelButtonText: this._t("CANCEL"),
            validate: function () {
                return dlgController.validate();
            }
        };
        return this.xuiDialogService.showModal(dlgSettings, dlgOptions).then(function (result) {
            if (result === "cancel") {
                return false;
            }
            assignNodesModel.getResultsCallback = function () {
                return {
                    selectedNodes: dialogData.selectedNodes,
                    selectedConfigTypes: dialogData.selectedConfigTypes
                };
            };
            return true;
        });
    };
    return BaselineAssignNodesDialogService;
}());
exports.BaselineAssignNodesDialogService = BaselineAssignNodesDialogService;


/***/ }),
/* 85 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog id=baseline-assign-nodes-dialog ng-init=vm.dialogOptions.viewModel.init() class=baseline-assign-nodes-dialog> <div _t>Set the <b>{{vm.dialogOptions.message}}</b> baseline to the following nodes:</div> <xui-divider></xui-divider> <xui-message type=error ng-if=!vm.dialogOptions.viewModel.atLeastOneNodeSelected><b><span _t>Please select at least one node</span></b></xui-message> <xui-message type=error ng-if=!vm.dialogOptions.viewModel.atLeastOneConfigTypeSelected><b><span _t>select at least one config type</span></b></xui-message> <node-selector class=node-selector selection=vm.dialogOptions.viewModel.assignedData filter-binary-configs=true></node-selector> </xui-dialog>";

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var configTypes_controller_1 = __webpack_require__(87);
var configTypes_config_1 = __webpack_require__(89);
__webpack_require__(91);
var selectNodeDialog_controller_1 = __webpack_require__(92);
exports.default = function (module) {
    module.controller("NcmConfigTypesController", configTypes_controller_1.default);
    module.controller("NcmSelectNodeDialogController", selectNodeDialog_controller_1.default);
    module.config(configTypes_config_1.default);
};


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var crumb_1 = __webpack_require__(88);
var require_1 = __webpack_require__(14);
var NcmConfigTypesController = /** @class */ (function () {
    /** @ngInject */
    NcmConfigTypesController.$inject = ["$log", "ncmConfigTypesService", "ncmErrorHandlerService", "ncmSelectNodesDialogService", "ncmMessageBoxService", "_t"];
    function NcmConfigTypesController($log, ncmConfigTypesService, ncmErrorHandlerService, ncmSelectNodesDialogService, ncmMessageBoxService, _t) {
        var _this = this;
        this.$log = $log;
        this.ncmConfigTypesService = ncmConfigTypesService;
        this.ncmErrorHandlerService = ncmErrorHandlerService;
        this.ncmSelectNodesDialogService = ncmSelectNodesDialogService;
        this.ncmMessageBoxService = ncmMessageBoxService;
        this._t = _t;
        this.longOperationsCount = 0;
        this.isBusy = false;
        this.showError = false;
        this.assignVendors = function (assignedVendors) {
            _this.isBusy = true;
            _this.ncmConfigTypesService.assignVendors(assignedVendors)
                .catch(function (ex) {
                _this.ncmErrorHandlerService.handle(ex);
            })
                .finally(function () {
                _this.isBusy = false;
            });
        };
    }
    NcmConfigTypesController.prototype.activate = function () {
        this.$log.info("ConfigTypesController:Activate()");
        this.settingsCrumbs = [
            new crumb_1.Crumb(this._t("Admin"), "/Orion/Admin/"),
            new crumb_1.Crumb(this._t("NCM Settings"), "/Orion/NCM/Admin/"),
            new crumb_1.Crumb(this._t("Config Types"), "")
        ];
        this.refreshConfigTypes();
    };
    NcmConfigTypesController.prototype.startLongOperation = function () {
        this.longOperationsCount++;
        this.isBusy = true;
    };
    NcmConfigTypesController.prototype.finishLongOperation = function () {
        this.longOperationsCount--;
        if (this.longOperationsCount === 0) {
            this.isBusy = false;
        }
    };
    NcmConfigTypesController.prototype.refreshConfigTypes = function () {
        var _this = this;
        this.startLongOperation();
        this.ncmConfigTypesService.getAll()
            .then(function (ct) {
            _this.configTypes = ct;
            _this.$log.info("ConfigTypesController: Got all config types");
        })
            .catch(function (ex) {
            _this.ncmErrorHandlerService.handle(ex);
        })
            .finally(function () {
            _this.finishLongOperation();
        });
    };
    NcmConfigTypesController.prototype.showValidationError = function (msg) {
        if (msg == null) {
            this.validationError = "";
            this.showError = false;
            return true;
        }
        this.validationError = msg;
        this.showError = true;
        return false;
    };
    NcmConfigTypesController.prototype.validateNewConfig = function () {
        var _this = this;
        if (this.newName == null || this.newName.trim() === "") {
            return this.showValidationError(this._t("Enter a name for the new Config Type first"));
        }
        if (_.find(this.configTypes, function (c) { return c.name.toUpperCase() === _this.newName.toUpperCase(); }) != null) {
            return this.showValidationError(this._t("The provided config type already exists"));
        }
        if (this.newName.length > 50) {
            return this.showValidationError(this._t("The length of provided config type name should be less then 50"));
        }
        return this.showValidationError(null);
    };
    NcmConfigTypesController.prototype.add = function () {
        var _this = this;
        if (!this.validateNewConfig()) {
            return;
        }
        this.startLongOperation();
        this.ncmConfigTypesService.create(this.newName)
            .then(function (x) {
            _this.$log.info("ConfigTypesController: Created a new config type");
            _this.refreshConfigTypes();
            _this.newName = "";
        })
            .catch(function (ex) {
            _this.ncmErrorHandlerService.handle(ex);
        })
            .finally(function () {
            _this.finishLongOperation();
        });
    };
    NcmConfigTypesController.prototype.delete = function (item) {
        var _this = this;
        require_1.Require.notNull(item, "item");
        this.$log.info("Start deleting a config type '" + item.name + "'");
        this.ncmMessageBoxService.showYesNoQuestion(this._t("Question"), this._t("Do you want to delete the config type?"), function () { return _this.startDelete(item); });
    };
    NcmConfigTypesController.prototype.edit = function (item) {
        require_1.Require.notNull(item, "item");
        this.$log.info("Start editing a config type '" + item.name + "'");
        this.ncmSelectNodesDialogService.showDialog(item, this.assignVendors);
    };
    NcmConfigTypesController.prototype.startDelete = function (item) {
        var _this = this;
        this.startLongOperation();
        this.ncmConfigTypesService.delete(item.id)
            .then(function (x) {
            _this.$log.info("ConfigTypesController: Deleted a new config type");
            _this.refreshConfigTypes();
        })
            .catch(function (ex) {
            _this.ncmErrorHandlerService.handle(ex);
        })
            .finally(function () {
            _this.finishLongOperation();
        });
    };
    return NcmConfigTypesController;
}());
exports.default = NcmConfigTypesController;


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Crumb = /** @class */ (function () {
    function Crumb(title, url) {
        this.title = title;
        this.url = url;
    }
    return Crumb;
}());
exports.Crumb = Crumb;


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


NcmConfigTypesConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function NcmConfigTypesConfig($stateProvider) {
    $stateProvider.state("ncmConfigTypes", {
        url: "/ncm/configTypes",
        i18Title: "_t(Config Types)",
        controller: "NcmConfigTypesController",
        controllerAs: "vm",
        template: __webpack_require__(90)
    });
}
exports.default = NcmConfigTypesConfig;


/***/ }),
/* 90 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content ng-init=vm.activate() page-title=\"Config Types\" class=ncm-config-types> <xui-page-header> <div class=xui-page-content__header> <h1 class=\"xui-h1 xui-page-content__header-title\" _t>Config Types</h1> <ncm-help-button help-url-fragment=OrionNCMAddConfigType></ncm-help-button> </div> </xui-page-header> <xui-breadcrumb crumbs=vm.settingsCrumbs></xui-breadcrumb> <div id=parent-config-type> <div class=col-md-5 id=mainGrid> <h4 class=top-label _t>Some configs are monitored by default and they cannot be removed. Add new custom config types to track.</h4> <div xui-busy=vm.isBusy> <xui-listview items-source=vm.configTypes header-template-url=header-template template-url=config-type-template controller=vm></xui-listview> </div> <form class=add-panel> <div class=row> <div class=col-md-10> <xui-textbox id=add-new-type-textbox ng-model=vm.newName></xui-textbox> </div> <div class=\"col-md-2 col-add-button\"> <xui-button class=add-new-button size=small ng-click=vm.add() display-style=secondary is-busy=vm.isBusy _t>Add new</xui-button> </div> </div> <div ng-if=vm.showError class=error-box> {{vm.validationError}} </div> </form> </div> </div> <script type=text/ng-template id=header-template> <h4 class=\"top-label\" _t>Config Types</h4> </script> <script type=text/ng-template id=config-type-template> <div class=\"row\">\n            <div style=\"overflow-wrap:break-word\" class=\"col-xs-5 col-sm-5 col-md-5 col-lg-6\" id=\"config-type-name\">{{item.name}}</div>\n            <xui-button class=\"col-xs-2 col-sm-3 col-md-3 col-lg-2\" size=\"small\" ng-click=\"vm.edit(item)\" display-style=\"tertiary\" name=\"edit-button\" icon=\"edit\" _t>Edit</xui-button>\n            <xui-button ng-if=\"item.isCustom\" class=\"col-xs-2 col-sm-3 col-md-3 col-lg-2\" size=\"small\" ng-click=\"vm.delete(item)\" display-style=\"tertiary\" name=\"delete-button\" icon=\"delete\" _t>Remove</xui-button>\n        <div> </script> </xui-page-content> ";

/***/ }),
/* 91 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var configTypeVendorLink_1 = __webpack_require__(93);
var vendorOperation_1 = __webpack_require__(94);
var NcmSelectNodeDialogController = /** @class */ (function () {
    /** @ngInject */
    NcmSelectNodeDialogController.$inject = ["$log", "ncmErrorHandlerService", "ncmConfigTypesService", "ncmNodeDetailsService", "currentConfigType", "_t"];
    function NcmSelectNodeDialogController($log, ncmErrorHandlerService, ncmConfigTypesService, ncmNodeDetailsService, currentConfigType, _t) {
        var _this = this;
        this.$log = $log;
        this.ncmErrorHandlerService = ncmErrorHandlerService;
        this.ncmConfigTypesService = ncmConfigTypesService;
        this.ncmNodeDetailsService = ncmNodeDetailsService;
        this.currentConfigType = currentConfigType;
        this._t = _t;
        this.isBusy = false;
        this.showError = false;
        this.dropdownPlaceholder = this._t("select item");
        this.vendorListCaption = this._t("Vendor");
        this.vendorOperationCaption = this._t("Vendor operation");
        this.validateAssignVendor = function () {
            _this.$log.info("NcmSelectNodeDialogController validateAssignVendor()");
            if (_this.selectedVendor == null || _this.selectedVendorOperation == null) {
                _this.validationError = _this._t("All fields are required");
                _this.showError = true;
                return false;
            }
            if (_this.assignedVendors.vendors.length > 0
                && _this.assignedVendors.vendors.indexOf(_this.selectedVendor) >= 0) {
                _this.validationError = _this._t("Vendor operation exists for selected vendor");
                _this.showError = true;
                return false;
            }
            _this.showError = false;
            return true;
        };
    }
    NcmSelectNodeDialogController.prototype.init = function () {
        this.$log.info("NcmSelectNodeDialogController init()");
        this.configTypeVendorOperations = this.initOperationDropDown();
        this.selectedVendorOperation = this.configTypeVendorOperations[0];
        this.prepareAssignedVendors(this.currentConfigType);
        this.prepareAllVendors();
    };
    NcmSelectNodeDialogController.prototype.initOperationDropDown = function () {
        var result = [];
        var item = new vendorOperation_1.VendorOperation();
        item.name = this._t("Is");
        item.enum = configTypeVendorLink_1.ConfigTypeVendorOperation.EqualsTo;
        item.value = configTypeVendorLink_1.ConfigTypeVendorOperation[configTypeVendorLink_1.ConfigTypeVendorOperation.EqualsTo];
        result.push(item);
        item = new vendorOperation_1.VendorOperation();
        item.name = this._t("Is not");
        item.enum = configTypeVendorLink_1.ConfigTypeVendorOperation.DifferentFrom;
        item.value = configTypeVendorLink_1.ConfigTypeVendorOperation[configTypeVendorLink_1.ConfigTypeVendorOperation.DifferentFrom];
        result.push(item);
        return result;
    };
    Object.defineProperty(NcmSelectNodeDialogController.prototype, "forAllVendors", {
        get: function () {
            return !this.currentConfigType.isCustom && this.assignedVendors != null && this.assignedVendors.vendors.length === 0;
        },
        enumerable: true,
        configurable: true
    });
    NcmSelectNodeDialogController.prototype.assignVendor = function () {
        this.$log.info("NcmSelectNodeDialogController assignVendor()");
        if (!this.validateAssignVendor()) {
            return;
        }
        this.assignedVendors.vendors.push(this.selectedVendor);
    };
    NcmSelectNodeDialogController.prototype.delete = function (item) {
        var index = this.assignedVendors.vendors.indexOf(item);
        if (index > -1) {
            this.assignedVendors.vendors.splice(index, 1);
        }
    };
    NcmSelectNodeDialogController.prototype.changeVendorOperation = function () {
        this.assignedVendors.operation = this.selectedVendorOperation.enum;
    };
    NcmSelectNodeDialogController.prototype.prepareAssignedVendors = function (configType) {
        var _this = this;
        this.ncmConfigTypesService.getAssignedVendors(configType.id).then(function (av) {
            _this.$log.info("NcmSelectNodeDialogController: Got all assigned vendors");
            if (av) {
                _this.assignedVendors = av;
                if (_this.assignedVendors.operation !== null) {
                    _this.selectedVendorOperation = _.find(_this.configTypeVendorOperations, function (x) { return x.enum === _this.assignedVendors.operation; });
                }
            }
            else {
                _this.assignedVendors = new configTypeVendorLink_1.ConfigTypeVendorLink();
                _this.assignedVendors.configTypeId = _this.currentConfigType.id;
                _this.assignedVendors.operation = _this.selectedVendorOperation.enum;
            }
        }).catch(function (ex) {
            _this.ncmErrorHandlerService.handle(ex);
        });
    };
    NcmSelectNodeDialogController.prototype.prepareAllVendors = function () {
        var _this = this;
        this.ncmNodeDetailsService.getVendors().then(function (v) {
            _this.$log.info("NcmSelectNodeDialogController: Got all vendors");
            _this.allVendors = v;
        }).catch(function (ex) {
            _this.ncmErrorHandlerService.handle(ex);
        });
    };
    return NcmSelectNodeDialogController;
}());
exports.default = NcmSelectNodeDialogController;


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ConfigTypeVendorOperation;
(function (ConfigTypeVendorOperation) {
    ConfigTypeVendorOperation[ConfigTypeVendorOperation["EqualsTo"] = 0] = "EqualsTo";
    ConfigTypeVendorOperation[ConfigTypeVendorOperation["DifferentFrom"] = 1] = "DifferentFrom";
})(ConfigTypeVendorOperation = exports.ConfigTypeVendorOperation || (exports.ConfigTypeVendorOperation = {}));
var ConfigTypeVendorLink = /** @class */ (function () {
    function ConfigTypeVendorLink() {
        this.vendors = [];
    }
    return ConfigTypeVendorLink;
}());
exports.ConfigTypeVendorLink = ConfigTypeVendorLink;


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VendorOperation = /** @class */ (function () {
    function VendorOperation() {
    }
    return VendorOperation;
}());
exports.VendorOperation = VendorOperation;


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var configDiff_controller_1 = __webpack_require__(96);
var configDiff_config_1 = __webpack_require__(99);
exports.default = function (module) {
    module.controller("ConfigDiffController", configDiff_controller_1.default);
    module.config(configDiff_config_1.default);
};


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var getDiffData_1 = __webpack_require__(97);
var diffConsts_1 = __webpack_require__(7);
var configDiffPageSettings_1 = __webpack_require__(15);
var enumUtils_1 = __webpack_require__(98);
var ConfigDiffController = /** @class */ (function () {
    /** @ngInject */
    ConfigDiffController.$inject = ["$scope", "$stateParams", "$log", "$q", "ncmConfigComparisonService", "ncmConfigService", "breadCrumbService", "_t"];
    function ConfigDiffController($scope, $stateParams, $log, $q, ncmConfigComparisonService, ncmConfigService, breadCrumbService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$stateParams = $stateParams;
        this.$log = $log;
        this.$q = $q;
        this.ncmConfigComparisonService = ncmConfigComparisonService;
        this.ncmConfigService = ncmConfigService;
        this.breadCrumbService = breadCrumbService;
        this._t = _t;
        this.configDiff = {
            hunks: [],
            numberOfLines: 0
        };
        this.isBusy = false;
        this.leftItems = [];
        this.rightItems = [];
        this.demandNumberOfChangedLinesToLoad = 100;
        this.selectConfigPlaceholder = this._t("Select config");
        this.busyMessageString = this._t("Loading...");
        this.checkTitle = function () {
            if (_this.isSelectionMode) {
                return _this._t("Compare Configs Of Selected Nodes");
            }
            if (enumUtils_1.EnumUtils.hasFlag(_this.pageSettings, configDiffPageSettings_1.ConfigDiffPageSettings.Pushed)) {
                return _this._t("Compare Pushed Configs (Panorama)");
            }
            return _this._t("Compare Configs");
        };
        this.checkSettings = function () {
            var urlParam = _this.$stateParams[diffConsts_1.DiffConsts.SETTINGS_PARAM_NAME];
            _this.$log.info(diffConsts_1.DiffConsts.SETTINGS_PARAM_NAME + ": " + urlParam);
            var result = configDiffPageSettings_1.ConfigDiffPageSettings.None;
            if (!_.isUndefined(urlParam)) {
                result = parseInt(urlParam, 10);
            }
            if (_.isUndefined(_this.leftConfigId) || _.isUndefined(_this.rightConfigId)) {
                // tslint:disable-next-line:no-bitwise
                result = result | configDiffPageSettings_1.ConfigDiffPageSettings.Selection;
            }
            return result;
        };
        this.activate = function () {
            _this.$log.info("ConfigDiffController:Activate()");
            if (_this.isSelectionMode) {
                _this.ncmConfigComparisonService.getSelectedNodes().then(function (nodeIds) {
                    _this.getNodesConfigHistory(nodeIds);
                });
            }
            else {
                _this.getDetails();
                _this.getDiff();
            }
        };
        this.onLeftChanged = function (newValue, oldValue) {
            _this.selectionChanged();
        };
        this.onRightChanged = function (newValue, oldValue) {
            _this.selectionChanged();
        };
        this.getDiffAsync = function (data) {
            if (enumUtils_1.EnumUtils.hasFlag(_this.pageSettings, configDiffPageSettings_1.ConfigDiffPageSettings.AdditionalCommands)) {
                return _this.ncmConfigComparisonService.getAdditionalCommandsDiff(data);
            }
            return _this.ncmConfigComparisonService.getDiff(data);
        };
        this.buildBreadCrumbs = function (coreNodeId, currentPageTitle) {
            _this.breadCrumbService.buildBreadCrumbsForConfigDiffView(coreNodeId, currentPageTitle).then(function (crumbs) {
                _this.crumbs = crumbs;
            });
        };
        this.getDisplayValue = function (nodeName, configHistory) {
            return nodeName + " - " + configHistory.configTitle + _this.getConfigTitleDecoration(configHistory.edited, configHistory.favorite);
        };
        this.getConfigTitleDecoration = function (isEdited, isFavorite) {
            var decoration = "";
            var separator = "";
            if (isFavorite) {
                decoration = _this._t("Favorite");
                separator = " ";
            }
            if (isEdited) {
                decoration += "" + separator + _this._t("Edited");
            }
            return decoration.length > 0 ? " (" + decoration + ")" : decoration;
        };
        this.leftConfigId = this.$stateParams[diffConsts_1.DiffConsts.LEFT_CONFIG_ID_PARAM_NAME];
        this.$log.info(diffConsts_1.DiffConsts.LEFT_CONFIG_ID_PARAM_NAME + ": " + this.leftConfigId);
        this.rightConfigId = this.$stateParams[diffConsts_1.DiffConsts.RIGHT_CONFIG_ID_PARAM_NAME];
        this.$log.info(diffConsts_1.DiffConsts.RIGHT_CONFIG_ID_PARAM_NAME + ": " + this.rightConfigId);
        this.additionalCommandKey = this.$stateParams[diffConsts_1.DiffConsts.ADDITIONAL_COMMAND_KEY];
        this.pageSettings = this.checkSettings();
        this.pageTitle = this.checkTitle();
    }
    Object.defineProperty(ConfigDiffController.prototype, "isSelectionMode", {
        get: function () {
            return enumUtils_1.EnumUtils.hasFlag(this.pageSettings, configDiffPageSettings_1.ConfigDiffPageSettings.Selection);
        },
        enumerable: true,
        configurable: true
    });
    ConfigDiffController.prototype.selectionChanged = function () {
        if (this.leftSelectedItem && this.rightSelectedItem) {
            this.leftConfigId = this.leftSelectedItem.configId;
            this.rightConfigId = this.rightSelectedItem.configId;
            this.getDiff();
        }
    };
    ;
    ConfigDiffController.prototype.getDetails = function () {
        var _this = this;
        var getLeftConfigDetails = this.ncmConfigService.getConfigDetails(this.leftConfigId);
        var getRightConfigDetails = this.ncmConfigService.getConfigDetails(this.rightConfigId);
        this.$q.all([getLeftConfigDetails, getRightConfigDetails]).then(function (results) {
            _this.leftConfigDetails = results[0];
            _this.rightConfigDetails = results[1];
            _this.buildBreadCrumbs(_this.leftConfigDetails.nodeId, _this.pageTitle);
        });
    };
    ;
    ConfigDiffController.prototype.getDiff = function () {
        var _this = this;
        this.isBusy = true;
        var diffData = new getDiffData_1.GetDiffData();
        diffData.leftConfigId = this.leftConfigId;
        diffData.rightConfigId = this.rightConfigId;
        diffData.command = this.additionalCommandKey;
        this.getDiffAsync(diffData)
            .then(function (diff) {
            _this.isBusy = false;
            _this.configDiff = diff;
        });
    };
    ;
    ConfigDiffController.prototype.loadMoreLines = function (startIndex, size) {
        var _this = this;
        this.isBusy = true;
        var diffData = new getDiffData_1.GetDiffData();
        diffData.leftConfigId = this.leftConfigId;
        diffData.rightConfigId = this.rightConfigId;
        diffData.onlyChangedLines = false;
        diffData.startIndex = startIndex;
        diffData.size = size;
        diffData.noCache = false;
        diffData.numberOfChangedLinesToLoad = 0;
        diffData.command = this.additionalCommandKey;
        return this.getDiffAsync(diffData)
            .then(function (diff) {
            _this.isBusy = false;
            return diff.hunks[0].lines;
        });
    };
    ;
    ConfigDiffController.prototype.loadMoreChangedLines = function (startIndex) {
        var _this = this;
        this.isBusy = true;
        var diffData = new getDiffData_1.GetDiffData();
        diffData.leftConfigId = this.leftConfigId;
        diffData.rightConfigId = this.rightConfigId;
        diffData.startIndex = startIndex;
        diffData.noCache = false;
        diffData.numberOfChangedLinesToLoad = this.demandNumberOfChangedLinesToLoad;
        diffData.command = this.additionalCommandKey;
        return this.getDiffAsync(diffData)
            .then(function (diff) {
            _this.isBusy = false;
            return diff;
        });
    };
    ;
    ConfigDiffController.prototype.getNodesConfigHistory = function (nodeIds) {
        var _this = this;
        this.isBusy = true;
        this.ncmConfigComparisonService.getNodesConfigHistory(nodeIds)
            .then(function (nodesConfigHistory) {
            var items = [];
            nodesConfigHistory.forEach(function (nodeConfigHistory) {
                if (items.length > 0) {
                    items.push({ separator: true });
                }
                nodeConfigHistory.history.forEach(function (configHistory) {
                    items.push({
                        configId: configHistory.configId,
                        displayValue: _this.getDisplayValue(nodeConfigHistory.nodeName, configHistory)
                    });
                });
            });
            _this.isBusy = false;
            _this.leftItems = items;
            _this.rightItems = items;
        });
    };
    ;
    return ConfigDiffController;
}());
exports.default = ConfigDiffController;


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var GetDiffData = /** @class */ (function () {
    function GetDiffData() {
        this.onlyChangedLines = true;
        this.startIndex = 0;
        this.size = 0;
        this.noCache = true;
        this.numberOfChangedLinesToLoad = 500;
        this.command = "";
    }
    return GetDiffData;
}());
exports.GetDiffData = GetDiffData;


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var EnumUtils;
(function (EnumUtils) {
    function hasFlag(toCheck, flag) {
        // tslint:disable-next-line:no-bitwise
        return (toCheck & flag) !== 0;
    }
    EnumUtils.hasFlag = hasFlag;
})(EnumUtils = exports.EnumUtils || (exports.EnumUtils = {}));


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
ConfigDiffConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
var diffConsts_1 = __webpack_require__(7);
/** @ngInject */
function ConfigDiffConfig($stateProvider) {
    $stateProvider.state("configDiff", {
        url: "/ncm/configDiff?" + diffConsts_1.DiffConsts.LEFT_CONFIG_ID_PARAM_NAME +
            ("&" + diffConsts_1.DiffConsts.RIGHT_CONFIG_ID_PARAM_NAME + "&" + diffConsts_1.DiffConsts.SETTINGS_PARAM_NAME + "&" + diffConsts_1.DiffConsts.ADDITIONAL_COMMAND_KEY),
        i18Title: "_t(Compare Configs)",
        controller: "ConfigDiffController",
        controllerAs: "vm",
        template: __webpack_require__(100)
    });
}
exports.default = ConfigDiffConfig;


/***/ }),
/* 100 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=config-diff-page ng-init=vm.activate() page-title={{vm.pageTitle}} is-busy=vm.isBusy busy-message={{vm.busyMessageString}}> <xui-page-breadcrumbs ng-if=!vm.isSelectionMode> <xui-breadcrumb crumbs=vm.crumbs></xui-breadcrumb> </xui-page-breadcrumbs> <sw-side-by-side-text-diff id=side-by-side-config-diff document-diff=vm.configDiff max-size-of-hunk-to-load=5 load-more-lines-fn=vm.loadMoreLines(index,size) load-more-changed-lines-fn=vm.loadMoreChangedLines(index)> <sw-side-by-side-text-diff-left-header> <div class=xui-tag ng-if=!vm.isSelectionMode> <b>{{vm.leftConfigDetails.nodeName}} - {{vm.leftConfigDetails.configName}}</b> </div> <xui-dropdown class=left-dropdown placeholder={{vm.selectConfigPlaceholder}} display-value=displayValue on-changed=\"vm.onLeftChanged(newValue, oldValue)\" selected-item=vm.leftSelectedItem items-source=vm.leftItems ng-if=vm.isSelectionMode> </xui-dropdown> </sw-side-by-side-text-diff-left-header> <sw-side-by-side-text-diff-right-header> <div class=xui-tag ng-if=!vm.isSelectionMode> <b>{{vm.rightConfigDetails.nodeName}} - {{vm.rightConfigDetails.configName}}</b> </div> <xui-dropdown class=right-dropdown placeholder={{vm.selectConfigPlaceholder}} display-value=displayValue on-changed=\"vm.onRightChanged(newValue, oldValue)\" selected-item=vm.rightSelectedItem items-source=vm.rightItems ng-if=vm.isSelectionMode> </xui-dropdown> </sw-side-by-side-text-diff-right-header> </sw-side-by-side-text-diff> </xui-page-content> ";

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var securityPolicies_controller_1 = __webpack_require__(102);
var securityPolicies_config_1 = __webpack_require__(104);
__webpack_require__(106);
exports.default = function (module) {
    module.controller("SecurityPoliciesController", securityPolicies_controller_1.default);
    module.config(securityPolicies_config_1.default);
};


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var securityPolicy_1 = __webpack_require__(2);
var SecurityPoliciesController = /** @class */ (function () {
    /** @ngInject */
    SecurityPoliciesController.$inject = ["$log", "$filter", "swSubviewInfo", "$q", "ncmSecurityPoliciesService", "ncmNavigationService", "$templateCache", "_t"];
    function SecurityPoliciesController($log, $filter, swSubviewInfo, $q, ncmSecurityPoliciesService, ncmNavigationService, $templateCache, _t) {
        var _this = this;
        this.$log = $log;
        this.$filter = $filter;
        this.swSubviewInfo = swSubviewInfo;
        this.$q = $q;
        this.ncmSecurityPoliciesService = ncmSecurityPoliciesService;
        this.ncmNavigationService = ncmNavigationService;
        this.$templateCache = $templateCache;
        this._t = _t;
        this.securityPolicies = [];
        this.tempSecurityPolicies = [];
        this.options = {
            smartMode: true,
            pageSize: 10,
            showAddRemoveFilterProperty: false,
            showMorePropertyValuesThreshold: 10,
            templateUrl: "security-policy-list-template",
            searchableColumns: [
                "name"
            ],
            emptyData: {
                templateUrl: "empty-template"
            },
            searchPlaceholder: this._t("Search...")
        };
        this.sorting = {
            sortableColumns: [
                { id: "action", label: this._t("Status") },
                { id: "name", label: this._t("Name") }
            ],
            sortBy: {
                id: "action",
                label: this._t("Status")
            },
            direction: "asc"
        };
        this.pagination = {
            page: 1,
            pageSize: 10
        };
        this.filterProperties = [];
        this.paloAltoFilters = (_a = {},
            _a[securityPolicy_1.TokenType[securityPolicy_1.TokenType.Action]] = this._t("Action"),
            _a[securityPolicy_1.TokenType[securityPolicy_1.TokenType.DestinationZone]] = this._t("Destination Zone"),
            _a[securityPolicy_1.TokenType[securityPolicy_1.TokenType.SourceZone]] = this._t("Source Zone"),
            _a[securityPolicy_1.TokenType[securityPolicy_1.TokenType.PolicyOrigin]] = this._t("Policy Origin"),
            _a);
        this.filterValues = (_b = {},
            _b[securityPolicy_1.TokenType[securityPolicy_1.TokenType.Action]] = [],
            _b[securityPolicy_1.TokenType[securityPolicy_1.TokenType.DestinationZone]] = [],
            _b[securityPolicy_1.TokenType[securityPolicy_1.TokenType.SourceZone]] = [],
            _b[securityPolicy_1.TokenType[securityPolicy_1.TokenType.PolicyOrigin]] = [],
            _b);
        this.isFirstLoad = true;
        this.onRefresh = function (filters, pagination, sorting, searching) {
            _this.setEmptyTemplate(searching);
            _this.$log.debug("onRefresh");
            if (!(_.isEmpty(_this.filterProperties))) {
                _this.refreshFilteredData();
            }
        };
        this.setEmptyTemplate = function (searching) {
            if (!(_.isEmpty(_this.filterProperties))) {
                if (searching) {
                    //empty template when searching
                    _this.options["emptyData"] = {
                        image: "no-search-results",
                        title: _this._t("Nothing found"),
                        description: _this._t("Try searching for something else")
                    };
                }
                else {
                    //empty template when filtering
                    _this.options["emptyData"] = {
                        image: "no-search-results",
                        title: _this._t("Nothing found"),
                        description: _this._t("No policies meet the selected criteria.")
                    };
                }
            }
            else {
                //empty template when no configs downloaded
                _this.options["emptyData"] = {
                    templateUrl: "empty-template"
                };
            }
            ;
        };
        this.getFilterProperties = function () {
            if (_this.isFirstLoad) {
                _this.isFirstLoad = false;
                return _this.$q.resolve();
            }
            if (_.isEmpty(_this.filterProperties)) {
                return _this.ncmSecurityPoliciesService.getSecurityPolicies(_this.nodeId)
                    .then(function (result) {
                    _this.securityPolicies = result;
                    _this.tempSecurityPolicies = result;
                    var filterData = _this.CollectFilterData();
                    _this.filterProperties = _this.buildFilterProperty(filterData);
                    return _this.filterProperties;
                });
            }
            else {
                return _this.filterProperties;
            }
        };
        this.getSecurityPolicyDetailsUrl = function (name) {
            return _this.ncmNavigationService.getSecurityPolicyDetailsUrl(_this.nodeId, name);
        };
        this.getSecurityPolicyActionDisplayName = function (action) {
            return securityPolicy_1.SecurityPolicyAction[action];
        };
        this.getSecurityPolicyOriginDisplayName = function (policyOrigin) {
            return _this.ncmSecurityPoliciesService.getSecurityPolicyOriginName(policyOrigin);
        };
        this.getLastChangeDisplayFormat = function (lastChange) {
            var options = { year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" };
            return new Date(lastChange).toLocaleString(undefined, options);
        };
        this.isNullOrEmpty = function (zones) {
            return _.isEmpty(zones);
        };
        this.CollectFilterData = function () {
            var filterData = {
                Action: [],
                SourceZone: [],
                DestinationZone: [],
                PolicyOrigin: []
            };
            _.forEach(_this.securityPolicies, function (policy) {
                filterData.Action.push(securityPolicy_1.SecurityPolicyAction[policy.action]);
                filterData.PolicyOrigin.push(_this.ncmSecurityPoliciesService.getSecurityPolicyOriginName(policy.origin));
                _.forEach(policy.destinationZones, function (zone) {
                    filterData.DestinationZone.push(zone);
                });
                _.forEach(policy.sourceZones, function (zone) {
                    filterData.SourceZone.push(zone);
                });
            });
            return filterData;
        };
        this.buildFilterProperty = function (filterData) {
            _.forEach(filterData, function (values, i) {
                var countBy = _.countBy(values);
                var filterValues = _.map(countBy, function (count, filter) {
                    return {
                        id: filter,
                        title: filter,
                        count: count
                    };
                });
                if (filterValues.length > 0) {
                    var filterProperty = {
                        refId: i.toString(),
                        title: _this.paloAltoFilters[i.toString()],
                        values: filterValues
                    };
                    _this.filterProperties.push(filterProperty);
                }
            });
            return _this.filterProperties;
        };
        this.refreshFilteredData = function () {
            _this.securityPolicies = angular.copy(_this.tempSecurityPolicies);
            if (_this.filterValues.DestinationZone.length > 0) {
                _this.filterDestinationZones();
            }
            if (_this.filterValues.SourceZone.length > 0) {
                _this.filterSourceZones();
            }
            if (_this.filterValues.Action.length > 0) {
                _this.filterActions();
            }
            if (_this.filterValues.PolicyOrigin.length > 0) {
                _this.filterPolicyOrigin();
            }
        };
        this.filterDestinationZones = function () {
            _this.securityPolicies = _.filter(_this.securityPolicies, function (policy) {
                return _.some(policy.destinationZones, function (zones) {
                    return _.includes(_this.filterValues.DestinationZone, zones);
                });
            });
        };
        this.filterSourceZones = function () {
            _this.securityPolicies = _.filter(_this.securityPolicies, function (policy) {
                return _.some(policy.sourceZones, function (zones) {
                    return _.includes(_this.filterValues.SourceZone, zones);
                });
            });
        };
        this.filterActions = function () {
            _this.securityPolicies = _.filter(_this.securityPolicies, function (policy) {
                return _.includes(_this.filterValues.Action, securityPolicy_1.SecurityPolicyAction[policy.action]);
            });
        };
        this.filterPolicyOrigin = function () {
            _this.securityPolicies = _.filter(_this.securityPolicies, function (policy) {
                return _.includes(_this.filterValues.PolicyOrigin, _this.ncmSecurityPoliciesService.getSecurityPolicyOriginName(policy.origin));
            });
        };
        this.nodeId = this.getNodeIdFromNetObject(this.swSubviewInfo.queryParams.netobject);
        this.$templateCache.put("empty-template", __webpack_require__(103));
        var _a, _b;
    }
    SecurityPoliciesController.prototype.getNodeIdFromNetObject = function (netObject) {
        if (_.startsWith(netObject, "N:")) {
            return Number(netObject.substring(2));
        }
        return null;
    };
    ;
    return SecurityPoliciesController;
}());
exports.default = SecurityPoliciesController;


/***/ }),
/* 103 */
/***/ (function(module, exports) {

module.exports = "<div class=xui-empty> <ncm-security-policies-empty-data></ncm-security-policies-empty-data> </div>";

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
SecurityPoliciesConfig.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function SecurityPoliciesConfig($stateProvider) {
    $stateProvider.state("securityPolicies", {
        parent: "subview",
        i18Title: "_t(Policies)",
        url: "/ncm/securityPoliciesSubview",
        controller: "SecurityPoliciesController",
        controllerAs: "vm",
        template: __webpack_require__(105)
    });
}
exports.default = SecurityPoliciesConfig;
;


/***/ }),
/* 105 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-security-policies> <xui-filtered-list items-source=vm.securityPolicies on-refresh=\"vm.onRefresh(filters, pagination, sorting, searching)\" options=vm.options sorting=vm.sorting pagination-data=vm.pagination filter-properties-fn=vm.getFilterProperties() filter-properties=vm.filterProperties filter-values=vm.filterValues controller=vm layout=fill> </xui-filtered-list> <script type=text/ng-template id=security-policy-list-template> <div class=\"row security-policy-list-item\">\n            <div class=\"col-md-2\">\n                <a class=\"policy-name\" href=\"{{vm.getSecurityPolicyDetailsUrl(item.name)}}\">{{::item.name}}</a>\n                <div class=\"xui-help-hint policy-action\"><b>{{::vm.getSecurityPolicyActionDisplayName(item.action)}}</b></div>\n            </div>\n            <div class=\"col-md-3\">\n                <span class=\"source-zones\" ng-if=\"vm.isNullOrEmpty(item.sourceZones)\" _t>none</span>\n                <span class=\"source-zones\" ng-if=\"!vm.isNullOrEmpty(item.sourceZones) && item.sourceZones.length <= 2\" ng-repeat=\"sourceZone in item.sourceZones\">                    \n                    <span>{{sourceZone}}</span><span ng-if=\"$last == false\">, </span>\n                </span>\n                <span class=\"source-zones\" ng-if=\"!vm.isNullOrEmpty(item.sourceZones) && item.sourceZones.length > 2\">                    \n                    {{::item.sourceZones[0]}}, {{::item.sourceZones[1]}}\n                    <span _t >and</span> {{::item.sourceZones.length - 2}} <span _t>more</span>\n                </span>\n                <div class=\"xui-help-hint\" _t>source zone(s)</div>\n            </div>\n            <div class=\"col-md-3\">\n                <span class=\"destination-zones\" ng-if=\"vm.isNullOrEmpty(item.destinationZones)\" _t>none</span>\n                <span class=\"destination-zones\" ng-if=\"!vm.isNullOrEmpty(item.destinationZones) && item.destinationZones.length <= 2\" ng-repeat=\"destinationZone in item.destinationZones\">                    \n                    <span>{{destinationZone}}</span><span ng-if=\"$last == false\">, </span>\n                </span>\n                <span class=\"destination-zones\" ng-if=\"!vm.isNullOrEmpty(item.destinationZones) && item.destinationZones.length > 2\">                    \n                    {{::item.destinationZones[0]}}, {{::item.destinationZones[1]}} \n                    <span _t>and</span> {{::item.destinationZones.length - 2}} <span _t>more</span>\n                </span>\n                <div class=\"xui-help-hint\" _t>destination zone(s)</div>\n            </div>\n            <div class=\"col-md-2\">\n                <span ng-if=\"item.lastChange !== null\">\n                    <span class=\"last-change\">{{::vm.getLastChangeDisplayFormat(item.lastChange)}}</span>\n                    <div class=\"xui-help-hint\" _t>last change</div>\n                </span>\n            </div>\n            <div class=\"col-md-2\">\n                <span ng-if=\"item.origin !== null\">\n                    <span class=\"policy-origin\"><b>{{::vm.getSecurityPolicyOriginDisplayName(item.origin)}}</b></span>\n                    <div class=\"xui-help-hint\" _t>policy origin</div>\n                </span>\n            </div>\n        </div> </script> </div>";

/***/ }),
/* 106 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(108);
var index_2 = __webpack_require__(119);
var index_3 = __webpack_require__(127);
var index_4 = __webpack_require__(132);
var index_5 = __webpack_require__(135);
var index_6 = __webpack_require__(140);
var index_7 = __webpack_require__(143);
var index_8 = __webpack_require__(148);
var index_9 = __webpack_require__(151);
var index_10 = __webpack_require__(155);
var index_11 = __webpack_require__(159);
var accessListEmptyData_1 = __webpack_require__(163);
exports.default = function (module) {
    index_1.default(module);
    index_5.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_6.default(module);
    index_7.default(module);
    index_8.default(module);
    index_9.default(module);
    index_10.default(module);
    index_11.default(module);
    accessListEmptyData_1.default(module);
};


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessList_directive_1 = __webpack_require__(109);
var accessList_directive_controller_1 = __webpack_require__(23);
var index_1 = __webpack_require__(112);
__webpack_require__(10);
__webpack_require__(22);
exports.default = function (module) {
    templates.$inject = ["$templateCache"];
    module.component("accessListSubview", accessList_directive_1.AccessList);
    module.controller("accessListSubviewDirectiveController", accessList_directive_controller_1.AccessListController);
    index_1.default(module);
    /* @ngInject */
    function templates($templateCache) {
        $templateCache.put("accessList-item-template-level1.html", __webpack_require__(116));
        $templateCache.put("accessList-item-template-level2.html", __webpack_require__(117));
        $templateCache.put("accessList-item-grid-row.html", __webpack_require__(118));
    }
    module.app().run(templates);
};


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(22);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var accessList_directive_controller_1 = __webpack_require__(23);
/**
 * Directive function for access control list component
 */
var AccessList = /** @class */ (function () {
    function AccessList() {
        this.restrict = "E";
        this.controller = accessList_directive_controller_1.AccessListController;
        this.controllerAs = "ctrlAccessList";
        this.template = __webpack_require__(111);
        this.replace = true;
        this.scope = {};
        this.bindToController = {
            nodeId: "<"
        };
    }
    return AccessList;
}());
exports.AccessList = AccessList;


/***/ }),
/* 110 */
/***/ (function(module, exports) {

module.exports = "<div class=xui-empty> <ncm-access-list-empty-data></ncm-access-list-empty-data> </div>";

/***/ }),
/* 111 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-access-list ng-init=ctrlAccessList.activate()> <div xui-busy=ctrlAccessList.isLoading xui-busy-message={{ctrlAccessList.aclLoadingString}}> <div class=acl-compare-button-container> <a href={{ctrlAccessList.getCompareAclUrl()}} id=acl-compare-href> <xui-button icon=compare class=id-acl-compare-button is-disabled=ctrlAccessList.navigateToCompareAclDisabled() display-style=link> <span _t>Compare 2 ACLs</span> </xui-button> </a> </div> <xui-divider class=acl-divider> </xui-divider> <xui-grid items-source=ctrlAccessList.itemsSource class=\"id-acl-grid-parent xui-edge-definer\" hide-toolbar=true pagination-data=ctrlAccessList.gridPagination options=ctrlAccessList.gridOptions sorting-data=ctrlAccessList.sorting smart-mode=true template-url=accessList-item-template-level1.html on-search=\"ctrlAccessList.onSearch(item, cancellation)\" on-pagination-change=\"ctrlAccessList.paginationChange(page, pageSize, total)\" empty-data=ctrlAccessList.emptyData show-empty=!ctrlAccessList.isLoading on-sorting-change=ctrlAccessList.onSort()> </xui-grid> </div> </div> ";

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var aclSrdIcon_directive_controller_1 = __webpack_require__(25);
var aclSrdIcon_directive_1 = __webpack_require__(113);
exports.default = function (module) {
    module.component("aclSrdIconSubview", aclSrdIcon_directive_1.AclSrdIcon);
    module.controller("aclSrdIconController", aclSrdIcon_directive_controller_1.AclSrdIconController);
};


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(114);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angularDirectiveUtils_1 = __webpack_require__(9);
var angularDirectiveUtils_2 = __webpack_require__(9);
var aclSrdIcon_directive_controller_1 = __webpack_require__(25);
var AclSrdIcon = /** @class */ (function () {
    function AclSrdIcon() {
        this.restrict = angularDirectiveUtils_1.Restrict.element;
        this.transclude = false;
        this.controller = aclSrdIcon_directive_controller_1.AclSrdIconController;
        this.controllerAs = "controller";
        this.template = __webpack_require__(115);
        this.replace = true;
        this.scope = {
            acl: angularDirectiveUtils_2.BindingScope.twoWay
        };
    }
    return AclSrdIcon;
}());
exports.AclSrdIcon = AclSrdIcon;


/***/ }),
/* 114 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 115 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=controller.activate() class=col-srd-desc> <div ng-if=\"controller.isLoading() && controller.canShowSrdIcon()\"> <xui-icon icon=busy-cube class=id-acl-srd-busy-icon icon-size=medium tool-tip=\"Rules state is being updated\"></xui-icon> </div> <xui-popover ng-if=\"!controller.isLoading() && controller.canShowSrdIcon()\" class=id-acl-srd-popover xui-popover-trigger=mouseenter xui-popover-placement=left xui-popover-content=controller.getSrdIconPopoverTemplate xui-popover-title=controller.overlappingRulesPlaceholder> <span class=srd-icon> <xui-icon icon=status_warning class=id-acl-srd-icon con-size=medium></xui-icon> </span> <span class=\"srd-counter id-acl-srd-counter\">{{controller.getTotalSrdCount()}}</span> </xui-popover> <script type=text/ng-template id=ncm-acl-srd-popover-template> <div class=\"id-acl-srd-icon-popover-dsc\">\n            <div ng-repeat=\"desc in controller.popoverDescriptions\">\n                {{desc}}\n            </div>\n        </div> </script> </div>";

/***/ }),
/* 116 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-acl-list__item_level1> <div ng-controller=\"accessListSubviewDirectiveController as ctrlAccessList\"> <div ng-if=\"item.innerItemsSource.length === 0\" class=expand-icon-offset> <div ng-include=\"'accessList-item-grid-row.html'\"></div> </div> <xui-expander ng-if=\"item.innerItemsSource.length !== 0\" class=id-acl-expander is-disabled=\"item.innerItemsSource.length === 0\" on-status-changed=\"ctrlAccessList.onStatusChangedOnExpander(isOpen, item)\"> <xui-expander-heading> <div> <div ng-include=\"'accessList-item-grid-row.html'\"></div> </div> </xui-expander-heading> <div class=row> <div class=col-md-12> <xui-grid items-source=item.innerItemsSource class=\"id-acl-grid-child acl-grid-child\" hide-toolbar=true pagination-data=ctrlAccessList.innerGridPagination options=ctrlAccessList.innerGridOptions smart-mode=true template-url=accessList-item-template-level2.html controller=ctrlAccessList> </xui-grid> </div> </div> </xui-expander> </div> </div> ";

/***/ }),
/* 117 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-acl-list__item_level2> <div ng-controller=\"accessListSubviewDirectiveController as ctrlAccessList\"> <div class=grid-row> <div class=col-checkBox> <xui-checkbox ng-model=item.selected class=id-acl-item-checkbox-child is-disabled=ctrlAccessList.isCheckboxDisabled(item) ng-change=ctrlAccessList.onCheckboxClick(item)></xui-checkbox> </div> <div class=col-icon> <xui-icon class=id-acl-item-icon-child icon=config-file></xui-icon> </div> <div class=col-name> <a role=button href={{ctrlAccessList.getAclDetailsUrl(item)}}><span class=id-acl-item-name-child ng-bind-html=::ctrlAccessList.highlightSearch(item.name)></span></a> <div class=\"text-muted id-acl-item-datetime-child\">{{::ctrlAccessList.toLocalDate(item.modificationTime)}}</div> </div> <div class=\"col-interface id-acl-item-interface-child\"> <div style=display:inline ng-if=\"::item.interfaces.length <= 2\" ng-repeat=\"aclInterface in ::item.interfaces\"> <span ng-if=ctrlAccessList.isInterfaceAvailable(aclInterface)> <a href={{ctrlAccessList.getInterfaceUrl(aclInterface)}}>{{aclInterface}}</a><span ng-if=\"$last == false\">,&nbsp;</span> </span> <span ng-if=!ctrlAccessList.isInterfaceAvailable(aclInterface)> <span>{{aclInterface}}</span><span ng-if=\"$last == false\">,&nbsp;</span> </span> </div> <div style=display:inline ng-if=\"::item.interfaces.length > 2\"> <span ng-if=!ctrlAccessList.areInterfacesEnabled()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </span> <span ng-if=ctrlAccessList.areInterfacesEnabled()> <a role=button ng-click=ctrlAccessList.navigateToInterfacesTab()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </a> </span> </div> </div> <acl-srd-icon acl=item></acl-srd-icon> </div> </div> </div> ";

/***/ }),
/* 118 */
/***/ (function(module, exports) {

module.exports = "<div class=grid-row> <div class=col-checkBox> <xui-checkbox ng-model=item.selected class=id-acl-item-checkbox-parent is-disabled=ctrlAccessList.isCheckboxDisabled(item) ng-change=ctrlAccessList.onCheckboxClick(item)></xui-checkbox> </div> <div class=col-icon> <xui-icon class=id-acl-item-icon-parent icon=config-file></xui-icon> </div> <div class=col-name> <a role=button href={{ctrlAccessList.getAclDetailsUrl(item)}}><span class=id-acl-item-name-parent ng-bind-html=::ctrlAccessList.highlightSearch(item.name)></span></a> <div class=\"text-muted id-acl-item-datetime-parent\">{{::ctrlAccessList.toLocalDate(item.modificationTime)}}</div> </div> <div class=\"col-interface id-acl-item-interface-parent\"> <div style=display:inline ng-if=\"::item.interfaces.length <= 2\" ng-repeat=\"aclInterface in ::item.interfaces\"> <span ng-if=ctrlAccessList.isInterfaceAvailable(aclInterface)> <a href={{ctrlAccessList.getInterfaceUrl(aclInterface)}}>{{aclInterface}}</a><span ng-if=\"$last == false\">,&nbsp;</span> </span> <span ng-if=!ctrlAccessList.isInterfaceAvailable(aclInterface)> <span>{{aclInterface}}</span><span ng-if=\"$last == false\">,&nbsp;</span> </span> </div> <div style=display:inline ng-if=\"::item.interfaces.length > 2\"> <span ng-if=!ctrlAccessList.areInterfacesEnabled()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </span> <span ng-if=ctrlAccessList.areInterfacesEnabled()> <a role=button ng-click=ctrlAccessList.navigateToInterfacesTab()> <span>{{::item.interfaces[0]}}, {{::item.interfaces[1]}} <span _t>and</span> {{::item.interfaces.length-2}} <span _t>more</span></span> </a> </span> </div> </div> <acl-srd-icon acl=item></acl-srd-icon> </div> ";

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ruleList_directive_1 = __webpack_require__(120);
var shadowRuleDetectionDialog_controller_1 = __webpack_require__(126);
__webpack_require__(10);
exports.default = function (module) {
    module.controller("ShadowRuleDetectionDialogController", shadowRuleDetectionDialog_controller_1.ShadowRuleDetectionDialogController);
    module.component("ruleList", ruleList_directive_1.RuleList);
};


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(121);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ruleList_directive_controller_1 = __webpack_require__(122);
/**
 * Directive function for rule list component
 */
var RuleList = /** @class */ (function () {
    /** @ngInject */
    RuleList.$inject = ["swUtil"];
    function RuleList(swUtil) {
        var _this = this;
        this.swUtil = swUtil;
        this.restrict = "E";
        this.transclude = false;
        this.controller = ruleList_directive_controller_1.RuleListController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(125);
        this.replace = true;
        this.scope = {
            nodeId: "=",
            aclName: "=",
            configId: "="
        };
        this.link = function (scope, element, attrs) {
            _this.swUtil.initComponent(attrs, "ruleList");
            scope.nodeId = attrs.nodeId;
            scope.aclName = attrs.aclName;
            scope.configId = attrs.configId;
        };
    }
    return RuleList;
}());
exports.RuleList = RuleList;


/***/ }),
/* 121 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var accessControlListRule_1 = __webpack_require__(5);
var accessControlListRule_2 = __webpack_require__(5);
var shadowRuleDetectionDialogViewModel_1 = __webpack_require__(123);
var utils_1 = __webpack_require__(6);
var RuleListController = /** @class */ (function () {
    /** @ngInject */
    RuleListController.$inject = ["$scope", "$log", "aclRulesService", "aclRulesDiffService", "ncmNavigationService", "dialogService", "windowsPerformanceMonitorService", "$q", "_t"];
    function RuleListController($scope, $log, aclRulesService, aclRulesDiffService, ncmNavigationService, dialogService, windowsPerformanceMonitorService, $q, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.aclRulesService = aclRulesService;
        this.aclRulesDiffService = aclRulesDiffService;
        this.ncmNavigationService = ncmNavigationService;
        this.dialogService = dialogService;
        this.windowsPerformanceMonitorService = windowsPerformanceMonitorService;
        this.$q = $q;
        this._t = _t;
        this.aclRules = [];
        this.aclRulesAll = [];
        this.aclRulesWithoutRemarks = [];
        this.getSrdIconPopoverTemplate = { url: "ncm-rule-list-srd-template" };
        this.isLoading = false;
        this.options = {
            smartMode: true,
            pageSize: 100,
            showAddRemoveFilterProperty: true,
            showMorePropertyValuesThreshold: 10,
            templateUrl: "rule-list-template",
            searchableColumns: [
                "rawData"
            ],
            emptyData: {
                image: "no-search-results",
                title: this._t("No rules of this Access List"),
                description: ""
            },
            searchPlaceholder: this._t("Search...")
        };
        this.sorting = {
            sortableColumns: [
                { id: "ordinalNumber", label: this._t("Line number") },
                { id: "matchCounter", label: this._t("Hit count") }
            ],
            sortBy: {
                id: "ordinalNumber",
                label: this._t("Line number")
            },
            direction: "asc"
        };
        this.pagination = {
            page: 1,
            pageSize: 10
        };
        this.aclFilters = (_a = {},
            _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Action]] = this._t("Rule Type"),
            _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.SourceAddress]] = this._t("Source"),
            _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.DestinationAddress]] = this._t("Destination"),
            _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.ObjectGroup]] = this._t("Object Group"),
            _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Object]] = this._t("Object"),
            _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Protocol]] = this._t("Protocol"),
            _a);
        this.aclFilterProperties = [];
        this.aclFilterPropertyGroup = this._t("Acl Entry");
        this.filterPropertyGroups = [
            this.aclFilterPropertyGroup
        ];
        this.filterValues = (_b = {},
            _b[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Action]] = [],
            _b[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.SourceAddress]] = [],
            _b[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.DestinationAddress]] = [],
            _b);
        this.activate = function () {
            _this.$log.info("RuleListController:Activate()");
            _this.windowsPerformanceMonitorService.monitorPageLoadingTime();
        };
        this.showHideRemarksClick = function () {
            _this.pagination.page = 1;
            _this.showRemarks = !_this.showRemarks;
            _this.refresh();
        };
        this.showHideRemarksDisabled = function () {
            return !_this.hasRemarks() || _this.hasAppliedFilters();
        };
        this.hasRemarks = function () {
            return _.some(_this.aclRulesAll, function (rule) { return rule.isRemark === true; });
        };
        this.hasAppliedFilters = function () {
            return _.some(_this.filterValues, function (filterValue) { return filterValue.length > 0; });
        };
        this.navigateToCompareAcl = function () {
            _this.aclRulesDiffService.getPreviousAclVersionConfigId(_this.nodeId, _this.configId, _this.aclName)
                .then(function (previousAclVersionConfigId) {
                if (previousAclVersionConfigId != null) {
                    _this.$log.info("RuleListController:navigateToCompareAcl()");
                    _this.ncmNavigationService.navigateToCompareAcl({
                        "leftNodeId": _this.nodeId,
                        "leftConfigId": previousAclVersionConfigId,
                        "leftAclName": _this.aclName,
                        "rightNodeId": _this.nodeId,
                        "rightConfigId": _this.configId,
                        "rightAclName": _this.aclName
                    });
                }
                else {
                    _this.dialogService.showError({
                        title: _this._t("Compare ACL"),
                        message: _this._t("No previous ACL version found to compare against.")
                    });
                }
            });
        };
        this.onTokenClickHandler = function (token) {
            _this.showSidePanel = true;
            _this.token = angular.copy(token);
            _this.tokenConfigId = angular.copy(_this.configId);
        };
        this.getFilterProperties = function () {
            if (_.isEmpty(_this.aclFilterProperties)) {
                _this.isLoading = true;
                var startDate_1 = Date.now();
                return _this.aclRulesService.getAclRules(_this.nodeId, _this.configId, _this.aclName)
                    .then(function (result) {
                    _this.aclRules = result.rules;
                    _this.aclRulesAll = result.rules;
                    _this.aclRulesWithoutRemarks = _.filter(result.rules, function (rule) { return rule.isRemark === false; });
                    _this.aclDate = utils_1.default.toLocalDateString(result.modificationTime);
                    _this.aclFilterProperties = _this.getAclFilterProperties(_this.aclRules);
                    var downloadTime = Date.now() - startDate_1;
                    _this.$log.debug("RuleListController: fetched ACL rules for " + _this.aclName +
                        " (config id: " + _this.configId + ") in " + downloadTime + "ms");
                    _this.isLoading = false;
                    return _this.aclFilterProperties;
                });
            }
            else {
                return _this.$q.when(_this.aclFilterProperties);
            }
        };
        this.onRefresh = function (filters, pagination, sorting, searching) {
            _this.refresh();
            _this.onSearch(searching);
        };
        this.refresh = function () {
            if (_this.showRemarks) {
                _this.aclRules = angular.copy(_this.aclRulesAll);
            }
            else {
                _this.aclRules = angular.copy(_this.aclRulesWithoutRemarks);
            }
            var aceTokenType = accessControlListRule_1.AceTokenType.Unknown;
            var aclRulesTmp1 = [];
            var aclRulesTmp2 = angular.copy(_this.aclRules);
            var atLeastOneFilterSelected = false;
            _.forEach(_this.filterValues, function (values, tokenType) {
                if (values.length > 0) {
                    atLeastOneFilterSelected = true;
                    if (aceTokenType !== _this.getAceTokenType(tokenType)) {
                        if (aceTokenType !== accessControlListRule_1.AceTokenType.Unknown) {
                            aclRulesTmp2 = angular.copy(aclRulesTmp1);
                        }
                        aclRulesTmp1 = [];
                        aceTokenType = _this.getAceTokenType(tokenType);
                    }
                    _.forEach(values, function (value) {
                        var tokensToFilter = new Array();
                        _.forEach(value.split(" "), function (item) {
                            var token = new accessControlListRule_1.AceToken();
                            token.value = item;
                            token.type = _this.getAceTokenType(tokenType);
                            tokensToFilter.push(token);
                        });
                        var filteredRules = [];
                        _.forEach(aclRulesTmp2, function (rule) {
                            var commonTokens = _.intersectionWith(rule.tokens, tokensToFilter, _this.comparator);
                            if ((commonTokens.length > 0) && (commonTokens.length === tokensToFilter.length)) {
                                var tokensString = _.join(_.map(rule.tokens, function (token) { return token.value; }), " ");
                                if (tokensString.indexOf(value) >= 0) {
                                    filteredRules.push(rule);
                                }
                            }
                        });
                        aclRulesTmp1 = _.concat(aclRulesTmp1, filteredRules);
                    });
                }
            });
            if (atLeastOneFilterSelected) {
                _this.aclRules = angular.copy(_.uniq(aclRulesTmp1));
            }
            _this.recalculateFilterPropertiesCount();
        };
        this.comparator = function (thisToken, otherToken) {
            /* tslint:disable:no-bitwise */
            if ((thisToken.value === otherToken.value) && (thisToken.type & otherToken.type)) {
                return true;
            }
            return false;
            /* tslint:enable:no-bitwise */
        };
        this.recalculateFilterPropertiesCount = function () {
            var tokenValuesByType = _this.getTokenValuesByType(_this.aclRules);
            _.forEach(_this.aclFilterProperties, function (filter) {
                _.forEach(filter.values, function (filterValue) {
                    var refId = filter.refId;
                    var values = tokenValuesByType[refId];
                    var count = _.sumBy(values, function (value) {
                        return value === filterValue.title ? 1 : 0;
                    });
                    filterValue.count = count ? count : undefined;
                });
            });
        };
        this.getAclFilterProperties = function (rules) {
            var tokenValuesByType = _this.getTokenValuesByType(rules);
            var filterProperties = [];
            _.forEach(tokenValuesByType, function (values, tokenType) {
                var countBy = _.countBy(values);
                var filterValues = _.map(countBy, function (count, filter) {
                    return {
                        id: filter,
                        title: filter,
                        count: count
                    };
                });
                if (filterValues.length > 0) {
                    var filterProperty = {
                        refId: tokenType,
                        title: _this.aclFilters[tokenType],
                        parent: _this.aclFilterPropertyGroup,
                        values: filterValues
                    };
                    filterProperties.push(filterProperty);
                }
            });
            return filterProperties;
        };
        this.getTokenValuesByType = function (rules) {
            var tokenValuesByType = (_a = {},
                _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Action]] = [],
                _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.SourceAddress]] = [],
                _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.DestinationAddress]] = [],
                _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.ObjectGroup]] = [],
                _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Object]] = [],
                _a[accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Protocol]] = [],
                _a);
            _.forEach(rules, function (rule) {
                _.forEach(tokenValuesByType, function (values, tokenType) {
                    return _this.getTokenValuesFromAclRule(rule, tokenType).forEach(function (value) { return values.push(value); });
                });
            });
            return tokenValuesByType;
            var _a;
        };
        this.getTokenValuesFromAclRule = function (rule, tokenType) {
            /* tslint:disable:no-bitwise */
            var aceTokenType = _this.getAceTokenType(tokenType);
            var result = _.reduce(rule.tokens, function (arr, token) {
                if ((token.type & aceTokenType) &&
                    (arr.length > 0 && arr[arr.length - 1].type & token.type)) {
                    arr[arr.length - 1].values.push(token.value);
                }
                else {
                    if (token.type & aceTokenType) {
                        var item = {
                            type: token.type,
                            values: []
                        };
                        item.values.push(token.value);
                        arr.push(item);
                    }
                    else {
                        if (_.find(arr, function (item) { return item.values.length === 0; })) {
                            arr.push({ type: aceTokenType, values: [] });
                        }
                    }
                }
                return arr;
            }, []);
            if (result.length > 0) {
                if (aceTokenType === accessControlListRule_1.AceTokenType.SourceAddress ||
                    aceTokenType === accessControlListRule_1.AceTokenType.DestinationAddress) {
                    return [_.join(result[0].values, " ")];
                }
                else {
                    return result[0].values;
                }
            }
            else {
                return [];
            }
            /* tslint:enable:no-bitwise */
        };
        this.getAceTokenType = function (key) {
            switch (key) {
                case accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Action]:
                    return accessControlListRule_1.AceTokenType.Action;
                case accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.SourceAddress]:
                    return accessControlListRule_1.AceTokenType.SourceAddress;
                case accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.DestinationAddress]:
                    return accessControlListRule_1.AceTokenType.DestinationAddress;
                case accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.ObjectGroup]:
                    return accessControlListRule_1.AceTokenType.ObjectGroup;
                case accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Object]:
                    return accessControlListRule_1.AceTokenType.Object;
                case accessControlListRule_1.AceTokenType[accessControlListRule_1.AceTokenType.Protocol]:
                    return accessControlListRule_1.AceTokenType.Protocol;
                default:
                    return accessControlListRule_1.AceTokenType.Unknown;
            }
        };
        this.canDisplaySrdIcon = function (shadowRuleDetectionResults) {
            if (shadowRuleDetectionResults == null || shadowRuleDetectionResults.length === 0) {
                return false;
            }
            for (var _i = 0, shadowRuleDetectionResults_1 = shadowRuleDetectionResults; _i < shadowRuleDetectionResults_1.length; _i++) {
                var result = shadowRuleDetectionResults_1[_i];
                if (result.type !== accessControlListRule_2.RuleCheckDetectionType.Unique) {
                    return true;
                }
            }
            return false;
        };
        this.buildSrdPopupTitle = function (shadowRuleDetectionResults) {
            var shadowed = false;
            var redundand = false;
            for (var _i = 0, shadowRuleDetectionResults_2 = shadowRuleDetectionResults; _i < shadowRuleDetectionResults_2.length; _i++) {
                var result = shadowRuleDetectionResults_2[_i];
                if (result.type === accessControlListRule_2.RuleCheckDetectionType.Shadow ||
                    result.type === accessControlListRule_2.RuleCheckDetectionType.PartiallyShadow) {
                    shadowed = true;
                }
                if (result.type === accessControlListRule_2.RuleCheckDetectionType.PartiallyRedundant ||
                    result.type === accessControlListRule_2.RuleCheckDetectionType.Redundant) {
                    redundand = true;
                }
            }
            if (shadowed && redundand) {
                return _this._t("Shadowed and Redundant Rules");
            }
            if (shadowed) {
                return _this._t("Shadowed Rules");
            }
            if (redundand) {
                return _this._t("Redundant Rules");
            }
            return "";
        };
        this.onSrdDetailsClickHandler = function (acl) {
            var viewModel = new shadowRuleDetectionDialogViewModel_1.ShadowRuleDetectionDialogViewModel();
            viewModel.currentAce = acl;
            viewModel.aclRules = _this.aclRulesAll;
            var setupDialogOptions = {
                title: _this.buildSrdPopupTitle(acl.shadowRuleDetectionResults),
                viewModel: viewModel,
                buttons: [
                    {
                        name: "closeButton",
                        isPrimary: true,
                        text: _this._t("Close"),
                        action: function () {
                            return true;
                        }
                    }
                ]
            };
            _this.dialogService.showModal({
                template: __webpack_require__(124)
            }, setupDialogOptions);
        };
        this.onSearch = function (searchTerm) {
            _this.searchTerm = searchTerm;
            if (_this.aclRules.length > 0 && searchTerm) {
                _this.findMatches(_this.aclRules, searchTerm);
            }
        };
        this.findMatches = function (rules, searchTerm) {
            var searchTokens = searchTerm.split(" ");
            var pattern = new RegExp(_.escapeRegExp(searchTerm), "gi");
            rules.forEach(function (rule) {
                if (rule != null) {
                    _this.findMatchesInRule(searchTokens, pattern, rule);
                }
            });
        };
        this.findMatchesInRule = function (searchTokens, pattern, rule) {
            var rawData = rule.tokens.map(function (x) { return x.value; }).join(" ");
            var match = pattern.exec(rawData);
            var matchesPosition = [];
            while (match != null) {
                var tokenPosition = rawData.substring(0, match.index).split(" ").length;
                matchesPosition.push(tokenPosition - 1);
                match = pattern.exec(rawData);
            }
            matchesPosition.forEach(function (position) {
                searchTokens.forEach(function (token, index) {
                    var escapedToken = _.escapeRegExp(token);
                    var regPattern;
                    if (searchTokens.length > 1) {
                        regPattern = index === 0 ? escapedToken + "(?!.*" + escapedToken + ")" : "^" + escapedToken;
                    }
                    else {
                        regPattern = escapedToken;
                    }
                    rule.tokens[position + index].toSelect = regPattern;
                });
            });
        };
        this.getSrdPopoverDescriptions = function (ace) {
            var shadowRuleDetectionResults = ace.shadowRuleDetectionResults;
            var descs = new Array();
            var ordered = new Array();
            var fullyShadowed = _this._t("The rule is fully shadowed.");
            var partiallyShadowed = _this._t("The rule is partially shadowed.");
            var fullyRedundant = _this._t("The rule is fully redundant.");
            var partiallyRedundant = _this._t("The rule is partially redundant.");
            for (var _i = 0, shadowRuleDetectionResults_3 = shadowRuleDetectionResults; _i < shadowRuleDetectionResults_3.length; _i++) {
                var result = shadowRuleDetectionResults_3[_i];
                switch (result.type) {
                    case accessControlListRule_2.RuleCheckDetectionType.Shadow:
                        descs.push(fullyShadowed);
                        break;
                    case accessControlListRule_2.RuleCheckDetectionType.PartiallyShadow:
                        descs.push(partiallyShadowed);
                        break;
                    case accessControlListRule_2.RuleCheckDetectionType.Redundant:
                        descs.push(fullyRedundant);
                        break;
                    case accessControlListRule_2.RuleCheckDetectionType.PartiallyRedundant:
                        descs.push(partiallyRedundant);
                        break;
                }
            }
            if (descs.some(function (s) { return s === fullyShadowed; })) {
                ordered.push(fullyShadowed);
            }
            if (descs.some(function (s) { return s === partiallyShadowed; })) {
                ordered.push(partiallyShadowed);
            }
            if (descs.some(function (s) { return s === fullyRedundant; })) {
                ordered.push(fullyRedundant);
            }
            if (descs.some(function (s) { return s === partiallyRedundant; })) {
                ordered.push(partiallyRedundant);
            }
            return ordered;
        };
        this.scope = $scope;
        this.nodeId = this.scope.nodeId;
        this.aclName = this.scope.aclName;
        this.configId = this.scope.configId;
        this.showSidePanel = false;
        this.tokenConfigId = this.configId;
        this.showRemarks = true;
        var _a, _b;
    }
    return RuleListController;
}());
exports.RuleListController = RuleListController;


/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ShadowRuleDetectionDialogViewModel = /** @class */ (function () {
    function ShadowRuleDetectionDialogViewModel() {
    }
    return ShadowRuleDetectionDialogViewModel;
}());
exports.ShadowRuleDetectionDialogViewModel = ShadowRuleDetectionDialogViewModel;


/***/ }),
/* 124 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"ShadowRuleDetectionDialogController as container\" ng-init=container.init(vm.dialogOptions.viewModel) class=ncm-rule-list> <xui-dialog> <div class=srd-dialog-content> <div ng-repeat=\"rulesVm in container.rulesViewModel\" ng-if=\"rulesVm.rules.length > 0\" class=srd-dialog-rules> <div>{{rulesVm.message}}</div> <xui-listview items-source=rulesVm.rules template-url=srd-item-template stripe=true row-padding=none class=ncm-rule-list-container> </xui-listview> </div> </div> </xui-dialog> <script type=text/ng-template id=srd-item-template> <rule-row clickable=\"false\" rule=\"item\"></rule-row> </script> </div> ";

/***/ }),
/* 125 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-rule-list ng-init=vm.activate()> <div class=row> <div ng-class=\"{'col-md-9': vm.showSidePanel, 'col-md-12': vm.showSidePanel==false}\"> <xui-filtered-list items-source=vm.aclRules on-refresh=\"vm.onRefresh(filters, pagination, sorting, searching)\" options=vm.options sorting=vm.sorting pagination-data=vm.pagination filter-property-groups=vm.filterPropertyGroups filter-properties-fn=vm.getFilterProperties() filter-values=vm.filterValues controller=vm class=ncm-rule-list-container id=ace-filtered-list ng-show=!vm.isLoading> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button class=\"id-ace-compare-button ncm-toolbar-button\" icon=compare ng-click=vm.navigateToCompareAcl() display-style=link> <span _t>Compare ACL</span> </xui-button> </xui-toolbar-item> <xui-toolbar-divider></xui-toolbar-divider> <xui-toolbar-item> <xui-button class=\"id-ace-hide-show-remark-button ncm-toolbar-button\" ng-click=vm.showHideRemarksClick() is-disabled=vm.showHideRemarksDisabled() display-style=link> <span ng-if=\"vm.showRemarks===true\" _t>Hide Remarks</span> <span ng-if=\"vm.showRemarks===false\" _t>Show Remarks</span> </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> <xui-filtered-list-footer class=id-ace-hit-count-datetime> <span _t>Last update of hit count: </span>{{vm.aclDate}} </xui-filtered-list-footer> </xui-filtered-list> </div> <div class=\"col-md-3 sticky-object-inspector-container\" ng-if=\"vm.showSidePanel==true\"> <object-inspector token=vm.token node-id=vm.nodeId config-id=vm.tokenConfigId on-close=\"vm.showSidePanel = false;\"> </object-inspector> </div> </div> <script type=text/ng-template id=rule-list-template> <div class=\"rule-row\" ng-class=\"{'debug-red': item.understood === false}\">\n            <div class=\"col-srd-icon ncm-rulehighlight-default\" ng-if=\"vm.canDisplaySrdIcon(item.shadowRuleDetectionResults)\">\n                <xui-popover xui-popover-trigger=\"mouseenter\"\n                             class=\"id-ace-srd-popover\"\n                             xui-popover-placement=\"left\"\n                             xui-popover-status=\"warning\"\n                             xui-popover-title=\"Overlapping Rules\"\n                             xui-popover-content=\"vm.getSrdIconPopoverTemplate\">\n                    <xui-icon icon=\"status_warning\"\n                              class=\"id-ace-srd-icon\"\n                              icon-size=\"medium\"></xui-icon>\n                </xui-popover>\n            </div>\n            <div class=\"col-srd-empty\" ng-if=\"!vm.canDisplaySrdIcon(item.shadowRuleDetectionResults)\"></div>\n            <rule-row clickable=\"true\" rule=\"item\" token-click-handler=\"vm.onTokenClickHandler\" search-term=\"vm.searchTerm\"></rule-row>\n            <div class=\"col-match-counter ncm-rulehighlight-default\" ng-if=\"item.matchCounter >= 0\">{{item.matchCounter}}</div>            \n        </div> </script> <script type=text/ng-template id=ncm-rule-list-srd-template> <div>\n            <div ng-repeat=\"desc in vm.getSrdPopoverDescriptions(item)\">\n                {{desc}}\n            </div>\n            <a role=\"button\" ng-click=\"::vm.onSrdDetailsClickHandler(item)\"_t>Show the details</a>\n        </div> </script> </div>";

/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var accessControlListRule_1 = __webpack_require__(5);
var ShadowRuleDetectionDialogRulesViewModel = /** @class */ (function () {
    function ShadowRuleDetectionDialogRulesViewModel(message, rules) {
        this.message = message;
        this.rules = rules;
    }
    return ShadowRuleDetectionDialogRulesViewModel;
}());
exports.ShadowRuleDetectionDialogRulesViewModel = ShadowRuleDetectionDialogRulesViewModel;
var ShadowRuleDetectionDialogController = /** @class */ (function () {
    /** @ngInject */
    ShadowRuleDetectionDialogController.$inject = ["$scope", "$log", "_t"];
    function ShadowRuleDetectionDialogController($scope, $log, _t) {
        this.$scope = $scope;
        this.$log = $log;
        this._t = _t;
    }
    ShadowRuleDetectionDialogController.prototype.init = function (viewModel) {
        this.viewModel = viewModel;
        this.shadowedOverlappingRules = this.getOverlappingRules(accessControlListRule_1.RuleCheckDetectionType.Shadow);
        this.redundantOverlappingRules = this.getOverlappingRules(accessControlListRule_1.RuleCheckDetectionType.Redundant);
        this.partiallyShadowedOverlappingRules = this.getOverlappingRules(accessControlListRule_1.RuleCheckDetectionType.PartiallyShadow);
        this.partiallyRedundantOverlappingRules = this.getOverlappingRules(accessControlListRule_1.RuleCheckDetectionType.PartiallyRedundant);
        this.ruleNumber = viewModel.currentAce.ordinalNumber;
        this.rulesViewModel = new Array();
        this.rulesViewModel.push(new ShadowRuleDetectionDialogRulesViewModel(this._t("The rule #") + this.ruleNumber +
            this._t(" is fully shadowed by:"), this.shadowedOverlappingRules));
        this.rulesViewModel.push(new ShadowRuleDetectionDialogRulesViewModel(this._t("The rule #") + this.ruleNumber +
            this._t(" is fully redundant because of:"), this.redundantOverlappingRules));
        this.rulesViewModel.push(new ShadowRuleDetectionDialogRulesViewModel(this._t("The rule #") + this.ruleNumber +
            this._t(" is partially shadowed by:"), this.partiallyShadowedOverlappingRules));
        this.rulesViewModel.push(new ShadowRuleDetectionDialogRulesViewModel(this._t("The rule #") + this.ruleNumber +
            this._t(" is partially redundant because of:"), this.partiallyRedundantOverlappingRules));
    };
    ShadowRuleDetectionDialogController.prototype.getOverlappingRules = function (type) {
        var indexes = new Array();
        for (var _i = 0, _a = this.viewModel.currentAce.shadowRuleDetectionResults; _i < _a.length; _i++) {
            var srd = _a[_i];
            if (srd.type === type) {
                for (var _b = 0, _c = srd.overlappingRules; _b < _c.length; _b++) {
                    var index = _c[_b];
                    indexes.push(index);
                }
            }
        }
        var overlappingRules = new Array();
        for (var _d = 0, indexes_1 = indexes; _d < indexes_1.length; _d++) {
            var index = indexes_1[_d];
            overlappingRules.push(this.viewModel.aclRules[index]);
        }
        return overlappingRules;
    };
    return ShadowRuleDetectionDialogController;
}());
exports.ShadowRuleDetectionDialogController = ShadowRuleDetectionDialogController;


/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var objectInspector_directive_1 = __webpack_require__(128);
exports.default = function (module) {
    module.component("objectInspector", objectInspector_directive_1.ObjectInspector);
};


/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(129);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var objectInspector_directive_controller_1 = __webpack_require__(130);
/**
 * Directive function for object inspector component
 */
var ObjectInspector = /** @class */ (function () {
    /** @ngInject */
    ObjectInspector.$inject = ["swUtil"];
    function ObjectInspector(swUtil) {
        this.swUtil = swUtil;
        this.restrict = "E";
        this.transclude = false;
        this.controller = objectInspector_directive_controller_1.ObjectInspectorController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(131);
        this.replace = true;
        this.scope = true;
        this.bindToController = {
            token: "=",
            nodeId: "=",
            configId: "=",
            onClose: "&?"
        };
    }
    return ObjectInspector;
}());
exports.ObjectInspector = ObjectInspector;


/***/ }),
/* 129 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var objectGroup_1 = __webpack_require__(13);
var accessControlListRule_1 = __webpack_require__(5);
var utils_1 = __webpack_require__(6);
var ObjectInspectorController = /** @class */ (function () {
    /** @ngInject */
    ObjectInspectorController.$inject = ["$scope", "$log", "objectGroupService", "objectDefinitionService", "ncmNavigationService", "dialogService", "_t"];
    function ObjectInspectorController($scope, $log, objectGroupService, objectDefinitionService, ncmNavigationService, dialogService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.objectGroupService = objectGroupService;
        this.objectDefinitionService = objectDefinitionService;
        this.ncmNavigationService = ncmNavigationService;
        this.dialogService = dialogService;
        this._t = _t;
        this.versions = new Array();
        this.headerContent = {
            title: "",
            type: "",
            displayVersion: "",
            icon: "group" //TODO replace with appropriate icon when will be available
        };
        this.getPopoverTemplate = { url: "ncm-objects-list-history-template" };
        this.objectInspector_selectedVersion_currentString = this._t("(Current)");
        this.refresh = function () {
            /* tslint:disable:no-bitwise */
            if (_this.token.type & accessControlListRule_1.AceTokenType.ObjectGroup) {
                _this.fetchObjectGroupDefinition(_this.token.value);
                if (!_this.versionChanged) {
                    _this.fetchObjectGroupHistory(_this.token.value);
                }
            }
            else if (_this.token.type & accessControlListRule_1.AceTokenType.Object) {
                _this.fetchObjectDefinition(_this.token.value);
                if (!_this.versionChanged) {
                    _this.fetchObjectDefinitionHistory(_this.token.value);
                }
            }
            /* tslint:enable:no-bitwise */
            _this.versionChanged = false;
        };
        this.onCloseClick = function () {
            _this.onClose();
        };
        this.compareDiffClick = function () {
            var right = _this.selectedVersion;
            var left = _.find(_this.versions, function (v) { return v.downloadTime < right.downloadTime; });
            if (left != null) {
                _this.ncmNavigationService.navigateToCompareGroups({
                    "leftNodeId": _this.nodeId,
                    "leftConfigId": left.configId,
                    "leftGroupName": _this.token.value,
                    "rightNodeId": _this.nodeId,
                    "rightConfigId": right.configId,
                    "rightGroupName": _this.token.value,
                    "referenceType": _this.isObjectItemObjectGroup ?
                        objectGroup_1.ObjectDefinitionReferenceType[objectGroup_1.ObjectDefinitionReferenceType.ObjectGroup] :
                        objectGroup_1.ObjectDefinitionReferenceType[objectGroup_1.ObjectDefinitionReferenceType.Object]
                });
            }
            else {
                _this.dialogService.showError({
                    title: _this._t("Compare") + "\xa0" + (_this.isObjectItemObjectGroup ? _this._t("Object Groups") : _this._t("Objects")),
                    message: _this._t("No previous") + "\xa0" + (_this.isObjectItemObjectGroup ? _this._t("Object Group") : _this._t("Object"))
                        + "\xa0" + _this._t("version found to compare against.")
                });
            }
        };
        this.isReferenceType = function (token) {
            /* tslint:disable:no-bitwise */
            return (token.type & accessControlListRule_1.AceTokenType.ObjectGroup) === accessControlListRule_1.AceTokenType.ObjectGroup ||
                (token.type & accessControlListRule_1.AceTokenType.Object) === accessControlListRule_1.AceTokenType.Object;
            /* tslint:enable:no-bitwise */
        };
        this.onTokenClickHandler = function (token) {
            _this.token = token;
        };
        this.versionChanged = false;
        var refresh = function () { _this.refresh(); };
        this.$scope.$watchCollection(function () { return [_this.token.value, _this.nodeId, _this.configId]; }, refresh);
    }
    ObjectInspectorController.prototype.renderVersionDisplayName = function (item) {
        var downloadTimeFormatted = utils_1.default.toLocalDateString(new Date(item.downloadTime));
        return item.isLatest ? String.format("{0}" + this.objectInspector_selectedVersion_currentString, downloadTimeFormatted) :
            downloadTimeFormatted;
    };
    ;
    ObjectInspectorController.prototype.onVersionClick = function (version) {
        this.versionChanged = true;
        this.selectedVersion = version;
        this.configId = version.configId;
    };
    ;
    ObjectInspectorController.prototype.fetchObjectGroupDefinition = function (name) {
        var _this = this;
        this.objectGroupService.getObjectGroup(this.nodeId, name, this.configId)
            .then(function (result) {
            if (result != null) {
                result.members.forEach(function (item) {
                    item.tokens = _this.tokenize(item.rawData, item);
                });
                _this.isObjectItemObjectGroup = true;
                _this.objectItem = result;
                _this.headerContent.title = _this.formatTitle(result.type, result.name);
                _this.headerContent.type = _this._t("Object group");
            }
            else {
                _this.onCloseClick();
                _this.dialogService.showWarning({
                    message: _this._t("Object Group was not found.")
                });
            }
        });
    };
    ;
    ObjectInspectorController.prototype.fetchObjectDefinition = function (name) {
        var _this = this;
        this.objectDefinitionService.getObjectDefinition(this.nodeId, name, this.configId)
            .then(function (result) {
            if (result != null) {
                _this.isObjectItemObjectGroup = false;
                _this.objectItem = result;
                _this.headerContent.title = _this.formatTitle(result.type, result.name);
                _this.headerContent.type = _this._t("Object");
            }
            else {
                _this.onCloseClick();
                _this.dialogService.showWarning({
                    message: _this._t("Object was not found.")
                });
            }
        });
    };
    ;
    ObjectInspectorController.prototype.fetchObjectGroupHistory = function (name) {
        var _this = this;
        this.objectGroupService.getObjectGroupHistory(this.nodeId, name, this.configId)
            .then(function (result) {
            if (result != null && result.historyEntries != null) {
                _this.versions = result.historyEntries;
                _this.selectedVersion = _.find(_this.versions, function (v) { return v.isPreselected === true; });
                _this.headerContent.displayVersion = utils_1.default.toLocalDateString(new Date(_this.selectedVersion.downloadTime));
            }
        });
    };
    ;
    ObjectInspectorController.prototype.fetchObjectDefinitionHistory = function (name) {
        var _this = this;
        this.objectDefinitionService.getObjectDefinitionHistory(this.nodeId, name, this.configId)
            .then(function (result) {
            if (result != null && result.historyEntries != null) {
                _this.versions = result.historyEntries;
                _this.selectedVersion = _.find(_this.versions, function (v) { return v.isPreselected === true; });
                _this.headerContent.displayVersion = _this.selectedVersion.isLatest
                    ? String.format("{0}" + _this.objectInspector_selectedVersion_currentString, _this.selectedVersion.downloadTime)
                    : _this.selectedVersion.downloadTime;
            }
        });
    };
    ;
    ObjectInspectorController.prototype.formatTitle = function (type, name) {
        return type + " " + name;
    };
    ;
    ObjectInspectorController.prototype.tokenize = function (line, member) {
        var _this = this;
        var arr = new Array();
        line.split(" ")
            .forEach(function (item) {
            var token = new accessControlListRule_1.AceToken();
            token.value = item;
            token.type = _this.getAceTokenType(member, item);
            arr.push(token);
        });
        return arr;
    };
    ;
    ObjectInspectorController.prototype.getAceTokenType = function (member, value) {
        if (member.isReference === true && member.value === value) {
            return member.referenceType === objectGroup_1.ObjectDefinitionReferenceType.ObjectGroup ?
                accessControlListRule_1.AceTokenType.ObjectGroup :
                accessControlListRule_1.AceTokenType.Object;
        }
        return accessControlListRule_1.AceTokenType.Unknown;
    };
    ;
    return ObjectInspectorController;
}());
exports.ObjectInspectorController = ObjectInspectorController;


/***/ }),
/* 131 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-object-inspector> <div class=vertical-divider> <div class=ncm-object-inspector-header> <sw-object-inspector-header header-text=vm.headerContent.title description=vm.headerContent.type icon=vm.headerContent.icon close-sidebar=vm.onCloseClick> <div> <xui-popover xui-popover-content=vm.getPopoverTemplate xui-popover-trigger=click xui-popover-placement=left> <div class=\"object-inspector-header ncm-object-inspector-header-transcluded\"> <xui-button size=small display-style=link> {{vm.selectedVersion.downloadTime}} </xui-button> {{vm.selectedVersion.isLatest ? vm.objectInspector_selectedVersion_currentString : \"\"}} </div> </xui-popover> </div> </sw-object-inspector-header> </div> <div class=ncm-object-inspector-button> <xui-button ng-click=vm.compareDiffClick() display-style=tertiary _t> Compare Diff </xui-button> </div> <xui-divider></xui-divider> <div class=ncm-object-inspector-description> <div> <strong><span _t>Description</span></strong> </div> <div ng-if=\"vm.objectItem.description.length > 0\" class=ncm-object-inspector-description-content> {{vm.objectItem.description}} </div> </div> <xui-divider></xui-divider> <div class=ncm-object-inspector-objects> <sw-object-inspector-content> <div ng-if=\"vm.isObjectItemObjectGroup==true\"> <div class=ncm-object-inspector-objects-title> <strong><span _t>Objects</span> ({{vm.objectItem.members.length}})</strong> </div> <div ng-repeat=\"item in vm.objectItem.members\" class=ncm-object-inspector-objects-rows> <span ng-repeat=\"token in item.tokens\"> <span ng-if=\"::vm.isReferenceType(token)===false\"> {{token.value}} </span> <a role=button ng-click=::vm.onTokenClickHandler(token) ng-if=\"::vm.isReferenceType(token)===true\"> {{token.value}} </a>&nbsp; </span> </div> </div> <div ng-if=\"vm.isObjectItemObjectGroup==false\"> <div class=ncm-object-inspector-objects-title><strong><span _t>Value</span></strong></div> <div ng-repeat=\"value in vm.objectItem.values\" class=ncm-object-inspector-objects-rows> <span>{{value}}</span> </div> </div> </sw-object-inspector-content> </div> </div> <script type=text/ng-template id=ncm-objects-list-history-template> <div>\n            <div ng-repeat=\"version in vm.versions\">\n                <xui-button size=\"small\"\n                            ng-click=\"vm.onVersionClick(version)\"\n                            display-style=\"link\">\n                    <span ng-style=\"{ 'font-weight': version.configId == vm.selectedVersion.configId ? 'bold' : 'normal' }\">{{version.downloadTime}}</span>\n                </xui-button>\n                <span>{{version.isLatest ? vm.objectInspector_selectedVersion_currentString : \"\"}}</span>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var aclPicker_directive_1 = __webpack_require__(133);
exports.default = function (module) {
    module.component("aclPicker", aclPicker_directive_1.AclPicker);
};


/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var aclPicker_directive_controller_1 = __webpack_require__(17);
var AclPicker = /** @class */ (function () {
    function AclPicker() {
        this.restrict = "E";
        this.transclude = false;
        this.controller = aclPicker_directive_controller_1.AclPickerController;
        this.controllerAs = "ctrl";
        this.template = __webpack_require__(134);
        this.replace = true;
        this.scope = true;
        this.bindToController = {
            itemsDataModel: "=",
            onChanged: "&?",
            nodeCaption: "@?"
        };
    }
    return AclPicker;
}());
exports.AclPicker = AclPicker;


/***/ }),
/* 134 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-acl-picker> <xui-dropdown caption={{ctrl.nodeCaption}} display-value=nodeName display-format=\"{{ctrl.itemsDataModel.isSameNode ? ctrl.aclSameNodeString : item.nodeName}}\" selected-item=ctrl.itemsDataModel.selectedNode items-source=ctrl.itemsDataModel.nodes on-changed=\"ctrl.onNodeChanged(newValue, oldValue)\" class=ncm-acl-picker__node> </xui-dropdown> <xui-popover xui-popover-trigger=mouseenter xui-popover-placement=left xui-popover-content=\"{url: 'ncm-acl-picker-interface-template'}\"> <xui-dropdown caption={{ctrl.aclCaptionInterfaceString}} display-value-fn=ctrl.renderInterface(item) display-format=\"{{ctrl.itemsDataModel.isSameInterface ? ctrl.aclSameInterfaceString : item.interfaceName}}\" selected-item=ctrl.itemsDataModel.selectedInterface items-source=ctrl.itemsDataModel.interfaces on-changed=\"ctrl.onInterfaceChanged(newValue, oldValue)\" class=ncm-acl-picker__interface> </xui-dropdown> </xui-popover> <xui-dropdown caption={{ctrl.aclCaptionAclString}} display-value-fn=ctrl.renderAclName(item) display-format=\"{{ctrl.itemsDataModel.isSameAcl ? ctrl.aclSameAclString : item.aclName}}\" selected-item=ctrl.itemsDataModel.selectedAcl items-source=ctrl.itemsDataModel.selectedInterface.acl on-changed=\"ctrl.onAclChanged(newValue, oldValue)\" class=\"{{ctrl.nodeCaption == ctrl.aclBaseNodeString ? 'ncm-acl-picker__acl base_node' : 'ncm-acl-picker__acl compared_node'}}\"> </xui-dropdown> <xui-dropdown caption={{ctrl.aclCaptionVersionString}} display-value-fn=ctrl.renderVersion(item) selected-item=ctrl.itemsDataModel.selectedVersion items-source=ctrl.itemsDataModel.selectedAcl.history on-changed=\"ctrl.onVersionChanged(newValue, oldValue)\" class=ncm-acl-picker__version> </xui-dropdown> <script type=text/ng-template id=ncm-acl-picker-interface-template> <div>\n            {{ctrl.getSelectedInterfaceName()}}\n        </div> </script> </div> ";

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ruleRow_directive_1 = __webpack_require__(136);
exports.default = function (module) {
    module.component("ruleRow", ruleRow_directive_1.RuleRow);
};


/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(137);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var angularDirectiveUtils_1 = __webpack_require__(9);
var angularDirectiveUtils_2 = __webpack_require__(9);
var ruleRow_directive_controller_1 = __webpack_require__(138);
/**
 * Directive function for rule list component
 */
var RuleRow = /** @class */ (function () {
    /** @ngInject */
    RuleRow.$inject = ["swUtil"];
    function RuleRow(swUtil) {
        this.swUtil = swUtil;
        this.restrict = angularDirectiveUtils_1.Restrict.element;
        this.transclude = false;
        this.controller = ruleRow_directive_controller_1.RuleRowController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(139);
        this.replace = true;
        this.scope = {
            clickable: angularDirectiveUtils_2.BindingScope.oneWay,
            rule: angularDirectiveUtils_2.BindingScope.twoWay,
            tokenClickHandler: angularDirectiveUtils_2.BindingScope.twoWay,
            searchTerm: angularDirectiveUtils_2.BindingScope.twoWayOptional,
            highlightStatus: angularDirectiveUtils_2.BindingScope.twoWay
        };
    }
    return RuleRow;
}());
exports.RuleRow = RuleRow;


/***/ }),
/* 137 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var accessControlListRule_1 = __webpack_require__(5);
var RuleRowController = /** @class */ (function () {
    /** @ngInject */
    RuleRowController.$inject = ["$scope", "$log", "aclRuleHighlightingService", "$window", "$sce"];
    function RuleRowController($scope, $log, aclRuleHighlightingService, $window, $sce) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.aclRuleHighlightingService = aclRuleHighlightingService;
        this.$window = $window;
        this.$sce = $sce;
        this.activate = function () {
            if (_this.model != null) {
                _this.$log.info("RuleRowController:Activate() for rule " + _this.model.rawData + " (clickable = " + _this.clickable + ")");
            }
        };
        this.isInactive = function () {
            if (_this.model != null) {
                /* tslint:disable:no-bitwise */
                var res = _.find(_this.model.tokens, function (token) {
                    return (token.type & accessControlListRule_1.AceTokenType.Keyword) === accessControlListRule_1.AceTokenType.Keyword && token.value === "inactive";
                });
                /* tslint:enable:no-bitwise */
                return !_.isUndefined(res);
            }
        };
        this.isUrlType = function (token) {
            return token.type === accessControlListRule_1.AceTokenType.Url;
        };
        this.resolveTokenCssClass = function (token) {
            return _this.aclRuleHighlightingService.resolveCssName(token);
        };
        this.isReferenceType = function (token) {
            /* tslint:disable:no-bitwise */
            return (token.type & accessControlListRule_1.AceTokenType.ObjectGroup) === accessControlListRule_1.AceTokenType.ObjectGroup ||
                (token.type & accessControlListRule_1.AceTokenType.Object) === accessControlListRule_1.AceTokenType.Object;
            /* tslint:enable:no-bitwise */
        };
        this.openUrl = function (url) {
            _this.$window.open(url, "_blank");
        };
        this.isObjectGroupLink = function (token) {
            return _this.clickable && _this.isReferenceType(token);
        };
        this.buildRemarkAce = function () {
            var tokensValues = _this.model.tokens.map(function (x) { return x.value; });
            return tokensValues.join(" ");
        };
        this.highlightSearch = function (token) {
            if (token.toSelect) {
                var regex = _.escapeRegExp("(" + token.toSelect + ")");
                var pattern = new RegExp(regex, "gi");
                var text = token.value.replace(pattern, "<span class=\"xui-highlighted\">$1</span>");
                return _this.$sce.trustAsHtml(text);
            }
            return token.value;
        };
        this.clickable = $scope.clickable;
        this.model = $scope.rule;
        this.tokenClickHandler = $scope.tokenClickHandler;
        this.searchTerm = $scope.searchTerm;
        this.highlightStatus = $scope.highlightStatus;
    }
    RuleRowController.prototype.getOrdinalNumberHighlightClass = function () {
        if (this.highlightStatus != null) {
            return "highlight-status-" + this.highlightStatus;
        }
    };
    return RuleRowController;
}());
exports.RuleRowController = RuleRowController;


/***/ }),
/* 139 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-rule-row ng-init=vm.activate()> <div class=\"col-ordinal-number ncm-rulehighlight-ordinalnumber id-ace-ordinalnumber {{vm.getOrdinalNumberHighlightClass()}}\" id=id-ace-ordinalnumber status={{vm.getOrdinalNumberHighlightClass()}}>{{vm.model.ordinalNumber}}</div> <div class=col-raw-data> <span ng-if=\"vm.model.isRemark===true\" id=id-highlight class=ncm-rulehighlight-remark ng-bind-html=\"vm.buildRemarkAce() | xuiHighlight:vm.searchTerm\"> </span> <span class=col-disabled-icon ng-if=vm.isInactive()> <xui-icon class=id-ace-disabled-icon icon=status_disabled icon-size=small tool-tip=Disabled> </xui-icon> </span> <span class=ncm-rule-token ng-repeat=\"token in vm.model.tokens\" ng-if=\"vm.model.isRemark===false\"> <span ng-if=\"vm.isUrlType(token)==false\"> <span id=id-highlight ng-class=vm.resolveTokenCssClass(token) ng-if=!vm.isObjectGroupLink(token)><span ng-bind-html=::vm.highlightSearch(token)></span></span> <a role=button ng-click=vm.tokenClickHandler(token) id=id-highlight ng-class=vm.resolveTokenCssClass(token) ng-if=vm.isObjectGroupLink(token)><span ng-bind-html=::vm.highlightSearch(token)></span></a> </span> <span ng-if=\"vm.isUrlType(token)==true\"> <a role=button class=ncm-rule-url ng-click=vm.openUrl(token.value)> {{token.value}} <span class=col-goto-icon> <xui-icon class=id-ace-goto-icon icon=goto icon-size=small></xui-icon> </span> </a> </span> </span> </div> </div>";

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var vpcInterfaceInfo_directive_1 = __webpack_require__(141);
var vpcInterfaceInfo_directive_controller_1 = __webpack_require__(27);
__webpack_require__(26);
exports.default = function (module) {
    module.controller("VpcInterfaceInfoController", vpcInterfaceInfo_directive_controller_1.VpcInterfaceInfoController);
    module.component("vpcInterfaceInfo", vpcInterfaceInfo_directive_1.VpcInterfaceInfo);
};


/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(26);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var vpcInterfaceInfo_directive_controller_1 = __webpack_require__(27);
var VpcInterfaceInfo = /** @class */ (function () {
    /** @ngInject */
    VpcInterfaceInfo.$inject = ["swUtil"];
    function VpcInterfaceInfo(swUtil) {
        var _this = this;
        this.swUtil = swUtil;
        this.restrict = "E";
        this.transclude = false;
        this.controller = vpcInterfaceInfo_directive_controller_1.VpcInterfaceInfoController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(142);
        this.replace = true;
        this.scope = {
            nodeName: "=",
            nodeId: "=",
            vpcStatus: "=",
            configChangedDate: "=",
            vpcConfiguration: "=",
            memberInterfacesConfiguration: "="
        };
        this.link = function (scope, element, attrs) {
            _this.swUtil.initComponent(attrs, "vpcInterfaceInfo");
            scope.nodeName = attrs.nodeName;
            scope.nodeId = attrs.nodeId;
            scope.vpcStatus = attrs.vpcStatus;
            scope.configChangedDate = attrs.configChangedDate;
            scope.vpcConfiguration = attrs.vpcConfiguration;
            scope.memberInterfacesConfiguration = attrs.memberInterfacesConfiguration;
        };
    }
    return VpcInterfaceInfo;
}());
exports.VpcInterfaceInfo = VpcInterfaceInfo;
;


/***/ }),
/* 142 */
/***/ (function(module, exports) {

module.exports = "<div class=vpc-interface-info ng-init=vm.activate()> <div class=header-bg> <div class=entity-title> <a ng-if=\"vm.vpcStatus !== 5\" class=entity-name href={{vm.getNodeDetailsUrl()}}>{{vm.nodeName}}</a> <span ng-if=\"vm.vpcStatus === 5\" class=entity-name>{{vm.nodeName}}</span> </div> <div class=entity-description> <div class=entity-description> <span class=description id=compared-node-description ng-bind-html=vm.getEntityDescription()></span> </div> </div> </div> <div class=not-empty-data ng-if=vm.isStatusOk()> <div class=table-td> <div class=vpc-interface-table id=vpc-config> <div ng-repeat=\"item in vm.vpcConfiguration\"> <div class=\"td-rawData {{vm.getCssClass(item.ordinalNumber)}}\"> <div id={{item.ordinalNumber}} ng-class=\"{vpcInterfaceCommand: {{item.isInterfaceCommand}}}\">{{item.rawData}}</div> </div> </div> </div> </div> <div> <xui-divider></xui-divider> </div> <div class=header-bg> <div> <span class=description _t>Member Interfaces</span> </div> </div> <div class=table-td> <div class=vpc-interface-table id=interface-config> <div ng-repeat=\"item in vm.memberInterfacesConfiguration\"> <div class=\"td-rawData {{vm.getCssClass(item.ordinalNumber)}}\"> <div id={{item.ordinalNumber}} ng-class=\"{vpcInterfaceCommand: {{item.isInterfaceCommand}}}\">{{item.rawData}}</div> </div> </div> </div> </div> </div> <div class=vpc-not-found ng-if=\"vm.isActivated && !vm.isStatusOk()\"> <div class=vpc-not-found-icon> <xui-icon class=icon icon=nexus-device icon-size=xlarge status=unknown></xui-icon> </div> <div class=\"vpc-not-found-description important description\"> <span _t>Unable to fetch config files</span> </div> <div class=vpc-not-found-description id=vpc-not-found-link> <a href={{vm.vpcNotFoundData.url}}>{{vm.vpcNotFoundData.description}}</a> </div> </div> </div> ";

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var nodeSelector_directive_1 = __webpack_require__(144);
exports.default = function (module) {
    module.component("nodeSelector", nodeSelector_directive_1.NodeSelector);
};


/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(145);

"use strict";
// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var nodeSelector_directive_controller_1 = __webpack_require__(146);
var NodeSelector = /** @class */ (function () {
    /** @ngInject */
    NodeSelector.$inject = ["swUtil"];
    function NodeSelector(swUtil) {
        this.swUtil = swUtil;
        this.restrict = "E";
        this.transclude = false;
        this.controller = nodeSelector_directive_controller_1.NodeSelectorController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(147);
        this.replace = true;
        this.scope = {
            selection: "<selection",
            filterBinaryConfigs: "@filterBinaryConfigs"
        };
    }
    return NodeSelector;
}());
exports.NodeSelector = NodeSelector;


/***/ }),
/* 145 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NodeSelectorController = /** @class */ (function () {
    /** @ngInject */
    NodeSelectorController.$inject = ["$scope", "$log", "ncmNodeDetailsService", "swSwisDataMappingService", "$q", "ncmConfigService", "_t"];
    function NodeSelectorController($scope, $log, ncmNodeDetailsService, swSwisDataMappingService, $q, ncmConfigService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.ncmNodeDetailsService = ncmNodeDetailsService;
        this.swSwisDataMappingService = swSwisDataMappingService;
        this.$q = $q;
        this.ncmConfigService = ncmConfigService;
        this._t = _t;
        this.binaryConfigTypeName = "Device State";
        this.filterBinaryConfigs = false;
        this.nodes = [];
        this.configTypes = [];
        this.filterProperties = [];
        this.filterValues = {
            "Status": [],
            "MachineType": [],
            "Vendor": []
        };
        this.oldFilterValues = this.filterValues;
        this.nodeFilterPropertyGroup = this._t("Node");
        this.filterPropertyGroups = [
            this.nodeFilterPropertyGroup
        ];
        this.sorting = {
            sortableColumns: [
                { id: "Caption", label: this._t("Caption") },
            ],
            sortBy: {
                id: "Caption",
                label: this._t("Caption")
            },
            direction: "asc"
        };
        this.options = {
            allowSelectAllPages: false,
            smartMode: false,
            rowPadding: "narrow",
            pageSize: 10,
            hidePagination: false,
            showEmptyFilterProperties: true,
            showAddRemoveFilterProperty: false,
            sortableColumns: this.sorting.sortableColumns,
            showMorePropertyValuesThreshold: 10,
            templateUrl: "node-row-template",
            searchPlaceholder: this._t("Search...")
        };
        this.pagination = {
            page: 1,
            pageSize: 10
        };
        this.searching = "";
        this.isFirstLoad = true;
        this.onRefresh = function (filters, pagination, sorting, searching) {
            if (_this.isFirstLoad) {
                _this.isFirstLoad = false;
                return _this.$q.resolve();
            }
            _this.searching = searching || "";
            var getNodes = _this.ncmNodeDetailsService.getNodes({
                currentPage: pagination.page,
                pageSize: pagination.pageSize,
                search: searching || "",
                sortByColumn: sorting.sortBy.id,
                sortByDirection: sorting.direction,
                filterValues: filters
            })
                .then(function (result) {
                _this.nodes = _.map(result.nodes, _this.applyNodeSelection);
                _this.pagination.total = result.totalCount;
            });
            var getConfigTypes = _this.ncmConfigService.getConfigTypes()
                .then(function (configTypes) {
                if (_this.filterBinaryConfigs) {
                    _this.configTypes = configTypes.filter(function (x) { return x !== _this.binaryConfigTypeName; });
                }
                else {
                    _this.configTypes = configTypes;
                }
            });
            var getFilters = _this.getFilters(filters);
            return _this.$q.all([getNodes, getFilters, getConfigTypes]);
        };
        this.getFilterProperties = function () {
            if (_.isEmpty(_this.filterProperties)) {
                return _this.ncmNodeDetailsService.getFilters(_this.filterValues)
                    .then(function (filterProperties) {
                    _this.filterProperties = _.map(filterProperties, _this.decorateFilter);
                    return _this.filterProperties;
                });
            }
            else {
                return _this.filterProperties;
            }
        };
        this.changeSelection = function (item) {
            if (item.isSelected) {
                _this.selection.selectedNodes.push(item);
            }
            else {
                _this.removeItemFromSelection(item);
            }
        };
        this.decorateFilter = function (filter) {
            if (!filter.parent) {
                filter.parent = _this.nodeFilterPropertyGroup;
                filter.parentTitle = _this.nodeFilterPropertyGroup;
            }
            filter.values.forEach(function (filterValue) {
                if (filter.refId === "Status") {
                    filterValue.title = _.capitalize(_this.swSwisDataMappingService.getStatus(filterValue.id));
                }
                if (filterValue.count === 0) {
                    filterValue.count = undefined;
                }
            });
            return filter;
        };
        this.applyNodeSelection = function (node) {
            node.isSelected = _this.selection.selectedNodes.some(function (n) { return n.nodeId === node.nodeId; });
            return node;
        };
        this.selection = this.$scope.selection;
        this.filterBinaryConfigs = this.$scope.filterBinaryConfigs;
        this.$scope.$watchCollection(function () { return _this.selection.selectedNodes; }, function () {
            _this.nodes = _.map(_this.nodes, _this.applyNodeSelection);
        });
    }
    NodeSelectorController.prototype.removeItemFromSelection = function (item) {
        var index = 0;
        for (var _i = 0, _a = this.selection.selectedNodes; _i < _a.length; _i++) {
            var node = _a[_i];
            if (node.nodeId === item.nodeId) {
                this.selection.selectedNodes.splice(index, 1);
                return;
            }
            index++;
        }
    };
    NodeSelectorController.prototype.clearSelection = function () {
        this.selection.selectedNodes.splice(0);
    };
    NodeSelectorController.prototype.getFilters = function (filters) {
        var _this = this;
        if (this.isDifference(filters, this.oldFilterValues)) {
            this.ncmNodeDetailsService.getFilters(filters).then(function (filterProperties) {
                _this.filterProperties = _.map(filterProperties, _this.decorateFilter);
            });
        }
        this.oldFilterValues = angular.copy(this.filterValues);
    };
    NodeSelectorController.prototype.isDifference = function (newFilters, oldFilters) {
        var keys = Object.keys(newFilters);
        for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
            var key = keys_1[_i];
            var newValues = newFilters[key];
            var oldValues = oldFilters[key];
            if (newValues.length !== oldValues.length) {
                return true;
            }
        }
        return false;
    };
    return NodeSelectorController;
}());
exports.NodeSelectorController = NodeSelectorController;


/***/ }),
/* 147 */
/***/ (function(module, exports) {

module.exports = " <div class=node-selector-with-config-types> <div class=node-selector> <xui-filtered-list items-source=vm.nodes on-refresh=\"vm.onRefresh(filters, pagination, sorting, searching)\" options=vm.options sorting=vm.sorting pagination-data=vm.pagination filter-property-groups=vm.filterPropertyGroups filter-properties=vm.filterProperties filter-properties-fn=vm.getFilterProperties() filter-values=vm.filterValues controller=vm layout=fill> </xui-filtered-list> </div> <div class=config-types-with-selection> <div class=config-type-selection> <h4 class=title _t>APPLY TO</h4> <xui-checkbox-group class=config-types ng-model=vm.selection.selectedConfigTypes> <xui-checkbox class=ncm-config-type ng-repeat=\"configType in vm.configTypes\" value=configType> {{configType}} </xui-checkbox> </xui-checkbox-group> </div> <div class=node-selection ng-if=\"vm.selection.selectedNodes.length > 0\"> <div class=row> <div class=\"col-xs-12 col-sm-12 col-md-7\"> <h4 class=title><span _t>SELECTED:</span> ({{vm.selection.selectedNodes.length}})</h4> </div> <div class=\"col-xs-12 col-sm-12 col-md-5\"> <xui-button class=clear-all ng-click=vm.clearSelection() display-style=link _t>Clear all</xui-button> </div> </div> <xui-listview class=selected-nodes-listview stripe=false row-padding=compact items-source=vm.selection.selectedNodes template-url=selected-node-template controller=vm layout=fill> </xui-listview> </div> </div> <script type=text/ng-template id=node-row-template> <div class=\"node-row\">\n            <div>\n                <xui-checkbox class=\"node-checkbox\" ng-model=\"item.isSelected\" ng-change=\"vm.changeSelection(item)\"></xui-checkbox>\n            </div>\n            <div>\n                <span class=\"node-name\" ng-bind-html=\"item.nodeName | xuiHighlight:vm.searching\"></span>\n            </div>\n        <div> </script> <script type=text/ng-template id=selected-node-template> <div class=\"selected-node\" ng-click=\"vm.removeItemFromSelection(item)\">\n            <div class=\"node-name\">{{item.nodeName}}</div>\n            <div class=\"icon-remove\">\n                <xui-icon icon-size=\"small\" icon=\"close\"></xui-icon>\n            </div>\n        </div> </script> </div>";

/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var aclConfigInfo_directive_controller_1 = __webpack_require__(28);
var aclConfigInfo_directive_1 = __webpack_require__(149);
__webpack_require__(29);
__webpack_require__(10);
exports.default = function (module) {
    module.controller("AclConfigInfoController", aclConfigInfo_directive_controller_1.AclConfigInfoController);
    module.component("aclConfigInfo", aclConfigInfo_directive_1.AclConfigInfo);
};


/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(29);

"use strict";
/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var aclConfigInfo_directive_controller_1 = __webpack_require__(28);
var AclConfigInfo = /** @class */ (function () {
    /** @ngInject */
    AclConfigInfo.$inject = ["swUtil", "$log"];
    function AclConfigInfo(swUtil, $log) {
        var _this = this;
        this.swUtil = swUtil;
        this.$log = $log;
        this.restrict = "E";
        this.transclude = false;
        this.controller = aclConfigInfo_directive_controller_1.AclConfigInfoController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(150);
        this.replace = true;
        this.scope = {
            aceLines: "=",
            aclInfo: "<",
            searchTerm: "=",
            tokenClickHandler: "="
        };
        this.link = function (scope, element, attrs) {
            _this.swUtil.initComponent(attrs, "aclConfigInfo");
            scope.aceLines = attrs.aceLines;
            scope.aclInfo = attrs.aclInfo;
            scope.$watch("searchTerm", function (newValue, oldValue) {
                if (oldValue !== newValue) {
                    scope.onReload();
                }
            }, true);
        };
    }
    return AclConfigInfo;
}());
exports.AclConfigInfo = AclConfigInfo;
;


/***/ }),
/* 150 */
/***/ (function(module, exports) {

module.exports = "<div class=acl-config-info> <div class=header-bg> <div> <strong class=entity-name>{{vm.aclInfo.aclName}}</strong> <span ng-if=\"vm.aclInfo.aclProperties.interfaces.length > 0\" _t>from</span> <span ng-if=\"vm.aclInfo.aclProperties.interfaces.length <= 2\" ng-repeat=\"aclInterface in vm.aclInfo.aclProperties.interfaces\"> <strong>{{aclInterface}}</strong><span ng-if=\"$last == false\">, </span> </span> <span ng-if=\"vm.aclInfo.aclProperties.interfaces.length > 2\"> <strong>{{vm.aclInfo.aclProperties.interfaces[0]}}</strong>, <strong>{{vm.aclInfo.aclProperties.interfaces[1]}}</strong> <span _t>and</span> {{vm.aclInfo.aclProperties.interfaces.length-2}} <span _t>more</span> </span> <span _t>on</span> <strong> <a href={{vm.getAclDetailsUrl()}}> {{vm.aclInfo.aclNode.nodeName}} </a> </strong> </div> <div> <span _t>ACL changed</span> <strong> <a href={{vm.getAclDetailsUrl()}}> {{vm.toLocalDate(vm.aclInfo.aclProperties.modificationTime)}} </a> </strong> <span ng-if=vm.aclInfo.aclProperties.current _t>(Current)</span> </div> </div> <div ng-if=\"vm.searchTerm && vm.aceDiff.length === 0\" class=xui-listview__empty _t> No results were found that match your query </div> <xui-listview class=xui-listview-diff row-padding=none items-source=vm.aceLines template-url=diff-item-template controller=vm> </xui-listview> <script type=text/ng-template id=diff-item-template> <rule-row class=\"rule-row {{vm.getCssClass(item.index)}}\" clickable=\"true\" token-click-handler=\"vm.tokenClickHandler\" name=\"rule-row-{{$index}}\" ng-class=\"$index%2 ? 'stripe-odd-bg' : 'stripe-even-bg'\" rule=\"item.accessControlListRule\" search-term=\"vm.$scope.searchTerm\" highlight-status=\"item.diffStatus\"></rule-row> </script> </div>";

/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baselineInspector_directive_1 = __webpack_require__(152);
exports.default = function (module) {
    module.component("baselineInspector", baselineInspector_directive_1.BaselineInspector);
};


/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var baselineInspector_directive_controller_1 = __webpack_require__(153);
var BaselineInspector = /** @class */ (function () {
    /** @ngInject */
    BaselineInspector.$inject = ["swUtil"];
    function BaselineInspector(swUtil) {
        this.swUtil = swUtil;
        this.restrict = "E";
        this.transclude = false;
        this.controller = baselineInspector_directive_controller_1.BaselineInspectorController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(154);
        this.replace = true;
        this.scope = true;
        this.bindToController = {
            baselineConfig: "=",
            onClose: "&?"
        };
    }
    return BaselineInspector;
}());
exports.BaselineInspector = BaselineInspector;


/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var baselineConfig_1 = __webpack_require__(11);
var BaselineInspectorController = /** @class */ (function () {
    /** @ngInject */
    BaselineInspectorController.$inject = ["$scope", "$log", "baselineManagementService", "ncmNavigationService", "_t"];
    function BaselineInspectorController($scope, $log, baselineManagementService, ncmNavigationService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.baselineManagementService = baselineManagementService;
        this.ncmNavigationService = ncmNavigationService;
        this._t = _t;
        this.gridPagination = {
            page: 1,
            pageSize: 10
        };
        this.gridOptions = {
            hideSearch: true
        };
        this.filteredBaselineViolations = [];
        this.allBaselineViolations = [];
        this.sortBy = { name: "Status", value: "state" };
        this.sortableColumns = [
            { name: this._t("Status"), value: "state" },
            { name: this._t("Name"), value: "nodeName" }
        ];
        this.sortDirection = "asc";
        this.configTypes = [];
        this.baselineInspectorSortCaption = this._t("Sort");
        this.baselineInspectorShowCaption = this._t("Show");
        this.baselineInspectorSearchPlaceholder = this._t("Search...");
        this.baselineInspectorErrorTitle = this._t("Error");
        this.onRefresh = function () {
            if (_this.baselineConfig == null) {
                return;
            }
            _this.baselineManagementService.getBaselineViolations(_this.baselineConfig.baselineId).then(function (baselineViolations) {
                _this.allBaselineViolations = baselineViolations;
                _this.sortBy = { name: _this._t("Status"), value: "state" };
                _this.sortDirection = "asc";
                _this.configTypes = _this.getConfigTypes(baselineViolations);
                _this.selectedConfigType = { name: _this._t("All"), value: "" };
                _this.searchValue = "";
                _this.filterBaselineViolations();
            });
        };
        this.closeBaselineInspector = function () {
            _this.onClose();
        };
        this.doSearch = function (value, cancellation) {
            _this.searchValue = value;
            _this.filterBaselineViolations();
        };
        this.clearSearch = function () {
            _this.searchValue = "";
            _this.filterBaselineViolations();
        };
        this.getConfigTypes = function (baselineViolations) {
            var configTypes = [{ name: _this._t("All"), value: "" }];
            _.uniqBy(baselineViolations, "configType").forEach(function (v) {
                configTypes.push({ name: v.configType, value: v.configType });
            });
            return configTypes;
        };
        this.filterBaselineViolations = function () {
            var filteredBaselineViolations = [];
            if (_this.selectedConfigType.value !== "") {
                filteredBaselineViolations = _.orderBy(_this.allBaselineViolations
                    .filter(function (v) { return v.configType === _this.selectedConfigType.value; }), [_this.sortBy.value], [_this.sortDirection]);
            }
            else {
                filteredBaselineViolations = _.orderBy(_this.allBaselineViolations, [_this.sortBy.value], [_this.sortDirection]);
            }
            if (_this.searchValue !== "") {
                var pattern_1 = new RegExp(_.escapeRegExp(_this.searchValue), "i");
                _this.filteredBaselineViolations = filteredBaselineViolations.filter(function (v) {
                    return pattern_1.test(v.nodeName);
                });
            }
            else {
                _this.filteredBaselineViolations = filteredBaselineViolations;
            }
        };
        this.configTypeChanged = function (newValue, oldValue) {
            _this.filterBaselineViolations();
        };
        this.sortChanged = function (newValue, oldValue) {
            _this.filterBaselineViolations();
        };
        this.showBaselineDiffLink = function (item) {
            var validStates = [baselineConfig_1.BaselineViolationState.Mismatches, baselineConfig_1.BaselineViolationState.NoIssues];
            return item.configId != null && validStates.indexOf(item.state) >= 0;
        };
        this.showBaselineViolationErrorPopover = function (state) {
            return state === baselineConfig_1.BaselineViolationState.Error;
        };
        this.getBaselineDiffUrl = function (item) {
            return _this.ncmNavigationService.getBaselineDiffUrl(_this.baselineConfig.baselineId, item.configId);
        };
        this.showBaselineViolationStatusIcon = function (state) {
            return state !== baselineConfig_1.BaselineViolationState.Unknown;
        };
        var onRefresh = function () { _this.onRefresh(); };
        this.$scope.$watch(function () { return _this.baselineConfig; }, onRefresh, true);
    }
    return BaselineInspectorController;
}());
exports.BaselineInspectorController = BaselineInspectorController;


/***/ }),
/* 154 */
/***/ (function(module, exports) {

module.exports = "<div class=baseline-inspector> <sw-object-inspector-header icon=\"vm.baselineConfig.baselineViolationStatus | baselineViolationStatusIconFilter\" header-text=vm.baselineConfig.baselineName description=vm.baselineConfig.baselineDescription close-sidebar=vm.closeBaselineInspector> </sw-object-inspector-header> <div class=\"row xui-padding-lgh\"> <div class=col-md-6> <xui-sorter caption={{vm.baselineInspectorSortCaption}} selected-item=vm.sortBy items-source=vm.sortableColumns display-value=name sort-direction=vm.sortDirection on-change=\"vm.sortChanged(newValue, oldValue)\"> </xui-sorter> </div> <div class=col-md-6> <xui-dropdown caption={{vm.baselineInspectorShowCaption}} items-source=vm.configTypes on-changed=\"vm.configTypeChanged(newValue, oldValue)\" display-value=name selected-item=vm.selectedConfigType class=pull-right> </xui-dropdown> </div> </div> <div class=\"row xui-padding-lgh\"> <div class=col-md-12> <xui-search placeholder={{vm.baselineInspectorSearchPlaceholder}} on-search=\"vm.doSearch(value, cancellation)\" on-clear=vm.clearSearch() items-source=[]> </xui-search> </div> </div> <xui-grid items-source=vm.filteredBaselineViolations hide-toolbar=true row-padding=narrow smart-mode=true pagination-data=vm.gridPagination options=vm.gridOptions template-url=baseline-violation-template controller=vm> </xui-grid> <script type=text/ng-template id=baseline-violation-template> <div class=\"row\">\n            <div class=\"col-md-4\">\n                <span class=\"node-name\" ng-bind-html=\"item.nodeName | xuiHighlight:vm.searchValue\"></span>\n            </div>\n            <div class=\"col-md-3\">\n                <span class=\"config-type\">{{::item.configType}}</span>\n            </div>\n            <div class=\"col-md-5\">\n                <xui-icon \n                    icon=\"{{item.state | baselineViolationStatusIconFilter}}\" \n                    text-alignment=\"right\"\n                    ng-if=\"vm.showBaselineViolationStatusIcon(item.state)\">\n                </xui-icon>\n                <span class=\"violation-state\">\n                    <xui-popover \n                        xui-popover-title=\"{{vm.baselineInspectorErrorTitle}}\"\n                        xui-popover-content=\"{url: 'baseline-violation-error-popover-template'}\"\n                        xui-popover-trigger=\"mouseenter click\"\n                        xui-popover-placement=\"left\"\n                        ng-if=\"vm.showBaselineViolationErrorPopover(item.state)\">\n                        <xui-button \n                            display-style=\"link\">{{item.state | baselineViolationStatusTextFilter}}\n                        </xui-button>\n                    </xui-popover>\n                    \n                    <span ng-if=\"!vm.showBaselineDiffLink(item) && !vm.showBaselineViolationErrorPopover(item.state)\">\n                        {{item.state | baselineViolationStatusTextFilter}}\n                    </span>\n                    <a ng-if=\"vm.showBaselineDiffLink(item)\" target=\"__blank\" href=\"{{vm.getBaselineDiffUrl(item)}}\">\n                        {{item.state | baselineViolationStatusTextFilter}}\n                    </a>\n                </span>\n            </div>\n        </div> </script> <script type=text/ng-template id=baseline-violation-error-popover-template> <xui-message type=\"error\">\n            <b><span _t>Please check NcmBusinessLayerPlugin.log for more details.</span> &raquo; <a target=\"__blank\" href=\"https://solarwinds.com/documentation/kbloader.aspx?kb=MT126991\"><span _t>Learn more</span></a></b>\n        </xui-message> </script> </div>";

/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ncmHelpButton_directive_1 = __webpack_require__(156);
exports.default = function (module) {
    module.component("ncmHelpButton", ncmHelpButton_directive_1.NcmHelpButton);
};


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ncmHelpButton_directive_controller_1 = __webpack_require__(157);
var angularDirectiveUtils_1 = __webpack_require__(9);
var angularDirectiveUtils_2 = __webpack_require__(9);
var NcmHelpButton = /** @class */ (function () {
    function NcmHelpButton() {
        this.restrict = angularDirectiveUtils_1.Restrict.element;
        this.transclude = false;
        this.controller = ncmHelpButton_directive_controller_1.NcmHelpButtonController;
        this.controllerAs = "vm";
        this.template = __webpack_require__(158);
        this.replace = true;
        this.scope = {
            helpUrlFragment: angularDirectiveUtils_2.BindingScope.oneWayOptional
        };
    }
    return NcmHelpButton;
}());
exports.NcmHelpButton = NcmHelpButton;


/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var NcmHelpButtonController = /** @class */ (function () {
    /** @ngInject */
    NcmHelpButtonController.$inject = ["$scope", "$log", "ncmNavigationService"];
    function NcmHelpButtonController($scope, $log, ncmNavigationService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.ncmNavigationService = ncmNavigationService;
        this.getHelpUrl = function (helpUrlFragment) {
            _this.ncmNavigationService.getHelpUrl(helpUrlFragment).then(function (result) {
                _this.helpUrl = result;
            });
        };
        this.getHelpUrl($scope.helpUrlFragment);
    }
    return NcmHelpButtonController;
}());
exports.NcmHelpButtonController = NcmHelpButtonController;


/***/ }),
/* 158 */
/***/ (function(module, exports) {

module.exports = "<a href={{vm.helpUrl}} target=__blank> <xui-icon icon=help icon-size=small text-alignment=right> </xui-icon><span _t>Help</span> </a>";

/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var securityPoliciesEmptyData_directive_1 = __webpack_require__(160);
exports.default = function (module) {
    module.component("ncmSecurityPoliciesEmptyData", securityPoliciesEmptyData_directive_1.SecurityPoliciesEmptyDataDirective);
};


/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var securityPoliciesEmptyData_controller_1 = __webpack_require__(161);
var SecurityPoliciesEmptyDataDirective = /** @class */ (function () {
    function SecurityPoliciesEmptyDataDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(162);
        this.transclude = false;
        this.replace = true;
        this.scope = {};
        this.controller = securityPoliciesEmptyData_controller_1.SecurityPoliciesEmptyDataController;
        this.controllerAs = "vm";
    }
    return SecurityPoliciesEmptyDataDirective;
}());
exports.SecurityPoliciesEmptyDataDirective = SecurityPoliciesEmptyDataDirective;


/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SecurityPoliciesEmptyDataController = /** @class */ (function () {
    /** @ngInject */
    SecurityPoliciesEmptyDataController.$inject = ["$log", "ncmNavigationService", "$location"];
    function SecurityPoliciesEmptyDataController($log, ncmNavigationService, $location) {
        var _this = this;
        this.$log = $log;
        this.ncmNavigationService = ncmNavigationService;
        this.$location = $location;
        this.url = "";
        this.init = function () {
            _this.nodeId = _this.getNodeId();
            if (_this.nodeId) {
                _this.ncmNavigationService.getConfigsSubViewUrl(_this.nodeId).then(function (x) {
                    return _this.url = x;
                });
            }
        };
    }
    SecurityPoliciesEmptyDataController.prototype.getNodeId = function () {
        var netObj = this.$location.search().NetObject;
        if (_.startsWith(netObj, "N:")) {
            return Number(netObj.substring(2));
        }
        return null;
    };
    ;
    return SecurityPoliciesEmptyDataController;
}());
exports.SecurityPoliciesEmptyDataController = SecurityPoliciesEmptyDataController;


/***/ }),
/* 162 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-security-policies-empty-data ng-init=vm.init()> <xui-image image=no-search-results /> <p style=width:70%;text-align:center><span _t>Cannot show policy data until configs have been downloaded.</span> <a href={{vm.url}}><span _t>Go here</span></a>&nbsp;<span _t>to download configs.</span> </p> </div>";

/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessListEmptyData_directive_1 = __webpack_require__(164);
exports.default = function (module) {
    module.component("ncmAccessListEmptyData", accessListEmptyData_directive_1.AccessListEmptyDataDirective);
};


/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var accessListEmptyData_controller_1 = __webpack_require__(165);
var AccessListEmptyDataDirective = /** @class */ (function () {
    function AccessListEmptyDataDirective() {
        this.restrict = "E";
        this.template = __webpack_require__(166);
        this.transclude = false;
        this.replace = true;
        this.scope = {};
        this.controller = accessListEmptyData_controller_1.AccessListEmptyDataController;
        this.controllerAs = "vm";
    }
    return AccessListEmptyDataDirective;
}());
exports.AccessListEmptyDataDirective = AccessListEmptyDataDirective;


/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var AccessListEmptyDataController = /** @class */ (function () {
    /** @ngInject */
    AccessListEmptyDataController.$inject = ["ncmNavigationService", "$location"];
    function AccessListEmptyDataController(ncmNavigationService, $location) {
        var _this = this;
        this.ncmNavigationService = ncmNavigationService;
        this.$location = $location;
        this.init = function () {
            _this.nodeId = _this.getNodeId();
            if (_this.nodeId) {
                _this.ncmNavigationService.getConfigsSubViewUrl(_this.nodeId).then(function (x) {
                    return _this.url = x;
                });
            }
        };
        this.url = "";
    }
    AccessListEmptyDataController.prototype.getNodeId = function () {
        var netObj = this.$location.search().NetObject;
        if (_.startsWith(netObj, "N:")) {
            return Number(netObj.substring(2));
        }
        return null;
    };
    ;
    return AccessListEmptyDataController;
}());
exports.AccessListEmptyDataController = AccessListEmptyDataController;


/***/ }),
/* 166 */
/***/ (function(module, exports) {

module.exports = "<div class=ncm-access-list-empty-data ng-init=vm.init()> <xui-image image=no-search-results style=display:block;margin:auto /> <p><span _t>Cannot show access list data until configs have been downloaded.</span> <a href={{vm.url}}><span _t>Go here</span></a><span>&nbsp;</span><span _t>to download configs.</span> </p> </div>";

/***/ }),
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessList_service_1 = __webpack_require__(168);
var aclRules_service_1 = __webpack_require__(169);
var aclRulesDiff_service_1 = __webpack_require__(170);
var ncmNavigation_service_1 = __webpack_require__(8);
var objectGroup_service_1 = __webpack_require__(171);
var objectDefinition_service_1 = __webpack_require__(172);
var aclRuleHighlighting_service_1 = __webpack_require__(173);
var ncmAccessListGrid_service_1 = __webpack_require__(24);
var windowsPerformanceMonitor_service_1 = __webpack_require__(174);
var vpcSubpage_service_1 = __webpack_require__(175);
var breadCrumb_service_1 = __webpack_require__(176);
var ncmViews_service_1 = __webpack_require__(12);
var baselineManagement_service_1 = __webpack_require__(177);
var ncmNodeDetails_service_1 = __webpack_require__(178);
var ncmConfig_service_1 = __webpack_require__(179);
var ncmConfigComparison_service_1 = __webpack_require__(180);
var ncmConfigTypes_service_1 = __webpack_require__(181);
var ncmErrorHandler_service_1 = __webpack_require__(183);
var ncmMessageBox_service_1 = __webpack_require__(184);
var ncmSelectNodesDialogService_1 = __webpack_require__(185);
var ncmSecurityPolicies_service_1 = __webpack_require__(0);
var _t_service_1 = __webpack_require__(3);
var ncmAccounts_service_1 = __webpack_require__(187);
exports.default = function (module) {
    // register services
    module.service("accessListService", accessList_service_1.AccessListService);
    module.service("aclRulesService", aclRules_service_1.AclRulesService);
    module.service("aclRulesDiffService", aclRulesDiff_service_1.AclRulesDiffService);
    module.service("ncmNavigationService", ncmNavigation_service_1.NcmNavigationService);
    module.service("objectGroupService", objectGroup_service_1.ObjectGroupService);
    module.service("objectDefinitionService", objectDefinition_service_1.ObjectDefinitionService);
    module.service("aclRuleHighlightingService", aclRuleHighlighting_service_1.AclRuleHighlightingService);
    module.service("ncmAccessListGridService", ncmAccessListGrid_service_1.NcmAccessListGridService);
    module.service("windowsPerformanceMonitorService", windowsPerformanceMonitor_service_1.WindowsPerformanceMonitorService);
    module.service("vpcSubpageService", vpcSubpage_service_1.VpcSubpageService);
    module.service("breadCrumbService", breadCrumb_service_1.BreadCrumbService);
    module.service("ncmViewsService", ncmViews_service_1.NcmViewsService);
    module.service("baselineManagementService", baselineManagement_service_1.BaselineManagementService);
    module.service("ncmNodeDetailsService", ncmNodeDetails_service_1.NcmNodeDetailsService);
    module.service("ncmConfigService", ncmConfig_service_1.NcmConfigService);
    module.service("ncmConfigComparisonService", ncmConfigComparison_service_1.NcmConfigComparisonService);
    module.service("ncmConfigTypesService", ncmConfigTypes_service_1.NcmConfigTypesService);
    module.service("ncmErrorHandlerService", ncmErrorHandler_service_1.NcmErrorHandlerService);
    module.service("ncmMessageBoxService", ncmMessageBox_service_1.NcmMessageBoxService);
    module.service("ncmSelectNodesDialogService", ncmSelectNodesDialogService_1.NcmSelectNodesDialogService);
    module.service("ncmSecurityPoliciesService", ncmSecurityPolicies_service_1.NcmSecurityPoliciesService);
    module.service("_t", _t_service_1.default);
    module.service("ncmAccountsService", ncmAccounts_service_1.NCMAccountsService);
};


/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiUrl = "ncm/acl/history";
var AccessListService = /** @class */ (function () {
    /** @ngInject */
    AccessListService.$inject = ["$log", "swApi"];
    function AccessListService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.getAccessControlLists = function (nodeId) {
            _this.$log.info("AccessListService.getAccessControlLists called");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + nodeId)
                .get();
        };
    }
    return AccessListService;
}());
exports.AccessListService = AccessListService;


/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiUrl = "ncm/aclRules";
var AclRulesService = /** @class */ (function () {
    /** @ngInject */
    AclRulesService.$inject = ["$log", "swApi"];
    function AclRulesService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.getAclRules = function (nodeId, configId, aclName) {
            _this.$log.info("AclRulesService.getAclRules called");
            var apiCall = nodeId + "?aclName=" + encodeURIComponent(aclName) +
                (configId != null && configId.length > 0 ? "&configId=" + configId : "");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + apiCall)
                .get();
        };
    }
    return AclRulesService;
}());
exports.AclRulesService = AclRulesService;


/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiUrl = "ncm/aclDiffOptions";
var AclRulesDiffService = /** @class */ (function () {
    /** @ngInject */
    AclRulesDiffService.$inject = ["$log", "swApi"];
    function AclRulesDiffService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.getAceDiff = function (leftNodeId, leftConfigId, leftAclName, rightNodeId, rightConfigId, rightAclName) {
            _this.$log.info("AclRulesDiffService.getAceDiff() called");
            var result = _this.swApi
                .api(false)
                .one(exports.swApiUrl +
                "?leftNodeId=" +
                leftNodeId +
                "&leftConfigId=" +
                leftConfigId +
                "&leftAclName=" +
                encodeURIComponent(leftAclName) +
                "&rightNodeId=" +
                rightNodeId +
                "&rightConfigId=" +
                rightConfigId +
                "&rightAclName=" +
                encodeURIComponent(rightAclName))
                .get();
            return result;
        };
        this.getPreviousAclVersionConfigId = function (nodeId, configId, aclName) {
            _this.$log.info("AclRulesDiffService.getPreviousAclVersionConfigId() called");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl +
                "/" +
                "aclHistory" +
                "/" +
                nodeId +
                "/" +
                configId +
                "?aclName=" +
                encodeURIComponent(aclName))
                .get();
        };
        this.getAclDiffInfo = function (leftNodeId, leftConfigId, leftAclName, rightNodeId, rightConfigId, rightAclName) {
            _this.$log.info("AclRulesDiffService.getAclDiffInfo() called");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl +
                "/" +
                "aclDiffInfo" +
                "?leftNodeId=" +
                leftNodeId +
                "&leftConfigId=" +
                leftConfigId +
                "&leftAclName=" +
                encodeURIComponent(leftAclName) +
                "&rightNodeId=" +
                rightNodeId +
                "&rightConfigId=" +
                rightConfigId +
                "&rightAclName=" +
                encodeURIComponent(rightAclName))
                .get();
        };
        this.getAclNodes = function () {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/AclNodes")
                .get();
        };
        this.getAccessControlListsFilter = function (nodeId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl +
                "/AclFilter" +
                "/" +
                nodeId)
                .get();
        };
    }
    return AclRulesDiffService;
}());
exports.AclRulesDiffService = AclRulesDiffService;


/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var diffData_1 = __webpack_require__(30);
exports.swApiUrl = "ncm/objectGroup";
var ObjectGroupService = /** @class */ (function () {
    /** @ngInject */
    ObjectGroupService.$inject = ["$log", "swApi"];
    function ObjectGroupService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.getObjectGroup = function (nodeId, name, configId) {
            _this.$log.info("ObjectGroupService.getObjectGroup called");
            var encodedName = encodeURIComponent(name);
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + nodeId + "?name=" + encodedName + "&configId=" + configId)
                .get();
        };
        this.getObjectGroupHistory = function (nodeId, groupName, baseConfigId) {
            _this.$log.info("ObjectGroupService.getObjectGroupHistory called");
            var encodedName = encodeURIComponent(groupName);
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl +
                "/" +
                "history" +
                "/" +
                nodeId +
                "?groupName=" +
                encodedName +
                "&baseConfigId=" +
                baseConfigId)
                .get();
        };
        this.getObjectGroupDiff = function (leftNodeId, leftConfigId, leftGroupName, rightNodeId, rightConfigId, rightGroupName) {
            _this.$log.info("ObjectGroupService.getObjectGroupDiff called");
            var data = new diffData_1.DiffData(leftNodeId, leftConfigId, leftGroupName, rightNodeId, rightConfigId, rightGroupName);
            return _this.swApi
                .api(false)
                .all(exports.swApiUrl +
                "/diff/")
                .post(data);
        };
    }
    return ObjectGroupService;
}());
exports.ObjectGroupService = ObjectGroupService;


/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var diffData_1 = __webpack_require__(30);
exports.swApiUrl = "ncm/aclObjects";
var ObjectDefinitionService = /** @class */ (function () {
    /** @ngInject */
    ObjectDefinitionService.$inject = ["$log", "swApi"];
    function ObjectDefinitionService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.getObjectDefinition = function (nodeId, name, configId) {
            _this.$log.info("ObjectDefinitionService.getObjectDefinition called");
            var encodedName = encodeURIComponent(name);
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + nodeId + "?name=" + encodedName + "&configId=" + configId)
                .get();
        };
        this.getObjectDefinitionHistory = function (nodeId, objectName, baseConfigId) {
            _this.$log.info("ObjectDefinitionService.getObjectDefinitionHistory called" + nodeId);
            var encodedName = encodeURIComponent(objectName);
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl +
                "/" +
                "history" +
                "/" +
                nodeId +
                "?objectName=" +
                encodedName +
                "&baseConfigId=" +
                baseConfigId)
                .get();
        };
        this.getObjectDefinitionDiff = function (leftNodeId, leftConfigId, leftObjectName, rightNodeId, rightConfigId, rightObjectName) {
            _this.$log.info("ObjectDefinitionService.getObjectDefinitionDiff called");
            var data = new diffData_1.DiffData(leftNodeId, leftConfigId, leftObjectName, rightNodeId, rightConfigId, rightObjectName);
            return _this.swApi
                .api(false)
                .all(exports.swApiUrl +
                "/diff/")
                .post(data);
        };
    }
    return ObjectDefinitionService;
}());
exports.ObjectDefinitionService = ObjectDefinitionService;


/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accessControlListRule_1 = __webpack_require__(5);
var AclRuleHighlightingService = /** @class */ (function () {
    function AclRuleHighlightingService() {
        var _this = this;
        /* tslint:disable:no-bitwise */
        this.resolveCssName = function (token) {
            if (token.type & accessControlListRule_1.AceTokenType.Action) {
                var testId = " id-ace-rule-action";
                return _this.resolveActionCss(token) + testId;
            }
            if (token.type & accessControlListRule_1.AceTokenType.SourceAddress) {
                return _this.resolveReferenceTypeCss(token, "ncm-rulehighlight-keyword-1");
            }
            if (token.type & accessControlListRule_1.AceTokenType.DestinationAddress) {
                return _this.resolveReferenceTypeCss(token, "ncm-rulehighlight-keyword-2");
            }
            if (token.type & accessControlListRule_1.AceTokenType.SourcePort) {
                return _this.resolveReferenceTypeCss(token, "ncm-rulehighlight-srcport");
            }
            if (token.type & accessControlListRule_1.AceTokenType.DestinationPort) {
                return _this.resolveReferenceTypeCss(token, "ncm-rulehighlight-destport");
            }
            if (token.type & accessControlListRule_1.AceTokenType.Protocol) {
                return "ncm-rulehighlight-protocol";
            }
            if ((token.type & accessControlListRule_1.AceTokenType.Object) || (token.type & accessControlListRule_1.AceTokenType.ObjectGroup)) {
                return _this.applyUnderlineStyle("ncm-rulehighlight-object");
            }
            if (token.type & accessControlListRule_1.AceTokenType.TimeRange) {
                return "ncm-rule-timerange";
            }
            if (token.type & accessControlListRule_1.AceTokenType.LogLevel) {
                return "ncm-rule-log-level";
            }
            if (token.type & accessControlListRule_1.AceTokenType.Interval) {
                return "ncm-rule-log-interlal";
            }
            if (token.type & accessControlListRule_1.AceTokenType.Keyword) {
                return "ncm-rule-keyword";
            }
            return "ncm-rulehighlight-default";
        };
        this.resolveActionCss = function (token) {
            if (token.value === "permit") {
                return "ncm-rulehighlight-permit";
            }
            if (token.value === "deny") {
                return "ncm-rulehighlight-deny";
            }
            return "ncm-rulehighlight-default";
        };
        this.resolveReferenceTypeCss = function (token, baseClass) {
            if (((token.type & accessControlListRule_1.AceTokenType.Object) === accessControlListRule_1.AceTokenType.Object) ||
                ((token.type & accessControlListRule_1.AceTokenType.ObjectGroup) === accessControlListRule_1.AceTokenType.ObjectGroup)) {
                return _this.applyUnderlineStyle(baseClass);
            }
            return baseClass;
        };
        this.applyUnderlineStyle = function (baseClass) {
            return baseClass + " ncm-rulehighlight-underline ";
        };
        /* tslint:enable:no-bitwise */
    }
    return AclRuleHighlightingService;
}());
exports.AclRuleHighlightingService = AclRuleHighlightingService;
;


/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var WindowsPerformanceMonitorService = /** @class */ (function () {
    /** @ngInject */
    WindowsPerformanceMonitorService.$inject = ["$log"];
    function WindowsPerformanceMonitorService($log) {
        var _this = this;
        this.$log = $log;
        this.monitorPageLoadingTime = function () {
            window.onload = _this.measurePageLoadingTime;
        };
        this.measurePageLoadingTime = function () {
            _this.$log.debug("Loaded page " + window.location.href +
                " in " + (Date.now() - window.performance.timing.navigationStart) + "ms");
        };
    }
    return WindowsPerformanceMonitorService;
}());
exports.WindowsPerformanceMonitorService = WindowsPerformanceMonitorService;


/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var VpcSubpageService = /** @class */ (function () {
    /** @ngInject */
    VpcSubpageService.$inject = ["$log", "swApi"];
    function VpcSubpageService($log, swApi) {
        this.$log = $log;
        this.swApi = swApi;
    }
    VpcSubpageService.prototype.getVpcNodeName = function (nodeId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.swApi.api(false)
                        .one("ncm/node/nodename/" + nodeId)
                        .get()];
            });
        });
    };
    VpcSubpageService.prototype.getVpcInterfacesData = function (localNodeId, vpcId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.swApi
                        .api(false)
                        .one("ncm/vpc/" + localNodeId + "/" + vpcId)
                        .get()];
            });
        });
    };
    return VpcSubpageService;
}());
exports.VpcSubpageService = VpcSubpageService;


/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var BreadCrumbService = /** @class */ (function () {
    /** @ngInject */
    BreadCrumbService.$inject = ["$log", "swisService", "$q", "ncmNavigationService", "_t"];
    function BreadCrumbService($log, swisService, $q, ncmNavigationService, _t) {
        var _this = this;
        this.$log = $log;
        this.swisService = swisService;
        this.$q = $q;
        this.ncmNavigationService = ncmNavigationService;
        this._t = _t;
        this.buildBreadCrumbsForAclRulesView = function (nodeId, currentPageTitle) {
            _this.$log.info("BreadCrumbService.buildBreadCrumbsForAclRulesView called");
            return _this.$q.all([_this.getNodeCaption(nodeId), _this.getNodeDetailsUrl(nodeId), _this.getAccessListsUrl(nodeId)])
                .then(function (result) {
                return [
                    {
                        title: _this._t("Home"),
                        url: "/Orion/SummaryView.aspx"
                    },
                    {
                        title: result[0],
                        url: result[1]
                    },
                    {
                        title: _this._t("Access Lists"),
                        url: result[2]
                    },
                    {
                        title: currentPageTitle,
                        url: "#"
                    }
                ];
            });
        };
        this.buildBreadCrumbsForConfigDiffView = function (nodeId, currentPageTitle) {
            _this.$log.info("BreadCrumbService.buildBreadCrumbsForConfigDiffView called");
            return _this.$q.all([_this.getNodeCaption(nodeId), _this.getNodeDetailsUrl(nodeId), _this.getConfigsSubViewUrl(nodeId)])
                .then(function (result) {
                return [
                    {
                        title: _this._t("Home"),
                        url: "/Orion/SummaryView.aspx"
                    },
                    {
                        title: result[0],
                        url: result[1]
                    },
                    {
                        title: _this._t("Configs"),
                        url: result[2]
                    },
                    {
                        title: currentPageTitle,
                        url: "#"
                    }
                ];
            });
        };
        this.getNodeCaption = function (nodeId) {
            return _this.swisService.query("SELECT Caption FROM Orion.Nodes WHERE NodeID=@nodeId", { nodeId: nodeId })
                .then(function (results) {
                if (results.rows.length > 0) {
                    return results.rows[0]["Caption"];
                }
                else {
                    return "";
                }
            });
        };
    }
    BreadCrumbService.prototype.getNodeDetailsUrl = function (nodeId) {
        return this.ncmNavigationService.getNodeDetailsUrl(nodeId);
    };
    ;
    BreadCrumbService.prototype.getConfigsSubViewUrl = function (nodeId) {
        return this.ncmNavigationService.getConfigsSubViewUrl(nodeId);
    };
    ;
    BreadCrumbService.prototype.getAccessListsUrl = function (nodeId) {
        return this.ncmNavigationService.getAccessListsUrl(nodeId);
    };
    ;
    return BreadCrumbService;
}());
exports.BreadCrumbService = BreadCrumbService;


/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var require_1 = __webpack_require__(14);
exports.swApiUrl = "ncm/baselines";
var BaselineManagementService = /** @class */ (function () {
    /** @ngInject */
    BaselineManagementService.$inject = ["$log", "swApi"];
    function BaselineManagementService($log, swApi) {
        var _this = this;
        this.$log = $log;
        this.swApi = swApi;
        this.getBaselines = function (nodesRequest) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + "pageable")
                .customPOST(nodesRequest);
        };
        this.getBaseline = function (baselineId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + baselineId)
                .get();
        };
        this.addBaseline = function (baselineConfig) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl)
                .customPOST(baselineConfig);
        };
        this.updateBaseline = function (baselineConfig) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl)
                .customPUT(baselineConfig);
        };
        this.deleteBaselines = function (baselinesIds) {
            _this.$log.info("BaselineManagementService.deleteBaselines called");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/bulkDelete")
                .customPOST(baselinesIds);
        };
        this.enableBaselines = function (baselinesIds) {
            _this.$log.info("BaselineManagementService.enableBaselines called");
            require_1.Require.notEmptyArray(baselinesIds, "baselinesIds");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/bulkEnable")
                .customPUT(baselinesIds);
        };
        this.disableBaselines = function (baselinesIds) {
            _this.$log.info("BaselineManagementService.disableBaselines called");
            require_1.Require.notEmptyArray(baselinesIds, "baselinesIds");
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/bulkDisable")
                .customPUT(baselinesIds);
        };
        this.getBaselineConfigTypes = function (baselineId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + baselineId + "/configTypes")
                .get();
        };
        this.getBaselineNodes = function (baselineId) {
            var nodes = _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + baselineId + "/nodes")
                .get();
            return nodes.then(function (result) {
                var casted = result;
                casted.forEach(function (element) {
                    element.isSelected = true;
                });
                return result;
            });
        };
        this.assignBaselineNodes = function (baselineId, nodes, selectedConfigTypes) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + baselineId + "/nodes")
                .customPUT({
                nodeIds: nodes.map(function (n) { return n.nodeId; }),
                selectedConfigTypes: selectedConfigTypes
            });
        };
        this.getBaselineViolations = function (baselineId) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/" + baselineId + "/violations")
                .get();
        };
        this.requestUpdateBaselineCache = function (baselineIds) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/updateCache")
                .customPUT(baselineIds);
        };
        this.fetchBaselinesState = function (baselineIds) {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/state")
                .customPOST(baselineIds);
        };
    }
    BaselineManagementService.prototype.getBaselineFromConfig = function (configId) {
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/promotions/" + configId)
            .get();
    };
    BaselineManagementService.prototype.getBaselineDiff = function (baselineId, configId) {
        this.$log.info("service: returning baselineDiff...");
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + baselineId + "/config/" + configId)
            .get();
    };
    BaselineManagementService.prototype.getBaselineName = function (baselineId) {
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + baselineId + "/name")
            .get();
    };
    BaselineManagementService.prototype.getBaselineNames = function () {
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/names")
            .get();
    };
    return BaselineManagementService;
}());
exports.BaselineManagementService = BaselineManagementService;


/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmNodeDetailsService = /** @class */ (function () {
    /** @ngInject */
    NcmNodeDetailsService.$inject = ["swisService", "swApi"];
    function NcmNodeDetailsService(swisService, swApi) {
        var _this = this;
        this.swisService = swisService;
        this.swApi = swApi;
        this.getNodeNames = function () {
            return _this.swApi
                .api(false)
                .one("ncm/node/names")
                .get();
        };
        this.getFilters = function (filterValues) {
            return _this.swApi
                .api(false)
                .one("ncm/node/filters")
                .customPOST(filterValues);
        };
        this.getNodes = function (nodesRequest) {
            return _this.swApi
                .api(false)
                .one("ncm/node")
                .customPOST(nodesRequest);
        };
        this.getVendors = function () {
            return _this.swApi
                .api(false)
                .one("ncm/node/vendors")
                .get();
        };
    }
    return NcmNodeDetailsService;
}());
exports.NcmNodeDetailsService = NcmNodeDetailsService;


/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmConfigService = /** @class */ (function () {
    /** @ngInject */
    NcmConfigService.$inject = ["swApi"];
    function NcmConfigService(swApi) {
        this.swApi = swApi;
    }
    NcmConfigService.prototype.getConfigNames = function (ncmNodeId) {
        return this.swApi
            .api(false)
            .one("ncm/config/node/" + ncmNodeId + "/names")
            .get();
    };
    NcmConfigService.prototype.getConfigTypes = function () {
        return this.swApi
            .api(false)
            .one("ncm/config/configTypes")
            .get();
    };
    NcmConfigService.prototype.getConfigDetails = function (configId) {
        return this.swApi
            .api(false)
            .one("ncm/config/" + configId + "/details")
            .get();
    };
    return NcmConfigService;
}());
exports.NcmConfigService = NcmConfigService;


/***/ }),
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmConfigComparisonService = /** @class */ (function () {
    /** @ngInject */
    NcmConfigComparisonService.$inject = ["swApi"];
    function NcmConfigComparisonService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.getDiffInternal = function (data, url) {
            return _this.swApi
                .api(false)
                .one(url)
                .get({
                leftConfigId: data.leftConfigId,
                rightConfigId: data.rightConfigId,
                onlyChangedLines: data.onlyChangedLines,
                startIndex: data.startIndex,
                size: data.size,
                noCache: data.noCache,
                numberOfChangedLinesToLoad: data.numberOfChangedLinesToLoad,
                command: data.command
            });
        };
        this.getSelectedNodes = function () {
            return _this.swApi
                .ws
                .one("../NCM/Services/ConfigManagement.asmx").post("GetSelectedNCMNodeIds")
                .then(function (result) {
                return result.d;
            });
        };
        this.getNodesConfigHistory = function (nodeIds) {
            return _this.swApi
                .api(false)
                .one("ncm/configComparison/GetNodesConfigHistory")
                .customPOST(nodeIds);
        };
    }
    NcmConfigComparisonService.prototype.getIgnoredLines = function (textLines) {
        return this.swApi
            .api(false)
            .one("ncm/configComparison/GetIgnoredLines")
            .customPOST(textLines);
    };
    NcmConfigComparisonService.prototype.getDiff = function (data) {
        return this.getDiffInternal(data, "ncm/configComparison/GetDiff");
    };
    NcmConfigComparisonService.prototype.getAdditionalCommandsDiff = function (data) {
        return this.getDiffInternal(data, "ncm/configComparison/GetAdditionalCommandsDiff");
    };
    return NcmConfigComparisonService;
}());
exports.NcmConfigComparisonService = NcmConfigComparisonService;


/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var newConfigType_1 = __webpack_require__(182);
var require_1 = __webpack_require__(14);
exports.swApiUrl = "ncm/configTypes";
var NcmConfigTypesService = /** @class */ (function () {
    /** @ngInject */
    NcmConfigTypesService.$inject = ["$log", "swApi"];
    function NcmConfigTypesService($log, swApi) {
        this.$log = $log;
        this.swApi = swApi;
    }
    NcmConfigTypesService.prototype.getAll = function () {
        this.$log.info("ConfigTypesService: Getting all config types.");
        return this.swApi
            .api(false)
            .one(exports.swApiUrl)
            .get();
    };
    ;
    NcmConfigTypesService.prototype.delete = function (id) {
        this.$log.info("ConfigTypesService: Deleting a config type with ID " + id + ".");
        require_1.Require.notNull(id, "id");
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + id)
            .customDELETE("");
    };
    ;
    NcmConfigTypesService.prototype.create = function (name) {
        this.$log.info("ConfigTypesService: Creating a new config type " + name + ".");
        require_1.Require.notEmptyString(name, "name");
        var data = new newConfigType_1.NewConfigType();
        data.name = name;
        return this.swApi
            .api(false)
            .one(exports.swApiUrl)
            .customPOST(data);
    };
    ;
    NcmConfigTypesService.prototype.update = function (id, newName) {
        this.$log.info("ConfigTypesService: Updating name of config type with ID " + id + " to '" + newName + "'.");
        require_1.Require.notNull(id, "id");
        require_1.Require.notEmptyString(name, "name");
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + id)
            .customPUT(newName);
    };
    ;
    NcmConfigTypesService.prototype.assignVendors = function (data) {
        require_1.Require.notNull(data, "data");
        require_1.Require.notNull(data.configTypeId, "configTypeId");
        require_1.Require.notNull(data.vendors, "vendors");
        require_1.Require.notNull(data.operation, "operation");
        this.$log.info("ConfigTypesService: Assiging vendors to config type with ID " + data.configTypeId + ".");
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + data.configTypeId + "/vendors")
            .customPOST(data);
    };
    ;
    NcmConfigTypesService.prototype.getAssignedVendors = function (configTypeId) {
        this.$log.info("ConfigTypesService: Getting assigned vendors to config type with ID " + configTypeId + ".");
        require_1.Require.notNull(configTypeId, "configTypeId");
        return this.swApi
            .api(false)
            .one(exports.swApiUrl + "/" + configTypeId + "/vendors")
            .get();
    };
    ;
    return NcmConfigTypesService;
}());
exports.NcmConfigTypesService = NcmConfigTypesService;


/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NewConfigType = /** @class */ (function () {
    function NewConfigType() {
    }
    return NewConfigType;
}());
exports.NewConfigType = NewConfigType;


/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmErrorHandlerService = /** @class */ (function () {
    /** @ngInject */
    NcmErrorHandlerService.$inject = ["toastService"];
    function NcmErrorHandlerService(toastService) {
        this.toastService = toastService;
    }
    NcmErrorHandlerService.prototype.handle = function (ex) {
        if (typeof ex.data === "string") {
            this.toastService.error(ex.data);
        }
        else if (ex.data != null && ex.data.message != null) {
            this.toastService.error(ex.data.message);
        }
    };
    return NcmErrorHandlerService;
}());
exports.NcmErrorHandlerService = NcmErrorHandlerService;


/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmMessageBoxService = /** @class */ (function () {
    /** @ngInject */
    NcmMessageBoxService.$inject = ["xuiDialogService", "_t"];
    function NcmMessageBoxService(xuiDialogService, _t) {
        this.xuiDialogService = xuiDialogService;
        this._t = _t;
    }
    NcmMessageBoxService.prototype.showYesNoQuestion = function (title, message, callback) {
        return this.xuiDialogService.showModal(null, {
            title: title,
            message: message,
            buttons: [
                {
                    name: "yes",
                    isPrimary: false,
                    text: this._t("Yes"),
                    action: function () {
                        callback();
                        return true;
                    }
                },
                {
                    name: "no",
                    isPrimary: true,
                    text: this._t("No"),
                    action: function () {
                        return true;
                    }
                }
            ]
        });
    };
    return NcmMessageBoxService;
}());
exports.NcmMessageBoxService = NcmMessageBoxService;


/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NcmSelectNodesDialogService = /** @class */ (function () {
    /** @ngInject */
    NcmSelectNodesDialogService.$inject = ["$log", "xuiDialogService", "$controller", "_t"];
    function NcmSelectNodesDialogService($log, xuiDialogService, $controller, _t) {
        this.$log = $log;
        this.xuiDialogService = xuiDialogService;
        this.$controller = $controller;
        this._t = _t;
    }
    NcmSelectNodesDialogService.prototype.showDialog = function (item, okCallback) {
        var dlgController = this.$controller("NcmSelectNodeDialogController", {
            currentConfigType: item
        });
        var dlgSettings = {
            template: __webpack_require__(186),
            size: "select-nodes"
        };
        var dlgOptions = {
            title: this._t("VENDORS"),
            viewModel: dlgController,
            hideTopCancel: true,
            actionButtonText: this._t("OK"),
            cancelButtonText: this._t("CANCEL")
        };
        return this.xuiDialogService.showModal(dlgSettings, dlgOptions).then(function (result) {
            if (result === "cancel" || !dlgController.currentConfigType.isCustom) {
                return false;
            }
            okCallback(dlgController.assignedVendors);
            return true;
        });
    };
    return NcmSelectNodesDialogService;
}());
exports.NcmSelectNodesDialogService = NcmSelectNodesDialogService;


/***/ }),
/* 186 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog id=select-nodes-dialog class=select-nodes-dialog ng-init=vm.dialogOptions.viewModel.init()> <div ng-if=vm.dialogOptions.viewModel.forAllVendors> <h2 _t>All Nodes</h2> </div> <form class=add-panel ng-if=!vm.dialogOptions.viewModel.forAllVendors> <div class=row> <div class=\"col-md-4 xui-dropdown--justified\"> <xui-dropdown id=dropdown-vendor-operation ng-model=vm.dialogOptions.viewModel.selectedVendorOperation items-source=vm.dialogOptions.viewModel.configTypeVendorOperations display-value=name placeholder={{vm.dialogOptions.viewModel.dropdownPlaceholder}} caption={{vm.dialogOptions.viewModel.vendorOperationCaption}} on-changed=vm.dialogOptions.viewModel.changeVendorOperation() is-disabled=!vm.dialogOptions.viewModel.currentConfigType.isCustom> </xui-dropdown> </div> </div> <xui-divider></xui-divider> <div xui-busy=vm.dialogOptions.viewModel.isBusy> <xui-listview name=vendors-list items-source=vm.dialogOptions.viewModel.assignedVendors.vendors header-template-url=header-template template-url=vendors-template controller=vm></xui-listview> </div> <xui-divider></xui-divider> <div class=row ng-if=vm.dialogOptions.viewModel.currentConfigType.isCustom> <div class=\"col-md-4 xui-dropdown--justified\" ng-if=\"vm.dialogOptions.viewModel.allVendors != null\"> <xui-dropdown id=dropdown-vendor ng-model=vm.dialogOptions.viewModel.selectedVendor items-source=vm.dialogOptions.viewModel.allVendors placeholder={{vm.dialogOptions.viewModel.dropdownPlaceholder}} caption={{vm.dialogOptions.viewModel.vendorListCaption}}> </xui-dropdown> </div> <div class=\"col-md-4 col-add-button\"> <xui-button size=small name=add-new-button ng-click=vm.dialogOptions.viewModel.assignVendor() display-style=secondary is-busy=vm.dialogOptions.viewModel.isBusy _t>Add new </xui-button> </div> </div> <div ng-if=vm.dialogOptions.viewModel.showError class=error-box> {{vm.dialogOptions.viewModel.validationError}} </div> </form> <script type=text/ng-template id=header-template> <div class=\"row\" ng-if=\"vm.dialogOptions.viewModel.assignedVendors.vendors.length > 0\">\n            <h4 class=\"col-md-4 top-label\" _t> Vendor </h4>\n            <h4 class=\"col-md-8 top-label\" name=\"vendor-operation\" >\n                <span _t>Vendor operation:</span> {{ vm.dialogOptions.viewModel.selectedVendorOperation.name }}\n            </h4>\n        </div> </script> <script type=text/ng-template id=vendors-template> <div class=\"row\">\n            <div class=\"col-md-8\" name=\"vendor-name\"> {{ item }} </div>\n            <xui-button name=\"remove-vendor-button\" class=\"col-md-4\" size=\"small\" ng-click=\"vm.dialogOptions.viewModel.delete(item)\" display-style=\"tertiary\" icon=\"delete\"\n                ng-if=\"vm.dialogOptions.viewModel.currentConfigType.isCustom\" _t>Remove</xui-button>\n        </div> </script> </xui-dialog>";

/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.swApiUrl = "ncm/account";
var NCMAccountsService = /** @class */ (function () {
    /** @ngInject */
    NCMAccountsService.$inject = ["swApi"];
    function NCMAccountsService(swApi) {
        var _this = this;
        this.swApi = swApi;
        this.getAccountNCMRole = function () {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl + "/role")
                .get();
        };
    }
    return NCMAccountsService;
}());
exports.NCMAccountsService = NCMAccountsService;


/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baselineViolationStatusIcon_filter_1 = __webpack_require__(189);
var baselineViolationStatusText_filter_1 = __webpack_require__(190);
exports.default = function (module) {
    // register filters
    module.filter("baselineViolationStatusIconFilter", baselineViolationStatusIcon_filter_1.default);
    module.filter("baselineViolationStatusTextFilter", baselineViolationStatusText_filter_1.default);
};


/***/ }),
/* 189 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var baselineConfig_1 = __webpack_require__(11);
function baselineViolationStatusIconFilter() {
    return function (state) {
        if (state === baselineConfig_1.BaselineViolationState.Updating) {
            return "busy-cube";
        }
        else if (state === baselineConfig_1.BaselineViolationState.Mismatches) {
            return "severity_critical";
        }
        else if (state === baselineConfig_1.BaselineViolationState.NoIssues) {
            return "severity_ok";
        }
        else if (state === baselineConfig_1.BaselineViolationState.Error) {
            return "severity_error";
        }
        else {
            return "severity_unknown";
        }
    };
}
exports.default = baselineViolationStatusIconFilter;


/***/ }),
/* 190 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


baselineViolationStatusTextFilter.$inject = ["_t"];
Object.defineProperty(exports, "__esModule", { value: true });
var baselineConfig_1 = __webpack_require__(11);
/** @ngInject */
function baselineViolationStatusTextFilter(_t) {
    return function (state) {
        if (state === baselineConfig_1.BaselineViolationState.Updating) {
            return _t("Updating...");
        }
        else if (state === baselineConfig_1.BaselineViolationState.Mismatches) {
            return _t("Mismatched lines");
        }
        else if (state === baselineConfig_1.BaselineViolationState.NoIssues) {
            return _t("No issues");
        }
        else if (state === baselineConfig_1.BaselineViolationState.Error) {
            return _t("Error");
        }
        else {
            return _t("No config");
        }
    };
}
exports.default = baselineViolationStatusTextFilter;


/***/ })
/******/ ]);
//# sourceMappingURL=ncm.js.map