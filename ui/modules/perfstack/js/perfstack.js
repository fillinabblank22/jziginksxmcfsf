/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path='../ref.d.ts' />
	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var analysis_module_1 = __webpack_require__(1);
	var context = window;
	context.ga = (SW && SW.environment && SW.environment.oipEnabled) ? context.ga || function () { (ga.q = ga.q || []).push(arguments); } : function () { };
	ga.l = +new Date;
	angular.module('perfstack', ['orion', 'orion.services', analysis_module_1.default]);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	/// <reference path="../../../ref.d.ts" />
	var analysis_config_1 = __webpack_require__(2);
	var analysis_controller_1 = __webpack_require__(4);
	var ps_chart_directive_1 = __webpack_require__(14);
	var tile_1 = __webpack_require__(39);
	var ps_draggable_directive_1 = __webpack_require__(8);
	var ps_drop_target_directive_1 = __webpack_require__(57);
	var metric_service_1 = __webpack_require__(58);
	var utility_1 = __webpack_require__(59);
	var chart_manager_factory_1 = __webpack_require__(43);
	var format_1 = __webpack_require__(60);
	var icon_map_1 = __webpack_require__(61);
	var entity_service_1 = __webpack_require__(62);
	var entity_context_curator_module_1 = __webpack_require__(63);
	var project_service_1 = __webpack_require__(66);
	var project_curator_module_1 = __webpack_require__(67);
	var data_explorer_module_1 = __webpack_require__(70);
	var inspection_menu_module_1 = __webpack_require__(76);
	var settings_service_1 = __webpack_require__(51);
	var analytics_1 = __webpack_require__(79);
	exports.default = angular.module('perfstack.views.analysis', ['ui.router', ps_chart_directive_1.default, tile_1.default, metric_service_1.default,
	    entity_service_1.default, utility_1.default, chart_manager_factory_1.default, ps_draggable_directive_1.default, ps_drop_target_directive_1.default, format_1.default, icon_map_1.default,
	    entity_context_curator_module_1.default, project_service_1.default, project_curator_module_1.default, data_explorer_module_1.default, inspection_menu_module_1.default, settings_service_1.default, analytics_1.default])
	    .config(analysis_config_1.default)
	    .controller('AnalysisController', analysis_controller_1.default)
	    .name;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var settings_1 = __webpack_require__(3);
	config.$inject = ['$stateProvider', '$provide'];
	function config($stateProvider, $provide) {
	    $provide.decorator("$exceptionHandler", ['$delegate', function ($delegate) {
	            return function (exception, cause) {
	                $delegate(exception, cause);
	                logErrorEvent(exception.message);
	            };
	        }]);
	    $stateProvider
	        .state('Analysis', {
	        i18Title: '_t(Performance Analysis)',
	        url: '/perfstack/:id',
	        controller: 'AnalysisController',
	        controllerAs: 'controller',
	        templateUrl: 'app/views/analysis/analysis-view.html',
	        reloadOnSearch: false,
	        resolve: {
	            resolvedSettings: ['settingsService', function (settingsService) {
	                    return settingsService.getSettings();
	                }],
	            userSettings: ['$q', 'webAdminService', function ($q, webAdminService) {
	                    var deferred = $q.defer();
	                    var userSettings = {
	                        AllowRealTimePolling: null
	                    };
	                    webAdminService.getUserSetting(settings_1.UserSettings.AllowRealTimePolling).then(function (result) {
	                        userSettings.AllowRealTimePolling = result;
	                        deferred.resolve(userSettings);
	                    });
	                    return deferred.promise;
	                }]
	        }
	        // ,
	        // params: {
	        //   id : { squash: true, value: null}
	        // }
	    });
	    // log error not caught by angular
	    window.onerror = logErrorEvent;
	}
	exports.default = config;
	function logErrorEvent(msg, source, lineno, colno, error) {
	    ga('gmt1.send', {
	        hitType: 'event',
	        eventCategory: 'PerfStack',
	        eventAction: 'error',
	        eventLabel: msg
	    });
	}
	;


/***/ }),
/* 3 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.UserSettings = {
	    AllowRealTimePolling: "PerfStack.AllowRealTimePolling"
	};
	function getUserSetting(settingName) {
	    return _.find(SW.user.UserSettings, { Name: settingName });
	}
	exports.getUserSetting = getUserSetting;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path='../../../ref.d.ts' />
	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Metric = __webpack_require__(5);
	var Draggable = __webpack_require__(8);
	var entity_1 = __webpack_require__(9);
	var series_1 = __webpack_require__(6);
	var project_1 = __webpack_require__(10);
	var interaction_service_1 = __webpack_require__(11);
	var time_data_1 = __webpack_require__(13);
	//HACK: Needs to be moved to filter
	var DropMessage = {
	    entityDropDisallowed: 'Chart Already Here',
	    entityDropAllowed: 'Drop Here to Add Template',
	    metricDropDisallowed: 'Metric Already Here',
	    metricDropAllowed: 'Drop Here to Add Metric',
	    chartCreateFromMetricAllowed: 'Drop Here to Create a New Chart'
	};
	var Controller = (function () {
	    function Controller($log, $q, $timeout, $interval, $scope, $rootScope, _t, $state, $stateParams, $location, toast, xuiTimeframeService, metricService, entityService, utilityService, timeService, chartManager, dialogService, projectService, $window, interactionService, settings, swUtil, userSettings, analyticsService, helpLinkService) {
	        var _this = this;
	        this.$log = $log;
	        this.$q = $q;
	        this.$timeout = $timeout;
	        this.$interval = $interval;
	        this.$scope = $scope;
	        this.$rootScope = $rootScope;
	        this._t = _t;
	        this.$state = $state;
	        this.$stateParams = $stateParams;
	        this.$location = $location;
	        this.toast = toast;
	        this.xuiTimeframeService = xuiTimeframeService;
	        this.metricService = metricService;
	        this.entityService = entityService;
	        this.utilityService = utilityService;
	        this.timeService = timeService;
	        this.chartManager = chartManager;
	        this.dialogService = dialogService;
	        this.projectService = projectService;
	        this.$window = $window;
	        this.interactionService = interactionService;
	        this.settings = settings;
	        this.swUtil = swUtil;
	        this.userSettings = userSettings;
	        this.analyticsService = analyticsService;
	        this.helpLinkService = helpLinkService;
	        this.$onInit = function () {
	            _this.$rootScope.$on(Draggable.EventType.DragStart, _this.updateChartsDropTargetStatus);
	            _this.$scope.$on('$destroy', _this.destroy);
	            DropMessage.entityDropDisallowed = _this._t('Chart Already Here');
	            DropMessage.entityDropAllowed = _this._t('Drop Here to Add Template');
	            DropMessage.metricDropDisallowed = _this._t('Metric Already Here');
	            DropMessage.metricDropAllowed = _this._t('Drop Here to Add Metric');
	            DropMessage.chartCreateFromMetricAllowed = _this._t('Drop Here to Create a New Chart');
	            // Get context from query string (if it exists, otherwise empty array)
	            _this.entityContext = (_this.$location.search().context) ? _this.$location.search().context.split(',') : [];
	            _this.entityContextHasRelationships = (_this.$location.search().withRelationships === 'true');
	            _this.helpLink = _this.helpLinkService.getHelpLink("OrionCorePerformanceAnalysis");
	            // Prompt user if project is dirty
	            _this.$window.onbeforeunload = _this.confirmLocationChange;
	            _this.resetProject(false);
	            // Deserialize time from query string
	            _this.applyTimeQueryStringParams();
	            if (_this.$stateParams.id) {
	                _this.project.displayName = '';
	                _this.loadProject(_this.$stateParams.id, false).then(function () {
	                    _this.applyFastPollQueryStringParam();
	                    _this.initWatches();
	                });
	            }
	            else {
	                // Deserialize charts from query string
	                _this.applyChartQueryStringParams().then(function () {
	                    _this.applyFastPollQueryStringParam(true);
	                });
	                // Expand panel if starting in empty state
	                if (_this.charts.length === 0 && _this.isMetricPaletteCollapsed) {
	                    _this.toggleMetricPanel();
	                }
	                _this.initWatches();
	            }
	            if (_this.$location.search().fullScreen && (_this.$location.search().fullScreen.toLowerCase() === 'true')) {
	                _this.$timeout(function () {
	                    _this.toggleFullScreen();
	                }, 200);
	            }
	            _this.startTimeUpdates();
	            _this.startPolling();
	            _this.refreshEntities().then(function () {
	                if (_this.$location.search().seedFromContext && _this.$location.search().seedFromContext.toLowerCase() === 'true') {
	                    _this.generateChartsFromContext();
	                    _this.$location.search('seedFromContext', null);
	                    _this.$location.replace();
	                    // Expand panel if starting in empty state
	                    if (_this.charts.length === 0 && _this.isMetricPaletteCollapsed) {
	                        _this.toggleMetricPanel();
	                    }
	                }
	            });
	            _this.refreshRecentProjects();
	            _this.subscribeToInteractions();
	            _this.applyFastPollQueryStringParam();
	            _this.shareLink = _this.$location.absUrl();
	            //HACK: injecting tabs into the panel header
	            _this.$timeout(function () {
	                var leftPanelHeader = $('.ps-metric-tile-panel  ps-panel-header');
	                $('.ps-metric-tile-panel .xui-panel__heading').prepend(leftPanelHeader);
	            }, 0, false);
	        };
	        this.isFullScreen = false;
	        this.forwardBtnDisabled = true;
	        this.isRefreshingChartModels = false;
	        this.isBusy = 0;
	        this.isFastPolling = false;
	        this.isMetricPaletteBusy = 0;
	        this.isMetricSelectorBusy = 0;
	        this.isMetricPaletteCollapsed = true;
	        this.charts = [];
	        this.metricSearchTerm = '';
	        this.entitySearchTerm = '';
	        this.timepickerDisplay = {
	            timePickerData: {
	                startDatetime: this.timeService.current().start().toDate(),
	                endDatetime: this.timeService.current().end().toDate(),
	                selectedPresetId: "last12Hours"
	            },
	            xuiPreset: {
	                last10Minutes: {
	                    name: this._t('Last 10 Minutes'),
	                    startDatetimePattern: { minutes: -10 },
	                    endDatetimePattern: {}
	                },
	                lastHour: this.xuiTimeframeService.getDefaultPresets().lastHour,
	                last2Hours: {
	                    name: this._t('Last 2 Hours'),
	                    startDatetimePattern: { hours: -2 },
	                    endDatetimePattern: {}
	                },
	                last12Hours: this.xuiTimeframeService.getDefaultPresets().last12Hours,
	                last24Hours: this.xuiTimeframeService.getDefaultPresets().last24Hours,
	                last5Days: this.xuiTimeframeService.getDefaultPresets().last5Days,
	                last7Days: this.xuiTimeframeService.getDefaultPresets().last7Days,
	                last30Days: this.xuiTimeframeService.getDefaultPresets().last30Days,
	                last90Days: {
	                    name: this._t('Last 90 Days'),
	                    startDatetimePattern: { days: -90 },
	                    endDatetimePattern: {}
	                }
	            },
	            defaultPresetId: "last12Hours",
	            isLoaded: false
	        };
	        this.updateChartsDropTargetStatus = function (event, draggingModel) {
	            var isEntity = draggingModel.hasOwnProperty('instanceType');
	            if (isEntity) {
	                _this.emptyDropMessage = DropMessage.entityDropAllowed;
	            }
	            else {
	                _this.emptyDropMessage = DropMessage.metricDropAllowed;
	            }
	            _.forEach(_this.charts, function (chartModel) {
	                var alreadyPlotting = chartModel.series.hasOwnProperty(draggingModel.id);
	                if (isEntity || alreadyPlotting) {
	                    chartModel.isDropTarget = false;
	                    if (isEntity) {
	                        chartModel.dropMessage = DropMessage.entityDropDisallowed;
	                    }
	                    else {
	                        chartModel.dropMessage = DropMessage.metricDropDisallowed;
	                    }
	                }
	                else {
	                    chartModel.isDropTarget = true;
	                    chartModel.dropMessage = DropMessage.metricDropAllowed;
	                }
	            });
	        };
	        this.inUseRealTimeMetrics = {};
	        this.inUseRealTimeMetricsCounts = {};
	        this.fastPollTimeInterval = 1000;
	        this.maxRealTimeMetricsCount = this.settings.maxRealtimeMetricsPerPerfStackCount;
	        //HACK: removing/change XUI presets breaks Timepicker
	        this.presetTimeToWindowMap = {
	            'last10Minutes': [10, 'm'],
	            'lastHour': [1, 'h'],
	            'last2Hours': [2, 'h'],
	            'last12Hours': [12, 'h'],
	            'last24Hours': [24, 'h'],
	            'last5Days': [5, 'd'],
	            'last7Days': [7, 'd'],
	            'last30Days': [30, 'd'],
	            'last90Days': [90, 'd']
	        };
	        this.confirmLocationChange = function (event) {
	            if (_this.project.isDirty) {
	                return _this._t('You have unsaved changes to your project, are you sure you want to leave?');
	            }
	        };
	        this.selectedEntitiesFiltered = [];
	        this.inUseMetrics = {};
	        this.inUseMetricsFiltered = [];
	        this.realTimeMetricsFiltered = [];
	        this.metricsFiltered = [];
	        this.metrics = [];
	        this.realTimeMetrics = [];
	        this.entities = [];
	        this.entitiesFiltered = [];
	        this.selectedEntities = {};
	        this.defaultChartHeightIncrement = 48;
	        this.defaultChartHeight = 3 * this.defaultChartHeightIncrement;
	        this._interactionSubIds = [];
	        this.destroy = function () {
	            _.forEach(_this._interactionSubIds, function (subId) {
	                _this.interactionService.unsubscribe(subId);
	            });
	        };
	    }
	    Controller.prototype.toggleFullScreen = function () {
	        var _this = this;
	        this.isFullScreen = !this.isFullScreen;
	        if (this.isFullScreen) {
	            $('.sw-mega-menu').css('display', 'none');
	        }
	        else {
	            $('.sw-mega-menu').css('display', 'block');
	        }
	        this.$timeout(function () {
	            _this.chartManager.redrawAll();
	        }, 100);
	    };
	    Controller.prototype.onPanelToggleComplete = function () {
	        this.chartManager.redrawAll();
	        this.$rootScope.$digest();
	    };
	    Controller.prototype.toggleFastPolling = function (isZoomNeeded) {
	        if (isZoomNeeded === void 0) { isZoomNeeded = true; }
	        this.isFastPolling = !this.isFastPolling;
	        this.$timeout.cancel(this.fastPollingInterval);
	        this.$interval.cancel(this.fastUpdateInterval);
	        if (this.isFastPolling) {
	            if (isZoomNeeded) {
	                var timeframe = this.getFastPollTimeFrame();
	                // this.zoom(ZoomDirection.In, timeframe);
	                this.timepickerDisplay.timePickerData.selectedPresetId = 'last10Minutes';
	                this.displayDateChanged({
	                    startDatetime: timeframe.start().toDate(),
	                    endDatetime: timeframe.end().toDate(),
	                    selectedPresetId: 'last10Minutes'
	                }, true);
	            }
	            this.startFastPolling();
	            this.startFastUpdate();
	        }
	        else {
	            // HACK: Trigger a digest cycle to update any Real-Time Polling timeouts
	            this.$timeout(angular.noop, 5000);
	        }
	    };
	    Controller.prototype.validateFastPollingRequest = function (toggle) {
	        if (toggle === void 0) { toggle = false; }
	        // stop Real-Time Polling if more than 10 registered
	        var currentCount = _.keys(this.inUseRealTimeMetrics).length;
	        var hasPermission = this.hasRealtimePollingPermission();
	        if (hasPermission) {
	            if (currentCount > this.maxRealTimeMetricsCount) {
	                if (this.isFastPolling || toggle) {
	                    this.stopFastPolling();
	                    this.toast.warning(String.format(this._t('Real-Time Polling is available for {0} or fewer unique metrics.'), this.maxRealTimeMetricsCount), this._t('Exceeded Maximum Real-Time Polling Metrics Per Project'));
	                }
	            }
	            else if (toggle) {
	                this.toggleFastPolling();
	            }
	        }
	        else if (toggle) {
	            this.stopFastPolling();
	            this.toast.warning(String.format(this._t('Your account is not allowed to start real-time polling. Request your SolarWinds administrator to provision your current user account to allow real-time polling.'), this.maxRealTimeMetricsCount), this._t('Restricted'));
	        }
	    };
	    Controller.prototype.getFastPollingLabel = function () {
	        return (this.isFastPolling) ? this._t('Stop Real-Time Polling') : this._t('Start Real-Time Polling');
	    };
	    Controller.prototype.hasRealtimePollingPermission = function () {
	        // let userSetting = getUserSetting(UserSettings.AllowRealTimePolling);
	        var permissionToUseRealTimePolling = (((this.userSettings.AllowRealTimePolling) ?
	            (this.userSettings.AllowRealTimePolling.toLowerCase() !== 'false') : true) && !this.settings.isEoc);
	        return permissionToUseRealTimePolling;
	    };
	    Controller.prototype.buildEntityContext = function (clean, blocking) {
	        if (clean === void 0) { clean = false; }
	        if (blocking === void 0) { blocking = false; }
	        var chartEntityIds = [];
	        _.forEach(this.charts, function (chartModel) {
	            _.forEach(chartModel.series, function (series) {
	                chartEntityIds.push(series.id.split('-')[0]);
	            });
	        });
	        chartEntityIds = _.uniq(chartEntityIds);
	        if (clean) {
	            this.entityContext = chartEntityIds;
	            this.refreshEntities(blocking);
	            this.clearSearchTerm();
	        }
	        else {
	            var entitiesToAdd = _.difference(chartEntityIds, this.entityContext);
	            this.entityContext = _.concat(this.entityContext, entitiesToAdd);
	            this.addEntities(entitiesToAdd, blocking);
	        }
	    };
	    Controller.prototype.goToDetailsView = function (entity) {
	        if (!_.isEmpty(entity.detailsUrl)) {
	            this.$window.open(entity.detailsUrl, '_blank');
	        }
	        else {
	            this.$log.error('Entity detailUrl is blank');
	        }
	    };
	    Controller.prototype.addRelatedEntitiesToContext = function (entity) {
	        var _this = this;
	        this.isMetricSelectorBusy++;
	        this.entityService.getEntitiesAndRelatives([entity.id]).then(function (entities) {
	            var beforeCount = _this.entities.length;
	            _this.entities = _.unionBy(_this.entities, entities, 'id');
	            var afterCount = _this.entities.length;
	            var diff = afterCount - beforeCount;
	            _this.entityContext = _.union(_this.entityContext, _.map(entities, 'id'));
	            if (diff > 0) {
	                _this.toast.info(String.format(_this._t("{0} entities added to the metric palette."), diff));
	            }
	            else {
	                _this.toast.info(_this._t("All related entities are already available in the metric palette."));
	            }
	        }).finally(function () {
	            _this.isMetricSelectorBusy--;
	            _this.$timeout(function () {
	                _this.chartManager.redrawAll();
	            }, 500);
	        });
	    };
	    Controller.prototype.createChart = function (metricIds, insertionIndex) {
	        var _this = this;
	        var chartModel = {
	            id: this.utilityService.uuid(),
	            displayName: 'Custom Chart',
	            series: {},
	            calculatedHeight: this.defaultChartHeight,
	            isDropTarget: false,
	            isBusy: 0
	        };
	        var seriesCache = {};
	        var promises = [];
	        if (!_.isNaN(insertionIndex)) {
	            this.charts.splice(insertionIndex, 0, chartModel);
	        }
	        else {
	            this.charts.push(chartModel);
	        }
	        _.forEach(metricIds, function (metricId) {
	            chartModel.isBusy++;
	            var metricPromise = _this.metricService.getMetric([metricId]).then(function (metric) {
	                if (metric === null || metric.id === null) {
	                    _this.$log.warn('metricId is not accessible: ' + metricId);
	                    return;
	                }
	                var metricSeries = Metric.metricDtoToMetricSeries(metric);
	                _this.scrubMetricData(metricSeries);
	                // Reference requested id NOT delivered id
	                seriesCache[metricId] = metricSeries;
	            }).finally(function () {
	                chartModel.isBusy--;
	                _this.$timeout(function () {
	                    _this.chartManager.redrawAll();
	                }, 150);
	            });
	            promises.push(metricPromise);
	        });
	        return this.$q.all(promises).finally(function () {
	            // add metrics in the order requested
	            _.forEach(metricIds, function (metricId) {
	                if (seriesCache.hasOwnProperty(metricId)) {
	                    var chart = _this.chartManager.get(chartModel.id);
	                    var metricSeries = seriesCache[metricId];
	                    chart.addSeries([metricSeries]);
	                    _this.registerInUseRealTimeMetric(metricSeries);
	                    _this.inUseMetrics[metricSeries.id] = metricSeries;
	                    _this.updateCalculatedChartHeight(chartModel.id);
	                    _this.updateProjectData();
	                    _this.updateChartsQueryString();
	                }
	            });
	            // HACK: should abstract to remove chart function
	            if (_.size(chartModel.series) === 0) {
	                var index = _.findIndex(_this.charts, ['id', chartModel.id]);
	                _this.charts.splice(index, 1);
	                if (_this.charts.length === 0 && _this.isMetricPaletteCollapsed) {
	                    _this.toggleMetricPanel();
	                }
	            }
	        });
	    };
	    Controller.prototype.isDeleteDisabled = function () {
	        var hasCurrentOwner = (this.project.owner.toLocaleLowerCase() === SW.user.AccountID.toLocaleLowerCase());
	        return !(hasCurrentOwner && !_.isEmpty(this.project.id));
	    };
	    Controller.prototype.isSaveDisabled = function () {
	        var hasCurrentOwner = (this.project.owner.toLocaleLowerCase() === SW.user.AccountID.toLocaleLowerCase());
	        return !(hasCurrentOwner && this.project.isDirty || _.isEmpty(this.project.id));
	    };
	    Controller.prototype.isFastPollingDisabled = function () {
	        return _.keys(this.inUseRealTimeMetrics).length === 0;
	    };
	    Controller.prototype.saveProject = function () {
	        var _this = this;
	        this.updateProjectData();
	        if (_.isEmpty(this.project.id)) {
	            this.showSaveAsProjectDialog();
	        }
	        else {
	            this.isBusy++;
	            this.projectService.updateProject(this.project).then(function () {
	                _this.project.isDirty = false;
	                _this.$state.go('.', { id: _this.project.id }, { notify: false });
	                _this.toast.success(_this._t('Analysis project saved.'));
	            }, function (error) {
	                _this.toast.error(error.message + '<br/>' + error.exceptionMessage);
	            }).finally(function () {
	                _this.isBusy--;
	            });
	        }
	    };
	    Controller.prototype.showSaveAsProjectDialog = function () {
	        var _this = this;
	        try {
	            var viewModel_1 = angular.copy(this.project);
	            viewModel_1.id = null;
	            viewModel_1.owner = null;
	            var saveButton = {
	                name: 'saveProject',
	                isPrimary: true,
	                text: this._t("SAVE"),
	                action: function (dialog) {
	                    var deferred = _this.$q.defer();
	                    if (!dialog.$valid) {
	                        deferred.resolve(false);
	                    }
	                    else {
	                        _this.projectService.createProject(viewModel_1).then(function (project) {
	                            _this.project = project;
	                            _this.project.isDirty = false;
	                            _this.loadProject(project.id);
	                            _this.toast.success(_this._t("Analysis project saved."));
	                            deferred.resolve(true);
	                        }, function (error) {
	                            if (error) {
	                                _this.toast.error(error.message + '<br/>' + error.exceptionMessage);
	                            }
	                            deferred.resolve(false);
	                        });
	                    }
	                    return deferred.promise;
	                }
	            };
	            var dialogOptions = {
	                title: this._t('Save Project As'),
	                cancelButtonText: this._t("Cancel"),
	                viewModel: viewModel_1,
	                buttons: [
	                    saveButton
	                ]
	            };
	            this.dialogService.showModal({ templateUrl: 'app/components/dialog/save-project-dialog.html' }, dialogOptions);
	        }
	        catch (e) {
	            this.$log.error("Analysis.showSaveProjectDialog", e);
	        }
	    };
	    Controller.prototype.showLoadProjectDialog = function () {
	        var _this = this;
	        try {
	            var viewModel_2 = {
	                projectSelection: {
	                    items: []
	                }
	            };
	            var addButton = {
	                name: 'loadProjectAction',
	                isPrimary: true,
	                text: this._t("LOAD"),
	                action: function (dialog) {
	                    var deferred = _this.$q.defer();
	                    if (!dialog.$valid || viewModel_2.projectSelection.items.length === 0) {
	                        deferred.resolve(false);
	                    }
	                    else {
	                        // only add if not already there
	                        _this.loadProject(viewModel_2.projectSelection.items[0].id);
	                        deferred.resolve(true);
	                    }
	                    return deferred.promise;
	                }
	            };
	            var dialogOptions = {
	                title: this._t('Load Project'),
	                cancelButtonText: this._t("Cancel"),
	                viewModel: viewModel_2,
	                buttons: [
	                    addButton
	                ]
	            };
	            this.dialogService
	                .showModal({
	                templateUrl: 'app/components/dialog/load-project-dialog.html',
	                windowClass: "ps-dialog--large"
	            }, dialogOptions)
	                .then(function (entitiesAdded) {
	                if (entitiesAdded !== "cancel") {
	                    // project loaded
	                }
	            });
	        }
	        catch (e) {
	            this.$log.error("Analysis.showAddEntitiesDialog", e);
	        }
	    };
	    Controller.prototype.showAddEntitiesDialog = function (relatedTo) {
	        var _this = this;
	        try {
	            var previousCount_1 = this.entities.length;
	            var viewModel_3 = {
	                entitySelection: {
	                    items: []
	                },
	                relative: relatedTo
	            };
	            var addButton = {
	                name: 'addEntitiesAction',
	                isPrimary: true,
	                text: this._t("ADD SELECTED ITEMS"),
	                // isDisabled: (viewModel.entitySelection.items.length === 0),
	                action: function (dialog) {
	                    var deferred = _this.$q.defer();
	                    if (!dialog.$valid || viewModel_3.entitySelection.items.length === 0) {
	                        deferred.resolve(false);
	                    }
	                    else {
	                        // only add if not already there
	                        _this.entities = _.uniqBy(_.concat(_this.entities, viewModel_3.entitySelection.items), 'id');
	                        _this.entityContext = _.map(_this.entities, 'id');
	                        _this.clearSearchTerm();
	                        deferred.resolve(true);
	                    }
	                    return deferred.promise;
	                }
	            };
	            var dialogOptions = {
	                title: (viewModel_3.relative) ? String.format(this._t("Add entities related to '{0}' "), viewModel_3.relative.displayName) : this._t('Add Entities'),
	                cancelButtonText: this._t("Cancel"),
	                viewModel: viewModel_3,
	                buttons: [
	                    addButton
	                ]
	            };
	            this.dialogService
	                .showModal({
	                templateUrl: 'app/components/dialog/add-entities-dialog.html',
	                windowClass: "ps-dialog--large"
	            }, dialogOptions)
	                .then(function (entitiesAdded) {
	                if (entitiesAdded !== "cancel") {
	                    var addedCount = _this.entities.length - previousCount_1;
	                    if (addedCount > 0) {
	                        _this.toast.success(String.format(_this._t("{0} entities added to the metric palette."), addedCount));
	                    }
	                    else {
	                        _this.toast.info(_this._t("Selected entities are already available in the metric palette."));
	                    }
	                }
	            });
	        }
	        catch (e) {
	            this.$log.error("Analysis.showAddEntitiesDialog", e);
	        }
	    };
	    Controller.prototype.onCreateDrop = function (serializedModel, chartIndex) {
	        var model = angular.fromJson(serializedModel);
	        var isEntity = model.hasOwnProperty('instanceType');
	        if (isEntity) {
	            this.initChartsFromEntityTemplate(model, chartIndex);
	        }
	        else {
	            this.createChartViaMetricDrop(serializedModel, chartIndex);
	        }
	    };
	    Controller.prototype.createChartViaMetricDrop = function (serializedMetric, chartIndex) {
	        var _this = this;
	        var metric = angular.fromJson(serializedMetric);
	        var chartModel = {
	            id: this.utilityService.uuid(),
	            displayName: 'Custom Chart',
	            series: {},
	            calculatedHeight: this.defaultChartHeight,
	            isDropTarget: false,
	            isBusy: 0
	        };
	        chartModel.isBusy++;
	        this.charts.splice(chartIndex, 0, chartModel);
	        this.metricService.getMetric([metric.id]).then(function (resultingMetric) {
	            var chart = _this.chartManager.get(chartModel.id);
	            var metricSeries = Metric.metricDtoToMetricSeries(resultingMetric);
	            _this.scrubMetricData(metricSeries);
	            chart.addSeries([metricSeries]);
	            _this.registerInUseRealTimeMetric(metric);
	            _this.inUseMetrics[metric.id] = metric;
	            _this.updateCalculatedChartHeight(chartModel.id);
	            _this.project.isDirty = true;
	            _this.updateProjectData();
	            _this.updateChartsQueryString();
	        }).finally(function () {
	            chartModel.isBusy--;
	            _this.$timeout(function () {
	                _this.chartManager.redrawAll();
	            }, 150);
	        });
	    };
	    Controller.prototype.removeMetricFromChart = function (seriesId, chartId) {
	        //Remove from inUseMetrics if seriesId is used 1x
	        this.removeMetricFromInUseMetrics(seriesId);
	        var chart = this.chartManager.get(chartId);
	        chart.removeSeries(seriesId);
	        this.unregisterInUseRealTimeMetric(seriesId);
	        if (_.keys(this.inUseRealTimeMetrics).length === 0) {
	            this.stopFastPolling();
	        }
	        // remove chart if last metric removed
	        if (_.keys(chart.series()).length === 0) {
	            var index = _.findIndex(this.charts, ['id', chartId]);
	            this.charts.splice(index, 1);
	            if (this.charts.length === 0) {
	                this.resetTimeToDefault();
	                if (this.isFullScreen) {
	                    this.toggleFullScreen();
	                }
	                if (this.isMetricPaletteCollapsed) {
	                    this.toggleMetricPanel();
	                }
	                // cancel any active data inspection
	                this.clearInspectionSelection();
	            }
	        }
	        else {
	            this.updateCalculatedChartHeight(chartId);
	            this.$timeout(function () {
	                chart.draw();
	            }, 50);
	        }
	        this.project.isDirty = true;
	        this.updateProjectData();
	        this.updateChartsQueryString();
	    };
	    Controller.prototype.addMetricToChart = function (serializedMetric, chartId) {
	        var _this = this;
	        var metric = angular.fromJson(serializedMetric);
	        this.updateChartBusyState(chartId, 1);
	        this.metricService.getMetric([metric.id]).then(function (metricResult) {
	            var chart = _this.chartManager.get(chartId);
	            var metricSeries = Metric.metricDtoToMetricSeries(metricResult);
	            _this.scrubMetricData(metricSeries);
	            chart.addSeries([metricSeries]);
	            _this.registerInUseRealTimeMetric(metric);
	            _this.inUseMetrics[metric.id] = metric;
	            _this.updateCalculatedChartHeight(chartId);
	            _this.project.isDirty = true;
	            _this.updateProjectData();
	            _this.updateChartsQueryString();
	            _this.$timeout(function () {
	                chart.draw();
	            }, 50);
	        }).finally(function () {
	            _this.updateChartBusyState(chartId, -1);
	        });
	    };
	    Controller.prototype.getOrderedPlots = function (chartModel) {
	        var chart = this.chartManager.get(chartModel.id);
	        return chart.getPlots();
	    };
	    Controller.prototype.getAvailableRealTimeMetrics = function () {
	        this.realTimeMetricsFiltered.length = 0;
	        Array.prototype.push.apply(this.realTimeMetricsFiltered, this.filterMetrics(this.realTimeMetrics));
	        return this.realTimeMetricsFiltered;
	    };
	    Controller.prototype.getInUseMetrics = function () {
	        var _this = this;
	        this.inUseMetricsFiltered.length = 0;
	        var inUseOfSelectedEntities = _.filter(this.inUseMetrics, function (metric) {
	            return _.find(_this.selectedEntities, function (entity) {
	                return metric.entityId === entity.id;
	            });
	        });
	        Array.prototype.push.apply(this.inUseMetricsFiltered, this.filterMetrics(inUseOfSelectedEntities));
	        return this.inUseMetricsFiltered;
	    };
	    Controller.prototype.getSelectedEntities = function (filter) {
	        if (filter === void 0) { filter = true; }
	        this.selectedEntitiesFiltered.length = 0;
	        if (filter) {
	            Array.prototype.push.apply(this.selectedEntitiesFiltered, this.filterEntities(_.values(this.selectedEntities)));
	        }
	        else {
	            Array.prototype.push.apply(this.selectedEntitiesFiltered, _.values(this.selectedEntities));
	        }
	        return this.selectedEntitiesFiltered;
	    };
	    Controller.prototype.getEntities = function () {
	        this.entitiesFiltered.length = 0;
	        Array.prototype.push.apply(this.entitiesFiltered, this.filterEntities(this.entities));
	        return this.entitiesFiltered;
	    };
	    Controller.prototype.getMetrics = function () {
	        this.metricsFiltered.length = 0;
	        Array.prototype.push.apply(this.metricsFiltered, this.filterMetrics(this.metrics));
	        return this.metricsFiltered;
	    };
	    Controller.prototype.onMetricSearchTermChanged = function (value) {
	        // HACK: Until xui search directive accepts ng-model
	        this.metricSearchTerm = angular.element('#ps-metric-search_Input').val();
	    };
	    Controller.prototype.toggleEntitySelection = function (entity) {
	        if (this.selectedEntities.hasOwnProperty(entity.id)) {
	            delete this.selectedEntities[entity.id];
	            this.metrics = _.filter(this.metrics, function (metric) {
	                return metric.entityId !== entity.id;
	            });
	        }
	        else {
	            // HACK: Disabling multi-select for v1
	            for (var p in this.selectedEntities) {
	                if (this.selectedEntities.hasOwnProperty(p)) {
	                    delete this.selectedEntities[p];
	                }
	            }
	            this.metrics.length = 0;
	            this.realTimeMetrics.length = 0;
	            // END HACK
	            this.selectedEntities[entity.id] = entity;
	            this.addSelectedEntityMetrics(entity);
	        }
	    };
	    Controller.prototype.isEntitySelected = function (entity) {
	        return this.selectedEntities.hasOwnProperty(entity.id);
	    };
	    Controller.prototype.moveTimeWindowForward = function () {
	        var cloneEnd = this.timeService.current().end().clone();
	        var testEnd = cloneEnd.add(this.timeService.window()[0], this.timeService.window()[1]);
	        var currentMoment = moment();
	        if (testEnd.isAfter(currentMoment) || this.timeService.isCurrentTimeframeWithin(testEnd.seconds(), testEnd)) {
	            this.forwardBtnDisabled = true;
	            var diff = currentMoment.diff(testEnd);
	            this.timeService.current().start().add(diff);
	            this.timeService.current().end().add(diff);
	            this.timeService.updateToWindow(true);
	        }
	        else {
	            this.timeService.current().start().add(this.timeService.window()[0], this.timeService.window()[1]);
	            this.timeService.current().end().add(this.timeService.window()[0], this.timeService.window()[1]);
	            this.timepickerDisplay.timePickerData.startDatetime = this.timeService.current().start().local().toDate();
	            this.timepickerDisplay.timePickerData.endDatetime = this.timeService.current().end().local().toDate();
	            this.updateChartsToPickedTimeFrame();
	        }
	        this.updateTimePickerTitle();
	        //Remove inspection area- for reloading Timeline
	        this.clearInspectionSelection();
	    };
	    Controller.prototype.moveTimeWindowBack = function () {
	        this.stopFastPolling();
	        this.forwardBtnDisabled = false;
	        this.timeService.current().start().subtract(this.timeService.window()[0], this.timeService.window()[1]);
	        this.timeService.current().end().subtract(this.timeService.window()[0], this.timeService.window()[1]);
	        this.timepickerDisplay.timePickerData.selectedPresetId = null;
	        this.timepickerDisplay.timePickerData.title = this.getFormattedTitle();
	        this.timepickerDisplay.timePickerData.startDatetime = this.timeService.current().start().local().toDate();
	        this.timepickerDisplay.timePickerData.endDatetime = this.timeService.current().end().local().toDate();
	        this.updateChartsToPickedTimeFrame();
	        this.updateTimePickerTitle();
	        //Remove inspection area- for reloading Timeline
	        this.clearInspectionSelection();
	    };
	    Controller.prototype.displayDateChanged = function (model, keepFastPolling) {
	        if (keepFastPolling === void 0) { keepFastPolling = false; }
	        if (!this.timepickerDisplay.isLoaded) {
	            this.timepickerDisplay.isLoaded = true;
	        }
	        else {
	            if (model != null) {
	                if (!keepFastPolling) {
	                    this.stopFastPolling();
	                }
	                var testEnd = moment(model.endDatetime);
	                if (testEnd.isAfter(moment())) {
	                    model.startDatetime = this.timeService.current().start().toDate();
	                    model.endDatetime = this.timeService.current().end().toDate();
	                    model.title = this.getFormattedTitle();
	                }
	                else {
	                    this.timepickerDisplay.timePickerData.title = model.title;
	                    this.timeService.current().start(moment(model.startDatetime));
	                    this.timeService.current().end(moment(model.endDatetime));
	                    //HACK: Time-picker updated from get timeService not model: updating from UIF-4180
	                    this.timepickerDisplay.timePickerData.startDatetime = this.timeService.current().start().local().toDate();
	                    this.timepickerDisplay.timePickerData.endDatetime = this.timeService.current().end().local().toDate();
	                    if (model.selectedPresetId != null) {
	                        var getPresetModel = this.presetTimeToWindowMap[model.selectedPresetId];
	                        this.timeService.window(getPresetModel[0], getPresetModel[1]); //HACK: removing or change Xui Preset will break Time Picker
	                    }
	                    else {
	                        var newDuration = moment.duration(moment(model.endDatetime).diff(moment(model.startDatetime))).asMinutes();
	                        this.timeService.window(newDuration, 'm');
	                    }
	                    this.updateChartsToPickedTimeFrame();
	                }
	            }
	            //Note: Remove inspection area- for reloading Timeline
	            this.clearInspectionSelection();
	        }
	    };
	    Controller.prototype.updateTimePickerTitle = function () {
	        if (!this.timepickerDisplay.timePickerData.selectedPresetId) {
	            this.timepickerDisplay.timePickerData.selectedPresetId = null;
	            this.timepickerDisplay.timePickerData.title = this.getFormattedTitle();
	        }
	        else {
	            this.timepickerDisplay.timePickerData.title = this.timepickerDisplay.xuiPreset[this.timepickerDisplay.timePickerData.selectedPresetId].name;
	        }
	    };
	    Controller.prototype.getCurrentTimeEnd = function () {
	        return this.timeService.current().end();
	    };
	    Controller.prototype.getCurrentTimeStart = function () {
	        return this.timeService.current().start();
	    };
	    Controller.prototype.getForwardBtnDisabled = function () {
	        var forwardBtnState;
	        if (this.isRefreshingChartModels || this.timeService.isCurrentTimeframeWithin()) {
	            forwardBtnState = true;
	        }
	        else {
	            forwardBtnState = false;
	        }
	        this.forwardBtnDisabled = forwardBtnState;
	    };
	    Controller.prototype.removeEntityLocally = function (getEntity) {
	        if (this.isEntitySelected(getEntity)) {
	            this.toggleEntitySelection(getEntity);
	        }
	        ;
	        this.entities.splice(_.indexOf(this.entities, getEntity), 1);
	        this.entityContext.splice(_.indexOf(this.entityContext, getEntity.id), 1);
	    };
	    Controller.prototype.newProject = function () {
	        if (this.project.isDirty) {
	            if (this.$window.confirm(this._t('You have unsaved changes to the current project, are you sure you want to continue?'))) {
	                this.resetProject();
	            }
	        }
	        else {
	            this.resetProject();
	        }
	    };
	    Controller.prototype.resetProject = function (resetState) {
	        if (resetState === void 0) { resetState = true; }
	        this.resetFastPolling();
	        this.project = new project_1.Project({ owner: SW.user.AccountID });
	        this.project.displayName = this._t('New Analysis Project');
	        this.project.isDirty = false;
	        this.charts.length = 0;
	        if (resetState) {
	            this.$state.go('.', { id: null }, { notify: false });
	        }
	        this.refreshRecentProjects();
	        if (resetState && this.isMetricPaletteCollapsed) {
	            this.toggleMetricPanel();
	        }
	        // PS-829: Cancel any active data inspection
	        this.clearInspectionSelection();
	        //PS-834: Reset to Default Timeframe
	        if (resetState) {
	            this.resetTimeToDefault();
	            this.updateProjectData();
	            this.updateTimeQueryString();
	        }
	    };
	    Controller.prototype.deleteProject = function () {
	        var _this = this;
	        try {
	            this.projectService.getProjectViews(this.project.id).then(function (viewNames) {
	                var message = _this.getDeleteMessage(_this.project.displayName, viewNames);
	                _this.displayDeleteModal(message);
	            });
	        }
	        catch (e) {
	            this.$log.error("Analysis.deleteProject", e);
	        }
	    };
	    Controller.prototype.displayDeleteModal = function (message) {
	        var _this = this;
	        var viewModel = this.project;
	        var deleteButton = {
	            name: 'deleteProject',
	            isPrimary: true,
	            text: this._t("DELETE"),
	            action: function (dialog) {
	                var deferred = _this.$q.defer();
	                if (!dialog.$valid) {
	                    deferred.resolve(false);
	                }
	                else {
	                    _this.projectService.deleteProject(viewModel).then(function (project) {
	                        _this.resetProject();
	                        _this.toast.success(_this._t("Project deleted."));
	                        deferred.resolve(true);
	                    }, function (error) {
	                        if (error) {
	                            _this.toast.error(error.message + '<br/>' + error.exceptionMessage);
	                        }
	                        deferred.resolve(false);
	                    });
	                }
	                return deferred.promise;
	            }
	        };
	        var dialogOptions = {
	            title: this._t('Delete Project'),
	            message: message,
	            cancelButtonText: this._t("Cancel"),
	            viewModel: viewModel,
	            buttons: [
	                deleteButton
	            ]
	        };
	        this.dialogService.showModal({ size: "lg" }, dialogOptions);
	    };
	    Controller.prototype.updateShareLink = function () {
	        this.shareLink = this.$location.absUrl();
	        return this.shareLink;
	    };
	    ;
	    Controller.prototype.toastShareLinkCopied = function (error) {
	        if (error) {
	            this.toast.error(error.message);
	        }
	        else {
	            this.toast.success(this.updateShareLink(), this._t('Copied URL to Clipboard:'));
	            this.analyticsService.sendEvent('PerfStack', // hack: need to upgrade to typescript > 2.4 for string enum
	            'share-project', 'Share link copied via control ribbon share button');
	        }
	    };
	    ;
	    Controller.prototype.isProjectExportable = function () {
	        // charts present and none busy
	        return this.charts.length > 0 && !_.some(this.charts, function (chart) {
	            return chart.isBusy;
	        });
	    };
	    Controller.prototype.exportProject = function () {
	        var csv = this.projectService.toCSV(this.project, this.charts);
	        var exportAnchor = document.createElement('a');
	        var csvBlob = new Blob([csv], {
	            "type": "text/csv;charset=utf8;"
	        });
	        if (exportAnchor.download !== undefined) {
	            exportAnchor.setAttribute('href', this.$window.URL.createObjectURL(csvBlob));
	            exportAnchor.setAttribute('download', this.project.displayName + '.csv');
	            document.body.appendChild(exportAnchor);
	            exportAnchor.click();
	            document.body.removeChild(exportAnchor);
	        }
	        else {
	            // IE 11 support
	            var handle = this.$window.open();
	            handle.document.write('sep=,\r\n' + csv);
	            handle.document.close();
	            handle.document.execCommand('SaveAs', true, this.project.displayName + ".csv");
	            handle.close();
	        }
	        this.analyticsService.sendEvent('PerfStack', 'export-project', 'Project exported to csv');
	    };
	    Controller.prototype.getDeleteMessage = function (projectName, viewNames) {
	        var projectMsg = this.swUtil.formatString(this._t('Are you sure you want to delete "{0}"?'), projectName);
	        if (viewNames && viewNames.length) {
	            projectMsg += '\r\n' + this.swUtil.formatString((this._t('Deleting this PerfStack will also delete it from any views where it is being used as a widget.')) +
	                (this._t('This perfstack is being used as a widget in the following views:  {0}')), viewNames.join(", "));
	        }
	        return projectMsg;
	    };
	    Controller.prototype.registerInUseRealTimeMetric = function (metric) {
	        if (metric.realTime) {
	            if (this.inUseRealTimeMetrics.hasOwnProperty(metric.id)) {
	                this.inUseRealTimeMetricsCounts[metric.id]++;
	            }
	            else {
	                this.inUseRealTimeMetrics[metric.id] = metric;
	                this.inUseRealTimeMetricsCounts[metric.id] = 1;
	            }
	            this.validateFastPollingRequest();
	        }
	    };
	    Controller.prototype.getFastPollTimeFrame = function () {
	        return new time_data_1.Timeframe().end(moment()).start(moment().subtract(10, 'minutes'));
	    };
	    Controller.prototype.unregisterInUseRealTimeMetric = function (metricId) {
	        if (this.inUseRealTimeMetrics.hasOwnProperty(metricId)) {
	            this.inUseRealTimeMetricsCounts[metricId]--;
	            if (this.inUseRealTimeMetricsCounts[metricId] <= 0) {
	                delete this.inUseRealTimeMetrics[metricId];
	                delete this.inUseRealTimeMetricsCounts[metricId];
	            }
	        }
	    };
	    Controller.prototype.clearSearchTerm = function () {
	        this.entitySearchTerm = '';
	    };
	    Controller.prototype.resetTimeToDefault = function () {
	        this.timeService.setToDefaultTimeWindow();
	        this.timeService.updateToWindow(false);
	        this.timepickerDisplay.timePickerData.selectedPresetId = this.timepickerDisplay.defaultPresetId;
	        this.timepickerDisplay.timePickerData.title =
	            this.timepickerDisplay.xuiPreset[this.timepickerDisplay.timePickerData.selectedPresetId].name;
	        this.timepickerDisplay.timePickerData.endDatetime = this.getCurrentTimeEnd().toDate();
	        this.timepickerDisplay.timePickerData.startDatetime = this.getCurrentTimeStart().toDate();
	    };
	    Controller.prototype.getFormattedTitle = function () {
	        return this.getCurrentTimeStart().local().format('LLL') + '-' + this.getCurrentTimeEnd().local().format('LLL');
	    };
	    Controller.prototype.serializeCharts = function () {
	        var serializedCharts = (this.charts.length > 0) ? '' : null;
	        _.forEach(this.charts, function (chartModel) {
	            serializedCharts += _.map(chartModel.series, function (series) {
	                return series.id;
	            }).join(',') + ';';
	        });
	        return serializedCharts;
	    };
	    Controller.prototype.updateProjectData = function () {
	        this.project.data = {
	            charts: this.serializeCharts(),
	            startTime: (this.timepickerDisplay.timePickerData.selectedPresetId === null) ? this.timeService.current().start().utc().toISOString() : null,
	            endTime: (this.timepickerDisplay.timePickerData.selectedPresetId === null) ? this.timeService.current().end().utc().toISOString() : null,
	            presetTime: this.timepickerDisplay.timePickerData.selectedPresetId
	        };
	    };
	    // HACK: have to update clip-path absolute references because base element is in use and relative references break is some browsers (FF)
	    Controller.prototype.updateIRIFuncReferences = function () {
	        var _this = this;
	        if (typeof window.InstallTrigger !== 'undefined') {
	            $(".ps-chart-wrapper [clip-path]").each(function (index, element) {
	                var value = 'url(' + _this.$location.absUrl() + '#' + element.getAttribute('clip-path').split('#')[1];
	                element.setAttribute('clip-path', value);
	            });
	        }
	    };
	    Controller.prototype.updateTimeQueryString = function () {
	        this.$location.search('startTime', (_.isEmpty(this.project.id) && this.project.data) ? this.project.data.startTime : null);
	        this.$location.search('endTime', (_.isEmpty(this.project.id) && this.project.data) ? this.project.data.endTime : null);
	        this.$location.search('presetTime', (_.isEmpty(this.project.id) && this.project.data) ? this.project.data.presetTime : null);
	        if (this.isFastPolling) {
	            this.$location.replace();
	        }
	        this.updateIRIFuncReferences();
	    };
	    Controller.prototype.updateChartsQueryString = function () {
	        this.$location.search('charts', (_.isEmpty(this.project.id)) ? this.project.data.charts : null);
	        this.$location.replace();
	        this.updateIRIFuncReferences();
	    };
	    Controller.prototype.updateChartsToPickedTimeFrame = function () {
	        var _this = this;
	        if (!this.timepickerDisplay.isLoaded) {
	            this.timepickerDisplay.isLoaded = true;
	        }
	        else {
	            this.forwardBtnDisabled = true;
	            _.forEach(this.charts, function (chartModel) {
	                chartModel.isBusy++;
	            });
	            this.refreshChartModels(true).then(function () {
	                _this.getForwardBtnDisabled();
	                _.forEach(_this.charts, function (chartModel) {
	                    chartModel.isBusy--;
	                });
	            }).finally(function () {
	                _this.chartManager.redrawAll();
	            });
	        }
	        //Note: Timeframe changed: for all changes that Enabled Save Button: Time Forward, Back, Custom, ZoomIn, ZoomOut
	        console.log("*** project is dirty");
	        this.project.isDirty = true;
	    };
	    // private validateMetricForDisplay(metric: Metric.IMetric): boolean {
	    //   switch (true) {
	    //     case (metric.type !== Type.multi && metric.measurements.length === 0):
	    //     case (metric.type === Type.multi && metric.subMetrics.length === 0):
	    //       this.toast.warning((<any>String).format(this._t('No measurements are available for {0}.'), metric.displayName));
	    //       return false;
	    //   }
	    //   return true;
	    // }
	    Controller.prototype.updateChartBusyState = function (chartId, increment) {
	        var chartModel = _.find(this.charts, function (chartModel) {
	            return chartModel.id === chartId;
	        });
	        chartModel.isBusy += increment;
	    };
	    Controller.prototype.updateAllCalculatedChartHeights = function () {
	        var _this = this;
	        _.forEach(this.charts, function (chartModel) {
	            chartModel.calculatedHeight = _this.chartManager.get(chartModel.id).layout().update().totalHeight();
	        });
	    };
	    Controller.prototype.updateCalculatedChartHeight = function (chartId) {
	        var chartModel = _.find(this.charts, function (chartModel) {
	            return chartModel.id === chartId;
	        });
	        // HACK: shouldn't need to layout().update() here
	        chartModel.calculatedHeight = this.chartManager.get(chartId).layout().update().totalHeight();
	    };
	    Controller.prototype.filterMetrics = function (metricsList) {
	        var _this = this;
	        var filteredResult;
	        filteredResult = _.filter(metricsList, function (metric) {
	            return _this.isMatchToSearch(metric.displayName, _this.metricSearchTerm);
	        });
	        return filteredResult;
	    };
	    Controller.prototype.filterEntities = function (entitiesList) {
	        var _this = this;
	        this.entitySearchTerm = (this.entitySearchTerm || '').trim();
	        var filteredResult = [];
	        var generalQuery = /(.*)(?:status:)|(.*)(?:selected:)|(.*)/;
	        var generalTerm = this.applyQuery(generalQuery, this.entitySearchTerm).trim();
	        var generalResults;
	        var statusQuery = /(?:status:)(.*)/;
	        var statusTerm = this.applyQuery(statusQuery, this.entitySearchTerm).trim();
	        var statusResults;
	        var selectedQuery = /(?:selected:)(.*)/;
	        var selectedTerm = this.applyQuery(selectedQuery, this.entitySearchTerm).trim();
	        var selectedResults;
	        var intersections = [entitiesList];
	        if (generalTerm !== '') {
	            generalResults = _.filter(entitiesList, function (entity) {
	                return _this.isMatchToSearch(entity.displayName, generalTerm);
	            });
	            intersections.push(generalResults);
	        }
	        if (statusTerm !== '') {
	            statusResults = _.filter(entitiesList, function (entity) {
	                return _this.isMatchToSearch(entity.statusIconPostfix, statusTerm);
	            });
	            intersections.push(statusResults);
	        }
	        if (selectedTerm !== '') {
	            switch (selectedTerm) {
	                case '1':
	                case 'true':
	                    selectedResults = _.values(this.selectedEntities);
	                    break;
	                case '0':
	                    selectedResults = _.filter(entitiesList, function (entity) {
	                        return !_this.selectedEntities.hasOwnProperty(entity.id);
	                    });
	                    break;
	            }
	            intersections.push(selectedResults);
	        }
	        ;
	        filteredResult = _.intersection.apply(window, intersections);
	        return filteredResult;
	    };
	    Controller.prototype.applyQuery = function (query, target) {
	        var matches = target.match(query), result = '';
	        if (matches && matches.length) {
	            matches.shift();
	            result = _.filter(matches, function (x) { return (!angular.isUndefined(x)); })[0];
	        }
	        return result; //(matches && matches.length)? matches[matches.length-1]:"";
	    };
	    Controller.prototype.isMatchToSearch = function (value, searchTerm) {
	        if (angular.isUndefined(value) || value === null) {
	            return false;
	        }
	        return value.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
	    };
	    Controller.prototype.addSelectedEntityMetrics = function (entity) {
	        var _this = this;
	        this.isMetricPaletteBusy++;
	        this.metricService.getEntityMetrics([entity.id]).then(function (metrics) {
	            if (_this.selectedEntities.hasOwnProperty(entity.id)) {
	                _this.metrics.push.apply(_this.metrics, metrics);
	                _this.realTimeMetrics.push.apply(_this.realTimeMetrics, _.filter(metrics, function (metric) {
	                    return metric.realTime;
	                }));
	                _this.analyticsService.sendEvent('PerfStack', 'load-entity-metrics', 'Entity metrics loaded and added to palette', { eventValue: _this.metrics.length });
	            }
	        }).finally(function () {
	            _this.isMetricPaletteBusy--;
	            _this.$timeout(function () {
	                _this.chartManager.redrawAll();
	            }, 500);
	        });
	    };
	    // private toastUnreturnedEntities(requiredEntities: string[], entityList: IEntity[]) {
	    //   let unfoundIds = _.filter(requiredEntities, (id: string) => {
	    //     return (_.findIndex(entityList, {id: id}) < 0);
	    //   });
	    //   if (unfoundIds.length > 0) {
	    //     this.toast.error((<any>String).format(this._t('<p>Unable to find {0} {1} in passed context:</p><p>{2}</p>'),
	    //       unfoundIds.length,
	    //       (unfoundIds.length > 1) ? this._t('entities') : this._t('entity'),
	    //       unfoundIds.join(', ')));
	    //   }
	    // }
	    Controller.prototype.addEntities = function (entityIds, blocking) {
	        var _this = this;
	        if (entityIds.length > 0) {
	            if (blocking) {
	                this.isMetricSelectorBusy++;
	            }
	            this.entityService.getEntities(entityIds).then(function (result) {
	                _this.entities = _.unionBy(_this.entities, result.data, 'id');
	            }).finally(function () {
	                if (blocking) {
	                    _this.isMetricSelectorBusy--;
	                }
	            });
	        }
	    };
	    Controller.prototype.refreshEntities = function (blocking) {
	        var _this = this;
	        if (blocking === void 0) { blocking = true; }
	        var defered = this.$q.defer();
	        if (this.entityContext.length > 0) {
	            if (blocking) {
	                this.isMetricSelectorBusy++;
	            }
	            if (this.entityContext[0].toLocaleLowerCase() === "all") {
	                // Get everything; good luck out there...
	                this.entityService.getEntities().then(function (result) {
	                    _this.entities = result.data;
	                    _this.entityContext = _.map(_this.entities, 'id');
	                    defered.resolve(_this.entities);
	                }).finally(function () {
	                    if (blocking) {
	                        _this.isMetricSelectorBusy--;
	                    }
	                    _this.syncSelectedEntities();
	                    _this.$timeout(function () {
	                        _this.chartManager.redrawAll();
	                    }, 500);
	                });
	            }
	            else if (this.entityContextHasRelationships) {
	                this.entityService.getEntitiesAndRelatives(this.entityContext).then(function (entities) {
	                    _this.entities = entities;
	                    defered.resolve(_this.entities);
	                    // this.toastUnreturnedEntities(this.entityContext, entities);
	                }).finally(function () {
	                    if (blocking) {
	                        _this.isMetricSelectorBusy--;
	                    }
	                    _this.syncSelectedEntities();
	                    _this.$timeout(function () {
	                        _this.chartManager.redrawAll();
	                    }, 500);
	                });
	            }
	            else {
	                this.entityService.getEntities(this.entityContext, this.entityContext.length, 0).then(function (result) {
	                    _this.entities = result.data;
	                    defered.resolve(_this.entities);
	                    // this.toastUnreturnedEntities(this.entityContext, result.data);
	                }).finally(function () {
	                    if (blocking) {
	                        _this.isMetricSelectorBusy--;
	                    }
	                    _this.analyticsService.sendEvent('PerfStack', 'load-entity-context', 'Load entity context', { eventValue: _this.entityContext.length });
	                    _this.syncSelectedEntities();
	                    _this.$timeout(function () {
	                        _this.chartManager.redrawAll();
	                    }, 500);
	                });
	            }
	        }
	        else {
	            defered.resolve();
	        }
	        return defered.promise;
	        // else { // No context set, get everything
	        //   this.entityService.getEntities().then((result: IEntityResult) => {
	        //     this.entities = result.data;
	        //   }).finally(() => {
	        //     this.isMetricSelectorBusy--;
	        //     this.$timeout(() => { // HACK: to minimize scrollbar appearance/disappearce effect
	        //       this.chartManager.redrawAll();
	        //     }, 500);
	        //   });
	        // };
	    };
	    Controller.prototype.clearInspectionSelection = function () {
	        this.interactionService.inspect(null, {});
	    };
	    Controller.prototype.syncSelectedEntities = function () {
	        var _this = this;
	        if (_.keys(this.selectedEntities).length > 0) {
	            _.forEach(this.selectedEntities, function (selectedEntity) {
	                var foundIn = _.findIndex(_this.entities, function (entity) {
	                    return entity.id === selectedEntity.id;
	                });
	                if (foundIn === -1) {
	                    if (_this.isEntitySelected(selectedEntity)) {
	                        _this.toggleEntitySelection(selectedEntity);
	                    }
	                    delete _this.selectedEntities[selectedEntity.id];
	                }
	            });
	        }
	    };
	    Controller.prototype.scrubMetricData = function (metricSeries) {
	        switch (metricSeries.type) {
	            case series_1.Type.alert:
	            case series_1.Type.state:
	            case series_1.Type.event:
	                // Removing extra (non-plottable) data for bucket based series PS-757
	                var bisectDate = d3.bisector(function (d) { return d.date; }).left;
	                var bisectedIndex_1 = bisectDate(metricSeries.data, this.timeService.current().start());
	                metricSeries.data.splice(0, bisectedIndex_1);
	                _.forEach(metricSeries.subSeries, function (subSeries) {
	                    subSeries.data.splice(0, bisectedIndex_1);
	                });
	        }
	    };
	    Controller.prototype.refreshChartModels = function (clean) {
	        var _this = this;
	        if (clean === void 0) { clean = false; }
	        this.isRefreshingChartModels = true;
	        this.forwardBtnDisabled = true;
	        var promises = [];
	        _.forEach(this.charts, function (chartModel) {
	            _.forEach(chartModel.series, function (series) {
	                if (!_this.isFastPolling || !series.realTime) {
	                    var metricPromise = _this.metricService.getMetric([series.id]).then(function (metric) {
	                        var metricSeries = Metric.metricDtoToMetricSeries(metric);
	                        _this.scrubMetricData(metricSeries);
	                        if (!clean && series.type === series_1.Type.gauge && !(_.isEmpty(series.rawData) || _.isEmpty(metricSeries.rawData))) {
	                            // merge data instead of replace (i.e. keep real-time data)
	                            var bisectDate = d3.bisector(function (d) { return d.date; }).left;
	                            // if new data starts before current data prepend merge
	                            if (metricSeries.rawData[0].date.isBefore(series.rawData[0].date)) {
	                                var startIndex = bisectDate(metricSeries.rawData, series.rawData[0].date);
	                                metricSeries.rawData.splice(startIndex, metricSeries.rawData.length - startIndex);
	                                series.rawData.unshift.apply(series.rawData, metricSeries.rawData);
	                                var lastViewableIndex = bisectDate(series.rawData, _this.timeService.current().end());
	                                //
	                                lastViewableIndex = (lastViewableIndex < (series.rawData.length - 1)) ? lastViewableIndex + 1 : lastViewableIndex;
	                                series.rawData.splice(lastViewableIndex, series.rawData.length - lastViewableIndex);
	                            }
	                            else {
	                                var startIndex = bisectDate(metricSeries.rawData, series.rawData[series.rawData.length - 1].date);
	                                metricSeries.rawData.splice(0, startIndex);
	                                series.rawData.push.apply(series.rawData, metricSeries.rawData);
	                                series.rawData.splice(0, bisectDate(series.rawData, _this.timeService.getResultionAdjustedStartTime()));
	                            }
	                        }
	                        else {
	                            series.rawData = metricSeries.rawData;
	                        }
	                        if (!_.isFunction(series.transformer)) {
	                            series.data = series.rawData;
	                        }
	                        else {
	                            series.data = series.transformer(series.rawData, series.isPercentile);
	                        }
	                        // HACK: replacing subseries as before (not increasing scope of bug fix)
	                        series.subSeries = metricSeries.subSeries;
	                        // update last polled value
	                        series.lastCurrentData = metricSeries.lastCurrentData;
	                    });
	                    promises.push(metricPromise);
	                }
	            });
	        });
	        return this.$q.all(promises).then(function () {
	            _this.chartManager.redrawAll();
	            _this.updateAllCalculatedChartHeights();
	            _this.isRefreshingChartModels = false;
	            _this.restartPoller();
	        });
	    };
	    Controller.prototype.restartPoller = function () {
	        this.$interval.cancel(this.pollingInterval);
	        this.startPolling();
	    };
	    Controller.prototype.calcPollingInterval = function () {
	        var result = this.timeService.getResolution() / 3;
	        var min = 60; // 1 minute
	        var max = (60 * 60 * 4); // 4 hours
	        // Minimum 1 minute resultion, max 4 hours, otherwise 1/3 data resolution
	        result = (result < min) ? min : (result > max) ? max : result;
	        return result * 1000;
	    };
	    Controller.prototype.applyTimeQueryStringParams = function () {
	        var queryPresetTime = (this.$location.search().presetTime) ? this.$location.search().presetTime : null;
	        var queryEndTime = (this.$location.search().endTime) ? moment(this.$location.search().endTime) : undefined;
	        var queryStartTime = (this.$location.search().startTime) ? moment(this.$location.search().startTime) : undefined;
	        this.applyTimeUpdate(queryPresetTime, queryStartTime, queryEndTime);
	    };
	    Controller.prototype.startTimeUpdates = function () {
	        var _this = this;
	        var timeUpdateInterval = this.$interval(function () {
	            if (_this.timeService.isCurrentTimeframeWithin()) {
	                _this.$interval.cancel(timeUpdateInterval);
	                _this.timeService.updateToWindow(true);
	                _this.getForwardBtnDisabled();
	                _this.updateTimePickerTitle();
	                if (!_this.isRefreshingChartModels) {
	                    _this.chartManager.redrawAll();
	                }
	            }
	            _this.startTimeUpdates();
	        }, moment().add(1, 'minute').startOf('minute').diff(moment().subtract(1, 'seconds'), 'seconds') * 1000, 1);
	    };
	    Controller.prototype.startPolling = function () {
	        var _this = this;
	        this.pollingInterval = this.$interval(function () {
	            if (_this.timeService.isCurrentTimeframeWithin() && !_this.isRefreshingChartModels) {
	                // this.timeService.updateToWindow(true);
	                _this.refreshChartModels().finally(function () {
	                    _this.startPolling();
	                });
	                _this.getForwardBtnDisabled();
	                _this.updateTimePickerTitle();
	            }
	        }, this.calcPollingInterval(), 1);
	    };
	    Controller.prototype.startFastPolling = function (startTime, count) {
	        var _this = this;
	        if (_.isUndefined(startTime)) {
	            startTime = moment(this.timeService.current().start());
	        }
	        // startTime = startTime || moment(this.timeService.current().start());
	        this.fastPollingInterval = this.$timeout(function () {
	            if (_this.isFastPolling) {
	                _this.metricService.getRealTimeMetrics(_.keys(_this.inUseRealTimeMetrics), { startTime: startTime, count: count }).then(function (metricDtos) {
	                    var metricSeries = [];
	                    var isUserNotifiedOfGlobalRealtimeLimit = false;
	                    _.forEach(metricDtos, function (metric) {
	                        // Notify user of realtime global limit exception
	                        if (!isUserNotifiedOfGlobalRealtimeLimit && metric.isAfterRealtimeLimit) {
	                            isUserNotifiedOfGlobalRealtimeLimit = true;
	                            _this.toast.warning(_this._t('You have exceeded the recommended number of active real-time polling metrics across all accounts on the system.'), _this._t('Exceeded Recommended Global Real-Time Polling Metrics'));
	                        }
	                        metricSeries.push(Metric.metricDtoToMetricSeries(metric));
	                    });
	                    var bisectDate = d3.bisector(function (d) { return d.date; }).left;
	                    _.forEach(_this.charts, function (chartModel) {
	                        _.forEach(metricSeries, function (metric) {
	                            if (chartModel.series.hasOwnProperty(metric.id) && metric.data.length > 0) {
	                                // Slow poll data trumps Real-Time Polling data
	                                // let startIndex = bisectDate(metric.data, chartModel.series[metric.id].data[chartModel.series[metric.id].data.length-1].date);
	                                // metric.data.splice(0,startIndex);
	                                // chartModel.series[metric.id].data.push.apply(chartModel.series[metric.id].data, metric.data);
	                                // Merge realtime data with existing
	                                var series = chartModel.series[metric.id];
	                                var startIndex = bisectDate(series.rawData, metric.data[0].date);
	                                series.rawData.splice(startIndex, series.rawData.length - startIndex);
	                                series.rawData.push.apply(series.rawData, metric.data);
	                                if (metric.data.length) {
	                                    series.lastRealTimeUpdate = moment(_.last(metric.data).date);
	                                }
	                                // Remove trailing data elements (unviewable data points)
	                                var maxDataLength = _this.timeService.current().end().diff(_this.timeService.current().start()) / _this.fastPollTimeInterval;
	                                if (series.rawData.length > maxDataLength) {
	                                    series.rawData.splice(0, series.rawData.length - maxDataLength);
	                                }
	                                if (!_.isFunction(series.transformer)) {
	                                    series.data = series.rawData;
	                                }
	                                else {
	                                    series.data = series.transformer(series.rawData, series.isPercentile);
	                                }
	                            }
	                        });
	                    });
	                    // this.timeService.updateToWindow(true);
	                    _this.chartManager.redrawAll();
	                    // get last set of data points recorded relative to transaction delay of last request
	                    _this.startFastPolling(null, _.max([moment().diff(startTime, 'seconds'), 1]) * 2);
	                });
	                startTime = moment();
	            }
	        }, this.fastPollTimeInterval);
	    };
	    Controller.prototype.startFastUpdate = function () {
	        var _this = this;
	        this.fastUpdateInterval = this.$interval(function () {
	            _this.timeService.updateToWindow(true);
	            _this.chartManager.redrawAll();
	            _this.startFastUpdate();
	        }, 1000, 1, false);
	    };
	    Controller.prototype.applyTimeUpdate = function (queryPresetTime, queryStartTime, queryEndTime) {
	        // Update be preset
	        if (queryPresetTime !== null && this.presetTimeToWindowMap.hasOwnProperty(queryPresetTime)) {
	            this.timeService.window(this.presetTimeToWindowMap[queryPresetTime][0], this.presetTimeToWindowMap[queryPresetTime][1]);
	            this.timeService.updateToWindow(false);
	            this.timepickerDisplay.timePickerData.selectedPresetId = queryPresetTime;
	            this.timepickerDisplay.timePickerData.endDatetime = this.timeService.current().end().toDate();
	            this.timepickerDisplay.timePickerData.startDatetime = this.timeService.current().start().toDate();
	            this.updateTimePickerTitle();
	            // HACK: Need a better way of doing this; avoid setting a new chart to dirty (perhaps in watch)
	            if (_.isEmpty(this.project.id) && this.charts.length > 0) {
	                this.project.isDirty = true;
	            }
	        }
	        else if (queryStartTime !== undefined && queryEndTime !== undefined) {
	            this.timeService.current()
	                .start(queryStartTime)
	                .end(queryEndTime);
	            this.timepickerDisplay.timePickerData.selectedPresetId = queryPresetTime;
	            this.timepickerDisplay.timePickerData.endDatetime = this.timeService.current().end().toDate();
	            this.timepickerDisplay.timePickerData.startDatetime = this.timeService.current().start().toDate();
	            this.updateTimePickerTitle();
	            // HACK: Need a better way of doing this; avoid setting a new chart to dirty (perhaps in watch)
	            if (_.isEmpty(this.project.id) && this.charts.length > 0) {
	                this.project.isDirty = true;
	            }
	        }
	    };
	    Controller.prototype.initWatches = function () {
	        var _this = this;
	        this.$scope.$watch(function () { return _this.timeService.current(); }, function (newValue, oldValue) {
	            _this.updateProjectData();
	            _this.updateTimeQueryString();
	        }, true);
	    };
	    Controller.prototype.initChartsFromSerialization = function (serializeCharts, insertionIndex) {
	        var _this = this;
	        if (insertionIndex === void 0) { insertionIndex = 0; }
	        var promises = [];
	        _.forEach(serializeCharts, function (serializedMetricIds, index) {
	            if (serializedMetricIds !== '') {
	                var metricIds = serializedMetricIds.split(',').filter(function (d) { return d !== undefined && d !== ''; });
	                if (metricIds.length > 0) {
	                    promises.push(_this.createChart(metricIds, insertionIndex + index));
	                }
	            }
	        });
	        return this.$q.all(promises);
	    };
	    Controller.prototype.initChartsFromEntityTemplate = function (entityModel, chartIndex) {
	        if (chartIndex === void 0) { chartIndex = 0; }
	        if (entity_1.InstanceTypeTemplate[entityModel.instanceType]) {
	            var templateCharts = String.format(entity_1.InstanceTypeTemplate[entityModel.instanceType], entityModel.id);
	            this.initChartsFromSerialization(templateCharts.split(';'), chartIndex);
	        }
	        else {
	            var templateCharts = String.format(entity_1.InstanceTypeTemplate.default, entityModel.id);
	            this.initChartsFromSerialization(templateCharts.split(';'), chartIndex);
	        }
	    };
	    Controller.prototype.generateChartsFromContext = function () {
	        var _this = this;
	        var typeGroupings = {};
	        // group entities by type
	        this.entities.forEach(function (entity) {
	            typeGroupings[entity.instanceType] = (typeGroupings[entity.instanceType] || []).concat([entity]);
	        });
	        _.forEach(typeGroupings, function (entities, type) {
	            var projectTemplate = entity_1.InstanceTypeTemplate[type] || entity_1.InstanceTypeTemplate.default;
	            var chartTemplates = projectTemplate.split(';');
	            var chartsSerialized = [];
	            // merge all entities of a type to one template todo: should not allow more than a chart max metrics
	            chartTemplates.forEach(function (chartTemplate, index) {
	                chartsSerialized[index] = '';
	                entities.forEach(function (entity, entityIndex) {
	                    chartsSerialized[index] += String.format(chartTemplate, entity.id);
	                    if (entityIndex < entities.length - 1) {
	                        chartsSerialized[index] += ',';
	                    }
	                });
	            });
	            _this.initChartsFromSerialization(chartsSerialized, 0);
	        });
	    };
	    Controller.prototype.applyFastPollQueryStringParam = function (zoomNeeded) {
	        if (zoomNeeded === void 0) { zoomNeeded = false; }
	        var param = this.$location.search().activateFastPolling;
	        var activateFastPolling = (param) ? (param.toLowerCase() === 'true') : false;
	        if (activateFastPolling && !this.isFastPolling && _.keys(this.inUseRealTimeMetrics).length > 0) {
	            this.toggleFastPolling(zoomNeeded);
	        }
	    };
	    Controller.prototype.applyChartQueryStringParams = function () {
	        var _this = this;
	        var charts = (this.$location.search().charts) ? this.$location.search().charts.split(';') : [];
	        return this.initChartsFromSerialization(charts).finally(function () {
	            _this.buildEntityContext();
	            if (_.isEmpty(_this.project.id) && charts.length > 0) {
	                _this.project.isDirty = true;
	            }
	        });
	    };
	    Controller.prototype.resetFastPolling = function () {
	        this.stopFastPolling();
	        this.inUseRealTimeMetrics = {};
	    };
	    Controller.prototype.loadProject = function (id, resetState) {
	        var _this = this;
	        if (resetState === void 0) { resetState = true; }
	        if (this.project.isDirty && !this.$window.confirm(this._t('You have unsaved changes to the current project, are you sure you want to continue?'))) {
	            return;
	        }
	        this.resetFastPolling();
	        var deferred = this.$q.defer();
	        //PS-854 Clear and Selection on Project Load
	        this.clearInspectionSelection();
	        this.isBusy++;
	        this.projectService.getProjects([id]).then(function (result) {
	            _this.project = result.data[0];
	            if (resetState) {
	                _this.$state.go('.', { id: _this.project.id }, { notify: false });
	            }
	            _this.charts.length = 0;
	            if (_this.project.data && !_.isEmpty(_this.project.data.charts)) {
	                if (_this.$location.search().activateFastPolling && _this.$location.search().activateFastPolling.toLowerCase() === 'true') {
	                    var timeframe = _this.getFastPollTimeFrame();
	                    _this.applyTimeUpdate(null, timeframe.start(), timeframe.end());
	                }
	                else if ((!resetState && _this.$location.search().presetTime || (_this.$location.search().startTime && _this.$location.search().endTime)) !== undefined) {
	                    _this.applyTimeQueryStringParams();
	                }
	                else {
	                    var queryPresetTime = (_this.project.data.presetTime) ? _this.project.data.presetTime : null;
	                    var queryEndTime = (_this.project.data.endTime) ? moment(_this.project.data.endTime) : undefined;
	                    var queryStartTime = (_this.project.data.startTime) ? moment(_this.project.data.startTime) : undefined;
	                    _this.applyTimeUpdate(queryPresetTime, queryStartTime, queryEndTime);
	                }
	                _this.initChartsFromSerialization(_this.project.data.charts.split(';')).then(function () {
	                    _this.buildEntityContext();
	                }).finally(function () {
	                    // ga('send', {
	                    //   hitType: 'event',
	                    //   eventCategory: 'PerfStack', // hack: need to upgrade to typescript > 2.4 for string enum
	                    //   eventAction: 'load-project',
	                    //   eventLabel: 'Load project chart metrics',
	                    //   eventValue: this.charts.reduce((sum: number,chartModel: IChartModel) => {return sum + _.keys(chartModel.series).length}, 0)
	                    // });
	                    deferred.resolve();
	                });
	            }
	            // Collapse panel if needed
	            if (_this.charts.length > 0 && !_this.isMetricPaletteCollapsed) {
	                _this.toggleMetricPanel();
	            }
	            _this.refreshRecentProjects();
	        }, function (error) {
	            _this.toast.error(error.message + '<br/>' + error.exceptionMessage);
	        }).finally(function () {
	            _this.isBusy--;
	        });
	        return deferred.promise;
	    };
	    Controller.prototype.refreshRecentProjects = function () {
	        var _this = this;
	        this.projectService.getRecentProjects().then(function (result) {
	            // remove the current project from the quick list
	            result.data = _.filter(result.data, function (project) {
	                return (project.id !== _this.project.id);
	            });
	            _this.recentProjects = result.data.slice(0, 5);
	        }, function (error) {
	            _this.$log.error('refreshRecentProjects:', error);
	        });
	    };
	    Controller.prototype.toggleMetricPanel = function () {
	        var _this = this;
	        this.$timeout(function () {
	            _this.isMetricPaletteCollapsed = !_this.isMetricPaletteCollapsed;
	        }, 0, true);
	    };
	    Controller.prototype.onInspectToExplore = function () {
	        this.showInspectorPanel();
	        if (this.isFullScreen) {
	            this.toggleFullScreen();
	        }
	    };
	    Controller.prototype.showInspectorPanel = function () {
	        if (this.isMetricPaletteCollapsed) {
	            this.toggleMetricPanel();
	        }
	        this.leftPanelSelectedTab = 'data-explorer';
	    };
	    Controller.prototype.subscribeToInteractions = function () {
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.inspect, this.onInspectionInteraction, this));
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.inspectToExplore, this.onInspectToExplore, this));
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.inspectToZoom, this.zoom, this));
	    };
	    Controller.prototype.stopFastPolling = function () {
	        if (this.isFastPolling) {
	            this.toggleFastPolling();
	        }
	    };
	    Controller.prototype.zoom = function (direction, timeframe) {
	        var _this = this;
	        if (direction === interaction_service_1.ZoomDirection.In) {
	            var startAdjustment = this.timeService.current().start().diff(timeframe.start(), 'minutes');
	            var endAdjustment = this.timeService.current().end().diff(timeframe.end(), 'minutes');
	            this.timeService.current().start().subtract(startAdjustment, 'minutes');
	            this.timeService.current().end().subtract(endAdjustment, 'minutes');
	            this.clearInspectionSelection();
	        }
	        else {
	            this.stopFastPolling();
	            var currentDiff = this.timeService.current().end().diff(this.timeService.current().start(), 'minutes');
	            var inspectionDiff = timeframe.end().diff(timeframe.start(), 'minutes');
	            var diffLeft = timeframe.start().diff(this.timeService.current().start(), 'minutes');
	            var diffRight = this.timeService.current().end().diff(timeframe.end(), 'minutes');
	            var startAdjustment = inspectionDiff * ((diffLeft / currentDiff) - 1);
	            var endAdjustment = inspectionDiff * (1 - (diffRight / currentDiff));
	            this.timeService.current().start().add(startAdjustment, 'minutes');
	            this.timeService.current().end().add(endAdjustment, 'minutes');
	            // Don't zoom to the future (should probably be handled in time service)
	            if (this.timeService.current().end().isAfter(moment())) {
	                this.timeService.current().end().subtract(this.timeService.current().end().diff(moment()));
	            }
	        }
	        this.timepickerDisplay.timePickerData.selectedPresetId = null;
	        this.$timeout(function () {
	            _this.updateChartsToPickedTimeFrame();
	            _this.updateTimePickerTitle();
	        }, 0, true);
	    };
	    Controller.prototype.onInspectionInteraction = function (timeframe, config) {
	        if (config.metricIds && config.metricIds.length > 0) {
	            this.stopFastPolling();
	            this.leftPanelSelectedTab = 'data-explorer';
	        }
	        else {
	            this.leftPanelSelectedTab = 'metric-selector';
	        }
	    };
	    Controller.prototype.removeMetricFromInUseMetrics = function (seriesId) {
	        var seriesIdFound = [];
	        _.forEach(this.charts, function (chart) {
	            _.forEach(chart.series, function (series) {
	                if (series.id === seriesId) {
	                    seriesIdFound.push(series.id);
	                }
	            });
	        });
	        //Remove only id last inUseMetrics
	        if (seriesIdFound.length === 1) {
	            delete this.inUseMetrics[seriesId];
	        }
	    };
	    return Controller;
	}());
	Controller.$inject = ['$log', '$q', '$timeout', '$interval', '$scope', '$rootScope', 'getTextService', '$state', '$stateParams',
	    '$location', 'xuiToastService', 'xuiTimeframeService', 'metricService', 'entityService', 'utilityService',
	    'timeService', 'chartManager', 'xuiDialogService', 'projectService', '$window', 'interactionService', 'resolvedSettings', 'swUtil', 'userSettings',
	    'analyticsService', 'helpLinkService'];
	exports.default = Controller;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var series_1 = __webpack_require__(6);
	var Mark = __webpack_require__(7);
	function toSeriesColorClass(metric) {
	    if (_.isEmpty(metric.colorType) && metric.type === series_1.Type.event) {
	        return 'ps-color-1';
	    }
	    return (metric.colorType) ? 'ps-color-' + metric.colorType : '';
	}
	function metricDtoToMetricSeries(metric) {
	    // scrub entity ancestry: remove self
	    if (metric.ancestorDisplayNames) {
	        metric.ancestorDisplayNames.pop();
	    }
	    var result = {
	        id: metric.id,
	        metricId: metric.metricId,
	        entityId: metric.entityId,
	        type: metric.type,
	        displayName: metric.displayName,
	        entityDisplayName: metric.entityDisplayName,
	        ancestorDisplayNames: metric.ancestorDisplayNames,
	        sourceDisplayName: metric.entityDisplayName,
	        colorClass: toSeriesColorClass(metric),
	        isPercentile: (metric.units === '%'),
	        unitName: metric.units,
	        data: metricDataToITimeSeriesData(metric.measurements),
	        subSeries: _.map(metric.subMetrics, function (subMetric) {
	            return metricDtoToMetricSeries(subMetric);
	        }),
	        realTime: metric.realTime,
	        lastRealTimeUpdate: null,
	        lastCurrentData: metric.latestDetailMeasurement,
	        isAfterRealtimeLimit: metric.isAfterRealtimeLimit
	    };
	    result.rawData = result.data;
	    // Pick the mark/renderer (charts shouldn't have this logic)
	    if (metric.displayType) {
	        result.markType = metric.displayType;
	    }
	    else {
	        switch (metric.type) {
	            case series_1.Type.event:
	                if (!_.isEmpty(metric.subMetrics)) {
	                    result.markType = Mark.Type.barStacked;
	                }
	                else {
	                    result.markType = Mark.Type.bar;
	                }
	                break;
	            case series_1.Type.state:
	                result.markType = Mark.Type.state;
	                break;
	            case series_1.Type.alert:
	                result.markType = Mark.Type.stateStacked;
	                break;
	            case series_1.Type.multi:
	                result.markType = Mark.Type.areaStacked;
	                break;
	            default:
	            case series_1.Type.gauge:
	                result.markType = Mark.Type.line;
	                break;
	        }
	    }
	    return result;
	}
	exports.metricDtoToMetricSeries = metricDtoToMetricSeries;
	function metricDataToITimeSeriesData(data) {
	    return _.map(data, function (d) {
	        return {
	            date: moment(d.dateTimeStamp),
	            value: d.value,
	            resolution: d.resolution
	        };
	    });
	}
	exports.metricDataToITimeSeriesData = metricDataToITimeSeriesData;
	function observationDtoToData(data) {
	    return _.map(data, function (d) {
	        return {
	            date: moment(d.dateTimeStamp),
	            value: d.value,
	            metricId: d.metricId,
	            propertyBag: d.propertyBag
	        };
	    });
	}
	exports.observationDtoToData = observationDtoToData;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Type;
	(function (Type) {
	    Type[Type["undefined"] = 0] = "undefined";
	    Type[Type["gauge"] = 1] = "gauge";
	    Type[Type["counter"] = 2] = "counter";
	    Type[Type["event"] = 3] = "event";
	    Type[Type["alert"] = 4] = "alert";
	    Type[Type["state"] = 5] = "state";
	    Type[Type["multi"] = 6] = "multi";
	    Type[Type["dpaWaitTime"] = 7] = "dpaWaitTime";
	})(Type = exports.Type || (exports.Type = {}));
	var Series = (function () {
	    function Series(series) {
	        this.id = null;
	        this.displayName = null;
	        this.sourceDisplayName = null;
	        this.data = null;
	        this.lastCurrentData = null;
	        this.type = null;
	        this.isPercentile = null;
	        this.colorClass = null;
	        this.unitName = null;
	        this.subSeries = null;
	        for (var key in series) {
	            if (this.hasOwnProperty(key)) {
	                this[key] = series[key];
	            }
	        }
	    }
	    return Series;
	}());
	exports.Series = Series;


/***/ }),
/* 7 */
/***/ (function(module, exports) {

	/// <reference path='../../../../ref.d.ts' />
	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Type;
	(function (Type) {
	    Type[Type["undefined"] = 0] = "undefined";
	    Type[Type["line"] = 1] = "line";
	    Type[Type["bar"] = 2] = "bar";
	    Type[Type["barStacked"] = 3] = "barStacked";
	    Type[Type["area"] = 4] = "area";
	    Type[Type["areaStacked"] = 5] = "areaStacked";
	    Type[Type["state"] = 6] = "state";
	    Type[Type["stateStacked"] = 7] = "stateStacked";
	})(Type = exports.Type || (exports.Type = {}));
	var Mark = (function () {
	    function Mark() {
	    }
	    Mark.prototype.draw = function (series, domain) {
	        this.updateMark(series, domain);
	        return this;
	    };
	    Mark.prototype.target = function (group) {
	        if (group === undefined) {
	            return this._target;
	        }
	        this._target = group;
	        return this;
	    };
	    Mark.prototype.class = function (className) {
	        if (className === undefined) {
	            return this._className;
	        }
	        this._className = className;
	        return this;
	    };
	    Mark.prototype.highlightData = function (display, data, dataIndex, series, domain) {
	        return this;
	    };
	    Mark.prototype.destroy = function () {
	        // Stub
	    };
	    Mark.prototype.updateMark = function (series, domain) {
	        // Stub
	    };
	    return Mark;
	}());
	exports.default = Mark;


/***/ }),
/* 8 */
/***/ (function(module, exports) {

	/// <reference path='../../../ref.d.ts' />
	"use strict";
	PsDraggable.$inject = ["$log", "$rootScope"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var EventType = (function () {
	    function EventType() {
	    }
	    return EventType;
	}());
	EventType.DragStart = 'perfstack.components.dragDrop.dragStart';
	EventType.DragEnd = 'perfstack.components.dragDrop.dragEnd';
	exports.EventType = EventType;
	PsDraggable.prototype.$inject = ['$log', '$rootScope'];
	function PsDraggable($log, $rootScope) {
	    return {
	        restrict: 'A',
	        scope: {
	            draggableData: '='
	        },
	        link: link
	    };
	    function link(scope, element) {
	        angular.element(element).attr('draggable', 'true');
	        element.bind('dragstart', function (e) {
	            element.addClass('ps-dragging');
	            e.originalEvent.dataTransfer.setData('text', angular.toJson(scope.draggableData));
	            $rootScope.$apply(function () {
	                $rootScope.$emit(EventType.DragStart, scope.draggableData);
	            });
	        });
	        element.bind('dragend', function (e) {
	            element.removeClass('ps-dragging');
	            $rootScope.$apply(function () {
	                $rootScope.$emit(EventType.DragEnd, scope.draggableData);
	            });
	        });
	        scope.$on('$destroy', function () {
	            element.off(['dragstart', 'dragend']);
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.dragDrop.directives', [])
	    .directive('psDraggable', PsDraggable)
	    .name;


/***/ }),
/* 9 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Status;
	(function (Status) {
	    Status[Status["down"] = 0] = "down";
	    Status[Status["up"] = 1] = "up";
	    Status[Status["warning"] = 2] = "warning";
	})(Status = exports.Status || (exports.Status = {}));
	exports.InstanceTypeTemplate = {
	    'Orion.Nodes': '{0}-Orion.CPULoad.AvgLoad,{0}-Orion.CPULoad.AvgPercentMemoryUsed;{0}-Orion.ResponseTime.AvgResponseTime;{0}-Orion.LoadAverage.LoadAverage1Avg,{0}-Orion.LoadAverage.LoadAverage5Avg,{0}-Orion.LoadAverage.LoadAverage15Avg;{0}-Orion.PerfStack.Alerts;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Status;',
	    'Orion.Groups': '{0}-Orion.ContainerStatus.Status;{0}-Orion.ContainerStatus.PercentAvailability;{0}-Orion.ContainerStatus.PercentMembersAvailability;',
	    'Orion.NPM.Interfaces': '{0}-Orion.PerfStack.Status;{0}-Orion.PerfStack.Alerts;{0}-Orion.NPM.InterfaceTraffic.InPercentUtil,{0}-Orion.NPM.InterfaceTraffic.OutPercentUtil;{0}-Orion.NPM.InterfaceTraffic.InAveragebps,{0}-Orion.NPM.InterfaceTraffic.OutAveragebps;{0}-Orion.NPM.InterfaceErrors.InDiscards,{0}-Orion.NPM.InterfaceErrors.InErrors;{0}-Orion.NPM.InterfaceErrors.OutDiscards,{0}-Orion.NPM.InterfaceErrors.OutErrors;{0}-Orion.PerfStack.Events;',
	    'Orion.IpSla.Operations': '{0}-Orion.IpSla.PathHopOperationCurrentStats.HopIpAddressV4;{0}-Orion.IpSla.PathHopOperationCurrentStats.Jitter;{0}-Orion.IpSla.PathHopOperationCurrentStats.Latency;{0}-Orion.IpSla.PathHopOperationCurrentStats.PacketLoss;{0}-Orion.IpSla.PathHopOperationCurrentStats.RoundTripTime;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.VIM.Clusters': '{0}-Orion.VIM.ClusterStatistics.AvgCPULoad,{0}-Orion.VIM.ClusterStatistics.AvgMemoryUsage;{0}-Orion.VIM.ClusterStatistics.AvgCPUUsageMHz;{0}-Orion.VIM.ClusterStatistics.AvgMemoryUsageMB;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.VIM.Datastores': '{0}-Orion.VIM.DatastoreStatistics.AvgIOPSTotal,{0}-Orion.VIM.DatastoreStatistics.AvgIOPSRead,{0}-Orion.VIM.DatastoreStatistics.AvgIOPSWrite;{0}-Orion.VIM.DatastoreStatistics.AvgLatencyTotal,{0}-Orion.VIM.DatastoreStatistics.AvgLatencyRead,{0}-Orion.VIM.DatastoreStatistics.AvgLatencyWrite;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.VIM.Hosts': '{0}-Orion.VIM.HostStatistics.AvgNetworkUsageRate,{0}-Orion.VIM.HostStatistics.AvgNetworkTransmitRate,{0}-Orion.VIM.HostStatistics.AvgNetworkReceiveRate;{0}-Orion.VIM.HostStatistics.AvgCpuLoad,{0}-Orion.VIM.HostStatistics.AvgMemUsage;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.VIM.VirtualMachines': '{0}-Orion.VIM.VMStatistics.AvgCPUUsageMHz,{0}-Orion.VIM.VMStatistics.AvgMemoryUsage;{0}-Orion.VIM.VMStatistics.AvgIOPSRead,{0}-Orion.VIM.VMStatistics.AvgIOPSWrite,{0}-Orion.VIM.VMStatistics.AvgIOPSTotal;{0}-Orion.VIM.VMStatistics.AvgLatencyRead,{0}-Orion.VIM.VMStatistics.AvgLatencyWrite,{0}-Orion.VIM.VMStatistics.AvgLatencyTotal;{0}-Orion.VIM.VMStatistics.AvgNetworkTransmitRate,{0}-Orion.VIM.VMStatistics.AvgNetworkReceiveRate,{0}-Orion.VIM.VMStatistics.AvgNetworkUsageRate;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.SRM.LUNs': '{0}-Orion.SRM.LUNStatistics.IOPSRead,{0}-Orion.SRM.LUNStatistics.IOPSWrite,{0}-Orion.SRM.LUNStatistics.IOPSOther;{0}-Orion.SRM.LUNStatistics.BytesRead,{0}-Orion.SRM.LUNStatistics.BytesWrite;{0}-Orion.SRM.LUNStatistics.IOLatencyRead,{0}-Orion.SRM.LUNStatistics.IOLatencyWrite,{0}-Orion.SRM.LUNStatistics.IOLatencyOther;{0}-Orion.SRM.LUNStatistics.QueueLength;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.SRM.Pools': '{0}-Orion.SRM.PoolStatistics.IOPSRead,{0}-Orion.SRM.PoolStatistics.IOPSWrite,{0}-Orion.SRM.PoolStatistics.IOPSOther;{0}-Orion.SRM.PoolStatistics.BytesRead,{0}-Orion.SRM.PoolStatistics.BytesWrite;{0}-Orion.SRM.PoolStatistics.IOLatencyRead,{0}-Orion.SRM.PoolStatistics.IOLatencyWrite,{0}-Orion.SRM.PoolStatistics.IOLatencyOther;{0}-Orion.SRM.PoolStatistics.CacheHitRatio;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.SRM.StorageArrays': '{0}-Orion.SRM.StorageArrayStatistics.IOPSRead,{0}-Orion.SRM.StorageArrayStatistics.IOPSWrite,{0}-Orion.SRM.StorageArrayStatistics.IOPSOther;{0}-Orion.SRM.StorageArrayStatistics.BytesRead,{0}-Orion.SRM.StorageArrayStatistics.BytesWrite;{0}-Orion.SRM.StorageArrayStatistics.CacheHitRatio;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.SRM.Volumes': '{0}-Orion.SRM.VolumeStatistics.IOPSRead,{0}-Orion.SRM.VolumeStatistics.IOPSWrite,{0}-Orion.SRM.VolumeStatistics.IOPSOther;{0}-Orion.SRM.VolumeStatistics.BytesRead,{0}-Orion.SRM.VolumeStatistics.BytesWrite;{0}-Orion.SRM.VolumeStatistics.IOLatencyRead,{0}-Orion.SRM.VolumeStatistics.IOLatencyWrite,{0}-Orion.SRM.VolumeStatistics.IOLatencyOther;{0}-Orion.SRM.VolumeStatistics.CacheHitRatio,{0}-Orion.SRM.VolumeStatistics.QueueLength;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.SRM.VServers': '{0}-Orion.SRM.VServerStatistics.IOPSRead,{0}-Orion.SRM.VServerStatistics.IOPSWrite,{0}-Orion.SRM.VServerStatistics.IOPSOther;{0}-Orion.SRM.VServerStatistics.BytesRead,{0}-Orion.SRM.VServerStatistics.BytesWrite;{0}-Orion.SRM.VServerStatistics.CacheHitRatio;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.Cloud.Aws.Instances': '{0}-Orion.Cloud.Aws.InstanceStatistics.AvgCPULoad;{0}-Orion.Cloud.Aws.InstanceStatistics.AvgIOPSTotal,{0}-Orion.Cloud.Aws.InstanceStatistics.AvgIOPSRead,{0}-Orion.Cloud.Aws.InstanceStatistics.AvgIOPSWrite;{0}-Orion.Cloud.Aws.InstanceStatistics.AvgDiskReadInBytesPerSecond,{0}-Orion.Cloud.Aws.InstanceStatistics.AvgDiskWriteInBytesPerSecond;{0}-Orion.Cloud.Aws.InstanceStatistics.AvgNetworkUsageRate,{0}-Orion.Cloud.Aws.InstanceStatistics.AvgNetworkTransmitRate,{0}-Orion.Cloud.Aws.InstanceStatistics.AvgNetworkReceiveRate;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.Cloud.Azure.Instances': '{0}-Orion.Cloud.Azure.InstanceStatistics.AvgCPULoad;{0}-Orion.Cloud.Azure.InstanceStatistics.AvgIOPSTotal,{0}-Orion.Cloud.Azure.InstanceStatistics.AvgIOPSRead,{0}-Orion.Cloud.Azure.InstanceStatistics.AvgIOPSWrite;{0}-Orion.Cloud.Azure.InstanceStatistics.AvgDiskReadInBytesPerSecond,{0}-Orion.Cloud.Azure.InstanceStatistics.AvgDiskWriteInBytesPerSecond;{0}-Orion.Cloud.Azure.InstanceStatistics.AvgNetworkUsageRate,{0}-Orion.Cloud.Azure.InstanceStatistics.AvgNetworkTransmitRate,{0}-Orion.Cloud.Azure.InstanceStatistics.AvgNetworkReceiveRate;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.APM.GenericApplication': '{0}-Orion.APM.ApplicationStatus.PercentAvailability;{0}-Orion.APM.ApplicationStatus.Availability;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.APM.Exchange.Application': '{0}-Orion.APM.ApplicationStatus.PercentAvailability;{0}-Orion.APM.ApplicationStatus.Availability;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.APM.IIS.Application': '{0}-Orion.APM.ApplicationStatus.PercentAvailability;{0}-Orion.APM.ApplicationStatus.Availability;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.APM.SqlServerApplication': '{0}-Orion.APM.ApplicationStatus.PercentAvailability;{0}-Orion.APM.ApplicationStatus.Availability;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.APM.ActiveDirectory.Application': '{0}-Orion.APM.ApplicationStatus.PercentAvailability;{0}-Orion.APM.ApplicationStatus.Availability;{0}-Orion.PerfStack.Events;{0}-Orion.PerfStack.Alerts;',
	    'Orion.Volumes': '{0}-Orion.VolumePerformanceHistory.AvgDiskQueueLength;{0}-Orion.VolumePerformanceHistory.AvgDiskReads,{0}-Orion.VolumePerformanceHistory.AvgDiskWrites;{0}-Orion.VolumePerformanceHistory.AvgDiskTransfer;{0}-Orion.VolumeUsageHistory.AvgDiskUsed,{0}-Orion.VolumeUsageHistory.PercentDiskUsed;{0}-Orion.PerfStack.Status;',
	    'Orion.DPA.DatabaseInstance': '{0}-DPA.TimeSeriesData.DatabaseWaitTime.TopWaittime;{0}-DPA.TimeSeriesData.CPU.CPUUtilization,{0}-DPA.TimeSeriesData.Memory.MemoryUtilization,{0}-DPA.TimeSeriesData.DTU.SQLDBPercentMaxDTU,{0}-DPA.TimeSeriesData.CPU.SQLDBCPUPercent,{0}-DPA.TimeSeriesData.Disk.SQLDBDataIOPercent,{0}-DPA.TimeSeriesData.Memory.BufferCacheHitRatio,{0}-DPA.TimeSeriesData.Memory.BufferPoolHitRatio,{0}-DPA.TimeSeriesData.CPU.CPUUtilizationByDatabase,{0}-DPA.TimeSeriesData.Memory.ProcedureCacheHitRatio;{0}-DPA.TimeSeriesData.Memory.PageLifeExpectancy;{0}-DPA.TimeSeriesData.Network.RoundtripTimems,{0}-DPA.TimeSeriesData.Statements.Questions,{0}-DPA.TimeSeriesData.Disk.DiskReadTimeCommits;{0}-DPA.TimeSeriesData.Disk.SQLDiskWriteLatency,{0}-DPA.TimeSeriesData.Disk.SQLDiskReadLatency,{0}-DPA.TimeSeriesData.Disk.InnoDBDataReads,{0}-DPA.TimeSeriesData.Disk.InnoDBDataWrites,{0}-DPA.TimeSeriesData.Disk.DiskReadTimeSingleBlock,{0}-DPA.TimeSeriesData.Disk.DiskReadTimeMultiBlock,{0}-DPA.TimeSeriesData.Disk.PhysicalIORate;',
	    'Cortex.Orion.PowerControlUnit': '{0}-Cortex.Orion.PowerControlUnit.Metrics.AvgBatteryCapacity,{0}-Cortex.Orion.PowerControlUnit.Metrics.AvgBatteryTemperature,{0}-Cortex.Orion.PowerControlUnit.Metrics.AvgOutputPercentLoad;',
	    'Orion.APIPoller.ValueToMonitor': '{0}-Orion.APIPoller.ValueToMonitor.Metrics.Status;{0}-Orion.APIPoller.ValueToMonitor.Metrics.AvgMetric;',
	    'default': '{0}-Orion.PerfStack.Status;'
	};
	var InstanceType = (function () {
	    function InstanceType() {
	    }
	    return InstanceType;
	}());
	InstanceType.CortexOrionPowerControlUnit = 'Cortex.Orion.PowerControlUnit';
	InstanceType.OrionApiPollerApiPoller = 'Orion.APIPoller.ApiPoller';
	InstanceType.OrionApiPollerValueToMonitor = 'Orion.APIPoller.ValueToMonitor';
	InstanceType.OrionHardwareHealthHardwareItem = 'Orion.HardwareHealth.HardwareItem';
	InstanceType.OrionGroups = 'Orion.Groups';
	InstanceType.OrionNodes = 'Orion.Nodes';
	InstanceType.OrionAPMApplication = 'Orion.APM.Application';
	InstanceType.OrionSEUMTransactions = 'Orion.SEUM.Transactions';
	InstanceType.OrionSEUMTransactionSteps = 'Orion.SEUM.TransactionSteps';
	InstanceType.OrionVIMVirtualMachines = 'Orion.VIM.VirtualMachines';
	InstanceType.OrionNodesOrionVIMVirtualMachines = 'Orion.Nodes+Orion.VIM.VirtualMachines';
	InstanceType.OrionNodesOrionVIMHosts = 'Orion.Nodes+Orion.VIM.Hosts';
	InstanceType.OrionNodesOrionVIMVCenters = 'Orion.Nodes+Orion.VIM.VCenters';
	InstanceType.OrionNodesOrionAPMApplication = 'Orion.Nodes+Orion.APM.Application';
	InstanceType.OrionVIMHosts = 'Orion.VIM.Hosts';
	InstanceType.OrionVIMClusters = 'Orion.VIM.Clusters';
	InstanceType.OrionVIMDataCenters = 'Orion.VIM.DataCenters';
	InstanceType.OrionVIMVCenters = 'Orion.VIM.VCenters';
	InstanceType.OrionVIMDatastores = 'Orion.VIM.Datastores';
	InstanceType.OrionSRMLUNs = 'Orion.SRM.LUNs';
	InstanceType.OrionSRMVolumes = 'Orion.SRM.Volumes';
	InstanceType.OrionSRMPools = 'Orion.SRM.Pools';
	InstanceType.OrionSRMVServers = 'Orion.SRM.VServers';
	InstanceType.OrionSRMStorageArrays = 'Orion.SRM.StorageArrays';
	InstanceType.OrionVolumes = 'Orion.Volumes';
	InstanceType.OrionDPADatabaseInstance = 'Orion.DPA.DatabaseInstance';
	InstanceType.OrionAPMGenericApplication = 'Orion.APM.GenericApplication';
	InstanceType.OrionAPMIISApplication = 'Orion.APM.IIS.Application';
	InstanceType.OrionAPMIISApplicationPool = 'Orion.APM.IIS.ApplicationPool';
	InstanceType.OrionAPMIISSite = 'Orion.APM.IIS.Site';
	InstanceType.OrionAPMSqlDatabase = 'Orion.APM.SqlDatabase';
	InstanceType.OrionAPMSqlServerApplication = 'Orion.APM.SqlServerApplication';
	InstanceType.OrionDPADpaServer = 'Orion.DPA.DpaServer';
	InstanceType.OrionDPIApplications = 'Orion.DPI.Applications';
	InstanceType.OrionHardwareHealthHardwareCategoryStatus = 'Orion.HardwareHealth.HardwareCategoryStatus';
	InstanceType.OrionHardwareHealthHardwareInfo = 'Orion.HardwareHealth.HardwareInfo';
	InstanceType.OrionNPMInterfaces = 'Orion.NPM.Interfaces';
	InstanceType.OrionNetPathEndpointServices = 'Orion.NetPath.EndpointServices';
	exports.InstanceType = InstanceType;
	function scrubEnityAncestorDisplayNames(entities) {
	    _.forEach(entities, function (entity) {
	        // Remove self from ancestry list
	        if (entity.ancestorDisplayNames) {
	            entity.ancestorDisplayNames.pop();
	        }
	    });
	}
	exports.scrubEnityAncestorDisplayNames = scrubEnityAncestorDisplayNames;


/***/ }),
/* 10 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function convertFromDTO(rawProject) {
	    rawProject.data = angular.fromJson(rawProject.data);
	    rawProject.updateDateTime = moment(rawProject.updateDateTime);
	    rawProject.createDateTime = moment(rawProject.createDateTime);
	    rawProject.isDirty = false;
	    return rawProject;
	}
	exports.convertFromDTO = convertFromDTO;
	function convertToDTO(project) {
	    var resultDTO = angular.copy(project);
	    delete resultDTO.isDirty;
	    resultDTO.data = angular.toJson(project.data);
	    resultDTO.updateDateTime = project.updateDateTime.utc().toISOString();
	    resultDTO.createDateTime = project.createDateTime.utc().toISOString();
	    return resultDTO;
	}
	exports.convertToDTO = convertToDTO;
	var Project = (function () {
	    function Project(dto) {
	        if (dto) {
	            var converted = convertFromDTO(dto);
	            for (var key in converted) {
	                if (converted.hasOwnProperty(key)) {
	                    this[key] = converted[key];
	                }
	            }
	        }
	    }
	    return Project;
	}());
	exports.Project = Project;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var pubsub_1 = __webpack_require__(12);
	exports.InteractionMessages = {
	    time: 'InteractionMessages-time',
	    seriesPassive: 'InteractionMessages-seriesPassive',
	    inspect: 'InteractionMessages-inspect',
	    inspectToExplore: 'InteractionMessages-inspectToExplore',
	    inspectToZoom: 'InteractionMessages-inspectToZoom',
	    inspectDisplayUpdate: 'InteractionMessages-displayUpdate'
	};
	var ZoomDirection;
	(function (ZoomDirection) {
	    ZoomDirection[ZoomDirection["In"] = 0] = "In";
	    ZoomDirection[ZoomDirection["Out"] = 1] = "Out";
	})(ZoomDirection = exports.ZoomDirection || (exports.ZoomDirection = {}));
	var InteractionService = (function (_super) {
	    __extends(InteractionService, _super);
	    function InteractionService() {
	        var _this = this;
	        if (InteractionService._instance) {
	            throw new Error("Error: InteractionService already instantiated.");
	        }
	        _this = _super.call(this) || this;
	        InteractionService._instance = _this;
	        return _this;
	    }
	    InteractionService.getInstance = function () {
	        if (InteractionService._instance) {
	            return InteractionService._instance;
	        }
	        else {
	            return new InteractionService();
	        }
	    };
	    InteractionService.prototype.time = function (value, data) {
	        var other = [];
	        for (var _i = 2; _i < arguments.length; _i++) {
	            other[_i - 2] = arguments[_i];
	        }
	        if (value === undefined) {
	            return this._time;
	        }
	        this._time = value;
	        this.publish(exports.InteractionMessages.time, value, data, other);
	        return this;
	    };
	    InteractionService.prototype.seriesPassive = function (seriesId, seriesIndex) {
	        var other = [];
	        for (var _i = 2; _i < arguments.length; _i++) {
	            other[_i - 2] = arguments[_i];
	        }
	        this.publish(exports.InteractionMessages.seriesPassive + seriesId, seriesId, seriesIndex, other);
	        return this;
	    };
	    InteractionService.prototype.inspect = function (timeframe, config) {
	        var other = [];
	        for (var _i = 2; _i < arguments.length; _i++) {
	            other[_i - 2] = arguments[_i];
	        }
	        if (timeframe === undefined) {
	            return this._inspectionTimeframe;
	        }
	        this._inspectionTimeframe = timeframe;
	        this.publish(exports.InteractionMessages.inspect, timeframe, config, other);
	        return this;
	    };
	    InteractionService.prototype.inspectToExplore = function (metricIds, config) {
	        var other = [];
	        for (var _i = 2; _i < arguments.length; _i++) {
	            other[_i - 2] = arguments[_i];
	        }
	        // this.publish.apply(this, [InteractionMessages.inspectToExplore, metricIds].concat(other));
	        this.publish(exports.InteractionMessages.inspectToExplore, metricIds, config, other);
	        return this;
	    };
	    InteractionService.prototype.inspectToZoom = function (direction, timeframe) {
	        var other = [];
	        for (var _i = 2; _i < arguments.length; _i++) {
	            other[_i - 2] = arguments[_i];
	        }
	        this.publish.apply(this, [exports.InteractionMessages.inspectToZoom, direction, timeframe].concat(other));
	        return this;
	    };
	    InteractionService.prototype.inspectDisplayUpdate = function (inspectionArea, config) {
	        var other = [];
	        for (var _i = 2; _i < arguments.length; _i++) {
	            other[_i - 2] = arguments[_i];
	        }
	        this.publish(exports.InteractionMessages.inspectDisplayUpdate, inspectionArea, config, other);
	        return this;
	    };
	    return InteractionService;
	}(pubsub_1.PubSub));
	exports.InteractionService = InteractionService;
	exports.default = angular.module('perfstack.interaction.Service', [])
	    .factory('interactionService', function () { return InteractionService.getInstance(); })
	    .name;


/***/ }),
/* 12 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var PubSub = (function () {
	    function PubSub() {
	        // constructor
	        this._subscriptions = {};
	    }
	    PubSub.prototype.publish = function (messageKey, value, data) {
	        var other = [];
	        for (var _i = 3; _i < arguments.length; _i++) {
	            other[_i - 3] = arguments[_i];
	        }
	        _.forEach(this._subscriptions[messageKey], function (subscription) {
	            subscription.callback.apply(subscription.scope, [value, data, other]); //TODO: should this be async?
	        });
	    };
	    ;
	    PubSub.prototype.subscribe = function (messageId, callback, scope) {
	        if (typeof callback !== "function") {
	            return null;
	        }
	        var id = _.uniqueId('pubsub');
	        if (!this._subscriptions[messageId]) {
	            this._subscriptions[messageId] = {};
	        }
	        this._subscriptions[messageId][id] = { callback: callback, scope: scope };
	        return id;
	    };
	    ;
	    PubSub.prototype.unsubscribe = function (subscriptionId) {
	        _.forEach(this._subscriptions, function (messageSubs) {
	            delete messageSubs[subscriptionId];
	        });
	    };
	    ;
	    return PubSub;
	}());
	exports.PubSub = PubSub;


/***/ }),
/* 13 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Timeframe = (function () {
	    function Timeframe() {
	    }
	    Timeframe.prototype.start = function (value) {
	        if (value === undefined) {
	            return this._start;
	        }
	        this._start = value;
	        return this;
	    };
	    Timeframe.prototype.end = function (value) {
	        if (value === undefined) {
	            return this._end;
	        }
	        this._end = value;
	        return this;
	    };
	    return Timeframe;
	}());
	exports.Timeframe = Timeframe;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../../ref.d.ts" />
	"use strict";
	PsChart.$inject = ["$log", "$window", "$timeout", "timeService", "interactionService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var Chart = __webpack_require__(15);
	var time_service_1 = __webpack_require__(38);
	var chart_config_1 = __webpack_require__(37);
	var interaction_service_1 = __webpack_require__(11);
	PsChart.prototype.$inject = ['$log', '$window', '$timeout', 'timeService', 'interactionService'];
	function PsChart($log, $window, $timeout, timeService, interactionService) {
	    return {
	        restrict: 'E',
	        scope: {
	            id: '@',
	            model: '=',
	            manager: '=?'
	        },
	        link: link
	    };
	    function link(scope, element) {
	        var chartConfig = new chart_config_1.default();
	        chartConfig.time.timeframe(timeService.current());
	        var chart = new Chart.Chart()
	            .id(scope.id)
	            .config(chartConfig)
	            .target(element[0])
	            .render();
	        chart.series(scope.model.series)
	            .draw();
	        if (scope.manager) {
	            scope.manager.register(chart, scope.id);
	        }
	        var onResize = function () {
	            chart.draw(); // HACK: Need to debounce
	        };
	        angular.element($window).on('resize', onResize);
	        scope.$on('$destroy', function () {
	            angular.element($window).off('resize', onResize);
	            chart.destroy();
	            if (scope.manager) {
	                scope.manager.deregister(scope.id);
	            }
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.chart.directives', [time_service_1.default, interaction_service_1.default])
	    .directive('psChart', PsChart)
	    .name;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	/// <reference path='../../../ref.d.ts' />
	var plot_timeseries_1 = __webpack_require__(16);
	var Series = __webpack_require__(6);
	var plot_config_1 = __webpack_require__(24);
	var tick_config_1 = __webpack_require__(23);
	var layout_1 = __webpack_require__(35);
	var chart_config_1 = __webpack_require__(37);
	var interaction_service_1 = __webpack_require__(11);
	var Chart = (function () {
	    function Chart() {
	        var _this = this;
	        this._plotDictionary = {};
	        this._seriesDictionary = {};
	        this._isRendered = false;
	        this._gaugePlotId = null;
	        this._gaugePlots = [];
	        this._multiPlots = [];
	        this._statePlots = [];
	        this._waitingForUpdateFrame = false;
	        this._mouseState = {
	            coords: [0, 0],
	            isInteracting: false
	        };
	        this._dimension = {
	            width: 0,
	            height: 0,
	            padding: {
	                base: 0,
	                top: 5,
	                right: 35,
	                bottom: 20,
	                left: 35
	            }
	        };
	        this.onMouseMove = function () {
	            _this.updateMouseState();
	            // Debounce interaction
	            _this.requestInteractionUpdate();
	        };
	        this.onMouseClick = function () {
	            _this.updateMouseState();
	            // Drag end handles click interaction
	            // this.updateClickInteraction();
	        };
	        this.onMouseLeave = function () {
	            _this._mouseState.isInteracting = false;
	            _this._interactionPubSub.time(null);
	        };
	        this._id = _.uniqueId('chart-');
	        this._config = new chart_config_1.default();
	        this._layout = new layout_1.default();
	        this._interactionSubIds = [];
	        this.interactionPubSub(interaction_service_1.InteractionService.getInstance());
	    }
	    Chart.prototype.render = function () {
	        this.buildStage();
	        this.updateDimensions();
	        this.buildPlotLayer();
	        this.buildInteractionLayer();
	        this._isRendered = true;
	        return this;
	    };
	    Chart.prototype.id = function (value) {
	        if (value === undefined) {
	            return this._id;
	        }
	        this._id = value;
	        return this;
	    };
	    ;
	    Chart.prototype.layout = function (value) {
	        if (value === undefined) {
	            return this._layout;
	        }
	        this._layout = value;
	        // TODO: add available plots to new layout
	        return this;
	    };
	    ;
	    Chart.prototype.destroy = function () {
	        this.unsubscribeToInteractions();
	    };
	    Chart.prototype.draw = function () {
	        this.updateDimensions();
	        this.updatePlots();
	        this.updateInteraction();
	        return this;
	    };
	    Chart.prototype.target = function (element) {
	        if (element === undefined) {
	            return this._target;
	        }
	        this._target = d3.select(element);
	        return this;
	    };
	    Chart.prototype.config = function (value) {
	        if (value === undefined) {
	            return this._config;
	        }
	        this._config = value;
	        return this;
	    };
	    Chart.prototype.series = function (series) {
	        var _this = this;
	        if (series === undefined) {
	            return this._seriesDictionary;
	        }
	        this._seriesDictionary = series;
	        // TODO: Reset chart plots
	        _.forEach(this._seriesDictionary, function (s) {
	            _this.addPlot(s);
	        });
	        return this;
	    };
	    Chart.prototype.addSeries = function (series) {
	        var _this = this;
	        _.forEach(series, function (s) {
	            if (_this._seriesDictionary.hasOwnProperty(s.id)) {
	                throw new Error('Adding series that already exists in chart.');
	            }
	            //console.debug('Adding series: ', s);
	            _this._seriesDictionary[s.id] = s;
	            _this.addPlot(s);
	        });
	        return this;
	    };
	    Chart.prototype.removeSeries = function (seriesId) {
	        this.removePlot(this._seriesDictionary[seriesId]);
	        delete this._seriesDictionary[seriesId];
	        return this;
	    };
	    Chart.prototype.getPlots = function () {
	        return this._layout.getOrderedPlots();
	    };
	    Chart.prototype.interactionPubSub = function (value) {
	        if (value === undefined) {
	            return this._interactionPubSub;
	        }
	        this.unsubscribeToInteractions();
	        this._interactionPubSub = value;
	        this.subscribeToInteractions();
	        return this;
	    };
	    Chart.prototype.buildInteractionLayer = function () {
	        // this._interactionLayer = this._stage.append("g")
	        //   .classed("sw-interaction-layer",true);
	        this._svg
	            .on("mousemove", this.onMouseMove)
	            .on("mouseleave", this.onMouseLeave)
	            .on("click", this.onMouseClick);
	    };
	    ;
	    Chart.prototype.updateInteraction = function () {
	        this._waitingForUpdateFrame = false;
	        if (this._mouseState.isInteracting) {
	            // console.log("Mouse Coords", this._mouseState.coords);
	            // Look up what plot mouse is over
	            var plotId = this._layout.getPlotIdByCoords(this._mouseState.coords);
	            if (plotId) {
	                this._plotDictionary[plotId].coordsToInteraction(this._mouseState.coords);
	            }
	        }
	    };
	    // private updateClickInteraction() {
	    //   let plotId = this._layout.getPlotIdByCoords(this._mouseState.coords);
	    //   if (plotId) {
	    //     this._plotDictionary[plotId].clickToInteraction(this._mouseState.coords);
	    //   }
	    // }
	    Chart.prototype.requestInteractionUpdate = function () {
	        var _this = this;
	        if (!this._waitingForUpdateFrame) {
	            this._waitingForUpdateFrame = true;
	            requestAnimationFrame(function () { return _this.updateInteraction(); });
	        }
	    };
	    Chart.prototype.onTimeInteraction = function (value) {
	        var data = [];
	        for (var _i = 1; _i < arguments.length; _i++) {
	            data[_i - 1] = arguments[_i];
	        }
	        // console.log('Interaction Received: ', value);
	    };
	    Chart.prototype.subscribeToInteractions = function () {
	        this._interactionSubIds.push(this._interactionPubSub.subscribe(interaction_service_1.InteractionMessages.time, this.onTimeInteraction));
	    };
	    Chart.prototype.unsubscribeToInteractions = function () {
	        var _this = this;
	        _.forEach(this._interactionSubIds, function (id) {
	            _this._interactionPubSub.unsubscribe(id);
	        });
	        this._interactionSubIds.length = 0;
	    };
	    Chart.prototype.removePlot = function (series) {
	        var plotId;
	        switch (series.type) {
	            case Series.Type.gauge:
	                plotId = this._gaugePlotId;
	                this._plotDictionary[plotId]
	                    .removeSeries(series);
	                if (_.keys(this._plotDictionary[plotId].series()).length === 0) {
	                    this._layout
	                        .removePlot(this._plotDictionary[plotId].id())
	                        .update();
	                    this._plotDictionary[plotId].destroy();
	                    delete this._plotDictionary[plotId];
	                    this._gaugePlots.splice(0, 1);
	                }
	                else {
	                    /* Fix for PS-698: null Error removing last isPercentile Metric from Plot */
	                    this._plotDictionary[plotId].draw();
	                }
	                break;
	            default:
	                var plot = _.find(this._plotDictionary, function (plot) {
	                    return plot.series()[series.id] !== undefined;
	                });
	                this._layout
	                    .removePlot(plot.id())
	                    .update();
	                plot
	                    .removeSeries(series)
	                    .draw()
	                    .destroy();
	                this._statePlots.splice(this._statePlots.indexOf(plot.id()), 1);
	                delete this._plotDictionary[plot.id()];
	                break;
	        }
	    };
	    Chart.prototype.addPlot = function (series) {
	        var _this = this;
	        var id;
	        var plotGroup;
	        var plotConfig = new plot_config_1.default();
	        plotConfig.time = this._config.time;
	        plotConfig.interaction = this._config.interaction;
	        switch (series.type) {
	            case Series.Type.event:
	            case Series.Type.dpaWaitTime:
	                id = _.uniqueId('plot-event');
	                plotGroup = this._plotLayer.append('g').attr('id', id);
	                this._plotDictionary[id] = new plot_timeseries_1.default()
	                    .id(id)
	                    .parent(this._id)
	                    .target(plotGroup)
	                    .isStacked(!_.isEmpty(series.subSeries))
	                    .config(plotConfig)
	                    .addSeries(series);
	                this._layout
	                    .addPlot(this._plotDictionary[id]);
	                setTimeout(function () {
	                    _this.updatePlotDimensions();
	                    _this._plotDictionary[id].render();
	                }, 160);
	                break;
	            case Series.Type.multi:
	                id = _.uniqueId('plot-multi');
	                this._multiPlots.push(id);
	                plotGroup = this._plotLayer.append('g').attr('id', id);
	                this._plotDictionary[id] = new plot_timeseries_1.default()
	                    .id(id)
	                    .parent(this._id)
	                    .target(plotGroup)
	                    .isStacked(true)
	                    .config(plotConfig)
	                    .addSeries(series);
	                this._layout.addPlot(this._plotDictionary[id]);
	                setTimeout(function () {
	                    _this.updatePlotDimensions();
	                    _this._plotDictionary[id].render();
	                }, 160);
	                break;
	            case Series.Type.gauge:
	                //console.debug('Adding gauge plot: ', s);
	                this._gaugePlotId = this._gaugePlotId || _.uniqueId('gauge');
	                id = this._gaugePlotId;
	                if (this._plotDictionary[id]) {
	                    this._plotDictionary[id]
	                        .addSeries(series);
	                    this._layout.update();
	                    setTimeout(function () {
	                        _this.updatePlotDimensions();
	                        _this.draw();
	                    }, 100);
	                }
	                else {
	                    plotGroup = this._plotLayer.append('g').attr('id', id);
	                    this._gaugePlots.push(id);
	                    // .dimension(this._dimension)
	                    this._plotDictionary[id] = new plot_timeseries_1.default()
	                        .id(id)
	                        .parent(this._id)
	                        .target(plotGroup)
	                        .config(plotConfig)
	                        .addSeries(series);
	                    this._layout.addPlot(this._plotDictionary[id]);
	                    setTimeout(function () {
	                        _this.updatePlotDimensions();
	                        _this._plotDictionary[id].render();
	                    }, 100);
	                }
	                break;
	            default:
	                // State series always get new plot
	                id = _.uniqueId('plot-state'); //series.type + '--' + series.id;
	                plotGroup = this._plotLayer.append('g').attr('id', id);
	                this._statePlots.push(id);
	                this._plotDictionary[id] = new plot_timeseries_1.default()
	                    .id(id)
	                    .parent(this._id)
	                    .target(plotGroup)
	                    .config(plotConfig)
	                    .addSeries(series);
	                this._layout.addPlot(this._plotDictionary[id]);
	                setTimeout(function () {
	                    _this.updatePlotDimensions();
	                    _this._plotDictionary[id].render();
	                }, 100);
	                this._plotDictionary[id].config().grid.axis.bottom.visible = false;
	                this._plotDictionary[id].config().grid.ticks.tickFormat = function () { return ''; };
	                this._plotDictionary[id].config().grid.ticks.topTick.visible = false;
	                this._plotDictionary[id].config().grid.ticks.bottomTick.visible = false;
	                this._plotDictionary[id].config().grid.ticks.middleTicks = [new tick_config_1.default()];
	                break;
	        }
	    };
	    Chart.prototype.updatePlots = function () {
	        _.forEach(this._plotDictionary, function (plot) {
	            plot
	                .draw();
	        });
	    };
	    Chart.prototype.buildStage = function () {
	        this._svg = this._target.append('svg');
	        this._definitions = this._svg.append('defs');
	        this._stage = this._svg.append('g')
	            .attr('class', 'sw-chart-base-layer')
	            .attr('transform', 'translate(' + this._dimension.padding.left + ',' + this._dimension.padding.top + ')');
	    };
	    Chart.prototype.buildPlotLayer = function () {
	        this._plotLayer = this._stage.append('g')
	            .attr('class', 'sw-chart-plot-layer');
	    };
	    Chart.prototype.updateDimensions = function () {
	        if (!this._target.empty()) {
	            this._dimension.width = $(this._target.node()).width() - this._dimension.padding.left - this._dimension.padding.right;
	            this._dimension.height = $(this._target.node()).height() - this._dimension.padding.top - this._dimension.padding.bottom;
	        }
	        this._dimension.width = (this._dimension.width > 0) ? this._dimension.width : 0;
	        this._dimension.height = (this._dimension.height > 0) ? this._dimension.height : 0;
	        this._layout.config().dimension()
	            .width(this._dimension.width)
	            .height(this._dimension.height);
	        this._layout.update();
	        this._svg
	            .classed('ps-inspectable', this._config.interaction.inspectionInteraction())
	            .attr('width', this._dimension.width + this._dimension.padding.left + this._dimension.padding.right)
	            .attr('height', (this._layout.totalHeight() || 0) + this._dimension.padding.top + this._dimension.padding.bottom);
	        this._stage
	            .attr('width', this._dimension.width)
	            .attr('height', this._layout.totalHeight());
	        this.updatePlotDimensions();
	    };
	    Chart.prototype.updatePlotDimensions = function () {
	        var _this = this;
	        _.forEach(this._plotDictionary, function (plot) {
	            var bbox = _this._layout.plotBox(plot.id());
	            if (bbox !== undefined) {
	                plot.config().dimension
	                    .height(bbox.height())
	                    .width(bbox.width())
	                    .x(bbox.x())
	                    .y(bbox.y());
	            }
	            plot.config().grid.axis.bottom.visible = false;
	        });
	        var orderedPlots = this._layout.getOrderedPlots();
	        if (orderedPlots.length > 0) {
	            orderedPlots[orderedPlots.length - 1].config().grid.axis.bottom.visible = true;
	        }
	    };
	    Chart.prototype.updateMouseState = function () {
	        var mouseCoords = d3.mouse(d3.event.currentTarget);
	        mouseCoords[0] -= this._dimension.padding.left;
	        mouseCoords[1] -= this._dimension.padding.top;
	        // Update mouse state
	        this._mouseState.coords = mouseCoords;
	        this._mouseState.isInteracting = true;
	    };
	    return Chart;
	}());
	exports.Chart = Chart;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path='../../../../ref.d.ts' />
	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var Series = __webpack_require__(6);
	var Plot = __webpack_require__(17);
	var Domain = __webpack_require__(19);
	var MarkLine = __webpack_require__(28);
	var Mark = __webpack_require__(7);
	var mark_status_1 = __webpack_require__(29);
	var mark_alert_1 = __webpack_require__(30);
	var mark_area_1 = __webpack_require__(31);
	var interaction_service_1 = __webpack_require__(11);
	var series_1 = __webpack_require__(6);
	var mark_bar_stacked_1 = __webpack_require__(32);
	var time_data_1 = __webpack_require__(13);
	var config_1 = __webpack_require__(26);
	var mark_bar_1 = __webpack_require__(33);
	var PlotTimeSeries = (function (_super) {
	    __extends(PlotTimeSeries, _super);
	    function PlotTimeSeries() {
	        var _this = _super !== null && _super.apply(this, arguments) || this;
	        _this._inspectionBorder = {
	            left: null,
	            right: null
	        };
	        _this._series = {};
	        _this._domainLeft = {
	            type: null,
	            x: null,
	            y: null,
	            accessorKeys: {
	                x: 'date',
	                y: 'value'
	            }
	        };
	        _this._domainRight = {
	            type: null,
	            x: null,
	            y: null,
	            accessorKeys: {
	                x: 'date',
	                y: 'value'
	            }
	        };
	        _this._seriesPercentile = {};
	        _this._seriesNumber = {};
	        return _this;
	    }
	    PlotTimeSeries.prototype.addSeries = function (series) {
	        if (series) {
	            if (series.isPercentile) {
	                this._domainLeft.type = Domain.Type.percent;
	                this._seriesPercentile[series.id] = series;
	            }
	            else {
	                this._domainRight.type = Domain.Type.number;
	                this._seriesNumber[series.id] = series;
	            }
	            return _super.prototype.addSeries.call(this, series);
	        }
	    };
	    PlotTimeSeries.prototype.removeSeries = function (series) {
	        this._marks[series.id].destroy();
	        delete this._marks[series.id];
	        var isLastSeriesOfDomain = (series.isPercentile) ? _.keys(this._seriesPercentile).length === 1 : _.keys(this._seriesNumber).length === 1;
	        if (isLastSeriesOfDomain) {
	            if (series.isPercentile) {
	                this._domainLeft.type = null;
	            }
	            else {
	                this._domainRight.type = null;
	            }
	        }
	        if (series.isPercentile) {
	            delete this._seriesPercentile[series.id];
	        }
	        else {
	            delete this._seriesNumber[series.id];
	        }
	        return _super.prototype.removeSeries.call(this, series);
	    };
	    PlotTimeSeries.prototype.coordsToInteraction = function (coords) {
	        _super.prototype.coordsToInteraction.call(this, coords);
	        var domain = (this._domainLeft.type !== null) ? this._domainLeft : this._domainRight;
	        if (domain.x === null) {
	            // domain is not ready yet
	            return;
	        }
	        var plotMoment;
	        plotMoment = moment(domain.x.invert(coords[0]));
	        // Publish time interaction
	        this._interactionPubSub.time(plotMoment, { plotId: this._id, parentId: this._parent });
	        // Dispatch if mouse over inspection area
	        // let bbox = _.extend({},this._inspectionArea.node().getBBox());
	        var boundingRect = this._inspectionArea.node().getBBox(); // HACK: shouldn't/can't use extend
	        var bbox = { y: boundingRect.y, x: boundingRect.x, height: boundingRect.height, width: boundingRect.width };
	        bbox.y += config_1.getTranslationCoords(this._target)[1];
	        if (!this._inspectionArea.classed("sw-hidden") && config_1.coordsInBBox(coords, bbox)) {
	            this._interactionPubSub.inspectDisplayUpdate(this._inspectionBorder.right.node().getBoundingClientRect(), { showMenu: true, isExploringEnabled: this.isExploringEnabled(), metricIds: _.keys(this._series), series: this._series });
	        }
	    };
	    PlotTimeSeries.prototype.clickToInteraction = function (coords) {
	        var _this = this;
	        _super.prototype.coordsToInteraction.call(this, coords);
	        var domain = (this._domainLeft.type !== null) ? this._domainLeft : this._domainRight;
	        var plotMoment = moment((domain.x).invert(coords[0]));
	        // Publish time interaction
	        this._interactionPubSub.time(moment(plotMoment), { plotId: this._id, parentId: this._parent });
	        if (this._config.interaction.inspectionInteraction()) {
	            var inspectionTimeframe_1 = new time_data_1.Timeframe();
	            var seriesType_1 = -1;
	            var maxResolution_1;
	            _.forEach(this._series, function (series) {
	                var dataIndex = (series.type === series_1.Type.gauge || series.type === series_1.Type.multi) ? _this.timeToSeriesIndex(series, plotMoment) : _this.timeToBucketSeriesIndex(series, plotMoment);
	                seriesType_1 = series.type;
	                if (dataIndex > -1) {
	                    maxResolution_1 = _.max([series.data[dataIndex].resolution, maxResolution_1]);
	                    inspectionTimeframe_1.start(((!inspectionTimeframe_1.start() || series.data[dataIndex].date < inspectionTimeframe_1.start()) ? moment(series.data[dataIndex].date) : inspectionTimeframe_1.start()));
	                    inspectionTimeframe_1.end(((!inspectionTimeframe_1.end() || series.data[dataIndex].date > inspectionTimeframe_1.end()) ? moment(series.data[dataIndex].date) : inspectionTimeframe_1.end()));
	                }
	            });
	            if (maxResolution_1 === undefined) {
	                // HACK: magic data density target
	                maxResolution_1 = moment(domain.x.domain()[1]).diff(domain.x.domain()[0], 'seconds') / 240;
	                inspectionTimeframe_1.start(moment(plotMoment));
	                inspectionTimeframe_1.end(moment(plotMoment));
	            }
	            if (seriesType_1 !== series_1.Type.gauge && seriesType_1 !== series_1.Type.multi) {
	                // HACK: need to select bucketed data points better (the point we have is the endpoint not the middle)
	                inspectionTimeframe_1.start(inspectionTimeframe_1.start().subtract(maxResolution_1, 'seconds'));
	                // inspectionTimeframe.end(inspectionTimeframe.end().add(maxResolution/2, 'seconds'));
	            }
	            else {
	                inspectionTimeframe_1.start(inspectionTimeframe_1.start().subtract(maxResolution_1 / 2, 'seconds'));
	                inspectionTimeframe_1.end(inspectionTimeframe_1.end().add(maxResolution_1 / 2, 'seconds'));
	            }
	            this.snapTimeframeToDomain(inspectionTimeframe_1, domain);
	            // this._interactionPubSub.inspect(inspectionTimeframe, _.keys(this._series), this.id());
	            this._interactionPubSub.inspect(inspectionTimeframe_1, { metricIds: _.keys(this._series), plotId: this.id(), series: this._series });
	        }
	    };
	    PlotTimeSeries.prototype.buildInspectionLayer = function () {
	        _super.prototype.buildInspectionLayer.call(this);
	        this._inspectionArea = this._inspectionLayer
	            .append("rect")
	            .attr("class", "ps-inspection-area sw-hidden");
	    };
	    PlotTimeSeries.prototype.buildInteractionLayer = function () {
	        _super.prototype.buildInteractionLayer.call(this);
	        this._inspectionOverlay = this._interactionLayer.append('g')
	            .attr('filter', 'url(#' + this._inspectionFilters.leftId + ')')
	            .attr('mask', 'url(#' + this._inspectionMask.id + ')')
	            .classed('sw-hidden', true);
	        this._inspectionOverlay.append("rect")
	            .attr('x', 0)
	            .attr('y', 0)
	            .attr('width', '100%')
	            .attr('height', '100%');
	        this._inspectionBorder.left = this._interactionLayer
	            .append("line")
	            .attr("class", "ps-inspection-border sw-hidden");
	        this._inspectionBorder.right = this._interactionLayer
	            .append("line")
	            .attr("class", "ps-inspection-border sw-hidden");
	        this._interactionLine = this._interactionLayer
	            .append("line")
	            .attr("class", "sw-interaction-line sw-hidden");
	        this._interactionTimeTip = this._interactionLayer.append('g')
	            .classed('sw-time-tip', true)
	            .classed('sw-hidden', true);
	        this._interactionTimeTip.append('rect')
	            .attr("class", "ps-background")
	            .attr('rx', '5')
	            .attr('ry', '5');
	        this._interactionTimeTip.append('text')
	            .attr('class', 'sw-time-label')
	            .style("text-anchor", "middle")
	            .text("time");
	    };
	    PlotTimeSeries.prototype.initBehaviors = function () {
	        _super.prototype.initBehaviors.call(this);
	        var self = this;
	        var startX = 0;
	        var timeframe = new time_data_1.Timeframe();
	        this._target.call(d3.drag()
	            .on('start', function () {
	            startX = d3.event.x;
	            self.onDragStart.call(self, timeframe);
	        })
	            .on('drag', function () {
	            self.onDragging.call(self, timeframe, startX);
	        })
	            .on('end', function () {
	            if (Math.abs(d3.event.x - startX) > 5) {
	                self.onDragEnd.call(self, timeframe);
	            }
	            else {
	                self.clickToInteraction([d3.event.x, d3.event.y]); // handle small drags like a click
	            }
	        }));
	    };
	    ;
	    PlotTimeSeries.prototype.snapTimeframeToDomain = function (timeframe, domain) {
	        var domainStart = moment(domain.x.domain()[0]);
	        var domainEnd = moment(domain.x.domain()[1]);
	        if (timeframe.start().isBefore(domainStart)) {
	            timeframe.start(domainStart);
	        }
	        if (timeframe.end().isAfter(domainEnd)) {
	            timeframe.end(domainEnd);
	        }
	    };
	    PlotTimeSeries.prototype.onDragStart = function (timeframe) {
	        _super.prototype.onDragStart.call(this, timeframe);
	        var domain = (this._domainLeft.type !== null) ? this._domainLeft : this._domainRight;
	        timeframe.start(moment((domain.x).invert(d3.event.x)))
	            .end(moment((domain.x).invert(d3.event.x)));
	        this.snapTimeframeToDomain(timeframe, domain);
	        if (this._config.interaction.inspectionInteraction()) {
	            this.displayInspectionArea(true, timeframe);
	            // hide menu when starting inspection
	            // this._interactionPubSub.inspectDisplayUpdate(this._inspectionBorder.right.node().getBoundingClientRect(),{showMenu: false})
	        }
	    };
	    PlotTimeSeries.prototype.onDragging = function (timeframe, startX) {
	        _super.prototype.onDragging.call(this, timeframe, startX);
	        var edgeMoment;
	        var domain = ((this._domainLeft.type !== null) ? this._domainLeft : this._domainRight);
	        var x = d3.event.x;
	        // Respect range of chart (stop updates if mouse out of visible range)
	        if (x < domain.x.range()[0] || x > domain.x.range()[1]) {
	            x = (x < domain.x.range()[0]) ? domain.x.range()[0] : domain.x.range()[1];
	        }
	        edgeMoment = moment(domain.x.invert(x));
	        if (x < startX) {
	            timeframe.start(edgeMoment);
	        }
	        else {
	            timeframe.end(edgeMoment);
	        }
	        if (this._config.interaction.inspectionInteraction()) {
	            this.displayInspectionArea(true, timeframe);
	        }
	        this._interactionPubSub.time(edgeMoment, { plotId: this._id, parentId: this._parent });
	    };
	    PlotTimeSeries.prototype.onDragEnd = function (timeframe) {
	        _super.prototype.onDragEnd.call(this, timeframe);
	        if (this._config.interaction.inspectionInteraction()) {
	            this._interactionPubSub.inspect(timeframe, { metricIds: _.keys(this._series), plotId: this.id(), series: this._series });
	            this._interactionPubSub.inspectDisplayUpdate(this._inspectionBorder.right.node().getBoundingClientRect(), { showMenu: true, isExploreEnabled: this.isExploringEnabled(), metricIds: _.keys(this._series), series: this._series });
	        }
	    };
	    PlotTimeSeries.prototype.subscribeToInteractions = function () {
	        _super.prototype.subscribeToInteractions.call(this);
	        this._interactionSubIds.push(this._interactionPubSub.subscribe(interaction_service_1.InteractionMessages.time, this.onTimeInteraction, this));
	        this._interactionSubIds.push(this._interactionPubSub.subscribe(interaction_service_1.InteractionMessages.inspect, this.onInspection, this));
	    };
	    PlotTimeSeries.prototype.updateInspectionLayer = function () {
	        _super.prototype.updateInspectionLayer.call(this);
	        var inspectionTimeframe = this._interactionPubSub.inspect();
	        if (inspectionTimeframe) {
	            this.displayInspectionArea(true, inspectionTimeframe, false);
	        }
	    };
	    PlotTimeSeries.prototype.updateMarkLayer = function () {
	        var _this = this;
	        _super.prototype.updateMarkLayer.call(this);
	        _.forEach(this._series, function (series) {
	            if (series.isPercentile) {
	                _this.updateMark(series, _this._domainLeft);
	            }
	            else {
	                _this.updateMark(series, _this._domainRight);
	            }
	        });
	    };
	    PlotTimeSeries.prototype.updateGridLayer = function () {
	        _super.prototype.updateGridLayer.call(this);
	        this._grid.draw(this._domainLeft, this._domainRight);
	    };
	    PlotTimeSeries.prototype.updateDomains = function () {
	        if (!this._series) {
	            console.warn('Plot tried to update domains without series');
	            return;
	        }
	        _super.prototype.updateDomains.call(this);
	        if (this._domainLeft.type !== null) {
	            this._domainLeft.x = d3.scaleTime();
	            this._domainLeft.x
	                .domain(this.getExtents(this._seriesPercentile, this._domainLeft.accessorKeys.x)) //d3.extent(this._series.data, (d: SeriesTime.ITimeSeriesData) => d[this._domainLeft.accessorKeys.x])
	                .range([0, this._config.dimension.width()]);
	            this._domainLeft.y = d3.scaleLinear();
	            this._domainLeft.y.domain([0, 100]); // Known domain for percentile
	            this._domainLeft.y.range([this._config.dimension.height(), 0]);
	        }
	        if (this._domainRight.type !== null) {
	            this._domainRight.x = d3.scaleTime();
	            this._domainRight.x
	                .domain(this.getExtents(this._seriesNumber, this._domainRight.accessorKeys.x)) //d3.extent(this._series.data, (d: SeriesTime.ITimeSeriesData) => d[this._domainRight.accessorKeys.x])
	                .range([0, this._config.dimension.width()]);
	            this._domainRight.y = d3.scaleLinear();
	            if (this._isStacked) {
	                this._domainRight.y
	                    .domain(this.getStackedExtents(this._seriesNumber));
	            }
	            else {
	                this._domainRight.y
	                    .domain(this.getExtents(this._seriesNumber, this._domainRight.accessorKeys.y)); //d3.extent(this._series.data, (d: SeriesTime.ITimeSeriesData) => d[this._domainRight.accessorKeys.y])
	            }
	            this._domainRight.y.range([this._config.dimension.height(), 0]);
	        }
	    };
	    PlotTimeSeries.prototype.getStackedExtents = function (seriesList) {
	        var stackedData = this.pivotSeriesToStackData(seriesList[_.keys(seriesList)[0]].subSeries, this._domainRight);
	        return stackedData.data.extent;
	    };
	    PlotTimeSeries.prototype.getExtents = function (series, accessorKey) {
	        var result;
	        var allSeriesExtents = [];
	        // HACK: Use config extents if set for x-axis
	        if (accessorKey === this._domainLeft.accessorKeys.x && !this._config.time.isDefault()) {
	            return [this._config.time.timeframe().start(), this._config.time.timeframe().end()];
	        }
	        _.forEach(series, function (seriesItem) {
	            if (seriesItem.subSeries) {
	                _.forEach(seriesItem.subSeries, function (subSeries) {
	                    allSeriesExtents = allSeriesExtents.concat(d3.extent(subSeries.data, function (d) { return d[accessorKey]; }));
	                });
	            }
	            allSeriesExtents = allSeriesExtents.concat(d3.extent(seriesItem.data, function (d) { return d[accessorKey]; }));
	        });
	        result = d3.extent(allSeriesExtents);
	        // Special case when no measurements/data
	        result = (result[0] === undefined) ? [0, 0] : result;
	        // For events, ensure max is an even number for intuitive (non-fraction) ticks
	        // HACK: should go in plot-timeseries-event; assuming 4 ticks (if assumption is approved)
	        var type = _.values(series)[0].type;
	        if ((type === Series.Type.event || type === Series.Type.dpaWaitTime) && (result[1] === 0 || result[1] % 4 > 0)) {
	            result[1] = (result[1] + 4) - (result[1] % 4);
	        }
	        var extentRange = Math.abs((result[1] - result[0]) / ((result[1] + result[0]) / 2));
	        // For small domain ranges increase domain so that spikes are not exaggerated
	        if (accessorKey !== this._domainLeft.accessorKeys.x && extentRange < 0.5) {
	            result[0] = result[0] - (Math.abs(result[0]) * 0.5);
	            result[1] = result[1] + (Math.abs(result[1]) * 0.5);
	        }
	        // Special case for Zero
	        if (result[0] === result[1] && result[1] === 0) {
	            result[0] = -1;
	            result[1] = 1;
	        }
	        // Special case for real small number since not using si prefix anymore
	        if (accessorKey !== this._domainLeft.accessorKeys.x && (Math.abs(result[1] - result[0]) < 0.04)) {
	            result[1] += 0.04;
	        }
	        result.sort(function (a, b) { return a - b; });
	        return result;
	    };
	    PlotTimeSeries.prototype.pivotSeriesToStackData = function (seriesList, domain) {
	        var stack = d3.stack();
	        var pivotData = [];
	        var keys = {}; //_.keys(seriesList);
	        // Pivot the series list into timebucket layers
	        _.forEach(seriesList[0].data, function (data, index) {
	            var timeBucket = {};
	            timeBucket[domain.accessorKeys.x] = data[domain.accessorKeys.x];
	            pivotData.push(timeBucket); // add time layer
	            _.forEach(seriesList, function (series) {
	                keys[series.id] = series.colorClass;
	                // Add each series data point to the time layer
	                timeBucket[series.id] = (series.isPercentile) ? series.data[index][domain.accessorKeys.y] / seriesList.length : series.data[index][domain.accessorKeys.y];
	            });
	        });
	        var keysList = _.keys(keys);
	        var timeBucketSums = [];
	        _.forEach(pivotData, function (d, index) {
	            var sums = {};
	            _.forEach(keysList, function (key) {
	                if (sums.hasOwnProperty('t' + index)) {
	                    sums['t' + index] += d[key];
	                }
	                else {
	                    sums['t' + index] = d[key];
	                }
	            });
	            timeBucketSums.push(_.values(sums));
	        });
	        var max = d3.max(_.flatten(timeBucketSums));
	        stack.keys(keysList);
	        var result = stack(pivotData);
	        result.data = {
	            extent: [0, max]
	        };
	        // For events, ensure max is an even number for intutive (non-fraction) ticks
	        // HACK: should go in plot-timeseries-event; assuming 4 ticks (if assumption is approved)
	        if ((seriesList[0].type === Series.Type.event || seriesList[0].type === Series.Type.dpaWaitTime) && (result.data.extent[1] === 0 || result.data.extent[1] % 4 > 0)) {
	            result.data.extent[1] = (result.data.extent[1] + 4) - (result.data.extent[1] % 4);
	        }
	        _.forEach(result, function (seriesLayer) {
	            seriesLayer.data = {
	                colorClass: keys[seriesLayer.key]
	            };
	        });
	        return result;
	    };
	    PlotTimeSeries.prototype.plotType = function () {
	        var series = _.values(this._series);
	        return (series.length > 0) ? series[0].type : 0;
	    };
	    PlotTimeSeries.prototype.isExploringEnabled = function () {
	        var plotType = this.plotType();
	        return (plotType !== series_1.Type.gauge && plotType !== series_1.Type.multi && plotType !== series_1.Type.state);
	    };
	    PlotTimeSeries.prototype.updateMark = function (series, domain) {
	        var _this = this;
	        if (this._marks.hasOwnProperty(series.id)) {
	            if (series.type === Series.Type.multi || series.type === Series.Type.event || series.type === Series.Type.alert || (series.type === Series.Type.dpaWaitTime && series.markType === Mark.Type.barStacked)) {
	                _.forEach(series.subSeries, function (subSeries) {
	                    _this.assignColorClass(subSeries);
	                });
	            }
	            this._marks[series.id].draw(series, domain);
	        }
	        else {
	            switch (series.markType) {
	                case Mark.Type.areaStacked:
	                    _.forEach(series.subSeries, function (subSeries) {
	                        _this.assignColorClass(subSeries);
	                    });
	                    this._marks[series.id] = new mark_area_1.default()
	                        .class(series.colorClass)
	                        .target(this._markLayer)
	                        .draw(series, domain);
	                    break;
	                case Mark.Type.bar:
	                    this._marks[series.id] = new mark_bar_1.default()
	                        .class(series.colorClass)
	                        .target(this._markLayer)
	                        .draw(series, domain);
	                    break;
	                case Mark.Type.barStacked:
	                    _.forEach(series.subSeries, function (subSeries) {
	                        _this.assignColorClass(subSeries);
	                    });
	                    this._marks[series.id] = new mark_bar_stacked_1.default()
	                        .target(this._markLayer)
	                        .draw(series, domain);
	                    break;
	                case Mark.Type.line:
	                    this._marks[series.id] = new MarkLine.default()
	                        .class(series.colorClass)
	                        .target(this._markLayer)
	                        .draw(series, domain);
	                    break;
	                case Mark.Type.state:
	                    this._marks[series.id] = new mark_status_1.default()
	                        .class(series.colorClass)
	                        .target(this._markLayer)
	                        .draw(series, domain);
	                    break;
	                case Mark.Type.stateStacked:
	                    this._config.grid.ticks.middleTicks = [];
	                    this._marks[series.id] = new mark_alert_1.default()
	                        .class(series.colorClass)
	                        .target(this._markLayer)
	                        .draw(series, domain);
	                    break;
	            }
	        }
	    };
	    PlotTimeSeries.prototype.getUsedDomain = function () {
	        if (this._domainLeft.type !== null) {
	            return this._domainLeft;
	        }
	        else {
	            return this._domainRight;
	        }
	    };
	    PlotTimeSeries.prototype.displayInspectionArea = function (display, timeframe, updateVerticalPosition) {
	        if (updateVerticalPosition === void 0) { updateVerticalPosition = true; }
	        this._inspectionArea
	            .classed("sw-hidden", !display);
	        this._inspectionOverlay
	            .classed('sw-hidden', !display);
	        if (display) {
	            var domain = this.getUsedDomain();
	            var x1 = domain.x(timeframe.start());
	            var x2 = domain.x(timeframe.end());
	            var width = ((x2 - x1) > 0) ? (x2 - x1) : 0;
	            this._inspectionArea
	                .classed("sw-hidden", !display)
	                .classed("ps-inspection-active", this._isInspecting)
	                .attr('data-start', timeframe.start().format())
	                .attr('data-end', timeframe.end().format())
	                .attr("x", x1)
	                .attr("y", 0)
	                .attr("height", this._config.dimension.height())
	                .attr("width", width);
	            this._inspectionBorder.left
	                .classed("sw-hidden", !display)
	                .classed("ps-inspection-active", this._isInspecting)
	                .attr("x1", x1)
	                .attr("y1", 0)
	                .attr("y2", this._config.dimension.height())
	                .attr("x2", x1);
	            this._inspectionBorder.right
	                .classed("sw-hidden", !display)
	                .classed("ps-inspection-active", this._isInspecting)
	                .attr("x1", x2)
	                .attr("y1", 0)
	                .attr("y2", this._config.dimension.height())
	                .attr("x2", x2);
	            this._inspectionMask.hideRegion
	                .attr("x", x1)
	                .attr('width', width);
	            if (this._isInspecting) {
	                var boundingRect = this._inspectionBorder.right.node().getBoundingClientRect(); // HACK: shouldn't/can't use extend
	                var mutableRect = { top: boundingRect.top, left: boundingRect.left, right: boundingRect.right, bottom: boundingRect.bottom, height: boundingRect.height, width: boundingRect.width };
	                if (!updateVerticalPosition) {
	                    mutableRect.top = null;
	                }
	                this._interactionPubSub.inspectDisplayUpdate(mutableRect, {});
	            }
	        }
	        else {
	            this._inspectionArea
	                .classed("sw-hidden", !display)
	                .classed("ps-inspection-active", this._isInspecting);
	            this._inspectionBorder.left
	                .classed("sw-hidden", !display)
	                .classed("ps-inspection-active", this._isInspecting);
	            this._inspectionBorder.right
	                .classed("sw-hidden", !display)
	                .classed("ps-inspection-active", this._isInspecting);
	        }
	    };
	    PlotTimeSeries.prototype.displayInteractionLine = function (display, timeMoment) {
	        this._interactionLine
	            .classed("sw-hidden", !display);
	        if (display) {
	            var domain = this.getUsedDomain();
	            var x = domain.x(timeMoment);
	            this._interactionLine
	                .classed("sw-hidden", !display)
	                .attr("x1", x)
	                .attr("y1", 0)
	                .attr("y2", this._config.dimension.height())
	                .attr("x2", x);
	        }
	    };
	    PlotTimeSeries.prototype.displayInteractionTime = function (display, timeMoment) {
	        this._interactionTimeTip
	            .classed("sw-hidden", !display);
	        if (display) {
	            var domain = this.getUsedDomain();
	            var x = domain.x(timeMoment);
	            var heightOffset = (this._config.dimension.height() + 15);
	            this._interactionTimeTip.select('text')
	                .attr("transform", "translate(0," + heightOffset + ")")
	                .text(timeMoment.local().format('L') + ' ' + timeMoment.format('LT'));
	            var bbox = this._interactionTimeTip.select('text').node().getBBox();
	            var width = bbox.width + 10;
	            var translationRange = [(width / 2), this._config.dimension.width() - (width / 2)];
	            x = (x < translationRange[0]) ? translationRange[0] : (x > translationRange[1]) ? translationRange[1] : x;
	            this._interactionTimeTip
	                .attr("transform", "translate(" + x + ",0)").select('rect')
	                .attr("transform", "translate(0," + heightOffset + ")")
	                .attr('x', -(width / 2))
	                .attr('y', bbox.y)
	                .attr('width', width)
	                .attr('height', bbox.height);
	        }
	    };
	    PlotTimeSeries.prototype.getClosestDate = function (a, b, test) {
	        var diffWithA = Math.abs(test.diff(a));
	        var diffWithB = Math.abs(test.diff(b));
	        return (diffWithA <= diffWithB) ? 0 : 1;
	    };
	    PlotTimeSeries.prototype.timeToSeriesIndex = function (series, timeMoment) {
	        var bisectDate = d3.bisector(function (d) { return d.date; }).left;
	        var closestDataIndex = bisectDate(series.data, timeMoment);
	        // Compensate for bisect (should write on compareator
	        closestDataIndex = (closestDataIndex > 0) ? closestDataIndex - 1 : closestDataIndex;
	        // Ensure inbounds reference if right end of data
	        closestDataIndex = (closestDataIndex >= series.data.length - 1 && series.data.length !== 1) ? series.data.length - 2 : closestDataIndex;
	        var indexIncrement = ((series.data.length > 1) ? this.getClosestDate(series.data[closestDataIndex].date, series.data[closestDataIndex + 1].date, timeMoment) : 0);
	        return closestDataIndex + indexIncrement;
	    };
	    PlotTimeSeries.prototype.timeToBucketSeriesIndex = function (series, timeMoment) {
	        var bisectDate = d3.bisector(function (d) { return d.date; }).right;
	        var closestDataIndex = bisectDate(series.data, timeMoment);
	        closestDataIndex = (closestDataIndex === series.data.length) ? closestDataIndex - 1 : closestDataIndex;
	        return closestDataIndex;
	    };
	    PlotTimeSeries.prototype.displayDataHighlights = function (display, timeMoment) {
	        var _this = this;
	        _.forEach(this._series, function (series) {
	            var domain;
	            if (display && series.data.length) {
	                domain = (series.isPercentile) ? _this._domainLeft : _this._domainRight;
	                var closestsDataIndex = (series.type === series_1.Type.gauge || series.type === series_1.Type.multi) ? _this.timeToSeriesIndex(series, timeMoment) : _this.timeToBucketSeriesIndex(series, timeMoment);
	                // Update marks
	                if (_this._marks[series.id]) {
	                    if (series.type === series_1.Type.multi) {
	                        // HACK: area chart perf short cut (using stack data bound to elements)
	                        _this._marks[series.id].highlightData(display, [closestsDataIndex], closestsDataIndex, series, domain);
	                    }
	                    else {
	                        _this._marks[series.id].highlightData(display, [series.data[closestsDataIndex]], closestsDataIndex, series, domain);
	                    }
	                }
	                // Publish passive series interaction
	                _this._interactionPubSub.seriesPassive(series.id + _this._parent, closestsDataIndex);
	            }
	            else {
	                if (_this._marks[series.id]) {
	                    _this._marks[series.id].highlightData(display, [], null, series, domain);
	                }
	                // Publish passive series interaction
	                _this._interactionPubSub.seriesPassive(series.id + _this._parent, null);
	            }
	        });
	    };
	    PlotTimeSeries.prototype.onInspection = function (timeframe, config, other) {
	        if (!this._isRendered) {
	            return;
	        }
	        var displayInteraction = (timeframe !== null && this._config.interaction.inspectionInteraction());
	        this._isInspecting = (displayInteraction && config.plotId === this.id());
	        this.displayInspectionArea(displayInteraction, timeframe);
	    };
	    PlotTimeSeries.prototype.onTimeInteraction = function (value, data) {
	        if (!this._isRendered) {
	            return;
	        }
	        var displayInteraction = (value !== null);
	        this.displayInteractionLine(displayInteraction, value);
	        this.displayInteractionTime((displayInteraction && this._isLastInLayout && data && data.parentId === this._parent), value);
	        this.displayDataHighlights(displayInteraction, value);
	    };
	    return PlotTimeSeries;
	}(Plot.default));
	exports.default = PlotTimeSeries;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path='../../../../ref.d.ts' />
	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Grid = __webpack_require__(18);
	var plot_config_1 = __webpack_require__(24);
	var interaction_service_1 = __webpack_require__(11);
	var Plot = (function () {
	    function Plot() {
	        this._series = {};
	        this._marks = {};
	        this._colorMap = Array(29);
	        this._config = new plot_config_1.default();
	        this._inspectionFilters = {
	            leftId: '',
	            left: null,
	            rightId: '',
	            right: null
	        };
	        this._isLastInLayout = false;
	        this._inspectionMask = {
	            id: null,
	            mask: null,
	            showRegion: null,
	            hideRegion: null
	        };
	        this._interactionSubIds = [];
	        this._id = _.uniqueId('Plot');
	        this._interactionPubSub = interaction_service_1.InteractionService.getInstance();
	        this.subscribeToInteractions();
	    }
	    Plot.prototype.id = function (value) {
	        if (value === undefined) {
	            return this._id;
	        }
	        this._id = value;
	        return this;
	    };
	    ;
	    Plot.prototype.parent = function (value) {
	        if (value === undefined) {
	            return this._parent;
	        }
	        this._parent = value;
	        return this;
	    };
	    ;
	    Plot.prototype.isLastInLayout = function (value) {
	        if (value === undefined) {
	            return this._isLastInLayout;
	        }
	        this._isLastInLayout = value;
	        return this;
	    };
	    ;
	    Plot.prototype.render = function () {
	        if (this._isRendered) {
	            return this;
	        }
	        this.buildDefs();
	        this.buildGridLayer();
	        this.buildInspectionLayer();
	        this.buildMarkLayer();
	        this.buildInteractionLayer();
	        this.initBehaviors();
	        this._isRendered = true;
	        this.draw();
	        return this;
	    };
	    Plot.prototype.draw = function () {
	        if (!this._isRendered) {
	            return this;
	        }
	        this.updateConfig();
	        this.updateClipPaths();
	        this.updateDomains();
	        this.updateGridLayer();
	        this.updateInspectionLayer();
	        this.updateMarkLayer();
	        return this;
	    };
	    Plot.prototype.series = function () {
	        return this._series;
	    };
	    Plot.prototype.isStacked = function (isStacked) {
	        if (isStacked === undefined) {
	            return this._isStacked;
	        }
	        this._isStacked = isStacked;
	        return this;
	    };
	    Plot.prototype.target = function (group) {
	        if (group === undefined) {
	            return this._target;
	        }
	        this._target = group;
	        return this;
	    };
	    Plot.prototype.config = function (config) {
	        if (config === undefined) {
	            return this._config;
	        }
	        this._config = config;
	        return this;
	    };
	    Plot.prototype.addSeries = function (series) {
	        var _this = this;
	        this._series[series.id] = series;
	        if (series.subSeries && series.subSeries.length > 0) {
	            _.forEach(series.subSeries, function (subSeries) {
	                _this.assignColorClass(subSeries);
	            });
	        }
	        else {
	            this.assignColorClass(series);
	        }
	        return this;
	    };
	    Plot.prototype.removeSeries = function (series) {
	        delete this._series[series.id];
	        var colorIndex = this._colorMap.indexOf(series.id);
	        this._colorMap[colorIndex] = undefined;
	        return this;
	    };
	    Plot.prototype.destroy = function (removeTarget) {
	        if (removeTarget === void 0) { removeTarget = true; }
	        if (removeTarget) {
	            this._target.remove();
	        }
	        else if (this._grid) {
	            this._grid.target().remove();
	        }
	        this.unsubscribeToInteractions();
	    };
	    Plot.prototype.coordsToInteraction = function (coords) {
	        // stub
	    };
	    Plot.prototype.clickToInteraction = function (coords) {
	        // stub
	    };
	    Plot.prototype.updateDomains = function () {
	        // Override
	    };
	    Plot.prototype.updateConfig = function () {
	        this._target
	            .attr('transform', 'translate(' + this._config.dimension.x() + ',' + this._config.dimension.y() + ')');
	    };
	    Plot.prototype.buildDefs = function () {
	        var defs = this._target.append('defs');
	        this._clipPathId = _.uniqueId('plot-clip-path-');
	        this._clipPath = defs.append('clipPath')
	            .attr('id', this._clipPathId)
	            .append('rect');
	        this._inspectionMask.id = _.uniqueId('plot-mask-');
	        this._inspectionMask.mask = defs.append('mask')
	            .attr('id', this._inspectionMask.id)
	            .attr('x', 0)
	            .attr('y', 0)
	            .attr('width', '100%')
	            .attr('height', '100%');
	        this._inspectionMask.showRegion = this._inspectionMask.mask.append('rect')
	            .attr('x', 0)
	            .attr('y', 0)
	            .attr('width', '100%')
	            .attr('height', '100%')
	            .attr('fill', 'white');
	        this._inspectionMask.hideRegion = this._inspectionMask.mask.append('rect')
	            .attr('x', 0)
	            .attr('y', 0)
	            .attr('width', '100%')
	            .attr('height', '100%')
	            .attr('fill', 'black');
	        //defs.append('filter')
	        // <mask id="inspectionMask" maskUnits="userSpaceOnUse" x="0" y="0" width="100%" height="100%">
	        //   <rect x="0" y="0" width="100%" height="100%" fill="white"/>
	        //   <rect x="50" y="0" width="50" height="100%" fill="black"/>
	        // </mask>
	        this._inspectionFilters.leftId = _.uniqueId('plot-filter-');
	        this._inspectionFilters.left = defs.append('filter')
	            .attr('id', this._inspectionFilters.leftId)
	            .attr('x', 0)
	            .attr('y', 0)
	            .attr('width', '100%')
	            .attr('height', '100%');
	        this._inspectionFilters.left
	            .append('feImage') //<feImage x="0" y="0" width="100%" height="100%" xlink:href="#marks" result="imageMarks"/>
	            .attr('xlink:href', '#ps-marks-' + this.id())
	            .attr('x', 0)
	            .attr('y', 0)
	            .attr('width', '100%')
	            .attr('height', '100%')
	            .attr('result', 'imageMarks');
	        this._inspectionFilters.left
	            .append('feColorMatrix') //<feColorMatrix in="imageMarks" result="greyscale" x="0" y="0" width="100%" height="100%" type="saturate" values="0"></feColorMatrix>
	            .attr('in', 'imageMarks')
	            .attr('result', 'greyscale')
	            .attr('x', 0)
	            .attr('y', 0)
	            .attr('width', '100%')
	            .attr('height', '100%')
	            .attr('type', 'saturate')
	            .attr('values', 0);
	    };
	    Plot.prototype.buildGridLayer = function () {
	        var gridLayer = this._target.append('g')
	            .classed('sw-grid-layer', true);
	        this._config.grid.dimension = this._config.dimension;
	        this._grid = new Grid.default()
	            .target(gridLayer)
	            .config(this._config.grid)
	            .render();
	    };
	    Plot.prototype.updateGridLayer = function () {
	        // Override
	    };
	    Plot.prototype.buildMarkLayer = function () {
	        if (!this._markLayer) {
	            this._markLayer = this._target.append('g')
	                .attr('id', 'ps-marks-' + this.id())
	                .attr('clip-path', 'url(' + window.location + '#' + this._clipPathId + ')')
	                .classed('sw-mark-layer', true);
	        }
	        this._marks = {};
	    };
	    Plot.prototype.updateMarkLayer = function () {
	        if (this._markLayer) {
	            this._markLayer
	                .attr('clip-path', 'url(' + window.location + '#' + this._clipPathId + ')');
	        }
	    };
	    Plot.prototype.updateInspectionLayer = function () {
	        // stub
	    };
	    Plot.prototype.assignColorClass = function (series) {
	        // Check if color already assigned
	        var colorIndex = this._colorMap.indexOf(series.id);
	        if (colorIndex > -1) {
	            series.colorClass = 'ps-color-' + (colorIndex + 1);
	            return;
	        }
	        // Assign the series to a color index
	        if (_.isEmpty(series.colorClass)) {
	            for (var i = 0; i < this._colorMap.length; i++) {
	                if (angular.isUndefined(this._colorMap[i])) {
	                    this._colorMap[i] = series.id;
	                    series.colorClass = 'ps-color-' + (i + 1);
	                    break;
	                }
	            }
	        }
	    };
	    Plot.prototype.subscribeToInteractions = function () {
	        // Stub
	    };
	    Plot.prototype.unsubscribeToInteractions = function () {
	        var _this = this;
	        _.forEach(this._interactionSubIds, function (id) {
	            _this._interactionPubSub.unsubscribe(id);
	        });
	        this._interactionSubIds.length = 0;
	    };
	    Plot.prototype.buildInteractionLayer = function () {
	        this._interactionLayer = this._target.append("g")
	            .classed("sw-interaction-layer", true);
	    };
	    ;
	    Plot.prototype.initBehaviors = function () {
	        //stub
	    };
	    ;
	    Plot.prototype.onDragStart = function (timeframe) {
	        //stub
	    };
	    Plot.prototype.onDragging = function (timeframe, startX) {
	        // stub
	    };
	    Plot.prototype.onDragEnd = function (timeframe) {
	        // stub
	    };
	    Plot.prototype.buildInspectionLayer = function () {
	        this._inspectionLayer = this._target.append("g")
	            .classed("ps-inspection-layer", true);
	    };
	    ;
	    Plot.prototype.updateClipPaths = function () {
	        this._clipPath
	            .attr('x', '0')
	            .attr('y', '-5')
	            .attr('width', this._config.dimension.width() + 1)
	            .attr('height', this._config.dimension.height() + 10);
	    };
	    return Plot;
	}());
	exports.default = Plot;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Domain = __webpack_require__(19);
	var grid_config_1 = __webpack_require__(20);
	var Grid = (function () {
	    function Grid() {
	        this._axisX = {};
	        this._axisYLeft = {};
	        this._axisYRight = {};
	        this._gridY = {};
	        this._borders = {};
	        this._config = new grid_config_1.default();
	    }
	    Grid.prototype.borders = function (config) {
	        return this;
	    };
	    Grid.prototype.render = function () {
	        this.buildDefs();
	        this.buildAxis();
	        this.buildBorders();
	        return this;
	    };
	    Grid.prototype.draw = function (domainLeft, domainRight) {
	        this.updateBorders();
	        this.updateAxis(domainLeft, domainRight);
	        this.updateClipPaths();
	        return this;
	    };
	    Grid.prototype.target = function (group) {
	        if (group === undefined) {
	            return this._target;
	        }
	        this._target = group;
	        return this;
	    };
	    Grid.prototype.config = function (config) {
	        if (config === undefined) {
	            return this._config;
	        }
	        this._config = config;
	        return this;
	    };
	    Grid.prototype.buildDefs = function () {
	        this._clipPathId = _.uniqueId('grid-clip-path-');
	        this._clipPath = this._target.append('defs').append('clipPath')
	            .attr('id', this._clipPathId)
	            .append('rect');
	    };
	    Grid.prototype.updateClipPaths = function () {
	        this._clipPath
	            .attr('x', '0')
	            .attr('y', '0')
	            .attr('width', this._config.dimension.width())
	            .attr('height', 35);
	    };
	    Grid.prototype.buildAxis = function () {
	        this._gridY.group = this._target.append('g')
	            .classed('sw-axis sw-axis-gridY', true);
	        this._axisX.group = this._target.append('g')
	            .classed('sw-axis sw-axis-x', true)
	            .attr('clip-path', 'url(' + window.location + '#' + this._clipPathId + ')');
	        this._axisYLeft.group = this._target.append('g')
	            .classed('sw-axis sw-axis-y', true);
	        this._axisYRight.group = this._target.append('g')
	            .classed('sw-axis sw-axis-y', true);
	    };
	    Grid.prototype.multiFormat = function (date) {
	        return (d3.timeMinute(date) < date ? formatSecond
	            : d3.timeDay(date) < date ? formatMinute
	                : d3.timeMonth(date) < date ? formatWeek
	                    : d3.timeYear(date) < date ? formatMonth
	                        : formatYear)(date);
	    };
	    Grid.prototype.updateAxisForPercent = function (axisY, alignment) {
	        var xOffset = (alignment === 'right') ? {
	            width: this._config.dimension.width(),
	            line: 5,
	            text: 35,
	            anchor: 'end'
	        } : {
	            width: 0,
	            line: -5,
	            text: -35,
	            anchor: 'start'
	        };
	        this._gridY.group
	            .attr('transform', 'translate(0,0)')
	            .call(this._gridY.axis
	            .tickSizeInner(-this._config.dimension.width())
	            .tickValues([0, 25, 50, 75, 100])
	            .tickFormat(function (d) {
	            return '';
	        }));
	        axisY.group
	            .attr('transform', 'translate(' + xOffset.width + ', 0)')
	            .call(axisY.axis
	            .tickSize(35)
	            .tickValues([0, 25, 50, 75, 100])
	            .tickFormat(function (d) {
	            return d + '%';
	        }));
	        axisY.group.selectAll('.tick line')
	            .attr('x1', xOffset.line);
	        axisY.group.selectAll('.tick text')
	            .attr('transform', 'translate(0,10)')
	            .attr('x', xOffset.text)
	            .style('text-anchor', xOffset.anchor);
	    };
	    Grid.prototype.updateAxisForNumber = function (axisY, domainRange, alignment) {
	        var _this = this;
	        var tickValues = [];
	        if (this._config.ticks.topTick.visible) {
	            tickValues.push(domainRange[1]);
	        }
	        if (this._config.ticks.bottomTick.visible) {
	            tickValues.push(domainRange[0]);
	        }
	        var middleTickCount = this._config.ticks.middleTicks.length;
	        var tickIncrement = 1 / (middleTickCount + 1);
	        _.forEach(this._config.ticks.middleTicks, function (tickConfig, index) {
	            if (tickConfig.visible) {
	                tickValues.push(((domainRange[1] - domainRange[0]) * (tickIncrement * (index + 1))) + domainRange[0]);
	            }
	        });
	        var xOffset = (alignment === 'left') ? {
	            width: 0,
	            line: -5,
	            text: -35
	        } : {
	            width: this._config.dimension.width(),
	            line: 5,
	            text: 5
	        };
	        var extrema = ((Math.abs(domainRange[1]) > Math.abs(domainRange[0])) ? Math.abs(domainRange[1]) : Math.abs(domainRange[0]));
	        this._gridY.group
	            .attr('transform', 'translate(0,0)')
	            .call(this._gridY.axis
	            .tickSizeInner(-this._config.dimension.width())
	            .tickValues(tickValues)
	            .tickFormat(function (d) {
	            return '';
	        }));
	        axisY.group
	            .attr('transform', 'translate(' + xOffset.width + ', 0)')
	            .call(axisY.axis
	            .tickSize(35)
	            .tickValues(tickValues)
	            .tickFormat(function (d) {
	            return _this._config.ticks.tickFormat(d, extrema);
	        }));
	        axisY.group.selectAll('.tick line')
	            .attr('x1', xOffset.line);
	        axisY.group.selectAll('.tick text')
	            .attr('transform', 'translate(0,10)')
	            .attr('x', xOffset.text)
	            .style('text-anchor', 'start');
	    };
	    Grid.prototype.updateAxis = function (domainLeft, domainRight) {
	        var primaryDomain = (domainLeft.type !== null) ? domainLeft : domainRight;
	        if (primaryDomain.type !== null) {
	            this._axisX.axis = d3.axisBottom(primaryDomain.x)
	                .tickFormat(this.multiFormat);
	            this._axisX.group
	                .attr('transform', 'translate(0,' + this._config.dimension.height() + ')')
	                .attr('clip-path', 'url(' + window.location + '#' + this._clipPathId + ')')
	                .classed('ps-hidden', !this._config.axis.bottom.visible)
	                .call(this._axisX.axis);
	            // Left Y axis
	            var primaryLeftDomain = (domainLeft.type !== null) ? domainLeft : domainRight;
	            this._axisYLeft.axis = d3.axisLeft(primaryLeftDomain.y);
	            this._axisYLeft.group.classed('ps-hidden', !this._config.axis.left.visible);
	            this._gridY.axis = d3.axisLeft(primaryLeftDomain.y);
	            if (primaryLeftDomain.type === Domain.Type.percent) {
	                this.updateAxisForPercent(this._axisYLeft);
	            }
	            else {
	                this.updateAxisForNumber(this._axisYLeft, primaryLeftDomain.y.domain(), 'left');
	            }
	            // Right Y axis
	            var primaryRightDomain = (domainRight.type !== null) ? domainRight : domainLeft;
	            this._axisYRight.axis = d3.axisRight(primaryRightDomain.y);
	            this._axisYRight.group.classed('ps-hidden', !this._config.axis.right.visible);
	            this._gridY.axis = d3.axisLeft(primaryRightDomain.y);
	            if (primaryRightDomain.type === Domain.Type.percent) {
	                this.updateAxisForPercent(this._axisYRight, 'right');
	            }
	            else {
	                this.updateAxisForNumber(this._axisYRight, primaryRightDomain.y.domain(), 'right');
	            }
	        }
	    };
	    Grid.prototype.buildBorders = function () {
	        this._borders.top = this._target.append('line')
	            .attr('class', 'sw-border sw-border-top');
	        this._borders.right = this._target.append('line')
	            .attr('class', 'sw-border sw-border-right');
	        this._borders.bottom = this._target.append('line')
	            .attr('class', 'sw-border sw-border-bottom');
	        this._borders.left = this._target.append('line')
	            .attr('class', 'sw-border sw-border-left');
	    };
	    Grid.prototype.updateBorders = function () {
	        this._borders.top
	            .attr('x1', 0)
	            .attr('y1', 0)
	            .attr('x2', this._config.dimension.width())
	            .attr('y2', 0);
	        this._borders.right
	            .attr('x1', this._config.dimension.width())
	            .attr('y1', 0)
	            .attr('x2', this._config.dimension.width())
	            .attr('y2', this._config.dimension.height() + 35);
	        this._borders.bottom
	            .attr('x1', 0)
	            .attr('y1', this._config.dimension.height())
	            .attr('x2', this._config.dimension.width())
	            .attr('y2', this._config.dimension.height());
	        this._borders.left
	            .attr('x1', 0)
	            .attr('y1', 0)
	            .attr('x2', 0)
	            .attr('y2', this._config.dimension.height() + 35);
	    };
	    return Grid;
	}());
	exports.default = Grid;
	function formatSecond(date) { return moment(date).format("LTS"); }
	function formatMinute(date) { return moment(date).format("LT"); }
	function formatWeek(date) { return moment(date).format("D MMM"); }
	function formatMonth(date) { return moment(date).format("MMMM"); }
	function formatYear(date) { return moment(date).format("YYYY"); }


/***/ }),
/* 19 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Type;
	(function (Type) {
	    Type[Type["percent"] = 0] = "percent";
	    Type[Type["number"] = 1] = "number";
	})(Type = exports.Type || (exports.Type = {}));


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var config_dimension_1 = __webpack_require__(21);
	var axis_config_1 = __webpack_require__(22);
	var tick_config_1 = __webpack_require__(23);
	var GridConfig = (function () {
	    function GridConfig() {
	        this.dimension = new config_dimension_1.default();
	        this.axis = {
	            top: new axis_config_1.default(),
	            right: new axis_config_1.default(),
	            bottom: new axis_config_1.default(),
	            left: new axis_config_1.default()
	        };
	        this.ticks = {
	            tickFormat: function (d, extrema) {
	                if (extrema < 10 || (d < 1 && d > -1)) {
	                    return d3.format('.2f')(d);
	                }
	                else {
	                    return d3.format('.3s')(d);
	                }
	            },
	            topTick: new tick_config_1.default(),
	            middleTicks: [new tick_config_1.default(), new tick_config_1.default(), new tick_config_1.default()],
	            bottomTick: new tick_config_1.default()
	        };
	        // Config constructor
	    }
	    return GridConfig;
	}());
	exports.default = GridConfig;


/***/ }),
/* 21 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var ConfigDimension = (function () {
	    function ConfigDimension() {
	        this._x = 0;
	        this._y = 0;
	        this._width = 0;
	        this._height = 0;
	        // Config constructor
	    }
	    ConfigDimension.prototype.containsCoords = function (coords, augmentations) {
	        if (augmentations === void 0) { augmentations = new ConfigDimension(); }
	        return (((this._x + augmentations.x()) <= coords[0] && (((this._x + augmentations.x()) + (this._width + augmentations.width()) >= coords[0]))) &&
	            ((this._y + augmentations.y()) <= coords[1] && (((this._y + augmentations.y()) + (this._height + augmentations.height()) >= coords[1]))));
	    };
	    ConfigDimension.prototype.x = function (value) {
	        if (value === undefined) {
	            return this._x;
	        }
	        this._x = value;
	        return this;
	    };
	    ConfigDimension.prototype.y = function (value) {
	        if (value === undefined) {
	            return this._y;
	        }
	        this._y = value;
	        return this;
	    };
	    ConfigDimension.prototype.width = function (value) {
	        if (value === undefined) {
	            return this._width;
	        }
	        this._width = value;
	        return this;
	    };
	    ConfigDimension.prototype.height = function (value) {
	        if (value === undefined) {
	            return this._height;
	        }
	        this._height = value;
	        return this;
	    };
	    return ConfigDimension;
	}());
	exports.default = ConfigDimension;


/***/ }),
/* 22 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var AxisConfig = (function () {
	    function AxisConfig() {
	        this.visible = true;
	        // Config constructor
	    }
	    return AxisConfig;
	}());
	exports.default = AxisConfig;


/***/ }),
/* 23 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var TickConfig = (function () {
	    function TickConfig() {
	        this.visible = true;
	        // Config constructor
	    }
	    return TickConfig;
	}());
	exports.default = TickConfig;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var config_dimension_1 = __webpack_require__(21);
	var grid_config_1 = __webpack_require__(20);
	var time_config_1 = __webpack_require__(25);
	var interaction_config_1 = __webpack_require__(27);
	var PlotConfig = (function () {
	    function PlotConfig() {
	        this.dimension = new config_dimension_1.default();
	        this.grid = new grid_config_1.default();
	        this.time = new time_config_1.default();
	        this.interaction = new interaction_config_1.default();
	    }
	    return PlotConfig;
	}());
	exports.default = PlotConfig;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var config_1 = __webpack_require__(26);
	var TimeConfig = (function (_super) {
	    __extends(TimeConfig, _super);
	    function TimeConfig() {
	        return _super !== null && _super.apply(this, arguments) || this;
	    }
	    TimeConfig.prototype.timeframe = function (value) {
	        if (value === undefined) {
	            return this._timeframe;
	        }
	        this._timeframe = value;
	        this._isDefault = false;
	        return this;
	    };
	    return TimeConfig;
	}(config_1.default));
	exports.default = TimeConfig;


/***/ }),
/* 26 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Config = (function () {
	    function Config() {
	        this._isDefault = true;
	    }
	    Config.prototype.isDefault = function (value) {
	        if (value === undefined) {
	            return this._isDefault;
	        }
	        this._isDefault = value;
	        return this;
	    };
	    return Config;
	}());
	exports.default = Config;
	function coordsInBBox(coords, bbox) {
	    return (((bbox.x) <= coords[0] && (((bbox.x) + (bbox.width) >= coords[0]))) &&
	        ((bbox.y) <= coords[1] && (((bbox.y) + (bbox.height) >= coords[1]))));
	}
	exports.coordsInBBox = coordsInBBox;
	function getTranslationCoords(selection) {
	    var matches = selection.attr('transform').match(/translate\((.*),(.*)\)/) || selection.attr('transform').match(/translate\((.*) (.*)\)/) || [0, 0, 0];
	    matches.shift();
	    return matches;
	}
	exports.getTranslationCoords = getTranslationCoords;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var config_1 = __webpack_require__(26);
	var InteractionConfig = (function (_super) {
	    __extends(InteractionConfig, _super);
	    function InteractionConfig() {
	        var _this = _super !== null && _super.apply(this, arguments) || this;
	        _this._showTimeInteraction = true;
	        _this._inspectionInteraction = true;
	        return _this;
	    }
	    InteractionConfig.prototype.showTimeInteraction = function (value) {
	        if (value === undefined) {
	            return this._showTimeInteraction;
	        }
	        this._showTimeInteraction = value;
	        this._isDefault = false;
	        return this;
	    };
	    InteractionConfig.prototype.inspectionInteraction = function (value) {
	        if (value === undefined) {
	            return this._inspectionInteraction;
	        }
	        this._inspectionInteraction = value;
	        this._isDefault = false;
	        return this;
	    };
	    return InteractionConfig;
	}(config_1.default));
	exports.default = InteractionConfig;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path='../../../../ref.d.ts' />
	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var Mark = __webpack_require__(7);
	var MarkLine = (function (_super) {
	    __extends(MarkLine, _super);
	    function MarkLine() {
	        var _this = _super !== null && _super.apply(this, arguments) || this;
	        _this._line = d3.line();
	        return _this;
	    }
	    MarkLine.prototype.destroy = function () {
	        var _this = this;
	        // Test animation
	        var totalLength = ((this._path.node().hasOwnProperty('getTotalLength')) ? this._path.node().getTotalLength() : 0);
	        this._path
	            .attr('stroke-dasharray', totalLength + ' ' + totalLength)
	            .attr('stroke-dashoffset', 0)
	            .transition()
	            .duration(500)
	            .ease(d3.easeLinear)
	            .attr('stroke-dashoffset', totalLength)
	            .on('end', function () {
	            _this.target().selectAll('.ps-highlight-data').remove();
	            _this._path
	                .remove();
	        });
	    };
	    MarkLine.prototype.highlightData = function (display, data, dataIndex, series, domain) {
	        var id = series.id.replace(/\./g, '').replace(/\-/g, '');
	        if (display) {
	            var highlights = this._target.selectAll('.ps-highlight-' + id).data(data);
	            highlights.enter().append('circle')
	                .classed('ps-highlight-data', true)
	                .classed('ps-highlight-' + id, true)
	                .classed(series.colorClass, true);
	            highlights.exit().remove();
	            highlights
	                .classed('sw-hidden', false)
	                .transition()
	                .duration(100)
	                .ease(d3.easeLinear)
	                .attr('cx', function (d) { return domain.x(d[domain.accessorKeys.x]); })
	                .attr('cy', function (d) { return domain.y(d[domain.accessorKeys.y]); })
	                .attr('r', 3);
	        }
	        else {
	            this._target.selectAll('.ps-highlight-' + id).classed('sw-hidden', true);
	        }
	        return this;
	    };
	    MarkLine.prototype.updateMark = function (series, domain) {
	        var _this = this;
	        _super.prototype.updateMark.call(this, series, domain);
	        var id = series.id.replace(/\./g, '').replace(/\-/g, '');
	        this._line
	            .x(function (d) { return domain.x(d[domain.accessorKeys.x]); })
	            .y(function (d) {
	            return domain.y(d[domain.accessorKeys.y]);
	        });
	        // Plot point if only one data point available (no line would be rendered)
	        if (series.data.length === 1) {
	            var circles = this._target.selectAll('.ps-point-' + id).data(series.data);
	            circles.enter().append('circle')
	                .classed('ps-initial-point', true)
	                .classed('ps-point-' + id, true);
	            circles
	                .classed(series.colorClass, true)
	                .attr('cx', function (d) { return domain.x(d[domain.accessorKeys.x]); })
	                .attr('cy', function (d) { return domain.y(d[domain.accessorKeys.y]); })
	                .attr('r', 4);
	        }
	        else {
	            this._target.selectAll('.ps-point-' + id).remove();
	        }
	        if (this._path) {
	            this._path
	                .data([series.data])
	                .attr('d', this._line);
	        }
	        else {
	            this._path = this._target.append('path');
	            this._path
	                .data([series.data])
	                .attr('class', 'sw-line')
	                .classed(this._className, true)
	                .attr('d', this._line);
	            // Test animation
	            var totalLength = ((this._path.node().hasOwnProperty('getTotalLength')) ? this._path.node().getTotalLength() : 0);
	            this._path
	                .attr('stroke-dasharray', totalLength + ' ' + totalLength)
	                .attr('stroke-dashoffset', totalLength)
	                .transition()
	                .delay(250)
	                .duration(750)
	                .ease(d3.easeLinear)
	                .attr('stroke-dashoffset', 0)
	                .on('end', function () {
	                _this._path
	                    .attr('stroke-dasharray', null)
	                    .attr('stroke-dashoffset', null);
	            });
	        }
	    };
	    return MarkLine;
	}(Mark.default));
	exports.default = MarkLine;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var mark_1 = __webpack_require__(7);
	var MarkStatus = (function (_super) {
	    __extends(MarkStatus, _super);
	    function MarkStatus() {
	        return _super !== null && _super.apply(this, arguments) || this;
	    }
	    MarkStatus.prototype.destroy = function () {
	        // Remove elements gracefully
	    };
	    MarkStatus.prototype.highlightData = function (display, data, dataIndex, series, domain) {
	        var _this = this;
	        if (display) {
	            var y_1 = (domain.y.range()[0] - domain.y.range()[1]) / 2;
	            var highlights = this._target.selectAll('.ps-highlight-status-data').data(data);
	            highlights.enter().append('rect')
	                .classed('ps-highlight-status-data', true);
	            highlights.exit().remove();
	            highlights
	                .attr('class', function (d) { return 'sw-status-mark-' + d[domain.accessorKeys.y]; })
	                .classed('ps-highlight-status-data', true)
	                .classed('sw-hidden', false)
	                .attr('x', function (d) { return (domain.x(d[domain.accessorKeys.x]) - (_this.calcRectWidth(d, domain, series) + 2)); })
	                .transition()
	                .duration(100)
	                .ease(d3.easeLinear)
	                .attr('y', function (d) {
	                return y_1 - (Math.floor(_this.calcRectHeight(d, domain) + 10) / 2);
	            })
	                .attr('height', function (d) {
	                return _this.calcRectHeight(d, domain) + 10;
	            })
	                .attr('width', function (d) { return _this.calcRectWidth(d, domain, series) + 4; });
	            highlights.moveToFront();
	        }
	        else {
	            this._target.selectAll('.ps-highlight-status-data').classed('sw-hidden', true);
	        }
	        return this;
	    };
	    MarkStatus.prototype.updateMark = function (series, domain) {
	        _super.prototype.updateMark.call(this, series, domain);
	        var rects = this._target
	            .selectAll('.ps-status-rect')
	            .data(series.data);
	        rects.exit().remove();
	        this.updateRects(rects.enter().append('rect').classed('ps-status-rect', true), domain, series, true);
	        this.updateRects(rects, domain, series);
	        this._target.selectAll('.sw-status-mark-1').moveToBack();
	    };
	    MarkStatus.prototype.calcRectWidth = function (d, domain, series) {
	        var timeProportion = moment(domain.x.domain()[1]).diff(moment(domain.x.domain()[0]), 'seconds') / d.resolution;
	        return (domain.x.range()[1] - domain.x.range()[0]) / timeProportion;
	    };
	    MarkStatus.prototype.calcRectHeight = function (d, domain) {
	        return (d[domain.accessorKeys.y] === 1 || d[domain.accessorKeys.y] === 22) ? 3 : 17;
	    };
	    MarkStatus.prototype.updateRects = function (rects, domain, series, transition) {
	        var _this = this;
	        if (transition === void 0) { transition = false; }
	        var y = (domain.y.range()[0] - domain.y.range()[1]) / 2;
	        var delay = (transition) ? 250 : 0;
	        var duration = (transition) ? 750 : 0;
	        var result = rects
	            .attr('y', function (d) {
	            return y - Math.floor(_this.calcRectHeight(d, domain) / 2);
	        })
	            .attr('height', function (d) {
	            return _this.calcRectHeight(d, domain);
	        })
	            .attr('x', function (d) { return (domain.x(d[domain.accessorKeys.x]) - _this.calcRectWidth(d, domain, series)); })
	            .attr('class', function (d) { return 'sw-status-mark-' + d[domain.accessorKeys.y]; })
	            .classed('ps-status-rect', true)
	            .transition()
	            .delay(delay)
	            .duration(duration)
	            .ease(d3.easeLinear)
	            .attr('width', function (d) { return _this.calcRectWidth(d, domain, series); });
	        return result;
	    };
	    return MarkStatus;
	}(mark_1.default));
	exports.default = MarkStatus;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var mark_1 = __webpack_require__(7);
	var MarkAlert = (function (_super) {
	    __extends(MarkAlert, _super);
	    function MarkAlert() {
	        return _super !== null && _super.apply(this, arguments) || this;
	    }
	    MarkAlert.prototype.destroy = function () {
	        // Remove elements gracefully
	    };
	    MarkAlert.prototype.highlightData = function (display, data, dataIndex, series, domain) {
	        var _this = this;
	        if (display) {
	            var transition_1 = d3.transition('ps-chart-interaction-highlights')
	                .duration(100)
	                .ease(d3.easeLinear);
	            this.renderHighlight(display, data, dataIndex, series, domain, 0, transition_1);
	            _.forEach(series.subSeries, function (subSeries, index) {
	                _this.renderHighlight(display, data, dataIndex, subSeries, domain, index + 1, transition_1);
	            });
	        }
	        else {
	            this._target.selectAll('.ps-highlight-alert-data').classed('sw-hidden', true);
	        }
	        return this;
	    };
	    MarkAlert.prototype.updateMark = function (series, domain) {
	        var _this = this;
	        _super.prototype.updateMark.call(this, series, domain);
	        // this._target.append('line')
	        //   .classed('ps-alert-line',true)
	        //   // .classed(pathClass,true)
	        //   .attr('x1',0)
	        //   .attr('x2',domain.x.range()[1])
	        //   .attr('y1',(d:any, offsetIndex:number) => this.calcPlotLineHeight(0))
	        //   .attr('y2',(d:any, offsetIndex:number) => this.calcPlotLineHeight(0))
	        //   .lower();
	        var pathClass = 'ps-alert-line' + series.id.replace(/\./g, '').replace(/\-/g, '');
	        var data = [series].concat(series.subSeries);
	        var lines = this._target.selectAll('.' + pathClass)
	            .data(data, function (d) {
	            return d.id;
	        });
	        // console.log('plotLines',paths.enter().size(),paths.exit().size(),paths.size(), data);
	        lines.exit().remove();
	        lines.enter().append('line')
	            .classed('ps-alert-line', true)
	            .classed(pathClass, true)
	            .attr('x1', 0)
	            .attr('x2', domain.x.range()[1])
	            .attr('y1', function (d, offsetIndex) {
	            return _this.calcPlotLineHeight(offsetIndex);
	        })
	            .attr('y2', function (d, offsetIndex) {
	            return _this.calcPlotLineHeight(offsetIndex);
	        }).lower();
	        lines
	            .classed('ps-alert-line', true)
	            .classed(pathClass, true)
	            .attr('x1', 0)
	            .attr('x2', domain.x.range()[1])
	            .attr('y1', function (d, offsetIndex) {
	            return _this.calcPlotLineHeight(offsetIndex);
	        })
	            .attr('y2', function (d, offsetIndex) {
	            return _this.calcPlotLineHeight(offsetIndex);
	        }).lower();
	        var className = 'ps-alert-' + series.id.replace(/\./g, '').replace(/\-/g, '');
	        var rects = this._target
	            .selectAll('.' + className)
	            .data(series.data);
	        rects.exit().remove();
	        this.updateRects(rects.enter().append('rect').classed('ps-alert-' + series.id, true), domain, series, className, 0, true);
	        this.updateRects(rects, domain, series, className, 0);
	        var subSeriesClassName = 'ps-sub-alert';
	        var subGroups = this._target
	            .selectAll('.' + subSeriesClassName)
	            .data(series.subSeries, function (d) {
	            return d.id;
	        });
	        subGroups.enter().append('g')
	            .attr('class', function (d) {
	            return subSeriesClassName + d.id.replace(/\./g, '').replace(/\-/g, '');
	        })
	            .classed(subSeriesClassName, true);
	        subGroups.exit().remove();
	        subGroups
	            .attr('class', function (d) {
	            return subSeriesClassName + d.id.replace(/\./g, '').replace(/\-/g, '');
	        })
	            .classed(subSeriesClassName, true);
	        // console.log('subGroups',subGroups.enter().size(),subGroups.exit().size(),subGroups.size());
	        _.forEach(series.subSeries, function (subSeries, index) {
	            var subClassName = 'ps-alert-' + subSeries.id.replace(/\./g, '').replace(/\-/g, '');
	            var targetClassname = subSeriesClassName + subSeries.id.replace(/\./g, '').replace(/\-/g, '');
	            var target = _this._target.select('.' + targetClassname);
	            // console.log('subgroup target', target.size(), targetClassname);
	            var subRects = target
	                .selectAll('.' + subClassName)
	                .data(subSeries.data);
	            subRects.exit().remove();
	            _this.updateRects(subRects.enter().append('rect'), domain, subSeries, subClassName, index + 1, true);
	            _this.updateRects(subRects, domain, subSeries, subClassName, index + 1);
	        });
	    };
	    MarkAlert.prototype.renderHighlight = function (display, data, dataIndex, series, domain, seriesIndex, transition) {
	        var _this = this;
	        var y = this.calcPlotLineHeight(seriesIndex);
	        var isSubSeries = (seriesIndex > 0);
	        var className = 'ps-highlight-alert' + series.id.replace(/\./g, '').replace(/\-/g, '');
	        if (isSubSeries) {
	            data = [series.data[dataIndex]];
	        }
	        var highlights = this._target.selectAll('.' + className).data(data);
	        // console.log(className, highlights.enter().size(),highlights.remove().size(),highlights.size(), data);
	        highlights.enter().append('rect')
	            .classed('ps-highlight-alert-data', true)
	            .classed(className, true)
	            .classed(series.colorClass, true);
	        highlights.exit().remove();
	        highlights
	            .attr('class', function (d) {
	            return 'sw-alert-mark-' + ((d[domain.accessorKeys.y] > 0) ? (isSubSeries) ? '' : 'active' : 'inactive');
	        })
	            .classed('ps-highlight-alert-data', true)
	            .classed(className, true)
	            .classed(series.colorClass, true)
	            .classed('sw-hidden', false)
	            .attr('x', function (d) { return (domain.x(d[domain.accessorKeys.x]) - (_this.calcRectWidth(d, domain, series) + 2)); })
	            .transition('ps-chart-interaction-highlights')
	            .duration(100)
	            .ease(d3.easeLinear)
	            .attr('y', function (d) {
	            return y - (Math.floor(_this.calcRectHeight(d, domain, isSubSeries) + 10) / 2);
	        })
	            .attr('height', function (d) {
	            return _this.calcRectHeight(d, domain, isSubSeries) + 10;
	        })
	            .attr('width', function (d) { return _this.calcRectWidth(d, domain, series) + 4; });
	        highlights.moveToFront();
	    };
	    MarkAlert.prototype.calcRectWidth = function (d, domain, series) {
	        var timeProportion = moment(domain.x.domain()[1]).diff(moment(domain.x.domain()[0]), 'seconds') / d.resolution;
	        return (domain.x.range()[1] - domain.x.range()[0]) / timeProportion;
	    };
	    MarkAlert.prototype.calcRectHeight = function (d, domain, isSubSeries) {
	        return (d[domain.accessorKeys.y] > 0) ? (isSubSeries) ? 10 : 23 : 0;
	    };
	    MarkAlert.prototype.calcPlotLineHeight = function (offsetIndex) {
	        var result; //(((domain.y.range()[0] - domain.y.range()[1]) / (1 + series.subSeries.length))/2) * ((offsetIndex + offsetIndex)+ 1);
	        var aggregateOffset = 45;
	        var subseriesOffset = 17.10;
	        if (offsetIndex > 0) {
	            result = aggregateOffset + 8 + (subseriesOffset * (offsetIndex - 1)); //((((domain.y.range()[0] - aggregateOffset) - domain.y.range()[1] ) / (series.subSeries.length))/2) * ((offsetIndex)+ 1);
	        }
	        else {
	            result = aggregateOffset / 2;
	        }
	        return result;
	    };
	    MarkAlert.prototype.updateRects = function (rects, domain, series, className, offsetIndex, transition) {
	        var _this = this;
	        if (transition === void 0) { transition = false; }
	        var y = this.calcPlotLineHeight(offsetIndex);
	        var delay = (transition) ? 250 : 0;
	        var duration = (transition) ? 750 : 0;
	        var result = rects
	            .attr('y', function (d) {
	            return y - Math.floor(_this.calcRectHeight(d, domain, (offsetIndex > 0)) / 2);
	        })
	            .attr('height', function (d) {
	            return _this.calcRectHeight(d, domain, (offsetIndex > 0));
	        })
	            .attr('class', function (d) {
	            if (offsetIndex > 0) {
	                return className + ' ' + series.colorClass;
	            }
	            else {
	                return className + ' sw-alert-mark-' + ((d[domain.accessorKeys.y] > 0) ? 'active' : 'inactive');
	            }
	        })
	            .attr('x', function (d) {
	            return (domain.x(d[domain.accessorKeys.x]) - _this.calcRectWidth(d, domain, series));
	        })
	            .transition()
	            .delay(delay)
	            .duration(duration)
	            .ease(d3.easeLinear)
	            .attr('width', function (d) { return _this.calcRectWidth(d, domain, series); });
	        return result;
	    };
	    return MarkAlert;
	}(mark_1.default));
	exports.default = MarkAlert;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path='../../../../ref.d.ts' />
	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var Mark = __webpack_require__(7);
	var MarkArea = (function (_super) {
	    __extends(MarkArea, _super);
	    function MarkArea() {
	        var _this = _super !== null && _super.apply(this, arguments) || this;
	        _this._area = d3.area();
	        _this._introComplete = false;
	        return _this;
	    }
	    MarkArea.prototype.destroy = function () {
	        // out animation
	    };
	    MarkArea.prototype.highlightData = function (display, data, dataIndex, series, domain) {
	        var _this = this;
	        if (display) {
	            _.forEach(this._target.selectAll('path').data(), function (pathData, index) {
	                var bucketData = pathData[data[0]];
	                // Get sube series stacked in reverse order
	                var subSeries = series.subSeries[series.subSeries.length - (index + 1)];
	                // HACK: Selector doesn't like dots and dashes in the id (probably just need fresh eyes)
	                var id = subSeries.id.replace(/\./g, '').replace(/\-/g, '');
	                var highlights = _this._target.selectAll('.ps-highlight-' + id).data([{ date: bucketData.data.date, value: bucketData[1] }]);
	                highlights.enter().append('circle')
	                    .classed('ps-area ps-highlight-data', true)
	                    .classed('ps-highlight-' + id, true)
	                    .classed(subSeries.colorClass, true);
	                highlights.exit().remove();
	                highlights
	                    .classed('sw-hidden', false)
	                    .transition()
	                    .duration(100)
	                    .ease(d3.easeLinear)
	                    .attr('cx', function (d) { return domain.x(d[domain.accessorKeys.x]); })
	                    .attr('cy', function (d) { return domain.y(d[domain.accessorKeys.y]); })
	                    .attr('r', 3);
	            });
	        }
	        else {
	            this._target.selectAll('.ps-area.ps-highlight-data').classed('sw-hidden', true);
	        }
	        return this;
	    };
	    MarkArea.prototype.updateMark = function (series, domain) {
	        _super.prototype.updateMark.call(this, series, domain);
	        var stackedData;
	        if (series.subSeries != null) {
	            stackedData = this.pivotSeriesToStackData(series.subSeries, domain);
	        }
	        var self = this;
	        var startArea = d3.area()
	            .x(function (d) {
	            return domain.x(d.data[domain.accessorKeys.x]);
	        })
	            .y0(function (d) {
	            return domain.y(0);
	        })
	            .y1(function (d) {
	            return domain.y(0);
	        });
	        this._area
	            .x(function (d) {
	            return domain.x(d.data[domain.accessorKeys.x]);
	        })
	            .y0(function (d) {
	            return domain.y(d[0]);
	        })
	            .y1(function (d) {
	            return domain.y(d[1]);
	        });
	        this._paths = this._target.selectAll('path').data(stackedData);
	        this._paths.enter()
	            .append('path')
	            .attr('class', function (d) {
	            return d.data.colorClass;
	        })
	            .classed('ps-area', true)
	            .attr('d', startArea)
	            .transition()
	            .delay(250)
	            .duration(750)
	            .ease(d3.easeQuadInOut)
	            .attr('d', this._area)
	            .on('end', function () {
	            self._introComplete = true;
	        });
	        this._paths.exit().remove();
	        if (this._introComplete) {
	            this._paths
	                .attr('class', function (d) {
	                return d.data.colorClass;
	            })
	                .classed('ps-area', true)
	                .attr('d', this._area);
	        }
	        ;
	        // if (this._layers) {
	        //   this._layers
	        //     .data(stackedData);
	        //   if(this._introComplete) {
	        //     this._layers.selectAll('path')
	        //       .attr('d', this._area);
	        //   }
	        // }else {
	        //   this._layers = this._target.selectAll('.layer')
	        //     .data(stackedData)
	        //     .enter().append('g')
	        //     .attr('class', 'layer');
	        //   this._layers.append('path')
	        //     .attr('d', startArea)
	        //     .attr('class', (d: any) => {
	        //       return d.data.colorClass;
	        //     })
	        //     .classed('ps-area', true)
	        //     .transition()
	        //     .delay(250)
	        //     .duration(750)
	        //     .ease(d3.easeQuadInOut)
	        //     .attr('d', this._area)
	        //     .on('end', () => {
	        //       self._introComplete = true;
	        //     });
	        //
	        // }
	    };
	    MarkArea.prototype.pivotSeriesToStackData = function (seriesList, domain) {
	        var stack = d3.stack();
	        var pivotData = [];
	        var keys = {}; //_.keys(seriesList);
	        var orderedKeys = [];
	        // Pivot the series list into timebucket layers
	        if (seriesList[0]) {
	            _.forEach(seriesList[0].data, function (data, index) {
	                var timeBucket = {};
	                timeBucket[domain.accessorKeys.x] = data[domain.accessorKeys.x];
	                pivotData.push(timeBucket); // add time layer
	                _.forEach(seriesList, function (series) {
	                    keys[series.id] = series.colorClass;
	                    // Add each series data point to the time layer
	                    timeBucket[series.id] = (series.isPercentile) ? series.data[index][domain.accessorKeys.y] / seriesList.length : series.data[index][domain.accessorKeys.y];
	                });
	            });
	        }
	        _.forEach(seriesList, function (series) {
	            orderedKeys.push(series.id);
	        });
	        stack.keys(_.reverse(orderedKeys));
	        var result = stack(pivotData);
	        _.forEach(result, function (seriesLayer) {
	            seriesLayer.data = {
	                colorClass: keys[seriesLayer.key]
	            };
	        });
	        return result;
	    };
	    return MarkArea;
	}(Mark.default));
	exports.default = MarkArea;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var mark_1 = __webpack_require__(7);
	var series_1 = __webpack_require__(6);
	var MarkBarStacked = (function (_super) {
	    __extends(MarkBarStacked, _super);
	    function MarkBarStacked() {
	        return _super !== null && _super.apply(this, arguments) || this;
	    }
	    MarkBarStacked.prototype.destroy = function () {
	        _super.prototype.destroy.call(this);
	    };
	    MarkBarStacked.prototype.highlightData = function (display, data, dataIndex, series, domain) {
	        if (display && _.isNumber(dataIndex)) {
	            var seriesClone_1 = new series_1.Series(series);
	            seriesClone_1.data = seriesClone_1.data.slice(dataIndex, dataIndex + 1);
	            seriesClone_1.subSeries = seriesClone_1.subSeries.slice(0, seriesClone_1.subSeries.length);
	            _.forEach(seriesClone_1.subSeries, function (subSeries, index) {
	                seriesClone_1.subSeries[index] = subSeries = new series_1.Series(subSeries);
	                subSeries.data = subSeries.data.slice(dataIndex, dataIndex + 1);
	            });
	            var stackedData = this.stackData(seriesClone_1, domain);
	            var transition = d3.transition(null)
	                .duration(100)
	                .ease(d3.easeLinear);
	            var outline = this._target.selectAll('.ps-highlight-outline').data(seriesClone_1.data);
	            this.updateOutline(outline.enter().append('rect').attr('class', 'ps-highlight-outline'), seriesClone_1, series, domain, 6, transition);
	            this.updateOutline(outline, seriesClone_1, series, domain, 6, transition);
	            var highlights = this._target.selectAll('.ps-highlight').data(stackedData[0].measurments);
	            this.updateRects(highlights.enter().append('rect').attr('class', function (d) { return 'ps-highlight ps-bar ' + d.colorClass; }), series, domain, 6, transition);
	            highlights.attr('class', function (d) { return 'ps-highlight ps-bar ' + d.colorClass; });
	            this.updateRects(highlights, series, domain, 6, transition);
	            highlights.exit().remove();
	            highlights.classed('sw-hidden', false);
	            highlights.moveToFront();
	            outline.classed('sw-hidden', false);
	        }
	        else {
	            this._target.selectAll('.ps-highlight').classed('sw-hidden', true);
	            this._target.selectAll('.ps-highlight-outline').classed('sw-hidden', true);
	        }
	        return this;
	    };
	    MarkBarStacked.prototype.updateMark = function (series, domain) {
	        _super.prototype.updateMark.call(this, series, domain);
	        var stackedData = this.stackData(series, domain);
	        var groups = this._target.selectAll('.ps-bar-bucket').data(stackedData);
	        var newGroups = groups.enter().append('g')
	            .classed('ps-bar-bucket', true);
	        groups.exit().remove();
	        if (newGroups.size() > 0) {
	            var rects = newGroups.selectAll('rect').data(function (d) { return d.measurments; });
	            this.startRects(rects.enter().append('rect')
	                .attr('class', function (d) { return 'ps-bar ' + d.colorClass; }), series, domain);
	            rects.attr('class', function (d) { return 'ps-bar ' + d.colorClass; });
	            var transition = d3.transition(null)
	                .delay(250)
	                .duration(750)
	                .ease(d3.easeQuadInOut);
	            this.updateRects(newGroups.selectAll('rect'), series, domain, 0, transition);
	            rects.exit().remove();
	            this.updateRects(rects, series, domain);
	        }
	        if (groups.size() > 0) {
	            var rects = groups.selectAll('rect').data(function (d) { return d.measurments; });
	            this.updateRects(rects.enter().append('rect')
	                .attr('class', function (d) { return 'ps-bar ' + d.colorClass; }), series, domain);
	            rects.exit().remove();
	            rects.attr('class', function (d) { return 'ps-bar ' + d.colorClass; });
	            this.updateRects(rects, series, domain);
	        }
	    };
	    MarkBarStacked.prototype.updateOutline = function (rect, highlightSeries, series, domain, widthAdditive, transition) {
	        var _this = this;
	        if (widthAdditive === void 0) { widthAdditive = 0; }
	        rect
	            .attr('x', function (d) { return (domain.x(d[domain.accessorKeys.x]) - (_this.calcRectWidth(d, domain, series) + (widthAdditive / 2))); })
	            .attr('width', function (d) { return _this.calcRectWidth(d, domain, series) + widthAdditive; })
	            .transition(transition)
	            .attr('y', function (d) {
	            return domain.y(d[domain.accessorKeys.y]);
	        })
	            .attr('height', function (d) {
	            return domain.y.range()[0] - domain.y(d[domain.accessorKeys.y]);
	        });
	    };
	    MarkBarStacked.prototype.startRects = function (rects, series, domain) {
	        var _this = this;
	        rects
	            .attr('y', function (d, dataIndex) {
	            return domain.y.range()[0];
	        })
	            .attr('x', function (d) { return (domain.x(d.rawData[domain.accessorKeys.x]) - (_this.calcRectWidth(d.rawData, domain, series))); })
	            .attr('width', function (d) { return _this.calcRectWidth(d.rawData, domain, series); })
	            .attr('height', function (d) {
	            return 0;
	        });
	    };
	    MarkBarStacked.prototype.updateRects = function (rects, series, domain, widthAdditive, transition) {
	        var _this = this;
	        if (widthAdditive === void 0) { widthAdditive = 0; }
	        var updates = (transition) ? rects.transition(transition) : rects;
	        rects
	            .attr('x', function (d) { return (domain.x(d.rawData[domain.accessorKeys.x]) - (_this.calcRectWidth(d.rawData, domain, series) + (widthAdditive / 2))); })
	            .attr('width', function (d) { return _this.calcRectWidth(d.rawData, domain, series) + widthAdditive; });
	        updates
	            .attr('y', function (d, dataIndex) {
	            return (domain.y.range()[0] - d.range[0]) - (d.range[1] - d.range[0]);
	        })
	            .attr('height', function (d) {
	            return d.range[1] - d.range[0];
	        });
	    };
	    // Experimenting with different stack algorithm (need to abstract out and use common stacking/d3.stack)
	    MarkBarStacked.prototype.stackData = function (series, domain) {
	        var _this = this;
	        var stackedData = [];
	        _.forEach(series.data, function (dataItem, index) {
	            var data = {
	                measurments: [],
	                xValue: dataItem[domain.accessorKeys.x]
	            };
	            // Build stacked data from the "bottom to top"
	            _.forEachRight(series.subSeries, function (subSeries, subIndex) {
	                // let range = (subIndex === 0)? [0,this.calcRectHeight(subSeries.data[index], domain)] : [data.measurments[subIndex-1].range[1], data.measurments[subIndex-1].range[1] + this.calcRectHeight(subSeries.data[index],domain)]
	                var subSeriesCount = series.subSeries.length - 1;
	                var range = (subIndex === subSeriesCount) ? [0, _this.calcRectHeight(subSeries.data[index], domain)] : [data.measurments[subSeriesCount - (subIndex + 1)].range[1], data.measurments[subSeriesCount - (subIndex + 1)].range[1] + _this.calcRectHeight(subSeries.data[index], domain)];
	                data.measurments.push({ colorClass: subSeries.colorClass, range: range, rawData: subSeries.data[index] });
	            });
	            stackedData.push(data);
	        });
	        return stackedData;
	    };
	    MarkBarStacked.prototype.calcRectWidth = function (d, domain, series) {
	        var timeProportion = moment(domain.x.domain()[1]).diff(moment(domain.x.domain()[0]), 'seconds') / d.resolution;
	        return (domain.x.range()[1] - domain.x.range()[0]) / timeProportion;
	    };
	    MarkBarStacked.prototype.calcRectHeight = function (d, domain) {
	        return domain.y.range()[0] - domain.y(d[domain.accessorKeys.y]);
	    };
	    return MarkBarStacked;
	}(mark_1.default));
	exports.default = MarkBarStacked;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var mark_1 = __webpack_require__(7);
	var d3Helpers_1 = __webpack_require__(34);
	var MarkBar = (function (_super) {
	    __extends(MarkBar, _super);
	    function MarkBar() {
	        var _this = _super.call(this) || this;
	        _this._transitioningIn = true;
	        return _this;
	    }
	    MarkBar.prototype.updateMark = function (series, domain) {
	        var _this = this;
	        _super.prototype.updateMark.call(this, series, domain);
	        var bars = this._target.selectAll('rect.ps-bar').data(series.data);
	        var transition;
	        if (this._transitioningIn) {
	            transition = {
	                transition: this._target.transition()
	                    .duration(750)
	                    .delay(250)
	                    .ease(d3.easeLinear)
	                    .on('end', function () {
	                    _this._transitioningIn = false;
	                })
	            };
	        }
	        this._target.classed('ps-mark-bar', true);
	        bars.exit().remove();
	        bars = bars.enter().append('rect')
	            .classed('ps-bar', true)
	            .classed(series.colorClass, true)
	            .attr('y', function (d, dataIndex) {
	            return domain.y.range()[0];
	        })
	            .merge(bars);
	        this.updateBars(bars, series, domain, transition);
	    };
	    MarkBar.prototype.highlightData = function (display, data, dataIndex, series, domain) {
	        var highlights = this._target.selectAll('.ps-highlight-data').data(data);
	        if (display) {
	            var transition = {
	                duration: 100,
	                ease: d3.easeLinear
	            };
	            highlights.exit().remove();
	            highlights = highlights.enter().append('rect')
	                .classed('ps-highlight-data', true)
	                .classed(series.colorClass, true)
	                .merge(highlights)
	                .classed('sw-hidden', false);
	            this.updateBars(highlights, series, domain, transition, 6);
	        }
	        else {
	            this._target.selectAll('.ps-highlight-data')
	                .classed('sw-hidden', true);
	        }
	        return this;
	    };
	    MarkBar.prototype.destroy = function () {
	        _super.prototype.destroy.call(this);
	        this._target.selectAll('.ps-highlight-data').remove();
	        this._target.selectAll('rect.ps-bar').remove();
	        this._target.classed('ps-mark-bar', false);
	    };
	    MarkBar.prototype.updateBars = function (bars, series, domain, transitionConfig, widthModifier) {
	        var _this = this;
	        if (transitionConfig === void 0) { transitionConfig = null; }
	        if (widthModifier === void 0) { widthModifier = 0; }
	        var updateTransition = (transitionConfig) ? d3Helpers_1.applyTransitionConfig(bars, transitionConfig) : bars;
	        bars
	            .attr('x', function (d) { return (domain.x(d[domain.accessorKeys.x]) - (_this.calcBarWidth(d, domain, series) + (widthModifier / 2))); })
	            .attr('width', function (d) { return _this.calcBarWidth(d, domain, series) + widthModifier; });
	        updateTransition
	            .attr('y', function (d, dataIndex) {
	            return (domain.y.range()[0] - _this.calcBarHeight(d, domain));
	        })
	            .attr('height', function (d) {
	            return _this.calcBarHeight(d, domain);
	        });
	    };
	    MarkBar.prototype.calcBarWidth = function (d, domain, series) {
	        var timeProportion = moment(domain.x.domain()[1]).diff(moment(domain.x.domain()[0]), 'seconds') / d.resolution;
	        return (domain.x.range()[1] - domain.x.range()[0]) / timeProportion;
	    };
	    MarkBar.prototype.calcBarHeight = function (d, domain) {
	        return domain.y.range()[0] - domain.y(d[domain.accessorKeys.y]);
	    };
	    return MarkBar;
	}(mark_1.default));
	exports.default = MarkBar;


/***/ }),
/* 34 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function applyTransitionConfig(selection, config) {
	    var transition = (config.transition) ? selection.transition(config.transition) : selection.transition();
	    if (config.duration) {
	        transition.duration(config.duration);
	    }
	    if (config.delay) {
	        transition.delay(config.delay);
	    }
	    if (config.ease) {
	        transition.ease(config.ease);
	    }
	    if (config.on) {
	        _.forEach(config.on, function (callback, key) {
	            transition.on(key, callback);
	        });
	    }
	    return transition;
	}
	exports.applyTransitionConfig = applyTransitionConfig;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var layout_config_1 = __webpack_require__(36);
	var config_dimension_1 = __webpack_require__(21);
	var series_1 = __webpack_require__(6);
	var Layout = (function () {
	    function Layout() {
	        this._plots = [];
	        this._config = new layout_config_1.default();
	        this._plotBoxes = {};
	    }
	    Layout.prototype.config = function (value) {
	        if (value === undefined) {
	            return this._config;
	        }
	        this._config = value;
	        // this.updateLayout();
	        return this;
	    };
	    Layout.prototype.totalHeight = function () {
	        return this._totalHeight;
	    };
	    Layout.prototype.getOrderedPlots = function () {
	        return this._plots;
	    };
	    Layout.prototype.addPlot = function (plot) {
	        this._plots.push(plot);
	        // this.updateLayout();
	        return this;
	    };
	    Layout.prototype.removePlot = function (plotId) {
	        this._plots.splice(_.findIndex(this._plots, function (plot) { return plot.id() === plotId; }), 1);
	        delete this._plotBoxes[plotId];
	        // this.updateLayout();
	        return this;
	    };
	    Layout.prototype.getPlotIdByCoords = function (coords) {
	        var _loop_1 = function (plotId) {
	            if (this_1._plotBoxes.hasOwnProperty(plotId)) {
	                var augmentations = new config_dimension_1.default();
	                var plotIndex = _.findIndex(this_1._plots, function (plot) {
	                    return plot.id() === plotId;
	                });
	                var plotType = _.values(this_1._plots[plotIndex].series())[0].type;
	                // Adjust for plot spacing for gauge/multi or axis on last plot
	                if (plotType === series_1.Type.gauge || plotType === series_1.Type.multi || plotType === series_1.Type.event || plotType === series_1.Type.dpaWaitTime || plotIndex === (this_1._plots.length - 1)) {
	                    augmentations.height(20);
	                }
	                if (this_1._plotBoxes[plotId].containsCoords(coords, augmentations)) {
	                    return { value: plotId };
	                }
	            }
	        };
	        var this_1 = this;
	        for (var plotId in this._plotBoxes) {
	            var state_1 = _loop_1(plotId);
	            if (typeof state_1 === "object")
	                return state_1.value;
	        }
	        return null;
	    };
	    Layout.prototype.plotBox = function (plotId) {
	        return this._plotBoxes[plotId];
	    };
	    Layout.prototype.update = function () {
	        this.updateLayout();
	        return this;
	    };
	    /**
	     * Group by non-state in order added, then states in order added
	     */
	    Layout.prototype.updateLayout = function (fitRatio) {
	        var _this = this;
	        if (fitRatio === void 0) { fitRatio = 1; }
	        if (this._plots.length === 0) {
	            return;
	        }
	        var nonStates = [];
	        var states = [];
	        var totalHeight = 0;
	        var gapCount = 0;
	        var statePlotsOffset = 0;
	        // group plots
	        _.forEach(this._plots, function (plot) {
	            plot.isLastInLayout(false);
	            switch (_.values(plot.series())[0].type) {
	                case series_1.Type.gauge:
	                case series_1.Type.multi:
	                case series_1.Type.event:
	                case series_1.Type.dpaWaitTime:
	                    nonStates.push(plot);
	                    break;
	                default:
	                    states.push(plot);
	                    break;
	            }
	        });
	        _.forEach(nonStates, function (plot, index) {
	            var box = new config_dimension_1.default();
	            var y = totalHeight;
	            var height;
	            var growthHeight;
	            var plotSeries = _.values(plot.series());
	            //grow multi by sub series and gauge by series
	            switch (plotSeries[0].type) {
	                case series_1.Type.multi:
	                case series_1.Type.event:
	                case series_1.Type.dpaWaitTime:
	                    growthHeight = 63 + plotSeries[0].subSeries.length * 20; //this._config.growthIncrement(); //magic numbers for chart growth until approved
	                    height = (growthHeight > _this._config.defaultPlotHeights().multi) ? growthHeight : _this._config.defaultPlotHeights().multi;
	                    break;
	                case series_1.Type.gauge:
	                    growthHeight = plotSeries.length * _this._config.growthIncrement();
	                    height = (growthHeight > _this._config.defaultPlotHeights().gauge) ? growthHeight : _this._config.defaultPlotHeights().gauge;
	                    break;
	            }
	            height = (height - _this._config.plotSpacing()) * fitRatio;
	            box
	                .x(_this._config.translationOffsets()[0])
	                .y(y + _this._config.translationOffsets()[1])
	                .width(_this._config.dimension().width())
	                .height(height);
	            totalHeight += height + _this._config.plotSpacing();
	            gapCount++;
	            _this._plotBoxes[plot.id()] = box;
	        });
	        // unbound state plots
	        if (nonStates.length > 0) {
	            statePlotsOffset = totalHeight; //this._config.plotSpacing() + this._plotBoxes[nonStates[nonStates.length - 1].id()].y() + this._plotBoxes[nonStates[nonStates.length - 1].id()].height();
	        }
	        _.forEach(states, function (plot, index) {
	            var box = new config_dimension_1.default();
	            var y = totalHeight; //(index === 0) ? statePlotsOffset : this._plotBoxes[states[index - 1].id()].y() + this._plotBoxes[states[index - 1].id()].height();
	            var height;
	            var growthHeight = 0;
	            var plotSeries = _.values(plot.series());
	            switch (plotSeries[0].type) {
	                case series_1.Type.alert:
	                    growthHeight = 45 + 8 + (plotSeries[0].subSeries.length) * 17;
	                    height = (growthHeight > _this._config.defaultPlotHeights().alert) ? growthHeight : _this._config.defaultPlotHeights().alert;
	                    break;
	                default:
	                    height = _this._config.defaultPlotHeights().status * fitRatio;
	            }
	            box
	                .x(0)
	                .y(y)
	                .width(_this._config.dimension().width())
	                .height(height);
	            totalHeight += height;
	            _this._plotBoxes[plot.id()] = box;
	        });
	        // Allow for final state plot gap for axis
	        if (states.length > 0) {
	            totalHeight += this._config.plotSpacing();
	        }
	        this._plots = nonStates.concat(states);
	        this._plots[this._plots.length - 1].isLastInLayout(true);
	        this._totalHeight = totalHeight;
	        // bound pass
	        fitRatio = this._config.dimension().height() / (totalHeight - (gapCount * this._config.plotSpacing()));
	        // console.log('fit ratio', fitRatio, this._config.dimension().height(),'/',(totalHeight - (gapCount * this._config.plotSpacing())));
	        if (fitRatio !== 1) {
	            // this.updateLayout(fitRatio);
	        }
	    };
	    return Layout;
	}());
	exports.default = Layout;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var config_dimension_1 = __webpack_require__(21);
	var LayoutConfig = (function () {
	    function LayoutConfig() {
	        this._dimension = new config_dimension_1.default();
	        this._defaultPlotHeights = {
	            'gauge': 130,
	            'multi': 120,
	            'alert': 47,
	            'event': 120,
	            'status': 47,
	            'dpaWaitTime': 120
	        };
	        this._growthIncrement = 48;
	        this._plotSpacing = 25;
	        this._translationOffsets = [0, 0];
	    }
	    LayoutConfig.prototype.dimension = function (config) {
	        if (config === undefined) {
	            return this._dimension;
	        }
	        this._dimension = config;
	    };
	    LayoutConfig.prototype.plotSpacing = function (value) {
	        if (value === undefined) {
	            return this._plotSpacing;
	        }
	        this._plotSpacing = value;
	    };
	    LayoutConfig.prototype.growthIncrement = function (config) {
	        if (config === undefined) {
	            return this._growthIncrement;
	        }
	        this._growthIncrement = config;
	    };
	    LayoutConfig.prototype.defaultPlotHeights = function (value) {
	        if (value === undefined) {
	            return this._defaultPlotHeights;
	        }
	        this._defaultPlotHeights = value;
	        return this;
	    };
	    LayoutConfig.prototype.translationOffsets = function (value) {
	        if (value === undefined) {
	            return this._translationOffsets;
	        }
	        this._translationOffsets = value;
	        return this;
	    };
	    return LayoutConfig;
	}());
	exports.default = LayoutConfig;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || (function () {
	    var extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
	    return function (d, b) {
	        extendStatics(d, b);
	        function __() { this.constructor = d; }
	        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	    };
	})();
	Object.defineProperty(exports, "__esModule", { value: true });
	var time_config_1 = __webpack_require__(25);
	var config_1 = __webpack_require__(26);
	var interaction_config_1 = __webpack_require__(27);
	var ChartConfig = (function (_super) {
	    __extends(ChartConfig, _super);
	    function ChartConfig() {
	        var _this = _super.call(this) || this;
	        _this.time = new time_config_1.default();
	        _this.interaction = new interaction_config_1.default();
	        return _this;
	    }
	    return ChartConfig;
	}(config_1.default));
	exports.default = ChartConfig;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../ref.d.ts" />
	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var time_data_1 = __webpack_require__(13);
	var _defaultWindowAmount = 12;
	var _defaultWindowUnit = 'h';
	var TimeService = (function () {
	    function TimeService($log, $q) {
	        this.$log = $log;
	        this.$q = $q;
	        this._windowUnit = 'h';
	        this._windowAmount = 12;
	        // constructor
	        this._current = new time_data_1.Timeframe();
	        this.updateToWindow(false);
	    }
	    /**
	     * Gets the desired data resolution in seconds.
	     * @returns {number}
	     */
	    TimeService.prototype.getResolution = function () {
	        var diffHours = (this._current.end().diff(this._current.start(), 'hours'));
	        var tempDivisor = diffHours / 4;
	        var divisor = (tempDivisor < 1) ? 1 : tempDivisor;
	        return (60 * divisor);
	    };
	    TimeService.prototype.setToDefaultTimeWindow = function () {
	        this.window(_defaultWindowAmount, _defaultWindowUnit);
	    };
	    /**
	     * Adjusted start time for data retrieval to be the nearest multiple of current resolution
	     * (with an arbitrary 15 minutes gap; so no trailing edge on charts)
	     * @returns {Moment}
	     */
	    TimeService.prototype.getResultionAdjustedStartTime = function () {
	        return moment((Math.floor(moment(this.current().start()).subtract(15, 'minutes').unix() / this.getResolution()) * this.getResolution()) * 1000);
	    };
	    TimeService.prototype.current = function (value) {
	        if (value === undefined) {
	            return this._current;
	        }
	        this._current = value;
	        return this;
	    };
	    TimeService.prototype.window = function (amount, unitOfTime) {
	        if (amount === undefined || unitOfTime === undefined) {
	            return [this._windowAmount, this._windowUnit];
	        }
	        this._windowAmount = amount;
	        this._windowUnit = unitOfTime;
	        return this;
	    };
	    TimeService.prototype.updateToWindow = function (bUpdateToWindow) {
	        if (bUpdateToWindow === void 0) { bUpdateToWindow = true; }
	        if (bUpdateToWindow) {
	            var millisecondsDiff = moment().diff(this._current.end());
	            this._current.end().add(millisecondsDiff);
	            this._current.start().add(millisecondsDiff);
	            //this._current.start(moment(this._current.end()).subtract(this._windowAmount));
	        }
	        else {
	            this._current.end(moment());
	            this._current.start(moment(this._current.end()).subtract(this._windowAmount, this._windowUnit));
	        }
	        //console.log('updateToWindow', this._current);
	        return this;
	    };
	    TimeService.prototype.isCurrentTimeframeWithin = function (seconds, testEnd) {
	        if (seconds === void 0) { seconds = 110; }
	        if (testEnd === void 0) { testEnd = this._current.end(); }
	        var secondsDiff = moment().diff(testEnd, 'seconds');
	        return (secondsDiff < seconds); // if less than a minute and a half off, consider it up
	    };
	    return TimeService;
	}());
	exports.TimeService = TimeService;
	exports.default = angular.module('perfstack.time.TimeService', [])
	    .service('timeService', ['$log', '$q', TimeService])
	    .name;


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	var tile_legend_multi_directive_1 = __webpack_require__(40);
	var tile_legend_gauge_directive_1 = __webpack_require__(42);
	var tile_legend_status_directive_1 = __webpack_require__(54);
	var tile_metric_directive_1 = __webpack_require__(56);
	function getLegendValue(series) {
	    return series.lastCurrentData ? series.lastCurrentData.value : series.data.length > 0 ? series.data[series.data.length - 1].value : null;
	}
	exports.getLegendValue = getLegendValue;
	exports.default = angular.module('perfstack.components.tile.directives', [
	    interaction_service_1.default,
	    tile_legend_gauge_directive_1.default, tile_legend_multi_directive_1.default, tile_legend_status_directive_1.default,
	    tile_metric_directive_1.default
	])
	    .name;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../../ref.d.ts" />
	"use strict";
	TileLegendMultiDirective.$inject = ["$log", "$filter", "interactionService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	var interaction_service_2 = __webpack_require__(11);
	var tile_legend_multi_part_directive_1 = __webpack_require__(41);
	var series_1 = __webpack_require__(6);
	var Tile = __webpack_require__(39);
	TileLegendMultiDirective.prototype.$inject = ['$log', '$filter', 'interactionService'];
	function TileLegendMultiDirective($log, $filter, interactionService) {
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/tile/tile-legend-multi-directive.html',
	        scope: {
	            series: '=',
	            onRemove: '&'
	        },
	        link: link
	    };
	    function link(scope, element, attrs) {
	        var base = d3.select(element.get(0));
	        var valueContainer = d3.select($('.ps-value-container', element).get(0));
	        var prefixContainer = d3.select($('.ps-unit-container', element).get(0));
	        var colorContainer = d3.select($('.ps-interaction .ps-color-icon', element).get(0));
	        var chartId = attrs.chartId || '';
	        scope.chartId = chartId;
	        var passiveSeriesSubscription = interactionService.subscribe(interaction_service_1.InteractionMessages.seriesPassive + scope.series.id + chartId, function (seriesId, dataIndex) {
	            if (dataIndex !== null) {
	                base.classed('ps-interacting', true);
	                if (scope.series.type === series_1.Type.alert) {
	                    colorContainer.classed('ps-active', scope.series.data[dataIndex].value > 0);
	                }
	                valueContainer
	                    .transition()
	                    .duration(100)
	                    .tween('value', function () {
	                    var currentValue = valueContainer.datum();
	                    currentValue = currentValue || 0;
	                    var i = d3.interpolate(currentValue, (scope.series.data[dataIndex] !== undefined) ? scope.series.data[dataIndex].value : 0);
	                    return function (t) {
	                        valueContainer.text($filter('siDigitsExtended')(i(t), scope.series.isPercentile, false));
	                        prefixContainer.text($filter('siPrefix')(i(t), scope.series.isPercentile));
	                        valueContainer.datum(i(t));
	                    };
	                });
	            }
	            else {
	                base.classed('ps-interacting', false);
	            }
	        });
	        scope.$on('$destroy', function () {
	            interactionService.unsubscribe(passiveSeriesSubscription);
	        });
	        scope.getLegendValue = (function () {
	            return Tile.getLegendValue(scope.series);
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.tile.legendMulti', [interaction_service_2.default, tile_legend_multi_part_directive_1.default])
	    .directive('psTileLegendMulti', TileLegendMultiDirective)
	    .name;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../../ref.d.ts" />
	"use strict";
	TileLegendMultiPartDirective.$inject = ["$log", "$filter", "interactionService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	var interaction_service_2 = __webpack_require__(11);
	var Tile = __webpack_require__(39);
	TileLegendMultiPartDirective.prototype.$inject = ['$log', '$filter', 'interactionService'];
	function TileLegendMultiPartDirective($log, $filter, interactionService) {
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/tile/tile-legend-multi-part-directive.html',
	        scope: {
	            series: '=',
	            subSeries: '='
	        },
	        link: link
	    };
	    function link(scope, element, attrs) {
	        var base = d3.select(element.get(0));
	        var valueContainer = d3.select($('.ps-value-container', element).get(0));
	        var prefixContainer = d3.select($('.ps-unit-container', element).get(0));
	        var subIndex = attrs.subIndex || 0;
	        var chartId = attrs.chartId || '';
	        var passiveSeriesSubscription = interactionService.subscribe(interaction_service_1.InteractionMessages.seriesPassive + scope.series.id + chartId, function (seriesId, dataIndex) {
	            if (dataIndex !== null) {
	                base.classed('ps-interacting', true);
	                valueContainer
	                    .transition()
	                    .duration(100)
	                    .tween('value', function () {
	                    var currentValue = valueContainer.datum();
	                    currentValue = currentValue || 0;
	                    var i = d3.interpolate(currentValue, (scope.series.subSeries[subIndex].data[dataIndex] !== undefined) ? scope.series.subSeries[subIndex].data[dataIndex].value : 0);
	                    return function (t) {
	                        valueContainer.text($filter('siDigitsExtended')(i(t), scope.series.isPercentile, false));
	                        prefixContainer.text($filter('siPrefix')(i(t), scope.series.isPercentile));
	                        valueContainer.datum(i(t));
	                    };
	                });
	            }
	            else {
	                base.classed('ps-interacting', false);
	            }
	        });
	        scope.getLegendValue = (function () {
	            return Tile.getLegendValue(scope.subSeries);
	        });
	        scope.$on('$destroy', function () {
	            interactionService.unsubscribe(passiveSeriesSubscription);
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.tile.legendMultiPart', [interaction_service_2.default])
	    .directive('psTileLegendMultiPart', TileLegendMultiPartDirective)
	    .name;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../../ref.d.ts" />
	"use strict";
	TileLegendGaugeDirective.$inject = ["$log", "$filter", "interactionService", "chartManager", "settingsService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	var interaction_service_2 = __webpack_require__(11);
	var chart_manager_factory_1 = __webpack_require__(43);
	var transformer_loess_1 = __webpack_require__(44);
	var transformer_difference_1 = __webpack_require__(45);
	var transformer_percentile_std_1 = __webpack_require__(46);
	var transformer_loess_standardize_1 = __webpack_require__(48);
	var transformer_standardize_1 = __webpack_require__(49);
	var transformer_change_point_1 = __webpack_require__(50);
	var settings_service_1 = __webpack_require__(51);
	var transformer_lin_reg_1 = __webpack_require__(52);
	var transformer_normalize_1 = __webpack_require__(47);
	var transformer_floating_average_1 = __webpack_require__(53);
	var Tile = __webpack_require__(39);
	TileLegendGaugeDirective.prototype.$inject = ['$log', '$filter', 'interactionService', 'chartManager', 'settingsService'];
	function TileLegendGaugeDirective($log, $filter, interactionService, chartManager, settingsService) {
	    var DEFAULT_FAST_POLL_TIMEOUT = 7;
	    var fastPollTimeOut = DEFAULT_FAST_POLL_TIMEOUT;
	    settingsService.getSettings().then(function (result) {
	        fastPollTimeOut = _.max([result.realtimePollRateSeconds * 2, DEFAULT_FAST_POLL_TIMEOUT]);
	    });
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/tile/tile-legend-gauge-directive.html',
	        scope: {
	            series: '=',
	            isFastPolling: '=',
	            onRemove: '&',
	            showIcon: '=?bind'
	        },
	        link: link
	    };
	    function link(scope, element, attrs) {
	        var base = d3.select(element.get(0));
	        var valueContainer = d3.select($('.ps-value-container', element).get(0));
	        var prefixContainer = d3.select($('.ps-unit-container', element).get(0));
	        var chartId = attrs.chartId || '';
	        scope.showIcon = false;
	        var passiveSeriesSubscription = interactionService.subscribe(interaction_service_1.InteractionMessages.seriesPassive + scope.series.id + chartId, function (seriesId, dataIndex) {
	            if (dataIndex !== null) {
	                base.classed('ps-interacting', true);
	                valueContainer
	                    .transition()
	                    .duration(100)
	                    .tween('value', function () {
	                    var currentValue = valueContainer.datum();
	                    currentValue = currentValue || 0;
	                    var i = d3.interpolate(currentValue, (scope.series.data[dataIndex] !== undefined) ? scope.series.data[dataIndex].value : 0);
	                    return function (t) {
	                        valueContainer.text($filter('siDigitsExtended')(i(t), scope.series.isPercentile, false));
	                        prefixContainer.text($filter('siPrefix')(i(t), scope.series.isPercentile));
	                        valueContainer.datum(i(t));
	                    };
	                });
	            }
	            else {
	                base.classed('ps-interacting', false);
	            }
	            // valueContainer.text(($filter('siDigits') as any)(scope.series.data[dataIndex].value, true));
	        });
	        scope.getFastPollStatus = function () {
	            var status = 'inactive';
	            if (scope.isFastPolling) {
	                var now = moment();
	                status = (scope.series.lastRealTimeUpdate && scope.series.lastRealTimeUpdate.isAfter(now.subtract(fastPollTimeOut, 'seconds'))) ? 'active' : 'pending';
	                // console.log('********* realtime timing: ', now, scope.series.lastRealTimeUpdate,scope.series.lastRealTimeUpdate.isAfter(now.subtract(FAST_POLL_TIMEOUT, 'seconds')),status,now.diff(scope.series.lastRealTimeUpdate,'seconds'));
	            }
	            return status;
	        };
	        scope.getLegendValue = (function () {
	            return Tile.getLegendValue(scope.series);
	        });
	        scope.toggleTransformer = (function (chartManager) {
	            return function (series, transformerName) {
	                var getTransformer;
	                switch (transformerName) {
	                    case 'changePoint':
	                        getTransformer = transformer_change_point_1.transformChangePoint;
	                        break;
	                    case 'difference':
	                        getTransformer = transformer_difference_1.transformDifference;
	                        break;
	                    case 'linear':
	                        getTransformer = transformer_lin_reg_1.transformLinReg;
	                        break;
	                    case 'normalize':
	                        getTransformer = transformer_normalize_1.transformNormalize;
	                        break;
	                    case 'percentileStd':
	                        getTransformer = transformer_percentile_std_1.transformPercentileStd;
	                        break;
	                    case 'smoothing':
	                        getTransformer = transformer_loess_1.transformLoessSmoothing;
	                        break;
	                    case 'loessStandardize':
	                        getTransformer = transformer_loess_standardize_1.transformLoessStandardize;
	                        break;
	                    case 'standardize':
	                        getTransformer = transformer_standardize_1.transformStandardize;
	                        break;
	                    case 'floatingAverage':
	                        getTransformer = transformer_floating_average_1.transformFloatingAverage;
	                        break;
	                    default:
	                        getTransformer = series.transformer = undefined;
	                        break;
	                }
	                if (series.transformer !== getTransformer) {
	                    series.data = series.rawData;
	                    series.transformer = getTransformer;
	                    series.data = series.transformer(series.data, series.isPercentile);
	                    scope.showIcon = transformerName;
	                }
	                else {
	                    series.transformer = null;
	                    series.data = series.rawData;
	                    scope.showIcon = "false";
	                }
	                chartManager.get(chartId).draw();
	            };
	        })(chartManager);
	        scope.$on('$destroy', function () {
	            interactionService.unsubscribe(passiveSeriesSubscription);
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.tile.legendGauge', [interaction_service_2.default, chart_manager_factory_1.default, settings_service_1.default])
	    .directive('psTileLegendGauge', TileLegendGaugeDirective)
	    .name;


/***/ }),
/* 43 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var ChartManager = (function () {
	    function ChartManager($log, $q, utilityService) {
	        this.$log = $log;
	        this.$q = $q;
	        this.utilityService = utilityService;
	        this.charts = {};
	        // constructor
	    }
	    ChartManager.prototype.register = function (chart, id) {
	        if (id === void 0) { id = this.utilityService.uuid(); }
	        this.charts[id] = chart;
	        return id;
	    };
	    ChartManager.prototype.deregister = function (id) {
	        delete this.charts[id];
	    };
	    ChartManager.prototype.redrawAll = function () {
	        _.forEach(this.charts, function (chart) {
	            chart.draw();
	        });
	    };
	    ChartManager.prototype.get = function (chartKey) {
	        return this.charts[chartKey];
	    };
	    return ChartManager;
	}());
	exports.ChartManager = ChartManager;
	exports.default = angular.module('perfstack.components.chart.ChartManager', [])
	    .factory('chartManager', ['$log', '$q', 'utilityService', function ($log, $q, utilityService) { return new ChartManager($log, $q, utilityService); }])
	    .name;


/***/ }),
/* 44 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function transformLoessSmoothing(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    var dataValues = {
	        x: [],
	        y: []
	    };
	    _.forEach(transformed, function (data) {
	        dataValues.x.push(data.date.unix());
	        dataValues.y.push(data.value);
	    });
	    var smoothedData = science.stats.loess().bandwidth(.1)(dataValues.x, dataValues.y);
	    _.forEach(smoothedData, function (value, index) {
	        transformed[index].value = value;
	    });
	    return transformed;
	}
	exports.transformLoessSmoothing = transformLoessSmoothing;


/***/ }),
/* 45 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function transformDifference(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    var dataValues = {
	        x: [],
	        y: []
	    };
	    var n = data.length;
	    _.forEach(transformed, function (data) {
	        dataValues.x.push(data.date.unix());
	        dataValues.y.push(data.value);
	    });
	    function differenceData(data) {
	        var newData = [];
	        var currentYvalue;
	        var previousYvalue;
	        var answer;
	        for (var i = 0; i < n; i++) {
	            if (i === n - 1) {
	                newData.push(answer);
	                break;
	            }
	            currentYvalue = data[i + 1];
	            previousYvalue = data[i];
	            answer = (currentYvalue - previousYvalue);
	            if (hasPercetile) {
	                answer = Math.abs(answer);
	            }
	            newData.push(answer);
	        }
	        return newData;
	    }
	    var differencedData = differenceData(dataValues.y);
	    _.forEach(differencedData, function (value, index) {
	        transformed[index].value = value;
	    });
	    return transformed;
	}
	exports.transformDifference = transformDifference;


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var transformer_normalize_1 = __webpack_require__(47);
	function transformPercentileStd(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    var dataValues = {
	        x: [],
	        y: []
	    };
	    _.forEach(transformed, function (data) {
	        dataValues.x.push(data.date.unix());
	        dataValues.y.push(data.value);
	    });
	    function percentileStandardizeData(data) {
	        var median = d3.median(data);
	        var medianAbsoluteDeviation = getMedianAbsoluteDeviation(data, median);
	        //formula to transform data (percentile deviation)
	        var newData = data.map(function (value) {
	            return (value - median) / medianAbsoluteDeviation;
	        });
	        return newData;
	    }
	    function getMedianAbsoluteDeviation(values, median) {
	        var absoluteValueOfMinusMedian = values.map(function (value) {
	            return Math.abs(value - median);
	        });
	        var medianAbsoluteDeviation = d3.median(absoluteValueOfMinusMedian);
	        //median absolute deviation
	        // If the MAD is less than one then just set it to one in order to just show raw difference from the median
	        if (medianAbsoluteDeviation < 1.0) {
	            medianAbsoluteDeviation = 1.0;
	        }
	        return medianAbsoluteDeviation;
	    }
	    var percentileStandardizedData = percentileStandardizeData(dataValues.y);
	    _.forEach(percentileStandardizedData, function (value, index) {
	        transformed[index].value = hasPercetile ? Math.abs(value) : value;
	    });
	    if (hasPercetile) {
	        transformed = transformer_normalize_1.transformNormalize(transformed, hasPercetile);
	    }
	    return transformed;
	}
	exports.transformPercentileStd = transformPercentileStd;


/***/ }),
/* 47 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function transformNormalize(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    var dataValues = {
	        x: [],
	        y: []
	    };
	    _.forEach(transformed, function (data) {
	        dataValues.x.push(data.date.unix());
	        dataValues.y.push(data.value);
	    });
	    function normalize(values) {
	        // find max and min
	        var max = Math.max.apply(Math, values);
	        var min = Math.min.apply(Math, values);
	        // if max = min --> return every value = 0
	        // else  --> return normalized data values
	        return max === min ? values.map(function (value) { return 0; }) : values.map(function (value) { return (value - min) / (max - min); });
	    }
	    ;
	    //let multiplier:number = hasPercetile ? 100.0 : 1.0;
	    var normalizedData = normalize(dataValues.y);
	    _.forEach(normalizedData, function (value, index) {
	        transformed[index].value = value * 100.0;
	    });
	    return transformed;
	}
	exports.transformNormalize = transformNormalize;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var transformer_loess_1 = __webpack_require__(44);
	var transformer_standardize_1 = __webpack_require__(49);
	function transformLoessStandardize(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    transformed = transformer_loess_1.transformLoessSmoothing(transformed, hasPercetile);
	    transformed = transformer_standardize_1.transformStandardize(transformed, hasPercetile);
	    return transformed;
	}
	exports.transformLoessStandardize = transformLoessStandardize;


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var transformer_normalize_1 = __webpack_require__(47);
	function transformStandardize(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    var dataValues = {
	        x: [],
	        y: []
	    };
	    _.forEach(transformed, function (data) {
	        dataValues.x.push(data.date.unix());
	        dataValues.y.push(data.value);
	    });
	    function standardizeData(data) {
	        // to standardize array of data --> subtract mean from each value, divide each value by standard devation (except if standard deviation < 1 --> divide by 1)
	        var stddev = d3.deviation(data);
	        return data.map(function (value) { return (value - d3.mean(data)) / (stddev < 1 ? 1 : stddev); });
	    }
	    var standardizedData = standardizeData(dataValues.y);
	    _.forEach(standardizedData, function (value, index) {
	        transformed[index].value = hasPercetile ? Math.abs(value) : value;
	    });
	    if (hasPercetile) {
	        transformed = transformer_normalize_1.transformNormalize(transformed, hasPercetile);
	    }
	    return transformed;
	}
	exports.transformStandardize = transformStandardize;


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var transformer_loess_1 = __webpack_require__(44);
	function transformChangePoint(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    var sectionSize = 20; //to change section size
	    var criticalValue = 1.729; //one-tail 0.05
	    transformed = transformer_loess_1.transformLoessSmoothing(transformed, hasPercetile);
	    var t_score; //The t_score formula enables you to take an individual score and transform it into a standardized form>one which helps you to compare scores.
	    var meanOfFirstSection;
	    var standardDeviationOfFirstSection;
	    var meanOfSecondSection;
	    var standardDeviationOfSecondSection;
	    var yValuesOfFirstSection = [];
	    var yValuesOfSecondSection = [];
	    var merging = true;
	    var startIndex = 0;
	    var endIndex = sectionSize - 1;
	    var startIndex2 = sectionSize;
	    var endIndex2 = startIndex2 + endIndex;
	    while (merging) {
	        merging = false;
	        yValuesOfFirstSection = [];
	        yValuesOfSecondSection = [];
	        for (var i = startIndex; i <= endIndex; i++) {
	            yValuesOfFirstSection.push(data[i].value);
	        }
	        for (var j = startIndex2; j <= endIndex2; j++) {
	            yValuesOfSecondSection.push(data[j].value);
	        }
	        meanOfFirstSection = d3.mean(yValuesOfFirstSection);
	        meanOfSecondSection = d3.mean(yValuesOfSecondSection);
	        standardDeviationOfFirstSection = d3.deviation(yValuesOfFirstSection);
	        if (standardDeviationOfFirstSection < 1.0) {
	            standardDeviationOfFirstSection = 1.0;
	        }
	        standardDeviationOfSecondSection = d3.deviation(yValuesOfSecondSection);
	        if (standardDeviationOfSecondSection < 1.0) {
	            standardDeviationOfSecondSection = 1.0;
	        }
	        t_score = getT_Score(meanOfFirstSection, meanOfSecondSection, standardDeviationOfFirstSection, standardDeviationOfSecondSection, yValuesOfFirstSection.length, yValuesOfSecondSection.length, criticalValue);
	        if (t_score > criticalValue) {
	            for (var k = startIndex; k <= endIndex; k++) {
	                transformed[k].value = meanOfFirstSection;
	            }
	            startIndex = startIndex2;
	        }
	        endIndex = endIndex2;
	        if (endIndex + sectionSize < data.length) {
	            merging = true;
	            startIndex2 = endIndex + 1;
	            endIndex2 = startIndex2 + sectionSize - 1;
	        }
	        else {
	            endIndex = data.length - 1;
	        }
	    }
	    for (var k = startIndex; k <= endIndex; k++) {
	        transformed[k].value = meanOfFirstSection;
	    }
	    return transformed;
	}
	exports.transformChangePoint = transformChangePoint;
	function getT_Score(meanOfFirstSection1, meanOfSecondSection, standardDeviationOfFirstSection, standardDeviationOfSecondSection, sizeOfFirstSection, sizeOfSecondSection, criticalValue) {
	    var differenceOfSampleMeans = meanOfFirstSection1 - meanOfSecondSection;
	    var standardDeviationOfBothSections = (Math.pow(standardDeviationOfFirstSection, 2) / sizeOfFirstSection) + (Math.pow(standardDeviationOfSecondSection, 2) / sizeOfSecondSection);
	    var squareRootofResult = Math.sqrt(standardDeviationOfBothSections);
	    var t_score = differenceOfSampleMeans / squareRootofResult;
	    if (t_score > criticalValue) {
	        if ((meanOfFirstSection1 < (meanOfSecondSection + 2 * standardDeviationOfSecondSection) && meanOfFirstSection1 > (meanOfSecondSection - 2 * standardDeviationOfSecondSection))
	            && (meanOfSecondSection < (meanOfFirstSection1 + 2 * standardDeviationOfFirstSection) && meanOfSecondSection > (meanOfFirstSection1 - 2 * standardDeviationOfFirstSection))) {
	            t_score = 0.0;
	        }
	    }
	    return Math.abs(t_score);
	}


/***/ }),
/* 51 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var SettingsService = (function () {
	    function SettingsService($log, $http, $q, $httpParamSerializer) {
	        this.$log = $log;
	        this.$http = $http;
	        this.$q = $q;
	        this.$httpParamSerializer = $httpParamSerializer;
	        // constructor
	    }
	    SettingsService.prototype.getSettings = function () {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            if (this.cachedSettings) {
	                deferred.resolve(this.cachedSettings);
	            }
	            else {
	                this.$http.get('/api2/perfstack/v2/metadata/settings/').then(function (successResponse) {
	                    _this.cachedSettings = successResponse.data;
	                    deferred.resolve(_this.cachedSettings);
	                }, function (errorResponse) {
	                    deferred.reject();
	                    _this.$log.error('API request failed');
	                });
	            }
	        }
	        catch (e) {
	            this.$log.error('SettingsService.getSettings: ', e);
	        }
	        return deferred.promise;
	    };
	    return SettingsService;
	}());
	exports.SettingsService = SettingsService;
	exports.default = angular.module('perfstack.settings.SettingsService', [])
	    .service('settingsService', ['$log', '$http', '$q', '$httpParamSerializer', SettingsService])
	    .name;


/***/ }),
/* 52 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function transformLinReg(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    var dataValues = {
	        x: [],
	        y: []
	    };
	    _.forEach(transformed, function (data) {
	        dataValues.x.push(data.date.unix());
	        dataValues.y.push(data.value);
	    });
	    function linearRegression(xValues, yValues) {
	        var xSum = 0, ySum = 0, xySum = 0, xxSum = 0, count = 0, x = 0, y = 0;
	        if (xValues.length !== yValues.length) {
	            throw new Error('The x and y data arrays need to be the same length');
	        }
	        if (xValues.length === 0) {
	            throw new Error('There\'s no data to work with');
	        }
	        for (var i = 0; i < xValues.length; i++) {
	            x = xValues[i];
	            y = yValues[i];
	            xSum += x;
	            ySum += y;
	            xxSum += Math.pow(x, 2);
	            xySum += x * y;
	            count++;
	        }
	        var m = (count * xySum - xSum * ySum) / (count * xxSum - xSum * xSum);
	        var b = (ySum / count) - (m * xSum) / count;
	        var resultXValues = [];
	        var resultYValues = [];
	        for (var i = 0; i < xValues.length; i++) {
	            x = xValues[i];
	            y = x * m + b;
	            resultXValues.push(x);
	            resultYValues.push(y);
	        }
	        return resultYValues;
	    }
	    var linRegData = linearRegression(dataValues.x, dataValues.y);
	    _.forEach(linRegData, function (value, index) {
	        transformed[index].value = value;
	    });
	    return transformed;
	}
	exports.transformLinReg = transformLinReg;


/***/ }),
/* 53 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function transformFloatingAverage(data, hasPercetile) {
	    var transformed = _.cloneDeep(data);
	    function getAverageFromLastHour(data, index) {
	        var sum = 0.0;
	        var count = 0;
	        var dateTime = data[index].date;
	        while (index >= 0 && dateTime.diff(data[index].date, "minutes") <= 60) {
	            count++;
	            sum += data[index].value;
	            index--;
	        }
	        return sum / count;
	    }
	    ;
	    _.forEach(transformed, function (value, index) {
	        transformed[index].value = getAverageFromLastHour(transformed, index);
	    });
	    return transformed;
	}
	exports.transformFloatingAverage = transformFloatingAverage;


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../../ref.d.ts" />
	"use strict";
	TileLegendStatusDirective.$inject = ["$log", "$filter", "interactionService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	var interaction_service_2 = __webpack_require__(11);
	var status_map_1 = __webpack_require__(55);
	TileLegendStatusDirective.prototype.$inject = ['$log', '$filter', 'interactionService'];
	function TileLegendStatusDirective($log, $filter, interactionService) {
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/tile/tile-legend-status-directive.html',
	        scope: {
	            series: '=',
	            onRemove: '&'
	        },
	        link: link
	    };
	    function link(scope, element, attrs) {
	        var base = d3.select(element.get(0));
	        var valueContainer = d3.select($('.ps-value-container', element).get(0));
	        var colorContainer = d3.select($('.ps-interaction .ps-color-icon', element).get(0));
	        var chartId = attrs.chartId || '';
	        var passiveSeriesSubscription = interactionService.subscribe(interaction_service_1.InteractionMessages.seriesPassive + scope.series.id + chartId, function (seriesId, dataIndex) {
	            if (dataIndex !== null) {
	                base.classed('ps-interacting', true);
	                colorContainer.attr('style', 'background-color: ' + $filter('statusColorMap')(scope.series.data[dataIndex].value));
	                if (scope.series.data[dataIndex].metadata) {
	                    valueContainer.text(scope.series.data[dataIndex].metadata[scope.series.data[dataIndex].metadata.length - 1].message);
	                }
	            }
	            else {
	                base.classed('ps-interacting', false);
	            }
	            // valueContainer.text(($filter('siDigits') as any)(scope.series.data[dataIndex].value, true));
	        });
	        scope.$on('$destroy', function () {
	            interactionService.unsubscribe(passiveSeriesSubscription);
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.tile.legendStatus', [interaction_service_2.default, status_map_1.default])
	    .directive('psTileLegendStatus', TileLegendStatusDirective)
	    .name;


/***/ }),
/* 55 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function statusColorMap(statusValue) {
	    //
	    switch (statusValue) {
	        case 1:
	        case 22:
	            return '#2ca02c';
	        case 2:
	        case 8:
	        case 14:
	            return '#d62728';
	        case 3:
	        case 15:
	        case 19:
	            return '#d8b365';
	        case 9:
	        case 10:
	        case 30:
	            return '#0097eb';
	        case 11:
	            return '#00bba1';
	        case 12:
	            return '#222222';
	        default:
	            return '#969696';
	    }
	}
	exports.statusColorMap = statusColorMap;
	exports.default = angular.module('perfstack.utility.statusMapper', [])
	    .filter('statusColorMap', function () { return statusColorMap; })
	    .name;


/***/ }),
/* 56 */
/***/ (function(module, exports) {

	"use strict";
	TileMetrictDirective.$inject = ["$log", "$filter"];
	Object.defineProperty(exports, "__esModule", { value: true });
	TileMetrictDirective.prototype.$inject = ['$log', '$filter'];
	function TileMetrictDirective($log, $filter) {
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/tile/metric/tile-metric-directive.html',
	        scope: {
	            metric: '='
	        },
	        link: link
	    };
	    function link(scope, element) {
	        // post link
	    }
	}
	exports.default = angular.module('perfstack.components.tile.metric', [])
	    .directive('psTileMetric', TileMetrictDirective)
	    .name;


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	PsDropTarget.$inject = ["$log", "$rootScope", "$templateRequest", "$compile"];
	Object.defineProperty(exports, "__esModule", { value: true });
	/// <reference path='../../../ref.d.ts' />
	var Draggable = __webpack_require__(8);
	PsDropTarget.prototype.$inject = ['$log', '$rootScope', '$templateRequest', '$compile'];
	function PsDropTarget($log, $rootScope, $templateRequest, $compile) {
	    return {
	        restrict: 'A',
	        scope: {
	            onDrop: '&',
	            dropTargetData: '=',
	            psDropTarget: '=',
	            dropLabelActive: '@',
	            dropLabelInactive: '@'
	        },
	        link: link
	    };
	    function link(scope, element) {
	        var bindings = [];
	        // Manipulate dom
	        element.addClass('ps-drop-target');
	        $templateRequest('app/components/dragDrop/ps-drop-target-directive.html').then(function (template) {
	            var shim = angular.element(template);
	            element.append($compile(shim)(scope));
	        });
	        // Bind to events
	        element.on('dragover', function (e) {
	            if (e.preventDefault) {
	                e.preventDefault(); // Necessary. Allows us to drop.
	            }
	            element.addClass('ps-drop-over'); // Hack: Browser issue with firing leave immediately after enter if entering over a dashed border
	            if (scope.psDropTarget) {
	                e.originalEvent.dataTransfer.dropEffect = 'copy';
	            }
	            else {
	                e.originalEvent.dataTransfer.dropEffect = 'none';
	            }
	            return false;
	        });
	        element.on('drop', function (e) {
	            if (e.preventDefault) {
	                e.preventDefault(); // Necessary. Allows us to drop.
	            }
	            if (e.stopPropagation) {
	                e.stopPropagation(); // Necessary. Allows us to drop.
	            }
	            var data = e.originalEvent.dataTransfer.getData('text');
	            if (scope.psDropTarget) {
	                scope.onDrop({ dragData: data, targetData: scope.dropTargetData });
	            }
	            element.removeClass('ps-drop-over');
	        });
	        element.on('dragleave', function (e) {
	            element.removeClass('ps-drop-over'); // this / e.target is previous target element.
	        });
	        bindings.push($rootScope.$on(Draggable.EventType.DragStart, function (e) {
	            element.addClass('ps-dragging-active');
	        }));
	        bindings.push($rootScope.$on(Draggable.EventType.DragEnd, function (e) {
	            element.removeClass('ps-dragging-active');
	        }));
	        scope.$on('$destroy', function () {
	            element.off(['dragover', 'dragleave', 'drop']);
	            bindings.forEach(function (removeBinding) {
	                removeBinding();
	            });
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.dragDrop.directives')
	    .directive('psDropTarget', PsDropTarget)
	    .name;


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var time_service_1 = __webpack_require__(38);
	var metric_1 = __webpack_require__(5);
	var MetricsService = (function () {
	    function MetricsService($log, $http, $q, $httpParamSerializer, timeService) {
	        this.$log = $log;
	        this.$http = $http;
	        this.$q = $q;
	        this.$httpParamSerializer = $httpParamSerializer;
	        this.timeService = timeService;
	        // constructor
	    }
	    MetricsService.prototype.getObservations = function (ids, length, offset, filters, canceler) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        var query = angular.extend({ ids: ids.join(','), offset: offset, length: length }, filters);
	        if (filters.startTime && filters.endTime) {
	            query.startTime = filters.startTime.utc().toISOString();
	            query.endTime = filters.endTime.utc().toISOString();
	        }
	        var queryString = this.$httpParamSerializer(query);
	        var config = {};
	        if (canceler) {
	            config.timeout = canceler;
	        }
	        try {
	            var url = '/api2/perfstack/v2/metrics/observations/' + ((queryString.length > 0) ? '?' + queryString : '');
	            this.$http.get(url, config).then(function (successResponse) {
	                successResponse.data.data = metric_1.observationDtoToData(successResponse.data.data);
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                if (config.timeout && config.timeout.$$state && config.timeout.$$state.status !== 0) {
	                    _this.$log.debug('API request canceled: getObservations');
	                    deferred.reject(true);
	                }
	                else {
	                    _this.$log.error('API request failed: getObservations');
	                    deferred.reject(true);
	                }
	            });
	        }
	        catch (e) {
	            this.$log.error('MetricsService.getInspectedMeasurements', e);
	        }
	        return deferred.promise;
	    };
	    MetricsService.prototype.getEntityMetrics = function (entityIds) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        var requests = [];
	        var metrics = [];
	        try {
	            _.forEach(entityIds, function (id) {
	                requests.push(_this.$http.get('/api2/perfstack/entities/' + id + '/metrics/').then(function (successResponse) {
	                    metrics = metrics.concat(_.filter(successResponse.data, function (metric) { return !metric.isHidden; }));
	                }, function (errorResponse) {
	                    deferred.reject();
	                    _this.$log.error('API request failed');
	                }));
	            });
	            this.$q.all(requests).finally(function () {
	                deferred.resolve(metrics);
	            });
	        }
	        catch (e) {
	            this.$log.error('MetricsService.getAvailableMetrics', e);
	        }
	        return deferred.promise;
	    };
	    MetricsService.prototype.getMetric = function (metricIds, filters) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        filters = _.assignIn({}, filters);
	        try {
	            // let startTime = moment(this.timeService.current().start()).subtract(this.timeService.getResolution(),'seconds');
	            // Keep startTime at the nearest multiple of the resolution
	            // let startTime = moment((Math.floor(this.timeService.current().start().unix()/this.timeService.getResolution())*this.timeService.getResolution())*1000);
	            var params = {
	                'startTime': filters.startTime ? filters.startTime.utc().toISOString() : this.timeService.current().start().utc().toISOString(),
	                'endTime': filters.endTime ? filters.endTime.utc().toISOString() : this.timeService.current().end().utc().toISOString(),
	                'resolution': filters.resolution || this.timeService.getResolution()
	            };
	            var url = '/api2/perfstack/metrics/' + metricIds[0] + '/?' + this.$httpParamSerializer(params);
	            this.$http.get(url).then(
	            // this.$http.get('/api2/perfstack/metrics/' + metricIds[0] + '/?resolution=60&count=240').then(
	            function (successResponse) {
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                deferred.reject();
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('MetricsService.getEntityMetric', e);
	        }
	        return deferred.promise;
	    };
	    MetricsService.prototype.getRealTimeMetrics = function (metricIds, filters) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        filters = _.assignIn({}, filters);
	        filters.ids = metricIds.join(',');
	        if (filters.startTime) {
	            filters.startTime = filters.startTime.utc().toISOString();
	        }
	        try {
	            var url = '/api2/perfstack/v2/metrics/realtime/?' + this.$httpParamSerializer(filters);
	            this.$http.get(url).then(function (successResponse) {
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                deferred.reject();
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('MetricsService.getRealTimeMetrics', e);
	        }
	        return deferred.promise;
	    };
	    return MetricsService;
	}());
	exports.MetricsService = MetricsService;
	exports.default = angular.module('perfstack.metrics.Service', [time_service_1.default])
	    .service('metricService', ['$log', '$http', '$q', '$httpParamSerializer', 'timeService', MetricsService])
	    .name;


/***/ }),
/* 59 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var UtilityService = (function () {
	    function UtilityService() {
	    }
	    UtilityService.prototype.uuid = function () {
	        function getPart(makeChunckedPart) {
	            var p = (Math.random().toString(16) + '000000000').substr(2, 8);
	            return makeChunckedPart ? '-' + p.substr(0, 4) + '-' + p.substr(4, 4) : p;
	        }
	        return getPart() + getPart(true) + getPart(true) + getPart();
	    };
	    return UtilityService;
	}());
	exports.UtilityService = UtilityService;
	function isEmpty(subject) {
	    return _.isEmpty(subject);
	}
	exports.isEmpty = isEmpty;
	exports.default = angular.module('perfstack.utility.Service', [])
	    .filter('isEmpty', function () { return isEmpty; })
	    .service('utilityService', UtilityService)
	    .name;


/***/ }),
/* 60 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function entityAncestors(ancestors) {
	    if (!_.isEmpty(ancestors)) {
	        return ancestors.join(' / ');
	    }
	    return '';
	}
	exports.entityAncestors = entityAncestors;
	function timeByMostSignificantUnit(duration) {
	    return '';
	}
	exports.timeByMostSignificantUnit = timeByMostSignificantUnit;
	function legendAncestors(ancestors) {
	    if (angular.isUndefined(ancestors) || ancestors == null || ancestors.toString() === '') {
	        return '--';
	    }
	    var topLine = '';
	    var bottomLine = '';
	    _.forEach(ancestors, function (item, index) {
	        if (index < ancestors.length - 1) {
	            topLine += item + ' / ';
	        }
	        else {
	            bottomLine += "<strong>" + item + "</strong>";
	        }
	    });
	    if (topLine.length === 0) {
	        topLine = '--';
	    }
	    return (topLine + "<br>" + bottomLine);
	}
	exports.legendAncestors = legendAncestors;
	function unitLabel(unitName, ignore) {
	    if (ignore === void 0) { ignore = []; }
	    if (ignore.indexOf(unitName) > -1 || unitName.trim().length === 0) {
	        return '';
	    }
	    return '(' + unitName + ')';
	}
	exports.unitLabel = unitLabel;
	function siDigitsExtended(numberValue, isPercentile, truncateDecimals) {
	    if (truncateDecimals === void 0) { truncateDecimals = false; }
	    var result = siDigits(numberValue, isPercentile);
	    if (truncateDecimals) {
	        return result.split('.')[0];
	    }
	    return result;
	}
	exports.siDigitsExtended = siDigitsExtended;
	function dpaPercentile(numberValue) {
	    if (numberValue < 0.01) {
	        return '<0.01';
	    }
	    else {
	        return siDigits(numberValue, true);
	    }
	}
	exports.dpaPercentile = dpaPercentile;
	function clipToLineCount(text, lineCountTarget, width) {
	    var words = text.split(/(\s+)/);
	    // let lineHeight = parseFloat(element.css('line-height'));
	    var lineHeight = 20;
	    var shadowElement = $('<div></div>');
	    // HACK: should get dimension concerning available
	    shadowElement.css({ position: 'absolute', top: -2000, left: -2000, width: width, 'line-height': lineHeight + 'px' });
	    $('body').append(shadowElement);
	    var totalLines = 0;
	    var totalWords = 0;
	    while (totalLines < lineCountTarget && totalWords < words.length) {
	        shadowElement.text(shadowElement.text() + words[totalWords]);
	        totalWords++;
	        totalLines = shadowElement.height() / lineHeight;
	    }
	    if (totalWords < words.length) {
	        shadowElement.text(shadowElement.text() + '...');
	    }
	    shadowElement.remove();
	    return shadowElement.text();
	}
	exports.clipToLineCount = clipToLineCount;
	function siDigits(numberValue, isPercentile) {
	    if (angular.isUndefined(numberValue) || numberValue == null || numberValue.toString() === '') {
	        return '--';
	    }
	    if (isPercentile) {
	        var digitCount_1 = 0;
	        var result_1 = '';
	        var allowedCharacterCount_1 = (numberValue < 0) ? 4 : 3;
	        _.forEach(numberValue.toFixed(2).toString(), function (character) {
	            if (digitCount_1 < allowedCharacterCount_1) {
	                result_1 += character;
	                if (!isNaN(parseInt(character, 10))) {
	                    digitCount_1++;
	                }
	            }
	        });
	        return result_1;
	    }
	    if (numberValue < 1 && numberValue > -1) {
	        return d3.format('.2f')(numberValue);
	    }
	    else if (numberValue > 999 || (numberValue < 1 && numberValue > -1) || numberValue < -999) {
	        return d3.format('.3s')(numberValue).slice(0, -1);
	    }
	    else {
	        return d3.format('.3s')(numberValue);
	    }
	}
	exports.siDigits = siDigits;
	function siPrefix(value, isPercentile) {
	    if (angular.isUndefined(value)) {
	        return '';
	    }
	    if (isPercentile) {
	        return '%';
	    }
	    else if (value < 1 && value > -1) {
	        return '';
	    }
	    else {
	        var result = d3.format('.3s')(value);
	        var lastCharacter = result.substring(result.length - 1, result.length);
	        if (isNaN(lastCharacter)) {
	            return lastCharacter;
	        }
	        else {
	            return '';
	        }
	    }
	}
	exports.siPrefix = siPrefix;
	var AlertMessageParts;
	(function (AlertMessageParts) {
	    AlertMessageParts[AlertMessageParts["source"] = 0] = "source";
	    AlertMessageParts[AlertMessageParts["trigger"] = 1] = "trigger";
	    AlertMessageParts[AlertMessageParts["message"] = 2] = "message";
	})(AlertMessageParts = exports.AlertMessageParts || (exports.AlertMessageParts = {}));
	function duration(seconds, humanize) {
	    if (humanize === void 0) { humanize = true; }
	    if (humanize) {
	        return (seconds === undefined) ? 'UNKNOWN' : moment.duration(seconds, 'seconds').humanize();
	    }
	    else {
	        return countdown(moment().subtract(seconds, 'seconds')).toString();
	    }
	}
	exports.duration = duration;
	function inspectTime(time, isActive) {
	    if (isActive) {
	        return 'ACTIVE';
	    }
	    if (_.isEmpty(time)) {
	        return 'UNKNOWN';
	    }
	    else {
	        return moment(time).local().format('L LTS');
	    }
	}
	exports.inspectTime = inspectTime;
	exports.default = angular.module('perfstack.utility.format', [])
	    .filter('unitLabel', function () { return unitLabel; })
	    .filter('siDigits', function () { return siDigits; })
	    .filter('siDigitsExtended', function () { return siDigitsExtended; })
	    .filter('siPrefix', function () { return siPrefix; })
	    .filter('entityAncestors', function () { return entityAncestors; })
	    .filter('legendAncestors', function () { return legendAncestors; })
	    .filter('duration', function () { return duration; })
	    .filter('inspectTime', function () { return inspectTime; })
	    .filter('dpaPercentile', function () { return dpaPercentile; })
	    .filter('clipToLineCount', function () { return clipToLineCount; })
	    .name;


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var entity_1 = __webpack_require__(9);
	function iconEntityMap(instanceType) {
	    switch (instanceType) {
	        case entity_1.InstanceType.OrionVIMVirtualMachines:
	        case entity_1.InstanceType.OrionNodesOrionVIMVirtualMachines:
	            return 'virtualmachine';
	        case entity_1.InstanceType.OrionAPMIISApplicationPool:
	        case entity_1.InstanceType.OrionSRMPools:
	            return 'pool';
	        case entity_1.InstanceType.OrionDPADpaServer:
	        case entity_1.InstanceType.OrionSRMVServers:
	        case entity_1.InstanceType.OrionAPMApplication:
	            return 'virtual-server';
	        case entity_1.InstanceType.OrionAPMIISApplication:
	        case entity_1.InstanceType.OrionAPMSqlServerApplication:
	        case entity_1.InstanceType.OrionDPIApplications:
	        case entity_1.InstanceType.OrionNodesOrionAPMApplication:
	        case entity_1.InstanceType.OrionAPMGenericApplication:
	            return 'application';
	        case entity_1.InstanceType.OrionVIMVCenters:
	        case entity_1.InstanceType.OrionNodesOrionVIMVCenters:
	            return 'virtual-center';
	        case entity_1.InstanceType.OrionVIMDatastores:
	            return 'datastore';
	        case entity_1.InstanceType.OrionVIMDataCenters:
	            return 'vmwaredatacenter';
	        case entity_1.InstanceType.OrionVIMClusters:
	            return 'vmwarecluster';
	        case entity_1.InstanceType.OrionDPADatabaseInstance:
	            return 'dpa-database';
	        case entity_1.InstanceType.OrionHardwareHealthHardwareCategoryStatus:
	        case entity_1.InstanceType.OrionHardwareHealthHardwareItem:
	        case entity_1.InstanceType.OrionHardwareHealthHardwareInfo:
	            return 'hardware-sensor';
	        case entity_1.InstanceType.OrionSRMVolumes:
	            return 'nas-volume';
	        case entity_1.InstanceType.OrionVolumes:
	            return 'volume';
	        case entity_1.InstanceType.OrionNodesOrionVIMHosts:
	        case entity_1.InstanceType.OrionVIMHosts:
	            return 'virtual-host';
	        case entity_1.InstanceType.OrionNetPathEndpointServices:
	            return 'network-path';
	        case entity_1.InstanceType.OrionSRMLUNs:
	            return 'lun';
	        case entity_1.InstanceType.OrionNPMInterfaces:
	            return 'network-interface';
	        case entity_1.InstanceType.OrionDPADatabaseInstance:
	        case entity_1.InstanceType.OrionAPMSqlDatabase:
	            return 'database';
	        case entity_1.InstanceType.OrionAPMIISSite:
	        case entity_1.InstanceType.OrionSEUMTransactions:
	            return 'website';
	        case entity_1.InstanceType.OrionNodes:
	            return 'unknownnode';
	        case entity_1.InstanceType.OrionApiPollerApiPoller:
	            return 'api';
	        case entity_1.InstanceType.OrionApiPollerValueToMonitor:
	            return 'api-value';
	        default:
	            return 'unknownnode';
	    }
	}
	exports.iconEntityMap = iconEntityMap;
	exports.default = angular.module('perfstack.utility.iconMapper', [])
	    .filter('iconEntityMap', function () { return iconEntityMap; })
	    .name;


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var entity_1 = __webpack_require__(9);
	var EntityService = (function () {
	    function EntityService($log, $http, $q, $httpParamSerializer, $location) {
	        this.$log = $log;
	        this.$http = $http;
	        this.$q = $q;
	        this.$httpParamSerializer = $httpParamSerializer;
	        this.$location = $location;
	        this.maxUrlSegmentLength = 260;
	        // constructor
	        this.maxUrlSegmentLength -= this.$location.host().length;
	    }
	    EntityService.prototype.getStates = function () {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            this.$http.get('/api2/perfstack/entities/states/').then(function (successResponse) {
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                deferred.reject();
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('EntitysService.getStates: ', e);
	        }
	        return deferred.promise;
	    };
	    EntityService.prototype.getTypes = function () {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            this.$http.get('/api2/perfstack/entities/types/').then(function (successResponse) {
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                deferred.reject();
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('EntitysService.getTypes: ', e);
	        }
	        return deferred.promise;
	    };
	    EntityService.prototype.getEntitiesAndRelatives = function (ids) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        var query = {};
	        if (ids.length > 0) {
	            query.ids = ids.join(',');
	            query.offset = 0;
	        }
	        var queryString = this.$httpParamSerializer(query);
	        var errorOccurred = false;
	        try {
	            var url = '/api2/perfstack/v2/entities/relationships/' + ((queryString.length > 0) ? '?' + queryString : '');
	            this.$http.get(url).then(function (successResponse) {
	                var responseData = successResponse.data;
	                entity_1.scrubEnityAncestorDisplayNames(responseData.data);
	                deferred.resolve(responseData.data);
	            }, function (errorResponse) {
	                errorOccurred = true;
	                deferred.reject();
	                _this.$log.error('API request failed: getEntitiesAndRelatives');
	            });
	        }
	        catch (e) {
	            deferred.reject();
	            this.$log.error('EntitysService.getEntitiesAndRelatives', e);
	        }
	        return deferred.promise;
	    };
	    EntityService.prototype.getEntities = function (ids, length, offset, filterset, canceler) {
	        var _this = this;
	        if (ids === void 0) { ids = []; }
	        var deferred = this.$q.defer();
	        var query = angular.extend({ offset: offset, length: length }, filterset);
	        if (ids.length > 0) {
	            query.ids = ids.join(',');
	        }
	        var queryString = this.$httpParamSerializer(query);
	        var errorOccurred = false;
	        var isCanceled = false;
	        var result;
	        var config = {};
	        if (canceler) {
	            config.timeout = canceler;
	        }
	        try {
	            var url = '/api2/perfstack/v2/entities/' + ((queryString.length > 0) ? '?' + queryString : '');
	            this.$http.get(url, config).then(function (successResponse) {
	                result = successResponse.data;
	                entity_1.scrubEnityAncestorDisplayNames(result.data);
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                errorOccurred = true;
	                if (config.timeout && config.timeout.$$state && config.timeout.$$state.status !== 0) {
	                    isCanceled = true;
	                    _this.$log.debug('API request canceled: getEntities');
	                }
	                else {
	                    deferred.reject();
	                    _this.$log.error('API request failed: getEntities');
	                }
	            });
	        }
	        catch (e) {
	            deferred.reject();
	            this.$log.error('EntitysService.getEntities', e);
	        }
	        return deferred.promise;
	    };
	    EntityService.prototype.chunkIdsForSegmentLength = function (ids, maxSetStringLength) {
	        if (maxSetStringLength === void 0) { maxSetStringLength = 260; }
	        var sets = [];
	        var remaining = [];
	        while (ids.join(',').length >= maxSetStringLength) {
	            remaining.push(ids.pop());
	        }
	        sets.push(ids);
	        if (remaining.length) {
	            var chunkedRemaining = this.chunkIdsForSegmentLength(remaining, maxSetStringLength);
	            sets = sets.concat(chunkedRemaining);
	        }
	        return sets;
	    };
	    return EntityService;
	}());
	exports.EntityService = EntityService;
	exports.default = angular.module('perfstack.entity.EntityService', [])
	    .service('entityService', ['$log', '$http', '$q', '$httpParamSerializer', '$location', EntityService])
	    .name;


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var entity_context_curator_controller_1 = __webpack_require__(64);
	var entity_context_curator_directive_1 = __webpack_require__(65);
	exports.default = angular.module('perfstack.components.entityContextCurator', ['filtered-list', entity_context_curator_directive_1.default])
	    .controller('EntityContextCuratorController', entity_context_curator_controller_1.default)
	    .name;


/***/ }),
/* 64 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Controller = (function () {
	    function Controller($log, $q, $scope, $timeout, _t, entityService) {
	        var _this = this;
	        this.$log = $log;
	        this.$q = $q;
	        this.$scope = $scope;
	        this.$timeout = $timeout;
	        this._t = _t;
	        this.entityService = entityService;
	        this.$onInit = function () {
	            _this.isBusy++;
	            _this.refreshTypesFilterOptions().finally(function () {
	                _this.isBusy--;
	            });
	            //HACK: for injected type selector
	            _this.$scope.$watch(function () { return _this.selectedType; }, function () {
	                _this.pagination.page = 1;
	                _this.remoteControl.refreshListData();
	            });
	            //HACK: xui-filter list current resets selections when changing filters; hook to override this functionality
	            _this.$scope.$watch(function () { return _this.$scope.selection.items.length; }, function () {
	                if (_this.$scope.selection.items.length === 0 && _this.selectedEntitiesStateCache.length !== 0) {
	                    _this.restoreSelectedEntities();
	                }
	                else {
	                    _this.selectedEntitiesStateCache = _this.$scope.selection.items.concat([]);
	                }
	            });
	            //HACK: injecting the type selector into the xui-grid toolset
	            _this.$timeout(function () {
	                var dropdownElement = $('.ps-entity-context-curator-content .ps-filter-list .ps-type-filter-selector');
	                $('.ps-entity-context-curator-content .ps-filter-list .xui-grid__listview-tools.xui-strip-layout').prepend(dropdownElement);
	            }, 0, false);
	        };
	        this.entities = [];
	        this.remoteControl = {
	            refreshListData: angular.noop
	        };
	        this.listOptions = {
	            allowSelectAllPages: false,
	            pageSize: 10,
	            showMorePropertyValuesThreshold: 10,
	            selectionMode: 'multi',
	            templateUrl: 'app/components/entityContextCurator/entity-list-item-template.html',
	            emptyTemplateUrl: "app/components/entityContextCurator/entity-list-item-empty-template.html",
	            showAddRemoveFilterProperty: false
	        };
	        this.statusFilterModel = {
	            refId: "status",
	            title: this._t("Status"),
	            values: []
	        };
	        this.typesFilterModel = {
	            refId: "type",
	            title: this._t("Type"),
	            values: []
	        };
	        this.filterTypes = [
	            { name: "Node", value: 'Orion.Nodes' },
	            { name: 'All', value: '' }
	        ];
	        this.filterOptions = [
	            this.statusFilterModel,
	            this.typesFilterModel
	        ];
	        this.filterValues = {
	            status: []
	            // ,    type: []
	        };
	        this.filterGroups = {
	            status: {
	                label: this._t('Status')
	            }
	            // ,type: {
	            //   label: this._t('Type')
	            // }
	        };
	        this.pagination = {
	            page: 1,
	            pageSize: 10,
	            total: 10
	        };
	        this.selectedType = { title: "", id: null };
	        this.isBusy = 0;
	        this.selectedEntitiesStateCache = [];
	    }
	    Controller.prototype.getFilterProperties = function () {
	        var _this = this;
	        var filterPromises = [];
	        var deferred = this.$q.defer();
	        filterPromises.push(this.refreshStatusFilterOptions());
	        // filterPromises.push(this.refreshTypesFilterOptions());
	        this.$q.all(filterPromises).then(function () {
	            //HACK: PS-352, Xui Chiclets list breaks removing item at 0 index
	            _.forEach(_this.filterOptions[0].values, function (option) {
	                var id = parseInt(option.id, 10);
	                option.id = id + 1;
	            });
	            deferred.resolve(_this.filterOptions);
	        });
	        return deferred.promise;
	    };
	    Controller.prototype.clearSelection = function () {
	        this.selectedEntitiesStateCache.length = this.$scope.selection.items.length = 0;
	    };
	    Controller.prototype.refresh = function (filters, pagination, sorts, search) {
	        var _this = this;
	        // HACK: Search string is not passed by filter-list
	        search = search || ($('ps-entity-context-curator .ps-filter-list .xui-search__input-cancel:hover').length) ? '' : $('ps-entity-context-curator .ps-filter-list .xui-search__input-control').val();
	        pagination = pagination || this.pagination;
	        //HACK: Xui Chiclets list breaks removing item at 0 indexAdjust Status Index for look up
	        var adjustStatus = [];
	        _.forEach(this.filterValues.status, function (statusItem) {
	            statusItem = statusItem - 1;
	            adjustStatus.push(statusItem);
	        });
	        if (angular.isDefined(search)) {
	            //to lowercase for search w capitol letters
	            search = search.toLowerCase();
	        }
	        //HACK: PS-352, Xui Chiclets list breaks removing item at 0 index, Apply updated index
	        var filterset = {
	            displayName: (search === '' || search === undefined) ? null : search,
	            status: (this.filterValues.status.length > 0) ? adjustStatus.join(',') : null,
	            type: this.selectedType.id
	        };
	        //HACK: PS-352, Original filterset
	        /*let filterset:IEntityFilterset = {
	          displayName: (search === '' || search === undefined) ? null : search,
	          status: (this.filterValues.status.length > 0) ? this.filterValues.status.join(',') : null,
	          type: this.selectedType.id
	        };*/
	        var deferred = this.$q.defer();
	        var offset = (pagination.pageSize * (pagination.page - 1));
	        if (this.$scope.relative) {
	            // Hack: until api supports paging/filtering
	            // TODO: Remove, this path is no longer used
	            this.entityService.getEntitiesAndRelatives([this.$scope.relative.id]).then(function (result) {
	                _this.pagination.total = result.length;
	                _this.pagination.pageSize = result.length;
	                _this.entities = result;
	                deferred.resolve(_this.entities);
	            });
	        }
	        else {
	            // Set to empty if no filters are set; we don't ever want to get the entire set (ref PS-534)
	            if (filterset.displayName === null && filterset.status === null && filterset.type === null) {
	                this.entities = [];
	                this.pagination.total = 0;
	                deferred.resolve(this.entities);
	            }
	            else {
	                if (filterset.type === 'all') {
	                    filterset.type = null;
	                }
	                if (!_.isEqual(this.currentRequestState, { pageSize: pagination.pageSize, offset: offset, filterset: filterset })) {
	                    if (this.refreshCanceler) {
	                        this.refreshCanceler.resolve();
	                    }
	                    this.refreshCanceler = this.$q.defer();
	                    this.currentRequestState = { pageSize: pagination.pageSize, offset: offset, filterset: angular.copy(filterset) };
	                    this.entityService.getEntities([], pagination.pageSize, offset, filterset, this.refreshCanceler.promise).then(function (result) {
	                        _this.pagination.total = result.query.total;
	                        _this.entities = result.data;
	                        deferred.resolve(_this.entities);
	                    }).catch(function (isCanceled) {
	                        if (isCanceled) {
	                            _this.$log.debug('Refresh canceled');
	                        }
	                        else {
	                            deferred.resolve(_this.entities);
	                        }
	                    });
	                }
	                else {
	                    deferred.resolve(this.entities);
	                }
	            }
	        }
	        return deferred.promise;
	    };
	    Controller.prototype.restoreSelectedEntities = function () {
	        (_a = this.$scope.selection.items).push.apply(_a, this.selectedEntitiesStateCache);
	        var _a;
	    };
	    Controller.prototype.refreshStatusFilterOptions = function () {
	        var _this = this;
	        var deferred = this.$q.defer();
	        this.entityService.getStates().then(function (states) {
	            _this.statusFilterModel.values = _.map(states, function (status) {
	                return { title: status.shortDescription, id: status.statusId };
	            });
	            deferred.resolve(states);
	        });
	        return deferred.promise;
	    };
	    Controller.prototype.refreshTypesFilterOptions = function () {
	        var _this = this;
	        var deferred = this.$q.defer();
	        this.entityService.getTypes().then(function (types) {
	            _this.typesFilterModel.values = _.map(types, function (type) {
	                var optionModel = { title: type.displayName, id: type.fullName };
	                // select Nodes type by default
	                if (optionModel.id === 'Orion.Nodes') {
	                    _this.selectedType = optionModel;
	                }
	                return optionModel;
	            });
	            if (_.findIndex(_this.typesFilterModel.values, _this.selectedType) < 0) {
	                _this.selectedType = _this.typesFilterModel.values[0];
	            }
	            _this.typesFilterModel.values.unshift({ title: _this._t('All'), id: 'all' });
	            deferred.resolve(types);
	        });
	        return deferred.promise;
	    };
	    return Controller;
	}());
	Controller.$inject = ['$log', '$q', '$scope', '$timeout', 'getTextService', 'entityService'];
	exports.default = Controller;


/***/ }),
/* 65 */
/***/ (function(module, exports) {

	"use strict";
	PsEntityContextCurator.$inject = ["$log"];
	Object.defineProperty(exports, "__esModule", { value: true });
	PsEntityContextCurator.prototype.$inject = ['$log'];
	function PsEntityContextCurator($log) {
	    return {
	        controller: 'EntityContextCuratorController',
	        controllerAs: 'controller',
	        templateUrl: 'app/components/entityContextCurator/entity-context-curator-directive.html',
	        restrict: 'E',
	        scope: {
	            selection: '=',
	            relative: '='
	        },
	        link: link
	    };
	    function link(scope, element) {
	        // console.log('*******linking curator');
	    }
	}
	exports.default = angular.module('perfstack.components.entityContextCurator.directives', [])
	    .directive('psEntityContextCurator', PsEntityContextCurator)
	    .name;


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var project_1 = __webpack_require__(10);
	var ProjectService = (function () {
	    function ProjectService($log, $http, $q, $httpParamSerializer, $location, _t, timeService) {
	        this.$log = $log;
	        this.$http = $http;
	        this.$q = $q;
	        this.$httpParamSerializer = $httpParamSerializer;
	        this.$location = $location;
	        this._t = _t;
	        this.timeService = timeService;
	        // constructor
	    }
	    ProjectService.prototype.deleteProject = function (project) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            this.$http.delete('/api2/perfstack/projects/' + project.id + '/').then(function (successResponse) {
	                deferred.resolve(true);
	            }, function (errorResponse) {
	                deferred.reject(errorResponse.data);
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('ProjectService.updateProject: ', e);
	        }
	        return deferred.promise;
	    };
	    ProjectService.prototype.getRecentProjects = function () {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            this.$http.get('/api2/perfstack/recent/projects/').then(function (successResponse) {
	                successResponse.data.data = _.map(successResponse.data.data, function (projectDTO) {
	                    return new project_1.Project(projectDTO);
	                });
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                deferred.reject();
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('ProjectService.getRecentProjects: ', e);
	        }
	        return deferred.promise;
	    };
	    ProjectService.prototype.getProjects = function (ids, length, offset, orderBy, sort, filterset) {
	        var _this = this;
	        if (ids === void 0) { ids = []; }
	        var deferred = this.$q.defer();
	        var idQuery = ids.join(',');
	        var query = angular.extend({ offset: offset, length: length, orderBy: orderBy, sort: sort }, filterset);
	        if (ids.length > 0) {
	            query = angular.extend(query, { updateLastAccessed: true });
	        }
	        var queryString = this.$httpParamSerializer(query);
	        var url = '/api2/perfstack/projects/'
	            + ((!_.isEmpty(idQuery)) ? idQuery + '/' : '')
	            + ((queryString.length > 0) ? '?' + queryString : '');
	        try {
	            this.$http.get(url).then(function (successResponse) {
	                successResponse.data.data = _.map(successResponse.data.data, function (projectDTO) {
	                    return new project_1.Project(projectDTO);
	                });
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                deferred.reject();
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('ProjectService.getProject: ', e);
	        }
	        return deferred.promise;
	    };
	    ProjectService.prototype.createProject = function (project) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            var dto = project_1.convertToDTO(project);
	            this.$http.post('/api2/perfstack/projects/', dto, { swAlertOnError: SW.environment.demoMode }).then(function (successResponse) {
	                if (successResponse.status === 202) {
	                    deferred.reject();
	                }
	                else {
	                    deferred.resolve(new project_1.Project(successResponse.data));
	                }
	            }, function (errorResponse) {
	                deferred.reject(errorResponse.data);
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('ProjectService.createProject: ', e);
	        }
	        return deferred.promise;
	    };
	    ProjectService.prototype.updateProject = function (project) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            var dto = project_1.convertToDTO(project);
	            this.$http.put('/api2/perfstack/projects/' + project.id + '/', dto).then(function (successResponse) {
	                deferred.resolve(new project_1.Project(successResponse.data));
	            }, function (errorResponse) {
	                deferred.reject(errorResponse.data);
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('ProjectService.updateProject: ', e);
	        }
	        return deferred.promise;
	    };
	    /**
	     * Serialize the project to CSV formated string.
	     * @param project
	     * @param charts
	     * @returns {string}
	     */
	    // TODO: Make charts optional and fetch from backend API
	    ProjectService.prototype.toCSV = function (project, charts) {
	        var _this = this;
	        var csv = this.escapeCsvData(project.displayName) + ',"=HYPERLINK(""' + this.$location.absUrl() + '"")"\r\n' +
	            this._t('Start') + ',' + this.timeService.current().start().format('YYYY-MM-DD HH:mm:ss') + ',' +
	            this._t('End') + ',' + this.timeService.current().end().format('YYYY-MM-DD HH:mm:ss') + '\r\n\r\n';
	        _.forEach(charts, function (chart) {
	            csv += (_this.escapeCsvData(chart.displayName) || '') + '\r\n';
	            var seriesKeys = _.keys(chart.series);
	            var headerModel = [];
	            var maxDataSize = _.max(_.map(chart.series, function (series) {
	                return series.data.length;
	            }));
	            _.forEach(chart.series, function (series) {
	                headerModel.push.apply(headerModel, [_this._t('Entity'), _this._t('Metric'), _this._t('Date/Time'), _this._t('Value'), '']);
	                _.forEach(series.subSeries, function () {
	                    headerModel.pop(); // No spacer column between sub-series or parent
	                    headerModel.push.apply(headerModel, [_this._t('Entity'), _this._t('Metric'), _this._t('Date/Time'), _this._t('Value'), '']);
	                });
	            });
	            csv += headerModel.join(',') + '\r\n';
	            var _loop_1 = function (i) {
	                var dataRowCsv = '';
	                _.forEach(seriesKeys, function (key, index) {
	                    var series = chart.series[key];
	                    var data = series.data[i];
	                    var columnsExpected = 4 + ((series.subSeries) ? series.subSeries.length * 4 : 0);
	                    if (!_.isUndefined(data)) {
	                        dataRowCsv = _this.appendCsvDataItem(dataRowCsv, index, series, data, false);
	                        _.forEach(series.subSeries, function (subSeries) {
	                            dataRowCsv = _this.appendCsvDataItem(dataRowCsv, 4, subSeries, subSeries.data[i], true);
	                        });
	                    }
	                    else {
	                        dataRowCsv += Array(columnsExpected).join(',');
	                    }
	                    dataRowCsv += ',,';
	                });
	                csv += dataRowCsv + '\r\n';
	            };
	            for (var i = 0; i < maxDataSize; i++) {
	                _loop_1(i);
	            }
	            csv += '\r\n';
	        });
	        return csv;
	    };
	    ProjectService.prototype.getProjectViews = function (projectId) {
	        var _this = this;
	        var deferred = this.$q.defer();
	        try {
	            this.$http.get("/api2/perfstack/v2/projects/views/" + projectId + "/").then(function (successResponse) {
	                deferred.resolve(successResponse.data);
	            }, function (errorResponse) {
	                deferred.reject();
	                _this.$log.error('API request failed');
	            });
	        }
	        catch (e) {
	            this.$log.error('ProjectService.getProjectViews: ', e);
	        }
	        return deferred.promise;
	    };
	    ProjectService.prototype.appendCsvDataItem = function (csv, columnIndex, series, data, isSubSeries) {
	        var unitsLabel = ((series.unitName || series.isPercentile) && !isSubSeries) ? ' (' + (series.unitName || '%') + ')' : '';
	        var metricDisplay = series.displayName + unitsLabel;
	        csv += ((columnIndex === 0 || csv.slice(-1) === ',') ? '' : ',') + [this.escapeCsvData(series.sourceDisplayName), this.escapeCsvData(metricDisplay), data.date.format('YYYY-MM-DD HH:mm:ss'), data.value.toString()].join(',');
	        return csv;
	    };
	    ProjectService.prototype.escapeCsvData = function (data) {
	        data = '"' + data.replace('"', '""') + '"';
	        return data;
	    };
	    return ProjectService;
	}());
	exports.ProjectService = ProjectService;
	exports.default = angular.module('perfstack.project.ProjectService', [])
	    .service('projectService', ['$log', '$http', '$q', '$httpParamSerializer', '$location', 'getTextService', 'timeService', ProjectService])
	    .name;


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var project_curator_controller_1 = __webpack_require__(68);
	var project_curator_directive_1 = __webpack_require__(69);
	exports.default = angular.module('perfstack.components.projectCurator', ['filtered-list', project_curator_directive_1.default])
	    .controller('ProjectCuratorController', project_curator_controller_1.default)
	    .name;


/***/ }),
/* 68 */
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Controller = (function () {
	    function Controller($log, $q, $scope, _t, projectService) {
	        this.$log = $log;
	        this.$q = $q;
	        this.$scope = $scope;
	        this._t = _t;
	        this.projectService = projectService;
	        this.projects = [];
	        this.pagination = {
	            page: 1,
	            pageSize: 10,
	            total: 20
	        };
	        this.sorting = {
	            sortableColumns: [
	                { id: "displayName", label: this._t("Project Name") },
	                { id: "updateDateTime", label: this._t("Updated") },
	                { id: "createDateTime", label: this._t("Created") }
	            ],
	            sortBy: {
	                id: "displayName",
	                label: this._t("Project Name")
	            },
	            direction: "asc"
	        };
	        this.listOptions = {
	            allowSelectAllPages: false,
	            showMorePropertyValuesThreshold: 5,
	            selectionMode: 'single',
	            templateUrl: 'app/components/projectCurator/project-list-item-template.html',
	            showAddRemoveFilterProperty: false
	        };
	    }
	    ;
	    Controller.prototype.refresh = function (filters, pagination, sorts, search) {
	        // HACK: Search string is not passed by filter-list
	        var _this = this;
	        var xuiFilterListSearch = ($('ps-project-curator .ps-filter-list .xui-search__input-cancel:hover').length) ? '' : $('ps-project-curator .ps-filter-list .xui-search__input-control').val();
	        search = search || xuiFilterListSearch;
	        var filterset = {
	            displayName: (search === '') ? null : search
	        };
	        var deferred = this.$q.defer();
	        var offset = (pagination.pageSize * (pagination.page - 1));
	        this.projectService.getProjects([], pagination.pageSize, offset, sorts.sortBy.id, sorts.direction, filterset).then(function (result) {
	            _this.pagination.total = result.query.total;
	            _this.projects = result.data;
	            deferred.resolve(_this.projects);
	        });
	        return deferred.promise;
	    };
	    return Controller;
	}());
	Controller.$inject = ['$log', '$q', '$scope', 'getTextService', 'projectService'];
	exports.default = Controller;


/***/ }),
/* 69 */
/***/ (function(module, exports) {

	"use strict";
	PsProjectCurator.$inject = ["$log"];
	Object.defineProperty(exports, "__esModule", { value: true });
	PsProjectCurator.prototype.$inject = ['$log'];
	function PsProjectCurator($log) {
	    return {
	        controller: 'ProjectCuratorController',
	        controllerAs: 'controller',
	        templateUrl: 'app/components/projectCurator/project-curator-directive.html',
	        restrict: 'E',
	        scope: {
	            selection: '='
	        },
	        link: link
	    };
	    function link(scope, element) {
	        // console.log('*******linking project curator');
	    }
	}
	exports.default = angular.module('perfstack.components.projectCurator.directives', [])
	    .directive('psProjectCurator', PsProjectCurator)
	    .name;


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var data_explorer_controller_1 = __webpack_require__(71);
	var data_explorer_directive_1 = __webpack_require__(72);
	var events_directive_1 = __webpack_require__(73);
	var alerts_directive_1 = __webpack_require__(74);
	var dpa_directive_1 = __webpack_require__(75);
	exports.default = angular.module('perfstack.components.dataExplorer', [data_explorer_directive_1.default, events_directive_1.default, alerts_directive_1.default, dpa_directive_1.default])
	    .controller('DataExplorerController', data_explorer_controller_1.default)
	    .name;


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	var time_data_1 = __webpack_require__(13);
	var series_1 = __webpack_require__(6);
	var Controller = (function () {
	    function Controller($log, $q, $scope, $timeout, interactionService, metricService, chartManager) {
	        var _this = this;
	        this.$log = $log;
	        this.$q = $q;
	        this.$scope = $scope;
	        this.$timeout = $timeout;
	        this.interactionService = interactionService;
	        this.metricService = metricService;
	        this.chartManager = chartManager;
	        this.$onInit = function () {
	            _this.$scope.$on('$destroy', _this.destroy);
	            _this.subscribeToInteractions();
	            _this.refresh();
	            _this.$scope.$watch(function () { return _this.getSelectedFilters(); }, function () {
	                _this.onControlAction();
	            });
	        };
	        this.isBusy = 0;
	        this.searchTerm = '';
	        this.filterOptions = {};
	        // public selectedCategory: IOption = this.filterOptions[0];
	        this.overviewChart = {
	            id: 'ps-data-explorer-overview-chart',
	            displayName: '',
	            series: {},
	            isDropTarget: false,
	            calculatedHeight: 0,
	            isBusy: 0
	        };
	        this.pagination = {
	            page: 1,
	            pageSize: 75,
	            total: 75
	        };
	        this.observations = [];
	        this.seriesDictionary = {};
	        this._interactionSubIds = [];
	        this.destroy = function () {
	            _.forEach(_this._interactionSubIds, function (subId) {
	                _this.interactionService.unsubscribe(subId);
	            });
	        };
	        this._metricIds = [];
	        this.isReset = true;
	        this.timeframe = new time_data_1.Timeframe();
	    }
	    Controller.prototype.onControlAction = function () {
	        this.pagination.page = 1;
	        this.refresh();
	    };
	    Controller.prototype.onPagination = function (page, pageSize, total) {
	        this.pagination.page = page;
	        this.pagination.pageSize = pageSize;
	        this.refresh();
	    };
	    Controller.prototype.onSearch = function (searchTerm) {
	        this.searchTerm = searchTerm;
	        this.onControlAction();
	    };
	    Controller.prototype.onSearchClear = function () {
	        if (this.isPolicyEvent()) {
	            this.searchTerm = "";
	            this.onControlAction();
	        }
	    };
	    Controller.prototype.showEmptyState = function () {
	        return (!this.sourceMetric || (!this.isAlertType && !this.isEventType() && !this.isAlertType() && !this.isPolicyEvent()));
	    };
	    Controller.prototype.isAlertType = function () {
	        return this.sourceMetric.type === series_1.Type.alert;
	    };
	    Controller.prototype.isEventType = function () {
	        return this.sourceMetric.type === series_1.Type.event;
	    };
	    Controller.prototype.isDpaWaitTime = function () {
	        return this.sourceMetric.type === series_1.Type.dpaWaitTime;
	    };
	    Controller.prototype.isPolicyEvent = function () {
	        return this.sourceMetric.type === series_1.Type.multi && this.sourceMetric.groupId === "Orion.PerfStack.PoliciesCompliance";
	    };
	    Controller.prototype.isPolicyEventOverEntities = function () {
	        return this.isPolicyEvent() && this.observations.length > 0 && this.observations[0].propertyBag.overEntities;
	    };
	    Controller.prototype.getSelectedFilters = function () {
	        var selected = [];
	        _.forEach(this.filterOptions, function (options, key) {
	            selected = selected.concat(_.filter(options, function (option) {
	                return option.checked;
	            }));
	        });
	        return selected.join(',');
	    };
	    Controller.prototype.subscribeToInteractions = function () {
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.inspect, this.onTimeframeInspection, this));
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.inspectToExplore, this.onInspectToExplore, this));
	    };
	    Controller.prototype.refresh = function () {
	        var _this = this;
	        if (this._metricIds.length) {
	            var filterset_1 = {
	                startTime: this.timeframe.start(),
	                endTime: this.timeframe.end()
	            };
	            _.forEach(this.filterOptions, function (options, key) {
	                filterset_1[key] = _.map(_.filter(options, function (option) {
	                    return option.checked;
	                }), function (option) {
	                    return option.value;
	                }).join(',');
	                if (_.isEmpty(filterset_1[key])) {
	                    delete filterset_1[key];
	                }
	            });
	            if (this.searchProperty && !_.isEmpty(this.searchTerm)) {
	                if (this.isReset) {
	                    this.searchTerm = '';
	                }
	                filterset_1[this.searchProperty] = this.searchTerm;
	            }
	            var offset = (this.pagination.pageSize * (this.pagination.page - 1));
	            if (this._refreshCanceler) {
	                this._refreshCanceler.resolve();
	            }
	            this._refreshCanceler = this.$q.defer();
	            this.$timeout(function () {
	                _this.isBusy++;
	            }, 0);
	            this.metricService.getObservations(this._metricIds, this.pagination.pageSize, offset, filterset_1, this._refreshCanceler.promise)
	                .then(function (successResponse) {
	                _this.observations = successResponse.data;
	                _this.metrics = _.values(successResponse.metricMetadata);
	                if (_this.isReset) {
	                    _this.updateFilterOptions(successResponse.filterMetadata, successResponse.displayNames);
	                    _this.updateSearchProperty(successResponse.searchMetadata);
	                    _this.updateSources(successResponse.metricMetadata);
	                    _this.isReset = false;
	                }
	                _this.updatePagination(successResponse.query);
	            })
	                .finally(function () {
	                _this.isBusy--;
	            });
	        }
	        else {
	            this.observations = null;
	            this.metrics = [];
	            this.sourceMetric = null;
	        }
	    };
	    Controller.prototype.updatePagination = function (query) {
	        this.pagination.total = query.total;
	    };
	    Controller.prototype.updateSources = function (metricMetadata) {
	        var metric = metricMetadata[_.keys(metricMetadata)[0]];
	        this.sourceMetric = metric;
	        this.sourceEntity = metric.entityDisplayName;
	    };
	    Controller.prototype.updateSearchProperty = function (searchProperties) {
	        if (searchProperties.length) {
	            this.searchProperty = searchProperties[0]; // only handling one for now
	        }
	        else {
	            this.searchProperty = null;
	        }
	    };
	    Controller.prototype.updateFilterOptions = function (optionDictionary, displayNamesDictionary) {
	        _.forEach(optionDictionary, function (optionList, key) {
	            optionDictionary[key] = _.map(optionList, function (option) {
	                return {
	                    value: option,
	                    title: displayNamesDictionary[option.toLocaleLowerCase()],
	                    checked: false
	                };
	            });
	        });
	        this.filterOptions = optionDictionary;
	        this.displayNames = displayNamesDictionary;
	    };
	    Controller.prototype.onInspectToExplore = function (metricIds, config) {
	        var series = (config && config.series) ? config.series : {};
	        this.onTimeframeInspection(this.timeframe, { metricIds: metricIds, series: series });
	    };
	    Controller.prototype.onTimeframeInspection = function (timeframe, config) {
	        if (_.isEqual(this.timeframe, timeframe) && _.isEqual(this._metricIds, config.metricIds)) {
	            return;
	        }
	        if (!_.isEqual(this._metricIds, config.metricIds)) {
	            this.isReset = true;
	        }
	        this._metricIds = config.metricIds || [];
	        this.seriesDictionary = config.series || {};
	        this.timeframe = (timeframe) ? new time_data_1.Timeframe().start(moment(timeframe.start())).end(moment(timeframe.end())) : null;
	        this.refresh();
	    };
	    return Controller;
	}());
	Controller.$inject = ['$log', '$q', '$scope', '$timeout', 'interactionService', 'metricService', 'chartManager'];
	exports.default = Controller;


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../../ref.d.ts" />
	"use strict";
	DataExplorerDirective.$inject = ["$log", "$filter", "interactionService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	DataExplorerDirective.prototype.$inject = ['$log', '$filter', 'interactionService'];
	function DataExplorerDirective($log, $filter, interactionService) {
	    return {
	        restrict: 'E',
	        controller: 'DataExplorerController',
	        controllerAs: 'controller',
	        templateUrl: 'app/components/dataExplorer/data-explorer-directive.html',
	        scope: {},
	        link: link
	    };
	    function link(scope, element, attrs) {
	        // init logic
	    }
	}
	exports.default = angular.module('perfstack.components.dataExplorer.directive', [interaction_service_1.default])
	    .directive('psDataExplorer', DataExplorerDirective)
	    .name;


/***/ }),
/* 73 */
/***/ (function(module, exports) {

	"use strict";
	EventsExplorerDirective.$inject = ["$log", "$filter", "interactionService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	EventsExplorerDirective.prototype.$inject = ['$log', '$filter', 'interactionService'];
	function EventsExplorerDirective($log, $filter, interactionService) {
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/dataExplorer/events/events-directive.html',
	        scope: {
	            parentController: '='
	        },
	        link: link
	    };
	    function link(scope, element, attrs) {
	        // init logic
	    }
	}
	exports.default = angular.module('perfstack.components.dataExplorer.events', [])
	    .directive('psEventsExplorer', EventsExplorerDirective)
	    .name;


/***/ }),
/* 74 */
/***/ (function(module, exports) {

	"use strict";
	AlertsExplorerDirective.$inject = ["$log", "$filter", "interactionService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	AlertsExplorerDirective.prototype.$inject = ['$log', '$filter', 'interactionService'];
	function AlertsExplorerDirective($log, $filter, interactionService) {
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/dataExplorer/alerts/alerts-directive.html',
	        scope: {
	            parentController: '='
	        },
	        link: link
	    };
	    function link(scope, element, attrs) {
	        // init logic
	    }
	}
	exports.default = angular.module('perfstack.components.dataExplorer.alerts', [])
	    .directive('psAlertsExplorer', AlertsExplorerDirective)
	    .name;


/***/ }),
/* 75 */
/***/ (function(module, exports) {

	"use strict";
	DpaExplorerDirective.$inject = ["$log", "$filter", "interactionService", "getTextService"];
	Object.defineProperty(exports, "__esModule", { value: true });
	DpaExplorerDirective.prototype.$inject = ['$log', '$filter', 'interactionService', 'getTextService'];
	function DpaExplorerDirective($log, $filter, interactionService, getTextService) {
	    return {
	        restrict: 'E',
	        templateUrl: 'app/components/dataExplorer/dpa/dpa-directive.html',
	        scope: {
	            parentController: '='
	        },
	        link: link
	    };
	    function link(scope, element, attrs) {
	        scope.getPsInfo1Label = function (observation) {
	            switch (observation.propertyBag.primaryDimension) {
	                case "TopWaiters":
	                    return getTextService("Waiting Time");
	                case "TopRootBlockers":
	                    return getTextService("Total Impact");
	                case "Deadlockcounts":
	                    return getTextService("Wait Time Impact");
	                default:
	                    return getTextService("Wait Time");
	            }
	        };
	        scope.getPsInfo2Label = function (observation) {
	            switch (observation.propertyBag.primaryDimension) {
	                case "Deadlockcounts":
	                    return getTextService("# of Sessions");
	                default:
	                    return getTextService("% of Total");
	            }
	        };
	        scope.isExpandable = function (observations) {
	            var observation = _.first(observations);
	            var result = false;
	            if (observation && observation.propertyBag) {
	                if (observation.propertyBag.primaryDimension && observation.propertyBag.primaryDimension === "Deadlockcounts") {
	                    result = false;
	                }
	                else {
	                    result = observation.propertyBag.executions ||
	                        observation.propertyBag.databases ||
	                        observation.propertyBag.dbUsers ||
	                        observation.propertyBag.programs ||
	                        observation.propertyBag.text;
	                }
	            }
	            return result;
	        };
	        // HACK: Message is the only mappable property shared between observations and subseries
	        scope.getColorClass = function (message, propertyMap) {
	            if (propertyMap === void 0) { propertyMap = 'displayName'; }
	            var className = 'ps-color-10';
	            _.forEach(_.values(scope.parentController.seriesDictionary), function (series) {
	                if (message === series[propertyMap]) {
	                    className = series.colorClass;
	                }
	                _.forEach(series.subSeries, function (subSeries) {
	                    if (message === subSeries[propertyMap]) {
	                        className = subSeries.colorClass;
	                    }
	                });
	            });
	            return className;
	        };
	    }
	}
	exports.default = angular.module('perfstack.components.dataExplorer.dpa', [])
	    .directive('psDpaExplorer', DpaExplorerDirective)
	    .name;


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var inspection_menu_controller_1 = __webpack_require__(77);
	var inspection_menu_directive_1 = __webpack_require__(78);
	var time_service_1 = __webpack_require__(38);
	exports.default = angular.module('perfstack.components.inspectionMenu', [inspection_menu_directive_1.default, time_service_1.default])
	    .controller('InspectionMenuController', inspection_menu_controller_1.default)
	    .name;


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	var Controller = (function () {
	    function Controller($log, $q, $scope, interactionService, timeService) {
	        var _this = this;
	        this.$log = $log;
	        this.$q = $q;
	        this.$scope = $scope;
	        this.interactionService = interactionService;
	        this.timeService = timeService;
	        this._interactionSubIds = [];
	        this.destroy = function () {
	            _.forEach(_this._interactionSubIds, function (subId) {
	                _this.interactionService.unsubscribe(subId);
	            });
	        };
	        this.$onInit = function () {
	            _this.subscribeToInteractions();
	            _this.$scope.$on('$destroy', _this.destroy);
	        };
	    }
	    Controller.prototype.show = function (coords) {
	        if (coords) {
	            this.$scope.updatePosition(coords);
	        }
	        this.$scope.show();
	        return this;
	    };
	    Controller.prototype.hide = function () {
	        this.$scope.hide();
	        return this;
	    };
	    Controller.prototype.goExplore = function () {
	        this.interactionService.inspectToExplore(this._metricIds, { series: this._seriesHash });
	        this.hide();
	    };
	    Controller.prototype.goZoomIn = function () {
	        this.interactionService.inspectToZoom(interaction_service_1.ZoomDirection.In, this._timeframe);
	        this.hide();
	    };
	    Controller.prototype.goZoomOut = function () {
	        this.interactionService.inspectToZoom(interaction_service_1.ZoomDirection.Out, this._timeframe);
	        this.hide();
	    };
	    Controller.prototype.cancel = function () {
	        this.interactionService.inspect(null, {});
	        this.hide();
	    };
	    Controller.prototype.isZoomInDisabled = function () {
	        //Disabled if time window is less than ~ 10 minutes
	        return (this.timeService.current().end().diff(this.timeService.current().start(), 'minutes') < 11);
	    };
	    Controller.prototype.isExploringDisabled = function () {
	        return !this._isExploringEnabled;
	    };
	    Controller.prototype.updateInspectionMenu = function (inspectionArea, config) {
	        if (config === void 0) { config = {}; }
	        this.$scope.updatePosition([inspectionArea.left, inspectionArea.top]);
	        if (config.showMenu === false && !this.$scope.isInteracting()) {
	            this.hide();
	        }
	        else if (config.showMenu) {
	            var isExploringEnabled = _.isBoolean(config.isExploringEnabled) ? config.isExploringEnabled : false;
	            if (this._isExploringEnabled !== isExploringEnabled) {
	                this._isExploringEnabled = isExploringEnabled;
	                this.$scope.$digest();
	            }
	            this.show();
	        }
	        if (config.metricIds) {
	            this._metricIds = config.metricIds;
	        }
	        if (config.series) {
	            this._seriesHash = config.series;
	        }
	    };
	    Controller.prototype.subscribeToInteractions = function () {
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.inspectDisplayUpdate, this.updateInspectionMenu, this));
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.inspect, this.onInspectionInteraction, this));
	        this._interactionSubIds.push(this.interactionService.subscribe(interaction_service_1.InteractionMessages.time, this.onTimeInteraction, this));
	    };
	    Controller.prototype.onTimeInteraction = function (value) {
	        //Trigger publish w null closes inspection Menu
	        if (value === null && (!this.$scope.isInteracting() && (d3.event && d3.event.relatedTarget) && d3.event.relatedTarget.className !== 'ps-inspection-menu')) {
	            this.hide();
	        }
	    };
	    Controller.prototype.onInspectionInteraction = function (timeframe) {
	        this._timeframe = timeframe;
	        if (timeframe) {
	            this.show();
	        }
	        else {
	            this.hide();
	        }
	    };
	    ;
	    return Controller;
	}());
	Controller.$inject = ['$log', '$q', '$scope', 'interactionService', 'timeService'];
	exports.default = Controller;


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

	/// <reference path="../../../ref.d.ts" />
	"use strict";
	InspectionMenuDirective.$inject = ["$log", "$filter"];
	Object.defineProperty(exports, "__esModule", { value: true });
	var interaction_service_1 = __webpack_require__(11);
	InspectionMenuDirective.prototype.$inject = ['$log', '$filter'];
	function InspectionMenuDirective($log, $filter) {
	    return {
	        restrict: 'E',
	        controller: 'InspectionMenuController',
	        controllerAs: 'controller',
	        templateUrl: 'app/components/inspectionMenu/inspection-menu-directive.html',
	        scope: {},
	        link: link
	    };
	    function link(scope, element, attrs) {
	        scope._isInteracting = false;
	        element.on('mouseenter', function () {
	            scope._isInteracting = true;
	            scope.show();
	        });
	        element.on('mouseleave', function () {
	            scope._isInteracting = false;
	            scope.hide();
	        });
	        scope.updatePosition = function (coords) {
	            if (coords[1] === null) {
	                element.css({ left: coords[0] });
	            }
	            else {
	                var pageHeader = $('.ps-page-header'); //Hack: shouldn't know about this
	                var maxTop = $('body').height() - element.outerHeight();
	                var minTop = (pageHeader.is(':visible')) ? pageHeader.offset().top + pageHeader.outerHeight() : 0;
	                if (coords[1] < minTop) {
	                    coords[1] = minTop;
	                }
	                if (coords[1] > maxTop) {
	                    coords[1] = maxTop;
	                }
	                element.css({ left: coords[0], top: coords[1] });
	            }
	        };
	        scope.show = function () {
	            element.removeClass('ps-hidden');
	        };
	        scope.hide = function () {
	            element.addClass('ps-hidden');
	        };
	        scope.isInteracting = function () {
	            // return element.is(':hover');
	            return scope._isInteracting;
	        };
	        scope.hide();
	        $(document.body).append(element);
	        scope.$on('$destroy', function () {
	            element.off(['mouseenter,mouseleave']);
	            element.remove();
	        });
	    }
	}
	exports.default = angular.module('perfstack.components.contextMenu.directive', [interaction_service_1.default])
	    .directive('psInspectionMenu', InspectionMenuDirective)
	    .name;


/***/ }),
/* 79 */
/***/ (function(module, exports) {

	/// <reference path='../../ref.d.ts' />
	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var AnalyticsService = (function () {
	    function AnalyticsService() {
	        this.trackers = {};
	    }
	    AnalyticsService.prototype.sendEvent = function (category, action, label, fields) {
	        label += (fields && fields.eventValueUnit) ? "(" + fields.eventValueUnit + ")" : "";
	        ga("gtm1.send", "event", category, action, label, fields);
	    };
	    ;
	    return AnalyticsService;
	}());
	AnalyticsService.$inject = [];
	exports.AnalyticsService = AnalyticsService;
	exports.default = angular.module('perfstack.analytics.AnalyticsService', [])
	    .service('analyticsService', [AnalyticsService])
	    .name;


/***/ })
/******/ ]);
angular.module("perfstack").run(["$templateCache", function($templateCache) {$templateCache.put("app/components/dataExplorer/data-explorer-directive.html","<div class=ps-data-explorer-content xui-busy=\"(controller.isBusy > 0)\"><div ng-if=controller.showEmptyState() class=ps-empty><div class=ps-help-title _t>What is this tab?</div><div class=ps-help-figure></div><p _t>The Data Explorer lets you drill into charts.</p><p _t>Click and drag a selection on a chart to open the Data Explorer to the raw data feeding into your selection. You can also open the Data Explorer from the menu that opens when you\'ve made a selection.</p><p _t>If you\'ve already created some charts, try clicking on one now!</p><p _t>If you haven\'t created any charts, use the Metric Palette tab up there to create a few charts.</p></div><div class=ps-content-wrapper ng-if=!controller.showEmptyState()><ps-events-explorer ng-if=\"controller.isEventType() || controller.isPolicyEvent()\" parent-controller=controller></ps-events-explorer><ps-alerts-explorer ng-if=controller.isAlertType() parent-controller=controller></ps-alerts-explorer><ps-dpa-explorer ng-if=controller.isDpaWaitTime() parent-controller=controller></ps-dpa-explorer></div></div>");
$templateCache.put("app/components/dialog/add-entities-dialog.html","<xui-dialog class=ps-add-entities><ps-entity-context-curator selection=vm.dialogOptions.viewModel.entitySelection relative=vm.dialogOptions.viewModel.relative></ps-entity-context-curator></xui-dialog>");
$templateCache.put("app/components/dialog/load-project-dialog.html","<xui-dialog class=ps-load-project><ps-project-curator selection=vm.dialogOptions.viewModel.projectSelection></ps-project-curator></xui-dialog>");
$templateCache.put("app/components/dialog/save-project-dialog.html","<xui-dialog><xui-textbox name=displayName caption=\"_t(Project Name)\" placeholder=\"_t(Project Name)\" ng-model=vm.dialogOptions.viewModel.displayName validators=\"required,maxlength=50\" _ta></xui-textbox></xui-dialog>");
$templateCache.put("app/components/dragDrop/ps-drop-target-directive.html","<div class=ps-drop-shim ng-class=\"{\'ps-drop-disabled\':!psDropTarget}\"><div class=ps-inner-shim></div><h1 ng-if=psDropTarget>{{dropLabelActive}}</h1><h1 ng-if=!psDropTarget>{{dropLabelInactive}}</h1></div>");
$templateCache.put("app/components/entityContextCurator/entity-context-curator-directive.html","<div class=ps-entity-context-curator-content xui-busy=\"(controller.isBusy > 0)\"><xui-filtered-list class=ps-filter-list remote-control=controller.remoteControl filter-property-groups=controller.filterGroups filter-properties-fn=controller.getFilterProperties() filter-values=controller.filterValues items-source=controller.entities selection=selection options=controller.listOptions pagination-data=controller.pagination on-refresh=\"controller.refresh(filters, pagination, sorting, search)\"><!--<div xui-busy=\"(controller.isGridBusy > 0)\"></div>--><div><xui-dropdown class=ps-type-filter-selector items-source=controller.typesFilterModel.values ng-model=controller.selectedType display-value=title dropdown-append-to-body=false></xui-dropdown></div></xui-filtered-list><div class=ps-selected-list><div class=ps-title><h4 _t>SELECTED: ({{selection.items.length}})</h4><xui-button class=ps-clear-all size=small display-style=tertiary ng-click=controller.clearSelection() _t>Clear All</xui-button></div><div ng-repeat=\"entity in selection.items\" class=\"ps-entity-tile ps-selected\" ng-repeat=\"entity in entityList\"><xui-icon class=ps-entity-status-icon icon=status_{{::entity.statusIconPostfix}} icon-size=small></xui-icon><div class=ps-title>{{::entity.displayName}}</div><xui-button class=ps-button-remove icon=close size=small display-style=tertiary ng-click=selection.items.splice($index,1)></xui-button></div></div></div>");
$templateCache.put("app/components/entityContextCurator/entity-list-item-empty-template.html","<div class=\"ps-entity-list-item ps-empty\"><p _t>No entities to display.</p></div>");
$templateCache.put("app/components/entityContextCurator/entity-list-item-template.html","<div class=ps-entity-list-item><div class=ps-item-title><xui-icon style=\"margin:0px 7px\" icon=\"{{::item.instanceType | iconEntityMap}}\" is-dynamic=true css-class=ps-icon icon-size=small status={{::item.statusIconPostfix}}></xui-icon>{{::item.displayName}}</div><div ng-show=\"item.ancestorDisplayNames.length > 0\" class=ps-item-subtitle><div class=ps-item-subtitle-emphasize uib-tooltip=\"{{::item.ancestorDisplayNames.join(\' / \')}}\"><span _t>on</span> {{::item.ancestorDisplayNames | entityAncestors}}</div></div><!--For later--><!--<a ng-href=\"{{::item.detailsUrl}}\" display-style=\"link\">view details</a>--></div>");
$templateCache.put("app/components/inspectionMenu/inspection-menu-directive.html","<div class=ps-inspection-menu><xui-button title=\"_t(Inspect selection in the data explorer)\" _ta size=small icon=details class=ps-data-explorer-go-explore display-style=tertiary ng-click=controller.goExplore() is-disabled=controller.isExploringDisabled()></xui-button><div class=ps-divider></div><xui-button title=\"_t(Zoom In)\" _ta size=small icon=zoom-in class=ps-data-explorer-zoom-in display-style=tertiary ng-click=controller.goZoomIn() is-disabled=controller.isZoomInDisabled()></xui-button><xui-button title=\"_t(Zoom Out)\" _ta size=small icon=zoom-out class=ps-data-explorer-zoom-out display-style=tertiary ng-click=controller.goZoomOut()></xui-button><div class=ps-divider></div><xui-button title=\"_t(Clear selection)\" _ta size=small icon=remove class=ps-data-explorer-cancel display-style=tertiary ng-click=controller.cancel()></xui-button></div>");
$templateCache.put("app/components/projectCurator/project-curator-directive.html","<div class=ps-project-curator-content><xui-filtered-list class=ps-filter-list items-source=controller.projects selection=selection options=controller.listOptions sorting=controller.sorting pagination-data=controller.pagination on-refresh=\"controller.refresh(filters, pagination, sorting, search)\"></xui-filtered-list></div>");
$templateCache.put("app/components/projectCurator/project-list-item-template.html","<div class=ps-project-list-item><div class=ps-item-title>{{::item.displayName}}</div><div class=ps-item-subtitle><div class=ps-item-updated><span class=ps-label _t>Last Updated: </span>{{::item.updateDateTime.local().format(\'LLL\')}}</div><div class=ps-item-created><span class=ps-label _t>Created: </span>{{::item.createDateTime.local().format(\'LLL\')}}</div><div class=ps-item-owner><span class=ps-label _t>Owner: </span>{{::item.owner}}</div></div></div>");
$templateCache.put("app/components/tile/tile-legend-gauge-directive.html","<div class=ps-tile><div class=\"ps-legend-tile ps-live\"><div class=\"ps-color-icon {{::series.colorClass}}\">{{getLegendValue()|siDigitsExtended:series.isPercentile:false}}<div class=ps-unit-label>{{getLegendValue()|siPrefix:series.isPercentile}}</div></div><div class=ps-label-area><div class=ps-title><div class=ps-name title={{::series.displayName}}>{{::series.displayName}}</div><div class=ps-unit>{{::series.unitName|unitLabel:[\'%\']}}</div><xui-icon ng-if=::series.realTime icon-size=small icon=ps-icon-fast-polling-{{getFastPollStatus()}} css-class=ps-icon-fast-poll is-dynamic=true></xui-icon><xui-icon class=transformedIcon ng-if=\"series.transformer != null\" icon-size=small icon-color=primary-blue icon=performance css-class=\"ps-icon-fast-poll ps-display-transform\" is-dynamic=true></xui-icon></div><div class=ps-entity-name title={{::series.entityDisplayName}}>{{::series.entityDisplayName}}</div><div class=ps-ancestry ng-if=\"series.ancestorDisplayNames && series.ancestorDisplayNames.length\" title=\"{{::series.ancestorDisplayNames.join(\' / \')}}\"><span _t>on</span> {{::series.ancestorDisplayNames.join(\' / \')}}</div></div><xui-menu icon=menu class=\"ps-button menu {{(series.transformer == null)? \'\':\'ps-active\'}}\" display-style=default><xui-menu-action action=onRemove() title=\"_t(Remove this metric from the chart)\" _ta _t>Remove</xui-menu-action><xui-menu-divider></xui-menu-divider><xui-menu-header _t>Display Transforms</xui-menu-header><xui-menu-action action=\"toggleTransformer(series, \'none\')\" title=\"_t(remove active display transforms)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'none\'}\" _t>None</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'changePoint\')\" title=\"_t(detect significant moments of change in data set)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'changePoint\'}\" _t>Change Point</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'difference\')\" title=\"_t(understand volatility of data set)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'difference\'}\" _t>Difference</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'linear\')\" title=\"_t(apply a best-fit linear trend line)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'linear\'}\" _t>Linear Regression</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'normalize\')\" title=\"_t(adjust data values to a 0-1 scale)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'normalize\'}\" _t>Normalized</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'percentileStd\')\" title=\"_t(find outliers by centering data around the median)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'percentileStd\'}\" _t>Percentile Standardized</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'smoothing\')\" title=\"_t(reduce noise to see clearer patterns)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'smoothing\'}\" _t>Smoothing</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'loessStandardize\')\" title=\"_t(see clearer patterns and find outliers)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'loessStandardize\'}\" _t>Smoothing/Standardized</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'standardize\')\" title=\"_t(find outliers by centering data around the mean)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'standardize\'}\" _t>Standardized</div></xui-menu-action><xui-menu-action action=\"toggleTransformer(series, \'floatingAverage\')\" title=\"_t(hourly floating average)\" _ta><div ng-class=\"{\'ps-active\': showIcon === \'floatingAverage\'}\" _t>Floating average</div></xui-menu-action></xui-menu><!--<xui-button class=\"ps-button-remove\" icon=\"close\" size=\"small\" ng-click=\"onRemove()\" title=\"_t(Remove this metric from the chart)\" _ta></xui-button>--></div><div class=\"ps-legend-tile ps-interaction\"><div class=\"ps-color-icon {{::series.colorClass}}\"><span class=ps-value-container>{{series.data[series.data.length-1].value|siDigitsExtended:series.isPercentile:series.markType === 1}}</span><div class=ps-unit-label><span class=ps-unit-container>{{series.data[series.data.length-1].value|siPrefix:series.isPercentile}}</span></div></div><div class=ps-label-area><div class=ps-title><div class=ps-name>{{::series.displayName}}</div><div class=ps-unit>{{::series.unitName|unitLabel:[\'%\']}}</div><xui-icon ng-if=::series.realTime icon=ps-icon-fast-polling-{{getFastPollStatus()}}-inverted css-class=ps-icon-fast-poll is-dynamic=true></xui-icon><xui-icon class=transformedIcon ng-if=\"series.transformer != null\" icon-size=small icon-color=white icon=performance css-class=\"ps-icon-fast-poll ps-display-transform\" is-dynamic=true></xui-icon></div><div class=ps-entity-name>{{::series.entityDisplayName}}</div><div class=ps-ancestry ng-if=\"series.ancestorDisplayNames && series.ancestorDisplayNames.length\"><span _t>on</span> {{::series.ancestorDisplayNames.join(\' / \')}}</div></div></div></div>");
$templateCache.put("app/components/tile/tile-legend-multi-directive.html","<div class=ps-tile><div class=\"ps-legend-tile ps-gauge ps-live\"><div class=\"ps-color-icon ps-parent\" ng-class=\"{\'ps-active\':(series.type === 4 && getLegendValue() > 0)}\">{{getLegendValue()|siDigitsExtended:series.isPercentile:false}}<div class=ps-unit-label>{{getLegendValue()|siPrefix:series.isPercentile}}</div></div><!--<div class=\"ps-unit-label\">{{::series.displayUnit}}</div>--><div class=ps-label-area><div class=ps-title><div class=ps-name title={{::series.displayName}}>{{::series.displayName}}</div><div class=ps-unit>{{::series.unitName|unitLabel:[\'%\']}}</div></div><div class=ps-entity-name title={{::series.entityDisplayName}}>{{::series.entityDisplayName}}</div><div class=ps-ancestry ng-if=\"series.ancestorDisplayNames && series.ancestorDisplayNames.length\" title=\"{{::series.ancestorDisplayNames.join(\' / \')}}\"><span _t>on</span> {{::series.ancestorDisplayNames.join(\' / \')}}</div></div><xui-button class=ps-button-remove icon=close size=small ng-click=onRemove()></xui-button></div><div class=\"ps-legend-tile ps-gauge ps-interaction\"><div class=\"ps-color-icon {{::series.colorClass}} ps-parent\"><span class=ps-value-container>{{series.data[series.data.length-1].value|siDigitsExtended:series.isPercentile:(series.type === 3 || series.type === 4)}}</span><div class=ps-unit-label><span class=ps-unit-container>{{series.data[series.data.length-1].value|siPrefix:series.isPercentile}}</span></div></div><!--<div class=\"ps-unit-label\">{{::series.displayUnit}}</div>--><div class=ps-label-area><div class=ps-title><div class=ps-name>{{::series.displayName}}</div><div class=ps-unit>{{::series.unitName|unitLabel:[\'%\']}}</div></div><div class=ps-entity-name>{{::series.entityDisplayName}}</div><div class=ps-ancestry ng-if=\"series.ancestorDisplayNames && series.ancestorDisplayNames.length\"><span _t>on</span> {{::series.ancestorDisplayNames.join(\' / \')}}</div></div><xui-button class=ps-button-remove icon=close size=small ng-click=onRemove()></xui-button></div></div><ps-tile-legend-multi-part ng-repeat=\"subSeries in series.subSeries\" series=series sub-series=subSeries sub-series-id={{subSeries.id}} sub-index={{$index}} chart-id={{chartId}}></ps-tile-legend-multi-part>");
$templateCache.put("app/components/tile/tile-legend-multi-part-directive.html","<div class=ps-tile><div class=\"ps-legend-tile ps-legend-sub-tile ps-live\"><div class=\"ps-color-icon {{subSeries.colorClass}}\">{{getLegendValue()|siDigitsExtended:series.isPercentile:false}} {{getLegendValue()|siPrefix:subSeries.isPercentile}}</div><div class=ps-label-area><div class=ps-title><div class=ps-name title={{::subSeries.displayName}}>{{::subSeries.displayName}}</div></div></div></div><div class=\"ps-legend-tile ps-legend-sub-tile ps-interaction\"><div class=\"ps-color-icon {{subSeries.colorClass}}\"><span class=ps-value-container>{{subSeries.data[subSeries.data.length-1].value|siDigitsExtended:series.isPercentile:(series.type === 3 || series.type === 4)}}</span> <span class=ps-unit-container>{{subSeries.data[subSeries.data.length-1].value|siPrefix:subSeries.isPercentile}}</span></div><div class=ps-label-area><div class=ps-title><div class=ps-name>{{::subSeries.displayName}}</div></div></div></div></div>");
$templateCache.put("app/components/tile/tile-legend-status-directive.html","<div class=ps-tile><div class=\"ps-legend-tile ps-live\"><div class=\"ps-color-icon {{series.colorClass}}\" style=\"background-color: {{series.data[series.data.length-1].value|statusColorMap}}\">{{series.data[series.data.length-1].metadata[series.data[series.data.length-1].metadata.length-1].message}}</div><div class=ps-label-area><div class=ps-title><div class=ps-name title={{::series.displayName}}>{{::series.displayName}}</div><div class=ps-unit>{{::series.unitName|unitLabel:[\'%\']}}</div></div><div class=ps-entity-name title={{::series.entityDisplayName}}>{{::series.entityDisplayName}}</div><div class=ps-ancestry ng-if=\"series.ancestorDisplayNames && series.ancestorDisplayNames.length\" title=\"{{::series.ancestorDisplayNames.join(\' / \')}}\"><span _t>on</span> {{::series.ancestorDisplayNames.join(\' / \')}}</div></div><xui-button class=ps-button-remove icon=close size=small ng-click=onRemove()></xui-button></div><div class=\"ps-legend-tile ps-interaction\"><div class=\"ps-color-icon {{series.colorClass}}\"><span class=ps-value-container></span></div><div class=ps-label-area><div class=ps-title><div class=ps-name>{{::series.displayName}}</div><div class=ps-unit>{{::series.unitName|unitLabel:[\'%\']}}</div></div><div class=ps-entity-name>{{::series.entityDisplayName}}</div><div class=ps-ancestry ng-if=\"series.ancestorDisplayNames && series.ancestorDisplayNames.length\"><span _t>on</span> {{::series.ancestorDisplayNames.join(\' / \')}}</div></div><xui-button class=ps-button-remove icon=close size=small ng-click=onRemove()></xui-button></div></div>");
$templateCache.put("app/views/analysis/analysis-view.html","<xui-page-content class=ps-analysis page-layout=fill is-busy=\"controller.isBusy > 0\" ng-class=\"{\'ps-full-screen\':controller.isFullScreen}\"><xui-page-header><div class=ps-page-header><div class=ps-branding><div class=ps-title>PERFSTACK</div><div class=ps-subtitle>analysis</div></div><h1>{{controller.project.displayName}}</h1><xui-toolbar><xui-toolbar-item><xui-button icon=\"ps-icon-fast-polling-{{(controller.isFastPolling)? \'active\':\'inactive\'}}\" display-style=tertiary ng-click=controller.validateFastPollingRequest(true) is-disabled=controller.isFastPollingDisabled() ng-show=controller.hasRealtimePollingPermission()>{{controller.getFastPollingLabel()}}</xui-button></xui-toolbar-item><xui-toolbar-item><xui-button icon=add display-style=tertiary ng-click=controller.newProject() _t>New</xui-button></xui-toolbar-item><xui-toolbar-item><xui-button icon=save display-style=tertiary ng-click=controller.saveProject() is-disabled=controller.isSaveDisabled() _t>Save</xui-button></xui-toolbar-item><xui-menu class=ps-load-menu icon=browse title=_t(Load) display-style=tertiary _ta><xui-menu-action action=controller.loadProject(project.id) class=ps-project-load-action ng-repeat=\"project in controller.recentProjects\">{{::project.displayName}}</xui-menu-action><xui-menu-divider ng-if=\"controller.recentProjects.length > 0\"></xui-menu-divider><xui-menu-action action=controller.showLoadProjectDialog() _t>Find Projects...</xui-menu-action></xui-menu><xui-toolbar-item><xui-button icon=export display-style=tertiary ng-click=controller.exportProject() is-disabled=!controller.isProjectExportable() _t>Export</xui-button></xui-toolbar-item><xui-toolbar-item><!-- Share Button --><xui-button display-style=tertiary xui-clipboard=controller.updateShareLink() on-clipboard-success=controller.toastShareLinkCopied() on-clipboard-error=controller.toastShareLinkCopied(error) icon=copy _t>Share</xui-button></xui-toolbar-item><xui-toolbar-divider></xui-toolbar-divider><xui-toolbar-item><xui-menu title=_t(MORE) display-style=tertiary _ta><xui-menu-action action=controller.showSaveAsProjectDialog() _t>Save As...</xui-menu-action><xui-menu-action action=controller.deleteProject() is-disabled=controller.isDeleteDisabled() _t>Delete</xui-menu-action><xui-menu-divider></xui-menu-divider><xui-menu-link url={{::controller.helpLink}} target=_blank _t>Help</xui-menu-link></xui-menu></xui-toolbar-item></xui-toolbar></div></xui-page-header><xui-panel class=\"ps-metric-tile-panel ps-view\" is-collapsible=true pane-width=568 panel-layout=fill is-collapsed=controller.isMetricPaletteCollapsed collapse-changed=controller.onPanelToggleComplete()><xui-panel-left-pane><ps-panel-header><xui-tabs default-tab-id=metric-selector selected-tab-id=controller.leftPanelSelectedTab class=ps-tab><xui-tab tab-id=metric-selector tab-title=\"_t(Metric Palette)\" _ta></xui-tab><xui-tab tab-id=data-explorer tab-title=\"_t(Data Explorer)\" _ta></xui-tab></xui-tabs></ps-panel-header><ps-data-explorer ng-show=\"controller.leftPanelSelectedTab === \'data-explorer\'\"></ps-data-explorer><div class=ps-metric-selector xui-busy=\"controller.isMetricSelectorBusy > 0\" ng-show=\"controller.leftPanelSelectedTab === \'metric-selector\'\"><div class=ps-entity-filter><xui-search ng-show=\"controller.entities.length > 0\" value=controller.entitySearchTerm class=ps-entity-search placeholder=\"_t(Find an entity...)\" name=ps-entity-search items-source=[] _ta></xui-search><!-- HACK: Disabling multi-select for v1\n              <xui-expander class=\"ps-entity-group ps-static-group\">\n                <xui-expander-heading>\n                  <div class=\"ps-entity-group-title\" _t>SELECTED</div><div class=\"ps-entity-group-count\">({{controller.getSelectedEntities().length}})</div>\n                </xui-expander-heading>\n                <div class=\"ps-entity-tile\" ng-repeat=\"entity in controller.getSelectedEntities()\" ng-class=\"{\'ps-selected\': controller.isEntitySelected(entity)}\" ng-click=\"controller.toggleEntitySelection(entity)\">\n                  <xui-icon class=\"ps-entity-status-icon\" icon=\"status_{{::entity.statusIconPostfix}}\" icon-size=\"small\"></xui-icon>\n                  <div class=\"ps-title\">{{::entity.displayName}}</div>\n                </div>\n              </xui-expander>\n              END HACK --><div class=ps-entity-toolbar><xui-button ng-click=controller.showAddEntitiesDialog() class=add-entities-btn icon=add display-style=tertiary _t>Add Entities</xui-button><xui-button ng-click=\"controller.buildEntityContext(true, true)\" class=add-entities-btn icon=reset display-style=tertiary uib-tooltip=\"_t(Reset to the current project\'s entity context.)\" tooltip-append-to-body=true is-disabled=!controller.charts.length _ta></xui-button></div><div class=ps-entity-group-divider></div><div ng-show=\"controller.entities.length === 0\"><div class=ps-warning-header _t>Load some entities</div><div class=perfstack-help-1></div><div class=ps-instruction-caption><p _t>Entities are the stuff in your environment, like servers and routers.</p><p _t>Click the <b>Add Entities</b> button up there to pick a few items to work with.</p></div></div><xui-expander class=ps-entity-group ng-repeat=\"(instanceType, entityList) in controller.getEntities() | groupBy:\'instanceType\'\" ng-init=\"expanderModel={isOpen:false}\" is-open=expanderModel.isOpen><xui-expander-heading><div class=ps-expander-wrapper title=\"{{:: entityList[0].groupDisplayName}}\"><xui-icon icon=\"{{::instanceType | iconEntityMap}}\" icon-size=small></xui-icon><div class=ps-entity-group-title>{{:: entityList[0].groupDisplayName}}</div><div class=ps-entity-group-count>({{entityList.length}})</div></div></xui-expander-heading><div ng-if=expanderModel.isOpen><div class=ps-entity-tile ng-repeat=\"entity in entityList track by entity.id\" ng-class=\"{\'ps-selected\': controller.isEntitySelected(entity)}\" ng-click=controller.toggleEntitySelection(entity) ps-draggable draggable-data=entity><xui-icon css-class=ps-entity-status-icon icon=status_{{::entity.statusIconPostfix}} icon-size=small></xui-icon><xui-icon css-class=ps-entity-grip-icon icon=drag icon-size=small></xui-icon><div class=ps-title><span class=ps-entity-name>{{::entity.displayName}}</span><div ng-show=\"entity.ancestorDisplayNames.length > 0\"><span _t>on</span> {{::entity.ancestorDisplayNames|entityAncestors}}</div></div><div class=ps-action-bar><xui-button title=\"_t(Open details view)\" _ta class=ps-button-link icon=ps-link size=small display-style=link ng-click=\"controller.goToDetailsView(entity); $event.stopPropagation();\"></xui-button><xui-button title=\"_t(Add all other entities related to this entity)\" _ta class=ps-button-add-related icon=ps-add-related size=small display-style=link ng-click=\"controller.addRelatedEntitiesToContext(entity); $event.stopPropagation();\"></xui-button><xui-button title=\"_t(Remove this entity from the metric palette)\" _ta class=ps-button-remove icon=close size=small ng-click=\"controller.removeEntityLocally(entity); $event.stopPropagation();\"></xui-button></div></div></div></xui-expander></div><div class=ps-metric-palette xui-busy=\"controller.isMetricPaletteBusy > 0\" ng-class=\"((controller.entities.length == 0) ? \'ps-instructions-disabled\' : \'ps-instructions-enabled\')\"><xui-search ng-show=\"controller.getSelectedEntities(false).length > 0\" class=ps-metric-search placeholder=\"_t(Find a metric...)\" name=ps-metric-search items-source=[] on-change=controller.onMetricSearchTermChanged() ng-click=controller.onMetricSearchTermChanged() _ta></xui-search><div ng-if=\"controller.getSelectedEntities(false).length == 0\" class=ps-infographic-margin><div class=ps-warning-header _t>Pick an entity to work with</div><div class=perfstack-help-2></div><div class=ps-warning-body><p _t>We collect all kinds of data about the entities in your environment.</p><p _t>Click one of the entities to the left. We’ll show you a list of metrics we’ve collected for that entity</p></div></div><xui-expander class=\"ps-metric-group ps-static-group\" ng-if=\"controller.getSelectedEntities(false).length > 0\"><xui-expander-heading><div class=ps-expander-wrapper><div class=ps-metric-group-title _t>Plotted</div><div class=ps-entity-group-count>({{controller.getInUseMetrics().length}})</div></div></xui-expander-heading><ps-tile-metric ng-repeat=\"metric in controller.getInUseMetrics() track by metric.id\" metric=metric title={{::metric.displayName}}></ps-tile-metric></xui-expander><xui-expander class=\"ps-metric-group ps-static-group\" ng-if=\"controller.getSelectedEntities(false).length > 0\"><xui-expander-heading><div class=ps-expander-wrapper><div class=ps-metric-group-title _t>Real-Time Polling</div><div class=ps-entity-group-count>({{controller.getAvailableRealTimeMetrics().length}})</div></div></xui-expander-heading><ps-tile-metric ng-repeat=\"metric in controller.getAvailableRealTimeMetrics() track by metric.id\" metric=metric title={{::metric.displayName}}></ps-tile-metric></xui-expander><div class=ps-entity-group-divider ng-if=\"controller.entities.length > 0\"></div><xui-expander class=ps-metric-group ng-repeat=\"(groupId, metricList) in controller.getMetrics() | groupBy: \'groupId\'\" ng-init=\"expanderModel={isOpen:false}\" is-open=expanderModel.isOpen><xui-expander-heading><div class=ps-expander-wrapper title=\"{{:: metricList[0].groupDisplayName}}\"><div class=ps-metric-group-title>{{:: metricList[0].groupDisplayName}}</div><div class=ps-entity-group-count>({{metricList.length}})</div></div></xui-expander-heading><div ng-if=expanderModel.isOpen><ps-tile-metric ng-repeat=\"metric in metricList track by metric.id\" metric=metric title={{::metric.displayName}}></ps-tile-metric></div></xui-expander></div></div></xui-panel-left-pane><div class=\"ps-panel ps-visualization-panel\"><div class=ps-panel-header ng-show=\"controller.charts.length > 0\"><!--Timeframe Picker Wrapper --><div class=ps-timepicker-wrapper><!--Left Btn Group--><div><xui-button class=ps-timepicker-item display-style=tertiary size=small name=timeBackBtn icon=move-left isbusy=true ng-click=controller.moveTimeWindowBack() is-empty=true ng-model-options=\"{ debounce:1000 }\" is-disabled=controller.isRefreshingChartModels></xui-button><div class=\"ps-timepicker-item ps-timepicker-display\" ng-show=controller.isMetricPaletteCollapsed><div class=ps-blue _t>Start:</div><div>{{controller.getCurrentTimeStart().local().format(\'D MMM YYYY, LT\')}}</div></div></div><!--Center Btn Group--><div class=ps-timepicker-display><xui-timeframe-picker class=ps-timepicker-item on-change=controller.displayDateChanged(model) ng-model=controller.timepickerDisplay.timePickerData presets=controller.timepickerDisplay.xuiPreset datetime-title-format=\"D MMM YYYY, LT\" disabled id=timePicker></xui-timeframe-picker></div><!--Right Btn Group--><div><div class=\"ps-timepicker-item ps-timepicker-display\" ng-show=controller.isMetricPaletteCollapsed><div class=ps-blue _t>End:</div><div>{{controller.getCurrentTimeEnd().local().format(\'D MMM YYYY, LT\')}}</div></div><xui-button class=ps-timepicker-item display-style=tertiary size=small name=timeBackBtn icon=move-right isbusy=true ng-click=controller.moveTimeWindowForward() is-empty=true ng-model-options=\"{ debounce: 1000 }\" is-disabled=controller.forwardBtnDisabled></xui-button><!--<xui-button class=\"ps-timepicker-item\"--><!--display-style=\"tertiary\"--><!--size=\"small\"--><!--name=\"timeBackBtn\"--><!--icon=\"move-right-page\"--><!--isBusy=\"true\"--><!--ng-click=\"controller.moveTimeWindowCurrent()\"--><!--is-empty=\"true\"--><!--ng-model-options=\"{ debounce: 500 }\">--><!--</xui-button>--></div></div><!--Timeframe Picker Wrapper End --><div class=ps-view-controls><xui-button display-style=tertiary size=small icon=\"{{(controller.isFullScreen)? \'fullscreen-exit\':\'fullscreen\'}}\" isbusy=true ng-click=controller.toggleFullScreen() is-empty=true ng-model-options=\"{ debounce: 1000 }\" _ta></xui-button></div></div><!--PS-235 Empty State Instructions--><div ng-show=\"controller.charts.length == 0\" ng-class=\"((controller.getSelectedEntities(false).length == 0 && controller.isMetricPaletteCollapsed == false) ? \'ps-instructions-disabled\' : \'ps-instructions-enabled\')\" class=ps-chart-instructions-wrapper ps-drop-target=true drop-target-data=0 on-drop=\"controller.onCreateDrop(dragData, targetData)\" drop-label-active={{controller.emptyDropMessage}}><div class=ps-chart-instructions-inner><div class=ps-chart-instructions><div class=ps-instruction-box><div class=ps-warning-header _t>Expand groups to find metrics</div><div class=perfstack-help-3></div><div class=ps-warning-body><p _t>Some entities have hundreds of metrics. To keep things organized, metrics are grouped by category.</p><p _t>Expand a category to see all the metric tiles in that category.</p></div></div><div class=ps-instruction-box><div class=ps-warning-header _t>Drag metrics to create charts</div><div class=perfstack-help-4></div><div class=ps-warning-body _t>Grab a metric tile from the left and drag it over to this space. Drop it to create a new chart.</div></div><div class=ps-instruction-box><div class=ps-warning-header _t>Use the URL to share and save</div><div class=perfstack-help-5></div><div class=ps-warning-body _t>You can bookmark the URL, share it, or paste it into a help-desk ticket.</div></div></div></div></div><!-- End PS-235 Empty State Instructions--><div class=ps-chart-create ps-drop-target=true drop-target-data=0 on-drop=\"controller.onCreateDrop(dragData, targetData)\" drop-label-active={{controller.emptyDropMessage}} ng-show=\"controller.charts.length > 0\"></div><div ng-repeat=\"chart in controller.charts track by chart.id\" xui-busy=\"chart.isBusy > 0\"><div class=ps-chart-group ps-drop-target=chart.isDropTarget drop-target-data=chart.id on-drop=\"controller.addMetricToChart(dragData, targetData)\" drop-label-active={{chart.dropMessage}} drop-label-inactive={{chart.dropMessage}} _ta><div class=ps-chart-wrapper><ps-chart ng-style=\"{height: chart.calculatedHeight + \'px\'}\" id={{::chart.id}} model=chart manager=controller.chartManager></ps-chart></div><div class=ps-legend-wrapper><div class=ps-plot-legend-wrapper ng-repeat=\"plot in controller.getOrderedPlots(chart) track by plot.id()\" ng-style=\"{top: plot.config().dimension.y() +\'px\'}\"><div ng-repeat=\"series in plot.series() track by series.id\"><div ng-if=\"series.type === 1 || series.markType === 2\" class=ps-legend-gauges><ps-tile-legend-gauge is-fast-polling=controller.isFastPolling series=series on-remove=\"controller.removeMetricFromChart(series.id, chart.id)\" chart-id={{::chart.id}}></ps-tile-legend-gauge></div><div ng-if=\"series.type === 6 || series.markType === 3 || series.type === 4\" class=ps-legend-states><ps-tile-legend-multi ng-class=\"{\'ps-event\': series.type === 3, \'ps-alert\': series.type === 4, \'ps-area\': series.type === 6}\" series=series on-remove=\"controller.removeMetricFromChart(series.id, chart.id)\" chart-id={{::chart.id}}></ps-tile-legend-multi></div><div ng-if=\"series.type == 5\" class=ps-legend-states><ps-tile-legend-status series=series on-remove=\"controller.removeMetricFromChart(series.id, chart.id)\" chart-id={{::chart.id}}></ps-tile-legend-status></div></div></div></div></div><div class=ps-chart-create ps-drop-target=true drop-target-data=$index+1 on-drop=\"controller.onCreateDrop(dragData, targetData)\" drop-label-active={{controller.emptyDropMessage}}></div></div></div></xui-panel><ps-inspection-menu></ps-inspection-menu></xui-page-content>");
$templateCache.put("app/components/dataExplorer/alerts/alerts-directive.html","<div class=ps-alerts-view><div class=ps-header><div class=ps-sources-container><div class=ps-source-wrapper><div class=ps-entity><h4>{{parentController.sourceMetric.displayName}}</h4></div><div class=ps-metric><a href={{parentController.sourceMetric.entityUrl}} target=_blank>{{parentController.sourceEntity}}</a></div></div><div class=ps-timeframe>{{parentController.timeframe.start().local().format(\'L LT\') + \' - \' + parentController.timeframe.end().local().format(\'L LT\') }}</div></div><div class=ps-controls-wrapper><div class=ps-dropdown><xui-menu title=_t(Filters) _ta><div class=ps-filter-menu-wrapper ng-repeat=\"(key, optionList) in parentController.filterOptions\"><h5>{{::parentController.displayNames[key.toLowerCase()]}}</h5><xui-menu-option is-checked=option.checked ng-repeat=\"option in optionList\">{{::option.title}}</xui-menu-option></div></xui-menu></div><div class=ps-search-field><xui-search value=parentController.searchTerm placeholder=\"_t(Search data observations...)\" on-search=parentController.onSearch(value) _ta></xui-search></div></div></div><div class=ps-content><xui-expander class=ps-observation ng-repeat=\"observation in parentController.observations\"><xui-expander-heading><div class=ps-observation-header><div class=ps-color-icon><xui-icon icon=severity_{{::observation.propertyBag.severity.toLowerCase()}}></xui-icon></div><div class=ps-message-brief><div class=ps-title><a href={{::observation.propertyBag.alertUrl}} target=_blank>{{::observation.propertyBag.message}}</a></div><div class=ps-subtitle _t>Triggered by <a href={{::observation.propertyBag.entityUrl}} target=_blank>{{::observation.propertyBag.entityCaption}}</a> On <a href={{::parentController.sourceMetric.entityUrl}} target=_blank>{{::parentController.sourceMetric.entityDisplayName}}</a></div></div><div class=ps-timestamp>{{::observation.propertyBag.duration | duration}}<div class=ps-active ng-if=::observation.propertyBag.active _t>Active</div></div></div></xui-expander-heading><div class=ps-data><div class=ps-label _t>Duration</div>{{::observation.propertyBag.startTime | inspectTime}} - {{::observation.date | inspectTime:observation.propertyBag.active}}</div><div class=ps-data><div class=ps-label _t>Triggered By</div><a href={{::observation.propertyBag.entityUrl}} target=_blank>{{::observation.propertyBag.entityCaption}}</a></div><div class=ps-data><div class=ps-label _t>Related</div><a href={{::parentController.sourceMetric.entityUrl}} target=_blank>{{::parentController.sourceMetric.entityDisplayName}}</a></div><div class=ps-data><div class=ps-label _t>Message</div>{{::observation.propertyBag.message}}</div></xui-expander></div><div class=ps-footer><xui-pager hide-if-empty=true page=parentController.pagination.page page-size=parentController.pagination.pageSize total=parentController.pagination.total pager-action=\"parentController.onPagination(page, pageSize, total)\"></xui-pager></div></div>");
$templateCache.put("app/components/dataExplorer/dpa/dpa-directive.html","<div class=ps-dpa-view><div class=ps-header><div class=ps-sources-container><div class=ps-source-wrapper><div class=ps-entity><h4>{{parentController.sourceMetric.displayName}}</h4></div><div class=ps-metric><a href={{parentController.sourceMetric.entityUrl}} target=_blank>{{parentController.sourceEntity}}</a></div></div><div class=ps-timeframe></div></div><div class=ps-controls-wrapper><div class=ps-search-field><xui-search value=parentController.searchTerm placeholder=\"_t(Search data observations...)\" on-search=parentController.onSearch(value) _ta></xui-search></div></div><div class=ps-title>{{parentController.sourceMetric.subtitle}} ({{parentController.timeframe.start().local().format(\'L LTS\') + \' - \' + parentController.timeframe.end().local().format(\'L LTS\') }})</div></div><xui-empty ng-if=\"parentController.observations && parentController.observations.length === 0\"></xui-empty><div class=ps-content ng-class=\"{\'ps-unexpandable\': !isExpandable(parentController.observations)}\"><xui-expander class=ps-observation ng-repeat=\"observation in parentController.observations\" ng-class-odd=\"\'ps-even\'\" ng-init=\"expanderModel={isOpen:false}\" is-open=expanderModel.isOpen><xui-expander-heading><div class=ps-observation-header><div ng-if=\"parentController.sourceMetric.metricId !== \'TopWaittime\' && observation.propertyBag.primaryDimension !== \'Deadlockcounts\'\" class=\"ps-color-icon {{getColorClass(observation.propertyBag.message)}}\"></div><div class=ps-message-brief ng-if=\"observation.propertyBag.primaryDimension !== \'Deadlockcounts\'\">{{::observation.propertyBag.message}}</div><div class=ps-message-brief ng-if=\"observation.propertyBag.primaryDimension === \'Deadlockcounts\'\"><a ng-href={{::observation.propertyBag.dpaDetailUrl}} target=_blank>{{::observation.propertyBag.message}}</a></div><div class=ps-info-1 title=\"{{::observation.propertyBag.waitTime | duration:false}}\"><span class=ps-label>{{::getPsInfo1Label(observation)}}:</span>{{::observation.propertyBag.waitTime}}s</div><div class=ps-info-2><span class=ps-label>{{::getPsInfo2Label(observation)}}:</span> {{:: observation.propertyBag.primaryDimension !== \'Deadlockcounts\' ? (observation.propertyBag.totalWaitPercentage | dpaPercentile) : observation.propertyBag.executions }}</div></div></xui-expander-heading><div ng-if=\"isExpandable(parentController.observations) && expanderModel.isOpen\"><div class=ps-data ng-if=::observation.propertyBag.executions><div class=ps-label _t>Executions:</div>{{::observation.propertyBag.executions}}</div><div class=ps-data ng-if=::observation.propertyBag.databases><div class=ps-label _t>Databases:</div>{{::observation.propertyBag.databases}}</div><div class=ps-data ng-if=::observation.propertyBag.dbUsers><div class=ps-label _t>Users:</div>{{::observation.propertyBag.dbUsers}}</div><div class=ps-data ng-if=::observation.propertyBag.programs><div class=ps-label _t>Programs:</div>{{::observation.propertyBag.programs}}</div><div class=ps-data ng-if=::observation.propertyBag.text>{{::observation.propertyBag.text | clipToLineCount:4:482}} &raquo; <a href={{::observation.propertyBag.dpaDetailUrl}} target=_blank ng-if=::observation.propertyBag.dpaDetailUrl _t>Details</a></div></div></xui-expander></div><div class=ps-footer><xui-pager hide-if-empty=true page=parentController.pagination.page page-size=parentController.pagination.pageSize total=parentController.pagination.total pager-action=\"parentController.onPagination(page, pageSize, total)\"></xui-pager></div></div>");
$templateCache.put("app/components/dataExplorer/events/events-directive.html","<div class=ps-events-view><div class=ps-header><div class=ps-sources-container><div class=ps-source-wrapper ng-if=!parentController.isPolicyEventOverEntities()><div class=ps-entity><h4>{{parentController.sourceMetric.displayName}}</h4></div><div class=ps-metric><a href={{parentController.sourceMetric.entityUrl}} target=_blank rel=\"noopener noreferrer\">{{parentController.sourceEntity}}</a></div></div><div class=ps-source-wrapper ng-if=parentController.isPolicyEventOverEntities()><div class=ps-entity><h4>{{parentController.observations[0].propertyBag.policyName}}</h4></div></div><div class=ps-timeframe>{{parentController.timeframe.start().local().format(\'L LT\') + \' - \' + parentController.timeframe.end().local().format(\'L LT\') }}</div></div><div class=ps-controls-wrapper><div class=ps-dropdown ng-if=!(parentController.filterOptions|isEmpty)><xui-menu title=_t(Filters) _ta><div class=ps-filter-menu-wrapper ng-repeat=\"(key, optionList) in parentController.filterOptions\"><h5>{{::parentController.displayNames[key.toLowerCase()]}}</h5><xui-menu-option is-checked=option.checked ng-repeat=\"option in optionList\">{{::option.title}}</xui-menu-option></div></xui-menu></div><div class=ps-search-field><xui-search value=parentController.searchTerm placeholder=\"_t(Search data observations...)\" on-search=parentController.onSearch(value) on-clear=parentController.onSearchClear() _ta></xui-search></div></div></div><div class=ps-content ng-if=parentController.isPolicyEvent()><xui-expander class=ps-observation ng-repeat=\"observation in parentController.observations\"><xui-expander-heading><div class=ps-observation-header><div class=ps-policy-no-severity ng-if=\"observation.propertyBag.severityCode === 0\"></div><div class=\"ps-policy-severity ps-policy-severity-high\" ng-if=\"observation.propertyBag.severityCode === 300\">{{::observation.propertyBag.severity}}</div><div class=\"ps-policy-severity ps-policy-severity-med\" ng-if=\"observation.propertyBag.severityCode === 200\">{{::observation.propertyBag.severity}}</div><div class=\"ps-policy-severity ps-policy-severity-low\" ng-if=\"observation.propertyBag.severityCode === 100\">{{::observation.propertyBag.severity}}</div>&nbsp;<div class=ps-rule-id><a href={{::observation.propertyBag.detailsUrl}} target=_blank rel=\"noopener noreferrer\">{{::observation.propertyBag.ruleDisplayID}}</a>:</div>&nbsp;&nbsp; <span class=status-type-icon ng-if=observation.propertyBag.statusIcon><xui-icon icon-size=small icon={{::observation.propertyBag.statusIcon}}></span>&nbsp;<div>{{::observation.propertyBag.status}}</div><div class=ps-pe-data-inlined ng-if=observation.propertyBag.overEntities><span>on</span> <span><a href={{::observation.propertyBag.policyNodeSubviewUrl}} target=_blank rel=\"noopener noreferrer\">{{::observation.propertyBag.entityName}}</a></span></div><div class=\"ps-timestamp ps-timestamp-fix-layout\">{{::observation.date.local().format(\'L LTS\')}}</div></div></xui-expander-heading><div class=ps-rule-name>{{::observation.propertyBag.ruleName}}</div><div class=\"ps-pe-data ps-label\" _t>Description</div><div class=ps-pe-data-text-pre-wrap>{{::observation.propertyBag.ruleDescription}}</div><div class=\"ps-pe-data ps-label\" _t>Remediation (fix) description</div><div class=ps-pe-data-text-pre-wrap>{{::observation.propertyBag.remediationDescription}}</div><div class=ps-rule-name></div><div class=ps-pe-data><span class=ps-label _t>Policy:</span> <span><a href={{::observation.propertyBag.policyDetailsUrl}} target=_blank rel=\"noopener noreferrer\">{{observation.propertyBag.overEntities?observation.propertyBag.policyName:parentController.sourceMetric.displayName}}</a></span></div><div class=ps-pe-data><span class=ps-label _t>Evaluated on:</span> <span><a href={{::observation.propertyBag.policyNodeSubviewUrl}} target=_blank rel=\"noopener noreferrer\">{{observation.propertyBag.overEntities?observation.propertyBag.entityName:parentController.sourceEntity}}</a></span></div></xui-expander></div><div class=ps-content ng-if=!parentController.isPolicyEvent()><xui-expander class=ps-observation ng-repeat=\"observation in parentController.observations\"><xui-expander-heading><div class=ps-observation-header><div class=\"ps-color-icon ps-color-{{::observation.propertyBag.severity.toLowerCase()}}\"></div><div class=ps-message-brief ng-if=\"parentController.sourceMetric.metricId !== \'ScmChanges\'\">{{::observation.propertyBag.message}} <span ng-if=\"::observation.propertyBag.message === null\" _t>No message data available.</span></div><div class=ps-message-brief ng-if=\"parentController.sourceMetric.metricId === \'ScmChanges\'\">{{::observation.propertyBag.title}} <span class=\"xui-text-a xui-ellipsis\">{{::observation.propertyBag.displayAliasFirstPart}} </span><span class=\"xui-text-s xui-ellipsis\">{{::observation.propertyBag.displayAliasSecondPart}}</span></div><span class=change-type-icon ng-if=observation.propertyBag.changeTypeIcon><xui-icon icon-size=xsmall icon={{::observation.propertyBag.changeTypeIcon}} icon-color=gray uib-tooltip={{::observation.propertyBag.changeTypeTooltip}} tooltip-append-to-body=true tooltip-class=custom-tooltip></span><div class=ps-timestamp>{{::observation.date.local().format(\'L LTS\')}}</div></div></xui-expander-heading><div ng-if=\"parentController.sourceMetric.metricId != \'ConfigurationChanges\' && parentController.sourceMetric.metricId != \'ScmChanges\'\">{{::observation.propertyBag.message}}<div ng-if=::observation.propertyBag.detailUrl><a ng-href={{::observation.propertyBag.detailUrl}} target=_blank rel=\"noopener noreferrer\" _t>&raquo; Details</a></div></div><div class=ps-ncm-data ng-if=\"parentController.sourceMetric.metricId === \'ConfigurationChanges\'\"><div class=ps-data><div class=ps-label _t>Config Title</div><a href={{::observation.propertyBag.configUrl}} target=_blank rel=\"noopener noreferrer\">{{::observation.propertyBag.title}}</a></div><div class=ps-data><div class=ps-label _t>Config Changes</div><a href={{::observation.propertyBag.differenceUrl}} target=_blank rel=\"noopener noreferrer\" _t>View Report</a></div><div class=ps-data><div class=ps-label _t>Triggered By</div><a href={{::observation.propertyBag.ncmDetailsUrl}} target=_blank rel=\"noopener noreferrer\">{{::parentController.sourceMetric.entityDisplayName}}</a></div><div class=ps-data><div class=ps-label _t>Message</div>{{::observation.propertyBag.message}}</div></div><div class=ps-ncm-data ng-if=\"parentController.sourceMetric.metricId === \'ScmChanges\'\"><div class=ps-data><div class=ps-label _t>Profile</div>{{::observation.propertyBag.title}}</div><div class=ps-data ng-if=observation.propertyBag.lastModifiedBy><div class=ps-label _t>Changed by</div><span class=xui-text-s>{{::observation.propertyBag.lastModifiedBy}}</span></div><div class=ps-data><div class=ps-label _t>Element</div><a href={{::observation.propertyBag.differenceUrl}} target=_blank rel=\"noopener noreferrer\" class=xui-text-a>{{::observation.propertyBag.displayAliasFirstPart}}</a> <span class=xui-text-s>{{::observation.propertyBag.displayAliasSecondPart}}</span></div><div class=ps-data><div class=ps-label _t>Triggered By</div><a href={{::observation.propertyBag.ncmDetailsUrl}} target=_blank rel=\"noopener noreferrer\">{{::parentController.sourceMetric.entityDisplayName}}</a></div></div><div ass=ps-olm-data cl ng-if=\"parentController.sourceMetric.metricId === \'LogFiles\'\"><div class=ps-data><div class=ps-label _t>Log Profile</div><span class=xui-text-s>{{::observation.propertyBag.profileName}}</span></div><div class=ps-data><div class=ps-label _t>Log File</div><span class=xui-text-s>{{::observation.propertyBag.filename}}</span></div></div></xui-expander></div><div class=ps-footer><xui-pager hide-if-empty=true page=parentController.pagination.page page-size=parentController.pagination.pageSize total=parentController.pagination.total pager-action=\"parentController.onPagination(page, pageSize, total)\"></xui-pager></div></div>");
$templateCache.put("app/components/tile/metric/tile-metric-directive.html","<div class=ps-tile-metric ps-draggable draggable-data=metric><xui-icon icon=drag icon-size=small css-class=ps-drag-icon></xui-icon><div class=ps-title-wrapper><div class=ps-title>{{::metric.displayName}}</div><div class=ps-subtitle>{{::metric.entityDisplayName}}</div></div><xui-icon ng-if=::metric.realTime icon=ps-icon-fast-polling-inactive css-class=ps-icon-fast-poll></xui-icon><div class=ps-dragging-shim></div></div>");}]);