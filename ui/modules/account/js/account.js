/*!
 * @solarwinds/account-management 1.4.0-1701
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <xui-wizard finish-text=_t(Save) _ta hide-header=false on-finish=vm.dialogOptions.viewModel.onFinish(vm.configureForm) on-enter-step=vm.dialogOptions.viewModel.onEnterStep(step) on-next-step=\"vm.dialogOptions.viewModel.onNextStep(from, to, cancellation, [vm.primaryWebsiteForm, vm.additionalWebsitesForm])\" _ta> <xui-wizard-step label=enter-orion-url title=\"_t(Enter Orion Web Console Addresses)\" short-title=\"_t(Enter Orion URL)\" description=\"_t(Change these URLs according to your configured Orion environment.)\" _ta> <a class=account-management-saml-page__learn-more-link href={{vm.dialogOptions.viewModel.samlEnterOrionUrlLink}} target=_blank rel=\"noopener noreferrer\" _t> <xui-icon icon=help text-alignment=left-right></xui-icon>Learn more</a> <form id=primaryWebsiteForm name=vm.primaryWebsiteForm class=xui-validation> <xui-textbox type=url ng-model=vm.dialogOptions.viewModel.tmpServiceProvider.getPrimaryWebsite().externalUrl name=externalUrl caption=\"_t(Orion Web Console External URL)\" help-text=\"_t(External URL of the main Orion Web Console)\" validators=required _ta> <div ng-messages=vm.primaryWebsiteForm.externalUrl.$error> <div ng-message=required _t>Orion Web Console URL is required</div> <div ng-message=url _t>Orion Web Console URL is not valid</div> </div> </xui-textbox> </form> <form id=additionalWebsitesForm name=vm.additionalWebsitesForm class=xui-validation> <xui-expander class=account-management-saml-page__expander heading=\"_t(Additional Web Console external URLs)\" _ta ng-if=\"vm.dialogOptions.viewModel.tmpServiceProvider.getAdditionalWebsites().length > 0\" is-open=vm.dialogOptions.viewModel.isExpanderOpen on-status-changed=vm.dialogOptions.viewModel.onExpanderStatusChanged()> <div class=account-management-saml-page__enter-orion-url_aw xui-scrollbar> <xui-textbox type=url name=awUrl{{aw.websiteId}} ng-repeat=\"aw in vm.dialogOptions.viewModel.tmpServiceProvider.getAdditionalWebsites()\" ng-model=aw.externalUrl caption={{aw.serverName}} placeholder=\"_t(If left empty the main Orion Web Console URL would be used.)\" validators=url _ta> </xui-textbox> </div> </xui-expander> </form> <div class=account-management-saml-page__aw-error-msg ng-if=vm.additionalWebsitesForm.$invalid _t>Additional Website URLs are not valid</div> </xui-wizard-step> <xui-wizard-step label=prepare-idp title=\"_t(Specify SSO Service URLs To Your Identity Provider)\" short-title=\"_t(Prepare IdP)\" description=\"_t(Use these URLs in your identity provider to begin the setup.)\" _ta> <a class=account-management-saml-page__learn-more-link href={{vm.dialogOptions.viewModel.samlPrepareIdpLink}} target=_blank rel=\"noopener noreferrer\" _t> <xui-icon icon=help text-alignment=left-right></xui-icon>Learn more</a> <div class=account-management-saml-page__copy-box-border> <div class=account-management-saml-page__header-wrapper> <div class=xui-text-l _t>Audience URI</div> <div class=xui-text-dscrn _t>Service Provider Entity ID</div> </div> <div class=account-management-saml-page__copy-box-inner> <div class=account-management-saml-page__copy-box-item> <div class=xui-padding-smt>{{vm.dialogOptions.viewModel.tmpServiceProvider.getPrimaryWebsite().externalUrl}}</div> <xui-button class=xui-button-small-buttons icon=copy display-style=tertiary ng-click=vm.dialogOptions.viewModel.copyToClipboardClick() xui-clipboard=vm.dialogOptions.viewModel.tmpServiceProvider.getPrimaryWebsite().externalUrl _t>Copy</xui-button> </div> </div> </div> <div class=account-management-saml-page__copy-box-border> <div class=account-management-saml-page__header-wrapper> <div class=xui-text-l _t>SSO Service URLs</div> <div class=xui-text-dscrn _t>SAML Assertion Consumer Service (ACS)</div> </div> <div class=account-management-saml-page__copy-box-inner xui-scrollbar> <div class=account-management-saml-page__copy-box-item ng-repeat=\"serviceUrl in vm.dialogOptions.viewModel.tmpServiceProvider.getDistinctServiceUrls()\"> <div class=xui-padding-smt>{{serviceUrl}}</div> <xui-button class=xui-button-small-buttons display-style=tertiary icon=copy ng-click=vm.dialogOptions.viewModel.copyToClipboardClick() xui-clipboard=serviceUrl _t>Copy</xui-button> </div> </div> </div> </xui-wizard-step> <xui-wizard-step label=configure title=\"_t(Paste In Your Identity Provider Information)\" short-title=_t(Configure) _ta> <a class=account-management-saml-page__learn-more-link href={{vm.dialogOptions.viewModel.samlConfigureLink}} target=_blank rel=\"noopener noreferrer\" _t> <xui-icon icon=help text-alignment=left-right></xui-icon>Learn more</a> <ng-form name=vm.configureForm class=xui-validation ng-model=vm.dialogOptions.viewModel.tmpIdentityProvider> <xui-textbox name=name ng-model=vm.dialogOptions.viewModel.tmpIdentityProvider.name caption=\"_t(Identity Provider Name)\" placeholder=\"_t(e.g. My SSO service)\" validators=required _ta> <div ng-messages=vm.configureForm.name.$error> <div ng-message=required _t>Name is required</div> </div> </xui-textbox> <xui-textbox type=url name=issuerServiceUrl ng-model=vm.dialogOptions.viewModel.tmpIdentityProvider.issuerServiceUrl caption=\"_t(SSO Target URL)\" help-text=\"_t(URL used to log in to your SSO provider)\" validators=required _ta> <div ng-messages=vm.configureForm.issuerServiceUrl.$error> <div ng-message=required _t>Target URL is required</div> <div ng-message=url _t>Target URL is not valid URL</div> </div> </xui-textbox> <xui-textbox name=issuerEntityId ng-model=vm.dialogOptions.viewModel.tmpIdentityProvider.issuerEntityId caption=\"_t(Issuer URI)\" help-text=\"_t(Identity Provider Entity ID)\" validators=required _ta> <div ng-messages=vm.configureForm.issuerEntityId.$error> <div ng-message=required _t>Entity ID is required</div> </div> </xui-textbox> <xui-textbox name=issuerCertificate ng-model=vm.dialogOptions.viewModel.tmpIdentityProvider.issuerCertificate caption=\"_t(X.509 Signing Certificate)\" placeholder=\"_t(Paste in a X.509 encoded certificate including header and footer)\" rows=3 validators=required help-text=\"_t(Base64 encoded public key in .CER or .PEM format)\" _ta> <div ng-messages=vm.configureForm.issuerCertificate.$error> <div ng-message=required _t>Certificate is required</div> </div> </xui-textbox> </ng-form> </xui-wizard-step> </xui-wizard> </xui-dialog> ";

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <xui-message class=xui-margin-lgb type=info _t>Click Test to find out whether you have configured SAML login for the website correctly. See the test request sent to your Identity provider and its response. To test SAML login for another user, click Copy, send the link to the user and ask the user to share the test result with you. </xui-message> <div class=account-management-saml-page__copy-box-border> <div class=account-management-saml-page__header-wrapper> <div class=xui-text-l _t>Test URLs</div> <div class=xui-text-dscrn _t>Use this URLs to test your SAML configuration</div> </div> <div class=account-management-saml-page__copy-box-inner> <div class=\"xui-text-l xui-padding-mdt\" _t>Main website</div> <div class=account-management-saml-page__copy-box-item> <div class=xui-padding-smt>{{vm.dialogOptions.viewModel.primaryWebsiteTestLink}}</div> <div> <xui-button size=small icon=copy display-style=tertiary ng-click=vm.dialogOptions.viewModel.copyToClipboardClick() xui-clipboard=vm.dialogOptions.viewModel.primaryWebsiteTestLink _t>Copy </xui-button> <xui-button size=small icon=execute display-style=tertiary ng-click=vm.dialogOptions.viewModel.navigateToTestConnection(vm.dialogOptions.viewModel.primaryWebsiteTestLink) _t>Test </xui-button> </div> </div> <div ng-if=\"vm.dialogOptions.viewModel.additionalWebsiteTestLinks.length > 0\" class=\"xui-text-l xui-padding-lgt\" _t>Additional websites</div> <div class=account-management-saml-page__copy-box-item ng-repeat=\"testUrl in vm.dialogOptions.viewModel.additionalWebsiteTestLinks\"> <div class=xui-padding-smt>{{testUrl}}</div> <div> <xui-button size=small icon=copy display-style=tertiary ng-click=vm.dialogOptions.viewModel.copyToClipboardClick() xui-clipboard=testUrl _t>Copy </xui-button> <xui-button size=small icon=execute display-style=tertiary ng-click=vm.dialogOptions.viewModel.navigateToTestConnection(testUrl) _t>Test </xui-button> </div> </div> </div> </div> </xui-dialog> ";

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var StringUtils = /** @class */ (function () {
    function StringUtils() {
    }
    StringUtils.isNullOrWhiteSpace = function (str) {
        return str == null || str.replace(/\s/g, "").length === 0;
    };
    StringUtils.trimTrailingSlashes = function (str) {
        if (StringUtils.isNullOrWhiteSpace(str)) {
            return str;
        }
        return str.replace(/\/+$/, "");
    };
    return StringUtils;
}());
exports.default = StringUtils;


/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<xui-breadcrumb crumbs=vm.crumbTrail></xui-breadcrumb> <xui-page-content id=SamlConfig page-title=\"_t(SAML Configuration)\" page-layout=form link-text=\"_t(Learn more)\" link-icon=help link-target=_blank link-url={{vm.samlOverviewLink}} _ta> <div class=account-management-saml-page xui-busy=vm.isBusy> <xui-message name=samlConfiguredMsg type=info ng-if=\"!vm.serviceProvider.hasAnySamlAccounts() && vm.isIdpConfigured()\"> <b _t>To enable SAML login, make sure you have SAML individual or SAML group accounts defined on the <a href=/Orion/Admin/Accounts/Accounts.aspx>Manage Accounts</a> page.</b> </xui-message> <div class=account-management-saml-page__no-idp-yet ng-show=\"!vm.isIdpConfigured() && vm.pageLoaded\"> <xui-empty class=account-management-saml-page__empty empty-data=vm.noIdPModel></xui-empty> <xui-button class=account-management-saml-page__add-idp-button display-style=primary ng-click=vm.createConfiguration() _t>Add identity provider</xui-button> </div> <div ng-show=\"vm.isIdpConfigured() && vm.pageLoaded\"> <xui-switch class=xui-margin-smb-neg ng-class=\"!vm.serviceProvider.hasAnySamlAccounts() ? 'xui-margin-lgt xui-text-l' : 'xui-text-l'\" ng-model=vm.identityProvider.enabled ng-change=vm.onEnableSamlChange() _t>Use SAML as login method for SolarWinds </xui-switch> <div> <xui-divider></xui-divider> <div class=\"account-management-saml-page__idp-summary-buttons xui-button-small-buttons\"> <xui-button display-style=tertiary icon=edit size=small ng-click=vm.editConfiguration() _t>EDIT</xui-button> <xui-button display-style=tertiary icon=execute size=small ng-click=vm.testConfiguration() _t>TEST</xui-button> <xui-button class=xui-margin-lgl display-style=tertiary icon=delete size=small ng-click=vm.removeConfiguration() _t>REMOVE</xui-button> </div> <div class=xui-margin-lgv> <div class=xui-text-h2>{{vm.identityProvider.name}}</div> <div class=xui-text-s _t>SAML Authentication</div> </div> <div ng-class=\"vm.identityProvider.enabled ? '' : 'account-management-saml-page__opacity'\"> <div class=xui-margin-lgv> <div class=xui-text-l _t>SSO Target URL</div> <div class=xui-text-r>{{vm.identityProvider.issuerServiceUrl}}</div> </div> <div class=xui-margin-lgv> <div class=xui-text-l _t>Issuer (Entity ID)</div> <div class=xui-text-r>{{vm.identityProvider.issuerEntityId}}</div> </div> </div> </div> </div> </div> </xui-page-content> ";

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(5);
module.exports = __webpack_require__(6);


/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(7);
var config_1 = __webpack_require__(8);
var index_1 = __webpack_require__(9);
var index_2 = __webpack_require__(11);
var index_3 = __webpack_require__(20);
var index_4 = __webpack_require__(21);
var templates_1 = __webpack_require__(22);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
index_4.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("account.services", []);
angular.module("account.templates", []);
angular.module("account.components", []);
angular.module("account.filters", []);
angular.module("account.providers", []);
angular.module("account", [
    "orion",
    "account.services",
    "account.templates",
    "account.components",
    "account.filters",
    "account.providers"
]);
// create and register Xui (Orion) module wrapper
var accountmanagement = Xui.registerModule("account");
exports.default = accountmanagement;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: account-management");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(10);
exports.default = function (module) {
    module.service("account-managementConstants", constants_1.default);
};


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(12);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var saml_controller_1 = __webpack_require__(13);
var saml_service_1 = __webpack_require__(14);
var saml_config_1 = __webpack_require__(18);
__webpack_require__(19);
exports.default = function (module) {
    module.config(saml_config_1.default);
    module.controller("SamlController", saml_controller_1.default);
    module.service("samlService", saml_service_1.default);
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SamlController = /** @class */ (function () {
    /** @ngInject */
    SamlController.$inject = ["$scope", "$log", "$q", "$timeout", "$element", "$window", "samlService", "toastService", "xuiDialogService", "xuiWizardDialogService", "getTextService", "helpLinkService"];
    function SamlController($scope, $log, $q, $timeout, $element, $window, samlService, toastService, xuiDialogService, xuiWizardDialogService, getTextService, helpLinkService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$q = $q;
        this.$timeout = $timeout;
        this.$element = $element;
        this.$window = $window;
        this.samlService = samlService;
        this.toastService = toastService;
        this.xuiDialogService = xuiDialogService;
        this.xuiWizardDialogService = xuiWizardDialogService;
        this.getTextService = getTextService;
        this.helpLinkService = helpLinkService;
        this.isBusy = true;
        this.pageLoaded = false;
        this.idpConfigEditing = false;
        this.createWizardStarted = false;
        /**
         * onFinish
         */
        this.onFinish = function (form) {
            _this.$log.debug("on finish");
            if (!_this.validateForm(form)) {
                return _this.$q.when(false);
            }
            _this.isBusy = true;
            _this.identityProvider = _.cloneDeep(_this.tmpIdentityProvider);
            _this.serviceProvider = _.cloneDeep(_this.tmpServiceProvider);
            var spPromise = _this.samlService.updateServiceProvider(_this.serviceProvider);
            if (_this.idpConfigEditing) {
                var idpPromise = _this.samlService.updateIdentityProvider(_this.identityProvider);
                _this.$q.all([spPromise, idpPromise])
                    .then(function () {
                    _this.idpConfigEditing = false;
                    _this.isBusy = false;
                    _this.toastService.info(_this._t("The identity provider successfully saved."));
                    _this.setTestLinks();
                })
                    .catch(function (reason) {
                    _this.$log.error("onFinish() failed", reason);
                    _this.isBusy = false;
                });
            }
            else {
                _this.identityProvider.enabled = true;
                var idpPromise = _this.samlService.createIdentityProvider(_this.identityProvider);
                _this.$q.all([spPromise, idpPromise])
                    .then(function (result) {
                    _this.identityProvider = result[1];
                    _this.createWizardStarted = false;
                    _this.isBusy = false;
                    _this.toastService.info(_this._t("The identity provider successfully saved."));
                    _this.setTestLinks();
                })
                    .catch(function (reason) {
                    _this.$log.error("onFinish() failed", reason);
                    _this.isBusy = false;
                });
            }
            return _this.$q.when(true);
        };
        /**
         * loadServiceProvider
         */
        this.loadServiceProvider = function () {
            return _this.samlService.getServiceProvider()
                .then(
            /* success */
            function (serviceProvider) {
                _this.$log.info(serviceProvider);
                _this.serviceProvider = serviceProvider;
            }, 
            /* error */
            function (result) {
                _this.$log.error("loadServiceProvider() failed: " + result);
            });
        };
        /**
         * loadIdentityProvider
         */
        this.loadIdentityProvider = function () {
            return _this.samlService.getIdentityProvider()
                .then(
            /* success */
            function (identityProvider) {
                _this.$log.info(identityProvider);
                _this.identityProvider = identityProvider;
            }, 
            /* error */
            function (result) {
                _this.$log.error("loadIdentityProvider() failed: " + result);
            });
        };
        /**
         * validateForm
         */
        this.validateForm = function (form) {
            var isValid = true;
            if (form.$invalid) {
                isValid = false;
                form.$setDirty();
                angular.forEach(form, function (value, key) {
                    if (typeof value === "object" && value.hasOwnProperty("$modelValue")) {
                        value.$setDirty();
                    }
                });
            }
            return isValid;
        };
        /**
         * validateForms
         */
        this.validateForms = function (forms) {
            var areValid = true;
            for (var _i = 0, forms_1 = forms; _i < forms_1.length; _i++) {
                var form = forms_1[_i];
                var isFormValid = _this.validateForm(form);
                if (areValid) {
                    areValid = isFormValid;
                }
            }
            return areValid;
        };
    }
    /**
     * onInit
     */
    SamlController.prototype.$onInit = function () {
        var _this = this;
        this._t = this.getTextService;
        // Removes xui-empty image "no data" title
        this.$element.find(document).ready(function () {
            jQuery("svg title").text("");
        });
        this.crumbTrail = [
            { title: this._t("All Settings"), url: "/orion/admin/default_orion.aspx" },
            { title: this._t("SAML Configuration"), url: "/ui/account/saml/config" }
        ];
        this.noIdPModel = {
            description: this._t("No SAML Identity Provider configured yet.")
        };
        var spPromise = this.loadServiceProvider();
        var idpPromise = this.loadIdentityProvider();
        this.$q.all([spPromise, idpPromise]).then(function (result) {
            _this.isBusy = false;
            _this.pageLoaded = true;
            _this.isExpanderOpen = _this.serviceProvider.hasAnyAdditionalWebsiteExternalUrl();
            _this.setTestLinks();
        });
        this.samlOverviewLink = this.helpLinkService.getHelpLink("orioncoreconfiguresaml");
        this.samlEnterOrionUrlLink = this.helpLinkService.getHelpLink("orioncoresamlenterorionurl");
        this.samlPrepareIdpLink = this.helpLinkService.getHelpLink("orioncoresamlprepareidp");
        this.samlConfigureLink = this.helpLinkService.getHelpLink("orioncoresamlconfigureidp");
    };
    /**
     * isIdpConfigured
     */
    SamlController.prototype.isIdpConfigured = function () {
        return !!this.identityProvider && !this.createWizardStarted;
    };
    /**
     * showDialog
     */
    SamlController.prototype.showDialog = function (title) {
        this.tmpIdentityProvider = _.cloneDeep(this.identityProvider);
        this.tmpServiceProvider = _.cloneDeep(this.serviceProvider);
        this.xuiWizardDialogService
            .showModal({
            title: title,
            template: __webpack_require__(0),
            size: "lg",
            viewModel: this
        });
    };
    /**
     * showTestConfigurationDialog
     */
    SamlController.prototype.showTestConfigurationDialog = function () {
        this.xuiDialogService.showModal({
            size: "lg",
            template: __webpack_require__(1)
        }, {
            title: this._t("Test Configuration"),
            cancelButtonText: "Close",
            viewModel: this
        });
    };
    /**
    * navigateToTestConnection
    */
    SamlController.prototype.navigateToTestConnection = function (testLink) {
        var win = window.open(testLink, "_blank");
        win.focus();
    };
    /**
     * onEnterStep
     */
    SamlController.prototype.onEnterStep = function (step) {
        var _this = this;
        this.$log.debug("onEnterStep" + step.label);
        if (step.label === "enter-orion-url") {
            this.$timeout(function () {
                _this.validateForms([
                    angular.element("#primaryWebsiteForm").controller("form"),
                    angular.element("#additionalWebsitesForm").controller("form")
                ]);
            });
        }
    };
    /**
     * onNextStep
     */
    SamlController.prototype.onNextStep = function (from, to, cancellation, forms) {
        this.$log.debug("onNextStep from: " + from.label + " to: " + to.label);
        if (from.label === "enter-orion-url") {
            return this.$q.resolve(this.validateForms(forms));
        }
    };
    /**
     * onExpanderStatusChanged
     */
    SamlController.prototype.onExpanderStatusChanged = function () {
        var _this = this;
        this.$timeout(function () {
            var form = angular.element("#additionalWebsitesForm").controller("form");
            if (_this.isExpanderOpen) {
                _this.validateForm(form);
            }
        });
    };
    /**
    * onEnableSamlChange
    */
    SamlController.prototype.onEnableSamlChange = function () {
        var _this = this;
        this.isBusy = true;
        this.$log.debug("onEnableSamlChange");
        this.samlService.updateIdentityProvider(this.identityProvider)
            .then(function () {
            _this.isBusy = false;
        })
            .catch(function (reason) {
            _this.$log.error("onEnableSamlChange() failed", reason);
            _this.isBusy = false;
        });
    };
    /**
     * createConfiguration
     */
    SamlController.prototype.createConfiguration = function () {
        this.createWizardStarted = true;
        this.showDialog(this._t("Add Identity Provider"));
    };
    /**
    * editConfiguration
    */
    SamlController.prototype.editConfiguration = function () {
        this.idpConfigEditing = true;
        this.showDialog(this._t("Edit Identity Provider"));
    };
    /**
    * testConfiguration
    */
    SamlController.prototype.testConfiguration = function () {
        this.showTestConfigurationDialog();
    };
    /**
    * copyToClipboardClick
    */
    SamlController.prototype.copyToClipboardClick = function () {
        this.toastService.info(this._t("The link copied to the clipboard"));
    };
    /**
    * removeConfiguration
    */
    SamlController.prototype.removeConfiguration = function () {
        var thiz = this;
        this.xuiDialogService.showWarning({
            title: this._t("Are you sure?"),
            message: this._t("The identity provider configuration will be removed.")
        }, {
            name: "remove",
            text: this._t("Remove"),
            action: function (dialogResult) {
                thiz.isBusy = true;
                var idpPromise = thiz.samlService.deleteIdentityProvider(thiz.identityProvider.id);
                var spPromise = thiz.samlService.resetServiceProvider();
                thiz.$q.all([idpPromise, spPromise])
                    .then(function (result) {
                    thiz.identityProvider = null;
                    thiz.serviceProvider = result[1];
                    thiz.isExpanderOpen = false;
                    thiz.isBusy = false;
                    thiz.toastService.info(thiz._t("The identity provider successfully removed."));
                })
                    .catch(function (reason) {
                    thiz.$log.error("removeConfiguration() failed: " + reason);
                    thiz.isBusy = false;
                });
                return true;
            }
        });
    };
    /**
     * setTestLinks
     */
    SamlController.prototype.setTestLinks = function () {
        if (!this.serviceProvider || !this.identityProvider) {
            return;
        }
        this.primaryWebsiteTestLink = this.serviceProvider.getPrimaryWebsiteTestUrl(this.identityProvider.testKey);
        this.additionalWebsiteTestLinks = this.serviceProvider.getAdditionalWebsiteTestUrls(this.identityProvider.testKey);
    };
    return SamlController;
}());
exports.default = SamlController;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var identityProvider_1 = __webpack_require__(15);
var serviceProvider_1 = __webpack_require__(16);
exports.idpServiceUrl = "account/identityprovider";
exports.spServiceUrl = "account/serviceprovider";
var SamlService = /** @class */ (function () {
    /** @ngInject */
    SamlService.$inject = ["swApi", "$log"];
    function SamlService(swApi, $log) {
        this.swApi = swApi;
        this.$log = $log;
    }
    SamlService.prototype.getServiceProvider = function () {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.spServiceUrl)
            .get()
            .then(function (response) {
            _this.$log.debug(response);
            if (response) {
                return new serviceProvider_1.default(response);
            }
            return null;
        });
    };
    SamlService.prototype.updateServiceProvider = function (sp) {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.spServiceUrl)
            .customPUT(sp)
            .then(function (response) {
            _this.$log.debug(response);
        });
    };
    SamlService.prototype.resetServiceProvider = function () {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.spServiceUrl)
            .remove()
            .then(function (response) {
            _this.$log.debug(response);
            if (response) {
                return new serviceProvider_1.default(response);
            }
            return null;
        });
    };
    SamlService.prototype.getIdentityProvider = function () {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.idpServiceUrl)
            .get()
            .then(function (response) {
            _this.$log.debug(response);
            if (response && response.length > 0) {
                return new identityProvider_1.default(response[0]);
            }
            return null;
        });
    };
    SamlService.prototype.createIdentityProvider = function (idP) {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.idpServiceUrl)
            .post(null, idP)
            .then(function (response) {
            _this.$log.debug(response);
            return new identityProvider_1.default(response);
        });
    };
    SamlService.prototype.updateIdentityProvider = function (idP) {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.idpServiceUrl)
            .customPUT(idP, idP.id.toString())
            .then(function (response) {
            _this.$log.debug(response);
        });
    };
    SamlService.prototype.deleteIdentityProvider = function (idpId) {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.idpServiceUrl)
            .customDELETE(idpId.toString())
            .then(function (response) {
            _this.$log.debug(response);
            return response;
        });
    };
    SamlService.prototype.setIdentityProviderState = function (idpId, enabled) {
        var _this = this;
        return this.swApi.api(false)
            .one(exports.idpServiceUrl + "/enable")
            .put({ "idpId": idpId, "enabled": enabled }, null)
            .then(function (response) {
            _this.$log.debug(response);
        });
    };
    return SamlService;
}());
exports.default = SamlService;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IdentityProvider = /** @class */ (function () {
    function IdentityProvider(model) {
        if (model) {
            this.id = model.IdentityProviderId;
            this.name = model.Name;
            this.issuerEntityId = model.IssuerEntityId;
            this.issuerServiceUrl = model.IssuerServiceUrl;
            this.issuerCertificate = model.IssuerCertificate;
            this.enabled = model.Enabled;
            this.testKey = model.TestKey;
        }
    }
    return IdentityProvider;
}());
exports.default = IdentityProvider;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var website_1 = __webpack_require__(17);
var string_utils_1 = __webpack_require__(2);
var ServiceProvider = /** @class */ (function () {
    function ServiceProvider(model) {
        if (model) {
            this.samlLoginPath = model.SamlLoginPath;
            this.samlAccountCount = model.SamlAccountCount;
            this.samlInitPath = model.SamlInitPath;
            this.websites = new Array();
            for (var _i = 0, _a = model.Websites; _i < _a.length; _i++) {
                var w = _a[_i];
                this.websites.push(new website_1.default(w));
            }
        }
    }
    ServiceProvider.prototype.getPrimaryWebsite = function () {
        return this.websites.filter(function (w) { return w.type === 0; })[0];
    };
    ServiceProvider.prototype.getAdditionalWebsites = function () {
        return this.websites.filter(function (w) { return w.type === 1; });
    };
    ServiceProvider.prototype.hasAnySamlAccounts = function () {
        return this.samlAccountCount > 0;
    };
    ServiceProvider.prototype.hasAnyAdditionalWebsiteExternalUrl = function () {
        return this.websites
            .filter(function (w) { return !string_utils_1.default.isNullOrWhiteSpace(w.externalUrl) && w.type === 1; })
            .map(function (w) { return w.externalUrl; })
            .length > 0;
    };
    ServiceProvider.prototype.getDistinctServiceUrls = function () {
        var distinctServiceUrls = [];
        for (var _i = 0, _a = this.websites; _i < _a.length; _i++) {
            var website = _a[_i];
            var externalUrl = website.externalUrlForView;
            if (!string_utils_1.default.isNullOrWhiteSpace(externalUrl)
                && !(distinctServiceUrls.indexOf(externalUrl) > -1)) {
                distinctServiceUrls.push(externalUrl + this.samlLoginPath);
            }
        }
        return distinctServiceUrls;
    };
    ServiceProvider.prototype.getPrimaryWebsiteTestUrl = function (testKey) {
        return this.getPrimaryWebsite().externalUrl + this.samlInitPath + "?testkey=" + testKey;
    };
    ServiceProvider.prototype.getAdditionalWebsiteTestUrls = function (testKey) {
        var distinctAwTestUrls = [];
        for (var _i = 0, _a = this.getAdditionalWebsites(); _i < _a.length; _i++) {
            var aw = _a[_i];
            var externalUrl = aw.externalUrlForView;
            if (!string_utils_1.default.isNullOrWhiteSpace(externalUrl)
                && !(distinctAwTestUrls.indexOf(externalUrl) > -1)) {
                distinctAwTestUrls.push(externalUrl + this.samlInitPath + "?testkey=" + testKey);
            }
        }
        return distinctAwTestUrls;
    };
    return ServiceProvider;
}());
exports.default = ServiceProvider;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var string_utils_1 = __webpack_require__(2);
var Website = /** @class */ (function () {
    function Website(model) {
        if (model) {
            this.uri = model.Uri;
            this.type = model.Type;
            this.serverName = model.ServerName;
            this.externalUrl = model.ExternalUrl;
        }
    }
    Object.defineProperty(Website.prototype, "externalUrlForView", {
        get: function () {
            return string_utils_1.default.trimTrailingSlashes(this.externalUrl);
        },
        enumerable: true,
        configurable: true
    });
    return Website;
}());
exports.default = Website;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
config.$inject = ["$stateProvider", "$urlRouterProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function config($stateProvider, $urlRouterProvider) {
    // configure angular to show our new page when using 'account/saml' url
    $stateProvider.state("samlConfig", {
        url: "/account/saml/config",
        title: "SAML Configuration",
        controller: "SamlController",
        controllerAs: "vm",
        template: __webpack_require__(3),
        params: {
            errorType: "auth",
            errorCode: "401",
            message: "Unauthorized",
            location: "./saml.html"
        },
        authProfile: {
            permissions: [],
            roles: ["admin"]
        }
    });
}
exports.default = config;
;


/***/ }),
/* 19 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    // register components
};


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
exports.default = function (module) {
    // register services
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(23);
    req.keys().forEach(function (r) {
        var key = "account-management" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./views/saml/saml.html": 3,
	"./views/saml/test-configuration-template.html": 1,
	"./views/saml/wizard-template.html": 0
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 23;

/***/ })
/******/ ]);
//# sourceMappingURL=account.js.map