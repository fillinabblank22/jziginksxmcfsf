/*!
 * @solarwinds/vimcloud 2020.2.5-63
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 28);
/******/ })
/************************************************************************/
/******/ ({

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(29);


/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var CloudInstanceManagementService = /** @class */ (function () {
    /** @ngInject */
    CloudInstanceManagementService.$inject = ["swApi", "xuiToastService", "getTextServiceEx", "$q"];
    function CloudInstanceManagementService(swApi, xuiToastService, getTextServiceEx, $q) {
        this.swApi = swApi;
        this.xuiToastService = xuiToastService;
        this.getTextServiceEx = getTextServiceEx;
        this.$q = $q;
        this.instanceManagementServiceUrl = "/Orion/VIMCloudMonitoring/Services/InstanceManagementService.asmx/";
        /*prevent tslint error*/
    }
    CloudInstanceManagementService.prototype.PollNow = function (virtualMachine) {
        var _this = this;
        if (virtualMachine.isDemo) {
            demoAction();
            return this.GetPromise();
        }
        var payload = {
            virtualMachineId: virtualMachine.virtualMachineId
        };
        return this.swApi.ws.allUrl("cloudInstances", this.instanceManagementServiceUrl + "PollNow")
            .post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextServiceEx.translateIfNeeded("_t(VM/Instance being polled)"));
        }, function () {
            _this.xuiToastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Polling VM/Instance failed)"));
        });
    };
    CloudInstanceManagementService.prototype.Start = function (virtualMachine) {
        var _this = this;
        if (virtualMachine.isDemo) {
            demoAction();
            return this.GetPromise();
        }
        var payload = {
            virtualMachineId: virtualMachine.virtualMachineId
        };
        return this.swApi.ws.allUrl("cloudInstances", this.instanceManagementServiceUrl + "Start")
            .post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextServiceEx.translateIfNeeded("_t(VM/Instance started)"));
        }, function () {
            _this.xuiToastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Starting VM/Instance failed)"));
        });
    };
    CloudInstanceManagementService.prototype.Stop = function (stopParameters) {
        var _this = this;
        if (stopParameters.isDemo) {
            demoAction();
            return this.GetPromise();
        }
        var payload = {
            force: false,
            virtualMachineId: stopParameters.virtualMachineId,
        };
        return this.swApi.ws.allUrl("cloudInstances", this.instanceManagementServiceUrl + "Stop")
            .post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextServiceEx.translateIfNeeded("_t(VM/Instance stopped)"));
        }, function () {
            _this.xuiToastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Stopping VM/Instance failed)"));
        });
    };
    CloudInstanceManagementService.prototype.Unmanage = function (virtualMachine) {
        var _this = this;
        if (virtualMachine.isDemo) {
            demoAction();
            return this.GetPromise();
        }
        var payload = {
            virtualMachineId: virtualMachine.virtualMachineId
        };
        return this.swApi.ws.allUrl("cloudInstances", this.instanceManagementServiceUrl + "Unmanage")
            .post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextServiceEx.translateIfNeeded("_t(VM/Instance unmanaged)"));
        }, function () {
            _this.xuiToastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Unmanaging VM/Instance failed)"));
        });
    };
    CloudInstanceManagementService.prototype.Remanage = function (virtualMachine) {
        var _this = this;
        if (virtualMachine.isDemo) {
            demoAction();
            return this.GetPromise();
        }
        var payload = {
            virtualMachineId: virtualMachine.virtualMachineId
        };
        return this.swApi.ws.allUrl("cloudInstances", this.instanceManagementServiceUrl + "Remanage")
            .post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextServiceEx.translateIfNeeded("_t(VM/Instance remanaged)"));
        }, function () {
            _this.xuiToastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Remanaging VM/Instance failed)"));
        });
    };
    CloudInstanceManagementService.prototype.Reboot = function (virtualMachine) {
        var _this = this;
        if (virtualMachine.isDemo) {
            demoAction();
            return this.GetPromise();
        }
        var payload = {
            virtualMachineId: virtualMachine.virtualMachineId
        };
        return this.swApi.ws.allUrl("cloudInstances", this.instanceManagementServiceUrl + "Reboot")
            .post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextServiceEx.translateIfNeeded("_t(VM/Instance rebooting)"));
        }, function () {
            _this.xuiToastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Rebooting VM/Instance failed)"));
        });
    };
    CloudInstanceManagementService.prototype.Delete = function (virtualMachine) {
        var _this = this;
        if (virtualMachine.isDemo) {
            demoAction();
            return this.GetPromise();
        }
        var payload = {
            removeNode: false,
            virtualMachineId: virtualMachine.virtualMachineId
        };
        return this.swApi.ws.allUrl("cloudInstances", this.instanceManagementServiceUrl + "Delete")
            .post(payload).then(function () {
            _this.xuiToastService.success(_this.getTextServiceEx.translateIfNeeded("_t(VM/Instance deleted)"));
        }, function () {
            _this.xuiToastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Deleting VM/Instance failed)"));
        });
    };
    CloudInstanceManagementService.prototype.GetPromise = function () {
        var deffer = this.$q.defer();
        return deffer.promise;
    };
    return CloudInstanceManagementService;
}());
exports.CloudInstanceManagementService = CloudInstanceManagementService;
angular.module("orion.components").service("cloudInstanceManagementService", CloudInstanceManagementService);


/***/ })

/******/ });
//# sourceMappingURL=cloudInstanceTooltip.js.map