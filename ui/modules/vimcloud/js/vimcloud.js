/*!
 * @solarwinds/vimcloud 2020.2.5-63
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = "<div class=\"breadcrumbs xui-padding-lgr xui-padding-lgl xui-padding-lgt\"> <a class=xui-text-a href=/Orion/Admin/ _t>Admin</a> <xui-icon icon=triangle-right icon-size=small></xui-icon> <a class=xui-text-a href=/Orion/CloudMonitoring/Admin/CloudMonitoringSettings.aspx _t>Cloud Infrastructure Monitoring</a> <xui-icon icon=triangle-right icon-size=small></xui-icon> <a class=xui-text-a href=/ui/clm/accounts target=_self _t>Manage Cloud Accounts</a> <xui-icon icon=triangle-right icon-size=small></xui-icon> <span class=xui-text-s _t=\"['{{wizard.accountDisplayName}}']\">Choose Instances/VMs ({0})</span> </div> <xui-page-content is-busy=wizard.isUpdating busy-message={{wizard.savingMessage}} _ta> <xui-page-header> <div class=\"xui-page-content__header clm-choose-instances__header\"> <span class=\"xui-h1 xui-page-content__header-title\" _t=\"['{{wizard.accountDisplayName}}']\"> Choose Instances/VMs for <strong>{0}</strong> </span> <div class=xui-page-content__header-link> <i uib-tooltip=\"\" uib-tooltip-append-to-body=\"\" css-class=xui-page-content__header-link-item icon=help ng-show=true class=\"xui-icon xui-icon-help filled xui-page-content__header-link-item\" aria-hidden=false> <xui-icon icon=help></xui-icon> </i> <a class=xui-page-content__title-link-item href=\"http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&amp;topic=OrionCloudManageCloudAccounts.htm\" target=_blank aria-hidden=false _t> Help </a> </div> </div> <div> <error-info class=clm-error-wrapper message=\"wizard.data.data['error']\"> </error-info> </div> </xui-page-header> <div class=clm-choose-instances> <div ng-include src=\"'/ui/modules/vimcloud/templates/addAccountWizard/ChooseInstancesWizardStep.html'\" ng-init=wizard.onTemplateIncluded()> </div> <div class=\"xui-wizard__footer clm-choose-instances__footer\"> <xui-button id=Save display-style=primary is-disabled=\"!wizard.data.data['vimcloud-instances-is-loaded'] || wizard.isUpdating\" ng-click=wizard.save() _t> Save </xui-button> <xui-button id=Cancel display-style=tertiary is-disabled=wizard.isUpdating ng-click=wizard.redirectToManageAccounts() _t> Cancel </xui-button> </div> </div> </xui-page-content> ";

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ProviderType;
(function (ProviderType) {
    ProviderType[ProviderType["Aws"] = 1] = "Aws";
    ProviderType[ProviderType["Azure"] = 2] = "Azure";
    ProviderType[ProviderType["Unknown"] = -1] = "Unknown";
})(ProviderType = exports.ProviderType || (exports.ProviderType = {}));


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var Status;
(function (Status) {
    Status[Status["GRANTED"] = 200] = "GRANTED";
    Status[Status["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    Status[Status["FORBIDDEN"] = 403] = "FORBIDDEN";
})(Status = exports.Status || (exports.Status = {}));


/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = "<div id=clmErrorInfo class=\"xui-padding-lgh xui-padding-mdv clm-error-info\" ng-if=vm.canShow()> <xui-icon icon=severity_error></xui-icon> <span class=\"xui-text-critical xui-margin-lgl\">{{vm.message}}</span> <span>{{vm.details}}</span> </div> ";

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(5);
module.exports = __webpack_require__(6);


/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the main entry point for webpack tool. You need to import all stuff you want to have in the final package.
 **/
var app_1 = __webpack_require__(7);
var config_1 = __webpack_require__(8);
var index_1 = __webpack_require__(9);
var index_2 = __webpack_require__(11);
var index_3 = __webpack_require__(22);
var templates_1 = __webpack_require__(24);
index_1.default(app_1.default);
config_1.default(app_1.default);
index_2.default(app_1.default);
index_3.default(app_1.default);
templates_1.default(app_1.default);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
// create Angular modules
angular.module("vimcloud.services", []);
angular.module("vimcloud.templates", []);
angular.module("vimcloud.components", []);
angular.module("vimcloud.filters", []);
angular.module("vimcloud.providers", []);
angular.module("vimcloud", [
    "orion",
    "vimcloud.services",
    "vimcloud.templates",
    "vimcloud.components",
    "vimcloud.filters",
    "vimcloud.providers"
]);
// create and register Xui (Orion) module wrapper
var vimcloud = Xui.registerModule("vimcloud");
exports.default = vimcloud;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
run.$inject = ["$log"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log) {
    $log.info("Run module: vimcloud");
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run);
};
exports.default.$inject = ["module"];


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(10);
exports.default = function (module) {
    module.service("vimcloudConstants", constants_1.default);
};


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    return Constants;
}());
exports.default = Constants;
;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


handleAuthorizationErrors.$inject = ["$rootScope", "$state"];
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(12);
var index_2 = __webpack_require__(19);
var status_1 = __webpack_require__(2);
/** @ngInject */
function handleAuthorizationErrors($rootScope, $state) {
    $rootScope.$on("$stateChangeError", function (e, toState, toParams, fromState, fromParams, error) {
        if (error && error.authorizationStatus === status_1.Status.UNAUTHORIZED) {
            $state.go("Error", {
                errorType: "auth",
                errorCode: "403",
                message: "Access Denied",
                location: null
            });
            e.preventDefault();
        }
    });
}
;
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    module.app().run(handleAuthorizationErrors);
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
var choose_instances_component_1 = __webpack_require__(13);
var choose_instances_controller_1 = __webpack_require__(14);
var methods_1 = __webpack_require__(17);
__webpack_require__(18);
/** @ngInject */
function config($stateProvider) {
    $stateProvider.state("chooseInstances", {
        title: "Choose Instances/VMs for a Cloud Account",
        url: "/vimcloud/chooseInstances?providerType&accountId",
        template: "<choose-instances/>",
        resolve: {
            isAuthorized: methods_1.restrictAccessToAdminOrDemoMode
        }
    });
}
;
exports.default = function (module) {
    module.component("chooseInstances", choose_instances_component_1.default);
    module.controller("chooseInstancesController", choose_instances_controller_1.default);
    module.config(config);
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ChooseInstancesComponent = /** @class */ (function () {
    function ChooseInstancesComponent() {
        this.template = __webpack_require__(0);
        this.controllerAs = "wizard";
        this.controller = "chooseInstancesController";
        this.restrict = "E";
    }
    return ChooseInstancesComponent;
}());
exports.default = ChooseInstancesComponent;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(15);
var ChooseInstancesController = /** @class */ (function () {
    /** @ngInject */
    ChooseInstancesController.$inject = ["accountsService", "toastService", "swDemoService", "$routeParams", "$stateParams", "$q", "$window", "$timeout", "$cookies", "getTextServiceEx"];
    function ChooseInstancesController(accountsService, toastService, swDemoService, $routeParams, $stateParams, $q, $window, $timeout, $cookies, getTextServiceEx) {
        this.accountsService = accountsService;
        this.toastService = toastService;
        this.swDemoService = swDemoService;
        this.$routeParams = $routeParams;
        this.$stateParams = $stateParams;
        this.$q = $q;
        this.$window = $window;
        this.$timeout = $timeout;
        this.$cookies = $cookies;
        this.getTextServiceEx = getTextServiceEx;
    }
    ChooseInstancesController.prototype.$onInit = function () {
        this.initializeData();
        this.data.providerType = this.getProviderTypeFromUrl();
        this.data.accountId = this.getAccountIdFromUrl();
        if (!this.data.isAccountDefined()) {
            this.redirectToManageAccounts();
        }
    };
    ChooseInstancesController.prototype.initializeData = function () {
        var self = this;
        this.isUpdating = false;
        this.data = new index_1.InstanceContext(this.toastService, this.$timeout, this.getTextServiceEx);
        this.data.data["vimcloud-step-choose-instances-data"] = { canShowList: false };
        this.data.data["cloud-settings-selected-provider"] = { id: 0 };
        this.data.data["polling-options-accountId"] = 0;
        this.data.data["is-demo-mode"] = this.swDemoService.isDemoMode();
        this.data.data["show-demo-error-toast"] = function () { return self.swDemoService.showDemoErrorToast(); };
        this.data.data["XSRF-TOKEN"] = this.$cookies.get("XSRF-TOKEN");
    };
    ChooseInstancesController.prototype.onTemplateIncluded = function () {
        this.savingMessage = this.getBusyMessageForSavingInstances();
        this.loadAccount(this.data.providerType, this.data.accountId);
    };
    ChooseInstancesController.prototype.redirectToManageAccounts = function () {
        this.$window.location.href = "/ui/clm/accounts";
    };
    ChooseInstancesController.prototype.save = function () {
        var _this = this;
        if (this.inDemoModeShowErrorToast()) {
            return;
        }
        this.isUpdating = true;
        this.getGridPlugin().onFinish(this.data)
            .then(function () {
            if (_this.data.data["error"]) {
                _this.toastService.error(_this.data.data["error"].responseText, _this.getTextServiceEx.translateIfNeeded("_t(Error while updating instances)"));
                _this.data.data["error"] = undefined;
            }
            else {
                _this.redirectToManageAccounts();
            }
            _this.delay(function () { _this.isUpdating = false; }, 100);
        }, function (reason) {
            _this.toastService.error(reason, _this.getTextServiceEx.translateIfNeeded("_t(Error while updating instances)"));
            _this.delay(function () { _this.isUpdating = false; }, 100);
        });
    };
    ChooseInstancesController.prototype.getProviderTypeFromUrl = function () {
        var providerType = this.$stateParams.providerType;
        switch (providerType) {
            case "1":
                return index_1.ProviderType.Aws;
            case "2":
                return index_1.ProviderType.Azure;
            default:
                return index_1.ProviderType.Unknown;
        }
    };
    ChooseInstancesController.prototype.getAccountIdFromUrl = function () {
        return parseInt(this.$stateParams.accountId || "0", 10);
    };
    ChooseInstancesController.prototype.loadAccount = function (providerType, accountId) {
        var _this = this;
        var loadPromise;
        var that = this;
        this.data.data["cloud-settings-selected-provider"].id = providerType;
        this.data.data["polling-options-accountId"] = accountId;
        switch (providerType) {
            case index_1.ProviderType.Aws:
                loadPromise = this.accountsService.getAwsAccount(accountId);
                break;
            case index_1.ProviderType.Azure:
                loadPromise = this.accountsService.getAzureAccount(accountId);
                break;
            default:
                loadPromise = this.$q.reject(this.getTextServiceEx.translateIfNeeded("_t(Not supported account type)"));
                break;
        }
        loadPromise.then(function (account) {
            _this.account = account;
            _this.hideBusyIndicatorAndShowInstancesGrid(function () { return _this.loadInstancesForAccount.apply(that); });
        }, function () {
            _this.hideBusyIndicatorAndShowInstancesGrid();
        });
    };
    ChooseInstancesController.prototype.loadInstancesForAccount = function () {
        if (!this.account) {
            return;
        }
        var gridPlugin = this.getGridPlugin();
        gridPlugin.onEnter(this.data);
    };
    ChooseInstancesController.prototype.getBusyMessageForSavingInstances = function () {
        var message = "";
        switch (this.data.providerType) {
            case index_1.ProviderType.Aws:
                message = this.getTextServiceEx.translateIfNeeded("_t(Saving EC2 Instances to monitor...)");
                break;
            case index_1.ProviderType.Azure:
                message = this.getTextServiceEx.translateIfNeeded("_t(Saving Azure VMs to monitor...)");
                break;
            default:
                break;
        }
        return message;
    };
    ChooseInstancesController.prototype.hideBusyIndicatorAndShowInstancesGrid = function (gridCallback) {
        if (gridCallback === void 0) { gridCallback = null; }
        this.initInstanceGridTemplate(gridCallback);
    };
    ChooseInstancesController.prototype.initInstanceGridTemplate = function (callback) {
        var _this = this;
        if (callback === void 0) { callback = null; }
        var that = this;
        // we need to be sure that global variable from template is visible
        this.delay(function () {
            var gridPlugin = that.getGridPlugin();
            if (!gridPlugin) {
                that.toastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Could not load account instances)"), _this.getTextServiceEx.translateIfNeeded("_t(Unexpected error)"));
                return;
            }
            gridPlugin.init(that.data);
            that.data.data["vimcloud-step-choose-instances-data"].canShowList = true;
            that.data.data["vimcloud-step-choose-instances-data"].showHeader = false;
            that.data.data["vimcloud-step-choose-instances-data"].providerType = that.data.providerType;
            if (callback) {
                callback();
            }
        }, 500);
    };
    ChooseInstancesController.prototype.getGridPlugin = function () {
        return this.$window["chooseInstances"];
    };
    ChooseInstancesController.prototype.delay = function (action, delay) {
        this.$timeout(action, delay);
    };
    ChooseInstancesController.prototype.inDemoModeShowErrorToast = function () {
        var isDemoMode = this.swDemoService.isDemoMode();
        if (isDemoMode) {
            this.swDemoService.showDemoErrorToast();
        }
        ;
        return isDemoMode;
    };
    Object.defineProperty(ChooseInstancesController.prototype, "accountDisplayName", {
        get: function () {
            return _.escape(this.account ? this.account.Name : "");
        },
        enumerable: true,
        configurable: true
    });
    return ChooseInstancesController;
}());
exports.default = ChooseInstancesController;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(1));
__export(__webpack_require__(16));


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var provider_type_1 = __webpack_require__(1);
var InstanceContext = /** @class */ (function () {
    function InstanceContext(toastService, $timeout, getTextServiceEx) {
        this.toastService = toastService;
        this.$timeout = $timeout;
        this.getTextServiceEx = getTextServiceEx;
        this.data = {};
    }
    InstanceContext.prototype.isAccountDefined = function () {
        return this.providerType !== provider_type_1.ProviderType.Unknown && this.accountId > 0;
    };
    InstanceContext.prototype.setNotificationMessage = function (notificationMessage) {
        this.toastService.info(notificationMessage.message);
    };
    InstanceContext.prototype.delayExecute = function (action, delay) {
        if (!action) {
            return;
        }
        this.$timeout(action, delay);
    };
    return InstanceContext;
}());
exports.InstanceContext = InstanceContext;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


restrictAccessToAdminOrDemoMode.$inject = ["constants", "swDemoService", "$q"];
Object.defineProperty(exports, "__esModule", { value: true });
var status_1 = __webpack_require__(2);
/** @ngInject */
function restrictAccessToAdminOrDemoMode(constants, swDemoService, $q) {
    var isGranted = constants.user.AllowAdmin || swDemoService.isDemoMode();
    if (isGranted) {
        return $q.resolve(status_1.Status.GRANTED);
    }
    else {
        return $q.reject({
            authorizationStatus: status_1.Status.UNAUTHORIZED
        });
    }
}
exports.restrictAccessToAdminOrDemoMode = restrictAccessToAdminOrDemoMode;
;


/***/ }),
/* 18 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var error_info_controller_1 = __webpack_require__(20);
__webpack_require__(21);
exports.default = function (module) {
    module.controller("errorInfoController", error_info_controller_1.default);
    module.app().component("errorInfo", {
        template: __webpack_require__(3),
        controller: "errorInfoController",
        controllerAs: "vm",
        bindings: {
            message: "<",
            details: "<"
        }
    });
};


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ErrorInfoController = /** @class */ (function () {
    /** @ngInject */
    ErrorInfoController.$inject = ["$location"];
    function ErrorInfoController($location) {
        this.$location = $location;
    }
    ErrorInfoController.prototype.canShow = function () {
        return this.message ? true : false;
    };
    return ErrorInfoController;
}());
exports.default = ErrorInfoController;


/***/ }),
/* 21 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var accounts_service_1 = __webpack_require__(23);
exports.default = function (module) {
    module.service("accountsService", accounts_service_1.AccountsService);
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var AccountsService = /** @class */ (function () {
    /** @ngInject */
    AccountsService.$inject = ["$http", "$q", "toastService", "getTextServiceEx"];
    function AccountsService($http, $q, toastService, getTextServiceEx) {
        this.$http = $http;
        this.$q = $q;
        this.toastService = toastService;
        this.getTextServiceEx = getTextServiceEx;
    }
    AccountsService.prototype.getAwsAccount = function (accountId) {
        var _this = this;
        var deferred = this.$q.defer();
        var url = "/api/CloudAccount/GetAwsCloudAccount?cloudAccountId=" + accountId;
        this.$http.get(url)
            .then(function (response) {
            deferred.resolve(response.data);
        }, function (reason) {
            _this.toastService.error(_this.getTextServiceEx.translateIfNeeded("_t(AWS Account not found)"), _this.getTextServiceEx.translateIfNeeded("_t(Account not found)"));
            deferred.reject(null);
        });
        return deferred.promise;
    };
    AccountsService.prototype.getAzureAccount = function (accountId) {
        var _this = this;
        var deferred = this.$q.defer();
        var url = "/api/CloudAccount/GetAzureCloudAccount?cloudAccountId=" + accountId;
        this.$http.get(url)
            .then(function (response) {
            deferred.resolve(response.data);
        }, function (reason) {
            _this.toastService.error(_this.getTextServiceEx.translateIfNeeded("_t(Azure Account not found)"), _this.getTextServiceEx.translateIfNeeded("_t(Account not found)"));
            deferred.reject(null);
        });
        return deferred.promise;
    };
    return AccountsService;
}());
exports.AccountsService = AccountsService;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


///<reference path="../ref.d.ts"/>
templates.$inject = ["$templateCache"];
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Populate the template cache from .html files scattered throughout the project.
 *
 * @ngInject
 */
function templates($templateCache) {
    var req = __webpack_require__(25);
    req.keys().forEach(function (r) {
        var key = "vimcloud" + r.substr(1);
        var html = req(r);
        $templateCache.put(key, html);
    });
}
exports.default = function (module) {
    module.app().run(templates);
};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/chooseInstances/choose-instances.html": 0,
	"./components/errorInfo/error-info.html": 3,
	"./components/tooltips/cloudInstanceTooltip.html": 26,
	"./templates/addAccountWizard/ChooseInstancesWizardStep.html": 27
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 25;

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<div ng-repeat=\"(key, value) in popoverData.extensionData\"> <div class=whiteRow ng-class-even=\"'colouredRow'\"> <span class=info> {{key}} </span> <span ng-if=\"key == 'provider'\" class=vendorIcon> <sw-vendor-icon vendor=\"{{value[key] == 'Microsoft Azure' ? '311.gif' : '4843.gif'}}\"></sw-vendor-icon> </span> <div ng-repeat=\"(keyInside, valueInside) in value\"> <span class=value> {{valueInside}} </span> </div> </div> </div> ";

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = "<style type=text/css>img.vm-item-template__details-icon{vertical-align:top}.vm-item-template__details-state{text-transform:capitalize}.vm-item-template__content{margin-left:15px}.vm-item-template__content--inline{display:inline-block}.clm-instances-list{border-top:solid 1px #d5d5d5}.text-ellipsis-overflow{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}</style> <script>var chooseInstances=function(e){var l,r,u={isInit:\"vimcloud-instances-is-init\",isInstancesLoaded:\"vimcloud-instances-is-loaded\",accountId:\"polling-options-accountId\",bindingData:\"vimcloud-step-choose-instances-data\",isDemoMode:\"is-demo-mode\",showDemoToast:\"show-demo-error-toast\",error:\"error\"};function o(a){return(r=a.isAccountDefined&&a.isAccountDefined()?function(e){var t;switch(e.providerType){case 1:n=e.accountId,t=s(\"/api/CloudInstanceAdmin/GetAwsInstancesForAccount\",n);break;case 2:a=e.accountId,t=s(\"/api/CloudInstanceAdmin/GetAzureInstancesForAccount\",a);break;default:t=$.Deferred().reject(\"Not supported\")}var a;var n;return t}(a):function(e){var t;switch(v(e)){case 1:u=(i=e).data[\"cloud-settings-aws\"],d={accessKeyId:u.accessKeyId,secretAccessKey:u.secretAccessKey},c=i.data[\"cloud-settings-aws\"].autoMonitoring,l=f(i),t=I(\"/api/CloudInstanceAdmin/GetAwsInstances?autoMonitoring=\"+c,d,l);break;case 2:o=(a=e).data[\"cloud-settings-azure\"],n={subscriptionId:o.subscriptionId,tenantId:o.tenantId,clientId:o.clientId,applicationSecretKey:o.applicationSecretKey},r=a.data[\"cloud-settings-azure\"].autoMonitoring,s=f(a),t=I(\"/api/CloudInstanceAdmin/GetAzureInstances?autoMonitoring=\"+r,n,s);break;default:t=$.Deferred().reject(\"Not supported\")}var a,n,r,s,o;var i,d,c,l,u;return t}(a)).then(function(e){if(!e.Success)return n(a,[]),a.data[u.error]=e.ErrorMessage,[];var t=function(e){for(var t=[],a=0;a<e.length;a++)t.push(_.assign({},e[a]));return t}(e.CloudInstances);return function(t,e){for(var a=v(t),n=0;n<e.length;n++)i(e[n],a),e[n].onMonitorChanged=function(e){e&&c(t,e)},2===a&&(e[n].getState=d)}(a,t),n(a,t),function(e,t){var a,n=N(e,u.bindingData);switch(v(e)){case 1:a={State:[],Region:[],AvailabilityZone:[],Type:[],Platform:[],Monitored:[]};break;case 2:a={State:[],ResourceGroup:[],Location:[],Type:[],Platform:[],Monitored:[]};break;default:throw new Error(l.translateIfNeeded(\"_t(Not supported)\"))}n.filterValues=a}(a),function(e,t){var a=N(e,u.bindingData),n=v(e),r={};switch(n){case 1:r={sortableColumns:[{id:\"State\",label:l.translateIfNeeded(\"_t(AWS Instance State)\")},{id:\"InstanceId\",label:l.translateIfNeeded(\"_t(Instance ID)\")},{id:\"Monitored\",label:l.translateIfNeeded(\"_t(Monitoring)\")},{id:\"Name\",label:l.translateIfNeeded(\"_t(Name)\")},{id:\"Region\",label:l.translateIfNeeded(\"_t(Region)\")},{id:\"Type\",label:l.translateIfNeeded(\"_t(Type)\")}],sortBy:{id:\"Name\",label:l.translateIfNeeded(\"_t(Name)\")},direction:\"asc\"};break;case 2:r={sortableColumns:[{id:\"State\",label:l.translateIfNeeded(\"_t(Azure VM Status)\")},{id:\"Name\",label:l.translateIfNeeded(\"_t(Name)\")},{id:\"Monitored\",label:l.translateIfNeeded(\"_t(Monitoring)\")},{id:\"ResourceGroup\",label:l.translateIfNeeded(\"_t(Resource Group)\")},{id:\"Type\",label:l.translateIfNeeded(\"_t(Type)\")},{id:\"InstanceId\",label:l.translateIfNeeded(\"_t(Virtual Machine ID)\")}],sortBy:{id:\"Name\",label:l.translateIfNeeded(\"_t(Name)\")},direction:\"asc\"}}a.sorting=r}(a),t},function(e){n(a,[]),a.data[u.error]=e})}function a(e){e.data[u.isInstancesLoaded]=!1,r=null}function s(e,t){var a=t||0;return $.ajax({type:\"Get\",url:e+\"?cloudAccountId=\"+a,dataType:\"json\"})}function f(e){return e.data[\"XSRF-TOKEN\"]}function I(e,t,a){return $.ajax({type:\"Post\",beforeSend:function(e){e.setRequestHeader(\"X-XSRF-TOKEN\",a)},data:t,url:e,dataType:\"json\"})}function p(e,t,a){return $.ajax({type:\"Post\",beforeSend:function(e){e.setRequestHeader(\"X-XSRF-TOKEN\",a)},data:JSON.stringify(t),url:e,dataType:\"json\",contentType:\"application/json; charset=utf-8\"})}function h(e,t){return{cloudAccountId:e,VirtualMachines:N(t,u.bindingData).rawData}}function v(e){return e.data[\"cloud-settings-selected-provider\"].id}function N(e,t){return e.data[t]}function n(e,t){var a=N(e,u.bindingData);a.items=t,a.rawData=t,a.providerType=v(e),a.canShowList=t&&t.constructor===Array}function i(e,t){switch(t){case 1:e.$templateUrl=\"aws-item-template\";break;case 2:e.$templateUrl=\"azure-item-template\"}}function d(e){var t;switch(e){case 0:t=l.translateIfNeeded(\"_t(Starting)\");break;case 16:t=l.translateIfNeeded(\"_t(Running)\");break;case 32:t=l.translateIfNeeded(\"_t(Deallocating)\");break;case 48:t=l.translateIfNeeded(\"_t(Deallocated)\");break;case 64:t=l.translateIfNeeded(\"_t(Stopping)\");break;case 80:t=l.translateIfNeeded(\"_t(Stopped)\");break;default:t=l.translateIfNeeded(\"_t(Unknown)\")}return t}function c(e,t){var a,n,r,s=N(e,u.bindingData),o=s.rawData;r=(n=e).data[u.showDemoToast],n.data[u.isDemoMode]&&r&&r();for(var i=0;i<o.length;i++)if(o[i].InstanceId===t.InstanceId){a=o[i];break}a&&(a.Monitored=t.Monitored),s.refreshFilters()}function t(){var s=this;function t(e,t,a){for(var n={},r=0;r<e.length;r++){var s=e[r][t],o=s;a&&(o=a(s)),n[s]||(n[s]={id:s,title:o,count:0}),n[s].count++}return Object.keys(n).sort().map(function(e){return n[e]})}function a(e){return e?l.translateIfNeeded(\"_t(On)\"):l.translateIfNeeded(\"_t(Off)\")}this.canShowList=!1,this.showHeader=!0,this.providerType=0,this.items=[],this.rawData=[],this.filterValues={},this.sorting={},this.selection={items:[]},this.remoteControl={refreshListData:function(){}},this.getFilters=function(e){switch(this.providerType){case 1:return function(){var e=this;return[{refId:\"State\",title:l.translateIfNeeded(\"_t(AWS Instance State)\"),values:t(e.rawData,\"State\")},{refId:\"Region\",title:l.translateIfNeeded(\"_t(Region)\"),values:t(e.rawData,\"Region\")},{refId:\"AvailabilityZone\",title:l.translateIfNeeded(\"_t(Availability Zone)\"),values:t(e.rawData,\"AvailabilityZone\")},{refId:\"Type\",title:l.translateIfNeeded(\"_t(Type)\"),values:t(e.rawData,\"Type\")},{refId:\"Platform\",title:l.translateIfNeeded(\"_t(Platform)\"),values:t(e.rawData,\"Platform\")},{refId:\"Monitored\",title:l.translateIfNeeded(\"_t(Monitoring)\"),values:t(e.rawData,\"Monitored\",a)}]}.apply(e);case 2:return function(){var e=this;return[{refId:\"State\",title:l.translateIfNeeded(\"_t(Azure VM Status)\"),values:t(e.rawData,\"State\",function(e){var t;switch(e){case 0:t=l.translateIfNeeded(\"_t(Starting)\");break;case 16:t=l.translateIfNeeded(\"_t(Running)\");break;case 32:t=l.translateIfNeeded(\"_t(Deallocating)\");break;case 48:t=l.translateIfNeeded(\"_t(Deallocated)\");break;case 64:t=l.translateIfNeeded(\"_t(Stopping)\");break;case 80:t=l.translateIfNeeded(\"_t(Stopped)\");break;default:t=l.translateIfNeeded(\"_t(Unknown)\")}return t})},{refId:\"ResourceGroup\",title:l.translateIfNeeded(\"_t(Resource Group)\"),values:t(e.rawData,\"ResourceGroup\")},{refId:\"Location\",title:l.translateIfNeeded(\"_t(Location)\"),values:t(e.rawData,\"Location\")},{refId:\"Type\",title:l.translateIfNeeded(\"_t(Type)\"),values:t(e.rawData,\"Type\")},{refId:\"Platform\",title:l.translateIfNeeded(\"_t(Platform)\"),values:t(e.rawData,\"Platform\")},{refId:\"Monitored\",title:l.translateIfNeeded(\"_t(Monitoring)\"),values:t(e.rawData,\"Monitored\",a)}]}.apply(e);default:throw new Error(l.translateIfNeeded(\"_t(Not supported)\"))}},this.options={selectionMode:\"multi\",selectionProperty:\"InstanceId\",smartMode:!0,hideSearch:!1,hidePagination:!1,pageSize:25,showAddRemoveFilterProperty:!0},this.onUpdate=function(e,t,a){var n=$.Deferred();return e.data[u.isInstancesLoaded]?(this.filterDataSource(e,t,a),n.resolve(!0)):r?r.then(function(e){n.resolve(!0)}):o(e).then(function(){s.filterDataSource(e,t,a),e.data[u.isInstancesLoaded]=!0,n.resolve(!0)}),n.promise()},this.filterDataSource=function(e,t,a){var n=function(e){for(var t=[],a=0;a<e.length;a++){var n=_.assign({},e[a]);t.push(n)}return t}(this.rawData);n=function(n,t){var e=Object.keys(t).map(function(e){return{key:e,data:t[e],hasFilters:0<t[e].length}}).filter(function(e){return e.hasFilters});if(0===e.length)return n;for(var r={},a=0;a<n.length;a++){var s=n[a];r[a]=[];for(var o=0;o<e.length;o++){for(var i=e[o],d=!1,c=0;c<i.data.length;c++)if(s[i.key]===i.data[c]){d=!0;break}r[a].push(d)}}var l=[];return Object.keys(r).forEach(function(e,t,a){r[e].every(function(e){return!0===e})&&l.push(n[e])}),l}(n=function(e,t){if(!t)return e;for(var a=[],n=0;n<e.length;n++){var r=e[n];-1===r.InstanceId.indexOf(t)&&-1===r.Name.indexOf(t)||a.push(r)}return s.selection={items:[]},a}.apply(this,[n,a]),t),this.items=n},this.setMonitoredValue=function(n,e){var r=this,s=0;r.selection.items.forEach(function(t){var e=_.find(r.items,function(e){return e.InstanceId===t}),a=_.find(r.rawData,function(e){return e.InstanceId===t});null!=e?(e.Monitored=n,a.Monitored=n,e.MonitoredByAccount&&s++):console.error()(l.translateIfNeeded(\"_t(Unable to toggle Monitored status. Could not find instance with ID %s)\"),t)}),0<s&&e.setNotificationMessage({message:s+l.translateIfNeeded(\"_t(of selected instances are monitored by another cloud account. No action was triggered for those cloud instances.)\")}),this.refreshFilters()},this.refreshFilters=function(){this.remoteControl.refreshListData()}}return{name:\"Instances/VMs selection\",init:function(e){return l=e.getTextServiceEx,e.data[u.bindingData]=new t,e.data[u.isInit]=!1,e.data[u.isDemoMode]=e.data[u.isDemoMode]||!1,a(e),!0},canGoNext:function(e){return!0},canGoBack:function(e){return!0},canFinish:function(e){return e.data[u.isInstancesLoaded]},onEnter:function(e){var t=$.Deferred();return a(e),e.delayExecute(function(){n(e,[]),e.data[\"vimcloud-instances-is-init\"]=!0,N(e,u.bindingData).remoteControl.refreshListData()},500),t.resolve(!0).promise()},onExit:function(e){return $.Deferred().resolve(!0).promise()},onFinish:function(t){var a=$.Deferred();return function(e){var t,a=v(e),n=N(e,u.accountId);(isNaN(n)||n<=0)&&$.Deferred().reject(l.translateIfNeeded(\"_t(Could not save instances for non-existent account.)\"));switch(e.busyMessage=l.translateIfNeeded(\"_t(Saving Instances/VMs...)\"),a){case 1:d=h(n,i=e),c=f(i),t=p(\"/api/CloudInstanceAdmin/AddOrUpdateAwsInstances\",d,c);break;case 2:s=h(n,r=e),o=f(r),t=p(\"/api/CloudInstanceAdmin/AddOrUpdateAzureInstances\",s,o);break;default:t=$.Deferred().reject(l.translateIfNeeded(\"_t(Not supported)\"))}var r,s,o;var i,d,c;return t}(t).then(function(e){a.resolve(!0)},function(e){t.data[u.error]=e,a.resolve(!1)}),a.promise()}}}()</script> <div ng-init=\"wizard.data.data['vimcloud-instances-init'](wizard.data)\"> <div class=vm-header ng-if='wizard.data.data[\"vimcloud-step-choose-instances-data\"].showHeader'> <div class=xui-text-h2 _t=\"['{{wizard.data.accountName}}']\"> Instances/VMs ({0}) </div> <div class=xui-margin-smv> <span _t>Choose which instances you want to monitor. By default, monitoring is <strong>turned on</strong> for all <strong>running instances/VMs</strong>. You can change what is monitored at any time from</span> <a target=_blank href=/ui/clm/accounts _t>Manage Cloud Accounts</a>. </div> <p></p> </div> <div ng-if=\"wizard.data.data['vimcloud-instances-is-init']\" class=clm-instances-list> <xui-filtered-list busy-message=\"_t(Retrieving instances/VMs...)\" busy-show-cancel-button=false items-source='wizard.data.data[\"vimcloud-step-choose-instances-data\"].items' on-refresh='wizard.data.data[\"vimcloud-step-choose-instances-data\"].onUpdate(wizard.data, filters, searching)' options='wizard.data.data[\"vimcloud-step-choose-instances-data\"].options' sorting='wizard.data.data[\"vimcloud-step-choose-instances-data\"].sorting' selection='wizard.data.data[\"vimcloud-step-choose-instances-data\"].selection' filter-values='wizard.data.data[\"vimcloud-step-choose-instances-data\"].filterValues' filter-properties-fn='wizard.data.data[\"vimcloud-step-choose-instances-data\"].getFilters(wizard.data.data[\"vimcloud-step-choose-instances-data\"])' remote-control='wizard.data.data[\"vimcloud-step-choose-instances-data\"].remoteControl' _ta> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item ng-show='wizard.data.data[\"vimcloud-step-choose-instances-data\"].selection.items.length > 0'> <xui-button icon=check class=text-uppercase display-style=tertiary is-disabled=\"wizard.data.data['is-demo-mode']\" ng-click='wizard.data.data[\"vimcloud-step-choose-instances-data\"].setMonitoredValue(true,wizard.data)' _t> MONITOR </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-show='wizard.data.data[\"vimcloud-step-choose-instances-data\"].selection.items.length > 0'> <xui-button icon=disable class=text-uppercase display-style=tertiary is-disabled=\"wizard.data.data['is-demo-mode']\" ng-click='wizard.data.data[\"vimcloud-step-choose-instances-data\"].setMonitoredValue(false,wizard.data)' _t> DO NOT MONITOR </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list> </div> </div> <script type=text/ng-template id=azure-item-template> <div class=\"container-fluid vm-item-template\">\n        <div class=\"row\" ng-class=\"{ 'text-muted': !item.Monitored }\">\n            <div class=\"col-md-7 xui-padding-sml vm-item-template__details\">\n                <img src=\"/ui/modules/vimcloud/images/icons/Azure_VMs.png\" class=\"vm-item-template__details-icon\" />\n\n                <div class=\"vm-item-template__content xui-margin-sml vm-item-template__content--inline\">\n                    <span title=\"_t(Name)\" _ta>{{::item.Name}}</span>\n\n                    <div class=\"text-muted\">\n                        <span title=\"_t(Instance ID)\" _ta>{{::item.InstanceId}}</span>,&nbsp;<span title=\"_t(Type)\" _ta>{{::item.Type}}</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-2 vm-item-template__details-state\">\n                <span title=\"_t(Instance State)\" _ta>{{::item.getState(item.State)}}</span>\n            </div>\n            <div class=\"col-md-2 text-ellipsis-overflow\">\n                <span title=\"_t(Resource Group: {0})\" _ta=\"['{{item.ResourceGroup}}']\">{{::item.ResourceGroup}}</span>\n            </div>\n            <div class=\"col-md-1 pull-right\">\n                <div ng-if='!item.MonitoredByAccount'>\n                    <div class=\"xui-text-dscrn\" _t>Monitoring</div>\n                    <xui-switch ng-if=\"item.Monitored\" ng-model=\"item.Monitored\" ng-change=\"item.onMonitorChanged(item)\"\n                                title=\"_t(This instance is monitored. Click to stop monitoring this instance.)\" _ta>\n                    </xui-switch>\n                    <xui-switch ng-if=\"!item.Monitored\" ng-model=\"item.Monitored\" ng-change=\"item.onMonitorChanged(item)\"\n                                title=\"_t(This instance is not monitored. Click to monitor this instance.)\" _ta>\n                    </xui-switch>\n                </div>\n                <div ng-if='item.MonitoredByAccount'>\n                    <div class=\"xui-text-dscrn\" _t>Monitored by</div>\n                    <div class=\"xui-text-r\">{{::item.MonitoredByAccount}}</div>\n                </div>\n            </div>\n        </div>\n    </div> </script> <script type=text/ng-template id=aws-item-template> <div class=\"container-fluid vm-item-template\">\n        <div class=\"row\" ng-class=\"{ 'text-muted': !item.Monitored }\">\n            <div class=\"col-md-7 xui-padding-sml vm-item-template__details\">\n                <img src=\"/ui/modules/vimcloud/images/icons/Amazon_VMs.png\" class=\"vm-item-template__details-icon\" />\n\n                <div class=\"vm-item-template__content xui-margin-sml vm-item-template__content--inline\">\n                    <span title=\"_t(Name)\" _ta>{{::item.Name}}</span>\n\n                    <div class=\"text-muted\">\n                        <span title=\"_t(Instance ID)\" _ta>{{::item.InstanceId}}</span>,&nbsp;<span title=\"_t(Type)\" _ta>{{::item.Type}}</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-2 vm-item-template__details-state\">\n                <span title=\"_t(Instance State)\" _ta>{{::item.State}}</span>\n            </div>\n            <div class=\"col-md-2 text-ellipsis-overflow\">\n                <span title=\"_t(Region: {0})\" _ta=\"['{{item.Region}}']\">{{::item.Region}}</span>\n            </div>\n            <div class=\"col-md-1 pull-right\">\n                <div ng-if='!item.MonitoredByAccount'>\n                    <div class=\"xui-text-dscrn\" _t>Monitoring</div>\n                    <xui-switch ng-if=\"item.Monitored\" ng-model=\"item.Monitored\" ng-change=\"item.onMonitorChanged(item)\"\n                                title=\"_t(This instance is monitored. Click to stop monitoring this instance.)\" _ta>\n                    </xui-switch>\n                    <xui-switch ng-if=\"!item.Monitored\" ng-model=\"item.Monitored\" ng-change=\"item.onMonitorChanged(item)\"\n                                title=\"_t(This instance is not monitored. Click to monitor this instance.)\" _ta>\n                    </xui-switch>\n                </div>\n                <div ng-if='item.MonitoredByAccount'>\n                    <div class=\"xui-text-dscrn\" _t>Monitored by</div>\n                    <div class=\"xui-text-r\">{{::item.MonitoredByAccount}}</div> \n                </div>\n            </div>\n        </div>\n    </div> </script> ";

/***/ })
/******/ ]);
//# sourceMappingURL=vimcloud.js.map