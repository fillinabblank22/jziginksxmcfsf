/*!
 * @solarwinds/ha-frontend 2019.6.0-3158
 * @copyright 2021 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 20);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
        this.useTestServer = false;
        this.testServerUrl = "http://localhost:8888/test_server";
        this.settingsEnableHighAvailability = "HA-EnableHighAvailability";
        this.settingsMemberDownInterval = "HA-DefaultPoolIntervalMemberDown";
        this.settingsEmailMemberStatusChanged = "HA-EmailMeOnServerStatusChange";
        this.settingsEmailFacilityStatusChanged = "HA-EmailMeOnFacilityStatusChange";
        this.settingsEmailResourceStatusChanged = "HA-EmailMeOnResourceStatusChange";
        this.settingsIntervalTakeoverRetention = "HA-IntervalTakeoverRetention";
        this.validatePoolStartEvent = "validate-pool-begin";
        this.validatePoolEndEvent = "validate-pool-end";
        this.addPoolStepEnterEvent = "pool-step-enter";
        this.haPorts = {
            bothRequired: 5671,
            primaryServerRequired: 4369,
            secondaryServerRequired: 25672
        };
        this.links = {
            solarWindsOrionInstaller: "/sapi/installer/get",
            solarWindsContact: "https://www.solarwinds.com/company/contact.aspx",
            buyNow: "https://us.store.solarwinds.com/addtocart.aspx?skinid=1&SKU=45607&quantity=1",
            haLicensingPricing: "https://www.solarwinds.com/onlinequotes/#/onlineQuote?productcode=170",
            reportDevicesNeedUpdated: "#",
            updateInstruction: "#",
            solarWindsOrionAdminAlertSettings: "/Orion/Admin/AlertSettings.aspx",
            demoDownloads: "https://www.solarwinds.com/downloads/?CMPSource=DEMO_ORION"
        };
        this.summary = {
            updateInterval: 30000,
            haSyslogAndTrapsReportName: "'Devices Sending Syslog or SNMP-Traps to Orion'"
        };
    }
    //Plugins names
    Constants.pluginsNames = {
        virtualIpResourcePluginName: "virtualIpResource",
        virtualHostNameResource: "virtualHostNameResource",
        poolConfiguration: "poolConfiguration"
    };
    Constants.allPluginsNames = [
        Constants.pluginsNames.virtualIpResourcePluginName,
        Constants.pluginsNames.virtualHostNameResource,
        Constants.pluginsNames.poolConfiguration
    ];
    return Constants;
}());
exports.default = Constants;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQ0FBb0M7O0FBRXBDO0lBQUE7UUFDVyxrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUMvQixrQkFBYSxHQUFXLG1DQUFtQyxDQUFDO1FBQzVELG1DQUE4QixHQUFXLDJCQUEyQixDQUFDO1FBQ3JFLCtCQUEwQixHQUFXLGtDQUFrQyxDQUFDO1FBQ3hFLHFDQUFnQyxHQUFXLGdDQUFnQyxDQUFDO1FBQzVFLHVDQUFrQyxHQUFXLGtDQUFrQyxDQUFDO1FBQ2hGLHVDQUFrQyxHQUFXLGtDQUFrQyxDQUFDO1FBQ2hGLHNDQUFpQyxHQUFXLDhCQUE4QixDQUFDO1FBQzNFLDJCQUFzQixHQUFXLHFCQUFxQixDQUFDO1FBQ3ZELHlCQUFvQixHQUFXLG1CQUFtQixDQUFDO1FBQ25ELDBCQUFxQixHQUFXLGlCQUFpQixDQUFDO1FBRWxELFlBQU8sR0FBRztZQUNiLFlBQVksRUFBRSxJQUFJO1lBQ2xCLHFCQUFxQixFQUFFLElBQUk7WUFDM0IsdUJBQXVCLEVBQUUsS0FBSztTQUNqQyxDQUFDO1FBRUssVUFBSyxHQUFHO1lBQ1gsd0JBQXdCLEVBQUUscUJBQXFCO1lBQy9DLGlCQUFpQixFQUFFLGlEQUFpRDtZQUNwRSxNQUFNLEVBQUUsOEVBQThFO1lBQ3RGLGtCQUFrQixFQUFFLHVFQUF1RTtZQUMzRix3QkFBd0IsRUFBRSxHQUFHO1lBQzdCLGlCQUFpQixFQUFFLEdBQUc7WUFDdEIsaUNBQWlDLEVBQUUsaUNBQWlDO1lBQ3BFLGFBQWEsRUFBRSw0REFBNEQ7U0FDOUUsQ0FBQztRQUVGLFlBQU8sR0FBRztZQUNOLGNBQWMsRUFBRSxLQUFLO1lBQ3JCLDBCQUEwQixFQUFFLGlEQUFpRDtTQUNoRixDQUFDO0lBY04sQ0FBQztJQVpHLGVBQWU7SUFDRCxzQkFBWSxHQUFHO1FBQ3pCLDJCQUEyQixFQUFFLG1CQUFtQjtRQUNoRCx1QkFBdUIsRUFBRSx5QkFBeUI7UUFDbEQsaUJBQWlCLEVBQUUsbUJBQW1CO0tBQ3pDLENBQUM7SUFFWSx5QkFBZSxHQUFHO1FBQzVCLFNBQVMsQ0FBQyxZQUFZLENBQUMsMkJBQTJCO1FBQ2xELFNBQVMsQ0FBQyxZQUFZLENBQUMsdUJBQXVCO1FBQzlDLFNBQVMsQ0FBQyxZQUFZLENBQUMsaUJBQWlCO0tBQzNDLENBQUM7SUFDTixnQkFBQztDQUFBLEFBL0NELElBK0NDO2tCQS9Db0IsU0FBUyJ9

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var OrionServerPollingEngine;
(function (OrionServerPollingEngine) {
    OrionServerPollingEngine[OrionServerPollingEngine["mainPoller"] = 0] = "mainPoller";
    OrionServerPollingEngine[OrionServerPollingEngine["mainPollerStandby"] = 1] = "mainPollerStandby";
    OrionServerPollingEngine[OrionServerPollingEngine["additionalPoller"] = 2] = "additionalPoller";
    OrionServerPollingEngine[OrionServerPollingEngine["additionalPollerStandby"] = 3] = "additionalPollerStandby";
    OrionServerPollingEngine[OrionServerPollingEngine["additionalWebsite"] = 4] = "additionalWebsite";
})(OrionServerPollingEngine = exports.OrionServerPollingEngine || (exports.OrionServerPollingEngine = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25TZXJ2ZXJQb2xsaW5nRW5naW5lLWVudW0uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmlvblNlcnZlclBvbGxpbmdFbmdpbmUtZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHVDQUF1QztBQUN2QyxJQUFZLHdCQU1YO0FBTkQsV0FBWSx3QkFBd0I7SUFDaEMsbUZBQVUsQ0FBQTtJQUNWLGlHQUFpQixDQUFBO0lBQ2pCLCtGQUFnQixDQUFBO0lBQ2hCLDZHQUF1QixDQUFBO0lBQ3ZCLGlHQUFpQixDQUFBO0FBQ3JCLENBQUMsRUFOVyx3QkFBd0IsR0FBeEIsZ0NBQXdCLEtBQXhCLGdDQUF3QixRQU1uQyJ9

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(0);
var PoolModel = /** @class */ (function () {
    function PoolModel(model) {
        var _this = this;
        this.itemType = "pool";
        this.dnsTypes = ["Microsoft", "BIND", "Other"];
        this.isProductMismatch = function () {
            return !_this.children.every(function (value, index, array) {
                return (!(value.installedModulesMismatch)
                    || value.installedModulesMismatch.length === 0);
            });
        };
        this.iconMap = [
            "orion-sitemaster-pool",
            "orion-ha-pool"
        ];
        this.statusIconMap = [
            "unknown",
            "up",
            "down",
            "warning",
            "disabled"
        ];
        if (model) {
            _.extend(this, model);
        }
        this.name = this.displayName;
        this.icon = this.iconMap[this.poolType];
        this.iconStatus = this.statusIconMap[this.status];
    }
    PoolModel.prototype.getNormalized = function () {
        var clonedPool = _.cloneDeep(this);
        if (!clonedPool.pluginsData) {
            return clonedPool;
        }
        var pluginsData = clonedPool.pluginsData;
        //virtual IP
        if (pluginsData.virtualIpResource && !pluginsData.virtualIpResource.ipAddress) {
            pluginsData.virtualIpResource = undefined;
        }
        //virtual hostname
        if (pluginsData.virtualHostNameResource &&
            pluginsData.virtualHostNameResource.hostName &&
            pluginsData.virtualHostNameResource.dnsType) {
            //Other
            if (pluginsData.virtualHostNameResource.dnsType === this.dnsTypes[2]) {
                if (!pluginsData.virtualHostNameResource.dnsIP || !pluginsData.virtualHostNameResource.dnsZone) {
                    pluginsData.virtualHostNameResource = undefined;
                }
                else {
                    pluginsData.virtualHostNameResource.dnsUserName = undefined;
                    pluginsData.virtualHostNameResource.dnsPassword = undefined;
                }
                //MS + BIND
            }
            else if (!pluginsData.virtualHostNameResource.dnsIP ||
                !pluginsData.virtualHostNameResource.dnsZone ||
                !pluginsData.virtualHostNameResource.dnsUserName ||
                !pluginsData.virtualHostNameResource.dnsPassword) {
                pluginsData.virtualHostNameResource = undefined;
            }
        }
        else {
            pluginsData.virtualHostNameResource = undefined;
        }
        //PoolConfiguration
        if (pluginsData.poolConfiguration &&
            pluginsData.poolConfiguration.preferredMemberId) {
            pluginsData.poolConfiguration.preferredMemberId = Number(pluginsData.poolConfiguration.preferredMemberId);
            if (pluginsData.poolConfiguration.preferredMemberId === 0) {
                pluginsData.poolConfiguration = undefined;
            }
        }
        else {
            pluginsData.poolConfiguration = undefined;
        }
        return clonedPool;
    };
    PoolModel.prototype.getNormalizedWithPoolPlugin = function (includedPluginName) {
        var pool = this.getNormalized();
        if (!pool.pluginsData) {
            return pool;
        }
        constants_1.default.allPluginsNames.forEach(function (pluginName) {
            if (pool.pluginsData[pluginName] !== undefined && pluginName !== includedPluginName) {
                pool.pluginsData[pluginName] = undefined;
            }
        });
        return pool;
    };
    Object.defineProperty(PoolModel.prototype, "engineType", {
        //Virtual properties below used to compare pools and servers models with standard Array sort compareFunction
        get: function () {
            return this.poolType;
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(PoolModel.prototype, "poolName", {
        get: function () {
            return this.name;
        },
        enumerable: true,
        configurable: true
    });
    return PoolModel;
}());
exports.default = PoolModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbC1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBvb2wtbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFNQSxvREFBK0M7QUFFL0M7SUEyQkksbUJBQVksS0FBaUI7UUFBN0IsaUJBT0M7UUEzQk0sYUFBUSxHQUFHLE1BQU0sQ0FBQztRQWdCbEIsYUFBUSxHQUFrQixDQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFpRmhFLHNCQUFpQixHQUFHO1lBQ2hCLE1BQU0sQ0FBQyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLO2dCQUM1QyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQW9CLEtBQU0sQ0FBQyx3QkFBd0IsQ0FBQzt1QkFDbkMsS0FBTSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQztZQUM1RSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQztRQUVNLFlBQU8sR0FBa0I7WUFDN0IsdUJBQXVCO1lBQ3ZCLGVBQWU7U0FDbEIsQ0FBQztRQUVNLGtCQUFhLEdBQWtCO1lBQ25DLFNBQVM7WUFDVCxJQUFJO1lBQ0osTUFBTTtZQUNOLFNBQVM7WUFDVCxVQUFVO1NBQ2IsQ0FBQztRQTlGRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUIsQ0FBQztRQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVNLGlDQUFhLEdBQXBCO1FBQ0ksSUFBTSxVQUFVLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBWSxJQUFJLENBQUMsQ0FBQztRQUVoRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQzFCLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDdEIsQ0FBQztRQUVELElBQUksV0FBVyxHQUFnQixVQUFVLENBQUMsV0FBVyxDQUFDO1FBRXRELFlBQVk7UUFDWixFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1RSxXQUFXLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxDQUFDO1FBQzlDLENBQUM7UUFFRCxrQkFBa0I7UUFDbEIsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLHVCQUF1QjtZQUNuQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsUUFBUTtZQUM1QyxXQUFXLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUU5QyxPQUFPO1lBQ1AsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkUsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQzdGLFdBQVcsQ0FBQyx1QkFBdUIsR0FBRyxTQUFTLENBQUM7Z0JBQ3BELENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osV0FBVyxDQUFDLHVCQUF1QixDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUM7b0JBQzVELFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO2dCQUNoRSxDQUFDO2dCQUNELFdBQVc7WUFDZixDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLEtBQUs7Z0JBQ2pELENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLE9BQU87Z0JBQzVDLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLFdBQVc7Z0JBQ2hELENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELFdBQVcsQ0FBQyx1QkFBdUIsR0FBRyxTQUFTLENBQUM7WUFDcEQsQ0FBQztRQUNMLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLFdBQVcsQ0FBQyx1QkFBdUIsR0FBRyxTQUFTLENBQUM7UUFDcEQsQ0FBQztRQUVELG1CQUFtQjtRQUNuQixFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsaUJBQWlCO1lBQzdCLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7WUFDbEQsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUUxRyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEQsV0FBVyxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQztZQUM5QyxDQUFDO1FBQ0wsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osV0FBVyxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQztRQUM5QyxDQUFDO1FBRUQsTUFBTSxDQUFDLFVBQVUsQ0FBQztJQUN0QixDQUFDO0lBRU0sK0NBQTJCLEdBQWxDLFVBQW1DLGtCQUEwQjtRQUN6RCxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDbEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNwQixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRCxtQkFBUyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFVO1lBQ3pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEtBQUssU0FBUyxJQUFJLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xGLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEdBQUcsU0FBUyxDQUFDO1lBQzdDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQXVCRCxzQkFBSSxpQ0FBVTtRQURkLDRHQUE0RzthQUM1RztZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBO0lBQUEsQ0FBQztJQUVGLHNCQUFJLCtCQUFRO2FBQVo7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyQixDQUFDOzs7T0FBQTtJQUNMLGdCQUFDO0FBQUQsQ0FBQyxBQXBJRCxJQW9JQyJ9

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var MultiTemplate = /** @class */ (function () {
    function MultiTemplate() {
        var _this = this;
        this.restrict = "E";
        this.template = function (element, attrs) {
            if (attrs["isReadOnly"]) {
                return _this.readOnlyTemplateHtml;
            }
            else {
                return _this.templateHtml;
            }
        };
    }
    return MultiTemplate;
}());
exports.MultiTemplate = MultiTemplate;
exports.default = MultiTemplate;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGlUZW1wbGF0ZS1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtdWx0aVRlbXBsYXRlLWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQUE7UUFBQSxpQkFZQztRQVhVLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFJZixhQUFRLEdBQUcsVUFBQyxPQUE0QixFQUFFLEtBQXFCO1lBQ2xFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLE1BQU0sQ0FBQyxLQUFJLENBQUMsb0JBQW9CLENBQUM7WUFDckMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDO1lBQzdCLENBQUM7UUFDTCxDQUFDLENBQUM7SUFDTixDQUFDO0lBQUQsb0JBQUM7QUFBRCxDQUFDLEFBWkQsSUFZQztBQVpZLHNDQUFhO0FBYzFCLGtCQUFlLGFBQWEsQ0FBQyJ9

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionServerPollingEngine_enum_1 = __webpack_require__(1);
var runType_enum_1 = __webpack_require__(5);
var orionServerProperty_model_1 = __webpack_require__(27);
var sumary_item_property_1 = __webpack_require__(13);
var OrionServerModel = /** @class */ (function () {
    function OrionServerModel(model) {
        this.itemType = "orionServer";
        this.iconMap = [
            "orion-sitemaster",
            "orion-sitemaster-backup",
            "orion-ape",
            "orion-ape-backup",
            "orion-webserver"
        ];
        this.statusIconMap = {
            0: "unknown",
            1: "up",
            2: "down",
            14: "critical",
            27: "disabled"
        };
        if (model) {
            _.extend(this, model);
        }
        this.name = this.hostName;
        this.displayName = this.hostName;
        this.icon = this.iconMap[this.pollingEngineType];
        this.iconStatus = this.statusIconMap[this.status];
        this.populateProperties(this);
    }
    OrionServerModel.prototype.populateProperties = function (server) {
        server.properties = [];
        for (var property in server) {
            if (server.hasOwnProperty(property) && this.showInDetails(property)) {
                if (server.runType === runType_enum_1.RunType.Standby) {
                    if (property !== "lastDatabaseUpdate" &&
                        property !== "pollingEngineType" &&
                        property !== "runType") {
                        continue;
                    }
                }
                if (server.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalWebsite
                    || server.poolMemberId === 0) {
                    if (property !== "pollingEngineType") {
                        continue;
                    }
                }
                var serverProperty = new orionServerProperty_model_1.default();
                serverProperty.name = property;
                serverProperty.displayName = this.getPropertyDisplayName(property);
                if (property === "pollingEngineType") {
                    var enumString = orionServerPollingEngine_enum_1.OrionServerPollingEngine[server.pollingEngineType];
                    serverProperty.value = this.camelCaseSplit(enumString).toUpperCase().replace(" STANDBY", "");
                }
                else if (property === "runType") {
                    var enumString = runType_enum_1.RunType[server.runType];
                    serverProperty.value = this.camelCaseSplit(enumString).toUpperCase();
                }
                else if (property === "pollingCompletion") {
                    serverProperty.value = server[property].toString() + " %";
                }
                else {
                    serverProperty.value = server[property];
                }
                if (property === "pollingRate") {
                    if (serverProperty.value !== null && serverProperty.value !== undefined) {
                        for (var i = 0; i < serverProperty.value.length; i++) {
                            var pollingRateProperty = new sumary_item_property_1.default();
                            pollingRateProperty.key = serverProperty.value[i].displayName;
                            pollingRateProperty.value = serverProperty.value[i].value;
                            server.properties.push(pollingRateProperty);
                        }
                        continue;
                    }
                }
                if (serverProperty.value !== null && serverProperty.value !== undefined) {
                    var prop = new sumary_item_property_1.default();
                    prop.key = serverProperty.displayName;
                    prop.value = serverProperty.value;
                    server.properties.push(prop);
                }
            }
        }
    };
    OrionServerModel.prototype.showInDetails = function (property) {
        var excludeFromDetails = [
            "itemType",
            "properties",
            "hostName",
            "displayName",
            "name",
            "ipAddress",
            "description",
            "statusDescription",
            "children",
            "status",
            "poolMemberId",
            "poolId",
            "icon",
            "iconMap",
            "iconStatus",
            "statusIconMap",
            "poolType",
            "installedModulesOk",
            "installedModulesMismatch"
        ];
        return excludeFromDetails.indexOf(property) === -1;
    };
    OrionServerModel.prototype.getPropertyDisplayName = function (property) {
        // Localization should go here (returning human readable property names)
        switch (property) {
            case "runType": return "HA Role";
            case "pollingEngineType": return "Server Type";
            default: return this.camelCaseSplit(property);
        }
    };
    OrionServerModel.prototype.camelCaseSplit = function (camelCaseString) {
        return camelCaseString
            .replace(/([a-z])([A-Z])/g, "$1 $2")
            .replace(/\b([A-Z]+)([A-Z])([a-z])/, "$1 $2$3");
    };
    Object.defineProperty(OrionServerModel.prototype, "engineType", {
        //Virtual properties below used to compare pools and servers models with standard Array sort compareFunction
        get: function () {
            if (this.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPoller ||
                this.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPollerStandby) {
                return 0;
            }
            return 1;
        },
        enumerable: true,
        configurable: true
    });
    return OrionServerModel;
}());
exports.default = OrionServerModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25TZXJ2ZXItbW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmlvblNlcnZlci1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLGlGQUEyRTtBQUMzRSwrQ0FBeUM7QUFDekMseUVBQThEO0FBQzlELHdFQUFrRTtBQUVsRTtJQWlDSSwwQkFBWSxLQUF3QjtRQXZCN0IsYUFBUSxHQUFHLGFBQWEsQ0FBQztRQWtDeEIsWUFBTyxHQUFrQjtZQUM3QixrQkFBa0I7WUFDbEIseUJBQXlCO1lBQ3pCLFdBQVc7WUFDWCxrQkFBa0I7WUFDbEIsaUJBQWlCO1NBQ3BCLENBQUM7UUFFTSxrQkFBYSxHQUNyQjtZQUNJLENBQUMsRUFBRSxTQUFTO1lBQ1osQ0FBQyxFQUFFLElBQUk7WUFDUCxDQUFDLEVBQUUsTUFBTTtZQUNULEVBQUUsRUFBRSxVQUFVO1lBQ2QsRUFBRSxFQUFFLFVBQVU7U0FDakIsQ0FBQztRQXpCRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUIsQ0FBQztRQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFtQk8sNkNBQWtCLEdBQTFCLFVBQTJCLE1BQXdCO1FBQy9DLE1BQU0sQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLEdBQUcsQ0FBQyxDQUFDLElBQUksUUFBUSxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDMUIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEUsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sS0FBSyxzQkFBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ3JDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxvQkFBb0I7d0JBQ2pDLFFBQVEsS0FBSyxtQkFBbUI7d0JBQ2hDLFFBQVEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO3dCQUN6QixRQUFRLENBQUM7b0JBQ2IsQ0FBQztnQkFDTCxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsS0FBSyx3REFBd0IsQ0FBQyxpQkFBaUI7dUJBQ3BFLE1BQU0sQ0FBQyxZQUFZLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDL0IsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLG1CQUFtQixDQUFDLENBQUMsQ0FBQzt3QkFDbkMsUUFBUSxDQUFDO29CQUNiLENBQUM7Z0JBQ0wsQ0FBQztnQkFFRCxJQUFJLGNBQWMsR0FBRyxJQUFJLG1DQUFtQixFQUFFLENBQUM7Z0JBQy9DLGNBQWMsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDO2dCQUMvQixjQUFjLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFFbkUsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLG1CQUFtQixDQUFDLENBQUMsQ0FBQztvQkFDbkMsSUFBTSxVQUFVLEdBQUcsd0RBQXdCLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUM7b0JBQ3RFLGNBQWMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNqRyxDQUFDO2dCQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDaEMsSUFBTSxVQUFVLEdBQUcsc0JBQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQzNDLGNBQWMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDekUsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLG1CQUFtQixDQUFDLENBQUMsQ0FBQztvQkFDMUMsY0FBYyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxFQUFFLEdBQUcsSUFBSSxDQUFDO2dCQUM5RCxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLGNBQWMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1QyxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUM3QixFQUFFLENBQUMsQ0FBQyxjQUFjLENBQUMsS0FBSyxLQUFLLElBQUksSUFBSSxjQUFjLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQzs0QkFDbkQsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLDhCQUFtQixFQUFFLENBQUM7NEJBRXBELG1CQUFtQixDQUFDLEdBQUcsR0FBRyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs0QkFDOUQsbUJBQW1CLENBQUMsS0FBSyxHQUFHLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDOzRCQUMxRCxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO3dCQUNoRCxDQUFDO3dCQUNELFFBQVEsQ0FBQztvQkFDYixDQUFDO2dCQUNMLENBQUM7Z0JBRUQsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLEtBQUssS0FBSyxJQUFJLElBQUksY0FBYyxDQUFDLEtBQUssS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUN0RSxJQUFJLElBQUksR0FBRyxJQUFJLDhCQUFtQixFQUFFLENBQUM7b0JBQ3JDLElBQUksQ0FBQyxHQUFHLEdBQUcsY0FBYyxDQUFDLFdBQVcsQ0FBQztvQkFDdEMsSUFBSSxDQUFDLEtBQUssR0FBRyxjQUFjLENBQUMsS0FBSyxDQUFDO29CQUNsQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakMsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVPLHdDQUFhLEdBQXJCLFVBQXNCLFFBQWdCO1FBQ2xDLElBQU0sa0JBQWtCLEdBQUc7WUFDdkIsVUFBVTtZQUNWLFlBQVk7WUFDWixVQUFVO1lBQ1YsYUFBYTtZQUNiLE1BQU07WUFDTixXQUFXO1lBQ1gsYUFBYTtZQUNiLG1CQUFtQjtZQUNuQixVQUFVO1lBQ1YsUUFBUTtZQUNSLGNBQWM7WUFDZCxRQUFRO1lBQ1IsTUFBTTtZQUNOLFNBQVM7WUFDVCxZQUFZO1lBQ1osZUFBZTtZQUNmLFVBQVU7WUFDVixvQkFBb0I7WUFDcEIsMEJBQTBCO1NBQzdCLENBQUM7UUFDRixNQUFNLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFTyxpREFBc0IsR0FBOUIsVUFBK0IsUUFBZ0I7UUFDM0Msd0VBQXdFO1FBQ3hFLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDZixLQUFLLFNBQVMsRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ2pDLEtBQUssbUJBQW1CLEVBQUUsTUFBTSxDQUFDLGFBQWEsQ0FBQztZQUMvQyxTQUFTLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xELENBQUM7SUFDTCxDQUFDO0lBRU8seUNBQWMsR0FBdEIsVUFBdUIsZUFBdUI7UUFDMUMsTUFBTSxDQUFDLGVBQWU7YUFFakIsT0FBTyxDQUFDLGlCQUFpQixFQUFFLE9BQU8sQ0FBQzthQUVuQyxPQUFPLENBQUMsMEJBQTBCLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUdELHNCQUFJLHdDQUFVO1FBRGQsNEdBQTRHO2FBQzVHO1lBQ0ksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixLQUFLLHdEQUF3QixDQUFDLFVBQVU7Z0JBQzlELElBQUksQ0FBQyxpQkFBaUIsS0FBSyx3REFBd0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7Z0JBRXhFLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDYixDQUFDO1lBRUQsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNiLENBQUM7OztPQUFBO0lBQ0wsdUJBQUM7QUFBRCxDQUFDLEFBM0tELElBMktDIn0=

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var RunType;
(function (RunType) {
    RunType[RunType["Active"] = 0] = "Active";
    RunType[RunType["Standby"] = 1] = "Standby";
})(RunType = exports.RunType || (exports.RunType = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicnVuVHlwZS1lbnVtLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicnVuVHlwZS1lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBWSxPQUdYO0FBSEQsV0FBWSxPQUFPO0lBQ2YseUNBQU0sQ0FBQTtJQUNOLDJDQUFPLENBQUE7QUFDWCxDQUFDLEVBSFcsT0FBTyxHQUFQLGVBQU8sS0FBUCxlQUFPLFFBR2xCIn0=

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var HaBaseController = /** @class */ (function () {
    function HaBaseController() {
    }
    HaBaseController.prototype.validateForm = function (formCtrl) {
        var _this = this;
        if (formCtrl != null && !formCtrl.$valid) {
            _.each(formCtrl.$error, function (errors, validator) {
                _.each(errors, function (formItem) {
                    //Form item is the form
                    if (formItem.$submitted !== undefined) {
                        if (!_this.validateForm(formItem)) {
                            return false;
                        }
                    }
                    else {
                        //Form item is the field
                        formCtrl[formItem.$name].$dirty = true;
                        formCtrl[formItem.$name].$setValidity(validator, false);
                    }
                });
            });
            return false;
        }
        return true;
    };
    return HaBaseController;
}());
exports.default = HaBaseController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGFCYXNlLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJoYUJhc2UtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQUE7SUF1QkEsQ0FBQztJQXJCYSx1Q0FBWSxHQUF0QixVQUF1QixRQUE2QjtRQUFwRCxpQkFvQkM7UUFuQkcsRUFBRSxDQUFDLENBQUMsUUFBUSxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxVQUFDLE1BQVcsRUFBRSxTQUFjO2dCQUNoRCxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFDLFFBQWE7b0JBQ3pCLHVCQUF1QjtvQkFDdkIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO3dCQUNwQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUMvQixNQUFNLENBQUMsS0FBSyxDQUFDO3dCQUNqQixDQUFDO29CQUNMLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osd0JBQXdCO3dCQUN4QixRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDNUQsQ0FBQztnQkFDTCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0wsdUJBQUM7QUFBRCxDQUFDLEFBdkJELElBdUJDIn0=

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var UpdateController = /** @class */ (function () {
    function UpdateController(swApi, $interval, constants, demoService) {
        var _this = this;
        this.swApi = swApi;
        this.$interval = $interval;
        this.constants = constants;
        this.demoService = demoService;
        this.$onInit = function () {
            _this.showIframe = false;
            //call the SWA authentication every 5 minutes
            if (_this.constants.environment.authTimeoutMilliseconds !== 0) {
                _this.$interval(function () {
                    _this.SaveAuthenticationCookie();
                }, _this.constants.environment.authTimeoutMilliseconds / 2);
            }
        };
    }
    UpdateController.prototype.SaveAuthenticationCookie = function () {
        var _this = this;
        if (this.demoService.isDemoMode()) {
            this.demoService.showDemoToast();
            return this.demoService.createActionResultPromise().then(function () { return false; });
        }
        return this.swApi.api(false).one("swa/update/saveAuthenticationCookie").get().then(function (response) {
            _this.showIframe = true;
            return true;
        }).catch(function (error) {
            return false;
        });
    };
    ;
    UpdateController.$inject = ["swApi", "$interval", "constants", "demoService"];
    return UpdateController;
}());
exports.default = UpdateController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ1cGRhdGUtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUtBO0lBSUksMEJBQ1ksS0FBVSxFQUNWLFNBQThCLEVBQzlCLFNBQXFCLEVBQ3JCLFdBQXdCO1FBSnBDLGlCQUl3QztRQUg1QixVQUFLLEdBQUwsS0FBSyxDQUFLO1FBQ1YsY0FBUyxHQUFULFNBQVMsQ0FBcUI7UUFDOUIsY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUU3QixZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUV4Qiw2Q0FBNkM7WUFDN0MsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0QsS0FBSSxDQUFDLFNBQVMsQ0FBQztvQkFDUCxLQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztnQkFDcEMsQ0FBQyxFQUNELEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLHVCQUF1QixHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2hFLENBQUM7UUFDTCxDQUFDLENBQUM7SUFacUMsQ0FBQztJQWNqQyxtREFBd0IsR0FBL0I7UUFBQSxpQkFZQztRQVhHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDakMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCLEVBQVcsQ0FBQyxJQUFJLENBQUMsY0FBTSxPQUFBLEtBQUssRUFBTCxDQUFLLENBQUMsQ0FBQztRQUNuRixDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FDOUUsVUFBQyxRQUFvQztZQUNqQyxLQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLEtBQVU7WUFDaEIsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFBQSxDQUFDO0lBakNZLHdCQUFPLEdBQUcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQztJQWtDL0UsdUJBQUM7Q0FBQSxBQW5DRCxJQW1DQztrQkFuQ29CLGdCQUFnQiJ9

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var settings_service_1 = __webpack_require__(9);
var settings_service_demo_mock_1 = __webpack_require__(26);
exports.default = function (module) {
    module.factory("settingsService", function (constants, $injector) {
        return constants.environment.demoMode
            ? $injector.instantiate(settings_service_demo_mock_1.default)
            : $injector.instantiate(settings_service_1.default);
    }, ["constants", "$injector"]);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVEQUFpRDtBQUNqRCwyRUFBK0Q7QUFFL0Qsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsVUFBQyxTQUFjLEVBQUUsU0FBYztRQUM3RCxNQUFNLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRO1lBQ2pDLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLG9DQUFtQixDQUFDO1lBQzVDLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLDBCQUFlLENBQUMsQ0FBQztJQUNqRCxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQztBQUNuQyxDQUFDLENBQUMifQ==

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var settings_model_1 = __webpack_require__(10);
var SettingsService = /** @class */ (function () {
    function SettingsService(swApi, haconstants, $log, _t) {
        this.swApi = swApi;
        this.haconstants = haconstants;
        this.$log = $log;
        this._t = _t;
        this._settingsOrder = [
            haconstants.settingsEnableHighAvailability,
            haconstants.settingsMemberDownInterval,
            haconstants.settingsIntervalTakeoverRetention,
            haconstants.settingsEmailMemberStatusChanged,
            haconstants.settingsEmailFacilityStatusChanged,
            haconstants.settingsEmailResourceStatusChanged
        ];
    }
    Object.defineProperty(SettingsService.prototype, "settingsOrder", {
        get: function () {
            return this._settingsOrder.slice();
        },
        enumerable: true,
        configurable: true
    });
    SettingsService.prototype.getSettings = function () {
        var _this = this;
        var settings = new Array();
        return this.swApi.api(false).one("ha/settings/get").get()
            .then(function (response) {
            response.forEach(function (item, index) {
                var idx = _.indexOf(_this._settingsOrder, item.settingId);
                if (idx >= 0) {
                    settings[idx] = new settings_model_1.default(item, _this._t);
                }
                else {
                    _this.$log.error("Setting '" + item.settingId + "' is not supported.");
                }
            });
            return settings.filter(function (s) { return s !== undefined; });
        }).catch(function (error) {
            _this.$log.error("Error in getSettings:");
            _this.$log.error(error);
        });
    };
    SettingsService.prototype.updateSettings = function (settingsModels) {
        var _this = this;
        return this.swApi.api(false).one("ha/settings").post("updatevalues", settingsModels)
            .then(function (response) {
            return response;
        }).catch(function (error) {
            _this.$log.error("Error in updateSettings:");
            _this.$log.error(error);
        });
    };
    SettingsService.prototype.isFipsEnabled = function () {
        var _this = this;
        return this.swApi.api(false).one("ha/settings/isFipsEnabled").get()
            .then(function (response) {
            return response;
        }).catch(function (error) {
            _this.$log.error("Error in isFipsEnabled:");
            _this.$log.error(error);
        });
    };
    SettingsService.$inject = ["swApi", "haconstants", "$log", "getTextService"];
    return SettingsService;
}());
exports.default = SettingsService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3Mtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNldHRpbmdzLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSx1RUFBaUU7QUFFakU7SUFLSSx5QkFBb0IsS0FBVSxFQUFVLFdBQXNCLEVBQ2xELElBQW9CLEVBQVksRUFBb0M7UUFENUQsVUFBSyxHQUFMLEtBQUssQ0FBSztRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFXO1FBQ2xELFNBQUksR0FBSixJQUFJLENBQWdCO1FBQVksT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDNUUsSUFBSSxDQUFDLGNBQWMsR0FBRztZQUNsQixXQUFXLENBQUMsOEJBQThCO1lBQzFDLFdBQVcsQ0FBQywwQkFBMEI7WUFDdEMsV0FBVyxDQUFDLGlDQUFpQztZQUM3QyxXQUFXLENBQUMsZ0NBQWdDO1lBQzVDLFdBQVcsQ0FBQyxrQ0FBa0M7WUFDOUMsV0FBVyxDQUFDLGtDQUFrQztTQUNqRCxDQUFDO0lBQ04sQ0FBQztJQUVELHNCQUFJLDBDQUFhO2FBQWpCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUFFTSxxQ0FBVyxHQUFsQjtRQUFBLGlCQWtCQztRQWpCRyxJQUFJLFFBQVEsR0FBRyxJQUFJLEtBQUssRUFBaUIsQ0FBQztRQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsR0FBRyxFQUFFO2FBQ3BELElBQUksQ0FBQyxVQUFDLFFBQThCO1lBQ2pDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFtQixFQUFFLEtBQWE7Z0JBQ2hELElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBRXpELEVBQUUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNYLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLHdCQUFhLENBQUMsSUFBSSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDckQsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFZLElBQUksQ0FBQyxTQUFTLHdCQUFxQixDQUFDLENBQUM7Z0JBQ3JFLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLFNBQVMsRUFBZixDQUFlLENBQUMsQ0FBQztRQUNqRCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxLQUFVO1lBQ2hCLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDekMsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0sd0NBQWMsR0FBckIsVUFBc0IsY0FBb0M7UUFBMUQsaUJBUUM7UUFQRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsY0FBYyxDQUFDO2FBQy9FLElBQUksQ0FBQyxVQUFDLFFBQWlCO1lBQ3BCLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsS0FBVTtZQUNoQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1lBQzVDLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLHVDQUFhLEdBQXBCO1FBQUEsaUJBUUM7UUFQRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLENBQUMsR0FBRyxFQUFFO2FBQzlELElBQUksQ0FBQyxVQUFDLFFBQWlCO1lBQ3BCLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsS0FBVTtZQUNoQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQzNDLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQTFEYSx1QkFBTyxHQUFHLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQTJEL0Usc0JBQUM7Q0FBQSxBQTVERCxJQTREQztrQkE1RG9CLGVBQWUifQ==

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SettingsModel = /** @class */ (function () {
    function SettingsModel(model, _t) {
        this._t = _t;
        if (model) {
            _.extend(this, model);
        }
        this.type = "string";
        if (this.units) {
            if (this.units.toLocaleLowerCase().indexOf("bool") === 0) {
                this.type = "boolean";
                this.currentValue = !!this.currentValue;
                this.defaultValue = !!this.defaultValue;
            }
            else if (this.units.toLocaleLowerCase() === "ms" || this.units.toLocaleLowerCase() === "s"
                || this.units.toLocaleLowerCase() === "minutes") {
                switch (this.units.toLocaleLowerCase()) {
                    case "ms":
                        this.unitsDisplayValue = this._t("milliseconds");
                        break;
                    case "s":
                        this.unitsDisplayValue = this._t("seconds");
                        break;
                    case "minutes":
                        this.unitsDisplayValue = this._t("minutes");
                        break;
                }
                this.type = "number";
                this.currentValue = +this.currentValue;
                this.defaultValue = +this.defaultValue;
            }
        }
    }
    return SettingsModel;
}());
exports.default = SettingsModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MtbW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZXR0aW5ncy1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QztJQWFJLHVCQUFZLEtBQW9CLEVBQVUsRUFBb0M7UUFBcEMsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDMUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNSLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFCLENBQUM7UUFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQztRQUVyQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNiLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkQsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDNUMsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixFQUFFLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxHQUFHO21CQUNyRixJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixFQUFFLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFFbEQsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDckMsS0FBSyxJQUFJO3dCQUNMLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUNqRCxLQUFLLENBQUM7b0JBQ1YsS0FBSyxHQUFHO3dCQUNKLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUM1QyxLQUFLLENBQUM7b0JBQ1YsS0FBSyxTQUFTO3dCQUNWLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUM1QyxLQUFLLENBQUM7Z0JBQ2QsQ0FBQztnQkFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQztnQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQzNDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUNMLG9CQUFDO0FBQUQsQ0FBQyxBQTlDRCxJQThDQyJ9

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var summary_service_1 = __webpack_require__(12);
var summary_service_demo_mock_1 = __webpack_require__(28);
exports.default = function (module) {
    module.factory("summaryService", function (constants, $injector) {
        return constants.environment.demoMode
            ? $injector.instantiate(summary_service_demo_mock_1.default)
            : $injector.instantiate(summary_service_1.default);
    }, ["constants", "$injector"]);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFEQUErQztBQUMvQyx5RUFBNkQ7QUFFN0Qsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsVUFBQyxTQUFjLEVBQUUsU0FBYztRQUM1RCxNQUFNLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRO1lBQ2pDLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLG1DQUFrQixDQUFDO1lBQzNDLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLHlCQUFjLENBQUMsQ0FBQztJQUNoRCxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQztBQUNuQyxDQUFDLENBQUMifQ==

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var orionServer_model_1 = __webpack_require__(4);
var pool_model_1 = __webpack_require__(2);
var SummaryService = /** @class */ (function () {
    function SummaryService($q, swApi, haconstants) {
        this.$q = $q;
        this.swApi = swApi;
        this.haconstants = haconstants;
    }
    SummaryService.prototype.isAvailableLicenseToCreateHAPool = function () {
        return this.swApi.api(false).one("ha/deploymentsummary/isavailablelicensetocreatehapool").get()
            .then(function (response) {
            return response;
        });
    };
    SummaryService.prototype.canActivateHAEvaluation = function () {
        return this.swApi.api(false).one("ha/deploymentsummary/canactivatehaevaluation").get()
            .then(function (response) {
            return response;
        });
    };
    SummaryService.prototype.activateEvaluationForHA = function () {
        return this.swApi.api(false).one("ha/deploymentsummary/activateevaluationforha").post()
            .then(function (response) {
            return response;
        });
    };
    SummaryService.prototype.getPools = function () {
        var pools = new Array();
        return this.swApi.api(false).one("ha/deploymentsummary/getpools").get()
            .then(function (response) {
            response.forEach(function (item, index) {
                pools.push(new pool_model_1.default(item));
            });
            return pools;
        });
    };
    SummaryService.prototype.getListOfAPENotInPool = function (memberId) {
        var orionServers = new Array();
        return this.swApi.api(false).one("ha/deploymentsummary/listOfAPENotInPool/" + memberId).get()
            .then(function (response) {
            response.forEach(function (item, index) {
                orionServers.push(new orionServer_model_1.default(item));
            });
            return orionServers;
        });
    };
    SummaryService.prototype.createHaPool = function (poolModel) {
        return this.swApi.api(false).one("ha/deploymentsummary").post("createhapool", poolModel)
            .then(function (response) {
            return response;
        }).catch(function (e) {
            return e.data;
        });
    };
    SummaryService.prototype.deleteHaPool = function (poolId) {
        return this.swApi.api(false).one("ha/deploymentsummary").post("deletehapool", poolId)
            .then(function (response) {
            return response;
        });
    };
    SummaryService.prototype.deleteStaleEngine = function (hostName) {
        return this.swApi.api(false).one("ha/deploymentsummary").post("deletestaleengine", JSON.stringify(hostName))
            .then(function (response) {
            return response;
        });
    };
    SummaryService.prototype.updateHaPoolState = function (poolId, enabled) {
        var apiCall = (enabled ? "enablehapool/" : "disablehapool/") + poolId;
        return this.swApi.api(false).one("ha/deploymentsummary").post(apiCall)
            .then(function (response) {
            return response;
        });
    };
    SummaryService.prototype.switchoverHaPool = function (poolId) {
        return this.swApi.api(false).one("ha/deploymentsummary").post("switchoverhapool", poolId)
            .then(function (response) {
            return response;
        });
    };
    SummaryService.prototype.getReportUrl = function () {
        return this.swApi.api(false).one("ha/deploymentsummary")
            .post("getreporturl", this.haconstants.summary.haSyslogAndTrapsReportName)
            .then(function (response) {
            return response;
        }).catch(function (e) {
            return e.data.message;
        });
    };
    SummaryService.prototype.getOrionServers = function () {
        var orionServers = new Array();
        return this.swApi.api(false).one("ha/deploymentsummary/orionservers").get()
            .then(function (response) {
            response.forEach(function (item, index) {
                var server = new orionServer_model_1.default(item);
                orionServers.push(server);
            });
            return orionServers;
        });
    };
    SummaryService.prototype.getPoolType = function (servers) {
        return this.swApi.api(false).one("ha/deploymentsummary").post("getpooltype", servers);
    };
    SummaryService.$inject = ["$q", "swApi", "haconstants"];
    return SummaryService;
}());
exports.default = SummaryService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VtbWFyeS1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3VtbWFyeS1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsMkVBQXFFO0FBQ3JFLDJEQUFxRDtBQU1yRDtJQUdJLHdCQUNjLEVBQWdCLEVBQ2xCLEtBQVUsRUFDVixXQUFzQjtRQUZwQixPQUFFLEdBQUYsRUFBRSxDQUFjO1FBQ2xCLFVBQUssR0FBTCxLQUFLLENBQUs7UUFDVixnQkFBVyxHQUFYLFdBQVcsQ0FBVztJQUNsQyxDQUFDO0lBRU0seURBQWdDLEdBQXZDO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyx1REFBdUQsQ0FBQyxDQUFDLEdBQUcsRUFBRTthQUMxRixJQUFJLENBQUMsVUFBQyxRQUFzRDtZQUN6RCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLGdEQUF1QixHQUE5QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsOENBQThDLENBQUMsQ0FBQyxHQUFHLEVBQUU7YUFDakYsSUFBSSxDQUFDLFVBQUMsUUFBb0M7WUFDdkMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSxnREFBdUIsR0FBOUI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLDhDQUE4QyxDQUFDLENBQUMsSUFBSSxFQUFFO2FBQ2xGLElBQUksQ0FBQyxVQUFDLFFBQW9DO1lBQ3ZDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0saUNBQVEsR0FBZjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksS0FBSyxFQUFhLENBQUM7UUFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLEdBQUcsRUFBRTthQUNsRSxJQUFJLENBQUMsVUFBQyxRQUEwQjtZQUM3QixRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBZSxFQUFFLEtBQWE7Z0JBQzVDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxvQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLDhDQUFxQixHQUE1QixVQUE2QixRQUFnQjtRQUN6QyxJQUFJLFlBQVksR0FBRyxJQUFJLEtBQUssRUFBb0IsQ0FBQztRQUNqRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLDBDQUEwQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLEdBQUcsRUFBRTthQUN4RixJQUFJLENBQUMsVUFBQyxRQUFpQztZQUNwQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBc0IsRUFBRSxLQUFhO2dCQUNuRCxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksMkJBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNsRCxDQUFDLENBQUMsQ0FBQztZQUNILE1BQU0sQ0FBQyxZQUFZLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0scUNBQVksR0FBbkIsVUFBb0IsU0FBb0I7UUFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsU0FBUyxDQUFDO2FBQ25GLElBQUksQ0FBQyxVQUFDLFFBQW1DO1lBQ3RDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBTTtZQUNaLE1BQU0sQ0FBNEIsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSxxQ0FBWSxHQUFuQixVQUFvQixNQUFjO1FBQzlCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLE1BQU0sQ0FBQzthQUNoRixJQUFJLENBQUMsVUFBQyxRQUFpQztZQUNwQyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLDBDQUFpQixHQUF4QixVQUF5QixRQUFnQjtRQUNyQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDdkcsSUFBSSxDQUFDLFVBQUMsUUFBaUM7WUFDcEMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSwwQ0FBaUIsR0FBeEIsVUFBeUIsTUFBYyxFQUFFLE9BQWdCO1FBQ3JELElBQUksT0FBTyxHQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQzlFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ2pFLElBQUksQ0FBQyxVQUFDLFFBQWlDO1lBQ3BDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0seUNBQWdCLEdBQXZCLFVBQXdCLE1BQWM7UUFDbEMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxNQUFNLENBQUM7YUFDcEYsSUFBSSxDQUFDLFVBQUMsUUFBaUM7WUFDcEMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSxxQ0FBWSxHQUFuQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUM7YUFDbkQsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQzthQUN6RSxJQUFJLENBQUMsVUFBQyxRQUFnQjtZQUNuQixNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQU07WUFDWixNQUFNLENBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0sd0NBQWUsR0FBdEI7UUFDSSxJQUFJLFlBQVksR0FBRyxJQUFJLEtBQUssRUFBb0IsQ0FBQztRQUNqRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsR0FBRyxFQUFFO2FBQ3RFLElBQUksQ0FBQyxVQUFDLFFBQWlDO1lBQ3BDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFzQixFQUFFLEtBQWE7Z0JBQ25ELElBQUksTUFBTSxHQUFHLElBQUksMkJBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3hDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsWUFBWSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLG9DQUFXLEdBQWxCLFVBQW1CLE9BQWdDO1FBQy9DLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQzFGLENBQUM7SUFqSGEsc0JBQU8sR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFrSDNELHFCQUFDO0NBQUEsQUFuSEQsSUFtSEM7a0JBbkhvQixjQUFjIn0=

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var SummaryItemProperty = /** @class */ (function () {
    function SummaryItemProperty() {
    }
    return SummaryItemProperty;
}());
exports.default = SummaryItemProperty;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VtYXJ5LWl0ZW0tcHJvcGVydHkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdW1hcnktaXRlbS1wcm9wZXJ0eS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsdUNBQXVDOztBQUV2QztJQUFBO0lBR0EsQ0FBQztJQUFELDBCQUFDO0FBQUQsQ0FBQyxBQUhELElBR0MifQ==

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var NavigationService = /** @class */ (function () {
    function NavigationService($state, $window, haconstants) {
        this.$state = $state;
        this.$window = $window;
        this.haconstants = haconstants;
    }
    NavigationService.prototype.navigateToSummaryPage = function (params) {
        if (params === void 0) { params = null; }
        this.$state.go("summary", params);
    };
    ;
    NavigationService.prototype.navigateToAddPoolPage = function (params) {
        if (params === void 0) { params = null; }
        this.$state.go("pool", params);
    };
    ;
    NavigationService.prototype.navigateToHAReportPage = function (reportPath) {
        if (reportPath === void 0) { reportPath = null; }
        this.$window.open(reportPath, "_blank");
    };
    ;
    NavigationService.prototype.navigateToBuyNowPage = function () {
        this.$window.open(this.haconstants.links.buyNow, "_blank");
    };
    ;
    NavigationService.prototype.navigateToEmailSalesPage = function () {
        this.$window.open(this.haconstants.links.solarWindsContact, "_blank");
    };
    ;
    NavigationService.prototype.downloadSolarWindsOrionInstaller = function () {
        this.$window.open(this.haconstants.links.solarWindsOrionInstaller, "_blank");
    };
    ;
    NavigationService.prototype.navigateToSolarWindsOrionAdminAlertSettings = function () {
        this.$window.open(this.haconstants.links.solarWindsOrionAdminAlertSettings, "_self");
    };
    ;
    NavigationService.$inject = ["$state", "$window", "haconstants"];
    return NavigationService;
}());
exports.default = NavigationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibmF2aWdhdGlvbi1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0E7SUFHSSwyQkFBb0IsTUFBMkIsRUFBVSxPQUEwQixFQUN2RSxXQUFzQjtRQURkLFdBQU0sR0FBTixNQUFNLENBQXFCO1FBQVUsWUFBTyxHQUFQLE9BQU8sQ0FBbUI7UUFDdkUsZ0JBQVcsR0FBWCxXQUFXLENBQVc7SUFDbEMsQ0FBQztJQUVNLGlEQUFxQixHQUE1QixVQUE2QixNQUFrQjtRQUFsQix1QkFBQSxFQUFBLGFBQWtCO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBQUEsQ0FBQztJQUVLLGlEQUFxQixHQUE1QixVQUE2QixNQUFrQjtRQUFsQix1QkFBQSxFQUFBLGFBQWtCO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBQUEsQ0FBQztJQUVLLGtEQUFzQixHQUE3QixVQUE4QixVQUF5QjtRQUF6QiwyQkFBQSxFQUFBLGlCQUF5QjtRQUNuRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUFBLENBQUM7SUFFSyxnREFBb0IsR0FBM0I7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUFBLENBQUM7SUFFSyxvREFBd0IsR0FBL0I7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBQUEsQ0FBQztJQUVLLDREQUFnQyxHQUF2QztRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLHdCQUF3QixFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7SUFBQSxDQUFDO0lBRUssdUVBQTJDLEdBQWxEO1FBQ0ksSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUFBLENBQUM7SUFoQ1kseUJBQU8sR0FBRyxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFpQ2pFLHdCQUFDO0NBQUEsQUFsQ0QsSUFrQ0M7a0JBbENvQixpQkFBaUIifQ==

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PoolService = /** @class */ (function () {
    function PoolService(swApi) {
        this.swApi = swApi;
    }
    PoolService.prototype.areMembersFromSameSubnet = function (poolMembersIds) {
        return this.swApi.api(false).one("ha/AddPool").post("areMembersFromSameSubnet", poolMembersIds)
            .then(function (response) {
            return response;
        });
    };
    PoolService.prototype.validatePool = function (poolModel) {
        return this.swApi.api(false).one("ha/AddPool").post("validatePool", poolModel)
            .then(function (response) {
            return response;
        });
    };
    PoolService.prototype.ensurePool = function (poolModel) {
        return this.swApi.api(false).one("ha/AddPool").post("ensurePool", poolModel)
            .then(function (response) {
            return response;
        });
    };
    PoolService.$inject = ["swApi"];
    return PoolService;
}());
exports.default = PoolService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbC1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9vbC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBSUE7SUFHSSxxQkFBb0IsS0FBVTtRQUFWLFVBQUssR0FBTCxLQUFLLENBQUs7SUFDOUIsQ0FBQztJQUVNLDhDQUF3QixHQUEvQixVQUFnQyxjQUF3QjtRQUNwRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRSxjQUFjLENBQUM7YUFDMUYsSUFBSSxDQUFDLFVBQUMsUUFBaUI7WUFDcEIsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSxrQ0FBWSxHQUFuQixVQUFvQixTQUFvQjtRQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsU0FBUyxDQUFDO2FBQ3pFLElBQUksQ0FBQyxVQUFDLFFBQTBDO1lBQzdDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU0sZ0NBQVUsR0FBakIsVUFBa0IsU0FBb0I7UUFDbEMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQzthQUN2RSxJQUFJLENBQUMsVUFBQyxRQUFtQztZQUN0QyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQXhCYSxtQkFBTyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7SUF5QnRDLGtCQUFDO0NBQUEsQUExQkQsSUEwQkM7a0JBMUJvQixXQUFXIn0=

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PoolOperationEnum;
(function (PoolOperationEnum) {
    PoolOperationEnum[PoolOperationEnum["Create"] = 0] = "Create";
    PoolOperationEnum[PoolOperationEnum["Edit"] = 1] = "Edit";
})(PoolOperationEnum = exports.PoolOperationEnum || (exports.PoolOperationEnum = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbE9wZXJhdGlvbi1lbnVtLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9vbE9wZXJhdGlvbi1lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBWSxpQkFHWDtBQUhELFdBQVksaUJBQWlCO0lBQ3pCLDZEQUFNLENBQUE7SUFDTix5REFBSSxDQUFBO0FBQ1IsQ0FBQyxFQUhXLGlCQUFpQixHQUFqQix5QkFBaUIsS0FBakIseUJBQWlCLFFBRzVCIn0=

/***/ }),
/* 17 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 18 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 19 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(21);
module.exports = __webpack_require__(22);


/***/ }),
/* 21 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(23);
var config_1 = __webpack_require__(24);
var index_1 = __webpack_require__(25);
var index_2 = __webpack_require__(36);
var index_3 = __webpack_require__(89);
var index_4 = __webpack_require__(90);
var index_5 = __webpack_require__(92);
index_3.default(app_1.default);
index_2.default(app_1.default);
config_1.default(app_1.default);
index_1.default(app_1.default);
index_4.default(app_1.default);
index_5.default(app_1.default);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDZCQUF1QjtBQUN2QixtQ0FBOEI7QUFDOUIsMENBQXdDO0FBQ3hDLHVDQUFrQztBQUNsQyx3Q0FBb0M7QUFDcEMseUNBQXNDO0FBQ3RDLDRDQUE0QztBQUU1QyxlQUFNLENBQUMsYUFBRSxDQUFDLENBQUM7QUFDWCxlQUFLLENBQUMsYUFBRSxDQUFDLENBQUM7QUFDVixnQkFBTSxDQUFDLGFBQUUsQ0FBQyxDQUFDO0FBQ1gsZUFBUSxDQUFDLGFBQUUsQ0FBQyxDQUFDO0FBQ2IsZUFBTyxDQUFDLGFBQUUsQ0FBQyxDQUFDO0FBQ1osZUFBVSxDQUFDLGFBQUUsQ0FBQyxDQUFDIn0=

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
angular.module("ha.services", []);
angular.module("ha.components", []);
angular.module("ha.filters", []);
angular.module("ha.providers", []);
angular.module("ha", [
    "orion",
    "orion-ui-components",
    "ha.services",
    "ha.components",
    "ha.filters",
    "ha.providers"
]);
var ha = Xui.registerModule("ha");
exports.default = ha;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxpQ0FBaUM7O0FBRWpDLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ2pDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ25DLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ2hDLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ2xDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO0lBQ2IsT0FBTztJQUNQLHFCQUFxQjtJQUNyQixhQUFhO0lBQ2IsZUFBZTtJQUNmLFlBQVk7SUFDWixjQUFjO0NBQ3JCLENBQUMsQ0FBQztBQUVILElBQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDcEMsa0JBQWUsRUFBRSxDQUFDIn0=

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
/**
 * Configuration file for repository module
 **/
run.$inject = ["$log", "$rootScope", "$state"];
config.$inject = ["$stateProvider"];
Object.defineProperty(exports, "__esModule", { value: true });
/** @ngInject */
function run($log, $rootScope, $state) {
    $log.info("Run module: HA");
    $rootScope.$on("$stateChangeStart", function (evt, to, params) {
        if (to.redirectTo) {
            evt.preventDefault();
            $state.go(to.redirectTo, params, { location: "replace" });
        }
    });
}
/** @ngInject */
function config($stateProvider) {
    $stateProvider.state("ha", {
        abstract: true,
        url: "/ha",
        template: "<div ui-view></div>"
    });
}
// following injection line is not necessary, because of using @ngInject comment above
// run.$inject = ["$log"];
exports.default = function (module) {
    module.app()
        .run(run)
        .config(config);
};
exports.default.$inject = ["module"];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxpQ0FBaUM7QUFDakM7O0lBRUk7O0FBS0osZ0JBQWdCO0FBQ2hCLGFBQWEsSUFBb0IsRUFBRSxVQUFnQyxFQUFFLE1BQTJCO0lBQzVGLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUU1QixVQUFVLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLFVBQUMsR0FBcUIsRUFBRSxFQUFPLEVBQUUsTUFBVztRQUM1RSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNoQixHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDckIsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQzlELENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFFRCxnQkFBZ0I7QUFDaEIsZ0JBQWdCLGNBQXlDO0lBQ3JELGNBQWMsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO1FBQ3ZCLFFBQVEsRUFBRSxJQUFJO1FBQ2QsR0FBRyxFQUFFLEtBQUs7UUFDVixRQUFRLEVBQUUscUJBQXFCO0tBQ2xDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFFRCxzRkFBc0Y7QUFDdEYsMEJBQTBCO0FBRTFCLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsR0FBRyxFQUFFO1NBQ1AsR0FBRyxDQUFDLEdBQUcsQ0FBQztTQUNSLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUN4QixDQUFDLENBQUMifQ==

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var settings_1 = __webpack_require__(8);
var settings_2 = __webpack_require__(8);
var summary_1 = __webpack_require__(11);
var summary_2 = __webpack_require__(11);
var navigation_1 = __webpack_require__(29);
var demo_1 = __webpack_require__(31);
var pool_1 = __webpack_require__(34);
exports.default = function (module) {
    settings_1.default(module);
    settings_2.default(module);
    summary_1.default(module);
    summary_2.default(module);
    navigation_1.default(module);
    demo_1.default(module);
    pool_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVDQUF5QztBQUN6Qyx1Q0FBNkM7QUFDN0MscUNBQXVDO0FBQ3ZDLHFDQUEyQztBQUMzQywyQ0FBNkM7QUFDN0MsK0JBQWlDO0FBQ2pDLCtCQUFpQztBQUVqQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0Isa0JBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN4QixrQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM1QixpQkFBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZCLGlCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLG9CQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFCLGNBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQixjQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDeEIsQ0FBQyxDQUFDIn0=

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var settings_service_1 = __webpack_require__(9);
var settings_model_1 = __webpack_require__(10);
var SettingsServiceDemoMock = /** @class */ (function (_super) {
    __extends(SettingsServiceDemoMock, _super);
    function SettingsServiceDemoMock($q, demoService, swApi, haconstants, $log, _t) {
        var _this = _super.call(this, swApi, haconstants, $log, _t) || this;
        _this.$q = $q;
        _this.demoService = demoService;
        return _this;
    }
    SettingsServiceDemoMock.prototype.getSettings = function () {
        var _this = this;
        return this.$q(function (resolve) {
            resolve([
                new settings_model_1.default({
                    settingId: "HA-EnableHighAvailability",
                    name: "Enable High Availability",
                    units: "boolean",
                    currentValue: 1,
                    defaultValue: 1,
                    minimum: 0,
                    maximum: 1,
                    hint: ""
                }, _this._t),
                new settings_model_1.default({
                    settingId: "HA-DefaultPoolIntervalMemberDown",
                    name: "Default interval to consider a member as down in a pool",
                    units: "s",
                    currentValue: 32,
                    defaultValue: 32,
                    minimum: 15,
                    maximum: 60,
                    hint: ""
                }, _this._t),
                new settings_model_1.default({
                    settingId: "HA-IntervalTakeoverRetention",
                    /* tslint:disable:max-line-length */
                    name: "Keep the secondary server disabled for a period of time after a failover event to prevent continuous failover",
                    /* tslint:enable:max-line-length */
                    units: "minutes",
                    currentValue: 10,
                    defaultValue: 10,
                    minimum: 0,
                    maximum: 30,
                    hint: ""
                }, _this._t),
                new settings_model_1.default({
                    settingId: "HA-EmailMeOnServerStatusChange",
                    name: "Email me when server status is changed",
                    units: "boolean",
                    currentValue: 1,
                    defaultValue: 1,
                    minimum: 0,
                    maximum: 1,
                    hint: ""
                }, _this._t),
                new settings_model_1.default({
                    settingId: "HA-EmailMeOnFacilityStatusChange",
                    name: "Email me when facility status is changed (e.g. MSMQ)",
                    units: "boolean",
                    currentValue: 0,
                    defaultValue: 0,
                    minimum: 0,
                    maximum: 1,
                    hint: ""
                }, _this._t),
                new settings_model_1.default({
                    settingId: "HA-EmailMeOnResourceStatusChange",
                    name: "Email me when resource status is changed (e.g. Polling)",
                    units: "boolean",
                    currentValue: 0,
                    defaultValue: 0,
                    minimum: 0,
                    maximum: 1,
                    hint: ""
                }, _this._t)
            ]);
        });
    };
    SettingsServiceDemoMock.prototype.updateSettings = function (settingsModels) {
        this.demoService.showDemoToast();
        var defered = this.$q.defer();
        return defered.promise;
    };
    SettingsServiceDemoMock.prototype.isFipsEnabled = function () {
        return this.$q(function (resolve) {
            resolve(false);
        });
    };
    SettingsServiceDemoMock.$inject = ["$q", "demoService", "swApi", "haconstants", "$log", "getTextService"];
    return SettingsServiceDemoMock;
}(settings_service_1.default));
exports.default = SettingsServiceDemoMock;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3Mtc2VydmljZS1kZW1vLW1vY2suanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZXR0aW5ncy1zZXJ2aWNlLWRlbW8tbW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBdUM7QUFDdkMsdURBQWlEO0FBQ2pELHVFQUFpRTtBQUlqRTtJQUFxRCwyQ0FBZTtJQUdoRSxpQ0FBb0IsRUFBZ0IsRUFDeEIsV0FBd0IsRUFDaEMsS0FBVSxFQUNWLFdBQXNCLEVBQ3RCLElBQW9CLEVBQ3BCLEVBQW9DO1FBTHhDLFlBTUksa0JBQU0sS0FBSyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFNBQ3RDO1FBUG1CLFFBQUUsR0FBRixFQUFFLENBQWM7UUFDeEIsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBTXBDLENBQUM7SUFFTSw2Q0FBVyxHQUFsQjtRQUFBLGlCQXFFQztRQXBFRyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLE9BQU87WUFDbkIsT0FBTyxDQUNIO2dCQUNJLElBQUksd0JBQWEsQ0FBZ0I7b0JBQzdCLFNBQVMsRUFBRSwyQkFBMkI7b0JBQ3RDLElBQUksRUFBRSwwQkFBMEI7b0JBQ2hDLEtBQUssRUFBRSxTQUFTO29CQUNoQixZQUFZLEVBQUUsQ0FBQztvQkFDZixZQUFZLEVBQUUsQ0FBQztvQkFDZixPQUFPLEVBQUUsQ0FBQztvQkFDVixPQUFPLEVBQUUsQ0FBQztvQkFDVixJQUFJLEVBQUUsRUFBRTtpQkFDWCxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsSUFBSSx3QkFBYSxDQUFnQjtvQkFDN0IsU0FBUyxFQUFFLGtDQUFrQztvQkFDN0MsSUFBSSxFQUFFLHlEQUF5RDtvQkFDL0QsS0FBSyxFQUFFLEdBQUc7b0JBQ1YsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLFlBQVksRUFBRSxFQUFFO29CQUNoQixPQUFPLEVBQUUsRUFBRTtvQkFDWCxPQUFPLEVBQUUsRUFBRTtvQkFDWCxJQUFJLEVBQUUsRUFBRTtpQkFDWCxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsSUFBSSx3QkFBYSxDQUFnQjtvQkFDN0IsU0FBUyxFQUFFLDhCQUE4QjtvQkFDekMsb0NBQW9DO29CQUNwQyxJQUFJLEVBQUUsK0dBQStHO29CQUNySCxtQ0FBbUM7b0JBQ25DLEtBQUssRUFBRSxTQUFTO29CQUNoQixZQUFZLEVBQUUsRUFBRTtvQkFDaEIsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLE9BQU8sRUFBRSxDQUFDO29CQUNWLE9BQU8sRUFBRSxFQUFFO29CQUNYLElBQUksRUFBRSxFQUFFO2lCQUNYLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQztnQkFDWCxJQUFJLHdCQUFhLENBQWdCO29CQUM3QixTQUFTLEVBQUUsZ0NBQWdDO29CQUMzQyxJQUFJLEVBQUUsd0NBQXdDO29CQUM5QyxLQUFLLEVBQUUsU0FBUztvQkFDaEIsWUFBWSxFQUFFLENBQUM7b0JBQ2YsWUFBWSxFQUFFLENBQUM7b0JBQ2YsT0FBTyxFQUFFLENBQUM7b0JBQ1YsT0FBTyxFQUFFLENBQUM7b0JBQ1YsSUFBSSxFQUFFLEVBQUU7aUJBQ1gsRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNYLElBQUksd0JBQWEsQ0FBZ0I7b0JBQzdCLFNBQVMsRUFBRSxrQ0FBa0M7b0JBQzdDLElBQUksRUFBRSxzREFBc0Q7b0JBQzVELEtBQUssRUFBRSxTQUFTO29CQUNoQixZQUFZLEVBQUUsQ0FBQztvQkFDZixZQUFZLEVBQUUsQ0FBQztvQkFDZixPQUFPLEVBQUUsQ0FBQztvQkFDVixPQUFPLEVBQUUsQ0FBQztvQkFDVixJQUFJLEVBQUUsRUFBRTtpQkFDWCxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsSUFBSSx3QkFBYSxDQUFnQjtvQkFDN0IsU0FBUyxFQUFFLGtDQUFrQztvQkFDN0MsSUFBSSxFQUFFLHlEQUF5RDtvQkFDL0QsS0FBSyxFQUFFLFNBQVM7b0JBQ2hCLFlBQVksRUFBRSxDQUFDO29CQUNmLFlBQVksRUFBRSxDQUFDO29CQUNmLE9BQU8sRUFBRSxDQUFDO29CQUNWLE9BQU8sRUFBRSxDQUFDO29CQUNWLElBQUksRUFBRSxFQUFFO2lCQUNYLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQzthQUNkLENBQ0osQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLGdEQUFjLEdBQXJCLFVBQXNCLGNBQW9DO1FBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDakMsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQStCLENBQUM7SUFDbkQsQ0FBQztJQUVNLCtDQUFhLEdBQXBCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBQyxPQUFPO1lBQ25CLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUE1RmEsK0JBQU8sR0FBRyxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQTZGcEcsOEJBQUM7Q0FBQSxBQTlGRCxDQUFxRCwwQkFBZSxHQThGbkU7a0JBOUZvQix1QkFBdUIifQ==

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var OrionServerProperty = /** @class */ (function () {
    function OrionServerProperty() {
    }
    return OrionServerProperty;
}());
exports.default = OrionServerProperty;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Jpb25TZXJ2ZXJQcm9wZXJ0eS1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9yaW9uU2VydmVyUHJvcGVydHktbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7QUFFdkM7SUFBQTtJQUlBLENBQUM7SUFBRCwwQkFBQztBQUFELENBQUMsQUFKRCxJQUlDIn0=

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var orionServer_model_1 = __webpack_require__(4);
var pool_model_1 = __webpack_require__(2);
var summary_service_1 = __webpack_require__(12);
var orionServerPollingEngine_enum_1 = __webpack_require__(1);
var SummaryServiceDemoMock = /** @class */ (function (_super) {
    __extends(SummaryServiceDemoMock, _super);
    function SummaryServiceDemoMock($q, swApi, haconstants, demoService) {
        var _this = _super.call(this, $q, swApi, haconstants) || this;
        _this.demoService = demoService;
        return _this;
    }
    SummaryServiceDemoMock.prototype.isAvailableLicenseToCreateHAPool = function () {
        return this.$q(function (resolve) {
            return resolve({
                errorCode: null,
                errorMessage: null,
                result: {
                    poolLicenseIsAvailable: true,
                    isLicenseStoreAvailable: true,
                    evaluationExpired: null,
                    activatedCommercial: null,
                    mainEnginePollerName: null
                },
                exceptionMessage: null,
                message: null,
                status: null
            });
        });
    };
    SummaryServiceDemoMock.prototype.getListOfAPENotInPool = function (memberId) {
        return this.$q(function (resolve) { return resolve([
            {
                displayName: "Additional Poller",
                name: "Additional Poller",
                ipAddress: "10.0.100.3",
                icon: "server",
                description: "Active",
                pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPoller,
                poolId: 0,
                poolMemberId: 3,
                statusDescription: "",
                children: [],
                itemType: "orionServer",
                iconStatus: "up",
                status: 1,
                runType: 1
            },
            {
                displayName: "Additional Poller 2",
                name: "Additional Poller 2",
                ipAddress: "10.0.200.3",
                icon: "server",
                description: "Active",
                pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPoller,
                poolId: 0,
                poolMemberId: 4,
                statusDescription: "",
                children: [],
                itemType: "orionServer",
                iconStatus: "up",
                status: 1,
                runType: 1
            }
        ]); });
    };
    SummaryServiceDemoMock.prototype.deleteHaPool = function (poolId) {
        this.demoService.showDemoToast();
        return this.demoService.createActionResultPromise();
    };
    SummaryServiceDemoMock.prototype.updateHaPoolState = function (poolId, enabled) {
        this.demoService.showDemoToast();
        return this.demoService.createActionResultPromise();
    };
    SummaryServiceDemoMock.prototype.switchoverHaPool = function (poolId) {
        this.demoService.showDemoToast();
        return this.demoService.createActionResultPromise();
    };
    SummaryServiceDemoMock.prototype.getPoolType = function (servers) {
        return this.$q(function (resolve) {
            return resolve({
                errorCode: 0,
                errorMessage: "",
                result: "AdditionalPoller",
                status: null,
                message: null,
                exceptionMessage: null
            });
        });
    };
    SummaryServiceDemoMock.prototype.getPools = function () {
        return this.$q(function (resolve) {
            resolve([
                new pool_model_1.default({
                    displayName: "Main Pool",
                    description: "",
                    statusDescription: "Status is Fully Operational",
                    enabled: true,
                    isHighAvailabilityEnabled: true,
                    poolId: 1,
                    poolType: 0,
                    icon: "orion-sitemaster-pool",
                    iconStatus: "up",
                    status: 1,
                    itemType: "pool",
                    isLicensed: true,
                    isLicenseStoreAvailable: true,
                    properties: [
                        ({
                            key: "Virtual Host Name",
                            value: "demovhost"
                        }),
                        ({
                            key: "DNS IP Address",
                            value: "10.10.10.10"
                        }),
                        ({
                            key: "DNS Type",
                            value: "Microsoft"
                        }),
                        ({
                            key: "DNS Zone",
                            value: "solarwinds.com"
                        }),
                        ({
                            key: "Virtual IP address",
                            value: "10.0.100.100"
                        })
                    ],
                    pluginsData: {
                        virtualIpResource: { ipAddress: "10.0.100.100" },
                        virtualHostNameResource: {
                            dnsZone: "solarwinds.com",
                            hostName: "demovhost",
                            dnsIP: "10.10.10.10",
                            dnsUserName: undefined,
                            dnsPassword: undefined,
                            dnsType: "Microsoft"
                        },
                        poolConfiguration: { preferredMemberId: 1 }
                    }
                })
            ]);
        });
    };
    SummaryServiceDemoMock.prototype.getOrionServers = function () {
        return this.$q(function (resolve) {
            resolve([new orionServer_model_1.default({
                    hostName: "Main Server",
                    ipAddress: "10.0.100.1",
                    lastDatabaseUpdate: "3 hours ago",
                    description: "Active",
                    pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPoller,
                    poolId: 1,
                    poolMemberId: 1,
                    statusDescription: "Status is UP",
                    itemType: "orionServer",
                    status: 1,
                    runType: 0,
                    installedModulesOk: [
                        "Orion Platform 2017.3",
                        "VNQM 4.4.0",
                        "CloudMonitoring 1.1.0",
                        "SRM 6.6.0",
                        "SAM 7.0.0",
                        "DPAIM 11.0.0",
                        "NCM 7.7",
                        "VIM 8.1.0",
                        "QoE 2.4",
                        "NetPath 1.1.1",
                        "NPM 12.2",
                        "NTA 4.4.0",
                        "UDT 3.3.0"
                    ],
                    installedModulesMismatch: [],
                    properties: [],
                    pollingCompletion: 100.0,
                    elements: 53,
                    pollingRate: [
                        ({
                            displayName: "POLLING RATE",
                            value: 0,
                            name: "POLLING RATE"
                        }),
                        ({
                            displayName: "POLLING RATE",
                            value: 1,
                            name: "POLLING RATE"
                        }),
                        ({
                            displayName: "POLLING RATE",
                            value: 0,
                            name: "POLLING RATE"
                        })
                    ]
                }), new orionServer_model_1.default({
                    hostName: "Main Server Backup",
                    ipAddress: "10.0.100.2",
                    lastDatabaseUpdate: "3 hours ago",
                    icon: "orion-sitemaster-backup",
                    description: "Standby",
                    pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPollerStandby,
                    poolId: 1,
                    poolMemberId: 2,
                    statusDescription: "Status is UP",
                    itemType: "orionServer",
                    iconStatus: "up",
                    status: 1,
                    runType: 1,
                    installedModulesOk: [
                        "Orion Platform 2017.3",
                        "VNQM 4.4.0",
                        "CloudMonitoring 1.1.0",
                        "SRM 6.6.0",
                        "SAM 7.0.0",
                        "DPAIM 11.0.0",
                        "NCM 7.7",
                        "VIM 8.1.0",
                        "QoE 2.4",
                        "NetPath 1.1.1",
                        "NPM 12.2",
                        "NTA 4.4.0",
                        "UDT 3.3.0"
                    ],
                    installedModulesMismatch: [],
                    properties: []
                }), new orionServer_model_1.default({
                    hostName: "Additional Poller",
                    ipAddress: "10.0.100.3",
                    lastDatabaseUpdate: "3 hours ago",
                    icon: "orion-ape",
                    description: "Active",
                    pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPoller,
                    poolId: 0,
                    poolMemberId: 3,
                    statusDescription: "Status is UP",
                    itemType: "orionServer",
                    iconStatus: "up",
                    status: 1,
                    runType: 0,
                    pollingCompletion: 100.0,
                    elements: 5
                }), new orionServer_model_1.default({
                    hostName: "Additional Poller 2",
                    ipAddress: "10.0.200.3",
                    lastDatabaseUpdate: "3 hours ago",
                    icon: "orion-ape",
                    description: "Active",
                    pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPoller,
                    poolId: 0,
                    poolMemberId: 4,
                    statusDescription: "Status is UP",
                    itemType: "orionServer",
                    iconStatus: "up",
                    status: 1,
                    runType: 0,
                    pollingCompletion: 100.0,
                    elements: 5
                }), new orionServer_model_1.default({
                    hostName: "Additional Poller Backup",
                    ipAddress: "10.0.100.4",
                    lastDatabaseUpdate: "3 hours ago",
                    icon: "orion-ape-backup",
                    description: "Standby",
                    pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPollerStandby,
                    poolId: 0,
                    poolMemberId: 5,
                    statusDescription: "Status is UP",
                    itemType: "orionServer",
                    iconStatus: "up",
                    status: 1,
                    runType: 1
                }), new orionServer_model_1.default({
                    hostName: "Additional Poller Backup 2",
                    ipAddress: "10.0.200.4",
                    lastDatabaseUpdate: "3 hours ago",
                    icon: "orion-ape-backup",
                    description: "Standby",
                    pollingEngineType: orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPollerStandby,
                    poolId: 0,
                    poolMemberId: 6,
                    statusDescription: "Status is UP",
                    itemType: "orionServer",
                    iconStatus: "up",
                    status: 1,
                    runType: 1
                })]);
        });
    };
    SummaryServiceDemoMock.$inject = ["$q", "swApi", "haconstants", "demoService"];
    return SummaryServiceDemoMock;
}(summary_service_1.default));
exports.default = SummaryServiceDemoMock;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VtbWFyeS1zZXJ2aWNlLWRlbW8tbW9jay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInN1bW1hcnktc2VydmljZS1kZW1vLW1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHVDQUF1Qzs7Ozs7Ozs7Ozs7O0FBRXZDLDJFQUFxRTtBQUdyRSwyREFBcUQ7QUFHckQscURBQStDO0FBQy9DLG1HQUE2RjtBQUs3RjtJQUFvRCwwQ0FBYztJQUc5RCxnQ0FDSSxFQUFnQixFQUNoQixLQUFVLEVBQ1YsV0FBc0IsRUFDZCxXQUF3QjtRQUpwQyxZQUtJLGtCQUFNLEVBQUUsRUFBRSxLQUFLLEVBQUUsV0FBVyxDQUFDLFNBQ2hDO1FBRlcsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBRXBDLENBQUM7SUFFRCxpRUFBZ0MsR0FBaEM7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLE9BQU87WUFDbkIsT0FBQSxPQUFPLENBQUM7Z0JBQ0osU0FBUyxFQUFFLElBQUk7Z0JBQ2YsWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLE1BQU0sRUFBRTtvQkFDSixzQkFBc0IsRUFBRSxJQUFJO29CQUM1Qix1QkFBdUIsRUFBRSxJQUFJO29CQUM3QixpQkFBaUIsRUFBRSxJQUFJO29CQUN2QixtQkFBbUIsRUFBRSxJQUFJO29CQUN6QixvQkFBb0IsRUFBRSxJQUFJO2lCQUM3QjtnQkFDRCxnQkFBZ0IsRUFBRSxJQUFJO2dCQUN0QixPQUFPLEVBQUUsSUFBSTtnQkFDYixNQUFNLEVBQUUsSUFBSTthQUNmLENBQUM7UUFiRixDQWFFLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCxzREFBcUIsR0FBckIsVUFBc0IsUUFBZ0I7UUFDbEMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBQyxPQUFPLElBQUssT0FBQSxPQUFPLENBQUM7WUFDZDtnQkFDZCxXQUFXLEVBQUUsbUJBQW1CO2dCQUNoQyxJQUFJLEVBQUUsbUJBQW1CO2dCQUN6QixTQUFTLEVBQUUsWUFBWTtnQkFDdkIsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsV0FBVyxFQUFFLFFBQVE7Z0JBQ3JCLGlCQUFpQixFQUFFLHdEQUF3QixDQUFDLGdCQUFnQjtnQkFDNUQsTUFBTSxFQUFFLENBQUM7Z0JBQ1QsWUFBWSxFQUFFLENBQUM7Z0JBQ2YsaUJBQWlCLEVBQUUsRUFBRTtnQkFDckIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixNQUFNLEVBQUUsQ0FBQztnQkFDVCxPQUFPLEVBQUUsQ0FBQzthQUNiO1lBQ2lCO2dCQUNkLFdBQVcsRUFBRSxxQkFBcUI7Z0JBQ2xDLElBQUksRUFBRSxxQkFBcUI7Z0JBQzNCLFNBQVMsRUFBRSxZQUFZO2dCQUN2QixJQUFJLEVBQUUsUUFBUTtnQkFDZCxXQUFXLEVBQUUsUUFBUTtnQkFDckIsaUJBQWlCLEVBQUUsd0RBQXdCLENBQUMsZ0JBQWdCO2dCQUM1RCxNQUFNLEVBQUUsQ0FBQztnQkFDVCxZQUFZLEVBQUUsQ0FBQztnQkFDZixpQkFBaUIsRUFBRSxFQUFFO2dCQUNyQixRQUFRLEVBQUUsRUFBRTtnQkFDWixRQUFRLEVBQUUsYUFBYTtnQkFDdkIsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLE1BQU0sRUFBRSxDQUFDO2dCQUNULE9BQU8sRUFBRSxDQUFDO2FBQ2I7U0FDSixDQUFDLEVBakMwQixDQWlDMUIsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUVELDZDQUFZLEdBQVosVUFBYSxNQUFjO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDakMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCLEVBQVEsQ0FBQztJQUM5RCxDQUFDO0lBRUQsa0RBQWlCLEdBQWpCLFVBQWtCLE1BQWMsRUFBRSxPQUFnQjtRQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2pDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLHlCQUF5QixFQUFRLENBQUM7SUFDOUQsQ0FBQztJQUVELGlEQUFnQixHQUFoQixVQUFpQixNQUFjO1FBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDakMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCLEVBQVEsQ0FBQztJQUM5RCxDQUFDO0lBRUQsNENBQVcsR0FBWCxVQUFZLE9BQWdDO1FBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQUMsT0FBTztZQUNuQixPQUFBLE9BQU8sQ0FBQztnQkFDSixTQUFTLEVBQUUsQ0FBQztnQkFDWixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsTUFBTSxFQUFFLGtCQUFrQjtnQkFDMUIsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTthQUN6QixDQUFDO1FBUEYsQ0FPRSxDQUFDLENBQUM7SUFDWixDQUFDO0lBRU0seUNBQVEsR0FBZjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQUMsT0FBTztZQUNuQixPQUFPLENBQUM7Z0JBQ0osSUFBSSxvQkFBUyxDQUFZO29CQUNyQixXQUFXLEVBQUUsV0FBVztvQkFDeEIsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsaUJBQWlCLEVBQUUsNkJBQTZCO29CQUNoRCxPQUFPLEVBQUUsSUFBSTtvQkFDYix5QkFBeUIsRUFBRSxJQUFJO29CQUMvQixNQUFNLEVBQUUsQ0FBQztvQkFDVCxRQUFRLEVBQUUsQ0FBQztvQkFDWCxJQUFJLEVBQUUsdUJBQXVCO29CQUM3QixVQUFVLEVBQUUsSUFBSTtvQkFDaEIsTUFBTSxFQUFFLENBQUM7b0JBQ1QsUUFBUSxFQUFFLE1BQU07b0JBQ2hCLFVBQVUsRUFBRSxJQUFJO29CQUNoQix1QkFBdUIsRUFBRSxJQUFJO29CQUM3QixVQUFVLEVBQUU7d0JBQ1ksQ0FBQzs0QkFDakIsR0FBRyxFQUFFLG1CQUFtQjs0QkFDeEIsS0FBSyxFQUFFLFdBQVc7eUJBQ3JCLENBQUM7d0JBQ2tCLENBQUM7NEJBQ2pCLEdBQUcsRUFBRSxnQkFBZ0I7NEJBQ3JCLEtBQUssRUFBRSxhQUFhO3lCQUN2QixDQUFDO3dCQUNrQixDQUFDOzRCQUNqQixHQUFHLEVBQUUsVUFBVTs0QkFDZixLQUFLLEVBQUUsV0FBVzt5QkFDckIsQ0FBQzt3QkFDa0IsQ0FBQzs0QkFDakIsR0FBRyxFQUFFLFVBQVU7NEJBQ2YsS0FBSyxFQUFFLGdCQUFnQjt5QkFDMUIsQ0FBQzt3QkFDa0IsQ0FBQzs0QkFDakIsR0FBRyxFQUFFLG9CQUFvQjs0QkFDekIsS0FBSyxFQUFFLGNBQWM7eUJBQ3hCLENBQUM7cUJBQ0w7b0JBQ0QsV0FBVyxFQUFlO3dCQUN0QixpQkFBaUIsRUFBRSxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUU7d0JBQ2hELHVCQUF1QixFQUFFOzRCQUNyQixPQUFPLEVBQUUsZ0JBQWdCOzRCQUN6QixRQUFRLEVBQUUsV0FBVzs0QkFDckIsS0FBSyxFQUFFLGFBQWE7NEJBQ3BCLFdBQVcsRUFBRSxTQUFTOzRCQUN0QixXQUFXLEVBQUUsU0FBUzs0QkFDdEIsT0FBTyxFQUFFLFdBQVc7eUJBQ3ZCO3dCQUNELGlCQUFpQixFQUFFLEVBQUUsaUJBQWlCLEVBQUUsQ0FBQyxFQUFFO3FCQUM5QztpQkFDSixDQUFDO2FBQ0wsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sZ0RBQWUsR0FBdEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLE9BQU87WUFDbkIsT0FBTyxDQUNILENBQUMsSUFBSSwyQkFBZ0IsQ0FBbUI7b0JBQ3BDLFFBQVEsRUFBRSxhQUFhO29CQUN2QixTQUFTLEVBQUUsWUFBWTtvQkFDdkIsa0JBQWtCLEVBQUUsYUFBYTtvQkFDakMsV0FBVyxFQUFFLFFBQVE7b0JBQ3JCLGlCQUFpQixFQUFFLHdEQUF3QixDQUFDLFVBQVU7b0JBQ3RELE1BQU0sRUFBRSxDQUFDO29CQUNULFlBQVksRUFBRSxDQUFDO29CQUNmLGlCQUFpQixFQUFFLGNBQWM7b0JBQ2pDLFFBQVEsRUFBRSxhQUFhO29CQUN2QixNQUFNLEVBQUUsQ0FBQztvQkFDVCxPQUFPLEVBQUUsQ0FBQztvQkFDVixrQkFBa0IsRUFBRTt3QkFDaEIsdUJBQXVCO3dCQUN2QixZQUFZO3dCQUNaLHVCQUF1Qjt3QkFDdkIsV0FBVzt3QkFDWCxXQUFXO3dCQUNYLGNBQWM7d0JBQ2QsU0FBUzt3QkFDVCxXQUFXO3dCQUNYLFNBQVM7d0JBQ1QsZUFBZTt3QkFDZixVQUFVO3dCQUNWLFdBQVc7d0JBQ1gsV0FBVztxQkFBQztvQkFDaEIsd0JBQXdCLEVBQUUsRUFBRTtvQkFDNUIsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsaUJBQWlCLEVBQUUsS0FBSztvQkFDeEIsUUFBUSxFQUFFLEVBQUU7b0JBQ1osV0FBVyxFQUFFO3dCQUNZLENBQUM7NEJBQ2xCLFdBQVcsRUFBRSxjQUFjOzRCQUMzQixLQUFLLEVBQUUsQ0FBQzs0QkFDUixJQUFJLEVBQUUsY0FBYzt5QkFDdkIsQ0FBQzt3QkFDbUIsQ0FBQzs0QkFDbEIsV0FBVyxFQUFFLGNBQWM7NEJBQzNCLEtBQUssRUFBRSxDQUFDOzRCQUNSLElBQUksRUFBRSxjQUFjO3lCQUN2QixDQUFDO3dCQUNtQixDQUFDOzRCQUNsQixXQUFXLEVBQUUsY0FBYzs0QkFDM0IsS0FBSyxFQUFFLENBQUM7NEJBQ1IsSUFBSSxFQUFFLGNBQWM7eUJBQ3ZCLENBQUM7cUJBQ0w7aUJBQ0osQ0FBQyxFQUFFLElBQUksMkJBQWdCLENBQW1CO29CQUN2QyxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixTQUFTLEVBQUUsWUFBWTtvQkFDdkIsa0JBQWtCLEVBQUUsYUFBYTtvQkFDakMsSUFBSSxFQUFFLHlCQUF5QjtvQkFDL0IsV0FBVyxFQUFFLFNBQVM7b0JBQ3RCLGlCQUFpQixFQUFFLHdEQUF3QixDQUFDLGlCQUFpQjtvQkFDN0QsTUFBTSxFQUFFLENBQUM7b0JBQ1QsWUFBWSxFQUFFLENBQUM7b0JBQ2YsaUJBQWlCLEVBQUUsY0FBYztvQkFDakMsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLFVBQVUsRUFBRSxJQUFJO29CQUNoQixNQUFNLEVBQUUsQ0FBQztvQkFDVCxPQUFPLEVBQUUsQ0FBQztvQkFDVixrQkFBa0IsRUFBRTt3QkFDaEIsdUJBQXVCO3dCQUN2QixZQUFZO3dCQUNaLHVCQUF1Qjt3QkFDdkIsV0FBVzt3QkFDWCxXQUFXO3dCQUNYLGNBQWM7d0JBQ2QsU0FBUzt3QkFDVCxXQUFXO3dCQUNYLFNBQVM7d0JBQ1QsZUFBZTt3QkFDZixVQUFVO3dCQUNWLFdBQVc7d0JBQ1gsV0FBVztxQkFBQztvQkFDaEIsd0JBQXdCLEVBQUUsRUFBRTtvQkFDNUIsVUFBVSxFQUFFLEVBQUU7aUJBQ2pCLENBQUMsRUFBRSxJQUFJLDJCQUFnQixDQUFtQjtvQkFDdkMsUUFBUSxFQUFFLG1CQUFtQjtvQkFDN0IsU0FBUyxFQUFFLFlBQVk7b0JBQ3ZCLGtCQUFrQixFQUFFLGFBQWE7b0JBQ2pDLElBQUksRUFBRSxXQUFXO29CQUNqQixXQUFXLEVBQUUsUUFBUTtvQkFDckIsaUJBQWlCLEVBQUUsd0RBQXdCLENBQUMsZ0JBQWdCO29CQUM1RCxNQUFNLEVBQUUsQ0FBQztvQkFDVCxZQUFZLEVBQUUsQ0FBQztvQkFDZixpQkFBaUIsRUFBRSxjQUFjO29CQUNqQyxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsVUFBVSxFQUFFLElBQUk7b0JBQ2hCLE1BQU0sRUFBRSxDQUFDO29CQUNULE9BQU8sRUFBRSxDQUFDO29CQUNWLGlCQUFpQixFQUFFLEtBQUs7b0JBQ3hCLFFBQVEsRUFBRSxDQUFDO2lCQUNkLENBQUMsRUFBRSxJQUFJLDJCQUFnQixDQUFtQjtvQkFDdkMsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsU0FBUyxFQUFFLFlBQVk7b0JBQ3ZCLGtCQUFrQixFQUFFLGFBQWE7b0JBQ2pDLElBQUksRUFBRSxXQUFXO29CQUNqQixXQUFXLEVBQUUsUUFBUTtvQkFDckIsaUJBQWlCLEVBQUUsd0RBQXdCLENBQUMsZ0JBQWdCO29CQUM1RCxNQUFNLEVBQUUsQ0FBQztvQkFDVCxZQUFZLEVBQUUsQ0FBQztvQkFDZixpQkFBaUIsRUFBRSxjQUFjO29CQUNqQyxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsVUFBVSxFQUFFLElBQUk7b0JBQ2hCLE1BQU0sRUFBRSxDQUFDO29CQUNULE9BQU8sRUFBRSxDQUFDO29CQUNWLGlCQUFpQixFQUFFLEtBQUs7b0JBQ3hCLFFBQVEsRUFBRSxDQUFDO2lCQUNkLENBQUMsRUFBRSxJQUFJLDJCQUFnQixDQUFtQjtvQkFDdkMsUUFBUSxFQUFFLDBCQUEwQjtvQkFDcEMsU0FBUyxFQUFFLFlBQVk7b0JBQ3ZCLGtCQUFrQixFQUFFLGFBQWE7b0JBQ2pDLElBQUksRUFBRSxrQkFBa0I7b0JBQ3hCLFdBQVcsRUFBRSxTQUFTO29CQUN0QixpQkFBaUIsRUFBRSx3REFBd0IsQ0FBQyx1QkFBdUI7b0JBQ25FLE1BQU0sRUFBRSxDQUFDO29CQUNULFlBQVksRUFBRSxDQUFDO29CQUNmLGlCQUFpQixFQUFFLGNBQWM7b0JBQ2pDLFFBQVEsRUFBRSxhQUFhO29CQUN2QixVQUFVLEVBQUUsSUFBSTtvQkFDaEIsTUFBTSxFQUFFLENBQUM7b0JBQ1QsT0FBTyxFQUFFLENBQUM7aUJBQ2IsQ0FBQyxFQUFFLElBQUksMkJBQWdCLENBQW1CO29CQUN2QyxRQUFRLEVBQUUsNEJBQTRCO29CQUN0QyxTQUFTLEVBQUUsWUFBWTtvQkFDdkIsa0JBQWtCLEVBQUUsYUFBYTtvQkFDakMsSUFBSSxFQUFFLGtCQUFrQjtvQkFDeEIsV0FBVyxFQUFFLFNBQVM7b0JBQ3RCLGlCQUFpQixFQUFFLHdEQUF3QixDQUFDLHVCQUF1QjtvQkFDbkUsTUFBTSxFQUFFLENBQUM7b0JBQ1QsWUFBWSxFQUFFLENBQUM7b0JBQ2YsaUJBQWlCLEVBQUUsY0FBYztvQkFDakMsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLFVBQVUsRUFBRSxJQUFJO29CQUNoQixNQUFNLEVBQUUsQ0FBQztvQkFDVCxPQUFPLEVBQUUsQ0FBQztpQkFDYixDQUFDLENBQUMsQ0FDTixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBblNhLDhCQUFPLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQztJQW9TMUUsNkJBQUM7Q0FBQSxBQXJTRCxDQUFvRCx5QkFBYyxHQXFTakU7a0JBclNvQixzQkFBc0IifQ==

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var navigation_service_1 = __webpack_require__(14);
var navigation_service_demo_mock_1 = __webpack_require__(30);
exports.default = function (module) {
    module.factory("navigationService", function (constants, $injector) {
        return constants.environment.demoMode
            ? $injector.instantiate(navigation_service_demo_mock_1.default)
            : $injector.instantiate(navigation_service_1.default);
    }, ["constants", "$injector"]);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJEQUFxRDtBQUNyRCwrRUFBdUU7QUFFdkUsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEVBQUUsVUFBQyxTQUFjLEVBQUUsU0FBYztRQUMvRCxNQUFNLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRO1lBQ2pDLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLHNDQUF5QixDQUFDO1lBQ2xELENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLDRCQUFpQixDQUFDLENBQUM7SUFDbkQsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var navigation_service_1 = __webpack_require__(14);
var NavigationServiceDemoMock = /** @class */ (function (_super) {
    __extends(NavigationServiceDemoMock, _super);
    function NavigationServiceDemoMock($state, $window, haconstants, demoService) {
        var _this = _super.call(this, $state, $window, haconstants) || this;
        _this.demoService = demoService;
        return _this;
    }
    NavigationServiceDemoMock.prototype.navigateToSummaryPage = function (params) {
        if (params === void 0) { params = null; }
        if (params) {
            return;
        }
        _super.prototype.navigateToSummaryPage.call(this, params);
    };
    ;
    NavigationServiceDemoMock.prototype.downloadSolarWindsOrionInstaller = function () {
        this.demoService.showDemoToast();
    };
    ;
    NavigationServiceDemoMock.$inject = ["$state", "$window", "haconstants", "demoService"];
    return NavigationServiceDemoMock;
}(navigation_service_1.default));
exports.default = NavigationServiceDemoMock;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1zZXJ2aWNlLWRlbW8tbW9jay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5hdmlnYXRpb24tc2VydmljZS1kZW1vLW1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsMkRBQXFEO0FBSXJEO0lBQXVELDZDQUFpQjtJQUdwRSxtQ0FDSSxNQUEyQixFQUMzQixPQUEwQixFQUMxQixXQUFzQixFQUNkLFdBQXdCO1FBSnBDLFlBS0ksa0JBQU0sTUFBTSxFQUFFLE9BQU8sRUFBRSxXQUFXLENBQUMsU0FDdEM7UUFGVyxpQkFBVyxHQUFYLFdBQVcsQ0FBYTs7SUFFcEMsQ0FBQztJQUVNLHlEQUFxQixHQUE1QixVQUE2QixNQUFrQjtRQUFsQix1QkFBQSxFQUFBLGFBQWtCO1FBQzNDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDVCxNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsaUJBQU0scUJBQXFCLFlBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUFBLENBQUM7SUFFSyxvRUFBZ0MsR0FBdkM7UUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3JDLENBQUM7SUFBQSxDQUFDO0lBbkJZLGlDQUFPLEdBQUcsQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQztJQW9CaEYsZ0NBQUM7Q0FBQSxBQXJCRCxDQUF1RCw0QkFBaUIsR0FxQnZFO2tCQXJCb0IseUJBQXlCIn0=

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var demo_service_1 = __webpack_require__(32);
exports.default = function (module) {
    module.service("demoService", demo_service_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtDQUF5QztBQUV6QyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsc0JBQVcsQ0FBQyxDQUFDO0FBQy9DLENBQUMsQ0FBQyJ9

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var actionResult_model_1 = __webpack_require__(33);
var DemoService = /** @class */ (function () {
    function DemoService($q, $window, $log, $rootScope, xuiToastService, swDemoService) {
        this.$q = $q;
        this.$window = $window;
        this.$log = $log;
        this.$rootScope = $rootScope;
        this.xuiToastService = xuiToastService;
        this.swDemoService = swDemoService;
    }
    DemoService.prototype.showDemoToast = function () {
        try {
            this.swDemoService.showDemoErrorToast();
        }
        catch (e) {
            this.$log.error(e);
        }
    };
    DemoService.prototype.isDemoMode = function () {
        return this.swDemoService.isDemoMode();
    };
    DemoService.prototype.createActionResultPromise = function () {
        var defered = this.$q.defer();
        defered.resolve(new actionResult_model_1.default({ errorCode: 1, errorMessage: "" }));
        return defered.promise;
    };
    DemoService.$inject = ["$q", "$window", "$log", "$rootScope", "xuiToastService", "swDemoService"];
    return DemoService;
}());
exports.default = DemoService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVtby1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGVtby1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsdUNBQXVDO0FBQ3ZDLCtFQUF5RTtBQUV6RTtJQUdJLHFCQUNZLEVBQWdCLEVBQ2hCLE9BQTBCLEVBQzFCLElBQW9CLEVBQ3BCLFVBQWdDLEVBQ2hDLGVBQWtDLEVBQ2xDLGFBQWtCO1FBTGxCLE9BQUUsR0FBRixFQUFFLENBQWM7UUFDaEIsWUFBTyxHQUFQLE9BQU8sQ0FBbUI7UUFDMUIsU0FBSSxHQUFKLElBQUksQ0FBZ0I7UUFDcEIsZUFBVSxHQUFWLFVBQVUsQ0FBc0I7UUFDaEMsb0JBQWUsR0FBZixlQUFlLENBQW1CO1FBQ2xDLGtCQUFhLEdBQWIsYUFBYSxDQUFLO0lBRTlCLENBQUM7SUFFTSxtQ0FBYSxHQUFwQjtRQUNJLElBQUksQ0FBQztZQUNELElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM1QyxDQUFDO1FBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7SUFDTCxDQUFDO0lBRU0sZ0NBQVUsR0FBakI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMzQyxDQUFDO0lBRU0sK0NBQXlCLEdBQWhDO1FBQ0ksSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQXdCLENBQUM7UUFDdEQsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLDRCQUFpQixDQUEwQixFQUFFLFNBQVMsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztJQUMzQixDQUFDO0lBNUJhLG1CQUFPLEdBQUcsQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsaUJBQWlCLEVBQUUsZUFBZSxDQUFDLENBQUM7SUE2QnhHLGtCQUFDO0NBQUEsQUE5QkQsSUE4QkM7a0JBOUJvQixXQUFXIn0=

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var ActionResultModel = /** @class */ (function () {
    function ActionResultModel(model) {
        if (model) {
            _.extend(this, model);
        }
    }
    return ActionResultModel;
}());
exports.default = ActionResultModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aW9uUmVzdWx0LW1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWN0aW9uUmVzdWx0LW1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDO0lBUUksMkJBQVksS0FBNEI7UUFDcEMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNSLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFCLENBQUM7SUFDTCxDQUFDO0lBQ0wsd0JBQUM7QUFBRCxDQUFDLEFBYkQsSUFhQyJ9

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pool_service_1 = __webpack_require__(15);
var pool_service_demo_mock_1 = __webpack_require__(35);
exports.default = function (module) {
    module.factory("addPoolService", function (constants, $injector) {
        return constants.environment.demoMode
            ? $injector.instantiate(pool_service_demo_mock_1.default)
            : $injector.instantiate(pool_service_1.default);
    }, ["constants", "$injector"]);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtDQUF5QztBQUN6QyxtRUFBdUQ7QUFFdkQsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsVUFBQyxTQUFjLEVBQUUsU0FBYztRQUM1RCxNQUFNLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRO1lBQ2pDLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGdDQUFlLENBQUM7WUFDeEMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsc0JBQVcsQ0FBQyxDQUFDO0lBQzdDLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0FBQ25DLENBQUMsQ0FBQyJ9

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var pool_service_1 = __webpack_require__(15);
var PoolServiceDemoMock = /** @class */ (function (_super) {
    __extends(PoolServiceDemoMock, _super);
    function PoolServiceDemoMock($q, swApi, demoService) {
        var _this = _super.call(this, swApi) || this;
        _this.$q = $q;
        _this.demoService = demoService;
        return _this;
    }
    PoolServiceDemoMock.prototype.areMembersFromSameSubnet = function (poolMembersIds) {
        return this.$q(function (resolve) {
            resolve((poolMembersIds[0] === 6 && poolMembersIds[1] === 4) ||
                (poolMembersIds[0] === 5 && poolMembersIds[1] === 3) ||
                (poolMembersIds[0] === 1 && poolMembersIds[1] === 2));
        });
    };
    PoolServiceDemoMock.prototype.validatePool = function (poolModel) {
        var poolConfiguration = poolModel.pluginsData.poolConfiguration;
        var preferredMemberId = poolConfiguration ? poolConfiguration.preferredMemberId : 0;
        return this.$q(function (resolve) {
            return resolve({
                errorCode: (preferredMemberId === 2 || preferredMemberId === 5 || preferredMemberId === 6) ? 32 : 0,
                errorMessage: "",
                result: null,
                message: null,
                exceptionMessage: null,
                status: null
            });
        });
    };
    PoolServiceDemoMock.prototype.ensurePool = function (poolModel) {
        this.demoService.showDemoToast();
        return this.demoService.createActionResultPromise();
    };
    PoolServiceDemoMock.$inject = ["$q", "swApi", "demoService"];
    return PoolServiceDemoMock;
}(pool_service_1.default));
exports.default = PoolServiceDemoMock;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbC1zZXJ2aWNlLWRlbW8tbW9jay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBvb2wtc2VydmljZS1kZW1vLW1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBR0EsK0NBQXlDO0FBR3pDO0lBQWlELHVDQUFXO0lBR3hELDZCQUFvQixFQUFnQixFQUFFLEtBQVUsRUFBVSxXQUF3QjtRQUFsRixZQUNJLGtCQUFNLEtBQUssQ0FBQyxTQUNmO1FBRm1CLFFBQUUsR0FBRixFQUFFLENBQWM7UUFBc0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBRWxGLENBQUM7SUFFTSxzREFBd0IsR0FBL0IsVUFBZ0MsY0FBd0I7UUFDcEQsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBQyxPQUFPO1lBQ25CLE9BQU8sQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEQsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BELENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSwwQ0FBWSxHQUFuQixVQUFvQixTQUFvQjtRQUNwQyxJQUFNLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUM7UUFDbEUsSUFBTSxpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0RixNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLE9BQU87WUFDbkIsT0FBQSxPQUFPLENBQUM7Z0JBQ0osU0FBUyxFQUFFLENBQUMsaUJBQWlCLEtBQUssQ0FBQyxJQUFJLGlCQUFpQixLQUFLLENBQUMsSUFBSSxpQkFBaUIsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNuRyxZQUFZLEVBQUUsRUFBRTtnQkFDaEIsTUFBTSxFQUFFLElBQUk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtnQkFDdEIsTUFBTSxFQUFFLElBQUk7YUFDZixDQUFDO1FBUEYsQ0FPRSxDQUFDLENBQUM7SUFDWixDQUFDO0lBRU0sd0NBQVUsR0FBakIsVUFBa0IsU0FBb0I7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsRUFBVSxDQUFDO0lBQ2hFLENBQUM7SUEvQmEsMkJBQU8sR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFnQzNELDBCQUFDO0NBQUEsQUFqQ0QsQ0FBaUQsc0JBQVcsR0FpQzNEO2tCQWpDb0IsbUJBQW1CIn0=

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pool_1 = __webpack_require__(37);
var settings_1 = __webpack_require__(47);
var summary_1 = __webpack_require__(51);
var deployment_1 = __webpack_require__(73);
var update_1 = __webpack_require__(77);
var healthpage_1 = __webpack_require__(81);
var diagnostics_1 = __webpack_require__(85);
exports.default = function (module) {
    pool_1.default(module);
    settings_1.default(module);
    summary_1.default(module);
    deployment_1.default(module);
    update_1.default(module);
    healthpage_1.default(module);
    diagnostics_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtCQUEwQjtBQUMxQix1Q0FBa0M7QUFDbEMscUNBQWdDO0FBQ2hDLDJDQUFzQztBQUN0QyxtQ0FBOEI7QUFDOUIsMkNBQXNDO0FBQ3RDLDZDQUF3QztBQUV4QyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsY0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2Isa0JBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQixpQkFBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hCLG9CQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbkIsZ0JBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNmLG9CQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbkIscUJBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUN4QixDQUFDLENBQUMifQ==

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pool_config_1 = __webpack_require__(38);
__webpack_require__(45);
__webpack_require__(46);
exports.default = function (module) {
    module.config(pool_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDZDQUF1QztBQUN2Qyx1QkFBcUI7QUFDckIsdUNBQXFDO0FBRXJDLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsTUFBTSxDQUFDLHFCQUFVLENBQUMsQ0FBQztBQUM5QixDQUFDLENBQUMifQ==

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pool_controller_1 = __webpack_require__(39);
function PoolConfig($stateProvider, constants) {
    $stateProvider
        .state("pool", {
        i18Title: "_t(Set Up High Availability Pool)",
        url: "/ha/pool",
        params: { poolData: {} },
        controller: pool_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(44),
        resolve: {
            pools: ["summaryService", function (summaryService) {
                    return summaryService.getPools();
                }]
        }
    });
}
PoolConfig.$inject = ["$stateProvider", "constants"];
exports.default = PoolConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbC1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwb29sLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHFEQUErQztBQUcvQyxvQkFBb0IsY0FBb0MsRUFBRSxTQUFjO0lBQ3BFLGNBQWM7U0FDVCxLQUFLLENBQUMsTUFBTSxFQUNSO1FBQ0QsUUFBUSxFQUFFLG1DQUFtQztRQUM3QyxHQUFHLEVBQUUsVUFBVTtRQUNmLE1BQU0sRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUU7UUFDeEIsVUFBVSxFQUFFLHlCQUFjO1FBQzFCLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQVMsYUFBYSxDQUFDO1FBQ3hDLE9BQU8sRUFBRTtZQUNMLEtBQUssRUFBRSxDQUFDLGdCQUFnQixFQUFFLFVBQUMsY0FBOEI7b0JBQ3JELE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQztTQUNMO0tBQ0osQ0FBQyxDQUFDO0FBQ1gsQ0FBQztBQUVELFVBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxXQUFXLENBQUMsQ0FBQztBQUNyRCxrQkFBZSxVQUFVLENBQUMifQ==

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(0);
var pool_model_1 = __webpack_require__(2);
var orionServerPollingEngine_enum_1 = __webpack_require__(1);
var poolType_enum_1 = __webpack_require__(40);
var haBase_controller_1 = __webpack_require__(6);
var pluginsData_model_1 = __webpack_require__(41);
var poolConfiguration_model_1 = __webpack_require__(42);
var runType_enum_1 = __webpack_require__(5);
var poolOperation_enum_1 = __webpack_require__(16);
var PoolController = /** @class */ (function (_super) {
    __extends(PoolController, _super);
    function PoolController($scope, $stateParams, $state, $timeout, summaryService, _t, pools, navigationService, xuiDialogService, haconstants, sortingService, $log, poolService, $q, helpLinkService, settingsService) {
        var _this = _super.call(this) || this;
        _this.$scope = $scope;
        _this.$stateParams = $stateParams;
        _this.$state = $state;
        _this.$timeout = $timeout;
        _this.summaryService = summaryService;
        _this._t = _t;
        _this.pools = pools;
        _this.navigationService = navigationService;
        _this.xuiDialogService = xuiDialogService;
        _this.haconstants = haconstants;
        _this.sortingService = sortingService;
        _this.$log = $log;
        _this.poolService = poolService;
        _this.$q = $q;
        _this.helpLinkService = helpLinkService;
        _this.settingsService = settingsService;
        _this.serverToProtectStep = "serverToProtectStep";
        _this.poolPropertiesStep = "poolPropertiesStep";
        _this.dnsSettingsStep = "dnsSettingsStep";
        _this.summaryStep = "summaryStep";
        _this.poolPropertiesFormName = "poolPropertiesForm";
        _this.dnsSettingsStepFormName = "dnsSettingsStepForm";
        _this.$onInit = function () {
            _this.haDeploymentLink = _this.helpLinkService.getHelpLink("OrionCoreHADeployment");
            _this.steps = new Array(_this.serverToProtectStep, _this.poolPropertiesStep, _this.dnsSettingsStep, _this.summaryStep);
            _this.settingsService.isFipsEnabled().then(function (result) {
                _this.$scope.isFipsEnabled = result;
            });
            _this.$scope.areMembersFromSameSubnet = true;
            _this.$scope.$watch("availableServersSelection", function () {
                if (!_this.$scope.availableServersSelection || !_.isArray(_this.$scope.availableServersSelection)
                    || _this.$scope.availableServersSelection.length === 0) {
                    return;
                }
                if (!_.includes(_this.$scope.pool.poolMembersIds, _this.$scope.availableServersSelection[0].poolMemberId)) {
                    if (_this.$scope.pool.poolMembersIds.length > 1) {
                        _this.$scope.pool.poolMembersIds.pop();
                    }
                    _this.$scope.pool.poolMembersIds.push(_this.$scope.availableServersSelection[0].poolMemberId);
                }
                _this.progressBar.message = _this._t("Checking selected server...");
                _this.progressBar.show = true;
                _this.poolService.areMembersFromSameSubnet(_this.$scope.pool.poolMembersIds)
                    .then(function (response) {
                    if (_this.$scope.areMembersFromSameSubnet !== response) {
                        _this.$scope.areMembersFromSameSubnet = response;
                    }
                    _this.progressBar.show = false;
                });
            });
            _this.setAvailableServersGridSortProperties();
            _this.$scope.shouldShowFirstWizardPage = true;
            if (_.isEmpty(_this.$stateParams.poolData)) {
                _this.fixEmptyPoolScope();
                _this.navigationService.navigateToSummaryPage();
                return;
            }
            _this.$scope.pool = _this.$stateParams.poolData;
            if (!_this.$scope.pool.pluginsData) {
                _this.$scope.pool.pluginsData = new pluginsData_model_1.default();
            }
            if (!_this.$scope.pool.pluginsData.poolConfiguration) {
                _this.$scope.pool.pluginsData.poolConfiguration = new poolConfiguration_model_1.default();
                _this.$scope.pool.pluginsData.poolConfiguration.preferredMemberId = 0;
            }
            if (_this.$stateParams.poolData.poolId) {
                _this.$scope.shouldShowFirstWizardPage = false;
                _this.$scope.pool.poolMembersIds = new Array();
                _this.$stateParams.poolData.children.forEach(function (element) {
                    if (element.runType === runType_enum_1.RunType.Standby) {
                        _this.$scope.backupServer = element;
                    }
                    else {
                        _this.$scope.availableServersSelection = new Array(element);
                    }
                    _this.$scope.pool.poolMembersIds.push(element.poolMemberId);
                });
            }
            else {
                _this.$scope.backupServer = _this.$scope.pool.children[0];
                _this.$scope.availableServersSelection = new Array(_this.$scope.pool.availableServers[0]);
                if (_this.$scope.backupServer.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPollerStandby
                    && _this.$scope.pool.availableServers && _this.$scope.pool.availableServers.length === 1) {
                    _this.$scope.shouldShowFirstWizardPage = false;
                }
            }
            _this.title = _this.$state.current.title;
            _this.$scope.$on(_this.haconstants.validatePoolStartEvent, function () {
                _this.progressBar.message = _this._t("Testing pool settings...");
                _this.progressBar.show = true;
            });
            _this.$scope.$on(_this.haconstants.validatePoolEndEvent, function () {
                _this.progressBar.message = "";
                _this.progressBar.show = false;
            });
            _this.progressBar = {
                show: false,
                message: ""
            };
        };
        _this.scopeFormInit = function (formName, form) {
            _this.$scope[formName] = form;
        };
        _this.setAvailableServersGridSortProperties = function () {
            _this.$scope.availableServersGridSortingData = {
                sortableColumns: [{ name: "displayName", displayValue: _this._t("Display Name") },
                    { name: "status", displayValue: _this._t("Status") }],
                sortDirection: "asc",
                selectedItem: { name: "status", displayValue: _this._t("Status") }
            };
            _this.$scope.availableServersGridSortingData.sortChanged = function () {
                _this.$scope.pool.availableServers = _this.sortingService.sortBy(_this.$scope.pool.availableServers, _this.$scope.availableServersGridSortingData.selectedItem.name, _this.$scope.availableServersGridSortingData.sortDirection);
            };
        };
        _this.validatePool = function () {
            _this.progressBar.message = _this._t("Validating pool...");
            _this.progressBar.show = true;
            if (!_this.validateAllForms()) {
                _this.progressBar.show = false;
                return false;
            }
            if (_this.$scope.availableServersSelection &&
                !_.includes(_this.$scope.pool.poolMembersIds, _this.$scope.availableServersSelection[0].poolMemberId)) {
                _this.$scope.pool.poolMembersIds.push(_this.$scope.availableServersSelection[0].poolMemberId);
            }
            _this.progressBar.show = false;
            return true;
        };
        _this.finishWizard = function () {
            if (!_this.$scope.pool) {
                return;
            }
            if (!_this.validatePool()) {
                return;
            }
            _this.$scope.errorMessage = "";
            _this.progressBar.message = _this._t(_this.$scope.pool.poolId ? "Editing pool..." : "Creating pool...");
            _this.progressBar.show = true;
            _this.ensurePool();
        };
        _this.ensurePool = function () {
            _this.poolService.ensurePool(_this.$scope.pool.getNormalized())
                .then(function (response) {
                _this.$scope.errorMessage = response.errorMessage || response.message;
                _this.response = response;
                if (_.isEmpty(_this.$scope.errorMessage)) {
                    _this.navigationService.navigateToSummaryPage({
                        poolId: response.result,
                        poolOperation: _this.$scope.pool.poolId ? poolOperation_enum_1.PoolOperationEnum.Edit : poolOperation_enum_1.PoolOperationEnum.Create,
                        ignoreLicenseCheck: true
                    });
                }
                _this.progressBar.show = false;
            });
        };
        _this.getFinishWizardButtonText = function () {
            return _this.$scope.pool.poolId ? _this._t("Edit pool") : _this._t("Create pool");
        };
        _this.getSummaryStepDescription = function () {
            return _this._t("Review the pool properties and settings before it is " + (_this.$scope.pool.poolId ? "edited" : "created"));
        };
        _this.onPoolPropertiesStepEnter = function () {
            _this.addPoolStepEnter();
            if (_.isEmpty(_this.$scope.pool.displayName)) {
                _this.getSuggestedPoolName();
            }
        };
        _this.addPoolStepEnter = function () {
            _this.$scope.errorMessage = "";
            _this.$scope.$broadcast(_this.haconstants.addPoolStepEnterEvent);
        };
        _this.getSuggestedPoolName = function () {
            _this.summaryService.getPoolType(_this.$scope.pool.children)
                .then(function (actionResult) {
                if (actionResult.errorCode === 1) {
                    _this.$scope.errorMessage = actionResult.errorMessage;
                    return;
                }
                var poolType = poolType_enum_1.PoolTypeEnum[actionResult.result];
                var defaultName = (poolType === poolType_enum_1.PoolTypeEnum.MainPoller) ?
                    _this._t("Main Pool") :
                    _this._t("Additional Poller Pool");
                var matchingPools = _this.pools.filter(function (pool) { return pool.displayName.indexOf(defaultName) === 0; });
                //to be able start AP name with index 1
                var suggestedName = defaultName;
                if (poolType === poolType_enum_1.PoolTypeEnum.AdditionalPoller) {
                    suggestedName = suggestedName + " 1";
                }
                // find a name with unused index
                var index = 1;
                while (matchingPools.some(function (item) { return item.displayName === suggestedName; })) {
                    suggestedName = defaultName + " " + ++index;
                }
                _this.$scope.pool.displayName = suggestedName;
            });
        };
        _this.fixEmptyPoolScope = function () {
            if (_.isEmpty(_this.$scope.pool)) {
                _this.$scope.pool = new pool_model_1.default();
                _this.$scope.pool.availableServers = new Array();
            }
        };
        return _this;
    }
    PoolController.prototype.goToStep = function (stepName) {
        var idxOffset = this.$scope.shouldShowFirstWizardPage ? 0 : 1;
        var stepIndex = this.steps.indexOf(stepName) - idxOffset;
        this.$scope.currentWizardStepIndex = stepIndex;
    };
    PoolController.prototype.validateAddPoolStep = function () {
        var _this = this;
        var result = this.$q.defer();
        if (!this.validateCurrentStepForm()) {
            result.resolve(false);
            return result.promise;
        }
        var poolModelToValidate = null;
        if (this.getCurrentStep() === this.poolPropertiesStep) {
            poolModelToValidate = this.$scope.pool.getNormalizedWithPoolPlugin(constants_1.default.pluginsNames.virtualIpResourcePluginName);
        }
        if (this.getCurrentStep() === this.dnsSettingsStep) {
            poolModelToValidate = this.$scope.pool.getNormalizedWithPoolPlugin(constants_1.default.pluginsNames.virtualHostNameResource);
            var bindDnsType = this.$scope.pool.dnsTypes[1];
            if (this.$scope.isFipsEnabled === true &&
                this.$scope.pool.pluginsData.virtualHostNameResource.dnsType === bindDnsType) {
                result.resolve(false);
                return result.promise;
            }
        }
        if (poolModelToValidate === null) {
            result.resolve(true);
            return result.promise;
        }
        //!!! HACK !!! 
        //this is the only way to show modal without actually beeing blocked by busy overlay which is above modal
        //Apollo (UIF-4019) was created to resolve this
        this.$timeout(function () {
            angular.element($(".xui-wizard")).controller("xuiWizard").isBusy = false;
        }, 0);
        //!!! END HACK !!!
        this.$scope.$broadcast(this.haconstants.validatePoolStartEvent);
        this.poolService.validatePool(poolModelToValidate)
            .then(function (actionResult) {
            _this.$scope.$broadcast(_this.haconstants.validatePoolEndEvent);
            if (jQuery.inArray(actionResult.errorCode, [25]) === -1) {
                if (actionResult.errorCode !== 0 && actionResult.errorMessage) {
                    _this.$scope.errorMessage = actionResult.errorMessage;
                    result.resolve(false);
                    return;
                }
                result.resolve(true);
                return;
            }
            //Rest of code is about Virtual Host Name exists modal dialog feature
            var shouldContinue = false;
            _this.$scope.resolvedIpAddresses = actionResult.result;
            var modalSettings = {
                size: "md",
                template: __webpack_require__(43),
                scope: _this.$scope
            };
            var dialogOptions = {
                title: _this._t("VIRTUAL HOST NAME ALREADY EXISTS IN DNS"),
                cancelButtonText: _this._t("CANCEL"),
                status: "warning",
                buttons: [
                    {
                        isPrimary: true,
                        name: "acceptModalButton",
                        text: _this._t("YES, OVERWRITE ENTRY"),
                        action: function () {
                            shouldContinue = true;
                            return true;
                        }
                    },
                    {
                        name: "reenterModalButton",
                        displayStyle: "md",
                        text: _this._t("NO, ENTER DIFFERENT"),
                        action: function () {
                            shouldContinue = false;
                            return true;
                        }
                    }
                ]
            };
            _this.xuiDialogService.showModal(modalSettings, dialogOptions).then(function (dialogResult) {
                if (dialogResult === "cancel") {
                    return;
                }
                result.resolve(shouldContinue);
                if (!shouldContinue) {
                    _this.goToStep(_this.poolPropertiesStep);
                }
            });
        });
        return result.promise;
    };
    PoolController.prototype.validateCurrentStepForm = function () {
        if (this.getCurrentStep() === this.poolPropertiesStep) {
            return this.validateForm(this.$scope[this.poolPropertiesFormName]);
        }
        if (this.getCurrentStep() === this.dnsSettingsStep) {
            return this.validateForm(this.$scope[this.dnsSettingsStepFormName]);
        }
        return true;
    };
    PoolController.prototype.validateAllForms = function () {
        return this.validateForm(this.$scope[this.poolPropertiesFormName])
            && this.validateForm(this.$scope[this.dnsSettingsStepFormName]);
    };
    PoolController.prototype.getCurrentStep = function () {
        var idxOffset = this.$scope.shouldShowFirstWizardPage ? 0 : 1;
        return this.steps[this.$scope.currentWizardStepIndex + idxOffset];
    };
    ;
    PoolController.$inject = ["$scope", "$stateParams", "$state", "$timeout",
        "summaryService", "getTextService", "pools", "navigationService", "xuiDialogService",
        "haconstants", "xuiSortingService", "$log", "addPoolService", "$q", "helpLinkService", "settingsService"];
    return PoolController;
}(haBase_controller_1.default));
exports.default = PoolController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbC1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9vbC1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7Ozs7Ozs7Ozs7OztBQUd2QyxvREFBK0M7QUFDL0MsMkRBQXFEO0FBTXJELG1HQUE2RjtBQUM3RixpRUFBK0Q7QUFDL0Qsb0VBQThEO0FBRTlELDRFQUFpRTtBQUNqRSx3RkFBNkU7QUFDN0UsaUVBQTJEO0FBQzNELDJFQUF1RTtBQUd2RTtJQUE0QyxrQ0FBZ0I7SUF1QnhELHdCQUFtQixNQUE0QixFQUNwQyxZQUFpQixFQUNoQixNQUFXLEVBQ1gsUUFBNEIsRUFDNUIsY0FBK0IsRUFDL0IsRUFBb0MsRUFDcEMsS0FBa0IsRUFDbEIsaUJBQW9DLEVBQ3BDLGdCQUFvQyxFQUNyQyxXQUFzQixFQUNyQixjQUFtQyxFQUNuQyxJQUFvQixFQUNwQixXQUF5QixFQUN6QixFQUFnQixFQUNoQixlQUEwQyxFQUMxQyxlQUFpQztRQWY3QyxZQWdCSSxpQkFBTyxTQUNWO1FBakJrQixZQUFNLEdBQU4sTUFBTSxDQUFzQjtRQUNwQyxrQkFBWSxHQUFaLFlBQVksQ0FBSztRQUNoQixZQUFNLEdBQU4sTUFBTSxDQUFLO1FBQ1gsY0FBUSxHQUFSLFFBQVEsQ0FBb0I7UUFDNUIsb0JBQWMsR0FBZCxjQUFjLENBQWlCO1FBQy9CLFFBQUUsR0FBRixFQUFFLENBQWtDO1FBQ3BDLFdBQUssR0FBTCxLQUFLLENBQWE7UUFDbEIsdUJBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxzQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9CO1FBQ3JDLGlCQUFXLEdBQVgsV0FBVyxDQUFXO1FBQ3JCLG9CQUFjLEdBQWQsY0FBYyxDQUFxQjtRQUNuQyxVQUFJLEdBQUosSUFBSSxDQUFnQjtRQUNwQixpQkFBVyxHQUFYLFdBQVcsQ0FBYztRQUN6QixRQUFFLEdBQUYsRUFBRSxDQUFjO1FBQ2hCLHFCQUFlLEdBQWYsZUFBZSxDQUEyQjtRQUMxQyxxQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUE5QnRDLHlCQUFtQixHQUFHLHFCQUFxQixDQUFDO1FBQzVDLHdCQUFrQixHQUFHLG9CQUFvQixDQUFDO1FBQzFDLHFCQUFlLEdBQUcsaUJBQWlCLENBQUM7UUFDcEMsaUJBQVcsR0FBRyxhQUFhLENBQUM7UUFTM0IsNEJBQXNCLEdBQUcsb0JBQW9CLENBQUM7UUFDOUMsNkJBQXVCLEdBQUcscUJBQXFCLENBQUM7UUFxQmpELGFBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ2xGLEtBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQ2xCLEtBQUksQ0FBQyxtQkFBbUIsRUFDeEIsS0FBSSxDQUFDLGtCQUFrQixFQUN2QixLQUFJLENBQUMsZUFBZSxFQUNwQixLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFdEIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFlO2dCQUN0RCxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7WUFDdkMsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsTUFBTSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQztZQUM1QyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQywyQkFBMkIsRUFBRTtnQkFDNUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDO3VCQUN4RixLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN4RCxNQUFNLENBQUM7Z0JBQ1gsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN0RyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzdDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztvQkFDMUMsQ0FBQztvQkFDRCxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2hHLENBQUM7Z0JBRUQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO2dCQUNsRSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO3FCQUNyRSxJQUFJLENBQUMsVUFBQyxRQUFpQjtvQkFDcEIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyx3QkFBd0IsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO3dCQUNwRCxLQUFJLENBQUMsTUFBTSxDQUFDLHdCQUF3QixHQUFHLFFBQVEsQ0FBQztvQkFDcEQsQ0FBQztvQkFDRCxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMscUNBQXFDLEVBQUUsQ0FBQztZQUM3QyxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQztZQUU3QyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQkFDekIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLENBQUM7Z0JBQy9DLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBYyxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztZQUV6RCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLDJCQUFXLEVBQUUsQ0FBQztZQUNyRCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxpQ0FBaUIsRUFBRSxDQUFDO2dCQUN6RSxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDO1lBQ3pFLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLEtBQUssQ0FBQztnQkFDOUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksS0FBSyxFQUFVLENBQUM7Z0JBQ3RELEtBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUF5QjtvQkFDbEUsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sS0FBSyxzQkFBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ3RDLEtBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQztvQkFDdkMsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLElBQUksS0FBSyxDQUFtQixPQUFPLENBQUMsQ0FBQztvQkFDakYsQ0FBQztvQkFDRCxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDL0QsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLElBQUksS0FBSyxDQUFtQixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxRyxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsS0FBSyx3REFBd0IsQ0FBQyxpQkFBaUI7dUJBQ3RGLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN6RixLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLEtBQUssQ0FBQztnQkFDbEQsQ0FBQztZQUNMLENBQUM7WUFFRCxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUV2QyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLHNCQUFzQixFQUFFO2dCQUNyRCxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLDBCQUEwQixDQUFDLENBQUM7Z0JBQy9ELEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNqQyxDQUFDLENBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ25ELEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztnQkFDOUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLFdBQVcsR0FBRztnQkFDZixJQUFJLEVBQUUsS0FBSztnQkFDWCxPQUFPLEVBQUUsRUFBRTthQUNkLENBQUM7UUFDTixDQUFDLENBQUM7UUFFSyxtQkFBYSxHQUFHLFVBQUMsUUFBZ0IsRUFBRSxJQUF3QjtZQUM5RCxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUNqQyxDQUFDLENBQUE7UUFFTSwyQ0FBcUMsR0FBRztZQUMzQyxLQUFJLENBQUMsTUFBTSxDQUFDLCtCQUErQixHQUFHO2dCQUMxQyxlQUFlLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLEVBQUU7b0JBQ2hGLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDO2dCQUNwRCxhQUFhLEVBQUUsS0FBSztnQkFDcEIsWUFBWSxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRTthQUNwRSxDQUFDO1lBRUYsS0FBSSxDQUFDLE1BQU0sQ0FBQywrQkFBK0IsQ0FBQyxXQUFXLEdBQUc7Z0JBQ3RELEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUMxRCxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLCtCQUErQixDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQ2hHLEtBQUksQ0FBQyxNQUFNLENBQUMsK0JBQStCLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDbkUsQ0FBQyxDQUFDO1FBQ04sQ0FBQyxDQUFDO1FBRUssa0JBQVksR0FBRztZQUNsQixLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDekQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRTdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7Z0JBQzlCLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDakIsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCO2dCQUNyQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0RyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDaEcsQ0FBQztZQUVELEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztZQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQztRQUVLLGtCQUFZLEdBQUc7WUFDbEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDOUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3JHLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUM3QixLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFBO1FBRU8sZ0JBQVUsR0FBRztZQUNqQixLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztpQkFDeEQsSUFBSSxDQUFDLFVBQUMsUUFBbUM7Z0JBQ3RDLEtBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxZQUFZLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQztnQkFDckUsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7Z0JBRXpCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQzt3QkFDekMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxNQUFNO3dCQUN2QixhQUFhLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxzQ0FBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHNDQUFpQixDQUFDLE1BQU07d0JBQzFGLGtCQUFrQixFQUFFLElBQUk7cUJBQzNCLENBQUMsQ0FBQztnQkFDUCxDQUFDO2dCQUVELEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztZQUNsQyxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVLLCtCQUF5QixHQUFHO1lBQy9CLE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFBO1FBRU0sK0JBQXlCLEdBQUc7WUFDL0IsTUFBTSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsMkRBQ1gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBRSxDQUFDLENBQUM7UUFDMUQsQ0FBQyxDQUFBO1FBRU0sK0JBQXlCLEdBQUc7WUFDL0IsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDeEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQ2hDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFSyxzQkFBZ0IsR0FBRztZQUN0QixLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDOUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQTtRQWlIUywwQkFBb0IsR0FBRztZQUM3QixLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBMEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUM5RSxJQUFJLENBQUMsVUFBQyxZQUF1QztnQkFDMUMsRUFBRSxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMvQixLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDO29CQUNyRCxNQUFNLENBQUM7Z0JBQ1gsQ0FBQztnQkFFRCxJQUFNLFFBQVEsR0FBaUIsNEJBQVksQ0FBQyxZQUFZLENBQUMsTUFBYSxDQUFRLENBQUM7Z0JBQy9FLElBQU0sV0FBVyxHQUFHLENBQUMsUUFBUSxLQUFLLDRCQUFZLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDeEQsS0FBSSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUN0QixLQUFJLENBQUMsRUFBRSxDQUFDLHdCQUF3QixDQUFDLENBQUM7Z0JBRXRDLElBQUksYUFBYSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUEzQyxDQUEyQyxDQUFDLENBQUM7Z0JBRTdGLHVDQUF1QztnQkFDdkMsSUFBSSxhQUFhLEdBQUcsV0FBVyxDQUFDO2dCQUNoQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssNEJBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7b0JBQzdDLGFBQWEsR0FBTSxhQUFhLE9BQUksQ0FBQztnQkFDekMsQ0FBQztnQkFFRCxnQ0FBZ0M7Z0JBQ2hDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztnQkFDZCxPQUFPLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsV0FBVyxLQUFLLGFBQWEsRUFBbEMsQ0FBa0MsQ0FBQyxFQUFFLENBQUM7b0JBQ3RFLGFBQWEsR0FBTSxXQUFXLFNBQUksRUFBRSxLQUFPLENBQUM7Z0JBQ2hELENBQUM7Z0JBQ0QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsQ0FBQztZQUNqRCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQW1CTSx1QkFBaUIsR0FBRztZQUN4QixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLG9CQUFTLEVBQUUsQ0FBQztnQkFDbkMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxLQUFLLEVBQW9CLENBQUM7WUFDdEUsQ0FBQztRQUNMLENBQUMsQ0FBQzs7SUEvVkYsQ0FBQztJQTRMTSxpQ0FBUSxHQUFmLFVBQWdCLFFBQWdCO1FBQzVCLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hFLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFNBQVMsQ0FBQztRQUMzRCxJQUFJLENBQUMsTUFBTSxDQUFDLHNCQUFzQixHQUFHLFNBQVMsQ0FBQztJQUNuRCxDQUFDO0lBRU0sNENBQW1CLEdBQTFCO1FBQUEsaUJBdUdDO1FBdEdHLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFL0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0QixNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUMxQixDQUFDO1FBRUQsSUFBSSxtQkFBbUIsR0FBYyxJQUFJLENBQUM7UUFFMUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDcEQsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQzlELG1CQUFTLENBQUMsWUFBWSxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDNUQsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUNqRCxtQkFBbUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FDOUQsbUJBQVMsQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUNwRCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0MsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEtBQUssSUFBSTtnQkFDbEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLE9BQU8sS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUMvRSxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztZQUMxQixDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLG1CQUFtQixLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDL0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUMxQixDQUFDO1FBRUQsZUFBZTtRQUNmLHlHQUF5RztRQUN6RywrQ0FBK0M7UUFDL0MsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNWLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDN0UsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ04sa0JBQWtCO1FBRWxCLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQzthQUM3QyxJQUFJLENBQUMsVUFBQyxZQUE4QztZQUVyRCxLQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFFOUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RELEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLEtBQUssQ0FBQyxJQUFJLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUM1RCxLQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDO29CQUNyRCxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN0QixNQUFNLENBQUM7Z0JBQ1gsQ0FBQztnQkFFRCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNyQixNQUFNLENBQUM7WUFDWCxDQUFDO1lBRUQscUVBQXFFO1lBQ3JFLElBQUksY0FBYyxHQUFHLEtBQUssQ0FBQztZQUMzQixLQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7WUFFdEQsSUFBTSxhQUFhLEdBQW1DO2dCQUNsRCxJQUFJLEVBQUUsSUFBSTtnQkFDVixRQUFRLEVBQUUsT0FBTyxDQUFDLDhDQUE4QyxDQUFDO2dCQUNqRSxLQUFLLEVBQUUsS0FBSSxDQUFDLE1BQU07YUFDckIsQ0FBQztZQUVGLElBQUksYUFBYSxHQUF1QjtnQkFDcEMsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMseUNBQXlDLENBQUM7Z0JBQ3pELGdCQUFnQixFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNuQyxNQUFNLEVBQUUsU0FBUztnQkFDakIsT0FBTyxFQUFFO29CQUNMO3dCQUNJLFNBQVMsRUFBRSxJQUFJO3dCQUNmLElBQUksRUFBRSxtQkFBbUI7d0JBQ3pCLElBQUksRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLHNCQUFzQixDQUFDO3dCQUNyQyxNQUFNLEVBQUU7NEJBQ0osY0FBYyxHQUFHLElBQUksQ0FBQzs0QkFDdEIsTUFBTSxDQUFDLElBQUksQ0FBQzt3QkFDaEIsQ0FBQztxQkFDSjtvQkFDRDt3QkFDSSxJQUFJLEVBQUUsb0JBQW9CO3dCQUMxQixZQUFZLEVBQUUsSUFBSTt3QkFDbEIsSUFBSSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMscUJBQXFCLENBQUM7d0JBQ3BDLE1BQU0sRUFBRTs0QkFDSixjQUFjLEdBQUcsS0FBSyxDQUFDOzRCQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNoQixDQUFDO3FCQUNKO2lCQUFDO2FBQ1QsQ0FBQztZQUVGLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFlBQVk7Z0JBQzVFLEVBQUUsQ0FBQyxDQUFDLFlBQVksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUM1QixNQUFNLENBQUM7Z0JBQ1gsQ0FBQztnQkFDRCxNQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMvQixFQUFFLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQ2xCLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQzNDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDMUIsQ0FBQztJQWdDTyxnREFBdUIsR0FBL0I7UUFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLEtBQUssSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztZQUNwRCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDdkUsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUNqRCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7UUFDeEUsQ0FBQztRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVPLHlDQUFnQixHQUF4QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7ZUFDM0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQVNPLHVDQUFjLEdBQXRCO1FBQ0ksSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsR0FBRyxTQUFTLENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBQUEsQ0FBQztJQTNZWSxzQkFBTyxHQUFHLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsVUFBVTtRQUNuRSxnQkFBZ0IsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsa0JBQWtCO1FBQ3BGLGFBQWEsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLGlCQUFpQixDQUFDLENBQUM7SUEwWWxILHFCQUFDO0NBQUEsQUE3WUQsQ0FBNEMsMkJBQWdCLEdBNlkzRDtrQkE3WW9CLGNBQWMifQ==

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var PoolTypeEnum;
(function (PoolTypeEnum) {
    PoolTypeEnum[PoolTypeEnum["MainPoller"] = 0] = "MainPoller";
    PoolTypeEnum[PoolTypeEnum["AdditionalPoller"] = 1] = "AdditionalPoller";
})(PoolTypeEnum = exports.PoolTypeEnum || (exports.PoolTypeEnum = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbFR5cGUtZW51bS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBvb2xUeXBlLWVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx1Q0FBdUM7QUFDdkMsSUFBWSxZQUdYO0FBSEQsV0FBWSxZQUFZO0lBQ3BCLDJEQUFVLENBQUE7SUFDVix1RUFBZ0IsQ0FBQTtBQUNwQixDQUFDLEVBSFcsWUFBWSxHQUFaLG9CQUFZLEtBQVosb0JBQVksUUFHdkIifQ==

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PluginsData = /** @class */ (function () {
    function PluginsData() {
    }
    return PluginsData;
}());
exports.default = PluginsData;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGx1Z2luc0RhdGEtbW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwbHVnaW5zRGF0YS1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUlBO0lBQUE7SUFLQSxDQUFDO0lBQUQsa0JBQUM7QUFBRCxDQUFDLEFBTEQsSUFLQyJ9

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PoolConfiguration = /** @class */ (function () {
    function PoolConfiguration() {
    }
    return PoolConfiguration;
}());
exports.default = PoolConfiguration;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbENvbmZpZ3VyYXRpb24tbW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwb29sQ29uZmlndXJhdGlvbi1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQUE7SUFFQSxDQUFDO0lBQUQsd0JBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQyJ9

/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <p class=text-left _t> The host name <b>{{ pool.pluginsData.virtualHostNameResource.hostName }}</b> already exists in the selected server. The following IP addresses are assigned to this host name: <br/><br/> <b ng-repeat=\"ipAddress in resolvedIpAddresses\">{{ipAddress}}<br/></b> <br/><br/>If you continue, the existing DNS entry will be <b>overwritten.</b><br/> </p> </xui-dialog> ";

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content page-title={{vm.title}} link-icon=help link-text=\"_t(LEARN MORE ABOUT HIGH AVAILABILITY)\" link-url={{::vm.haDeploymentLink}} is-busy=vm.progressBar.show busy-message={{vm.progressBar.message}} _ta> <div class=\"ha-pool-page row\"> <xui-wizard hide-header=false ng-model=vm finish-text=\"{{ vm.getFinishWizardButtonText() }}\" on-finish=vm.finishWizard(cancellation) on-cancel=vm.navigationService.navigateToSummaryPage() current-step-index=vm.$scope.currentWizardStepIndex on-next-step=vm.validateAddPoolStep() _ta> <xui-wizard-step label=first short-title=\"_t(SERVER TO PROTECT)\" next-text=\"_t(Next >)\" title=\"_t(HIGH AVAILABILITY SERVER PROVIDING THE PROTECTION)\" ng-if=vm.$scope.shouldShowFirstWizardPage _ta> <ng-form name=availableServersForm> <div ng-init=\"vm.scopeFormInit('availableServersForm', availableServersForm)\"> <div class=clearfix> <div class=\"pull-left node-icon\"> <xui-icon icon={{vm.$scope.backupServer.icon}} status={{vm.$scope.backupServer.iconStatus}}></xui-icon> </div> <div class=\"pull-left node-details\"> <div class=text-caption> {{vm.$scope.backupServer.displayName}} </div> <div> <div> {{vm.$scope.backupServer.ipAddress}} </div> <div class=text-muted> {{vm.$scope.backupServer.description ? vm._t(vm.$scope.backupServer.description) : ''}} </div> </div> </div> </div> <br> <div> <div> <strong _t>Available Servers To Protect</strong> <strong>({{vm.$scope.pool.availableServers.length}})</strong> </div> <xui-sorter items-source=vm.$scope.availableServersGridSortingData.sortableColumns display-value=displayValue sort-direction=vm.$scope.availableServersGridSortingData.sortDirection selected-item=vm.$scope.availableServersGridSortingData.selectedItem on-change=vm.$scope.availableServersGridSortingData.sortChanged()> </xui-sorter> <xui-listview items-source=vm.$scope.pool.availableServers template-url=available-servers-grid-template selection=vm.$scope.availableServersSelection selection-mode=single> </xui-listview> <script type=text/ng-template id=available-servers-grid-template> <div class=\"clearfix\">\n                                        <div class=\"pull-left node-icon\">\n                                            <xui-icon icon=\"{{item.icon}}\" status=\"{{item.iconStatus}}\"></xui-icon>\n                                        </div>\n                                        <div class=\"pull-left node-details\">\n                                            <div class=\"text-caption\">\n                                                {{item.displayName}}\n                                            </div>\n                                            <div>\n                                                <div>\n                                                    {{item.ipAddress}}\n                                                </div>\n                                                <div class=\"text-muted\">\n                                                    {{item.description ? vm._t(item.description) : ''}}\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div> </script> </div> </div> </ng-form> </xui-wizard-step> <xui-wizard-step label=pool-properties title=\"_t(POOL PROPERTIES)\" on-enter=vm.onPoolPropertiesStepEnter() ng-if=true _ta> <xui-message name=poolPropertiesErrorMessage type=error ng-show=vm.$scope.errorMessage> <span ng-bind-html=vm.$scope.errorMessage></span> </xui-message> <ng-form name=poolPropertiesForm> <div ng-init=\"vm.scopeFormInit('poolPropertiesForm', poolPropertiesForm)\"> <div class=pull-right> <label _t>MEMBERS IN THIS POOL:</label> <poolmembers></poolmembers> </div> <poolproperties></poolproperties> <preferredmember></preferredmember> </div> </ng-form> </xui-wizard-step> <xui-wizard-step label=dns-settings ng-if=\"vm.$scope.pool.pluginsData.virtualHostNameResource.hostName || !vm.$scope.areMembersFromSameSubnet\" title=\"_t(DNS SETTINGS)\" description=\"_t(Provide the DNS server details and credentials.  Orion uses this information to update DNS entries when a failover occurs and a different pool member becomes the active server.)\" on-enter=vm.addPoolStepEnter() _ta> <ng-form name=dnsSettingsStepForm> <div ng-init=\"vm.scopeFormInit('dnsSettingsStepForm', dnsSettingsStepForm)\"> <div class=pull-right> <label _t>MEMBERS IN THIS POOL:</label> <poolmembers></poolmembers> </div> <div class=field> <dnssettings></dnssettings> </div> </div> </ng-form> </xui-wizard-step> <xui-wizard-step label=summary title=_t(SUMMARY) description=\"{{ vm.getSummaryStepDescription() }}\" ng-if=true on-enter=vm.addPoolStepEnter() _ta> <xui-message name=summaryErrorMessage type=error ng-show=vm.$scope.errorMessage> <span ng-bind-html=\"vm.response | actionResultModelErrorMessage\"></span> </xui-message> <div class=summary-box> <span><label _t>SERVER TO PROTECT</label></span> <span class=link ng-if=vm.$scope.shouldShowFirstWizardPage ng-click=vm.goToStep(vm.serverToProtectStep)>Edit</span> <poolmembers></poolmembers> </div> <div class=summary-box> <span><label _t>POOL PROPERTIES</label></span> <span class=link ng-click=vm.goToStep(vm.poolPropertiesStep)>Edit</span> <poolproperties is-read-only=true></poolproperties> </div> <div class=summary-box ng-if=\"vm.$scope.pool.pluginsData.virtualHostNameResource.hostName || !vm.$scope.areMembersFromSameSubnet\"> <span><label _t>DNS SETTINGS</label></span> <span class=link ng-click=vm.goToStep(vm.dnsSettingsStep)>Edit</span> <dnssettings is-read-only=true /> </div> </xui-wizard-step> </xui-wizard> </div> </xui-page-content> ";

/***/ }),
/* 45 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/ha_diagram.svg";

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var settings_config_1 = __webpack_require__(48);
exports.default = function (module) {
    module.config(settings_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFEQUErQztBQUUvQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyx5QkFBYyxDQUFDLENBQUM7QUFDbEMsQ0FBQyxDQUFDIn0=

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var settings_controller_1 = __webpack_require__(49);
function SummaryConfig($stateProvider) {
    $stateProvider
        .state("settings", {
        title: "High Availability Settings",
        url: "/ha/settings",
        controller: settings_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(50)
    });
}
SummaryConfig.$inject = ["$stateProvider"];
exports.default = SummaryConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MtY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2V0dGluZ3MtY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsdUNBQXVDO0FBQ3ZDLDZEQUF1RDtBQUV2RCx1QkFBdUIsY0FBb0M7SUFDdkQsY0FBYztTQUNULEtBQUssQ0FBQyxVQUFVLEVBQU87UUFDcEIsS0FBSyxFQUFFLDRCQUE0QjtRQUNuQyxHQUFHLEVBQUUsY0FBYztRQUNuQixVQUFVLEVBQUUsNkJBQWtCO1FBQzlCLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQVMsaUJBQWlCLENBQUM7S0FDL0MsQ0FBQyxDQUFDO0FBQ1gsQ0FBQztBQUVELGFBQWEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0FBRTNDLGtCQUFlLGFBQWEsQ0FBQyJ9

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SettingsController = /** @class */ (function () {
    function SettingsController($scope, $rootScope, $state, $log, haconstants, settingsService, $timeout, _t, xuiDialogService, navigationService, xuiToastService, demoService) {
        var _this = this;
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$log = $log;
        this.haconstants = haconstants;
        this.settingsService = settingsService;
        this.$timeout = $timeout;
        this._t = _t;
        this.xuiDialogService = xuiDialogService;
        this.navigationService = navigationService;
        this.xuiToastService = xuiToastService;
        this.demoService = demoService;
        this.$onInit = function () {
            _this.title = _this.$state.current.title;
            _this.loadSettings();
        };
        this.loadSettings = function () {
            _this.settingsService.getSettings().then(function (settings) {
                _this.$log.info("SettingsController:loadSettings()");
                _this.initSettingsFields(settings);
                _this.prepareSettingsData();
            });
        };
        this.getTemplate = function (model) {
            if (model.type === "boolean") {
                return "settings-listview-protection-template";
            }
            else {
                return "settings-listview-item-template";
            }
        };
        this.buttonDefaultClick = function () {
            _this.$scope.settingsFields.forEach(function (item) {
                //if item does not have default then set current value
                item.currentValue = _.isUndefined(item.defaultValue) ? item.currentValue : item.defaultValue;
            });
            _this.xuiToastService.success(_this._t("Settings have been successfully reset to their default values."), _this._t("RESET TO DEFAULT"));
        };
        this.buttonSaveClick = function () {
            var result = _this.xuiDialogService.showModal({}, {
                title: _this._t("Information"),
                message: _this._t("Settings will be changed for all pools in your environment.  " +
                    "Do you wish to continue?"),
                actionButtonText: _this._t("Save"),
                cancelButtonText: _this._t("Cancel"),
                buttons: [{
                        name: "save",
                        isPrimary: true,
                        text: _this._t("Save"),
                        action: function (dialog) {
                            _this.settingsService.updateSettings(_this.$scope.settingsFields).then(function (response) {
                                if (response === true) {
                                    _this.$scope.defaultValues = angular.copy(_this.$scope.settingsFields);
                                    _this.xuiToastService.success(_this._t("Changes were saved successfully."), _this._t("SAVE CHANGES"));
                                }
                                else {
                                    _this.xuiToastService.error(_this._t("An error has occurred while saving settings."), _this._t("SAVE CHANGES"));
                                    _this.loadSettings();
                                }
                            });
                            return true;
                        }
                    }]
            });
        };
        this.buttonCancelClick = function () {
            _this.$scope.settingsFields.forEach(function (item, index) {
                item.currentValue = _this.$scope.defaultValues[index].currentValue;
            });
            _this.xuiToastService.info(_this._t("Changes were reverted to their initial settings."), _this._t("CANCEL"));
        };
        this.prepareSettingsData = function () {
            if (_this.$scope.settingsFields) {
                _this.$timeout(function () {
                    _this.$scope.defaultValues = angular.copy(_this.$scope.settingsFields);
                }, 0);
            }
            else {
                _this.$log.info("Settings were not loaded.");
            }
        };
        this.updateSettings = function (settingsModels) {
            _this.settingsService.updateSettings(settingsModels);
        };
        this.navigateToSolarWindsOrionAdminAlertSettings = function () {
            _this.navigationService.navigateToSolarWindsOrionAdminAlertSettings();
        };
    }
    SettingsController.prototype.initSettingsFields = function (settings) {
        this.$scope.settingsFields = settings;
        this.$scope.defaultValues = [];
    };
    SettingsController.$inject = ["$scope", "$rootScope", "$state", "$log",
        "haconstants", "settingsService",
        "$timeout", "getTextService", "xuiDialogService", "navigationService",
        "xuiToastService", "demoService"];
    return SettingsController;
}());
exports.default = SettingsController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNldHRpbmdzLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFZQTtJQVFJLDRCQUFtQixNQUFzQixFQUM3QixVQUFnQyxFQUNoQyxNQUE0QixFQUM1QixJQUFvQixFQUNwQixXQUFzQixFQUN0QixlQUFpQyxFQUNqQyxRQUE0QixFQUM1QixFQUFvQyxFQUNwQyxnQkFBb0MsRUFDcEMsaUJBQW9DLEVBQ3BDLGVBQWtDLEVBQ2xDLFdBQXdCO1FBWHBDLGlCQVd3QztRQVhyQixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUM3QixlQUFVLEdBQVYsVUFBVSxDQUFzQjtRQUNoQyxXQUFNLEdBQU4sTUFBTSxDQUFzQjtRQUM1QixTQUFJLEdBQUosSUFBSSxDQUFnQjtRQUNwQixnQkFBVyxHQUFYLFdBQVcsQ0FBVztRQUN0QixvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFDakMsYUFBUSxHQUFSLFFBQVEsQ0FBb0I7UUFDNUIsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDcEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFvQjtRQUNwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLG9CQUFlLEdBQWYsZUFBZSxDQUFtQjtRQUNsQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUU3QixZQUFPLEdBQUc7WUFDYixLQUFJLENBQUMsS0FBSyxHQUFTLEtBQUksQ0FBQyxNQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUM5QyxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDeEIsQ0FBQyxDQUFDO1FBRUssaUJBQVksR0FBRztZQUNsQixLQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQXlCO2dCQUM5RCxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO2dCQUNwRCxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2xDLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQy9CLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRUssZ0JBQVcsR0FBRyxVQUFDLEtBQW9CO1lBQ3RDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsTUFBTSxDQUFDLHVDQUF1QyxDQUFDO1lBQ25ELENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLENBQUMsaUNBQWlDLENBQUM7WUFDN0MsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVLLHVCQUFrQixHQUFHO1lBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQW1CO2dCQUNuRCxzREFBc0Q7Z0JBQ3RELElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDakcsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLGdFQUFnRSxDQUFDLEVBQ2xHLEtBQUksQ0FBQyxFQUFFLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBRXJDLENBQUMsQ0FBQztRQUVLLG9CQUFlLEdBQUc7WUFDckIsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FDMUMsRUFBRSxFQUNGO2dCQUNJLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsK0RBQStEO29CQUM1RSwwQkFBMEIsQ0FBQztnQkFDL0IsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7Z0JBQ2pDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNuQyxPQUFPLEVBQUUsQ0FBQzt3QkFDTixJQUFJLEVBQUUsTUFBTTt3QkFDWixTQUFTLEVBQUUsSUFBSTt3QkFDZixJQUFJLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7d0JBQ3JCLE1BQU0sRUFBRSxVQUFDLE1BQTBCOzRCQUMvQixLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQWlCO2dDQUNuRixFQUFFLENBQUMsQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztvQ0FDcEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29DQUNyRSxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLGtDQUFrQyxDQUFDLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dDQUN2RyxDQUFDO2dDQUFDLElBQUksQ0FBQyxDQUFDO29DQUNKLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsOENBQThDLENBQUMsRUFDOUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO29DQUM3QixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0NBQ3hCLENBQUM7NEJBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQzt3QkFDaEIsQ0FBQztxQkFDSixDQUFDO2FBQ0wsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRUssc0JBQWlCLEdBQUc7WUFDdkIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBbUIsRUFBRSxLQUFhO2dCQUNsRSxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQztZQUN0RSxDQUFDLENBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsa0RBQWtELENBQUMsRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDOUcsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUc7WUFDekIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUU3QixLQUFJLENBQUMsUUFBUSxDQUFDO29CQUNWLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDekUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ1YsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDaEQsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVLLG1CQUFjLEdBQUcsVUFBQyxjQUFvQztZQUN6RCxLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN4RCxDQUFDLENBQUM7UUFFSyxnREFBMkMsR0FBRztZQUNqRCxLQUFJLENBQUMsaUJBQWlCLENBQUMsMkNBQTJDLEVBQUUsQ0FBQztRQUN6RSxDQUFDLENBQUM7SUF6RnFDLENBQUM7SUEyRmhDLCtDQUFrQixHQUExQixVQUEyQixRQUF5QjtRQUNoRCxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUM7UUFDdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFoSGEsMEJBQU8sR0FBRyxDQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLE1BQU07UUFDN0QsYUFBYSxFQUFFLGlCQUFpQjtRQUNoQyxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsa0JBQWtCLEVBQUUsbUJBQW1CO1FBQ3JFLGlCQUFpQixFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBOEcxQyx5QkFBQztDQUFBLEFBbEhELElBa0hDO2tCQWxIb0Isa0JBQWtCIn0=

/***/ }),
/* 50 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content> <xui-page-header> <div class=ha-settings-page-header> <xui-button ui-sref=summary icon=list-all display-style=tertiary class=\"pull-right ha-menu\"> <span _t>High Availability Summary</span> </xui-button> <xui-button ng-click=vm.navigateToSolarWindsOrionAdminAlertSettings() icon=gear display-style=tertiary class=\"pull-right ha-menu\"> <span _t>Email Settings</span> </xui-button> <h1 class=ha-title _t>High Availability Settings</h1> </div> </xui-page-header> <div class=ha-settings-page-toolbar> <xui-toolbar> <xui-toolbar-item> <xui-button icon=reset ng-click=vm.buttonDefaultClick() display-style=tertiary> <span _t>Reset To Default</span> </xui-button> </xui-toolbar-item> </xui-toolbar> </div> <ng-form name=vm.settingsForm> <div class=ha-settings-page> <xui-listview stripe=true items-source=vm.$scope.settingsFields template-fn=vm.getTemplate></xui-listview> </div> </ng-form> <div class=ha-settings-page-footer> <xui-page-footer> <div class=text-left> <xui-button ng-click=vm.buttonSaveClick() display-style=primary> <span _t>Save Changes</span> </xui-button> <xui-button ng-click=vm.buttonCancelClick() display-style=tertiary> <span _t>Cancel</span> </xui-button> </div> </xui-page-footer> </div> </xui-page-content> <script type=text/ng-template id=settings-listview-protection-template> <div class=\"container fields\">\n        <div class=\"col-md-5\">\n            <div class=\"ha-settings-page-name\">\n                <p>{{::item.name}}</p>\n            </div>\n        </div>\n        <div class=\"col-md-2\">\n            <div class=\"ha-settings-page-value\">\n                <xui-switch ng-model=\"item.currentValue\"\n                            ng-click=\"$parent.$emit('xuiSettingsSwitchClick', item.currentValue);\"></xui-switch>\n            </div>\n        </div>\n        <div class=\"col-xs-1 col-sm-1 col-md-1\">\n            <div class=\"ha-settings-page-units\">\n                <span ng-show=\"item.currentValue\" _t>\n                    On\n                </span>\n                <span ng-show=\"!item.currentValue\" _t>\n                    Off\n                </span>\n            </div>\n        </div>\n    </div> </script> <script type=text/ng-template id=settings-listview-item-template> <div class=\"container fields\">\n        <div class=\"col-md-5\">\n            <div class=\"ha-settings-page-name\">\n                {{::item.name}}\n            </div>\n        </div>\n        <div class=\"col-md-2\">\n            <div class=\"ha-settings-page-value\">\n                <xui-textbox class=\"no-label\" type=\"{{::item.type}}\"\n                             ng-model=\"item.currentValue\"\n                             validators=\"required,min={{::item.minimum}},max={{::item.maximum}}\">\n                    <div ng-message=\"required\" _t>This field is required</div>\n                    <div ng-message=\"min\" _t=\"['{{::item.minimum}}']\">Value must be {0} or higher</div>\n                    <div ng-message=\"max\" _t=\"['{{::item.maximum}}']\">Value must be {0} or lower</div>\n                </xui-textbox>\n            </div>\n        </div>\n        <div class=\"col-sm-3 col-md-1 col-lg-1\">\n            <div class=\"ha-settings-page-units\">\n                {{::item.unitsDisplayValue}}\n            </div>\n        </div>\n        <div class=\"col-sm-6 col-md-4 col-lg-4\">\n            <div>\n                <span class=\"text-muted\" ng-show=\"{{::item.defaultValue !== '' }}\">\n                    <span _t>Default</span>: {{::item.defaultValue}}\n                </span>\n                <span class=\"text-muted\" ng-show=\"{{::item.hint !== ''}}\">\n                    {{::item.hint}}\n                </span>\n            </div>\n        </div>\n    </div> </script> ";

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var summary_config_1 = __webpack_require__(52);
var setup_1 = __webpack_require__(59);
__webpack_require__(70);
__webpack_require__(71);
exports.default = function (module) {
    module.config(summary_config_1.default);
    setup_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1EQUE2QztBQUM3QyxpQ0FBNEI7QUFDNUIsMEJBQXdCO0FBQ3hCLHdCQUFzQjtBQUV0QixrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyx3QkFBYSxDQUFDLENBQUM7SUFDN0IsZUFBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ2xCLENBQUMsQ0FBQyJ9

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var summary_controller_1 = __webpack_require__(53);
function SummaryConfig($stateProvider) {
    $stateProvider
        .state("deployment.summary", {
        title: "High Availability Deployment Summary",
        url: "/summary?serverName?demo",
        params: { poolId: null, poolOperation: null, ignoreLicenseCheck: false },
        controller: summary_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(58)
    })
        .state("summary", {
        url: "/ha/summary?serverName?demo",
        params: { poolId: null, poolOperation: null, ignoreLicenseCheck: false },
        redirectTo: "deployment.summary"
    });
}
SummaryConfig.$inject = ["$stateProvider"];
exports.default = SummaryConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VtbWFyeS1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdW1tYXJ5LWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHVDQUF1QztBQUN2QywyREFBcUQ7QUFFckQsdUJBQXVCLGNBQW9DO0lBQ3ZELGNBQWM7U0FDVCxLQUFLLENBQUMsb0JBQW9CLEVBQU87UUFDOUIsS0FBSyxFQUFFLHNDQUFzQztRQUM3QyxHQUFHLEVBQUUsMEJBQTBCO1FBQy9CLE1BQU0sRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxrQkFBa0IsRUFBRSxLQUFLLEVBQUU7UUFDeEUsVUFBVSxFQUFFLDRCQUFpQjtRQUM3QixZQUFZLEVBQUUsSUFBSTtRQUNsQixRQUFRLEVBQUUsT0FBTyxDQUFTLGdCQUFnQixDQUFDO0tBQzlDLENBQUM7U0FDRCxLQUFLLENBQUMsU0FBUyxFQUFRO1FBQ3BCLEdBQUcsRUFBRSw2QkFBNkI7UUFDbEMsTUFBTSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLGtCQUFrQixFQUFFLEtBQUssRUFBRTtRQUN4RSxVQUFVLEVBQUUsb0JBQW9CO0tBQ25DLENBQUMsQ0FBQztBQUNYLENBQUM7QUFDRCxhQUFhLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztBQUUzQyxrQkFBZSxhQUFhLENBQUMifQ==

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="../../ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var orionServer_model_1 = __webpack_require__(4);
var pool_model_1 = __webpack_require__(2);
var orionServerPollingEngine_enum_1 = __webpack_require__(1);
var runType_enum_1 = __webpack_require__(5);
var poolMemberStatus_enum_1 = __webpack_require__(54);
var sumary_item_property_1 = __webpack_require__(13);
var poolOperation_enum_1 = __webpack_require__(16);
var SummaryController = /** @class */ (function () {
    function SummaryController($scope, $state, xuiDialogService, $log, _t, $rootScope, summaryService, navigationService, xuiToastService, sortingService, $q, $timeout, $interval, demoService, haconstants) {
        var _this = this;
        this.$scope = $scope;
        this.$state = $state;
        this.xuiDialogService = xuiDialogService;
        this.$log = $log;
        this._t = _t;
        this.$rootScope = $rootScope;
        this.summaryService = summaryService;
        this.navigationService = navigationService;
        this.xuiToastService = xuiToastService;
        this.sortingService = sortingService;
        this.$q = $q;
        this.$timeout = $timeout;
        this.$interval = $interval;
        this.demoService = demoService;
        this.haconstants = haconstants;
        this.dataModel = [];
        this.sortDirection = "asc";
        this.$onInit = function () {
            _this.treeOptions = { selectionMode: "single", onSelect: _this.selectNode };
            _this.title = _this.$state.current.title;
            _this.sortItems = [
                { name: "name", value: _this._t("Name") },
                { name: "poolName", value: _this._t("Pool name") },
                { name: "status", value: _this._t("Status") },
                { name: "engineType", value: _this._t("Type") }
            ];
            _this.selectedItem = _this.sortItems[0];
            _this.updateTree();
            _this.$interval(function () {
                _this.updateTree();
            }, _this.haconstants.summary.updateInterval);
            _this.showToast();
            //License activation dialog options
            _this.activateLicenseDialogTitle = _this._t("Activate License");
            _this.xuiSidebarSettings = {
                display: true,
                position: "right",
                shrink: true
            };
            _this.$scope.$watch("vm.inspectedNode.$model.enabled", function (newValue, oldValue) {
                if (oldValue === undefined) {
                    return;
                }
                if (_this.treeContext && _this.treeContext.selectedNode && _this.inspectedNode.$model.itemType === "pool") {
                    _this.isBusy = true;
                    _this.summaryService.updateHaPoolState(_this.treeContext.selectedNode.$model.poolId, newValue).then(function () { return _this.isBusy = false; });
                }
            });
        };
        this.buildTree = function () {
            var tree = [].concat(_this.pools);
            var i;
            var couldCreatePool = function (server) {
                return !_.isEmpty(_this.getAvailableServers(server)) &&
                    server.poolId === 0 &&
                    server.poolMemberId > 0 &&
                    server.runType === runType_enum_1.RunType.Standby;
            };
            for (i = 0; i < _this.orionServers.length; i++) {
                if (_this.orionServers[i].poolId > 0) {
                    var serverPool = _this.pools.filter(function (p) {
                        return p.poolId === _this.orionServers[i].poolId;
                    })[0];
                    if (serverPool) {
                        if (!serverPool.children) {
                            serverPool.children = [];
                        }
                        serverPool.children.push(_this.orionServers[i]);
                        _this.addFilledInstalledModulesInfoProperties(_this.orionServers[i], serverPool);
                    }
                }
                else {
                    if (_this.orionServers[i].runType === runType_enum_1.RunType.Standby) {
                        _this.orionServers[i].addToHAPool = _this.evaluate;
                        _this.orionServers[i].canAddToHAPool = couldCreatePool;
                    }
                    tree.push(_this.orionServers[i]);
                }
            }
            _this.dataModel = tree;
        };
        this.evaluate = function (item, evaluate) {
            if (evaluate === void 0) { evaluate = false; }
            if (evaluate) {
                _this.activateEvaluationForHA(function () { _this.addToHAPool(item); });
            }
            else {
                _this.summaryService.isAvailableLicenseToCreateHAPool()
                    .then(function (response) {
                    _this.$scope.licenseAvailabilityResult = response.result;
                    _this.showActivateHAMessage = !response.result.poolLicenseIsAvailable
                        && response.result.isLicenseStoreAvailable;
                    _this.showLicenseStoreNotAvailableMessage = !response.result.isLicenseStoreAvailable;
                    if (response.result.evaluationExpired) {
                        _this.showLicenseExpiredMessage = true;
                        _this.showActivateHAMessage = false;
                    }
                    if (!response.result.poolLicenseIsAvailable || response.result.evaluationExpired
                        || !response.result.isLicenseStoreAvailable) {
                        _this.errorMessage = "";
                    }
                    else {
                        _this.addToHAPool(item);
                    }
                });
            }
        };
        this.addToHAPool = function (item) {
            if (item.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPollerStandby) {
                _this.summaryService.getListOfAPENotInPool(item.poolMemberId)
                    .then(function (response) {
                    _this.availableServers = response;
                    _this.navigateToAddPoolPage(item);
                });
            }
            else {
                _this.availableServers = _this.getAvailableServers(item);
                if (!_.isEmpty(_this.availableServers) && _this.availableServers.length > 1
                    && item.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPollerStandby) {
                    _this.$log.log("Multiple main pollers were found in the database.");
                    _this.errorMessage = _this._t("Multiple main pollers were found in the database.");
                }
                else {
                    _this.navigateToAddPoolPage(item);
                }
            }
        };
        this.inspectItem = function (node) {
            if (node.$model) {
                _this.localize(node.$model);
            }
            _this.inspectedNode = node;
            var elem = angular.element(".xui-sidebar-container");
            if (!elem.hasClass("xui-sidebar-container-opened")) {
                elem.addClass("xui-sidebar-container-opened");
            }
            node.checked = true;
            _this.xuiSidebarSettings.display = true;
        };
        this.navigateToHAReport = function () {
            _this.summaryService.getReportUrl().then(function (reportPath) {
                _this.navigationService.navigateToHAReportPage(reportPath);
            });
        };
        this.forceFailover = function () {
            _this.xuiDialogService.showModal({}, {
                status: "warning",
                title: _this._t("Manual Failover"),
                message: _this._t("This will cause a manual failover to the standby server.") + "<br />" +
                    _this._t("If active pool member is the preferred active server, the HA software will failback" +
                        " to the preferred server at the end of the self-recovery period.") + "<br /><br />" +
                    _this._t("Do you wish to continue?") + "<br />",
                cancelButtonText: _this._t("Cancel"),
                buttons: [{
                        name: "Ok",
                        isPrimary: true,
                        text: _this._t("Ok"),
                        action: function (dialog) {
                            _this.summaryService.switchoverHaPool(_this.treeContext.selectedNode.$model.poolId)
                                .then(function (response) {
                                if (response.errorCode && response.errorCode > 0) {
                                    _this.errorMessage = response.errorMessage;
                                    return false;
                                }
                                _this.xuiToastService.success(_this._t("Manual Failover has started. " +
                                    "Once the failover is complete, " +
                                    "refresh your browser to view the Orion Web Console."));
                                return true;
                            });
                            return true;
                        }
                    }]
            });
        };
        this.editPool = function () {
            _this.navigationService.navigateToAddPoolPage({ "poolData": _this.treeContext.selectedNode.$model });
        };
        this.removePool = function () {
            var activeServer = _.find(_this.treeContext.selectedNode.$model.children, function (server) {
                return server.runType === runType_enum_1.RunType.Active;
            });
            if (activeServer === undefined) {
                _this.$log.error("There is no active server in the pool!");
                return false;
            }
            var result = _this.xuiDialogService.showModal({}, {
                status: "warning",
                title: _this._t("Remove pool"),
                message: _this._t("Are you sure you want to remove a pool?"),
                cancelButtonText: _this._t("No"),
                buttons: [{
                        name: "delete",
                        isPrimary: true,
                        text: _this._t("Yes"),
                        action: function (dialog) {
                            _this.summaryService.deleteHaPool(_this.treeContext.selectedNode.$model.poolId)
                                .then(function (response) {
                                if (response.errorCode === 0) {
                                    _this.xuiToastService.success(_this._t("Pool removed successfully"));
                                    _this.$state.reload();
                                }
                                else {
                                    _this.errorMessage = "" + response.errorMessage;
                                }
                            });
                            return true;
                        }
                    }]
            });
        };
        this.canStaleEngineBeRemoved = function () {
            return _this.inspectedNode.$model.runType === runType_enum_1.RunType.Standby &&
                _this.inspectedNode.$model.poolId === 0 &&
                _this.inspectedNode.$model.status === poolMemberStatus_enum_1.PoolMemberStatus.Down;
        };
        this.removeStaleEngine = function () {
            var result = _this.xuiDialogService.showModal({}, {
                status: "warning",
                title: _this._t("Delete member"),
                message: _this._t("Are you sure you want to delete the member\n                " + _this.treeContext.selectedNode.$model.displayName + " from the HA Deployment Summary view?"),
                cancelButtonText: _this._t("No"),
                buttons: [{
                        name: "OK",
                        isPrimary: true,
                        text: _this._t("OK"),
                        action: function (dialog) {
                            _this.summaryService.deleteStaleEngine(_this.treeContext.selectedNode.$model.hostName)
                                .then(function (response) {
                                if (response.errorCode === 0) {
                                    _this.xuiToastService.success(_this._t("Stale engine removed successfully"));
                                    _this.$state.reload();
                                }
                                else {
                                    _this.errorMessage = "" + response.errorMessage;
                                }
                            });
                            return true;
                        }
                    }]
            });
        };
        this.setUpHALicensing = function (setupNewHAServer) {
            if (setupNewHAServer === void 0) { setupNewHAServer = false; }
            _this.$scope.activation = "activate";
            //TODO: Remove this assignment
            _this.$scope.pricingLink = _this.haconstants.links.haLicensingPricing;
            _this.xuiDialogService.showModal({ template: __webpack_require__(55) }, {
                title: _this._t("Set Up a High Availability Server"),
                viewModel: _this.$scope,
                cancelButtonText: _this._t("Cancel"),
                buttons: [{
                        name: "next",
                        isPrimary: true,
                        text: _this._t("Next >"),
                        action: function (result) {
                            if (_this.$scope.activation === "evaluate") {
                                _this.canEvaluate(setupNewHAServer);
                            }
                            else {
                                _this.$timeout(function () {
                                    angular.element(".ha-licensing-modal").click();
                                }, 0);
                            }
                            return true;
                        }
                    }]
            });
        };
        this.canEvaluate = function (setupNewHAServer) {
            _this.summaryService.canActivateHAEvaluation()
                .then(function (canActivate) {
                if (canActivate === true) {
                    if (setupNewHAServer) {
                        // Setup HA server (show download dialog)
                        _this.activateEvaluationForHA(function () { ; });
                        _this.showDownloadDialog();
                    }
                    else {
                        // Create pool (navigate to pool page)
                        _this.evaluate(_this.treeContext.selectedNode.$model, true);
                    }
                }
                else {
                    _this.xuiToastService.error("High Availability can't be evaluated", null);
                }
            });
        };
        this.setUpAvailabilityServer = function () {
            var setupDialogOptions = {
                title: _this._t("GETTING STARTED WITH HIGH AVAILABILITY"),
                buttons: [{
                        name: "SetUpServer",
                        isPrimary: true,
                        text: _this._t("GET STARTED SETTING UP A SERVER"),
                        action: function (setupDialogResult) {
                            _this.downloadOrionInstaller();
                            return true;
                        }
                    }]
            };
            _this.xuiDialogService.showModal({
                template: __webpack_require__(56)
            }, setupDialogOptions)
                .then(function (response) {
                if (response === "setUpServer") {
                    _this.downloadOrionInstaller();
                }
            });
        };
        this.sortTree = function (newValue, oldValue) {
            //Put pools on top when sort by poolName.
            if (newValue.sortBy.name === "poolName") {
                _this.dataModel = _this.sortingService.sortBy(_this.dataModel, "poolId", "desc");
            }
            _this.dataModel = _this.sortingService.sortBy(_this.dataModel, newValue.sortBy.name, newValue.direction);
        };
        this.shouldShowResourcesWarnMessage = function () {
            return _this.$state.params["poolId"] && _this.createdPool && _this.createdPool.pluginsData &&
                (_this.createdPool.pluginsData.virtualIpResource || _this.createdPool.pluginsData.virtualHostNameResource);
        };
        this.getResourcesMessage = function () {
            var message = "";
            if (_this.createdPool && _this.createdPool.pluginsData) {
                if (_this.createdPool.pluginsData.virtualIpResource) {
                    message = message + _this._t("virtual IP address")
                        + (" (" + _this.createdPool.pluginsData.virtualIpResource.ipAddress + ")");
                }
                if (_this.createdPool.pluginsData.virtualHostNameResource) {
                    if (_this.createdPool.pluginsData.virtualIpResource) {
                        message = message + " " + _this._t("or") + " ";
                    }
                    message = message + _this._t("virtual host name ")
                        + (" (" + _this.createdPool.pluginsData.virtualHostNameResource.hostName)
                        + ("." + _this.createdPool.pluginsData.virtualHostNameResource.dnsZone + ")");
                }
            }
            return message;
        };
        this.onLicenseActivate = function (result) {
            if (result.status.toLowerCase() === "success") {
                _this.xuiToastService.success("Activation has been performed successfully");
                _this.$state.reload();
            }
        };
        this.activateEvaluationForHA = function (fun) {
            _this.summaryService.activateEvaluationForHA().then(function (activated) {
                if (activated) {
                    fun();
                    _this.xuiToastService.success(_this._t("Evaluation has been started"), null);
                }
                else {
                    _this.xuiToastService.error(_this._t("Evaluation can't be started"), null);
                }
            });
        };
        this.addFilledInstalledModulesInfoProperties = function (orionServer, serverPool) {
            var prop = new sumary_item_property_1.default();
            prop.key = orionServer.displayName;
            prop.value = orionServer.installedModulesOk.join(", ");
            if (orionServer.installedModulesOk.length > 0 &&
                orionServer.installedModulesMismatch.length > 0) {
                prop.value += ", <span class=\"mismatch\">" + orionServer.installedModulesMismatch.join(", ") + "</span>";
            }
            serverPool.properties.push(prop);
        };
        this.selectNode = function (event, node) {
            _this.isBusy = true;
            _this.$q(function (resolve) { return _this.$timeout(function () { return resolve(); }, 0); })
                .then(function () {
                _this.updateTree();
            }).then(function () {
                _this.inspectItem(node);
            });
        };
        this.updateTree = function () {
            //TODO: get rid of this when tree will be able to save/refresh state
            var focusedNode = (_this.treeContext) ? _this.treeContext.focusedNode : null;
            _this.$q.all([
                _this.summaryService.getOrionServers(),
                _this.summaryService.getPools()
            ]).then(function (results) {
                if (_this.orionServers && _this.pools
                    && _this.summaryArraysEquals(_this.orionServers, results[0])
                    && _this.summaryArraysEquals(_this.pools, results[1])) {
                    _this.isBusy = false;
                    return false;
                }
                _this.orionServers = results[0];
                _this.pools = results[1];
                _this.buildTree();
                _this.sortTree({ sortBy: { name: _this.selectedItem.name }, direction: _this.sortDirection });
                _this.restoreTreeState(focusedNode);
                _this.isBusy = false;
            });
        };
        this.localizeOrionServer = function (server) {
            if (!server.properties) {
                return;
            }
            for (var _i = 0, _a = server.properties; _i < _a.length; _i++) {
                var property = _a[_i];
                switch (property.key) {
                    case "polling Completion":
                    case "elements":
                        break;
                    case "Polling Rate":
                        property.value = property.value + " " + _this._t("% of polling capacity");
                        break;
                    case "last Database Update":
                        var index = property.value.indexOf(" ");
                        property.value = property.value.substring(0, index) + " " + _this._t(property.value.substring(index + 1));
                        break;
                    default:
                        if (typeof property.value === "string") {
                            property.value = _this._t(property.value);
                        }
                }
                if (typeof property.key === "string") {
                    property.key = _this._t(property.key);
                }
            }
        };
        this.localizePool = function (pool) {
            if (!pool.properties) {
                return;
            }
            for (var _i = 0, _a = pool.properties; _i < _a.length; _i++) {
                var property = _a[_i];
                property.key = _this._t(property.key);
                if (typeof property.value === "string") {
                    property.value = _this._t(property.value);
                }
            }
        };
        this.summaryArraysEquals = function (array1, array2) {
            if (!array1.length && !array2.length) {
                return true;
            }
            var newArray1 = angular.copy(array1);
            _.forEach(newArray1, function (item) {
                delete item.lastDatabaseUpdate;
                delete item.properties;
                delete item.children;
            });
            var newArray2 = angular.copy(array2);
            _.forEach(newArray2, function (item) {
                delete item.lastDatabaseUpdate;
                delete item.properties;
                delete item.children;
            });
            return angular.toJson(newArray1) === angular.toJson(newArray2);
        };
        //TODO: function will be changed when tree will be able to save/refresh state
        this.restoreTreeState = function (node) {
            _this.$q(function (resolve) { return _this.$timeout(function () { return resolve(); }, 0); })
                .then(function () {
                if (!node) {
                    _this.selectDefaultNode();
                    return false;
                }
                var focusedNode = {};
                _this.treeContext.rootNode.$children.forEach(function (item) {
                    if (item.$model.name === node.$model.name) {
                        focusedNode = item;
                        focusedNode.collapsed = node.collapsed;
                        return false;
                    }
                    if (item.$hasChildren) {
                        item.$children.forEach(function (childItem) {
                            if (childItem.$model.name === node.$model.name) {
                                focusedNode = childItem;
                                focusedNode.$parent.collapsed = false;
                                return false;
                            }
                        });
                    }
                });
                _this.treeContext.focusedNode = focusedNode;
                _this.treeContext.focusedNode.checked = node.checked;
                _this.treeContext.selectedNode = focusedNode;
            });
        };
        this.downloadOrionInstaller = function () {
            _this.summaryService.isAvailableLicenseToCreateHAPool()
                .then(function (response) {
                _this.$scope.licenseAvailabilityResult = response.result;
                if (!response.result.poolLicenseIsAvailable && response.result.isLicenseStoreAvailable
                    || response.result.evaluationExpired) {
                    return _this.setUpHALicensing(true);
                }
                return _this.showDownloadDialog();
            });
        };
        this.showDownloadDialog = function () {
            var downloadDialogOptions = {
                title: _this._t("Set Up a High Availability Server"),
                hideCancel: true,
                buttons: [{
                        name: "SetUpServer",
                        isPrimary: true,
                        text: _this._t("DOWNLOAD INSTALLER NOW"),
                        action: function (downloadDialogResult) {
                            _this.navigationService.downloadSolarWindsOrionInstaller();
                            return true;
                        }
                    }]
            };
            _this.xuiDialogService.showModal({
                template: __webpack_require__(57)
            }, downloadDialogOptions);
        };
        this.navigateToAddPoolPage = function (item) {
            if (!_.isEmpty(_this.availableServers)) {
                var poolData = new pool_model_1.default();
                poolData.availableServers = _this.availableServers;
                poolData.poolMembersIds = [item.poolMemberId];
                poolData.children = [new orionServer_model_1.default(item)];
                _this.navigationService.navigateToAddPoolPage({ "poolData": poolData });
            }
            else {
                _this.$log.log("No servers are available to create a pool.");
                _this.showActivateHAMessage = false;
                _this.errorMessage = _this._t("There are no servers available to create a pool.");
            }
        };
        this.getAvailableServers = function (item) {
            var serversList = _this.orionServers.filter(function (server) {
                //fast fix to allow AP pools, make this properly in HA-1201, add PoolType and base comparison on that
                var validServerType = item.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPoller
                    ? orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPollerStandby :
                    item.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPollerStandby
                        ? orionServerPollingEngine_enum_1.OrionServerPollingEngine.mainPoller :
                        item.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPoller
                            ? orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPollerStandby :
                            item.pollingEngineType === orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPollerStandby
                                ? orionServerPollingEngine_enum_1.OrionServerPollingEngine.additionalPoller :
                                -1;
                return server.pollingEngineType === validServerType && server.poolId === 0
                    && item.poolMemberId !== server.poolMemberId;
            });
            return serversList.length === 0 ? null : serversList;
        };
        this.showToast = function () {
            //TODO: add conditions to show toast message based on backend response
            if (_this.$state.params["serverName"]) {
                var title = _this.$state.params["serverName"] + " has been successfully installed.";
                var message = "Add this server together with main poller into the High Availability pool " +
                    "to finish the HA protection setup. В»" +
                    "<a href='#' _t> Set Up HA Pool</a>";
                _this.xuiToastService.success(message, title);
            }
        };
        this.selectDefaultNode = function () {
            var pool = _.find(_this.treeContext.rootNode.$children, (function (item) { return item.$model.poolId === _this.$state.params["poolId"] && item.$model.itemType === "pool"; }));
            if (pool) {
                var unbindWatcher_1 = _this.$scope.$watch(function () { return _this.treeContext; }, function (initialTreeContext) {
                    if (initialTreeContext) {
                        _this.createdPool = _.cloneDeep(pool.$model);
                        var operation = _this.$state.params["poolOperation"];
                        var operationMsg = operation === poolOperation_enum_1.PoolOperationEnum.Create
                            ? _this._t("created")
                            : _this._t("edited");
                        var message = pool.$model.name + " " + _this._t("has been successfully") + " " + operationMsg + ".";
                        _this.xuiToastService.success(message, null, {}, pool);
                        _this.selectTreeNode(pool);
                        // only ignore license check right after pool creation
                        // as license doesn't have to be propagated yet to backup
                        if (_this.$state.params["ignoreLicenseCheck"]) {
                            _this.$state.params["ignoreLicenseCheck"] = false;
                            _this.treeContext.selectedNode.$model.isLicensed = true;
                        }
                        unbindWatcher_1();
                    }
                });
            }
            else {
                var unbindWatcher_2 = _this.$scope.$watch(function () { return _this.treeContext; }, function (initialTreeContext) {
                    if (initialTreeContext && initialTreeContext.rootNode.$children[0]) {
                        _this.selectTreeNode(_this.treeContext.rootNode.$children[0]);
                        unbindWatcher_2();
                    }
                });
            }
        };
        this.selectTreeNode = function (node) {
            _this.treeContext.selectedNode = node;
            _this.treeContext.focusedNode = node;
            _this.inspectItem(node);
            if (node.$hasChildren) {
                node.collapsed = false;
            }
        };
    }
    SummaryController.prototype.localize = function (nodeModel) {
        if (nodeModel.itemType === "orionServer") {
            this.localizeOrionServer(nodeModel);
            return;
        }
        if (nodeModel.itemType === "pool") {
            this.localizePool(nodeModel);
            return;
        }
    };
    SummaryController.$inject = [
        "$scope",
        "$state",
        "xuiDialogService",
        "$log",
        "getTextService",
        "$rootScope",
        "summaryService",
        "navigationService",
        "xuiToastService",
        "xuiSortingService",
        "$q",
        "$timeout",
        "$interval",
        "demoService",
        "haconstants"
    ];
    return SummaryController;
}());
exports.default = SummaryController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VtbWFyeS1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3VtbWFyeS1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx1Q0FBdUM7O0FBRXZDLDJFQUFxRTtBQUVyRSwyREFBcUQ7QUFLckQsbUdBQTZGO0FBRTdGLGlFQUEyRDtBQUMzRCxtRkFBNkU7QUFDN0Usa0ZBQTRFO0FBRzVFLDJFQUF5RTtBQVN6RTtJQWlESSwyQkFBb0IsTUFBcUIsRUFDN0IsTUFBMkIsRUFDM0IsZ0JBQW9DLEVBQ3BDLElBQW9CLEVBQ3BCLEVBQW9DLEVBQ3BDLFVBQWdDLEVBQ2hDLGNBQStCLEVBQy9CLGlCQUFvQyxFQUNwQyxlQUFrQyxFQUNsQyxjQUFtQyxFQUNuQyxFQUFnQixFQUNoQixRQUE0QixFQUM1QixTQUE4QixFQUM5QixXQUF3QixFQUN6QixXQUFzQjtRQWRqQyxpQkFjcUM7UUFkakIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUM3QixXQUFNLEdBQU4sTUFBTSxDQUFxQjtRQUMzQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9CO1FBQ3BDLFNBQUksR0FBSixJQUFJLENBQWdCO1FBQ3BCLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBQ3BDLGVBQVUsR0FBVixVQUFVLENBQXNCO1FBQ2hDLG1CQUFjLEdBQWQsY0FBYyxDQUFpQjtRQUMvQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLG9CQUFlLEdBQWYsZUFBZSxDQUFtQjtRQUNsQyxtQkFBYyxHQUFkLGNBQWMsQ0FBcUI7UUFDbkMsT0FBRSxHQUFGLEVBQUUsQ0FBYztRQUNoQixhQUFRLEdBQVIsUUFBUSxDQUFvQjtRQUM1QixjQUFTLEdBQVQsU0FBUyxDQUFxQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN6QixnQkFBVyxHQUFYLFdBQVcsQ0FBVztRQWxDMUIsY0FBUyxHQUFtQixFQUFFLENBQUM7UUFLL0Isa0JBQWEsR0FBVyxLQUFLLENBQUM7UUErQjlCLFlBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxXQUFXLEdBQXlCLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQVksS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQzFHLEtBQUksQ0FBQyxLQUFLLEdBQVMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFRLENBQUMsS0FBSyxDQUFDO1lBQzlDLEtBQUksQ0FBQyxTQUFTLEdBQUc7Z0JBQ2IsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUN4QyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUU7Z0JBQ2pELEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDNUMsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2FBQ2pELENBQUM7WUFDRixLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBRWxCLEtBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQ1gsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUU1QyxLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFakIsbUNBQW1DO1lBQ25DLEtBQUksQ0FBQywwQkFBMEIsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFFOUQsS0FBSSxDQUFDLGtCQUFrQixHQUF5QjtnQkFDNUMsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLE1BQU0sRUFBRSxJQUFJO2FBQ2YsQ0FBQztZQUVGLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGlDQUFpQyxFQUFFLFVBQUMsUUFBaUIsRUFBRSxRQUFpQjtnQkFDdkYsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLE1BQU0sQ0FBQztnQkFDWCxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLElBQUksS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ3JHLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO29CQUNuQixLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQzdFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLEVBQW5CLENBQW1CLENBQUMsQ0FBQztnQkFDbEQsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBRUssY0FBUyxHQUFHO1lBQ2YsSUFBSSxJQUFJLEdBQXdCLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBUyxDQUFDO1lBQ2QsSUFBSSxlQUFlLEdBQUcsVUFBQyxNQUF3QjtnQkFDM0MsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQy9DLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQztvQkFDbkIsTUFBTSxDQUFDLFlBQVksR0FBRyxDQUFDO29CQUN2QixNQUFNLENBQUMsT0FBTyxLQUFLLHNCQUFPLENBQUMsT0FBTyxDQUFDO1lBQzNDLENBQUMsQ0FBQztZQUVGLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzVDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2xDLElBQUksVUFBVSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQzt3QkFDakMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBQ3BELENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNOLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7d0JBQ2IsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFDdkIsVUFBVSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7d0JBQzdCLENBQUM7d0JBQ0QsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMvQyxLQUFJLENBQUMsdUNBQXVDLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztvQkFDbkYsQ0FBQztnQkFDTCxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLHNCQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDbkQsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQzt3QkFDakQsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLEdBQUcsZUFBZSxDQUFDO29CQUMxRCxDQUFDO29CQUNELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxDQUFDO1lBQ0wsQ0FBQztZQUNELEtBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQzFCLENBQUMsQ0FBQztRQUVLLGFBQVEsR0FBRyxVQUFDLElBQXNCLEVBQUUsUUFBeUI7WUFBekIseUJBQUEsRUFBQSxnQkFBeUI7WUFDaEUsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDWCxLQUFJLENBQUMsdUJBQXVCLENBQUMsY0FBUSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEUsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0NBQWdDLEVBQUU7cUJBQ2pELElBQUksQ0FBQyxVQUFDLFFBQXNEO29CQUN6RCxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7b0JBRXhELEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsc0JBQXNCOzJCQUM3RCxRQUFRLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDO29CQUMvQyxLQUFJLENBQUMsbUNBQW1DLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDO29CQUVwRixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQzt3QkFDcEMsS0FBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQzt3QkFDdEMsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFDdkMsQ0FBQztvQkFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsc0JBQXNCLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUI7MkJBQ3pFLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7d0JBQzlDLEtBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO29CQUMzQixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNCLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUssZ0JBQVcsR0FBRyxVQUFDLElBQXNCO1lBQ3hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsS0FBSyx3REFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzlFLEtBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztxQkFDdkQsSUFBSSxDQUFDLFVBQUMsUUFBNEI7b0JBQy9CLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdkQsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQzt1QkFDbEUsSUFBSSxDQUFDLGlCQUFpQixLQUFLLHdEQUF3QixDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDM0UsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsbURBQW1ELENBQUMsQ0FBQztvQkFDbkUsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLG1EQUFtRCxDQUFDLENBQUM7Z0JBQ3JGLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNyQyxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLGdCQUFXLEdBQUcsVUFBQyxJQUFzQjtZQUN6QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDZCxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMvQixDQUFDO1lBRUQsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBRXJELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakQsSUFBSSxDQUFDLFFBQVEsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBQ2xELENBQUM7WUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNwQixLQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUMzQyxDQUFDLENBQUM7UUFFSyx1QkFBa0IsR0FBRztZQUN4QixLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFVBQWtCO2dCQUN2RCxLQUFJLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUQsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyxrQkFBYSxHQUFHO1lBQ25CLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQzNCLEVBQUUsRUFDRjtnQkFDSSxNQUFNLEVBQUUsU0FBUztnQkFDakIsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUM7Z0JBQ2pDLE9BQU8sRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLDBEQUEwRCxDQUFDLEdBQUcsUUFBUTtvQkFDbkYsS0FBSSxDQUFDLEVBQUUsQ0FBQyxxRkFBcUY7d0JBQ3pGLGtFQUFrRSxDQUFDLEdBQUcsY0FBYztvQkFDeEYsS0FBSSxDQUFDLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQyxHQUFHLFFBQVE7Z0JBQ2xELGdCQUFnQixFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNuQyxPQUFPLEVBQUUsQ0FBQzt3QkFDTixJQUFJLEVBQUUsSUFBSTt3QkFDVixTQUFTLEVBQUUsSUFBSTt3QkFDZixJQUFJLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0JBQ25CLE1BQU0sRUFBRSxVQUFDLE1BQTBCOzRCQUMvQixLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7aUNBQzVFLElBQUksQ0FBQyxVQUFDLFFBQWlDO2dDQUNwQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQ0FDL0MsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDO29DQUMxQyxNQUFNLENBQUMsS0FBSyxDQUFDO2dDQUNqQixDQUFDO2dDQUNELEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUN4QixLQUFJLENBQUMsRUFBRSxDQUFDLCtCQUErQjtvQ0FDbkMsaUNBQWlDO29DQUNqQyxxREFBcUQsQ0FDeEQsQ0FBQyxDQUFDO2dDQUNQLE1BQU0sQ0FBQyxJQUFJLENBQUM7NEJBQ2hCLENBQUMsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sQ0FBQyxJQUFJLENBQUM7d0JBQ2hCLENBQUM7cUJBQ0osQ0FBQzthQUNMLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUVLLGFBQVEsR0FBRztZQUNkLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLFVBQVUsRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZHLENBQUMsQ0FBQTtRQUVNLGVBQVUsR0FBRztZQUNoQixJQUFJLFlBQVksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsVUFBQyxNQUF3QjtnQkFDOUYsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEtBQUssc0JBQU8sQ0FBQyxNQUFNLENBQUM7WUFDN0MsQ0FBQyxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsQ0FBQyxZQUFZLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztnQkFDMUQsTUFBTSxDQUFDLEtBQUssQ0FBQztZQUNqQixDQUFDO1lBRUQsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FDMUMsRUFBRSxFQUNGO2dCQUNJLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixLQUFLLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLHlDQUF5QyxDQUFDO2dCQUMzRCxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnQkFDL0IsT0FBTyxFQUFFLENBQUM7d0JBQ04sSUFBSSxFQUFFLFFBQVE7d0JBQ2QsU0FBUyxFQUFFLElBQUk7d0JBQ2YsSUFBSSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3dCQUNwQixNQUFNLEVBQUUsVUFBQyxNQUEwQjs0QkFDL0IsS0FBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztpQ0FDeEUsSUFBSSxDQUFDLFVBQUMsUUFBaUM7Z0NBQ3BDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQ0FDM0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUM7b0NBQ25FLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7Z0NBQ3pCLENBQUM7Z0NBQUMsSUFBSSxDQUFDLENBQUM7b0NBQ0osS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFHLFFBQVEsQ0FBQyxZQUFjLENBQUM7Z0NBQ25ELENBQUM7NEJBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsTUFBTSxDQUFDLElBQUksQ0FBQzt3QkFDaEIsQ0FBQztxQkFDSixDQUFDO2FBQ0wsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRUssNEJBQXVCLEdBQUc7WUFDN0IsTUFBTSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sS0FBSyxzQkFBTyxDQUFDLE9BQU87Z0JBQ3hELEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDO2dCQUN0QyxLQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEtBQUssd0NBQWdCLENBQUMsSUFBSSxDQUFDO1FBQ25FLENBQUMsQ0FBQztRQUVLLHNCQUFpQixHQUFHO1lBQ3ZCLElBQU0sTUFBTSxHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQzFDLEVBQUUsRUFDRjtnQkFDSSxNQUFNLEVBQUUsU0FBUztnQkFDakIsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsZUFBZSxDQUFDO2dCQUMvQixPQUFPLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxpRUFDZixLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsV0FBVywwQ0FBdUMsQ0FBQztnQkFDMUYsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0JBQy9CLE9BQU8sRUFBRSxDQUFDO3dCQUNOLElBQUksRUFBRSxJQUFJO3dCQUNWLFNBQVMsRUFBRSxJQUFJO3dCQUNmLElBQUksRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt3QkFDbkIsTUFBTSxFQUFFLFVBQUMsTUFBMEI7NEJBQy9CLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztpQ0FDL0UsSUFBSSxDQUFDLFVBQUMsUUFBaUM7Z0NBQ3BDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQ0FDM0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7b0NBQzNFLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7Z0NBQ3pCLENBQUM7Z0NBQUMsSUFBSSxDQUFDLENBQUM7b0NBQ0osS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFHLFFBQVEsQ0FBQyxZQUFjLENBQUM7Z0NBQ25ELENBQUM7NEJBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBRVAsTUFBTSxDQUFDLElBQUksQ0FBQzt3QkFDaEIsQ0FBQztxQkFDSixDQUFDO2FBQ0wsQ0FDSixDQUFDO1FBQ04sQ0FBQyxDQUFDO1FBRUsscUJBQWdCLEdBQUcsVUFBQyxnQkFBaUM7WUFBakMsaUNBQUEsRUFBQSx3QkFBaUM7WUFDeEQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1lBQ3BDLDhCQUE4QjtZQUM5QixLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQztZQUVwRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLEVBQUUsUUFBUSxFQUFFLE9BQU8sQ0FBUyw4QkFBOEIsQ0FBQyxFQUFFLEVBQUU7Z0JBQzNGLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLG1DQUFtQyxDQUFDO2dCQUNuRCxTQUFTLEVBQUUsS0FBSSxDQUFDLE1BQU07Z0JBQ3RCLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNuQyxPQUFPLEVBQUUsQ0FBQzt3QkFDTixJQUFJLEVBQUUsTUFBTTt3QkFDWixTQUFTLEVBQUUsSUFBSTt3QkFDZixJQUFJLEVBQUUsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7d0JBQ3ZCLE1BQU0sRUFBRSxVQUFDLE1BQU07NEJBQ1gsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQztnQ0FDeEMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUN2QyxDQUFDOzRCQUFDLElBQUksQ0FBQyxDQUFDO2dDQUNKLEtBQUksQ0FBQyxRQUFRLENBQUM7b0NBQ1YsT0FBTyxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dDQUNuRCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7NEJBQ1YsQ0FBQzs0QkFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNoQixDQUFDO3FCQUNKLENBQUM7YUFDTCxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFSyxnQkFBVyxHQUFHLFVBQUMsZ0JBQXlCO1lBQzNDLEtBQUksQ0FBQyxjQUFjLENBQUMsdUJBQXVCLEVBQUU7aUJBQ3hDLElBQUksQ0FBQyxVQUFDLFdBQW9CO2dCQUN2QixFQUFFLENBQUMsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDdkIsRUFBRSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO3dCQUNuQix5Q0FBeUM7d0JBQ3pDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxjQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDMUMsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7b0JBQzlCLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osc0NBQXNDO3dCQUN0QyxLQUFJLENBQUMsUUFBUSxDQUFtQixLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ2hGLENBQUM7Z0JBQ0wsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxzQ0FBc0MsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDN0UsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRUssNEJBQXVCLEdBQUc7WUFFN0IsSUFBSSxrQkFBa0IsR0FBMkM7Z0JBQzdELEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLHdDQUF3QyxDQUFDO2dCQUN4RCxPQUFPLEVBQUUsQ0FBQzt3QkFDTixJQUFJLEVBQUUsYUFBYTt3QkFDbkIsU0FBUyxFQUFFLElBQUk7d0JBQ2YsSUFBSSxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsaUNBQWlDLENBQUM7d0JBQ2hELE1BQU0sRUFBRSxVQUFDLGlCQUFpQjs0QkFDdEIsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7NEJBQzlCLE1BQU0sQ0FBQyxJQUFJLENBQUM7d0JBQ2hCLENBQUM7cUJBQ0osQ0FBQzthQUNMLENBQUM7WUFDRixLQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO2dCQUM1QixRQUFRLEVBQUUsT0FBTyxDQUFTLG9EQUFvRCxDQUFDO2FBRWxGLEVBQUUsa0JBQWtCLENBQUM7aUJBQ2pCLElBQUksQ0FBQyxVQUFDLFFBQWE7Z0JBQ2hCLEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUM3QixLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDbEMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRUssYUFBUSxHQUFHLFVBQUMsUUFBYyxFQUFFLFFBQWM7WUFDN0MseUNBQXlDO1lBQ3pDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDbEYsQ0FBQztZQUVELEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUcsQ0FBQyxDQUFDO1FBRUssbUNBQThCLEdBQUc7WUFDcEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEtBQUksQ0FBQyxXQUFXLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXO2dCQUNuRixDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLGlCQUFpQixJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDakgsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUc7WUFDekIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNuRCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2pELE9BQU8sR0FBRyxPQUFPLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQzsyQkFDM0MsT0FBSyxLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLE1BQUcsQ0FBQSxDQUFDO2dCQUMzRSxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztvQkFDdkQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO3dCQUNqRCxPQUFPLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQztvQkFDbEQsQ0FBQztvQkFDRCxPQUFPLEdBQUcsT0FBTyxHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsb0JBQW9CLENBQUM7MkJBQzNDLE9BQUssS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsUUFBVSxDQUFBOzJCQUNwRSxNQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLE9BQU8sTUFBRyxDQUFBLENBQUM7Z0JBQzlFLENBQUM7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUNuQixDQUFDLENBQUE7UUFFTSxzQkFBaUIsR0FBRyxVQUFDLE1BQStCO1lBQ3ZELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDNUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsNENBQTRDLENBQUMsQ0FBQztnQkFDM0UsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUN6QixDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRU0sNEJBQXVCLEdBQUcsVUFBQyxHQUFhO1lBQzVDLEtBQUksQ0FBQyxjQUFjLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFrQjtnQkFDbEUsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDWixHQUFHLEVBQUUsQ0FBQztvQkFDTixLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLDZCQUE2QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQy9FLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUM3RSxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFTSw0Q0FBdUMsR0FBRyxVQUFDLFdBQTZCLEVBQUUsVUFBcUI7WUFDbkcsSUFBSSxJQUFJLEdBQUcsSUFBSSw4QkFBbUIsRUFBRSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FBQztZQUNuQyxJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdkQsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDO2dCQUN6QyxXQUFXLENBQUMsd0JBQXdCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyxLQUFLLElBQUksNkJBQTZCLEdBQUcsV0FBVyxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxTQUFTLENBQUM7WUFDOUcsQ0FBQztZQUVELFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQztRQUVNLGVBQVUsR0FBRyxVQUFDLEtBQVUsRUFBRSxJQUFzQjtZQUNwRCxLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQixLQUFJLENBQUMsRUFBRSxDQUFDLFVBQUMsT0FBTyxJQUFLLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxjQUFNLE9BQUEsT0FBTyxFQUFFLEVBQVQsQ0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFqQyxDQUFpQyxDQUFDO2lCQUNsRCxJQUFJLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDSixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRU0sZUFBVSxHQUFHO1lBQ2pCLG9FQUFvRTtZQUNwRSxJQUFJLFdBQVcsR0FBUSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUVoRixLQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQztnQkFDUixLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRTtnQkFDckMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUU7YUFDakMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE9BQU87Z0JBQ1osRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFlBQVksSUFBSSxLQUFJLENBQUMsS0FBSzt1QkFDNUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUksQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO3VCQUN2RCxLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3RELEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUNwQixNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUNqQixDQUFDO2dCQUVELEtBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFeEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUNqQixLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsU0FBUyxFQUFFLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO2dCQUMzRixLQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ25DLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO1FBY00sd0JBQW1CLEdBQUcsVUFBQyxNQUFXO1lBQ3RDLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxHQUFHLENBQUMsQ0FBaUIsVUFBaUIsRUFBakIsS0FBQSxNQUFNLENBQUMsVUFBVSxFQUFqQixjQUFpQixFQUFqQixJQUFpQjtnQkFBakMsSUFBSSxRQUFRLFNBQUE7Z0JBQ2IsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ25CLEtBQUssb0JBQW9CLENBQUM7b0JBQzFCLEtBQUssVUFBVTt3QkFDWCxLQUFLLENBQUM7b0JBQ1YsS0FBSyxjQUFjO3dCQUNmLFFBQVEsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO3dCQUN6RSxLQUFLLENBQUM7b0JBQ1YsS0FBSyxzQkFBc0I7d0JBQ3ZCLElBQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUMxQyxRQUFRLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDekcsS0FBSyxDQUFDO29CQUNWO3dCQUNJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sUUFBUSxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDOzRCQUNyQyxRQUFRLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUM3QyxDQUFDO2dCQUNULENBQUM7Z0JBRUQsRUFBRSxDQUFDLENBQUMsT0FBTyxRQUFRLENBQUMsR0FBRyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ25DLFFBQVEsQ0FBQyxHQUFHLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3pDLENBQUM7YUFDSjtRQUNMLENBQUMsQ0FBQztRQUVNLGlCQUFZLEdBQUcsVUFBQyxJQUFTO1lBQzdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ25CLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxHQUFHLENBQUMsQ0FBaUIsVUFBZSxFQUFmLEtBQUEsSUFBSSxDQUFDLFVBQVUsRUFBZixjQUFlLEVBQWYsSUFBZTtnQkFBL0IsSUFBSSxRQUFRLFNBQUE7Z0JBQ2IsUUFBUSxDQUFDLEdBQUcsR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDckMsRUFBRSxDQUFDLENBQUMsT0FBTyxRQUFRLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3JDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7YUFDSjtRQUNMLENBQUMsQ0FBQztRQUVNLHdCQUFtQixHQUFHLFVBQUMsTUFBc0IsRUFBRSxNQUFzQjtZQUN6RSxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUQsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNyQyxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxVQUFDLElBQVM7Z0JBQzNCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO2dCQUMvQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7Z0JBQ3ZCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUN6QixDQUFDLENBQUMsQ0FBQztZQUVILElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDckMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsVUFBQyxJQUFTO2dCQUMzQixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztnQkFDL0IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUN2QixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDekIsQ0FBQyxDQUFDLENBQUM7WUFFSCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQztRQUVGLDZFQUE2RTtRQUNyRSxxQkFBZ0IsR0FBRyxVQUFDLElBQVM7WUFDakMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLE9BQU8sSUFBSyxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsY0FBTSxPQUFBLE9BQU8sRUFBRSxFQUFULENBQVMsRUFBRSxDQUFDLENBQUMsRUFBakMsQ0FBaUMsQ0FBQztpQkFDbEQsSUFBSSxDQUFDO2dCQUNGLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDUixLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztvQkFDekIsTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDakIsQ0FBQztnQkFFRCxJQUFJLFdBQVcsR0FBUSxFQUFFLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFTO29CQUMzRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ3hDLFdBQVcsR0FBRyxJQUFJLENBQUM7d0JBQ25CLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQzt3QkFDdkMsTUFBTSxDQUFDLEtBQUssQ0FBQztvQkFDakIsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzt3QkFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxTQUFjOzRCQUMzQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0NBQzdDLFdBQVcsR0FBRyxTQUFTLENBQUM7Z0NBQ3hCLFdBQVcsQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQ0FDdEMsTUFBTSxDQUFDLEtBQUssQ0FBQzs0QkFDakIsQ0FBQzt3QkFDTCxDQUFDLENBQUMsQ0FBQztvQkFDUCxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztnQkFDM0MsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztZQUNoRCxDQUFDLENBQ0EsQ0FBQztRQUNWLENBQUMsQ0FBQztRQUVNLDJCQUFzQixHQUFHO1lBQzdCLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0NBQWdDLEVBQUU7aUJBQ2pELElBQUksQ0FBQyxVQUFDLFFBQXNEO2dCQUN6RCxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7Z0JBQ3hELEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLHVCQUF1Qjt1QkFDL0UsUUFBUSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZDLE1BQU0sQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3ZDLENBQUM7Z0JBQ0QsTUFBTSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO1FBRU0sdUJBQWtCLEdBQUc7WUFFekIsSUFBSSxxQkFBcUIsR0FBMkM7Z0JBQ2hFLEtBQUssRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLG1DQUFtQyxDQUFDO2dCQUNuRCxVQUFVLEVBQUUsSUFBSTtnQkFDaEIsT0FBTyxFQUFFLENBQUM7d0JBQ04sSUFBSSxFQUFFLGFBQWE7d0JBQ25CLFNBQVMsRUFBRSxJQUFJO3dCQUNmLElBQUksRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO3dCQUN2QyxNQUFNLEVBQUUsVUFBQyxvQkFBb0I7NEJBQ3pCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQ0FBZ0MsRUFBRSxDQUFDOzRCQUMxRCxNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNoQixDQUFDO3FCQUNKLENBQUM7YUFDTCxDQUFDO1lBQ0YsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztnQkFDNUIsUUFBUSxFQUFFLE9BQU8sQ0FBUywyQ0FBMkMsQ0FBQzthQUN6RSxFQUFFLHFCQUFxQixDQUFDLENBQUM7UUFDOUIsQ0FBQyxDQUFDO1FBRU0sMEJBQXFCLEdBQUcsVUFBQyxJQUFzQjtZQUNuRCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLFFBQVEsR0FBRyxJQUFJLG9CQUFTLEVBQUUsQ0FBQztnQkFDL0IsUUFBUSxDQUFDLGdCQUFnQixHQUF1QixLQUFJLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ3RFLFFBQVEsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzlDLFFBQVEsQ0FBQyxRQUFRLEdBQW1CLENBQUMsSUFBSSwyQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNqRSxLQUFJLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUMzRSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsNENBQTRDLENBQUMsQ0FBQztnQkFDNUQsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztnQkFDbkMsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsRUFBRSxDQUFDLGtEQUFrRCxDQUFDLENBQUM7WUFDcEYsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVNLHdCQUFtQixHQUFHLFVBQUMsSUFBc0I7WUFDakQsSUFBSSxXQUFXLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBVSxNQUFNO2dCQUN2RCxxR0FBcUc7Z0JBQ3JHLElBQUksZUFBZSxHQUNmLElBQUksQ0FBQyxpQkFBaUIsS0FBSyx3REFBd0IsQ0FBQyxVQUFVO29CQUMxRCxDQUFDLENBQUMsd0RBQXdCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztvQkFDOUMsSUFBSSxDQUFDLGlCQUFpQixLQUFLLHdEQUF3QixDQUFDLGlCQUFpQjt3QkFDakUsQ0FBQyxDQUFDLHdEQUF3QixDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUN2QyxJQUFJLENBQUMsaUJBQWlCLEtBQUssd0RBQXdCLENBQUMsZ0JBQWdCOzRCQUNoRSxDQUFDLENBQUMsd0RBQXdCLENBQUMsdUJBQXVCLENBQUMsQ0FBQzs0QkFDcEQsSUFBSSxDQUFDLGlCQUFpQixLQUFLLHdEQUF3QixDQUFDLHVCQUF1QjtnQ0FDdkUsQ0FBQyxDQUFDLHdEQUF3QixDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0NBQzdDLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixNQUFNLENBQUMsTUFBTSxDQUFDLGlCQUFpQixLQUFLLGVBQWUsSUFBSSxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUM7dUJBQ25FLElBQUksQ0FBQyxZQUFZLEtBQUssTUFBTSxDQUFDLFlBQVksQ0FBQztZQUNyRCxDQUFDLENBQUMsQ0FBQztZQUVILE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7UUFDekQsQ0FBQyxDQUFDO1FBRU0sY0FBUyxHQUFHO1lBQ2hCLHNFQUFzRTtZQUN0RSxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLElBQUksS0FBSyxHQUFXLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFHLG1DQUFtQyxDQUFDO2dCQUMzRixJQUFJLE9BQU8sR0FBVyw0RUFBNEU7b0JBQzlGLHVDQUF1QztvQkFDdkMsb0NBQW9DLENBQUM7Z0JBQ3pDLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNqRCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRU0sc0JBQWlCLEdBQUc7WUFDeEIsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQ2pELENBQUMsVUFBQyxJQUFTLElBQUssT0FBQSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sS0FBSyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsS0FBSyxNQUFNLEVBQXRGLENBQXNGLENBQUMsQ0FBQyxDQUFDO1lBRTdHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsSUFBSSxlQUFhLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQ2xDLGNBQU0sT0FBQSxLQUFJLENBQUMsV0FBVyxFQUFoQixDQUFnQixFQUN0QixVQUFDLGtCQUF1QjtvQkFDcEIsRUFBRSxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO3dCQUNyQixLQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUM1QyxJQUFNLFNBQVMsR0FBc0IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUM7d0JBQ3pFLElBQU0sWUFBWSxHQUFHLFNBQVMsS0FBSyxzQ0FBaUIsQ0FBQyxNQUFNOzRCQUN2RCxDQUFDLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7NEJBQ3BCLENBQUMsQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN4QixJQUFNLE9BQU8sR0FBTSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksU0FBSSxLQUFJLENBQUMsRUFBRSxDQUFDLHVCQUF1QixDQUFDLFNBQUksWUFBWSxNQUFHLENBQUM7d0JBQzNGLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUN0RCxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUUxQixzREFBc0Q7d0JBQ3RELHlEQUF5RDt3QkFDekQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQzNDLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsS0FBSyxDQUFDOzRCQUNqRCxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzt3QkFDM0QsQ0FBQzt3QkFDRCxlQUFhLEVBQUUsQ0FBQztvQkFDcEIsQ0FBQztnQkFDTCxDQUFDLENBQ0osQ0FBQztZQUNOLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixJQUFJLGVBQWEsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FDbEMsY0FBTSxPQUFBLEtBQUksQ0FBQyxXQUFXLEVBQWhCLENBQWdCLEVBQ3RCLFVBQUMsa0JBQXVCO29CQUNwQixFQUFFLENBQUMsQ0FBQyxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDakUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDNUQsZUFBYSxFQUFFLENBQUM7b0JBQ3BCLENBQUM7Z0JBQ0wsQ0FBQyxDQUNKLENBQUM7WUFDTixDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRU0sbUJBQWMsR0FBRyxVQUFDLElBQVM7WUFDL0IsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUNwQyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNwQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUMzQixDQUFDO1FBQ0wsQ0FBQyxDQUFDO0lBbnBCa0MsQ0FBQztJQXdhN0Isb0NBQVEsR0FBaEIsVUFBaUIsU0FBYztRQUMzQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxLQUFLLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM3QixNQUFNLENBQUM7UUFDWCxDQUFDO0lBQ0wsQ0FBQztJQWhmYSx5QkFBTyxHQUFHO1FBQ3BCLFFBQVE7UUFDUixRQUFRO1FBQ1Isa0JBQWtCO1FBQ2xCLE1BQU07UUFDTixnQkFBZ0I7UUFDaEIsWUFBWTtRQUNaLGdCQUFnQjtRQUNoQixtQkFBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLG1CQUFtQjtRQUNuQixJQUFJO1FBQ0osVUFBVTtRQUNWLFdBQVc7UUFDWCxhQUFhO1FBQ2IsYUFBYTtLQUNoQixDQUFDO0lBa3NCTix3QkFBQztDQUFBLEFBbnRCRCxJQW10QkM7a0JBbnRCb0IsaUJBQWlCIn0=

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var PoolMemberStatus;
(function (PoolMemberStatus) {
    PoolMemberStatus[PoolMemberStatus["Up"] = 1] = "Up";
    PoolMemberStatus[PoolMemberStatus["Down"] = 2] = "Down";
    PoolMemberStatus[PoolMemberStatus["Dormant"] = 6] = "Dormant";
    PoolMemberStatus[PoolMemberStatus["Critical"] = 14] = "Critical";
    PoolMemberStatus[PoolMemberStatus["Disabled"] = 27] = "Disabled";
})(PoolMemberStatus = exports.PoolMemberStatus || (exports.PoolMemberStatus = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbE1lbWJlclN0YXR1cy1lbnVtLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9vbE1lbWJlclN0YXR1cy1lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBWSxnQkFNWDtBQU5ELFdBQVksZ0JBQWdCO0lBQ3hCLG1EQUFNLENBQUE7SUFDTix1REFBUSxDQUFBO0lBQ1IsNkRBQVcsQ0FBQTtJQUNYLGdFQUFhLENBQUE7SUFDYixnRUFBYSxDQUFBO0FBQ2pCLENBQUMsRUFOVyxnQkFBZ0IsR0FBaEIsd0JBQWdCLEtBQWhCLHdCQUFnQixRQU0zQiJ9

/***/ }),
/* 55 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div _t> A High Availability pool license is required to use this feature. An appropriate license was not found on this installation. </div> <div _t> You may need to purchase or activate a High Availability pool license. You can also try High Availability for 30 days. </div> <br> <div> <xui-radio ng-model=vm.dialogOptions.viewModel.activation value=activate class=ha-radio-evaluation> <strong _t> Activate a High Availability pool license </strong> </xui-radio> <div class=ha-radio-description> <div _t> Select this option if you've already purchased a license for a High Availability pool and need to activate it. </div> <div> <a ng-href=\"{{:: vm.dialogOptions.viewModel.pricingLink }}\" target=_blank rel=\"noopener noreferrer\">&raquo;&nbsp;<span _t>Explore pricing</span></a> </div> </div> <br> <xui-radio ng-model=vm.dialogOptions.viewModel.activation is-disabled=\"vm.dialogOptions.viewModel.licenseAvailabilityResult.activatedCommercial || vm.dialogOptions.viewModel.licenseAvailabilityResult.evaluationExpired\" value=evaluate class=ha-radio-activation> <strong _t>Evaluate High Availability (no license needed)</strong> </xui-radio> <div class=ha-radio-description ng-if=\"!vm.dialogOptions.viewModel.licenseAvailabilityResult.activatedCommercial && !vm.dialogOptions.viewModel.licenseAvailabilityResult.evaluationExpired\" _t>30 days remaining</div> <div class=ha-radio-description ng-if=vm.dialogOptions.viewModel.licenseAvailabilityResult.evaluationExpired _t>0 days remaining (evaluation has expired)</div> </div> </xui-dialog> ";

/***/ }),
/* 56 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div ng-controller=\"SetUpHighAvailabilityServerController as vm\"> <div class=ha-setup-high-availability-server-dialog> <xui-tabs> <xui-tab title=\"{{vm._t('WHAT DOES IT DO?')}}\"> <div class=row> <div class=col-md-12> <h4 _t>Protects Key Services</h4> <div class=row> <div class=col-md-6> <img class=img-setup-dialog src=modules/ha/images/protects-key-services.svg alt=\"protects key services\"/> </div> <div class=col-md-6 _t> Each of your Orion servers can be pooled with a secondary, backup server. If the primary goes down, the secondary server will take over all important services, like polling and alerting. </div> </div> </div> </div> <div class=row> <div class=col-md-12> <h4 _t>Prevents Data Loss</h4> <span _t>Single Subnet Pool</span> <div class=row> <div class=col-md-6> <img class=img-setup-dialog src=modules/ha/images/single-subnet-pool.svg alt=\"single subnet pool\"/> </div> <div class=col-md-6> <span _t>Your monitored devices can send data to a pool's </span><span class=bold-text _t>Virtual IP (VIP).</span> <span _t> The VIP is a new address shared by members of a pool. If your primary server goes down, a secondary server takes over the VIP.</span> </div> </div> </div> </div> <div class=row> <div class=col-md-12> <br/> <span _t>Multi-Subnet Pool</span> <div class=row> <div class=col-md-6> <img src=modules/ha/images/multi-subnet-pool.svg alt=\"multi subnet pool\"/> </div> <div class=col-md-6> <span _t>Your monitored devices can send data to a pool's </span><span class=bold-text _t>Virtual Hostname.</span> <span _t> The Virtual Hostname is the hostname that will be used with DNS to refer to the currently active server. If your primary server goes down, DNS is updated with the Virtual Hostname pointing to the secondary server.</span> </div> </div> </div> </div> </xui-tab> <xui-tab title=\"{{vm._t('HOW DO I SET IT UP?')}}\"> <div class=\"row howto-setup-row\"> <div class=\"col-md-3 vcenter\"> <img class=img-setup-dialog src=modules/ha/images/set-up-secondary-server.png alt=\"set up a secondary server\"/> </div> <div class=\"col-md-9 vcenter\"> <h4 _t>1. Set Up a Secondary Server</h4> <div> <span _t> Stand up a new Windows server. Then copy the Orion HA Installer to the new server. The installer will automatically download and configure all required software. </span> </div> <a href ng-click=vm.downloadOrionInstaller(this)>&raquo;&nbsp;<span _t>Download the Orion HA Installer</span></a> </div> </div> <div class=\"row howto-setup-row\"> <div class=\"col-md-3 vcenter\"> <img class=img-setup-dialog src=modules/ha/images/ha-open-ports.png alt=\"open ports\"/> </div> <div class=\"col-md-9 vcenter\"> <h4 _t>2. Open Ports</h4> <div> <span _t=\"[ '{{:: vm.haconstants.haPorts.bothRequired}}', '{{:: vm.haconstants.haPorts.primaryServerRequired}}', '{{:: vm.haconstants.haPorts.secondaryServerRequired}}' ]\"> Open port {0} on all primary and secondary servers. To protect your main poller, also open ports {1} and {2} on the primary and secondary servers. </span> </div> </div> </div> <div class=\"row howto-setup-row\"> <div class=\"col-md-3 vcenter\"> <img class=img-setup-dialog src=modules/ha/images/ha-set-up-pool.png alt=\"set up a high availability pool\"/> </div> <div class=\"col-md-9 vcenter\"> <h4 _t>3. Set Up a High Availability Pool</h4> <div> <span _t> Set up a high availability pool which includes your primary and secondary servers. You can set up High Availability pools from the Orion Deployment Summary. </span> </div> <sw-help-link topic=OrionCoreHAPoolSetup target=_blank rel=\"noopener noreferrer\" icon=double-caret-right><span _t>Learn more about pool setup</span></sw-help-link> </div> </div> <div class=\"row howto-setup-row\"> <div class=\"col-md-3 vcenter\"> <img class=img-setup-dialog src=modules/ha/images/ha-license-pool.png alt=\"license the pool\"/> </div> <div class=\"col-md-9 vcenter\"> <h4 _t>4. License the Pool</h4> <div> <span _t> Each high availability pool requires its own High Availability pool license. You can evaluate High Availability for 30 days. The 30-day evaluation can be enabled when setting up a High Availability pool. </span> </div> <a ng-href=\"{{:: vm.haconstants.links.haLicensingPricing }}\" target=_blank rel=\"noopener noreferrer\">&raquo;&nbsp;<span _t>Explore pricing</span></a> </div> </div> </xui-tab> </xui-tabs> </div> </div> </xui-dialog> ";

/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <div class=ha-download-ha-installer-dialog ng-controller=\"DownloadInstallerController as vm\"> <div _t> To set up a secondary server, download the installer provided below and run it on the secondary server. We install all of the Orion software needed for the server. </div> <sw-help-link topic=OrionCoreHAGetStarted target=_blank rel=\"noopener noreferrer\" icon=double-caret-right><span _t>Learn more about High Availability protection setup</span></sw-help-link> <div> <p class=download-list-title _t>Download the Installer File</p> <ul class=list-instruction> <li _t>Download the installer and manually copy it to the secondary server or open the Orion Web Console from the secondary server and download there.</li> <li _t>Run the installer on the secondary server</li> </ul> </div> <xui-message type=info> <div _t>Open Ports Needed</div> <div _t=\"[ '{{:: vm.haconstants.haPorts.bothRequired}}', '{{:: vm.haconstants.haPorts.primaryServerRequired}}', '{{:: vm.haconstants.haPorts.secondaryServerRequired}}' ]\"> Open port {0} on all primary and secondary servers. To protect your main poller, also open ports {1} and {2} on the primary and secondary servers. </div> </xui-message> </div> </xui-dialog> ";

/***/ }),
/* 58 */
/***/ (function(module, exports) {

module.exports = "<div xui-busy=vm.isBusy xui-busy-message=_t(Loading...) class=margin-top-neg _ta> <xui-page-content page-layout=fullWidth class=xui-padding-zr> <xui-page-header> <div class=ha-summary-header> <div class=pull-right> <sw-help-link topic=OrionCoreHAGetStarted icon=help target=_blank rel=\"noopener noreferrer\"><span _t>Getting started with high availability protection</span></sw-help-link> </div> <xui-button ui-sref=settings icon=gear display-style=tertiary class=\"pull-right ha-summary-header-settings\"> <span _t>High Availability Settings</span> </xui-button> <h1 class=ha-summary-header-title _t>High Availability Deployment Summary</h1> </div> </xui-page-header> <div class=\"ha-summary xui-padding-lgh\"> <div> <span _t> The diagram below shows the deployment of your polling engines and high availability protection. </span> <sw-help-link topic=OrionCoreHADeployment target=_blank rel=\"noopener noreferrer\" icon=double-caret-right><span _t>Learn more about High Availability Deployment</span></sw-help-link> </div> <div class=row> <xui-divider></xui-divider> </div> <div> <xui-button display-style=tertiary ng-click=!vm.setUpAvailabilityServer() icon=orion-ha-pool> <span _t>Set Up a New HA Server</span> </xui-button> </div> <div class=row> <xui-divider></xui-divider> </div> <div ng-show=vm.errorMessage> <xui-message type=error> {{vm.errorMessage}} </xui-message> <br> </div> <div ng-show=vm.showLicenseExpiredMessage> <xui-message type=error> <div _t> High Availability Evaluation has <strong>Expired</strong>. To continue using this pool, a license is needed. </div> <div> <a class=\"cl-ha-licensing-link ha-licensing-modal\" sw-license-activation-modal dialog-title=vm.activateLicenseDialogTitle on-activate=vm.onLicenseActivate(result)>&raquo; <span _t>Activate a License</span></a> <a ng-click=vm.navigationService.navigateToBuyNowPage() class=cl-ha-licensing-link>&raquo; <span _t>Buy Now</span></a> <a ng-click=vm.navigationService.navigateToEmailSalesPage() class=cl-ha-licensing-link>&raquo; <span _t>Email Sales</span></a> </div> </xui-message> <br> </div> <div ng-show=vm.showActivateHAMessage> <xui-message type=error> <strong _t> A High Availability (HA) pool license is required. </strong> <span _t> Activate or reallocate an HA pool license in the License Manager to create the pool or purchase an HA pool license. </span> <span ng-if=!vm.$scope.licenseAvailabilityResult.activatedCommercial _t> You can also try High Availability for 30 days. </span> <a ng-click=vm.setUpHALicensing() class=cl-ha-licensing-link>&raquo <span _t>Set Up HA Licensing</span></a> </xui-message> <br> </div> <div ng-show=vm.showLicenseStoreNotAvailableMessage> <xui-message type=error> <span _t=\"['{{::vm.$scope.licenseAvailabilityResult.mainEnginePollerName}}']\"> The pool cannot be created. The licensing information cannot be retrieved while {0} is down. </span> </xui-message> <br> </div> <div class=row> <div class=\"ha-summary-list col-sm-12\"> <xui-message name=summaryWarningMessage type=warning ng-show=vm.shouldShowResourcesWarnMessage()> <strong _t=\"['{{vm.createdPool.name}}', '{{vm.getResourcesMessage()}}']\"> You may need to update some of your monitored devices to send Syslog, SNMP-Trap, or NetFlow data to the {0}'s {1}. </strong> <a href=\"\" ng-click=vm.navigateToHAReport() _t>List of Devices</a> / <a href=\"\" ng-click=vm.navigateToHAReport() _t>Learn More</a> </xui-message> <xui-sorter items-source=vm.sortItems display-value=value sort-direction=vm.sortDirection selected-item=vm.selectedItem on-change=\"vm.sortTree(newValue, oldValue)\"> </xui-sorter> <xui-sidebar-container> <div class=ha-summary-list-container> <xui-treeview model=vm.dataModel context=vm.treeContext options=vm.treeOptions class=list-tree> <div class=list-tree-item> <div class=node-details> <div ng-show=\"node.$model.itemType === 'orionServer' && node.$model.canAddToHAPool(node.$model)\" class=\"pull-right command-btn\"> <xui-button display-style=secondary ng-click=node.$model.addToHAPool(node.$model);$event.stopPropagation();> <span _t> SET UP HIGH AVAILABILITY POOL </span> </xui-button> </div> <div class=\"pull-right xui-text-s\" ng-if=\"node.$model.itemType === 'orionServer' && node.$model.poolMemberId === node.$parent.$model.pluginsData.poolConfiguration.preferredMemberId\"> <xui-icon icon=checkmark icon-color=gray></xui-icon> <span _t>Preferred Active</span> </div> <div class=text-caption> {{node.$model.name}} </div> <div class=text-description> {{node.$model.description}} </div> </div> </div> </xui-treeview> </div> <xui-sidebar settings=vm.xuiSidebarSettings> <div class=ha-summary-server-details> <sw-object-inspector-header icon=vm.inspectedNode.$model.icon icon-status=vm.inspectedNode.$model.iconStatus title=vm.inspectedNode.$model.name ip=vm.inspectedNode.$model.ipAddress description=\"vm.inspectedNode.$model.description ? vm._t(vm.inspectedNode.$model.description) : ''\" status=\"vm.inspectedNode.$model.statusDescription ? vm._t(vm.inspectedNode.$model.statusDescription) : ''\"> </sw-object-inspector-header> <div class=commands ng-if=\"vm.inspectedNode.$model.itemType === 'pool'\"> <xui-menu title=_t(commands) display-style=tertiary text-align=left _ta> <xui-menu-action action=vm.forceFailover() is-disabled=\"!vm.inspectedNode.$model.enabled || !vm.inspectedNode.$model.isLicensed\" _t>Force Failover</xui-menu-action> <xui-menu-action action=vm.editPool() _t>Edit Pool</xui-menu-action> <xui-menu-action action=vm.removePool() _t>Remove Pool</xui-menu-action> </xui-menu> </div> <div class=commands ng-if=\"vm.inspectedNode.$model.itemType === 'orionServer'\"> <xui-menu title=_t(commands) display-style=tertiary text-align=left _ta> <xui-menu-action action=vm.removeStaleEngine() is-disabled=!vm.canStaleEngineBeRemoved() _t>Delete</xui-menu-action> </xui-menu> </div> <div class=toggle-buttons ng-if=\"vm.inspectedNode.$model.itemType === 'pool'\"> <span class=caption _t>High Availability</span> <div class=row> <div class=col-xs-12> <xui-switch ng-model=vm.inspectedNode.$model.enabled ng-if=\"vm.inspectedNode.$model.isHighAvailabilityEnabled && vm.inspectedNode.$model.isLicensed && !vm.inspectedNode.$model.isProductMismatch()\" class=pull-left></xui-switch> <xui-switch ng-model=vm.inspectedNode.$model.enabled ng-if=\"!vm.inspectedNode.$model.isHighAvailabilityEnabled || !vm.inspectedNode.$model.isLicensed || vm.inspectedNode.$model.isProductMismatch()\" disabled=disabled class=pull-left></xui-switch> <div class=\"switch-label pull-left\"> <div ng-show=\"vm.inspectedNode.$model.enabled && vm.inspectedNode.$model.isLicensed\" _t>On</div> <div ng-show=\"!vm.inspectedNode.$model.enabled || !vm.inspectedNode.$model.isLicensed\" _t> <span ng-if=vm.inspectedNode.$model.isLicensed _t>Off</span> </div> </div> </div> <div class=col-xs-12> <xui-message ng-if=!vm.inspectedNode.$model.isHighAvailabilityEnabled type=info _t>The pool cannot be enabled because HA is globally turned off. <a href=/ui/ha/settings>» Enable HA on the settings page</a></xui-message> <xui-message ng-if=vm.inspectedNode.$model.isProductMismatch() type=warning _t><b>The pool cannot be enabled.</b> The products and product version of all pool members must match. <sw-help-link topic=OrionCoreHAProductMismatch target=_blank rel=\"noopener noreferrer\" icon=double-caret-right>Learn how to resolve a product mismatch</sw-help-link></xui-message> <xui-message ng-if=\"!vm.inspectedNode.$model.isLicensed && vm.inspectedNode.$model.isLicenseStoreAvailable\" type=warning _t><b>A High Availability pool license is required.</b> You can apply an HA pool license, purchase HA pool licenses. <a href=\"/ui/licensing/license-manager?fromHa=true\" target=_self>» Set up HA licenses</a></xui-message> <xui-message ng-if=!vm.inspectedNode.$model.isLicenseStoreAvailable type=warning>The licensing details for the HA pools cannot be retrieved because the primary engine poller is down.</xui-message> </div> </div> </div> <div class=details-list> <xui-listview stripe=true items-source=vm.inspectedNode.$model.properties template-url=server-property></xui-listview> <script type=text/ng-template id=server-property> <div class=\"row item\">\n                                        <div class=\"name\">{{::item.key}}</div>\n                                        <div class=\"value\" ng-bind-html=\"::item.value\" />\n                                    </div> </script> </div> </div> </xui-sidebar> </xui-sidebar-container> </div> </div> </div></xui-page-content> </div> ";

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var setUpHighAvailabilityServer_controller_1 = __webpack_require__(60);
__webpack_require__(61);
__webpack_require__(62);
__webpack_require__(63);
__webpack_require__(64);
__webpack_require__(65);
__webpack_require__(66);
__webpack_require__(67);
__webpack_require__(68);
__webpack_require__(69);
exports.default = function (module) {
    module.controller("SetUpHighAvailabilityServerController", setUpHighAvailabilityServer_controller_1.default);
    module.controller("DownloadInstallerController", setUpHighAvailabilityServer_controller_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1HQUE2RjtBQUU3RiwrQ0FBNkM7QUFDN0Msd0RBQXNEO0FBQ3RELCtDQUE2QztBQUM3Qyw4Q0FBNEM7QUFDNUMsNkNBQTJDO0FBQzNDLHVEQUFxRDtBQUNyRCxxREFBbUQ7QUFDbkQsa0RBQWdEO0FBQ2hELGlEQUErQztBQUUvQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFVBQVUsQ0FBQyx1Q0FBdUMsRUFBRSxnREFBcUMsQ0FBQyxDQUFDO0lBQ2xHLE1BQU0sQ0FBQyxVQUFVLENBQUMsNkJBQTZCLEVBQUUsZ0RBQXFDLENBQUMsQ0FBQztBQUM1RixDQUFDLENBQUMifQ==

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SetUpHighAvailabilityServerController = /** @class */ (function () {
    function SetUpHighAvailabilityServerController(_t, haconstants) {
        this._t = _t;
        this.haconstants = haconstants;
        this.downloadOrionInstaller = function (dialogInstance) {
            dialogInstance.$close("setUpServer");
        };
    }
    SetUpHighAvailabilityServerController.$inject = ["getTextService", "haconstants"];
    return SetUpHighAvailabilityServerController;
}());
exports.default = SetUpHighAvailabilityServerController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0VXBIaWdoQXZhaWxhYmlsaXR5U2VydmVyLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZXRVcEhpZ2hBdmFpbGFiaWxpdHlTZXJ2ZXItY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBO0lBR0ksK0NBQW1CLEVBQW9DLEVBQVMsV0FBc0I7UUFBbkUsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFBUyxnQkFBVyxHQUFYLFdBQVcsQ0FBVztRQUkvRSwyQkFBc0IsR0FBRyxVQUFDLGNBQW1CO1lBQ2hELGNBQWMsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDO0lBSkYsQ0FBQztJQUphLDZDQUFPLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxhQUFhLENBQUMsQ0FBQztJQVM5RCw0Q0FBQztDQUFBLEFBVkQsSUFVQztrQkFWb0IscUNBQXFDIn0=

/***/ }),
/* 61 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 62 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/ha-license-pool.png";

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/ha-set-up-pool.png";

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/ha-open-ports.png";

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/set-up-secondary-server.png";

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/protects-key-services.svg";

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/single-subnet-pool.svg";

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/multi-subnet-pool.svg";

/***/ }),
/* 70 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(72);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDZCQUEyQiJ9

/***/ }),
/* 72 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var deployment_config_1 = __webpack_require__(74);
exports.default = function (module) {
    module.config(deployment_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlEQUFtRDtBQUVuRCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQywyQkFBZ0IsQ0FBQyxDQUFDO0FBQ3BDLENBQUMsQ0FBQyJ9

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var deployment_controller_1 = __webpack_require__(75);
function DeploymentConfig($stateProvider) {
    $stateProvider
        .state("deployment", {
        title: "My Orion Deployment",
        url: "/ha/deployment",
        params: { poolId: null, poolOperation: null, ignoreLicenseCheck: false },
        controller: deployment_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(76)
    });
}
DeploymentConfig.$inject = ["$stateProvider"];
exports.default = DeploymentConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVwbG95bWVudC1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkZXBsb3ltZW50LWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHVDQUF1QztBQUN2QyxpRUFBMkQ7QUFFM0QsMEJBQTBCLGNBQW9DO0lBQzFELGNBQWM7U0FDVCxLQUFLLENBQUMsWUFBWSxFQUFPO1FBQ3RCLEtBQUssRUFBRSxxQkFBcUI7UUFDNUIsR0FBRyxFQUFFLGdCQUFnQjtRQUNyQixNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxFQUFFO1FBQ3hFLFVBQVUsRUFBRSwrQkFBb0I7UUFDaEMsWUFBWSxFQUFFLElBQUk7UUFDbEIsUUFBUSxFQUFFLE9BQU8sQ0FBUyxtQkFBbUIsQ0FBQztLQUNqRCxDQUFDLENBQUM7QUFDWCxDQUFDO0FBRUQsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztBQUU5QyxrQkFBZSxnQkFBZ0IsQ0FBQyJ9

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DeploymentController = /** @class */ (function () {
    function DeploymentController() {
    }
    return DeploymentController;
}());
exports.default = DeploymentController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVwbG95bWVudC1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGVwbG95bWVudC1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFBQTtJQUVBLENBQUM7SUFBRCwyQkFBQztBQUFELENBQUMsQUFGRCxJQUVDIn0=

/***/ }),
/* 76 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content class=xui-page-content--no-padding page-title=\"_t(My Orion Deployment)\" _ta> <xui-tabs xui-tabs-router> <xui-tab tab-id=deployment.summary tab-title=_t(Servers) _ta></xui-tab> <xui-tab tab-id=deployment.healthpage tab-title=\"_t(Deployment Health)\" _ta></xui-tab> <xui-tab tab-id=deployment.update tab-title=\"_t(Updates & Evaluations)\" _ta></xui-tab> <xui-tab tab-id=deployment.diagnostics tab-title=_t(Diagnostics) _ta></xui-tab> </xui-tabs> </xui-page-content> ";

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var update_config_1 = __webpack_require__(78);
__webpack_require__(80);
exports.default = function (module) {
    module.config(update_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlEQUEyQztBQUMzQyx5QkFBdUI7QUFFdkIsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQUMsdUJBQVksQ0FBQyxDQUFDO0FBQ2hDLENBQUMsQ0FBQyJ9

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var update_controller_1 = __webpack_require__(7);
function UpdateConfig($stateProvider) {
    $stateProvider
        .state("deployment.update", {
        title: "Updates Available",
        url: "/update",
        controller: update_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(79)
    });
}
UpdateConfig.$inject = ["$stateProvider"];
exports.default = UpdateConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInVwZGF0ZS1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx1Q0FBdUM7QUFDdkMseURBQW1EO0FBRW5ELHNCQUFzQixjQUFvQztJQUN0RCxjQUFjO1NBQ1QsS0FBSyxDQUFDLG1CQUFtQixFQUFPO1FBQzdCLEtBQUssRUFBRSxtQkFBbUI7UUFDMUIsR0FBRyxFQUFFLFNBQVM7UUFDZCxVQUFVLEVBQUUsMkJBQWdCO1FBQzVCLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQVMsZUFBZSxDQUFDO0tBQzdDLENBQUMsQ0FBQztBQUNYLENBQUM7QUFFRCxZQUFZLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztBQUUxQyxrQkFBZSxZQUFZLENBQUMifQ==

/***/ }),
/* 79 */
/***/ (function(module, exports) {

module.exports = "<div class=\"ha-update-page xui-margin-lgv-neg\" ng-init=vm.SaveAuthenticationCookie()> <ha-sizeable-frame ng-if=vm.showIframe src=/Administration/SolarWinds.Administration.CentralizedUpgrade.Web.centralized.html></ha-sizeable-frame> </div> ";

/***/ }),
/* 80 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var healthpage_config_1 = __webpack_require__(82);
exports.default = function (module) {
    module.config(healthpage_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlEQUFtRDtBQUVuRCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQywyQkFBZ0IsQ0FBQyxDQUFDO0FBQ3BDLENBQUMsQ0FBQyJ9

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var healthpage_controller_1 = __webpack_require__(83);
function HealthpageConfig($stateProvider) {
    $stateProvider
        .state("deployment.healthpage", {
        title: "Deployment Health",
        url: "/healthpage",
        controller: healthpage_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(84)
    });
}
HealthpageConfig.$inject = ["$stateProvider"];
exports.default = HealthpageConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhbHRocGFnZS1jb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJoZWFsdGhwYWdlLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHVDQUF1QztBQUN2QyxpRUFBMkQ7QUFFM0QsMEJBQTBCLGNBQW9DO0lBQzFELGNBQWM7U0FDVCxLQUFLLENBQUMsdUJBQXVCLEVBQU87UUFDakMsS0FBSyxFQUFFLG1CQUFtQjtRQUMxQixHQUFHLEVBQUUsYUFBYTtRQUNsQixVQUFVLEVBQUUsK0JBQW9CO1FBQ2hDLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQVMsbUJBQW1CLENBQUM7S0FDakQsQ0FBQyxDQUFDO0FBQ1gsQ0FBQztBQUVELGdCQUFnQixDQUFDLE9BQU8sR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFFOUMsa0JBQWUsZ0JBQWdCLENBQUMifQ==

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var update_controller_1 = __webpack_require__(7);
var HealthpageController = /** @class */ (function (_super) {
    __extends(HealthpageController, _super);
    function HealthpageController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return HealthpageController;
}(update_controller_1.default));
exports.default = HealthpageController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhbHRocGFnZS1jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaGVhbHRocGFnZS1jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLGlFQUEyRDtBQUUzRDtJQUFrRCx3Q0FBZ0I7SUFBbEU7O0lBRUEsQ0FBQztJQUFELDJCQUFDO0FBQUQsQ0FBQyxBQUZELENBQWtELDJCQUFnQixHQUVqRSJ9

/***/ }),
/* 84 */
/***/ (function(module, exports) {

module.exports = "<div class=\"ha-update-page margin-top-neg\" ng-init=vm.SaveAuthenticationCookie()> <ha-sizeable-frame ng-if=vm.showIframe src=/Administration/SolarWinds.Administration.HealthPage.Web.healthpage.html></ha-sizeable-frame> </div>";

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var diagnostics_config_1 = __webpack_require__(86);
exports.default = function (module) {
    module.config(diagnostics_config_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJEQUFxRDtBQUVyRCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyw0QkFBaUIsQ0FBQyxDQUFDO0FBQ3JDLENBQUMsQ0FBQyJ9

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="../../ref.d.ts" />
var diagnostics_controller_1 = __webpack_require__(87);
function DiagnosticsConfig($stateProvider) {
    $stateProvider
        .state("deployment.diagnostics", {
        title: "Diagnostics",
        url: "/diagnostics",
        controller: diagnostics_controller_1.default,
        controllerAs: "vm",
        template: __webpack_require__(88)
    });
}
DiagnosticsConfig.$inject = ["$stateProvider"];
exports.default = DiagnosticsConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhZ25vc3RpY3MtY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGlhZ25vc3RpY3MtY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsdUNBQXVDO0FBQ3ZDLG1FQUE2RDtBQUU3RCwyQkFBMkIsY0FBb0M7SUFDM0QsY0FBYztTQUNULEtBQUssQ0FBQyx3QkFBd0IsRUFBTztRQUNsQyxLQUFLLEVBQUUsYUFBYTtRQUNwQixHQUFHLEVBQUUsY0FBYztRQUNuQixVQUFVLEVBQUUsZ0NBQXFCO1FBQ2pDLFlBQVksRUFBRSxJQUFJO1FBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQUMsb0JBQW9CLENBQUM7S0FDMUMsQ0FBQyxDQUFDO0FBQ1gsQ0FBQztBQUVELGlCQUFpQixDQUFDLE9BQU8sR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFFL0Msa0JBQWUsaUJBQWlCLENBQUMifQ==

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var update_controller_1 = __webpack_require__(7);
var DiagnosticsController = /** @class */ (function (_super) {
    __extends(DiagnosticsController, _super);
    function DiagnosticsController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return DiagnosticsController;
}(update_controller_1.default));
exports.default = DiagnosticsController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhZ25vc3RpY3MtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRpYWdub3N0aWNzLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsaUVBQTJEO0FBRTNEO0lBQW1ELHlDQUFnQjtJQUFuRTs7SUFFQSxDQUFDO0lBQUQsNEJBQUM7QUFBRCxDQUFDLEFBRkQsQ0FBbUQsMkJBQWdCLEdBRWxFIn0=

/***/ }),
/* 88 */
/***/ (function(module, exports) {

module.exports = "<div class=\"ha-update-page xui-margin-lgv-neg\" ng-init=vm.SaveAuthenticationCookie()> <ha-sizeable-frame ng-if=vm.showIframe src=/Administration/SolarWinds.Administration.Dow.Web.dow.html></ha-sizeable-frame> </div>";

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(0);
exports.default = function (module) {
    module.service("haconstants", constants_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlDQUFvQztBQUVwQyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsbUJBQVMsQ0FBQyxDQUFDO0FBQzdDLENBQUMsQ0FBQyJ9

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var actionResultModelErrorMessage_filter_1 = __webpack_require__(91);
exports.default = function (module) {
    module.filter("actionResultModelErrorMessage", actionResultModelErrorMessage_filter_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLCtGQUF5RjtBQUV6RixrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQywrQkFBK0IsRUFBRyw4Q0FBbUMsQ0FBQyxDQUFDO0FBQ3pGLENBQUMsQ0FBQyJ9

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function ActionResultModelErrorMessageFilter(_t) {
    var installedModulesDoesNotMatch = 14;
    var constructErrorMessageDifferentProductsInstalled = function (resDataModel) {
        var resultMessage = "";
        if (_.isEmpty(resDataModel)) {
            return resultMessage;
        }
        resultMessage = _t("There are different products and/or versions installed on pool members:");
        resDataModel.forEach(function (opResult) {
            resultMessage += "<br/>" + opResult.poolMemberHostName + " - ";
            var firstModule = true;
            opResult.modulesInfo.forEach(function (moduleInfo) {
                var otherMembers = resDataModel
                    .filter(function (item) { return item.poolMemberId !== opResult.poolMemberId; });
                if (!firstModule) {
                    resultMessage += ", ";
                }
                if (otherMembers
                    .filter(function (item) { return item.modulesInfo.filter(function (m) { return m.displayName === moduleInfo.displayName; })
                    .length >
                    0; })
                    .length ===
                    0) {
                    resultMessage += "<span class='incorrectModuleVersion'> " + moduleInfo.name + " " + moduleInfo.version + "</span>";
                }
                else {
                    resultMessage += "<span> " + moduleInfo.name + " " + moduleInfo.version + "</span>";
                }
                firstModule = false;
            });
        });
        return resultMessage;
    };
    return function (response) {
        console.log(response);
        if (_.isEmpty(response)) {
            return "";
        }
        var message = response.errorMessage || response.message;
        if (response.errorCode === installedModulesDoesNotMatch) {
            message = constructErrorMessageDifferentProductsInstalled(response.result);
        }
        return message;
    };
}
;
ActionResultModelErrorMessageFilter.$inject = ["getTextService"];
exports.default = ActionResultModelErrorMessageFilter;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aW9uUmVzdWx0TW9kZWxFcnJvck1lc3NhZ2UtZmlsdGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWN0aW9uUmVzdWx0TW9kZWxFcnJvck1lc3NhZ2UtZmlsdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0EsNkNBQTZDLEVBQW9DO0lBQzdFLElBQUksNEJBQTRCLEdBQVcsRUFBRSxDQUFDO0lBRTlDLElBQUksK0NBQStDLEdBQUcsVUFBQyxZQUFpQztRQUVwRixJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDdkIsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsTUFBTSxDQUFDLGFBQWEsQ0FBQztRQUN6QixDQUFDO1FBRUQsYUFBYSxHQUFHLEVBQUUsQ0FBQyx5RUFBeUUsQ0FBQyxDQUFDO1FBRTlGLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRO1lBQzFCLGFBQWEsSUFBSSxVQUFRLFFBQVEsQ0FBQyxrQkFBa0IsUUFBSyxDQUFDO1lBQzFELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQztZQUN2QixRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQVU7Z0JBQ3BDLElBQUksWUFBWSxHQUFHLFlBQVk7cUJBQzFCLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxZQUFZLEtBQUssUUFBUSxDQUFDLFlBQVksRUFBM0MsQ0FBMkMsQ0FBQyxDQUFDO2dCQUNqRSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQ2YsYUFBYSxJQUFJLElBQUksQ0FBQztnQkFDMUIsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxZQUFZO3FCQUNYLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFdBQVcsS0FBSyxVQUFVLENBQUMsV0FBVyxFQUF4QyxDQUF3QyxDQUFDO3FCQUNqRixNQUFNO29CQUNQLENBQUMsRUFGVyxDQUVYLENBQUM7cUJBQ0wsTUFBTTtvQkFDUCxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNKLGFBQWEsSUFBSSwyQ0FBeUMsVUFBVSxDQUFDLElBQUksU0FBSSxVQUFVLENBQUMsT0FBTyxZQUNsRixDQUFDO2dCQUNsQixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLGFBQWEsSUFBSSxZQUFVLFVBQVUsQ0FBQyxJQUFJLFNBQUksVUFBVSxDQUFDLE9BQU8sWUFBUyxDQUFDO2dCQUM5RSxDQUFDO2dCQUVELFdBQVcsR0FBRyxLQUFLLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxhQUFhLENBQUM7SUFDekIsQ0FBQyxDQUFDO0lBRUYsTUFBTSxDQUFDLFVBQUMsUUFBZ0Q7UUFDcEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0QixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QixNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ2QsQ0FBQztRQUVELElBQUksT0FBTyxHQUFXLFFBQVEsQ0FBQyxZQUFZLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQztRQUNoRSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxLQUFLLDRCQUE0QixDQUFDLENBQUMsQ0FBQztZQUN0RCxPQUFPLEdBQUcsK0NBQStDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9FLENBQUM7UUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUMsQ0FBQztBQUNOLENBQUM7QUFBQSxDQUFDO0FBRUYsbUNBQW1DLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztBQUNqRSxrQkFBZSxtQ0FBbUMsQ0FBQyJ9

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(93);
var index_2 = __webpack_require__(99);
var index_3 = __webpack_require__(103);
var index_4 = __webpack_require__(105);
var index_5 = __webpack_require__(111);
var index_6 = __webpack_require__(113);
var index_7 = __webpack_require__(117);
var poolproperties_1 = __webpack_require__(119);
var preferredmember_1 = __webpack_require__(123);
var sizeableframe_1 = __webpack_require__(128);
__webpack_require__(131);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
    index_7.default(module);
    poolproperties_1.default(module);
    preferredmember_1.default(module);
    sizeableframe_1.default(module);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFDQUF1QztBQUN2QyxpREFBK0Q7QUFDL0QsMERBQWlGO0FBQ2pGLDZDQUF1RDtBQUN2RCxrREFBaUU7QUFDakUsNkNBQXVEO0FBQ3ZELG1EQUFtRTtBQUNuRSxtREFBdUQ7QUFDdkQscURBQXlEO0FBQ3pELGlEQUE0QztBQUM1Qyw2QkFBMkI7QUFFM0Isa0JBQWUsVUFBQyxNQUFlO0lBQzNCLGVBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNyQixlQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLGVBQWlDLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUMsZUFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM3QixlQUF5QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLGVBQW9CLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDN0IsZUFBMEIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyx3QkFBdUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoQyx5QkFBd0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyx1QkFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzFCLENBQUMsQ0FBQyJ9

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var virtualIp_directive_1 = __webpack_require__(94);
exports.default = function (module) {
    module.component("virtualip", virtualIp_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDZEQUE4QztBQUU5QyxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsNkJBQVMsQ0FBQyxDQUFDO0FBQzdDLENBQUMsQ0FBQyJ9

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var virtualIp_controller_1 = __webpack_require__(95);
var multiTemplate_directive_1 = __webpack_require__(3);
var VirtualIp = /** @class */ (function (_super) {
    __extends(VirtualIp, _super);
    function VirtualIp() {
        var _this = _super.call(this) || this;
        _this.restrict = "E";
        _this.controller = virtualIp_controller_1.default;
        _this.controllerAs = "vipCtrl";
        _this.templateHtml = __webpack_require__(97);
        _this.readOnlyTemplateHtml = __webpack_require__(98);
        return _this;
    }
    return VirtualIp;
}(multiTemplate_directive_1.default));
exports.VirtualIp = VirtualIp;
exports.default = VirtualIp;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlydHVhbElwLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpcnR1YWxJcC1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBQWdFO0FBQ2hFLHNFQUF1RDtBQUV2RDtJQUErQiw2QkFBYTtJQUt4QztRQUFBLFlBQ0ksaUJBQU8sU0FHVjtRQVJNLGNBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixnQkFBVSxHQUFHLDhCQUEwQixDQUFDO1FBQ3hDLGtCQUFZLEdBQUcsU0FBUyxDQUFDO1FBSTVCLEtBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFTLDRCQUE0QixDQUFDLENBQUM7UUFDbEUsS0FBSSxDQUFDLG9CQUFvQixHQUFHLE9BQU8sQ0FBUyxvQ0FBb0MsQ0FBQyxDQUFDOztJQUN0RixDQUFDO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDLEFBVkQsQ0FBK0IsaUNBQWEsR0FVM0M7QUFWWSw4QkFBUztBQVl0QixrQkFBZSxTQUFTLENBQUMifQ==

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
var VirtualIpControlController = /** @class */ (function () {
    function VirtualIpControlController(_t, xuiDialogService, $scope) {
        var _this = this;
        this._t = _t;
        this.xuiDialogService = xuiDialogService;
        this.$scope = $scope;
        this.openVirtualIpAddressDialog = function () {
            _this.xuiDialogService.showModal({
                template: __webpack_require__(96)
            }, {
                title: _this._t("What is a Virtual IP Address?"),
                hideCancel: true,
                actionButtonText: _this._t("OK")
            });
        };
    }
    return VirtualIpControlController;
}());
VirtualIpControlController.$inject = ["getTextService", "xuiDialogService", "$scope"];
exports.default = VirtualIpControlController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlydHVhbElwLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2aXJ0dWFsSXAtY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUF5RSxDQUFDO0FBRTFFO0lBQ0ksb0NBQ1ksRUFBb0MsRUFDcEMsZ0JBQW9DLEVBQ3BDLE1BQTRCO1FBSHhDLGlCQUtDO1FBSlcsT0FBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDcEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFvQjtRQUNwQyxXQUFNLEdBQU4sTUFBTSxDQUFzQjtRQUlqQywrQkFBMEIsR0FBRztZQUNoQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFpQztnQkFDNUQsUUFBUSxFQUFFLE9BQU8sQ0FBUyx5REFBeUQsQ0FBQzthQUN2RixFQUN1QjtnQkFDaEIsS0FBSyxFQUFFLEtBQUksQ0FBQyxFQUFFLENBQUMsK0JBQStCLENBQUM7Z0JBQy9DLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixnQkFBZ0IsRUFBRSxLQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzthQUNsQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7SUFYRixDQUFDO0lBWUwsaUNBQUM7QUFBRCxDQUFDLEFBbEJELElBa0JDO0FBQ0QsMEJBQTBCLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsa0JBQWtCLEVBQUUsUUFBUSxDQUFDLENBQUM7QUFFdEYsa0JBQWUsMEJBQTBCLENBQUMifQ==

/***/ }),
/* 96 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <p class=text-left _t> The VIP is a unique address shared by members of the pool. It is used to receive any communication from the network into the pool; for example, syslogs, traps, NetFlow, and so on. If your primary server goes down, a secondary server continues to receive communication from the network into the pool. </p> <div class=text-center> <img src=modules/ha/images/ha_diagram.svg alt=\"_t(virtual ip address)\" _ta/> </div> </xui-dialog> ";

/***/ }),
/* 97 */
/***/ (function(module, exports) {

module.exports = "<label _t>Virtual IP Address:</label> <div class=\"ha-directive clearfix\"> <xui-textbox name=virtualIp class=\"textbox-field pull-left\" type=text ng-model=pool.pluginsData.virtualIpResource.ipAddress help-text=\"_t(The Virtual IP must be on the same subnet as the servers in this pool.)\" validators=customipvalidator ng-model-options=\"{ updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } }\" _ta> <div ng-message=required _t>Enter an IP Address</div> <div ng-message=customip _t>Invalid IP Address</div> </xui-textbox> <a href=# class=textbox-sufix ng-click=vipCtrl.openVirtualIpAddressDialog()> <xui-icon icon=double-caret-right></xui-icon> <span _t>What is a Virtual IP Address?</span> </a> </div>";

/***/ }),
/* 98 */
/***/ (function(module, exports) {

module.exports = "<div class=ha-directive> <div _t>Virtual IP Address</div> <div class=bold> {{pool.pluginsData.virtualIpResource.ipAddress}} </div> </div> ";

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var virtualHostName_directive_1 = __webpack_require__(100);
exports.default = function (module) {
    module.component("virtualhostname", virtualHostName_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlFQUEwRDtBQUUxRCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxtQ0FBZSxDQUFDLENBQUM7QUFDekQsQ0FBQyxDQUFDIn0=

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var multiTemplate_directive_1 = __webpack_require__(3);
var VirtualHostName = /** @class */ (function (_super) {
    __extends(VirtualHostName, _super);
    function VirtualHostName() {
        var _this = _super.call(this) || this;
        _this.restrict = "E";
        _this.templateHtml = __webpack_require__(101);
        _this.readOnlyTemplateHtml = __webpack_require__(102);
        return _this;
    }
    return VirtualHostName;
}(multiTemplate_directive_1.default));
exports.VirtualHostName = VirtualHostName;
exports.default = VirtualHostName;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlydHVhbEhvc3ROYW1lLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpcnR1YWxIb3N0TmFtZS1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsc0VBQXVEO0FBRXZEO0lBQXFDLG1DQUFhO0lBRzlDO1FBQUEsWUFDSSxpQkFBTyxTQUdWO1FBTk0sY0FBUSxHQUFHLEdBQUcsQ0FBQztRQUlsQixLQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBUyxrQ0FBa0MsQ0FBQyxDQUFDO1FBQ3hFLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxPQUFPLENBQVMsMENBQTBDLENBQUMsQ0FBQzs7SUFDNUYsQ0FBQztJQUNMLHNCQUFDO0FBQUQsQ0FBQyxBQVJELENBQXFDLGlDQUFhLEdBUWpEO0FBUlksMENBQWU7QUFVNUIsa0JBQWUsZUFBZSxDQUFDIn0=

/***/ }),
/* 101 */
/***/ (function(module, exports) {

module.exports = "<div class=\"ha-directive clearfix\"> <xui-textbox name=virtualhostname class=textbox-field caption=\"_t(Virtual Host Name:)\" _ta type=text ng-model=pool.pluginsData.virtualHostNameResource.hostName is-required=!areMembersFromSameSubnet validators=virtualhostnamevalidator ng-maxlenth=63> <div ng-message=virtualhostnamevalidator _t>Virtual host name is not valid</div> <div ng-message=required _t>Virtual host name is required</div> </xui-textbox> </div> ";

/***/ }),
/* 102 */
/***/ (function(module, exports) {

module.exports = "<div class=ha-directive> <div _t>Virtual Hostname</div> <div class=bold> {{pool.pluginsData.virtualHostNameResource.hostName}} </div> </div> ";

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var virtualHostNameValidator_directive_1 = __webpack_require__(104);
exports.default = function (module) {
    module.component(virtualHostNameValidator_directive_1.default.virtualHostNameValidatorName, virtualHostNameValidator_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJGQUE0RTtBQUU1RSxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyw0Q0FBd0IsQ0FBQyw0QkFBNEIsRUFBRSw0Q0FBd0IsQ0FBQyxDQUFDO0FBQ3RHLENBQUMsQ0FBQyJ9

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var VirtualHostNameValidator = /** @class */ (function () {
    function VirtualHostNameValidator() {
        this.require = "ngModel";
        this.restrict = "A";
        this.link = function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$validators.virtualhostnamevalidator = function (modelValue) {
                return !modelValue || (modelValue.length <= 63
                    && (new RegExp("^[a-zA-Z0-9-]+$")).test(modelValue));
            };
        };
    }
    VirtualHostNameValidator.virtualHostNameValidatorName = "virtualhostnamevalidator";
    return VirtualHostNameValidator;
}());
exports.VirtualHostNameValidator = VirtualHostNameValidator;
exports.default = VirtualHostNameValidator;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlydHVhbEhvc3ROYW1lVmFsaWRhdG9yLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZpcnR1YWxIb3N0TmFtZVZhbGlkYXRvci1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQUFBO1FBR1csWUFBTyxHQUFHLFNBQVMsQ0FBQztRQUNwQixhQUFRLEdBQUcsR0FBRyxDQUFDO1FBRWYsU0FBSSxHQUFHLFVBQUMsS0FBZ0IsRUFBRSxPQUE0QixFQUFFLEtBQXFCLEVBQUUsV0FBZ0I7WUFDbEcsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNmLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxXQUFXLENBQUMsV0FBVyxDQUFDLHdCQUF3QixHQUFHLFVBQUMsVUFBa0I7Z0JBQ2xFLE1BQU0sQ0FBQyxDQUFDLFVBQVUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksRUFBRTt1QkFDdkMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDN0QsQ0FBQyxDQUFDO1FBQ04sQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQWRpQixxREFBNEIsR0FBRywwQkFBMEIsQ0FBQztJQWM1RSwrQkFBQztDQUFBLEFBaEJELElBZ0JDO0FBaEJZLDREQUF3QjtBQWtCckMsa0JBQWUsd0JBQXdCLENBQUMifQ==

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dnsSettings_directive_1 = __webpack_require__(106);
__webpack_require__(110);
__webpack_require__(17);
exports.default = function (module) {
    module.component("dnssettings", dnsSettings_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlFQUFrRDtBQUNsRCxpQ0FBK0I7QUFDL0Isd0NBQXNDO0FBRXRDLGtCQUFlLFVBQUMsTUFBZTtJQUMzQixNQUFNLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSwrQkFBVyxDQUFDLENBQUM7QUFDakQsQ0FBQyxDQUFDIn0=

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(17);

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var multiTemplate_directive_1 = __webpack_require__(3);
var dnsSettings_controller_1 = __webpack_require__(107);
var DnsSettings = /** @class */ (function (_super) {
    __extends(DnsSettings, _super);
    function DnsSettings() {
        var _this = _super.call(this) || this;
        _this.restrict = "E";
        _this.controller = dnsSettings_controller_1.default;
        _this.controllerAs = "dnsSettingsCtrl";
        _this.templateHtml = __webpack_require__(108);
        _this.readOnlyTemplateHtml = __webpack_require__(109);
        return _this;
    }
    return DnsSettings;
}(multiTemplate_directive_1.default));
exports.DnsSettings = DnsSettings;
exports.default = DnsSettings;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG5zU2V0dGluZ3MtZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZG5zU2V0dGluZ3MtZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHNFQUF1RDtBQUN2RCxtRUFBNkQ7QUFFN0Q7SUFBaUMsK0JBQWE7SUFLMUM7UUFBQSxZQUNJLGlCQUFPLFNBR1Y7UUFSTSxjQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsZ0JBQVUsR0FBRyxnQ0FBcUIsQ0FBQztRQUNuQyxrQkFBWSxHQUFHLGlCQUFpQixDQUFDO1FBSXBDLEtBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFTLDhCQUE4QixDQUFDLENBQUM7UUFDcEUsS0FBSSxDQUFDLG9CQUFvQixHQUFHLE9BQU8sQ0FBUyxzQ0FBc0MsQ0FBQyxDQUFDOztJQUN4RixDQUFDO0lBQ0wsa0JBQUM7QUFBRCxDQUFDLEFBVkQsQ0FBaUMsaUNBQWEsR0FVN0M7QUFWWSxrQ0FBVztBQVl4QixrQkFBZSxXQUFXLENBQUMifQ==

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(0);
var haBase_controller_1 = __webpack_require__(6);
var DnsSettingsController = /** @class */ (function (_super) {
    __extends(DnsSettingsController, _super);
    function DnsSettingsController($scope, addPoolService, _t, haconstants) {
        var _this = _super.call(this) || this;
        _this.$scope = $scope;
        _this.addPoolService = addPoolService;
        _this._t = _t;
        _this.haconstants = haconstants;
        _this.$onInit = function () {
            _this.hidePassword();
            _this.applyTexts(false);
            _this.$scope.$watch("pool.pluginsData.virtualHostNameResource.dnsType", function () {
                _this.isBindSelected = _this.isOtherSelected = false;
                if (_this.$scope.pool.pluginsData &&
                    _this.$scope.pool.pluginsData.virtualHostNameResource &&
                    _this.$scope.pool.pluginsData.virtualHostNameResource.dnsType) {
                    _this.isBindSelected =
                        _this.$scope.pool.pluginsData.virtualHostNameResource.dnsType === _this.$scope.pool.dnsTypes[1];
                    _this.applyTexts(_this.isBindSelected);
                    _this.isOtherSelected =
                        _this.$scope.pool.pluginsData.virtualHostNameResource.dnsType === _this.$scope.pool.dnsTypes[2];
                }
            });
            _this.$scope.$watch("errorMessage", function () {
                _this.testErrorMessage = _this.$scope.errorMessage;
                _this.testSuccessMessage = "";
            });
            _this.$scope.$on(_this.haconstants.addPoolStepEnterEvent, function () { _this.hideDnsSettingsTestMessage(); });
        };
        _this.showPassword = function () {
            if (_this.$scope.pool.pluginsData.virtualHostNameResource &&
                _this.$scope.pool.pluginsData.virtualHostNameResource.dnsPassword) {
                _this.dnsPassword = _this.$scope.pool.pluginsData.virtualHostNameResource.dnsPassword;
            }
        };
        _this.showFipsError = function () {
            return _this.$scope.isFipsEnabled && _this.isBindSelected;
        };
        _this.hidePassword = function () {
            _this.dnsPassword = "****************";
        };
        return _this;
    }
    DnsSettingsController.prototype.validatePool = function () {
        var _this = this;
        if (!this.validateForm(this.$scope["dnsSettingsForm"])) {
            return;
        }
        this.hideDnsSettingsTestMessage();
        this.$scope.$emit(this.haconstants.validatePoolStartEvent);
        this.addPoolService.validatePool(this.$scope.pool.getNormalizedWithPoolPlugin(constants_1.default.pluginsNames.virtualHostNameResource))
            .then(function (actionResult) {
            _this.$scope.$emit(_this.haconstants.validatePoolEndEvent);
            _this.showDnsSettingsTestMessage(actionResult.errorMessage, actionResult.errorCode);
        });
    };
    DnsSettingsController.prototype.applyTexts = function (isBind) {
        if (isBind) {
            this.userNameLabel = this._t("TSIG Shared Secret Key Name");
            this.passwordLabel = this._t("TSIG Shared Secret Key Value");
            this.userNameValidationMsg = this._t("TSIG Key Name is required");
        }
        else {
            this.userNameLabel = this._t("User Name");
            this.passwordLabel = this._t("Password");
            this.userNameValidationMsg = this._t("You must enter a user name that can update DNS records.");
        }
    };
    DnsSettingsController.prototype.showDnsSettingsTestMessage = function (message, errorCode) {
        this.hideDnsSettingsTestMessage();
        if (errorCode === 0 || errorCode === 25 || errorCode === 32) {
            this.testSuccessMessage = this._t("Test Successful.");
            return;
        }
        this.testErrorMessage = this._t(message);
    };
    DnsSettingsController.prototype.hideDnsSettingsTestMessage = function () {
        this.testSuccessMessage = "";
        this.testErrorMessage = "";
    };
    return DnsSettingsController;
}(haBase_controller_1.default));
DnsSettingsController.$inject = [
    "$scope",
    "addPoolService",
    "getTextService",
    "haconstants"
];
exports.default = DnsSettingsController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG5zU2V0dGluZ3MtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRuc1NldHRpbmdzLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBR0Esb0RBQStDO0FBQy9DLG9FQUE4RDtBQUU5RDtJQUFvQyx5Q0FBZ0I7SUFVaEQsK0JBQ1csTUFBNEIsRUFDM0IsY0FBNEIsRUFDNUIsRUFBb0MsRUFDcEMsV0FBc0I7UUFKbEMsWUFNSSxpQkFBTyxTQUNWO1FBTlUsWUFBTSxHQUFOLE1BQU0sQ0FBc0I7UUFDM0Isb0JBQWMsR0FBZCxjQUFjLENBQWM7UUFDNUIsUUFBRSxHQUFGLEVBQUUsQ0FBa0M7UUFDcEMsaUJBQVcsR0FBWCxXQUFXLENBQVc7UUFLM0IsYUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFdkIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsa0RBQWtELEVBQUU7Z0JBQ25FLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7Z0JBRW5ELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVc7b0JBQzVCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUI7b0JBQ3BELEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUMvRCxLQUFJLENBQUMsY0FBYzt3QkFDZixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsT0FBTyxLQUFLLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFbEcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBRXJDLEtBQUksQ0FBQyxlQUFlO3dCQUNoQixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsT0FBTyxLQUFLLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEcsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFO2dCQUMvQixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7Z0JBQ2pELEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7WUFDakMsQ0FBQyxDQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixFQUFFLGNBQVEsS0FBSSxDQUFDLDBCQUEwQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxRyxDQUFDLENBQUM7UUFFSyxrQkFBWSxHQUFHO1lBQ2xCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUI7Z0JBQ3BELEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNuRSxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUM7WUFDeEYsQ0FBQztRQUNMLENBQUMsQ0FBQTtRQUVNLG1CQUFhLEdBQUc7WUFDbkIsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUM7UUFDNUQsQ0FBQyxDQUFBO1FBa0JNLGtCQUFZLEdBQUc7WUFDbEIsS0FBSSxDQUFDLFdBQVcsR0FBRyxrQkFBa0IsQ0FBQztRQUMxQyxDQUFDLENBQUE7O0lBM0RELENBQUM7SUF5Q00sNENBQVksR0FBbkI7UUFBQSxpQkFjQztRQWJHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckQsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUVELElBQUksQ0FBQywwQkFBMEIsRUFBRSxDQUFDO1FBRWxDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsbUJBQVMsQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsQ0FBQzthQUM1RixJQUFJLENBQUMsVUFBQyxZQUE4QztZQUNqRCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDekQsS0FBSSxDQUFDLDBCQUEwQixDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQU1PLDBDQUFVLEdBQWxCLFVBQW1CLE1BQWU7UUFDOUIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNULElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDdEUsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyx5REFBeUQsQ0FBQyxDQUFDO1FBQ3BHLENBQUM7SUFDTCxDQUFDO0lBRU8sMERBQTBCLEdBQWxDLFVBQW1DLE9BQWUsRUFBRSxTQUFpQjtRQUNqRSxJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztRQUVsQyxFQUFFLENBQUMsQ0FBQyxTQUFTLEtBQUssQ0FBQyxJQUFJLFNBQVMsS0FBSyxFQUFFLElBQUksU0FBUyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUN0RCxNQUFNLENBQUM7UUFDWCxDQUFDO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVPLDBEQUEwQixHQUFsQztRQUNJLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBQ0wsNEJBQUM7QUFBRCxDQUFDLEFBekdELENBQW9DLDJCQUFnQixHQXlHbkQ7QUFDRCxxQkFBcUIsQ0FBQyxPQUFPLEdBQUc7SUFDNUIsUUFBUTtJQUNSLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsYUFBYTtDQUFDLENBQUM7QUFFbkIsa0JBQWUscUJBQXFCLENBQUMifQ==

/***/ }),
/* 108 */
/***/ (function(module, exports) {

module.exports = "<div class=\"ha-directive clearfix\"> <ng-form name=dnsSettingsForm> <label _t>DNS Type</label> <xui-dropdown name=dnsTypeDropdown placeholder=\"_t(Please select)\" _ta items-source=pool.dnsTypes ng-model=pool.pluginsData.virtualHostNameResource.dnsType is-required=true display-value-fn=dnsSettingsCtrl._t(item)> <div ng-message=required _t>DNS type is required</div> </xui-dropdown> <div ng-if=dnsSettingsCtrl.showFipsError() class=bindDnsFipsError> <xui-message name=fipsErrorMessage type=error> <span _t>BIND is not supported when FIPS is enabled</span> </xui-message> </div> <div ng-style=\"{'visibility' : pool.pluginsData.virtualHostNameResource.dnsType ? 'visible' : 'hidden'}\"> <label _t>DNS Server IP Address</label> <xui-textbox name=dnsIpAddress class=textbox-field placeholder=192.168.2.10 ng-model=pool.pluginsData.virtualHostNameResource.dnsIP help-text=\"_t(Enter the IPv4 address of the DNS server.)\" validators=required,customipvalidator _ta> <div ng-message=required _t>You must enter an IPv4 formatted DNS server address.</div> <div ng-message=customip _t>You must enter an IPv4 formatted DNS server address.</div> </xui-textbox> <label _t>DNS Zone</label> <xui-textbox name=dnsZone class=textbox-field placeholder=public.local ng-model=pool.pluginsData.virtualHostNameResource.dnsZone help-text=\"_t(Enter the primary or master DNS zone.)\" validators=required,dnszonevalidator _ta> <div ng-message=required _t>You must enter the primary or master DNS zone.</div> <div ng-message=dnszonevalidator _t>You must enter the primary or master DNS zone.</div> </xui-textbox> <div ng-if=dnsSettingsCtrl.isOtherSelected> <xui-message type=warning allow-dismiss=false class=otherDnsMessageBox> <span> <b _t>Wait! You aren't quite done - Alert Manager setup is needed to support other DNS types.</b> <span _t>To support redundancy, you need to configure an alert that will update the DNS server entry on failover. An example alert has been provided that you can use for guidance.</span> </span> <sw-help-link topic=OrionCoreHAAlertAfterFailoverConfig class=link target=_blank rel=\"noopener noreferrer\" icon=double-caret-right> <span _t>Help me configure this alert</span> </sw-help-link> </xui-message> </div> <div ng-if=!dnsSettingsCtrl.isOtherSelected> <div class=box-left> <label>{{dnsSettingsCtrl.userNameLabel}}</label> <xui-textbox name=dnsUserName class=textbox-field ng-model=pool.pluginsData.virtualHostNameResource.dnsUserName validators=required _ta> <div ng-message=required>{{dnsSettingsCtrl.userNameValidationMsg}}</div> </xui-textbox> <label>{{dnsSettingsCtrl.passwordLabel}}</label> <xui-textbox ng-if=!dnsSettingsCtrl.isBindSelected name=dnsPassword type=password class=textbox-field ng-model=pool.pluginsData.virtualHostNameResource.dnsPassword validators=required _ta> <div ng-message=required _t>You must enter the correct password for the user name.</div> </xui-textbox> <xui-textbox ng-if=dnsSettingsCtrl.isBindSelected name=dnsPassword class=textbox-field-double ng-model=pool.pluginsData.virtualHostNameResource.dnsPassword validators=required _ta> <div ng-message=required _t>TSIG Key Value is required</div> </xui-textbox> <div style=min-height:60px> <div class=box-left> <xui-button id=dnsTestButton title=\"_t(Test DNS Settings)\" ng-click=dnsSettingsCtrl.validatePool() display-style=secondary _ta _t>TEST</xui-button> </div> <div class=box-left style=margin-left:15px> <xui-message name=testErrorMessage type=error ng-show=dnsSettingsCtrl.testErrorMessage> <span ng-bind-html=dnsSettingsCtrl.testErrorMessage></span> </xui-message> <xui-message name=testSuccessMessage type=success ng-show=dnsSettingsCtrl.testSuccessMessage> <span ng-bind-html=dnsSettingsCtrl.testSuccessMessage></span> </xui-message> </div> </div> </div> </div> </div> </ng-form> </div> ";

/***/ }),
/* 109 */
/***/ (function(module, exports) {

module.exports = "<div class=ha-directive> <div class=field> <div _t>DNS Type</div> <div class=bold> {{pool.pluginsData.virtualHostNameResource.dnsType}} </div> </div> <div class=field> <div _t>DNS Server IP Address</div> <div class=bold> {{pool.pluginsData.virtualHostNameResource.dnsIP}} </div> </div> <div class=field> <div _t>DNS Zone</div> <div class=bold> {{pool.pluginsData.virtualHostNameResource.dnsZone}} </div> </div> <div ng-if=!dnsSettingsCtrl.isOtherSelected> <div class=field> <div>{{dnsSettingsCtrl.userNameLabel}}</div> <div class=bold> {{pool.pluginsData.virtualHostNameResource.dnsUserName}} </div> </div> <div class=field> <div>{{dnsSettingsCtrl.passwordLabel}}</div> <div ng-if=!dnsSettingsCtrl.isBindSelected class=bold> <div class=passwordBox>{{dnsSettingsCtrl.dnsPassword}}</div> <div>&nbsp;&nbsp;<img src=modules/ha/images/show.svg class=passwordEye ng-mousedown=dnsSettingsCtrl.showPassword() ng-mouseup=dnsSettingsCtrl.hidePassword() alt=\"_t(Show password)\" title=\"_t(Show password)\" _ta> </div> </div> <div ng-if=dnsSettingsCtrl.isBindSelected class=bold> {{pool.pluginsData.virtualHostNameResource.dnsPassword}} </div> </div> </div> </div>";

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/show.svg";

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dnsZoneValidator_directive_1 = __webpack_require__(112);
exports.default = function (module) {
    module.component(dnsZoneValidator_directive_1.default.dnsZoneValidator, dnsZoneValidator_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJFQUE0RDtBQUU1RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxvQ0FBZ0IsQ0FBQyxnQkFBZ0IsRUFBRSxvQ0FBZ0IsQ0FBQyxDQUFDO0FBQzFFLENBQUMsQ0FBQyJ9

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var DnsZoneValidator = /** @class */ (function () {
    function DnsZoneValidator() {
        var _this = this;
        this.require = "ngModel";
        this.restrict = "A";
        this.link = function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$validators.dnszonevalidator = function (modelValue) {
                if (!modelValue) {
                    return true;
                }
                if (modelValue.length > 253) {
                    return false;
                }
                var labels = modelValue.split(".");
                for (var _i = 0, labels_1 = labels; _i < labels_1.length; _i++) {
                    var label = labels_1[_i];
                    if (_this.validateLabel(label) === false) {
                        return false;
                    }
                }
                return true;
            };
        };
    }
    DnsZoneValidator.prototype.validateLabel = function (label) {
        var asciiNameRegex = new RegExp("^[a-zA-Z0-9-]+$");
        if (!label || label.length === 0 || label.length > 63) {
            return false;
        }
        if (!asciiNameRegex.test(label)) {
            return false;
        }
        return true;
    };
    DnsZoneValidator.dnsZoneValidator = "dnszonevalidator";
    return DnsZoneValidator;
}());
exports.DnsZoneValidator = DnsZoneValidator;
exports.default = DnsZoneValidator;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG5zWm9uZVZhbGlkYXRvci1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkbnNab25lVmFsaWRhdG9yLWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQUE7UUFBQSxpQkF5Q0M7UUF2Q1UsWUFBTyxHQUFHLFNBQVMsQ0FBQztRQUNwQixhQUFRLEdBQUcsR0FBRyxDQUFDO1FBY2YsU0FBSSxHQUFHLFVBQUMsS0FBZ0IsRUFBRSxPQUE0QixFQUFFLEtBQXFCLEVBQUUsV0FBZ0I7WUFDbEcsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNmLE1BQU0sQ0FBQztZQUNYLENBQUM7WUFFRCxXQUFXLENBQUMsV0FBVyxDQUFDLGdCQUFnQixHQUFHLFVBQUMsVUFBa0I7Z0JBQzFELEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNoQixDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDMUIsTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDakIsQ0FBQztnQkFFRCxJQUFNLE1BQU0sR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyQyxHQUFHLENBQUMsQ0FBYyxVQUFNLEVBQU4saUJBQU0sRUFBTixvQkFBTSxFQUFOLElBQU07b0JBQW5CLElBQUksS0FBSyxlQUFBO29CQUNWLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDdEMsTUFBTSxDQUFDLEtBQUssQ0FBQztvQkFDakIsQ0FBQztpQkFDSjtnQkFFRCxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQztRQUNOLENBQUMsQ0FBQTtJQUNMLENBQUM7SUFwQ1csd0NBQWEsR0FBckIsVUFBc0IsS0FBYTtRQUMvQixJQUFNLGNBQWMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3JELEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwRCxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQWRhLGlDQUFnQixHQUFHLGtCQUFrQixDQUFDO0lBd0N4RCx1QkFBQztDQUFBLEFBekNELElBeUNDO0FBekNZLDRDQUFnQjtBQTJDN0Isa0JBQWUsZ0JBQWdCLENBQUMifQ==

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var poolMembers_directive_1 = __webpack_require__(114);
__webpack_require__(18);
exports.default = function (module) {
    module.component("poolmembers", poolMembers_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLGlFQUFrRDtBQUNsRCx3Q0FBc0M7QUFFdEMsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLCtCQUFXLENBQUMsQ0FBQztBQUNqRCxDQUFDLENBQUMifQ==

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(18);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var poolMembers_controller_1 = __webpack_require__(115);
var PoolMembers = /** @class */ (function () {
    function PoolMembers() {
        this.restrict = "E";
        this.controller = poolMembers_controller_1.default;
        this.controllerAs = "poolMembersCtrl";
        this.template = __webpack_require__(116);
    }
    return PoolMembers;
}());
exports.PoolMembers = PoolMembers;
exports.default = PoolMembers;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbE1lbWJlcnMtZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9vbE1lbWJlcnMtZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbUVBQTZEO0FBRTdEO0lBQUE7UUFDVyxhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsZUFBVSxHQUFHLGdDQUFxQixDQUFDO1FBQ25DLGlCQUFZLEdBQUcsaUJBQWlCLENBQUM7UUFDakMsYUFBUSxHQUFHLE9BQU8sQ0FBUyw4QkFBOEIsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFBRCxrQkFBQztBQUFELENBQUMsQUFMRCxJQUtDO0FBTFksa0NBQVc7QUFPeEIsa0JBQWUsV0FBVyxDQUFDIn0=

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var haBase_controller_1 = __webpack_require__(6);
var PoolMembersController = /** @class */ (function (_super) {
    __extends(PoolMembersController, _super);
    function PoolMembersController($scope, _t) {
        var _this = _super.call(this) || this;
        _this.$scope = $scope;
        _this._t = _t;
        _this.isActivePreferred = false;
        _this.isBackupPreferred = false;
        _this.$onInit = function () {
            _this.$scope.$watch("pool.pluginsData.poolConfiguration.preferredMemberId", function () {
                _this.isActivePreferred = _this.isBackupPreferred = false;
                if (_this.$scope.pool.pluginsData &&
                    _this.$scope.pool.pluginsData.poolConfiguration &&
                    _this.$scope.pool.pluginsData.poolConfiguration.preferredMemberId &&
                    _this.$scope.availableServersSelection &&
                    _this.$scope.availableServersSelection.length > 0) {
                    var preferredMemberId = Number(_this.$scope.pool.pluginsData.poolConfiguration.preferredMemberId);
                    if (preferredMemberId === 0) {
                        return;
                    }
                    _this.isActivePreferred = preferredMemberId
                        === _this.$scope.availableServersSelection[0].poolMemberId;
                    _this.isBackupPreferred = !_this.isActivePreferred;
                }
            });
        };
        return _this;
    }
    return PoolMembersController;
}(haBase_controller_1.default));
PoolMembersController.$inject = ["$scope", "getTextService"];
exports.default = PoolMembersController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbE1lbWJlcnMtY29udHJvbGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBvb2xNZW1iZXJzLWNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQ0Esb0VBQThEO0FBRTlEO0lBQW9DLHlDQUFnQjtJQUtoRCwrQkFDVyxNQUE0QixFQUMzQixFQUFvQztRQUZoRCxZQUlJLGlCQUFPLFNBQ1Y7UUFKVSxZQUFNLEdBQU4sTUFBTSxDQUFzQjtRQUMzQixRQUFFLEdBQUYsRUFBRSxDQUFrQztRQUx6Qyx1QkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBU25DLGFBQU8sR0FBRztZQUNiLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLHNEQUFzRCxFQUFFO2dCQUN2RSxLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztnQkFFeEQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVztvQkFDNUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQjtvQkFDOUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQjtvQkFDaEUsS0FBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUI7b0JBQ3JDLEtBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ25ELElBQU0saUJBQWlCLEdBQUcsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO29CQUVuRyxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMxQixNQUFNLENBQUM7b0JBQ1gsQ0FBQztvQkFFRCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsaUJBQWlCOzRCQUNsQyxLQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztvQkFDOUQsS0FBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDO2dCQUNyRCxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7O0lBdEJGLENBQUM7SUF1QkwsNEJBQUM7QUFBRCxDQUFDLEFBakNELENBQW9DLDJCQUFnQixHQWlDbkQ7QUFDRCxxQkFBcUIsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxRQUFRLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztBQUU3RCxrQkFBZSxxQkFBcUIsQ0FBQyJ9

/***/ }),
/* 116 */
/***/ (function(module, exports) {

module.exports = "<div class=\"ha-directive poolMember-directive clearfix\"> <div class=server-map> <div class=\"serverWrapper pull-left\"> <div class=\"server pull-left\"> <div class=\"pull-left node-icon\"> <xui-icon icon=orion-sitemaster status={{availableServersSelection[0].iconStatus}}></xui-icon> </div> <div class=\"node-details pull-left\"> <div class=text-caption> {{availableServersSelection[0].displayName}} </div> <div class=text-muted> {{availableServersSelection[0].ipAddress}} </div> <div class=text-description>{{availableServersSelection[0].description ? poolMembersCtrl._t(availableServersSelection[0].description) : ''}}</div> </div> </div> <div class=preferredMember ng-if=poolMembersCtrl.isActivePreferred> <xui-icon icon=checkmark icon-color=gray icon-size=small></xui-icon> <span _t>Preferred active server</span> </div> </div> <div class=\"connector pull-left\" _t> will be protected by: </div> <div class=\"serverWrapper pull-left\"> <div class=\"server pull-left\"> <div class=\"pull-left node-icon\"> <xui-icon icon={{backupServer.icon}} status={{backupServer.iconStatus}}></xui-icon> </div> <div class=\"node-details pull-left\"> <div class=text-caption> {{backupServer.displayName}} </div> <div class=text-muted> {{backupServer.ipAddress}} </div> <div class=text-description> {{backupServer.description ? poolMembersCtrl._t(backupServer.description) : ''}} </div> </div> </div> <div class=preferredMember ng-if=poolMembersCtrl.isBackupPreferred> <xui-icon icon=checkmark icon-color=gray icon-size=small></xui-icon> <span _t>Preferred active server</span> </div> </div> </div> </div>";

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var customipValidatorDirective_1 = __webpack_require__(118);
exports.default = function (module) {
    module.component("customipvalidator", customipValidatorDirective_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDJFQUE2RDtBQUU3RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRSxvQ0FBaUIsQ0FBQyxDQUFDO0FBQzdELENBQUMsQ0FBQyJ9

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var CustomIpValidator = /** @class */ (function () {
    function CustomIpValidator() {
        this.require = "ngModel";
        this.restrict = "";
        this.link = function (scope, element, attrs, ctrl) {
            if (ctrl) {
                ctrl.$parsers.unshift(function (modelValue) {
                    if (modelValue === "") {
                        ctrl.$setValidity("customip", true);
                    }
                    else {
                        var isValid = (new RegExp("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.)" +
                            "{3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")).test(modelValue);
                        ctrl.$setValidity("customip", isValid);
                    }
                    return modelValue;
                });
            }
        };
    }
    return CustomIpValidator;
}());
exports.CustomIpValidator = CustomIpValidator;
exports.default = CustomIpValidator;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9taXBWYWxpZGF0b3JEaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjdXN0b21pcFZhbGlkYXRvckRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQUE7UUFFVyxZQUFPLEdBQUcsU0FBUyxDQUFDO1FBQ3BCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFFZCxTQUFJLEdBQUcsVUFBQyxLQUFnQixFQUFFLE9BQTRCLEVBQUUsS0FBcUIsRUFBRSxJQUFTO1lBQzNGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFlO29CQUNsQyxFQUFFLENBQUMsQ0FBQyxVQUFVLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzt3QkFDcEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ3hDLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osSUFBSSxPQUFPLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxrREFBa0Q7NEJBQ3hFLDhDQUE4QyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQ3RFLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUMzQyxDQUFDO29CQUVELE1BQU0sQ0FBQyxVQUFVLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztRQUNMLENBQUMsQ0FBQTtJQUNMLENBQUM7SUFBRCx3QkFBQztBQUFELENBQUMsQUFwQkQsSUFvQkM7QUFwQlksOENBQWlCO0FBc0I5QixrQkFBZSxpQkFBaUIsQ0FBQyJ9

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var poolProperties_directive_1 = __webpack_require__(120);
exports.default = function (module) {
    module.component("poolproperties", poolProperties_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHVFQUF3RDtBQUV4RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxrQ0FBYyxDQUFDLENBQUM7QUFDdkQsQ0FBQyxDQUFDIn0=

/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var multiTemplate_directive_1 = __webpack_require__(3);
var PoolProperties = /** @class */ (function (_super) {
    __extends(PoolProperties, _super);
    function PoolProperties() {
        var _this = _super.call(this) || this;
        _this.restrict = "E";
        _this.templateHtml = __webpack_require__(121);
        _this.readOnlyTemplateHtml = __webpack_require__(122);
        return _this;
    }
    return PoolProperties;
}(multiTemplate_directive_1.default));
exports.PoolProperties = PoolProperties;
exports.default = PoolProperties;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9vbFByb3BlcnRpZXMtZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9vbFByb3BlcnRpZXMtZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHNFQUF1RDtBQUV2RDtJQUFvQyxrQ0FBYTtJQUc3QztRQUFBLFlBQ0ksaUJBQU8sU0FHVjtRQU5NLGNBQVEsR0FBRyxHQUFHLENBQUM7UUFJbEIsS0FBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQVMsaUNBQWlDLENBQUMsQ0FBQztRQUN2RSxLQUFJLENBQUMsb0JBQW9CLEdBQUcsT0FBTyxDQUFTLHlDQUF5QyxDQUFDLENBQUM7O0lBQzNGLENBQUM7SUFDTCxxQkFBQztBQUFELENBQUMsQUFSRCxDQUFvQyxpQ0FBYSxHQVFoRDtBQVJZLHdDQUFjO0FBVTNCLGtCQUFlLGNBQWMsQ0FBQyJ9

/***/ }),
/* 121 */
/***/ (function(module, exports) {

module.exports = "<div class=ha-directive> <div class=field> <label _t>Pool Name:</label> <xui-textbox name=poolName class=textbox-field ng-model=vm.$scope.pool.displayName validators=required _ta> <div ng-message=required _t>Enter a pool name</div> </xui-textbox> </div> <div class=field ng-if=vm.$scope.areMembersFromSameSubnet> <virtualip></virtualip> </div> <div class=field> <virtualhostname></virtualhostname> </div> </div>";

/***/ }),
/* 122 */
/***/ (function(module, exports) {

module.exports = "<div class=ha-directive> <div class=field> <div _t>Pool Name</div> <div class=bold>{{pool.displayName}}</div> </div> <div class=field ng-if=vm.$scope.pool.pluginsData.virtualIpResource.ipAddress> <virtualip is-read-only=true /> </div> <div class=field ng-if=vm.$scope.pool.pluginsData.virtualHostNameResource.hostName> <virtualhostname is-read-only=true /> </div> </div>";

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var preferredMember_directive_1 = __webpack_require__(124);
__webpack_require__(19);
exports.default = function (module) {
    module.component("preferredmember", preferredMember_directive_1.default);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlFQUFzRDtBQUN0RCw0Q0FBMEM7QUFFMUMsa0JBQWUsVUFBQyxNQUFlO0lBQzNCLE1BQU0sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsbUNBQVcsQ0FBQyxDQUFDO0FBQ3JELENBQUMsQ0FBQyJ9

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(19);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var preferredMember_controller_1 = __webpack_require__(125);
var PreferredMember = /** @class */ (function () {
    function PreferredMember() {
        this.restrict = "E";
        this.template = __webpack_require__(127);
        this.controller = preferredMember_controller_1.default;
        this.controllerAs = "preferredMemberCtrl";
    }
    return PreferredMember;
}());
exports.PreferredMember = PreferredMember;
exports.default = PreferredMember;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlZmVycmVkTWVtYmVyLWRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInByZWZlcnJlZE1lbWJlci1kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwyRUFBNEU7QUFFNUU7SUFBQTtRQUNXLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixhQUFRLEdBQUcsT0FBTyxDQUFTLGtDQUFrQyxDQUFDLENBQUM7UUFDL0QsZUFBVSxHQUFHLG9DQUFnQyxDQUFDO1FBQzlDLGlCQUFZLEdBQUcscUJBQXFCLENBQUM7SUFDaEQsQ0FBQztJQUFELHNCQUFDO0FBQUQsQ0FBQyxBQUxELElBS0M7QUFMWSwwQ0FBZTtBQU81QixrQkFBZSxlQUFlLENBQUMifQ==

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(0);
var PreferredMemberControlController = /** @class */ (function () {
    function PreferredMemberControlController(_t, xuiDialogService, $scope, $q, addPoolService) {
        var _this = this;
        this._t = _t;
        this.xuiDialogService = xuiDialogService;
        this.$scope = $scope;
        this.$q = $q;
        this.addPoolService = addPoolService;
        this.backupAsPreferredMessage = this._t("You cannot choose a backup server while creating a pool. Edit the pool later to choose a backup server.");
        this.$onInit = function () {
            _this.$scope.$watch("pool.pluginsData.poolConfiguration.preferredMemberId", function (value, oldValue) {
                if (_this.$scope.backupServer === undefined || value === undefined || value === oldValue) {
                    return;
                }
                //No warning for the first time when Edit pool and backup server is selected
                if (_this.$scope.pool.poolId && oldValue === undefined
                    && value === _this.$scope.backupServer.poolMemberId) {
                    return;
                }
                _this.addPoolService.validatePool(_this.$scope.pool.getNormalizedWithPoolPlugin(constants_1.default.pluginsNames.poolConfiguration))
                    .then(function (result) {
                    if (result.errorCode === 32) {
                        _this.openBackupSelectedAsPreferredDialog(Number(value), Number(oldValue));
                    }
                });
            });
        };
    }
    PreferredMemberControlController.prototype.openBackupSelectedAsPreferredDialog = function (value, oldValue) {
        var _this = this;
        var result = this.$q.defer();
        this.xuiDialogService.showModal({
            template: __webpack_require__(126),
            scope: this.$scope
        }, {
            title: this._t("FAILOVER WILL OCCUR"),
            hideCancel: false,
            actionButtonText: this._t("YES, MAKE PREFERRED"),
            status: "warning",
            cancelButtonText: this._t("CANCEL")
        })
            .then(function (dialogResult) {
            if (dialogResult === "cancel") {
                _this.$scope.pool.pluginsData.poolConfiguration.preferredMemberId = oldValue;
                return;
            }
            result.resolve(true);
        });
        return result.promise;
    };
    return PreferredMemberControlController;
}());
PreferredMemberControlController.$inject = [
    "getTextService",
    "xuiDialogService",
    "$scope",
    "$q",
    "addPoolService"
];
exports.default = PreferredMemberControlController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlZmVycmVkTWVtYmVyLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcmVmZXJyZWRNZW1iZXItY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBLG9EQUErQztBQUcvQztJQUlJLDBDQUNZLEVBQW9DLEVBQ3BDLGdCQUFvQyxFQUNwQyxNQUE0QixFQUM1QixFQUFnQixFQUNoQixjQUE0QjtRQUx4QyxpQkFNSTtRQUxRLE9BQUUsR0FBRixFQUFFLENBQWtDO1FBQ3BDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBb0I7UUFDcEMsV0FBTSxHQUFOLE1BQU0sQ0FBc0I7UUFDNUIsT0FBRSxHQUFGLEVBQUUsQ0FBYztRQUNoQixtQkFBYyxHQUFkLGNBQWMsQ0FBYztRQVBqQyw2QkFBd0IsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLHlHQUF5RyxDQUFDLENBQUM7UUFVOUksWUFBTyxHQUFHO1lBQ2IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsc0RBQXNELEVBQUUsVUFBQyxLQUFLLEVBQUUsUUFBUTtnQkFDdkYsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxTQUFTLElBQUksS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3RGLE1BQU0sQ0FBQztnQkFDWCxDQUFDO2dCQUVELDRFQUE0RTtnQkFDNUUsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLFFBQVEsS0FBSyxTQUFTO3VCQUM5QyxLQUFLLEtBQXdCLEtBQUksQ0FBQyxNQUFNLENBQUMsWUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQ3pFLE1BQU0sQ0FBQztnQkFDWCxDQUFDO2dCQUVELEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUM1QixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxtQkFBUyxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO3FCQUN0RixJQUFJLENBQUMsVUFBQyxNQUF3QztvQkFDM0MsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUUxQixLQUFJLENBQUMsbUNBQW1DLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUM5RSxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7SUF2QkMsQ0FBQztJQXlCSSw4RUFBbUMsR0FBM0MsVUFBNEMsS0FBYSxFQUFFLFFBQWdCO1FBQTNFLGlCQXNCQztRQXJCRyxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRS9CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQWlDO1lBQzVELFFBQVEsRUFBRSxPQUFPLENBQVMsdURBQXVELENBQUM7WUFDbEYsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3JCLEVBQ3VCO1lBQ2hCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ3JDLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDaEQsTUFBTSxFQUFFLFNBQVM7WUFDakIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7U0FDdEMsQ0FBQzthQUNELElBQUksQ0FBQyxVQUFDLFlBQVk7WUFDZixFQUFFLENBQUMsQ0FBQyxZQUFZLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDNUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQztnQkFDNUUsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUNELE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUM7UUFDUCxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUMxQixDQUFDO0lBQ0wsdUNBQUM7QUFBRCxDQUFDLEFBMURELElBMERDO0FBRUQsZ0NBQWdDLENBQUMsT0FBTyxHQUFHO0lBQ3ZDLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLElBQUk7SUFDSixnQkFBZ0I7Q0FBQyxDQUFDO0FBRXRCLGtCQUFlLGdDQUFnQyxDQUFDIn0=

/***/ }),
/* 126 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog> <p class=text-left> <span _t>If yout set</span>&nbsp;&lt;{{ backupServer.displayName }}&gt; ({{ backupServer.ipAddress }})&nbsp;<span _t>to the preferred active server, the HA pool will immediately failover to</span>&nbsp;&lt;{{ backupServer.displayName }}&gt; ({{ backupServer.ipAddress}}) <br/><br/><span _t>Are you sure you want to set a preferred active server?</span><br/> </p> </xui-dialog>";

/***/ }),
/* 127 */
/***/ (function(module, exports) {

module.exports = "<div class=\"ha-directive preferredMember-directive clearfix\"> <xui-expander is-open=false heading=\"_t(Preferred Server Settings)\" _ta> <div class=\"server-map pull-left\"> <label class=clearfix _t>Preferred Active Server</label> <div class=\"server clearfix\"> <xui-radio class=pull-left name=preferredMember ng-model=pool.pluginsData.poolConfiguration.preferredMemberId ng-value=availableServersSelection[0].poolMemberId /> <div class=\"pull-left node-icon\"> <xui-icon icon=orion-sitemaster status={{availableServersSelection[0].iconStatus}}></xui-icon> </div> <div class=\"node-details pull-left\"> <div class=text-caption> {{availableServersSelection[0].displayName}} </div> <div class=text-muted> {{availableServersSelection[0].ipAddress}} </div> <div class=text-description>{{availableServersSelection[0].description ? preferredMemberCtrl._t(availableServersSelection[0].description) : ''}}</div> </div> </div> <div class=\"server clearfix\"> <xui-popover class=pull-left ng-if=!pool.poolId xui-popover-title=\"_t(Backup server as preferred)\" xui-popover-content=::preferredMemberCtrl.backupAsPreferredMessage xui-popover-trigger=\"mouseenter click\" xui-popover-placement=right xui-popover-status=warning _ta> <xui-radio class=preferredBackupMemberRadio name=preferredMember ng-model=pool.pluginsData.poolConfiguration.preferredMemberId ng-value=backupServer.poolMemberId is-disabled=true></xui-radio> </xui-popover> <xui-radio class=\"pull-left preferredBackupMemberRadio\" name=preferredMember ng-if=pool.poolId ng-model=pool.pluginsData.poolConfiguration.preferredMemberId ng-value=backupServer.poolMemberId></xui-radio> <div class=\"pull-left node-icon\"> <xui-icon icon={{backupServer.icon}} status={{backupServer.iconStatus}}></xui-icon> </div> <div class=\"node-details pull-left\"> <div class=text-caption> {{backupServer.displayName}} </div> <div class=text-muted> {{backupServer.ipAddress}} </div> <div class=text-description> {{backupServer.description ? preferredMemberCtrl._t(backupServer.description) : ''}} </div> </div> </div> <div class=\"server clearfix\"> <xui-radio name=preferredMember ng-model=pool.pluginsData.poolConfiguration.preferredMemberId ng-value=0 _t>No preferred active server</xui-radio> </div> </div> <div class=clearfix> <xui-message type=info allow-dismiss=false class=preferredMemberInfo> <span> <span _t>If a server is set as the preferred active server, it is always the active whenever it is available.</span> <br/><br/> <span _t> If a system failure occurs and the preferred server goes offline, Orion will fail over to the non-preferred server. As soon as the preferred server is online again, Orion will fail back to that server, and the non-preferred server will return to a standby state. </span> </span> <br/><br/> <sw-help-link topic=OrionCoreHAPreferredMemberConfig class=link target=_blank rel=\"noopener noreferrer\" icon=double-caret-right> <span _t>Learn more about settings a preferred active server</span> </sw-help-link> </xui-message> </div> </xui-expander> </div>";

/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var sizeableFrame_directive_1 = __webpack_require__(129);
exports.default = function (module) {
    module.directive("haSizeableFrame", sizeableFrame_directive_1.default.Factory());
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFFQUFzRDtBQUV0RCxrQkFBZSxVQUFDLE1BQWU7SUFDM0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxpQ0FBYSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7QUFDakUsQ0FBQyxDQUFDIn0=

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var SizeableFrame = /** @class */ (function () {
    function SizeableFrame() {
        this.scope = {
            src: "@"
        };
        this.restrict = "E";
        this.template = __webpack_require__(130);
        this.link = function (scope, element) {
            var iframe = element.find("iframe");
            if (!(iframe[0] instanceof HTMLIFrameElement)) {
                throw new Error("IFrame expected");
            }
            var iframeElement = iframe[0];
            var handler = function (event) {
                if (event.data.name === "centralizedUpgrade.resize") {
                    iframeElement.style.height = event.data.height + "px";
                }
            };
            window.addEventListener("message", handler);
            iframe.attr("src", scope.src);
            element.on("$destroy", function () {
                window.removeEventListener("message", handler);
            });
        };
    }
    SizeableFrame.Factory = function () {
        var directive = function () {
            return new SizeableFrame();
        };
        return directive;
    };
    return SizeableFrame;
}());
exports.default = SizeableFrame;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2l6ZWFibGVGcmFtZS1kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzaXplYWJsZUZyYW1lLWRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBO0lBQUE7UUFFSSxVQUFLLEdBQUc7WUFDSixHQUFHLEVBQUUsR0FBRztTQUNYLENBQUM7UUFDRixhQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ2YsYUFBUSxHQUFHLE9BQU8sQ0FBUyxnQ0FBZ0MsQ0FBQyxDQUFDO1FBRXRELFNBQUksR0FBRyxVQUFDLEtBQXlCLEVBQUUsT0FBNEI7WUFDbEUsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0QyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxZQUFZLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM1QyxNQUFNLElBQUksS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDdkMsQ0FBQztZQUVELElBQU0sYUFBYSxHQUFzQixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkQsSUFBTSxPQUFPLEdBQUcsVUFBQyxLQUFtQjtnQkFDaEMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssMkJBQTJCLENBQUMsQ0FBQyxDQUFDO29CQUNsRCxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQzFELENBQUM7WUFDTCxDQUFDLENBQUM7WUFFRixNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzVDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUU5QixPQUFPLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRTtnQkFDbkIsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNuRCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQTtJQVFMLENBQUM7SUFOVSxxQkFBTyxHQUFkO1FBQ0ksSUFBTSxTQUFTLEdBQUc7WUFDZCxNQUFNLENBQUMsSUFBSSxhQUFhLEVBQUUsQ0FBQztRQUMvQixDQUFDLENBQUM7UUFDRixNQUFNLENBQUMsU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFDTCxvQkFBQztBQUFELENBQUMsQUFuQ0QsSUFtQ0MifQ==

/***/ }),
/* 130 */
/***/ (function(module, exports) {

module.exports = "<iframe class=update-content></iframe>";

/***/ }),
/* 131 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=ha.js.map