/*!
 * @solarwinds/recommendations 2.1.2-394
 * @copyright 2020 SolarWinds, Inc.
 * http://solarwinds.com/
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 42);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(9));
__export(__webpack_require__(10));
__export(__webpack_require__(15));
/**
 * Names of all registered services
 *
 * @class
 */
var ServiceNames = /** @class */ (function () {
    function ServiceNames() {
    }
    ServiceNames.batchingService = "recommendationsBatchingService";
    ServiceNames.cacheService = "recommendationsCacheService";
    ServiceNames.dataGroupStatusService = "recommendationsDataGroupStatusService";
    ServiceNames.dialogService = "recommendationsDialogService";
    ServiceNames.intervalCallerService = "recommendationsIntervalCallerService";
    ServiceNames.orionInfoService = "recommendationsOrionInfoService";
    ServiceNames.pluggabilityService = "recommendationsPluggabilityService";
    ServiceNames.recommendationListFiltersService = "recommendationListFiltersService";
    ServiceNames.recommendationListParamsService = "recommendationListParamsService";
    ServiceNames.routingService = "recommendationsRoutingService";
    ServiceNames.statsService = "recommendationsStatsService";
    ServiceNames.statusService = "recommendationsStatusService";
    ServiceNames.swApiService = "recommendationsSwApiService";
    ServiceNames.swisService = "recommendationsSwisService";
    ServiceNames.swProductsService = "recommendationsSwProductsService";
    ServiceNames.dateTimeService = "recommendationsDateTimeService";
    ServiceNames.stringService = "recommendationsStringService";
    ServiceNames.permissionsService = "recommendationsPermissionsService";
    ServiceNames.routingRegistrationService = "recommendationsRoutingRegistration";
    ServiceNames.apolloSwisService = "swisService";
    ServiceNames.apolloSwApiService = "swApi";
    ServiceNames.getTextService = "getTextService";
    ServiceNames.apolloConstants = "constants";
    ServiceNames.apolloDateTimeService = "swDateTimeService";
    ServiceNames.demoService = "swDemoService";
    ServiceNames.xuiDialogService = "xuiDialogService";
    ServiceNames.xuiToastService = "xuiToastService";
    ServiceNames.ngInjectorService = "$injector";
    ServiceNames.ngQService = "$q";
    ServiceNames.ngProvideService = "$provide";
    ServiceNames.ngControllerService = "$controller";
    ServiceNames.ngRootScopeService = "$rootScope";
    ServiceNames.ngScope = "$scope";
    ServiceNames.ngTimeoutService = "$timeout";
    ServiceNames.ngIntervalService = "$interval";
    ServiceNames.ngCompileService = "$compile";
    ServiceNames.ngFilterService = "$filter";
    ServiceNames.ngTemplateCacheService = "$templateCache";
    ServiceNames.ngHttpBackendService = "$httpBackend";
    ServiceNames.ngLocationProvider = "$locationProvider";
    ServiceNames.ngState = "$state";
    ServiceNames.ngStateParams = "$stateParams";
    ServiceNames.ngUrlService = "$urlService";
    ServiceNames.ngLog = "$log";
    ServiceNames.ngDateFilter = "dateFilter";
    return ServiceNames;
}());
exports.ServiceNames = ServiceNames;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Helper function for autogenerating of class's $inject property
 *
 * @param {string} dependency Name of a dependency under which injected component is registered (e.g. xuiToastService)
 * @example
 * import {Inject} from "decorators/di";
 * class MyClass { constructor(@Inject("xuiToastService") private svc: IXuiToastService){} };
 */
function Inject(dependency) {
    return function (prototype, method, argumentPosition) {
        prototype.$inject = prototype.$inject || [];
        prototype.$inject[argumentPosition] = dependency;
    };
}
exports.Inject = Inject;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Recommendation priority enum
 *
 * @enum
 */
var RecommendationPriority;
(function (RecommendationPriority) {
    RecommendationPriority[RecommendationPriority["High"] = 1] = "High";
    RecommendationPriority[RecommendationPriority["Medium"] = 2] = "Medium";
    RecommendationPriority[RecommendationPriority["Low"] = 3] = "Low";
})(RecommendationPriority = exports.RecommendationPriority || (exports.RecommendationPriority = {}));
;
/**
 * Recommendation kind enum
 *
 * @enum
 */
var RecommendationKind;
(function (RecommendationKind) {
    RecommendationKind[RecommendationKind["regular"] = 0] = "regular";
    RecommendationKind[RecommendationKind["realtime"] = 1] = "realtime";
})(RecommendationKind = exports.RecommendationKind || (exports.RecommendationKind = {}));
/**
 * Batch status enum
 *
 * @enum
 */
var BatchStatus;
(function (BatchStatus) {
    BatchStatus[BatchStatus["Scheduled"] = 0] = "Scheduled";
    BatchStatus[BatchStatus["Running"] = 1] = "Running";
    BatchStatus[BatchStatus["FinishedSuccessfully"] = 2] = "FinishedSuccessfully";
    BatchStatus[BatchStatus["FinishedWithError"] = 3] = "FinishedWithError";
    BatchStatus[BatchStatus["Paused"] = 4] = "Paused";
    BatchStatus[BatchStatus["NotFound"] = 5] = "NotFound";
    BatchStatus[BatchStatus["Canceling"] = 6] = "Canceling";
    BatchStatus[BatchStatus["Canceled"] = 7] = "Canceled";
    BatchStatus[BatchStatus["Pending"] = 8] = "Pending";
    // Dummy states forcing the running/finished tab to filter batches that have/haven't revert batch in progress
    BatchStatus[BatchStatus["RevertBatchInProgress_Indication"] = 9] = "RevertBatchInProgress_Indication";
    BatchStatus[BatchStatus["RevertBatchNotInProgress_Indication"] = 10] = "RevertBatchNotInProgress_Indication";
})(BatchStatus = exports.BatchStatus || (exports.BatchStatus = {}));
;
/**
 * Batch cancelation reason
 *
 * @enum
 */
var BatchCancelationReason;
(function (BatchCancelationReason) {
    BatchCancelationReason[BatchCancelationReason["CanceledByUser"] = 1] = "CanceledByUser";
    BatchCancelationReason[BatchCancelationReason["CanceledDueToDependencyFailure"] = 2] = "CanceledDueToDependencyFailure";
})(BatchCancelationReason = exports.BatchCancelationReason || (exports.BatchCancelationReason = {}));
/**
 * Batch action status enum
 *
 * @enum
 */
var ActionStatus;
(function (ActionStatus) {
    ActionStatus[ActionStatus["Running"] = 0] = "Running";
    ActionStatus[ActionStatus["FinishedSuccessfully"] = 1] = "FinishedSuccessfully";
    ActionStatus[ActionStatus["FinishedWithError"] = 2] = "FinishedWithError";
    ActionStatus[ActionStatus["Canceled"] = 3] = "Canceled";
    ActionStatus[ActionStatus["Canceling"] = 4] = "Canceling";
    ActionStatus[ActionStatus["Created"] = 5] = "Created";
    ActionStatus[ActionStatus["NotFound"] = 6] = "NotFound";
    ActionStatus[ActionStatus["Scheduled"] = 7] = "Scheduled";
})(ActionStatus = exports.ActionStatus || (exports.ActionStatus = {}));
;
var ExecutionType;
(function (ExecutionType) {
    ExecutionType[ExecutionType["Now"] = 1] = "Now";
    ExecutionType[ExecutionType["Schedule"] = 2] = "Schedule";
})(ExecutionType = exports.ExecutionType || (exports.ExecutionType = {}));
;
/**
 * SW product enum
 *
 * @enum
 */
var SwProduct;
(function (SwProduct) {
    SwProduct[SwProduct["VIM"] = 0] = "VIM";
    SwProduct[SwProduct["SAM"] = 1] = "SAM";
})(SwProduct = exports.SwProduct || (exports.SwProduct = {}));
/**
 * Result status of validation
 *
 * @enum
 */
var ValidationResultStatus;
(function (ValidationResultStatus) {
    ValidationResultStatus[ValidationResultStatus["Success"] = 1] = "Success";
    ValidationResultStatus[ValidationResultStatus["Warning"] = 2] = "Warning";
    ValidationResultStatus[ValidationResultStatus["Error"] = 3] = "Error";
})(ValidationResultStatus = exports.ValidationResultStatus || (exports.ValidationResultStatus = {}));
/**
 * Alert severity
 *
 * @enum
 */
var AlertSeverity;
(function (AlertSeverity) {
    AlertSeverity[AlertSeverity["Info"] = 0] = "Info";
    AlertSeverity[AlertSeverity["Warning"] = 1] = "Warning";
    AlertSeverity[AlertSeverity["Critical"] = 2] = "Critical";
})(AlertSeverity = exports.AlertSeverity || (exports.AlertSeverity = {}));


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of all registered services in VIM plugin
 *
 * @class
 */
var ServiceNames = /** @class */ (function () {
    function ServiceNames() {
    }
    ServiceNames.swisService = "vimSwisService";
    ServiceNames.vmInfoService = "vimVmInfoService";
    ServiceNames.routingService = "vimRoutingService";
    ServiceNames.dialogService = "vimDialogService";
    ServiceNames.recommendationDetailService = "vimRecommendationDetailService";
    return ServiceNames;
}());
exports.ServiceNames = ServiceNames;
;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * VIM management action type
 *
 * @enum
 */
var ManagementActionType;
(function (ManagementActionType) {
    ManagementActionType[ManagementActionType["PowerOn"] = 0] = "PowerOn";
    ManagementActionType[ManagementActionType["Resume"] = 1] = "Resume";
    ManagementActionType[ManagementActionType["PowerOff"] = 2] = "PowerOff";
    ManagementActionType[ManagementActionType["Suspend"] = 3] = "Suspend";
    ManagementActionType[ManagementActionType["Pause"] = 4] = "Pause";
    ManagementActionType[ManagementActionType["Reboot"] = 5] = "Reboot";
    ManagementActionType[ManagementActionType["TakeSnapshot"] = 6] = "TakeSnapshot";
    ManagementActionType[ManagementActionType["DeleteSnapshots"] = 7] = "DeleteSnapshots";
    ManagementActionType[ManagementActionType["GetSnapshots"] = 8] = "GetSnapshots";
    ManagementActionType[ManagementActionType["ChangeSettings"] = 9] = "ChangeSettings";
    ManagementActionType[ManagementActionType["DeleteVM"] = 10] = "DeleteVM";
    ManagementActionType[ManagementActionType["UnregisterVM"] = 11] = "UnregisterVM";
    ManagementActionType[ManagementActionType["PerformMigration"] = 12] = "PerformMigration";
    ManagementActionType[ManagementActionType["PerformRelocation"] = 13] = "PerformRelocation";
    ManagementActionType[ManagementActionType["DeleteDatastoreFile"] = 14] = "DeleteDatastoreFile";
})(ManagementActionType = exports.ManagementActionType || (exports.ManagementActionType = {}));
/**
 * VIM entity types
 * @enum
 */
var EntityType;
(function (EntityType) {
    EntityType[EntityType["VirtualMachine"] = 0] = "VirtualMachine";
    EntityType[EntityType["Host"] = 1] = "Host";
    EntityType[EntityType["Cluster"] = 2] = "Cluster";
    EntityType[EntityType["Datastore"] = 3] = "Datastore";
})(EntityType = exports.EntityType || (exports.EntityType = {}));


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Entity types
 *
 * @class
 */
var EntityTypes = /** @class */ (function () {
    function EntityTypes() {
    }
    EntityTypes.host = "VH";
    EntityTypes.resourcePool = "VRP";
    EntityTypes.virtualMachine = "VVM";
    EntityTypes.dataStore = "VMS";
    EntityTypes.cluster = "VMC";
    return EntityTypes;
}());
exports.EntityTypes = EntityTypes;
/**
 * VIM constraint types
 *
 * @class
 */
var ConstraintType = /** @class */ (function () {
    function ConstraintType() {
    }
    ConstraintType.exclude = "Exclude";
    ConstraintType.mgmtActionDisabled = "VIM.MgmtAction.Disabled";
    return ConstraintType;
}());
exports.ConstraintType = ConstraintType;
/**
 * VIM constraint parameters
 *
 * @class
 */
var ConstraintParameters = /** @class */ (function () {
    function ConstraintParameters() {
    }
    ConstraintParameters.migrationAction = "Migration";
    ConstraintParameters.changeCpuAction = "ChangeCpu";
    ConstraintParameters.changeMemoryAction = "ChangeMemory";
    ConstraintParameters.relocationAction = "Relocation";
    return ConstraintParameters;
}());
exports.ConstraintParameters = ConstraintParameters;
/**
 * Names of used params
 *
 * @class
 */
var ConstraintsEditorParamNames = /** @class */ (function () {
    function ConstraintsEditorParamNames() {
    }
    ConstraintsEditorParamNames.constraintId = "constraintId";
    return ConstraintsEditorParamNames;
}());
exports.ConstraintsEditorParamNames = ConstraintsEditorParamNames;
/**
 * VIM strategies ids
 *
 * @class
 */
var StrategyType = /** @class */ (function () {
    function StrategyType() {
    }
    StrategyType.realtimeCriticalSituation = "90f89fcc-dd53-42e2-a394-48fa7cc92ca0";
    StrategyType.criticalSituation = "72f3e62c-ccda-4d50-a14a-bc139c702328";
    StrategyType.realtimeStorageDepletion = "2e6b11b5-a311-41d3-b0f5-08baa4c2354a";
    StrategyType.storageCapacity = "cc2c2758-7c67-4fe4-ab00-c8a790768623";
    StrategyType.storagePerformance = "ca9fad10-3ec6-4f37-9e98-ab945952ccae";
    StrategyType.overallocation = "4ecab031-bef3-4be7-b080-11d878e2b795";
    StrategyType.underallocation = "87fa607e-2284-4af5-8fd4-e920705b9d9d";
    StrategyType.optimizingVmPlacement = "1ef4d17a-995a-4205-9e8f-677506cc380a";
    return StrategyType;
}());
exports.StrategyType = StrategyType;
/**
 * VIM strategies groups
 *
 * @class
 */
var StrategyGroup = /** @class */ (function () {
    function StrategyGroup() {
    }
    StrategyGroup.vmPlacement = "62192b99-bacc-475e-aaf2-b6e2b737d3c7";
    StrategyGroup.vmSizing = "abed4067-75f1-46d3-829e-c1786339185a";
    StrategyGroup.storage = "76c09ff4-f8de-4883-9f7e-05622c4c4a54";
    StrategyGroup.hostSolvingCritical = "03f39453-29fa-4741-9b19-7bd56ec0ae1a";
    return StrategyGroup;
}());
exports.StrategyGroup = StrategyGroup;
/**
 * VIM action types
 *
 * @class
 */
var ActionTypes = /** @class */ (function () {
    function ActionTypes() {
    }
    ActionTypes.changeSettings = "ChangeSettings";
    ActionTypes.performMigration = "PerformMigration";
    ActionTypes.performRelocation = "PerformRelocation";
    return ActionTypes;
}());
exports.ActionTypes = ActionTypes;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Plugin template urls
 *
 * @const
 */
exports.PluginTemplateUrls = {
    recommendationDetailActionItem: "recommendations/plugins/vim/recommendationDetailActionItem.html",
    recommendationDetailOutcomeItem: "recommendations/plugins/vim/recommendationDetailOutcomeItem.html",
    recommendationDetailOutcomeItemHeader: "recommendations/plugins/vim/recommendationDetailOutcomeItemHeader.html",
    recommendationDetailActionsSummary: "recommendations/plugins/vim/recommendationDetailActionsSummary.html",
    constraintsExplorer: "recommendations/plugins/vim/constraintsExplorer.html",
    constraintsPicker: "recommendations/plugins/vim/constraintsPicker.html",
    strategiesSettings: "recommendations/plugins/vim/strategiesSettings.html"
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of all registered providers
 *
 * @class
 */
var ProviderNames = /** @class */ (function () {
    function ProviderNames() {
    }
    ProviderNames.routingRegistrationProvider = "recommendationsRoutingRegistrationProvider";
    ProviderNames.ngStateProvider = "$stateProvider";
    ProviderNames.ngUrlRouterProvider = "$urlRouterProvider";
    return ProviderNames;
}());
exports.ProviderNames = ProviderNames;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Well known recommendation tags
 */
var WellKnownRecommendationTags = /** @class */ (function () {
    function WellKnownRecommendationTags() {
    }
    /**
     * Tag used to specify that the recommendation should appear with [now] label on UI,
     * even if its kind is regular
     */
    WellKnownRecommendationTags.isNow = "Recommendations.IsNow";
    return WellKnownRecommendationTags;
}());
exports.WellKnownRecommendationTags = WellKnownRecommendationTags;
/**
 * Possible orion user roles
 */
var Role = /** @class */ (function () {
    function Role() {
    }
    Role.admin = "admin";
    Role.demo = "demo";
    return Role;
}());
exports.Role = Role;
/**
 * Recommendation specific permissions
 */
var Permission = /** @class */ (function () {
    function Permission() {
    }
    Permission.recommendations = "recommendations";
    return Permission;
}());
exports.Permission = Permission;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Base class for services communicating through swApi
 *
 * @class
 * @abstract
 */
var BaseSwApiService = /** @class */ (function () {
    function BaseSwApiService(swApi) {
        var _this = this;
        this.swApi = swApi;
        /**
         * Calls url with GET method
         *
         * @param {string} method Get method name (e.g. GetRecommendations)
         * @param {any} request Optional request object
         */
        this.callGetMethod = function (method, request) {
            var url = _this.getControllerUrl() + "/" + method;
            return _this.swApi.api(false).one(url).get(request);
        };
        /**
         * Calls url with POST method
         *
         * @param {string} method Post method name (e.g. InsertPolicy)
         * @param {any} request Optional request object
         */
        this.callPostMethod = function (method, request) {
            return _this.swApi.api(false).one(_this.getControllerUrl()).post(method, request);
        };
        /**
         * Gets only array items from restangular result which typically contains unnecessary data
         */
        this.getArrayFromResult = function (result) {
            var arr = [];
            for (var i = 0; i < result.length; i++) {
                arr.push(result[i]);
            }
            return arr;
        };
    }
    return BaseSwApiService;
}());
exports.BaseSwApiService = BaseSwApiService;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Type guard for active dialog result
 *
 * @function
 */
exports.ActiveDetailDialogResultTypeGuard = function (value) {
    return value && typeof value.status === typeof ActiveDetailDialogResultStatus;
};
/**
 * Active detail dialog result status
 *
 * @enum
 */
var ActiveDetailDialogResultStatus;
(function (ActiveDetailDialogResultStatus) {
    ActiveDetailDialogResultStatus[ActiveDetailDialogResultStatus["CreatedConstraint"] = 1] = "CreatedConstraint";
    ActiveDetailDialogResultStatus[ActiveDetailDialogResultStatus["Apply"] = 2] = "Apply";
    ActiveDetailDialogResultStatus[ActiveDetailDialogResultStatus["Cancel"] = 3] = "Cancel";
})(ActiveDetailDialogResultStatus = exports.ActiveDetailDialogResultStatus || (exports.ActiveDetailDialogResultStatus = {}));
/**
 * Create constraint dialog result status
 *
 * @enum
 */
var CreateConstraintDialogResultStatus;
(function (CreateConstraintDialogResultStatus) {
    CreateConstraintDialogResultStatus[CreateConstraintDialogResultStatus["CreatedConstraint"] = 1] = "CreatedConstraint";
    CreateConstraintDialogResultStatus[CreateConstraintDialogResultStatus["Cancel"] = 2] = "Cancel";
})(CreateConstraintDialogResultStatus = exports.CreateConstraintDialogResultStatus || (exports.CreateConstraintDialogResultStatus = {}));
/**
 * Active recommendation detail dialog result status
 *
 * @enum
 */
var DialogResultStatus;
(function (DialogResultStatus) {
    DialogResultStatus[DialogResultStatus["Apply"] = 0] = "Apply";
    DialogResultStatus[DialogResultStatus["Cancel"] = 1] = "Cancel";
})(DialogResultStatus = exports.DialogResultStatus || (exports.DialogResultStatus = {}));
/**
 * Disable recommendations dialog result
 *
 * @enum
 */
var DisableRecommendationsDialogResult;
(function (DisableRecommendationsDialogResult) {
    DisableRecommendationsDialogResult[DisableRecommendationsDialogResult["Disable"] = 0] = "Disable";
    DisableRecommendationsDialogResult[DisableRecommendationsDialogResult["Cancel"] = 1] = "Cancel";
})(DisableRecommendationsDialogResult = exports.DisableRecommendationsDialogResult || (exports.DisableRecommendationsDialogResult = {}));
/**
 * Cancel and revert recommendation confirmation dialog
 *
 * @enum
 */
var CancelAndRevertRecommendationDialogResult;
(function (CancelAndRevertRecommendationDialogResult) {
    CancelAndRevertRecommendationDialogResult[CancelAndRevertRecommendationDialogResult["CancelAndRevert"] = 0] = "CancelAndRevert";
    CancelAndRevertRecommendationDialogResult[CancelAndRevertRecommendationDialogResult["Close"] = 1] = "Close";
})(CancelAndRevertRecommendationDialogResult = exports.CancelAndRevertRecommendationDialogResult || (exports.CancelAndRevertRecommendationDialogResult = {}));


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Holder for app-specific event names
 *
 * @class
 */
var EventNames = /** @class */ (function () {
    function EventNames() {
    }
    EventNames.prefix = "recommendations";
    EventNames.applyRecommendation = EventNames.prefix + ".applyRecommendation";
    EventNames.unscheduleRecommendations = EventNames.prefix + ".unscheduleRecommendations";
    EventNames.constraintCreated = EventNames.prefix + ".constraintCreated";
    return EventNames;
}());
exports.EventNames = EventNames;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationListFilterValues_1 = __webpack_require__(17);
/**
 * Filtering component for Recommendations module.
 *
 * @class
 * @implements IRecommendationsFilter
 **/
var RecommendationsFilter = /** @class */ (function () {
    function RecommendationsFilter() {
        this.pagination = null;
        this.sorting = null;
        this.search = null;
        this.onlyAppliedActions = null;
        this.filters = new recommendationListFilterValues_1.RecommendationListFilterValues();
    }
    return RecommendationsFilter;
}());
exports.RecommendationsFilter = RecommendationsFilter;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Representantion of the one single constraint.
 *
 * @class
 * @implements IConstraint
 **/
var Constraint = /** @class */ (function () {
    function Constraint(args) {
        if (!args) {
            return;
        }
        this.id = args.id;
        this.name = args.name;
        this.description = args.description;
        this.type = args.type;
        this.dateCreated = args.dateCreated;
        this.expirationDate = args.expirationDate;
        this.enabled = args.enabled;
        this.constraintObjects = args.constraintObjects;
        this.userId = args.userId;
        this.owner = args.owner;
        this.parameters = args.parameters;
        this.inherited = args.inherited;
        this.priority = args.priority;
    }
    ;
    return Constraint;
}());
exports.Constraint = Constraint;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var enums_1 = __webpack_require__(2);
var pluginServiceNames_1 = __webpack_require__(37);
var actionDetails_1 = __webpack_require__(204);
var batchAction_1 = __webpack_require__(39);
var index_1 = __webpack_require__(0);
/**
 * Recommendation detail dialog controller
 *
 * @class
 */
var RecommendationDetailDialogController = /** @class */ (function () {
    function RecommendationDetailDialogController($scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) {
        var _this = this;
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$log = $log;
        this.$q = $q;
        this.$compile = $compile;
        this.$timeout = $timeout;
        this.statusService = statusService;
        this.swApiService = swApiService;
        this.swisService = swisService;
        this.pluggabilityService = pluggabilityService;
        this._t = _t;
        this.dialogService = dialogService;
        this.dateTimeService = dateTimeService;
        this.actionStatus = enums_1.ActionStatus;
        this.getActionIcon = function (action) {
            // check if already present
            if (!_this.getActionIconDelegate) {
                return "";
            }
            var type;
            if (batchAction_1.TypeGuard(action)) {
                type = action.action.Type;
            }
            else if (actionDetails_1.TypeGuard(action)) {
                type = action.Type;
            }
            else {
                throw new Error("Action not recognized.");
            }
            var actionIcon = _this.getActionIconDelegate(type);
            return actionIcon;
        };
        /**
         * Computes action duration in minutes
         */
        this.computeActionDuration = function (action) {
            var dateStarted;
            var dateFinished;
            if (batchAction_1.TypeGuard(action)) {
                dateStarted = action.dateStarted;
                dateFinished = action.dateFinished;
            }
            else if (actionDetails_1.TypeGuard(action)) {
                dateStarted = action.DateStarted;
                dateFinished = action.DateFinished;
            }
            else {
                throw new Error("Action not recognized.");
            }
            if (!dateStarted || !dateFinished) {
                return null;
            }
            var started = moment(dateStarted);
            var finished = moment(dateFinished);
            var minutes = finished.diff(started, "minutes");
            if (minutes > 0) {
                return _this._t("{0} min").format(minutes);
            }
            var seconds = finished.diff(started, "seconds");
            return _this._t("{0} sec").format(seconds);
        };
        /**
         * Indicates whether the given action is prerequisite for any recommendation
         */
        this.isPrerequisite = function (action) {
            if (action.Optional) {
                return false;
            }
            // check whether recommendation connected with given action has any dependency
            var recommendation = _.find(_this.recommendations, function (r) { return r.id === action.RecommendationID; });
            if (recommendation && recommendation.dependency) {
                return true;
            }
            return false;
        };
        /**
         * Gets the dependency details context for the given action
         */
        this.getDependencyDetailsContext = function (action) {
            var recommendation = _.find(_this.recommendations, function (r) { return r.id === action.RecommendationID; });
            if (!recommendation || !recommendation.dependency) {
                return null;
            }
            return {
                reason: recommendation.dependency.reason,
                hasHighestPriority: recommendation.hasHighestPriority(),
                riskShort: recommendation.riskShort
            };
        };
        /**
         * Reacts on actions enabled property change event
         */
        this.onActionsEnabledChanged = function (e, actions, enabled) {
            e.stopPropagation();
            var matchingActions = _.filter(_this.actions, function (a) { return _.some(actions, function (a2) { return a2.ActionID === a.ActionID; }); });
            if (_.isEmpty(matchingActions)) {
                return;
            }
            _.each(matchingActions, function (a) {
                // keep orig value
                a.$Enabled = a.Enabled;
                a.Enabled = enabled;
            });
            _this.$timeout(function () {
                _this.updateFilteredActions();
                // fire event so action summary can consume that
                var summaryContext = _this.createActionSummaryContext();
                _this.$scope.$broadcast("actionSummaryContextChanged", summaryContext);
            }, 0);
        };
        this.loadRecommendations = function (recommendationIds, loadDependencies) {
            var handler;
            if (loadDependencies) {
                handler = _this.swisService.getRecommendationsWithDependencies(recommendationIds);
            }
            else {
                handler = _this.swisService.getRecommendationsWithJustification(recommendationIds);
            }
            return handler.then(_this.onRecommendationsLoaded);
        };
        this.onRecommendationsLoaded = function (result) {
            // if no recommendations get loaded, dialog closes automatically
            if (_.isEmpty(result)) {
                _this.dialogInstance.cancel();
                return _this.$q.resolve();
            }
            _this.recommendations = result;
            _this.changedEntitiesCount = _this.getChangedEntitiesCount();
            var recommendationIds = _.map(result, function (r) { return r.id; });
            return _this.swisService.getRecommendationActions(recommendationIds).then(_this.processLoadedActions);
        };
        this.processLoadedActions = function (actions) {
            return _this.pluggabilityService.getModuleServices(pluginServiceNames_1.PluginServiceNames.recommendationDetailService)
                .then(function (pluginServices) {
                if (_this.allowedChange) {
                    // process actions through plugin specific logic
                    _.each(pluginServices, function (service) { service.processActions(actions); });
                }
                _this.actions = actions || [];
                _this.updateFilteredActions();
                if (_this.actionSummaryContextDeferred) {
                    var summaryContext = _this.createActionSummaryContext();
                    _this.actionSummaryContextDeferred.resolve(summaryContext);
                }
            });
        };
        /**
         * Gets count of changed entities within all recommendations
         */
        this.getChangedEntitiesCount = function () {
            var entities = _.flatten(_.map(_this.recommendations, function (r) { return r.justification.ChangedEntities || []; }));
            var uniqueEntities = _.uniqBy(entities, function (e) { return e.Id.Id; });
            return uniqueEntities.length;
        };
        this.createActionSummaryContext = function () {
            return {
                recommendations: _this.recommendations,
                actions: _this.filteredActions
            };
        };
        this.updateFilteredActions = function () {
            var actions = _this.filterActions(_this.actions);
            // compute info specific for bound items
            _.each(actions, function (a) {
                a.isPrerequisite = _this.isPrerequisite(a);
                a.dependencyDetailsContext = _this.getDependencyDetailsContext(a);
            });
            _this.filteredActions = actions;
            _this.filteredActionsPerRecommendation = _.groupBy(actions, function (a) { return a.RecommendationID; });
            _this.onFilteredActionsUpdated();
        };
        this.openPrerequisiteDetail = function (recommendationId) {
            return _this.statusService.checkRecommendationExists(recommendationId)
                .then(function (exists) {
                if (!exists) {
                    // close dialog (re-initialization of dialog is too complex to implement)
                    _this.dialogInstance.cancel();
                    return null;
                }
                var referrer = _this.createReferrer();
                var context = _this.context || { referrers: [] };
                context.referrers.push(referrer);
                // open new dialog
                return _this.swisService.getRecommendation(recommendationId)
                    .then(function (result) {
                    // close current dialog
                    _this.dialogInstance.cancel();
                    _this.dialogService.showDetailDialog(result, context);
                });
            });
        };
        this.onAlertsLoaded = function (alerts) {
            _this.resolvesAlerts = alerts.length > 0;
        };
        this.initActionSummaryContextPromise = function () {
            _this.actionSummaryContextDeferred = _this.$q.defer();
            _this.actionSummaryContextPromise = _this.actionSummaryContextDeferred.promise;
        };
        this.initStatusChangeChecking = function () {
            var statusChangeChecker = _this.createStatusChangeChecker();
            // ensure recurring logic is also destroyed
            _this.$scope.$on("$destroy", function () {
                statusChangeChecker.destroy();
                statusChangeChecker = null;
            });
        };
        this.onStatusChanged = function () {
            _this.dialogInstance.cancel();
        };
        this.getAppliedActions = function () {
            return _.filter(_this.actions, function (a) { return a.Enabled; });
        };
    }
    RecommendationDetailDialogController.prototype.init = function (dialogInstance, loadDependencies, allowedChange) {
        var _this = this;
        var promises = [];
        this.actions = [];
        this.filteredActions = [];
        this.changedEntitiesCount = 0;
        this.resolvesAlerts = false;
        this.context = dialogInstance.dialogOptions.viewModel.context;
        this.dialogInstance = dialogInstance;
        this.loadDependencies = loadDependencies;
        this.allowedChange = allowedChange;
        // prepare action summary promise
        this.initActionSummaryContextPromise();
        promises.push(this.pluggabilityService.getActionIconDelegate()
            .then(function (d) {
            _this.getActionIconDelegate = d;
        }));
        promises.push(this.loadRecommendationsOnInit());
        // hook on actions enabled update event
        this.$scope.$on("actionsUpdateEnabled", this.onActionsEnabledChanged);
        // ensure change detection is running
        this.initStatusChangeChecking();
        return this.$q.all(promises);
    };
    ;
    RecommendationDetailDialogController.prototype.loadRecommendationsOnInit = function () {
        // keep single detailed recommendation
        this.initialRecommendation = this.dialogInstance.dialogOptions.viewModel.recommendation;
        // load one selected recommendation
        return this.loadRecommendations([this.initialRecommendation.id], this.loadDependencies);
    };
    ;
    /**
     * Filters given actions to be displayed
     */
    RecommendationDetailDialogController.prototype.filterActions = function (actions) {
        return _.filter(actions, function (a) { return a.Enabled; });
    };
    ;
    /**
     * Hook thrown after displayed actions get reloaded
     */
    RecommendationDetailDialogController.prototype.onFilteredActionsUpdated = function () {
    };
    ;
    /**
     * Creates instance of status change checker
     */
    RecommendationDetailDialogController.prototype.createStatusChangeChecker = function () {
        return this.statusService.createRecommendationStatusChangeChecker(this.initialRecommendation, this.onStatusChanged);
    };
    ;
    /**
     * Creates single referrer upon current dialog
     */
    RecommendationDetailDialogController.prototype.createReferrer = function () {
        return {
            recommendation: this.initialRecommendation
        };
    };
    ;
    RecommendationDetailDialogController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngRootScopeService)),
        __param(2, di_1.Inject(index_1.ServiceNames.ngLog)),
        __param(3, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(4, di_1.Inject(index_1.ServiceNames.ngCompileService)),
        __param(5, di_1.Inject(index_1.ServiceNames.ngTimeoutService)),
        __param(6, di_1.Inject(index_1.ServiceNames.statusService)),
        __param(7, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(8, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(9, di_1.Inject(index_1.ServiceNames.pluggabilityService)),
        __param(10, di_1.Inject(index_1.ServiceNames.getTextService)),
        __param(11, di_1.Inject(index_1.ServiceNames.dialogService)),
        __param(12, di_1.Inject(index_1.ServiceNames.dateTimeService)),
        __metadata("design:paramtypes", [Object, Object, Object, Function, Function, Function, Object, Object, Object, Object, Function, Object, Object])
    ], RecommendationDetailDialogController);
    return RecommendationDetailDialogController;
}());
exports.RecommendationDetailDialogController = RecommendationDetailDialogController;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Enum with data that can be loaded with a recommendation entity
 * @enum
 */
var RecommendationData;
(function (RecommendationData) {
    RecommendationData[RecommendationData["None"] = 0] = "None";
    RecommendationData[RecommendationData["PrimaryAction"] = 1] = "PrimaryAction";
    RecommendationData[RecommendationData["Actions"] = 2] = "Actions";
    RecommendationData[RecommendationData["Batch"] = 4] = "Batch";
    RecommendationData[RecommendationData["RevertBatch"] = 8] = "RevertBatch";
})(RecommendationData = exports.RecommendationData || (exports.RecommendationData = {}));


/***/ }),
/* 16 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Supported filter values for recommendations list
 *
 * @class
 * @implements IRecommendationListFilterValues
 **/
var RecommendationListFilterValues = /** @class */ (function () {
    function RecommendationListFilterValues() {
        this.entity = [];
        this.severity = [];
        this.status = [];
        this.kind = [];
        this.strategiesGroup = [];
    }
    return RecommendationListFilterValues;
}());
exports.RecommendationListFilterValues = RecommendationListFilterValues;
/**
 * Checks whether values in specified filters are equal
 *
 * @function
 */
exports.RecommendationListFilterValuesEquals = function (first, second) {
    return _.isEqual(first.entity.sort(), second.entity.sort())
        && _.isEqual(first.kind.sort(), second.kind.sort())
        && _.isEqual(first.severity.sort(), second.severity.sort())
        && _.isEqual(first.strategiesGroup.sort(), second.strategiesGroup.sort())
        && _.isEqual(first.status.sort(), second.status.sort());
};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var enums_1 = __webpack_require__(2);
var recommendationListFilterValues_1 = __webpack_require__(17);
/**
 * Base recommendations list controller.
 *
 * @class
 */
var RecommendationListController = /** @class */ (function () {
    function RecommendationListController($rootScope, $scope, $q, xuiToastService, batchingService, swisRecommendationsService, dialogService, statsService, statusService, routingService, paramsService, filtersService, pluggabilityService, dataGroupStatusService, intervalCallerService, permissionsService, _t, demoService) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$scope = $scope;
        this.$q = $q;
        this.xuiToastService = xuiToastService;
        this.batchingService = batchingService;
        this.swisRecommendationsService = swisRecommendationsService;
        this.dialogService = dialogService;
        this.statsService = statsService;
        this.statusService = statusService;
        this.routingService = routingService;
        this.paramsService = paramsService;
        this.filtersService = filtersService;
        this.pluggabilityService = pluggabilityService;
        this.dataGroupStatusService = dataGroupStatusService;
        this.intervalCallerService = intervalCallerService;
        this.permissionsService = permissionsService;
        this._t = _t;
        this.demoService = demoService;
        this.batchStatus = enums_1.BatchStatus;
        this.subscriberCallback = function () {
            _this.updateData();
        };
        /**
         * Initializes filter values storage service.
         *
         * @param {string} filtersServiceKey Key to filters service where current filter values are stored.
         */
        this.initFiltersService = function (filtersServiceKey) {
            _this.filterValues = _this.filtersService[filtersServiceKey];
            _this.$scope.$on("$destroy", function () {
                // store filter values
                _this.filtersService[filtersServiceKey] = _this.filterValues;
            });
        };
        this.selectedItemsCount = function () {
            if (!_this.selectedRecommendations) {
                return 0;
            }
            return _this.selectedRecommendations.items ? _this.selectedRecommendations.items.length : 0;
        };
        /**
         * Gets the icon of recommendation's priority.
         */
        this.getPriorityIcon = function (recommendation) {
            if (recommendation.priority === enums_1.RecommendationPriority.High) {
                return "severity_critical";
            }
            if (recommendation.priority === enums_1.RecommendationPriority.Medium) {
                return "severity_warning";
            }
            return "severity_info";
        };
        /**
         * Gets the label css class.
         */
        this.getPriorityLabelClass = function (recommendation) {
            if (recommendation.priority === enums_1.RecommendationPriority.High) {
                return "label-danger";
            }
            if (recommendation.priority === enums_1.RecommendationPriority.Medium) {
                return "label-warning";
            }
            return "label-info";
        };
        /**
         * Updates data in list
         */
        this.updateData = function () {
            if (!_this.remoteControl) {
                return _this.$q.when();
            }
            return _this.remoteControl.refreshListData();
        };
        this.refreshRecommendationList = function () {
            _this.clearSelectedRecommendations();
            return _this.updateData();
        };
        /**
        * Gets the promise for returning selected recommendations.
        */
        this.getSelectedRecommendations = function () {
            if (_this.selectedItemsCount() === 0) {
                return _this.$q.resolve([]);
            }
            return _this.getData().then(function (data) {
                var selectedIds = _this.selectedRecommendations.items;
                var recommendations = data.items;
                return _.filter(recommendations, function (rid) { return _.includes(selectedIds, rid.id); });
            });
        };
        this.clearSelectedRecommendations = function () {
            _this.selectedRecommendations = {
                items: [],
                blacklist: false
            };
        };
        /**
         * Creates entity filter property upon given result set.
         */
        this.createEntityFilterProperty = function (stats) {
            var deferred = _this.$q.defer();
            _this.dataGroupStatusService.getStates().then(function (status) {
                var filterValues = [];
                var statsPerDataGroups = _.groupBy(stats, function (st) { return st.id; });
                _.forEach(statsPerDataGroups, function (statsPerDataGroup, id) {
                    var dataGroupStatus = _.find(status, function (st) { return st.identifier.id === id; });
                    var entityFilterValue = {
                        id: id,
                        title: statsPerDataGroup[0].name,
                        count: statsPerDataGroup.length,
                        icon: dataGroupStatus ? dataGroupStatus.icon : null,
                        status: dataGroupStatus ? dataGroupStatus.statusIcon : null
                    };
                    filterValues.push(entityFilterValue);
                });
                deferred.resolve({ refId: "entity", title: _this._t("Clusters/Hosts"), values: filterValues });
            });
            return deferred.promise;
        };
        /**
         * Creates strategies filter property upon given result set.
         */
        this.createStrategyFilterProperty = function (stats) {
            var deferred = _this.$q.defer();
            _this.swisRecommendationsService.getStrategiesGroups().then(function (groups) {
                var filterValues = [];
                var statsPerStrategy = _.groupBy(stats, function (st) { return st.strategiesGroupId; });
                _.forEach(groups, function (group, id) {
                    var stat = statsPerStrategy[group.id];
                    var entityFilterValue = {
                        id: group.id,
                        title: group.name,
                        count: stat ? stat.length : 0,
                        icon: null,
                        status: null
                    };
                    filterValues.push(entityFilterValue);
                });
                deferred.resolve({ refId: "strategiesGroup", title: _this._t("Strategies"), values: filterValues });
            });
            return deferred.promise;
        };
        /**
         * Creates severity filter property upon given result set.
         */
        this.createSeverityFilterProperty = function (stats) {
            var statsPerPriorities = _.groupBy(stats, function (st) { return st.recommendationPriority; });
            var filterValues = [
                {
                    id: enums_1.RecommendationPriority.High,
                    title: _this._t("High"),
                    count: (statsPerPriorities[enums_1.RecommendationPriority.High]) ? statsPerPriorities[enums_1.RecommendationPriority.High].length : 0,
                    icon: "severity_critical"
                }, {
                    id: enums_1.RecommendationPriority.Medium,
                    title: _this._t("Medium"),
                    count: (statsPerPriorities[enums_1.RecommendationPriority.Medium]) ? statsPerPriorities[enums_1.RecommendationPriority.Medium].length : 0,
                    icon: "severity_warning"
                }, {
                    id: enums_1.RecommendationPriority.Low,
                    title: _this._t("Low"),
                    count: (statsPerPriorities[enums_1.RecommendationPriority.Low]) ? statsPerPriorities[enums_1.RecommendationPriority.Low].length : 0,
                    icon: "severity_info"
                }
            ];
            return { refId: "severity", title: _this._t("Severity"), values: filterValues };
        };
        /**
         * Creates recommendation kind filter property upon given result set.
         */
        this.createKindFilterProperty = function (stats) {
            var filterValues = [
                {
                    id: enums_1.RecommendationKind.realtime.toString(),
                    title: _this._t("Active"),
                    count: _this.computeFilterRealtimeRecommendationsCount(stats),
                    icon: "clock"
                }, {
                    id: enums_1.RecommendationKind.regular.toString(),
                    title: _this._t("Predicted"),
                    count: _this.computeFilterRegularRecommendationsCount(stats),
                    icon: "calendar"
                }
            ];
            return { refId: "kind", title: _this._t("Type"), values: filterValues };
        };
        /**
         * Computes total number of realtime recommendations in filter
         */
        this.computeFilterRealtimeRecommendationsCount = function (stats) {
            return _.sumBy(stats, function (st) {
                return st.recommendationKind === enums_1.RecommendationKind.realtime || st.isNow ? 1 : 0;
            });
        };
        /**
         * Computes total number of regular recommendations in filter
         */
        this.computeFilterRegularRecommendationsCount = function (stats) {
            return _.sumBy(stats, function (st) {
                return st.recommendationKind === enums_1.RecommendationKind.regular ? 1 : 0;
            });
        };
        /**
         * Resolves sorting if any, otherwise returns default.
         */
        this.getSortBy = function (sorting, defaultSortBy) {
            return sorting && sorting.sortBy ? sorting.sortBy : defaultSortBy;
        };
        /**
         * Resolves direction if any, otherwise returns default.
         */
        this.getSortDirection = function (sorting, defaultDirection) {
            if (defaultDirection === void 0) { defaultDirection = "asc"; }
            return sorting && sorting.direction ? sorting.direction : defaultDirection;
        };
        /**
         * Handles resolved data.
         */
        this.onFilteredListDataCreating = function (result) {
            // ensures params refer to valid filters
            _this.paramsService.updateStateParams(_this.filterValues);
        };
        /**
         * Declaration method for returning used swis filter.
         */
        this.getRecommendationsFilter = function (sorting, pagination, search) {
            var filter = _this.getEmptyRecommendationsFilter();
            filter.sorting = sorting;
            filter.pagination = pagination;
            filter.search = search;
            if (!_.isEmpty(_this.filterValues.entity)) {
                filter.filters.entity = _this.filterValues.entity;
            }
            if (!_.isEmpty(_this.filterValues.kind)) {
                // the filter values are actually array of strings which causes issues later,
                // this will make sure that we are really working with number array
                filter.filters.kind = _.map(_this.filterValues.kind, function (kind) {
                    return parseInt(kind, 10);
                });
            }
            if (!_.isEmpty(_this.filterValues.severity)) {
                filter.filters.severity = _this.filterValues.severity;
            }
            if (!_.isEmpty(_this.filterValues.status)) {
                filter.filters.status = _this.filterValues.status;
            }
            if (!_.isEmpty(_this.filterValues.strategiesGroup)) {
                filter.filters.strategiesGroup = _this.filterValues.strategiesGroup;
            }
            return filter;
        };
        /**
         * Gets flag indicating whether filter is empty
         */
        this.filterIsEmpty = function () {
            var empty = _this.getEmptyRecommendationsFilter();
            return recommendationListFilterValues_1.RecommendationListFilterValuesEquals(empty.filters, _this.filterValues);
        };
        /**
         * Determines whether Manage Policy button is displayed
         */
        this.isManagePoliciesButtonEnabled = function () {
            // in demo mode, we always want to show button
            return _this.demoService.isDemoMode() || _this.permissionsService.isAdminAllowed();
        };
    }
    RecommendationListController.prototype.$onInit = function () {
        var _this = this;
        this.initProperties();
        this.$scope.$on("$destroy", function () {
            if (_this.saveFilterValues) {
                _this.saveFilterValues();
            }
        });
        this.intervalCallerService.subscribe(this);
        this.$scope.$on("$destroy", function () {
            _this.intervalCallerService.unsubscribe(_this);
        });
    };
    RecommendationListController.prototype.initProperties = function () {
        this.itemsSource = [];
        this.filterValues = new recommendationListFilterValues_1.RecommendationListFilterValues();
        this.remoteControl = {};
        this.pagination = { page: 0, pageSize: 10, total: undefined };
        this.objectTypes = ["Recommendation"];
        this.subscriberId = "ListControllerBase";
        this.sortBySeverity = { id: "Priority", label: this._t("Severity") };
        this.sortByName = { id: "Name", label: this._t("Name") };
        this.sortByScheduledDate = { id: "ScheduledDate", label: this._t("Scheduled Time") };
        this.sortByUserId = { id: "UserID", label: this._t("User") };
        this.sortByDateStarted = { id: "DateStarted", label: this._t("Execution Time") };
        this.sortByDateFinished = { id: "DateFinished", label: this._t("Completion Time") };
        this.options = {
            sortableColumns: [this.sortBySeverity],
            selectionMode: "multi",
            templateUrl: "item-template",
            selectionProperty: "id",
            pageSize: 10,
            showAddRemoveFilterProperty: false,
            emptyData: {
                description: this._t("No recommendations available.")
            },
            showMorePropertyValuesThreshold: 10,
            hideSearch: false,
            triggerSearchOnChange: true,
            allowSelectAllPages: false
        };
        this.sorting = {
            sortBy: this.sortBySeverity,
            direction: "asc"
        };
        this.selectedRecommendations = {
            items: [],
            blacklist: false
        };
    };
    ;
    /**
     * Declaration for method returning list data.
     */
    RecommendationListController.prototype.getData = function (filters, pagination, sorting, search) {
        var _this = this;
        var recommendationsFilter = this.getRecommendationsFilter(sorting, pagination, search);
        // Load data
        var deferred = this.$q.defer();
        this.swisRecommendationsService.getNotRunningRecommendations(recommendationsFilter)
            .then(function (result) {
            _this.onFilteredListDataCreating(result);
            var filteredListData = {
                items: result.recommendations,
                total: result.filter.pagination.total
            };
            _this.onFilteredListDataCreated(filteredListData);
            // update filtered-list bound settings
            //      we differentiate between common loading (i.e. request sent by filtered-list) and helper loading
            var isCommonLoading = filters || pagination || sorting || search;
            if (isCommonLoading) {
                _this.pagination.total = filteredListData.total;
                _this.itemsSource = filteredListData.items;
            }
            deferred.resolve(filteredListData);
        });
        return deferred.promise;
    };
    ;
    /**
     * Handles created list data.
     */
    RecommendationListController.prototype.onFilteredListDataCreated = function (filteredListData) {
        // keep last data so it can be compared later
        this.lastFilteredListData = filteredListData;
    };
    ;
    RecommendationListController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngRootScopeService)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(2, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(3, di_1.Inject(index_1.ServiceNames.xuiToastService)),
        __param(4, di_1.Inject(index_1.ServiceNames.batchingService)),
        __param(5, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(6, di_1.Inject(index_1.ServiceNames.dialogService)),
        __param(7, di_1.Inject(index_1.ServiceNames.statsService)),
        __param(8, di_1.Inject(index_1.ServiceNames.statusService)),
        __param(9, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(10, di_1.Inject(index_1.ServiceNames.recommendationListParamsService)),
        __param(11, di_1.Inject(index_1.ServiceNames.recommendationListFiltersService)),
        __param(12, di_1.Inject(index_1.ServiceNames.pluggabilityService)),
        __param(13, di_1.Inject(index_1.ServiceNames.dataGroupStatusService)),
        __param(14, di_1.Inject(index_1.ServiceNames.intervalCallerService)),
        __param(15, di_1.Inject(index_1.ServiceNames.permissionsService)),
        __param(16, di_1.Inject(index_1.ServiceNames.getTextService)),
        __param(17, di_1.Inject(index_1.ServiceNames.demoService)),
        __metadata("design:paramtypes", [Object, Object, Function, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Function, Object])
    ], RecommendationListController);
    return RecommendationListController;
}());
exports.RecommendationListController = RecommendationListController;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var rootStateName = "recommendations";
var rootSettingsName = "recommendationsSettings";
/**
 * Route names used by Recommendations engine
 *
 * @const
 */
exports.RouteNames = {
    recommendationsSummaryStateName: rootStateName,
    activeRecommendationsStateName: rootStateName + ".active",
    activeRecommendationDetailStateName: rootStateName + ".active.detail",
    scheduledRecommendationsStateName: rootStateName + ".scheduled",
    runningRecommendationsStateName: rootStateName + ".running",
    finishedRecommendationsStateName: rootStateName + ".finished",
    settingsStateName: rootSettingsName,
    strategiesSettingsStateName: rootSettingsName + ".strategies",
    constraintsStateName: rootSettingsName + ".constraints"
};


/***/ }),
/* 20 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var angular = __webpack_require__(44);
var moduleNames_1 = __webpack_require__(22);
/**
 * Helper function indicating whether code is currently under test
 */
exports.isUnderTest = function () {
    return !!angular.mock;
};
exports.default = function () {
    angular.module(moduleNames_1.ModuleNames.providers, []);
    angular.module(moduleNames_1.ModuleNames.services, []);
    angular.module(moduleNames_1.ModuleNames.components, []);
    angular.module(moduleNames_1.ModuleNames.filters, []);
    angular.module(moduleNames_1.ModuleNames.main, [
        "orion",
        "orion-ui-components",
        "sw-charts",
        "filtered-list",
        moduleNames_1.ModuleNames.providers,
        moduleNames_1.ModuleNames.services,
        moduleNames_1.ModuleNames.components,
        moduleNames_1.ModuleNames.filters,
        "ui.router"
    ]);
    return Xui.registerModule(moduleNames_1.ModuleNames.main);
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Module names registered by Recommendations engine
 *
 * @const
 */
exports.ModuleNames = {
    main: "recommendations",
    mainSpecs: "recommendations.specs",
    providers: "recommendations.providers",
    services: "recommendations.services",
    components: "recommendations.components",
    filters: "recommendations.filters"
};


/***/ }),
/* 23 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 24 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 25 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 26 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 27 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 28 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 29 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 30 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 31 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 32 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 33 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 34 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 35 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var RunningRecommendationsControllerBase = /** @class */ (function () {
    function RunningRecommendationsControllerBase($q, swApiService, swisService, demoService) {
        var _this = this;
        this.$q = $q;
        this.swApiService = swApiService;
        this.swisService = swisService;
        this.demoService = demoService;
        this.batchStatus = enums_1.BatchStatus;
        this.actionStatus = enums_1.ActionStatus;
        /**
         * Refreshes recommendation's primary batch with actions and optionally loads revert bach if primary has been canceled
         */
        this.refreshRecommendationStatus = function (recommendation) {
            // if finished, then there is no need to refresh it
            if (!_this.recommendationNeedsRefreshing(recommendation) || _this.demoService.isDemoMode()) {
                return _this.$q.when({});
            }
            // it was canceled and revert batch already loaded? update just both results
            if ((recommendation.batch.status === enums_1.BatchStatus.Canceled
                || recommendation.batch.status === enums_1.BatchStatus.FinishedWithError) && recommendation.batch.revertBatch) {
                return _this.refreshRevertAndPrimaryBatch(recommendation);
            }
            return _this.refreshRunningRecommendationStatus(recommendation);
        };
        /**
         * Indicates whether given recommendation still needs refreshing from server
         */
        this.recommendationNeedsRefreshing = function (recommendation) {
            var isFinished = recommendation.batch.status === enums_1.BatchStatus.FinishedSuccessfully
                || recommendation.batch.status === enums_1.BatchStatus.FinishedWithError
                    && (
                    // does not have any revert batch
                    !recommendation.batch.revertBatchId
                        // or has one and is already finished
                        || (recommendation.batch.revertBatch && recommendation.batch.revertBatch.isFinished()));
            var isFullyCanceled = recommendation.batch.status === enums_1.BatchStatus.Canceled
                && (
                // does not have any revert batch
                !recommendation.batch.revertBatchId
                    // or has one and is already finished
                    || (recommendation.batch.revertBatch && recommendation.batch.revertBatch.isFinished()));
            return !isFinished && !isFullyCanceled;
        };
        /**
         * Refreshes recommendation which is not yet canceled
         */
        this.refreshRunningRecommendationStatus = function (recommendation) {
            return _this.swApiService.getBatchResult(recommendation.batch.id)
                .then(function (batchResult) {
                // merge batch info
                _this.mergePrimaryBatchResult(recommendation, batchResult);
                // if canceled, tries to load revert batch
                if (batchResult.status === enums_1.BatchStatus.Canceled || batchResult.status === enums_1.BatchStatus.FinishedWithError) {
                    return _this.tryLoadRevertBatch(recommendation);
                }
            });
        };
        /**
         * Tries to load revert batch of given recommendation
         */
        this.tryLoadRevertBatch = function (recommendation) {
            return _this.swisService.getRevertBatchWithActions(recommendation.batch.id)
                .then(function (revertBatch) {
                if (revertBatch) {
                    recommendation.batch.revertBatchId = revertBatch.id;
                    recommendation.batch.revertBatch = revertBatch;
                    return _this.swApiService.getBatchResult(revertBatch.id)
                        .then(function (revertBatchResult) {
                        // merge revert batch info
                        _this.mergeRevertBatchResult(revertBatch, revertBatchResult);
                    });
                }
            });
        };
        /**
         * Refreshes recommendation's primary and revert batch
         */
        this.refreshRevertAndPrimaryBatch = function (recommendation) {
            var batchIds = [recommendation.batch.id, recommendation.batch.revertBatchId];
            return _this.swApiService.getBatchResults(batchIds).then(function (results) {
                // merge batch info
                _this.mergePrimaryBatchResult(recommendation, results[recommendation.batch.id]);
                // merge revert batch info
                _this.mergeRevertBatchResult(recommendation.batch.revertBatch, results[recommendation.batch.revertBatchId]);
            });
        };
        /**
         * Merges batch result with recommendation's batch
         */
        this.mergePrimaryBatchResult = function (recommendation, batchResult) {
            recommendation.batch.failureMessage = batchResult.failureMessage;
            recommendation.batch.status = batchResult.status;
            recommendation.batch.dateFinished = batchResult.dateFinished;
            recommendation.batch.cancelationUserId = batchResult.cancelationUserId;
            recommendation.batch.cancelationReason = batchResult.cancelationReason;
            // merge each action
            _.each(batchResult.actionResults, function (actionResult, id) {
                var action = _.find(recommendation.actions, function (a) { return a.id === id; });
                if (action) {
                    action.progress = actionResult.progress;
                    action.status = actionResult.status;
                    action.failureMessage = actionResult.failureMessage;
                    action.dateFinished = actionResult.dateFinished;
                }
            });
        };
        /**
         * Merges batch result with revert batch instance
         */
        this.mergeRevertBatchResult = function (batch, batchResult) {
            batch.failureMessage = batchResult.failureMessage;
            batch.status = batchResult.status;
            batch.dateFinished = batchResult.dateFinished;
            // merge each action
            _.each(batchResult.actionResults, function (actionResult, id) {
                var action = _.find(batch.actions, function (a) { return a.id === id; });
                if (action) {
                    action.progress = actionResult.progress;
                    action.status = actionResult.status;
                    action.failureMessage = actionResult.failureMessage;
                    action.dateFinished = actionResult.dateFinished;
                }
            });
        };
    }
    return RunningRecommendationsControllerBase;
}());
exports.RunningRecommendationsControllerBase = RunningRecommendationsControllerBase;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of plugin services
 *
 * @class
 */
var PluginServiceNames = /** @class */ (function () {
    function PluginServiceNames() {
    }
    PluginServiceNames.recommendationDetailService = "recommendationDetailService";
    return PluginServiceNames;
}());
exports.PluginServiceNames = PluginServiceNames;
;


/***/ }),
/* 38 */
/***/ (function(module, exports) {

module.exports = "<recommendations-active-list></recommendations-active-list> ";

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Batch Action represention.
 *
 * @class
 * @implements IBatchAction
 **/
var BatchAction = /** @class */ (function () {
    function BatchAction() {
    }
    return BatchAction;
}());
exports.BatchAction = BatchAction;
/**
 * TypeGuard for IBatchAction
 *
 * @function
 */
exports.TypeGuard = function (batchAction) {
    return batchAction && batchAction.id && _.isNumber(batchAction.status) && batchAction.batchId;
};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Holder for recommendation filter names
 *
 * @class
 */
var FilterNames = /** @class */ (function () {
    function FilterNames() {
    }
    FilterNames.bytesFormatFilter = "reBytesFormat";
    FilterNames.numberFormatFilter = "reNumberFormat";
    return FilterNames;
}());
exports.FilterNames = FilterNames;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Representantion of the one single constraint object.
 *
 * @class
 * @implements IConstraintObject
 **/
var ConstraintObject = /** @class */ (function () {
    function ConstraintObject(args) {
        if (!args) {
            return;
        }
        this.constraintId = args.constraintId;
        this.entityIdentifier = args.entityIdentifier;
        this.objectId = args.objectId;
        this.entityType = args.entityType;
    }
    ;
    return ConstraintObject;
}());
exports.ConstraintObject = ConstraintObject;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(20);
module.exports = __webpack_require__(43);


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


/// <reference path="ref.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(21);
var index_1 = __webpack_require__(45);
var index_2 = __webpack_require__(61);
var index_3 = __webpack_require__(143);
var index_4 = __webpack_require__(159);
var index_5 = __webpack_require__(190);
var index_6 = __webpack_require__(193);
var index_7 = __webpack_require__(213);
var recommendationsModule = app_1.default();
index_1.default(recommendationsModule);
index_2.default(recommendationsModule);
index_3.default(recommendationsModule);
index_4.default(recommendationsModule);
index_5.default(recommendationsModule);
index_6.default(recommendationsModule);
index_7.default(recommendationsModule);
exports.default = recommendationsModule;


/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = angular;

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = __webpack_require__(46);
var routes_1 = __webpack_require__(47);
__webpack_require__(48);
exports.default = function (module) {
    http_1.default(module);
    routes_1.default(module);
};


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
function configHttp($httpProvider) {
    // configure $http service to combine processing of multiple http responses received at around the same time 
    $httpProvider.useApplyAsync(true);
}
exports.configHttp = configHttp;
;
configHttp.$inject = ["$httpProvider"];
exports.default = function (module) {
    module.config(configHttp);
};


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(7);
/**
 * Configures basic routing settings on config event
 *
 * @function
 */
var configureRouting = function (provider) {
    provider.registerRoutes();
};
/**
 * Configures denied access on run event
 *
 * @function
 */
var configureRoutingDenied = function (routingRegistrationService) {
    routingRegistrationService.registerRoutingDeniedHandler();
};
exports.default = function (module) {
    module.app().config([index_2.ProviderNames.routingRegistrationProvider, configureRouting]);
    module.app().run([index_1.ServiceNames.routingRegistrationService, configureRoutingDenied]);
};


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */

/**
 * Requires dynamic context for .less files - i.e. loads all of them
 */
var req = __webpack_require__(49);


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./directives/expirationPicker/expirationPicker-directive.less": 23,
	"./directives/iconLabel/iconLabel-directive.less": 16,
	"./directives/pluginPlaceholder/pluginPlaceholder-directive.less": 24,
	"./directives/recommendationDetailDialog/footerContents/footerContents-directive.less": 25,
	"./directives/recommendationDetailDialog/recommendationDescription/recommendationDescription-directive.less": 26,
	"./directives/recommendationDetailDialog/recommendationProblem/recommendationProblem-directive.less": 27,
	"./directives/recommendationDetailDialog/recommendationStatistics/recommendationStatistics-directive.less": 28,
	"./directives/recommendationDetailDialog/recommendationStatus/recommendationStatus-directive.less": 29,
	"./directives/recommendationList/activeList/activeList-directive.less": 30,
	"./directives/recommendationList/finishedList/finishedList-directive.less": 31,
	"./directives/recommendationList/runningList/runningList-directive.less": 32,
	"./directives/recommendationList/scheduledList/scheduledList-directive.less": 33,
	"./plugins/vim/detailDialog/actionDecorator/actionDecorator.less": 50,
	"./plugins/vim/detailDialog/actionsSummaryDecorator/actionsSummaryDecorator.less": 51,
	"./plugins/vim/detailDialog/outcomeDecorator/outcomeDecorator.less": 52,
	"./plugins/vim/directives/constraintsEditor/constraintsEditorControls-directive.less": 34,
	"./plugins/vim/directives/constraintsExplorer/constraints.less": 53,
	"./plugins/vim/views/constraints/exclude/exclude-controller.less": 35,
	"./styles.less": 20,
	"./styles/recommendations.less": 54,
	"./views/settings/settings.less": 55,
	"./views/settings/tabs/constraintsTab/dialogs/createConstraint-dialog.less": 56,
	"./views/settings/tabs/strategiesTab/strategies-tab.less": 57,
	"./views/summary/dialogs/detailDialog/activeMultiple/activeMultipleDetail-dialog.less": 58,
	"./views/summary/dialogs/detailDialog/detail-dialog.less": 59,
	"./views/summary/summary.less": 60
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 49;

/***/ }),
/* 50 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 51 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 52 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 53 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 54 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 55 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 56 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 57 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 58 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 59 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 60 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(62);
var index_2 = __webpack_require__(65);
var index_3 = __webpack_require__(69);
var index_4 = __webpack_require__(73);
var index_5 = __webpack_require__(75);
var index_6 = __webpack_require__(79);
var index_7 = __webpack_require__(96);
var index_8 = __webpack_require__(131);
var index_9 = __webpack_require__(134);
var index_10 = __webpack_require__(139);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
    index_7.default(module);
    index_8.default(module);
    index_9.default(module);
    index_10.default(module);
};


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var dependencyDetails_directive_1 = __webpack_require__(63);
exports.default = function (module) {
    module.component("dependencyDetails", dependencyDetails_directive_1.DependencyDetailsDirective);
};


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Dependency label directive
 *
 * @class
 * @implements IDirective
 */
var DependencyDetailsDirective = /** @class */ (function () {
    function DependencyDetailsDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(64);
        this.scope = {
            context: "<"
        };
    }
    return DependencyDetailsDirective;
}());
exports.DependencyDetailsDirective = DependencyDetailsDirective;


/***/ }),
/* 64 */
/***/ (function(module, exports) {

module.exports = "<span class=dependency-details> <span class=\"label label-info\" xui-popover=\"\" xui-popover-content=\"{url: 'prerequisite-popover'}\" xui-popover-trigger=mouseenter _t> PREREQUISITE </span> <script type=text/ng-template id=prerequisite-popover> <span ng-bind-html=\"::context.reason\"></span>\n        <br />\n        <br />\n        <span ng-class=\"::['risk', {critical: context.hasHighestPriority}]\"\n              ng-bind-html=\"::context.riskShort\">\n        </span> </script> </span>";

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var expirationPicker_directive_1 = __webpack_require__(66);
exports.default = function (module) {
    module.component("expirationPicker", expirationPicker_directive_1.ExpirationPickerDirective);
};


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(23);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var expirationPicker_controller_1 = __webpack_require__(67);
/**
 * Directive for contents in recommendation detail dialog footer
 */
var ExpirationPickerDirective = /** @class */ (function () {
    function ExpirationPickerDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(68);
        this.controller = expirationPicker_controller_1.ExpirationPickerController;
        this.controllerAs = "vm";
        this.bindToController = {
            label: "<",
            expirationDate: "="
        };
        this.scope = {};
    }
    return ExpirationPickerDirective;
}());
exports.ExpirationPickerDirective = ExpirationPickerDirective;


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var ExpirationPickerController = /** @class */ (function () {
    function ExpirationPickerController($scope, _t) {
        var _this = this;
        this.$scope = $scope;
        this._t = _t;
        this.$onInit = function () {
            _this.initProperties();
            // switch the combobox to "Custom" when the value changes from null to some defined date
            // the combobox will not be switched to "Custom" in case that new date is either 3 days or 1 week in future
            var unregisterWatch = _this.$scope.$watch(function () { return _this.expirationDate; }, function (newValue, oldValue, scope) {
                if (!oldValue && newValue && newValue !== _this.dayOffset && newValue !== _this.weekOffset) {
                    _this.selectedTimeLimit = _this.timeLimits[2];
                    unregisterWatch();
                }
            });
        };
        this.onChange = function (newValue) {
            if (newValue.key === _this.custom.key) {
                _this.expirationDate = _this.customOffset;
            }
            else {
                _this.expirationDate = newValue.value;
            }
        };
        this.getCanShowDatePicker = function () {
            return _this.selectedTimeLimit.key === _this.custom.key;
        };
    }
    ExpirationPickerController.prototype.initProperties = function () {
        this.today = new Date();
        this.customOffset = new Date();
        this.label = this._t("Make policy valid for");
        this.dayOffset = moment().add(3, "days").toDate();
        this.weekOffset = moment().add(7, "days").toDate();
        this.day = { key: "3days", name: this._t("3 days"), value: this.dayOffset };
        this.week = { key: "week", name: this._t("1 week"), value: this.weekOffset };
        this.custom = { key: "custom", name: this._t("Custom"), value: null };
        this.always = { key: "always", name: this._t("Always"), value: null };
        this.timeLimits = [this.day, this.week, this.custom, this.always];
        this.selectedTimeLimit = this.timeLimits[3];
    };
    ;
    ExpirationPickerController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Function])
    ], ExpirationPickerController);
    return ExpirationPickerController;
}());
exports.ExpirationPickerController = ExpirationPickerController;


/***/ }),
/* 68 */
/***/ (function(module, exports) {

module.exports = "<div class=expiration-picker> <div class=expiration> <span class=\"caption pull-left margin-right\">{{::vm.label}}</span> <xui-dropdown class=\"pull-left margin-right\" ng-model=vm.selectedTimeLimit display-value=name items-source=vm.timeLimits on-changed=\"vm.onChange(newValue, oldValue)\"> </xui-dropdown> <xui-date-picker ng-if=vm.getCanShowDatePicker() class=pull-left ng-model=vm.expirationDate min-date=vm.today is-required=true></xui-date-picker> </div> </div> ";

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var iconLabel_directive_1 = __webpack_require__(70);
__webpack_require__(16);
exports.default = function (module) {
    module.component("reIconLabel", iconLabel_directive_1.IconLabelDirective);
};


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(16);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var iconLabel_controller_1 = __webpack_require__(71);
var IconLabelDirective = /** @class */ (function () {
    function IconLabelDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(72);
        this.controller = iconLabel_controller_1.IconLabelController;
        this.controllerAs = "vm";
        this.bindToController = true;
        this.scope = {
            value: "=",
            status: "=",
            iconSize: "="
        };
    }
    return IconLabelDirective;
}());
exports.IconLabelDirective = IconLabelDirective;


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Icon label directive's controller
 *
 * @class
 */
var IconLabelController = /** @class */ (function () {
    function IconLabelController(stringService) {
        var _this = this;
        this.stringService = stringService;
        this.getStatusCssClass = function () {
            var status = _this.getStatusValue();
            if (status) {
                return "sw-recommendations-icon-label-" + status;
            }
            return null;
        };
        this.isIconDisplayed = function () {
            return _this.statusHasValidValue();
        };
        this.getIcon = function () {
            var status = _this.getStatusValue();
            if (status) {
                return "severity_" + status;
            }
            return null;
        };
        this.getIconSize = function () {
            var hasValidValue = _this.iconSizeHasValidValue();
            if (hasValidValue) {
                return _this.iconSize.toLowerCase();
            }
            return "small";
        };
        this.getStatusValue = function () {
            var hasValidValue = _this.statusHasValidValue();
            if (hasValidValue) {
                var lower = _this.status.toLowerCase();
                return lower;
            }
            return null;
        };
        this.statusHasValidValue = function () {
            return _.some(["critical", "error", "info", "ok", "tip", "unknown", "warning"], function (s) { return _this.stringService.equalsIgnoreCase(s, _this.status); });
        };
        this.iconSizeHasValidValue = function () {
            return _.some(["xsmall", "small", "large", "xlarge"], function (s) { return _this.stringService.equalsIgnoreCase(s, _this.iconSize); });
        };
    }
    ;
    IconLabelController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.stringService)),
        __metadata("design:paramtypes", [Object])
    ], IconLabelController);
    return IconLabelController;
}());
exports.IconLabelController = IconLabelController;


/***/ }),
/* 72 */
/***/ (function(module, exports) {

module.exports = "<span ng-class=\"::['sw-recommendations-icon-label', vm.getStatusCssClass()]\"> <xui-icon ng-if=vm.isIconDisplayed() icon={{vm.getIcon()}} icon-size={{vm.getIconSize()}}> </xui-icon> {{vm.value}} </span> ";

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var ngModelPrevious_directive_1 = __webpack_require__(74);
exports.default = function (module) {
    module.component("ngModelPrevious", ngModelPrevious_directive_1.NgModelPreviousDirective);
};


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Directive for keeping previous value of ngModel
 *
 * @class
 * @implements IDirective
 */
var NgModelPreviousDirective = /** @class */ (function () {
    function NgModelPreviousDirective($parse) {
        var _this = this;
        this.$parse = $parse;
        this.restrict = "A";
        this.replace = false;
        this.require = "ngModel";
        this.transclude = false;
        this.link = function (scope, elem, attrs, ngModel) {
            var attr = attrs["ngModelPrevious"];
            if (!attr) {
                return;
            }
            // parse this directive attribute so it can be assigned later
            var parsed = _this.$parse(attr);
            ngModel.$validators["ngModelBefore"] = function (modelValue, viewValue) {
                // in case of change, we keep the old value
                if (modelValue !== viewValue) {
                    parsed.assign(scope, modelValue);
                }
                // always returning true, this is just kind of ngModel interceptor
                return true;
            };
        };
    }
    NgModelPreviousDirective.$inject = ["$parse"];
    return NgModelPreviousDirective;
}());
exports.NgModelPreviousDirective = NgModelPreviousDirective;


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var pluginPlaceholder_directive_1 = __webpack_require__(76);
exports.default = function (module) {
    module.component("rePluginPlaceholder", pluginPlaceholder_directive_1.PluginPlaceholderDirective);
};


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(24);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pluginPlaceholder_controller_1 = __webpack_require__(77);
var PluginPlaceholderDirective = /** @class */ (function () {
    function PluginPlaceholderDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(78);
        this.controller = pluginPlaceholder_controller_1.PluginPlaceholderController;
        this.controllerAs = "vm";
        this.bindToController = true;
        this.scope = {
            areaName: "@",
            context: "="
        };
    }
    return PluginPlaceholderDirective;
}());
exports.PluginPlaceholderDirective = PluginPlaceholderDirective;


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var PluginPlaceholderController = /** @class */ (function () {
    function PluginPlaceholderController(pluggabilityService) {
        var _this = this;
        this.pluggabilityService = pluggabilityService;
        this.init = function () {
            _this.templatesLoaded = false;
            _this.pluggabilityService.getTemplatesForArea(_this.areaName).then(function (result) {
                _this.templates = result;
                _this.templatesLoaded = true;
            });
        };
    }
    PluginPlaceholderController.$inject = [index_1.ServiceNames.pluggabilityService];
    return PluginPlaceholderController;
}());
exports.PluginPlaceholderController = PluginPlaceholderController;


/***/ }),
/* 78 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=vm.init()> <div ng-repeat=\"template in vm.templates\" ng-show=vm.templatesLoaded> <ng-include src=template ng-init=\"context=vm.context\"/> </div> </div>";

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(80);
var index_2 = __webpack_require__(84);
var index_3 = __webpack_require__(88);
var index_4 = __webpack_require__(92);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
};


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var activeList_directive_1 = __webpack_require__(81);
exports.default = function (module) {
    module.component("recommendationsActiveList", activeList_directive_1.ActiveRecommendationListDirective);
};


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(30);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var activeList_controller_1 = __webpack_require__(82);
var ActiveRecommendationListDirective = /** @class */ (function () {
    function ActiveRecommendationListDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(83);
        this.controller = activeList_controller_1.ActiveRecommendationListController;
        this.controllerAs = "vm";
        this.scope = {};
    }
    return ActiveRecommendationListDirective;
}());
exports.ActiveRecommendationListDirective = ActiveRecommendationListDirective;
;


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var dialog_service_1 = __webpack_require__(10);
var swis_service_1 = __webpack_require__(15);
var enums_1 = __webpack_require__(2);
var eventNames_1 = __webpack_require__(11);
var recommendationsFilter_1 = __webpack_require__(12);
var list_controller_base_1 = __webpack_require__(18);
/**
 * Controller for active recommendations list
 *
 * @class
 * @extends RecommendationsListController
 */
var ActiveRecommendationListController = /** @class */ (function (_super) {
    __extends(ActiveRecommendationListController, _super);
    function ActiveRecommendationListController($rootScope, $scope, $q, xuiToastService, batchingService, swisRecommendationsService, dialogService, statsService, statusService, routingService, paramsService, filtersService, pluggabilityService, dataGroupStatusService, intervalCallerService, permissionsService, _t, demoService, $timeout, $state, $stateParams, swApi) {
        var _this = _super.call(this, $rootScope, $scope, $q, xuiToastService, batchingService, swisRecommendationsService, dialogService, statsService, statusService, routingService, paramsService, filtersService, pluggabilityService, dataGroupStatusService, intervalCallerService, permissionsService, _t, demoService) || this;
        _this.$timeout = $timeout;
        _this.$state = $state;
        _this.$stateParams = $stateParams;
        _this.swApi = swApi;
        _this.executionType = enums_1.ExecutionType;
        _this.openRecommendationDetailById = function (recommendationId) {
            return _this.swisRecommendationsService.getRecommendation(recommendationId, swis_service_1.RecommendationData.PrimaryAction).then(function (r) {
                if (!r) {
                    _this.xuiToastService.error(_this._t("Requested recommendation couldn't be found."));
                    return;
                }
                if (!r.isActive()) {
                    _this.xuiToastService.error(_this._t("Requested recommendation is not active."));
                    return;
                }
                return _this.dialogService.showActiveDetailDialog(r)
                    .then(_this.routingService.goToActiveRecommendations);
            });
        };
        _this.openRecommendationDetail = function (recommendation) {
            return _this.statusService.checkRecommendationStatusHasChanged(recommendation).then(function (checkResult) {
                if (checkResult.isChanged) {
                    return _this.updateData();
                }
                return _this.dialogService.showActiveDetailDialog(recommendation);
            });
        };
        _this.openActiveMultipleRecommendationsDialog = function (recommendations) {
            return _this.dialogService.showActiveMultipleDetailDialog(recommendations);
        };
        _this.openSelectedRecommendationsDetail = function () {
            return _this.getSelectedRecommendations().then(function (recommendations) {
                if (recommendations.length === 1) {
                    return _this.openRecommendationDetail(recommendations[0]);
                }
                else if (recommendations.length > 1) {
                    return _this.openActiveMultipleRecommendationsDialog(recommendations);
                }
                else {
                    throw Error("No recommendation is selected.");
                }
            });
        };
        _this.getFilterProperties = function () {
            var recommendationsFilter = _this.getEmptyRecommendationsFilter();
            return _this.swisRecommendationsService.getRecommendationsFilter(recommendationsFilter).then(function (stats) {
                return _this.createEntityFilterProperty(stats)
                    .then(function (entityFilterProperty) {
                    return _this.createStrategyFilterProperty(stats).then(function (strategiesFilterProperty) {
                        var kindFilterProperty = _this.createKindFilterProperty(stats);
                        var severityFilterProperty = _this.createSeverityFilterProperty(stats);
                        var filter = [
                            entityFilterProperty,
                            kindFilterProperty,
                            severityFilterProperty,
                            strategiesFilterProperty
                        ];
                        return filter;
                    });
                });
            });
        };
        _this.getEmptyRecommendationsFilter = function () {
            var filter = new recommendationsFilter_1.RecommendationsFilter();
            filter.onlyAppliedActions = false;
            return filter;
        };
        _this.createConstraint = function () {
            _this.getSelectedRecommendations().then(function (recommendations) {
                //It is possilbe to ignore only one recommendation at the time
                var recommendation = recommendations[0];
                _this.dialogService.showCreateConstraintDialog(recommendation, null).then(function (dialogResult) {
                    if (dialogResult === dialog_service_1.CreateConstraintDialogResultStatus.CreatedConstraint) {
                        return _this.refreshRecommendationList();
                    }
                    return null;
                });
            });
        };
        /**
         * Saves current filter state to shared filter service
         */
        _this.saveFilterValues = function () {
            _this.filtersService.activeRecommendationsFilterValues = _this.filterValues;
        };
        _this.updateEmptyDataOptions = function () {
            // note: show sample data empty template if enabled, otherwise default template with our message
            _this.options.emptyData.templateUrl = _this.isSampleModeEnabled ? "sampledata-template" : undefined;
        };
        return _this;
    }
    ActiveRecommendationListController.prototype.$onInit = function () {
        _super.prototype.$onInit.call(this);
        this.isSampleModeEnabled = false;
        this.ignoreSelectedButtonTitle = this._t("Create Policy");
        this.managePoliciesButtonTitle = this._t("Manage Policies");
        if (this.$stateParams.recommendationId) {
            this.openRecommendationDetailById(this.$stateParams.recommendationId);
        }
        this.filterValues = this.filtersService.activeRecommendationsFilterValues;
        this.paramsService.initFilterValues(this.filterValues);
        this.$rootScope.$on(eventNames_1.EventNames.applyRecommendation, this.refreshRecommendationList);
        this.$rootScope.$on(eventNames_1.EventNames.constraintCreated, this.refreshRecommendationList);
    };
    ;
    /**
     * Custom function for getting current recommendations data that also sets a sample data flag
     */
    ActiveRecommendationListController.prototype.getData = function (filters, pagination, sorting, search) {
        var _this = this;
        return this.swisRecommendationsService.anyRecommendationExists()
            .then(function (anyExists) {
            // still in sample mode => reject it to prevent screen blinking
            if (_this.isSampleModeEnabled && !anyExists) {
                return _this.$q.reject("Rejecting since still in sample mode.");
            }
            _this.isSampleModeEnabled = !anyExists;
            _this.updateEmptyDataOptions();
            return _super.prototype.getData.call(_this, filters, pagination, sorting, search);
        });
    };
    ;
    ActiveRecommendationListController.$inject = [].concat(list_controller_base_1.RecommendationListController.$inject, index_1.ServiceNames.ngTimeoutService, index_1.ServiceNames.ngState, index_1.ServiceNames.ngStateParams, index_1.ServiceNames.swApiService);
    return ActiveRecommendationListController;
}(list_controller_base_1.RecommendationListController));
exports.ActiveRecommendationListController = ActiveRecommendationListController;


/***/ }),
/* 83 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-list-active data-selector=recommendation-list-active> <xui-message type=hint ng-if=vm.isSampleModeEnabled> <span _t>The Recommendations Engine provides different levels of detail and precision based on how long it has been collecting data:</span> <ul class=recommendation-hint> <li _t>- 1 hour: The amount of time required to generate recommendations for immediate, real-time issues.</li> <li _t>- 1 week: The amount of time required to generate predictive recommendations about future issues.</li> <li _t>- 4 weeks: The amount of time required to generate optimal and more precise predictive recommendations.</li> </ul> </xui-message> <xui-filtered-list items-source=vm.itemsSource on-refresh=\"vm.getData(filters, pagination, sorting, searching)\" filter-properties-fn=vm.getFilterProperties() options=::vm.options sorting=::vm.sorting pagination-data=::vm.pagination filter-values=vm.filterValues selection=vm.selectedRecommendations filter-property-groups=::vm.objectTypes controller=vm remote-control=vm.remoteControl> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() >= 1\"> <xui-button icon=step-complete display-style=tertiary ng-click=vm.openSelectedRecommendationsDetail() _t data-selector=apply-selected-button> Apply Selected </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() === 0\"> <xui-button icon=edit display-style=tertiary ng-click=vm.routingService.goToConstraints() ng-if=vm.isManagePoliciesButtonEnabled() data-selector=manage-constraints-button> {{::vm.managePoliciesButtonTitle}} </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() === 1\"> <xui-button icon=ignore display-style=tertiary ng-click=vm.createConstraint() ng-if=vm.isManagePoliciesButtonEnabled() data-selector=create-constraint-button> {{::vm.ignoreSelectedButtonTitle}} </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list> <script type=text/ng-template id=item-template> <div class=\"row detail\" data-selector=\"recommendation-item\">\n            <div class=\"col-xs-6\">\n                <div class=\"media\">\n                    <div class=\"media-left\" data-selector=\"recommendation-priority\">\n                        <xui-icon icon=\"{{::vm.getPriorityIcon(item)}}\" icon-size=\"small\"></xui-icon>\n                    </div>\n                    <div class=\"media-left\" ng-if=\"::item.isNow()\" data-selector=\"recommendation-is-now\">\n                        <span ng-class=\"::['label', vm.getPriorityLabelClass(item)]\" _t>NOW</span>\n                    </div>\n                    <div ng-class=\"::['media-body', 'risk', {critical: item.hasHighestPriority()}]\"\n                        ng-bind-html=\"::item.riskShort\"\n                        ng-click=\"vm.openRecommendationDetail(item)\"\n                        title=\"_t(Click to see more details about this recommendation)\" _ta\n                        data-selector=\"recommendation-risk-short\"></div>\n                </div>\n            </div>\n            <div class=\"col-xs-6\" ng-click=\"vm.openRecommendationDetail(item)\">\n                <div class=\"description\"\n                    ng-bind-html=\"::item.description\"\n                    title=\"_t(Click to see more details about this recommendation)\" _ta \n                    data-selector=\"recommendation-description\"></div>\n            </div>\n        </div> </script> <script type=text/ng-template id=sampledata-template> <div class=\"sample-wrapper\">\n            <div class=\"sample\" data-selector=\"recommendations-sample-data\"></div>\n        </div> </script> </div> ";

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scheduledList_directive_1 = __webpack_require__(85);
exports.default = function (module) {
    module.component("recommendationsScheduledList", scheduledList_directive_1.ScheduledRecommendationListDirective);
};


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(33);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var scheduledList_controller_1 = __webpack_require__(86);
var ScheduledRecommendationListDirective = /** @class */ (function () {
    function ScheduledRecommendationListDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(87);
        this.controller = scheduledList_controller_1.ScheduledRecommendationListController;
        this.controllerAs = "vm";
        this.scope = {};
    }
    return ScheduledRecommendationListDirective;
}());
exports.ScheduledRecommendationListDirective = ScheduledRecommendationListDirective;
;


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var recommendationsFilter_1 = __webpack_require__(12);
var list_controller_base_1 = __webpack_require__(18);
/**
 * Controller for scheduled recommendations list.
 *
 * @class
 * @extends RecommendationsListController
 */
var ScheduledRecommendationListController = /** @class */ (function (_super) {
    __extends(ScheduledRecommendationListController, _super);
    function ScheduledRecommendationListController($rootScope, $scope, $q, xuiToastService, batchingService, swisRecommendationsService, dialogService, statsService, statusService, routingService, paramsService, filtersService, pluggabilityService, dataGroupStatusService, intervalCallerService, permissionsService, _t, demoService) {
        var _this = _super.call(this, $rootScope, $scope, $q, xuiToastService, batchingService, swisRecommendationsService, dialogService, statsService, statusService, routingService, paramsService, filtersService, pluggabilityService, dataGroupStatusService, intervalCallerService, permissionsService, _t, demoService) || this;
        _this.openRecommendationDetail = function (recommendation) {
            return _this.statusService.checkRecommendationStatusHasChanged(recommendation).then(function (result) {
                if (result.isChanged) {
                    return _this.updateData();
                }
                return _this.dialogService.showScheduledDetailDialog(recommendation);
            });
        };
        _this.cancelSchedules = function () {
            return _this.getSelectedRecommendations().then(function (data) {
                return _this.batchingService.showAndHandleCancelScheduleConfirmDialog(data).then(function () {
                    _this.xuiToastService.success(_this._t("Schedule of the recommendation has been canceled and you can see it among current recommendations."));
                    _this.refreshRecommendationList();
                });
            });
        };
        _this.getFilterProperties = function () {
            var recommendationsFilter = _this.getEmptyRecommendationsFilter();
            return _this.swisRecommendationsService.getRecommendationsFilter(recommendationsFilter).then(function (stats) {
                return _this.createEntityFilterProperty(stats).then(function (entityFilterProperty) {
                    return _this.createStrategyFilterProperty(stats).then(function (strategiesFilterProperty) {
                        var kindFilterProperty = _this.createKindFilterProperty(stats);
                        var severityFilterProperty = _this.createSeverityFilterProperty(stats);
                        var filter = [
                            entityFilterProperty,
                            kindFilterProperty,
                            severityFilterProperty,
                            strategiesFilterProperty
                        ];
                        return filter;
                    });
                });
            });
        };
        _this.getEmptyRecommendationsFilter = function () {
            var filter = new recommendationsFilter_1.RecommendationsFilter();
            filter.onlyAppliedActions = false;
            filter.filters.status = [enums_1.BatchStatus[enums_1.BatchStatus.Scheduled]];
            return filter;
        };
        _this.saveFilterValues = function () {
            _this.filtersService.scheduledRecommendationsFilterValues = _this.filterValues;
        };
        return _this;
    }
    ScheduledRecommendationListController.prototype.$onInit = function () {
        _super.prototype.$onInit.call(this);
        this.options.sortableColumns.push(this.sortByScheduledDate);
        this.options.sortableColumns.push(this.sortByUserId);
        this.options.emptyText = this._t("No recommendations scheduled.");
        this.filterValues = this.filtersService.scheduledRecommendationsFilterValues;
        this.paramsService.initFilterValues(this.filterValues);
    };
    ScheduledRecommendationListController.$inject = list_controller_base_1.RecommendationListController.$inject;
    return ScheduledRecommendationListController;
}(list_controller_base_1.RecommendationListController));
exports.ScheduledRecommendationListController = ScheduledRecommendationListController;


/***/ }),
/* 87 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-list-scheduled data-selector=recommendation-list-scheduled> <xui-filtered-list items-source=vm.itemsSource on-refresh=\"vm.getData(filters, pagination, sorting, searching)\" filter-properties-fn=vm.getFilterProperties() options=::vm.options sorting=::vm.sorting pagination-data=::vm.pagination filter-values=vm.filterValues selection=vm.selectedRecommendations filter-property-groups=::vm.objectTypes controller=vm remote-control=vm.remoteControl> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() === 0\"> <xui-button icon=edit display-style=tertiary ng-click=vm.routingService.goToConstraints() ng-if=vm.isManagePoliciesButtonEnabled() _t> Manage Policies </xui-button> </xui-toolbar-item> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() > 0\"> <xui-button icon=close display-style=tertiary ng-click=vm.cancelSchedules() _t> Cancel Schedules </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list> <script type=text/ng-template id=item-template> <div class=\"row detail\">\n            <div class=\"col-xs-5\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"{{::vm.getPriorityIcon(item)}}\"\n                                icon-size=\"small\"></xui-icon>\n                    </div>\n                    <div class=\"media-left\"\n                        ng-if=\"::item.isNow()\">\n                        <span ng-class=\"::['label', vm.getPriorityLabelClass(item)]\" _t>\n                            NOW\n                        </span>\n                    </div>\n                    <div ng-class=\"::['media-body', 'risk', {critical: item.hasHighestPriority()}]\"\n                        ng-bind-html=\"::item.riskShort\"\n                        ng-click=\"vm.openRecommendationDetail(item)\"\n                        title=\"_t(Click to see more details about this recommendation)\" _ta></div>\n                </div>\n            </div>\n            <div class=\"col-xs-4\"\n                ng-click=\"vm.openRecommendationDetail(item)\">\n\n                <div class=\"description\"\n                    ng-bind-html=\"::item.description\"\n                    title=\"_t(Click to see more details about this recommendation)\" _ta>\n                </div>\n            </div>\n            <div class=\"col-xs-3\" _t>\n                Scheduled to {{::item.batch.scheduledDate | date: 'short'}} by {{item.batch.user}}\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var runningList_directive_1 = __webpack_require__(89);
exports.default = function (module) {
    module.component("recommendationsRunningList", runningList_directive_1.RunningRecommendationListDirective);
};


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(32);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var runningList_controller_1 = __webpack_require__(90);
var RunningRecommendationListDirective = /** @class */ (function () {
    function RunningRecommendationListDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(91);
        this.controller = runningList_controller_1.RunningRecommendationListController;
        this.controllerAs = "vm";
        this.scope = {};
    }
    return RunningRecommendationListDirective;
}());
exports.RunningRecommendationListDirective = RunningRecommendationListDirective;
;


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var enums_1 = __webpack_require__(2);
var recommendationsFilter_1 = __webpack_require__(12);
var di_1 = __webpack_require__(1);
var running_controller_base_1 = __webpack_require__(36);
var RunningRecommendationListController = /** @class */ (function (_super) {
    __extends(RunningRecommendationListController, _super);
    function RunningRecommendationListController($scope, $interval, recommendationsService, routingService, batchingService, dialogService, $q, swApiService, swisService, demoService) {
        var _this = _super.call(this, $q, swApiService, swisService, demoService) || this;
        _this.$scope = $scope;
        _this.$interval = $interval;
        _this.recommendationsService = recommendationsService;
        _this.routingService = routingService;
        _this.batchingService = batchingService;
        _this.dialogService = dialogService;
        _this.recommendations = [];
        _this.actionsPerRecommendationVisible = {};
        _this.init = function () {
            _this.options = {
                hideSearch: true
            };
            _this.initFilter();
            _this.intervalPromise = _this.$interval(_this.loadRecommendations, 3000);
            _this.$scope.$on("$destroy", function () {
                _this.$interval.cancel(_this.intervalPromise);
            });
            return _this.loadRecommendations();
        };
        _this.initFilter = function () {
            _this.filter = new recommendationsFilter_1.RecommendationsFilter();
            _this.filter.sorting = {
                sortableColumns: null,
                sortBy: {
                    id: "Actions.[Order]"
                },
                direction: "ASC"
            };
            _this.filter.filters.status = [enums_1.BatchStatus[enums_1.BatchStatus.Running],
                enums_1.BatchStatus[enums_1.BatchStatus.Canceling],
                enums_1.BatchStatus[enums_1.BatchStatus.Paused],
                enums_1.BatchStatus[enums_1.BatchStatus.Pending],
                // Dummy state forcing the running tab to filter batches that finished with error and have revert batch in progress
                enums_1.BatchStatus[enums_1.BatchStatus.RevertBatchInProgress_Indication]];
            _this.filter.onlyAppliedActions = true;
        };
        /**
         * Loads recommendations upon existing filter
         */
        _this.loadRecommendations = function () {
            return _this.swisService.getRunningRecommendations(_this.filter).then(function (results) {
                var ids = _.map(_this.recommendations, function (r) { return r.id; });
                _.each(results.recommendations, function (r) {
                    if (!_.includes(ids, r.id)) {
                        _this.recommendations.push(r);
                    }
                    ;
                });
                return _this.refreshStatus().then(function () {
                    _this.expandOrCollapseRecommendations();
                });
            });
        };
        /**
         * Refreshes batch status of all recommendations
         */
        _this.refreshStatus = function (recommendations) {
            // take parameter or all current recommendations
            recommendations = recommendations || _this.recommendations;
            var refreshPromises = _.map(recommendations, function (r) { return _this.refreshRecommendationStatus(r); });
            return _this.$q.all(refreshPromises);
        };
        /**
         * Indicates whether cancel and revert all functionality is available
         */
        _this.isCancelingAllAvailable = function () {
            return _this.getCancelableRecommendations().length > 0;
        };
        _this.openRecommendationDetail = function (r) {
            if (r.isRunning()) {
                return _this.dialogService.showRunningDetailDialog(r);
            }
            if (r.isFinished()) {
                return _this.dialogService.showFinishedDetailDialog(r);
            }
            return _this.$q.resolve();
        };
        /**
         * Gets all cancelable recommenations
         */
        _this.getCancelableRecommendations = function () {
            return _.filter(_this.recommendations, _this.isRecommendationCancelable);
        };
        /**
         * Gets flag indicating whether given recommendations is able to be canceled
         */
        _this.isRecommendationCancelable = function (r) {
            return r.batch && (r.batch.status === enums_1.BatchStatus.Running || r.batch.status === enums_1.BatchStatus.Pending);
        };
        /**
         * Cancels and reverts all running recommendations
         */
        _this.cancelAndRevertAll = function () {
            var cancelableRecommendations = _this.getCancelableRecommendations();
            if (_.isEmpty(cancelableRecommendations)) {
                return _this.$q.reject("No recommendations to cancel.");
            }
            return _this.batchingService.showAndHandleCancelAndRevertConfirmDialog(cancelableRecommendations)
                .then(function () { return _this.refreshStatus(cancelableRecommendations); })
                .then(function () {
                // TODO: UIF-4279 - remove this when bug is fixed
                _this.expandRecommendations(cancelableRecommendations);
            });
        };
        /**
         * Cancels and reverts the single recommendation
         */
        _this.cancelAndRevertSingle = function (r) {
            return _this.batchingService.showAndHandleCancelAndRevertConfirmDialog([r])
                .then(function () { return _this.refreshRecommendationStatus(r); });
        };
        _this.showActionsForRecommendation = function (r) {
            return r.batch.status !== _this.batchStatus.Pending;
        };
        _this.isCancelButtonVisible = function (r) {
            return _this.isRecommendationCancelable(r);
        };
        _this.expandOrCollapseRecommendations = function () {
            _.each(_this.recommendations, function (r) {
                if (!(r.id in _this.actionsPerRecommendationVisible)) {
                    _this.actionsPerRecommendationVisible[r.id] = _this.showActionsForRecommendation(r);
                }
            });
        };
        _this.expandRecommendations = function (recommendations) {
            _.each(recommendations, function (r) {
                _this.actionsPerRecommendationVisible[r.id] = true;
            });
        };
        _this.isApplyingRecommendationTextVisible = function (r) {
            return r.batch.status === _this.batchStatus.Running || r.batch.status === _this.batchStatus.Paused;
        };
        _this.isSuccessMessageVisible = function (r) {
            return r.batch.status === _this.batchStatus.FinishedSuccessfully;
        };
        _this.isErrorMessageVisible = function (r) {
            return r.batch.status === _this.batchStatus.FinishedWithError;
        };
        _this.isMoreDetailsButtonVisible = function (r) {
            return r.batch.isRunning() || r.batch.isFinished();
        };
        return _this;
    }
    RunningRecommendationListController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngIntervalService)),
        __param(2, di_1.Inject(index_1.ServiceNames.statsService)),
        __param(3, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(4, di_1.Inject(index_1.ServiceNames.batchingService)),
        __param(5, di_1.Inject(index_1.ServiceNames.dialogService)),
        __param(6, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(7, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(8, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(9, di_1.Inject(index_1.ServiceNames.demoService)),
        __metadata("design:paramtypes", [Object, Function, Object, Object, Object, Object, Function, Object, Object, Object])
    ], RunningRecommendationListController);
    return RunningRecommendationListController;
}(running_controller_base_1.RunningRecommendationsControllerBase));
exports.RunningRecommendationListController = RunningRecommendationListController;


/***/ }),
/* 91 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-list-running data-selector=recommendation-list-running ng-init=vm.init()> <xui-grid items-source=vm.recommendations template-url=item-template controller=vm selection-property=id options=::vm.options empty-text=\"_t(No recommendations available.)\" _ta> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item> <xui-button icon=redo display-style=tertiary is-disabled=!vm.isCancelingAllAvailable() ng-click=vm.cancelAndRevertAll() _t> Cancel &amp; Revert All </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-grid> <script type=text/ng-template id=item-template> <xui-expander is-open=\"vm.actionsPerRecommendationVisible[item.id]\">\n            <xui-expander-heading>\n                <div class=\"row detail\">\n                    <div class=\"col-xs-5 description\">\n                        <div ng-bind-html=\"item.description\"\n                            title=\"_t(Click to see more details about this recommendation)\" _ta>\n                        </div>\n                    </div>\n                    <div class=\"col-xs-3\" ng-if=\"vm.isApplyingRecommendationTextVisible(item)\">\n                        <div>\n                            <xui-spinner show=\"true\"\n                                        size=\"20\"\n                                        class=\"spinner\">\n                            </xui-spinner>\n                            <span _t>Applying recommendation...</span>\n                        </div>\n                    </div>\n                    <div class=\"col-xs-3\" ng-if=\"vm.isSuccessMessageVisible(item)\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"severity_ok\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\">\n                                <span class=\"dots successfull\" _t>\n                                    Succesfully executed on {{item.batch.dateFinished | date:'short'}}.\n                                    This recommendation now displays in the\n                                    <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                                </span>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-xs-3\" ng-if=\"vm.isErrorMessageVisible(item)\">\n                        <div class=\"col-xs-12\" ng-if=\"item.batch.revertBatch\">\n                            <ng-include src=\"'revert-batch-status-template'\"></ng-include>\n                        </div>\n                        <div class=\"col-xs-12\" ng-if=\"!item.batch.revertBatch\">\n                            <div class=\"media\">\n                                <div class=\"media-left\">\n                                    <xui-icon icon=\"severity_error\"></xui-icon>\n                                </div>\n                                <div class=\"media-body\">\n                                    <span class=\"dots error\" _t>\n                                        Executed on {{item.batch.dateFinished | date:'short'}} with error: {{item.batch.failureMessage}}.\n                                        This recommendation now displays in the\n                                        <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                                    </span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-xs-3\" ng-if=\"item.batch.status === vm.batchStatus.Canceling\">\n                        <xui-spinner show=\"true\"\n                                    size=\"20\"\n                                    class=\"spinner\">\n                        </xui-spinner>\n                        <span _t>Canceling...</span>\n                    </div>\n                    <div class=\"col-xs-3\" ng-if=\"item.batch.status === vm.batchStatus.Canceled && item.batch.revertBatch\">\n                        <ng-include src=\"'revert-batch-status-template'\"></ng-include>\n                    </div>\n                    <div class=\"col-xs-3\" ng-if=\"item.batch.status === vm.batchStatus.Canceled && !item.batch.revertBatch\">\n                        <div class=\"media\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"severity_warning\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\">\n                                <span class=\"dots warning\">\n                                    <span ng-if=\"item.batch.isCanceledByUser()\" _t>\n                                        Canceled by user {{::item.batch.cancelationUserId}} on {{::item.batch.dateFinished | date:'short'}}.\n                                        This recommendation now displays in the\n                                        <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                                    </span>\n                                    <span ng-if=\"item.batch.isCanceledDueToDependencyFailure()\" _t>\n                                        Canceled due to failure of one or more dependencies.\n                                        This recommendation now displays in the\n                                        <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                                    </span>\n                                </span>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-xs-5\" ng-if=\"item.batch.status === vm.batchStatus.Pending\">\n                        <span _t>Pending...</span>\n                    </div>\n                    <div class=\"col-xs-2\" ng-if=\"vm.isMoreDetailsButtonVisible(item)\">\n                        <a class=\"pull-right\"\n                        ng-click=\"vm.openRecommendationDetail(item)\" _t>\n                            More Details\n                        </a>\n                    </div>\n                    <div class=\"col-xs-2\" ng-show=\"vm.isCancelButtonVisible(item)\">\n                        <xui-button class=\"pull-right\"\n                                    size=\"xsmall\"\n                                    display-style=\"tertiary\"\n                                    icon=\"redo\"\n                                    ng-click=\"vm.cancelAndRevertSingle(item)\" _t>\n                            Cancel &amp; Revert\n                        </xui-button>\n                    </div>\n                </div>\n            </xui-expander-heading>\n\n            <div class=\"action-list\">\n                <xui-listview items-source=\"item.actions\"\n                            template-url=\"action-template\"\n                            controller=\"vm\"\n                            stripe=\"false\">\n                </xui-listview>\n            </div>\n\n            <div ng-if=\"item.batch.revertBatch\">\n                <xui-divider></xui-divider>\n\n                <ng-include src=\"'revert-batch-template'\"></ng-include>\n            </div>\n        </xui-expander> </script> <script type=text/ng-template id=revert-batch-template> <div class=\"row detail\">\n            <div class=\"col-xs-5\">\n                <strong _t>Revert Actions</strong>\n            </div>\n            <div class=\"col-xs-5\" ng-if=\"item.batch.revertBatch.status === vm.batchStatus.Running\">\n                <xui-spinner show=\"true\"\n                            size=\"20\"\n                            class=\"spinner\">\n                </xui-spinner><span _t> Applying revert actions...</span>\n            </div>\n            <div class=\"col-xs-5\" ng-if=\"item.batch.revertBatch.status === vm.batchStatus.FinishedSuccessfully\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"severity_ok\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span class=\"dots successfull\" _t>\n                            Succesfully executed on {{item.batch.revertBatch.dateFinished | date:'short'}}.\n                        </span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-5\" ng-if=\"item.batch.revertBatch.status === vm.batchStatus.FinishedWithError\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"severity_error\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span class=\"dots error\" _t>\n                            Executed on {{item.batch.revertBatch.dateFinished | date:'short'}}\n                            with error: {{item.batch.revertBatch.failureMessage}}.\n                        </span>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"action-list\">\n            <xui-listview items-source=\"item.batch.revertBatch.actions\"\n                        template-url=\"action-template\"\n                        controller=\"vm\"\n                        stripe=\"false\">\n            </xui-listview>\n        </div> </script> <script type=text/ng-template id=revert-batch-status-template> <div>\n            <div ng-if=\"item.batch.revertBatch.status === vm.batchStatus.Running\">\n                <div>\n                    <xui-spinner show=\"true\"\n                                size=\"20\"\n                                class=\"spinner\">\n                    </xui-spinner><span _t> Reverting Actions...</span>\n                </div>\n            </div>\n            <div ng-if=\"item.batch.revertBatch.status === vm.batchStatus.FinishedSuccessfully\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"severity_ok\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span ng-if=\"item.batch.status === vm.batchStatus.FinishedWithError\" class=\"dots successfull\" _t>\n                            Executed on {{item.batch.dateFinished | date:'short'}} with error: {{item.batch.failureMessage}}.\n                            Roll back finished successfully. This recommendation now displays in the\n                            <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                        </span>\n                        <span ng-if=\"item.batch.status === vm.batchStatus.Canceled\" class=\"dots successfull\">\n                            <span ng-if=\"item.batch.isCanceledByUser()\" _t>\n                                Canceled by user. Roll back finished successfully. This recommendation now displays in the\n                                <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                            </span>\n                            <span ng-if=\"item.batch.isCanceledDueToDependencyFailure()\" _t>\n                                Canceled due to dependency failure. Roll back finished successfully. This recommendation now displays in the\n                                <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                            </span>\n                        </span>\n                    </div>\n                </div>\n            </div>\n            <div ng-if=\"item.batch.revertBatch.status === vm.batchStatus.FinishedWithError\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"severity_error\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span ng-if=\"item.batch.status === vm.batchStatus.FinishedWithError\" class=\"dots error\" _t>\n                            Executed on {{item.batch.dateFinished | date:'short'}} with error: {{item.batch.failureMessage}}.\n                            Roll back failed. This recommendation now displays in the\n                            <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                        </span>\n                        <span ng-if=\"item.batch.status === vm.batchStatus.Canceled\" class=\"dots error\">\n                            <span ng-if=\"item.batch.isCanceledByUser()\" _t>\n                                Canceled by user. Roll back failed. This recommendation now displays in the\n                                <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                            </span>\n                            <span ng-if=\"item.batch.isCanceledDueToDependencyFailure()\" _t>\n                                Canceled due to dependency failure. Roll back failed. This recommendation now displays in the\n                                <a ng-click=\"vm.routingService.goToFinishedRecommendations()\">finished recommendations</a>.\n                            </span>\n                        </span>\n                    </div>\n                </div>\n            </div>\n        </div> </script> <script type=text/ng-template id=action-template> <running-action-list-item action=\"::item\"></running-action-list-item> </script> </div> ";

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var finishedList_directive_1 = __webpack_require__(93);
exports.default = function (module) {
    module.component("recommendationsFinishedList", finishedList_directive_1.FinishedRecommendationListDirective);
};


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(31);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var finishedList_controller_1 = __webpack_require__(94);
var FinishedRecommendationListDirective = /** @class */ (function () {
    function FinishedRecommendationListDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(95);
        this.controller = finishedList_controller_1.FinishedRecommendationListController;
        this.controllerAs = "vm";
        this.scope = {};
    }
    return FinishedRecommendationListDirective;
}());
exports.FinishedRecommendationListDirective = FinishedRecommendationListDirective;
;


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var recommendationsFilter_1 = __webpack_require__(12);
var list_controller_base_1 = __webpack_require__(18);
/**
 * Controller for finished recommendations list.
 *
 * @class
 * @extends RecommendationsListController
 */
var FinishedRecommendationListController = /** @class */ (function (_super) {
    __extends(FinishedRecommendationListController, _super);
    function FinishedRecommendationListController($rootScope, $scope, $q, batchingService, xuiToastService, swisRecommendationsService, dialogService, statsService, statusService, routingService, paramsService, filtersService, pluggabilityService, dataGroupStatusService, intervalCallerService, permissionsService, _t, demoService) {
        var _this = _super.call(this, $rootScope, $scope, $q, xuiToastService, batchingService, swisRecommendationsService, dialogService, statsService, statusService, routingService, paramsService, filtersService, pluggabilityService, dataGroupStatusService, intervalCallerService, permissionsService, _t, demoService) || this;
        _this.openRecommendationDetail = function (recommendation) {
            return _this.statusService.checkRecommendationStatusHasChanged(recommendation).then(function (result) {
                if (result.isChanged) {
                    return _this.updateData();
                }
                return _this.dialogService.showFinishedDetailDialog(recommendation);
            });
        };
        _this.getFilterProperties = function () {
            var recommendationsFilter = _this.getEmptyRecommendationsFilter();
            return _this.swisRecommendationsService.getRecommendationsFilter(recommendationsFilter).then(function (stats) {
                return _this.createEntityFilterProperty(stats).then(function (entityFilterProperty) {
                    return _this.createStrategyFilterProperty(stats).then(function (strategiesFilterProperty) {
                        var statusFilterProperty = _this.createStatusFilterProperty(stats);
                        var kindFilterProperty = _this.createKindFilterProperty(stats);
                        var severityFilterProperty = _this.createSeverityFilterProperty(stats);
                        var filter = [
                            entityFilterProperty,
                            statusFilterProperty,
                            kindFilterProperty,
                            severityFilterProperty,
                            strategiesFilterProperty
                        ];
                        return filter;
                    });
                });
            });
        };
        /**
         * Creates status filter property upon given result set.
         */
        _this.createStatusFilterProperty = function (stats) {
            var statsPerBatchStatus = _.groupBy(stats, function (st) { return st.batchStatus; });
            var filterValues = [
                {
                    id: enums_1.BatchStatus[enums_1.BatchStatus.FinishedWithError],
                    title: _this._t("Error"),
                    count: (statsPerBatchStatus[enums_1.BatchStatus[enums_1.BatchStatus.FinishedWithError]])
                        ? statsPerBatchStatus[enums_1.BatchStatus[enums_1.BatchStatus.FinishedWithError]].length
                        : 0,
                    icon: "severity_critical"
                }, {
                    id: enums_1.BatchStatus[enums_1.BatchStatus.FinishedSuccessfully],
                    title: _this._t("Successfully Performed"),
                    count: (statsPerBatchStatus[enums_1.BatchStatus[enums_1.BatchStatus.FinishedSuccessfully]])
                        ? statsPerBatchStatus[enums_1.BatchStatus[enums_1.BatchStatus.FinishedSuccessfully]].length
                        : 0,
                    icon: "severity_ok"
                }, {
                    id: enums_1.BatchStatus[enums_1.BatchStatus.Canceled],
                    title: _this._t("Canceled"),
                    count: (statsPerBatchStatus[enums_1.BatchStatus[enums_1.BatchStatus.Canceled]])
                        ? statsPerBatchStatus[enums_1.BatchStatus[enums_1.BatchStatus.Canceled]].length
                        : 0,
                    icon: "severity_warning"
                }
            ];
            return { refId: "status", title: _this._t("Status"), values: filterValues };
        };
        _this.getEmptyRecommendationsFilter = function () {
            var filter = new recommendationsFilter_1.RecommendationsFilter();
            filter.onlyAppliedActions = false;
            filter.filters.status = [
                enums_1.BatchStatus[enums_1.BatchStatus.FinishedSuccessfully],
                enums_1.BatchStatus[enums_1.BatchStatus.FinishedWithError],
                enums_1.BatchStatus[enums_1.BatchStatus.Canceled],
                // Dummy state forcing the finished tab  NOT to filter batches that finished with error and have revert batch in progress
                enums_1.BatchStatus[enums_1.BatchStatus.RevertBatchNotInProgress_Indication]
            ];
            return filter;
        };
        _this.getActionIcon = function (recommendation) {
            // check if already present
            if (!_this.getActionIconDelegate) {
                return "";
            }
            var firstAction = recommendation.actions[0];
            var actionIcon = _this.getActionIconDelegate(firstAction.type);
            return actionIcon;
        };
        _this.saveFilterValues = function () {
            _this.filtersService.finishedRecommendationsFilterValues = _this.filterValues;
        };
        return _this;
    }
    FinishedRecommendationListController.prototype.$onInit = function () {
        var _this = this;
        _super.prototype.$onInit.call(this);
        this.pluggabilityService.getActionIconDelegate().then(function (d) {
            _this.getActionIconDelegate = d;
        });
        this.options.sortableColumns = [this.sortByDateFinished, this.sortByDateStarted];
        this.options.selectionMode = "";
        this.sorting.sortBy = this.sortByDateFinished;
        this.sorting.direction = "desc";
        this.filterValues = this.filtersService.finishedRecommendationsFilterValues;
        this.paramsService.initFilterValues(this.filterValues);
    };
    FinishedRecommendationListController.$inject = list_controller_base_1.RecommendationListController.$inject;
    return FinishedRecommendationListController;
}(list_controller_base_1.RecommendationListController));
exports.FinishedRecommendationListController = FinishedRecommendationListController;


/***/ }),
/* 95 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-list-finished data-selector=recommendation-list-finished> <xui-filtered-list items-source=vm.itemsSource on-refresh=\"vm.getData(filters, pagination, sorting, searching)\" filter-properties-fn=vm.getFilterProperties() options=::vm.options sorting=::vm.sorting pagination-data=::vm.pagination filter-values=vm.filterValues selection=vm.selectedRecommendations filter-property-groups=::vm.objectTypes controller=vm remote-control=vm.remoteControl> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() === 0\"> <xui-button icon=edit display-style=tertiary ng-click=vm.routingService.goToConstraints() ng-if=vm.isManagePoliciesButtonEnabled() _t> Manage Policies </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list> <script type=text/ng-template id=item-template> <div class=\"row detail\">\n            <div class=\"col-xs-5\">\n                <div class=\"media\">\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"{{::vm.getActionIcon(item)}}\"\n                                icon-size=\"small\"></xui-icon>\n                    </div>\n\n                    <div class=\"media-body\">\n                        <span class=\"description\"\n                            title=\"_t(Click to see more details about this recommendation)\"\n                            ng-bind-html=\"::item.description\"\n                            ng-click=\"vm.openRecommendationDetail(item)\" _ta>\n                        </span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-5\"\n                ng-click=\"vm.openRecommendationDetail(item)\">\n\n                <div class=\"media status-ok\"\n                    ng-if=\"::item.batch.status === vm.batchStatus.FinishedSuccessfully\">\n\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"severity_ok\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\" _t>\n                        Successfully finished on {{::item.batch.dateFinished | date:\"short\"}}\n                    </div>\n                </div>\n\n                <div class=\"media status-critical\"\n                    ng-if=\"::item.batch.status === vm.batchStatus.FinishedWithError\">\n\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"severity_error\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\" _t>\n                        Execution failed on {{::item.batch.dateFinished | date:\"short\"}}.\n                        Error: {{::item.batch.failureMessage}}\n                    </div>\n                </div>\n\n                <div class=\"media status-warning\"\n                    ng-if=\"::item.batch.status === vm.batchStatus.Canceled\">\n\n                    <div class=\"media-left\">\n                        <xui-icon icon=\"severity_warning\"></xui-icon>\n                    </div>\n                    <div class=\"media-body\">\n                        <span ng-if=\"item.batch.isCanceledByUser()\" _t>\n                            Canceled by user {{::item.batch.cancelationUserId}} on {{::item.batch.dateFinished | date:\"short\"}}.\n                        </span>\n                        <span ng-if=\"item.batch.isCanceledDueToDependencyFailure()\" _t>\n                            Canceled due to failure of one or more dependencies.\n                        </span>\n                    </div>\n                </div>\n                <div class=\"media status-critical\"\n                    ng-if=\"::item.batch.revertBatch\">\n                    <div class=\"media-body\">\n                        <div class=\"media status-ok\"\n                            ng-if=\"::item.batch.revertBatch.status === vm.batchStatus.FinishedSuccessfully\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"severity_ok\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" _t>\n                                Revert successfully finished on {{::item.batch.revertBatch.dateFinished | date:\"short\"}}\n                            </div>\n                        </div>\n                        <div class=\"media status-critical\"\n                            ng-if=\"::item.batch.revertBatch.status === vm.batchStatus.FinishedWithError\">\n                            <div class=\"media-left\">\n                                <xui-icon icon=\"severity_error\"></xui-icon>\n                            </div>\n                            <div class=\"media-body\" _t>\n                                Revert failed on {{::item.batch.revertBatch.dateFinished | date:\"short\"}}.\n                                Error: {{::item.batch.revertBatch.failureMessage}}\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-2 text-right\" _t>\n                {{::item.batch.dateStarted | date:'short'}}, by {{::item.batch.user}}\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(97);
var index_2 = __webpack_require__(101);
var index_3 = __webpack_require__(105);
var index_4 = __webpack_require__(109);
var index_5 = __webpack_require__(113);
var index_6 = __webpack_require__(119);
var index_7 = __webpack_require__(123);
var index_8 = __webpack_require__(127);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
    index_6.default(module);
    index_7.default(module);
    index_8.default(module);
};


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var footerContents_directive_1 = __webpack_require__(98);
exports.default = function (module) {
    module.component("recommendationDetailDialogFooterContents", footerContents_directive_1.RecommendationDetailDialogFooterContentsDirective);
};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(25);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var footerContents_controller_1 = __webpack_require__(99);
/**
 * Directive for contents in recommendation detail dialog footer
 */
var RecommendationDetailDialogFooterContentsDirective = /** @class */ (function () {
    function RecommendationDetailDialogFooterContentsDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(100);
        this.controller = footerContents_controller_1.FooterContentsController;
        this.controllerAs = "vm";
        this.bindToController = {
            enableCreatePolicy: "<",
            parentCtrl: "<"
        };
        this.scope = true;
    }
    return RecommendationDetailDialogFooterContentsDirective;
}());
exports.RecommendationDetailDialogFooterContentsDirective = RecommendationDetailDialogFooterContentsDirective;


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var dialog_service_1 = __webpack_require__(10);
var index_1 = __webpack_require__(0);
var FooterContentsController = /** @class */ (function () {
    function FooterContentsController($rootScope, xuiToastService, dialogService, swApiService, swisService, permissionsService, _t) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.xuiToastService = xuiToastService;
        this.dialogService = dialogService;
        this.swApiService = swApiService;
        this.swisService = swisService;
        this.permissionsService = permissionsService;
        this._t = _t;
        this.$onInit = function () {
            _this.recommendation = _this.parentCtrl.dialogOptions.viewModel.recommendation;
        };
        this.createConstraint = function () {
            _this.dialogService.showCreateConstraintDialog(_this.recommendation, _this.parentCtrl).then(function (result) {
                switch (result) {
                    case dialog_service_1.CreateConstraintDialogResultStatus.CreatedConstraint:
                        _this.parentCtrl.close({ status: dialog_service_1.ActiveDetailDialogResultStatus.CreatedConstraint });
                        break;
                    default:
                        return;
                }
            });
        };
        this.isExportDiagnosticsDisabled = function () {
            return _this.$rootScope["demoMode"];
        };
        this.displayMoreActions = function () {
            return _this.$rootScope["demoMode"] || _this.permissionsService.isAdminAllowed();
        };
        this.exportDiagnostics = function () {
            // Opening a new window going directly to webservice causes file download
            var urlBase = "/api2/recommendations/GetRecommendationDiagnostics";
            window.open(urlBase + "?jobId=" + _this.recommendation.jobId + "&kind=" + _this.recommendation.kind, "_blank");
        };
    }
    FooterContentsController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngRootScopeService)),
        __param(1, di_1.Inject(index_1.ServiceNames.xuiToastService)),
        __param(2, di_1.Inject(index_1.ServiceNames.dialogService)),
        __param(3, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(4, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(5, di_1.Inject(index_1.ServiceNames.permissionsService)),
        __param(6, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object, Function])
    ], FooterContentsController);
    return FooterContentsController;
}());
exports.FooterContentsController = FooterContentsController;


/***/ }),
/* 100 */
/***/ (function(module, exports) {

module.exports = "<div> <xui-menu class=\"pull-left recommendation-more-actions-menu\" title=\"_t(More actions)\" display-style=tertiary ng-if=vm.displayMoreActions() _ta> <xui-menu-action ng-if=vm.enableCreatePolicy action=vm.createConstraint()> <xui-icon icon-size=small icon=ignore></xui-icon> <span _t>Create Recommendation Policy</span> </xui-menu-action> <xui-menu-action action=vm.exportDiagnostics() is-disabled=vm.isExportDiagnosticsDisabled()> <xui-icon icon-size=small icon=export></xui-icon> <span _t>Export Diagnostics</span> </xui-menu-action> </xui-menu> <xui-button ng-repeat=\"button in ::vm.parentCtrl.dialogOptions.buttons\" display-style=\"{{::button.isPrimary ? 'primary' : button.displayStyle}}\" ng-click=\"vm.parentCtrl.execute(button, vm.parentCtrl.modalDialog)\" ng-class=\"::[button.cssClass , {'pull-left': button.pullLeft}]\" ng-hide=button.isHidden is-disabled=\"vm.parentCtrl.isBusy || button.isDisabled\"> {{button.text}} </xui-button> <xui-button display-style=tertiary ng-click=vm.parentCtrl.cancel() ng-hide=vm.parentCtrl.dialogOptions.hideCancel is-disabled=vm.parentCtrl.isBusy> {{::vm.parentCtrl.dialogOptions.cancelButtonText}} </xui-button> </div> ";

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationAlerts_directive_1 = __webpack_require__(102);
exports.default = function (module) {
    module.component("recommendationAlerts", recommendationAlerts_directive_1.RecommendationAlertsDirective);
};


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationAlerts_controller_1 = __webpack_require__(103);
/**
 * Display related active alerts for given set of recommendations
 */
var RecommendationAlertsDirective = /** @class */ (function () {
    function RecommendationAlertsDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(104);
        this.controller = recommendationAlerts_controller_1.RecommendationAlertsController;
        this.controllerAs = "vm";
        this.scope = {};
        this.bindToController = {
            recommendations: "<",
            onLoad: "&?"
        };
    }
    return RecommendationAlertsDirective;
}());
exports.RecommendationAlertsDirective = RecommendationAlertsDirective;


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var index_1 = __webpack_require__(0);
/**
 * Controller for directive displaying related active alerts
 *
 * @class
 */
var RecommendationAlertsController = /** @class */ (function () {
    function RecommendationAlertsController(dateTimeService, swApiService) {
        var _this = this;
        this.dateTimeService = dateTimeService;
        this.swApiService = swApiService;
        this.alertSeverity = enums_1.AlertSeverity;
        this.alerts = [];
        this.$onChanges = function (changes) {
            if (!changes.recommendations || !changes.recommendations.currentValue) {
                return;
            }
            var recommendations = changes.recommendations.currentValue;
            var ids = _.map(recommendations, function (r) { return r.id; });
            _this.swApiService.getAlertsResolvedByRecommendations(ids).then(function (alerts) {
                _this.alerts = alerts;
                if (_this.onLoad) {
                    var context = { alerts: alerts };
                    _this.onLoad(context);
                }
            });
        };
        this.formatAlertActiveTime = function (alert) {
            return _this.dateTimeService.formatMinutesDuration(alert.activeTime, { useLongNames: false });
        };
        this.getAlertIcon = function (alert) {
            switch (alert.severity) {
                case enums_1.AlertSeverity.Info:
                    return "severity_info";
                case enums_1.AlertSeverity.Warning:
                    return "severity_warning";
                case enums_1.AlertSeverity.Critical:
                    return "severity_critical";
            }
            ;
        };
        this.getAlertDetailUrl = function (alert) {
            return "/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:" + alert.objectId;
        };
    }
    RecommendationAlertsController.$inject = [
        index_1.ServiceNames.dateTimeService,
        index_1.ServiceNames.swApiService
    ];
    return RecommendationAlertsController;
}());
exports.RecommendationAlertsController = RecommendationAlertsController;


/***/ }),
/* 104 */
/***/ (function(module, exports) {

module.exports = "<div class=alerts ng-show=\"vm.alerts.length > 0\"> <div ng-if=\"vm.alerts.length > 0\"> <h4> <span _t>{{::vm.alerts.length}} Active Alerts Will be Resolved</span> &nbsp; <xui-icon icon=help icon-size=small tool-tip=\"_t(This recommendation represents a solution for these active alerts)\" _ta> </xui-icon> </h4> <div ng-repeat=\"al in ::vm.alerts | orderBy:'-severity' | limitTo: 2 track by al.id\"> <div class=row> <div class=col-xs-12> <div class=\"media alert alert-{{::vm.alertSeverity[al.severity].toLowerCase()}}\"> <div class=media-left> <xui-icon icon={{::vm.getAlertIcon(al)}} icon-size=small> </xui-icon> </div> <div class=media-body> <a href={{::vm.getAlertDetailUrl(al)}} target=_blank _t> {{::al.name}} on {{::al.entityCaption}} </a> <br/> {{::al.description}}<br/> <span _t>Active time:</span> {{::vm.formatAlertActiveTime(al)}} </div> </div> </div> </div> </div> <div ng-if=\"vm.alerts.length > 2\"> <span _t=\"['{{::vm.alerts.length - 2}}']\">Another {0} alerts will be resolved too.</span> </div> </div> </div> ";

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationDescription_directive_1 = __webpack_require__(106);
exports.default = function (module) {
    module.component("recommendationDescription", recommendationDescription_directive_1.RecommendationDescriptionDirective);
};


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(26);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var recommendationDescription_controller_1 = __webpack_require__(107);
/**
 * Display recommendation description in recommendation detail dialog.
 */
var RecommendationDescriptionDirective = /** @class */ (function () {
    function RecommendationDescriptionDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(108);
        this.controller = recommendationDescription_controller_1.RecommendationDescriptionController;
        this.controllerAs = "vm";
        this.bindToController = true;
        this.scope = {
            recommendation: "="
        };
    }
    return RecommendationDescriptionDirective;
}());
exports.RecommendationDescriptionDirective = RecommendationDescriptionDirective;


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var RecommendationDescriptionController = /** @class */ (function () {
    function RecommendationDescriptionController() {
    }
    return RecommendationDescriptionController;
}());
exports.RecommendationDescriptionController = RecommendationDescriptionController;


/***/ }),
/* 108 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-description> <h4 _t>Recommendation</h4> <p class=resolution-description ng-bind-html=::vm.recommendation.description></p> </div>";

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationProblem_directive_1 = __webpack_require__(110);
exports.default = function (module) {
    module.component("recommendationProblem", recommendationProblem_directive_1.RecommendationProblemDirective);
};


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(27);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var recommendationProblem_controller_1 = __webpack_require__(111);
/**
 * Display recommendation problem in recommendation detail dialog.
 *
 * @class
 */
var RecommendationProblemDirective = /** @class */ (function () {
    function RecommendationProblemDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(112);
        this.controller = recommendationProblem_controller_1.RecommendationProblemController;
        this.controllerAs = "vm";
        this.bindToController = true;
        this.scope = {
            recommendation: "=",
            resolved: "="
        };
    }
    return RecommendationProblemDirective;
}());
exports.RecommendationProblemDirective = RecommendationProblemDirective;


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var RecommendationProblemController = /** @class */ (function () {
    function RecommendationProblemController() {
        this.priority = enums_1.RecommendationPriority;
    }
    return RecommendationProblemController;
}());
exports.RecommendationProblemController = RecommendationProblemController;


/***/ }),
/* 112 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-problem> <h4 _t>Problem</h4> <p class=risk-description ng-class=\"(vm.resolved) ? 'risk-low' : {'risk-low': vm.recommendation.priority === vm.priority.Low,\n                                                         'risk-medium': vm.recommendation.priority === vm.priority.Medium,\n                                                         'risk-high': vm.recommendation.priority === vm.priority.High}\" ng-bind-html=::vm.recommendation.riskFull> </p> </div>";

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationReferrer_directive_1 = __webpack_require__(114);
exports.default = function (module) {
    module.component("recommendationReferrer", recommendationReferrer_directive_1.RecommendationReferrerDirective);
};


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationReferrer_controller_1 = __webpack_require__(115);
/**
 * Display recommendation context for navigation to previous recommendation.
 */
var RecommendationReferrerDirective = /** @class */ (function () {
    function RecommendationReferrerDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(118);
        this.controller = recommendationReferrer_controller_1.RecommendationReferrerController;
        this.controllerAs = "vm";
        this.bindToController = true;
        this.scope = {
            context: "<",
            dialog: "<"
        };
    }
    return RecommendationReferrerDirective;
}());
exports.RecommendationReferrerDirective = RecommendationReferrerDirective;


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var recommendationReferrer_1 = __webpack_require__(116);
var recommendationMultipleReferrer_1 = __webpack_require__(117);
/**
 * Controller of dialog referrer directive
 *
 * @class
 */
var RecommendationReferrerController = /** @class */ (function () {
    function RecommendationReferrerController(swisService, dialogService, stringService) {
        var _this = this;
        this.swisService = swisService;
        this.dialogService = dialogService;
        this.stringService = stringService;
        this.$onInit = function () {
            _this.isEnabled = !!_this.context && !!_this.context.referrers && !_.isEmpty(_this.context.referrers);
        };
        this.isReferrerSingle = function () {
            var last = _this.getLastReferrer();
            return last ? recommendationReferrer_1.RecommendationReferrerTypeGuard(last) : undefined;
        };
        this.isReferrerMultiple = function () {
            var last = _this.getLastReferrer();
            return last ? recommendationMultipleReferrer_1.RecommendationMultipleReferrerTypeGuard(last) : undefined;
        };
        this.getLastReferrer = function () {
            if (_this.context.referrers && !_.isEmpty(_this.context.referrers)) {
                return _this.context.referrers[_this.context.referrers.length - 1];
            }
            return null;
        };
        this.getReferrerName = function () {
            var lastReferrer = _this.context.referrers[_this.context.referrers.length - 1];
            if (recommendationReferrer_1.RecommendationReferrerTypeGuard(lastReferrer)) {
                return _this.stringService.stripHtml(lastReferrer.recommendation.description);
            }
            if (recommendationMultipleReferrer_1.RecommendationMultipleReferrerTypeGuard(lastReferrer)) {
                var descriptions = _.map(lastReferrer.recommendations, function (r) { return _this.stringService.stripHtml(r.description); });
                return descriptions.join("<br/><br/>");
            }
            throw new Error("Unrecognized referrer");
        };
        this.openRecommendationDetail = function () {
            var lastReferrer = _this.context.referrers.pop();
            if (recommendationReferrer_1.RecommendationReferrerTypeGuard(lastReferrer)) {
                _this.dialog.cancel();
                return _this.dialogService.showDetailDialog(lastReferrer.recommendation, _this.context);
            }
            if (recommendationMultipleReferrer_1.RecommendationMultipleReferrerTypeGuard(lastReferrer)) {
                _this.dialog.cancel();
                // note: we assume that referring multiple dialog is only active
                return _this.dialogService.showActiveMultipleDetailDialog(lastReferrer.recommendations, _this.context);
            }
            throw new Error("Not supported referrer.");
        };
    }
    RecommendationReferrerController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(1, di_1.Inject(index_1.ServiceNames.dialogService)),
        __param(2, di_1.Inject(index_1.ServiceNames.stringService)),
        __metadata("design:paramtypes", [Object, Object, Object])
    ], RecommendationReferrerController);
    return RecommendationReferrerController;
}());
exports.RecommendationReferrerController = RecommendationReferrerController;
;


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Type guard for strong type check of single referrer
 */
exports.RecommendationReferrerTypeGuard = function (value) {
    return !!value && !!value.recommendation && !!value.recommendation.id;
};


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Type guard for strong type check of multiple referrer
 */
exports.RecommendationMultipleReferrerTypeGuard = function (value) {
    return !!value && !!value.recommendations && value.recommendations.constructor === Array;
};


/***/ }),
/* 118 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-referrer ng-show=::vm.isEnabled> <div class=row ng-if=::vm.isEnabled> <div class=col-xs-12> <span class=link ng-if=::vm.isReferrerSingle() ng-click=vm.openRecommendationDetail() xui-popover=\"\" xui-popover-trigger=mouseenter xui-popover-content=::vm.getReferrerName() _t> Back to previous recommendation </span> <span class=link ng-if=::vm.isReferrerMultiple() ng-click=vm.openRecommendationDetail() xui-popover=\"\" xui-popover-trigger=mouseenter xui-popover-content=::vm.getReferrerName() _t> Back to previous recommendations </span> </div> </div> </div>";

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationSchedule_directive_1 = __webpack_require__(120);
exports.default = function (module) {
    module.component("recommendationSchedule", recommendationSchedule_directive_1.RecommendationScheduleDirective);
};


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationSchedule_controller_1 = __webpack_require__(121);
/**
 * Scheduling recommendations directive
 *
 * @class
 */
var RecommendationScheduleDirective = /** @class */ (function () {
    function RecommendationScheduleDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(122);
        this.controller = recommendationSchedule_controller_1.RecommendationScheduleController;
        this.controllerAs = "vm";
        this.bindToController = {
            executionType: "=",
            scheduledTime: "="
        };
        this.scope = {};
    }
    return RecommendationScheduleDirective;
}());
exports.RecommendationScheduleDirective = RecommendationScheduleDirective;


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var index_1 = __webpack_require__(0);
var di_1 = __webpack_require__(1);
/**
 * Controller of scheduling recommendation in dialog
 *
 * @class
 */
var RecommendationScheduleController = /** @class */ (function () {
    function RecommendationScheduleController(dateTimeService) {
        var _this = this;
        this.dateTimeService = dateTimeService;
        this.executionTypeEnum = enums_1.ExecutionType;
        this.$onInit = function () {
            _this.resetTime();
        };
        this.resetTime = function () {
            var orionNowDate = _this.dateTimeService.getOrionNowDate();
            _this.minDate = orionNowDate;
            _this.scheduledTime = orionNowDate;
        };
    }
    RecommendationScheduleController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.dateTimeService)),
        __metadata("design:paramtypes", [Object])
    ], RecommendationScheduleController);
    return RecommendationScheduleController;
}());
exports.RecommendationScheduleController = RecommendationScheduleController;


/***/ }),
/* 122 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-schedule> <xui-radio class=radio-inline ng-model=vm.executionType ng-click=vm.resetTime() ng-value=vm.executionTypeEnum.Now _t> Perform Now </xui-radio> <xui-radio class=radio-inline ng-model=vm.executionType ng-value=vm.executionTypeEnum.Schedule _t> Schedule Recommendation </xui-radio> <div class=radio-inline> <xui-datetime-picker ng-model=vm.scheduledTime is-required=\"vm.executionType === vm.executionTypeEnum.Schedule\" is-disabled=\"vm.executionType !== vm.executionTypeEnum.Schedule\" min-date=vm.minDate display-mode=inline> </xui-datetime-picker> </div> </div>";

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationStatistics_directive_1 = __webpack_require__(124);
exports.default = function (module) {
    module.component("recommendationStatistics", recommendationStatistics_directive_1.RecommendationStatisticsDirectve);
};


/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(28);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var recommendationStatistics_controller_1 = __webpack_require__(125);
/**
 * Display recommendation statistics in recommendation detail dialog.
 *
 * @class
 */
var RecommendationStatisticsDirectve = /** @class */ (function () {
    function RecommendationStatisticsDirectve() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(126);
        this.controller = recommendationStatistics_controller_1.RecommendationStatisticsController;
        this.controllerAs = "vm";
        this.bindToController = true;
        this.scope = {
            recommendations: "<"
        };
    }
    return RecommendationStatisticsDirectve;
}());
exports.RecommendationStatisticsDirectve = RecommendationStatisticsDirectve;


/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Stats controller
 *
 * @class
 */
var RecommendationStatisticsController = /** @class */ (function () {
    function RecommendationStatisticsController($q, swisService) {
        var _this = this;
        this.$q = $q;
        this.swisService = swisService;
        this.$onInit = function () {
            _this.initProperties();
            _this.ensureJustificationsLoaded().then(function () {
                // keep all justifications together
                _this.justifications = _.map(_this.recommendations, function (r) { return r.justification; });
                // get all entities (select and flatten to single array)
                var allChangedEntities = _.flatten(_.map(_this.justifications, function (j) { return j.ChangedEntities; }));
                // get just unique entities
                var uniqueChangedEntities = _.uniqBy(allChangedEntities, function (e) { return e.Id.Id; });
                _this.changedEntities = uniqueChangedEntities;
                _.each(_this.changedEntities, function (e) {
                    _this.changedEntitiesExpanded[e.Id.Id] = false;
                });
                _this.justificationIsLoaded = true;
            });
        };
        /**
         * Ensures that all recommendations have its justification field loaded.
         */
        this.ensureJustificationsLoaded = function () {
            var withoutJustifications = _.filter(_this.recommendations, function (r) { return !r.justification; });
            if (_.isEmpty(withoutJustifications)) {
                return _this.$q.when(true);
            }
            var withoutJustificationIds = _.map(withoutJustifications, function (r) { return r.id; });
            return _this.swisService.getRecommendationJustifications(withoutJustificationIds)
                .then(function (result) {
                _.each(withoutJustifications, function (r) {
                    r.justification = result[r.id];
                });
            });
        };
        this.getEntityStatusIconUrl = function (entity) {
            return "/Orion/StatusIcon.ashx?entity=" + entity.SwisEntityName + "&KeyPropertyValue=" + entity.ObjectId;
        };
    }
    RecommendationStatisticsController.prototype.initProperties = function () {
        this.justifications = [];
        this.justificationIsLoaded = false;
        this.changedEntitiesExpanded = {};
        this.changedEntities = [];
        this.isRealtime = _.last(this.recommendations).isRealtime();
    };
    ;
    RecommendationStatisticsController = __decorate([
        __param(0, di_1.Inject("$q")),
        __param(1, di_1.Inject(index_1.ServiceNames.swisService)),
        __metadata("design:paramtypes", [Function, Object])
    ], RecommendationStatisticsController);
    return RecommendationStatisticsController;
}());
exports.RecommendationStatisticsController = RecommendationStatisticsController;


/***/ }),
/* 126 */
/***/ (function(module, exports) {

module.exports = "<div> <div class=recommendation-statistics ng-if=vm.justificationIsLoaded> <xui-listview items-source=::vm.changedEntities controller=vm template-url=outcome-item-template></xui-listview> <script type=text/ng-template id=outcome-item-template> <xui-expander is-open=\"vm.changedEntitiesExpanded[item.Id.Id]\">\n                <xui-expander-heading>\n                    <div class=\"row vertical-center\">\n                        <div class=\"col-xs-4 entity\">\n                            <img ng-src=\"{{::vm.getEntityStatusIconUrl(item)}}\" />\n                            <strong>{{::item.Name}}</strong>\n                        </div>\n                        \n                        <div class=\"col-xs-8\">\n                            <re-plugin-placeholder area-name=\"recommendationDetailOutcomeItemHeader\"\n                                                   context=\"{item: item, justifications: vm.justifications}\">\n                            </re-plugin-placeholder>\n                        </div>\n                    </div>\n                </xui-expander-heading>\n                <div class=\"outcome\">\n                    <xui-if-show visible=\"vm.changedEntitiesExpanded[item.Id.Id]\">\n                        <re-plugin-placeholder area-name=\"recommendationDetailOutcomeItem\"\n                                               context=\"{item: item, isRealtime: vm.isRealtime, justifications: vm.justifications}\">\n                        </re-plugin-placeholder>\n                    </xui-if-show>\n                </div>\n            </xui-expander> </script> </div> </div> ";

/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationStatus_directive_1 = __webpack_require__(128);
exports.default = function (module) {
    module.component("recommendationStatus", recommendationStatus_directive_1.RecommendationStatusDirective);
};


/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(29);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var recommendationStatus_controller_1 = __webpack_require__(129);
/**
 * Display recommendation status in recommendation detail dialog.
 *
 * @class
 */
var RecommendationStatusDirective = /** @class */ (function () {
    function RecommendationStatusDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(130);
        this.controller = recommendationStatus_controller_1.RecommendationStatusController;
        this.controllerAs = "vm";
        this.bindToController = {
            recommendation: "<",
            actions: "<"
        };
        this.scope = {};
    }
    return RecommendationStatusDirective;
}());
exports.RecommendationStatusDirective = RecommendationStatusDirective;


/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var index_1 = __webpack_require__(0);
var RecommendationStatusController = /** @class */ (function () {
    function RecommendationStatusController(swisService, _t) {
        var _this = this;
        this.swisService = swisService;
        this.batchStatus = enums_1.BatchStatus;
        this.$onInit = function () {
            _this.revertBatch = null;
            _this.revertBatchLoaded = null;
        };
        this.$onChanges = function (obj) {
            // check if recommendation is set
            if (!obj.recommendation || !obj.recommendation.currentValue) {
                return;
            }
            var r = obj.recommendation.currentValue;
            // load revert batch if any
            if (!r.batch.isCanceledAndReverted()) {
                return;
            }
            _this.swisService.getRevertBatchWithActions(r.batch.id)
                .then(function (revertBatch) {
                _this.revertBatch = revertBatch;
                _this.revertBatchLoaded = true;
            });
        };
        this.moreActionsFailed = function () {
            return _.filter(_this.actions, function (a) { return a.Status === enums_1.ActionStatus.FinishedWithError; }).length > 1;
        };
    }
    RecommendationStatusController.$inject = [
        index_1.ServiceNames.swisService,
        index_1.ServiceNames.getTextService
    ];
    return RecommendationStatusController;
}());
exports.RecommendationStatusController = RecommendationStatusController;


/***/ }),
/* 130 */
/***/ (function(module, exports) {

module.exports = "<div class=recommendation-status> <xui-message type=ok ng-if=\"vm.recommendation.batch.status === vm.batchStatus.FinishedSuccessfully\" _t> This recommendation has been performed on {{::vm.recommendation.batch.dateStarted | date:\"short\"}} by {{::vm.recommendation.batch.user}} and successfully finished on {{::vm.recommendation.batch.dateFinished | date:\"short\"}}. </xui-message> <xui-message type=error ng-if=\"vm.recommendation.batch.status === vm.batchStatus.FinishedWithError\"> <span ng-if=vm.moreActionsFailed() _t> This recommendation has been performed on {{::vm.recommendation.batch.dateStarted | date:\"short\"}} by {{::vm.recommendation.batch.user}} and finished on {{::vm.recommendation.batch.dateFinished | date:\"short\"}} with multiple errors. </span> <span ng-if=!vm.moreActionsFailed() _t> This recommendation has been performed on {{::vm.recommendation.batch.dateStarted | date:\"short\"}} by {{::vm.recommendation.batch.user}} and finished on {{::vm.recommendation.batch.dateFinished | date:\"short\"}} with error. </span> </xui-message> <xui-message type=warning ng-if=\"vm.recommendation.batch.status === vm.batchStatus.Canceled && !vm.revertBatch\"> <span ng-if=vm.recommendation.batch.isCanceledByUser() _t> This recommendation was canceled on {{::vm.recommendation.batch.dateFinished | date:\"short\"}} by {{::vm.recommendation.batch.cancelationUserId}} </span> <span ng-if=vm.recommendation.batch.isCanceledDueToDependencyFailure() _t> This recommendation was canceled on {{::vm.recommendation.batch.dateFinished | date:\"short\"}} due to failure of one or more dependencies. </span> </xui-message> <xui-message type=warning ng-if=\"vm.revertBatch.status === vm.batchStatus.FinishedSuccessfully\" _t> This recommendation was successfully canceled and reverted on {{::vm.recommendation.batch.dateFinished | date:\"short\"}} by {{::vm.recommendation.batch.cancelationUserId}} </xui-message> <xui-message type=error ng-if=\"vm.revertBatch.status === vm.batchStatus.FinishedWithError\" _t> This recommendation has been canceled and revert failed on {{::vm.recommendation.batch.dateFinished | date:\"short\"}} by {{::vm.recommendation.batch.cancelationUserId}} </xui-message> <div ng-if=\"vm.recommendation.batch.status === vm.batchStatus.Running\"> <xui-spinner show=true size=20 class=spinner> </xui-spinner> <span _t>Applying recommendation...</span> </div> <div ng-if=\"vm.recommendation.batch.status === vm.batchStatus.Canceling\"> <xui-spinner show=true size=20 class=spinner> </xui-spinner> <span _t>Canceling...</span> </div> </div> ";

/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var runningActionListItem_directive_1 = __webpack_require__(132);
exports.default = function (module) {
    module.component("runningActionListItem", runningActionListItem_directive_1.RunningActionListItemDirective);
};


/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
/**
 * Running action item directive
 *
 * @class
 * @implements IDirective
 */
var RunningActionListItemDirective = /** @class */ (function () {
    function RunningActionListItemDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(133);
        this.scope = {
            action: "="
        };
        this.link = function (scope) {
            scope.actionStatus = enums_1.ActionStatus;
        };
    }
    return RunningActionListItemDirective;
}());
exports.RunningActionListItemDirective = RunningActionListItemDirective;


/***/ }),
/* 133 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row detail\"> <div class=\"col-xs-5 description\"> <div ng-bind-html=action.description></div> </div> <div class=col-xs-7> <div ng-if=\"action.status === actionStatus.FinishedSuccessfully\"> <div class=media> <div class=media-left> <xui-icon icon=severity_ok></xui-icon> </div> <div class=media-body> <span class=\"dots successfull\" _t> Successfully finished on {{action.dateFinished | date:'short'}} </span> </div> </div> </div> <div ng-if=\"action.status === actionStatus.FinishedWithError\"> <div class=media> <div class=media-left> <xui-icon icon=severity_error></xui-icon> </div> <div class=media-body> <span class=\"dots error\" _t> Executed with error: {{action.failureMessage}} </span> </div> </div> </div> <div ng-show=\"action.status === actionStatus.Running\"> <xui-progress show=true show-progress=true thin=true percent={{action.progress}} message=\"_t(Executing the action, please wait...)\" _ta> </xui-progress> </div> <div ng-show=\"action.status === actionStatus.Canceling\"> <span class=dots _t> Cancelling... </span> </div> <div ng-show=\"action.status === actionStatus.Canceled\"> <div class=media> <div class=media-left> <xui-icon icon=severity_warning></xui-icon> </div> <div class=media-body> <span class=\"dots warning\" _t> Action was canceled </span> </div> </div> </div> </div> </div> ";

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var strategySwitch_directive_1 = __webpack_require__(135);
exports.default = function (module) {
    module.component("reStrategySwitch", strategySwitch_directive_1.StrategySwitchDirective);
};


/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var strategySwitch_controller_1 = __webpack_require__(136);
var layoutCompact = "compact";
/**
 * Allows enable or disable strategy
 *
 * @param layout Values can be "regular" (default) or "compact"
 * @param config IStrategy - strategy configuration
 * @param name strategy name
 * @param description strategy description
 */
var StrategySwitchDirective = /** @class */ (function () {
    function StrategySwitchDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = function (element, attrs) {
            return attrs["layout"] === layoutCompact
                ? __webpack_require__(137)
                : __webpack_require__(138);
        };
        this.controller = strategySwitch_controller_1.StrategySwitchController;
        this.controllerAs = "vm";
        this.bindToController = {
            config: "=",
            name: "@",
            description: "@",
            disabled: "<"
        };
        this.scope = {};
    }
    return StrategySwitchDirective;
}());
exports.StrategySwitchDirective = StrategySwitchDirective;
;


/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var StrategySwitchController = /** @class */ (function () {
    function StrategySwitchController($scope, swApiService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.swApiService = swApiService;
        this._t = _t;
        this.$onInit = function () {
            _this.isSwitchingInProgress = false;
            _this.enabledLabel = _this._t("Enabled");
            _this.disabledLabel = _this._t("Disabled");
            _this.$scope.$watch(function () { return _this.config ? _this.config.enabled : undefined; }, function (newValue) {
                if (!_.isUndefined(newValue)) {
                    _this.updateStatusLabel(newValue);
                }
            });
        };
        this.toggleEnabled = function () {
            _this.isSwitchingInProgress = true;
            return _this.swApiService.enableDisableStrategy(_this.config.id, _this.config.enabled)
                .catch(function () {
                _this.config.enabled = !_this.config.enabled;
            })
                .finally(function () {
                _this.isSwitchingInProgress = false;
            });
        };
        this.updateStatusLabel = function (value) {
            _this.statusLabel = value ? _this.enabledLabel : _this.disabledLabel;
        };
        this.isDisabled = function () {
            return _this.disabled || _this.isSwitchingInProgress;
        };
    }
    StrategySwitchController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(2, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Object, Function])
    ], StrategySwitchController);
    return StrategySwitchController;
}());
exports.StrategySwitchController = StrategySwitchController;
;


/***/ }),
/* 137 */
/***/ (function(module, exports) {

module.exports = "<div class=media> <div class=media-left> <xui-switch ng-click=vm.toggleEnabled() ng-model=vm.config.enabled ng-disabled=vm.isDisabled()> </xui-switch> </div> <div class=media-body data-selector=strategy-name> <strong>{{::vm.name}}</strong> <p>{{::vm.description}}</p> </div> </div>";

/***/ }),
/* 138 */
/***/ (function(module, exports) {

module.exports = "<div class=strategy> <div class=row> <div class=col-xs-3 data-selector=strategy-name> <strong>{{::vm.name}}</strong> </div> <div class=col-xs-7> <p>{{::vm.description}}</p> </div> <div class=col-xs-1> <xui-switch class=pull-right ng-click=vm.toggleEnabled() ng-model=vm.config.enabled ng-disabled=vm.isDisabled()> </xui-switch> </div> <div class=col-xs-1> <strong ng-class=\"{'sw-recommendations-switch-label-enabled': vm.config.enabled}\"> {{vm.statusLabel}} </strong> </div> </div> </div>";

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var strategySwitchGroup_directive_1 = __webpack_require__(140);
exports.default = function (module) {
    module.component("reStrategySwitchGroup", strategySwitchGroup_directive_1.StrategySwitchGroupDirective);
};


/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var strategySwitchGroup_controller_1 = __webpack_require__(141);
/**
 * Allows enable or disable all strategies in given group
 */
var StrategySwitchGroupDirective = /** @class */ (function () {
    function StrategySwitchGroupDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = true;
        this.template = __webpack_require__(142);
        this.controller = strategySwitchGroup_controller_1.StrategySwitchGroupController;
        this.controllerAs = "vm";
        this.bindToController = {
            config: "=",
            name: "@",
            description: "@",
            disabled: "<"
        };
        this.scope = {};
    }
    return StrategySwitchGroupDirective;
}());
exports.StrategySwitchGroupDirective = StrategySwitchGroupDirective;
;


/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var StrategySwitchGroupController = /** @class */ (function () {
    function StrategySwitchGroupController($scope, $q, swApiService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$q = $q;
        this.swApiService = swApiService;
        this._t = _t;
        this.switchInProgress = false;
        this.enabledLabel = this._t("Enabled");
        this.disabledLabel = this._t("Disabled");
        this.$onInit = function () {
            _this.statusLabel = _this.enabledLabel;
            _this.$scope.$watchCollection(function () { return _.map(_this.config, function (strategy) { return strategy.enabled; }); }, function (newValue) {
                _this.enabled = _.some(newValue);
                _this.updateStatusLabel();
            });
        };
        this.toggleEnabled = function () {
            if (_this.switchInProgress) {
                throw Error("Changing configuration is in progress.");
            }
            _this.switchInProgress = true;
            var changePromises = [];
            _.each(_this.config, function (strategy, id) {
                strategy.enabled = _this.enabled;
                var promise = _this.swApiService.enableDisableStrategy(strategy.id, strategy.enabled)
                    .catch(function () {
                    strategy.enabled = !strategy.enabled;
                });
                changePromises.push(promise);
            });
            return _this.$q.all(changePromises)
                .finally(function () {
                _this.switchInProgress = false;
            });
        };
        this.isSwitchingInProgress = function () {
            return _this.switchInProgress;
        };
        this.updateStatusLabel = function () {
            _this.statusLabel = _this.enabled ? _this.enabledLabel : _this.disabledLabel;
        };
        this.isDisabled = function () {
            return _this.disabled || _this.isSwitchingInProgress();
        };
    }
    StrategySwitchGroupController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(2, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(3, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Function, Object, Function])
    ], StrategySwitchGroupController);
    return StrategySwitchGroupController;
}());
exports.StrategySwitchGroupController = StrategySwitchGroupController;


/***/ }),
/* 142 */
/***/ (function(module, exports) {

module.exports = "<div class=strategy> <div class=row> <div class=col-xs-3 data-selector=strategy-name> <strong>{{::vm.name}}</strong> </div> <div class=col-xs-7> <p ng-bind-html=::vm.description></p> </div> <div class=col-xs-1> <xui-switch class=pull-right ng-click=vm.toggleEnabled() ng-disabled=vm.isDisabled() ng-model=vm.enabled> </xui-switch> </div> <div class=col-xs-1> <strong ng-class=\"{'sw-recommendations-switch-label-enabled': vm.enabled}\"> {{vm.statusLabel}} </strong> </div> </div> <xui-expander heading=\"_t(Advanced Options)\" _ta> <div ng-transclude=\"\"></div> </xui-expander> </div>";

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(7);
var routingRegistration_provider_1 = __webpack_require__(144);
var replaceProviderKeyword = function (name) {
    return name.replace("Provider", "");
};
exports.default = function (module) {
    module.provider(replaceProviderKeyword(index_1.ProviderNames.routingRegistrationProvider), routingRegistration_provider_1.RoutingRegistrationProvider);
};


/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(145);
var routingDeniedArgs_1 = __webpack_require__(148);
var index_2 = __webpack_require__(0);
var index_3 = __webpack_require__(7);
var summary_controller_1 = __webpack_require__(149);
var settings_controller_1 = __webpack_require__(150);
var strategies_tab_controller_1 = __webpack_require__(151);
var errorStateParams = {
    errorType: "auth",
    errorCode: "403",
    message: "Access Denied",
    location: null
};
var constraintEditUrlParams = "{constraintId:string}";
/**
 * Function used as a resolve dependency to validate user permissions through "authProfile" route property
 *
 * @function
 */
var isAuthorized = function (permissionsService, $q, $stateParams) {
    // TODO: UIF-3806
    var authProfile = angular.copy($stateParams.authProfile);
    authProfile.permissions.push(index_1.Permission.recommendations);
    return permissionsService.hasUserPermissions(authProfile).then(function (result) {
        if (result) {
            return $q.resolve();
        }
        else {
            var args = {
                recommendationsAccessDenied: true
            };
            return $q.reject(args);
        }
    });
};
isAuthorized.$inject = [index_2.ServiceNames.permissionsService, index_2.ServiceNames.ngQService, index_2.ServiceNames.ngStateParams];
/**
 * Function used for ensuring pluggability is initialized
 *
 * @function
 */
var isPluggabilityLoaded = function (pluggabilityService) { return pluggabilityService.ensureIsInitialized(); };
isPluggabilityLoaded.$inject = [index_2.ServiceNames.pluggabilityService];
/**
 * Provider for routing-related registration
 *
 * @class
 */
var RoutingRegistrationProvider = /** @class */ (function () {
    function RoutingRegistrationProvider($stateProvider, $urlRouterProvider) {
        var _this = this;
        this.$stateProvider = $stateProvider;
        this.$urlRouterProvider = $urlRouterProvider;
        this.getPluginConstraintRouteName = function (pluginName, constraintName) {
            return "recommendations_constraint_plugin_" + pluginName.toLowerCase() + "_" + constraintName;
        };
        this.registerPluginConstraintRoute = function (configuration) {
            var stateName = _this.getPluginConstraintRouteName(configuration.pluginName, configuration.constraintName);
            var constraintsUrlBase = index_1.RouteUrls.pluginsRootUrl + "/" + configuration.pluginName.toLowerCase() + "/constraints";
            var stateUrl = constraintsUrlBase + "/" + configuration.constraintName + "?{constraintId:string}";
            var routerState = {
                i18Title: configuration.i18title,
                url: stateUrl,
                controller: configuration.controller,
                controllerAs: "vm",
                template: configuration.template,
                params: angular.extend({}, errorStateParams, { location: stateUrl, authProfile: index_1.AuthProfiles.guestProfile }),
                authProfile: index_1.AuthProfiles.guestProfile,
                resolve: {
                    authenticate: isAuthorized
                }
            };
            _this.$stateProvider.state(stateName, routerState);
        };
        this.registerRoutes = function () {
            _this.registerSummaryRoutes();
            _this.registerSettingsRoutes();
        };
        this.registerSummaryRoutes = function () {
            _this.registerRecommendationsSummaryRoute();
            _this.registerActiveRecommendationsRoute();
            _this.registerScheduledRecommendationsRoute();
            _this.registerRunningRecommendationsRoute();
            _this.registerFinishedRecommendationsRoute();
        };
        this.registerSettingsRoutes = function () {
            _this.registerSettingsRoute();
            _this.registerConstraintSettingsRoute();
            _this.registerStrategiesSettingsRoute();
        };
        this.registerRecommendationsSummaryRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.recommendationsSummaryStateName, index_1.RouteUrls.rootUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Recommendations summary)",
                controller: summary_controller_1.SummaryController,
                template: __webpack_require__(152)
            });
        };
        this.registerActiveRecommendationsRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.activeRecommendationsStateName, index_1.RouteUrls.activeRecommendationsChildUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Current recommendations)",
                template: __webpack_require__(38)
            });
            _this.registerInternalRoute(index_1.RouteNames.activeRecommendationDetailStateName, index_1.RouteUrls.activeRecommendationDetailChildUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Current recommendations)",
                template: __webpack_require__(38)
            });
        };
        this.registerScheduledRecommendationsRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.scheduledRecommendationsStateName, index_1.RouteUrls.scheduledRecommendationsChildUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Scheduled recommendations)",
                template: __webpack_require__(153)
            });
        };
        this.registerRunningRecommendationsRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.runningRecommendationsStateName, index_1.RouteUrls.runningRecommendationsChildUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Running recommendations)",
                template: __webpack_require__(154)
            });
        };
        this.registerFinishedRecommendationsRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.finishedRecommendationsStateName, index_1.RouteUrls.finishedRecommendationsChildUrl, index_1.AuthProfiles.guestProfile, {
                i18title: "_t(Historical recommendations)",
                template: __webpack_require__(155)
            });
        };
        this.registerSettingsRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.settingsStateName, index_1.RouteUrls.settingsStateUrl, index_1.AuthProfiles.adminProfileDemoAllowed, {
                i18title: "_t(Recommendations settings)",
                controller: settings_controller_1.SettingsController,
                template: __webpack_require__(156)
            });
        };
        this.registerConstraintSettingsRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.constraintsStateName, index_1.RouteUrls.constraintsChildUrl, index_1.AuthProfiles.adminProfileDemoAllowed, {
                i18title: "_t(Recommendations policies settings)",
                template: __webpack_require__(157)
            });
        };
        this.registerStrategiesSettingsRoute = function () {
            _this.registerInternalRoute(index_1.RouteNames.strategiesSettingsStateName, index_1.RouteUrls.strategiesChildUrl, index_1.AuthProfiles.adminProfileDemoAllowed, {
                i18title: "_t(Recommendations strategies settings)",
                controller: strategies_tab_controller_1.StrategiesTabController,
                template: __webpack_require__(158)
            });
        };
        this.registerInternalRoute = function (stateName, stateUrl, authProfile, configuration) {
            var routerState = {
                i18Title: configuration.i18title,
                url: stateUrl,
                template: configuration.template,
                params: angular.extend({}, errorStateParams, { location: stateUrl, authProfile: authProfile }),
                authProfile: authProfile,
                resolve: {
                    authenticate: isAuthorized,
                    isPluggabilityLoaded: isPluggabilityLoaded
                }
            };
            if (configuration.controller) {
                routerState.controller = configuration.controller;
                routerState.controllerAs = "vm";
            }
            _this.$stateProvider.state(stateName, routerState);
        };
        this.registerRoutingDeniedHandler = function ($rootScope, routingService) {
            $rootScope.$on("$stateChangeError", function (e, toState, toParams, fromState, fromParams, error) {
                if (error && error.detail && routingDeniedArgs_1.RoutingDeniedArgsTypeGuard(error.detail)) {
                    routingService.goToError(toState.params);
                    e.preventDefault();
                }
            });
        };
        this.initGet = function () {
            _this.$get = [index_2.ServiceNames.ngRootScopeService, index_2.ServiceNames.routingService,
                function ($rootScope, routingService) {
                    var service = {
                        getPluginConstraintRouteName: _this.getPluginConstraintRouteName,
                        registerRoutingDeniedHandler: function () { return _this.registerRoutingDeniedHandler($rootScope, routingService); }
                    };
                    return service;
                }];
        };
        this.initGet();
    }
    RoutingRegistrationProvider = __decorate([
        __param(0, di_1.Inject(index_3.ProviderNames.ngStateProvider)),
        __param(1, di_1.Inject(index_3.ProviderNames.ngUrlRouterProvider)),
        __metadata("design:paramtypes", [Object, Object])
    ], RoutingRegistrationProvider);
    return RoutingRegistrationProvider;
}());
exports.RoutingRegistrationProvider = RoutingRegistrationProvider;
;


/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(146));
__export(__webpack_require__(8));
__export(__webpack_require__(2));
__export(__webpack_require__(11));
__export(__webpack_require__(22));
__export(__webpack_require__(37));
__export(__webpack_require__(19));
__export(__webpack_require__(147));


/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(8);
/**
 * Auth profiles used by Recommendations engine
 *
 * @const
 * @var
 */
exports.AuthProfiles = {
    guestProfile: {
        roles: [],
        permissions: []
    },
    adminProfile: {
        roles: [constants_1.Role.admin],
        permissions: []
    },
    adminProfileDemoAllowed: {
        roles: [constants_1.Role.admin, constants_1.Role.demo],
        permissions: []
    }
};


/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var rootUrl = "/recommendations";
var recommendationsUrlParams = "{groupId:string}";
/**
 * Urls for routes used by Recommendations engine
 *
 * @const
 */
exports.RouteUrls = {
    rootUrl: rootUrl,
    activeRecommendationsChildUrl: "/current?" + recommendationsUrlParams,
    activeRecommendationDetailChildUrl: "/{recommendationId:[\\w-]{36}}",
    scheduledRecommendationsChildUrl: "/scheduled?" + recommendationsUrlParams,
    runningRecommendationsChildUrl: "/running?" + recommendationsUrlParams,
    finishedRecommendationsChildUrl: "/history?" + recommendationsUrlParams,
    settingsStateUrl: rootUrl + "/settings",
    constraintsChildUrl: "/policies",
    strategiesChildUrl: "/strategies",
    pluginsRootUrl: rootUrl + "/plugins"
};


/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Type guard for type checking
 *
 * @func
 */
exports.RoutingDeniedArgsTypeGuard = function (value) {
    return !!value && !!value.recommendationsAccessDenied;
};


/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
/**
 * Summary controller for wraping relevant list of recommendations
 *
 * @class
 */
var SummaryController = /** @class */ (function () {
    function SummaryController($scope, $interval, $state, permissionsService, statsService, routingService, _t, orionInfoService, swApiService, intervalCallerService, demoService) {
        var _this = this;
        this.$scope = $scope;
        this.$interval = $interval;
        this.$state = $state;
        this.permissionsService = permissionsService;
        this.statsService = statsService;
        this.routingService = routingService;
        this._t = _t;
        this.orionInfoService = orionInfoService;
        this.swApiService = swApiService;
        this.intervalCallerService = intervalCallerService;
        this.demoService = demoService;
        this.init = function () {
            _this.initProperties();
            _this.intervalCallerService.subscribe(_this);
            _this.$scope.$on("$destroy", function () {
                _this.intervalCallerService.unsubscribe(_this);
            });
            _this.isSettingsPageGranted = _this.demoService.isDemoMode() || _this.permissionsService.isAdminAllowed();
            return _this.getHelpLink();
        };
        this.subscriberCallback = function () {
            _this.statsService.getTotalRecommendationsCount();
        };
        this.getActiveTabTitle = function () {
            var count = _this.statsService.getActiveRecommendationsCount();
            return _this.getTabTitle(_this.activeTabTitle, count);
        };
        this.getScheduledTabTitle = function () {
            var count = _this.statsService.getScheduledRecommendationsCount();
            return _this.getTabTitle(_this.scheduledTabTitle, count);
        };
        this.getRunningTabTitle = function () {
            var count = _this.statsService.getRunningRecommendationsCount();
            return _this.getTabTitle(_this.runningTabTitle, count);
        };
        this.getFinishedTabTitle = function () {
            return _this.finishedTabTitle;
        };
        this.getTabTitle = function (title, count) {
            if (count) {
                return title + " (" + count + ")";
            }
            return title;
        };
        this.getHelpLink = function () {
            return _this.orionInfoService.getHelpUrl("OrionVMPH_Recommendations_Overview").then(function (result) {
                _this.helpLink = result;
            });
        };
    }
    SummaryController.prototype.initProperties = function () {
        this.helpLink = "#";
        this.subscriberId = "SummaryController";
        this.title = this._t("All Recommendations");
        this.activeTabTitle = this._t("Current");
        this.scheduledTabTitle = this._t("Scheduled");
        this.runningTabTitle = this._t("Running");
        this.finishedTabTitle = this._t("History");
    };
    ;
    SummaryController.$inject = [
        index_1.ServiceNames.ngScope,
        index_1.ServiceNames.ngIntervalService,
        index_1.ServiceNames.ngState,
        index_1.ServiceNames.permissionsService,
        index_1.ServiceNames.statsService,
        index_1.ServiceNames.routingService,
        index_1.ServiceNames.getTextService,
        index_1.ServiceNames.orionInfoService,
        index_1.ServiceNames.swApiService,
        index_1.ServiceNames.intervalCallerService,
        index_1.ServiceNames.demoService
    ];
    return SummaryController;
}());
exports.SummaryController = SummaryController;


/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var SettingsController = /** @class */ (function () {
    function SettingsController(routingService, _t, orionInfoService) {
        var _this = this;
        this.routingService = routingService;
        this._t = _t;
        this.orionInfoService = orionInfoService;
        this.init = function () {
            _this.helpLink = "#";
            _this.constraintsTabTitle = _this._t("Policies");
            _this.strategiesTabTitle = _this._t("Strategies");
            _this.title = _this._t("Recommendations Settings");
            _this.initRecommendationsUrl();
            return _this.getHelpLink();
        };
        this.initRecommendationsUrl = function () {
            if (_this.routingService.isComingFromRecommendations()) {
                _this.recommendationsUrl = _this.routingService.getPreviousStateHref();
            }
            else {
                _this.recommendationsUrl = _this.routingService.getRecommendationsHref();
            }
        };
        this.getHelpLink = function () {
            return _this.orionInfoService.getHelpUrl("OrionVMPH_Configure_Recommendations_Settings").then(function (result) {
                _this.helpLink = result;
            });
        };
    }
    SettingsController.$inject = [
        index_1.ServiceNames.routingService,
        index_1.ServiceNames.getTextService,
        index_1.ServiceNames.orionInfoService
    ];
    return SettingsController;
}());
exports.SettingsController = SettingsController;


/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var StrategiesTabController = /** @class */ (function () {
    function StrategiesTabController($q, $scope, $log, xuiToastService, statusService, dialogService, swApiService, swisService, _t) {
        var _this = this;
        this.$q = $q;
        this.$scope = $scope;
        this.$log = $log;
        this.xuiToastService = xuiToastService;
        this.statusService = statusService;
        this.dialogService = dialogService;
        this.swApiService = swApiService;
        this.swisService = swisService;
        this._t = _t;
        this.init = function () {
            _this.isRecommendationsEnabled = false;
            _this.isHistoryAvailable = false;
            _this.isSwitchingInProgress = false;
            _this.statusLabel = _this._t("All features of recommendations are currently disabled.");
            _this.switchLabel = _this._t("Disabled");
            _this.pluginSettingsDeferred = _this.$q.defer();
            _this.swisService.isRecommendationsEnabled().then(function (enabled) {
                _this.isRecommendationsEnabled = enabled;
                _this.onRecommendationsEnabledChanged();
                // allow plugin settings to initialize
                _this.pluginSettingsDeferred.resolve({ recommendationsEnabled: enabled });
                _this.swisService.isHistoryAvailable().then(function (historyAvailable) {
                    _this.isHistoryAvailable = historyAvailable;
                });
            });
        };
        /**
         * Shows the dialog with confirmation of enabling/disabling whole module
         */
        this.showConfirmationDialog = function () {
            _this.isSwitchingInProgress = true;
            if (_this.isRecommendationsEnabled) {
                _this.switchLabel = _this._t("Enabling...");
                _this.swApiService.enableRecommendations().then(function () {
                    _this.onRecommendationsEnabledChanged();
                }).catch(function (error) {
                    _this.dialogService.showError(_this._t("Enabling recommendations failed: ") + error.statusText);
                    _this.isRecommendationsEnabled = false;
                    _this.setEnabledRecommendationsText();
                });
            }
            else {
                _this.switchLabel = _this._t("Disabling...");
                _this.dialogService.showDisableRecommendationsDialog().then(function () {
                    _this.onRecommendationsEnabledChanged();
                }).catch(function () {
                    _this.isRecommendationsEnabled = true;
                    _this.setEnabledRecommendationsText();
                });
            }
        };
        this.onRecommendationsEnabledChanged = function () {
            _this.setEnabledRecommendationsText();
            // also raise event
            _this.$scope.$broadcast("recommendationsEnabledChanged", _this.isRecommendationsEnabled);
        };
        this.setEnabledRecommendationsText = function () {
            if (_this.isRecommendationsEnabled) {
                _this.statusLabel = _this._t("All features of recommendations are currently enabled.");
                _this.switchLabel = _this._t("Enabled");
            }
            else {
                _this.statusLabel = _this._t("All features of recommendations are currently disabled.");
                _this.switchLabel = _this._t("Disabled");
            }
            _this.isSwitchingInProgress = false;
        };
        this.recomputeRecommendations = function () {
            _this.swApiService.recomputeRecommendations().then(function () {
                _this.xuiToastService.info(_this._t("Recalculating recommendations"), _this._t("Recalculation Status"));
            });
        };
    }
    StrategiesTabController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(2, di_1.Inject(index_1.ServiceNames.ngLog)),
        __param(3, di_1.Inject(index_1.ServiceNames.xuiToastService)),
        __param(4, di_1.Inject(index_1.ServiceNames.statusService)),
        __param(5, di_1.Inject(index_1.ServiceNames.dialogService)),
        __param(6, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(7, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(8, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Function, Object, Object, Object, Object, Object, Object, Object, Function])
    ], StrategiesTabController);
    return StrategiesTabController;
}());
exports.StrategiesTabController = StrategiesTabController;


/***/ }),
/* 152 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content ng-init=vm.init() page-layout=fullWidth> <xui-page-header> <div class=header-container> <div class=title-container> <span class=h1>{{::vm.title}}</span> </div> <div class=menu-container> <div class=link ng-if=::vm.isSettingsPageGranted> <xui-icon icon=gear></xui-icon> <a href={{::vm.routingService.getSettingsHref()}} _t>Recommendations Settings</a> </div> <div class=link> <xui-icon icon=help></xui-icon> <a href={{vm.helpLink}} target=_blank _t>About Recommendations</a> </div> </div> </div> </xui-page-header> <xui-tabs selected-tab-id=vm.selectedTabId xui-tabs-router=\"\"> <xui-tab tab-id=recommendations.active title={{vm.getActiveTabTitle()}}> </xui-tab> <xui-tab tab-id=recommendations.scheduled title={{vm.getScheduledTabTitle()}}> </xui-tab> <xui-tab tab-id=recommendations.running title={{vm.getRunningTabTitle()}}> </xui-tab> <xui-tab tab-id=recommendations.finished title={{::vm.getFinishedTabTitle()}}> </xui-tab> </xui-tabs> </xui-page-content> ";

/***/ }),
/* 153 */
/***/ (function(module, exports) {

module.exports = "<recommendations-scheduled-list></recommendations-scheduled-list> ";

/***/ }),
/* 154 */
/***/ (function(module, exports) {

module.exports = "<recommendations-running-list></recommendations-running-list> ";

/***/ }),
/* 155 */
/***/ (function(module, exports) {

module.exports = "<recommendations-finished-list></recommendations-finished-list> ";

/***/ }),
/* 156 */
/***/ (function(module, exports) {

module.exports = "<xui-page-content ng-init=vm.init() page-layout=fullWidth> <xui-page-header> <div class=header-container> <div class=title-container> <span class=h1>{{::vm.title}}</span> </div> <div class=menu-container> <div class=link> <xui-icon icon=list-all></xui-icon> <a href={{::vm.recommendationsUrl}} _t>Recommendations</a> </div> <div class=link> <xui-icon icon=help></xui-icon> <a href={{vm.helpLink}} target=_blank _t>About Recommendations</a> </div> </div> </div> </xui-page-header> <xui-tabs xui-tabs-router=\"\"> <xui-tab tab-id=strategies title={{::vm.strategiesTabTitle}}> </xui-tab> <xui-tab tab-id=constraints title={{::vm.constraintsTabTitle}}> </xui-tab> </xui-tabs> </xui-page-content> ";

/***/ }),
/* 157 */
/***/ (function(module, exports) {

module.exports = "<re-plugin-placeholder area-name=constraintsExplorer> </re-plugin-placeholder>";

/***/ }),
/* 158 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-recommendations-settings-strategies ng-init=vm.init()> <div class=\"row row-margin\"> <div class=col-xs-12> <h2 _t>General Settings</h2> </div> </div> <div class=row> <div class=col-xs-3 data-selector=strategy-name> <strong _t>Recommendations Engine:</strong> </div> <div class=col-xs-7> <span ng-bind=vm.statusLabel></span> </div> <div class=col-xs-1> <xui-switch class=pull-right name=areRecommendationsDisabled ng-click=vm.showConfirmationDialog() ng-model=vm.isRecommendationsEnabled ng-disabled=vm.isSwitchingInProgress> </xui-switch> </div> <div class=col-xs-1> <strong ng-class=\"{'sw-recommendations-switch-label-enabled': vm.isRecommendationsEnabled}\" ng-bind=vm.switchLabel> </strong> </div> </div> <xui-divider></xui-divider> <div class=\"row row-margin\"> <div class=col-xs-12> <h2 _t>Strategies</h2> </div> </div> <div class=row> <div class=col-xs-12> <xui-message type=warning ng-hide=vm.isHistoryAvailable _t> Recommendations using predictive analysis are not displayed until a sufficient amount of historical data is collected. If your environment is polled regularly, all recommendations should be displayed within 7 days. </xui-message> </div> </div> <div class=\"row row-margin\"> <div class=col-xs-12> <re-plugin-placeholder area-name=strategiesSettings context=vm.pluginSettingsDeferred.promise> </re-plugin-placeholder> </div> </div> <xui-divider></xui-divider> <div class=\"row row-margin\"> <div class=col-xs-12> <h2 _t>Get the Latest Recommendations</h2> </div> <div class=col-xs-12> <p _t>All recommendations are calculated daily. Recalculation will automatically occur when:</p> <ul> <li _t>the environment changes</li> <li _t>a recommendation policy is modified or created</li> <li _t>clicking on the “recalculate now” button</li> </ul> </div> </div> <div class=\"row row-margin\"> <div class=col-xs-12> <xui-button display-style=secondary is-disabled=!vm.isRecommendationsEnabled ng-click=vm.recomputeRecommendations() _t> Recalculate Now </xui-button> </div> </div> </div> ";

/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __webpack_require__(21);
var index_1 = __webpack_require__(0);
var batching_service_1 = __webpack_require__(160);
var dialog_service_1 = __webpack_require__(161);
var cache_service_1 = __webpack_require__(169);
var dateTime_service_1 = __webpack_require__(170);
var string_service_1 = __webpack_require__(171);
var dataGroupStatus_service_1 = __webpack_require__(172);
var pluggability_service_1 = __webpack_require__(173);
var recommendationListParams_service_1 = __webpack_require__(174);
var recommendationListFilters_service_1 = __webpack_require__(175);
var permissions_service_1 = __webpack_require__(176);
var stats_service_1 = __webpack_require__(177);
var status_service_1 = __webpack_require__(178);
var routing_service_1 = __webpack_require__(179);
var swApi_service_1 = __webpack_require__(180);
var swis_service_1 = __webpack_require__(181);
var swProducts_service_1 = __webpack_require__(187);
var orionInfo_service_1 = __webpack_require__(188);
var intervalCaller_service_1 = __webpack_require__(189);
/**
 * Ensures datetime service is correctly initialized
 */
var configureDateTimeService = function (dateTimeService) {
    if (!app_1.isUnderTest()) {
        dateTimeService.loadOrionTimeZoneOffset();
    }
};
exports.default = function (module) {
    module.service(index_1.ServiceNames.batchingService, batching_service_1.BatchingService);
    module.service(index_1.ServiceNames.cacheService, cache_service_1.CacheService);
    module.service(index_1.ServiceNames.dateTimeService, dateTime_service_1.DateTimeService);
    module.service(index_1.ServiceNames.stringService, string_service_1.StringService);
    module.service(index_1.ServiceNames.dataGroupStatusService, dataGroupStatus_service_1.DataGroupStatusService);
    module.service(index_1.ServiceNames.dialogService, dialog_service_1.DialogService);
    module.service(index_1.ServiceNames.intervalCallerService, intervalCaller_service_1.IntervalCallerService);
    module.service(index_1.ServiceNames.orionInfoService, orionInfo_service_1.OrionInfoService);
    module.service(index_1.ServiceNames.pluggabilityService, pluggability_service_1.PluggabilityService);
    module.service(index_1.ServiceNames.recommendationListFiltersService, recommendationListFilters_service_1.RecommendationListFiltersService);
    module.service(index_1.ServiceNames.recommendationListParamsService, recommendationListParams_service_1.RecommendationListParamsService);
    module.service(index_1.ServiceNames.routingService, routing_service_1.RoutingService);
    module.service(index_1.ServiceNames.statsService, stats_service_1.StatsService);
    module.service(index_1.ServiceNames.statusService, status_service_1.StatusService);
    module.service(index_1.ServiceNames.swApiService, swApi_service_1.SwApiService);
    module.service(index_1.ServiceNames.swisService, swis_service_1.SwisService);
    module.service(index_1.ServiceNames.swProductsService, swProducts_service_1.SwProductsService);
    module.service(index_1.ServiceNames.permissionsService, permissions_service_1.PermissionsService);
    module.app().run([index_1.ServiceNames.dateTimeService, configureDateTimeService]);
};


/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var index_1 = __webpack_require__(0);
/**
 * Batching service implementation
 *
 * @class
 * @implements IBatchingService
 */
var BatchingService = /** @class */ (function () {
    function BatchingService($q, dialogService, swApiService) {
        var _this = this;
        this.$q = $q;
        this.dialogService = dialogService;
        this.swApiService = swApiService;
        this.showAndHandleCancelAndRevertConfirmDialog = function (recommendations) {
            return _this.dialogService.showCancelAndRevertConfirmDialog(recommendations)
                .then(function () { return _this.cancelAndRevertRecommendations(recommendations); })
                .catch(angular.noop);
        };
        this.showAndHandleCancelScheduleConfirmDialog = function (recommendations) {
            return _this.dialogService.showCancelSchedulesConfirmDialog(recommendations)
                .then(function () { return _this.cancelSchedules(recommendations); })
                .catch(angular.noop);
        };
        this.cancelAndRevertRecommendations = function (recommendations) {
            // immediately set "Canceling"
            _.each(recommendations, function (r) {
                r.batch.status = enums_1.BatchStatus.Canceling;
            });
            var batchIds = _.map(recommendations, function (r) { return r.batch.id; });
            return _this.swApiService.cancelAndRevertBatches(batchIds);
        };
        this.cancelAndRevertRecommendation = function (r) {
            // immediately set "Canceling"
            r.batch.status = enums_1.BatchStatus.Canceling;
            return _this.swApiService.cancelAndRevertBatch(r.batch.id);
        };
        this.cancelSchedules = function (recommendations) {
            var ids = _.map(recommendations, function (r) { return r.id; });
            return _this.swApiService.unscheduleRecommendations(ids);
        };
    }
    BatchingService.$inject = [
        "$q",
        index_1.ServiceNames.dialogService,
        index_1.ServiceNames.swApiService
    ];
    return BatchingService;
}());
exports.BatchingService = BatchingService;


/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var dialog_service_1 = __webpack_require__(10);
var enums_1 = __webpack_require__(2);
/**
 * Dialog service implementation
 *
 * @class
 * @interface IDialogService
 */
var DialogService = /** @class */ (function () {
    function DialogService($q, xuiDialogService, xuiToastService, routingService, swApiService, swisService, dateTimeService, stringService, _t, swDemoService) {
        var _this = this;
        this.$q = $q;
        this.xuiDialogService = xuiDialogService;
        this.xuiToastService = xuiToastService;
        this.routingService = routingService;
        this.swApiService = swApiService;
        this.swisService = swisService;
        this.dateTimeService = dateTimeService;
        this.stringService = stringService;
        this._t = _t;
        this.swDemoService = swDemoService;
        this.showDetailDialog = function (recommendation, context) {
            if (recommendation.isScheduled()) {
                return _this.showScheduledDetailDialog(recommendation, context);
            }
            if (recommendation.isRunning()) {
                return _this.showRunningDetailDialog(recommendation, context);
            }
            if (recommendation.isFinished()) {
                return _this.showFinishedDetailDialog(recommendation, context);
            }
            return _this.showActiveDetailDialog(recommendation, context);
        };
        this.showActiveDetailDialog = function (recommendation, context) {
            var viewModel = {
                recommendation: recommendation,
                context: context,
                executionType: enums_1.ExecutionType.Now,
                scheduledTime: null,
                getAppliedActionsCallback: null
            };
            return _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(162)
            }, {
                title: recommendation.name,
                hideCancel: true,
                bodyText: "",
                viewModel: viewModel,
                buttons: [
                    {
                        name: "applyButton",
                        isPrimary: true,
                        text: _this._t("Apply this recommendation"),
                        cssClass: "recommendations-apply-button",
                        action: function () { return _this.handleApplyRecommendationButtonClick(viewModel); }
                    }
                ]
            }).then(function (result) {
                return _this.handleActiveDetailDialogResult(viewModel, result);
            });
        };
        this.showActiveMultipleDetailDialog = function (recommendations, context) {
            var viewModel = {
                recommendation: null,
                recommendations: recommendations,
                context: context,
                executionType: enums_1.ExecutionType.Now,
                scheduledTime: null,
                getAppliedActionsCallback: null
            };
            return _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(163)
            }, {
                title: _this._t("Apply Recommendations"),
                hideCancel: false,
                cancelButtonText: _this._t("Cancel"),
                viewModel: viewModel,
                buttons: [
                    {
                        name: "applyButton",
                        isPrimary: true,
                        text: _this._t("Apply Recommendations"),
                        action: function () { return _this.handleApplyRecommendationButtonClick(viewModel); }
                    }
                ]
            }).then(function (result) {
                return _this.handleActiveDetailDialogResult(viewModel, result);
            });
        };
        this.handleActiveDetailDialogResult = function (viewModel, result) {
            if (_this.isCancelButtonResult(result)) {
                return _this.$q.resolve({
                    status: dialog_service_1.ActiveDetailDialogResultStatus.Cancel
                });
            }
            // in case form instance is returned, recommendation(s) will be applied
            if (_this.isFormControllerResult(result)) {
                var isScheduled = viewModel.executionType !== enums_1.ExecutionType.Now;
                var scheduledTime_1 = isScheduled ? _this.dateTimeService.moveUserInputToOrionDateTime(viewModel.scheduledTime) : null;
                var ids_1 = _.map(viewModel.getAppliedActionsCallback(), function (a) { return a.ActionID; });
                return _this.swApiService.applyRecommendation(ids_1, scheduledTime_1).then(function () {
                    return {
                        status: dialog_service_1.ActiveDetailDialogResultStatus.Apply,
                        actionIds: ids_1,
                        scheduledTime: scheduledTime_1
                    };
                });
            }
            if (dialog_service_1.ActiveDetailDialogResultTypeGuard(result) && result.status === dialog_service_1.ActiveDetailDialogResultStatus.CreatedConstraint) {
                return _this.$q.resolve({
                    status: dialog_service_1.ActiveDetailDialogResultStatus.CreatedConstraint
                });
            }
            return _this.$q.resolve({
                status: dialog_service_1.ActiveDetailDialogResultStatus.Cancel
            });
        };
        this.showScheduledDetailDialog = function (recommendation, context) {
            return _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(164)
            }, {
                title: recommendation.name,
                hideCancel: true,
                bodyText: "",
                viewModel: { recommendation: recommendation, context: context },
                buttons: [
                    {
                        name: "cancelButton",
                        isPrimary: true,
                        text: _this._t("Cancel the scheduled action(s)"),
                        action: function () {
                            var deferred = _this.$q.defer();
                            _this.swApiService.unscheduleRecommendations([recommendation.id])
                                .then(function () {
                                _this.routingService.goToActiveRecommendations(null, { location: true });
                                _this.xuiToastService.success(_this._t("Schedule of the recommendation has been canceled and you can see it among current recommendations."));
                                deferred.resolve(true);
                            })
                                .catch(function (error) {
                                if (!_this.swDemoService.isDemoMode()) {
                                    _this.xuiToastService.error(error.statusText, _this._t("Discarding of the scheduled recommendations failed."));
                                }
                                deferred.resolve(false);
                            });
                            return deferred.promise;
                        }
                    }
                ]
            }).then(function (result) {
                if (_this.isCancelButtonResult(result)) {
                    return;
                }
            });
        };
        this.showRunningDetailDialog = function (recommendation, context) {
            var vm = {
                recommendation: recommendation,
                context: context,
                cancelAndRevertCallback: null
            };
            var buttons = [];
            if (recommendation.batch.isRunning()) {
                buttons.push({
                    name: "runningButton",
                    isPrimary: true,
                    text: _this._t("Cancel & Revert"),
                    action: function () {
                        return vm.cancelAndRevertCallback();
                    }
                });
            }
            else {
                // use just "close" button for rec. being canceled
                buttons.push({
                    name: "closeButton",
                    isPrimary: true,
                    text: _this._t("Close"),
                    action: function () { return true; }
                });
            }
            return _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(165)
            }, {
                title: recommendation.name,
                hideCancel: true,
                bodyText: "",
                viewModel: vm,
                buttons: buttons
            }).then(function (result) {
                if (_this.isCancelButtonResult(result)) {
                    return;
                }
            });
        };
        this.showFinishedDetailDialog = function (recommendation, context) {
            return _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(166)
            }, {
                title: recommendation.name,
                hideCancel: true,
                bodyText: "",
                viewModel: { recommendation: recommendation, context: context },
                buttons: [
                    {
                        name: "finishedButton",
                        isPrimary: true,
                        text: _this._t("Close"),
                        action: function () { return true; }
                    }
                ]
            });
        };
        this.showDisableRecommendationsDialog = function () {
            var initPromise = _this.$q.defer();
            _this.xuiDialogService.showModal({
                template: __webpack_require__(167)
            }, {
                title: _this._t("Disable Recommendations"),
                buttons: [
                    {
                        name: "disableRecommendationsButton",
                        isPrimary: true,
                        text: _this._t("Disable Recommendations"),
                        action: function () { return true; }
                    }
                ],
                actionButtonText: _this._t("Disable Recommendations"),
                cancelButtonText: _this._t("Cancel")
            })
                .then(function (result) {
                if (_this.isCancelButtonResult(result)) {
                    initPromise.reject();
                }
                else {
                    _this.swApiService.disableRecommendations()
                        .then(function () {
                        initPromise.resolve();
                    })
                        .catch(function (error) {
                        if (!_this.swDemoService.isDemoMode()) {
                            _this.xuiToastService.error(error.statusText, _this._t("Disabling recommendations failed"));
                        }
                        initPromise.reject();
                    });
                }
            })
                .catch(function () { return initPromise.reject(); });
            return initPromise.promise;
        };
        this.showCancelAndRevertConfirmDialog = function (recommendations) {
            var isMultiple = recommendations.length > 1;
            return _this.xuiDialogService.showModal({
                size: "sm"
            }, {
                hideCancel: false,
                status: "warning",
                actionButtonText: _this._t("Yes, revert"),
                cancelButtonText: _this._t("No"),
                title: isMultiple ? _this._t("Revert {0} Recommendations").format(recommendations.length) : _this._t("Revert Recommendation"),
                message: _this._t("Are you sure that you want to cancel execution of selected chain of recommendations and revert performed actions?")
            })
                .then(function (result) {
                var deferred = _this.$q.defer();
                if (_this.isCancelButtonResult(result)) {
                    deferred.reject();
                }
                else {
                    deferred.resolve();
                }
                return deferred.promise;
            });
        };
        this.showCancelSchedulesConfirmDialog = function (recommendations) {
            var isMultiple = recommendations.length > 1;
            var title = isMultiple ?
                _this.stringService.format(_this._t("Cancel {0} recommendation schedules"), recommendations.length)
                : _this._t("Cancel recommendation schedule");
            return _this.xuiDialogService.showModal({
                size: "m"
            }, {
                hideCancel: false,
                status: "warning",
                actionButtonText: _this._t("Yes, cancel"),
                cancelButtonText: _this._t("No"),
                title: title,
                message: _this._t("Are you sure that you want to cancel schedules of selected chain of recommendations?")
            })
                .then(function (result) {
                var deferred = _this.$q.defer();
                if (_this.isCancelButtonResult(result)) {
                    deferred.reject();
                }
                else {
                    deferred.resolve();
                }
                return deferred.promise;
            });
        };
        this.showMessage = function (body) {
            return _this.xuiDialogService.showMessage({
                message: body
            });
        };
        this.showWarning = function (body) {
            return _this.xuiDialogService.showWarning({
                message: body
            });
        };
        this.showError = function (body) {
            return _this.xuiDialogService.showError({
                message: body
            });
        };
        this.showCreateConstraintDialog = function (recommendation, parentDialog) {
            var viewModel = { recommendation: recommendation, parentDialog: parentDialog };
            return _this.xuiDialogService.showModal({
                size: "lg",
                template: __webpack_require__(168)
            }, {
                title: _this._t("Create Policy"),
                viewModel: viewModel,
                buttons: [
                    {
                        name: "createPolicy",
                        isPrimary: true,
                        text: _this._t("Create Policy"),
                        action: function () { return viewModel.handleCreateConstraintButtonClick(); }
                    }
                ],
                actionButtonText: _this._t("Create Policy"),
                cancelButtonText: _this._t("Cancel")
            })
                .then(function (result) {
                if (_this.isCancelButtonResult(result)) {
                    return dialog_service_1.CreateConstraintDialogResultStatus.Cancel;
                }
                return dialog_service_1.CreateConstraintDialogResultStatus.CreatedConstraint;
            });
        };
        this.handleApplyRecommendationButtonClick = function (vm) {
            // call execution validation
            var actionIds = _.map(vm.getAppliedActionsCallback(), function (a) { return a.ActionID; });
            return _this.swApiService.validateActions(actionIds)
                .then(function (result) {
                if (result.status === enums_1.ValidationResultStatus.Success) {
                    return true;
                }
                var getMessagesFn = function (status) {
                    var actionResults = _.flatten(_.map(result.actionAggregateResults, function (rs) { return rs.actionResults; }));
                    var results = _.filter(actionResults, function (ar) { return ar.status === status; });
                    var messages = _.uniq(_.map(results, function (ar) { return ar.message; }));
                    return messages.join("<br/>");
                };
                if (result.status === enums_1.ValidationResultStatus.Warning) {
                    var warnings = getMessagesFn(enums_1.ValidationResultStatus.Warning);
                    _this.xuiToastService.warning(warnings, _this._t("Recommendation executed with warning"));
                    return true;
                }
                // display msg to user
                var errors = getMessagesFn(enums_1.ValidationResultStatus.Error);
                _this.xuiToastService.error(errors, _this._t("Recommendation cannot be executed"));
                return false;
            });
        };
        /**
         * Indicates whether given dialog result equals to cancel button action
         */
        this.isCancelButtonResult = function (result) {
            if (_.isString(result)) {
                return _this.stringService.equalsIgnoreCase("cancel", result);
            }
            return false;
        };
        /**
         * Indicates whether given result is instance of form controller
         *
         * @description It is especially suitable when dialog button's action has succeeded
         */
        this.isFormControllerResult = function (result) {
            var form = result;
            // note: this property is available only with IFormController
            if (form.$valid) {
                return true;
            }
            return false;
        };
        this.getCancelButton = function () {
            return {
                name: "cancelButton",
                isPrimary: false,
                text: _this._t("Cancel"),
                action: function () { return true; }
            };
        };
    }
    DialogService.$inject = [
        index_1.ServiceNames.ngQService,
        index_1.ServiceNames.xuiDialogService,
        index_1.ServiceNames.xuiToastService,
        index_1.ServiceNames.routingService,
        index_1.ServiceNames.swApiService,
        index_1.ServiceNames.swisService,
        index_1.ServiceNames.dateTimeService,
        index_1.ServiceNames.stringService,
        index_1.ServiceNames.getTextService,
        index_1.ServiceNames.demoService
    ];
    return DialogService;
}());
exports.DialogService = DialogService;
;


/***/ }),
/* 162 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=sw-recommendation-dialog> <div ng-controller=\"ActiveDetailDialogController as ctl\" ng-init=\"ctl.init(vm, true, true)\"> <div class=container-fluid> <div class=\"row recommendation-header\" ng-class=\"{'with-alerts': ctl.resolvesAlerts}\"> <div class=recommendation-info> <recommendation-referrer context=::ctl.context dialog=vm> </recommendation-referrer> <recommendation-problem recommendation=::ctl.initialRecommendation> </recommendation-problem> <xui-divider></xui-divider> <recommendation-description recommendation=::ctl.initialRecommendation> </recommendation-description> </div> <recommendation-alerts recommendations=ctl.recommendations on-load=ctl.onAlertsLoaded(alerts)> </recommendation-alerts> </div> <xui-tabs> <xui-tab tab-id=actions title=\"_t(Steps to Perform)\" _ta> <xui-listview class=action-list items-source=ctl.filteredActions controller=::ctl template-url=action-item-template> </xui-listview> <div class=row> <div class=col-xs-12> <re-plugin-placeholder area-name=recommendationDetailActionsSummary context=::ctl.actionSummaryContextPromise> </re-plugin-placeholder> </div> </div> </xui-tab> <xui-tab tab-id=statistics title=\"_t(Statistics ({{ctl.changedEntitiesCount}}))\" _ta> <recommendation-statistics recommendations=ctl.recommendations> </recommendation-statistics> </xui-tab> </xui-tabs> <xui-divider></xui-divider> <div class=row> <div class=col-xs-12> <recommendation-schedule execution-type=ctl.viewModel.executionType scheduled-time=ctl.viewModel.scheduledTime> </recommendation-schedule> </div> </div> </div> </div> <xui-dialog-footer> <recommendation-detail-dialog-footer-contents enable-create-policy=true parent-ctrl=vm /> </xui-dialog-footer> </xui-dialog> <script type=text/ng-template id=action-item-template> <div class=\"row vertical-center\">\n        <div class=\"col-xs-6\">\n            <div class=\"media\">\n                <div class=\"media-left\">\n                    <xui-icon icon=\"{{vm.getActionIcon(item)}}\" icon-size=\"small\"></xui-icon>\n                </div>\n                <div class=\"link media-left\"\n                     title=\"_t(View details)\"\n                     ng-if=\"::item.isPrerequisite\"\n                     ng-click=\"::vm.openPrerequisiteDetail(item.RecommendationID)\"\n                     _ta>\n                    {{$index + 1}}. {{::item.Name}}\n                </div>\n                <div class=\"media-left\" ng-if=\"::!item.isPrerequisite\">\n                    {{$index + 1}}. {{::item.Name}}\n                </div>\n                <div class=\"media-right media-middle\" ng-if=\"::item.isPrerequisite\">\n                    <dependency-details context=\"::item.dependencyDetailsContext\">\n                    </dependency-details>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-xs-6 text-right\">\n            <re-plugin-placeholder area-name=\"recommendationDetailActionItem\"\n                                   context=\"::{item: item, actions: vm.actions, recommendations: vm.recommendations}\">\n            </re-plugin-placeholder>\n        </div>\n    </div> </script> ";

/***/ }),
/* 163 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=\"sw-recommendation-dialog sw-active-multiple-recommendation-dialog\"> <div ng-controller=\"ActiveMultipleDetailDialogController as ctl\" ng-init=\"ctl.init(vm, true, true)\"> <div class=container-fluid> <p _t>Recommendations will be executed in this order:</p> <div ng-if=!ctl.isLoaded> <div> <xui-spinner show=true size=20 class=spinner> </xui-spinner> <span _t>Loading recommendations...</span> </div> </div> <xui-listview ng-if=ctl.isLoaded class=recommendation-list items-source=ctl.recommendations controller=::ctl template-url=recommendation-item-template> </xui-listview> <div class=row> <div class=col-xs-12> <re-plugin-placeholder area-name=recommendationDetailActionsSummary context=::ctl.actionSummaryContextPromise> </re-plugin-placeholder> </div> </div> <xui-divider></xui-divider> <div class=row> <div class=col-xs-12> <recommendation-schedule execution-type=ctl.viewModel.executionType scheduled-time=ctl.viewModel.scheduledTime> </recommendation-schedule> </div> </div> </div> </div> </xui-dialog> <script type=text/ng-template id=recommendation-item-template> <xui-expander is-open=\"false\">\n\n        <xui-expander-heading>\n            <strong>{{::item.name}}</strong>\n        </xui-expander-heading>\n\n        <xui-listview class=\"action-list\"\n                      items-source=\"vm.filteredActionsPerRecommendation[item.id]\"\n                      controller=\"::vm\"\n                      template-url=\"action-item-template\"\n                      stripe=\"false\">\n        </xui-listview>\n\n    </xui-expander> </script> <script type=text/ng-template id=action-item-template> <div class=\"row vertical-center\">\n        <div class=\"col-xs-6\">\n            <div class=\"media\">\n                <div class=\"media-left\">\n                    <xui-icon icon=\"{{vm.getActionIcon(item)}}\" icon-size=\"small\"></xui-icon>\n                </div>\n                <div class=\"link media-left\"\n                     title=\"_t(View details)\"\n                     ng-if=\"::item.isPrerequisite\"\n                     ng-click=\"::vm.openPrerequisiteDetail(item.RecommendationID)\"\n                     _ta>\n                    {{$index + 1}}. {{::item.Name}}\n                </div>\n                <div class=\"media-left\" ng-if=\"::!item.isPrerequisite\">\n                    {{$index + 1}}. {{::item.Name}}\n                </div>\n                <div class=\"media-right media-middle\" ng-if=\"::item.isPrerequisite\">\n                    <dependency-details context=\"::item.dependencyDetailsContext\">\n                    </dependency-details>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-xs-6 text-right\">\n            <re-plugin-placeholder area-name=\"recommendationDetailActionItem\"\n                                   context=\"::{item: item, actions: vm.actions, recommendations: vm.recommendations}\">\n            </re-plugin-placeholder>\n        </div>\n    </div> </script>";

/***/ }),
/* 164 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=sw-recommendation-dialog> <div ng-controller=\"ScheduledDetailDialogController as detailController\" ng-init=\"detailController.init(vm, false, false)\"> <div class=container-fluid> <recommendation-referrer context=::detailController.context dialog=vm> </recommendation-referrer> <recommendation-problem recommendation=::detailController.initialRecommendation> </recommendation-problem> <xui-divider></xui-divider> <recommendation-description recommendation=::detailController.initialRecommendation> </recommendation-description> <xui-tabs> <xui-tab tab-id=actions title=\"_t(Steps to Perform)\" _ta> <xui-listview class=action-list items-source=detailController.filteredActions controller=::detailController template-url=action-item-template> </xui-listview> <div class=row> <div class=col-xs-12> <re-plugin-placeholder area-name=recommendationDetailActionsSummary context=::detailController.actionSummaryContextPromise> </re-plugin-placeholder> </div> </div> <input type=text ng-model=detailController.actions name=actions style=display:none /> </xui-tab> <xui-tab tab-id=statistics title=\"_t(Statistics ({{detailController.changedEntitiesCount}}))\" _ta> <recommendation-statistics recommendations=detailController.recommendations> </recommendation-statistics> </xui-tab> </xui-tabs> </div> </div> <xui-dialog-footer> <recommendation-detail-dialog-footer-contents enable-create-policy=false parent-ctrl=vm /> </xui-dialog-footer> </xui-dialog> <script type=text/ng-template id=action-item-template> <div class=\"row vertical-center\">\n        <div class=\"col-xs-12\">\n            <div class=\"media\">\n                <div class=\"media-left\">\n                    <xui-icon icon=\"{{vm.getActionIcon(item)}}\" icon-size=\"small\"></xui-icon>\n                </div>\n                <div class=\"media-body\">\n                    {{::$index + 1}}. {{::item.Name}}\n                </div>\n                <div class=\"media-right media-middle\"\n                     ng-if=\"::item.isPrerequisite\">\n\n                    <dependency-details context=\"::item.dependencyDetailsContext\">\n                    </dependency-details>\n                </div>\n            </div>\n        </div>\n    </div> </script> ";

/***/ }),
/* 165 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=sw-recommendation-dialog> <div ng-controller=\"RunningDetailDialogController as detailController\" ng-init=detailController.init(vm)> <div class=container-fluid> <recommendation-referrer context=::detailController.context dialog=vm> </recommendation-referrer> <recommendation-status recommendation=::detailController.recommendation> </recommendation-status> <xui-divider></xui-divider> <recommendation-problem recommendation=::detailController.recommendation> </recommendation-problem> <xui-divider></xui-divider> <recommendation-description ng-if=detailController.isRunning() recommendation=::detailController.recommendation> </recommendation-description> <xui-tabs> <xui-tab tab-id=actions title=\"_t(Performed Actions)\" _ta> <xui-listview class=action-list items-source=detailController.recommendation.actions controller=::detailController template-url=action-item-template> </xui-listview> </xui-tab> <xui-tab tab-id=statistics title=_t(Statistics) _ta> <recommendation-statistics recommendations=::[detailController.recommendation]> </recommendation-statistics> </xui-tab> </xui-tabs> </div> </div> </xui-dialog> <script type=text/ng-template id=action-item-template> <running-action-list-item action=\"::item\"></running-action-list-item> </script> ";

/***/ }),
/* 166 */
/***/ (function(module, exports) {

module.exports = "<xui-dialog class=sw-recommendation-dialog> <div ng-controller=\"FinishedDetailDialogController as detailController\" ng-init=\"detailController.init(vm, true, false)\"> <div class=container-fluid> <recommendation-referrer context=::detailController.context dialog=vm> </recommendation-referrer> <recommendation-status recommendation=::detailController.initialRecommendation actions=detailController.actions> </recommendation-status> <xui-divider></xui-divider> <recommendation-problem resolved=true recommendation=::detailController.initialRecommendation> </recommendation-problem> <xui-tabs> <xui-tab tab-id=actions title=\"_t(Performed Actions)\" _ta> <xui-listview class=action-list items-source=detailController.filteredActions controller=::detailController template-url=action-item-template empty-text=\"_t(No actions were performed.)\" _ta> </xui-listview> </xui-tab> <xui-tab tab-id=statistics title=\"_t(Outcome ({{detailController.changedEntitiesCount}}))\" _ta> <recommendation-statistics recommendations=::detailController.recommendations> </recommendation-statistics> </xui-tab> </xui-tabs> </div> </div> <xui-dialog-footer> <recommendation-detail-dialog-footer-contents enable-create-policy=false parent-ctrl=vm /> </xui-dialog-footer> </xui-dialog> <script type=text/ng-template id=action-item-template> <div class=\"row vertical-center\">\n        <div class=\"col-xs-6\">\n            <div class=\"media\">\n                <div class=\"media-left\">\n                    <xui-icon icon=\"{{vm.getActionIcon(item)}}\" icon-size=\"small\"></xui-icon>\n                </div>\n                <div class=\"link media-left\"\n                     title=\"_t(View details)\"\n                     ng-if=\"::item.isPrerequisite\"\n                     ng-click=\"::vm.openPrerequisiteDetail(item.RecommendationID)\"\n                     _ta>\n                    {{$index + 1}}. {{::item.Name}}\n                </div>\n                <div class=\"media-left\" ng-if=\"::!item.isPrerequisite\">\n                    {{$index + 1}}. {{::item.Name}}\n                </div>\n                <div class=\"media-right media-middle\" ng-if=\"::item.isPrerequisite\">\n                    <dependency-details context=\"::item.dependencyDetailsContext\">\n                    </dependency-details>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-xs-6\">\n            <div ng-if=\"::item.Status === vm.actionStatus.FinishedSuccessfully\">\n                <xui-icon icon=\"severity_ok\"></xui-icon>\n                <span class=\"dots successfull\" _t>\n                    Successfully finished on {{item.DateFinished | date:\"short\"}},\n                    Duration: {{::vm.computeActionDuration(item)}}.\n                </span>\n            </div>\n            <div ng-if=\"::item.Status === vm.actionStatus.FinishedWithError\">\n                <xui-icon icon=\"severity_error\"></xui-icon>\n                <span class=\"dots error\" _t>\n                    Executed with error: {{::item.FailureMessage}}\n                </span>\n            </div>\n            <div ng-if=\"::item.Status === vm.actionStatus.Canceled\">\n                <xui-icon icon=\"severity_warning\"></xui-icon>\n                <span class=\"dots warning\" _t>\n                    Action was canceled\n                </span>\n            </div>\n        </div>\n    </div> </script> <script type=text/ng-template id=revert-action-item-template> <div class=\"row vertical-center\">\n        <div class=\"col-xs-6\">\n            <div class=\"media\">\n                <div class=\"media-left\">\n                    <xui-icon icon=\"{{vm.getActionIcon(item)}}\" icon-size=\"small\"></xui-icon>\n                </div>\n                <div class=\"media-body\">\n                    {{vm.revertBatchActions.length + $index + 1}}. {{::item.description}}\n                </div>\n            </div>\n        </div>\n        <div class=\"col-xs-6\">\n            <div ng-if=\"::item.status === vm.actionStatus.FinishedSuccessfully\">\n                <xui-icon icon=\"severity_ok\"></xui-icon>\n                <span class=\"dots successfull\" _t>\n                    Successfully reverted on {{item.dateFinished | date:\"short\"}},\n                    Duration: {{::vm.computeActionDuration(item)}}.\n                </span>\n            </div>\n            <div ng-if=\"::item.status === vm.actionStatus.FinishedWithError\">\n                <xui-icon icon=\"severity_error\"></xui-icon>\n                <span class=\"dots error\" _t>\n                    Reverting action failed: {{::item.failureMessage}}\n                </span>\n            </div>\n        </div>\n    </div> </script> ";

/***/ }),
/* 167 */
/***/ (function(module, exports) {

module.exports = "﻿<xui-dialog> <div class=container-fluid> <div class=row> <div class=col-xs-2> <xui-icon icon=severity_warning icon-size=large></xui-icon> </div> <div class=col-xs-10> <p _t>Are you sure you want to disable the recommendations?</p> <p _t>When this feature is disabled new recommendations are no longer provided and all scheduled recommendations won't be executed.</p> </div> </div> </div> </xui-dialog>";

/***/ }),
/* 168 */
/***/ (function(module, exports) {

module.exports = "﻿<xui-dialog> <div ng-controller=\"CreateConstraintDialogController as ctrl\" ng-init=ctrl.initByViewModel(vm)> <div class=\"row create-constraint-dialog\"> <div class=col-xs-12> <p _t=\"['{{::ctrl.routingService.getConstraintsHref()}}']\">Please select policies you want to create for selected recommendations. You can manage created and existing recommendation policies through the <a href={0}>Recommendation policies page</a>.</p> <div class=\"panel panel-primary well\"> <div class=\"media visible\"> <div class=media-left> <xui-icon icon={{::ctrl.getActionIcon()}} icon-size=small></xui-icon> </div> <div class=\"media-body visible\"> <div class=description ng-bind-html=::ctrl.viewModel.recommendation.description> </div> <p>Suggested Recommendation Policies:</p> <re-plugin-placeholder area-name=constraintsPicker context=::ctrl.constraintsPickerContext></re-plugin-placeholder> <expiration-picker class=expiration expiration-date=ctrl.expirationDate></expiration-picker> </div> </div> </div> <xui-message type=warning> <strong _t>All Recommendations will be recalculated!</strong> <p _t=\"['{{ctrl.constraintsHelpLink}}']\">Selected recommendations will be excluded from the list of active recommendations. Because of the policy can affect other recommendations the process of creating policies requires to recalculate all active and scheduled recommendations. Created policies can be modified or removed later. <a href={0} target=_blank>» Learn More</a></p> </xui-message> </div> </div> </div> </xui-dialog> ";

/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Cache service implementation
 *
 * @class
 * @implements ICacheService
 */
var CacheService = /** @class */ (function () {
    function CacheService($q) {
        var _this = this;
        this.$q = $q;
        /**
         * Loaded items collection.
         */
        this.items = {};
        /**
         * Promises of items currently being loaded to provide simple sync functionality.
         */
        this.itemsPromises = {};
        this.getOrAdd = function (key, timeout, getData) {
            var deferred = _this.$q.defer();
            // if currently being loaded, just hand over the promise
            if (_this.itemsPromises[key]) {
                _this.itemsPromises[key].then(function (loadedItem) {
                    deferred.resolve(loadedItem);
                });
                return deferred.promise;
            }
            var item = _this.items[key];
            if (item && !_this.isItemExpired(item)) {
                deferred.resolve(item);
                return deferred.promise;
            }
            // call fn to get data
            var promise = getData()
                .then(function (data) {
                var loadDate = new Date();
                var timeoutDate = new Date(loadDate.valueOf() + (timeout * 1000));
                if (item) {
                    var previousValue = item.value;
                    item.value = data;
                    item.previousValue = previousValue;
                    item.loadDate = loadDate;
                    item.timeoutDate = timeoutDate;
                }
                else {
                    item = {
                        key: key,
                        value: data,
                        loadDate: loadDate,
                        timeoutDate: timeoutDate
                    };
                    _this.items[key] = item;
                }
                _this.itemsPromises[key] = null;
                deferred.resolve(item);
                return item;
            })
                .catch(function (reason) {
                _this.itemsPromises[key] = null;
                deferred.reject(reason);
            });
            // register promise
            _this.itemsPromises[key] = promise;
            return deferred.promise;
        };
        this.isItemExpired = function (item) {
            return item.timeoutDate.valueOf() <= new Date().valueOf();
        };
    }
    CacheService.$inject = ["$q"];
    return CacheService;
}());
exports.CacheService = CacheService;


/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var di_1 = __webpack_require__(1);
/**
 * Datetime service implementation
 *
 * @class
 * @implements IDateTimeService
 */
var DateTimeService = /** @class */ (function () {
    function DateTimeService(dateFilter, swis, _t) {
        var _this = this;
        this.dateFilter = dateFilter;
        this.swis = swis;
        this._t = _t;
        this.localTimezoneOffsetMinutes = moment().utcOffset();
        this.minutesPerHour = moment.duration({ hours: 1 }).asMinutes();
        this.minutesPerDay = moment.duration({ days: 1 }).asMinutes();
        this.loadOrionTimeZoneOffset = function () {
            var query = "SELECT TOP 1 MINUTEDIFF(GETUTCDATE(), GETDATE()) AS Offset FROM Orion.Engines";
            return _this.swis.query(query).then(function (results) {
                _this.orionTimeZoneOffsetMinutes = results.rows[0].Offset;
            });
        };
        this.getLocalTimezoneOffsetMinutes = function () {
            return _this.localTimezoneOffsetMinutes;
        };
        this.getOrionTimezoneOffsetMinutes = function () {
            return _this.orionTimeZoneOffsetMinutes;
        };
        this.getOrionTimezoneOffsetFormatted = function () {
            var dur = moment.duration({ minutes: _this.orionTimeZoneOffsetMinutes });
            var minutes = Math.abs(dur.minutes());
            return dur.hours() + ":" + (minutes < 10 ? "0" + minutes : minutes);
        };
        this.getNowMiliseconds = function () {
            return moment().valueOf();
        };
        this.getOrionNowMiliseconds = function () {
            var orionOffset = _this.getOrionTimezoneOffsetMinutes();
            return moment().add(orionOffset, "minutes").valueOf();
        };
        this.getOrionNowDate = function () {
            // note: our desire is to make the result look like it is Orion datetime but with local timezone
            return moment()
                .add(-_this.localTimezoneOffsetMinutes, "minutes")
                .add(_this.orionTimeZoneOffsetMinutes, "minutes")
                .toDate();
        };
        this.getOrionNowMoment = function () {
            return moment().utcOffset(_this.orionTimeZoneOffsetMinutes);
        };
        this.convertStringToUtc = function (d) {
            return moment(d).toDate();
        };
        this.convertSwisMsDateStringToDate = function (msDate) {
            if (!msDate) {
                return null;
            }
            var timeStampString = msDate.match(/\d+/)[0];
            if (!timeStampString) {
                return null;
            }
            var timeStampNumber = parseInt(timeStampString, 10);
            // steps: 
            //      1) neutralize local shift (to make it look correctly since Date object always shifts time with local timezone)
            //      2) add orion timezone shift twice to shift it to UTC and then to Orion timezone since it shifted 
            //         twice by Orion timezone when returning from swis
            return moment(timeStampNumber)
                .add(-_this.localTimezoneOffsetMinutes, "minutes")
                .add(2 * _this.orionTimeZoneOffsetMinutes, "minutes")
                .toDate();
        };
        this.convertSwApiIsoStringToDate = function (isoString) {
            if (!isoString) {
                return null;
            }
            // steps: 
            //      1) neutralize local shift (so it looks correct when displayed later)
            //      2) add orion timezone shift
            return moment(isoString)
                .add(-_this.localTimezoneOffsetMinutes, "minutes")
                .add(_this.orionTimeZoneOffsetMinutes, "minutes")
                .toDate();
        };
        this.moveUserInputToOrionDateTime = function (d) {
            var input = moment(d);
            // add local offset
            input = input.add(_this.localTimezoneOffsetMinutes, "minutes");
            // subtract Orion timezone so later moving to that zone makes it look like the desired datetime
            input = input.add(-_this.orionTimeZoneOffsetMinutes, "minutes");
            // move to Orion timezone so the final datetime looks correct
            input = input.utcOffset(_this.orionTimeZoneOffsetMinutes);
            return input.toISOString();
        };
        this.moveUserInputToOrionDateEndOfTheDay = function (d) {
            if (!d) {
                return null;
            }
            var input = moment(d);
            input.set({ hour: 23, minute: 59, second: 0, millisecond: 0 });
            // move to Orion timezone so the final datetime looks correct
            input = input.utcOffset(_this.orionTimeZoneOffsetMinutes);
            return input.toISOString();
        };
        this.formatMinutesDuration = function (minutes, options) {
            if (!minutes || !_.isNumber(minutes)) {
                return _this.formatDuration({ minutes: 0 }, options);
            }
            if (minutes >= _this.minutesPerDay) {
                var days = Math.floor(minutes / _this.minutesPerDay);
                var hours = Math.floor((minutes - (days * _this.minutesPerDay)) / _this.minutesPerHour);
                var mins = minutes % (days * _this.minutesPerDay + hours * _this.minutesPerHour);
                return _this.formatDuration({ days: days, hours: hours, minutes: mins }, options);
            }
            if (minutes >= _this.minutesPerHour) {
                var hours = Math.floor(minutes / _this.minutesPerHour);
                return _this.formatDuration({ hours: hours, minutes: minutes % _this.minutesPerHour }, options);
            }
            return _this.formatDuration({ minutes: minutes }, options);
        };
        this.formatDuration = function (duration, options) {
            options = options || { useLongNames: false };
            var format = "";
            var params = [];
            var daysPresent = !!duration.days && duration.days > 0, hoursPresent = !!duration.hours && duration.hours > 0, minutesPresent = !!duration.minutes && duration.minutes > 0, secondsPresent = !!duration.seconds && duration.seconds > 0;
            if (daysPresent) {
                format += options.useLongNames ? " {" + params.length + "} days" : " {" + params.length + "}d";
                params.push(duration.days);
            }
            if (hoursPresent || (daysPresent && (minutesPresent || secondsPresent))) {
                format += options.useLongNames ? " {" + params.length + "} hours" : " {" + params.length + "}h";
                params.push(duration.hours);
            }
            if (minutesPresent || ((daysPresent || hoursPresent) && secondsPresent)) {
                format += options.useLongNames ? " {" + params.length + "} min" : " {" + params.length + "}m";
                params.push(duration.minutes);
            }
            if (secondsPresent) {
                format += options.useLongNames ? " {" + params.length + "} sec" : " {" + params.length + "}s";
                params.push(duration.seconds);
            }
            return String.format(format, params);
        };
        this.formatShortDate = function (date) {
            return _this.dateFilter(date, "short");
        };
        this.formatDateOnly = function (date) {
            return _this.dateFilter(date, "shortDate");
        };
    }
    DateTimeService = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngDateFilter)),
        __param(1, di_1.Inject(index_1.ServiceNames.apolloSwisService)),
        __param(2, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Function, Object, Function])
    ], DateTimeService);
    return DateTimeService;
}());
exports.DateTimeService = DateTimeService;
;


/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Implementation of helper string service
 */
var StringService = /** @class */ (function () {
    function StringService() {
        var _this = this;
        this.format = function (template) {
            var variables = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                variables[_i - 1] = arguments[_i];
            }
            return template.format(variables);
        };
        this.equalsIgnoreCase = function (s1, s2) {
            if (s1 === null || typeof s1 === "undefined") {
                return false;
            }
            if (s2 === null || typeof s2 === "undefined") {
                return false;
            }
            var regex = new RegExp(s1, "i");
            return regex.test(s2);
        };
        this.isTrueStringIgnoreCase = function (s) {
            return _this.equalsIgnoreCase(s, "true");
        };
        this.stripHtml = function (s) {
            return $("<div/>").html(s).text();
        };
    }
    return StringService;
}());
exports.StringService = StringService;
;


/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
exports.swApiUrl = "recommendations/pluggability/dataGroupStates";
exports.cacheKey = "dataGroupStates";
/**
 * Data group status service
 *
 * @class
 * @implements IDataGroupStatusService
 */
var DataGroupStatusService = /** @class */ (function () {
    function DataGroupStatusService(swApi, cacheService) {
        var _this = this;
        this.swApi = swApi;
        this.cacheService = cacheService;
        this.getStates = function () {
            return _this.cacheService.getOrAdd(exports.cacheKey, 20, _this.callApi)
                .then(function (cacheItem) {
                return cacheItem.value;
            });
        };
        this.getStatusById = function (identifier) {
            return _this.getStates()
                .then(function (results) {
                // try to find by identifier
                var found = _.find(results, function (s) { return _.eq(s.identifier, identifier); });
                if (found) {
                    return found;
                }
                return null;
            });
        };
        this.callApi = function () {
            return _this.swApi
                .api(false)
                .one(exports.swApiUrl)
                .get();
        };
    }
    DataGroupStatusService.$inject = [
        "swApi",
        index_1.ServiceNames.cacheService
    ];
    return DataGroupStatusService;
}());
exports.DataGroupStatusService = DataGroupStatusService;


/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Pluggability service
 *
 * @class
 * @implements IPluggabilityService
 */
var PluggabilityService = /** @class */ (function () {
    function PluggabilityService($injector, $q, swApiService, stringService) {
        var _this = this;
        this.$injector = $injector;
        this.$q = $q;
        this.swApiService = swApiService;
        this.stringService = stringService;
        this.ensureIsInitialized = function () {
            return _this.configurationPromise.then(function () { });
        };
        this.getTemplatesForArea = function (areaName) {
            return _this.configurationPromise.then(function (cfg) { return cfg.templates[areaName]; });
        };
        this.getActionIconDelegate = function () {
            return _this.configurationPromise.then(function (cfg) {
                var actionIcons = cfg.actionIcons;
                return function (iconName) { return _this.getActionIconInternal(iconName, actionIcons); };
            });
        };
        this.getModuleServices = function (serviceName) {
            return _this.configurationPromise.then(function (cfg) {
                var pluginServiceNames = cfg.moduleServices[serviceName];
                if (!pluginServiceNames || _.isEmpty(pluginServiceNames)) {
                    return [];
                }
                return _.map(pluginServiceNames, function (pluginServiceName) { return _this.$injector.get(pluginServiceName); });
            });
        };
        /**
         * Finds the given icon in the specified dictionary.
         */
        this.getActionIconInternal = function (iconName, allIcons) {
            var icon = allIcons[iconName];
            if (!!icon) {
                return icon;
            }
            // try to find icon with another letter case
            for (var key in allIcons) {
                if (_this.stringService.equalsIgnoreCase(key, iconName)) {
                    return allIcons[key];
                }
            }
            return null;
        };
    }
    Object.defineProperty(PluggabilityService.prototype, "configurationPromise", {
        get: function () {
            return this._configurationPromise = this._configurationPromise || this.swApiService.getPluggabilityConfiguration();
        },
        enumerable: true,
        configurable: true
    });
    ;
    PluggabilityService = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngInjectorService)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(2, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(3, di_1.Inject(index_1.ServiceNames.stringService)),
        __metadata("design:paramtypes", [Object, Function, Object, Object])
    ], PluggabilityService);
    return PluggabilityService;
}());
exports.PluggabilityService = PluggabilityService;


/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Names of used params
 */
var RecommendationListParamNames = /** @class */ (function () {
    function RecommendationListParamNames() {
    }
    RecommendationListParamNames.groupId = "groupId";
    return RecommendationListParamNames;
}());
exports.RecommendationListParamNames = RecommendationListParamNames;
/**
 * Service reading recommendation list params
 *
 * @class
 * @implements IRecommendationListParamsService
 */
var RecommendationListParamsService = /** @class */ (function () {
    function RecommendationListParamsService($state, $log, routingService) {
        var _this = this;
        this.$state = $state;
        this.$log = $log;
        this.routingService = routingService;
        this.getParams = function () {
            var groupId = _this.$state.params[RecommendationListParamNames.groupId];
            // make it lowercase to make it conformant with apollo which is returning lowercase guid
            if (groupId) {
                groupId = groupId.toLowerCase();
            }
            return {
                groupId: groupId
            };
        };
        this.initFilterValues = function (filterValues) {
            var params = _this.getParams();
            if (!!params.groupId) {
                filterValues.entity.push(params.groupId);
            }
        };
        this.updateStateParams = function (filterValues) {
            var params = _this.getParams();
            // if data group is not present in state, there is no need to sync
            if (!params.groupId) {
                return;
            }
            // if data group filter remains, do not change anything
            if (filterValues.entity && filterValues.entity.length === 1
                && filterValues.entity[0].toLowerCase() === params.groupId) {
                return;
            }
            params[RecommendationListParamNames.groupId] = undefined;
            _this.routingService.reload(params, { notify: true, location: true, reload: false });
        };
    }
    RecommendationListParamsService = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngState)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngLog)),
        __param(2, di_1.Inject(index_1.ServiceNames.routingService)),
        __metadata("design:paramtypes", [Object, Object, Object])
    ], RecommendationListParamsService);
    return RecommendationListParamsService;
}());
exports.RecommendationListParamsService = RecommendationListParamsService;


/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var recommendationListFilterValues_1 = __webpack_require__(17);
/**
 * Service implementation for storing recommendation list filters.
 *
 * @implements IRecommendationListFiltersService
 */
var RecommendationListFiltersService = /** @class */ (function () {
    function RecommendationListFiltersService() {
        this.activeRecommendationsFilterValues = new recommendationListFilterValues_1.RecommendationListFilterValues();
        this.scheduledRecommendationsFilterValues = new recommendationListFilterValues_1.RecommendationListFilterValues();
        this.finishedRecommendationsFilterValues = new recommendationListFilterValues_1.RecommendationListFilterValues();
    }
    RecommendationListFiltersService.$inject = [];
    return RecommendationListFiltersService;
}());
exports.RecommendationListFiltersService = RecommendationListFiltersService;


/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var base_swApi_service_1 = __webpack_require__(9);
var constants_1 = __webpack_require__(8);
/**
 * Available permissions related api methods
 *
 * @enum
 */
var Methods;
(function (Methods) {
    Methods[Methods["HasUserPermissions"] = 0] = "HasUserPermissions";
})(Methods = exports.Methods || (exports.Methods = {}));
;
/**
 * Permissions service implementation
 *
 * @class
 * @implements IPermissionsService
 */
var PermissionsService = /** @class */ (function (_super) {
    __extends(PermissionsService, _super);
    function PermissionsService($q, $rootScope, swApi, constants, cacheService) {
        var _this = _super.call(this, swApi) || this;
        _this.$q = $q;
        _this.$rootScope = $rootScope;
        _this.constants = constants;
        _this.cacheService = cacheService;
        _this.getControllerUrl = function () {
            return "recommendations";
        };
        _this.isAdminAllowed = function () {
            return _this.constants.user.AllowAdmin;
        };
        _this.hasUserPermissions = function (authProfile) {
            var expirationSeconds = 5 * 60;
            var key = "has-user-permissions-" + authProfile.roles + authProfile.permissions;
            return _this.cacheService.getOrAdd(key, expirationSeconds, function () { return _this.processAuthProfile(authProfile); })
                .then(function (cacheItem) {
                return cacheItem.value;
            });
        };
        _this.processAuthProfile = function (authProfile) {
            var rolesToCheck = authProfile.roles;
            if (_.includes(authProfile.roles, constants_1.Role.demo)) {
                rolesToCheck = authProfile.roles.filter(function (item) {
                    return item !== constants_1.Role.demo;
                });
                if (_this.$rootScope["demoMode"]) {
                    return _this.$q.resolve(true);
                }
            }
            return _this.callGetMethod(Methods[Methods.HasUserPermissions], { Roles: rolesToCheck, Permissions: authProfile.permissions });
        };
        return _this;
    }
    PermissionsService.$inject = [
        index_1.ServiceNames.ngQService,
        index_1.ServiceNames.ngRootScopeService,
        index_1.ServiceNames.apolloSwApiService,
        index_1.ServiceNames.apolloConstants,
        index_1.ServiceNames.cacheService
    ];
    return PermissionsService;
}(base_swApi_service_1.BaseSwApiService));
exports.PermissionsService = PermissionsService;


/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var eventNames_1 = __webpack_require__(11);
var index_1 = __webpack_require__(0);
/**
 * Stats service
 *
 * @class
 * @implements IStatsService
 */
var StatsService = /** @class */ (function () {
    function StatsService($rootScope, $log, swisService, cacheService) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$log = $log;
        this.swisService = swisService;
        this.cacheService = cacheService;
        this.activeRecommendationsCount = null;
        this.scheduledRecommendationsCount = null;
        this.runningRecommendationsCount = null;
        // Indicates whether the count is taken from the view controller or DB
        this.runningControllerMode = false;
        this.getActiveRecommendationsCount = function () {
            return _this.activeRecommendationsCount;
        };
        this.setActiveRecommendationsCount = function (value) {
            _this.activeRecommendationsCount = value || 0;
        };
        this.getScheduledRecommendationsCount = function () {
            return _this.scheduledRecommendationsCount;
        };
        this.setScheduledRecommendationsCount = function (value) {
            _this.scheduledRecommendationsCount = value || 0;
        };
        this.getRunningRecommendationsCount = function () {
            return _this.runningRecommendationsCount;
        };
        this.setRunningRecommendationsCount = function (value) {
            _this.runningRecommendationsCount = value || 0;
        };
        this.getRunningRecommendationsControllerMode = function () {
            return _this.runningControllerMode;
        };
        this.setRunningRecommendationsControllerMode = function (value) {
            _this.runningControllerMode = value;
        };
        /*
        * Get total number of recommendations for Active and Scheduled tab.
        * Running tab has special counter which could be taken from the DB or from the number of lines displayed (running controller).
        * Finished tab doesn't have a counter at all.
        */
        this.getTotalRecommendationsCount = function () {
            _this.swisService.getTotalRecommendationsCount().then(function (results) {
                _.forEach(results, function (count) {
                    switch (count.Status) {
                        case "Active":
                            _this.setActiveRecommendationsCount(count.Cnt);
                            break;
                        case "Scheduled":
                            _this.setScheduledRecommendationsCount(count.Cnt);
                            break;
                        case "Running":
                            /*
                             * In controller mode it accepts the value only from controller
                             * In non controller mode it accepts only non controller value
                             */
                            if (!_this.runningControllerMode) {
                                _this.setRunningRecommendationsCount(count.Cnt);
                            }
                            break;
                    }
                });
            });
        };
        this.hasAnyGroupData = function () {
            var expirationSeconds = 60;
            var key = "is-sample-mode-enabled";
            return _this.cacheService.getOrAdd(key, expirationSeconds, function () { return _this.swisService.hasAnyGroupData(); })
                .then(function (cacheItem) {
                return cacheItem.value;
            });
        };
        $rootScope.$on(eventNames_1.EventNames.applyRecommendation, this.getTotalRecommendationsCount);
        $rootScope.$on(eventNames_1.EventNames.unscheduleRecommendations, this.getTotalRecommendationsCount);
    }
    StatsService.$inject = [
        "$rootScope",
        "$log",
        index_1.ServiceNames.swisService,
        index_1.ServiceNames.cacheService
    ];
    return StatsService;
}());
exports.StatsService = StatsService;


/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
/**
 * Status service for recommendations
 *
 * @implements IStatusService
 */
var StatusService = /** @class */ (function () {
    function StatusService($interval, _t, xuiToastService, swisService, swApiService) {
        var _this = this;
        this.$interval = $interval;
        this._t = _t;
        this.xuiToastService = xuiToastService;
        this.swisService = swisService;
        this.swApiService = swApiService;
        this.toastOptions = { timeOut: 5000 };
        this.recommendationStatusChangeToastTitle = this._t("Change detected");
        this.recommendationRemovedMessage = this._t("Recommendation is no longer available.");
        this.recommendationScheduledMessage = this._t("Recommendation has been scheduled.");
        this.recommendationUnscheduledMessage = this._t("Recommendation schedule has been canceled.");
        this.recommendationAlreadyRunningMessage = this._t("Recommendation has started processing.");
        this.recommendationAlreadyFinishedMessage = this._t("Recommendation has already finished.");
        this.multipleRecommendationsChangedMessage = this._t("One or more recommendations have changed.");
        this.checkRecommendationStatusHasChanged = function (recommendation) {
            return _this.swisService.getRecommendation(recommendation.id).then(function (after) {
                var result = _this.createRecommendationChangeCheckResult(recommendation, after);
                _this.showRecommendationChangeMessage(result);
                return result;
            });
        };
        this.checkRecommendationsStatusHasChanged = function (recommendations) {
            var ids = _.map(recommendations, function (r) { return r.id; });
            return _this.swisService.getRecommendations(ids).then(function (allAfter) {
                var results = [];
                _.each(recommendations, function (r) {
                    var after = _.find(allAfter, function (item) { return item.id === r.id; });
                    var result = _this.createRecommendationChangeCheckResult(r, after);
                    results.push(result);
                });
                var multipleResult = {
                    isChanged: _.some(results, function (r) { return r.isChanged; }),
                    results: results
                };
                _this.showMultipleRecommendationChangeMessage(multipleResult);
                return multipleResult;
            });
        };
        this.createRecommendationChangeCheckResult = function (before, after) {
            var result = {
                previousValue: before,
                currentValue: after,
                isChanged: false
            };
            if (!after || !before.hasSameStatus(after)) {
                result.isChanged = true;
            }
            return result;
        };
        this.checkRecommendationExists = function (recommendationId) {
            return _this.swisService.recommendationExists(recommendationId).then(function (exists) {
                if (!exists) {
                    _this.showRecommendationRemovedMessage();
                }
                return exists;
            });
        };
        /**
         * Shows toast message upon status change check result
         */
        this.showRecommendationChangeMessage = function (result) {
            if (!result.isChanged) {
                return;
            }
            if (!result.currentValue) {
                _this.showRecommendationRemovedMessage();
            }
            else {
                _this.showRecommendationStatusChangeMessage(result);
            }
        };
        /**
         * Shows toast message upon multiple status change check result
         */
        this.showMultipleRecommendationChangeMessage = function (result) {
            if (!result.isChanged) {
                return;
            }
            // shows generic message
            _this.xuiToastService.warning(_this.multipleRecommendationsChangedMessage, null, _this.toastOptions);
        };
        this.showRecommendationRemovedMessage = function () {
            _this.xuiToastService.warning(_this.recommendationRemovedMessage, null, _this.toastOptions);
        };
        this.showRecommendationStatusChangeMessage = function (result) {
            var changeMessage = _this.getRecommendationStatusChangeMessage(result);
            _this.xuiToastService.warning(changeMessage, null, _this.toastOptions);
        };
        this.createRecommendationStatusChangeChecker = function (recommendation, changedCallback) {
            var checker = new RecommendationStatusChangeChecker(recommendation, changedCallback, _this.$interval, _this.xuiToastService, _this);
            return checker;
        };
        this.createRecommendationsStatusChangeChecker = function (recommendations, changedCallback) {
            var checker = new RecommendationsStatusChangeChecker(recommendations, changedCallback, _this.$interval, _this.xuiToastService, _this);
            return checker;
        };
        this.getRecommendationStatusChangeMessage = function (result) {
            // catch specific situations which need specific wording
            // active => scheduled
            if (result.previousValue.isActive() && result.currentValue.isScheduled()) {
                return _this.recommendationScheduledMessage;
            }
            // scheduled => active
            if (result.previousValue.isScheduled() && result.currentValue.isActive()) {
                return _this.recommendationUnscheduledMessage;
            }
            // X => running
            if (result.currentValue.isRunning()) {
                return _this.recommendationAlreadyRunningMessage;
            }
            // X => finished
            if (result.currentValue.isFinished()) {
                return _this.recommendationAlreadyFinishedMessage;
            }
            // null for other cases which should not normally occur (e.g. transition from running to active)
            return null;
        };
        this.recomputeRecommendations = function () {
            return _this.swApiService.recomputeRecommendations().then(function () {
                _this.xuiToastService.info(_this._t("Recalculating recommendations"), _this._t("Recalculation Status"));
            });
        };
    }
    StatusService = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngIntervalService)),
        __param(1, di_1.Inject(index_1.ServiceNames.getTextService)),
        __param(2, di_1.Inject(index_1.ServiceNames.xuiToastService)),
        __param(3, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(4, di_1.Inject(index_1.ServiceNames.swApiService)),
        __metadata("design:paramtypes", [Function, Function, Object, Object, Object])
    ], StatusService);
    return StatusService;
}());
exports.StatusService = StatusService;
/**
 * Status change checker base class
 */
var StatusChangeCheckerBase = /** @class */ (function () {
    function StatusChangeCheckerBase(changedCallback, $interval, xuiToastService, statusService) {
        var _this = this;
        this.changedCallback = changedCallback;
        this.$interval = $interval;
        this.xuiToastService = xuiToastService;
        this.statusService = statusService;
        this.isInitialized = false;
        this.isDestroyed = false;
        this.init = function () {
            if (_this.isInitialized) {
                throw new Error("Status change checker already initialized.");
            }
            _this.timer = _this.$interval(_this.check, 5000);
        };
        this.check = function () {
            if (_this.isDestroyed) {
                throw Error("Recommendation status change checker is already destroyed.");
            }
            return _this.hasChanged().then(function (hasChanged) {
                if (!hasChanged) {
                    return;
                }
                _this.changedCallback();
                _this.destroy();
            });
        };
        this.destroy = function () {
            _this.$interval.cancel(_this.timer);
            _this.timer = null;
            _this.isDestroyed = true;
        };
    }
    return StatusChangeCheckerBase;
}());
/**
 * Recommendation status change checker
 *
 * @implements IStatusChangeChecker
 */
var RecommendationStatusChangeChecker = /** @class */ (function (_super) {
    __extends(RecommendationStatusChangeChecker, _super);
    function RecommendationStatusChangeChecker(recommendation, changedCallback, $interval, xuiToastService, statusService) {
        var _this = _super.call(this, changedCallback, $interval, xuiToastService, statusService) || this;
        _this.recommendation = recommendation;
        return _this;
    }
    RecommendationStatusChangeChecker.prototype.hasChanged = function () {
        return this.statusService.checkRecommendationStatusHasChanged(this.recommendation).then(function (result) {
            return result.isChanged;
        });
    };
    ;
    return RecommendationStatusChangeChecker;
}(StatusChangeCheckerBase));
/**
 * Recommendations status change checker
 *
 * @implements IStatusChangeChecker
 */
var RecommendationsStatusChangeChecker = /** @class */ (function (_super) {
    __extends(RecommendationsStatusChangeChecker, _super);
    function RecommendationsStatusChangeChecker(recommendations, changedCallback, $interval, xuiToastService, statusService) {
        var _this = _super.call(this, changedCallback, $interval, xuiToastService, statusService) || this;
        _this.recommendations = recommendations;
        return _this;
    }
    RecommendationsStatusChangeChecker.prototype.hasChanged = function () {
        return this.statusService.checkRecommendationsStatusHasChanged(this.recommendations).then(function (result) {
            return result.isChanged;
        });
    };
    ;
    return RecommendationsStatusChangeChecker;
}(StatusChangeCheckerBase));


/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var routeNames_1 = __webpack_require__(19);
/**
 * Routing service
 *
 * @class
 * @implements IRoutingService
 */
var RoutingService = /** @class */ (function () {
    function RoutingService($rootScope, $state) {
        var _this = this;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.getCurrentState = function () {
            return _this.$state.current;
        };
        this.getRecommendationsHref = function () {
            return _this.$state.href(routeNames_1.RouteNames.recommendationsSummaryStateName);
        };
        this.getSettingsHref = function () {
            return _this.$state.href(routeNames_1.RouteNames.settingsStateName);
        };
        this.getConstraintsHref = function () {
            return _this.$state.href(routeNames_1.RouteNames.constraintsStateName);
        };
        this.getPreviousStateHref = function () {
            if (!_this.$rootScope.previousState) {
                throw new Error("Previous state is not defined.");
            }
            return _this.$state.href(_this.$rootScope.previousState.name);
        };
        this.goToRecommendations = function () {
            _this.$state.go(routeNames_1.RouteNames.recommendationsSummaryStateName);
        };
        this.goToActiveRecommendations = function (params, options) {
            _this.$state.go(routeNames_1.RouteNames.activeRecommendationsStateName, params, options);
        };
        this.goToActiveRecommendationDetail = function (recommendationId) {
            _this.$state.go(routeNames_1.RouteNames.activeRecommendationDetailStateName, { recommendationId: recommendationId });
        };
        this.goToScheduledRecommendations = function (params, options) {
            _this.$state.go(routeNames_1.RouteNames.scheduledRecommendationsStateName, params, options);
        };
        this.goToRunningRecommendations = function (params, options) {
            _this.$state.go(routeNames_1.RouteNames.runningRecommendationsStateName, params, options);
        };
        this.goToFinishedRecommendations = function (params, options) {
            _this.$state.go(routeNames_1.RouteNames.finishedRecommendationsStateName, params, options);
        };
        this.goToSettings = function () {
            _this.$state.go(routeNames_1.RouteNames.settingsStateName);
        };
        this.goToConstraints = function () {
            _this.$state.go(routeNames_1.RouteNames.constraintsStateName);
        };
        this.goToStrategiesSettings = function () {
            _this.$state.go(routeNames_1.RouteNames.strategiesSettingsStateName);
        };
        this.isComingFromRecommendations = function () {
            return _this.$rootScope.previousState
                && _this.$rootScope.previousState.name.indexOf(routeNames_1.RouteNames.recommendationsSummaryStateName + ".") === 0;
        };
        this.reload = function (params, options) {
            _this.$state.go(".", params, options || { reload: true });
        };
        this.goToError = function (params) {
            _this.$state.go("Error", params ? params : undefined);
        };
        this.handleStateChangeSuccess = function (e, to, toParams, from, fromParams) {
            _this.$rootScope.previousState = from;
            _this.$rootScope.previousStateParams = fromParams;
        };
        $rootScope.$on("$stateChangeSuccess", this.handleStateChangeSuccess);
    }
    RoutingService.$inject = [index_1.ServiceNames.ngRootScopeService, index_1.ServiceNames.ngState];
    return RoutingService;
}());
exports.RoutingService = RoutingService;


/***/ }),
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var base_swApi_service_1 = __webpack_require__(9);
var index_1 = __webpack_require__(0);
var eventNames_1 = __webpack_require__(11);
exports.recommendationsControllerName = "recommendations";
/**
 * Available swApi methods
 *
 * @enum
 */
var Methods;
(function (Methods) {
    Methods[Methods["GetBatchResult"] = 0] = "GetBatchResult";
    Methods[Methods["GetBatchResults"] = 1] = "GetBatchResults";
    Methods[Methods["CancelBatch"] = 2] = "CancelBatch";
    Methods[Methods["CancelBatches"] = 3] = "CancelBatches";
    Methods[Methods["EnableRecommendations"] = 4] = "EnableRecommendations";
    Methods[Methods["DisableRecommendations"] = 5] = "DisableRecommendations";
    Methods[Methods["ApplyActions"] = 6] = "ApplyActions";
    Methods[Methods["UnscheduleRecommendations"] = 7] = "UnscheduleRecommendations";
    Methods[Methods["InsertConstraintsForRecommendation"] = 8] = "InsertConstraintsForRecommendation";
    Methods[Methods["InsertOrUpdateConstraints"] = 9] = "InsertOrUpdateConstraints";
    Methods[Methods["DeleteConstraints"] = 10] = "DeleteConstraints";
    Methods[Methods["ValidateActions"] = 11] = "ValidateActions";
    Methods[Methods["GetAlertsResolvedByRecommendations"] = 12] = "GetAlertsResolvedByRecommendations";
    Methods[Methods["DeleteRecommendation"] = 13] = "DeleteRecommendation";
    Methods[Methods["RecomputeRecommendations"] = 14] = "RecomputeRecommendations";
    Methods[Methods["EnableDisableStrategy"] = 15] = "EnableDisableStrategy";
})(Methods = exports.Methods || (exports.Methods = {}));
;
/**
 * Url used for resolving pluggability configuration
 */
exports.pluggabilityConfigurationUrl = "recommendations/pluggability/config";
/**
 * SwApi connection service
 *
 * @class
 * @implements ISwApiService
 */
var SwApiService = /** @class */ (function (_super) {
    __extends(SwApiService, _super);
    function SwApiService(swApi, $rootScope, cacheService, dateTimeService) {
        var _this = _super.call(this, swApi) || this;
        _this.$rootScope = $rootScope;
        _this.cacheService = cacheService;
        _this.dateTimeService = dateTimeService;
        _this.getPluggabilityConfiguration = function () {
            return _this.swApi.api(false).one(exports.pluggabilityConfigurationUrl).get();
        };
        _this.getBatchResult = function (batchId) {
            var request = { batchId: batchId };
            return _this.callPostMethod(Methods[Methods.GetBatchResult], request)
                .then(function (result) {
                return _this.generateBatchResult(result);
            });
        };
        _this.getBatchResults = function (batchIds) {
            var request = { batchIds: batchIds };
            return _this.callPostMethod(Methods[Methods.GetBatchResults], request)
                .then(function (result) {
                var value = {};
                _.each(batchIds, function (batchId) {
                    var singleResult = result[batchId];
                    var batchResult = _this.generateBatchResult(singleResult);
                    value[batchId] = batchResult;
                });
                return value;
            });
        };
        _this.cancelAndRevertBatch = function (batchId) {
            var request = { batchId: batchId, doRevert: true };
            return _this.callPostMethod(Methods[Methods.CancelBatch], request);
        };
        _this.cancelAndRevertBatches = function (batchIds) {
            var request = { batchIds: batchIds, doRevert: true };
            return _this.callPostMethod(Methods[Methods.CancelBatches], request);
        };
        _this.enableRecommendations = function () {
            return _this.callPostMethod(Methods[Methods.EnableRecommendations]);
        };
        _this.disableRecommendations = function () {
            return _this.callPostMethod(Methods[Methods.DisableRecommendations]);
        };
        _this.applyRecommendation = function (actionIds, scheduledTime) {
            var request = {
                IDs: actionIds,
                ScheduledTime: scheduledTime
            };
            return _this.callPostMethod(Methods[Methods.ApplyActions], request)
                .then(function () {
                _this.$rootScope.$broadcast(eventNames_1.EventNames.applyRecommendation);
            });
        };
        _this.unscheduleRecommendations = function (recommendationIds) {
            var request = { RecommendationIds: recommendationIds };
            return _this.callPostMethod(Methods[Methods.UnscheduleRecommendations], request)
                .then(function () {
                _this.$rootScope.$broadcast(eventNames_1.EventNames.unscheduleRecommendations);
            });
        };
        _this.insertConstraintsForRecommendation = function (recommendationId, constraints) {
            var request = {
                recommendationId: recommendationId,
                constraints: constraints
            };
            return _this.callPostMethod(Methods[Methods.InsertConstraintsForRecommendation], request)
                .then(function (result) {
                if (result) {
                    _this.$rootScope.$broadcast(eventNames_1.EventNames.constraintCreated);
                }
                return result;
            });
        };
        _this.insertOrUpdateConstraints = function (constraints) {
            var request = {
                Constraints: constraints
            };
            return _this.callPostMethod(Methods[Methods.InsertOrUpdateConstraints], request);
        };
        _this.deleteConstraints = function (constraintIds) {
            var request = { constraintIds: constraintIds };
            return _this.callPostMethod(Methods[Methods.DeleteConstraints], request);
        };
        _this.validateActions = function (actionIds) {
            var request = { ActionIds: actionIds };
            return _this.callPostMethod(Methods[Methods.ValidateActions], request);
        };
        _this.deleteRecommendation = function (recommendationId) {
            var request = {
                RecommendationId: recommendationId,
            };
            return _this.callPostMethod(Methods[Methods.DeleteRecommendation], request);
        };
        _this.getAlertsResolvedByRecommendations = function (recommendationIds) {
            var request = { RecommendationIds: recommendationIds };
            return _this.callGetMethod(Methods[Methods.GetAlertsResolvedByRecommendations], request)
                .then(function (result) {
                return _this.getArrayFromResult(result);
            });
        };
        _this.recomputeRecommendations = function () {
            return _this.callPostMethod(Methods[Methods.RecomputeRecommendations]);
        };
        _this.enableDisableStrategy = function (strategyId, enabled) {
            var request = {
                StrategyId: strategyId,
                Enabled: enabled
            };
            return _this.callPostMethod(Methods[Methods.EnableDisableStrategy], request);
        };
        _this.getControllerUrl = function () {
            return exports.recommendationsControllerName;
        };
        _this.generateBatchResult = function (data) {
            if (_.isEmpty(data)) {
                return null;
            }
            var batchResult = {
                status: data.status,
                cancelationUserId: data.cancelationUserId ? data.cancelationUserId : null,
                cancelationReason: data.cancelationReason ? data.cancelationReason : null,
                failureMessage: data.failureMessage,
                dateFinished: _this.dateTimeService.convertSwApiIsoStringToDate(data.dateFinished),
                actionResults: {}
            };
            _.each(data.actionsResults, function (aresult, id) {
                batchResult.actionResults[id] = {
                    status: aresult.status,
                    type: aresult.actionType,
                    progress: aresult.progress,
                    dateStarted: _this.dateTimeService.convertSwApiIsoStringToDate(aresult.dateStarted),
                    dateFinished: _this.dateTimeService.convertSwApiIsoStringToDate(aresult.dateFinished),
                    failureMessage: aresult.failureMessage
                };
            });
            return batchResult;
        };
        return _this;
    }
    SwApiService.$inject = [
        index_1.ServiceNames.apolloSwApiService,
        index_1.ServiceNames.ngRootScopeService,
        index_1.ServiceNames.cacheService,
        index_1.ServiceNames.dateTimeService
    ];
    return SwApiService;
}(base_swApi_service_1.BaseSwApiService));
exports.SwApiService = SwApiService;


/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var swis_service_1 = __webpack_require__(15);
var enums_1 = __webpack_require__(2);
var constants_1 = __webpack_require__(8);
var action_1 = __webpack_require__(182);
var batch_1 = __webpack_require__(183);
var batchAction_1 = __webpack_require__(39);
var constraint_1 = __webpack_require__(13);
var dependency_1 = __webpack_require__(184);
var recommendation_1 = __webpack_require__(185);
var strategy_1 = __webpack_require__(186);
/**
 * Swis connection service
 *
 * @class
 * @implements ISwisService
 */
var SwisService = /** @class */ (function () {
    function SwisService($log, $filter, $q, swis, dateTimeService, stringService) {
        var _this = this;
        this.$log = $log;
        this.$filter = $filter;
        this.$q = $q;
        this.swis = swis;
        this.dateTimeService = dateTimeService;
        this.stringService = stringService;
        this.getRecommendationInfoQuery = function (data) {
            var columns = [
                "Recommendations.RecommendationID",
                "Recommendations.Name",
                "Recommendations.Description",
                "Recommendations.GroupID",
                "Recommendations.JobID",
                "Recommendations.Priority",
                "Recommendations.RecommendationKind",
                "Recommendations.Tags",
                "Recommendations.Justification.RiskFull",
                "Recommendations.Justification.RiskShort",
                "Recommendations.Status",
                "(CASE WHEN Recommendations.Tags LIKE '%Recommendations.IsNow%' THEN 1 ELSE 0 END) AS IsNow",
                "(SELECT count(dep.DependencyID) as cnt FROM Orion.Recommendations.Dependencies AS dep\n            WHERE dep.RecommendationID = Recommendations.RecommendationID) as DependencyCount"
            ];
            var joins = [];
            if ((data & swis_service_1.RecommendationData.PrimaryAction) || (data & swis_service_1.RecommendationData.Actions)) {
                columns.push("Actions.ActionID", "Actions.Description AS ActionDescription", "Actions.Status AS ActionStatus", "Actions.Type AS ActionType", "Actions.FailureMessage AS ActionFailureMessage");
                if (data & swis_service_1.RecommendationData.PrimaryAction) {
                    joins.push("LEFT JOIN Orion.Recommendations.Actions AS Actions \n                    ON Actions.RecommendationID = Recommendations.RecommendationID AND Actions.Primary = TRUE");
                }
                else if (data & swis_service_1.RecommendationData.Actions) {
                    joins.push("LEFT JOIN Orion.Recommendations.Actions AS Actions \n                    ON Actions.RecommendationID = Recommendations.RecommendationID");
                }
            }
            if ((data & swis_service_1.RecommendationData.Batch) || (data & swis_service_1.RecommendationData.RevertBatch)) {
                columns.push("Batches.Status", "Batches.ScheduledDate", "Batches.BatchID", "Batches.DateStarted", "Batches.DateFinished", "Batches.FailureMessage", "Batches.UserID", "Batches.CancelationUserID", "Batches.CancelationReason", "Batches.RevertBatchID");
                joins.push("LEFT JOIN Orion.Batching.Batches AS Batches ON Batches.BatchID = Recommendations.BatchID");
            }
            if (data & swis_service_1.RecommendationData.RevertBatch) {
                columns.push("RevertBatches.BatchID AS RevertBatches_BatchID", "RevertBatches.Status AS RevertBatches_Status", "RevertBatches.ScheduledDate AS RevertBatches_ScheduledDate", "RevertBatches.DateStarted AS RevertBatches_DateStarted", "RevertBatches.DateFinished AS RevertBatches_DateFinished", "RevertBatches.FailureMessage AS RevertBatches_FailureMessage", "RevertBatches.UserID AS RevertBatches_UserID", "RevertBatches.CancelationUserID AS RevertBatches_CancelationUserID", "RevertBatches.CancelationReason AS RevertBatches_CancelationReason", "RevertBatches.RevertBatchID AS RevertBatches_RevertBatchID");
                joins.push("LEFT JOIN Orion.Batching.Batches AS RevertBatches ON RevertBatches.BatchID = Batches.RevertBatchID");
            }
            return "SELECT " + columns.join(", ") + "\n            FROM Orion.Recommendations.ActiveRecommendations AS Recommendations \n            " + joins.join("\n");
        };
        this.anyRecommendationExists = function () {
            var swql = "SELECT COUNT(*) AS Cnt FROM Orion.Recommendations.ActiveRecommendations";
            return _this.swis.runQuery(swql).then(function (results) {
                return results.rows[0].Cnt > 0;
            });
        };
        this.getRecommendationsFilter = function (filter) {
            var swql = "SELECT DataGroupInfo.ID, DataGroupInfo.Type, DataGroupInfo.Name, Recommendations.RecommendationID,\nRecommendations.Priority, Recommendations.RecommendationKind, Batches.Status, RevertBatches.Status AS RevertBatches_Status,\nRecommendations.Strategy.StrategiesGroupID, (CASE WHEN Recommendations.Tags LIKE '%Recommendations.IsNow%' THEN 1 ELSE 0 END) AS IsNow\nFROM Orion.Recommendations.DataGroupInfo AS DataGroupInfo\nINNER JOIN Orion.Recommendations.ActiveRecommendations AS Recommendations ON Recommendations.GroupID = DataGroupInfo.ID\nLEFT JOIN Orion.Batching.Batches AS Batches ON Batches.BatchID = Recommendations.BatchID\nLEFT JOIN Orion.Recommendations.Actions AS Actions ON Actions.RecommendationID = Recommendations.RecommendationID AND Actions.Primary = TRUE\nLEFT JOIN Orion.Batching.Batches AS RevertBatches ON RevertBatches.BatchID = Batches.RevertBatchID\n" + _this.filterRecommendations(filter);
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    var stats = {
                        id: row.ID,
                        type: row.Type,
                        name: row.Name,
                        recommendationId: row.RecommendationID,
                        recommendationPriority: row.Priority,
                        recommendationKind: row.RecommendationKind,
                        strategiesGroupId: row.StrategiesGroupID,
                        batchStatus: row.Status,
                        isNow: row.IsNow === 1
                    };
                    return stats;
                });
            });
        };
        this.getTotalRecommendationsCount = function () {
            var swql = "SELECT 'Active' AS Status, COUNT(*) AS Cnt\nFROM Orion.Recommendations.ActiveRecommendations\nWHERE BatchID IS NULL\nUNION\n(SELECT '" + enums_1.BatchStatus[enums_1.BatchStatus.Scheduled] + "' AS Status, COUNT(*) AS Cnt\nFROM Orion.Recommendations.ActiveRecommendations\nWHERE Status = '" + enums_1.BatchStatus[enums_1.BatchStatus.Scheduled] + "')\nUNION\n(SELECT '" + enums_1.BatchStatus[enums_1.BatchStatus.Running] + "' AS Status, COUNT(*) AS Cnt\nFROM Orion.Recommendations.ActiveRecommendations Recommendations\nWHERE Recommendations.Status IN ('" + enums_1.BatchStatus[enums_1.BatchStatus.Running] + "', '" + enums_1.BatchStatus[enums_1.BatchStatus.Paused] + "'))";
            return _this.swis.runQuery(swql).then(function (results) {
                return results.rows;
            });
        };
        this.getNotRunningRecommendations = function (filter) {
            var infoQueryFlags = swis_service_1.RecommendationData.PrimaryAction | swis_service_1.RecommendationData.Batch | swis_service_1.RecommendationData.RevertBatch;
            var infoQuery = _this.getRecommendationInfoQuery(infoQueryFlags);
            var filterCriteria = _this.filterRecommendations(filter, { sortByDependencyCount: true });
            var swql = infoQuery + " " + filterCriteria;
            return _this.swis.runQuery(swql).then(function (results) {
                var recommendations = _.map(results.rows, function (value) { return _this.generateRecommendation(value, infoQueryFlags); });
                // Deep clone of the filter object
                var newFilter = _.cloneDeep(filter);
                newFilter.pagination = newFilter.pagination || {};
                newFilter.pagination.total = results.total || 0;
                return {
                    recommendations: recommendations,
                    filter: newFilter
                };
            });
        };
        this.getStrategiesGroups = function () {
            var swql = "SELECT StrategiesGroupID, Name FROM Orion.Recommendations.StrategiesGroup";
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    var stats = {
                        id: row.StrategiesGroupID,
                        name: row.Name
                    };
                    return stats;
                });
            });
        };
        this.getRunningRecommendations = function (filter) {
            var infoQueryFlags = swis_service_1.RecommendationData.Actions | swis_service_1.RecommendationData.Batch | swis_service_1.RecommendationData.RevertBatch;
            var infoQuery = _this.getRecommendationInfoQuery(infoQueryFlags);
            var filterCriteria = _this.filterRecommendations(filter, { sortByDependencyCount: true });
            var swql = infoQuery + " " + filterCriteria;
            return _this.swis.runQuery(swql).then(function (results) {
                var actionsPerRecommendations = _.groupBy(results.rows, "RecommendationID");
                var recommendations = _.map(actionsPerRecommendations, function (data) {
                    var actions = _.map(data, function (action) { return _this.generateAction(action); });
                    // Taking first element because we are grouping by id
                    var r = _this.generateRecommendation(data[0], swis_service_1.RecommendationData.Batch | swis_service_1.RecommendationData.RevertBatch);
                    r.actions = actions;
                    return r;
                });
                // Deep clone of the filter object
                var newFilter = _.cloneDeep(filter);
                newFilter.pagination = newFilter.pagination || {};
                newFilter.pagination.total = results.total || 0;
                return {
                    recommendations: recommendations,
                    filter: newFilter
                };
            });
        };
        this.getRecommendationsWithJustification = function (recommendationIds) {
            var swql = "SELECT Recommendations.RecommendationID,\nRecommendations.Name,\nRecommendations.Description,\nRecommendations.GroupID,\nRecommendations.JobID,\nRecommendations.Priority,\nRecommendations.RecommendationKind,\nRecommendations.Tags,\nRecommendations.Justification.RiskFull,\nRecommendations.Justification.RiskFull,\nBatches.Status,\nBatches.ScheduledDate,\nBatches.BatchID,\nBatches.DateStarted,\nBatches.DateFinished,\nBatches.FailureMessage,\nBatches.UserID,\nBatches.CancelationUserID,\nBatches.CancelationReason,\nBatches.RevertBatchID,\nRecommendations.Justification.Data AS Data\nFROM Orion.Recommendations.ActiveRecommendations Recommendations\nLEFT JOIN Orion.Batching.Batches AS Batches ON Batches.BatchID = Recommendations.BatchID\nWHERE Recommendations.RecommendationID IN ('" + recommendationIds.join("','") + "')";
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (value) {
                    var batch = _this.generateBatch(value);
                    return new recommendation_1.Recommendation({
                        id: value.RecommendationID,
                        name: value.Name,
                        description: value.Description,
                        groupId: value.GroupID,
                        jobId: value.JobID,
                        priority: value.Priority,
                        riskFull: value.RiskFull,
                        riskShort: value.RiskShort,
                        kind: value.RecommendationKind,
                        tags: angular.fromJson(value.Tags),
                        batch: batch,
                        actions: [],
                        justification: angular.fromJson(value.Data)
                    });
                });
            });
        };
        this.getRecommendationJustifications = function (recommendationIds) {
            var swql = "SELECT Recommendations.RecommendationID,\nRecommendations.Justification.Data AS Data\nFROM Orion.Recommendations.ActiveRecommendations Recommendations\nLEFT JOIN Orion.Batching.Batches AS Batches ON Batches.BatchID = Recommendations.BatchID\nWHERE Recommendations.RecommendationID IN (" + _this.concatIds(recommendationIds) + ")";
            return _this.swis.runQuery(swql)
                .then(function (results) {
                if (_.isEmpty(results.rows)) {
                    return {};
                }
                var justifications = {};
                _.each(results.rows, function (row) {
                    justifications[row.RecommendationID] = angular.fromJson(row.Data);
                });
                return justifications;
            });
        };
        this.getRecommendationWithDependencies = function (recommendationId) {
            // 65535 is used to allow ordering of recommendations to make sure that the dependencies come before the recommendation itself
            var swql = "SELECT Recommendations.RecommendationID,\nRecommendations.Name,\nRecommendations.Description,\nRecommendations.GroupID,\nRecommendations.JobID,\nRecommendations.Priority,\nRecommendations.RecommendationKind,\nRecommendations.Tags,\nRecommendations.Justification.RiskFull,\nRecommendations.Justification.RiskShort,\nRecommendations.Justification.Data AS Data,\nRecommendations.Status,\nBatches.Status,\nBatches.ScheduledDate,\nBatches.BatchID,\nBatches.DateStarted,\nBatches.DateFinished,\nBatches.FailureMessage,\nBatches.UserID,\nBatches.CancelationUserID,\nBatches.CancelationReason,\nBatches.RevertBatchID,\nIsNull(Dependencies.[Order], 65535) AS DependencyOrder,\nDependencies.Reason AS DependencyReason\nFROM Orion.Recommendations.ActiveRecommendations AS Recommendations\nLEFT OUTER JOIN\nOrion.Batching.Batches AS Batches\nON Batches.BatchID = Recommendations.BatchID\nLEFT OUTER JOIN\nOrion.Recommendations.Dependencies AS Dependencies\nON Dependencies.RecommendationID = '" + recommendationId + "'\nAND Recommendations.RecommendationID = Dependencies.DependencyID\nWHERE (Recommendations.RecommendationID IN (SELECT DependencyID\n        FROM   Orion.Recommendations.Dependencies AS InnerDep\n        WHERE  InnerDep.RecommendationID = '" + recommendationId + "')\nAND Recommendations.Status <> 'FinishedSuccessfully')\nOR Recommendations.RecommendationID = '" + recommendationId + "'\nORDER BY DependencyOrder ASC;";
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    var batch = _this.generateBatch(row);
                    var dependency = null;
                    if (row.DependencyOrder < 65535) {
                        dependency = new dependency_1.Dependency();
                        dependency.reason = row.DependencyReason;
                        dependency.order = row.DependencyOrder;
                    }
                    return new recommendation_1.Recommendation({
                        id: row.RecommendationID,
                        name: row.Name,
                        description: row.Description,
                        groupId: row.GroupID,
                        jobId: row.JobID,
                        priority: row.Priority,
                        riskFull: row.RiskFull,
                        riskShort: row.RiskShort,
                        kind: row.RecommendationKind,
                        tags: angular.fromJson(row.Tags),
                        batch: batch,
                        actions: [],
                        justification: angular.fromJson(row.Data),
                        dependency: dependency
                    });
                });
            });
        };
        this.getRecommendationsWithDependencies = function (recommendationIds) {
            var promises = [];
            _.each(recommendationIds, function (id) {
                promises.push(_this.getRecommendationWithDependencies(id));
            });
            // wait until all swis queries are done, then collect recommendations in to one list
            return _this.$q.all(promises).then(function (result) {
                var recommendations = [];
                _.each(result, function (recommendationWithDependencies) {
                    recommendations = _.concat(recommendations, recommendationWithDependencies);
                });
                var uniqueRecommendations = _this.deduplicateRecommendationsWithDependencies(recommendations);
                return uniqueRecommendations;
            });
        };
        this.getRecommendation = function (recommendationId, data) {
            if (data === void 0) { data = swis_service_1.RecommendationData.Batch; }
            var infoQuery = _this.getRecommendationInfoQuery(data);
            var swql = infoQuery + " WHERE Recommendations.RecommendationID='" + recommendationId + "'";
            return _this.swis.runQuery(swql)
                .then(function (result) {
                if (_.isEmpty(result.rows)) {
                    return null;
                }
                var value = result.rows[0];
                return _this.generateRecommendation(value, data);
            });
        };
        this.getRecommendations = function (recommendationIds) {
            var ids = _this.concatIds(recommendationIds);
            var infoQueryFlags = swis_service_1.RecommendationData.Batch;
            var infoQuery = _this.getRecommendationInfoQuery(infoQueryFlags);
            var swql = infoQuery + " WHERE Recommendations.RecommendationID IN (" + ids + ")";
            return _this.swis.runQuery(swql)
                .then(function (result) {
                return _.map(result.rows, function (row) { return _this.generateRecommendation(row, infoQueryFlags); });
            });
        };
        this.getRecommendationActions = function (recommendationIds) {
            if (!recommendationIds || _.isEmpty(recommendationIds)) {
                return _this.$q.resolve([]);
            }
            var idsJoined = _.map(recommendationIds, function (id) { return "'" + id + "'"; }).join(",");
            var swql = "SELECT\nActionID,\nAction,\nDescription AS Name,\nType,\nPrimary,\nOptional,\nStatus,\nDateStarted,\nDateFinished,\nFailureMessage,\n[Order],\nRecommendationID,\nActions.Recommendation.Name AS RecommendationName,\nActions.Recommendation.Priority AS RecommendationPriority\nFROM Orion.Recommendations.Actions\nWHERE RecommendationID IN (" + idsJoined + ")\nORDER BY RecommendationID ASC, [Order] ASC";
            return _this.swis.runQuery(swql).then(function (results) {
                _.each(results.rows, function (row) {
                    var dateStarted = row.DateStarted;
                    var dateFinished = row.DateFinished;
                    row.DateStarted = dateStarted ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateStarted)) : null;
                    row.DateFinished = dateFinished ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateFinished)) : null;
                    row.Status = enums_1.ActionStatus[row.Status];
                    row.Enabled = true;
                    row.Details = angular.fromJson(row.Action);
                });
                // sort actions by requested recommendations and then by their order
                return _.sortBy(results.rows, function (item) { return (recommendationIds.indexOf(item.RecommendationID) * 100000 + item.Order); });
            });
        };
        this.recommendationExists = function (recommendationId) {
            var swql = "SELECT RecommendationID\nFROM Orion.Recommendations.ActiveRecommendations\nWHERE RecommendationID='" + recommendationId + "'";
            return _this.swis.runQuery(swql).then(function (result) {
                return !_.isEmpty(result.rows);
            });
        };
        this.isRecommendationsEnabled = function () {
            var swql = "SELECT CurrentValue FROM Orion.Settings WHERE SettingID = 'Recommendations_IsComputationEnabled'";
            return _this.swis.runQuery(swql).then(function (results) {
                if (_.isEmpty(results.rows)) {
                    return null;
                }
                return !!results.rows[0].CurrentValue;
            });
        };
        this.isHistoryAvailable = function () {
            var swql = "SELECT count(*) AS NoDataGroupCount FROM Orion.Recommendations.DataGroupInfo WHERE HasEnoughData = 0";
            return _this.swis.runQuery(swql).then(function (results) {
                if (_.isEmpty(results.rows)) {
                    return null;
                }
                return results.rows[0].NoDataGroupCount === 0;
            });
        };
        this.getLastComputedTime = function () {
            var swql = "SELECT Top 1 ComputedTime FROM Orion.Recommendations.ActiveRecommendations ORDER BY ComputedTime DESC";
            return _this.swis.runQuery(swql).then(function (results) {
                if (results.rows.length === 0) {
                    return null;
                }
                var lastDateTimeString = results.rows[0].ComputedTime;
                return _this.dateTimeService.convertMomentToOrionDate(moment.utc(lastDateTimeString));
            });
        };
        this.getRecommendationJob = function (recommendationId) {
            var swql = "SELECT TOP 1 Jobs.JobID FROM Orion.Recommendations.Jobs AS Jobs\nLEFT JOIN Orion.Recommendations.ActiveRecommendations AS Recommendations\nON Jobs.DataGroup.ID = Recommendations.GroupID\nWHERE Recommendations.RecommendationID = '" + recommendationId + "' AND Recommendations.RecommendationKind = Jobs.RecommendationKind\nORDER BY DateFinished DESC";
            return _this.swis.runQuery(swql).then(function (results) {
                if (results.rows.length === 0) {
                    return null;
                }
                return results.rows[0].JobID;
            });
        };
        this.getRevertBatchWithActions = function (batchId) {
            var swql = "SELECT\nRevertBatches.BatchID AS Batches_BatchID,\nRevertBatches.Status AS Batches_Status,\nRevertBatches.ScheduledDate AS Batches_ScheduledDate,\nRevertBatches.DateStarted AS Batches_DateStarted,\nRevertBatches.DateFinished AS Batches_DateFinished,\nRevertBatches.FailureMessage AS Batches_FailureMessage,\nRevertBatches.UserID AS Batches_UserID,\nRevertBatches.CancelationUserID AS Batches_CancelationUserID,\nRevertBatches.CancelationReason AS Batches_CancelationReason,\nRevertBatches.RevertBatchID AS Batches_RevertBatchID,\nBatchActions.ID AS BatchActions_ID,\nBatchActions.BatchID AS BatchActions_BatchID,\nBatchActions.Status AS BatchActions_Status,\nBatchActions.DateStarted AS BatchActions_DateStarted,\nBatchActions.DateFinished AS BatchActions_DateFinished,\nBatchActions.FailureMessage AS BatchActions_FailureMessage,\nBatchActions.Description AS BatchActions_Description,\nBatchActions.Action AS BatchActions_Action\nFROM Orion.Batching.Batches AS RevertBatches\nJOIN Orion.Batching.Batches AS Batches\n    ON Batches.RevertBatchID = RevertBatches.BatchID AND Batches.BatchID = '" + batchId + "'\nJOIN Orion.Batching.Actions AS BatchActions\n    ON BatchActions.BatchID = RevertBatches.BatchID\nORDER BY BatchActions.[Order] ASC";
            return _this.swis.runQuery(swql).then(function (results) {
                if (_.isEmpty(results.rows)) {
                    return null;
                }
                var batch = _this.generateBatch(results.rows[0], "Batches_");
                _.each(results.rows, function (data) {
                    var batchAction = _this.generateBatchAction(data, "BatchActions_");
                    batch.actions.push(batchAction);
                });
                return batch;
            });
        };
        this.getSwProducts = function () {
            var swql = "SELECT Name, Version, DisplayName, Description FROM Orion.InstalledModule";
            return _this.swis.runQuery(swql).then(function (results) {
                return results.rows;
            });
        };
        this.getSwProduct = function (product) {
            var name = enums_1.SwProduct[product];
            var swql = "SELECT Name, Version, DisplayName, Description FROM Orion.InstalledModule WHERE Name = '" + name + "'";
            return _this.swis.runQuery(swql).then(function (results) {
                if (!_.isEmpty(results.rows)) {
                    return results.rows[0];
                }
                return null;
            });
        };
        this.isSwProductInstalled = function (product) {
            return _this.getSwProduct(product).then(function (instance) { return !!instance; });
        };
        this.getConstraints = function (filter) {
            var swql = _this.getFilteredConstraintsQuery(filter);
            return _this.swis.runQuery(swql).then(function (results) {
                var constraints = _.map(results.rows, function (value) {
                    var dateCreated = value.DateCreated;
                    var expirationDate = value.ExpirationDate;
                    return new constraint_1.Constraint({
                        id: value.ConstraintID,
                        name: value.Name,
                        description: value.Description,
                        type: value.Type,
                        dateCreated: dateCreated ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateCreated)) : null,
                        expirationDate: expirationDate ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(expirationDate)) : null,
                        enabled: value.Enabled,
                        userId: value.UserID
                    });
                });
                // Deep clone of the filter object
                var newFilter = _.cloneDeep(filter);
                newFilter.pagination = newFilter.pagination || {};
                newFilter.pagination.total = results.total;
                return {
                    constraints: constraints,
                    filter: newFilter
                };
            });
        };
        this.hasAnyGroupData = function () {
            var swql = "SELECT ID FROM Orion.Recommendations.DataGroupInfo WHERE HasEnoughData = 1";
            return _this.swis.runQuery(swql).then(function (results) {
                return !_.isEmpty(results.rows);
            });
        };
        this.getStrategiesByModule = function (module) {
            var swql = "SELECT StrategyID, StrategiesGroupID, Enabled FROM Orion.Recommendations.Strategies WHERE Module = '" + module + "'";
            return _this.swis.runQuery(swql).then(function (results) {
                var strategies = {};
                _.each(results.rows, function (row) {
                    strategies[row.StrategyID] = _this.generateStrategy(row);
                });
                return strategies;
            });
        };
        this.filterRecommendations = function (filter, settings) {
            var nameColumn = "Recommendations.Name";
            var priorityColumn = "Recommendations.Priority";
            var kindColumn = "Recommendations.RecommendationKind";
            var groupColumn = "Recommendations.GroupID";
            var strategyColumn = "Recommendations.Strategy.StrategiesGroupID";
            var dependencyCountColumn = "DependencyCount";
            var isNowColumn = "IsNow";
            settings = settings || {};
            // We are always filtering at least by Batches.status, so where could be const
            var query = "";
            var whereConditions = [];
            // Full-text search
            if (!_.isEmpty(filter.search)) {
                whereConditions.push("(Recommendations.Description LIKE '%" + filter.search + "%' \n                OR Recommendations.Justification.RiskFull LIKE '%" + filter.search + "%')");
            }
            // Filter actions - used only for running tab
            if (filter.onlyAppliedActions) {
                whereConditions.push("Actions.Status IS NOT NULL");
            }
            // Filtering by entities
            if (!_.isEmpty(filter.filters.entity)) {
                var entityFilterValue = _.map(filter.filters.entity, function (id) { return "'" + id + "'"; }).join(", ");
                whereConditions.push(groupColumn + " IN (" + entityFilterValue + ")");
            }
            // Filtering by priorities
            if (!_.isEmpty(filter.filters.severity)) {
                var priorityFilterValue = filter.filters.severity.join(",");
                whereConditions.push(priorityColumn + " IN (" + priorityFilterValue + ")");
            }
            // Filtering by kind
            if (!_.isEmpty(filter.filters.kind)) {
                var kindFilterValue = filter.filters.kind.join(",");
                var kindNow = "";
                if (filter.filters.kind.indexOf(enums_1.RecommendationKind.realtime) >= 0) {
                    // we also want to show recommendations which are tagged to be shown with NOW label
                    kindNow = "OR Recommendations.Tags LIKE '%\"" + constants_1.WellKnownRecommendationTags.isNow + "\"%'";
                }
                whereConditions.push("(" + kindColumn + " IN (" + kindFilterValue + ") " + kindNow + ")");
            }
            // Filtering by status
            var statusWhereConditions = [];
            if (!_.isEmpty(filter.filters.status)) {
                var statusFilterValue = _.map(filter.filters.status, function (status) { return "'" + status + "'"; }).join(",");
                statusWhereConditions.push("Batches.Status IN (" + statusFilterValue + ")");
            }
            else {
                statusWhereConditions.push("Batches.BatchID IS NULL");
            }
            // Filtering by strategies
            if (!_.isEmpty(filter.filters.strategiesGroup)) {
                var strategiesGroupFilterValue = _.map(filter.filters.strategiesGroup, function (id) { return "'" + id + "'"; }).join(", ");
                whereConditions.push(strategyColumn + " IN (" + strategiesGroupFilterValue + ")");
            }
            if (!_.isEmpty(whereConditions)) {
                var whereConditionsJoined = whereConditions.join(" AND ");
                query += " WHERE (" + whereConditionsJoined + ")";
            }
            if (!_.isEmpty(statusWhereConditions)) {
                var statusWhereConditionsJoined = statusWhereConditions.join(" AND ");
                if (filter.filters.status.indexOf(enums_1.BatchStatus[enums_1.BatchStatus.RevertBatchInProgress_Indication]) !== -1) {
                    statusWhereConditionsJoined += " OR (Batches.Status IN ('FinishedWithError', 'Canceled') AND RevertBatches_Status = 'Running')";
                }
                if (filter.filters.status.indexOf(enums_1.BatchStatus[enums_1.BatchStatus.RevertBatchNotInProgress_Indication]) !== -1) {
                    statusWhereConditionsJoined += " AND ISNULL(RevertBatches_Status, '') != 'Running'";
                }
                if (!_.isEmpty(query)) {
                    query += " AND (" + statusWhereConditionsJoined + ")";
                }
                else {
                    query += " WHERE (" + statusWhereConditionsJoined + ")";
                }
            }
            // Order
            var sortByPriorityDefault = priorityColumn + " ASC";
            var sortByKindDefault = kindColumn + " DESC, " + isNowColumn + " DESC";
            var sortByDependencyCountDefault = dependencyCountColumn + " ASC";
            var useDefaultSorting = !filter.sorting || !filter.sorting.sortBy || !filter.sorting.sortBy.id;
            if (useDefaultSorting) {
                query += " ORDER BY " + sortByPriorityDefault + ", " + sortByKindDefault;
            }
            else {
                var sortBy = filter.sorting.sortBy.id.trim();
                var sortByLowerCase = sortBy.toLowerCase();
                var sortDirection = filter.sorting.direction || "ASC";
                if (sortByLowerCase === "priority") {
                    query += " ORDER BY " + priorityColumn + " " + sortDirection + ", " + sortByKindDefault;
                }
                else if (sortByLowerCase === "kind") {
                    query += " ORDER BY " + kindColumn + " " + sortDirection + ", " + isNowColumn + " " + sortDirection + ", " + sortByPriorityDefault;
                }
                else if (sortByLowerCase === "name") {
                    query += " ORDER BY " + nameColumn + " " + sortDirection + ", " + sortByPriorityDefault + ", " + sortByKindDefault;
                }
                else {
                    query += " ORDER BY " + sortBy + " " + sortDirection;
                }
            }
            // force sorting by dependency count if needed
            if (settings.sortByDependencyCount) {
                query += ", " + sortByDependencyCountDefault;
            }
            // Pagination
            if (filter.pagination) {
                var startRow = (filter.pagination.pageSize * (filter.pagination.page - 1)) + 1;
                var endRow = startRow + filter.pagination.pageSize - 1;
                query += " WITH ROWS " + startRow + " TO " + endRow + " WITH TOTALROWS";
            }
            return query;
        };
        this.getFilteredConstraintsQuery = function (filter) {
            var query = "SELECT DISTINCT\nConstraintID,\nName,\nDescription,\nType,\nDateCreated,\nExpirationDate,\nEnabled,\nUserID\nFROM Orion.Recommendations.Constraints";
            var whereConditions = [];
            // Full-text search
            if (!_.isEmpty(filter.search)) {
                whereConditions.push("(Constraints.Description LIKE '%" + filter.search + "%' OR Constraints.ConstraintID IN (\nSELECT co.ConstraintID\nFROM Orion.Recommendations.ConstraintObjects co\nLEFT JOIN Orion.VIM.Clusters c ON c.ClusterID = co.ObjectID AND EntityType = 'Cluster'\nLEFT JOIN Orion.VIM.Datastores ds ON ds.DataStoreID = co.ObjectID AND EntityType = 'Datastore'\nLEFT JOIN Orion.VIM.Hosts h ON h.HostID = co.ObjectID AND EntityType = 'Host'\nLEFT JOIN Orion.VIM.VirtualMachines vm ON vm.VirtualMachineID = co.ObjectID AND EntityType = 'VirtualMachine'\nWHERE (c.DisplayName LIKE '%" + filter.search + "%' OR ds.DisplayName LIKE '%" + filter.search + "%' \n    OR h.DisplayName LIKE '%" + filter.search + "%' OR vm.DisplayName LIKE '%" + filter.search + "%')\n))");
            }
            // Constrained objects
            if (!_.isEmpty(filter.constrainedObjects)) {
                var objectList = _this.concatIds(filter.constrainedObjects);
                whereConditions.push("Constraints.ConstraintObjects.EntityIdentifier IN (" + objectList + ")");
            }
            // DataGroup objects
            if (!_.isEmpty(filter.dataGroupObjects)) {
                whereConditions.push(_this.getSwqlForFilteringByDataGroup(filter.dataGroupObjects));
            }
            // constraint types
            if (!_.isEmpty(filter.constraintTypes)) {
                var typesList = _this.concatIds(filter.constraintTypes);
                whereConditions.push("Constraints.Type IN (" + typesList + ")");
            }
            if (!_.isEmpty(whereConditions)) {
                var whereConditionsJoined = whereConditions.join(" AND ");
                query += " WHERE " + whereConditionsJoined;
            }
            var useDefaultSorting = !filter.sorting || !filter.sorting.sortBy || !filter.sorting.sortBy.id;
            if (useDefaultSorting) {
                query += " ORDER BY DateCreated DESC";
            }
            else {
                var sortBy = filter.sorting.sortBy.id.trim();
                var sortByLowerCase = sortBy.toLowerCase();
                var sortDirection = filter.sorting.direction || "ASC";
                query += " ORDER BY " + sortBy + " " + sortDirection;
            }
            // Pagination
            if (filter.pagination) {
                var startRow = (filter.pagination.pageSize * (filter.pagination.page - 1)) + 1;
                var endRow = startRow + filter.pagination.pageSize - 1;
                query += " WITH ROWS " + startRow + " TO " + endRow + " WITH TOTALROWS";
            }
            return query;
        };
        this.concatIds = function (ids) {
            return _.map(ids, function (id) { return "'" + id + "'"; }).join(",");
        };
        this.getSwqlForFilteringByDataGroup = function (dataGroupObjects) {
            var objectList = _this.concatIds(dataGroupObjects);
            return "(\n(\nConstraints.ConstraintObjects.EntityType = 'VirtualMachine' AND Constraints.ConstraintObjects.ObjectID IN (\nSELECT VirtualMachineID FROM Orion.VIM.VirtualMachines vm WHERE ('VH:'+TOSTRING(vm.Host.HostID)) IN (" + objectList + ") OR ('VMC:'+TOSTRING(vm.Host.ClusterID) IN (" + objectList + "))\n)\n)\nOR (\nConstraints.ConstraintObjects.EntityType = 'Host' AND Constraints.ConstraintObjects.ObjectID IN (\nSELECT HostID FROM Orion.VIM.Hosts h WHERE ('VH:'+TOSTRING(h.HostID)) IN (" + objectList + ") OR ('VMC:'+TOSTRING(h.ClusterID) IN (" + objectList + "))\n)\n)\nOR (\nConstraints.ConstraintObjects.EntityType = 'Cluster' AND Constraints.ConstraintObjects.ObjectID IN (\nSELECT ClusterID FROM Orion.VIM.Clusters WHERE ('VMC:'+TOSTRING(ClusterID) IN (" + objectList + "))\n)\n)\nOR (\nConstraints.ConstraintObjects.EntityType = 'DataStore' AND Constraints.ConstraintObjects.ObjectID IN (\nSELECT DataStoreID FROM Orion.VIM.Datastores ds WHERE ('VH:'+TOSTRING(ds.Hosts.HostID)) IN (" + objectList + ") OR  ('VMC:'+TOSTRING(ds.Hosts.ClusterID) IN (" + objectList + "))\n)\n)\n)";
        };
        /**
         * Generates recommendation instance
         * @description Method cannot be used for generating multiple actions
         * */
        this.generateRecommendation = function (value, data) {
            var actions = [];
            if (data & swis_service_1.RecommendationData.PrimaryAction) {
                var action = _this.generateAction(value);
                actions.push(action);
            }
            var batch = undefined;
            if (data & swis_service_1.RecommendationData.Batch) {
                batch = _this.generateBatch(value);
            }
            if ((data & swis_service_1.RecommendationData.RevertBatch) && batch && batch.revertBatchId) {
                batch.revertBatch = _this.generateBatch(value, "RevertBatches_");
            }
            return new recommendation_1.Recommendation({
                id: value.RecommendationID,
                name: value.Name,
                description: value.Description,
                groupId: value.GroupID,
                jobId: value.JobID,
                priority: value.Priority,
                riskFull: value.RiskFull,
                riskShort: value.RiskShort,
                kind: value.RecommendationKind,
                tags: angular.fromJson(value.Tags),
                batch: batch,
                actions: actions
            });
        };
        this.generateBatch = function (data, columnPrefix) {
            if (columnPrefix === void 0) { columnPrefix = ""; }
            if (!data[columnPrefix + "BatchID"]) {
                return null;
            }
            var dateStarted = data[columnPrefix + "DateStarted"];
            var dateFinished = data[columnPrefix + "DateFinished"];
            var scheduledDate = data[columnPrefix + "ScheduledDate"];
            var batch = new batch_1.Batch();
            batch.id = data[columnPrefix + "BatchID"];
            batch.status = enums_1.BatchStatus[data[columnPrefix + "Status"]];
            batch.failureMessage = data[columnPrefix + "FailureMessage"];
            batch.dateStarted = dateStarted ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateStarted)) : null;
            batch.dateFinished = dateFinished ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateFinished)) : null;
            batch.scheduledDate = scheduledDate ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(scheduledDate)) : null;
            batch.user = data[columnPrefix + "UserID"];
            batch.cancelationUserId = data[columnPrefix + "CancelationUserID"];
            var cancelationReason = data[columnPrefix + "CancelationReason"];
            batch.cancelationReason = cancelationReason ? enums_1.BatchCancelationReason[cancelationReason] : null;
            batch.revertBatchId = data[columnPrefix + "RevertBatchID"];
            return batch;
        };
        this.generateAction = function (data) {
            if (!data.ActionID) {
                return null;
            }
            var action = new action_1.Action();
            action.id = data.ActionID;
            action.status = enums_1.ActionStatus[data.ActionStatus];
            action.description = data.ActionDescription;
            action.dateFinished = data.DateFinished ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(data.DateFinished)) : null;
            action.failureMessage = data.ActionFailureMessage;
            action.progress = 0;
            action.type = data.ActionType;
            return action;
        };
        this.generateBatchAction = function (data, columnPrefix) {
            if (columnPrefix === void 0) { columnPrefix = ""; }
            if (!data[columnPrefix + "ID"]) {
                return null;
            }
            var dateStarted = data[columnPrefix + "DateStarted"];
            var dateFinished = data[columnPrefix + "DateFinished"];
            var action = new batchAction_1.BatchAction();
            action.id = data[columnPrefix + "ID"];
            action.batchId = data[columnPrefix + "BatchID"];
            action.status = enums_1.ActionStatus[data[columnPrefix + "Status"]];
            action.dateStarted = dateStarted ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateStarted)) : null;
            action.dateFinished = dateFinished ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateFinished)) : null;
            action.failureMessage = data[columnPrefix + "FailureMessage"];
            action.description = data[columnPrefix + "Description"];
            action.action = angular.fromJson(data[columnPrefix + "Action"]);
            return action;
        };
        this.generateStrategy = function (data) {
            var strategy = new strategy_1.Strategy();
            strategy.id = data["StrategyID"];
            strategy.groupId = data["StrategiesGroupID"];
            strategy.enabled = data["Enabled"];
            return strategy;
        };
        /**
         * Deduplicates recommendations - i.e. duplicates without dependency will be deleted
         */
        this.deduplicateRecommendationsWithDependencies = function (recommendations) {
            var uniqRecommendations = [];
            // deduplicate recommendations list - duplicates without dependency will be deleted
            _.each(recommendations, function (r) {
                var duplicate = _.find(uniqRecommendations, function (ur) { return ur.id === r.id; });
                if (duplicate) {
                    if (r.dependency && !duplicate.dependency) {
                        _.remove(uniqRecommendations, function (ur) { return ur.id === duplicate.id; });
                        uniqRecommendations.push(r);
                    }
                }
                else {
                    uniqRecommendations.push(r);
                }
            });
            return uniqRecommendations;
        };
    }
    SwisService = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngLog)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngFilterService)),
        __param(2, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(3, di_1.Inject(index_1.ServiceNames.apolloSwisService)),
        __param(4, di_1.Inject(index_1.ServiceNames.apolloDateTimeService)),
        __param(5, di_1.Inject(index_1.ServiceNames.stringService)),
        __metadata("design:paramtypes", [Object, Function, Function, Object, Object, Object])
    ], SwisService);
    return SwisService;
}());
exports.SwisService = SwisService;


/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Recommendation Action
 *
 * @class
 * @implements IAction
 */
var Action = /** @class */ (function () {
    function Action() {
    }
    return Action;
}());
exports.Action = Action;


/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
/**
 * Batch represention.
 *
 * @class
 * @implements IBatch
 */
var Batch = /** @class */ (function () {
    function Batch() {
        var _this = this;
        this.actions = [];
        this.isScheduled = function () {
            return _this.status === enums_1.BatchStatus.Scheduled;
        };
        this.isRunning = function () {
            return _this.status === enums_1.BatchStatus.Running || _this.status === enums_1.BatchStatus.Canceling;
        };
        this.isFinished = function () {
            return _this.status === enums_1.BatchStatus.FinishedSuccessfully
                || _this.status === enums_1.BatchStatus.FinishedWithError
                || _this.status === enums_1.BatchStatus.Canceled;
        };
        this.isCanceledAndReverted = function () {
            return _this.status === enums_1.BatchStatus.Canceled && !!_this.revertBatchId;
        };
        this.isCanceledByUser = function () {
            return _this.status === enums_1.BatchStatus.Canceled
                && _this.cancelationReason === enums_1.BatchCancelationReason.CanceledByUser;
        };
        this.isCanceledDueToDependencyFailure = function () {
            return _this.status === enums_1.BatchStatus.Canceled
                && _this.cancelationReason === enums_1.BatchCancelationReason.CanceledDueToDependencyFailure;
        };
    }
    return Batch;
}());
exports.Batch = Batch;


/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Recommendation dependency
 *
 * @class
 * @implements IDependency
 */
var Dependency = /** @class */ (function () {
    function Dependency() {
    }
    return Dependency;
}());
exports.Dependency = Dependency;


/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(8);
/**
 * Recommendation class
 *
 * @class
 * @implements IRecommendation
 */
var Recommendation = /** @class */ (function () {
    function Recommendation(args) {
        var _this = this;
        this.actions = [];
        this.isRealtime = function () {
            return _this.kind === 1;
        };
        this.isNow = function () {
            if (_this.isRealtime()) {
                return true;
            }
            return _this.tags && _this.tags.indexOf(constants_1.WellKnownRecommendationTags.isNow) >= 0;
        };
        this.hasHighestPriority = function () {
            return _this.priority === 1;
        };
        this.isActive = function () {
            return _this.batch == null || _this.batch.status == null;
        };
        this.isScheduled = function () {
            return _this.batch && _this.batch.isScheduled();
        };
        this.isRunning = function () {
            return _this.batch && _this.batch.isRunning();
        };
        this.isFinished = function () {
            return _this.batch && _this.batch.isFinished();
        };
        this.hasSameStatus = function (other) {
            if (!other) {
                return false;
            }
            if (!_this.batch && !other.batch) {
                return true;
            }
            if ((_this.batch && !other.batch) || (!_this.batch && other.batch)) {
                return false;
            }
            return _this.batch.status === other.batch.status;
        };
        if (!args) {
            return;
        }
        this.id = args.id;
        this.name = args.name;
        this.description = args.description;
        this.groupId = args.groupId;
        this.jobId = args.jobId;
        this.riskFull = args.riskFull;
        this.riskShort = args.riskShort;
        this.priority = args.priority;
        this.kind = args.kind;
        this.justification = args.justification;
        this.batch = args.batch;
        this.actions = args.actions;
        this.dependency = args.dependency;
        this.tags = args.tags;
    }
    ;
    return Recommendation;
}());
exports.Recommendation = Recommendation;
;


/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Strategy info implementation
 *
 * @class
 * @implements IStrategy
 **/
var Strategy = /** @class */ (function () {
    function Strategy() {
    }
    return Strategy;
}());
exports.Strategy = Strategy;


/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(2);
var index_1 = __webpack_require__(0);
/**
 * Sw products meta data service
 *
 * @class
 * @implements ISwProductsService
 */
var SwProductsService = /** @class */ (function () {
    function SwProductsService(cacheService, swisService) {
        var _this = this;
        this.cacheService = cacheService;
        this.swisService = swisService;
        this.isProductInstalled = function (product) {
            var expirationSeconds = 5 * 60;
            var key = "is-" + enums_1.SwProduct[product] + "-installed";
            return _this.cacheService.getOrAdd(key, expirationSeconds, function () { return _this.swisService.isSwProductInstalled(product); })
                .then(function (cacheItem) {
                return cacheItem.value;
            });
        };
    }
    SwProductsService.$inject = [
        index_1.ServiceNames.cacheService,
        index_1.ServiceNames.swisService
    ];
    return SwProductsService;
}());
exports.SwProductsService = SwProductsService;


/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var base_swApi_service_1 = __webpack_require__(9);
/**
 * Available orionInfo methods
 *
 * @enum
 */
var Methods;
(function (Methods) {
    Methods[Methods["GetHelpUrl"] = 0] = "GetHelpUrl";
})(Methods = exports.Methods || (exports.Methods = {}));
;
/**
 * OrionInfo connection service
 *
 * @class
 * @implements IOrionInfoService
 */
var OrionInfoService = /** @class */ (function (_super) {
    __extends(OrionInfoService, _super);
    function OrionInfoService(swApi) {
        var _this = _super.call(this, swApi) || this;
        _this.getHelpUrl = function (fragment) {
            var request = { Fragment: fragment };
            return _this.callGetMethod(Methods[Methods.GetHelpUrl], request);
        };
        _this.getControllerUrl = function () {
            return "recommendations/orioninfo";
        };
        return _this;
    }
    OrionInfoService.$inject = ["swApi"];
    return OrionInfoService;
}(base_swApi_service_1.BaseSwApiService));
exports.OrionInfoService = OrionInfoService;


/***/ }),
/* 189 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var IntervalCallerService = /** @class */ (function () {
    function IntervalCallerService($interval, $log) {
        var _this = this;
        this.$interval = $interval;
        this.$log = $log;
        this.timeout = 10000;
        this.subscribers = [];
        this.intervalPromise = null;
        this.getIntervalValue = function () {
            return _this.timeout;
        };
        this.subscribe = function (subscriber) {
            if (_this.subscribers.indexOf(subscriber) !== -1) {
                throw Error("Subscriber already subscribed.");
            }
            _this.subscribers.push(subscriber);
            if (!_this.intervalPromise) {
                _this.intervalPromise = _this.$interval(_this.callCallbacks, _this.timeout);
            }
        };
        this.unsubscribe = function (subscriber) {
            var index = _this.subscribers.indexOf(subscriber);
            if (index === -1) {
                throw Error("Can not unsubscribe, not in subscribers list.");
            }
            _this.subscribers.splice(index, 1);
            if (_this.subscribers.length === 0) {
                _this.$interval.cancel(_this.intervalPromise);
                _this.intervalPromise = null;
            }
        };
        this.callCallbacks = function () {
            _this.subscribers.forEach(_this.call);
        };
        this.call = function (subscriber) {
            subscriber.subscriberCallback();
        };
    }
    IntervalCallerService.$inject = ["$interval", "$log"];
    return IntervalCallerService;
}());
exports.IntervalCallerService = IntervalCallerService;


/***/ }),
/* 190 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(40);
var bytesFormatFilter_1 = __webpack_require__(191);
var numberFormatFilter_1 = __webpack_require__(192);
exports.default = function (module) {
    module.filter(index_1.FilterNames.bytesFormatFilter, bytesFormatFilter_1.BytesFormatFilterFactory);
    module.filter(index_1.FilterNames.numberFormatFilter, numberFormatFilter_1.NumberFormatFilterFactory);
};


/***/ }),
/* 191 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var kilo = 1024;
/**
 * Filter factory
 *
 * @function
 */
exports.BytesFormatFilterFactory = function () {
    return function (bytes, precision, plusSign) {
        if (!isFinite(bytes)) {
            return "---";
        }
        precision = precision || 1;
        var prefix = plusSign && bytes > 0 ? "+" : "";
        var units = ["B", "kB", "MB", "GB", "TB", "PB"];
        var order = Math.floor(Math.log(Math.abs(bytes)) / Math.log(kilo));
        var value = bytes / Math.pow(kilo, Math.floor(order));
        // fix the precision edge case (we store bytes as float in the backend)
        var valueCeiled = Math.ceil(value);
        if (valueCeiled % kilo === 0) {
            value = valueCeiled / kilo;
            order += 1;
        }
        var strValue = (value).toFixed(precision);
        // remove trailing zeros
        var matches = strValue.match(/([0-9]+)\.0*$/);
        if (matches != null && matches.length === 2) {
            strValue = matches[1];
        }
        return prefix + strValue + " " + units[order];
    };
};


/***/ }),
/* 192 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Number format filter factory implemenatation
 *
 * @function
 */
exports.NumberFormatFilterFactory = function () {
    return function (input, precision, suffix, plusSign) {
        precision = angular.isNumber(precision) ? precision : 2;
        suffix = suffix || "%";
        var prefix = plusSign && input > 0 ? "+" : "";
        if (isNaN(input)) {
            return "---";
        }
        return prefix + Math.round(input * Math.pow(10, precision)) / Math.pow(10, precision) + suffix;
    };
};


/***/ }),
/* 193 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(194);
var index_2 = __webpack_require__(199);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
};


/***/ }),
/* 194 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(195);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 195 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(196);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 196 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(197);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 197 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var createConstraint_dialog_controller_1 = __webpack_require__(198);
exports.default = function (module) {
    module.controller("CreateConstraintDialogController", createConstraint_dialog_controller_1.CreateConstraintDialogController);
};


/***/ }),
/* 198 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var routeNames_1 = __webpack_require__(19);
var index_1 = __webpack_require__(0);
var di_1 = __webpack_require__(1);
var CreateConstraintDialogController = /** @class */ (function () {
    function CreateConstraintDialogController($scope, $q, xuiToastService, routingService, swApiService, pluggabilityService, orionInfoService, dateTimeService, statusService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$q = $q;
        this.xuiToastService = xuiToastService;
        this.routingService = routingService;
        this.swApiService = swApiService;
        this.pluggabilityService = pluggabilityService;
        this.orionInfoService = orionInfoService;
        this.dateTimeService = dateTimeService;
        this.statusService = statusService;
        this._t = _t;
        this.$onInit = function () {
            _this.constraintsHelpLink = "#";
            _this.scope = _this.$scope;
            _this.pluggabilityService.getActionIconDelegate().then(function (d) {
                _this.getActionIconDelegate = d;
            });
            _this.scope.$on("$stateChangeSuccess", _this.handleStateChangeSuccess);
            _this.expirationDate = null;
            return _this.getConstraintsHelpLink();
        };
        this.initByViewModel = function (dialogInstance) {
            _this.dialogInstance = dialogInstance;
            _this.viewModel = dialogInstance.dialogOptions.viewModel;
            _this.constraintsPickerContext = { recommendation: _this.viewModel.recommendation };
            _this.viewModel.handleCreateConstraintButtonClick = _this.createConstraint;
        };
        this.getActionIcon = function () {
            // check if already present
            if (!_this.getActionIconDelegate) {
                return "";
            }
            var primaryAction = _this.viewModel.recommendation.actions[0];
            var actionIcon = _this.getActionIconDelegate(primaryAction.type);
            return actionIcon;
        };
        this.getConstraintsHelpLink = function () {
            return _this.orionInfoService.getHelpUrl("OrionVIM_RecommendationsManageConstraints").then(function (result) {
                _this.constraintsHelpLink = result;
            });
        };
        this.handleStateChangeSuccess = function (e, to, toParams, from, fromParams) {
            if (to.name === routeNames_1.RouteNames.constraintsStateName) {
                _this.dialogInstance.cancel();
                if (_this.viewModel.parentDialog != null) {
                    _this.viewModel.parentDialog.cancel();
                }
            }
        };
        this.createConstraint = function () {
            var expirationDate;
            if (_this.expirationDate) {
                var expirationDateString = _this.dateTimeService.moveUserInputToOrionDateEndOfTheDay(_this.expirationDate);
                expirationDate = _this.dateTimeService.convertStringToUtc(expirationDateString);
            }
            var constraints = _this.constraintsPickerContext.getConstraints();
            if (!constraints || constraints.length === 0) {
                _this.xuiToastService.error(_this._t("There are no policies to create."));
                return _this.$q.resolve(false);
            }
            _.forEach(constraints, function (constraint) {
                constraint.expirationDate = expirationDate;
            });
            return _this.swApiService.insertConstraintsForRecommendation(_this.viewModel.recommendation.id, constraints).then(function (result) {
                if (result) {
                    _this.xuiToastService.success(_this._t("Constraints modification successfull."));
                    return true;
                }
                else {
                    _this.xuiToastService.error(_this._t("Constraints modification failed."));
                    return false;
                }
            });
        };
    }
    CreateConstraintDialogController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(2, di_1.Inject(index_1.ServiceNames.xuiToastService)),
        __param(3, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(4, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(5, di_1.Inject(index_1.ServiceNames.pluggabilityService)),
        __param(6, di_1.Inject(index_1.ServiceNames.orionInfoService)),
        __param(7, di_1.Inject(index_1.ServiceNames.dateTimeService)),
        __param(8, di_1.Inject(index_1.ServiceNames.statusService)),
        __param(9, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Function, Object, Object, Object, Object, Object, Object, Object, Function])
    ], CreateConstraintDialogController);
    return CreateConstraintDialogController;
}());
exports.CreateConstraintDialogController = CreateConstraintDialogController;


/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(200);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(201);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(202);
var index_2 = __webpack_require__(205);
var index_3 = __webpack_require__(207);
var index_4 = __webpack_require__(209);
var index_5 = __webpack_require__(211);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
};


/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var activeDetail_dialog_controller_1 = __webpack_require__(203);
exports.default = function (module) {
    module.controller("ActiveDetailDialogController", activeDetail_dialog_controller_1.ActiveDetailDialogController);
};


/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var detail_dialog_controller_1 = __webpack_require__(14);
/**
 * Active recommendation detail dialog controller
 *
 * @class
 * @extends RecommendationDetailDialogController
 */
var ActiveDetailDialogController = /** @class */ (function (_super) {
    __extends(ActiveDetailDialogController, _super);
    function ActiveDetailDialogController($scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) {
        return _super.call(this, $scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) || this;
    }
    /**
     * Initializes the callbacks which are trigerred whenever the primary dialog button is clicked
     */
    ActiveDetailDialogController.prototype.init = function (dialogInstance, loadDependencies, allowedChange) {
        this.viewModel = dialogInstance.dialogOptions.viewModel;
        this.viewModel.getAppliedActionsCallback = this.getAppliedActions;
        return _super.prototype.init.call(this, dialogInstance, loadDependencies, allowedChange);
    };
    ;
    ActiveDetailDialogController.$inject = detail_dialog_controller_1.RecommendationDetailDialogController.$inject;
    return ActiveDetailDialogController;
}(detail_dialog_controller_1.RecommendationDetailDialogController));
exports.ActiveDetailDialogController = ActiveDetailDialogController;


/***/ }),
/* 204 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * TypeGuard for IActionDetails
 *
 * @function
 */
exports.TypeGuard = function (actionDetails) {
    return actionDetails && actionDetails.ActionID && actionDetails.RecommendationID;
};


/***/ }),
/* 205 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var activeMultipleDetail_dialog_controller_1 = __webpack_require__(206);
exports.default = function (module) {
    module.controller("ActiveMultipleDetailDialogController", activeMultipleDetail_dialog_controller_1.ActiveMultipleDetailDialogController);
};


/***/ }),
/* 206 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var detail_dialog_controller_1 = __webpack_require__(14);
/**
 * Controller for apply multiple recommendations dialog
 *
 * @class
 * @extends RecommendationDetailDialogController
 */
var ActiveMultipleDetailDialogController = /** @class */ (function (_super) {
    __extends(ActiveMultipleDetailDialogController, _super);
    function ActiveMultipleDetailDialogController($scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) {
        return _super.call(this, $scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) || this;
    }
    ActiveMultipleDetailDialogController.prototype.loadRecommendationsOnInit = function () {
        var _this = this;
        // load all selected recommendations
        var ids = _.map(this.dialogInstance.dialogOptions.viewModel.recommendations, function (r) { return r.id; });
        return this.loadRecommendations(ids, this.loadDependencies).then(function () {
            _this.isLoaded = true;
        });
    };
    ;
    /**
     * Initializes the callbacks which are trigerred whenever the primary dialog button is clicked
     */
    ActiveMultipleDetailDialogController.prototype.init = function (dialogInstance, loadDependencies, allowedChange) {
        this.isLoaded = false;
        this.viewModel = dialogInstance.dialogOptions.viewModel;
        this.viewModel.getAppliedActionsCallback = this.getAppliedActions;
        return _super.prototype.init.call(this, dialogInstance, loadDependencies, allowedChange);
    };
    ;
    /**
     * Creates instance of multiple recommendations status change checker
     */
    ActiveMultipleDetailDialogController.prototype.createStatusChangeChecker = function () {
        return this.statusService.createRecommendationsStatusChangeChecker(this.recommendations, this.onStatusChanged);
    };
    ;
    /**
     * Creates multiple referrer
     */
    ActiveMultipleDetailDialogController.prototype.createReferrer = function () {
        return {
            recommendations: this.recommendations
        };
    };
    ;
    ActiveMultipleDetailDialogController.$inject = detail_dialog_controller_1.RecommendationDetailDialogController.$inject;
    return ActiveMultipleDetailDialogController;
}(detail_dialog_controller_1.RecommendationDetailDialogController));
exports.ActiveMultipleDetailDialogController = ActiveMultipleDetailDialogController;


/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var finishedDetail_dialog_controller_1 = __webpack_require__(208);
exports.default = function (module) {
    module.controller("FinishedDetailDialogController", finishedDetail_dialog_controller_1.FinishedDetailDialogController);
};


/***/ }),
/* 208 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var detail_dialog_controller_1 = __webpack_require__(14);
var enums_1 = __webpack_require__(2);
/**
 * Controller for finished detail dialog
 *
 * @class
 * @extends RecommendationDetailDialogController
 */
var FinishedDetailDialogController = /** @class */ (function (_super) {
    __extends(FinishedDetailDialogController, _super);
    function FinishedDetailDialogController($scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) {
        var _this = _super.call(this, $scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) || this;
        _this.loadRevertActions = function () {
            return _this.swisService.getRevertBatchWithActions(_this.initialRecommendation.batch.id)
                .then(function (revertBatch) {
                if (!revertBatch) {
                    // there are no revert action to show
                    return;
                }
                // set to each BatchAction revert-action template
                var actions = revertBatch.actions;
                _.each(actions, function (a) {
                    a.$templateUrl = "revert-action-item-template";
                });
                // concat ActionDetails with revert BatchAction 
                _this.filteredActions = _this.filteredActions.concat(actions);
            });
        };
        return _this;
    }
    FinishedDetailDialogController.prototype.init = function (dialogInstance, loadDependencies, allowedChange) {
        return _super.prototype.init.call(this, dialogInstance, loadDependencies, allowedChange);
    };
    ;
    FinishedDetailDialogController.prototype.filterActions = function (actions) {
        return _.filter(actions, function (a) { return a.Status === enums_1.ActionStatus.FinishedSuccessfully
            || a.Status === enums_1.ActionStatus.FinishedWithError
            || a.Status === enums_1.ActionStatus.Canceled; });
    };
    ;
    FinishedDetailDialogController.prototype.onFilteredActionsUpdated = function () {
        if (this.initialRecommendation.batch.revertBatchId) {
            this.loadRevertActions();
        }
    };
    ;
    FinishedDetailDialogController.$inject = detail_dialog_controller_1.RecommendationDetailDialogController.$inject;
    return FinishedDetailDialogController;
}(detail_dialog_controller_1.RecommendationDetailDialogController));
exports.FinishedDetailDialogController = FinishedDetailDialogController;


/***/ }),
/* 209 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var runningDetail_dialog_controller_1 = __webpack_require__(210);
exports.default = function (module) {
    module.controller("RunningDetailDialogController", runningDetail_dialog_controller_1.RunningDetailDialogController);
};


/***/ }),
/* 210 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var running_controller_base_1 = __webpack_require__(36);
/**
 * Controller for running detail dialog.
 *
 * @class
 * @extends RunningRecommendationsControllerBase
 */
var RunningDetailDialogController = /** @class */ (function (_super) {
    __extends(RunningDetailDialogController, _super);
    function RunningDetailDialogController($scope, $rootScope, $interval, $compile, batchingService, $q, swApiService, swisService, demoService) {
        var _this = _super.call(this, $q, swApiService, swisService, demoService) || this;
        _this.$scope = $scope;
        _this.$rootScope = $rootScope;
        _this.$interval = $interval;
        _this.$compile = $compile;
        _this.batchingService = batchingService;
        _this.init = function (dialogInstance) {
            _this.viewModel = dialogInstance.dialogOptions.viewModel;
            _this.recommendation = _this.viewModel.recommendation;
            _this.context = _this.viewModel.context;
            _this.intervalPromise = _this.$interval(_this.refreshStatus, 3000);
            _this.$scope.$on("$destroy", function () {
                _this.$interval.cancel(_this.intervalPromise);
            });
            _this.viewModel.cancelAndRevertCallback = _this.cancelAndRevertCurrent;
        };
        _this.refreshStatus = function () {
            _this.refreshRecommendationStatus(_this.recommendation);
        };
        _this.isRunning = function () {
            return _this.recommendation.batch.isRunning();
        };
        _this.cancelAndRevertCurrent = function () {
            return _this.batchingService.cancelAndRevertRecommendation(_this.recommendation);
        };
        return _this;
    }
    RunningDetailDialogController.$inject = [
        "$scope",
        "$rootScope",
        "$interval",
        "$compile",
        index_1.ServiceNames.batchingService,
        "$q",
        index_1.ServiceNames.swApiService,
        index_1.ServiceNames.swisService,
        index_1.ServiceNames.demoService
    ];
    return RunningDetailDialogController;
}(running_controller_base_1.RunningRecommendationsControllerBase));
exports.RunningDetailDialogController = RunningDetailDialogController;


/***/ }),
/* 211 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var scheduledDetail_dialog_controller_1 = __webpack_require__(212);
exports.default = function (module) {
    module.controller("ScheduledDetailDialogController", scheduledDetail_dialog_controller_1.ScheduledDetailDialogController);
};


/***/ }),
/* 212 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var detail_dialog_controller_1 = __webpack_require__(14);
/**
 * Controller for scheduled recommendation detail.
 *
 * @class
 * @extends RecommendationDetailDialogController
 */
var ScheduledDetailDialogController = /** @class */ (function (_super) {
    __extends(ScheduledDetailDialogController, _super);
    function ScheduledDetailDialogController($scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) {
        return _super.call(this, $scope, $rootScope, $log, $q, $compile, $timeout, statusService, swApiService, swisService, pluggabilityService, _t, dialogService, dateTimeService) || this;
    }
    ScheduledDetailDialogController.prototype.filterActions = function (actions) {
        // show only actions having status (i.e. are scheduled)
        return _.filter(actions, function (a) { return !!a.Status; });
    };
    ;
    ScheduledDetailDialogController.$inject = detail_dialog_controller_1.RecommendationDetailDialogController.$inject;
    return ScheduledDetailDialogController;
}(detail_dialog_controller_1.RecommendationDetailDialogController));
exports.ScheduledDetailDialogController = ScheduledDetailDialogController;


/***/ }),
/* 213 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(214);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 214 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(215);
var index_2 = __webpack_require__(228);
var index_3 = __webpack_require__(252);
var index_4 = __webpack_require__(255);
var index_5 = __webpack_require__(264);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
    index_5.default(module);
};


/***/ }),
/* 215 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(216);
var index_2 = __webpack_require__(219);
var index_3 = __webpack_require__(222);
var index_4 = __webpack_require__(225);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
};


/***/ }),
/* 216 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var pluginTemplateUrls_1 = __webpack_require__(6);
var actionDecorator_controller_1 = __webpack_require__(217);
var configTemplate = function ($templateCache) {
    $templateCache.put(pluginTemplateUrls_1.PluginTemplateUrls.recommendationDetailActionItem, __webpack_require__(218));
};
exports.default = function (module) {
    module.controller("VimActionDecoratorController", actionDecorator_controller_1.ActionDecoratorController);
    module.app().run([index_1.ServiceNames.ngTemplateCacheService, configTemplate]);
};


/***/ }),
/* 217 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
// VIM
var index_2 = __webpack_require__(3);
var enums_1 = __webpack_require__(4);
/**
 * VIM pluggable action decorator controller
 *
 * @class
 */
var ActionDecoratorController = /** @class */ (function () {
    function ActionDecoratorController($scope, $q, $timeout, vimVmInfoService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$q = $q;
        this.$timeout = $timeout;
        this.vimVmInfoService = vimVmInfoService;
        this._t = _t;
        /**
         * Initializes decorator upon the given context
         */
        this.init = function (context) {
            _this.isLoaded = false;
            _this.applications = [];
            _this.currentAction = context.item;
            _this.actions = context.actions;
            _this.currentActionType = enums_1.ManagementActionType[_this.currentAction.Type];
            _this.currentActionIndex = _.findIndex(_this.actions, function (a) { return a.ActionID === _this.currentAction.ActionID; });
            _this.poweringOffActions = [enums_1.ManagementActionType.ChangeSettings, enums_1.ManagementActionType.PerformMigration, enums_1.ManagementActionType.PerformRelocation];
            var siblings = _this.getPowerActionsSiblings();
            _this.powerControlAllowed = _this.isPowerControlAllowed(siblings);
            if (_this.powerControlAllowed) {
                return _this.powerControlInit(siblings)
                    .then(function () {
                    _this.isLoaded = true;
                });
            }
            else if (_this.displayMonitoredApplicationsComment()) {
                return _this.loadMonitoredApplications()
                    .then(function () {
                    _this.isLoaded = true;
                });
            }
            else {
                _this.isLoaded = true;
                return _this.$q.resolve();
            }
        };
        /**
         * Init for actions with power control allowed
         */
        this.powerControlInit = function (siblings) {
            _this.powerActionsSiblings = siblings;
            // set checkbox value based on value from preceding power on/off settings
            _this.powerControlEnabled = _this.powerActionsSiblings[0].Enabled;
            return _this.$q.resolve();
        };
        /**
         * Reacts on power control change
         */
        this.onPowerControlChanged = function () {
            // call in timeout to ensure digest() has been called
            _this.$timeout(_this.updateSiblingsEnabled);
        };
        /**
         * Gets additional comment of displayed action
         */
        this.getActionComment = function () {
            if (_this.currentActionType === enums_1.ManagementActionType.ChangeSettings) {
                return _this._t("Power off the VM, reconfigure, and then power on the VM.");
            }
            if (_this.currentActionType === enums_1.ManagementActionType.PerformRelocation) {
                return _this._t("Power off the VM, relocate, and then power on the VM.");
            }
            if (_this.currentActionType === enums_1.ManagementActionType.PerformMigration) {
                return _this._t("Power off the VM, migrate, and then power on the VM.");
            }
            return null;
        };
        /**
         * Updates siblings enabled status
         */
        this.updateSiblingsEnabled = function () {
            _this.$scope.$emit("actionsUpdateEnabled", _this.powerActionsSiblings, _this.powerControlEnabled);
        };
        /**
         * Indicates whether to allow user to change power on/off setting
         */
        this.isPowerControlAllowed = function (siblings) {
            var isPoweringOffAction = _this.poweringOffActions.indexOf(_this.currentActionType) >= 0;
            return isPoweringOffAction && !_.isEmpty(siblings);
        };
        /**
         * Gets optional power on/off sibling actions to the current action
         *
         * @returns Returns null if not found, otherwise array with preceding and following action
         */
        this.getPowerActionsSiblings = function () {
            var hasSiblings = _this.currentActionIndex > 0 && _this.currentActionIndex < (_this.actions.length - 1);
            if (!hasSiblings) {
                return null;
            }
            var predecessor = _this.actions[_this.currentActionIndex - 1];
            var successor = _this.actions[_this.currentActionIndex + 1];
            if (!predecessor.Optional
                || predecessor.Type !== enums_1.ManagementActionType[enums_1.ManagementActionType.PowerOff]
                || !successor.Optional
                || successor.Type !== enums_1.ManagementActionType[enums_1.ManagementActionType.PowerOn]) {
                return null;
            }
            return [_this.actions[_this.currentActionIndex - 1], _this.actions[_this.currentActionIndex + 1]];
        };
        /**
         * Indicates whether to show info regarding powering off the VM
         */
        this.displayMonitoredApplicationsComment = function () {
            return _this.currentActionType === enums_1.ManagementActionType.PowerOff;
        };
        /**
         * Loads monitored apps on current VM
         */
        this.loadMonitoredApplications = function () {
            return _this.vimVmInfoService.getMonitoredApplicationsByVm(_this.currentAction.Details.VirtualMachineId)
                .then(function (apps) {
                _this.applications = apps;
            });
        };
    }
    ActionDecoratorController.$inject = [
        index_1.ServiceNames.ngScope,
        index_1.ServiceNames.ngQService,
        index_1.ServiceNames.ngTimeoutService,
        index_2.ServiceNames.vmInfoService,
        index_1.ServiceNames.getTextService
    ];
    return ActionDecoratorController;
}());
exports.ActionDecoratorController = ActionDecoratorController;


/***/ }),
/* 218 */
/***/ (function(module, exports) {

module.exports = "<div class=sw-vim-detail-dialog-action-decorator ng-controller=\"VimActionDecoratorController as vm\" ng-init=vm.init(context)> <div ng-if=vm.isLoaded> <div class=pull-right ng-if=::vm.powerControlAllowed> <xui-checkbox name=powerControl_{{vm.currentAction.ActionID}} ng-model=vm.powerControlEnabled ng-change=vm.onPowerControlChanged()> <span class=checkbox-label> {{::vm.getActionComment()}} </span> </xui-checkbox> </div> <div class=application-shutdown-warning ng-if=::vm.displayMonitoredApplicationsComment()> <div ng-if=\"vm.applications.length > 0\"> <abbr xui-popover=\"_t(All applications)\" xui-popover-trigger=mouseenter xui-popover-placement=left _ta> <span ng-repeat=\"app in vm.applications\"><span ng-if=\"$index > 0\">, </span>{{::app.Name}}</span> </abbr> <span _t>on the VM will be shut down.</span> </div> <div ng-if=\"vm.applications.length === 0\" _t> All applications on the VM will be shut down. </div> </div> </div> </div> ";

/***/ }),
/* 219 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var pluginTemplateUrls_1 = __webpack_require__(6);
var actionsSummaryDecorator_controller_1 = __webpack_require__(220);
var configTemplate = function ($templateCache) {
    $templateCache.put(pluginTemplateUrls_1.PluginTemplateUrls.recommendationDetailActionsSummary, __webpack_require__(221));
};
exports.default = function (module) {
    module.controller("VimActionsSummaryDecorator", actionsSummaryDecorator_controller_1.ActionsSummaryDecoratorController);
    module.app().run([index_1.ServiceNames.ngTemplateCacheService, configTemplate]);
};


/***/ }),
/* 220 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// VIM
var enums_1 = __webpack_require__(4);
/**
 * Actions summary decorator
 *
 * @class
 */
var ActionsSummaryDecoratorController = /** @class */ (function () {
    function ActionsSummaryDecoratorController($scope, _t) {
        var _this = this;
        this.$scope = $scope;
        this._t = _t;
        /**
         * Initializes summary upon given context promise
         *
         * @param {IPromise} contextPromise Context promise which is expected to return context in future
         */
        this.init = function (contextPromise) {
            _this.isLoaded = false;
            _this.isImportant = false;
            contextPromise.then(function (value) {
                _this.initContext(value);
                // hook on context change event
                _this.$scope.$on("actionSummaryContextChanged", _this.onContextChanged);
            });
        };
        this.onContextChanged = function (e, context) {
            _this.initContext(context);
        };
        /**
         * Updates context with given instance and also updates some relevant data
         */
        this.initContext = function (context) {
            _this.context = context;
            _this.generateMessage();
            _this.isLoaded = true;
        };
        /**
         * Generates the message regarding actions with PowerOff type
         */
        this.generateMessage = function () {
            _this.isImportant = false;
            var powerOffCount = _this.getPoweredOffActionsCount();
            if (powerOffCount === 0) {
                if (_this.context.recommendations.length === 1) {
                    _this.message = _this._t("This recommendation will be attempted without shutting down any VMs.Please ensure that all hypervisors, guest OSes or applications running on guest OSes support 'hot' configuration changes.");
                }
                else {
                    _this.message = _this._t("These recommendations will be attempted without shutting down any VMs. Please ensure that all hypervisors, guest OSes or applications running on guest OSes support 'hot' configuration changes.");
                }
            }
            else if (powerOffCount === 1) {
                _this.isImportant = true;
                _this.message = _this._t("{0} VM will be powered off during this recommendation.").format(powerOffCount);
            }
            else {
                _this.isImportant = true;
                _this.message = _this._t("{0} VMs will be powered off during this recommendation.").format(powerOffCount);
            }
        };
        /**
         * Computes count of actions with PowerOff type
         */
        this.getPoweredOffActionsCount = function () {
            var count = 0;
            _.each(_this.context.actions, function (a) {
                if (a.Enabled && a.Type === enums_1.ManagementActionType[enums_1.ManagementActionType.PowerOff]) {
                    count++;
                }
            });
            return count;
        };
    }
    ActionsSummaryDecoratorController.$inject = [
        "$scope",
        "getTextService"
    ];
    return ActionsSummaryDecoratorController;
}());
exports.ActionsSummaryDecoratorController = ActionsSummaryDecoratorController;
;


/***/ }),
/* 221 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"VimActionsSummaryDecorator as vm\" ng-init=vm.init(context) class=action-summary-decorator-vim> <xui-message ng-if=vm.isLoaded type=info> <span class=summary-message ng-class=\"{'bold': vm.isImportant}\" ng-bind-html=vm.message> </span> </xui-message> </div> ";

/***/ }),
/* 222 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var pluginTemplateUrls_1 = __webpack_require__(6);
var outcomeDecorator_controller_1 = __webpack_require__(223);
var configTemplate = function ($templateCache) {
    $templateCache.put(pluginTemplateUrls_1.PluginTemplateUrls.recommendationDetailOutcomeItem, __webpack_require__(224));
};
exports.default = function (module) {
    module.controller("VimOutcomeDecoratorController", outcomeDecorator_controller_1.OutcomeDecoratorController);
    module.app().run([index_1.ServiceNames.ngTemplateCacheService, configTemplate]);
};


/***/ }),
/* 223 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// REC
var index_1 = __webpack_require__(0);
// VIM
var constants_1 = __webpack_require__(5);
;
;
/**
 * Outcome decorator controller
 *
 * @class
 */
var OutcomeDecoratorController = /** @class */ (function () {
    function OutcomeDecoratorController($scope, $log, $filter, swApi, dateTimeService, swisService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$filter = $filter;
        this.swApi = swApi;
        this.dateTimeService = dateTimeService;
        this.swisService = swisService;
        this._t = _t;
        this.ticksLength = 70;
        this.isChartVisible = false;
        this.justifications = [];
        this.zoomOptions = [
            { title: this._t("Last month"), value: 31, stepMinutes: 120 },
            { title: this._t("Last 2 weeks"), value: 14, stepMinutes: 60 },
            { title: this._t("Last week"), value: 7, stepMinutes: 30 },
            { title: this._t("Last 24 hours"), value: 1, stepMinutes: 10 }
        ];
        this.chartGroups = [];
        this.visibleCounters = [];
        this.isDisplayingData = false;
        this.customZoomSelected = false;
        /**
         * Gets overall outcome string
         */
        this.getOutcome = function () {
            var entityJustification = _.last(_this.justifications);
            if (!entityJustification || !entityJustification.Resolution || entityJustification.Resolution.length === 0) {
                return "";
            }
            return entityJustification.Resolution[0].Texts[0];
        };
        /**
         * Gets currently selected predefined zoom or custom if selected by user
         */
        this.getCurrentZoomOptions = function () {
            if (!_this.customZoomSelected) {
                return _this.selectedZoom;
            }
            var extremes = _this.chart.xAxis[0].getExtremes();
            // compute days
            var daysToLoad = Math.ceil(moment.duration(_this.orionNowMiliseconds - extremes.min).asDays());
            // compute step
            var numberOfWeeks = Math.ceil(moment.duration(extremes.max - extremes.min).asWeeks());
            var step = 10 + (numberOfWeeks * 10);
            //return this.selectedZoom;
            return { title: _this._t("Custom zoom"), value: daysToLoad, stepMinutes: step, isCustom: true };
        };
        /**
         * Reloads chart upon selected chart group
         */
        this.reloadChart = function () {
            var zoom = _this.getCurrentZoomOptions();
            var request = {
                AllSettings: {
                    VimEntities: [],
                    ResourceProperties: {}
                },
                DaysOfDataToLoad: zoom.value,
                SampleSizeInMinutes: zoom.stepMinutes,
                NetObjectIds: [],
                SeriesTypes: []
            };
            var objType = _this.entity.Id.Id.split(":")[0];
            _.each(_this.selectedChartGroup.Counters, function (ctr) {
                request.AllSettings.VimEntities.push({
                    Name: ctr.Name,
                    ObjectId: _this.entity.ObjectId,
                    NetObjectPrefix: objType,
                    Counter: ctr.Name
                });
                request.SeriesTypes.push("line");
            });
            return _this.swApi.ws.oneUrl("../VIM/Services/ChartData.asmx", null).post("MultiStatisticData", { request: request })
                .then(function (result) {
                _this.isDisplayingData = true;
                // remove old series
                _this.removeChartSeries();
                _this.orionNowMiliseconds = _this.dateTimeService.getOrionNowMiliseconds();
                _this.displayResult(result.d.DataSeries, zoom);
                _this.isDisplayingData = false;
            });
        };
        /**
         * Finds chart relevant for current entity
         */
        this.findChart = function () {
            return _.findLast(Highcharts.charts, function (x) {
                return x && x.options.tag === _this.chartConfig.tag;
            });
        };
        /**
         * Updates X axis after chart has its data configured
         */
        this.updateHorizontalAxisOptions = function () {
            if (!_this.anyVisibleCounterHasPredictedData()) {
                return;
            }
            // if we have predicted data: show 'now' indicator
            _this.chart.xAxis[0].update({
                plotLines: [
                    {
                        color: "#909090",
                        width: 1,
                        value: _this.orionNowMiliseconds,
                        zIndex: 5,
                        label: {
                            text: _this._t("Now"),
                            align: "center",
                            rotation: 0,
                            x: 0,
                            y: -5,
                            style: { color: "#909090" }
                        }
                    }
                ]
            });
        };
        /**
         * Gets a flag indicating whether any visible counter has predicted data
         */
        this.anyVisibleCounterHasPredictedData = function () {
            return _.some(_this.visibleCounters, function (c) { return c.hasPredictedData; });
        };
        /**
         * Updates Y axis after chart has its data configured
         */
        this.updateVerticalAxisMinMaxValues = function () {
            if (_this.selectedChartGroup.Units === "%") {
                _this.chart.yAxis[0].update({
                    min: 0,
                    max: 100,
                    tickPositions: null
                });
            }
            else {
                var extremes = _this.chart.yAxis[0].getExtremes();
                if (!extremes.dataMin || !extremes.dataMax) {
                    _this.chart.yAxis[0].update({
                        min: null,
                        max: null,
                        tickPositions: null
                    });
                    return;
                }
                // handling special case when min/max is nearly the same (i.e. difference is less than 5%)
                var isAlmostConstant = (extremes.dataMax / extremes.dataMin) - 1 < 0.05;
                var positions = [];
                var start = void 0, end = void 0;
                if (isAlmostConstant) {
                    start = Math.round(extremes.dataMin * 0.95);
                    end = Math.round(extremes.dataMax * 1.05);
                }
                else {
                    var diff = (extremes.dataMax - extremes.dataMin) / 2;
                    // add padding by increment so min/max does not hit chart borders
                    start = extremes.dataMin - diff;
                    end = extremes.dataMax + diff;
                    // in case start is less than zero, move whole range
                    if (start < 0) {
                        end += Math.abs(start);
                        start = 0;
                    }
                }
                // in case max is over capacity or max is near the capacity => set max to capacity
                var capacity = _this.getEntityCapacity();
                if (capacity && (capacity < end || end >= (capacity * 0.9))) {
                    end = capacity;
                }
                var increment = (end - start) / 4;
                if (increment > 0) {
                    for (var i = start; i <= end; i += increment) {
                        positions.push(i);
                    }
                }
                // note: chart creates extra padding so min/max values do not hit the chart borders
                _this.chart.yAxis[0].update({
                    min: null,
                    max: null,
                    tickPositions: positions
                });
            }
        };
        /**
         * Gets entity capacity in bytes
         */
        this.getEntityCapacity = function () {
            if (_this.entity.Id.Id.indexOf(constants_1.EntityTypes.dataStore) >= 0) {
                return _this.entity.Resources["Capacity.0"].Capacity;
            }
            return null;
        };
        /**
         * Sets chart extremes according currently selected zoom
         */
        this.updateZoom = function (zoom) {
            _this.chart.zoomOut();
            var zoomedMiliseconds = moment.duration({ days: zoom.value }).asMilliseconds();
            // zoom is applied only for x axis, thus reset is done for this axis
            _this.chart.xAxis[0].setExtremes(_this.orionNowMiliseconds - zoomedMiliseconds, null, false, false);
        };
        /**
         * Event of changing predefined zoom
         */
        this.onZoomChanged = function () {
            _this.customZoomSelected = false;
            _this.reloadChart();
        };
        this.onChartGroupChanged = function () {
            _this.selectDefaultZoom();
            _this.reloadChart();
        };
        this.formatValue = function (n, decimals) {
            var units = _this.selectedChartGroup.Units;
            var base = 1000;
            if (_this.selectedChartGroup.Units === "B") {
                base = 1024;
            }
            var numSymbols = _this.chart.options.lang.numericSymbols;
            // try to find units prefix (e.g. "M" in case of MB)
            var unitsPrefix = "";
            for (var x = numSymbols.length; x >= 1; x--) {
                if (n > Math.pow(base, x)) {
                    unitsPrefix = numSymbols[x - 1];
                    n /= Math.pow(base, x);
                    break;
                }
            }
            // do not display units in case of zero
            if (n === 0) {
                return "0";
            }
            return Highcharts.numberFormat(n, decimals) + "\u00A0" + unitsPrefix + units;
        };
        this.getStatus = function (value, threshold) {
            if (!threshold) {
                return "";
            }
            if (threshold.Critical && value > threshold.Critical) {
                return "critical";
            }
            if (threshold.Warning && value > threshold.Warning) {
                return "warning";
            }
            return "";
        };
        this.findThresholdByThresholdSuffix = function (thresholdSuffix) {
            if (!thresholdSuffix) {
                return null;
            }
            var threshold = _.find(_this.entity.Thresholds, function (t) { return _.endsWith(t.ThresholdName, thresholdSuffix); });
            if (!threshold) {
                return null;
            }
            // SpaceUsage threshold is in % but all other values are in bytes, so we need to convert to bytes too
            if (_.endsWith(threshold.ThresholdName, ".SpaceUsage")) {
                var thresholdCopy = _.cloneDeep(threshold);
                var capacity = _this.getEntityCapacity();
                thresholdCopy.Critical = thresholdCopy.Critical / 100 * capacity;
                thresholdCopy.Warning = thresholdCopy.Warning / 100 * capacity;
                return thresholdCopy;
            }
            return threshold;
        };
        this.findPredictionForCounter = function (counterName) {
            var entityJustification = _.findLast(_this.justifications, function (justification) {
                return justification.Predictions && justification.Predictions[counterName];
            });
            if (!entityJustification) {
                return null;
            }
            return entityJustification.Predictions[counterName];
        };
        this.getThresholdPrediction = function (prediction, status) {
            if (!prediction) {
                return [];
            }
            var predictions = [];
            if (prediction.WarningTime && status === "") {
                var warningTime = _this.dateTimeService.convertStringToUtc(prediction.WarningTime);
                predictions.push(_this.createThresholdPrediction("reach warning threshold", warningTime));
            }
            if (prediction.CriticalTime && (!status || status === "warning")) {
                var criticalTime = _this.dateTimeService.convertStringToUtc(prediction.CriticalTime);
                predictions.push(_this.createThresholdPrediction("reach critical threshold", criticalTime));
            }
            if (prediction.AtCapacityTime) {
                var atCapacityTime = _this.dateTimeService.convertStringToUtc(prediction.AtCapacityTime);
                predictions.push(_this.createThresholdPrediction("be at capacity", atCapacityTime));
            }
            return predictions;
        };
        this.createThresholdPrediction = function (name, depletionTime) {
            var diff = depletionTime.valueOf() - _this.dateTimeService.getNowMiliseconds();
            // less than 14 days: set warning status
            return {
                name: name,
                days: diff / moment.duration({ days: 1 }).asMilliseconds(),
                status: diff < moment.duration({ days: 14 }).asMilliseconds() ? "warning" : "none",
                timeDescription: _this.formatDays(diff)
            };
        };
        this.formatDays = function (diff) {
            var diffDays = Math.floor(diff / (24 * 36e5));
            if (diffDays < 1) {
                return _this._t("in 0 days");
            }
            if (diffDays === 1) {
                return _this._t("in 1 day");
            }
            if (diffDays <= 7) {
                return _this._t("in {0} days").format(diffDays);
            }
            // TODO: VIM-1989
            return _this._t("in more than week");
        };
        this.createSerie = function (name, data, color) {
            return {
                name: name,
                enableMouseTracking: false,
                marker: { enabled: false },
                lineWidth: 1,
                data: data,
                color: color
            };
        };
        /**
         * Adds constant predicted 12h long line based on first predicted value
         */
        this.createPredictedSerie = function (value, label, color) {
            var data = [];
            var i = 0;
            while (i < 12) {
                data.push([_this.orionNowMiliseconds + (36e5 * i), value]);
                i++;
            }
            return _this.createSerie(label, data, color);
        };
        this.processPredictedUsageData = function (points) {
            var data = [];
            _.each(points, function (point) {
                var time = parseInt(point[0], 10);
                var value = point.length < 2 ? null : parseFloat(point[1]);
                // add only future data (don't allow serie overlapping)
                if (time > _this.orionNowMiliseconds) {
                    if (data.length === 0) {
                        // fill the gap between 'now' and first prediction
                        data.push([_this.orionNowMiliseconds, value]);
                    }
                    data.push([time, value]);
                }
            });
            return data;
        };
        /**
         * Removes all existing series
         */
        this.removeChartSeries = function () {
            while (_this.chart.series.length > 0) {
                _this.chart.series[0].remove(false);
            }
        };
        this.displayResult = function (series, zoom) {
            // these pairs of colors correspond to applied vs. not applied recommendation
            var colors = [["#1f77b4", "#6baed6"], ["#a55194", "#e377c2"], ["#118F9C", "#17becf"], ["#6b6ecf", "#8B87BE"]];
            _this.visibleCounters = [];
            var colorIndex = 0;
            _.each(series, function (singleSeries) {
                var anyValidPoint = false;
                var lastValidValue;
                // single-length data point arrays break highcharts!
                _.each(singleSeries.Data, function (dataPoint) {
                    if (dataPoint.length < 2) {
                        dataPoint.push(null);
                    }
                    else {
                        anyValidPoint = true;
                        lastValidValue = dataPoint[1];
                    }
                });
                if (!anyValidPoint) {
                    return;
                }
                var counter = _this.findCounterInSelectedGroup(singleSeries.Label);
                var threshold = _this.findThresholdByThresholdSuffix(counter.ThresholdSuffix);
                var status = _this.getStatus(lastValidValue, threshold);
                // get next color or iterate from beginning of colors
                colorIndex = (colorIndex + 1) % colors.length;
                var colorNotApplied = colors[colorIndex][0];
                var colorApplied = colors[colorIndex][1];
                // add predictions
                var predictionSeries = _this.addPredictionSeries(singleSeries, colorApplied, colorNotApplied);
                // if using predicted data and there is a gap between last data point less then zoom step, fill the gap
                if (predictionSeries.hasPredictedData) {
                    var last = _.last(singleSeries.Data);
                    var stepMiliseconds = moment.duration({ minutes: zoom.stepMinutes }).asMilliseconds();
                    var lastPollMiliseconds = last[0];
                    if (_this.orionNowMiliseconds - lastPollMiliseconds <= (stepMiliseconds * 1.5)) {
                        var lastPollValue = last[1];
                        singleSeries.Data.push([_this.orionNowMiliseconds, lastPollValue]);
                    }
                }
                // add historical data
                _this.chart.addSeries(_this.createSerie(singleSeries.Label, singleSeries.Data, colorNotApplied), false, false);
                // add table row
                _this.visibleCounters.push({
                    Name: singleSeries.Label,
                    Description: counter.Description,
                    ThresholdSuffix: counter.ThresholdSuffix,
                    lastValue: _this.formatValue(lastValidValue, 2),
                    lastValueStatus: status,
                    predictedAfterValue: predictionSeries.firstPredictedAfter ? _this.formatValue(predictionSeries.firstPredictedAfter, 2) : null,
                    predictedAfterStatus: predictionSeries.firstPredictedAfter ? _this.getStatus(predictionSeries.firstPredictedAfter, threshold) : null,
                    thresholdPredictions: _this.getThresholdPrediction(predictionSeries.prediction, status),
                    visible: true,
                    colorNotApplied: colorNotApplied,
                    colorApplied: colorApplied,
                    hasPredictedData: predictionSeries.hasPredictedData
                });
            });
            _this.updateZoom(zoom);
            _this.updateThresholds();
            _this.updateHorizontalAxisOptions();
            _this.updateVerticalAxisMinMaxValues();
            _this.chart.redraw(false);
        };
        /**
         * Adds series for predictions in given data series
         *
         * @returns Object with predictions description
         */
        this.addPredictionSeries = function (series, colorApplied, colorNotApplied) {
            var prediction = _this.findPredictionForCounter(series.Label);
            var result = { prediction: prediction, hasPredictedData: false, firstPredictedAfter: null };
            if (!prediction) {
                return result;
            }
            // prediction if the recommendation is not applied
            if (prediction.PredictedUsageBefore) {
                var data = _this.processPredictedUsageData(prediction.PredictedUsageBefore);
                if (!_.isEmpty(data)) {
                    result.hasPredictedData = true;
                    _this.chart.addSeries(_this.createSerie(series.Label, data, colorNotApplied), false, false);
                }
            }
            // first predicted value and not whole predicted usage present
            if (prediction.FirstPredictedValueAfter && !prediction.PredictedUsageAfter) {
                result.hasPredictedData = true;
                result.firstPredictedAfter = prediction.FirstPredictedValueAfter;
                _this.chart.addSeries(_this.createPredictedSerie(result.firstPredictedAfter, series.Label, colorApplied));
            }
            // prediction after the recommendation is applied
            if (prediction.PredictedUsageAfter) {
                var data = _this.processPredictedUsageData(prediction.PredictedUsageAfter);
                if (!_.isEmpty(data)) {
                    result.hasPredictedData = true;
                    result.firstPredictedAfter = data[0][1];
                    _this.chart.addSeries(_this.createSerie(series.Label, data, colorApplied), false, false);
                }
            }
            return result;
        };
        this.addThresholdLine = function (axis, value, color) {
            axis.plotLines.push({
                color: color,
                width: 0.9,
                value: value,
                dashStyle: "dash"
            });
        };
        this.findCounterInSelectedGroup = function (name) {
            return _.find(_this.selectedChartGroup.Counters, function (c) { return c.Name === name; });
        };
        /**
         * Adds plot lines for thresholds of visible counters
         */
        this.updateThresholds = function () {
            var options = { plotLines: [] };
            var warning = null;
            var critical = null;
            var showWarning = true;
            var showCritical = true;
            // find thresholds and show them only in case they all have same value
            _.forEach(_this.visibleCounters, function (counter) {
                if (!counter.visible) {
                    return;
                }
                var threshold = _this.findThresholdByThresholdSuffix(counter.ThresholdSuffix);
                if (!threshold) {
                    return;
                }
                if (threshold.Warning) {
                    if (!warning) {
                        warning = threshold.Warning;
                    }
                    else if (warning !== threshold.Warning) {
                        showWarning = false;
                    }
                }
                if (threshold.Critical) {
                    if (!critical) {
                        critical = threshold.Critical;
                    }
                    else if (critical !== threshold.Critical) {
                        showCritical = false;
                    }
                }
            });
            if (showWarning && warning) {
                _this.addThresholdLine(options, warning, "#FEC405");
            }
            if (showCritical && critical) {
                _this.addThresholdLine(options, critical, "#D50000");
            }
            _this.chart.yAxis[0].update(options);
        };
        /**
         * Initializes highcharts configuration
         */
        this.setupHighcharts = function () {
            var self = _this;
            _this.chartConfig = {
                tag: _this.entity.Id.Id,
                chart: {
                    height: 170,
                    width: 745,
                    marginRight: 1,
                    marginTop: 15,
                    marginLeft: _this.ticksLength,
                    plotBackgroundColor: "#ffffff",
                    backgroundColor: null,
                    plotBorderColor: "#D5D5D5",
                    plotBorderWidth: 1,
                    // user-zooming
                    zoomType: "x",
                    resetZoomButton: {
                        theme: {
                            fill: "#297994",
                            "stroke-width": 0,
                            style: {
                                color: "#ffffff" // @xui-color-light-bg
                            },
                            r: 0,
                            states: {
                                hover: {
                                    fill: "#23677e" // @xui-color-active-hover
                                }
                            }
                        }
                    }
                },
                xAxis: {
                    type: "datetime",
                    tickPixelInterval: 120,
                    labels: {
                        style: {
                            color: "#909090" // TODO: what apollo color?
                        },
                        x: 5
                    },
                    events: {
                        afterSetExtremes: function () {
                            // do nothing in case data is currently being reloaded
                            if (_this.isDisplayingData) {
                                return;
                            }
                            // as we got here, we can presume custom user zoom has been made (i.e. selecting via mouse)
                            _this.customZoomSelected = true;
                            _this.updateVerticalAxisMinMaxValues();
                            _this.chart.redraw(false);
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    // ticks
                    gridLineWidth: 0.5,
                    gridLineDashStyle: "dash",
                    tickAmount: 5,
                    tickColor: "#D5D5D5",
                    tickWidth: 1,
                    tickLength: _this.ticksLength,
                    // labels
                    labels: {
                        align: "left",
                        style: {
                            textOverflow: "none",
                            color: "#909090" // TODO: what apollo color?
                        },
                        formatter: function () {
                            return self.formatValue(this.value, 2);
                        },
                        x: -_this.ticksLength,
                        y: 14
                    },
                    showFirstLabel: true
                },
                tooltip: {
                    enabled: false
                },
                series: [],
                legend: { enabled: false }
            };
            Highcharts.setOptions({
                lang: {
                    resetZoom: _this._t("Reset zoom"),
                    resetZoomTitle: _this._t("Reset zoom level 1:1")
                }
            });
        };
        this.onVisibleCounterChanged = function (item) {
            var series = _.filter(_this.chart.series, function (s) { return s.name === item.Name; });
            _.each(series, function (s) { return s.setVisible(item.visible, false); });
            _this.updateThresholds();
            _this.chart.redraw(false);
        };
        this.selectDefaultZoom = function () {
            _this.selectedZoom = _this.zoomOptions[1];
        };
        this.init = function (context) {
            var objType = context.item.Id.Id.split(":")[0];
            _.each(context.justifications, function (justification) {
                if (_.isUndefined(justification.EntityJustifications[context.item.Id.Id])) {
                    return;
                }
                if (justification.Charts) {
                    _.each(justification.Charts, function (group) {
                        if (!_.some(_this.chartGroups, function (g) { return group.GroupName === g.GroupName; })
                            && _.some(group.EntityTypes, function (f) { return f === objType; })) {
                            _this.chartGroups.push(group);
                        }
                    });
                }
                _this.justifications.push(justification.EntityJustifications[context.item.Id.Id]);
            });
            // no counters to display: hide chart and table area
            if (_.isEmpty(_this.chartGroups)) {
                return;
            }
            _this.selectDefaultZoom();
            _this.selectedChartGroup = _this.chartGroups[0];
            _this.isRealtime = context.isRealtime;
            _this.entity = context.item;
            // setup chart config and keep its instance
            var chartWatch = _this.$scope.$watch(function () { return _this.findChart(); }, function (chart) {
                if (!chart) {
                    return;
                }
                // remove chart watch
                chartWatch();
                chartWatch = null;
                // find the instance and load the chart
                _this.chart = _this.findChart();
                _this.reloadChart();
            });
            _this.setupHighcharts();
            _this.isChartVisible = true;
        };
    }
    OutcomeDecoratorController.$inject = [
        "$scope",
        "$log",
        "$filter",
        "swApi",
        index_1.ServiceNames.dateTimeService,
        index_1.ServiceNames.swisService,
        "getTextService"
    ];
    return OutcomeDecoratorController;
}());
exports.OutcomeDecoratorController = OutcomeDecoratorController;
;


/***/ }),
/* 224 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"VimOutcomeDecoratorController as ctl\" ng-init=ctl.init(context)> <div class=resolution-description ng-bind-html=::ctl.getOutcome()></div> <div ng-if=ctl.isChartVisible style=padding-right:5px> <xui-divider></xui-divider> <div class=\"row recommendation-outcome-item\"> <div class=col-xs-6> <xui-dropdown ng-show=\"::ctl.chartGroups.length > 1\" items-source=ctl.chartGroups display-value=GroupName selected-item=ctl.selectedChartGroup on-changed=ctl.onChartGroupChanged()> </xui-dropdown> </div> <div class=col-xs-6> <xui-dropdown class=pull-right items-source=ctl.zoomOptions display-value=title selected-item=ctl.selectedZoom on-changed=ctl.onZoomChanged()> </xui-dropdown> </div> </div> <div class=recommendation-outcome-item> <sw-chart type=line config=ctl.chartConfig></sw-chart> </div> <div ng-repeat=\"item in ctl.visibleCounters track by item.Name\"> <xui-divider></xui-divider> <div class=row> <div class=col-xs-12> <xui-checkbox ng-model=item.visible ng-change=ctl.onVisibleCounterChanged(item) _t> {{::item.Description}} on {{::vm.context.item.Name}} </xui-checkbox> </div> </div> <div class=row> <div class=col-xs-5> <div class=row> <div class=col-xs-12 style=margin-left:22px> <span class=recommendation-outcome-color-box ng-style=\"::{backgroundColor: item.colorNotApplied}\"> </span> {{item.Description}} </div> </div> <div ng-if=item.hasPredictedData class=row> <div class=col-xs-12 style=margin-left:22px> <span class=recommendation-outcome-color-box ng-style=\"::{backgroundColor: item.colorApplied}\"> </span> <span _t>{{::item.Description}} after recommendation</span> </div> </div> </div> <div class=col-xs-3> <div class=row style=padding-right:30px> <span class=recommendation-outcome-table-detail _t>Current value</span> <re-icon-label class=\"recommendation-outcome-counter-value pull-right\" value=item.lastValue status=item.lastValueStatus> </re-icon-label> </div> <div ng-if=::item.hasPredictedData class=row style=padding-right:30px> <span class=recommendation-outcome-table-detail _t>Predicted value</span> <re-icon-label class=\"recommendation-outcome-counter-value pull-right\" value=item.predictedAfterValue status=item.predictedAfterStatus> </re-icon-label> </div> </div> <div class=\"col-xs-4 recommendation-outcome-table-detail\"> <div ng-if=\"::item.thresholdPredictions.length > 0\"> <span _t> If recommendation isn't performed {{::item.Description}} will {{::item.thresholdPredictions[0].name}} </span> <span class={{::item.thresholdPredictions[0].status}}> {{::item.thresholdPredictions[0].timeDescription}} </span> <span ng-if=\"::item.thresholdPredictions.length > 1\"> <span _t>and will {{::item.thresholdPredictions[1].name}}</span> <span class={{::item.thresholdPredictions[1].status}}> {{::item.thresholdPredictions[1].timeDescription}} </span> </span> </div> </div> </div> </div> <xui-divider></xui-divider> </div> <a href=\"/Orion/View.aspx?NetObject={{::context.item.Id.Id}}\"> <strong _t>More details about {{::context.item.Name}} »</strong> </a> </div> ";

/***/ }),
/* 225 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var pluginTemplateUrls_1 = __webpack_require__(6);
var outcomeHeaderDecorator_controller_1 = __webpack_require__(226);
var configTemplate = function ($templateCache) {
    $templateCache.put(pluginTemplateUrls_1.PluginTemplateUrls.recommendationDetailOutcomeItemHeader, __webpack_require__(227));
};
exports.default = function (module) {
    module.controller("VimOutcomeHeaderDecoratorController", outcomeHeaderDecorator_controller_1.OutcomeHeaderDecoratorController);
    module.app().run([index_1.ServiceNames.ngTemplateCacheService, configTemplate]);
};


/***/ }),
/* 226 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Outcome header decorator controller
 *
 * @class
 */
var OutcomeHeaderDecoratorController = /** @class */ (function () {
    function OutcomeHeaderDecoratorController() {
        var _this = this;
        this.justifications = [];
        this.init = function (context) {
            _this.justifications = context.justifications;
            _this.entity = context.item;
            _this.processJustifications();
        };
        this.getEntityStatusIconUrl = function (entity) {
            return "/Orion/StatusIcon.ashx?entity=" + entity.SwisEntityName + "&KeyPropertyValue=" + entity.ObjectId;
        };
        this.getTrendIcon = function (wrapper) {
            var before = wrapper.first.Before;
            var after = wrapper.last.After;
            if (before < after) {
                return "arrow-up";
            }
            if (before > after) {
                return "arrow-down";
            }
            return "minus";
        };
        this.getThresholdTag = function (wrapper) {
            var counterJustification = wrapper.first;
            var threshold = _.find(_this.entity.Thresholds, { ThresholdName: counterJustification.ThresholdName });
            if (threshold) {
                if (counterJustification.Overall > threshold.Critical) {
                    return "critical";
                }
                if (counterJustification.Overall > threshold.Warning) {
                    return "warning";
                }
            }
            return "none";
        };
        this.isConstantTrend = function (wrapper) {
            return wrapper.first.Before === wrapper.last.After;
        };
        this.getDifference = function (wrapper) {
            return wrapper.last.After - wrapper.first.Before;
        };
        this.processJustifications = function () {
            _this.counterJustifications = {};
            _this.counterFormats = {};
            _.forEach(_this.justifications, function (justification) {
                var entityDetail = justification.EntityJustifications[_this.entity.Id.Id];
                if (!_.isUndefined(entityDetail)) {
                    _.forEach(entityDetail.CounterJustifications, function (counterJustification) {
                        var counterKey = _this.getCounterType(counterJustification.CounterType);
                        var currentWrapper = _this.counterJustifications[counterKey];
                        if (_.isUndefined(currentWrapper)) {
                            _this.counterJustifications[counterKey] = {
                                first: counterJustification,
                                last: counterJustification
                            };
                        }
                        else {
                            currentWrapper.last = counterJustification;
                        }
                    });
                }
                if (!_.isUndefined(justification.CounterFormats)) {
                    _.extend(_this.counterFormats, justification.CounterFormats);
                }
            });
        };
        this.getCounterType = function (counterType) {
            return counterType.Type + ":" + counterType.InstanceNumber;
        };
    }
    return OutcomeHeaderDecoratorController;
}());
exports.OutcomeHeaderDecoratorController = OutcomeHeaderDecoratorController;
;


/***/ }),
/* 227 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"VimOutcomeHeaderDecoratorController as ctl\" ng-init=ctl.init(context)> <div ng-repeat=\"(counter, wrapper) in ::ctl.counterJustifications\"> <div class=counter> {{::wrapper.first.Label}} </div> <div class=utilization> <span class=caption ng-if=::ctl.counterFormats[wrapper.first.CounterFormatKey]> {{::ctl.counterFormats[wrapper.first.CounterFormatKey].OverallLabel}} </span> <re-icon-label ng-if=::ctl.counterFormats[wrapper.first.CounterFormatKey].OverallLabel value=\"::wrapper.first.Overall | vimValueFormat: ctl.counterFormats[wrapper.first.CounterFormatKey].OverallUnitFormatter\" status=::ctl.getThresholdTag(wrapper)> </re-icon-label> </div> <div class=desired-utilization ng-if=\"wrapper.first.After != wrapper.first.Before\"> <span class=trend ng-if=::!ctl.isConstantTrend(wrapper)> <xui-icon icon={{::ctl.getTrendIcon(wrapper)}}></xui-icon> <span class=value>{{::ctl.getDifference(wrapper) | vimValueFormat: ctl.counterFormats[wrapper.first.CounterFormatKey].UnitFormatter: true}}</span> </span> <span class=caption ng-if=::ctl.counterFormats[wrapper.first.CounterFormatKey]> {{::ctl.counterFormats[wrapper.first.CounterFormatKey].AfterLabel}} </span> <span class=value> {{::wrapper.last.After | vimValueFormat: ctl.counterFormats[wrapper.first.CounterFormatKey].UnitFormatter}} </span> </div> <div class=clearfix></div> </div> </div> ";

/***/ }),
/* 228 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(229);
var index_2 = __webpack_require__(241);
var index_3 = __webpack_require__(246);
var index_4 = __webpack_require__(249);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
    index_3.default(module);
    index_4.default(module);
};


/***/ }),
/* 229 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constraintsEditorEntitySelector_directive_1 = __webpack_require__(230);
var constraintsEditorDescription_directive_1 = __webpack_require__(235);
var constraintsEditorControls_directive_1 = __webpack_require__(238);
exports.default = function (module) {
    module.component("constraintsEditorEntitySelector", constraintsEditorEntitySelector_directive_1.ConstraintsEditorEntitySelectorDirective);
    module.component("constraintsEditorDescription", constraintsEditorDescription_directive_1.ConstraintsEditorDescriptionDirective);
    module.component("constraintsEditorControls", constraintsEditorControls_directive_1.ConstraintsEditorControlsDirective);
};


/***/ }),
/* 230 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constraintsEditorEntitySelector_controller_1 = __webpack_require__(231);
/**
 * Display constraint's editor asset explorer.
 *
 * @class
 */
var ConstraintsEditorEntitySelectorDirective = /** @class */ (function () {
    function ConstraintsEditorEntitySelectorDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(234);
        this.controller = constraintsEditorEntitySelector_controller_1.ConstraintsEditorEntitySelectorController;
        this.controllerAs = "vm";
        this.bindToController = {
            editorParameters: "<"
        };
        this.scope = {};
    }
    return ConstraintsEditorEntitySelectorDirective;
}());
exports.ConstraintsEditorEntitySelectorDirective = ConstraintsEditorEntitySelectorDirective;


/***/ }),
/* 231 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var enums_1 = __webpack_require__(4);
var constants_1 = __webpack_require__(5);
var managedEntityFilter_1 = __webpack_require__(232);
var managedEntityListFilterValues_1 = __webpack_require__(233);
var index_2 = __webpack_require__(3);
/**
 * Constraints entity selector controller
 *
 * @class
 */
var ConstraintsEditorEntitySelectorController = /** @class */ (function () {
    function ConstraintsEditorEntitySelectorController($scope, $log, $q, $state, swApiService, routingService, dataGroupStatusService, _t, vimSwisService, xuiToastService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$q = $q;
        this.$state = $state;
        this.swApiService = swApiService;
        this.routingService = routingService;
        this.dataGroupStatusService = dataGroupStatusService;
        this._t = _t;
        this.vimSwisService = vimSwisService;
        this.xuiToastService = xuiToastService;
        this.$onInit = function () {
            _this.options = {
                sortableColumns: [_this.sortByName],
                selectionMode: "multi",
                templateUrl: "item-template",
                selectionProperty: "entityId",
                autoRefreshInterval: 60,
                pageSize: 10,
                showAddRemoveFilterProperty: false,
                emptyText: _this._t("No recommendation constraints available."),
                showMorePropertyValuesThreshold: 10,
                hideSearch: false,
                triggerSearchOnChange: true,
                allowSelectAllPages: false
            };
            _this.sorting = {
                sortBy: _this.sortByName,
                direction: "asc"
            };
            _this.editorParameters.selectedItems = {
                items: [],
                blacklist: false
            };
            _this.$scope.$watch(function () { return _this.editorParameters.objectType; }, function (newValue) {
                if (!newValue || !_this.pagination) {
                    return;
                }
                // Reseting selected items once the entity type changes
                _this.editorParameters.selectedItems = {
                    items: [],
                    blacklist: false
                };
                _this.updateData();
            });
        };
        this.searchString = "";
        this.filterValues = new managedEntityListFilterValues_1.ManagedEntityListFilterValues();
        this.remoteControl = {};
        this.objectTypes = ["Constraint"];
        this.sortByName = { id: "Name", label: this._t("Name") };
        this.getFilterProperties = function () {
            if (_this.editorParameters.constraintType === constants_1.ConstraintType.exclude || _this.editorParameters.constraintType === constants_1.ConstraintType.mgmtActionDisabled) {
                return _this.createClusterFilterProperty().then(function (clusterFilter) {
                    return _this.createHostFilterProperty().then(function (hostFilter) {
                        return _this.createDatastoreFilterProperty().then(function (datastoreFilter) {
                            var filter = [];
                            // add cluster filter only if there are any
                            if (!_.isEmpty(clusterFilter.values)) {
                                filter.push(clusterFilter);
                            }
                            // add hosts filter only if there are any and don't add host filter for host entities
                            if (!_.isEmpty(hostFilter.values) && _this.editorParameters.objectType !== enums_1.EntityType[enums_1.EntityType.Host]) {
                                filter.push(hostFilter);
                            }
                            // add datastores filter only if there are any and don't add datastores filter for datastore entities
                            if (!_.isEmpty(datastoreFilter.values) && _this.editorParameters.objectType !== enums_1.EntityType[enums_1.EntityType.Datastore]) {
                                filter.push(datastoreFilter);
                            }
                            return filter;
                        });
                    });
                });
            }
            else {
                var deferred = _this.$q.defer();
                return deferred.promise;
            }
        };
        this.getData = function (filters, pagination, sorting, search) {
            var managedEntitiesFilter = _this.getManagedEntitiesFilter(filters, sorting, pagination, search);
            return _this.vimSwisService.getConstrainedObjects(_this.editorParameters.objectType).then(function (constraintObjects) {
                return _this.vimSwisService.getManagedEntities(managedEntitiesFilter, _this.editorParameters.objectType).then(function (result) {
                    //We need to add number of policies for each virtual machine
                    _.forEach(result.managedEntities, function (managedEntity) {
                        var managedEntityConstraints = _(constraintObjects).filter({ "objectId": managedEntity.entityId });
                        managedEntity.constraintsCount = managedEntityConstraints.size();
                        if (_this.editorParameters.constraintId != null
                            && managedEntity.constraintsCount > 0
                            && _(managedEntityConstraints).some({ "constraintId": _this.editorParameters.constraintId })
                            && _this.editorParameters.selectedItems.items.indexOf(managedEntity.entityId) === -1) {
                            _this.editorParameters.selectedItems.items.push(managedEntity.entityId);
                        }
                    });
                    var filteredListData = {
                        items: result.managedEntities,
                        total: result.filter.pagination.total
                    };
                    return filteredListData;
                });
            });
        };
        /**
         * Creates entity filter property upon given result set.
         */
        this.createClusterFilterProperty = function () {
            return _this.vimSwisService.getClusterObjects(_this.editorParameters.objectType).then(function (clusterObjects) {
                var filterValues = [];
                _.forEach(clusterObjects, function (cluster, id) {
                    var entityFilterValue = {
                        id: cluster.dataGroupId,
                        title: cluster.name,
                        count: cluster.constraintsCount,
                        icon: _this.getIconForEntityType(cluster.entityType),
                        status: cluster.status
                    };
                    filterValues.push(entityFilterValue);
                });
                var filterProperty = {
                    refId: "clusterEntity",
                    title: _this._t("Cluster"),
                    values: filterValues
                };
                return filterProperty;
            });
        };
        /**
         * Creates entity filter property upon given result set.
         */
        this.createHostFilterProperty = function () {
            return _this.vimSwisService.getHostObjects(_this.editorParameters.objectType).then(function (hostObjects) {
                var filterValues = [];
                _.forEach(hostObjects, function (host, id) {
                    var entityFilterValue = {
                        id: host.dataGroupId,
                        title: host.name,
                        count: host.constraintsCount,
                        icon: _this.getIconForEntityType(host.entityType),
                        status: host.status
                    };
                    filterValues.push(entityFilterValue);
                });
                var filterProperty = {
                    refId: "hostEntity",
                    title: _this._t("Host"),
                    values: filterValues
                };
                return filterProperty;
            });
        };
        /**
         * Creates entity filter property upon given result set.
         */
        this.createDatastoreFilterProperty = function () {
            return _this.vimSwisService.getDatastoreObjects(_this.editorParameters.objectType).then(function (datastoreObjects) {
                var filterValues = [];
                _.forEach(datastoreObjects, function (ds, id) {
                    var entityFilterValue = {
                        id: ds.dataGroupId,
                        title: ds.name,
                        count: ds.constraintsCount,
                        icon: _this.getIconForEntityType(ds.entityType),
                        status: ds.status
                    };
                    filterValues.push(entityFilterValue);
                });
                var filterProperty = {
                    refId: "datastoreEntity",
                    title: _this._t("Datastore"),
                    values: filterValues
                };
                return filterProperty;
            });
        };
        this.getIconName = function () {
            return _this.getIconForEntityType(_this.editorParameters.objectType);
        };
        this.getIconForEntityType = function (entityType) {
            switch (entityType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    return "virtualmachine";
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    return "hypervhost";
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    return "hypervcluster";
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    return "datastore";
                default:
                    return "";
            }
        };
        this.getManagedEntitiesFilter = function (filters, sorting, pagination, search) {
            var filter = new managedEntityFilter_1.ManagedEntityFilter();
            filter.filters = new managedEntityListFilterValues_1.ManagedEntityListFilterValues();
            filter.sorting = sorting;
            filter.pagination = pagination;
            filter.search = search;
            if (filters != null) {
                filter.filters.clusterEntity = filters.clusterEntity;
                filter.filters.hostEntity = filters.hostEntity;
                filter.filters.datastoreEntity = filters.datastoreEntity;
            }
            filter.constraintId = _this.editorParameters.constraintId;
            return filter;
        };
        this.updateData = function () {
            if (!_this.remoteControl) {
                return _this.$q.when();
            }
            return _this.remoteControl.refreshListData();
        };
    }
    ConstraintsEditorEntitySelectorController = __decorate([
        __param(0, di_1.Inject("$scope")),
        __param(1, di_1.Inject("$log")),
        __param(2, di_1.Inject("$q")),
        __param(3, di_1.Inject("$state")),
        __param(4, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(5, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(6, di_1.Inject(index_1.ServiceNames.dataGroupStatusService)),
        __param(7, di_1.Inject("getTextService")),
        __param(8, di_1.Inject(index_2.ServiceNames.swisService)),
        __param(9, di_1.Inject("xuiToastService")),
        __metadata("design:paramtypes", [Object, Object, Function, Object, Object, Object, Object, Function, Object, Object])
    ], ConstraintsEditorEntitySelectorController);
    return ConstraintsEditorEntitySelectorController;
}());
exports.ConstraintsEditorEntitySelectorController = ConstraintsEditorEntitySelectorController;


/***/ }),
/* 232 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Managed entity filter class
 *
 * @class
 * @implements IManagedEntityFilter
 */
var ManagedEntityFilter = /** @class */ (function () {
    function ManagedEntityFilter() {
        this.pagination = null;
        this.sorting = null;
        this.search = null;
    }
    return ManagedEntityFilter;
}());
exports.ManagedEntityFilter = ManagedEntityFilter;


/***/ }),
/* 233 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Supported filter values for managed object list
 *
 * @class
 * @implements IManagedEntityListFilterValues
 **/
var ManagedEntityListFilterValues = /** @class */ (function () {
    function ManagedEntityListFilterValues() {
        this.clusterEntity = [];
        this.hostEntity = [];
        this.datastoreEntity = [];
    }
    return ManagedEntityListFilterValues;
}());
exports.ManagedEntityListFilterValues = ManagedEntityListFilterValues;


/***/ }),
/* 234 */
/***/ (function(module, exports) {

module.exports = "<div class=\"constraints-editor constraints-entity-selector\"> <xui-filtered-list items-source=vm.itemsSource datasource-fn=\"vm.getData(filters, pagination, sorting, searching)\" filter-properties-fn=vm.getFilterProperties() options=::vm.options sorting=::vm.sorting pagination-data=::vm.pagination filter-values=vm.filterValues selection=vm.editorParameters.selectedItems filter-property-groups=::vm.objectTypes controller=vm remote-control=vm.remoteControl> </xui-filtered-list> <script type=text/ng-template id=item-template> <div class=\"row detail\">\n            <div class=\"col-xs-10\">\n                <xui-icon icon=\"{{vm.getIconName()}}\" status=\"{{::item.status}}\" icon-size=\"small\"></xui-icon>\n                {{::item.name}}\n                <span ng-if=\"vm.editorParameters.objectType=='VirtualMachine'\">\n                    <span _t>on {{::item.parentEntityName}}</span>\n                </span>\n            </div>\n            <div class=\"col-xs-2\">\n                <span ng-if=\"item.constraintsCount==0\" _t>No policies</span>\n                <span ng-if=\"item.constraintsCount==1\" _t>1 policy</span>\n                <span ng-if=\"item.constraintsCount>1\" _t>{{::item.constraintsCount}} policies</span></span>\n            </div>\n        </div> </script> </div> ";

/***/ }),
/* 235 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var constraintsEditorDescription_controller_1 = __webpack_require__(236);
/**
 * Display constraint's editor description.
 *
 * @class
 */
var ConstraintsEditorDescriptionDirective = /** @class */ (function () {
    function ConstraintsEditorDescriptionDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(237);
        this.controller = constraintsEditorDescription_controller_1.ConstraintsEditorDescriptionController;
        this.controllerAs = "vm";
        this.bindToController = {
            editorParameters: "<"
        };
        this.scope = {};
    }
    return ConstraintsEditorDescriptionDirective;
}());
exports.ConstraintsEditorDescriptionDirective = ConstraintsEditorDescriptionDirective;


/***/ }),
/* 236 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(3);
/**
 * Constraints entity description controller
 *
 * @class
 */
var ConstraintsEditorDescriptionController = /** @class */ (function () {
    function ConstraintsEditorDescriptionController(vimSwisService) {
        this.vimSwisService = vimSwisService;
    }
    ConstraintsEditorDescriptionController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.swisService)),
        __metadata("design:paramtypes", [Object])
    ], ConstraintsEditorDescriptionController);
    return ConstraintsEditorDescriptionController;
}());
exports.ConstraintsEditorDescriptionController = ConstraintsEditorDescriptionController;


/***/ }),
/* 237 */
/***/ (function(module, exports) {

module.exports = "<div class=\"constraints-editor constraints-description\"> <div class=\"row row-full\"> <div class=col-xs-12> <xui-expander heading=\"_t(Policy Description (Optional))\" is-open=vm.editorParameters.descriptionExpanded _ta> <ng-form> <xui-textbox ng-model=vm.editorParameters.description caption=\"_t(Policy Description)\" rows=4 custom-box-width=500 _ta></xui-textbox> </ng-form> </xui-expander> </div> </div> </div>";

/***/ }),
/* 238 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(34);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constraintsEditorControls_controller_1 = __webpack_require__(239);
/**
 * Display constraints' editor controls
 *
 * @class
 */
var ConstraintsEditorControlsDirective = /** @class */ (function () {
    function ConstraintsEditorControlsDirective() {
        this.restrict = "E";
        this.replace = true;
        this.transclude = false;
        this.template = __webpack_require__(240);
        this.controller = constraintsEditorControls_controller_1.ConstraintsEditorControlsController;
        this.controllerAs = "vm";
        this.bindToController = true;
        this.scope = {
            editorParameters: "<"
        };
    }
    return ConstraintsEditorControlsDirective;
}());
exports.ConstraintsEditorControlsDirective = ConstraintsEditorControlsDirective;


/***/ }),
/* 239 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var constraint_1 = __webpack_require__(13);
var index_1 = __webpack_require__(0);
var enums_1 = __webpack_require__(4);
var constants_1 = __webpack_require__(5);
var index_2 = __webpack_require__(3);
/**
 * Constraints tab editor controls controller
 *
 * @class
 */
var ConstraintsEditorControlsController = /** @class */ (function () {
    function ConstraintsEditorControlsController($scope, $log, $q, $state, swApiService, routingService, _t, vimSwisService, xuiToastService, dateTimeService) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$q = $q;
        this.$state = $state;
        this.swApiService = swApiService;
        this.routingService = routingService;
        this._t = _t;
        this.vimSwisService = vimSwisService;
        this.xuiToastService = xuiToastService;
        this.dateTimeService = dateTimeService;
        this.insertOrUpdateConstraint = function () {
            var selectedManagedEntityIds = _this.editorParameters.selectedItems.items;
            var entityIdentifier = "";
            switch (_this.editorParameters.objectType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    entityIdentifier = "VVM";
                    break;
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    entityIdentifier = "VH";
                    break;
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    entityIdentifier = "VMC";
                    break;
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    entityIdentifier = "VMS";
                    break;
            }
            var constraintObjects = [];
            _.forEach(selectedManagedEntityIds, function (entityId) {
                constraintObjects.push({
                    EntityIdentifier: entityIdentifier + ":" + entityId,
                    ObjectId: entityId,
                    EntityType: _this.editorParameters.objectType
                });
            });
            if (constraintObjects.length === 0) {
                // constraint needs to contain at least one entity
                _this.xuiToastService.error(_this._t("Constraint needs to contain at least one entity!"), _this._t("Constraint was not saved"));
                return;
            }
            if (_this.editorParameters.constraintType === constants_1.ConstraintType.mgmtActionDisabled && _this.editorParameters.parameters.length === 0) {
                // constraint needs to contain at least one disallowed action
                _this.xuiToastService.error(_this._t("Constraint needs to contain at least one disallowed action!"), _this._t("Constraint was not saved"));
                return;
            }
            if (_this.editorParameters.expirationDate && moment().diff(_this.editorParameters.expirationDate, "days") > 0) {
                _this.xuiToastService.error(_this._t("Constraint expiration cannot be set in past!"), _this._t("Constraint was not saved"));
                return;
            }
            var name = _this.getPolicyName(selectedManagedEntityIds, _this.editorParameters.parameters);
            var expirationDate = _this.dateTimeService.convertStringToUtc(_this.dateTimeService.moveUserInputToOrionDateEndOfTheDay(_this.editorParameters.expirationDate));
            var constraint = new constraint_1.Constraint({
                id: _this.editorParameters.constraintId,
                name: name,
                description: _this.editorParameters.description,
                type: _this.editorParameters.constraintType,
                dateCreated: _this.dateTimeService.getOrionNowDate(),
                expirationDate: expirationDate,
                enabled: true,
                inherited: false,
                owner: "VIM",
                constraintObjects: constraintObjects,
                priority: 0,
                parameters: _this.editorParameters.parameters,
                userId: ""
            });
            _this.swApiService.insertOrUpdateConstraints([constraint]).then(function () {
                _this.routingService.goToConstraints();
            });
        };
        this.isEditMode = function () {
            return !!_this.editorParameters.constraintId;
        };
        this.getEntityName = function (singleEntity) {
            switch (_this.editorParameters.objectType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    return singleEntity ? _this._t("VM") : _this._t("VMs");
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    return singleEntity ? _this._t("host") : _this._t("hosts");
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    return singleEntity ? _this._t("cluster") : _this._t("clusters");
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    return singleEntity ? _this._t("datastore") : _this._t("datastores");
                default:
                    return "";
            }
        };
        this.getPolicyName = function (selectedManagedEntityIds, parameters) {
            var name = "";
            var formatString = "";
            var singleEntity = _.size(selectedManagedEntityIds) === 1;
            switch (_this.editorParameters.constraintType) {
                case constants_1.ConstraintType.exclude:
                    formatString = singleEntity
                        ? _this._t("Exclude {0} {1} from recommendations")
                        : _this._t("Exclude {0} {1} from recommendations");
                    name = formatString.format(_.size(selectedManagedEntityIds), _this.getEntityName(singleEntity));
                    break;
                case constants_1.ConstraintType.mgmtActionDisabled:
                    var singleAction = _.size(parameters) === 1;
                    if (singleAction) {
                        formatString = singleEntity
                            ? _this._t("Disallow \"{0}\" action for {1} {2}")
                            : _this._t("Disallow \"{0}\" action for {1} {2}");
                        name = formatString.format(_this.getParameterName(parameters[0]), _.size(selectedManagedEntityIds), _this.getEntityName(singleEntity));
                    }
                    else {
                        formatString = singleEntity
                            ? _this._t("Disallow {0} actions for {1} {2}")
                            : _this._t("Disallow {0} actions for {1} {2}");
                        name = formatString.format(_.size(parameters), _.size(selectedManagedEntityIds), _this.getEntityName(singleEntity));
                    }
                    break;
            }
            return name;
        };
        this.getParameterName = function (parameter) {
            switch (parameter) {
                case constants_1.ConstraintParameters.changeCpuAction:
                    return _this._t("Change CPU Resources");
                case constants_1.ConstraintParameters.changeMemoryAction:
                    return _this._t("Change Memory Resources");
                case constants_1.ConstraintParameters.migrationAction:
                    return _this._t("Move VM to a Different Host");
                case constants_1.ConstraintParameters.relocationAction:
                    return _this._t("Move VM to a Different Storage");
                default:
                    return "";
            }
        };
    }
    ConstraintsEditorControlsController = __decorate([
        __param(0, di_1.Inject("$scope")),
        __param(1, di_1.Inject("$log")),
        __param(2, di_1.Inject("$q")),
        __param(3, di_1.Inject("$state")),
        __param(4, di_1.Inject(index_1.ServiceNames.swApiService)),
        __param(5, di_1.Inject(index_1.ServiceNames.routingService)),
        __param(6, di_1.Inject("getTextService")),
        __param(7, di_1.Inject(index_2.ServiceNames.swisService)),
        __param(8, di_1.Inject("xuiToastService")),
        __param(9, di_1.Inject(index_1.ServiceNames.dateTimeService)),
        __metadata("design:paramtypes", [Object, Object, Function, Object, Object, Object, Function, Object, Object, Object])
    ], ConstraintsEditorControlsController);
    return ConstraintsEditorControlsController;
}());
exports.ConstraintsEditorControlsController = ConstraintsEditorControlsController;


/***/ }),
/* 240 */
/***/ (function(module, exports) {

module.exports = "<div class=\"constraints-editor constraints-editor-controls\"> <xui-divider></xui-divider> <div class=\"row row-full\"> <div class=col-xs-12> <xui-button display-style=primary ng-click=vm.insertOrUpdateConstraint()> <span ng-if=!vm.isEditMode() _t>Create policy</span> <span ng-if=vm.isEditMode() _t>Save policy</span> </xui-button> <xui-button display-style=tertiary ng-click=vm.routingService.goToConstraints() _t>Cancel</xui-button> </div> </div> </div>";

/***/ }),
/* 241 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var pluginTemplateUrls_1 = __webpack_require__(6);
var constraints_controller_1 = __webpack_require__(242);
var configTemplate = function ($templateCache) {
    $templateCache.put(pluginTemplateUrls_1.PluginTemplateUrls.constraintsExplorer, __webpack_require__(245));
};
exports.default = function (module) {
    module.controller("RecommendationConstraintsTabController", constraints_controller_1.RecommendationConstraintsTabController);
    module.app().run([index_1.ServiceNames.ngTemplateCacheService, configTemplate]);
};


/***/ }),
/* 242 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(3);
var constraintsFilter_1 = __webpack_require__(243);
var constraintListFilterValues_1 = __webpack_require__(244);
var enums_1 = __webpack_require__(4);
var constants_1 = __webpack_require__(5);
/**
 * Constraints tab controller
 *
 * @class
 */
var RecommendationConstraintsTabController = /** @class */ (function () {
    function RecommendationConstraintsTabController($scope, $log, $q, $state, swisRecommendationsService, routingService, vimRoutingService, pluggabilityService, dataGroupStatusService, swApiService, vimSwisService, vimDialogService, dateTimeService, _t) {
        var _this = this;
        this.$scope = $scope;
        this.$log = $log;
        this.$q = $q;
        this.$state = $state;
        this.swisRecommendationsService = swisRecommendationsService;
        this.routingService = routingService;
        this.vimRoutingService = vimRoutingService;
        this.pluggabilityService = pluggabilityService;
        this.dataGroupStatusService = dataGroupStatusService;
        this.swApiService = swApiService;
        this.vimSwisService = vimSwisService;
        this.vimDialogService = vimDialogService;
        this.dateTimeService = dateTimeService;
        this._t = _t;
        this.$onInit = function () {
            _this.filterValues = new constraintListFilterValues_1.ConstraintListFilterValues();
            _this.remoteControl = {};
            _this.objectTypes = ["Constraint"];
            _this.sortByDateCreated = { id: "DateCreated", label: _this._t("Creation Time") };
            _this.sortByExpirationDate = { id: "ExpirationDate", label: _this._t("Expiration Time") };
            _this.sortByName = { id: "Name", label: _this._t("Name") };
            _this.sortByDescription = { id: "Description", label: _this._t("Description") };
            _this.options = {
                sortableColumns: [
                    _this.sortByExpirationDate,
                    _this.sortByDateCreated,
                    _this.sortByName,
                    _this.sortByDescription
                ],
                selectionMode: "multi",
                templateUrl: "item-template",
                selectionProperty: "id",
                autoRefreshInterval: 60,
                pageSize: 10,
                showAddRemoveFilterProperty: false,
                emptyText: _this._t("No recommendation policies available."),
                showMorePropertyValuesThreshold: 10,
                hideSearch: false,
                triggerSearchOnChange: true
            };
            _this.sorting = {
                sortBy: _this.sortByDateCreated,
                direction: "desc"
            };
            _this.selectedItems = {
                items: [],
                blacklist: false
            };
        };
        this.getFilterProperties = function () {
            return _this.createEntityFilterProperty().then(function (filterProperty) {
                return _this.createDataGroupFilterProperty().then(function (dataGroupFilter) {
                    return _this.createConstraintTypeFilterProperty().then(function (constraintTypeFilter) {
                        var filter = [
                            filterProperty,
                            dataGroupFilter,
                            constraintTypeFilter
                        ];
                        return filter;
                    });
                });
            });
        };
        this.getData = function (filters, pagination, sorting, search) {
            var constraintsFilter = _this.getConstraintsFilter(filters, sorting, pagination, search);
            return _this.swisRecommendationsService.getConstraints(constraintsFilter).then(function (result) {
                _this.itemsById = _.groupBy(result.constraints, "id");
                var filteredListData = {
                    items: result.constraints,
                    total: result.filter.pagination.total
                };
                return filteredListData;
            });
        };
        this.getConstraintsFilter = function (filters, sorting, pagination, search) {
            var filter = new constraintsFilter_1.ConstraintsFilter();
            filter.sorting = sorting;
            filter.pagination = pagination;
            filter.search = search;
            filter.constrainedObjects = filters.affectedEntity;
            filter.dataGroupObjects = filters.dataGroupEntity;
            filter.constraintTypes = filters.constraintType;
            return filter;
        };
        this.selectedItemsCount = function () {
            if (!_this.selectedItems) {
                return 0;
            }
            var itemsLength = _this.selectedItems.items ? _this.selectedItems.items.length : 0;
            if (!_this.selectedItems.blacklist) {
                return itemsLength;
            }
            if (!_this.pagination) {
                return 0;
            }
            return !_this.pagination.total ? 0 : _this.pagination.total - itemsLength;
        };
        /**
         * Creates entity filter property upon given result set.
         */
        this.createEntityFilterProperty = function () {
            return _this.vimSwisService.getConstrainedObjects().then(function (constrainedObjects) {
                var filterValues = [];
                var constraintObjectsPerObject = _.groupBy(constrainedObjects, function (co) { return co.entityIdentifier; });
                _.forEach(constraintObjectsPerObject, function (objects, id) {
                    var entityFilterValue = {
                        id: id,
                        title: objects[0].displayName,
                        count: objects.length,
                        icon: _this.getIconForEntityType(objects[0].entityType),
                        status: objects[0].status
                    };
                    filterValues.push(entityFilterValue);
                });
                var filterProperty = {
                    refId: "affectedEntity",
                    title: _this._t("Affected Object"),
                    values: filterValues
                };
                return filterProperty;
            });
        };
        /**
         * Creates entity filter property upon given result set.
         */
        this.createDataGroupFilterProperty = function () {
            return _this.vimSwisService.getDataGroupObjects().then(function (dataGroupObjects) {
                var filterValues = [];
                _.forEach(dataGroupObjects, function (dataGroup, id) {
                    var entityFilterValue = {
                        id: dataGroup.dataGroupId,
                        title: dataGroup.name,
                        count: dataGroup.constraintsCount,
                        icon: _this.getIconForEntityType(dataGroup.entityType),
                        status: dataGroup.status
                    };
                    filterValues.push(entityFilterValue);
                });
                var filterProperty = {
                    refId: "dataGroupEntity",
                    title: _this._t("Clusters/Hosts"),
                    values: filterValues
                };
                return filterProperty;
            });
        };
        /**
         * Creates entity filter property upon given result set.
         */
        this.createConstraintTypeFilterProperty = function () {
            return _this.vimSwisService.getConstraintTypes().then(function (result) {
                var filterValues = [];
                _.forEach(result, function (type) {
                    var entityFilterValue = {
                        id: type.constraintType,
                        title: _this.getNameForConstraintType(type.constraintType),
                        count: type.count
                    };
                    filterValues.push(entityFilterValue);
                });
                var filterProperty = {
                    refId: "constraintType",
                    title: _this._t("Policy Types"),
                    values: filterValues
                };
                return filterProperty;
            });
        };
        this.getNameForConstraintType = function (constraintType) {
            switch (constraintType) {
                case constants_1.ConstraintType.mgmtActionDisabled:
                    return _this._t("Disallowed Actions");
                case constants_1.ConstraintType.exclude:
                    return _this._t("Exclude from Recommendations");
                default:
                    return null;
            }
        };
        this.getIconForEntityType = function (entityType) {
            switch (entityType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    return "hypervguest";
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    return "hypervcluster";
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    return "datastore";
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    return "hypervhost";
                default:
                    return null;
            }
        };
        this.deleteSelected = function () {
            return _this.vimDialogService.showDeletePolicyConfirmationDialog(_this.selectedItems.items).then(function (confirmed) {
                if (confirmed) {
                    return _this.swApiService.deleteConstraints(_this.selectedItems.items).then(function () {
                        _this.selectedItems.items = [];
                        return _this.updateData();
                    });
                }
                return null;
            });
        };
        this.goToEditConstraint = function (id) {
            var constraint = _this.itemsById[id][0];
            switch (constraint.type) {
                case constants_1.ConstraintType.exclude:
                    _this.vimRoutingService.goToExcludeConstraintDetail(constraint.id);
                    break;
                case constants_1.ConstraintType.mgmtActionDisabled:
                    _this.vimRoutingService.goToDisallowActionsConstraintDetail(constraint.id);
                    break;
            }
        };
        this.formatExpiration = function (expiration) {
            if (!expiration) {
                return _this._t("Doesn't expire");
            }
            var now = _this.dateTimeService.getOrionNowDate();
            var expirationFormated = _this.dateTimeService.formatDateOnly(expiration);
            if (expiration > now) {
                return _this._t("Expires on {0}").format(expirationFormated);
            }
            else {
                return _this._t("Expired on {0}").format(expirationFormated);
            }
        };
        this.updateData = function () {
            if (!_this.remoteControl) {
                return _this.$q.when();
            }
            return _this.remoteControl.refreshListData();
        };
    }
    RecommendationConstraintsTabController.$inject = [
        index_1.ServiceNames.ngScope,
        index_1.ServiceNames.ngLog,
        index_1.ServiceNames.ngQService,
        index_1.ServiceNames.ngState,
        index_1.ServiceNames.swisService,
        index_1.ServiceNames.routingService,
        index_2.ServiceNames.routingService,
        index_1.ServiceNames.pluggabilityService,
        index_1.ServiceNames.dataGroupStatusService,
        index_1.ServiceNames.swApiService,
        index_2.ServiceNames.swisService,
        index_2.ServiceNames.dialogService,
        index_1.ServiceNames.dateTimeService,
        index_1.ServiceNames.getTextService
    ];
    return RecommendationConstraintsTabController;
}());
exports.RecommendationConstraintsTabController = RecommendationConstraintsTabController;


/***/ }),
/* 243 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constraints filter class
 *
 * @class
 * @implements IConstraintsFilter
 */
var ConstraintsFilter = /** @class */ (function () {
    function ConstraintsFilter() {
    }
    return ConstraintsFilter;
}());
exports.ConstraintsFilter = ConstraintsFilter;


/***/ }),
/* 244 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Supported filter values for constraints list
 *
 * @class
 * @implements IConstraintListFilterValues
 **/
var ConstraintListFilterValues = /** @class */ (function () {
    function ConstraintListFilterValues() {
        this.affectedEntity = [];
        this.dataGroupEntity = [];
        this.constraintType = [];
    }
    return ConstraintListFilterValues;
}());
exports.ConstraintListFilterValues = ConstraintListFilterValues;


/***/ }),
/* 245 */
/***/ (function(module, exports) {

module.exports = "<div class=constraints-tab ng-controller=\"RecommendationConstraintsTabController as vm\"> <xui-filtered-list items-source=vm.itemsSource datasource-fn=\"vm.getData(filters, pagination, sorting, searching)\" filter-properties-fn=vm.getFilterProperties() options=::vm.options sorting=::vm.sorting pagination-data=::vm.pagination filter-values=vm.filterValues selection=vm.selectedItems filter-property-groups=::vm.objectTypes controller=vm remote-control=vm.remoteControl> <xui-grid-toolbar-container> <xui-toolbar> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() === 0\"> <xui-menu icon=add title=\"_t(Add policy)\" display-style=tertiary _ta> <xui-menu-action action=vm.vimRoutingService.goToExcludeConstraints(); _t>Exclude Objects from Recommendations</xui-menu-action> <xui-menu-action action=vm.vimRoutingService.goToDisallowActionsConstraints(); _t>Disallow Actions for Recommendations</xui-menu-action> </xui-menu> </xui-toolbar-item> <xui-toolbar-item ng-if-start=\"vm.selectedItemsCount() === 1\"> <xui-button icon=edit display-style=tertiary ng-click=vm.goToEditConstraint(vm.selectedItems.items[0]) _t> Edit </xui-button> </xui-toolbar-item> <xui-toolbar-divider ng-if-end></xui-toolbar-divider> <xui-toolbar-item ng-if=\"vm.selectedItemsCount() > 0\"> <xui-button ng-click=vm.deleteSelected() icon=delete display-style=tertiary _t> Delete </xui-button> </xui-toolbar-item> </xui-toolbar> </xui-grid-toolbar-container> </xui-filtered-list> </div> <script type=text/ng-template id=item-template> <div class=\"row detail\">\n        <div class=\"col-xs-6\" ng-click=\"vm.goToEditConstraint(item.id)\">\n           <span class=\"name\">{{::item.name}}</span>\n           <span class=\"description\">{{::item.description}}</span>\n        </div>\n        <div class=\"col-xs-3\" _t>\n            Created {{::item.dateCreated | date:'short'}} by {{::item.userId}}\n        </div>\n        <div class=\"col-xs-3\" ng-if=\"item.expirationDate\">\n            {{vm.formatExpiration(item.expirationDate)}}\n        </div>\n    </div> </script> ";

/***/ }),
/* 246 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var pluginTemplateUrls_1 = __webpack_require__(6);
var constraintsPicker_controller_1 = __webpack_require__(247);
var configTemplate = function ($templateCache) {
    $templateCache.put(pluginTemplateUrls_1.PluginTemplateUrls.constraintsPicker, __webpack_require__(248));
};
exports.default = function (module) {
    module.controller("ConstraintsPickerController", constraintsPicker_controller_1.ConstraintsPickerController);
    module.app().run([index_1.ServiceNames.ngTemplateCacheService, configTemplate]);
};


/***/ }),
/* 247 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var constraint_1 = __webpack_require__(13);
var constants_1 = __webpack_require__(5);
var constraintObject_1 = __webpack_require__(41);
var constants_2 = __webpack_require__(5);
var ISelectableConstraint = /** @class */ (function () {
    function ISelectableConstraint() {
    }
    return ISelectableConstraint;
}());
/**
 * Constraints picker controller
 *
 * @class
 */
var ConstraintsPickerController = /** @class */ (function () {
    function ConstraintsPickerController(swisService, dateTimeService, _t) {
        var _this = this;
        this.swisService = swisService;
        this.dateTimeService = dateTimeService;
        this._t = _t;
        this.init = function (context) {
            _this.availableConstraintTypes = [constants_1.ConstraintType.mgmtActionDisabled, constants_1.ConstraintType.exclude];
            _this.selectedConstraintIndex = "0";
            _this.constraintsPickerContext = context;
            _this.constraintsPickerContext.getConstraints = _this.createConstraints;
            return _this.swisService.getRecommendationActions([_this.constraintsPickerContext.recommendation.id]).then(function (actions) {
                _this.handleLoadedActions(actions || []);
            });
        };
        this.handleLoadedActions = function (actions) {
            _this.actions = _.filter(actions, function (a) { return a.Enabled && a.Optional === false; });
            _this.selectableConstraints = _this.getSelectableConstraints(_this.actions);
        };
        this.getSelectableConstraints = function (actions) {
            var selectableConstraints = [];
            _.forEach(_this.actions, function (action) {
                _.forEach(_this.availableConstraintTypes, function (constraintName) {
                    selectableConstraints.push({
                        actionId: action.ActionID,
                        type: constraintName,
                        radioCaption: _this.getRadioCaption(action, constraintName)
                    });
                });
            });
            return selectableConstraints;
        };
        this.createConstraints = function () {
            var selectedConstraint = _this.selectableConstraints[+_this.selectedConstraintIndex];
            if (!selectedConstraint) {
                return null;
            }
            var action = _.find(_this.actions, function (a) { return a.ActionID === selectedConstraint.actionId; });
            var constraint;
            if (selectedConstraint.type === constants_1.ConstraintType.exclude) {
                constraint = new constraint_1.Constraint({
                    id: 0,
                    name: _this._t("Exclude 1 VM from recommendations"),
                    description: "",
                    type: constants_1.ConstraintType.exclude,
                    dateCreated: _this.dateTimeService.getOrionNowDate(),
                    enabled: true,
                    owner: "VIM"
                });
            }
            else if (selectedConstraint.type === constants_1.ConstraintType.mgmtActionDisabled) {
                constraint = new constraint_1.Constraint({
                    id: 0,
                    name: _this._t("Disallow {0} for 1 VM").format(_this.convertActionTypeToConstraintActionName(action.Type)),
                    description: "",
                    type: constants_1.ConstraintType.mgmtActionDisabled,
                    dateCreated: _this.dateTimeService.getOrionNowDate(),
                    enabled: true,
                    owner: "VIM",
                    parameters: _this.convertActionToConstraintParameters(action)
                });
            }
            else {
                return null;
            }
            constraint.constraintObjects = [new constraintObject_1.ConstraintObject({
                    constraintId: 0,
                    entityIdentifier: "VVM:" + action.Details.VirtualMachineId,
                    objectId: action.Details.VirtualMachineId,
                    entityType: "VirtualMachine"
                })];
            return [constraint];
        };
        this.convertActionToConstraintParameters = function (action) {
            switch (action.Type) {
                case constants_2.ActionTypes.changeSettings:
                    return [constants_1.ConstraintParameters.changeCpuAction, constants_1.ConstraintParameters.changeMemoryAction];
                case constants_2.ActionTypes.performMigration:
                    return [constants_1.ConstraintParameters.migrationAction];
                case constants_2.ActionTypes.performRelocation:
                    return [constants_1.ConstraintParameters.relocationAction];
                default:
                    return null;
            }
        };
        this.convertActionTypeToConstraintActionName = function (actionType) {
            switch (actionType) {
                case constants_2.ActionTypes.changeSettings:
                    return _this._t("2 actions");
                case constants_2.ActionTypes.performMigration:
                    return _this._t("\"Move VM to a Different Host\" action");
                case constants_2.ActionTypes.performRelocation:
                    return _this._t("\"Move VM to a Different Storage\" action");
                default:
                    return actionType;
            }
        };
        this.getEntityCaption = function (action) {
            return action.Details.VirtualMachineName;
        };
        this.getRadioCaption = function (action, constraintType) {
            switch (constraintType) {
                case constants_1.ConstraintType.mgmtActionDisabled:
                    return _this.getDisallowRadioCaption(action);
                case constants_1.ConstraintType.exclude:
                    return _this.getExcludeRadioCaption(action);
            }
        };
        this.getDisallowRadioCaption = function (action) {
            var entityCaption = _this.getEntityCaption(action);
            switch (action.Type) {
                case constants_2.ActionTypes.changeSettings:
                    return _this._t("Exclude {0} from any resource allocation change recommendation.").format(entityCaption);
                case constants_2.ActionTypes.performMigration:
                    return _this._t("Exclude {0} from any host migration recommendation.").format(entityCaption);
                case constants_2.ActionTypes.performRelocation:
                    return _this._t("Exclude {0} from any storage migration recommendation.").format(entityCaption);
                default:
                    return _this._t("Exclude {0} from any {1} recommendation.").format(entityCaption, action.Type);
            }
        };
        this.getExcludeRadioCaption = function (action) {
            var entityCaption = _this.getEntityCaption(action);
            return _this._t("Exclude {0} from all recommendations").format(entityCaption);
        };
    }
    ConstraintsPickerController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(1, di_1.Inject(index_1.ServiceNames.dateTimeService)),
        __param(2, di_1.Inject(index_1.ServiceNames.getTextService)),
        __metadata("design:paramtypes", [Object, Object, Function])
    ], ConstraintsPickerController);
    return ConstraintsPickerController;
}());
exports.ConstraintsPickerController = ConstraintsPickerController;


/***/ }),
/* 248 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"ConstraintsPickerController as vm\" ng-init=vm.init(context)> <div ng-if=\"vm.actions.length > 0\"> <xui-radio ng-repeat=\"constraint in vm.selectableConstraints track by $index\" ng-model=vm.selectedConstraintIndex ng-value=$index.toString()> {{::constraint.radioCaption}} </xui-radio> </div> <div ng-if=\"vm.actions.length === 0\"> <xui-message type=warning _t>There are no actions for creating of the policy.</xui-message> </div> </div> ";

/***/ }),
/* 249 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var pluginTemplateUrls_1 = __webpack_require__(6);
var strategiesSettings_controller_1 = __webpack_require__(250);
var configTemplate = function ($templateCache) {
    $templateCache.put(pluginTemplateUrls_1.PluginTemplateUrls.strategiesSettings, __webpack_require__(251));
};
exports.default = function (module) {
    module.controller("StrategiesSettingsController", strategiesSettings_controller_1.StrategiesSettingsController);
    module.app().run([index_1.ServiceNames.ngTemplateCacheService, configTemplate]);
};


/***/ }),
/* 250 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var di_1 = __webpack_require__(1);
var constants_1 = __webpack_require__(5);
/**
 * Strategies settings controller
 *
 * @class
 */
var StrategiesSettingsController = /** @class */ (function () {
    function StrategiesSettingsController($scope, $q, $log, _t, swisService, orionInfoService) {
        var _this = this;
        this.$scope = $scope;
        this.$q = $q;
        this.$log = $log;
        this._t = _t;
        this.swisService = swisService;
        this.orionInfoService = orionInfoService;
        this.strategyGroup = constants_1.StrategyGroup;
        this.strategyType = constants_1.StrategyType;
        this.init = function (contextPromise) {
            _this.strategiesConfig = {};
            _this.thresholdsHelpLink = "#";
            _this.isRecommendationsEnabled = false;
            var helpLinkPromise = _this.getThresholdsHelpLink();
            var initPromise = contextPromise.then(function (context) {
                _this.isRecommendationsEnabled = context.recommendationsEnabled;
                _this.$scope.$on("recommendationsEnabledChanged", _this.onRecommendationsEnabledChanged);
                return _this.swisService.getStrategiesByModule("VIM")
                    .then(function (strategiesDictionary) {
                    _this.initByStrategiesSettings(strategiesDictionary);
                });
            });
            return _this.$q.all([helpLinkPromise, initPromise]);
        };
        /**
         * Initializes enabled strategies by dictionary(id,enabled)
         */
        this.initByStrategiesSettings = function (strategies) {
            _.each(strategies, function (strategy, id) {
                _this.strategiesConfig[strategy.groupId] = _this.strategiesConfig[strategy.groupId] || {};
                _this.strategiesConfig[strategy.groupId][strategy.id] = strategy;
            });
        };
        this.getThresholdsHelpLink = function () {
            return _this.orionInfoService.getHelpUrl("OrionVMPHHowThresholdsAffectRecommendations").then(function (result) {
                _this.thresholdsHelpLink = result;
            });
        };
        this.onRecommendationsEnabledChanged = function (e, enabled) {
            _this.isRecommendationsEnabled = enabled;
        };
    }
    StrategiesSettingsController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(2, di_1.Inject(index_1.ServiceNames.ngLog)),
        __param(3, di_1.Inject(index_1.ServiceNames.getTextService)),
        __param(4, di_1.Inject(index_1.ServiceNames.swisService)),
        __param(5, di_1.Inject(index_1.ServiceNames.orionInfoService)),
        __metadata("design:paramtypes", [Object, Function, Object, Function, Object, Object])
    ], StrategiesSettingsController);
    return StrategiesSettingsController;
}());
exports.StrategiesSettingsController = StrategiesSettingsController;


/***/ }),
/* 251 */
/***/ (function(module, exports) {

module.exports = "<div ng-controller=\"StrategiesSettingsController as vm\" ng-init=vm.init(context)> <re-strategy-switch-group name=\"_t(VM Right Sizing Optimizations:)\" description=\"_t(Identifies VMs that do not have enough resources to accommodate workload and recommends to provision more resources.<br>Identifies VMs that have too many resources allocated to them and recommends to reduce the allocated resources.)\" config=vm.strategiesConfig[vm.strategyGroup.vmSizing] disabled=!vm.isRecommendationsEnabled _ta> <re-strategy-switch name=_t(Over-allocation) description=\"_t(Reclaim unused CPU or memory resources from VMs.)\" config=vm.strategiesConfig[vm.strategyGroup.vmSizing][vm.strategyType.overallocation] disabled=!vm.isRecommendationsEnabled layout=compact _ta></re-strategy-switch> <re-strategy-switch name=_t(Under-allocation) description=\"_t(Provision additional CPU or memory resources to VMs with high utilization.)\" config=vm.strategiesConfig[vm.strategyGroup.vmSizing][vm.strategyType.underallocation] disabled=!vm.isRecommendationsEnabled layout=compact _ta></re-strategy-switch> </re-strategy-switch-group> <re-strategy-switch-group name=\"_t(Host Performance and Capacity Assurance:)\" description=\"_t(Identifies hosts that will run out of CPU or memory resources.)\" config=vm.strategiesConfig[vm.strategyGroup.hostSolvingCritical] disabled=!vm.isRecommendationsEnabled _ta> <re-strategy-switch name=\"_t(Active recommendations)\" description=\"_t(Identifies problems that have currently exceeded a threshold. These recommendations will resolve associated alerts.)\" config=vm.strategiesConfig[vm.strategyGroup.hostSolvingCritical][vm.strategyType.realtimeCriticalSituation] disabled=!vm.isRecommendationsEnabled layout=compact _ta></re-strategy-switch> <re-strategy-switch name=\"_t(Predictive recommendations)\" description=\"_t(Identifies usage patterns using historical data to predict a problem that will occur in less than 7 days.)\" config=vm.strategiesConfig[vm.strategyGroup.hostSolvingCritical][vm.strategyType.criticalSituation] disabled=!vm.isRecommendationsEnabled layout=compact _ta></re-strategy-switch> <xui-message type=info allow-dismiss=false> <strong _t>You can modify <a href=/Orion/VIM/Admin/BaselineThresholds.aspx>virtualization thresholds</a> to adjust recommendations that are generated by this strategy.</strong><br> <a href={{vm.thresholdsHelpLink}} target=_blank _t>» Learn More about how thresholds are affecting recommendations</a> </xui-message> </re-strategy-switch-group> <re-strategy-switch-group name=\"_t(Storage Optimizations:)\" description=\"_t(Identifies storage running out of space.)\" config=vm.strategiesConfig[vm.strategyGroup.storage] disabled=!vm.isRecommendationsEnabled _ta> <re-strategy-switch name=\"_t(Storage Capacity Assurance:)\" description=\"_t(Identifies host storage that will have storage capacity depleted.)\" config=vm.strategiesConfig[vm.strategyGroup.storage][vm.strategyType.storageCapacity] disabled=!vm.isRecommendationsEnabled layout=compact _ta></re-strategy-switch> </re-strategy-switch-group> <re-strategy-switch name=\"_t(Balancing VM Workloads on Hosts:)\" description=\"_t(Distributes VMs across hosts to balance VM workload utilization.)\" config=vm.strategiesConfig[vm.strategyGroup.vmPlacement][vm.strategyType.optimizingVmPlacement] disabled=!vm.isRecommendationsEnabled _ta></re-strategy-switch> </div> ";

/***/ }),
/* 252 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(253);
var valueFormatFilter_1 = __webpack_require__(254);
exports.default = function (module) {
    module.filter(index_1.FilterNames.valueFormatFilter, valueFormatFilter_1.ValueFormatFilterFactory);
};


/***/ }),
/* 253 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Names of VIM filters
 *
 * @class
 */
var FilterNames = /** @class */ (function () {
    function FilterNames() {
    }
    FilterNames.valueFormatFilter = "vimValueFormat";
    return FilterNames;
}());
exports.FilterNames = FilterNames;


/***/ }),
/* 254 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(40);
/**
 * VIM value format filter factory
 *
 * @function
 */
exports.ValueFormatFilterFactory = function (stringService, numberFilter, bytesFilter) {
    return function (input, format, plusSign) {
        if (stringService.equalsIgnoreCase(format, "cpu")) {
            return numberFilter(input, 0, " CPU", plusSign);
        }
        if (stringService.equalsIgnoreCase(format, "percent")) {
            return numberFilter(input, 2, "%", plusSign);
        }
        if (stringService.equalsIgnoreCase(format, "bytes")) {
            return bytesFilter(input, 1, plusSign);
        }
        return numberFilter(input, 2);
    };
};
exports.ValueFormatFilterFactory.$inject = [
    index_1.ServiceNames.stringService,
    index_2.FilterNames.numberFormatFilter + "Filter",
    index_2.FilterNames.bytesFormatFilter + "Filter"
];


/***/ }),
/* 255 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(3);
var swis_service_1 = __webpack_require__(256);
var vmInfo_service_1 = __webpack_require__(260);
var routing_service_1 = __webpack_require__(261);
var dialog_service_1 = __webpack_require__(262);
var recommendationDetail_service_1 = __webpack_require__(263);
exports.default = function (module) {
    module.service(index_1.ServiceNames.swisService, swis_service_1.SwisService);
    module.service(index_1.ServiceNames.vmInfoService, vmInfo_service_1.VmInfoService);
    module.service(index_1.ServiceNames.routingService, routing_service_1.RoutingService);
    module.service(index_1.ServiceNames.dialogService, dialog_service_1.DialogService);
    module.service(index_1.ServiceNames.recommendationDetailService, recommendationDetail_service_1.RecommendationDetailService);
};


/***/ }),
/* 256 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(4);
var constraintObject_1 = __webpack_require__(257);
var dataGroupObject_1 = __webpack_require__(258);
var managedEntity_1 = __webpack_require__(259);
var constraint_1 = __webpack_require__(13);
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var SwisService = /** @class */ (function () {
    function SwisService(swis, $q, dateTimeService) {
        var _this = this;
        this.swis = swis;
        this.$q = $q;
        this.dateTimeService = dateTimeService;
        this.getMonitoredApplicationsByVm = function (vmId) {
            var swql = "SELECT ApplicationID, Name FROM Orion.APM.Application \n                WHERE Node.VirtualMachine.VirtualMachineID = " + vmId;
            return _this.swis.runQuery(swql).then(function (results) {
                return results.rows;
            })
                .catch(function () { return []; });
        };
        this.getConstrainedObjectType = function (constraintId) {
            var swql = "SELECT TOP 1 EntityType FROM Orion.Recommendations.ConstraintObjects WHERE ConstraintID = " + constraintId;
            return _this.swis.runQuery(swql).then(function (result) {
                if (result.rows.length === 0) {
                    return null;
                }
                switch (result.rows[0].EntityType) {
                    case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                        return enums_1.EntityType.VirtualMachine;
                    case enums_1.EntityType[enums_1.EntityType.Host]:
                        return enums_1.EntityType.Host;
                    case enums_1.EntityType[enums_1.EntityType.Cluster]:
                        return enums_1.EntityType.Cluster;
                    case enums_1.EntityType[enums_1.EntityType.Datastore]:
                        return enums_1.EntityType.Datastore;
                    default:
                        return enums_1.EntityType.VirtualMachine;
                }
            });
        };
        this.getConstrainedObjects = function (entityType) {
            var swql = _this.getQueryForConstrainedObjects(entityType);
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    return new constraintObject_1.ConstraintObject({
                        constraintId: row.ConstraintID,
                        entityIdentifier: row.EntityIdentifier,
                        objectId: row.ObjectID,
                        entityType: row.EntityType,
                        status: row.StatusIcon,
                        displayName: row.DisplayName,
                        parentName: row.ParentName
                    });
                });
            });
        };
        this.getConstraintTypes = function () {
            var swql = "SELECT Count(*) AS Cnt, Type FROM Orion.Recommendations.Constraints GROUP BY Type";
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    return {
                        constraintType: row.Type,
                        count: row.Cnt
                    };
                });
            });
        };
        this.getDataGroupObjects = function (entityType) {
            var swql = _this.getQueryForDataGroupObjects(entityType);
            if (!swql) {
                return _this.$q.resolve([]);
            }
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    return new dataGroupObject_1.DataGroupObject({
                        dataGroupId: row.NetObjectID,
                        name: row.DisplayName,
                        constraintsCount: row.Cnt,
                        status: row.Status,
                        entityType: row.EntityType
                    });
                });
            });
        };
        this.getClusterObjects = function (entityType) {
            var swql = _this.getQueryForClusterObjects(entityType);
            if (!swql) {
                return _this.$q.resolve([]);
            }
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    return new dataGroupObject_1.DataGroupObject({
                        dataGroupId: row.NetObjectID,
                        name: row.DisplayName,
                        constraintsCount: row.Cnt,
                        status: row.Status,
                        entityType: row.EntityType
                    });
                });
            });
        };
        this.getHostObjects = function (entityType) {
            var swql = _this.getQueryForHostObjects(entityType);
            if (!swql) {
                return _this.$q.resolve([]);
            }
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    return new dataGroupObject_1.DataGroupObject({
                        dataGroupId: row.NetObjectID,
                        name: row.DisplayName,
                        constraintsCount: row.Cnt,
                        status: row.Status,
                        entityType: row.EntityType
                    });
                });
            });
        };
        this.getDatastoreObjects = function (entityType) {
            var swql = _this.getQueryForDatastoreObjects(entityType);
            if (!swql) {
                return _this.$q.resolve([]);
            }
            return _this.swis.runQuery(swql).then(function (results) {
                return _.map(results.rows, function (row) {
                    return new dataGroupObject_1.DataGroupObject({
                        dataGroupId: row.NetObjectID,
                        name: row.DisplayName,
                        constraintsCount: row.Cnt,
                        status: row.Status,
                        entityType: row.EntityType
                    });
                });
            });
        };
        this.getManagedEntities = function (filter, entityType) {
            var swql = _this.getQueryForManagedEntities(filter, entityType);
            return _this.swis.runQuery(swql).then(function (results) {
                var managedEntities = _.map(results.rows, function (row) {
                    return new managedEntity_1.ManagedEntity({
                        entityId: row.ID,
                        name: row.Name,
                        parentEntityName: row.HostName,
                        status: row.StatusIcon
                    });
                });
                // Deep clone of the filter object
                var newFilter = _.cloneDeep(filter);
                newFilter.pagination = newFilter.pagination || {};
                newFilter.pagination.total = results.total;
                return {
                    managedEntities: managedEntities,
                    filter: newFilter
                };
            });
        };
        this.craftFilterCondition = function (filter, hostSubquery, vmSubquery) {
            var whereConditions = [];
            if (!_.isEmpty(filter.filters.clusterEntity)) {
                var clustersInSubquery = _.map(filter.filters.clusterEntity, function (id) { return "'" + id + "'"; }).join(", ");
                whereConditions.push("('VMC:'+TOSTRING(" + hostSubquery + ".ClusterID)) IN (" + clustersInSubquery + ")");
            }
            if (!_.isEmpty(filter.filters.hostEntity)) {
                var hostsInSubquery = _.map(filter.filters.hostEntity, function (id) { return "'" + id + "'"; }).join(", ");
                whereConditions.push("('VH:'+TOSTRING(" + hostSubquery + ".HostID)) IN (" + hostsInSubquery + ")");
            }
            if (!_.isEmpty(filter.filters.datastoreEntity)) {
                var datastoresInSubquery = _.map(filter.filters.datastoreEntity, function (id) { return "'" + id + "'"; }).join(", ");
                if (vmSubquery) {
                    whereConditions.push("('VMS:'+TOSTRING(" + vmSubquery + ".DataStores.DataStoreID)) IN (" + datastoresInSubquery + ")");
                }
                else {
                    whereConditions.push("('VMS:'+TOSTRING(" + hostSubquery + ".DataStores.DataStoreID)) IN (" + datastoresInSubquery + ")");
                }
            }
            if (!_.isEmpty(whereConditions)) {
                var whereConditionsJoined = whereConditions.join(" AND ");
                return " WHERE " + whereConditionsJoined;
            }
            return "";
        };
        this.getQueryForManagedEntities = function (filter, entityType) {
            var selectedIdsSubquery = "0";
            if (!!filter.constraintId) {
                selectedIdsSubquery = "(CASE WHEN ID IN (SELECT ObjectID FROM Orion.Recommendations.ConstraintObjects WHERE ConstraintID=" + filter.constraintId + ") THEN 1 ELSE 0 END)";
            }
            var vmSwql = "SELECT DISTINCT vm.VirtualMachineID AS ID, vm.Name, status.IconPostfix AS StatusIcon, host.HostName, " + selectedIdsSubquery + " AS Selected FROM Orion.VIM.VirtualMachines AS vm \nINNER JOIN Orion.StatusInfo AS status ON vm.Status = status.StatusID\nINNER JOIN Orion.VIM.Hosts AS host ON vm.HostID = host.HostID\n" + _this.craftFilterCondition(filter, "host", "vm");
            var clusterSwql = "SELECT DISTINCT c.ClusterID AS ID, c.Name, status.IconPostfix AS StatusIcon, c.Name AS HostName, " + selectedIdsSubquery + " AS Selected FROM Orion.VIM.Clusters AS c\nINNER JOIN Orion.StatusInfo AS status ON c.Status = status.StatusID";
            // Filtering by clusters
            if (!_.isEmpty(filter.filters.datastoreEntity)) {
                var datastoresInSubquery = _.map(filter.filters.datastoreEntity, function (id) { return "'" + id + "'"; }).join(", ");
                clusterSwql += " WHERE ('VMS:'+TOSTRING(c.DataStores.DataStoreID)) IN (" + datastoresInSubquery + ")";
            }
            var hostSwql = "SELECT DISTINCT h.HostID AS ID, h.HostName AS Name, status.IconPostfix AS StatusIcon, h.HostName, " + selectedIdsSubquery + " AS Selected FROM Orion.VIM.Hosts AS h\nINNER JOIN Orion.StatusInfo AS status ON h.Status = status.StatusID";
            hostSwql += _this.craftFilterCondition(filter, "h");
            var datastoreSwql = "SELECT DISTINCT ds.DatastoreID AS ID, ds.Name, status.IconPostfix AS StatusIcon, ds.Name AS HostName, " + selectedIdsSubquery + " AS Selected FROM Orion.VIM.Datastores AS ds\nINNER JOIN Orion.StatusInfo AS status ON ds.Status = status.StatusID";
            datastoreSwql += _this.craftFilterCondition(filter, "ds.Hosts");
            var query = "";
            switch (entityType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    query = vmSwql;
                    break;
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    query = clusterSwql;
                    break;
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    query = hostSwql;
                    break;
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    query = datastoreSwql;
                    break;
                default:
                    query = clusterSwql + " UNION ALL (" + hostSwql + ") UNION ALL (" + vmSwql + ") UNION ALL (" + datastoreSwql + ")";
            }
            query = "SELECT ID, Name, StatusIcon, HostName FROM (" + query + ")";
            if (!_.isEmpty(filter.search)) {
                query += "WHERE Name LIKE '%" + filter.search + "%'";
            }
            // Order
            var useDefaultSorting = !filter.sorting || !filter.sorting.sortBy || !filter.sorting.sortBy.id;
            if (useDefaultSorting) {
                query += " ORDER BY Selected DESC, Name ASC";
            }
            else {
                query += " ORDER BY Selected DESC, " + filter.sorting.sortBy.id + " " + filter.sorting.direction;
            }
            // Pagination
            if (filter.pagination) {
                var startRow = (filter.pagination.pageSize * (filter.pagination.page - 1)) + 1;
                var endRow = startRow + filter.pagination.pageSize - 1;
                query += " WITH ROWS " + startRow + " TO " + endRow + " WITH TOTALROWS";
            }
            return query;
        };
        this.getQueryForConstrainedObjects = function (entityType) {
            var vmSwql = "SELECT co.ConstraintID, co.EntityIdentifier, co.ObjectID, co.EntityType, vm.DisplayName, vm.Host.DisplayName AS ParentName, si.IconPostfix AS StatusIcon\nFROM Orion.Recommendations.ConstraintObjects co\nINNER JOIN Orion.VIM.VirtualMachines vm ON co.ObjectID = vm.VirtualMachineID AND co.EntityType = 'VirtualMachine'\nINNER JOIN Orion.StatusInfo si ON si.StatusId = vm.Status";
            var clusterSwql = "SELECT co.ConstraintID, co.EntityIdentifier, co.ObjectID, co.EntityType, cl.DisplayName, NULL AS ParentName, si.IconPostfix AS StatusIcon\nFROM Orion.Recommendations.ConstraintObjects co\nINNER JOIN Orion.VIM.Clusters cl ON co.ObjectID = cl.ClusterID AND co.EntityType = 'Cluster'\nINNER JOIN Orion.StatusInfo si ON si.StatusId = cl.Status";
            var hostSwql = "SELECT co.ConstraintID, co.EntityIdentifier, co.ObjectID, co.EntityType, h.DisplayName, h.Cluster.DisplayName AS ParentName, si.IconPostfix AS StatusIcon\nFROM Orion.Recommendations.ConstraintObjects co\nINNER JOIN Orion.VIM.Hosts h ON co.ObjectID = h.HostID AND co.EntityType = 'Host'\nINNER JOIN Orion.StatusInfo si ON si.StatusId = h.Status";
            var datastoreSwql = "SELECT co.ConstraintID, co.EntityIdentifier, co.ObjectID, co.EntityType, d.DisplayName, NULL AS ParentName, si.IconPostfix AS StatusIcon\nFROM Orion.Recommendations.ConstraintObjects co\nINNER JOIN Orion.VIM.Datastores d ON co.ObjectID = d.DataStoreID AND co.EntityType = 'Datastore'\nINNER JOIN Orion.StatusInfo si ON si.StatusId = d.Status";
            switch (entityType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    return vmSwql;
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    return clusterSwql;
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    return hostSwql;
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    return datastoreSwql;
                default:
                    return clusterSwql + " UNION ALL (" + hostSwql + ") UNION ALL (" + vmSwql + ") UNION ALL (" + datastoreSwql + ")";
            }
        };
        this.getQueryForDataGroupObjects = function (entityType) {
            var vmClusterSwql = "SELECT ConstraintID, c.DisplayName, si.IconPostfix AS StatusIcon, c.OrionIdPrefix + TOSTRING(c.ClusterID) AS NetObjectID, 'Cluster' AS EntityType\nFROM Orion.Recommendations.Constraints\nINNER JOIN Orion.VIM.VirtualMachines vm ON vm.VirtualMachineID = Constraints.ConstraintObjects.ObjectID\nINNER JOIN Orion.VIM.Hosts h ON h.HostID = vm.HostID\nINNER JOIN Orion.VIM.Clusters c ON c.ClusterID = h.ClusterID\nINNER JOIN Orion.StatusInfo si ON si.StatusId = c.Status\nWHERE Constraints.ConstraintObjects.EntityType = 'VirtualMachine' AND Constraints.ConstraintObjects.ObjectID IN (SELECT Hosts.VirtualMachines.VirtualMachineID\nFROM Orion.VIM.Hosts\nWHERE ClusterID IS NOT NULL)";
            var vmStandaloneSwql = "SELECT ConstraintID, h.DisplayName, si.IconPostfix AS StatusIcon, h.OrionIdPrefix + TOSTRING(h.HostID) AS NetObjectID, 'Host' AS EntityType\nFROM Orion.Recommendations.Constraints\nINNER JOIN Orion.VIM.VirtualMachines vm ON vm.VirtualMachineID = Constraints.ConstraintObjects.ObjectID\nINNER JOIN Orion.VIM.Hosts h ON h.HostID = vm.HostID\nINNER JOIN Orion.StatusInfo si ON si.StatusId = h.Status\nWHERE Constraints.ConstraintObjects.EntityType = 'VirtualMachine' AND Constraints.ConstraintObjects.ObjectID IN (SELECT Hosts.VirtualMachines.VirtualMachineID\nFROM Orion.VIM.Hosts\nWHERE ClusterID IS NULL)";
            var hostClusterSwql = "SELECT ConstraintID, c.DisplayName, si.IconPostfix AS StatusIcon, c.OrionIdPrefix + TOSTRING(c.ClusterID) AS NetObjectID, 'Cluster' AS EntityType\nFROM Orion.Recommendations.Constraints\nINNER JOIN Orion.VIM.Hosts h ON h.HostID = Constraints.ConstraintObjects.ObjectID\nINNER JOIN Orion.VIM.Clusters c ON c.ClusterID = h.ClusterID\nINNER JOIN Orion.StatusInfo si ON si.StatusId = c.Status\nWHERE Constraints.ConstraintObjects.EntityType = 'Host' AND Constraints.ConstraintObjects.ObjectID IN (SELECT Hosts.HostID FROM Orion.VIM.Hosts WHERE ClusterID IS NOT NULL)";
            var hostStandaloneSwql = "SELECT ConstraintID, h.DisplayName, si.IconPostfix AS StatusIcon, h.OrionIdPrefix + TOSTRING(h.HostID) AS NetObjectID, 'Host' AS EntityType\nFROM Orion.Recommendations.Constraints\nINNER JOIN Orion.VIM.Hosts h ON h.HostID = Constraints.ConstraintObjects.ObjectID\nINNER JOIN Orion.StatusInfo si ON si.StatusId = h.Status\nWHERE Constraints.ConstraintObjects.EntityType = 'Host' AND Constraints.ConstraintObjects.ObjectID IN (SELECT Hosts.HostID FROM Orion.VIM.Hosts WHERE ClusterID IS NULL)";
            var clusterClusterSwql = "SELECT ConstraintID, c.DisplayName, si.IconPostfix AS StatusIcon, c.OrionIdPrefix + TOSTRING(c.ClusterID) AS NetObjectID, 'Cluster' AS EntityType\nFROM Orion.Recommendations.Constraints\nINNER JOIN Orion.VIM.Clusters c ON c.ClusterID = Constraints.ConstraintObjects.ObjectID\nINNER JOIN Orion.StatusInfo si ON si.StatusId = c.Status\nWHERE Constraints.ConstraintObjects.EntityType = 'Cluster' AND Constraints.ConstraintObjects.ObjectID IN (SELECT ClusterID FROM Orion.VIM.Clusters)";
            var dsClusterSwql = "SELECT ConstraintID, c.DisplayName, si.IconPostfix AS StatusIcon, c.OrionIdPrefix + TOSTRING(c.ClusterID) AS NetObjectID, 'Cluster' AS EntityType\nFROM Orion.Recommendations.Constraints\nINNER JOIN Orion.VIM.Datastores ds ON ds.DatastoreID = Constraints.ConstraintObjects.ObjectID\nINNER JOIN Orion.VIM.Hosts h ON h.HostID IN ds.Hosts.HostID\nINNER JOIN Orion.VIM.Clusters c ON c.ClusterID = h.ClusterID\nINNER JOIN Orion.StatusInfo si ON si.StatusId = c.Status\nWHERE Constraints.ConstraintObjects.EntityType = 'Datastore' AND h.ClusterID IS NOT NULL";
            var dsStandaloneSwql = "SELECT ConstraintID, h.DisplayName, si.IconPostfix AS StatusIcon, h.OrionIdPrefix + TOSTRING(h.HostID) AS NetObjectID, 'Host' AS EntityType\nFROM Orion.Recommendations.Constraints\nINNER JOIN Orion.VIM.Datastores ds ON ds.DatastoreID = Constraints.ConstraintObjects.ObjectID\nINNER JOIN Orion.VIM.Hosts h ON h.HostID IN ds.Hosts.HostID\nINNER JOIN Orion.StatusInfo si ON si.StatusId = h.Status\nWHERE Constraints.ConstraintObjects.EntityType = 'Datastore' AND h.ClusterID IS NULL";
            switch (entityType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT ConstraintID) AS Cnt, EntityType FROM (\n" + vmClusterSwql + " UNION ALL (" + vmStandaloneSwql + ")\n) GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT ConstraintID) AS Cnt, EntityType FROM (\n" + clusterClusterSwql + "\n) GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT ConstraintID) AS Cnt, EntityType FROM (\n" + hostClusterSwql + " UNION ALL (" + hostStandaloneSwql + ")\n) GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT ConstraintID) AS Cnt, EntityType FROM (\n" + dsClusterSwql + " UNION ALL (" + dsStandaloneSwql + ")\n) GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
                default:
                    return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT ConstraintID) AS Cnt, EntityType FROM (\n" + vmClusterSwql + " UNION ALL (" + vmStandaloneSwql + ") UNION ALL (" + clusterClusterSwql + ") UNION ALL (" + hostClusterSwql + ") UNION ALL (" + hostStandaloneSwql + ") UNION ALL (" + dsClusterSwql + ") UNION ALL (" + dsStandaloneSwql + ")\n) GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
            }
        };
        this.getFinalQueryForObjects = function (vmSubquery, hostSubquery, dsSubquery, clusterSubquery, entityType) {
            var subquery;
            switch (entityType) {
                case enums_1.EntityType[enums_1.EntityType.VirtualMachine]:
                    subquery = vmSubquery;
                    break;
                case enums_1.EntityType[enums_1.EntityType.Cluster]:
                    subquery = clusterSubquery;
                    break;
                case enums_1.EntityType[enums_1.EntityType.Host]:
                    subquery = hostSubquery;
                    break;
                case enums_1.EntityType[enums_1.EntityType.Datastore]:
                    subquery = dsSubquery;
                    break;
                default:
                    if (clusterSubquery) {
                        return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT s.EntityID) AS Cnt, EntityType FROM (\n" + vmSubquery + " UNION ALL (" + clusterSubquery + ") UNION ALL (" + hostSubquery + ") UNION ALL (" + dsSubquery + ")\n) s GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
                    }
                    return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT s.EntityID) AS Cnt, EntityType FROM (\n" + vmSubquery + " UNION ALL (" + hostSubquery + ") UNION ALL (" + dsSubquery + ")\n) s GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
            }
            if (subquery) {
                return "SELECT DisplayName, StatusIcon, NetObjectID, COUNT(DISTINCT s.EntityID) AS Cnt, EntityType FROM (" + subquery + ") s GROUP BY DisplayName, StatusIcon, NetObjectID, EntityType";
            }
            return "";
        };
        this.getQueryForClusterObjects = function (entityType) {
            var vmSwql = "SELECT c.DisplayName, si.IconPostfix AS StatusIcon, c.OrionIdPrefix + TOSTRING(c.ClusterID) AS NetObjectID, 'Cluster' AS EntityType, c.Hosts.VirtualMachines.VirtualMachineID as EntityID\nFROM Orion.VIM.Clusters c\nINNER JOIN Orion.StatusInfo si ON si.StatusId = c.Status";
            var hostSwql = "SELECT c.DisplayName, si.IconPostfix AS StatusIcon, c.OrionIdPrefix + TOSTRING(c.ClusterID) AS NetObjectID, 'Cluster' AS EntityType, c.Hosts.HostID as EntityID\nFROM Orion.VIM.Clusters c\nINNER JOIN Orion.StatusInfo si ON si.StatusId = c.Status";
            var dsSwql = "SELECT c.DisplayName, si.IconPostfix AS StatusIcon, c.OrionIdPrefix + TOSTRING(c.ClusterID) AS NetObjectID, 'Cluster' AS EntityType, c.DataStores.DatastoreID as EntityID\nFROM Orion.VIM.Clusters c\nINNER JOIN Orion.StatusInfo si ON si.StatusId = c.Status";
            return _this.getFinalQueryForObjects(vmSwql, hostSwql, dsSwql, "", entityType);
        };
        this.getQueryForHostObjects = function (entityType) {
            var vmSwql = "SELECT h.DisplayName, si.IconPostfix AS StatusIcon, h.OrionIdPrefix + TOSTRING(h.HostID) AS NetObjectID, 'Host' AS EntityType, h.VirtualMachines.VirtualMachineID as EntityID\nFROM Orion.VIM.Hosts h\nINNER JOIN Orion.StatusInfo si ON si.StatusId = h.Status";
            var hostSwql = "SELECT h.DisplayName, si.IconPostfix AS StatusIcon, h.OrionIdPrefix + TOSTRING(h.HostID) AS NetObjectID, 'Host' AS EntityType, h.HostID as EntityID\nFROM Orion.VIM.Hosts h\nINNER JOIN Orion.StatusInfo si ON si.StatusId = h.Status";
            var dsSwql = "SELECT h.DisplayName, si.IconPostfix AS StatusIcon, h.OrionIdPrefix + TOSTRING(h.HostID) AS NetObjectID, 'Host' AS EntityType, h.DataStores.DatastoreID as EntityID\nFROM Orion.VIM.Hosts h\nINNER JOIN Orion.StatusInfo si ON si.StatusId = h.Status";
            return _this.getFinalQueryForObjects(vmSwql, hostSwql, dsSwql, "", entityType);
        };
        this.getQueryForDatastoreObjects = function (entityType) {
            var vmSwql = "SELECT ds.DisplayName, si.IconPostfix AS StatusIcon, ds.OrionIdPrefix + TOSTRING(ds.DataStoreID) AS NetObjectID, 'DataStore' AS EntityType, ds.VirtualMachines.VirtualMachineID as EntityID\nFROM Orion.VIM.DataStores ds\nINNER JOIN Orion.StatusInfo si ON si.StatusId = ds.Status";
            var hostSwql = "SELECT ds.DisplayName, si.IconPostfix AS StatusIcon, ds.OrionIdPrefix + TOSTRING(ds.DataStoreID) AS NetObjectID, 'DataStore' AS EntityType, ds.Hosts.HostID as EntityID\nFROM Orion.VIM.DataStores ds\nINNER JOIN Orion.StatusInfo si ON si.StatusId = ds.Status";
            var clusterSwql = "SELECT ds.DisplayName, si.IconPostfix AS StatusIcon, ds.OrionIdPrefix + TOSTRING(ds.DataStoreID) AS NetObjectID, 'DataStore' AS EntityType, ds.Clusters.ClusterID as EntityID\nFROM Orion.VIM.DataStores ds\nINNER JOIN Orion.StatusInfo si ON si.StatusId = ds.Status WHERE ds.Clusters.ClusterID IS NOT NULL";
            var dsSwql = "SELECT ds.DisplayName, si.IconPostfix AS StatusIcon, ds.OrionIdPrefix + TOSTRING(ds.DataStoreID) AS NetObjectID, 'DataStore' AS EntityType, ds.DatastoreID as EntityID\nFROM Orion.VIM.DataStores ds\nINNER JOIN Orion.StatusInfo si ON si.StatusId = ds.Status";
            return _this.getFinalQueryForObjects(vmSwql, hostSwql, dsSwql, clusterSwql, entityType);
        };
    }
    SwisService.prototype.getConstraint = function (id) {
        var _this = this;
        var swql = "SELECT ConstraintID, Name, Description, Type, DateCreated, ExpirationDate, Enabled, UserID, Constraints.ConstraintParameters.Parameter FROM Orion.Recommendations.Constraints\nWHERE ConstraintID = " + id;
        return this.swis.runQuery(swql).then(function (result) {
            var params = [];
            _.forEach(result.rows, function (row) {
                if (row.Parameter) {
                    params.push(row.Parameter);
                }
            });
            var dateCreated = result.rows[0].DateCreated;
            var expirationDate = result.rows[0].ExpirationDate;
            return new constraint_1.Constraint({
                id: result.rows[0].ConstraintID,
                name: result.rows[0].Name,
                description: result.rows[0].Description,
                type: result.rows[0].Type,
                dateCreated: dateCreated ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(dateCreated)) : null,
                expirationDate: expirationDate ? _this.dateTimeService.convertMomentToOrionDate(moment.utc(expirationDate)) : null,
                enabled: result.rows[0].Enabled,
                constraintObjects: null,
                userId: result.rows[0].UserID,
                parameters: params
            });
        });
    };
    SwisService = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.apolloSwisService)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngQService)),
        __param(2, di_1.Inject(index_1.ServiceNames.apolloDateTimeService)),
        __metadata("design:paramtypes", [Object, Function, Object])
    ], SwisService);
    return SwisService;
}());
exports.SwisService = SwisService;


/***/ }),
/* 257 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var constraintObject_1 = __webpack_require__(41);
;
/**
 * Representantion of the one single constraint object.
 *
 * @class
 * @implements IConstraint
 **/
var ConstraintObject = /** @class */ (function (_super) {
    __extends(ConstraintObject, _super);
    function ConstraintObject(args) {
        var _this = _super.call(this, args) || this;
        if (!args) {
            return _this;
        }
        _this.status = args.status;
        _this.displayName = args.displayName;
        _this.parentName = args.parentName;
        return _this;
    }
    ;
    return ConstraintObject;
}(constraintObject_1.ConstraintObject));
exports.ConstraintObject = ConstraintObject;


/***/ }),
/* 258 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Representantion of the one single dataGroup object.
 *
 * @class
 * @implements IDataGroupObject
 **/
var DataGroupObject = /** @class */ (function () {
    function DataGroupObject(args) {
        if (!args) {
            return;
        }
        this.dataGroupId = args.dataGroupId;
        this.name = args.name;
        this.constraintsCount = args.constraintsCount;
        this.status = args.status;
        this.entityType = args.entityType;
    }
    ;
    return DataGroupObject;
}());
exports.DataGroupObject = DataGroupObject;


/***/ }),
/* 259 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
;
/**
 * Representantion of the single managed entity
 *
 * @class
 * @implements IManagedEntity
 **/
var ManagedEntity = /** @class */ (function () {
    function ManagedEntity(args) {
        if (!args) {
            return;
        }
        this.entityId = args.entityId;
        this.name = args.name;
        this.status = args.status;
        this.parentEntityName = args.parentEntityName;
    }
    return ManagedEntity;
}());
exports.ManagedEntity = ManagedEntity;


/***/ }),
/* 260 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
var enums_1 = __webpack_require__(2);
var index_2 = __webpack_require__(3);
/**
 * VM info service
 *
 * @class
 */
var VmInfoService = /** @class */ (function () {
    function VmInfoService(cacheService, swProductsService, vimSwisService) {
        var _this = this;
        this.cacheService = cacheService;
        this.swProductsService = swProductsService;
        this.vimSwisService = vimSwisService;
        this.getMonitoredApplicationsByVm = function (vmId) {
            return _this.swProductsService.isProductInstalled(enums_1.SwProduct.SAM)
                .then(function (installed) {
                if (installed) {
                    // if installed, then put it in cache
                    var key = "vim-vm-applications-" + vmId;
                    return _this.cacheService.getOrAdd(key, 5 * 60, function () { return _this.vimSwisService.getMonitoredApplicationsByVm(vmId); })
                        .then(function (item) { return item.value; });
                }
                return [];
            });
        };
    }
    VmInfoService.$inject = [
        index_1.ServiceNames.cacheService,
        index_1.ServiceNames.swProductsService,
        index_2.ServiceNames.swisService
    ];
    return VmInfoService;
}());
exports.VmInfoService = VmInfoService;


/***/ }),
/* 261 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
// RE
var index_1 = __webpack_require__(0);
/**
 * Routing service
 *
 * @class
 * @implements IRoutingService
 */
var RoutingService = /** @class */ (function () {
    function RoutingService($state, routingRegistrationProvider) {
        var _this = this;
        this.$state = $state;
        this.routingRegistrationProvider = routingRegistrationProvider;
        this.goToExcludeConstraints = function (params, options) {
            _this.$state.go(_this.excludeRouteName, params, options);
        };
        this.goToExcludeConstraintDetail = function (id) {
            _this.goToExcludeConstraints({ constraintId: id }, { location: true });
        };
        this.goToDisallowActionsConstraints = function (params, options) {
            _this.$state.go(_this.disallowActionRouteName, params, options);
        };
        this.goToDisallowActionsConstraintDetail = function (id) {
            _this.goToDisallowActionsConstraints({ constraintId: id }, { location: true });
        };
        this.excludeRouteName = routingRegistrationProvider.getPluginConstraintRouteName("VIM", "exclude");
        this.disallowActionRouteName = routingRegistrationProvider.getPluginConstraintRouteName("VIM", "disallowAction");
    }
    RoutingService.$inject = [index_1.ServiceNames.ngState, index_1.ServiceNames.routingRegistrationService];
    return RoutingService;
}());
exports.RoutingService = RoutingService;


/***/ }),
/* 262 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(0);
/**
 * Dialog service implementation
 *
 * @class
 * @interface IDialogService
 */
var DialogService = /** @class */ (function () {
    function DialogService($q, xuiDialogService, _t, stringService) {
        var _this = this;
        this.$q = $q;
        this.xuiDialogService = xuiDialogService;
        this._t = _t;
        this.stringService = stringService;
        this.showDeletePolicyConfirmationDialog = function (policyIds) {
            return _this.xuiDialogService.showModal({
                size: "sm"
            }, {
                hideCancel: false,
                status: "warning",
                actionButtonText: _this._t("Yes"),
                cancelButtonText: _this._t("No"),
                title: (policyIds.length > 1) ? _this._t("Delete Policies") : _this._t("Delete Policy"),
                message: (policyIds.length > 1) ? _this._t("Are you sure you want to delete the selected policies?") : _this._t("Are you sure you want to delete the selected policy?")
            })
                .then(function (result) {
                return !_this.isCancelButtonResult(result);
            });
        };
        /**
         * Indicates whether given dialog result equals to cancel button action
         */
        this.isCancelButtonResult = function (result) {
            if (_.isString(result)) {
                return _this.stringService.equalsIgnoreCase("cancel", result);
            }
            return false;
        };
    }
    DialogService.$inject = [
        index_1.ServiceNames.ngQService,
        index_1.ServiceNames.xuiDialogService,
        index_1.ServiceNames.getTextService,
        index_1.ServiceNames.stringService
    ];
    return DialogService;
}());
exports.DialogService = DialogService;


/***/ }),
/* 263 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = __webpack_require__(4);
/**
 * VIM plugin for recommendation action detail service
 *
 * @class
 * @interface IRecommendationDetailService
 */
var RecommendationDetailService = /** @class */ (function () {
    function RecommendationDetailService() {
        var _this = this;
        /**
         * Array of actions which commonly don't need powering off a VM
         */
        this.nonPoweringOffActions = [enums_1.ManagementActionType.PerformMigration, enums_1.ManagementActionType.PerformRelocation];
        /**
         * Process actions disable on/off actions for non powering off actions
         */
        this.processActions = function (actions) {
            _.each(actions, function (a, i) {
                var actionTypeToFind = enums_1.ManagementActionType[a.Type];
                if (_this.nonPoweringOffActions.indexOf(actionTypeToFind) >= 0) {
                    actions[i - 1].Enabled = false;
                    actions[i + 1].Enabled = false;
                }
            });
        };
    }
    return RecommendationDetailService;
}());
exports.RecommendationDetailService = RecommendationDetailService;
;


/***/ }),
/* 264 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(265);
exports.default = function (module) {
    index_1.default(module);
};


/***/ }),
/* 265 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(266);
var index_2 = __webpack_require__(269);
exports.default = function (module) {
    index_1.default(module);
    index_2.default(module);
};


/***/ }),
/* 266 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(7);
var disallowAction_controller_1 = __webpack_require__(267);
var configRoute = function (provider) {
    provider.registerPluginConstraintRoute({
        pluginName: "VIM",
        constraintName: "disallowAction",
        i18title: "_t(Disallow Management Actions in Recommendations)",
        controller: disallowAction_controller_1.DisallowActionController,
        template: __webpack_require__(268)
    });
};
exports.default = function (module) {
    module.config(configRoute, [index_1.ProviderNames.routingRegistrationProvider]);
};


/***/ }),
/* 267 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(3);
var enums_1 = __webpack_require__(4);
var constants_1 = __webpack_require__(5);
var DisallowActionController = /** @class */ (function () {
    function DisallowActionController($scope, $state, _t, vimSwisService) {
        var _this = this;
        this.$scope = $scope;
        this.$state = $state;
        this._t = _t;
        this.vimSwisService = vimSwisService;
        this.constraintParameters = constants_1.ConstraintParameters;
        this.$onInit = function () {
            _this.initProperties();
            var constraintId = _this.$state.params[constants_1.ConstraintsEditorParamNames.constraintId];
            if (constraintId) {
                _this.editorParameters.constraintId = _.parseInt(constraintId);
            }
            else {
                _this.editorParameters.constraintId = null;
            }
            if (_this.editorParameters.constraintId) {
                _this.vimSwisService.getConstrainedObjectType(_this.editorParameters.constraintId).then(function (result) {
                    //EntityType order is the same as objectTypes
                    _this.selectedObjectType = _this.objectTypes[result];
                });
            }
            if (_this.isEditMode()) {
                _this.vimSwisService.getConstraint(_this.editorParameters.constraintId).then(function (result) {
                    _this.editorParameters.description = result.description;
                    _this.editorParameters.descriptionExpanded = result.description && result.description.length > 0;
                    _this.editorParameters.parameters = result.parameters;
                    _this.editorParameters.expirationDate = result.expirationDate;
                });
            }
            _this.$scope.$watchCollection(function () {
                return [_this.selectedObjectType, _this.editorParameters.parameters, _this.selectAllValues];
            }, function (newValue, oldValue) {
                if (!newValue[0]) {
                    return;
                }
                _this.editorParameters.objectType = newValue[0].value;
                if (newValue[1].length !== oldValue[1].length || _.difference(newValue[1], oldValue[1]).length !== 0) {
                    if (_.intersection(newValue[1], _this.movementValues).length === _this.movementValues.length) {
                        if (newValue[2].indexOf("Move") < 0) {
                            _this.selectAllValues = _.union(newValue[2], ["Move"]);
                        }
                    }
                    else if (newValue[2].indexOf("Move") >= 0) {
                        _this.selectAllValues = _.difference(newValue[2], ["Move"]);
                    }
                    if (_.intersection(newValue[1], _this.configurationChangeValues).length === _this.configurationChangeValues.length) {
                        if (newValue[2].indexOf("Configuration") < 0) {
                            _this.selectAllValues = _.union(_this.selectAllValues, newValue[2], ["Configuration"]);
                        }
                    }
                    else if (newValue[2].indexOf("Configuration") >= 0) {
                        _this.selectAllValues = _.difference(newValue[2], ["Configuration"]);
                    }
                }
                if (newValue[2].length !== oldValue[2].length || _.difference(newValue[2], oldValue[2]).length !== 0) {
                    if (oldValue[2].indexOf("Move") < 0 && newValue[2].indexOf("Move") >= 0) {
                        _this.editorParameters.parameters = _.union(newValue[1], _this.movementValues);
                    }
                    else if (newValue[2].indexOf("Move") < 0 &&
                        oldValue[2].indexOf("Move") >= 0 && _.intersection(newValue[1], _this.movementValues).length === _this.movementValues.length) {
                        _this.editorParameters.parameters = _.difference(newValue[1], _this.movementValues);
                    }
                    else if (oldValue[2].indexOf("Configuration") < 0 && newValue[2].indexOf("Configuration") >= 0) {
                        _this.editorParameters.parameters = _.union(newValue[1], _this.configurationChangeValues);
                    }
                    else if (newValue[2].indexOf("Configuration") < 0 &&
                        oldValue[2].indexOf("Configuration") >= 0 && _.intersection(newValue[1], _this.configurationChangeValues).length === _this.configurationChangeValues.length) {
                        _this.editorParameters.parameters = _.difference(newValue[1], _this.configurationChangeValues);
                    }
                }
            });
        };
        this.isEditMode = function () {
            return !!_this.editorParameters.constraintId;
        };
        this.isAnySelected = function () {
            if (!_this.editorParameters.selectedItems || !_this.editorParameters.selectedItems.items) {
                return false;
            }
            if (_this.editorParameters.selectedItems.items.length > 0 || _this.editorParameters.selectedItems.blacklist) {
                return true;
            }
            return false;
        };
        this.isSelectorAvailable = function () {
            return !_this.isEditMode() && !_this.isAnySelected();
        };
        this.isInfoMessageAvailable = function () {
            return !_this.isEditMode() && _this.isAnySelected();
        };
        this.isDisallowedActionsMessageAvailable = function () {
            return _this.selectedObjectType !== _this.objectTypes[enums_1.EntityType.VirtualMachine];
        };
        this.isResourceChangeAvailable = function () {
            return _this.selectedObjectType !== _this.objectTypes[enums_1.EntityType.Datastore];
        };
    }
    DisallowActionController.prototype.initProperties = function () {
        this.title = this._t("Disallow Actions");
        this.objectTypes = [];
        this.selectAllValues = [];
        this.constraintType = constants_1.ConstraintType.exclude;
        this.constraintParameters = constants_1.ConstraintParameters;
        this.movementValues = [constants_1.ConstraintParameters.migrationAction, constants_1.ConstraintParameters.relocationAction];
        this.configurationChangeValues = [constants_1.ConstraintParameters.changeCpuAction, constants_1.ConstraintParameters.changeMemoryAction];
        this.objectTypes = [
            { name: this._t("Virtual Machine"), value: enums_1.EntityType[enums_1.EntityType.VirtualMachine] },
            { name: this._t("Host"), value: enums_1.EntityType[enums_1.EntityType.Host] },
            { name: this._t("Cluster"), value: enums_1.EntityType[enums_1.EntityType.Cluster] },
            { name: this._t("Datastore"), value: enums_1.EntityType[enums_1.EntityType.Datastore] }
        ];
        this.selectedObjectType = this.objectTypes[enums_1.EntityType.VirtualMachine];
        this.editorParameters = {
            objectType: this.selectedObjectType.value,
            constraintType: constants_1.ConstraintType.mgmtActionDisabled,
            description: "",
            selectedItems: {
                items: [],
                blacklist: false
            },
            parameters: [],
            descriptionExpanded: false,
            expirationDate: null
        };
    };
    ;
    DisallowActionController = __decorate([
        __param(0, di_1.Inject(index_1.ServiceNames.ngScope)),
        __param(1, di_1.Inject(index_1.ServiceNames.ngState)),
        __param(2, di_1.Inject(index_1.ServiceNames.getTextService)),
        __param(3, di_1.Inject(index_2.ServiceNames.swisService)),
        __metadata("design:paramtypes", [Object, Object, Function, Object])
    ], DisallowActionController);
    return DisallowActionController;
}());
exports.DisallowActionController = DisallowActionController;


/***/ }),
/* 268 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=vm.$onInit() class=vim-constraint-view> <xui-page-content page-title={{::vm.title}} page-layout=fullWidth> <div class=\"row row-full\"> <div class=col-xs-12> <h2 _t>1. Select scope of this policy</h2> <div class=clearfix> <div class=entity-selector> <xui-dropdown is-disabled=!vm.isSelectorAvailable() ng-model=vm.selectedObjectType display-value=name items-source=vm.objectTypes> </xui-dropdown> </div> <xui-message ng-show=vm.isInfoMessageAvailable() type=info allow-dismiss=false> <span _t>You can disallow actions for only one object type at the time. Uncheck selected objects to choose different object type.</span> </xui-message> </div> </div> </div> <xui-divider></xui-divider> <constraints-editor-entity-selector editor-parameters=vm.editorParameters> </constraints-editor-entity-selector> <xui-divider></xui-divider> <div class=\"row row-full\"> <div class=col-xs-12> <h2 _t>2. Select Disallowed actions</h2> </div> </div> <div class=\"row row-full\"> <div class=col-xs-12> <xui-message ng-show=vm.isDisallowedActionsMessageAvailable() type=info allow-dismiss=false> <span _t>The following actions are also disabled for all child virtual machines.</span> </xui-message> </div> </div> <div class=\"row row-full\"> <xui-checkbox-group ng-model=vm.selectAllValues> <div class=col-xs-12> <div class=col-xs-4> <xui-checkbox value=\"'Move'\" _t>Move VM</xui-checkbox> </div> <div class=col-xs-4 ng-show=vm.isResourceChangeAvailable()> <xui-checkbox value=\"'Configuration'\" _t>Configuration</xui-checkbox> </div> </div> </xui-checkbox-group> </div> <div class=\"row row-full\"> <xui-checkbox-group ng-model=vm.editorParameters.parameters> <div class=col-xs-4> <div class=checkbox-group-padding> <xui-checkbox value=vm.constraintParameters.migrationAction _t>Move VM to a Different Host</xui-checkbox> <xui-checkbox value=vm.constraintParameters.relocationAction _t>Move VM to a Different Storage</xui-checkbox> </div> </div> <div class=col-xs-4 ng-show=vm.isResourceChangeAvailable()> <div class=checkbox-group-padding> <xui-checkbox value=vm.constraintParameters.changeCpuAction _t>Change CPU Resources</xui-checkbox> <xui-checkbox value=vm.constraintParameters.changeMemoryAction _t>Change Memory Resources</xui-checkbox> </div> </div> </xui-checkbox-group> </div> <xui-divider></xui-divider> <div class=\"row row-full\"> <div class=col-xs-12> <expiration-picker expiration-date=vm.editorParameters.expirationDate></expiration-picker> </div> </div> <xui-divider></xui-divider> <constraints-editor-description editor-parameters=vm.editorParameters> </constraints-editor-description> <constraints-editor-controls editor-parameters=vm.editorParameters> </constraints-editor-controls> </xui-page-content> </div> ";

/***/ }),
/* 269 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* injects from baggage-loader */


Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(7);
var exclude_controller_1 = __webpack_require__(270);
var configRoute = function (provider) {
    provider.registerPluginConstraintRoute({
        pluginName: "VIM",
        constraintName: "exclude",
        i18title: "_t(Exclude from Recommendations)",
        controller: exclude_controller_1.ExcludeController,
        template: __webpack_require__(271)
    });
};
exports.default = function (module) {
    module.config(configRoute, [index_1.ProviderNames.routingRegistrationProvider]);
};


/***/ }),
/* 270 */
/***/ (function(module, exports, __webpack_require__) {


/* injects from baggage-loader */
__webpack_require__(35);

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var di_1 = __webpack_require__(1);
var index_1 = __webpack_require__(0);
var index_2 = __webpack_require__(3);
var enums_1 = __webpack_require__(4);
var constants_1 = __webpack_require__(5);
var ExcludeController = /** @class */ (function () {
    function ExcludeController($scope, $state, _t, vimSwisService) {
        var _this = this;
        this.$scope = $scope;
        this.$state = $state;
        this._t = _t;
        this.vimSwisService = vimSwisService;
        this.virtualMachine = { name: this._t("Virtual Machine"), value: enums_1.EntityType[enums_1.EntityType.VirtualMachine] };
        this.cluster = { name: this._t("Cluster"), value: enums_1.EntityType[enums_1.EntityType.Cluster] };
        this.host = { name: this._t("Host"), value: enums_1.EntityType[enums_1.EntityType.Host] };
        this.datastore = { name: this._t("Datastore"), value: enums_1.EntityType[enums_1.EntityType.Datastore] };
        this.$onInit = function () {
            _this.initProperties();
            var constraintId = _this.$state.params[constants_1.ConstraintsEditorParamNames.constraintId];
            if (constraintId) {
                _this.editorParameters.constraintId = _.parseInt(constraintId);
            }
            else {
                _this.editorParameters.constraintId = null;
            }
            if (_this.editorParameters.constraintId) {
                _this.vimSwisService.getConstrainedObjectType(_this.editorParameters.constraintId).then(function (result) {
                    //EntityType order is the same as objectTypes
                    _this.selectedObjectType = _this.objectTypes[result];
                });
            }
            if (_this.isEditMode()) {
                _this.vimSwisService.getConstraint(_this.editorParameters.constraintId).then(function (result) {
                    _this.editorParameters.description = result.description;
                    _this.editorParameters.descriptionExpanded = result.description && result.description.length > 0;
                    _this.editorParameters.parameters = result.parameters;
                    _this.editorParameters.expirationDate = result.expirationDate;
                });
            }
            _this.$scope.$watch(function () {
                return _this.selectedObjectType;
            }, function (newValue) {
                if (!newValue) {
                    return;
                }
                _this.editorParameters.objectType = newValue.value;
            });
        };
        this.isEditMode = function () {
            return !!_this.editorParameters.constraintId;
        };
        this.isAnySelected = function () {
            if (!_this.editorParameters.selectedItems || !_this.editorParameters.selectedItems.items) {
                return false;
            }
            if (_this.editorParameters.selectedItems.items.length > 0 || _this.editorParameters.selectedItems.blacklist) {
                return true;
            }
            return false;
        };
        this.isSelectorAvailable = function () {
            return !_this.isEditMode() && !_this.isAnySelected();
        };
        this.isInfoMessageAvailable = function () {
            return !_this.isEditMode() && _this.isAnySelected();
        };
    }
    ExcludeController.prototype.initProperties = function () {
        this.title = this._t("Exclude from Recommendations");
        this.objectTypes = [];
        this.constraintType = constants_1.ConstraintType.exclude;
        this.objectTypes = [this.virtualMachine, this.host, this.cluster, this.datastore];
        this.selectedObjectType = this.objectTypes[0];
        this.editorParameters = {
            objectType: this.selectedObjectType.value,
            constraintType: constants_1.ConstraintType.exclude,
            description: "",
            selectedItems: {
                items: [],
                blacklist: false
            },
            parameters: [],
            descriptionExpanded: false,
            expirationDate: null
        };
    };
    ;
    ExcludeController = __decorate([
        __param(0, di_1.Inject("$scope")),
        __param(1, di_1.Inject("$state")),
        __param(2, di_1.Inject(index_1.ServiceNames.getTextService)),
        __param(3, di_1.Inject(index_2.ServiceNames.swisService)),
        __metadata("design:paramtypes", [Object, Object, Function, Object])
    ], ExcludeController);
    return ExcludeController;
}());
exports.ExcludeController = ExcludeController;


/***/ }),
/* 271 */
/***/ (function(module, exports) {

module.exports = "<div ng-init=vm.$onInit()> <xui-page-content page-title={{::vm.title}} page-layout=fullWidth> <div class=\"row row-full\"> <div class=col-xs-12> <h2 _t>Select object types you want to exclude</h2> <div class=clearfix> <div class=entity-selector> <xui-dropdown is-disabled=!vm.isSelectorAvailable() ng-model=vm.selectedObjectType display-value=name items-source=vm.objectTypes> </xui-dropdown> </div> <xui-message ng-show=vm.isInfoMessageAvailable() type=info allow-dismiss=false> <span _t> You can exclude only one object type at the time. Uncheck selected objects to choose different object type.</span> </xui-message> </div> </div> </div> <xui-divider></xui-divider> <constraints-editor-entity-selector editor-parameters=vm.editorParameters> </constraints-editor-entity-selector> <xui-divider></xui-divider> <div class=\"row row-full\"> <div class=col-xs-12> <expiration-picker expiration-date=vm.editorParameters.expirationDate></expiration-picker> </div> </div> <xui-divider></xui-divider> <constraints-editor-description editor-parameters=vm.editorParameters> </constraints-editor-description> <constraints-editor-controls editor-parameters=vm.editorParameters> </constraints-editor-controls> </xui-page-content> </div> ";

/***/ })
/******/ ]);
//# sourceMappingURL=recommendations.js.map