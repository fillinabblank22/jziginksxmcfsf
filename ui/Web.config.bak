﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=301880
  -->
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" requirePermission="false" />
  </configSections>
  <connectionStrings />
  <appSettings>
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
  </appSettings>
  <log4net>
    <appender name="RollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="${ProgramData}\Application Data\SolarWinds\Logs\Orion\ApolloUI.log" />
      <encoding value="utf-8" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="5" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <header type="log4net.Util.PatternString" value="%date *** Starting SolarWinds Orion Web Site, .Net Runtime %property{Runtime.Version} ***%newline" />
        <conversionPattern value="%date [%thread] %-5level %logger - %message%newline" />
      </layout>
    </appender>
    <appender name="HubbleFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="${ProgramData}\Application Data\SolarWinds\Logs\Orion\Hubble.log" />
      <encoding value="utf-8" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="2" />
      <maximumFileSize value="10MB" />
      <layout type="log4net.Layout.PatternLayout">
        <header type="log4net.Util.PatternString" value="*** View this data at http://localhost/Orion/Admin/Hubble.aspx ***%newline" />
        <conversionPattern value="%date [%thread] %-5level %logger - %message%newline" />
      </layout>
    </appender>
    <appender name="OutputDebugStringAppender" type="log4net.Appender.OutputDebugStringAppender">
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date [%thread] %-5level %logger - %message%newline" />
      </layout>
    </appender>
    <logger name="HubbleLogger">
      <level value="ERROR" />
      <additivity value="false" />
      <appender-ref ref="HubbleFileAppender" />
    </logger>
    <root>
      <level value="WARN" />
      <appender-ref ref="RollingLogFileAppender" />
      <appender-ref ref="OutputDebugStringAppender" />
    </root>
  </log4net>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5" />
      </system.Web>
  -->
  <system.web>
    <compilation targetFramework="4.8" />
    <httpRuntime targetFramework="4.8" requestValidationMode="4.5" maxRequestLength="16384" />
    <globalization enableClientBasedCulture="true" culture="auto" uiCulture="auto" />
    <httpModules>
      <remove name="WebProxy" />
      <remove name="i18nRedirector" />
      <remove name="HubbleModule" />
      <remove name="Detector" />
      <remove name="OrionRedirector" />
      <remove name="AutoLoginHandler" />
      <remove name="CookieModule" />
      <remove name="Session" />
      <add name="Session" type="System.Web.SessionState.SessionStateModule" />
    </httpModules>
    <customErrors mode="On" redirectMode="ResponseRewrite" defaultRedirect="~/Views/Shared/Error.cshtml" />
    <pages>
      <namespaces>
        <clear />
      </namespaces>
    </pages>
    <machineKey validation="HMACSHA256" decryption="AES" decryptionKey="AutoGenerate" validationKey="AutoGenerate" />
    <sessionState mode="Off" />
  </system.web>
  <system.webServer>
    <staticContent>
      <remove fileExtension=".svg" />
      <remove fileExtension=".svgz" />
      <mimeMap fileExtension=".svg" mimeType="image/svg+xml" />
      <mimeMap fileExtension=".svgz" mimeType="image/svg+xml" />
      <clientCache cacheControlMode="UseMaxAge" cacheControlMaxAge="365.00:00:00" />
    </staticContent>
    <validation validateIntegratedModeConfiguration="false" />
    <modules>
      <remove name="WebProxy" />
      <remove name="i18nRedirector" />
      <remove name="HubbleModule" />
      <remove name="Detector" />
      <remove name="Session" />
      <remove name="OrionRedirector" />
      <remove name="AutoLoginHandler" />
      <remove name="CookieModule" />
    </modules>
    <handlers>
      <remove name="profiler" />
      <remove name="ExtensionlessUrlHandler-Integrated-4.0" />
      <remove name="OPTIONSVerbHandler" />
      <remove name="TRACEVerbHandler" />
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
      <add name="UrlRoutingHandler" type="System.Web.Routing.UrlRoutingHandler,                 System.Web, Version=4.0.0.0,                 Culture=neutral,                 PublicKeyToken=b03f5f7f11d50a3a" path="/ui/bundles/fonts/*" verb="GET" />
    </handlers>
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" culture="neutral" publicKeyToken="30ad4fe6b2a6aeed" />
        <bindingRedirect oldVersion="0.0.0.0-6.0.0.0" newVersion="6.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Optimization" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="1.1.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-1.6.5135.21930" newVersion="1.6.5135.21930" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.serviceModel>
    <extensions>
      <bindingElementExtensions>
        <add name="gzipMessageEncoding" type="SolarWinds.Orion.Core.Common.Bindings.GZipMessageEncodingElement, SolarWinds.Orion.Core.Common" />
      </bindingElementExtensions>
    </extensions>
    <client>
      <endpoint address="net.tcp://localhost:17777/SW/InformationService/Orion/ssl" binding="netTcpBinding" bindingConfiguration="TransportMessage" name="OrionTcpBinding_InformationService" contract="SolarWinds.InformationService.Contract.IInformationService" />
      <endpoint address="net.tcp://localhost:17777/SolarWinds/InformationService/Orion/ssl" binding="netTcpBinding" bindingConfiguration="TransportMessage" name="OrionTcpBinding_InformationServicev2" contract="SolarWinds.InformationService.Contract2.IInformationService" />
      <endpoint address="net.tcp://localhost:17777/SolarWinds/InformationService/v3/Orion/ssl" binding="netTcpBinding" bindingConfiguration="TransportMessage" name="OrionTcpBinding_InformationServicev3" contract="SolarWinds.InformationService.Contract2.IInformationService" />
      <endpoint address="net.tcp://localhost:17777/SolarWinds/InformationService/v3/Orion/certificate" binding="netTcpBinding" bindingConfiguration="Certificate" name="OrionCertificateTcpBinding_InformationServicev3" contract="SolarWinds.InformationService.Contract2.IInformationService" />
    </client>
    <bindings>
      <customBinding>
        <binding name="UserNameNamedPipe">
          <security authenticationMode="UserNameOverTransport" />
          <binaryMessageEncoding>
            <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
          </binaryMessageEncoding>
          <sslStreamSecurity requireClientCertificate="false" />
          <namedPipeTransport maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" />
        </binding>
        <binding name="SWOIS.Over.NamedPipes">
          <security authenticationMode="UserNameOverTransport" />
          <binaryMessageEncoding>
            <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
          </binaryMessageEncoding>
          <sslStreamSecurity requireClientCertificate="false" />
          <namedPipeTransport maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" />
        </binding>
        <binding name="Core.NetTcpStreamedBinding" sendTimeout="00:20:00" receiveTimeout="00:20:00">
          <gzipMessageEncoding innerMessageEncoding="binaryMessageEncoding" />
          <transactionFlow />
          <security authenticationMode="SecureConversation" requireSecurityContextCancellation="false">
            <secureConversationBootstrap authenticationMode="UserNameOverTransport" />
          </security>
          <sslStreamSecurity requireClientCertificate="false" />
          <tcpTransport maxReceivedMessageSize="2147483647" portSharingEnabled="true" maxBufferSize="2147483647" transferMode="Streamed" />
        </binding>
      </customBinding>
      <netTcpBinding>
        <binding name="Windows" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647">
          <security mode="Transport">
            <transport clientCredentialType="Windows" />
          </security>
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
        </binding>
        <binding name="TransportMessage" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647">
          <security mode="TransportWithMessageCredential">
            <message clientCredentialType="UserName" />
          </security>
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
        </binding>
        <binding name="NPM.WindowsClientOverTcp.ToOrionBusinessLayer" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" sendTimeout="00:05:00" portSharingEnabled="true">
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" maxBytesPerRead="2147483647" maxDepth="2147483647" />
          <security mode="TransportWithMessageCredential">
            <message clientCredentialType="UserName" />
          </security>
        </binding>
        <binding name="Core.WindowsClientOverTcp.ToOrionBusinessLayer" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" sendTimeout="00:05:00" portSharingEnabled="true">
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" maxBytesPerRead="2147483647" maxDepth="2147483647" />
          <security mode="TransportWithMessageCredential">
            <message clientCredentialType="UserName" />
          </security>
        </binding>
        <binding name="SWOIS.Over.TCP" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647">
          <security mode="TransportWithMessageCredential">
            <message clientCredentialType="UserName" />
          </security>
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
        </binding>
        <binding name="Certificate" maxBufferSize="2147483647" maxReceivedMessageSize="2147483647" portSharingEnabled="true">
          <readerQuotas maxStringContentLength="2147483647" maxArrayLength="2147483647" />
          <security mode="Transport">
            <transport clientCredentialType="Certificate" />
          </security>
        </binding>
        <binding name="Core.SWIS.Reporting" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" sendTimeout="00:10:00" receiveTimeout="00:10:00" openTimeout="00:10:00">
          <security mode="Transport">
            <transport clientCredentialType="Certificate" />
          </security>
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
        </binding>
      </netTcpBinding>
      <netNamedPipeBinding>
        <binding name="NamedPipe" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647">
          <security mode="Transport">
            <transport protectionLevel="EncryptAndSign" />
          </security>
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
        </binding>
        <binding name="NPM.NamedPipeClientBinding.ToOrionBusinessLayer" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" sendTimeout="00:03:00">
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
        </binding>
        <binding name="Core.NamedPipeClientBinding.ToOrionBusinessLayer" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" sendTimeout="00:03:00">
          <readerQuotas maxArrayLength="2147483647" maxStringContentLength="2147483647" />
        </binding>
      </netNamedPipeBinding>
    </bindings>
    <behaviors>
      <serviceBehaviors />
    </behaviors>
  </system.serviceModel>
  <system.codedom>
    <compilers>
      <compiler language="c#;cs;csharp" extension=".cs" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:6 /nowarn:1659;1699;1701" />
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.VBCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:14 /nowarn:41008 /define:_MYTYPE=\&quot;Web\&quot; /optionInfer+" />
    </compilers>
  </system.codedom>
</configuration>
<!--ProjectGuid: 3ADF4ED7-9508-42B9-8174-14340C79BF6A-->