﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_ApiPoller_Controls_ApiPollerManagementTasks : UserControl, IManagementTasksPlugin
{
    private readonly string ApiPollerGroupLabel = String.Format("<img src='/Orion/ApiPoller/Images/ApiPoller_icon.png'>&nbsp;{0}",
        Resources.Orion_ApiPollerContent.Management_DisplayName);

    public IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        if (netObject == null) yield break;

        Node node;
        if (TryConvertNetObjectToNode(netObject, out node) && (Profile.AllowAdmin || OrionConfiguration.IsDemoServer))
        {
            var nodeSectionName = CoreWebContent.WEBDATA_AK0_171;

            yield return new ManagementTaskItem
            {
                Group = ApiPollerGroupLabel,
                Section = nodeSectionName,
                LinkInnerHtml = Resources.Orion_ApiPollerContent.Management_Create,
                ClientID = "createPollerLink",
                LinkUrl = $"/apps/apipoller/builder/node/{node.NodeID}"
            };

            yield return new ManagementTaskItem
            {
                Group = ApiPollerGroupLabel,
                Section = nodeSectionName,
                LinkInnerHtml = Resources.Orion_ApiPollerContent.Management_Assign,
                ClientID = "assignPollerLink",
                LinkUrl = $"/apps/apipoller/wizard/node/{node.NodeID}"
            };

            var service = new SolarWinds.Orion.ApiPoller.Web.OrionApiPollerNodesService();
           
            var apiPollers = service
                .GetApiPollers(node.NodeID)
                .OrderBy(apiPoller => apiPoller.Name);

            foreach (var apiPoller in apiPollers)
                yield return new ManagementTaskItem
                {
                    Group = ApiPollerGroupLabel,
                    Section = nodeSectionName,
                    LinkInnerHtml = HttpUtility.HtmlEncode(apiPoller.Name),
                    ClientID = "editPollerLink",
                    LinkUrl = $"/apps/apipoller/builder/node/{node.NodeID};pollerId={apiPoller.Id}"
                };
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private bool TryConvertNetObjectToNode(NetObject netObject, out Node node)
    {
        node = netObject as Node;

        if (node != null) return true;

        var netObjectId = netObject.NetObjectID;
        if (NetObjectFactory.ConvertNetObject(ref netObjectId, new[] {"N"}))
            if (!string.IsNullOrEmpty(netObjectId))
                node = NetObjectFactory.Create(netObjectId) as Node;

        return node != null;
    }
}
