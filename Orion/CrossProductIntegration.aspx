<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="CrossProductIntegration.aspx.cs" 
        Inherits="Orion_CrossProductIntegration" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_LK0_1 %>" %>
<%@ Register TagPrefix="orion" TagName="WebIntegrationBanner" Src="~/Orion/Controls/WebIntegrationBanner.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <orion:Include runat="server" File="/IFrameManager.js" />
	<script type="text/javascript">
	    //<![CDATA[
	    $(function () {
	        var resize = function () {
	            $("#external").height($(window).height() - $("#external").offset().top - parseInt($("#content").css("padding-bottom"), 10));
	        };
	        $(window).resize(resize);
	        resize(); // set up initial size

	    });
	    $(document).ready(function () {
	        IFrameManager.InitParent('external');

	        setInterval(function () {
	            var iframe = document.getElementById('HiddenIFrame');
	            iframe.contentWindow.location.reload(true);
	        }, 5 * 60 * 1000);
	    });
    //]]>
	</script>
	<style type="text/css">
		
		.linkLabel {
		    color: #0000ff;
		    cursor: pointer;           
		}
         #mainContent{padding-left: 10px;}
	</style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="mainContent">
        <div id="Div1" runat="server" visible="<%# !CommonWebHelper.IsBreadcrumbsDisabled %>" class="sw-hdr-breadbox">
            <orion:DropDownMapPath ID="OrionSiteMapPath" runat="server"  Provider="AdminSitemapProvider" OnInit="OrionSiteMapPath_OnInit">
                <RootNodeTemplate>
                    <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><u> <%# DefaultSanitizer.SanitizeHtml(Eval("title")) %></u> </a>
                </RootNodeTemplate>
                <CurrentNodeTemplate>
	                <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><%# DefaultSanitizer.SanitizeHtml(Eval("title")) %> </a>
	            </CurrentNodeTemplate>
            </orion:DropDownMapPath>
	    </div>
               
         <% if (Profile.AllowAdmin)
            { %>
            <orion:WebIntegrationBanner runat="server" id="customerportalHint"  Header=<%# DefaultSanitizer.SanitizeHtml(Caption) %> />  
         <% }
            else
            { %>
                <h1><%= DefaultSanitizer.SanitizeHtml(this.Caption) %></h1>

         <% } %>
      
	    <iframe id="external" src="<%= DefaultSanitizer.SanitizeHtml(this.URL) %>" width="100%" frameborder="0"></iframe>
        <iframe id="HiddenIFrame" width="0" height="0" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" src="/Orion/Admin/Details/DatabaseDetails.aspx" style="visibility: hidden; position: absolute; top: -20000px; left: -20000px;"></iframe>
   </div>
</asp:Content>
