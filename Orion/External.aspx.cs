using System;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using System.Security;
using SolarWinds.Logging;
using System.Web;
using SolarWinds.Orion.Web.HtmlSanitizer.Helpers;

public partial class Orion_External : System.Web.UI.Page
{
	protected ExternalWebsite site;
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();


	protected void Page_Load(object sender, EventArgs e)
	{
		int siteId;
		if (int.TryParse(Request["Site"], out siteId))
		{
            using (var proxy = _blProxyCreator.Create(delegate(Exception ex) { throw ex; }))
			{
				site = proxy.GetExternalWebsite(siteId);
				if (site == null)
					throw new ArgumentOutOfRangeException(string.Format("External Website {0} not found.", siteId));
				if (string.IsNullOrEmpty(site.FullTitle))
					Page.Title = HttpUtility.HtmlEncode(site.ShortTitle);
				else
					Page.Title = HttpUtility.HtmlEncode(site.FullTitle);
			}
        }
        else if (!string.IsNullOrEmpty(Request["Title"]) && !string.IsNullOrEmpty(Request["URL"]))
        {
            // Url + Title request, make sure the link is present in MenuItems table, if not throw an exception, cause the target resource may not be authorized for displaying
            
            var relativeLink = Server.UrlDecode(  Request.Path + '?' + Request.QueryString );

            using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
            {
                if (proxy.MenuItemExists(relativeLink))
                {
                    site = new ExternalWebsite() { FullTitle = string.Empty, ShortTitle = HttpUtility.HtmlEncode(Request["Title"]), URL = SanitizedURL(Request["URL"]) };
                    Page.Title = site.ShortTitle;

                } else
                {
                    throw new SecurityException("All external resources must be defined as links in Menu Bar or as External Websites.");
                }
            } 
        }
		else
			throw new ArgumentException("The Site parameter must be a valid integer or you must specify a Title and URL.");
	}

    protected static string SanitizedURL(string str)
    {
                return WebSecurityHelper.SanitizeHtml(str);
    }
}
