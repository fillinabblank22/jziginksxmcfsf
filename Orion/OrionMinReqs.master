<%@ Master Language="C#" AutoEventWireup="true" Inherits="SolarWinds.Orion.Web.UI.OrionMinReqsMaster" %>

<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="AutomationTestingExtensions" Src="~/Orion/Controls/AutomationTestingExtensions.ascx" %>
<%@ Register TagPrefix="orion" TagName="GoogleTagManager" Src="~/Orion/Controls/GoogleTagManager.ascx" %>
<%@ Register TagPrefix="xui" TagName="Include" Src="~/Orion/Controls/XuiInclude.ascx" %>


<%--

  in-house non-markup changes go to SolarWinds.Orion.Web.UI.OrionMinReqsMaster  ... markup or 'in-field' changes, append in here
  doctype: Trigger strict mode for browser

--%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/C#" runat="server">
    private void AddIntegrationJsSupportCode()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<script type=\"text/javascript\">");

        sb.AppendLine(@"//<![CDATA[
            var SW = window.SW = window.SW || {};
            SW.Core = SW.Core || {}; 
            SW.Core.Info = SW.Core.Info || {};");

        sb.AppendFormat("SW.Core.Info.IsMobileView = {0};", IsMobileView.ToString().ToLower());
        sb.AppendLine();
        sb.AppendFormat("SW.Core.Info.ToolsetIntegration = {0};", (Profile != null && !IsLoginPage && !Profile.IsAnonymous) ? Profile.ToolsetIntegration.ToString().ToLower() : "false");
        sb.AppendLine();
        sb.AppendFormat("SW.Core.Info.AllowAdmin = {0};", (Profile != null && !IsLoginPage && !Profile.IsAnonymous && Profile.AllowAdmin).ToString().ToLower());
        sb.AppendLine();
        sb.AppendFormat("SW.Core.Info.OIPEnabled = {0};", IsOipEnabled().ToString().ToLower());
        sb.AppendLine();

        sb.AppendLine("//]]>");

        toolsetJavascript.Text = sb.ToString();
        toolsetJavascript.Text += "</script";
        toolsetJavascript.Text += ">" + Environment.NewLine;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        RedirectToLoginPageIfNeeded();

        var manager = ScriptManager.GetCurrent(this.Page);
        if (manager != null)
        {
            manager.EnableScriptGlobalization = true;
        }

        // toggle visibility, include / exclude

        if (IsMobileView == false)
        {
            OrionInclude.CoreFile("OrionMobileLayout.css").Exclude();

            try
            {
                if (!IsLoginPage && !Profile.IsAnonymous && Profile.ToolsetIntegration)
                    OrionInclude.CoreFile("ToolsetIntegration.js", OrionInclude.Section.Bottom);
            }
            catch (Exception)
            {
            }
        }

        if (IsApolloEnabled)
        {
            this.Body.Attributes["class"] += " " + "sw-orion";

            if (!PageHelper.IsUrlBlacklisted(Request.RawUrl))
            {
                // Idle-Sensor checks user activity
                // Idle catches activity on every tree element, started from the root element,
                // so to make it working on a whole page, we should add it to body(root element)
                this.Body.Attributes.Add("sw-idle-sensor", "");
            }
        }

        AddIntegrationJsSupportCode();
    }
</script>
<html lang="<%= System.Threading.Thread.CurrentThread.CurrentUICulture.ToString() %>" xml:lang="<%= System.Threading.Thread.CurrentThread.CurrentUICulture.ToString() %>" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SolarWinds Orion</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="/Orion/images/favicon.ico" />
    <orion:GoogleTagManager ID="OIPgtm" runat="server" />
    <orion:IncludedFiles Kind="Css" Section="Top" runat="server" />
    <orion:IncludedFiles Kind="Css" Section="Middle" runat="server" />
    <orion:IncludedFiles Kind="Css" Section="Bottom" runat="server" />
    <orion:Include File="modernizr/modernizr-2.5.3.js" Section="Top" runat="server" />
    <asp:Literal ID="toolsetJavascript" EnableViewState="false" Mode="PassThrough" runat="server" />
    <orion:IncludedFiles Kind="JavaScript" Section="Top" runat="server" />
    <orion:IncludedFiles Kind="JavaScript" Section="Middle" runat="server" />
    <orion:Include Framework="jQuery" FrameworkVersion="1.7.1" Section="Top" runat="server" />
    <asp:ContentPlaceHolder ID="HeadContent" runat="server" />
    <orion:AutomationTestingExtensions ID="AutomationTestingExtensions1" runat="server" />
    <orion:Include Enabled="false" File="jquery/jquery-1.7.1-vsdoc.js" Section="Bottom" runat="server" />
    <orion:Include File="OrionMinReqs.css" Section="Top" runat="server" />
    <orion:Include File="OrionMinReqs.js" Section="Top" runat="server" SpecificOrder="-1" />
    <orion:Include File="OrionMobileLayout.css" runat="server" />
    <%-- dynamic enforcement: only mobile --%>
    <xui:Include runat="server" />
    <%--
      automatically added: toolsetintegration.js, solarwinds.css, events.css, theme-css, module-css
    --%>
    <!--[if IE]>
    <script type="text/javascript">
         var console = { log: function() {}, warn: function() {} };
    </script>
    <![endif]-->
</head>
<body runat="server" id="Body" ng-app="orion.app" ng-strict-di>
    <asp:ContentPlaceHolder ID="BodyContent" runat="server" />
    <orion:IncludedFiles Kind="JavaScript" Section="Bottom" runat="server" />
    <orion:HubbleResults runat="server" />

    <!-- Element attachment target used for xui components (i.e. modals, toast, tooltips) -->
    <span class="xui"><span id="xuiAttachTarget"></span></span>

    <script language="javascript" type="text/javascript">
        // http://support.microsoft.com/kb/2000262
        // Fix: UpdatePanel Async Postbacks Slow in Internet Explorer

        function disposeTree(sender, args) {
            var elements = args.get_panelsUpdating();
            for (var i = elements.length - 1; i >= 0; i--) {
                var element = elements[i];
                var allnodes = element.getElementsByTagName('*'),
                    length = allnodes.length;
                var nodes = new Array(length);
                for (var k = 0; k < length; k++) {
                    nodes[k] = allnodes[k];
                }
                for (var j = 0, l = nodes.length; j < l; j++) {
                    var node = nodes[j];
                    if (node.nodeType === 1) {
                        if (node.dispose && typeof (node.dispose) === "function") {
                            node.dispose();
                        }
                        else if (node.control && typeof (node.control.dispose) === "function") {
                            node.control.dispose();
                        }
                        var behaviors = node._behaviors;
                        if (behaviors) {
                            behaviors = Array.apply(null, behaviors);
                            for (var k = behaviors.length - 1; k >= 0; k--) {
                                behaviors[k].dispose();
                            }
                        }
                    }
                }
                element.innerHTML = "";
            }
        }
        if (typeof Sys == "object") {
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoading(disposeTree);
        }
    </script>
</body>
</html>
