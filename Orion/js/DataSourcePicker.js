var pickers = SW.Core.namespace("SW.Core.Pickers");
var emptyGuid = '00000000-0000-0000-0000-000000000000';
var addNewDataSourceGuid = 'c6ec9b54-0e68-4144-9f23-790a42a9150c';
pickers.DATASOURCE_TYPE_ENUM = {
    UNDEFINED: 0,
    DYNAMIC: 1,
    CUSTOM_SWQL: 2,
    CUSTOM_SQL: 3,
    ENTITIES: 4
};

pickers.DATASOURCE_SELECTION_MODE = {
    ANY: 'Any',
    MULTIPLE: 'Multiple',
    SINGLE: 'Single'
};

pickers.userTypedDataSorceName = "";
pickers.isUserTypedDataSorce = false;

(function ($) {
    $.fn.dataSources = function (method, swisDataSourceOnly) {
        var methods = {
            init: function (options) {
                $("#dataSourceName").on("keyup", function () {
                    SW.Core.Pickers.userTypedDataSorceName = $("#dataSourceName").val();
                    SW.Core.Pickers.isUserTypedDataSorce = true;
                });

                $.fn.dataSources.gridPanel = new Ext.Panel({
                    layout: 'column', cls: 'no-border data-source-picker', autoHeight: true, border: false, stateful: false, id: Ext.id(), renderTo: 'previewDataSources'
                });

                $.fn.dataSources.dynamicPicker = new SW.Core.Pickers.DynamicDataSourcePicker({
                    wrapperID: 'dynamicModeEditor'
                });

                $("#previewDataSourcesBtn").empty();
                $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_SO0_67; E=js}', { type: 'secondary', id: 'previewDataSourcesBtn' })).appendTo("#previewDataSourcesBtn").click(function () {
                    if ($("#isOrionDemoMode").length != 0) {
                        demoAction("Core_Reporting_PreviewQuery", this);
                        return false;
                    }
                    var radioBtns = $('[name=queryType]');
                    var type = '0';
                    for (var i = 0; i < radioBtns.length; i++) {
                        if (radioBtns[i].checked) {
                            type = radioBtns[i].value;
                        }
                    }
                    var name = $('#dataSourceName').val();
                    var commandText = $('#customQueryText').val();
                    var isEditMode = $.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig != undefined;
                    var guid = (isEditMode) ? $.fn.dataSources.editDataSourceConfig.RefId : dataSourceHelpers.guidGenerator();
                    dataSourceHelpers.previewQueryResults({ RefId: guid, Name: name, Type: type, CommandText: commandText, MasterEntity: $.fn.dataSources.settings.masterEntity, Filter: null, EntityUri: [], NetObjectId: $('#netObjectId').val() });
                });

                $('#dataSourceType').change(function () {
                    $('#entitiesModeEditor').css('display', 'none');
                    $('#dynamicModeEditor').css('display', 'none');
                    $('#customModeEditor').css('display', 'none');
                    $('#errorContainer').hide();
                    $('#selectionMethodDescription').text('');
                    switch (this.value) {
                        case "Dynamic":
                            {
                                $('#dynamicModeEditor').css('display', 'block');
                                $.fn.dataSources.dynamicPicker.setFilter(null);
                                $.fn.dataSources.dynamicPicker.render();
                                $('#selectionMethodDescription').html(String.format('@{R=Core.Strings;K=WEBJS_AB0_1;E=js}', String.format("<a target='blank' style='color: #336699; text-decoration: underline;' href='{0}'>", SW.Core.KnowledgebaseHelper.getKBUrl(4754, '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}')), "</a>"));
                            }
                            break;
                        case "Entities":
                            {
                                $('#entitiesModeEditor').css('display', 'block');
                                $('#selectionMethodDescription').text('@{R=Core.Strings;K=WEBJS_AB0_2;E=js}');
                            }
                            break;
                        default:
                            if ($("#isOrionDemoMode").length != 0) {
                                demoAction("Core_Reporting_SWQL_SQLDataSource", this, "demoModeWarning");
                            }
                            $('#customModeEditor').css('display', 'block');

                            break;
                    }
                    $("#dataSourceName").val(pickers.userTypedDataSorceName);
                });

                $('input[name="selectObjects"]').change(function () {
                    if ($('input[name="selectObjects"]:checked').val() == '1') {
                        $('#newSelection').css('display', 'none');
                        $('#ddDataSources').removeAttr('disabled');
                    } else {
                        $('#newSelection').css('display', 'block');
                        $('#ddDataSources').attr('disabled', 'disabled').val('');
                        $.fn.dataSources.dynamicPicker.render();
                    }
                });

                $.fn.dataSources.dataSourcesSessionIdentifier = $('[id*=dataSourcesSessionIdentifier]')[0].value;

                $.fn.dataSources.settings = $.extend({}, $.fn.dataSources.defaults, options);

                dataSourceHelpers.initDataSourceComboStores();

                $.fn.dataSources.alowAdmin = $('#allowAdmin').val().toLowerCase() == 'true';

                $('input[name=queryType]').change(function () {
                    if ($(this).val() == '3' && !$.fn.dataSources.alowAdmin) {
                        $('#customQueryText').attr("disabled", "disabled");
                        $('#previewDataSourcesBtn').css('display', 'none');
                        if ($.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig != undefined && $.fn.dataSources.editDataSourceConfig.Type.toString() == '3') {
                            $('#customQueryText').val($.fn.dataSources.editDataSourceConfig.CommandText);
                        }
                    }
                    else {
                        $('#previewDataSourcesBtn').css('display', 'block');
                        $('#customQueryText').removeAttr("disabled");
                    }

                });
                if ($.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig.Type == 1) {
                    var isAnyFilter = $.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig.Filter != null;
                    $.fn.dataSources.dynamicPicker.setShowNeb(isAnyFilter);
                    $.fn.dataSources.dynamicPicker.setDataSource($.fn.dataSources.editDataSourceConfig);
                }
                return this.each(function () {
                    var $element = $(this),
                        element = this;
                });
            },
            InitDataSources: function (dataSourceRefId, supportedInterfaces, resFile) {
                var currentDataSourceListStore = dataSourceHelpers.getStoreBySupportedInterfaces(supportedInterfaces);
                $.fn.dataSources.dataSourceList = new Ext.form.ComboBox({
                    id: $.fn.dataSources.settings.controlId + '_data_sources', triggerAction: 'all', lazyRender: false, width: 150, mode: 'local', editable: false, emptyText: '@{R=Core.Strings;K=WEBJS_SO0_70; E=js}', allowBlank: false,
                    store: currentDataSourceListStore,
                    valueField: 'refId', displayField: 'displayText',
                    tpl: '<tpl for=".">'
                        + '<div class="x-combo-list-item">'
                        + '{displayText:htmlEncode}'
                        + '</div>'
                        + '</tpl>',
                    listeners: {
                        'select': function (record, index) {
                            $.fn.dataSources.frameId = this.id.replace("_data_sources", "").replace("dataSource_", "");
                            $.fn.dataSources.settings.masterEntity = supportedInterfaces[0].MasterEntity;
                            if (index.data.refId == emptyGuid) {
                                dataSourceHelpers.clearSourceSelections();
                                dataSourceHelpers.updateSelectDataSourceDialog(false, supportedInterfaces);
                                SW.Orion.NetObjectPicker.SetUpEntities(supportedInterfaces, supportedInterfaces[0].MasterEntity, []);
                                if ($.fn.dataSources.openEditDialogDelegate)
                                    $.fn.dataSources.openEditDialogDelegate(resFile, $.fn.dataSources.frameId, { RefId: addNewDataSourceGuid, Name: '', Type: 4, CommandText: '', MasterEntity: "Orion.Nodes", Filter: null, EntityUri: [] }, function () {
                                        if ($.fn.dataSources.syncDataSourceIdDelegate)
                                            $.fn.dataSources.syncDataSourceIdDelegate($.fn.dataSources.frameId, $.fn.dataSources.dataSourceData.RefId);
                                        $.fn.dataSources.updateSelectedDataSource($.fn.dataSources.frameId);
                                    });
                            }
                            else {
                                if (isNaN(index.data.refId)) {
                                    for (var s = 0; s < $.fn.dataSources.dataSourceDataArray.length; s++) {
                                        if ($.fn.dataSources.dataSourceDataArray[s].RefId == index.data.refId) {
                                            $.fn.dataSources.dataSourceData = $.fn.dataSources.dataSourceDataArray[s];
                                            break;
                                        }
                                    }
                                }
                                else {
                                    var guid = dataSourceHelpers.guidGenerator();
                                    $.fn.dataSources.dataSourceData = { RefId: guid, Name: '', Type: '0', CommandText: '', MasterEntity: $.fn.dataSources.settings.masterEntity, Filter: null, EntityUri: [] };
                                }
                                $.fn.dataSources.dataSourceDataDictionary[$.fn.dataSources.frameId] = $.fn.dataSources.dataSourceData;
                                if ($.fn.dataSources.syncDataSourceIdDelegate) $.fn.dataSources.syncDataSourceIdDelegate($.fn.dataSources.frameId, $.fn.dataSources.dataSourceData.RefId);
                                dataSourceHelpers.syncConfigsData();
                            }
                        }
                    }
                });
                var panel = new Ext.Panel({
                    layout: 'table', cls: 'no-border data-source-picker', autoHeight: true, border: false, stateful: false, id: Ext.id(), renderTo: $.fn.dataSources.settings.controlId,
                    items: [
                        $.fn.dataSources.dataSourceList,
                        {
                            html: String.format('<a class="layout-edit-link" href="javascript:$.fn.dataSources.editDataSource(\'{0}\', \'{1}\');">@{R=Core.Strings;K=CommonButtonType_Edit; E=js}</a>',
                                $.fn.dataSources.settings.controlId.replace('dataSource_', ''), resFile), border: false
                        }
                    ]
                });

                $.fn.dataSources.selectionModesDictionary[$.fn.dataSources.settings.controlId] = supportedInterfaces;
                $.fn.dataSources.sources.push({ ID: $.fn.dataSources.settings.controlId, FrameList: $.fn.dataSources.dataSourceList });
                dataSourceHelpers.initDataSourceValues($.fn.dataSources.dataSourceList.value, $.fn.dataSources.dataSourceList.lastSelectionText, $.fn.dataSources.settings.controlId.replace("dataSource_", ""), dataSourceRefId);
            },
            SetDataSource: function (data) {
                if (!data.RefId) return;
                $.fn.dataSources.dataSourceData = data;
                // check if this data source already exists within the list
                if (!dataSourceHelpers.dataSourceExists(data.RefId)) {
                    var dataSource = $.fn.dataSources.dataSourceList.getStore();
                    dataSource.insert(dataSource.data.items.length - 1, new Ext.data.Record({ refId: data.RefId, displayText: data.Name }));
                }
                $.fn.dataSources.dataSourceList.setValue(data.RefId);
            },
            GetDataSource: function (id) {
                return $.fn.dataSources.dataSourceDataDictionary[id];
            }
        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' + method + '" does not exist in timePeriods plugin!');
            return null;
        }
    };

    $.fn.dataSources.autoCompleteSourceName = function (suggestedName) {
        if ($.fn.dataSources.editDataSourceConfig == null && suggestedName != null && !pickers.isUserTypedDataSorce) {
            $("#dataSourceName").val(suggestedName);
        } else {
            $("#dataSourceName").val(SW.Core.Pickers.userTypedDataSorceName);
        }
    };

    $.fn.dataSources.setUpForm = function (refId, dataSource, supportedInterfaces) {
        $.fn.dataSources.gridPanel.removeAll();
        $.fn.dataSources.gridPanel.doLayout();

        dataSourceHelpers.clearSourceSelections();

        $.fn.dataSources.frameId = refId;
        $.fn.dataSources.settings.masterEntity = dataSource.MasterEntity;

        var isDemo = $("#isOrionDemoMode").length != 0;
        if (isDemo) {
            $('input[name=queryType][value=2]').attr('disabled', 'disabled');
            $('input[name=queryType][value=3]').attr('disabled', 'disabled');
            $('#customQueryText').attr("disabled", "disabled");
        }
        else {
            if (dataSource.RefId == emptyGuid && !$.fn.dataSources.alowAdmin) {
                $.fn.dataSources.editDataSourceConfig == null;
            }
            if (!$.fn.dataSources.alowAdmin && dataSource.Type != 3) {
                $('input[name=queryType][value=3]').attr('disabled', 'disabled');
            }
            else {
                $('input[name=queryType][value=3]').removeAttr('disabled');
            }
        }

        var isEditMode = $.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig != undefined;
        if (isEditMode) {
            $.fn.dataSources.dynamicPicker.updateSourceNameDelegate = null;
            SW.Orion.NetObjectPicker.updateSourceNameDelegate = null;
        } else {
            pickers.isUserTypedDataSorce = false;
            SW.Orion.NetObjectPicker.updateSourceNameDelegate = $.fn.dataSources.autoCompleteSourceName;
            $.fn.dataSources.dynamicPicker.updateSourceNameDelegate = $.fn.dataSources.autoCompleteSourceName;
        }

        dataSourceHelpers.updateSelectDataSourceDialog(isEditMode, supportedInterfaces, dataSource.RefId == addNewDataSourceGuid);
        if (dataSource.Name == '') {
            dataSource.Name = dataSourceHelpers.initDataSourceName();
            if ($('#dataSourceType option[value="Dynamic"]').length > 0) {
                dataSource.Type = 1;
            }
        }
        $('#entitiesModeEditor').css('display', 'none');
        $('#dynamicModeEditor').css('display', 'none');
        $('#customModeEditor').css('display', 'none');
        if (!isDemo)
            $('#customQueryText').removeAttr("disabled");
        $('#previewDataSourcesBtn').css('display', 'block');

        SW.Core.Pickers.userTypedDataSorceName = isEditMode ? $.fn.dataSources.editDataSourceConfig.Name : String.format('@{R=Core.Strings;K=WEBJS_TM0_137;E=js}', $.fn.dataSources.dataSourceDataArray.length + 1);
        $("#dataSourceName").val(pickers.userTypedDataSorceName);

        switch (parseInt(dataSource.Type)) {
            case 1:
                $('#dataSourceType').val('Dynamic');
                $('#dynamicModeEditor').css('display', 'block');
                SW.Orion.NetObjectPicker.SetUpEntities(supportedInterfaces, dataSource.MasterEntity, []);
                var isAnyFilter = $.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig.Filter != null;
                $.fn.dataSources.dynamicPicker.setShowNeb(isAnyFilter);
                $.fn.dataSources.dynamicPicker.render();
                $.fn.dataSources.dynamicPicker.setDataSource($.fn.dataSources.editDataSourceConfig);

                $('#selectionMethodDescription').html(String.format('@{R=Core.Strings;K=WEBJS_AB0_1;E=js}', String.format("<a target='blank' style='color: #336699; text-decoration: underline;' href='{0}'>", SW.Core.KnowledgebaseHelper.getKBUrl(4754, '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}')), "</a>"));
                break;
            case 2:
            case 3:
                $('#dataSourceType').val('Custom');
                if ($("#isOrionDemoMode").length != 0) {
                    demoAction("Core_Reporting_SWQL_SQLDataSource", this, "demoModeWarning");
                }
                $('#customModeEditor').css('display', 'block');
                var queryTypes = $('[name=queryType]');
                for (var i = 0; i < queryTypes.length; i++) {
                    if (queryTypes[i].value == dataSource.Type.toString()) {
                        queryTypes[i].checked = true;

                        if (queryTypes[i].value == '3' && !$.fn.dataSources.alowAdmin) {
                            $('#customQueryText').attr("disabled", "disabled");
                            if (!isDemo)
                                $('#previewDataSourcesBtn').css('display', 'none');
                        } else if (!$.fn.dataSources.alowAdmin) {
                            $('input[name=queryType][value=3]').attr('disabled', 'disabled');
                        }
                    }
                }
                $('#customQueryText').val(dataSource.CommandText);
                $('#selectionNameErrorMessage').css('display', 'none');
                $('#errorContainer').hide();
                SW.Orion.NetObjectPicker.SetUpEntities(supportedInterfaces, dataSource.MasterEntity, []);
                break;
            case 4:
                $('#dataSourceType').val('Entities');
                $('#entitiesModeEditor').css('display', 'block');

                var selMode = 'Single';
                // check if this resource support non single selection
                for (var l = 0; l < supportedInterfaces.length; l++) {
                    if (supportedInterfaces[l].MasterEntity == dataSource.MasterEntity && supportedInterfaces[l].SelectionMode != 'Single') {
                        selMode = supportedInterfaces[l].SelectionMode;
                        break;
                    }
                }

                if (dataSource.EntityUri.length == 1 &&
                    selMode != 'Single' &&
                    dataSourceHelpers.isDataSourceAssignedToSingleResource(dataSource.RefId, dataSource.MasterEntity)) {
                    $('#singleSelectWarning').css('display', 'block');
                    SW.Orion.NetObjectPicker.SetUpEntities(supportedInterfaces, dataSource.MasterEntity, dataSource.EntityUri, 'Single');
                } else {
                    $('#singleSelectWarning').css('display', 'none');
                    SW.Orion.NetObjectPicker.SetUpEntities(supportedInterfaces, dataSource.MasterEntity, dataSource.EntityUri);
                }

                $('#selectionMethodDescription').text('@{R=Core.Strings;K=WEBJS_AB0_2;E=js}');
                break;
        }
        $.fn.dataSources.dynamicPicker.isFirstLoad = false;

        var helpfullTextForSpecificSelection = $('#helpfullTextForSpecificSelection');
        var selectionMethodCtrl = $('#selectionMethodCtrl');

        SW.Orion.NetObjectPicker.SetupHintForSpecificObjectEntity = function (entityCount, entityDisplayName, entityDisplayNamePlural, selectionMode) {
            var hintText = '';
            // creating hint text for specific selection considering selectionMode and supportedInterfaces
            if (entityCount == 1) {
                if (selectionMode.toLowerCase() == 'single') {
                    hintText = String.format('@{R=Core.Strings;K=WEBJS_RB0_6;E=js}', entityDisplayName);
                } else {
                    hintText = String.format('@{R=Core.Strings;K=WEBJS_RB0_7;E=js}', entityDisplayNamePlural);
                }
            } else {
                hintText = '@{R=Core.Strings;K=WEBJS_RB0_8;E=js}';
            }
            $('#helpfullTextForSpecificSelection').text(hintText);
        };

        if ($('#dataSourceType option').length == 1) {
            helpfullTextForSpecificSelection.show();
            selectionMethodCtrl.hide();
        } else {
            selectionMethodCtrl.show();
            helpfullTextForSpecificSelection.hide();
        }
    };

    $.fn.dataSources.validateDataSource = function () {
        return dataSourceHelpers.validateInputs();
    };

    $.fn.dataSources.updateSelectedDataSource = function (id) {
        var currentFrameList = dataSourceHelpers.getCurrentSourceList(id);
        if (currentFrameList)
            currentFrameList.setValue($.fn.dataSources.dataSourceData.RefId);
    };

    $.fn.dataSources.formatDateTime = function (value, meta, record) {
        if (Ext.isEmpty(value) || value.length === 0)
            return '';

        var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        var dateVal = new Date(value);
        if (dateVal && dateVal != 'Invalid Date') {
            return dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        return '';
    };

    $.fn.dataSources.formatBool = function (value, meta, record) {
        if (!value || value.length === 0 || value.toString() == '0' || value.toString() == 'false')
            return '@{R=Core.Strings;K=WEBJS_IB0_13;E=js}';
        return '@{R=Core.Strings;K=WEBJS_IB0_12;E=js}';
    };

    $.fn.dataSources.getSWQLquery = function () {
        return dataSourceHelpers.getQuery($.fn.dataSources.dynamicPicker.getDataSource());
    };

    $.fn.dataSources.validate = function () {
        var isEditMode = $.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig != undefined;

        var filter;
        var dynamicSelectionType;
        var radioBtns = $('[name=queryType]');
        var type = '0';
        if ($('#dataSourceType').val() == "Custom") {
            for (var i = 0; i < radioBtns.length; i++) {
                if (radioBtns[i].checked) {
                    type = radioBtns[i].value;
                }
            }
        } else {
            type = ($('#dataSourceType').val() == "Dynamic") ? '1' : '4';
        }

        var selectedEntities = [];
        if (type == pickers.DATASOURCE_TYPE_ENUM.ENTITIES) {
            selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntities();
            $.fn.dataSources.settings.masterEntity = SW.Orion.NetObjectPicker.GetItemType();
            if (!selectedEntities || selectedEntities.length == 0)
                return false;
        } else if (type == pickers.DATASOURCE_TYPE_ENUM.DYNAMIC) {
            var dt = $.fn.dataSources.dynamicPicker.getDataSource();
            $.fn.dataSources.settings.masterEntity = dt.MasterEntity;
            filter = dt.Filter;
            dynamicSelectionType = dt.DynamicSelectionType;
        }

        var name = $('#dataSourceName').val();
        var commandText = $('#customQueryText').val();
        var guid = (isEditMode) ? $.fn.dataSources.editDataSourceConfig.RefId : dataSourceHelpers.guidGenerator();

        $.fn.dataSources.dataSourceData = {
            RefId: guid,
            Name: name,
            Type: type,
            CommandText: commandText,
            MasterEntity: $.fn.dataSources.settings.masterEntity,
            Filter: filter,
            EntityUri: selectedEntities,
            DynamicSelectionType: dynamicSelectionType,
            NetObjectId: $('#netObjectId').val()
        };

        return dataSourceHelpers.validateInputs();
    };

    $.fn.dataSources.validateAndSyncData = function () {
        var isEditMode = $.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig != undefined;

        var ddDataSourcesSelected = $('#ddDataSources').val();
        if (!isEditMode && $('#selectDataSrcHeader').css('display') != 'none' && $('#createdDataSources').is(':checked')) {
            var currentDataSourceList = dataSourceHelpers.getCurrentSourceList($.fn.dataSources.frameId);
            if (currentDataSourceList)
                currentDataSourceList.setValue(ddDataSourcesSelected);

            for (var s = 0; s < $.fn.dataSources.dataSourceDataArray.length; s++) {
                if ($.fn.dataSources.dataSourceDataArray[s].RefId == ddDataSourcesSelected) {
                    $.fn.dataSources.dataSourceData = $.fn.dataSources.dataSourceDataArray[s];
                    break;
                }
            }
            $.fn.dataSources.dataSourceDataDictionary[$.fn.dataSources.frameId] = $.fn.dataSources.dataSourceData;
            if ($.fn.dataSources.syncDataSourceIdDelegate)
                $.fn.dataSources.syncDataSourceIdDelegate($.fn.dataSources.frameId, $.fn.dataSources.dataSourceData.RefId);
            return true;
        }

        var radioBtns = $('[name=queryType]');
        var type = '0';
        if ($('#dataSourceType').val() == "Custom") {
            for (var i = 0; i < radioBtns.length; i++) {
                if (radioBtns[i].checked) {
                    type = radioBtns[i].value;
                }
            }
        }
        else {
            type = ($('#dataSourceType').val() == "Dynamic") ? '1' : '4';
        }

        var selectedEntities = [];
        if (type == pickers.DATASOURCE_TYPE_ENUM.ENTITIES) {
            selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntities();
            $.fn.dataSources.settings.masterEntity = SW.Orion.NetObjectPicker.GetItemType();
            if (!selectedEntities || selectedEntities.length == 0)
                return false;
        }

        else if (type == pickers.DATASOURCE_TYPE_ENUM.DYNAMIC) {
            var dt = $.fn.dataSources.dynamicPicker.getDataSource();
            $.fn.dataSources.settings.masterEntity = dt.MasterEntity;
            var filter = dt.Filter;
            var dynamicSelectionType = dt.DynamicSelectionType;
        }

        var name = $('#dataSourceName').val();
        var commandText = $('#customQueryText').val();
        var guid = (isEditMode) ? $.fn.dataSources.editDataSourceConfig.RefId : dataSourceHelpers.guidGenerator();

        $.fn.dataSources.dataSourceData = {
            RefId: guid,
            Name: name,
            Type: type,
            CommandText: commandText,
            MasterEntity: $.fn.dataSources.settings.masterEntity,
            Filter: filter,
            EntityUri: selectedEntities,
            DynamicSelectionType: dynamicSelectionType,
            NetObjectId: $('#netObjectId').val()
        };

        if (!dataSourceHelpers.validateInputs()) return false;
        if (!isEditMode) {
            $.fn.dataSources.dataSourceDataArray.push($.fn.dataSources.dataSourceData);
            if ($.fn.dataSources.syncDataSourceIdDelegate) $.fn.dataSources.syncDataSourceIdDelegate($.fn.dataSources.frameId, $.fn.dataSources.dataSourceData.RefId);

            dataSourceHelpers.addDataSourceToStores(guid, name, $.fn.dataSources.settings.masterEntity, dataSourceHelpers.getSelectionMode(type, selectedEntities.length));
        }
        else {
            for (var k = 0; k < $.fn.dataSources.dataSourceDataArray.length; k++) {
                if ($.fn.dataSources.dataSourceDataArray[k].RefId == guid) {
                    $.fn.dataSources.dataSourceDataArray[k] = $.fn.dataSources.dataSourceData;
                    break;
                }
            }
            dataSourceHelpers.updateDataSourceToStores(guid, name, $.fn.dataSources.settings.masterEntity, dataSourceHelpers.getSelectionMode(type, selectedEntities.length));
            dataSourceHelpers.updateDataSourceComboValues(guid);
            $.fn.dataSources.editDataSourceConfig = null;
        }
        $.fn.dataSources.dataSourceDataDictionary[$.fn.dataSources.frameId] = $.fn.dataSources.dataSourceData;
        $.fn.dataSources.updateSelectedDataSource($.fn.dataSources.frameId);

        dataSourceHelpers.syncConfigsData();
        return true;
    };

    $.fn.dataSources.getSelectedDataSourceId = function () {
        return $.fn.dataSources.dataSourceData.RefId;
    };

    $.fn.dataSources.editDataSource = function (controlId, resFile) {
        $.fn.dataSources.frameId = controlId;
        var currentDataSourceList = dataSourceHelpers.getCurrentSourceList($.fn.dataSources.frameId);
        if (currentDataSourceList.getValue() == '' || currentDataSourceList.getValue() == emptyGuid) {
            alert('@{R=Core.Strings;K=WEBJS_TM0_134;E=js}');
            return;
        }

        $.fn.dataSources.editDataSourceConfig = $.fn.dataSources.dataSourceDataDictionary[controlId];
        if ($.fn.dataSources.editDataSourceConfig != null && $.fn.dataSources.editDataSourceConfig != undefined) {
            for (var j = 0; j < $.fn.dataSources.dataSourceDataArray.length; j++) {
                if ($.fn.dataSources.dataSourceDataArray[j].RefId == $.fn.dataSources.editDataSourceConfig.RefId) {
                    $.fn.dataSources.editDataSourceConfig = $.fn.dataSources.dataSourceDataArray[j];
                    break;
                }
            }
            if ($.fn.dataSources.openEditDialogDelegate)
                $.fn.dataSources.openEditDialogDelegate(resFile, $.fn.dataSources.frameId, $.fn.dataSources.editDataSourceConfig, function () {
                    if ($.fn.dataSources.syncDataSourceIdDelegate)
                        $.fn.dataSources.syncDataSourceIdDelegate($.fn.dataSources.frameId, $.fn.dataSources.dataSourceData.RefId);
                    $.fn.dataSources.updateSelectedDataSource();
                });
        }
    };

    $.fn.dataSources.gridPanel = null;
    $.fn.dataSources.editDataSourceConfig = null;
    $.fn.dataSources.dataSources = {};
    $.fn.dataSources.dataSourcesKeys = [];
    $.fn.dataSources.dataSourcesSessionIdentifier = null;
    $.fn.dataSources.settings = { controlId: 'body', masterEntity: '' };
    $.fn.dataSources.dataSourceDataArray = new Array();
    $.fn.dataSources.sources = new Array();;
    $.fn.dataSources.syncDataSourceIdDelegate = null;
    $.fn.dataSources.dataSourceDataDictionary = {};
    $.fn.dataSources.dataSourceData = null;
    $.fn.dataSources.dataSourceList = null;
    $.fn.dataSources.frameId = null;
    $.fn.dataSources.defaultFrameIds = new Array();
    $.fn.dataSources.selectionModesDictionary = {};
    $.fn.dataSources.openEditDialogDelegate = null;
    $.fn.dataSources.alowAdmin = false;
    $.fn.dataSources.clear = function () { $.fn.dataSources.editDataSourceConfig = null; };

    var dataSourceHelpers = {
        initDataSourceValues: function (value, text, id, sourceRefId) {
            if (sourceRefId != emptyGuid) {
                for (var i = 0; i < $.fn.dataSources.dataSourceDataArray.length; i++) {
                    if ($.fn.dataSources.dataSourceDataArray[i].RefId == sourceRefId) {
                        $.fn.dataSources.dataSourceDataDictionary[id] = $.fn.dataSources.dataSourceDataArray[i];
                        $.fn.dataSources('SetDataSource', $.fn.dataSources.dataSourceDataArray[i]);
                        break;
                    }
                }
            }
        },
        initDataSourceComboStores: function () {
            SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'GetSessionData', { name: $.fn.dataSources.dataSourcesSessionIdentifier },
                function (res) {
                    if (res != null) {
                        var data = JSON.parse(res);
                        if (data != null) {
                            for (var i = 0; i < data.length; i++) {
                                dataSourceHelpers.addDataSourceToStores(data[i].RefId, data[i].Name, data[i].MasterEntity, dataSourceHelpers.getSelectionMode(data[i].Type, data[i].EntityUri.length));
                                $.fn.dataSources.dataSourceDataArray.push(data[i]);
                            }
                        }
                    }
                }
            );
        },
        getSelectionMode: function (dataSourceType, entitiesCount) {
            if (dataSourceType != pickers.DATASOURCE_TYPE_ENUM.ENTITIES)
                return pickers.DATASOURCE_SELECTION_MODE.ANY;
            if (entitiesCount > 1)
                return pickers.DATASOURCE_SELECTION_MODE.MULTIPLE;
            return pickers.DATASOURCE_SELECTION_MODE.SINGLE;
        },
        initDataSourceName: function () {
            return String.format('@{R=Core.Strings;K=WEBJS_TM0_137;E=js}', $.fn.dataSources.dataSourceDataArray.length + 1);
        },
        isDataSourceNameUnique: function (nameToCheck, currentName) {
            for (var i = 0; i < $.fn.dataSources.dataSourceDataArray.length; i++) {
                if ($.fn.dataSources.dataSourceDataArray[i].Name == currentName)
                    continue;
                if ($.fn.dataSources.dataSourceDataArray[i].Name == nameToCheck)
                    return false;
            }

            return true;
        },
        addDataSourceToStores: function (refId, name, masterEntity, selectionMode) {
            var recordToAdd = new Ext.data.Record({ refId: refId, displayText: name });

            // always add to "Any" datasource
            dataSourceHelpers.defineStoreInDictionary(pickers.DATASOURCE_SELECTION_MODE.ANY);
            $.fn.dataSources.dataSources[pickers.DATASOURCE_SELECTION_MODE.ANY].add([recordToAdd]);

            // add into "Any" datasource only
            if (selectionMode == pickers.DATASOURCE_SELECTION_MODE.ANY) return;

            var multiKey = this.getStoreKeyBySupportedInterfaces([{ MasterEntity: masterEntity, SelectionMode: pickers.DATASOURCE_SELECTION_MODE.MULTIPLE }]);
            if (selectionMode == pickers.DATASOURCE_SELECTION_MODE.SINGLE) {
                var singleKey = this.getStoreKeyBySupportedInterfaces([{ MasterEntity: masterEntity, SelectionMode: pickers.DATASOURCE_SELECTION_MODE.SINGLE }]);
                dataSourceHelpers.defineStoreInDictionary(singleKey);
                for (var i = 0; i < $.fn.dataSources.dataSourcesKeys.length; i++) {
                    if ($.fn.dataSources.dataSourcesKeys[i].indexOf(singleKey) > -1 && $.fn.dataSources.dataSourcesKeys[i].indexOf(multiKey) == -1) {
                        $.fn.dataSources.dataSources[$.fn.dataSources.dataSourcesKeys[i]].add([recordToAdd]);
                    }
                }
            }
            // always add to "Multiple" datasources
            dataSourceHelpers.defineStoreInDictionary(multiKey);
            for (var j = 0; j < $.fn.dataSources.dataSourcesKeys.length; j++) {
                if ($.fn.dataSources.dataSourcesKeys[j].indexOf(multiKey) > -1) {
                    $.fn.dataSources.dataSources[$.fn.dataSources.dataSourcesKeys[j]].add([recordToAdd]);
                }
            }
        },
        defineStoreInDictionary: function (key) {
            if ($.fn.dataSources.dataSources[key] == null) {
                $.fn.dataSources.dataSources[key] = new Ext.data.ArrayStore({ fields: ['refId', 'displayText'], data: [[emptyGuid, '@{R=Core.Strings;K=WEBJS_SO0_62; E=js}']] });
                $.fn.dataSources.dataSourcesKeys.push(key);
            }
        },
        updateDataSourceToStores: function (refId, name, masterEntity, selectionMode) {
            var recordToUpdate = new Ext.data.Record({ refId: refId, displayText: name });
            var singleKey = this.getStoreKeyBySupportedInterfaces([{ MasterEntity: masterEntity, SelectionMode: pickers.DATASOURCE_SELECTION_MODE.SINGLE }]);
            var multiKey = this.getStoreKeyBySupportedInterfaces([{ MasterEntity: masterEntity, SelectionMode: pickers.DATASOURCE_SELECTION_MODE.MULTIPLE }]);

            dataSourceHelpers.defineStoreInDictionary(singleKey);
            dataSourceHelpers.defineStoreInDictionary(multiKey);

            var isMulti = selectionMode == pickers.DATASOURCE_SELECTION_MODE.MULTIPLE;

            for (var i = 0; i < $.fn.dataSources.dataSourcesKeys.length; i++) {
                var index = -1;
                for (var j = $.fn.dataSources.dataSources[$.fn.dataSources.dataSourcesKeys[i]].getCount() - 1; j >= 0; j--) {
                    if ($.fn.dataSources.dataSources[$.fn.dataSources.dataSourcesKeys[i]].data.items[j].data.refId == refId) {
                        $.fn.dataSources.dataSources[$.fn.dataSources.dataSourcesKeys[i]].removeAt(j);
                        index = j;
                        break;
                    }
                }

                if (($.fn.dataSources.dataSourcesKeys[i] == pickers.DATASOURCE_SELECTION_MODE.ANY) ||
                    (selectionMode != pickers.DATASOURCE_SELECTION_MODE.ANY && (($.fn.dataSources.dataSourcesKeys[i].indexOf(singleKey) > -1 && !isMulti) || $.fn.dataSources.dataSourcesKeys[i].indexOf(multiKey) > -1))) {
                    if (index > -1) {
                        $.fn.dataSources.dataSources[$.fn.dataSources.dataSourcesKeys[i]].insert(index, recordToUpdate);
                    }
                    else {
                        $.fn.dataSources.dataSources[$.fn.dataSources.dataSourcesKeys[i]].add([recordToUpdate]);
                    }

                }
            }
        },
        getStoreKeyBySupportedInterfaces: function (supportedInterfaces) {
            var key = '';
            var sep = '';
            for (var i = 0; i < supportedInterfaces.length; i++) {
                if (supportedInterfaces[i].SelectionMode == pickers.DATASOURCE_SELECTION_MODE.ANY) return pickers.DATASOURCE_SELECTION_MODE.ANY;
                key += sep + supportedInterfaces[i].SelectionMode + '_' + supportedInterfaces[i].MasterEntity;
                sep = '$';
            }
            return key;
        },
        addRecordsToStore: function (key, recordsToAdd, keysToAdd) {
            for (var i = keysToAdd.length - 1; i >= 0; i--) {
                for (var j = $.fn.dataSources.dataSources[key].getCount() - 1; j >= 0; j--) {
                    if ($.fn.dataSources.dataSources[key].data.items[j].data.refId == keysToAdd[i]) {
                        keysToAdd.splice(i, 1);
                        break;
                    }
                }
            }
            for (var k = 0; k < keysToAdd.length; k++) {
                $.fn.dataSources.dataSources[key].add(recordsToAdd[keysToAdd[k]]);
            }
        },
        getStoreBySupportedInterfaces: function (supportedInterfaces) {
            var key = this.getStoreKeyBySupportedInterfaces(supportedInterfaces);
            dataSourceHelpers.defineStoreInDictionary(key);
            var recordsToAdd = {};
            var keysToAdd = [];
            for (var i = 0; i < supportedInterfaces.length; i++) {
                var subKey = this.getStoreKeyBySupportedInterfaces([{ MasterEntity: supportedInterfaces[i].MasterEntity, SelectionMode: supportedInterfaces[i].SelectionMode }]);
                dataSourceHelpers.defineStoreInDictionary(subKey);
                for (var j = 0; j < $.fn.dataSources.dataSources[subKey].getCount(); j++) {
                    var id = $.fn.dataSources.dataSources[subKey].data.items[j].data.refId;
                    if (!recordsToAdd[id]) {
                        recordsToAdd[id] = new Ext.data.Record({ refId: id, displayText: $.fn.dataSources.dataSources[subKey].data.items[j].data.displayText });
                        keysToAdd.push(id);
                    }
                }
            }
            this.addRecordsToStore(key, recordsToAdd, keysToAdd);
            return $.fn.dataSources.dataSources[key];
        },
        updateSelectDataSourceDialog: function (isEdit, supportedInterfaces, addNew) {
            $('#dataSourceType option[value="Dynamic"]').remove();
            $('#dataSourceType option[value="Custom"]').remove();
            for (var j = 0; j < supportedInterfaces.length; j++) {
                if (supportedInterfaces[j].SelectionMode == pickers.DATASOURCE_SELECTION_MODE.ANY) {
                    $('#dataSourceType').append(String.format('<option value="Dynamic">{0}</option>', '@{R=Core.Strings;K=WEBJS_YK0_5;E=js}'));
                    $('#dataSourceType').append(String.format('<option value="Custom">{0}</option>', swisDataSourceOnly ? '@{R=Core.Strings;K=Reports_CustomDataSourcePicker_JSOption_SWQL;E=js}' : '@{R=Core.Strings;K=WEBJS_TM0_135;E=js}'));
                    break;
                }
            }

            var length = $.fn.dataSources.dataSourceDataArray.length;
            if (!isEdit && length > 0) {
                var ddDataSrs = $('#ddDataSources');
                ddDataSrs.html(''); //cleaning ddl
                var isEmpty = true;
                var store = dataSourceHelpers.getStoreBySupportedInterfaces(supportedInterfaces);
                for (var k = 0; k < store.data.items.length; k++) {
                    if (store.data.items[k].data.refId != emptyGuid) {
                        ddDataSrs.append(String.format('<option value="{0}">{1}</option>',
                            store.data.items[k].data.refId,
                            Ext.util.Format.htmlEncode(store.data.items[k].data.displayText)));
                        isEmpty = false;
                    }
                }
                if (!isEmpty) {
                    $('#selectDataSrcHeader').css('display', 'block');
                    if (addNew === true) {
                        $('input[name="selectObjects"]:not(#createdDataSources)').attr('checked', true).change();
                    } else {
                        $('#createdDataSources').attr('checked', true).change();
                    }
                    return;
                }
            }

            $('#selectDataSrcHeader').css('display', 'none');
            $('#newSelection').css('display', 'block');
        },
        updateDataSourceComboValues: function (valueId) {
            //updating all selected values in comboboxes in case store modified
            for (var s = 0; s < $.fn.dataSources.sources.length; s++) {
                if ($.fn.dataSources.sources[s].FrameList.getValue() == valueId) {
                    var valueExists = false;
                    for (var i = 0; i < $.fn.dataSources.sources[s].FrameList.store.data.items.length; i++) {
                        if ($.fn.dataSources.sources[s].FrameList.store.data.items[i].data.refId == valueId) {
                            valueExists = true;
                            break;
                        }
                    }
                    if (valueExists) {
                        $.fn.dataSources.sources[s].FrameList.setValue(valueId);
                    } else {
                        $.fn.dataSources.sources[s].FrameList.setValue('');
                        if ($.fn.dataSources.syncDataSourceIdDelegate) $.fn.dataSources.syncDataSourceIdDelegate($.fn.dataSources.sources[s].ID.replace('dataSource_', ''), emptyGuid);
                    }
                }
            }
        },
        isDataSourceAssignedToSingleResource: function (valueId, masterEntity) {
            for (var s = 0; s < $.fn.dataSources.sources.length; s++) {
                if ($.fn.dataSources.sources[s].FrameList.getValue() == valueId) {
                    var supInt = $.fn.dataSources.selectionModesDictionary[$.fn.dataSources.sources[s].ID];
                    var supportMultiple = false;
                    for (var i = 0; i < supInt.length; i++) {
                        if (supInt[i].MasterEntity == masterEntity && supInt[i].SelectionMode != 'Single')
                            supportMultiple = true;
                    }

                    if (!supportMultiple) return true;
                }
            }
            return false;
        },
        clearSourceSelections: function () {
            $('#dataSourceName').val('');
            $('[name=queryType]')[0].checked = true;
            if (swisDataSourceOnly) {
                dataSourceHelpers.hideQueryTypeSelectionBox();
            }
            $('#customQueryText').val('');
            $('#selectionNameErrorMessage').css('display', 'none');
            $('#errorContainer').hide();
            SW.Orion.NetObjectPicker.SetSelectedEntities([]);
            $('#entitiesModeEditor').css('display', 'block');
            $('#dynamicModeEditor').css('display', 'none');
            $('#customModeEditor').css('display', 'none');
            $('#dataSourceType').val('Entities');
        },
        hideQueryTypeSelectionBox: function () {
            $('#container-query-type-header').css('display', 'none');
            $('#container-query-type-sql').css('display', 'none');
            $('#container-query-type-swis').css('display', 'none');
        },
        validateInputs: function () {
            $('#errorContainer').hide();
            var typeInt = parseInt($.fn.dataSources.dataSourceData.Type);
            var valid = true;
            var editName;
            if ($.fn.dataSources.editDataSourceConfig != null)
                editName = $.fn.dataSources.editDataSourceConfig.Name;

            if (Ext.isEmpty($.fn.dataSources.dataSourceData.Name)) {
                $('#selectionNameErrorMessage').html('@{R=Core.Strings;K=WEBJS_SO0_63; E=js}').css('display', 'block');
                return false;
            }
            else if (!dataSourceHelpers.isDataSourceNameUnique($.fn.dataSources.dataSourceData.Name, editName)) {
                $('#selectionNameErrorMessage').html('@{R=Core.Strings;K=WEBJS_TM0_138;E=js}').css('display', 'block');
                return false;
            } else
                $('#selectionNameErrorMessage').css('display', 'none');

            // default error message
            $('#errorMessage').html('@{R=Core.Strings;K=WEBJS_SO0_65; E=js}');

            switch (typeInt) {
                case 1: //dynamic selection
                    valid = $.fn.dataSources.dynamicPicker.isValid();
                    break;
                case 2: // custom SWQL 
                case 3: // or SQL
                    if (Ext.isEmpty($.fn.dataSources.dataSourceData.CommandText)) {
                        valid = false;
                        $('#errorMessage').html('@{R=Core.Strings;K=WEBJS_SO0_64; E=js}');
                    }
                    break;
                case 4: // entities
                    //todo
                    break;
            }

            if (valid && !dataSourceHelpers.validateQuery($.fn.dataSources.dataSourceData)) {
                valid = false;
                $('#errorMessage').html('@{R=Core.Strings;K=WEBJS_SO0_65; E=js}');
            }

            if (!valid) {
                $('#errorContainer').show();
                $.fn.dataSources.gridPanel.removeAll();
                $.fn.dataSources.gridPanel.doLayout();
            }

            return valid;
        },
        getCurrentSourceList: function (id) {
            var listId = 'dataSource_' + id;
            for (var s = 0; s < $.fn.dataSources.sources.length; s++) {
                if ($.fn.dataSources.sources[s].ID == listId) {
                    return $.fn.dataSources.sources[s].FrameList;
                }
            }
            return null;
        },
        dataSourceExists: function (refId) {
            var dataStore = $.fn.dataSources.dataSourceList.getStore();
            for (var i = 0; i < dataStore.data.items.length; i++) {
                if (dataStore.data.items[i].data.refId == refId)
                    return true;
            }
            return false;
        },
        syncConfigsData: function () {
            SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'SetSessionData', { name: $.fn.dataSources.dataSourcesSessionIdentifier, value: Ext.util.JSON.encode($.fn.dataSources.dataSourceDataArray) }, function () { });
        },
        validateQuery: function (dataSource) {
            if ($("#isOrionDemoMode").length != 0) {
                return true;
            }
            var valid = false;
            SW.Core.Services.callWebServiceSync('/Orion/Services/DataSourceManagement.asmx', 'ValidateQuery', { dataSource: Ext.util.JSON.encode(dataSource) }, function (res) { valid = res; return; }, function (error) { });
            return valid;
        },
        getQuery: function (dataSource) {
            if ($("#isOrionDemoMode").length != 0) {
                return true;
            }
            var swqlquery = "";
            SW.Core.Services.callWebServiceSync('/Orion/Services/DataSourceManagement.asmx', 'GetReportQuery', { dataSource: Ext.util.JSON.encode(dataSource) }, function (res) { swqlquery = res; return; }, function (error) { });
            return swqlquery;
        },
        previewQueryResults: function (dataSourceConfig) {
            var colConf = [];
            var columns = [];
            SW.Core.Services.callWebService('/Orion/Services/DataSourceManagement.asmx', 'GetQueryResults', { dataSource: Ext.util.JSON.encode(dataSourceConfig) },
                function (res) {
                    if (res == null || res == undefined) {
                        $('#errorContainer').show();
                        $('#errorMessage').html('@{R=Core.Strings;K=WEBJS_SO0_65; E=js}');
                        $.fn.dataSources.gridPanel.removeAll();
                        $.fn.dataSources.gridPanel.doLayout();
                        return false;
                    }
                    if (res.ColumnInfo && res.Data) {
                        for (var i = 0; i < res.ColumnInfo.length; i++) {
                            colConf.push({ name: res.ColumnInfo[i].Name, mapping: i });

                            switch (res.ColumnInfo[i].Type.toLowerCase()) {
                                case "system.boolean":
                                    columns.push({ header: res.ColumnInfo[i].Name, width: 100, sortable: false, dataIndex: res.ColumnInfo[i].Name, type: res.ColumnInfo[i].Type, renderer: $.fn.dataSources.formatBool });
                                    break;
                                case "system.datetime":
                                    columns.push({ header: res.ColumnInfo[i].Name, width: 100, sortable: false, dataIndex: res.ColumnInfo[i].Name, type: res.ColumnInfo[i].Type, renderer: $.fn.dataSources.formatDateTime });
                                    break;
                                default:
                                    columns.push({ header: res.ColumnInfo[i].Name, width: 100, sortable: false, dataIndex: res.ColumnInfo[i].Name, type: res.ColumnInfo[i].Type });
                                    break;
                            }
                        }

                        var gStore = new Ext.data.ArrayStore({
                            data: res.Data.DataTable.Rows,
                            fields: res.Data.DataTable.Columns
                        });
                        $('#errorContainer').hide();

                        var grid = new Ext.grid.GridPanel({ stripeRows: true, trackMouseOver: false, store: gStore, columns: columns, height: 150, width: 720, stateful: false });
                        $.fn.dataSources.gridPanel.removeAll();
                        $.fn.dataSources.gridPanel.add(grid);
                        $.fn.dataSources.gridPanel.doLayout();
                    }
                }, function (error) {
                    return false;
                });
            return true;
        },
        guidGenerator: function () {
            return SW.Core.Services.generateNewGuid();
        }
    };
})(jQuery);

