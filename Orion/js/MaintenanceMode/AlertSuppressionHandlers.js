$(function () {
    var maintenanceModeNamespace = SW.Core.namespace('SW.Core.MaintenanceMode');

    maintenanceModeNamespace.SuppressAlerts = function (uris, from, until, successCallback, failureCallback) {
        SW.Core.Services.callWebService(
            "/api/AlertSuppression",
            "SuppressAlerts",
            {
                uris: uris,
                suppressFrom: from || null,
                suppressUntil: until || null
            },
            successCallback, failureCallback);
    };

    maintenanceModeNamespace.ScheduleAlertSuppression = function (uris, from, until, onFinishedCallback) {
        var bulkSize = SW.Core.MaintenanceMode.BulkSize;
        var bulks = SW.Core.MaintenanceMode.GetBulksFromArray(uris, bulkSize);

        var sendApiRequest = function (uris, successCallback, failureCallback) {
            SW.Core.MaintenanceMode
                .SuppressAlerts(uris, from, until, successCallback, failureCallback);
        };

        SW.Core.MessageBox.ProgressDialog({
            title: "@{R=Core.Strings.2;K=WEBDATA_JP1_2;E=js}",
            items: bulks,
            bulkSize: bulkSize,
            totalItems: uris.length,
            serverMethod: sendApiRequest,
            getArg: function (id) { return id; },
            sucessMessage: "@{R=Core.Strings.2;K=WEBDATA_JP1_3;E=js}",
            failMessage: "@{R=Core.Strings.2;K=WEBDATA_JP1_4;E=js}",
            showOnlySummary: bulks.length > 1,
            afterFinished: onFinishedCallback
        });
    };

    maintenanceModeNamespace.SuppressAlertsNowSingleEntity = function (uri) {
        SW.Core.MaintenanceMode.ScheduleAlertSuppression([uri],
            null,
            null,
            function (numSucceeded) {
                if (numSucceeded > 0) {
                    window.location.reload(true);
                }
            });
    };

    maintenanceModeNamespace.ResumeAlerts = function (uris, onFinishedCallback) {
        var bulkSize = SW.Core.MaintenanceMode.BulkSize;
        var bulks = SW.Core.MaintenanceMode.GetBulksFromArray(uris, bulkSize);

        var sendApiRequest = function (uris, successCallback, failureCallback) {
            SW.Core.Services.callWebService(
                "/api/AlertSuppression",
                "ResumeAlerts",
                {
                    uris: uris
                },
                successCallback, failureCallback);
        };

        SW.Core.MessageBox.ProgressDialog({
            title: "@{R=Core.Strings.2;K=WEBDATA_JP1_5;E=js}",
            items: bulks,
            bulkSize: bulkSize,
            totalItems: uris.length,
            serverMethod: sendApiRequest,
            getArg: function (id) { return id; },
            sucessMessage: "@{R=Core.Strings.2;K=WEBDATA_JP1_6;E=js}",
            failMessage: "@{R=Core.Strings.2;K=WEBDATA_JP1_9;E=js}",
            showOnlySummary: bulks.length > 1,
            afterFinished: onFinishedCallback
        });
    };

    maintenanceModeNamespace.ResumeAlertsSingleEntity = function (uri) {
        SW.Core.MaintenanceMode.ResumeAlerts([uri],
            function (numSucceeded) {
                if (numSucceeded > 0) {
                    window.location.reload(true);
                }
            });
    };

    maintenanceModeNamespace.GetAlertSuppressionStates = function (entityUris) {
        var promise = $.Deferred();

        if (!entityUris || !entityUris.length) {
            promise.resolve();
            return promise;
        }

        SW.Core.Services.callController(
            "/api/AlertSuppression/GetAlertSuppressionState",
            {
                uris: entityUris
            },
            function(result) {
                promise.resolve(result);
            },
            function(error) {
                promise.reject(error);
            });

        return promise;
    }
});