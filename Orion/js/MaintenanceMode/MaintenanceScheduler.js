SW.Core.namespace("SW.Core.MaintenanceMode").MaintenanceScheduler = function() {

    var getSingleEntitySchedule = function (key, query) {
        var promiseSchedule = $.Deferred();

        SW.Core.Services.callController(
            '/api/Swis/Query',
            { query: query },
            function (result) {
                var schedule = {
                    key: key
                }

                if (result.length > 0) {
                    var from = result[0].DateTimeFrom;
                    var until = result[0].DateTimeUntil;
					var utcTimezoneMarker = "Z";
                    if (from) {
                        schedule.from = new Date(from + utcTimezoneMarker);
                    }
                    if (until) {
                        schedule.until = new Date(until + utcTimezoneMarker);
                    }
                }

                promiseSchedule.resolve(schedule);
            },
            function (error) {
                promiseSchedule.reject(error);
            });

        return promiseSchedule;
    };

    var getAlertSuppressionSchedule = function (entityUri) {
        var queryAlertSuppressionSchedule =
            "SELECT SuppressFrom AS DateTimeFrom, SuppressUntil AS DateTimeUntil" +
            " FROM Orion.AlertSuppression " +
            " WHERE EntityUri='" + entityUri + "' AND (SuppressUntil>=GetUtcDate() OR SuppressUntil IS NULL)";

        return getSingleEntitySchedule("alertSuppression", queryAlertSuppressionSchedule);
    };

    var getUnmanageSchedule = function (entityUri) {

        var queryUnManageSchedule =
            "SELECT UnmanageFrom AS DateTimeFrom, UnmanageUntil AS DateTimeUntil" +
            " FROM System.ManagedEntity " +
            " WHERE Uri='" + entityUri + "' AND UnManageUntil>=GetUtcDate()";

        return getSingleEntitySchedule("unmanage", queryUnManageSchedule);
    };

    var getScheduleForEntity = function (entityUri) {
        var promiseSchedules = $.Deferred();

        $.when(
            getAlertSuppressionSchedule(entityUri),
            getUnmanageSchedule(entityUri))
            .done(function (alertSuppressionSchedule, unmanageSchedule) {
                promiseSchedules.resolve([alertSuppressionSchedule, unmanageSchedule]);
            })
            .fail(function (error) {
                console.log("Failed to resolve schedule for " + entityUri + "." + error);
                promiseSchedules.reject("@{R=Core.Strings.2;K=MaintenanceScheduler_FailedToRetrieveMaintenanceSchedule;E=js}");
            });

        return promiseSchedules;
    };

    var onScheduleAlertSuppression = function(entityUri, schedule, onFinishedCallback, scheduleSuccessCallback, scheduleFailCallback) {
        SW.Core.MaintenanceMode.SuppressAlerts(
            [entityUri],
            schedule.from,
            schedule.until,
            function() {
                scheduleSuccessCallback();
                onFinishedCallback();
            },
            function() {
                scheduleFailCallback("@{R=Core.Strings.2;K=MaintenanceScheduler_FailedToScheduleMaintenance;E=js}");
            });
    };

    var onScheduleMultipleAlertSuppressions = function (entityUris, schedule, onFinishedCallback) {
        SW.Core.MaintenanceMode.ScheduleAlertSuppression(
            entityUris,
            schedule.from,
            schedule.until,
            onFinishedCallback);
    };

    var onScheduleUnmanage = function(netObjectIds, schedule, onFinishedCallback, scheduleSuccessCallback, scheduleFailCallback) {
        SW.Core.MaintenanceMode.UnmanageNetObjects(
            netObjectIds,
            schedule.from,
            schedule.until,
            function() {
                scheduleSuccessCallback();
                onFinishedCallback();
            },
            function() {
                scheduleFailCallback("@{R=Core.Strings.2;K=MaintenanceScheduler_FailedToScheduleMaintenance;E=js}");
            });
    };

    var onScheduleUnmanageMultipleEntities = function (netObjectIds, schedule, onFinishedCallback) {
        // Schedule unmanage for volumes is not supported
        var netObjectIdsWithoutVolumes = jQuery.extend(true, {}, netObjectIds);
        netObjectIdsWithoutVolumes.V = [];
        SW.Core.MaintenanceMode.ScheduleUnmanage(
            netObjectIdsWithoutVolumes,
            schedule.from,
            schedule.until,
            onFinishedCallback);
    };

    var showDialogForSingleEntity = function (entityUri, netObjectId, entityCaption, onFinishedCallback) {

        var promiseSchedules = getScheduleForEntity(entityUri);

        if (!onFinishedCallback) {
            onFinishedCallback = function () {
                window.location.reload(true);
            }
        }

        var config = {
            title: entityCaption,
            schedules: promiseSchedules,
            onSchedule: function(schedule, scheduleSuccessCallback, scheduleFailCallback) {

                if (schedule.key === "alertSuppression") {
                    onScheduleAlertSuppression(entityUri,
                        schedule,
                        onFinishedCallback,
                        scheduleSuccessCallback,
                        scheduleFailCallback);

                } else if (schedule.key === "unmanage") {
                    var netObjectIds = [netObjectId];
                    onScheduleUnmanage(netObjectIds,
                        schedule,
                        onFinishedCallback,
                        scheduleSuccessCallback,
                        scheduleFailCallback);
                };
            }
        };

        SW.Core.MaintenanceMode.MaintenanceSchedulerDialog.show(config);
    };

    var getNodesTitle = function (nodes)
    {
        if (nodes.length === 0) {
            return "";
        }

       return nodes.length === 1
            ? String.format("@{R=Core.Strings.2;K=MaintenanceMode_Title_Node;E=js}", nodes.length)
            : String.format("@{R=Core.Strings.2;K=MaintenanceMode_Title_Nodes;E=js}", nodes.length);
    };

    var getInterfacesTitle = function(interfaces) {
        if (interfaces.length === 0) {
            return "";
        }

        return interfaces.length === 1
            ? String.format("@{R=Core.Strings.2;K=MaintenanceMode_Title_Interface;E=js}", interfaces.length)
            : String.format("@{R=Core.Strings.2;K=MaintenanceMode_Title_Interfaces;E=js}", interfaces.length);
    }

    var getDialogTitle = function (netObjects) {

        var nodesTitle = getNodesTitle(netObjects.N);
        var interfacesTitle = getInterfacesTitle(netObjects.I);

        return nodesTitle + ((nodesTitle && interfacesTitle) ? ", " : "") + interfacesTitle;
    };

    var getDialogInfoMessage = function(netObjects) {
        return (netObjects.External.length + netObjects.V.length) > 0
            ? "@{R=Core.Strings.2;K=MaintenanceMode_Info_ChangesNotBeAppliedOnExternalNodesAndVolumes;E=js}"
            : null;
    };

    var showDialogForMultipleEntities = function (netObjects, onFinishedCallback) {

        var config = {
            title: getDialogTitle(netObjects),
            infoMessage: getDialogInfoMessage(netObjects),
            schedules: [],
            onSchedule: function (schedule, scheduleSuccessCallback) {

                if (schedule.key === "alertSuppression") {
                    // Close the scheduler dialog
                    scheduleSuccessCallback();

                    // Show progress dialog and apply changes
                    onScheduleMultipleAlertSuppressions(netObjects.MuteAlertsUris,
                        schedule,
                        onFinishedCallback);

                } else if (schedule.key === "unmanage") {
                    // Close the scheduler dialog
                    scheduleSuccessCallback();

                    // Show progress dialog and apply changes
                    onScheduleUnmanageMultipleEntities(netObjects,
                        schedule,
                        onFinishedCallback);
                };
            }
        };

        SW.Core.MaintenanceMode.MaintenanceSchedulerDialog.show(config);
    };

    // Export
    return {
        GetScheduleForEntity: getScheduleForEntity,
        ShowDialogForSingleEntity: showDialogForSingleEntity,
        ShowDialogForMultipleEntities: showDialogForMultipleEntities
    }
}();