﻿$(function () {

    var maintenanceModeNamespace = SW.Core.namespace('SW.Core.MaintenanceMode');

    // This enum needs to be synced with SolarWinds.Orion.Core.Common.Models.Alerts.EntityAlertSuppressionMode
    maintenanceModeNamespace.EntityAlertSuppressionMode = {
        NotSuppressed: 0,
        SuppressedByItself: 1,
        SuppressedByParent: 2,
        SuppressionScheduledForItself: 3,
        SuppressionScheduledForParent: 4
    };

    maintenanceModeNamespace.BulkSize = 100;

    maintenanceModeNamespace.GetBulksFromArray = function (items, bulkSize) {
        var R = [];
        for (var i = 0; i < items.length; i += bulkSize)
            R.push(items.slice(i, i + bulkSize));
        return R;
    }

    maintenanceModeNamespace.GetNetObjectIdsPerNodeInterfaceVolume = function (netObjectIds) {
        var idsPerNodeInterfaceVolume = { N: [], I: [], V: [] };
        jQuery.each(netObjectIds, function (idx, el) {
            var parts = el.split(':');
            if (parts[0] === 'N') {
                idsPerNodeInterfaceVolume.N.push(parts[1]);
            } else if (parts[0] === 'I' || parts[0] === 'IW') {
                idsPerNodeInterfaceVolume.I.push(parts[1]);
            } else if (parts[0] === 'V') {
                idsPerNodeInterfaceVolume.V.push(parts[1]);
            };
        });

        return idsPerNodeInterfaceVolume;
    };

    maintenanceModeNamespace.GetNetObjectIdsList = function (netObjectIds) {
        var netObjectIdsList = [];
        $.each(netObjectIds.N, function (idx, el) {
            netObjectIdsList.push('N:' + el);
        });

        $.each(netObjectIds.I, function (idx, el) {
            netObjectIdsList.push('I:' + el);
        });

        $.each(netObjectIds.V, function (idx, el) {
            netObjectIdsList.push('V:' + el);
        });

        return netObjectIdsList;
    };
});