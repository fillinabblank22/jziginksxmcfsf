﻿//<![CDATA[
SW.Core.namespace("SW.Core.MaintenanceMode").PlanServiceFactory = function(information) {
    var self = this;

    // dependencies
    self.informationService = information;

    // config
    var entityNames = {
        plan: "Orion.MaintenancePlan",
        assignment: "Orion.MaintenancePlanAssignment"
    };

    var queries = {
        getPlanId: "SELECT ID FROM Orion.MaintenancePlan WHERE Uri = '{0}'",
        getEntityTypeByPrefix: "SELECT Prefix, EntityType FROM Orion.NetObjectTypes WHERE Prefix IN ({0})"
    };

    // cache
    var prefixToEntityTypeCache = {
        _data: {},
        _prefixList: [],
        setPrefixList: function(ids) {
            this._prefixList = [];
            for (var netObjectPrefix in ids) {
                if (ids[netObjectPrefix].length) {
                    this._prefixList.push(netObjectPrefix);
                }
            }
        },
        load: function(informationService, successCallback) {
            var _self = this;
            var inPrefixes = "'" + this._prefixList.join("','") + "'";
            var getEntityMetadataQuery = String.format(queries.getEntityTypeByPrefix, inPrefixes);
            informationService.Query(getEntityMetadataQuery, function(result) {
                _self.store(result.Rows);
                successCallback();
            });
        },
        store: function(data) {
            for (var i = 0; i < data.length; ++i) {
                this._data[data[i][0]] = data[i][1];
            }
        },
        get: function(prefix) {
            return this._data ? this._data[prefix] : null;
        }
    };

    // methods & export
    var getEntityIdCounter = function(entityIds) {
        var entityIdCounter = { current: 0 };
        for (var key in entityIds) {
            if (entityIds.hasOwnProperty(key) && entityIds[key].length) {
                entityIdCounter.current += entityIds[key].length;
            }
        }
        entityIdCounter.total = entityIdCounter.current;
        return entityIdCounter;
    };

    return {

        /**
         * @param {int} planId 	ID of associated MaintenancePlan
         * @param {int} entityId 	ID of associated entity
         * @param {string} netObjectPrefix 	NetObjectPrefix of associated entity
         * @param {callback} progressHandler 	Optional callback when an assignment is created
         * @param {callback} successHandler 	Optional callback when all assignments are created
         * @param {callback} errorHandler 	Optional error callback
         * @param {object} asyncFinishCounter 	Optional object with counter property maintaining count of pending createMaintenancePlanAssignment calls for plan creation { current: int}
         */
        createMaintenancePlanAssignment: function(planId, entityId, netObjectPrefix, progressHandler, successHandler, errorHandler, asyncFinishCounter) {
            var params = {
                InstanceType: entityNames.assignment,
                EntityId: entityId,
                EntityType: prefixToEntityTypeCache.get(netObjectPrefix),
                EntityUri: "", // todo implement
                MaintenancePlanID: planId
            };

            self.informationService.Create(params, function() {
                progressHandler && progressHandler(asyncFinishCounter.current, asyncFinishCounter.total);
                if (asyncFinishCounter && --asyncFinishCounter.current <= 0) {
                    successHandler && successHandler(asyncFinishCounter.total);
                }
            });
        },

        /**
         * @param {int} params 	Dictionary, for definition see MaintenancePlan entity definition (optional properties can be omitted)
         * @param {int} netObjectIds 	Dictionary with keys being netObjectPrefixes and values array of int IDs
         * @param {callback} progressHandler 	Optional callback when an assignment is created
         * @param {callback} successHandler 	Optional callback when all assignments are created
         * @param {callback} errorHandler 	Optional error callback
         */
        createMaintenancePlan: function(params, netObjectIds, progressHandler, successHandler, errorHandler) {
            var selfPublic = this;
            var asyncFinishCounter = getEntityIdCounter(netObjectIds);

            var addAssignments = function(result) {
                var planId = parseInt(result.Rows[0][0]);

                // add assignments
                for (var netObjectPrefix in netObjectIds) {
                    if (netObjectIds.hasOwnProperty(netObjectPrefix)) {
                        for (var i = 0; i < netObjectIds[netObjectPrefix].length; ++i) {
                            var netObjectId = netObjectIds[netObjectPrefix][i];
                            selfPublic.createMaintenancePlanAssignment(planId, netObjectId, netObjectPrefix, progressHandler, successHandler, errorHandler, asyncFinishCounter);
                        }
                    }
                };
            };

            params["InstanceType"] = entityNames.plan;
            prefixToEntityTypeCache.setPrefixList(netObjectIds);
            prefixToEntityTypeCache.load(self.informationService, function() {
                self.informationService.Create(params, function(uri) {
                    var getPlanIdQuery = String.format(queries.getPlanId, uri);
                    self.informationService.Query(getPlanIdQuery, addAssignments, errorHandler);
                }, errorHandler);
            });
        },

        removeMaintenancePlanAssignments: function (nodeIds, interfaceIds, successHandler, errorHandler) {
            var request = { 'NodeIds': nodeIds, 'InterfaceIds': interfaceIds };
            SW.Core.Services.callControllerAction("/api/MaintenancePlanAssignment", "RemoveAssignments", request,
            function success() {
                if (typeof successHandler == "function") {
                    successHandler();
                }
            },
            function error() {
                if (typeof errorHandler == "function") {
                    errorHandler();
                }
            });
        }
    };

};

$(function () {
    SW.Core.MaintenanceMode.PlanService = SW.Core.MaintenanceMode.PlanServiceFactory(Information);
});
//]]>