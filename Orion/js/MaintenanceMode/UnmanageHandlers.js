﻿$(function () {
    var maintenanceModeNamespace = SW.Core.namespace('SW.Core.MaintenanceMode');

    /* Apply unmanage on netObjects and show progress in dialog */
    maintenanceModeNamespace.ScheduleUnmanage = function (netObjectIds, from, until, onFinishedCallback) {
        // For progress dialog, we need array of NetObjectsIds
        netObjectIds = (netObjectIds.N || netObjectIds.I || netObjectIds.V) ? SW.Core.MaintenanceMode.GetNetObjectIdsList(netObjectIds) : netObjectIds;

        var bulkSize = SW.Core.MaintenanceMode.BulkSize;
        var bulks = SW.Core.MaintenanceMode.GetBulksFromArray(netObjectIds, bulkSize);
        
        var unmanageFunction = function (netObjectIds, successCallback, failureCallback) {
            SW.Core.MaintenanceMode.UnmanageNetObjects(netObjectIds,
                from,
                until,
                successCallback,
                failureCallback);
        }
        SW.Core.MessageBox.ProgressDialog({
            title: "@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow;E=js}",
            items: bulks,
            bulkSize: bulkSize,
            totalItems: netObjectIds.length,
            serverMethod: unmanageFunction,
            getArg: function(id) { return id },
            sucessMessage: "@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow_Success;E=js}",
            failMessage: "@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow_Failed;E=js}",
            showOnlySummary: bulks.length > 1,
            afterFinished: onFinishedCallback
        });
        return false;
    };

    maintenanceModeNamespace.UnmanageNowSingleEntity = function(netObjectId) {
        SW.Core.MaintenanceMode.ScheduleUnmanage([netObjectId],
            null,
            null,
            function (numSucceeded) {
                if (numSucceeded > 0) {
                    window.location.reload(true);
                }
            });
    };

    maintenanceModeNamespace.UnmanageNetObjects = function (netObjectIds, startDate, endDate, successCallback, failureCallback) {
        netObjectIds = (netObjectIds.N || netObjectIds.I || netObjectIds.V) ? netObjectIds : SW.Core.MaintenanceMode.GetNetObjectIdsPerNodeInterfaceVolume(netObjectIds);

        if (netObjectIds.N.length > 0)
            NodeManagement.UnmanageNodes(netObjectIds.N,
                startDate || null,
                endDate || null,
                function () {
                    if (netObjectIds.I.length === 0) {
                        successCallback();
                    }
                },
                function (error) {
                    failureCallback(error);
                }
            );
        if (netObjectIds.I.length > 0)
            NodeManagement.UnmanageInterfaces(netObjectIds.I,
                startDate || null,
                endDate || null,
                function () { successCallback(); },
                function (error) {
                    failureCallback(error);
                }
            );
        if (netObjectIds.V.length > 0)
            NodeManagement.UnmanageVolumes(netObjectIds.V,
                startDate || null,
                endDate || null,
                function () { successCallback(); },
                function (error) {
                    failureCallback(error);
                }
            );
    };

    /* Manage netObjects again and show progress in dialog */
    maintenanceModeNamespace.ManageAgain = function (netObjectIds, onFinishedCallback) {
        // For progress dialog, we need array of NetObjectsIds
        netObjectIds = (netObjectIds.N || netObjectIds.I || netObjectIds.V) ? SW.Core.MaintenanceMode.GetNetObjectIdsList(netObjectIds) : netObjectIds;

        var bulkSize = SW.Core.MaintenanceMode.BulkSize;
        var bulks = SW.Core.MaintenanceMode.GetBulksFromArray(netObjectIds, bulkSize);

        var remanageFunction = function (netObjectIds, successCallback, failureCallback) {
            SW.Core.MaintenanceMode.RemanageNetObjects(netObjectIds,
                successCallback,
                failureCallback);
        }
        SW.Core.MessageBox.ProgressDialog({
            title: "@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain;E=js}",
            items: bulks,
            bulkSize: bulkSize,
            totalItems: netObjectIds.length,
            serverMethod: remanageFunction,
            getArg: function (id) { return id },
            sucessMessage: "@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain_Success;E=js}",
            failMessage: "@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain_Failed;E=js}",
            showOnlySummary: bulks.length > 1,
            afterFinished: onFinishedCallback
        });
        return false;
    };

    maintenanceModeNamespace.ManageAgainSingleEntity = function (netObjectId) {
        SW.Core.MaintenanceMode.ManageAgain([netObjectId],
            function (numSucceeded) {
                if (numSucceeded > 0) {
                    window.location.reload(true);
                }
            });
    };

    maintenanceModeNamespace.RemanageNetObjects = function (netObjectIds, successCallback, failureCallback) {
        netObjectIds = (netObjectIds.N || netObjectIds.I || netObjectIds.V) ? netObjectIds : SW.Core.MaintenanceMode.GetNetObjectIdsPerNodeInterfaceVolume(netObjectIds);

        if (netObjectIds.N.length > 0) {
            NodeManagement.RemanageNodes(netObjectIds.N,
                function () {
                    if (netObjectIds.I.length === 0) {
                        successCallback();
                    }
                },
                function (error) {
                    failureCallback(error);
                }
            );
        }

        if (netObjectIds.I.length > 0) {
            NodeManagement.RemanageInterfaces(netObjectIds.I,
                function () { successCallback(); },
                function (error) {
                    failureCallback(error);
                }
            );
        }
        if (netObjectIds.V.length > 0) {
            NodeManagement.RemanageVolumes(netObjectIds.V,
                function () { successCallback(); },
                function (error) {
                    failureCallback(error);
                }
            );
        }
    };

});