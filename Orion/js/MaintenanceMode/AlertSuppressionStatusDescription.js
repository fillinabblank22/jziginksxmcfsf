﻿(function () {
    var maintenanceModeNamespace = SW.Core.namespace('SW.Core.MaintenanceMode');

    maintenanceModeNamespace.GetAlertSuppressionPlan = function (uri) {

        var promise = $.Deferred();

        SW.Core.Services.callControllerAction(
            "/api/AlertSuppression",
            "GetAlertSuppressionState",
            { "uris": [uri] },
            promise.resolve,
            promise.reject
            );
        return promise;
    };

    var getEntityDisplayName = function(uri) {
        var promise = $.Deferred();

        Information.QueryWithParameters(
            "SELECT DisplayName FROM System.Entity Where Uri = @uri",
            { 'uri': uri },
            promise.resolve,
            promise.reject
            );

        return promise;
    };

    var appendCancelLink = function (text, uri, isDemo, hasRights) {
        var linkStart = String.format("<a class='cancelPlannedMaintenanceLink' href='#' onclick='SW.Core.MaintenanceMode.ResumeAlertsSingleEntity(\"{0}\");return false;'>", uri);
        var linkDemo = String.format("<a class='cancelPlannedMaintenanceLink' href='#' onclick='demoAction(\"{0}\");return false;'>", "Core_Alerting_Schedule");
        var linkCaption = "@{R=Core.Strings.2;K=AlertSuppression_CancelLink;E=js}";
        var linkEnd = "</a>";

        if (isDemo)
        {
            return String.format("{0} {1}{2}{3}", text, linkDemo, linkCaption, linkEnd);
        }
        else if (hasRights)
        {
            return String.format("{0} {1}{2}{3}", text, linkStart, linkCaption, linkEnd);
        }

        return text;
    }

    var buildMessage = function (uri, plan, isDemo, hasRights) {
        var config = {
            from: plan.SuppressedFrom,
            until: plan.SuppressedUntil,
            status: plan.SuppressionMode,
            parentName: plan.DisplayName
        };
		
		// transform to readable date-time
		var utcTimezoneMarker = "Z";
        var from = config.from ? SW.Core.DateHelper.formatShortDateTime(new Date(config.from + utcTimezoneMarker)) : null;
        var until = config.until ? SW.Core.DateHelper.formatShortDateTime(new Date(config.until + utcTimezoneMarker)) : null;

        var description = "";

        switch (config.status) {
            case SW.Core.MaintenanceMode.EntityAlertSuppressionMode.NotSuppressed:
                // empty description on purpose
                break;
            case SW.Core.MaintenanceMode.EntityAlertSuppressionMode.SuppressedByItself:
                // alert suppression is active for this entity
                description = "@{R=Core.Strings.2;K=AlertSuppression_SuppressedStatusDescription;E=js}";
                break;
            case SW.Core.MaintenanceMode.EntityAlertSuppressionMode.SuppressionScheduledForItself:
                // alert suppression scheduled for this entity
                description = (until !== null) ?
                    String.format("@{R=Core.Strings.2;K=AlertSuppression_ScheduledStatusDescription;E=js}", from, until) :
                    String.format("@{R=Core.Strings.2;K=AlertSuppression_ScheduledNoEndStatusDescription;E=js}", from);
                description = appendCancelLink(description, uri, isDemo, hasRights);
                break;
            case SW.Core.MaintenanceMode.EntityAlertSuppressionMode.SuppressedByParent:
                // alert suppression is active for parent
                description = String.format("@{R=Core.Strings.2;K=AlertSuppression_SuppressedByParentStatusDescription;E=js}", config.parentName);
                break;
            case SW.Core.MaintenanceMode.EntityAlertSuppressionMode.SuppressionScheduledForParent:
                // alert suppression scheduled for parent
                description = (until !== null) ?
                    String.format("@{R=Core.Strings.2;K=AlertSuppression_ScheduledByParentStatusDescription;E=js}", config.parentName, from, until) :
                    String.format("@{R=Core.Strings.2;K=AlertSuppression_ScheduledByParentNoEndStatusDescription;E=js}", config.parentName, from);
                break;
            default:
                break;
        }
        return description;
    }

    maintenanceModeNamespace.GetStatusDescription = function (uri, isDemo, hasRights) {
        var promise = $.Deferred();
        var planPromise = maintenanceModeNamespace.GetAlertSuppressionPlan(uri);
        planPromise.done(function (plan) {
			var displayPromise = $.Deferred();
			var firstPlan = plan[0];
			if (firstPlan.SuppressedParentUri) {
				var displayNamePromise = getEntityDisplayName(firstPlan.SuppressedParentUri);
				displayNamePromise.done(function(table) {
					var displayName = table.Rows && table.Rows.length > 0 ? table.Rows[0][0] : firstPlan.SuppressedParentUri;
					displayPromise.resolve(displayName);
				});
				displayNamePromise.fail(function() {
					displayPromise.resolve(firstPlan.SuppressedParentUri);
				});
			} else {
				displayPromise.resolve('');
			}
			displayPromise.done(function(displayName) {
				firstPlan.DisplayName = displayName;
                var description = buildMessage(uri, firstPlan, isDemo, hasRights);
                promise.resolve(description);
			});
        });
        planPromise.fail(function () {
            promise.resolve("<span class=\"sw-suggestion sw-suggestion-warn\"><span class=\"sw-suggestion-icon\"></span> @{R=Core.Strings.2;K=AlertSuppression_FailStatusDescription;E=js}</span>")
        });
        return promise;
    };
}());