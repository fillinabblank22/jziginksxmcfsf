﻿//<![CDATA[
SW.Core.namespace("SW.Core.MaintenanceMode").CreateMaintenanceModeDialog = function () {
    
    // Options
    var dialogOptions = {
        modal: true,
        resizable: false,
        title: "@{R=Core.Strings.2;K=WEBDATA_JP1_1;E=js}",
        width: 550,
        height: "auto",
        overlay: { "background-color": "black", opacity: 0.4 },
        open: function () {
            // Workaround, according to a jQuery UI ticket will be fixed in version 1.9 by using the focusSelector option
            $(this).find('input').attr("tabindex", "1");
        }
    };

    var dateMax = new Date(9999, 1);
        
    // Events
    var dateTimePickerEvents = {
        endingOnStatusChanged: "endingOnStatusChanged"
    };
        
    // Methods
    var preprocessNetObjectIds = function(netObjectIds) {
        var result = {};
        var length = netObjectIds.NetObjectIDs.length;
        for (var i = 0; i < length; i++) {
            var split = netObjectIds.NetObjectIDs[i].split(":");
            result[split[0]] = result[split[0]] || [];
            result[split[0]].push(parseInt(split[1]));
        }

        return result;
    };

    var getDateTimeRangePicker = function(dialog) {
        return $(".sw-dateTimeRangePicker", dialog);
    };

    var validateForm = function (dialog) {
        var addError = function(element, message) {
            var errorElement = $("<div>" + message + "</div>");
            errorElement.addClass("sw-validation-error").addClass("sw-suggestion").addClass("sw-suggestion-fail");
            errorElement.insertAfter(element);
        };

        dialog.find(".sw-validation-error").remove();

        var result = true;
        var rangePicker = getDateTimeRangePicker(dialog);
        var range = rangePicker.getDateTimeRange(new Date, dateMax);
        if (range.start > range.end) {
            addError(rangePicker, "@{R=Core.Strings.2;K=WEB_JS_CODE_VS1_12;E=js}");
            result = false;
        }

        var reason = $(".sw-startMaintenanceDialog-reason", dialog);
        if (reason.val().length == 0) {
            addError(reason, "@{R=Core.Strings.2;K=WEBDATA_JP1_8;E=js}");
            result = false;
        }

        return result;
    };

    var handleSubmit = function (netObjectIds, dialog) {
        if (dialog.data("in-progress")) {
            return;
        }

        if (validateForm(dialog) === false) {
            return;
        }

        dialog.data("in-progress", true);
        var spinner = $(".sw-startMaintenanceDialog-ajaxLoader", dialog);
        spinner.show();

        var reason = $(".sw-startMaintenanceDialog-reason", dialog).val();
        var dateTimeRange = getDateTimeRangePicker(dialog).getDateTimeRange(new Date, dateMax);
        netObjectIds = preprocessNetObjectIds(netObjectIds);

        // add plan
        var params = {
            AccountID: dialog.attr("data-account-id"),
            Reason: reason,
            UnmanageDate: dateTimeRange.start,
            RemanageDate: dateTimeRange.end
        };

        var progressHandler = function(finishedAssignments, totalAssignments) {

        };

        var successHandler = function (totalAssignments) {
            dialog.data("in-progress", false);
            spinner.hide();

            window.location.reload(true);
        };

        var errorHandler = function (error) {
            dialog.data("in-progress", false);
            spinner.hide();

            alert("@{R=Core.Strings.2;K=WEBDATA_JP1_7;E=js}");
        };

        SW.Core.MaintenanceMode.PlanService.createMaintenancePlan(params, netObjectIds, progressHandler, successHandler, errorHandler);
    };

    var initializeStartMaintenanceDialog = function (netObjectIds) {
        var key = "SW.Core.MaintenanceMode.GUI.boundDialog";
        var template = $(".sw-startMaintenanceDialog").last();

        if (template.data(key)) { // avoid reinitializing existing dialog
            dialog = template.data(key).dialog();
            dialog.data("maintenanceMode-netObjectIds", netObjectIds);

            return template.data(key).dialog();
        }

        var dialog = template.dialog(dialogOptions);
        dialog.data("maintenanceMode-netObjectIds", netObjectIds);

        // Elements
        var indefiniteModeWarningBox = $(".sw-startMaintenanceDialog-indefiniteModeWarning", dialog);

        // Initialize
        var now = new Date;
        var thisHour = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours());
        getDateTimeRangePicker(dialog)
            .setDateTimeRangeFrom(thisHour)
            .setDateTimeRangeTo(thisHour);
            
        // Bindings
        dialog.data("in-progress", false);
        $(".sw-btn-primary", dialog).click(function (e) {
            e.preventDefault();
            handleSubmit(dialog.data("maintenanceMode-netObjectIds"), dialog);
        });

        $(".sw-btn-secondary", dialog).click(function (e) {
            e.preventDefault();
            dialog.dialog("close");
        });

        $(".sw-dateTimeRangePicker", dialog).on(dateTimePickerEvents.endingOnStatusChanged, function (e, isChecked) {
            if (isChecked) {
                indefiniteModeWarningBox.hide();
            } else {
                indefiniteModeWarningBox.show();
            }
        });

        template.data(key, dialog);
        return dialog;
    };


    // Export
    return {
        show: function(netObjectIds) {
            initializeStartMaintenanceDialog(netObjectIds).show();
        }
    };

}();
//]]>