﻿// Extracted with slight modification from SchedulerControl
(function ($) {
    

    var placeholderSupported = typeof document.createElement("input").placeholder !== "undefined";
    if (placeholderSupported) {
        $("[customplaceholder]:not([placeholder])").each(function() {
            var self = this;
            self.attr("placeholder", self.attr("customplaceholder"));
        });
    } else {
        var initializeKey = '__jsPlaceholderJsInitialized';
        if (window[initializeKey]) {
            return;
        }

        var handleFocus = function() {
            var input = $(this);
            if (input.val() == input.attr('customplaceholder')) {
                input.val('');
                input.addClass('placeholder');
            } else {
                input.removeClass('placeholder');
            }
        };

        var handleBlur = function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('customplaceholder')) {
                input.addClass('placeholder');
                input.val(input.attr('customplaceholder'));
            } else {
                input.removeClass('placeholder');
            }
        };

        var handleChange = function() {
            var input = $(this);
            if (input.val() == input.attr('customplaceholder')) {
                input.addClass('placeholder');
            } else {
                input.removeClass('placeholder');
            }
        };

        var handleCleanupIfEmpty = function() {
            $(this).find('[customplaceholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('customplaceholder')) {
                    input.val('');
                }
            });
        };

        $(document).on('focus', '[customplaceholder]', handleFocus);
        $(document).on('blur', '[customplaceholder]', handleBlur);
        $(document).on('change', '[customplaceholder]', handleChange);
        $(document).on('submit', 'form', handleCleanupIfEmpty);
        window[initializeKey] = true;
    }
    
}(jQuery));
