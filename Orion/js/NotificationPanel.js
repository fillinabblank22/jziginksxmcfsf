function UpdateNotificationIcon(element) {

    $(element).closest("li").fadeOut("fast", function () {
        var notificationsCountElem = $("#notifications-count");
        var notificationsCount = parseInt($(notificationsCountElem).text());
        var notificationsCountTitle = $("#notification-count-title");
        if (notificationsCount > 1) {
            notificationsCount--
            $(notificationsCountElem).text(notificationsCount);
            $(notificationsCountTitle).text(notificationsCount);
        } else {
            $(notificationsCountTitle).parent().hide();
            $(notificationsCountElem).remove();
            $("#no-notifications").show();
        }
    });
}

function AcknowledgeNotificationItemsByType(element, type, createdBefore) {

    if ($("#isSWNotificationDemoMode").length != 0) {
        demoAction("Core_AcknowledgeNotificationByType", this);
        return;
    }
 
	$.ajax({
		type: 'Post',
		url: '/Orion/Services/NotificationPanel.asmx/AcknowledgeNotificationItemsByType',
		data: "{'type': '" + type + "', 'createdBefore': '" + createdBefore + "'}",
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: UpdateNotificationIcon(element),
		error: function(error) {
		}
    });
}

function AcknowledgeNotificationItemById(element, notificationId, createdBefore) {

    if ($("#isSWNotificationDemoMode").length != 0) {
        demoAction("Core_AcknowledgeNotificationById", this);
        return;
    }

    $.ajax({
        type: 'Post',
        url: '/Orion/Services/NotificationPanel.asmx/AcknowledgeNotificationItemById',
        data: "{'notificationId': '" + notificationId + "', 'createdBefore': '" + createdBefore + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: UpdateNotificationIcon(element),
        error: function(error) {
        }
    });
}

function AcknowledgeAllNotificationItems(element, createdBefore) {

    if ($("#isSWNotificationDemoMode").length != 0) {
        demoAction("Core_AcknowledgeAllNotification", this);
        return;
    }

    $.ajax({
        type: 'Post',
        url: '/Orion/Services/NotificationPanel.asmx/AcknowledgeAllNotificationItems',
        data: "{'createdBefore': '" + createdBefore + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function () {
            $("#notification-count-title").text("0");
            $("#notifications-container .notifications-list > li").each(function () {
                $(this).toggle();
                $("#notifications-count").remove();
            });
            $("#notifications-container .notifications-container-header").remove();
            $("#notifications-container .dismiss-all").remove();
        },
        error: function (error) {
        }
    });
}
