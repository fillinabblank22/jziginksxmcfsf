﻿(function (core) {
    var view = core.View = core.View || {};
    var ajaxInfos = [];
    var dataNeedsUpdate = 'AsyncViewNeedsUpdate';
    var isNocViewMode = false;
    var viewForceRefreshAll;
    var viewRefreshIntervalId;

    SW.Core.Observer.makeCancellableObserver(view);

    $.each(location.href.split('&'), function (index, el) { if (el.toLowerCase().indexOf('isnocview=true') > -1) isNocViewMode = true; });
    
    // this one needs to run after ScriptResource.axd for ClientScript manager
    var originalPostback = window.__doPostBack;
    
    if (originalPostback) {
    
        // run postback against RenderControl for specific view (mimic updatepanel)
        var doPostbackForControl = function(marker, eventTarget, eventArgument) {
        
            var theForm = $('<form />');
            var eventTargetInput = $('<input type="text" id="__EVENTTARGET" name="__EVENTTARGET" />');
            var eventArgumentInput = $('<input type="text" id="__EVENTARGUMENT" name="__EVENTARGUMENT" />');
            var viewStateInput = $('<input type="text" id="__VIEWSTATE" name="__VIEWSTATE" />');
            var viewStateGeneratorInput = $('<input type="text" id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" />');
            
            var target = marker.parents(".ResourceWrapperPanel").eq(0).find(".ResourceWrapper").eq(0);
            
            eventTargetInput.val(eventTarget);
            eventArgumentInput.val(eventArgument);
            viewStateInput.val(target.data('sw-viewstate'));
            viewStateGeneratorInput.val(target.data('sw-viewstate-generator'));
            
            theForm.append(eventTargetInput);
            theForm.append(eventArgumentInput);
            theForm.append(viewStateInput);
            theForm.append(viewStateGeneratorInput);
            
            // include also all elements inside of the resource
            $.each(target.find('input'), function(_, value) {
                var newHidden = $('<input type="text" />');
                newHidden.attr('id', value.id);
                newHidden.attr('name', value.name);
                newHidden.val(value.value);
                
                theForm.append(newHidden);
            });

            var data = theForm.serialize(); // is there any better way of achieving this?

            var rid = marker.data('resourceId');

            var details = {
                ResourceID: rid,
                NetObject: getParameterByName("NetObject"),
                isNOCView: getParameterByName("isNOCView"),
                currentUrl: marker.data('currentUrl') ? marker.data('currentUrl') : ''
            };
            
            SW.Core.Loader.Control(
                target,
                details,
                data,
                'replace',
                function () { 
                    SW.Core.VisibilityObserver.loadingCompleted(rid.toString());
                    resourceLoadErrorHandler(target, rid);
                },
                function () { 
                    SW.Core.VisibilityObserver.loadingCompleted(rid.toString());
                });
        };
    
        var renderControlEnabledPostback = function(eventTarget, eventArgument) {
        
            var item = $('#' + eventTarget.replace(/\$/g, '_'));
            var wrapper = item.closest('.ResourceWrapperPanel');
            var reloadMarker = wrapper.find('.resourceReloadMarker');
            
            var loadingMode = reloadMarker.data('resourceLoadingMode');
            if (loadingMode === 'RenderControl')
            {
                // we are in render control, so use specific handling
                doPostbackForControl(reloadMarker, eventTarget, eventArgument);
            }
            else
            {
                // no render control. do whatever was meant to be done before we intercepted.
                originalPostback(eventTarget, eventArgument);
            }
        };
            
        window.__doPostBack = renderControlEnabledPostback;
    }

    view.InitRefreshInterval = function (refreshDelay, forceRefreshAll) {
        if (typeof viewRefreshIntervalId !== 'undefined')
            clearInterval(viewRefreshIntervalId);

        viewForceRefreshAll = forceRefreshAll;
        viewRefreshIntervalId = setInterval(function() {
            SW.Core.View.Refresh(true);
        }, refreshDelay);
    }

    view.AddOnRefresh = function (onRefreshCallback, resourceWrapperId) {
        ajaxInfos.push({
            callback: onRefreshCallback,
            resourceWrapperId: resourceWrapperId
        });
    };

    view.AddOnRefreshWithRenderControl = function (resourceId, urlPath, resourceWrapperId) {
        // do not register refresh when rendered via render control
        if (urlPath.toLowerCase().indexOf('rendercontrol.aspx?') != -1) {
            return;
        }

        var refresh = function () {
            var marker = $("[data-resource-id='" + resourceId + "']");
            var target = marker.prevAll(".ResourceWrapper").eq(0);

            var details = {
                ResourceID: marker.data('resourceId'),
                NetObject: getParameterByName("NetObject"),
                isNOCView: getParameterByName("isNOCView")
            };

            if (view.trigger('refresh.start', details) === false ||
                view.trigger('refresh.start[' + resourceId + ']', details) === false)
                return;

            SW.Core.Loader.Control(
                    target,
                    details,
                    {},
                    'replace');
        };
        view.AddOnRefresh(refresh, resourceWrapperId);
    };

    hasSynchronousResources = function () {
        return $("[data-resource-loading-mode]").length == 0
            || $("[data-resource-loading-mode='Synchronous']").length > 0;
    }

    isRefreshDisabled = function () {
        return $("body").hasClass("sw-dashboard--editing")
            || $('.xui-modal-top').length > 0;
    }

    view.Refresh = function (refreshSyncResources) {
        if ((refreshSyncResources === true) && hasSynchronousResources() && !isRefreshDisabled()) {
            view.trigger('refresh.halt');

            if (viewForceRefreshAll)
                window.location.reload(true);
            else
                window.location.reload();
        } else {
            onRefresh(false);
        }
    };

    view.RefreshNocSlide = function (slide) {
        if (!isNocViewMode) return;
        $(slide).find("[data-resource-loading-mode='RenderControl']").each(function (index, element) {
            reloadByRenderControl($(element), true);
        });
        var i;
        // Ajax
        for (i = 0; i < ajaxInfos.length; ++i) {
            var ajaxInfo = ajaxInfos[i];
            if (ajaxInfo.resourceWrapperId) {
                var wrapper = $(slide).find('#' + ajaxInfo.resourceWrapperId);
                if (wrapper.length == 0) continue;
                ajaxInfo.callback();
                wrapper.data(dataNeedsUpdate, false);
            } else {
                ajaxInfo.callback();
            }
        }

        refreshPending = true;
    };

    var refreshPending = false;

    $(document).ajaxStop(function () {
        if (refreshPending) {
            refreshPending = false;
            var localOffset = (new Date().getTimezoneOffset() * 60 * 1000);
            $("div[id$=viewTimestamp]").text(new Date((new Date()).getTime() + SW.Core.Date.ServerTimeOffset + localOffset)._toFormattedString(window.Sys.CultureInfo.CurrentCulture.dateTimeFormat.FullDateTimePattern, window.Sys.CultureInfo.CurrentCulture));
        }
    });

    // from http://stackoverflow.com/questions/901115/get-query-string-values-in-javascript
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS, "i");
        var results = regex.exec(window.location.search);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var overlapsViewport = function (element) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(element).offset().top;
        var elemBottom = elemTop + $(element).height();

        return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom));
    };

    var reloadByRenderControl = function (marker, evenIfHidden) {
        var rid = marker.data('resourceId');
        SW.Core.VisibilityObserver.registerByResourceId(rid.toString(),function () {
            var target = marker.prevAll(".ResourceWrapper").eq(0);
            if (target.length === 0) {
                target = marker.prevAll("div").find(".ResourceWrapper").eq(0);
            }

            if (evenIfHidden || overlapsViewport(target)) {
                marker.data(dataNeedsUpdate, false);
                var errorMarker = target.find('#resourceError-' + rid);
                // show loading animation only during initial load of the resource
                if (evenIfHidden || errorMarker.length > 0) {
                    errorMarker.remove();
                    showLoadingAnimation(target, rid);
                }
    
                var details = {
                    ResourceID: rid,
                    NetObject: getParameterByName("NetObject"),
                    isNOCView: getParameterByName("isNOCView"),
                    currentUrl: marker.data('currentUrl') ? marker.data('currentUrl') : ''
                };
    
                if (view.trigger('refresh.start', details) === false ||
                    view.trigger('refresh.start[' + rid + ']', details) === false)
                    return;
    
                SW.Core.Loader.Control(
                        target,
                        details,
                        {},
                        'replace',
                    function () { 
                        SW.Core.VisibilityObserver.loadingCompleted(rid.toString());
                        resourceLoadErrorHandler(target, rid);},
                    function () { 
                        SW.Core.VisibilityObserver.loadingCompleted(rid.toString());});
            } else {
                //console.log("Skipping ", target, " because it is outside the viewport (render control)");
                marker.data(dataNeedsUpdate, true);
            }
        },true);
    };

    var showLoadingAnimation = function (target, resourceId) {
        var image = $('<img/>').attr({
            src: '/Orion/images/AJAX-Loader.gif',
            style: 'margin-right: 5px;'
        });
        var loadingSpan = $('<span/>').attr({
            style: 'font-weight: bold; color: #777;  text-align: center; display: block; margin-left: auto; margin-right: auto',
            id: 'resourceAnimation-' + resourceId
        });
        loadingSpan.append(image);
        loadingSpan.append('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}');
        target.append(loadingSpan);
    };

    var resourceLoadErrorHandler = function (target, resourceId) {
        target.find("div[id$='resContent']").html('');  // remove original resource content
        target.find('#resourceAnimation-' + resourceId).remove(); // remove animation

        var errorDiv = $('<div/>').
            attr('id', 'resourceError-' + resourceId);

        var suggestionDiv = $('<div/>').
            attr('class', 'sw-suggestion sw-suggestion-fail').
            append($('<span/>').attr('class', 'sw-suggestion-icon')).
            append(String.format('@{R=Core.Strings;K=WEBJS_PF0_2;E=js}', resourceId));

        errorDiv.append(suggestionDiv);
        errorDiv.append(String.format('<p>@{R=Core.Strings;K=WEBJS_PF0_1;E=js}</p>', '<a target="_blank" rel="noopener noreferrer" class="sw-link" href="https://knowledgebase.solarwinds.com/kb/">', '</a>'));
        target.append(errorDiv);
    };


    var onRefresh = function (initialLoadOnly) {
        var i;
        if (isNocViewMode) return;

      try { if(!initialLoadOnly) view.trigger('refresh.before');

        $("[data-resource-loading-mode='RenderControl']").each(function (index, element) {
            reloadByRenderControl($(element), initialLoadOnly);
        });

        if (!initialLoadOnly) {
            // Ajax
            for (i = 0; i < ajaxInfos.length; ++i) {
                var ajaxInfo = ajaxInfos[i];
                if (ajaxInfo.resourceWrapperId) {
                    var wrapper = $('#' + ajaxInfo.resourceWrapperId);
                    if (wrapper.length === 0) continue;
                    if (overlapsViewport(wrapper)) {
                        ajaxInfo.callback();
                        wrapper.data(dataNeedsUpdate, false);
                    } else {
                        //console.log("Skipping ", wrapper, " because it is outside the viewport (ajax)");
                        wrapper.data(dataNeedsUpdate, true);
                    }
                } else {
                    ajaxInfo.callback();
                }
            }
        }

        refreshPending = true;
            
      } finally { if (!initialLoadOnly) view.trigger('refresh.after'); }
    };

    $(window).scroll(function () {
        $("[data-resource-loading-mode='RenderControl']").each(function (index, element) {
            var marker = $(element);
            if (marker.data(dataNeedsUpdate))
                reloadByRenderControl(marker, false);
        });

        $.each(ajaxInfos, function (index, ajaxInfo) {
            if (ajaxInfo.resourceWrapperId) {
                var wrapper = $('#' + ajaxInfo.resourceWrapperId);
                if (wrapper.data(dataNeedsUpdate) && overlapsViewport(wrapper)) {
                    ajaxInfo.callback();
                    wrapper.data(dataNeedsUpdate, false);
                }
            }
        });
    });
    //$('<button>').text('Refresh').css({ position: 'fixed', top: 10, right: 10 }).appendTo('#content').click(function () { onRefresh(false); });
    onRefresh(true);
})(SW.Core);
