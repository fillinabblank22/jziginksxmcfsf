﻿/* 
String format like in C#
*/
String.prototype.format = function (params) {
    var pattern = /{([^}]+)}/g;
    return this.replace(pattern, function ($0, $1) { return params[$1]; });
};

function ObjectStringPropertiesToSecureString(obj) {
    if ((obj != null) && typeof (obj) == 'object') {
        try {
            obj = Object.create(obj); // create copy of object
        }
        catch (e) { }
    }

    if (typeof (obj) != 'undefined') {
        for (var p in obj) {
            if (typeof (p) != 'undefined') {
                if (typeof (obj[p]) == 'string') {
                    obj[p] = obj[p].toSecureString();
                }
            }
        }
    }

    return obj;
};

String.prototype.toSecureString = function () {
    return this.replace(/</ig, "&lt").replace(/>/ig, "&gt;");
};

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

