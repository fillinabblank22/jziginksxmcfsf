﻿SW.Orion.FrequencyController = function(config) {
    "use strict";

    var browserUtcOffset = (new Date()).getTimezoneOffset() * -1;

    var loadFrequencies = function (data, editMode) {
        var startCount = displayList.length+1;
        SW.Core.Services.callController("/api/Frequency/GetGroupSchedules", { "list": data },
            function(response) {
                var i = 0;
                displayList = [];
                for (var item in response) {
                    var k = 0;
                    displayList[i] = new Array();
                    var schedules = $.makeArray(response[i].Schedules);
                    for (var r in schedules) {
                        try {
                            if (response[i].Schedules[k] != undefined) {
                                displayList[i][k] = (JSON.parse(response[i].Schedules[k]));
                                displayList[i][k].state = getState(displayList[i][k].EnabledDuringTimePeriod);
                                displayList[i][k].ToolTip = response[i].ToolTip;
                            }
                            k++;
                        } catch (e) {

                        }
                    }
                    i++;
                }
                renderFrequencies(displayList);
                if (startCount > displayList.length && displayList.length != 0 && !editMode) {
                    Ext.Msg.show({
                        title: '@{R=Core.Strings.2;K=WEBJS_OM0_2;E=js}',
                        msg: '@{R=Core.Strings.2;K=WEBJS_OM0_3;E=js}',
                        minWidth: 200,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                }
            }
        );
    };

    var getState = function(enabledDuringTimePeriod) {
        var state = "";
        if (enabledDuringTimePeriod != null) {
            if (enabledDuringTimePeriod == true) {
                state = "@{R=Core.Strings;K=WEBJS_PS1_23;E=js}"; 
            } else {
                state = "@{R=Core.Strings;K=WEBJS_PS1_24;E=js}";
            }
        }
        return state;
    };

    var getEmptyScheduleType = function (useTimePeriod) {
        SW.Core.Services.callController("/api/Frequency/GetScheduleItemType",
            {
                useTimePeriod: useTimePeriod,
                browserUtcOffset: browserUtcOffset
            },
            function (response) {
                if (useTimePeriod == true) {
                    SW.Orion.FrequencyController.emptyTimeperiodModel = JSON.parse(response);
                    showScheduleControl(SW.Orion.FrequencyController.emptyTimeperiodModel, false);
                } else {
                    SW.Orion.FrequencyController.emptyReportModel = JSON.parse(response);
                    showScheduleControl(SW.Orion.FrequencyController.emptyReportModel, false);
                }

            }
        );
    };

    var getSchedulesItem = function (obj,editMode, id) {
        SW.Core.Services.callController("/api/Frequency/GetSchedule", JSON.stringify({ "object": obj }),
            function (response) {
                
                var tempStorage = [];
                if (editMode) {
                    displayList[id] = JSON.parse(response);
                    tempStorage = getFlatScheduleList();
                } else {
                    tempStorage = getFlatScheduleList();
                    tempStorage = tempStorage.concat(JSON.parse(response));
                }
                loadFrequencies(tempStorage, editMode);
            }
        );
    };

    var getFlatScheduleList = function() {
        var tempStorage = [];
        var i = 0;
        var l = 0;
        for (var item in displayList) {
            var k = 0;
            var schedules = $.makeArray(displayList[i]);
            for (var r in schedules) {
                try {
                    if (displayList[i][k] != undefined) {
                        tempStorage[l] = (displayList[i][k]);
                        l++;
                    }
                    k++;
                } catch (e) {

                }
            }
            i++;
        }
        return tempStorage;
    };
	
	var cloneEncodedFrequencies = function (frequencies) {
		if (null == frequencies || "object" != typeof frequencies) return frequencies;
		
		var copiedFreq;
		
		if (frequencies instanceof Array) {
			copiedFreq = [];
			for (var i = 0, len = frequencies.length; i < len; i++) {
				copiedFreq[i] = cloneEncodedFrequencies(frequencies[i]);
			}
			return copiedFreq;
		}
		
		copiedFreq = frequencies.constructor();
		for (var attr in frequencies) {
			if (frequencies.hasOwnProperty(attr)) copiedFreq[attr] = (attr == "DisplayName") ? Ext.util.Format.htmlEncode(frequencies[attr]) : frequencies[attr] ;
		}
		return copiedFreq;
	}

    var renderFrequencies = function (freq) {
        var template;
        if (freq && freq[0] != undefined) {
            template = $('#frequenciesListTemplate').html();
        } else {
            template = $('#noFrequencyTemplate').html();
        }
		
		var newFreq = cloneEncodedFrequencies(freq);
        var newHtml = _.template(template, { frequencies: newFreq, useTimePeriodMode: config.useTimePeriodMode });
        var html = $(newHtml);
        html.delegate('[data-form="addNewFrequency"]', "click", addNewAction);

        html.delegate('[data-form="deleteFrequency"]', "click", deleteFrequency);
        html.delegate('[data-form="copyFrequency"]', "click", copyFrequency);
        html.delegate('[data-form="editFrequency"]', "click", editFrequency);

        $container.find('[data-form="friquenciesList"]').html(html);
        
        $('#jsonActionsContainer').val(JSON.stringify(getFlatScheduleList()));

        if (config.singleScheduleMode) {
            if(freq && freq[0] != undefined) {
                $container.find(".bigButton").hide();
            } else {
                $container.find(".bigButton").show();
            }
        }
    };

    var showDialog = function(id, editMode, container) {
        var $elem = container;
        if (editMode) {
            var $notEditedPicker = $elem.clone(true);
            $($notEditedPicker).find('select').each(function(index, item) {
                //set new select to value of old select
                $(item).val($elem.find('select').eq(index).val());
            });
        }

        var title;
        if (config.useTimePeriodMode) {
        	if (editMode) {
        		title = "@{R=Core.Strings.2;K=WEBJS_BV0_0002;E=js}";
        	} else {
	            title = "@{R=Core.Strings.2;K=WEBJS_BV0_0003;E=js}";
	        }
        } else {
        	if (editMode) {
        		title = "@{R=Core.Strings.2;K=WEBJS_OM0_4;E=js}";
        	} else {
	            title = "@{R=Core.Strings.2;K=WEBJS_OM0_5;E=js}";
	        }
        }

        $elem.dialog({
            modal: true,
            draggable: true,
            resizable: false,
            position: ['center', 'center'],
            width: 540,
			title: title,
            dialogClass: 'ui-dialog-osx',
            autoOpen: true,
            close: function (ev, ui) {
                destroyDialog(id, container);
            }
        });
        $(SW.Core.Widgets.Button("@{R=Core.Strings.2;K=WEBJS_OM0_6;E=js}", { type: 'secondary', id: 'cancelBtn' })).appendTo($elem.find(".buttons")).css('float', 'right').css('margin', '10px').click(function () {
            destroyDialog(id, container);
        });

        if (editMode) {
        	var addButtonCaption = "@{R=Core.Strings.2;K=WEBJS_OM0_7;E=js}";
        } else {
        	if (config.useTimePeriodMode) {
        		var addButtonCaption = "@{R=Core.Strings.2;K=WEBJS_BV0_0004;E=js}";
        	} else {
        		var addButtonCaption = "@{R=Core.Strings.2;K=WEBJS_OM0_8;E=js}";
        	}
        }

        $(SW.Core.Widgets.Button(addButtonCaption, { type: 'primary', id: 'scheduleSelectedReportsBtn' })).appendTo($elem.find(".buttons")).css('float', 'right').css('margin', '10px').click(function () {
            if (SW.Orion.FrequencyPickerController.Validate()) {
                SW.Orion.FrequencyPickerController.CollectDateTimeJson($elem);
                var obj = SW.Orion.FrequencyPickerController.GetFrequenciesObject($elem);
                saveFrequency(obj, editMode, id);
                $elem.removeAttr("style");
                destroyDialog(id, container);
            }
        });
    };

    var destroyDialog = function(id, container) {
        container.dialog('destroy').empty();
        container.find("#schPickerContainer").empty();
        container.find(".buttons").empty();
    };

    var addNewAction = function () {
        getEmptyScheduleType(config.useTimePeriodMode);
    };

    var showScheduleControl = function (data, editMode, index) {

        var request = [];
        for (var item in data) {
            if (item != "remove") {
                request.push(data[item]);
            }
        }

        var dialog = $container.find('#scheduleDialogContainer').clone();
        $container.find("#dialogFrequencyContainer").append(dialog);
        SW.Core.Loader.Control(dialog.find('#schPickerContainer'), {
            Control: "~/Orion/Controls/SchedulerControl.ascx"
        }, {
            'config': {
                'SingleScheduleMode': config.singleScheduleMode,
                'UseTimePeriodMode': config.useTimePeriodMode,
                'BrowserUtcOffset': browserUtcOffset, // !!! needs to be before JsonSchedules otherwise it won't be set in SchedulerControl.ascx during DataBind
                'JsonSchedules': JSON.stringify(request),
                'DisableTimePeriodStr': config.disableTimePeriodStr,
                'EnableTimePeriodStr': config.enableTimePeriodStr
            }
        }, 'replace', function () {

        }, function () {
            showDialog(index, editMode, dialog);
        });
        dialog.removeClass('actionsOffPage');
    };

    var editFrequency = function (elem) {
        var id = $(elem.currentTarget).attr("data-index");
        SW.Orion.FrequencyControllerDefinition.loadScheduleControl(id, true);
    };

    var copyFrequency = function (elem) {

        var index = $(elem.currentTarget).attr("data-index");
        if (index > -1) {
            var request = [];
            for (var item in displayList) {
                if (item != "remove") {
                    request.push(displayList[item]);
                }
            }

            var copiedObject = displayList[index];
            for (var obj in copiedObject) {
                if (obj != "remove") {
                    copiedObject[obj].FrequencyId = 0;
                }
            }

            request.push(copiedObject);
            displayList = request;
            renderFrequencies(displayList);
        }
    };

    var deleteFrequency = function (elem) {
        var index = $(elem.currentTarget).attr("data-index");
        if (index > -1) {
            delete displayList[index];
            var request = [];
            for (var item in displayList) {
                if (item != "remove") {
                    request.push(displayList[item]);
                }
            }
            displayList = request;
            renderFrequencies(displayList);
        }
    };

    var saveFrequency = function (object, editMode, id) {
        if (editMode == true) {
            try {
                delete displayList[id];
            } catch (e) {
            }
        }
        //add client timezone Offset
        var today = new Date();
        object.timezoneOffset = getStandardOffset(today) * -1;
        object.supportsDaylightSavingTime = supportsDaylightSavingTime(today);
        object.browserUtcOffset = browserUtcOffset;
        object.dstDate = dstDate;
        getSchedulesItem(object, editMode, id);
    };

    
function daysInMonth(year, month) {
   return new Date(year, month + 1, 0).getDate();
}

var currentYear = (new Date()).getFullYear();
var winterOffset = new Date(currentYear, 0, 1).getTimezoneOffset();
var dstDateFound = false;
var dstDate;

// only months where can be DST
for (var i = 2; i < 4; i++) {
    var days = daysInMonth(currentYear, i);
    //document.write(days, '-', i, '<br />');
    
    for(var j = 1; j <= days; j++) {
        var x = new Date(currentYear, i, j, 14);
        if (x.getTimezoneOffset() != winterOffset) {            
            dstDate = x;            
            dstDateFound = true;
            break;
        }
    }   
    
    if (dstDateFound === true) {        
        //document.write(dstDate, '<br />');
        break;
    }
    
    // if DST is detected in a day then detect the hour
}
     
    //base offset of time zone
    var getStandardOffset = function (dateTime) {
        var standardOffset;
        if(daylightSavingTimeType(dateTime) < 0)
        {
            var jul = new Date(dateTime.getFullYear(), 6, 1);
            standardOffset = jul.getTimezoneOffset();
        }else
        {
            var jan = new Date(dateTime.getFullYear(), 0, 1);
            standardOffset = jan.getTimezoneOffset();
        }
        return standardOffset;
    };

    var supportsDaylightSavingTime = function (dateTime) {
        return (daylightSavingTimeType(dateTime) != 0)
    };

    // detects: 
    // whether time zone don't observes DST (0) 
    // or it is observes DST and it is Austral/South type (<0)
    // or it observes DST and it is Boreal/North (>0)
    var daylightSavingTimeType = function(dateTime){
        var jan = new Date(dateTime.getFullYear(), 0, 1);
        var jul = new Date(dateTime.getFullYear(), 6, 1);

        return jan.getTimezoneOffset() - jul.getTimezoneOffset();
    };

    this.init = function () {
        loadFrequencies(config.frequencies);
        $container = $(config.container);
    };

    this.getFrequency = function () {
        if ($container.find('[data-form="NoSchedule"] input').is(":checked")) {
            return [];
        } else {
            return getFlatScheduleList();
        }
    };

    this.loadScheduleControl = function (index, editMode) {
        if (index > -1) {
            showScheduleControl(displayList[index], editMode, index);
        } else {
            getEmptyScheduleType(true);
        }
    };

    var $container = null;
    var displayList = [];

    SW.Orion.FrequencyController.emptyTimeperiodModel = {};
    SW.Orion.FrequencyController.emptyReportModel = {};
}
