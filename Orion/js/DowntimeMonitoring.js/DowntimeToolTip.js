﻿SW.Core.DowntimeMonitoring.ToolTip = SW.Core.DowntimeMonitoring.ToolTip || {};

(function (obj) {
	
	function isIE8to10(){
		$html = $("html");
		return $html.hasClass("sw-is-msie-8") || $html.hasClass("sw-is-msie-9") || $html.hasClass("sw-is-msie-10");
	}
	
    obj.downtimeTooltip = function (options) {
        this.options = options;
		this.active = undefined;
		this.followed = undefined;
		
        this.initTooltip = function (attachedElement, followElement) {
			
			if(attachedElement[0] === this.active){
				return;
			} else{
				
				this.elementJObj = $(attachedElement);
				
				if(this.active){
					$(this.active).off("mousemove");
					$(this.active).off("mouseout");
				}
				
				this.active = attachedElement[0];
				
				this.elementJObj.mousemove(this, this.mouseMoveEvent);
				this.elementJObj.mouseout(this, this.mouseLeaveEvent);
			}
			
			if(followElement[0] === this.followed){
				return;
			} else{
				if(this.followed){
					$(this.followed).off("mousemove");
					$(this.followed).off("mouseout");
				}
				
				this.followed = followElement[0];
				
				if (followElement)
				{
					$(followElement).mousemove(this, this.mouseMoveEvent);
					$(followElement).mouseout(this, this.mouseLeaveEvent);
				}
			}
			
        };

        this.hideTooltip = function () {
            this.tipJObj.css({ visibility: 'hidden' });
        };

        this.wrapTooltip = function (tooltipElementId) {
            if (!this.tipJObj) {
                var tooltipClassName = this.options.tooltipClassName;
                var tooltipElement = document.getElementById(tooltipElementId);

                var tipElement = document.createElement('div');
                tipElement.className = tooltipClassName;

                var tipInnerContentElement = document.createElement('div');
                tipInnerContentElement.className = tooltipClassName + "-inner";
                tipInnerContentElement.appendChild(tooltipElement);

                tipElement.id = tooltipElementId + '-tooltip';
                $('#' + tipElement.id).remove();
                tipElement.appendChild(tipInnerContentElement);

                var tipJObj = $(tipElement);
                tipJObj.css({ top: 0, left: 0, visibility: 'hidden', display: 'block' }).appendTo(document.body);
                this.tipJObj = tipJObj;
            }
        };

        this.mouseLeaveEvent = function (e) {
            e.data.hideTooltip();
        };

        this.mouseMoveEvent = function (e) {
			
			// for IE8 up to v10 the pointer events don't work and for that reason
			// we need to figure out other way to get information which part of the
			// downtime timeline is mouse over right now. The small tooltip causes
			// that mousemove events of the timeline are not triggered, so the big tooltip
			// gets not updated when we move with the small tooltip rectangle.
			if(isIE8to10() && e.target)
			{
				var target = $(e.target);
				var td;
				
				if(target.is(".sw-downtime-part")){
					td = target[0];
				}
				else{
					var $td = $(e.target).find(".sw-downtime-part:first");
					
					if($td.length > 0){
						td = $td[0];
					}
					else{
						function getElsAt(top, left){
							return $("body")
							.find(".sw-downtime-part:visible")
							.filter(function(index, element) {
								
								var rect = element.getBoundingClientRect();
								
								return rect.top <= top
									&& rect.bottom >=top
									&& rect.left <= left
									&& rect.right >=left;
							});
						}
						
						var el = getElsAt(e.clientY, e.clientX);
						
						if(el.length > 0){
							td = el[0];
						}
					}
				}
				if(td)
				{
					SW.Core.DowntimeMonitoring.ToolTip.ContentManager.updateTooltip(td);
				}
			}
			e.data.setPosition(e.pageX);
        };

        this.setTooltipWidth = function (width) {
            this.tipJObj.width(width);
        };

        this.setPosition = function (pageX) {
            var tooltipObj = this;

            var pos = {
                width: tooltipObj.elementJObj.outerWidth(),
                height: tooltipObj.elementJObj.outerHeight(),
                top: tooltipObj.elementJObj.offset().top,
                left: tooltipObj.elementJObj.offset().left
            };

            var tipOuterWidth = tooltipObj.tipJObj.outerWidth();
            var tipOffsetWidth = tooltipObj.tipJObj.outerWidth();

            var tp = { top: pos.top + pos.height + tooltipObj.options.offsetY, left: pos.left };

            var axis = {
                xAxisStart: tp.left + tipOffsetWidth / 2,
                xAxisEnd: tp.left + pos.width - tipOuterWidth / 2,
                yAxisStart: tp.top - pos.height,
                yAxisEnd: tp.top
            };

            if (pageX > axis.xAxisStart) {
                if (pageX <= axis.xAxisEnd) {
                    tp.left = tp.left + pageX - axis.xAxisStart;
                }
            }
            if (pageX >= axis.xAxisEnd) {
                tp.left = axis.xAxisEnd - tipOffsetWidth / 2;
            }

            tooltipObj.tipJObj.css(tp);

            if (tooltipObj.options.fade) {
                tooltipObj.tipJObj.stop().css({ opacity: 0, display: 'block', visibility: 'visible' }).animate({ opacity: tooltipObj.options.opacity });
            } else {
                tooltipObj.tipJObj.css({ visibility: 'visible', opacity: this.options.opacity });
            }
        };
    };
})(SW.Core.DowntimeMonitoring.ToolTip);