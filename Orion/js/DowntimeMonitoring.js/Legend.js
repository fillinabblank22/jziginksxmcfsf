﻿SW.Core.DowntimeMonitoring.Legend = SW.Core.DowntimeMonitoring.Legend || {};

(function (obj) {
    obj.createLegend = function (callback) {
		if (typeof obj.legentTemplate === 'undefined') {
			var clbk = function(data) {
				obj.legentTemplate = data;
				callback(data);
			}
			
			$.ajax({
				type: 'GET',
				url: '/Orion/templates/DowntimeMonitoring/legendTmpl.html',
				success: function (data) {
					clbk($(data));
				},
				async: false
			});
		} else {
			callback($(obj.legentTemplate));
		}
    };

    obj.initLegend = function (template, uniqPrefix) {
        var statusHelper = SW.Core.DowntimeMonitoring.Status;
        var statusList = statusHelper.getStatusList();
        var legendDiv = document.createElement('div');
        var customTable = document.getElementById(uniqPrefix);
        legendDiv.id = uniqPrefix + '-' + "legend";
        $('#' + legendDiv.id).remove();
        customTable.parentElement.appendChild(legendDiv);
        var legendTmpl = template.html();
        var legendControl = _.template(legendTmpl, { statusList: statusList, statusHelper: statusHelper });
        $(legendDiv).append(legendControl);
    };

})(SW.Core.DowntimeMonitoring.Legend);