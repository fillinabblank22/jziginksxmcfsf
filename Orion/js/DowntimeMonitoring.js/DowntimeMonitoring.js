﻿SW.Core.DowntimeMonitoring = SW.Core.DowntimeMonitoring || {};

(function(){
	// For browsers without fromISO function we provide custom implementation which parses the 
	// input string and transforms it to a date object.
	// Snippet from http://stackoverflow.com/a/11021320
    var D= new Date('2011-06-02T09:34:29+02:00');
    if(!D || +D!== 1307000069000){
        Date.fromISO= function(s){
            var day, tz,
            rx=/^(\d{4}\-\d\d\-\d\d([tT ][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/,
            p= rx.exec(s) || [];
            if(p[1]){
                day= p[1].split(/\D/);
                for(var i= 0, L= day.length; i<L; i++){
                    day[i]= parseInt(day[i], 10) || 0;
                };
                day[1]-= 1;
                day= new Date(Date.UTC.apply(Date, day));
                if(!day.getDate()) return NaN;
                if(p[5]){
                    tz= (parseInt(p[5], 10)*60);
                    if(p[6]) tz+= parseInt(p[6], 10);
                    if(p[4]== '+') tz*= -1;
                    if(tz) day.setUTCMinutes(day.getUTCMinutes()+ tz);
                }
                return day;
            }
            return NaN;
        }
    }
    else{
        Date.fromISO= function(s){
            return new Date(s);
        }
    }
})();

(function (obj) {
    obj.createDowntimeMonitoring = function (callback) {
		if (typeof obj.downtimeTemplate === 'undefined') {
			var clbk = function(data) {
				obj.downtimeTemplate = data;
				callback(data);
			};
			
			$.ajax({
				type: 'GET',
				url: '/Orion/templates/DowntimeMonitoring/downtimeTmpl.html',
				success: function (data) {
					clbk($(data));
				},
				async: false
			});
		} else {
			callback($(obj.downtimeTemplate));
		}
    };

    var formatHelper = {
        //Parse input milliseconds from Settings into Date
        setInputDateFormat: function (d) {
            var milliseconds = parseInt(d, 10);
            var res = new Date(milliseconds);
            return res;
        },

        //Parse DateTime into suitable format for display
        setOuputDateFormat: function (d, downTimeSettings) {
			var date = Date.fromISO(d);
            var formattedDate = $.datepicker.formatDate(downTimeSettings.regionalDateFormat.dateFormat, date);
            var formattedTime = downTimeSettings.statusHelper.formatTime(date, downTimeSettings.regionalDateFormat.timeSeparator);
            var res = formattedDate + ' ' + formattedTime;
            return res;
        },

        setTooltipDateFormat: function (d) {
            var date = Date.fromISO(d);
            return date;
        },

        formatSlicesArray: function (parts, startDate, endDate) {
            var barObj = {
                slices: [],
                startDate: startDate,
                endDate: endDate
            }

            for (var i = 0; i < parts.length; i++) {
                for (var j = 0; j < parts[i].TimeParts.length; j++)
                    barObj.slices.push(parts[i].TimeParts[j].Slices);
            }
            return barObj;
        }
    };

    obj.initDowntimeMonitoring = function (id, element, downTimeSettings, template) {
        var result = '';
        SW.Core.Services.callWebServiceSync(
              "/Orion/Services/AsyncResources.asmx",
              "GetDowntimeData",
              {
                  netObject: id,
                  startDate: formatHelper.setInputDateFormat(downTimeSettings.startDate),
                  finalDate: formatHelper.setInputDateFormat(downTimeSettings.endDate),
                  hoursPerBlock: downTimeSettings.hoursPerBlock
              },
              function (data) {
				  if(data !== "")
				  {
						var bar = "";
						bar =  JSON.parse(data);
						result = formatHelper.formatSlicesArray(bar.TimeBlocks, formatHelper.setTooltipDateFormat(bar.StartDateTime), formatHelper.setTooltipDateFormat(bar.EndDateTime));
						var temp = template.html();
						var downtimeMonitoringHtml = _.template(temp, { items: bar, formatHelper: formatHelper, downTimeSettings: downTimeSettings });
						$(element).append(downtimeMonitoringHtml);
				  }
              }
          );
        return result;
    };

})(SW.Core.DowntimeMonitoring);