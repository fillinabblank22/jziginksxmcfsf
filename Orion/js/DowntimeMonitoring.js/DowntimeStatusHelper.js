﻿SW.Core.DowntimeMonitoring.Status = SW.Core.DowntimeMonitoring.Status || {};

(function (obj) {
    obj.getStatusList = function () {
        var statusList = "";
        SW.Core.Services.callWebServiceSync(
            "/Orion/Services/AsyncResources.asmx",
            "GetDowntimeStatusLegend",
            {
                objectType: 'I'
            },
                function (result) {
                    statusList = JSON.parse(result);
                });
        return statusList;
    };

    obj.setInterfaceColors = function (colorId) {
        switch (colorId) {
            case 0:
            case 5:
                return "#999999";
            case 1:
                return "#77BD2D";
            case 2:
                return "#950000";
            case 3:
                return "#FCD928";
            case 4:
                return "#F99D1C";
            case 9:
                return "#3366ff";
            case 10:
                return "#a8d8e8";
            case 12:
                return "#000000";
			case 14: 
				return "#D50000"
            default:
                return "#999999";
        }
    }

    obj.formatTime = function (dateTime, timeSeparator) {
        var hours = dateTime.getHours();
        var minutes = dateTime.getMinutes();

        var result = (hours < 10 ? "0" + hours : hours) + timeSeparator + (minutes < 10 ? "0" + minutes : minutes);
        return result;
    }

})(SW.Core.DowntimeMonitoring.Status);