﻿SW.Core.DowntimeMonitoring.ToolTip.ContentManager = SW.Core.DowntimeMonitoring.ToolTip.ContentManager || {};

(function (obj) {

    var downtimeBars = {};
    var templatePrefix;
    var settings;

    obj.initializeDowntimeControl = function (options) {
        settings = options;
        options.statusHelper = SW.Core.DowntimeMonitoring.Status;
        options.statusList = SW.Core.DowntimeMonitoring.Status.getStatusList();
        options.regionalDateFormat = JSON.parse(options.dateFormatSettings);

        templatePrefix = options.clientId + '-';

        var tooltipNamespace = SW.Core.DowntimeMonitoring.ToolTip;
        var bigTooltip = new tooltipNamespace.downtimeTooltip({
            tooltipClassName: "sw-downtime-tooltip",
            offsetY: 4,
            opacity: 1,
            fade: false,
            enabled: true
        });
        var smallTooltip = new tooltipNamespace.downtimeTooltip({
            tooltipClassName: "sw-downtime-tooltip",
            offsetY: -10,
            opacity: 1,
            fade: false,
            enabled: true
        });

        var downTimeSettings = {
            mode: options.mode,
            startDate: options.startDate,
            endDate: options.endDate,
            hoursPerBlock: options.hoursPerBlock,
            statusHelper: options.statusHelper,
            regionalDateFormat: options.regionalDateFormat
        };

        SW.Core.Resources.CustomQuery.initialize({
            uniqueId: options.uniqueId,
            initialPage: 0,
            rowsPerPage: options.rowsPerPage,
            searchTextBoxId: options.searchTextBoxId,
            searchButtonId: options.searchButtonId,
            allowSort: true,

            onLoad: function (rows) {
                var tableId = options.uniqueId;
                var tableRows = $("#Grid-" + tableId + " tr");
                $(tableRows[0]).addClass('sw-downtime-HeaderRow');

                var onMouseEnter = function () {
                    var n = this;

                    smallTooltip.initTooltip($(n), smallTooltip.tipJObj);
                    smallTooltip.setTooltipWidth(calculateTooltipWidth(this));
                    bigTooltip.initTooltip($(n), smallTooltip.tipJObj);


                }

                SW.Core.DowntimeMonitoring.createDowntimeMonitoring(function (downtimeMonitoringTmpl) {
                    for (var i = 1; i < tableRows.length; i++) {
                        var trElement = document.createElement('tr');
                        var tdElement = document.createElement('td');
                        var id = "I:" + rows[i - 1][2];
                        tdElement.id = templatePrefix + id;
                        tdElement.setAttribute("colspan", "3");
                        trElement.appendChild(tdElement);
                        $(tableRows[i]).addClass('sw-downtime-details-tr').after(trElement);

                        var bar = SW.Core.DowntimeMonitoring.initDowntimeMonitoring(id, tdElement, downTimeSettings, downtimeMonitoringTmpl);

                        downtimeBars[tdElement.id] = bar;

                        fillData(bar, templatePrefix, tdElement, options);

                        $element = $(".sw-downtime-control-div", tdElement);
                        $element.on("mouseenter", onMouseEnter).bind($element[0]);
                    }
                });
            }
        });

        createTooltip(function (tooltipTemplate) {
            var div = document.createElement('div');
            var customTable = document.getElementById(options.clientId);
            div.id = templatePrefix + "toolTipWrapper";
            customTable.appendChild(div);

            initTooltip(tooltipTemplate, $('#' + div.id), options.clientId);
            smallTooltip.wrapTooltip(templatePrefix + "downtimeSmallTooltipWrapper");
            bigTooltip.wrapTooltip(templatePrefix + "downtimeBigTooltipWrapper");
            options.startDateJObj = $('#' + templatePrefix + "downtimeStartTooltipDate");
            options.endDateJObj = $('#' + templatePrefix + "downtimeEndTooltipDate");
            options.startTimeJObj = $('#' + templatePrefix + "downtimeStartTooltipTime");
            options.endTimeJObj = $('#' + templatePrefix + "downtimeEndTooltipTime");
            options.timePerBlockJObj = $('#' + templatePrefix + "downtimeTimePerBlock");
            createTooltipMarkup(templatePrefix);
        });

        var refresh = function () { SW.Core.Resources.CustomQuery.refresh(options.scriptFriendlyResourceId); };

        SW.Core.View.AddOnRefresh(refresh, options.clientId);
        refresh();
    };

    obj.updateTooltip = function (tdElement) {
        var td = tdElement;
        var partId = td.attributes['data-partId'].value;

        var id = parseInt(td.attributes['data-partId'].value, 10);
        $host = $(td).parents("td[id][colspan=3]");
        var slicesNum = 3;

        var downtimeBar = downtimeBars[$host.attr("id")];


        if (downtimeBar.slices.length > 1) {
            // show always from positon 1 or to position last - 1 so we dont get the empty cells in tooltip
            if (id === downtimeBar.slices.length - 1) {
                id = downtimeBar.slices.length - 2;
            }
            else if (id === 0) {
                id = 1;
            }
        }

        var ids = getNeighborSlices(id, slicesNum - 1);

        var count = downtimeBar.slices.length;
        var startTooltipDate = downtimeBar.startDate;
        var endTooltipDate = downtimeBar.endDate;
        
        var totalSlices = _.reduce(downtimeBar.slices, function (memo, item) { return memo + item.length }, 0);
        var sliceSizeTicks = Math.abs((endTooltipDate.getTime() - startTooltipDate.getTime()) / totalSlices);
        var sliceSizeMinutes = Math.round(sliceSizeTicks / (60 * 1000));
        var timePartTicks = sliceSizeTicks * 5; // each part consists of 5 slides no matter what

        var minSliceSize = sliceSizeMinutes < 2 ? 2 : sliceSizeMinutes;

        var ticks = Date.parse(startTooltipDate);
        

        var startPartDate = startTooltipDate;
        var endPartDate = endTooltipDate;


        var partsList = [];

        if (count === 1) {
            partsList.length = 0;
            partsList.push(downtimeBar.slices[0]);
            setTooltipData(partsList, startPartDate, endPartDate, minSliceSize, templatePrefix, settings);
            return;
        }

        if (count > 2) {


            var quantity = id;
            var dif = (quantity < 0 ? 0 : quantity) * timePartTicks;
            var partDate = new Date(ticks + dif);
            var partTicks = Date.parse(partDate);
            startPartDate = new Date(partTicks - timePartTicks);
            endPartDate = new Date(partTicks + 2 * timePartTicks);

            if (endPartDate > endTooltipDate) {
                endPartDate = endTooltipDate;
            }
        }

        for (var i = 0; i < ids.length; i++) {
            if (downtimeBar.slices[ids[i]]) {
                partsList.push(downtimeBar.slices[ids[i]]);
            }
        };

        setTooltipData(partsList, startPartDate, endPartDate, minSliceSize, templatePrefix, settings);
    }

    function calculateTooltipWidth(element) {
        var partsArray = $(element).find('td.sw-downtime-part');
        var partsArraySize = partsArray.length;
        if (partsArraySize > 3)
            partsArraySize = 3;

        return partsArray[0].offsetWidth * partsArraySize;
    }

    function createTooltip(callback) {
		if (typeof obj.downtimeTooltipTemplate === 'undefined') {
			var clbk = function(data) {
				obj.downtimeTooltipTemplate = data;
				callback(data);
			}
			
			$.ajax({
				type: 'GET',
				url: '/Orion/templates/DowntimeMonitoring/downtimeTooltipTmpl.html',
				success: function (data) {
					clbk($(data));
				},
				async: false
			});
		} else {
			callback($(obj.downtimeTooltipTemplate));
		}
    }

    function initTooltip(tooltipTemplate, element, clientId) {
        var tooltipTmpl = tooltipTemplate.html();
        var tooltipControl = _.template(tooltipTmpl, { clientId: clientId });
        element.append(tooltipControl);
    }

    function createTooltipMarkup(templatePrefix) {
        var blockCount = 3;
        var slicePerBlockCount = 5;
        var fragment = document.createDocumentFragment();
        var sliceRow = document.getElementById(templatePrefix + 'downtimeSlicesRow');

        for (var i = 0; i < blockCount; i++) {
            var tdContainer = document.createElement('td');
            tdContainer.className = "sw-slice-block";

            var table = document.createElement('table');
            $(table).addClass("sw-slices-table").addClass(templatePrefix + "sw-slices-table");
            tdContainer.appendChild(table);

            var tbody = document.createElement('tbody');
            table.appendChild(tbody);

            var tr = document.createElement('tr');
            tbody.appendChild(tr);

            fragment.appendChild(tdContainer);

            for (var j = 0; j < slicePerBlockCount; j++) {
                var td = document.createElement('td');
                tr.appendChild(td);
            }
        }
        sliceRow.appendChild(fragment);
    };

    function fillData(downtimeBar, templatePrefix, element, options) {
        var timelines = $(element).find('.sw-downtime-timeline-table');

        timelines.mousemove(function (event) {
            var td = event.target;
            SW.Core.DowntimeMonitoring.ToolTip.ContentManager.updateTooltip(td);
        });

        timelines.mouseenter(function (event) {
            var td = event.target;
            SW.Core.DowntimeMonitoring.ToolTip.ContentManager.updateTooltip(td);
        });
    };

    // get slices that are neighbor to  given
    function getNeighborSlices(id, numOfNeighbor) {
        var intervalRange = numOfNeighbor / 2;
        var intervalFrom = id - intervalRange;
        var intervalTo = id + intervalRange;
        var ids = [];
        for (var i = intervalFrom; i < intervalTo + 1; i++) {
            if (i >= 0)
                ids.push(i);
        }
        return ids;
    };

    function setTooltipData(partsList, startPartDate, endPartDate, sliceSize, templatePrefix, options) {
        var sliceRows = $("." + templatePrefix + "sw-slices-table");

        // reset first
        for (var i = 0; i < sliceRows.length; i++) {
            var row = sliceRows[i].rows[0];
            for (var j = 0; j < row.children.length; j++) {
                var cell = row.children[j];
                cell.style.backgroundColor = "white";
            }
        };

        for (var i = 0; i < partsList.length; i++) {
            var row = sliceRows[i].rows[0];
            for (var j = 0; j < partsList[i].length; j++) {
                var cell = row.children[j];
                cell.style.backgroundColor = options.statusHelper.setInterfaceColors(partsList[i][j], options.statusList);
            }
        };

        options.startDateJObj.html($.datepicker.formatDate(options.regionalDateFormat.dateFormat, startPartDate));
        options.endDateJObj.html($.datepicker.formatDate(options.regionalDateFormat.dateFormat, endPartDate));
        options.startTimeJObj.html(options.statusHelper.formatTime(startPartDate, options.regionalDateFormat.timeSeparator));
        options.endTimeJObj.html(options.statusHelper.formatTime(endPartDate, options.regionalDateFormat.timeSeparator));
        options.timePerBlockJObj.html('= ' + sliceSize + ' ' + options.minRes);
    };
})(SW.Core.DowntimeMonitoring.ToolTip.ContentManager);