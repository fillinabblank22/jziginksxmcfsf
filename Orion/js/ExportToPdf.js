﻿(function () {

    var 
  exportID,
  textExportToPDF = '@{R=Core.Strings;K=LIBCODE_PCC_27;E=js}',
  textExportingToPDF = '@{R=Core.Strings;K=WEBJS_PCC_5;E=js}',
  textError = '@{R=Core.Strings;K=WEBJS_PCC_6;E=js}';

    /* i18n:exempt-start */
    // direct reference instead of through i18n.ashx will leave text instrumented.
    var i18nkey = "@{";
    if (!textExportToPDF.indexOf(i18nkey)) textExportToPDF = 'Export to PDF';
    if (!textExportingToPDF.indexOf(i18nkey)) textExportingToPDF = 'Exporting to PDF...';
    if (!textError.indexOf(i18nkey)) textError = 'There was an error trying to export the page to PDF, please try again later.\r\n\r\n';
    /* i18n:exempt-end */

    var exportEvents = {
        beforeExport: [],
        afterExport: []
    };

    var callAllEvents = function(eventArray) {
        $.each(eventArray, function (index, callback) {
            if (typeof (callback) !== "function")
                return;

            callback();
        });
    };

    var startExport = function () {
        startExportFromUrl(window.location);
    };

    var startExportFromUrl = function (pageUrl) {

        var link = $('#ExportToPdf');
        if (!link[0] || link.hasClass('busyExportToPdf')) return;

        callAllEvents(exportEvents.beforeExport);

	    //set or remove attribute checked in the page. ASP.NET checkbox often ommit it
        var checkboxes = $('input[type=checkbox]:enabled');
        for (var i = 0; i < checkboxes.length; i++) {
            if ($(checkboxes[i]).is(':checked')) {
                $(checkboxes[i]).attr('checked', 'checked');
            } else {
                $(checkboxes[i]).removeAttr('checked');
            }
        }

        //Calculate width  - to decide if we're going to use "calculated width" or "minimal width"
        var x = $("table.ResourceContainer")[0];
        var width;
        if (x) {
            try {
                width = $(x).width() + $(x).offset().left * 2;
            } catch (err) {
                width = $(x).width() + 86; //FB27713 - FF .offset() bug
            }
        } else {
            width = $("#content").width(); 
        }
        
        //Check is page is less than Winnovative A4 portrait vitrual device width (793x1122)
        if (width < 1325) {
            width = 1325;  //Use minimum width
        }

        $("#pageHeader").css("width", width);
        $("#footer").css("width", width);
        $("#content").css("width", width);

        var pageText = document.documentElement.innerHTML;
        $("#pageHeader").css("width", "");
        $("#footer").css("width", "");
        $("#content").css("width", "");

        // Re-fix FB27038. Remove the toolset integration javascript because it may block forever when the server tries to render this page.
        var toolsetIntPattern = new RegExp("<script src=\"http://localhost:17779/sw/toolset.*?</script>");
        pageText = pageText.replace(toolsetIntPattern, "");
        pageText = pageText.replace(/_pdfsig_4C5187C5165A4075878201F882534984_[\s\S]*?_4C5187C5165A4075878201F882534984_pdfsig_/g, '');

        exportID = Math.random();

        // Ensure the existing html classes are applied to the content when submitting.
        var htmlClass = (document.documentElement.className || '').replace(/[<>"]/g, ' ');

        var temp = "<!DOCTYPE html PUBLIC ' - W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\r\n";
        temp += "<html class='RenderPdf " + htmlClass + "' xmlns='http://www.w3.org/1999/xhtml'>\r\n" + pageText + "</HTML>";
        temp = '@{R=Live.Web;K=Core.AntiXsrfTokenInputKey}=' + SW.Core.Services.getXsrfTokenValue() + "&page=" + encodeURIComponent(pageUrl) + "&pageHTML=" + encodeURIComponent(temp) + "\r\n";


        var exportURL = "/Orion/ExportToPDF.aspx?ExportID=" + exportID + "&Width=" + width;
        exportURL += "&Title=" + document.title;

        $.ajax({
            url: exportURL,
            type: 'POST',
            data: temp
        });

        callAllEvents(exportEvents.afterExport);

        link.addClass('busyExportToPdf').html(textExportingToPDF);
        setTimeout(checkProgress, 2000);
    };

    var checkProgress = function () {
        var xsrfTokenValue = SW.Core.Services.getXsrfTokenValue();
        $.ajax({url: "/orion/ExportToPdf.aspx?ExportID=" + exportID + "&progresscheck=", type: 'POST', dataType: "text", data: "@{R=Live.Web;K=Core.AntiXsrfTokenInputKey}=" + xsrfTokenValue, success: function (data, textStatus, xmlhttprequest) {
            if (data == "NOTYET") {
                setTimeout(checkProgress, 2000);
            }
            else if (data == "DONE") {
                SW.Core.Services.postToTarget("/orion/ExportToPdf.aspx?ExportID=" + exportID + "&gimmethefile=", { '@{R=Live.Web;K=Core.AntiXsrfTokenInputKey}': xsrfTokenValue });
                $('#ExportToPdf').removeClass('busyExportToPdf').html(textExportToPDF);
            }
            else if (data.indexOf("ERROR") == 0) {
                $('#ExportToPdf').removeClass('busyExportToPdf').html(textExportToPDF);
                var errString = data.substr(5);
                if (errString.indexOf("Export to PDF is currently not supported on Windows 10 and Windows 2016.") > -1)
                {
                    textError = "";
                    errString = '@{R=Core.Strings;K=WEBJS_PS1_101;E=js}';
                }

                alert(textError + errString);
            }
        }
        });
    };

    window.ExportToPDF = startExport;
    window.ExportToPDFFromUrl = startExportFromUrl;
    window.ExportToPDFEvents = exportEvents;
})();

