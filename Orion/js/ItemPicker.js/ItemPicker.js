var pickers = SW.Core.namespace("SW.Core.Pickers");

pickers.ItemSelectedEventName = 'picker-item-selected';
pickers.SelectionMode = {
  Single: 'Single',
  Multiple : 'Multiple'
};

/* Object abstract is responsible for rendering Item Picker UI. 
 *
 * @param {Object} config 
 *  config.containerElementId {String} id of root picker element. 
 *  config.renderTo {String} id of the element where will be picker rendered.
 *  config.onCreated {Function} callback function which will be called when is resource picker created. To callback is passed instance of picker.
 *  config.selectedItemsRenderTo {String} id of the element where will be rendered selected resources.
 *  config.viewType {String} the type of view where is resource picker used. 
 *  config.selectionMode {String} determine how many items can be selected. Valid options are: Single | Multiple 
 *  config.groups.url {String} address of webmethod which provides data for group list.   
 *  config.categories.url {String} address of webmethod which provides data for categories list according to choosen group. 
 *  config.categories.render {Function} callback will be called when category record is rendered. Can be used for decorating result value in category grid. Signature render(value, metadata, record)  
 *  config.categories.onLoaded {Function} callback will be called when categories are loaded.
 *  config.categories.onRowSelected {Function} callback will be called when category is selected.
 *  config.items.url {String} address of webmethod which provides data for items list according to choosen group and category. 
 *  config.items.onLoaded {Function} callback will be called when items are loaded.
 *  config.requestParams {JSON object} object which properties will be passed to server in every data request. 
 *  config.items.onItemSelected {Function} callback will be called immediately when is item selected in grid. To callback is passed raw grid item record.
 *  config.items.onBeforeItemSelected {Function} 
 *  config.onGroupingMode  {Function} callback will be called when grouping mode is set. It means when is clicked on group or category.
 *  config.onSearchingMode {Function} callback will be called when searching mode is set. It means when is searched.
 *  config.onUICreated {Function} callback will be called when UI is already created
*/
SW.Core.Pickers.ItemPicker = function (config) {
    var defaults = {
        widths: {
            categoriesGrid: 200,
            itemsGridSingleMode: 660,
            itemsGridMultipleMode: 500
        },
        items: {
            readerFields: [
                { name: 'ItemID', mapping: 0 },
                { name: 'ItemCaption', mapping: 1 }
            ],
            columns: [
                { header: 'ItemID', hidden: true, hideable: false, dataIndex: 'ItemID' },
                { id: 'ItemCaption', header: config.items.headerName, width: '100%', sortable: true, menuDisabled: true, dataIndex: 'ItemCaption', renderer: renderTooltipForItem }
            ]
        },

        categories: {
            readerFields: [
                { name: 'CategoryID', mapping: 0 },
                { name: 'CategoryCaption', mapping: 1 },
                { name: 'CategoryTooltip', mapping: 2 }
            ]
        },

        groups: {
            readerFields: [
                { name: 'GroupID', mapping: 0 },
                { name: 'GroupCaption', mapping: 1 }
            ]
        }
    };

    config = jQuery.extend(true, defaults, config);

    var self = this,
        userPageSize = 10000000,
        $container = $('#' + config.containerElementId),
        prefix = $container.attr('id');

    //  DataStore objects
    var groupsDataStore, // serve for combobox group by
        categoriesDataStore, // serve for left panel GridPanel (list of group)
        itemsDataStore,
        favorDataStore = new ORION.WebServiceStore(
                "/Orion/Services/FieldPicker.asmx/GetObjectGroupProperties",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);; // favorite grouping datastore

    //  UI Data containers objects
    var groupsComboArray,
        favorComboArray,
        categoriesGrid,
        gridItems,
        groupByTopPanel,
        mainGridPanel;

    var itemsSelector, 
        selectedGroup = '',
        selectedCategory = '',
        selectedField = '',
        favoriteValue = '',
        netObjectUri = '',
        searchTerm,
        initSearch = false;
    // PUBLIC METHODS: 

    this.Render = function () {
        createAndInitializeUILayoutControls();
        mainGridPanel.render(config.renderTo);
    };

    this.Search = function (term) {
        selectedField = '';
        searchTerm = term;
        if (term == '') {
            selectedGroup = groupsComboArray.getValue();
            selectedCategory = categoriesGrid.getSelectionModel().getSelections()[0].data.CategoryID;
            refreshItemsGrid();
            return;
        }

        setToSearchingMode();
        refreshItemsGrid();
    };

    this.SetSearch = function (term) {
        selectedField = '';
        searchTerm = term;
        initSearch = true;
    };

    this.SearchLocally = function (term) {
        itemsDataStore.clearFilter();
        if (term) {
            itemsDataStore.filter('ItemCaption', term, true, false, false);
        }
    };

    this.IsSingleMode = function () {
        return config.selectionMode === pickers.SelectionMode.Single;
    };

    this.GetSelectedItems = function () {
        return itemsSelector.GetSelectedItems();
    };

    this.ClearSelections = function () {
        itemsSelector.ClearSelections();
    };

    this.ReloadItems = function () {
        refreshItemsGrid();
    };

    this.FieldTypeSelected = function (selField) {
        selectedField = selField;
        refreshItemsGrid();
    };

    this.FieldTypeSelectedRender = function (selField) {
        if(!initSearch)
        selectedField = selField;
    };

    this.SetNetobjectUri = function (uri, skipRefresh) {
        netObjectUri = uri;
        if (skipRefresh !== true)
            refreshItemsGrid();
    };

    this.Reload = function () {
        self.reloaded = true;
        refreshGroupsGrid();
    };

    this.Reset = function () {
        groupsDataStore.removeAll();
        categoriesDataStore.removeAll();
        itemsDataStore.removeAll();

        selectedGroup = '';
        selectedCategory = '';
        searchTerm = '';
        selectedField = '';
        favoriteValue = '';
        netObjectUri = '';

        setGetGroupsRequest();
        setGetCategoriesRequest();
        setGetItemsRequest();
    };

    this.GetItemsGrid = function () {
        return gridItems;
    };

    this.GetCategoriesGrid = function () {
        return categoriesGrid;
    };

    this.GetGroupByTopPanel = function() {
        return groupByTopPanel;
    };

    this.GetGroupsComboBox = function () {
        return groupsComboArray;
    };

    this.GetMainGridPanel = function () {
        return mainGridPanel;
    };

    this.ClearItemsGrid = function () {
        if (itemsDataStore != null) {
            itemsDataStore.removeAll();
        }
    };

    this.SetGroupValue = function(value) {
        groupsComboArray.setValue(value);
        selectedGroup = value;
    };

    this.GetGroupValue = function () {
        return selectedGroup;
    };

    this.GroupSelected = function(value) {
        groupSelected(value);
    };

    // PRIVATE METHODS:

    var createAndInitializeDataStores = function () {
        // Groups
        groupsDataStore = new ORION.WebServiceStore(
            config.groups.url,
            config.groups.readerFields,
            'GroupCaption'
        );

        refreshGroupsGrid();

        // Categories
        categoriesDataStore = new ORION.WebServiceStore(
            config.categories.url,
            config.categories.readerFields,
           'CategoryCaption'
        );

        setGetCategoriesRequest();

        // Items
        itemsDataStore = new ORION.WebServiceStore(
            config.items.url,
            config.items.readerFields,
            'ItemCaption'
        );
        setGetItemsRequest();
    };

    var execFunction = function (obj) {
        var cloned = _.clone(obj);
        for (var prop in cloned) {
            if (cloned.hasOwnProperty(prop))
                cloned[prop] = _.result(cloned, prop);
        }
        return cloned;
    };

    var setGetGroupsRequest = function () {
        var request = _.extend(execFunction(config.requestParams), {});

        groupsDataStore.proxy.conn.jsonData = {
            request: request
        };
    };

    var setGetCategoriesRequest = function () {
        var request = _.extend(execFunction(config.requestParams), {
            GroupID: selectedGroup || "",
            FavoriteID: favoriteValue || ""
        });

        categoriesDataStore.proxy.conn.jsonData = {
            request: request
        };
    };

    var setGetItemsRequest = function () {
        var request = _.extend(execFunction(config.requestParams), {
            SearchTerm: getSearchTerm() || "",
            GroupID: selectedGroup || "",
            CategoryID: selectedCategory || "",
            FieldType: selectedField || "",
            FavoriteID: favoriteValue || "",
            Uri: netObjectUri || ""
        });
        itemsDataStore.proxy.conn.jsonData = {
            request: request
        };
    };

    var groupSelected = function (selectedValue) {
        selectedGroup = selectedValue;
        selectedCategory = '';
        setToGroupingMode();

        if (getSearchTerm().length == 0) {
            refreshCategoriesGrid();
        } else {
            setToSearchingMode();
            initSearch = false;
            searchTerm = '';
        }
    };

    var favoriteSelected = function (selectedValue) {
        favoriteValue = selectedValue;
        selectedCategory = '';
        setToGroupingMode();
        refreshCategoriesGrid();

        if (selectedCategory != '') {
            if (selectedValue != null)
                refreshItemsGrid();
        }
    };

    var createAndInitializeDataUIControls = function () {
        groupsComboArray = new Ext.form.ComboBox(
        {
            id: prefix + '_groupBy_' + Ext.id(),
            fieldLabel: 'GroupID',
            displayField: 'GroupCaption',
            valueField: 'GroupID',
            store: groupsDataStore,
            mode: 'local',
            triggerAction: 'all',
            value: '@{R=Core.Strings;K=WEBJS_ZT0_10; E=js}',
            typeAhead: true,
            forceSelection: true,
            autoSelect: true,
            multiSelect: false,
            editable: false,
            allowBlank: false,
            width: 175,
            listeners: {
                'select': function (me) {
                    var value = me.store.data.items[me.selectedIndex].data.GroupID;
                    groupSelected(value);
                }
            }
        });

        groupsDataStore.on('load', function () {
            try {
                var value;
                if (selectedGroup != '') {
                    value = selectedGroup;
                } else {
                    value = groupsComboArray.store.getAt(0).get(groupsComboArray.valueField || groupsComboArray.displayField);
                }

                groupsComboArray.setValue(value);
                groupSelected(value);
            } catch (e) {

            }
        });

        favorComboArray = new Ext.form.ComboBox(
        {
            id: prefix + '_favor_' + Ext.id(),
            fieldLabel: 'Name',
            displayField: 'Name',
            valueField: 'Value',
            store: favorDataStore,
            mode: 'local',
            triggerAction: 'all',
            value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
            typeAhead: true,
            forceSelection: true,
            autoSelect: true,
            multiSelect: false,
            editable: false,
            allowBlank: false,
            width: 175,
            listeners: {
                'select': function(me) {
                    var value = me.store.data.items[me.selectedIndex].data.Value;
                    favoriteSelected(value);
                },
                'beforerender': function () {
                    favorDataStore.proxy.conn.jsonData = {};
                    favorDataStore.load();
                }
            }
        });

        favorDataStore.on('load', function () {
            try {
                var value = favorComboArray.store.getAt(0).get(favorComboArray.valueField || favorComboArray.displayField);
                favorComboArray.setValue(value);
                favoriteSelected(value);
            } catch (e) {

            }
        });

        categoriesGrid = new Ext.grid.GridPanel({
            id: prefix + '_categoriesGrid',
            store: categoriesDataStore,
            cls: 'hide-header',
            columns: [
                { editable: false, sortable: false, hidden: true, dataIndex: 'CategoryID', id: 'CategoryID' },
                { width: '100%', editable: false, sortable: false, dataIndex: 'CategoryCaption', renderer: renderCategory }
            ],
            selModel: new Ext.grid.RowSelectionModel(),
            autoScroll: true,
            loadMask: true,
            anchor: '0 0',
            viewConfig: { forceFit: true },
            split: false,
            autoExpandColumn: 'Value'
        });

        categoriesGrid.store.on('beforeload', function () { categoriesGrid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading'); });
        categoriesGrid.on('viewready', function () {
        });

        categoriesGrid.store.on('load', function () {
            if (jQuery.isFunction(config.categories.onLoaded)) {
                config.categories.onLoaded();
            }

            if (categoriesDataStore.totalLength > 0) {
                categoriesGrid.getSelectionModel().selectFirstRow();
            } else {
                gridItems.getStore().removeAll();
            }
            categoriesGrid.getEl().unmask();
        });

        categoriesGrid.getSelectionModel().on("rowselect", function (selMod, rowIndex, record) {
            var canceled = false;
            if (config.categories.onRowSelected && jQuery.isFunction(config.categories.onRowSelected)) {
                canceled = config.categories.onRowSelected(record);
            }

            if (!canceled) {
                selectedCategory = record.data["CategoryID"];
                setToGroupingMode();
                refreshItemsGrid();
                if (initSearch == true) {
                    setToSearchingMode();
                    initSearch = false;
                    searchTerm = '';
                }
            }
        });

        categoriesGrid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                msg: error ? error.Message : '@{R=Core.Strings;K=WEBJS_VB0_132;E=js}',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
            categoriesGrid.getEl().unmask();
        });

        var itemsSelectionModel = itemsSelector.GetSelectionModel();
        var gridItemsWidht = self.IsSingleMode() ? config.widths.itemsGridSingleMode : config.widths.itemsGridMultipleMode;

        gridItems = new Ext.grid.GridPanel({
            id: prefix + '_gridItems',
            region: 'center',
            viewConfig: { forceFit: true, emptyText: "@{R=Core.Strings;K=WEBJS_ZT0_7; E=js}" },
            store: itemsDataStore,
            baseParams: { start: 0, limit: userPageSize },
            stripeRows: true,
            trackMouseOver: false,
            columns: _.union([itemsSelectionModel], config.items.columns),
            sm: itemsSelectionModel,
            autoScroll: true,
            autoExpandColumn: 'ItemCaption',
            loadMask: true,
            width: gridItemsWidht,
            height: 540,
            //  bbar: pagingToolbar,
            enableHdMenu: false,
            enableColumnResize: true,
            enableColumnMove: false,
            cls: 'itemPickerItemsGrid'
        });

        gridItems.store.on('beforeload', function () { gridItems.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading'); });
        gridItems.store.on('load', function () {
            itemsSelector.UpdateGridAccordingToSelection();
            if (jQuery.isFunction(config.items.onLoaded)) {
                config.items.onLoaded();
            }
            gridItems.getEl().unmask();
        });

        gridItems.store.on("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                msg: error ? error.Message : '@{R=Core.Strings;K=WEBJS_VB0_132;E=js}',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
            gridItems.getEl().unmask();
        });
    };

    var createAndInitializeUILayoutControls = function () {

        // Here is creating layour controls
         groupByTopPanel = new Ext.Panel({
            region: 'centre',
            split: false,
            heigth: 50,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_ZT0_18; E=js}" }), groupsComboArray, new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), favorComboArray],
            cls: 'panel-no-border panel-bg-gradient'
        });

        var navPanel = new Ext.Panel({
            region: 'west',
            split: true,
            anchor: '0 0',
            width: config.widths.categoriesGrid,
            heigth: 500,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [groupByTopPanel, categoriesGrid],
            cls: 'panel-no-border'
        });

        navPanel.on("bodyresize", function () {
            categoriesGrid.setSize(navPanel.getSize().width, 450);
        });

        mainGridPanel = new Ext.Panel({
            region: 'center',
            split: true,
            layout: 'table',
            collapsible: false,
            items: [navPanel, gridItems]
        });

        if (config.onUICreated && jQuery.isFunction(config.onUICreated)) {
            config.onUICreated(this);
        }
    };

    function initilizeSelectionMode() {
        if (self.IsSingleMode()) {

            itemsSelector = new SW.Core.Pickers.PickerSingleItemSelector({
                onBeforeItemSelected: config.items.onBeforeItemSelected
            });

            // Hides containers for showing selected items.
            $container.find('.selectedItemsColumn').hide();

        } else { // Multiple selectoion mode by default.
            itemsSelector = new SW.Core.Pickers.PickerMultipleItemsSelector({
                selectedItemsRenderTo: config.selectedItemsRenderTo,
                onBeforeItemSelected: config.items.onBeforeItemSelected
            });
        }

        itemsSelector.on(SW.Core.Pickers.ItemSelectedEventName, onItemSelected);
    }

    function onItemSelected(itemRecord) {
        if (config.items.onItemSelected && jQuery.isFunction(config.items.onItemSelected)) {
            config.items.onItemSelected(itemRecord);
        }
    }

    function renderCategory(value, metadata, record) {
        var resultValue = renderTooltipForCategory(value, metadata, record);

        if (config.categories.render && jQuery.isFunction(config.categories.render)) {
            resultValue = config.categories.render(value, metadata, record);
        }

        return resultValue;
    }

    function renderTooltipForCategory(value, metadata, record) {
        var categoryTooltip = record.get('CategoryTooltip');
        if (categoryTooltip) {
            return '<div ext:qtitle="' + value + '" ext:qtip="' + categoryTooltip + '">' + value + '</div>';
        }
        return value;
    }

    function renderTooltipForItem(value, metadata, record) {
        var itemTooltip = record.get('ItemTooltip');
        if (itemTooltip) {
            return '<div ext:qtitle="' + value + '" ext:qtip="' + itemTooltip + '">' + value + '</div>';
        }
        return value;
    }

    var getSearchTerm = function () {
        return searchTerm || "";
    };

    var refreshGroupsGrid = function () {
        setGetGroupsRequest();
        groupsDataStore.load();
    };

    var refreshCategoriesGrid = function () {
        setGetCategoriesRequest();
        categoriesDataStore.load();
    };

    var refreshItemsGrid = function () {
        if (itemsDataStore != null) {
            setGetItemsRequest();
            itemsDataStore.load({ params: { start: 0, limit: userPageSize} });
        }
    };

    function setToGroupingMode() {

        if (initSearch == false)
            searchTerm = '';

        if (jQuery.isFunction(config.onGroupingMode)) {
            config.onGroupingMode();
        }
    }

    function setToSearchingMode() {
        if (initSearch == false) {
            selectedCategory = '';
            selectedGroup = '';
        }

        if (jQuery.isFunction(config.onSearchingMode)) {
            config.onSearchingMode();
        }
    }

    initilizeSelectionMode();
    createAndInitializeDataStores();
    createAndInitializeDataUIControls();

    Ext.QuickTips.init();

    Ext.apply(Ext.QuickTips.getQuickTip(), {
        showDelay: 0,
        trackMouse: false
    });
};
