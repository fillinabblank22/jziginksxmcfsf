﻿Ext.namespace("SW.Core.Pickers");

/* Object is responsible for manage selected resource in Resource Picker. 
 * @param {Object} config 
 */
SW.Core.Pickers.PickerSingleItemSelector = function(config) {
    var self = this;
    SW.Core.Observer.makeObserver(self);
    
    var ITEM_ID_KEY = 'ItemID',
        ITEM_CAPTION_KEY = 'ItemCaption';

    var selectedItem,
        selectorModel;

    // PRIVATE METHODS:

    function createSelectionModel() {
        selectorModel = new Ext.sw.grid.RadioSelectionModel();
        selectorModel.on("beforerowselect", beforeItemRowClick);
        selectorModel.on("rowselect", itemRowClick);
    }

    function beforeItemRowClick(selectionModel, rowIndex, keepExisting, record ) {
        var canceled = false;
        
        if (jQuery.isFunction(config.onBeforeItemSelected)) {
            canceled = !config.onBeforeItemSelected(_.clone(record.data));
        }

        selectionModel.selectionDisabled = canceled;
        return !canceled;       
    }

    function itemRowClick(selectionModel, rowIndex, record) {
        var itemId = record.data[ITEM_ID_KEY];
        var itemCaption = record.data[ITEM_CAPTION_KEY];  
        
        selectedItem = { itemID: itemId, itemCaption: itemCaption, data :_.clone(record.data) };

        self.trigger(SW.Core.Pickers.ItemSelectedEventName, _.clone(record.data));
    }

    // PUBLIC METHODS:

    this.GetSelectionModel = function() {
        if (!selectorModel) {
            createSelectionModel();
        }
        return selectorModel;
    };

    this.GetSelectedItems = function() {
        var items = [];
        if (selectedItem) {
            items.push(selectedItem);
        }
        return items;
    };
    
    this.UpdateGridAccordingToSelection = function() {
        if (!selectedItem) return;
       
        var store = selectorModel.grid.getStore();
        for (var index = 0, max = store.getCount(); index < max ; index++) {
            var record = store.getAt(index);
            if (record.data[ITEM_ID_KEY] === selectedItem.itemId) {
                selectorModel.selectRow(index, true, false);
                break;
            }          
        }
    };

    this.ClearSelections = function() {
        selectorModel.clearSelections();
        selectedItem = null;
    };

};