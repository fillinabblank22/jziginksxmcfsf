﻿Ext.namespace("SW.Core.Pickers");

/* Object is responsible for searching in resouces templates.
 *
 * @param {Object} config
 *  config.searchInput {String} id of the search input element.
 *  config.searchButtonHolder {String} id of the element where will be placed search button.
 *  config.viewType {String} the type of view where is resource picker used. 
 *  config.limit {Number} determines how many items will be shown in autocomplete.
 *  config.onSearched {Function} callback will be called on searchin. Will be passed search term to this callback.
 */
SW.Core.Pickers.ItemPickerSearcher = function (config) {

    var defaults = {
          autocomplete : {
              enabled  : true
          }
    };
    
    config = _.extend(defaults, config);

    var watermarkText = config.watermarkText,
        defaultLimit = 5,
        self = this,
        $searchInput = $(config.searchInput);

    createLocalizableSearchButton();
    createWatermarkTextbox();
    createAutocompleteTextbox();
    addSearchingOnEnterPressed();

    created();

    // PRIVATE METHODS:

    function createLocalizableSearchButton() {
        var searchButton = $(SW.Core.Widgets.Button('@{R=Core.Strings; K=WEBJS_ZT0_1; E=js}', {
            type: 'secondary'
        }));

        searchButton.appendTo(config.searchButtonHolder);
        searchButton.click(searched);
    }

    function createWatermarkTextbox() {
        var watermark = new SW.Core.Widgets.WatermarkTextbox(config.searchInput, watermarkText);
    }

    function createAutocompleteTextbox() {
        if (config.autocomplete.enabled) {
            var autocomplete = $searchInput.autocomplete({
                
                source: function(request, respond) {

                    var requestParams = jQuery.extend(true, config.customParams, {
                        Term: request.term,
                        Limit: config.limit || defaultLimit
                    });

                    SW.Core.Services.callWebService(
                        config.serviceName,
                        config.serviceMethodName,
                        { request: requestParams },
                        function(response) {
                            respond(response);
                        }
                    );
                },
                minLength: 3,
                select: function(event, ui) {
                    $searchInput.val(ui.item.value);
                    searched();
                }
            });
        }
    }

    function addSearchingOnEnterPressed() {
        var ENTER_KEY_CODE = 13;

        $searchInput.keyup(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            // Enter keycode
            if (code == ENTER_KEY_CODE) {
                searched();
            } else {
                keyPressed();
            }
        });
    }

    function getSearchTerm() {
        var searchTerm = $searchInput.val();
        
        if (watermarkText === searchTerm) {
            searchTerm = '';
        }

        return searchTerm;
    }

    function created() {
        if (config.onCreated && jQuery.isFunction(config.onCreated)) {
            config.onCreated(self);
        }
    }

    function searched() {
        var searchTerm = getSearchTerm();
       
        if (config.onSearched && jQuery.isFunction(config.onSearched)) {
            config.onSearched(searchTerm);
        }
    }
    
    function keyPressed() {
        var searchTerm = getSearchTerm();

        if (config.onKeyPressed && jQuery.isFunction(config.onKeyPressed)) {
            config.onKeyPressed(searchTerm);
        }
    }
};