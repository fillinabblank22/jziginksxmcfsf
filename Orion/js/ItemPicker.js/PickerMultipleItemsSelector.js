﻿SW.Core.namespace("SW.Core.Pickers");

/* Object is responsible for manage selected resources in Resource Picker. 
 * @param {Object} config 
 * config.selectedResourcesTo {String} id of the element where will be rendered selected resources.
 */
SW.Core.Pickers.PickerMultipleItemsSelector = function(config) {
    var self = this;
    SW.Core.Observer.makeObserver(self);


    var ITEM_ID_KEY = 'ItemID',
        ITEM_CAPTION_KEY = 'ItemCaption',
        ITEM_TOOLTIP_KEY = "ItemTooltip";
        
     var resourceItemTemplate = "" +
        "<td><div ext:qtitle='{{itemCaption}}' ext:qtip='{{itemTooltip}}'>{{itemCaption}}</div></td>" +
        "<td style='width:20px; cursor:pointer'><img data-item-id='{{itemID}}'  src='/Orion/images/Reports/delete_icon16x16.png'/></td>";


    var selectedItemsTable = $("#" + config.selectedItemsRenderTo + " table")[0],
        selectedItems = [], // { itemName: '', itemText: ''}
        selectedTr = [], // { itemName: '',  tr: refTr }
        $selectedItemsEmptyEl = $("#" + config.selectedItemsRenderTo).parents(".itemPicker").find(".selectedItemsEmpty"),
        selectorModel;

    // PRIVATE METHODS:

    function createSelectionModel() {
       selectorModel = new Ext.grid.CheckboxSelectionModel();
       selectorModel.on("beforerowselect", gridItemRowClick);
       selectorModel.on("rowdeselect", gridItemsRowDeselect);
    }

    function addItemToSelected (itemID, itemCaption, itemTooltip) {
        var tableBody = $("#" + config.selectedItemsRenderTo + " table > tbody")[0];

         var tr = document.createElement("tr");
        tableBody.appendChild(tr);

        var $tr = $(tr);
        var innerHtml = _.template(resourceItemTemplate, { itemID: itemID, itemCaption: itemCaption, itemTooltip: itemTooltip });
        $tr.html(innerHtml);

         $tr.find("[data-item-id]").on('click', function () {
            var id = $(this).data('item-id');
            removeItemFromSelected(id);
        });

        selectedTr.push({ itemID: itemID, refTr: tr });

    };

    // Cancel selection from grid and also remove row in right selection table
    function removeItemFromSelected(itemID, dontUpdateUiGrid) {
        var index = 0;
        var found = false;

        // removing from array described ui
        for (index = 0; index < selectedTr.length; index++) {
            if (selectedTr[index].itemID == itemID) {
                found = true;
                // Removes row from table tbody.
                selectedItemsTable.firstChild.removeChild(selectedTr[index].refTr);
                break;
            }
        }

        if (found) {
            selectedTr.splice(index, 1);
            selectedItems.splice(index, 1);
        }

        // updating grid ui
        if (!dontUpdateUiGrid) {
            var store = selectorModel.grid.getStore();
            for (index = 0; index < store.getCount(); index++) {
                var record = store.getAt(index);
                if (record.data[ITEM_ID_KEY] == itemID) {
                    selectorModel.deselectRow(index, false);
                    break;
                }
            }
        }

        showOrHideSelectedItemsPanelEmpty();
        
         self.trigger(SW.Core.Pickers.ItemSelectedEventName, null);
    };

    function gridItemsRowSelect(selMod, rowIndex, record) {
        // here will be adding records into array in the case that will not be found in selectedResources field
        var id = record.data[ITEM_ID_KEY];
        var caption = record.data[ITEM_CAPTION_KEY];
        var tooltip = record.data[ITEM_TOOLTIP_KEY] || '';
        var canAdd = true;
        for (var i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].itemID == id) {
                canAdd = false;
                break;
            }
        }

        if (canAdd) {
            addItemToSelected(id, caption, tooltip);
            selectedItems.push({ itemID: id, itemCaption: caption, data: _.clone(record.data) });
            self.trigger(SW.Core.Pickers.ItemSelectedEventName, _.clone(record.data));
        }

        showOrHideSelectedItemsPanelEmpty();
    }

    function showOrHideSelectedItemsPanelEmpty()
    {
        if (selectedItems.length > 0) {
            $selectedItemsEmptyEl.hide();
        } else {
             $selectedItemsEmptyEl.show(); 
        }
    }

    function gridItemsRowDeselect(selMod, rowIndex, record) {
        removeItemFromSelected(record.data[ITEM_ID_KEY], true);    
    }

    function gridItemRowClick(selectionModel, rowIndex, keepExistingSelection, record) {
        // Because we are calling selection/deselection rows manually we need suspend events for a while to avoid recursion error. 
        selectionModel.suspendEvents();
        var canceled = false;

        if (jQuery.isFunction(config.onBeforeItemSelected)) {
           canceled = !config.onBeforeItemSelected( _.clone(record.data) ); 
        }
        
       //selectionModel.selectionDisabled = canceled;

        if (!canceled) {
            var isRowSelected = selectionModel.isSelected(rowIndex);
            if (isRowSelected) {
                selectionModel.deselectRow(rowIndex);
                gridItemsRowDeselect(selectionModel, rowIndex, record);
            } else {
                selectionModel.selectRow(rowIndex, true);
                gridItemsRowSelect(selectionModel, rowIndex, record);
            }
        }
        
        selectionModel.resumeEvents();
 
        // Prevents default action on grid.
        return false;
    }

    // PUBLIC METHODS:

    this.GetSelectionModel = function() {
        if (!selectorModel) {
            createSelectionModel();
        }
        return selectorModel;
    };

    this.GetSelectedItems = function() {
        return selectedItems;
    };
    
    this.UpdateGridAccordingToSelection = function() {
        var store = selectorModel.grid.getStore();
        for (var index = 0; index < store.getCount(); index++) {
            var record = store.getAt(index);
            for (var i = 0; i < selectedItems.length; i++) {
                if (record.data[ITEM_ID_KEY] == selectedItems[i].itemID) {
                    selectorModel.selectRow(index, true, false);
                }
            }
        }
    };

    this.ClearSelections = function() {
        selectorModel.clearSelections();
        selectedItems = [];
       
        $("#" + config.selectedItemsRenderTo + " table > tbody").empty();
        showOrHideSelectedItemsPanelEmpty();
    };
};