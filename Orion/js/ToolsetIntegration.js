(function(Core){

var Toolset = Core.Toolset = Core.Toolset||{};

window.$SW = window.$SW||{}; // legacy.
$SW.TS = Toolset; // legacy.

 
// SolarWinds Toolset Cross Browser Integration v2
var 
    _clear = 'http://localhost:17779/sw/',
    _encrypted = 'https://localhost:17780/sw/',
    _site = document.location.protocol.toLowerCase() == "http:" ? _clear : _encrypted,
    _RawMenu, // menu data from toolset
    _RawLogic, // js logic from toolset
    _StylesLoaded,
    _forcingExtLoad,
    _CtxMnu;

$SW.XB = // toolset service hardcoded to jsonp with this prefix
Toolset.CallBack = function(obj){
  if( obj._t == 'sw/toolset/menu/render' )
  {
    _RawMenu = obj.obj;
    _RawLogic = obj.logic;
  }
};

var _MkTag = function( data, err ){
  var url = _site + 'toolset/' + data;
  if( !/&$/.test( data ) ) url += '&';
  url += 'cb=' + new Date().getTime();
  var e = document.createElement('script');
  e.src = url;
  e.type='text/javascript';
  e.onerror = err || function() {};
  return document.getElementsByTagName('head')[0].appendChild(e);
};

var _MkMnuItm = function(o,hostctx){
  var mnu = {};
  mnu.text = o.title;
  mnu.cls = 'sw_ts_menu';
  if( o.icon ) mnu.icon = _site + 'toolset/icons/' + encodeURIComponent(o.id);
  if( o.disabled ) mnu.disabled = true;

  if( o.children )
  {
    var items = [];

    Ext.each( o.children, function(c){
      if( !_RawLogic.MenuItemIsVisible(hostctx,c) )
        return;

      if(c.separator )
      {
        items.push('-');
      }
      else
      {
        items.push( _MkMnuItm(c,hostctx) );
      }
    });

    mnu.menu = items;
  }
  else
  {
    mnu.details = o;
    mnu.handler = function(mnu,e){
      _MkTag(
        'action/' + mnu.initialConfig.details.id +
        '?params=' + encodeURIComponent(hostctx.text) );
    };
  }

  return mnu;
};

var _MkMenu = function(e,elm){
  if( !_RawMenu )
  {
    return;
  }
  if((!_StylesLoaded) && (_forcingExtLoad))
  {
    var head = document.getElementsByTagName('head')[0];

    var elem = document.createElement('link');
    elem.rel = 'stylesheet';
    elem.type = 'text/css';
    elem.href = '/Orion/js/extjs/3.4/resources/css/ext-all-notheme.css.i18n.ashx?l=@{R=Core.LiveCss;K=Locale}&v=@{R=Core.LiveCss;K=WebVersion}&csd=@{R=Core.LiveCss;K=CssData}';
    head.appendChild(elem);

    elem = document.createElement('link');
    elem.rel = 'stylesheet';
    elem.type = 'text/css';
    elem.href = '/Orion/js/extjs/3.4/resources/css/xtheme-gray.css.i18n.ashx?l=@{R=Core.LiveCss;K=Locale}&v=@{R=Core.LiveCss;K=WebVersion}&csd=@{R=Core.LiveCss;K=CssData}';
    head.appendChild(elem);

    elem = document.createElement('link');
    elem.rel = 'stylesheet';
    elem.type = 'text/css';
    elem.href = '/Orion/styles/toolset/ToolsetIntegration.css.i18n.ashx?l=@{R=Core.LiveCss;K=Locale}&v=@{R=Core.LiveCss;K=WebVersion}&csd=@{R=Core.LiveCss;K=CssData}';
    head.appendChild(elem);

    _StylesLoaded = true;
  }

  while( elm && elm.tagName != 'A' )
    elm = elm.parentNode;

  var hostctx = _RawLogic.HostCtxMake(elm);
  if( !hostctx ) return;

  var old = $SW.ToolsetMenu, newid = 'toolsetmenu';
  if( old ) newid = old.id == 'toolsetmenu' ? 'toolsetmenu1' : 'toolsetmenu';

  var m = _MkMnuItm(_RawMenu, hostctx);
  m.id = newid;
  m.tshostctx = hostctx;
  m.items = m.menu;
  m.menu = undefined;
  $SW.ToolsetMenu = new Ext.menu.Menu(m);
  $SW.ToolsetMenuArg = elm;
  if( old ) old.destroy();
  $SW.ToolsetMenu.showAt(e.xy);
  e.preventDefault();
  e.stopEvent();
};

// Hook the right-click and integrate it with the client-side toolset integration server
Toolset.Integrate = function(){$(document).ready(function(){
  if(!window.Ext)
  {
    if(_forcingExtLoad) return;

    _forcingExtLoad = true;
    
    _ForceExtLoad();

    return;
  }

  // hook the entire document's contextmenu event
  if( !_CtxMnu ){
    Ext.fly(document).on({ 'contextmenu': _MkMenu });
    _CtxMnu = 1;
  }

  var e = document.createElement('script');
  e.src = _site + 'toolset/render?' + new Date().getTime();
  e.type='text/javascript';
  document.getElementsByTagName('head')[0].appendChild(e);
});};


var _ForceExtLoad = function(){
  var head = document.getElementsByTagName('head')[0];

  var elem = document.createElement('script');
  elem.type = 'text/javascript';
  elem.src = '/Orion/js/extjs/3.4/ext-all-plus-jquery_adapter.js.i18n.ashx?l=@{R=Core.LiveCss;K=Locale}&v=@{R=Core.LiveCss;K=WebVersion}';
  head.appendChild(elem);

  var cb = function() {
    if(window.Ext) { Toolset.Integrate(); }
    else { window.setTimeout(cb, 100); }
  };

  window.setTimeout(cb, 100);
};

window.DoAction = // legacy
Toolset.DoAction = function (strAction, strExtra) {
  var o = DoAction.caller;
  var target = null;
  $('a[onclick]').each(function(i,a){
    if(o === a.onclick){
      target = a;
    }
  });

  if(target == null) {
    $('img[onclick]').each(function(i,img){
      if(o === img.onclick){
        target = img;
      }
    });
  }

  if(target == null) return false;

  if( !_RawMenu)
  {
    return false;
  }

  var hostctx = _RawLogic.HostCtxMake(target);
  if(!hostctx) return false;

  _MkTag(
    'doaction?params=' + encodeURIComponent(hostctx.text) + 
    '&action=' + encodeURIComponent(strAction) +
    '&extra=' + encodeURIComponent(strExtra) );

  return false;
}

Toolset.Integrate();

// fix NCM 6.0 and previous
$(window).load(function () {
    try {
        var x = UltraWebTree_NodeCheckedDownloadConfig;
        if (x && !x['fixed'] && Array.prototype.remove) {
            delete Array.prototype.remove;
            if (Array.prototype.indexOf) { delete Array.prototype.indexOf; } // because of IE browser
        }
    }
    catch (ex) {
    }
});

})(SW.Core);