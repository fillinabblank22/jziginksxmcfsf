﻿var selectedStyle;
function SelectGauge(styleName) {
    var oldSelectedStyle = document.getElementById(selectedStyle);
    if (oldSelectedStyle != null) {
        oldSelectedStyle.className = "DeSelectedGauge";
    }
    var gaugesListSpan = document.getElementById("GaugesList");
    var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
    var gaugeSelect = gaugesSelect[0];
    for (var i = 0; i < gaugeSelect.options.length; i++) {
        if (gaugeSelect.options[i].value == styleName) {
            gaugeSelect.selectedIndex = i;
			var value = gaugeSelect.options[gaugeSelect.selectedIndex].value;
			SaveData('Style', value);
            break;
        }
    }
    var gaugeImageSpan = document.getElementById(styleName);
    gaugeImageSpan.className = "SelectedGauge";
    selectedStyle = gaugeImageSpan.id;
}

function SelectCurrentGauge() {
    var gaugesListSpan = document.getElementById("GaugesList");
	if (gaugesListSpan != null)
	{
	    var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
	    var gaugeSelect = gaugesSelect[0];
	    var value = gaugeSelect.options[gaugeSelect.selectedIndex].value;
	    SelectGauge(value);
		SaveData('Style', value);
	}
}

function SelectMe(element) {
    SelectStyle(element.getAttribute('stylename'));
}

function SelectCurrent(element) {
    SelectStyle(element.value)
}
    
function SelectStyle(styleName) {
    $(".gauge").css("border", "0px solid red");
    $('[stylename=\'' + styleName + '\']').css("border", "2px dashed red");

    var gaugesListSpan = document.getElementById("GaugesList");
    var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
    var gaugeSelect = gaugesSelect[0];
    for (var i = 0; i < gaugeSelect.options.length; i++) {
        if (gaugeSelect.options[i].value == styleName) {
            gaugeSelect.selectedIndex = i;
			var value = gaugeSelect.options[gaugeSelect.selectedIndex].value;
			SaveData('Style', value);
            break;
        }
    }
}

function CollapseAdvanced(collapse) {
    try {
        var advancedRows = document.getElementsByTagName("tr")
        var index
        var collapseButton = document.getElementById("advanced-collapse-button")
        var expandButton = document.getElementById("advanced-expand-button")

        for (index in advancedRows) {
            if (index != "length") {
                var row
                row = advancedRows[index]
                if (row.className == "advanced-collapse") {
                    if (collapse) 
                        row.style.display = "none"
                    else 
                        row.style.display = ""
                }
            }
        }
        if (collapse) {
            collapseButton.style.display = "none"
            expandButton.style.display = "block"
        }
        else {
            collapseButton.style.display = "block"
            expandButton.style.display = "none"
        }
    }
    catch (err) { }
}

function SaveData(key, value) {
    //to avoid script error onLoad page
} 

