﻿var pickers = SW.Core.namespace("SW.Core.Pickers");

SW.Core.Pickers.FieldPickerIndentationPresenter = function(config) {
/// <summary>Object responsible for rendering indentation in field picker.</summary>

    var paddingAmountInPixels = 8;

    this.RenderCategory = function (value, categoryRecordData) {
        var indentationLevel = categoryRecordData.get(pickers.INDENTION_LEVEL_KEY);
        var indentation = indentationLevel * paddingAmountInPixels;
        var image;  
        var indent = '';
		
        if (indentation > 0) {
            indent = String.format('<spans  ext:qtitle="" ext:qtip="{0}" >&nbsp;{0}</span>', value);
        }
        else
            indent = String.format('<span ext:qtitle="{0}" ext:qtip="{1}">{2}</span>',
				(categoryRecordData.data.CategoryTooltip.length > 0) ? value : '',
				(categoryRecordData.data.CategoryTooltip.length > 0) ? categoryRecordData.data.CategoryTooltip : (categoryRecordData.data.CategoryID == "favorites") ? categoryRecordData.data.CategoryCaption : value, value);
  
        if (indentationLevel >= 1) {
            indentation = (indentationLevel-1) * paddingAmountInPixels;
            image = String.format('<img  style="margin-top:-3px; margin-bottom:-3px; {0}" src="{1}"/>', indentationLevel > 1 ? 
				String.format('margin-left:{0}px;', indentation) : '', categoryRecordData.data.IsLastInLevel ? 
				"/Orion/images/Reports/TreeStructure_Icon_B_16x16.png" : "/Orion/images/Reports/TreeStructure_Icon_A_16x16.png");
            indent = image + String.format('<span ext:qtitle="" ext:qtip="{0}">&nbsp;{1}</span>', (categoryRecordData.data.CategoryID == "favorites") ? categoryRecordData.data.CategoryCaption : value, value);
        }
  
        return indent;
    };
};
