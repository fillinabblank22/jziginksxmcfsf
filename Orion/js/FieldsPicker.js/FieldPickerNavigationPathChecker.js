﻿var pickers = SW.Core.namespace("SW.Core.Pickers");

/// <summary>Object for applying and detecting limitation according to navigation path.</summary>
/// <summary>Limited navigation path causes cross joins in SQL.</summary>    
/// <param name="config" type="Object">Configuration object.</param>
/// <param name="config.fieldpicker" type="SW.Core.Pickers.FieldPicker">An instance of field picker.</param>
/// <param name="config.navigationPathFilterUp" type="String">The longest navigation path from already choosen fields (UP direction).</param>
/// <param name="config.navigationPathFilterDown" type="String">The longest navigation path from already choosen fields (DOWN direction).</param>    
pickers.FieldPickerNavigationPathChecker = function (config) {

    var NAVIGATION_PATH_LIMITATION_CSS_CLASS = 'disabledItem';
    var LIMITATION_APPLIED_KEY = '_limited_';

    // PRIVATE METHODS

    function isLimitationEnabled() {
        // Limitation is enabled when is navigation path filter set or field picker is in Multiple mode. 

        return !!config.navigationPathFilter
           || !config.fieldPicker.IsSingleMode();
    }

    function applyNavigationPathLimitationInGrid(grid, longestPath) {
        if (longestPath) {
            var gridView = grid.getView();

            // For each row/record in grid reads its navigation path and checks if is contained within longest path.
            _.each(grid.getStore().getRange(0), function (rec, index) {
                var navigationPath = rec.data[pickers.NAVIGATION_PATH_KEY];
                var longestPathToSingleCardinality = rec.data[pickers.LONGEST_PATH_TO_SINGLE_CARDINALITY_KEY];

                var isValid = true;

                if (navigationPath && longestPath) {
                    isValid = containsPath(navigationPath, longestPath) || containsPath(longestPath, navigationPath)
                        || containsPath(longestPathToSingleCardinality, longestPath) || containsPath(longestPath, longestPathToSingleCardinality);
                }

                var limited = !isValid;

                rec.data[LIMITATION_APPLIED_KEY] = limited;

                var row = gridView.getRow(index);
                setNavigationPathLimitationOnGridRow(limited, row);
            });
        }
    }

    function getLongestPaths(fields) {

        var fieldsLongestPath = '';
        var fieldsLongestNavigationPathLength = 0;

        // The longest path is calculated only from 1:N relantionships.
        // The longest path has the field with the most 1:N NavigationPathItems.
        _.each(fields, function (field) {

            var lastIndexOfManyCardinality = -1;
            var itemsWithManyCardinality = [];
            if (field.NavigationPathItems) {
                // find latest item where  max TargetCardinality is Many
                for (var i = 0; i < field.NavigationPathItems.length; i++) {
                    if (field.NavigationPathItems[i].TargetCardinality.Max === 2) {
                        itemsWithManyCardinality.push(field.NavigationPathItems[i]);
                        lastIndexOfManyCardinality = i;
                    }
                }
            }

            if (itemsWithManyCardinality.length > fieldsLongestNavigationPathLength) {

                var navigationPath = "";
                var sep = "";
                // build new navigation path to latest item with target cardinality equals to Many
                for (var i = 0; i <= lastIndexOfManyCardinality; i++) {
                    navigationPath += sep + field.NavigationPathItems[i].Name;
                    sep = ".";
                }

                fieldsLongestPath = navigationPath; 
                fieldsLongestNavigationPathLength = itemsWithManyCardinality.length;
            }
        });

        // computes longest navigation path from input filter and fields.
        var candidatesPaths = [];
        if (fieldsLongestPath) candidatesPaths.push(fieldsLongestPath);
        if (config.navigationPathFilter) candidatesPaths.push(config.navigationPathFilter);

        return computeLongestPath(candidatesPaths);
    }

    function computeLongestPath(pathsArray) {
        if (!pathsArray || pathsArray.length == 0) {
            return '';
        }
        // Compute longest navigation paths (up/down) from already selected fields.
        var actualLongestPath = pathsArray[0];
        var actualLongestPathLength = pathLength(actualLongestPath);

        for (var i = 1; i < pathsArray.length; i++) {
            var path = pathsArray[i];
            if (pathLength(path) > actualLongestPathLength) {
                actualLongestPath = path;
            }
        }

        return actualLongestPath;
    }

    function pathLength(path) {
            return path.split('.').length;
    }

    function setNavigationPathLimitationOnGridRow(limited, row) {
        $(row).toggleClass(NAVIGATION_PATH_LIMITATION_CSS_CLASS, limited)
            .find('input').attr('disabled', limited);
    }

    function containsPath(path1, path2) {
        return path1.indexOf(path2) === 0;
    }

    // PUBLIC METHODS

    this.isRecordLimited = function (itemRecordData) {
        // Item record from grid has to have limitation flag set to true
        // and there should be more fields selected or applied global filter.
        return itemRecordData
            && itemRecordData[LIMITATION_APPLIED_KEY]
            && isLimitationEnabled();
    };

    this.applyNavigationPathLimitation = function () {

        if (!isLimitationEnabled()) return;

        // Compute longest navigation paths (up/down) from selected fields.

        var allFields = _.reduce(config.fieldPicker.GetSelectedFields(), function (acc, fieldWrapper) {
            acc.push(fieldWrapper.Field);
            return acc;
        }, []);

        var longestPath = getLongestPaths(allFields);
        
        applyNavigationPathLimitationInGrid(config.fieldPicker.GetCategoriesGrid(), longestPath);
        applyNavigationPathLimitationInGrid(config.fieldPicker.GetItemsGrid(), longestPath);
    };
    
    this.getLongestPaths = function (fields) {
        return getLongestPaths(fields);
    };

};

