﻿var pickers = SW.Core.namespace("SW.Core.Pickers");

pickers.FieldPicker.FieldsCacheManager = (function() {
    var cache = [];

    function getFieldByRefID(fieldRefID) {
        return cache[fieldRefID];
    }

    function addFieldToCache(field) {
        cache[field.RefID.Data] = _.clone(field);
    }
    
    function addFieldsToCache(fields) {
        _.each(fields, function(field, index) {
            addFieldToCache(field);
        });
    }

    return {
      GetField : getFieldByRefID,
      AddField : addFieldToCache,
      AddFields: addFieldsToCache
    };

}());