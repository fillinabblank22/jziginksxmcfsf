﻿var pickers = SW.Core.namespace("SW.Core.Pickers");

// Enum is defined in SolarWinds.Reporting.Models.Data.DataDeclarationType and needs to be kept in sync.
pickers.DataDeclarationType = {
    NotSpecified: 0,
    DateTime: 1,
    Boolean: 2,
    Integer: 3,
    Float: 4,
    Text: 5,
    Char: 6,
    Uuid: 7,
    Enumerated: 8,
    CustomTransform: 9
};

// Enum is defined in SolarWinds.Reporting.Models.Selection.DataSourceType and needs to be kept in sync.
pickers.DataSourceType = pickers.DataSourceType || {
  Undefined: 0,
  Dynamic: 1,
  CustomSWQL: 2,
  CustomSQL: 3,
  Entities: 4
};