SW.Core.Pickers.FieldPicker.prototype.getTimestampForField = function (sourceField, onSuccess, onError) {
    /// <summary>Returns associated datetime/timestamp field for a given field.</summary>
    /// <param name="sourceField" type="JSON Field">The source field for which we wanna get related timestamp field.</param>
    /// <param name="onSuccess" type="Function">Callback function which will be called when timestamp field is received. Signature onSuccess(timestampField, sourceField).</param>
    /// <param name="onError" type="Function">Callback function will be called on error. Signature onError(errorMsg, sourceField).</param>

    var requestParam = {
        request: {
            SourceField: sourceField,
            DataSource: this.GetDataSource()
        }
    };

    SW.Core.Services.callWebService(SW.Core.Pickers.FIELD_PICKER_WEB_SERVICE_URL, "GetTimestampField", requestParam,
        // On success
        function (response) {
            if (response.HasField) {
                if (jQuery.isFunction(onSuccess)) {
                    onSuccess({
                        DisplayName: response.DisplayName,
                        Field: response.Field,
                        RefID: response.RefID
                    }, sourceField);
                }
            } else {
                if (jQuery.isFunction(onError)) {
                    onError('No related timestamp', sourceField);
                }
            }
        },
       // On error
       function (msg) {
           if (jQuery.isFunction(onError)) {
               onError(msg, sourceField);
           }
       });
};

SW.Core.Pickers.FieldPicker.getFields = function (dataSource, fieldsRefIDs, onSuccess, onError) {

    var requestParam = {
        request: {
            FieldsRefIDs: fieldsRefIDs,
            DataSource: dataSource
        }
    };

    SW.Core.Services.callWebService(SW.Core.Pickers.FIELD_PICKER_WEB_SERVICE_URL, "GetFields", requestParam,
        // On success
        function (response) {
            if ($.isFunction(onSuccess)) {
                onSuccess(response);
            }
        },
        // On error
        function (msg) {
            if ($.isFunction(onError)) {
                onError(msg);
            }
        });
};
