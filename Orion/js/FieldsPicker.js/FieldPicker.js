var pickers = SW.Core.namespace("SW.Core.Pickers");

// Constants
pickers.FIELD_PICKER_WEB_SERVICE_URL = '/Orion/Services/FieldPicker.asmx';
pickers.NAVIGATION_PATH_KEY = 'NavigationPath';
pickers.IS_NAVIGATION_PATH_DOWN_KEY = 'IsNavigationPathDown';
pickers.INDENTION_LEVEL_KEY =  'IndentationLevel';
pickers.LONGEST_PATH_TO_SINGLE_CARDINALITY_KEY = 'LongestPathToSingleCardinality';

/* Object is responsible for rendering Field Picker UI. 
 *
 * @param {Object} config 
 *  config.renderTo {String} id of the element where will be resource picker rendered.
 *  config.selectedItemsRenderTo {String} id of the element where will be rendered selected resources.
 *  config.selectionMode {String}  determine how many items can be selected. Valid options are: Single | Multiple.
 *  config.onCreated {Function} callback function which will be called when is resource picker created. To callback is passed instance of picker.
 *  config.onFieldsLoaded {Function} callback function which will be called when is items are loaded.
 *  config.onSelected {Function} callback function which will be called when filds are picked. To callback is passed instance of picker and customData.
 *  config.dataSourceJson {JSON} DataSource instance serialized to json object. Determines which fields will be visible in field picker. 
 *  config.navigationPathFilterFieldsJson {String []} The collection of field's ref ids. Will be used for computing the longest navigation path and limiting selection.
 */
pickers.FieldPicker = function (config) {

    // CONSTRUCTOR:

    var FIELD_DATAINFO_KEY = 'Field';
    var FIELD_PICKER_WEB_SERVICE_URL = config.dataProviderUrl ||  SW.Core.Pickers.FIELD_PICKER_WEB_SERVICE_URL;
    var GRID_HEADER_HEIGHT = 24;
    var LIMITATION_APPLIED_KEY = '_limited_';
    var fieldType = -1;
    var showFieldTypeDD = false;
    var currentNetObject = null;
    var container = $('#' + config.dialog.renderTo);
    var netOjectPickerContainer= $('#' + config.netObjectDialogId);
    var dataAggregationContainer = $('#' + config.dataAggregationRenderTo);
    var dataAggregationComboArray;

    //Controls
    var fieldTypeComboLabel,
        fieldTypeComboArray;

    //DataStore
    var fieldTypeDataStore;

    var defaults = {
        widths: {
            dialog: 900,
            categoriesGrid: 200
        }
    };

    container.find('#changeNetObjectSection').hide();

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function renderFavorite(value) {
        if (stringToBoolean(value == null ? false : value)) {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-full.png"/></a>');
        } else {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-empty.png"/></a>');
        }
    }

    function stringToBoolean(value) {
        switch (value.toString().toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    }

    function renderFavoriteText(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value ? '@{R=Core.Strings;K=WEBJS_SO0_72;E=js}' : '@{R=Core.Strings;K=WEBJS_SO0_73;E=js}';
    }

    function renderWithQtip(value) {
        if (value == null || value== '')
            return '';

    	// adding tooltip to display full cell value
        return String.format('<span style="width:100%; display: block;" title="{0}">{0}</span>', encodeHTML(value));
    }

    config = jQuery.extend(true, defaults, config);

    var self = this,
        rendered = false,
        dataTypeFilter = [],
        isStatisticFilter = null,
        dataUnitIdFilter = null,
        navigationPathFilter = '',
        dataSource = config.dataSourceJson,
        $dialogElement = $('#' + config.dialog.renderTo),
        indentationPresenter = new pickers.FieldPickerIndentationPresenter(),
        unrelatedDataExceptionPanel;

    // Creating subconfigs for the ItemPicker base class. 
    var customRequestParams = {
        DataTypeFilter: function () {
            return dataTypeFilter;
        },
        IsStatisticFilter: function () {
            return isStatisticFilter;
        },
        ApplicationTypeFilter: function () {
            return dataUnitIdFilter;
        },
        DataSource: function () {
            return dataSource;
        },
        ExtraRequestParams: function() {
            return config.extraRequestParams;
        }
    };

    //Field Type DataStore
    fieldTypeDataStore = new Ext.data.SimpleStore({
        fields: ['Name', 'Value'],
        data: [
                ['@{R=Core.Strings;K=WEBJS_AY0_46; E=js}', 'Property'],
                ['@{R=Core.Strings;K=WEBJS_AY0_47; E=js}', 'Event']
              ]
    });

    function updateDefaultNetObjectIfNeed(callback) {
            if (!currentNetObject || currentNetObject.entity != self.GetGroupValue()) {
                ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "LoadTopOneEntityByEntityType", { entityType: self.GetGroupValue()}, function(result) {
                    currentNetObject = {id: result.ID, entity:result.ItemType, uri: result.Uri, status: result.Status, fullName: result.FullName };
                    self.SetNetobjectUri(result.Uri, true);
                    renderCurrentNetObject(currentNetObject);
                });
            }
            if ($.isFunction(callback)) {
                callback();
            }
    }

    function GetSelectedGroup() {
        var group = self.GetGroupsComboBox();
        var selected = group.selectedIndex;
        if (selected == -1)
            selected = 0;
        return group.store.data.items[selected].data.GroupID;
    }

    function IsCurrentEntityManaged() {
        var group = self.GetGroupsComboBox();
        var selected = group.selectedIndex;
        if (selected == -1)
            selected = 0;
        return group.store.data.items[selected].data.IsManaged;
    }

    function showNetObjectPicker() {
        var value = self.GetGroupValue();
        SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: value }], value, [currentNetObject.uri], 'Single');
        $('#netObjectPickerPlaceHolder').prependTo(netOjectPickerContainer);
        netOjectPickerContainer.dialog({
            position: 'center',
            width: 700,
            height: 'auto',
            modal: true,
            draggable: true,
            title: '@{R=Core.Strings.2;K=WEBJS_IT0_3; E=js}',
            show: { duration: 0 }
        });
    }

    function renderCurrentNetObject(netOb) {
        var htmlNetObject = String.format('<img style="vertical-align:middle;" src="/Orion/StatusIcon.ashx?entity={2}&amp;status={0}&amp;size=small&EntityUri={3}"/><span>{1}</span>', netOb.status, netOb.fullName, self.GetGroupValue(), escape(netOb.uri != null ? netOb.uri : ""));
        container.find('#currentNetObjectPlaceHolder').html(htmlNetObject);
    }

    function changeNetObject() {
        var selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntitiesDefinitions();
         if (selectedEntities.length > 0) {
            currentNetObject = selectedEntities[0];
            renderCurrentNetObject(currentNetObject);
             self.SetNetobjectUri(currentNetObject.uri);
        }
    }

    var groupsConfig = {
        url: FIELD_PICKER_WEB_SERVICE_URL + '/GetGroups',
        readerFields: [
           { name: 'GroupID', mapping: 0 },
           { name: 'GroupCaption', mapping: 1 },
           { name: pickers.NAVIGATION_PATH_KEY, mapping: 2 },
           { name: 'IsManaged', mapping: 3 }
        ]
    };

    var categoriesConfig = {
        url: FIELD_PICKER_WEB_SERVICE_URL + '/GetCategories',
        render: renderCategory,
        readerFields: [
            { name: 'CategoryID', mapping: 0 },
            { name: 'CategoryCaption', mapping: 1 },
            { name: 'CategoryTooltip', mapping: 2 },
            { name: pickers.NAVIGATION_PATH_KEY, mapping: 3 },
            { name: pickers.INDENTION_LEVEL_KEY, mapping: 4 },
            { name: pickers.LONGEST_PATH_TO_SINGLE_CARDINALITY_KEY, mapping: 5 },
            { name: 'IsLastInLevel', mapping: 6}
        ],
        onRowSelected: categoryRowSelected,
        onLoaded: applyNavigationPathLimitation
    };

    var itemsConfig = {
        url: FIELD_PICKER_WEB_SERVICE_URL + '/GetColumns',
        headerName: config.fieldsGridHeaderText,
        readerFields: [
            { name: 'ItemID', mapping: 0 },
            { name: 'ItemCaption', mapping: 1 },
            { name: FIELD_DATAINFO_KEY, mapping: 2 },
            { name: 'ItemTooltip', mapping: 3 },
            { name: pickers.NAVIGATION_PATH_KEY, mapping: 4 },
            { name: pickers.LONGEST_PATH_TO_SINGLE_CARDINALITY_KEY, mapping: 5 },
            { name: 'Description', mapping: 6 },
            { name: 'Units', mapping: 7 },
            { name: 'IsFavorite', mapping: 8 },
            { name: 'PreviewValue', mapping: 9 }
        ],
        columns: [
             { header: 'ItemID', hidden: true, hideable: false, dataIndex: 'ItemID' },
             { id: 'isFavorite', header: '<img src="/Orion/images/Reports/Star-empty.png"/>', width: 28, sortable: true, dataIndex: 'IsFavorite', renderer: renderFavorite },
             { id: 'ItemCaption', header: config.fieldsGridHeaderText, width: 400, sortable: true, menuDisabled: true, dataIndex: 'ItemCaption', renderer: renderWithQtip },
             { id: 'PreviewValue', header: '@{R=Core.Strings.2;K=WEBJS_AY0_7; E=js}', width: 200, sortable: true, menuDisabled: true, dataIndex: 'PreviewValue', renderer: renderWithQtip },
             { id: 'Description', header: '@{R=Core.Strings;K=WEBJS_SO0_23; E=js}', hidden: true, width: 200, sortable: true, menuDisabled: true, dataIndex: 'Description', renderer: renderWithQtip },
             { id: 'ItemTooltip', header: '@{R=Core.Strings;K=WEBJS_ZT0_17; E=js}', hidden: true, width: 400, sortable: true, menuDisabled: true, dataIndex: 'ItemTooltip' }
        ],
        onLoaded: function() { applyNavigationPathLimitation();
            enableOrDisableDataAggregationSelection();
            if (jQuery.isFunction(config.onFieldsLoaded))
                config.onFieldsLoaded();
        },
        onBeforeItemSelected: function (itemRecord) { return enableOrDisableSelectionAccordingToPathLimitation(itemRecord); },
        onItemSelected: function (itemRecord) { if(self.IsSingleMode())enableOrDisableDataAggregationSelection(itemRecord);
            applyNavigationPathLimitation(itemRecord);
        }
    };

    // Creating config for the ItemPicker base class. 
    config = jQuery.extend(true, config, {
        requestParams: customRequestParams,
        groups: groupsConfig,
        categories: categoriesConfig,
        items: itemsConfig,
        onGroupingMode: function () { 
            setCategoryColumnVisibility(false);
            if (stringToBoolean(config.useNetOjectPicker)) {
                updateDefaultNetObjectIfNeed();
            }
        }, // Hide category column in items grid.
        onSearchingMode: function () { setCategoryColumnVisibility(true); }, // Show category column in items grid in searchin mode.
        onUICreated: uiCreated
    });

    // Inheritance based on renting constructor principle. 
    SW.Core.Pickers.ItemPicker.call(this, config);

    initDialogButtons();

    initializeNavigationPathFilter();

    if (stringToBoolean(config.useDataAggregation) && self.IsSingleMode())
        renderDataAggregationSection();

    // PRIVATE METHODS:

    function limitationEnabled() {
        return !config.disableNavigationPathLimitation;
    }

    function renderFieldTypeComboArray(gridItems) {
        var groupPanel = self.GetGroupByTopPanel();
        
        fieldTypeComboLabel = new Ext.form.Label(
        {
            text: "@{R=Core.Strings;K=WEBJS_IB0_11; E=js}",
        });

        fieldTypeComboArray = new Ext.form.ComboBox(
        {
            id: self.prefix + '_fieldType_' + Ext.id(),
            fieldLabel: 'Value',
            displayField: 'Name',
            valueField: 'Value',
            store: fieldTypeDataStore,
            mode: 'local',
            triggerAction: 'all',
            typeAhead: true,
            forceSelection: true,
            autoSelect: true,
            multiSelect: false,
            editable: false,
            allowBlank: false,
            width: 175,
            listeners: {
                'select': function(me) {
                    var value = me.store.data.items[me.selectedIndex].data.Value;
                    self.FieldTypeSelected(value);
                },
                'afterrender': function(me) {
                    try {
                        var value = me.store.getAt(fieldType).get(me.valueField || me.displayField);
                        me.setValue(value);
                        self.FieldTypeSelectedRender(value);
                    } catch (e) {
                    }
                }
            }
        });

        groupPanel.insert(0, fieldTypeComboLabel);
        groupPanel.insert(1, fieldTypeComboArray);

        gridItems.height = gridItems.height + 25;
    }

    function uiCreated(e) {
        var gridItems = self.GetItemsGrid();
        var mainPanel = self.GetMainGridPanel();

        if (showFieldTypeDD === true) {
            renderFieldTypeComboArray(gridItems);
        } 

        unrelatedDataExceptionPanel = new Ext.Panel({
            region: 'west',
            layout: 'vbox',
            split: true,
            anchor: '0 0',
            collapsible: false,
            height: gridItems.height - GRID_HEADER_HEIGHT,
            width: gridItems.width,
            items: [{
                        xtype: 'label',
                        text: '@{R=Core.Strings; K=WEBJS_LK0_2; E=js}',
                        margins: '5 0 30 10',
                        width: gridItems.width - 10,
                        style: 'font-weight:bold;'
                    },
                    {
                        xtype: 'label',
                        html: '@{R=Core.Strings; K=WEBJS_LK0_3; E=js}',
                        width: gridItems.width - 10,
                        margins: '0 0 0 10'
                    }],
            cls: 'panel-no-border',
            hidden: true
        });

        var itemsPanel = new Ext.Panel({
            region: 'west',
            split: true,
            anchor: '0 0',
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [gridItems, unrelatedDataExceptionPanel],
            cls: 'panel-no-border'
        });

        mainPanel.remove(gridItems);
        mainPanel.add(itemsPanel);

        gridItems.addListener('cellmousedown', function (grid, rowIndex, columnIndex, e) {
            var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
            if (fieldName == 'IsFavorite') {
                var record = grid.getStore().getAt(rowIndex); // Get the Record
                var data = record.get(fieldName);
                var f = JSON.parse(record.get('Field'));
                if ($("#isOrionDemoMode").length == 0) {
                    ORION.callWebService("/Orion/Services/FieldPicker.asmx",
                                        "ChangeFavoriteState", { entity: f.RefID.EntityName, property: f.RefID.PropertyName, value: stringToBoolean(data) }, function (result) {
                                            if (result) {
                                                record.set(fieldName, !stringToBoolean(data));
                                                record.commit();
                                            } else {
                                                // TODO: show message that value wasn't set
                                            }
                                        });
                }
                else {
                    // if it's demo mode than just change image
                    record.set(fieldName, !stringToBoolean(data));
                    record.commit();
                }
                return false;
            }
        });
    }

    function categoryRowSelected(itemRecord) {
        var gridItems = self.GetItemsGrid();

        if (stringToBoolean(config.useNetOjectPicker)) {
            var selectedGroup = GetSelectedGroup();
            var categoryId = itemRecord.data.CategoryID.split("||");

            if (IsCurrentEntityManaged() == false || categoryId[0] != selectedGroup) {
                container.find('#changeNetObjectSection').hide();
            } else if (IsCurrentEntityManaged() == true) { 
                container.find('#changeNetObjectSection').show();
            }
        }

        if (limitationEnabled() && itemRecord.data[LIMITATION_APPLIED_KEY] == true) {
            self.ClearItemsGrid();
            gridItems.setHeight(GRID_HEADER_HEIGHT);
            unrelatedDataExceptionPanel.show();
            return true;
        }
        unrelatedDataExceptionPanel.hide();
        gridItems.setHeight(unrelatedDataExceptionPanel.height + GRID_HEADER_HEIGHT);
        return false;
    }

    function initDialogButtons() {
        $(config.dialog.btnOk).click(selected);
        $(config.dialog.btnCancel).click(hideDialog);
        //netObjectPicker init buttons
        if (stringToBoolean(config.useNetOjectPicker)) {
            container.find('.objectLink').click(function(e) {
                e.preventDefault();
                updateDefaultNetObjectIfNeed(showNetObjectPicker);
            });

            container.find('.objectChange').click(function(e) {
                e.preventDefault();
                changeNetObject();
                netOjectPickerContainer.dialog('close');
            });

            container.find('.objectCancel').click(function(e) {
                e.preventDefault();
                netOjectPickerContainer.dialog('close');
            });
        }

        toggleDialogButtonsVisibility();
    }

    function toggleDialogButtonsVisibility() {
        if (dataSource === null) {
            $(config.dialog.btnOk).hide();
        } else {
            $(config.dialog.btnOk).show();
        }
    }

    function selected() {
        if (jQuery.isFunction(config.onSelected)) {
            config.onSelected(self, self.customData || {});
        }

        hideDialog();
    }

    function hideDialog() {
        resetSelctedGroup();
        $dialogElement.dialog('close');
    }

    function created() {
        if (config.onCreated && jQuery.isFunction(config.onCreated)) {
            config.onCreated(self);
        }
    }

    //Fix for FB374423 second part
    function resetSelctedGroup() {
        var group = self.GetGroupsComboBox();
        if (group != null) {
            group.selectedIndex = -1;
        }
    }

    function resetSearchTerm() {
        self.SetSearch('');
        $('.searchInput').val('');
    }

    function setCategoryColumnVisibility(value) {
        var itemsGrid = self.GetItemsGrid();

        itemsGrid.getColumnModel().setHidden(6, !value);
    }

    function applyNavigationPathLimitation(selectedItemRecord) {
        if (limitationEnabled()) {
            try {
                var checker = new pickers.FieldPickerNavigationPathChecker({
                    fieldPicker: self,
                    navigationPathFilter: navigationPathFilter
                });

                // Applies navigation path limitation on actual selection.
                checker.applyNavigationPathLimitation();

            } catch (e) {
                // Limitation can't break nothing in field picker.
            }
        }
    }

    function enableOrDisableSelectionAccordingToPathLimitation(itemRecord) {
        if (limitationEnabled()) {
            try {
                var checker = new pickers.FieldPickerNavigationPathChecker({
                    fieldPicker: self,
                    navigationPathFilter: navigationPathFilter
                });

                if (itemRecord) {
                    var isLimited = checker.isRecordLimited(itemRecord);

                    if (isLimited) {
                        Ext.Msg.show({
                            title: '@{R=Core.Strings;K=WEBJS_ZT0_15; E=js}',
                            msg: '@{R=Core.Strings;K=WEBJS_ZT0_16; E=js}',
                            width: 300,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }

                    return !isLimited;
                }

            } catch (e) {
                // Limitation can't break nothing in field picker.
            }
        }

        // If any error we allow selection. 
        return true;
    }

    function renderCategory(value, metadata, record) {

        if (record.data.CategoryID == "favorites") {
            value = "<img src = \"/Orion/images/Reports/Star-small.png\" style=\" padding-right:3px;\" >" + value;
        }

        try {
            if (isIndentationEnabled()) {
                return indentationPresenter.RenderCategory(value, record);
            }
        } catch (e) {
            // Original value is returned if something goes wrong.
        }

        return value;
    }

    function isIndentationEnabled() {
        // Indentation is enabled only for NON-Custom query datasources and only if is not applied statisticals filter.
        return (dataSource.Type == pickers.DataSourceType.Entities || dataSource.Type == pickers.DataSourceType.Dynamic);
    }

    function initializeNavigationPathFilter() {
        if (config.navigationPathFilterFieldsJson && config.navigationPathFilterFieldsJson.length > 0) {
            computeNavigationPath(config.navigationPathFilterFieldsJson);
        }
    }

    function computeNavigationPath(fieldIDs, currentFieldID, afterComputed) {
        var cache = SW.Core.Pickers.FieldPicker.FieldsCacheManager;
        var cachedFields = [];
        var uncachedFieldIDs = [];

        // Divide field's ids into two collections according to if field is in cache or is not.
        _.each(fieldIDs, function (fieldID) {
            if (fieldID === currentFieldID) return;

            var field = cache.GetField(fieldID);
            if (field) {
                cachedFields.push(field);
            } else {
                uncachedFieldIDs.push(fieldID);
            }
        });

        // Callback will be called synchronously (all field's were in cache ) or asynchronously (fields have to be downloaded first).
        var onFieldsReadyCallback = function (computedFields) {
            cache.AddFields(computedFields);

            var allFields = _.union(computedFields, cachedFields);

            var checker = new pickers.FieldPickerNavigationPathChecker({
                fieldPicker: self
            });

            // Sets navigation path filter for limit unrelated fields.
            navigationPathFilter = checker.getLongestPaths(allFields);

            if ($.isFunction(afterComputed)) {
                afterComputed();
            }
        };

        if (uncachedFieldIDs.length === 0) {
            onFieldsReadyCallback([]);
        } else {
            SW.Core.Pickers.FieldPicker.getFields(dataSource, uncachedFieldIDs, onFieldsReadyCallback, afterComputed);
        }
    }

    function renderDataAggregationSection() {
        var dataAggregationStore = new Ext.data.SimpleStore({
            fields: ['Aggregate', 'DisplayName']
        });

        var dataAggregationComboLabel = new Ext.form.Label(
        {
            text: '@{R=Core.Strings.2;K=WEBJS_VL0_9; E=js}',
        });

        dataAggregationComboArray = new Ext.form.ComboBox(
        {
            fieldLabel: 'Aggregate',
            displayField: 'DisplayName',
            valueField: 'Aggregate',
            store: dataAggregationStore,
            mode: 'local',
            triggerAction: 'all',
            typeAhead: true,
            forceSelection: true,
            autoSelect: true,
            multiSelect: false,
            editable: false,
            allowBlank: false
        });

        var panel = new Ext.Panel({
            region: 'center',
            split: true,
            anchor: '0 0',
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [dataAggregationComboLabel, dataAggregationComboArray],
            cls: 'panel-no-border'
        });
        panel.render(config.dataAggregationRenderTo);
    }

    var updateDataAggregations = function(items) {
        var data = [];
        $.each(items, function() {
            data.push([this.AggregateValue, this.DisplayName]);
        });
        dataAggregationComboArray.store.loadData(data);
    };

    var enableOrDisableDataAggregationSelection = function(itemRecord) {
        if (stringToBoolean(config.useDataAggregation)) {
            if (itemRecord === undefined) {
                dataAggregationContainer.hide();
                return;
            }

            var field = JSON.parse(itemRecord[FIELD_DATAINFO_KEY]);
            var typeInfo = field.DataTypeInfo;
            if (typeInfo.SupportedAggregations && typeInfo.SupportedAggregations.length > 0) {
                dataAggregationContainer.show();
                updateDataAggregations(typeInfo.SupportedAggregations);
                dataAggregationComboArray.setValue(typeInfo.DefaultAggregation);
            } else {
                dataAggregationContainer.hide();
            };
        };
    };

    // PUBLIC METHODS

    this.SetSearchTerm = function(searchTerm) {
       // Set searchTerm into search input textBox
         $('.searchInput').focus();
         $('.searchInput').val(searchTerm);
         $('.searchInput').css('color','black');

        self.SetSearch(searchTerm);

        // 1. Call function from ItemPicker for search. 
        // for correct work of this approach need add some flag or additional logic which will be correct work with searchTerm resetting  (ItemPicker line: 590)
        //
    };

    this.SetSerchingMode = function() {
        setCategoryColumnVisibility(true);
    };

    this.ShowInDialog = function (customData) {
        /// <summary>Render picker and show it in dialog.</summary>
        /// <param name="customData" type="JSON">Custom data which will be passed to onSelected callback.</param>

        self.customData = customData;

        if (!rendered) {
            self.Render();
            rendered = true;
        } else {
            if (!self.reloaded)
                self.ReloadItems();
        }

        if (stringToBoolean(config.useNetOjectPicker)) {
            updateDefaultNetObjectIfNeed();
            if (IsCurrentEntityManaged() == true) {
                container.find('#changeNetObjectSection').show();
            } else {
                container.find('#changeNetObjectSection').hide();
            }

        }

        if (stringToBoolean(config.useDataAggregation))
            dataAggregationContainer.hide();

        $dialogElement.parent().removeClass("itemPickerOffPage");
        $dialogElement.dialog({
            width: config.widths.dialog,
            height: 'auto',
            title: config.dialog.title,
            modal: true,
            close: function () {
                self.ClearSelections();
                resetSelctedGroup();
                resetSearchTerm();
            },
            disableAutoFocus: true
        });
    };

    this.SetIsStatisticFilter = function (value) {
        /// <summary>Set statistics filter on fields.</summary>
        /// <param name="value" type="BOOL?"></param>
        isStatisticFilter = value;
    };

    this.GetIsStatisticFilter = function () {
        return isStatisticFilter;
    };

    this.SetDataTypeFilter = function (value) {
        /// <summary>Set datatype filter on fields.</summary>
        /// <param name="value" type="SW.Core.Pickers.DataDeclarationType"></param>
        dataTypeFilter = [value];
    };

    /// <summary>Set datatypes filters on fields.</summary>
    /// <param Name="param1, param2, ..." type="SW.Core.Pickers.DataDeclarationType"></param>
    this.SetDataTypesFilters = function () {
        dataTypeFilter = Array.prototype.slice.call(arguments); ;
    };

    this.GetDataTypeFilter = function () {
        return dataTypeFilter;
    };

    this.GetDataSource = function () {
        return dataSource;
    };

    this.SetDataSource = function (value) {
        if (value && dataSource) {
            if (value.MasterEntity != dataSource.MasterEntity) {
                self.Reset();
            }
        }
        dataSource = value;
        toggleDialogButtonsVisibility();
    };

    this.SetDataUnitIdFilter = function (value) {
        dataUnitIdFilter = value;
    };

    this.GetDataUnitIdFilter = function () {
        return dataUnitIdFilter;
    };

    this.SetOnSelected = function (callback) {
        config.onSelected = callback;
    };

    this.SetOnFieldsLoaded = function(callback) {
        config.onFieldsLoaded = callback;
    };

    this.SetFieldTypeValue = function (value, showFieldTypes) {
        fieldType = value;
        showFieldTypeDD  = showFieldTypes;
        if (fieldTypeComboArray != null) {
            var selectedVlue = fieldTypeComboArray.store.getAt(fieldType).get(fieldTypeComboArray.valueField || fieldTypeComboArray.displayField);
            fieldTypeComboArray.setValue(selectedVlue);
            self.FieldTypeSelected(selectedVlue);
        }
    };

    this.GetSelectedFields = function () {
        var selectedItems = self.GetSelectedItems();
        var selectedFields = [];

        for (var i = 0; i < selectedItems.length; i++) {
            var item = selectedItems[i];

            var field = {
                RefID: item.itemID,
                DisplayName: item.itemCaption,
                Field: JSON.parse(item.data[FIELD_DATAINFO_KEY])
            };

            if (stringToBoolean(config.useDataAggregation) && field.Field.DataTypeInfo.SupportedAggregations
                && field.Field.DataTypeInfo.SupportedAggregations.length > 0) {
                field.Field.DataTypeInfo.DefinedAggregation = dataAggregationComboArray.getValue();
            }

            selectedFields.push(field);

            SW.Core.Pickers.FieldPicker.FieldsCacheManager.AddField(field.Field);
        }
        return selectedFields;
    };

    this.ComputeAndSetNavigationPathFilterAsync = function (fieldIDs, currentFieldID, afterComputed) {
         if (limitationEnabled()) {
             computeNavigationPath(fieldIDs, currentFieldID, afterComputed);
         }
         else {
             
            if ($.isFunction(afterComputed)) {
                afterComputed();
            }
         }
    };

    /// <summary>Set additional parameters which will be always send within server request.</summary>
    /// <param Name="value" type="Object"></param>
    this.SetExtraRequestParams = function(value) {
        config.extraRequestParams = value;
    };

    this.GetExtraRequestParams = function() {
        return config.extraRequestParams;
    };

    created();
};

/// <summary>Global collection of registered field picker instances.</summary>
SW.Core.Pickers.FieldPicker.Instances = [];
