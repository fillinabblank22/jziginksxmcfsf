﻿IFrameManager = function () {
    var parentPooler;
    var refreshInterval = 300;
    var iframe;
    var callerLocation;
    var targetOrigin;

    OnParentMessage = function (event) {
        var message = event.data;
        var height = Number(message);

        iframe.style.height = height + "px";
    };

    GetIframeContentSize = function () {
        if (iframe.contentWindow.postMessage) {
            iframe.contentWindow.postMessage("getheight", "*");
        }
    };

    OnChildMessage = function (event) {
        // check the origin of request
        if (callerLocation != null) {
            if ('domain' in event) {    // Opera before version 10
                if (event.domain != callerLocation)
                    return;
            }
            if ('origin' in event) {    // Firefox, Safari, Google Chrome, Internet Explorer from version 8 and Opera from version 10
                if (event.origin != ("http://" + callerLocation) && event.origin != ("https://" + callerLocation))
                    return;
            }
        }

        if (event.data == "getheight") {
            event.source.postMessage(GetBodyHeight(), event.origin);
        }
    };

    GetBodyHeight = function () {
        var D = document;
        return Math.max(
            Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
            Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
            Math.max(D.body.clientHeight, D.documentElement.clientHeight)
        );
    };

    return {
        InitParent: function (frameId) {
            iframe = document.getElementById(frameId);

            if (window.addEventListener) {  // all browsers except IE before version 9
                window.addEventListener("message", OnParentMessage, false);
                parentPooler = setInterval("GetIframeContentSize();", refreshInterval);
            } else if (window.attachEvent) {    // IE before version 9
                window.attachEvent("onmessage", OnParentMessage);
                parentPooler = setInterval("GetIframeContentSize();", refreshInterval);
            }
        },

        InitChild: function (location) {
            callerLocation = location;

            if (window.addEventListener) {  // all browsers except IE before version 9
                window.addEventListener("message", OnChildMessage, false);
            } else if (window.attachEvent) {    // IE before version 9
                window.attachEvent("onmessage", OnChildMessage);
            }
        }
    }
} ();