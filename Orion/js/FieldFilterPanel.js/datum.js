﻿// builder.config:preventGlobals="true"

function Datum(fn, pvals) {

    var trigger = fn || function(){};
    var me = this;
    var propArray = pvals ? $.extend(true, [], pvals) : [];
    var propMap = {};
    var prop, tval;

    $.each(propArray, function (k, v) { propMap[v.FieldRefId] = v; });

    me.Current = function () { return propArray; };

    me.Meta = Datum.Meta;

    me.ValueGetId = function(dom) {
        var e = (typeof (dom) == "string") ? $(dom) : $(dom).closest('[data-valueid]');
        var r = e.attr('data-valueid');
        return r === "null" && e.attr('data-isnull') == "1" ? null : r;
    };

    me.ValueGet = function (fieldref, valid) {
        if (typeof (fieldref) != "string") { // is dom.
            valid = me.ValueGetId(fieldref);
            fieldref = me.PropertyGetId(fieldref);
        }

        if (prop = propMap[fieldref]) {
            var vs = prop.Values || [];
            for (var i = 0, imax = vs.length, v; i < imax && (v = vs[i]) ; ++i) {
                if (v.Value === valid) return v;
            }
        }
        return null;
    };

    var valueSetInUse = function(fieldref, valid, inuse) {
        if (!(tval = me.ValueGet(fieldref, valid)) || tval.IsInUse == inuse) return;
        tval.IsInUse = inuse;
        trigger('model.changed', me);
    }

    me.ValueDisable = function (fieldref, valid) { valueSetInUse(fieldref, valid, false); };

    me.ValueEnable = function (fieldref, valid) { valueSetInUse(fieldref, valid, true); };

    me.ValueRemove = function (fieldref, valid) {
        if (typeof (fieldref) != "string") { // is dom.
            valid = me.ValueGetId(fieldref);
            if (valid === null) return false;
            fieldref = me.PropertyGetId(fieldref);
        }

        if (prop = propMap[fieldref]) {
            prop.Values = $.map(prop.Values || [], function (v, k) { if (v.Value !== valid) return v; });
            trigger('model.changed', me);
        }

        return !!prop;
    };

    me.ValueAppend = function (fieldref, val) {
        if (prop = propMap[me.PropertyGetId(fieldref)]) {
            var vs = prop.Values || (prop.Values = []);
            vs.splice(vs.length, 0, val);
            trigger('model.changed', me);
        }
    };

    me.ValuePrepend = function (fieldref, val) {
        if (prop = propMap[me.PropertyGetId(fieldref)]) {
            var vs = prop.Values || (prop.Values = []);
            vs.splice(0, 0, val);
            trigger('model.changed', me);
        }
    };

    me.PropertyIds = function() { return $.map(propArray, function(v) { return v.FieldRefId; }); };

    me.PropertyGetId = function (dom) { return typeof(dom) == "string" ? dom : ($(dom).closest('[data-keyid]').attr('data-keyid') || null); };

    me.PropertyGet = function (fieldref) { return propMap[me.PropertyGetId(fieldref)] || null; };

    me.PropertyRemove = function (fieldref) {
        if (prop = propMap[fieldref = me.PropertyGetId(fieldref)]) {
            propArray = $.map(propArray, function (v, k) { if (prop !== v) return v; });
            delete propMap[fieldref];
            trigger('model.changed', me);
        }
        return !!prop;
    };

    me.PropertySet = function (p, nodirty) {
        if (!p || !p.FieldRefId) return null;
        if (!nodirty) trigger('model.changed', me);
        var exist = propMap[p.FieldRefId];
        if (!exist) propArray.push(p);
        else propArray[jQuery.inArray(exist,propArray)] = p;
        return propMap[p.FieldRefId] = p;
    };

    me.PropertyMerge = function (p, alterSelection, nodirty) {
        if (!p || !p.FieldRefId) return null;
        if (!p.Values) p.Values = [];

        var exist = propMap[p.FieldRefId];

        if (!exist) {
            if (!alterSelection) $.each(p.Values, function (k, v) { v.IsInUse = false; });
            me.PropertySet(p, nodirty);
            return p;
        }

        if (!exist.Values) exist.Values = [];

        var selstate = {};
        $.each((alterSelection ? p : exist).Values, function (k, v) { selstate[v.Value] = v.IsInUse; });

        var valmap = {};
        $.each(exist.Values, function (k, v) { valmap[v.Value] = v; });

        $.each(p.Values, function (k, v) {
            if (valmap[v.Value]) return;
            valmap[v.Value] = v;
            exist.Values.push(v);
        });

        $.each(exist.Values, function (k, v) { v.IsInUse = !!selstate[v.Value]; });

        if (!nodirty) trigger('model.changed', me);
        return exist;
    };

    me.Clone = function() { return new Datum(null, propArray); };
}

Datum.Meta = function (obj, id, exact) {
    id = (id || '').toLowerCase();
    var d, m = (obj || {}).MetaData, i = 0, imax = m ? m.length : 0;
    for (; i < imax; ++i) {
        if ((d = m[i]) && (d.N || '').toLowerCase() == id)
            return exact ? d.V : (d.V || '').toLowerCase();
    }
    return exact ? undefined : '';
};
