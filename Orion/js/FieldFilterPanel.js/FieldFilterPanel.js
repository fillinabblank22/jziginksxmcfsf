﻿// builder.config:preventGlobals="true"

var self = this;
var uuid = 0;
var rowOffset = ($.browser.msie) ? 18 : 12;
var curY = 0;
var mouseTimer;
var isMouseDown = false;
var isMouseReady = false;
var lastWidth;
var fieldpicker = null;

// public functions

// reduce the number of values to hide, show when the picker is involved.
function valuesForRendering(prop, meta) {
    var r = prop.Values || [], m = meta.rowLimit + 1, c = 0, s = 0;

    if (meta.renderer.sortImpl) meta.renderer.sortImpl(prop, prop.Values||[]); // allow the render to sort the values prior to us giving them a subset of values

    if (!options.UseValuePicker || r.length <= m || state.Meta(prop, 'IsKeywordSearch') == 'true') return r;

    r = $.map(r, function (v) {
        if (v.IsInUse) { c++; s++; return v; }
        if (c < m) { c++; return v; }
    });

    if (r.length > m) {
        c = m - s;
        r = $.map(r, function (v) { if (v.IsInUse || c-- > 0) return v; });
    }

    return r;
}


function getPropDisplayMeta(prop) {
    var tag = prop.ApplicationType;
    var rowLimit = parseInt(state.Meta(prop, 'VisiblePropertiesLimit'), 10);
    if( isNaN(rowLimit) ) rowLimit = options.defaultNumberOfRows || 3;
    
    var renderer = renderers[tag];
    var isRemovable = state.Meta(prop, 'UiRemovable');
    isRemovable = (isRemovable == '' || isRemovable == 'true');

    if (!renderer) {
        if (state.Meta(prop, 'IsKeywordSearch') == 'true') {
            renderer = renderers[tag = 'ffp-textinput'];
        } else {
            renderer = renderers[tag = 'ffp-default'];
        }
    }

    return { renderTag: tag, renderer: renderer, isRemovable: isRemovable, rowLimit: rowLimit };
}

function addPropertyMarkup(id, prop) {
    var meta = getPropDisplayMeta(prop);
    var rowsToShow = meta.rowLimit;
    var renderType = meta.renderTag;
    var r = meta.renderer;
    var isRemovable = meta.isRemovable;

    var result = r(id, prop, valuesForRendering(prop,meta)) || {};

    var remove = !isRemovable ? '' : '<div class="key-remove" title="' + options.labels.remove + '"></div>';

    var template = '<div id="key_{:id}" data-keyid="{:FieldRefId}" data-keyname="{:DisplayName}" data-rendertype="{:renderType}" data-defaultrowcount="{rowsToShow}" data-show="default" class="key-value-group {:renderType}">' +
        '<div class="key-header key-expanded"><div class="key-header-name" title="{:DisplayNameCombined}">{:DisplayNameCombined} <span class="key-header-count"></span></div><div class="key-header-controls">{removeHtml}</div><div class="key-name-fader"></div></div>' +
        '<div class="value-list"><table id="values_{:id}">{contentHtml}</table></div>';

    var vars = { id: id, FieldRefId: prop.FieldRefId, DisplayName: prop.DisplayName, DisplayNameCombined: prop.DisplayName + (prop.OwnerDisplayName ? ' (' + prop.OwnerDisplayName + ')' : ''), rowsToShow: rowsToShow, renderType: renderType, removeHtml: remove, contentHtml: result.content };

    var html = format(template, vars);

    $("#key_list").append(html);

    var fn = result.onInsert || function () {};
    fn($("#values_" + id));

    addLink(id);
    showValuesDefault(id);
}

function updatePropertyMarkup(dom, prop) {
    var meta = getPropDisplayMeta(prop);
    var r = meta.renderer;
    var vtable = $(dom).find('.value-list > table')[0];
    var id = vtable.id.replace(/^values_/, '');
    var result = r(id, prop, valuesForRendering(prop, meta)) || {};

    $(vtable).empty();
    $(vtable).append('' + result.content);

    var fn = result.onUpdate || result.onInsert || function() {};
    fn($("#values_" + id));

    addLink(id);
    showValuesDefault(id);
}

filter.addPropertyValues = function(v, notDirty) {
    if (jQuery.isArray(v) == false) v = [v];

    var existing = {};

    $.each($("#key_list > div"), function(i, v) { existing[$(v).data('keyid')] = v; });

    for (var i = 0, imax = v.length; i < imax; ++i) {
        var e = v[i], dom = existing[e.FieldRefId];

        state.PropertySet(e, notDirty);
        if (!dom) {
            var id = uuid++;
            addPropertyMarkup(id, e);
        } else {
            updatePropertyMarkup(dom, e);
        }
    }

    updateHeight();
};

filter.mergePropertyValues = function(props, alterSelection, notDirty) {
    jQuery.each(props || [], function(k, p) { state.PropertyMerge(p, alterSelection, notDirty); });
    filter.addPropertyValues(state.Current(), notDirty);
};

function doCollapse(dom, collapse) {
    var me = $(dom);

    if (collapse === undefined) collapse = me.hasClass("expanded") ? 1 : 0;

    if (collapse) {
        if (!me.hasClass("expanded")) return;
        me.removeClass("expanded").addClass("collapsed");
        me.attr("title", options.labels.expand);
        me.height($("#leftpanel_filter").css("min-height"));
        if (options.WidthMax > options.WidthMin) $("#leftpanel_filter").resizable({ disabled: true });
        $("#leftpanel_filter").css("cursor", "pointer");
        $("#leftpanel_filter").animate({ "width": "20px" }, 200, function () { $(".expand_collapse_title").addClass("collapsed"); });
        $("#leftpanel_filter").children().not(".expand_collapse_btn,.expand_collapse_title").hide();
    } else {
        if (me.hasClass("expanded")) return;
        me.removeClass("collapsed").addClass("expanded");
        me.attr("title", options.labels.collapse);
        me.height("14px");
        $(".expand_collapse_title").removeClass("collapsed");
        if (options.WidthMax > options.WidthMin) $("#leftpanel_filter").resizable({ disabled: false });
        $("#leftpanel_filter").css("cursor", "auto");
        $("#leftpanel_filter").animate({ "width": lastWidth + "px" }, 200);
        $("#leftpanel_filter").children().show();
    }
}

function addEventHandlers() {
    $(document).on("change", ".value-cb :checkbox", function () {
        state[$(this).prop('checked') ? 'ValueEnable' : 'ValueDisable'](this);
    });

    $("#leftpanel_filter").on("resizestop", function (event, ui) {
        lastWidth = ui.size.width;
        setCookie("last_panel_width", lastWidth, "year", 1);
        filter.trigger('viewmodel.changed');
    });

    $(document).on("click", ".show-fewer-link", function (e) {
        var pid = e.currentTarget.id.split('_')[1];
        if ($("#key_" + pid + " :checked").length >= 1) {
            showValuesChecked(pid);
        } else {
            showValuesDefault(pid);
        }
        addLink(pid);
    });

    var pickerDlg = null;
    $(document).on("click", ".show-all-link", function (e) {
        var pid = e.currentTarget.id.split('_')[1];

        if (options.UseValuePicker) {
            var prop = state.PropertyGet(e.target);

            pickerDlg = pickerDlg || new FieldValuePickerPanel('#fieldValuePickerDialog');

            pickerDlg.Show(prop.Values, {
                property: prop.DisplayName + (prop.OwnerDisplayName ? ' (' + prop.OwnerDisplayName + ')' : ''), success: function (o) {
                if (o.changes) {
                    prop.Values = o.records;
                    filter.addPropertyValues(prop);
                    filter.trigger('model.changed', state);
                }
            }});

            return;
        }

        showValuesAll(pid);
        addLink(pid);
    });

    $(document).on("click", ".expand_collapse_btn", function () { doCollapse(this); filter.trigger('viewmodel.changed'); });

    $(document).on("click", ".key-header", function () {
        var me = $(this);
        me.toggleClass("key-expanded key-collapsed");
        me.parent().find('.value-list').toggle();
    });

    $(document).on({
        mouseenter: function () {
            if (!isMouseDown) {
                $(this).find(".key-header-controls").show();
                $(this).find(".key-name-fader").hide();
            }
        },
        mouseleave: function () {
            $(this).find(".key-header-controls").hide();
            $(this).find(".key-name-fader").show();
        }
    }, ".key-header");

    $(document).on("click", ".key-remove", function (e) {
        e.stopImmediatePropagation();
        state.PropertyRemove(this);
        $(this).closest(".key-value-group").remove();
    });

    $(document).mousedown(function (e) {
        if (e.which === 1) {
            isMouseDown = true;
            mouseTimer = setTimeout(function () { isMouseReady = true; }, 200);
        }
    });

    $(document).mouseup(function (e) {
        updateHeight();
        if (e.which === 1) {
            isMouseDown = false;
            isMouseReady = false;
            clearInterval(mouseTimer);
        }
    });

    $(document).on("mousemove", ".value-list", function (e) {
        if (isMouseDown && isMouseReady) {
            if ((curY > e.clientY + rowOffset) || (curY < e.clientY - rowOffset)) {
                curY = e.clientY;
                $(e.target).prop("checked", !$(e.target).is(':checked'));
                $(e.target).trigger("change");
            }
        }
    });

    $("#linkAddFilterProperty").click(function () {
        fieldpicker.ShowInDialog();
        return false;
    });

    $("#linkApplyButton, #linkApplyTopLink").click(function () {
        filter.Save();
        return false;
    });

    $("#linkClearAll").click(function () {
        var ids = state.PropertyIds(), alter = false;
        $.each(ids, function (k, v) {
            $.each(state.PropertyGet(v).Values || [], function (kk, vv) {
                if (vv.IsInUse) {
                    vv.IsInUse = false;
                    alter = true;
                    filter.trigger('model.changed', state);
                }
            });
        });

        if (alter) {
            filter.mergePropertyValues(state.Current(), true, false);
            filter.Save();
        }
    });

    $(document).on("click", ".value-row", function (e) {
        if (e.target.type !== 'checkbox') {
            var cb = $(this).find(":checkbox");
            cb.prop("checked", !cb.prop("checked"));
            cb.trigger("change");
        }
    });
}

// helper functions
function showValuesAll(pid) {
    $("#key_" + pid + " tr.value-row").show();
    $("#key_" + pid).attr("data-show", "all");
}


function showValuesDefault(pid) {
    var minRows = $("#key_" + pid).attr("data-defaultrowcount") || options.defaultNumberOfRows || 3;
    $("#key_" + pid + " tr.value-row").hide();
    $("#key_" + pid + " tr.value-row:lt(" + minRows + ")").show();
    $("#key_" + pid + " :checked").closest("tr").show();
    $("#key_" + pid).attr("data-show", "default");
}


function showValuesChecked(pid) {
    $("#key_" + pid + " tr.value-row").hide();
    $("#key_" + pid + " :checked").closest("tr").show();
    $("#key_" + pid).attr("data-show", "checked");
}

var isDirty = false;
function SetDirty(cmd) {
    if (cmd === undefined) cmd = 'set';

    if (cmd == 'clear') {
        if (isDirty) {
            isDirty = false;
            $("#linkApplyButton").addClass("sw-btn-secondary").removeClass("sw-btn-primary");
            $("#linkApplyTopLink").hide();
        }
    }
    else if (cmd == 'set') {
        if (!isDirty) {
            isDirty = true;
            $("#linkApplyButton").removeClass("sw-btn-secondary").addClass("sw-btn-primary");
            $("#linkApplyTopLink").show();
        }
    }
}

function updateHeight() {
    $("#leftpanel_filter").css("height", "auto");
    setTimeout(function () {
        var newHeight = $("#container").outerHeight() - options.pageHeightOffset;
        $("#leftpanel_filter").height(newHeight);
        if ($(".expand_collapse_btn").hasClass("collapsed")) {
            $(".expand_collapse_btn").height(newHeight);
        } else {
            $(".expand_collapse_btn").height(14);
        }
    }, 100);
}

function addLink(pid) {
    var minRows = $("#key_" + pid).attr("data-defaultrowcount") || options.defaultNumberOfRows || 3;

    if ($("#key_" + pid).find(".value-row").length <= minRows) {
        return;
    }

    if (!$("#key_" + pid + " tr:last").hasClass("value-row")) {
        $("#key_" + pid + " tr:last").remove();
    }

    if ($("#key_" + pid).attr("data-show") === "all") {
        $("#key_" + pid + " tr.value-row:last").after(format('<tr><td></td><td colspan="20"><span><a id="fewer_{:0}" class="ffp-link show-fewer-link">{:1}</a></span></td></tr>', pid, options.labels.showFewer));
    } else {
        $("#key_" + pid + " tr.value-row:last").after(format('<tr><td></td><td colspan="20"><span><a id="all_{:0}" class="ffp-link show-all-link">{:1}</a></span></td></tr>', pid, options.labels.showAll));
    }
}

//

var isInit = false;
filter.Init = function(config, model, vmodel) {

    fieldpicker = SW.Core.Pickers.FieldPicker.Instances[config.FieldPickerInstanceId];
    fieldpicker.SetOnSelected(function () { filter.trigger('property.selected', fieldpicker); });

    if (!isInit) {
        isInit = true;

        options = $.extend(options, config, vmodel || {});

        var pgHeader = $('#leftpanel_filter').parents('table')[0];
        var posHeader = $(pgHeader).position();
        var headerHeight = posHeader.top + $('#footer').outerHeight() + 30;

        options.pageHeightOffset = headerHeight;

        lastWidth = options.WidthCurrent || getCookie("last_panel_width") || options.WidthMin || 280;

        setCookie("last_panel_width", lastWidth, "year", 1);

        $("#leftpanel_filter").css({ "min-height": "200px" });

        if( !options.IsCollapsed )
            $("#leftpanel_filter").css({ "width": lastWidth + "px" });

        if (options.WidthMax > options.WidthMin) {
            $("#leftpanel_filter").resizable({ handles: "e", maxWidth: options.WidthMax, minWidth: options.WidthMin, resize: function () { $(this).css('height', 'auto'); } });
        }

        updateHeight();

        addEventHandlers();

        // hook up all the known managed entity application types for status property

        var r = renderers['ffp-status'];
        $.each(options.StatusApplicationTypes || [], function(k, v) { renderers[v] = r; });
    }

    if (model)
        filter.addPropertyValues(model, true);
};
