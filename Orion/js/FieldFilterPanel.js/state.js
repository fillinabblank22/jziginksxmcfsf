﻿// builder.config:preventGlobals="true"

var filter = SW.Core.namespace('SW.Core.Widgets.FieldFilterPanel');
filter.Meta = Datum.Meta;

// events

(function() {

    SW.Core.Observer.makeCancellableObserver(filter);
    filter.on('model.changed', function() { SetDirty(); });

})();

//

var renderers = {};
var state = new Datum(filter.trigger);
var options = {
    "pageHeightOffset": null,
    "defaultNumberOfRows": 3,
    "WidthCurrent": 280,
    "WidthMax": 200,
    "WidthMin": 500,
    "labels": {
        "showAll": "@{R=Core.Strings.2;K=FFP_ShowMore;E=js}",
        "showFewer": "@{R=Core.Strings.2;K=FFP_ShowFewer;E=js}",
        "remove": "@{R=Core.Strings.2;K=FFP_Remove;E=js}",
        "search": "@{R=Core.Strings.2;K=FFP_Search;E=js}",
        "collapse": "@{R=Core.Strings.2;K=FFP_Collapse;E=js}",
        "expand": "@{R=Core.Strings.2;K=FFP_Expand;E=js}",
        "add": "@{R=Core.Strings;K=WEBJS_PS0_1;E=js}",
        "noObjects": "@{R=Core.Strings.2;K=FFP_NoObjectsForProperty;E=js}"
    }
};

//

filter.Save = function() {
    if (isDirty && filter.trigger('model.save', state.Current())) {
        SetDirty('clear');
    }
};

filter.State = function () {
    return state.Clone();
};

filter.ViewState = function () {
    return {
        IsCollapsed: $('#leftpanel_filter .expand_collapse_btn').hasClass("expanded") == false,
        WidthCurrent: lastWidth
    };
};

var format = function (s, params) {
    var args = Array.prototype.slice.call(arguments, 1), obj = params || {};

    return s.replace(/[{][:]?[a-z0-9_\-.]+[}]/ig, function (m) {
        var tok = m.substring(1, m.length - 1), n, r, e = 0;
        if (tok[0] == ':') { e = 1, tok = tok.substring(1); }
        if (((n = parseInt(tok)) === NaN) || ((r = args[n]) === undefined)) r = obj[tok];
        if (e) return $('<div/>').text(''+r).html();
        return ''+r;
    });
}
