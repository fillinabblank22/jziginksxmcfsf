﻿
// panelfns = { redrawPanel: function([record]){} }
// op = { PageSize: 25 }

function FieldValuePickerState(panelfns, op) {
    var def = { PageSize: 25 };
    var panelFns = panelfns, me = this, options = $.extend(def, op || {});
    var raw = null, lookup = null, selMap = null, selected = [], searched = null, searchtext = null, sortInfo = { field: "Count", direction: "DESC" };
    var store = null, grid = null;

    var texts = {
        novaluesfound: '@{R=Core.Strings;K=FVP_NoResultsFound;E=js}',
        nosearchhits: '@{R=Core.Strings;K=FVP_NoSearchResults;E=js}'
    };

    this.PageSize = options.PageSize;

    this.SetGrid = function (g) {
        grid = g;

        var sm = grid.getSelectionModel();
        sm.on('rowdeselect', function(q, idx, r) {
            me.Unselect(r.data.Value);
        });
        sm.on('rowselect', function (q, idx, r) {
            me.Select(r.data.Value);
        });

        store.on('beforeload', function (s, op) {
            op.params = op.params || {};
            var start = op.params.start || 0;
            var limit = op.params.limit || me.PageSize;
            var dir = op.params.dir || sortInfo.direction;
            var sort = op.params.sort || sortInfo.field;
            var r = me.GetPage(start, limit, sort, dir);
            s.proxy.data = r.data;
            s.proxy.data.total = r.total;
            return true;
        });

        store.on('load', function (s, o) {
            var records = $.map(o, function (v) { return me.IsSelected(v.data.Value) ? v : undefined; });
            if (records.length) { window.setTimeout(function () { grid.getSelectionModel().selectRecords(records, true); }, 0); }
        });
    }

    this.IsSelected = function (val) {
        return selMap[val];
    };

    this.Select = function (val) {
        var r = lookup[val], repaint;
        if (r) {
            if (!selMap[val]) selected.push(repaint = r);
            selMap[val] = true;
            if (repaint) panelFns.redrawSelected(selected);
        }
    }

    this.UnselectByOrdinal = function (ord) {
        var s = selected[ord];
        if (s) {
            var m = grid.getSelectionModel(), found = false;
            var news = $.map(m.getSelections(), function (v) { if (v.data.Value !== s.Value) return v; else found = true; });

            if (found) m.selectRecords(news);
            else me.Unselect(s.Value);
        }
    };

    this.Unselect = function (val) {
        var r = lookup[val], repaint;
        if (r) {
            if (selMap[val]) {
                var idx = -1;
                for (var i = 0, imax = selected.length, v; v = selected[i], i < imax; ++i) {
                    if (v.Value === val) { idx = i; break; }
                }
                if (idx >= 0) repaint = selected.splice(idx, 1);
            }
            selMap[val] = false;
            if (repaint) panelFns.redrawSelected(selected);
        }
    }

    this.GetPage = function (start, n, sort, dir) {

        if (!searched || sortInfo.field != sort || sortInfo.direction != dir) {
            sortInfo.field = sort || "Count";
            sortInfo.direction = dir || "DESC";
            me.SetSearch();
        }

        start = start > searched.length ? searched.length : start;
        n = (start + n) < searched.length ? n : searched.length - start;

        var d = [];
        for (var i = start, imax = start + n; i < imax; ++i) d.push(searched[i]);

        return { data: d, total: searched.length };
    }

    this.SetSearch = function (val) {

        if (typeof (val) == "string" && val === searchtext) return false;

        if (val !== undefined) searchtext = val;
        else if (searchtext === null) searchtext = '';

        searched = [];

        var s = searchtext.toLocaleLowerCase(), pass = searchtext === '';

        $.each(raw, function (i, v) {
            var lv = v.LValue || '', idx = -1;
            if (!pass) idx = lv.toLocaleLowerCase().indexOf(s);
            if (idx >= 0) lv = lv.substring(0, idx) + "<span class='sw-fvp-searchhit'>" + lv.substring(idx, idx + s.length) + "</span>" + lv.substring(idx + s.length, lv.Length);
            if (pass || idx >= 0) searched.push([v.Value, lv, v.Count || 0, !!v.IsInUse, v.LValue.toLowerCase()]);
        });

        if (sortInfo.field == "LValue") { // raw array comes pre-sorted.
            if (sortInfo.direction == "ASC") searched.sort(function (a, b) { return a[4].localeCompare(b[4]); });
            else searched.sort(function (a, b) { return b[4].localeCompare(a[4]); });
        } else {
            if (sortInfo.direction == "ASC") searched.sort(function (a, b) { return a[2] - b[2] || a[4].localeCompare(b[4]); });
            else searched.sort(function (a, b) { return b[2] - a[2] || a[4].localeCompare(b[4]); });
        }

        return pass ? { emptyText: texts.novaluesfound } : { emptyText: Ext.util.Format.htmlEncode(String.format(texts.nosearchhits, searchtext)) };
    }

    this.GetStore = function () {
        if (!store) {
            var nameRecord = Ext.data.Record.create([{ name: 'Value' }, { name: 'LValue' }, { name: 'Count', type: 'float' }]);
            var arrayReader = new Ext.data.ArrayReader({ limit: me.PageSize, totalProperty: 'total', idIndex: 0 }, nameRecord);
            var memoryProxy = new Ext.data.MemoryProxy([]);
            store = new Ext.data.Store({ reader: arrayReader, autoLoad: false, proxy: memoryProxy, remoteSort: true });
        }

        return store;
    }

    this.SetData = function (data) {
        raw = data;
        lookup = {};
        selMap = {};
        selected = [];
        searchtext = null;

        $.each(raw, function(i, v) {
            lookup[v.Value] = v;
            if (v.IsInUse) {
                selMap[v.Value] = true;
                selected.push(v);
            }
        });
        me.SetSearch('');
        store.load();
        panelFns.redrawSelected(selected);
    }

    this.CommitSelection = function () {
        var changes = 0;
        $.each(raw, function (i, v) {
            var nv = !!selMap[v.Value];
            if (nv != v.IsInUse) { changes++; v.IsInUse = nv; }
        });

        return { changes: changes, records: raw };
    }
}
