﻿
// dlgid = #id
// op = { title: 'title {0}', searchhint: 'search {0}', selectedtext: '{0} selected', width: 781, panelHeight: 550, selectorWidth: 200, PageSize: 25 }

function FieldValuePickerPanel(dlgid, op) {

    var rootSelector = dlgid;
    var panelSelector = rootSelector + " .sw-pg-holder";

    var options = $.extend({
        title: '@{R=Core.Strings;K=FVP_FilterByTitle;E=js}',
        searchhint: '@{R=Core.Strings;K=FVP_SearchHint;E=js}',
        selectedtext: '@{R=Core.Strings;K=FVP_SelectedCount;E=js}',
        introtext: '@{R=Core.Strings;K=FVP_IntroductionText;E=js}',
        width: 781,
        panelHeight: 550,
        selectorWidth: 200,
        PageSize: 25
    }, op || {});

    var texts = {
        pageaftertext: '@{R=Core.Strings;K=FVP_PageAfterText;E=js}',
        pagebeforetext: '@{R=Core.Strings;K=FVP_PageBeforeText;E=js}',
        pagingposition: '@{R=Core.Strings;K=FVP_PagePosition;E=js}',
        columnmatches: '@{R=Core.Strings;K=FVP_ColumnMatches;E=js}',
        filteron: '@{R=Core.Strings;K=FVP_ColumnFilterOn;E=js}'
    };

    var xg = Ext.grid;

    var fvpstate = new FieldValuePickerState({ redrawSelected: setSelectionPanel }, { PageSize: options.PageSize });

    var sm = new xg.CheckboxSelectionModel();

    var pager = new Ext.PagingToolbar({
        pageSize: fvpstate.PageSize,
        store: fvpstate.GetStore(),
        displayInfo: true,
        displayMsg: texts.pagingposition,
        afterPageText: texts.pageaftertext,
        beforePageText: texts.pagebeforetext,
        emptyMsg: "",
        listeners: { afterrender: function () { this.refresh.hide(); } }
    });

    function updateSearch(t) {
        var ret = fvpstate.SetSearch(t);
        if (ret) {
            grid.getView().emptyText = ret.emptyText;
            if (pager) pager.moveFirst();
        }
    }

    var searchfield = new Ext.form.TextField({
        width: 300,
        emptyText: options.searchhint,
        enableKeyEvents: true,
        listeners: {
            specialkey: function (me, e) { if (e.getKey() == e.ENTER) updateSearch(me.getValue()); },
            blur: function (me) { updateSearch(me.getValue()); }
        }
    });

    var grid = new xg.GridPanel({
        cls: 'sw-fvp-grid',
        region: 'center',
        layout: 'fit',
        frame: false,
        border: true,

        store: fvpstate.GetStore(),

        cm: new xg.ColumnModel({
            defaults: { width: 120, sortable: true },

            columns: [
                sm,
                { id: 'text', header: "", dataIndex: 'LValue', resizable: false, hideable: false },
                { header: texts.columnmatches, dataIndex: 'Count', width: 80, fixed: true, hideable: false }
            ]
        }),

        sm: sm,
        autoExpandColumn: 'text',
        columnLines: false,
        enableColumnMove: false,
        enableHdMenu: false,
        enableColumnResize: false,

        tbar: [
            searchfield,
            { xtype: 'tbtext', text: '', width: 6 },
            { xtype: 'tbitem', id: 'fvpsearchtext', cls: 'sw-fvp-search', width: 22, height: 22 }
        ],

        bbar: pager
    });

    var selected = new Ext.Panel({
        region: 'east',
        layout: 'fit',
        cls: 'sw-fvp-list',
        width: options.selectorWidth,
        maxWidth: options.width - 26 - 360,
        minWidth: options.selectorWidth < 240 ? options.selectorWidth : 240,
        split: true,
        frame: false,
        border: true,
        collapsible: false,
        tbar: [
            texts.filteron,
            '->',
            { xtype: 'tbtext', text: String.format(options.selectedtext, 0), id: 'vfpselectedtext' }
        ]
    });

    var panel = new Ext.Panel({
        layout: 'border',
        width: options.width - 26, // default padding is 13px on either side.
        height: options.panelHeight,
        renderTo: $(panelSelector)[0],
        border: false,

        items: [grid, selected]
    });

    fvpstate.SetGrid(grid);

    var successCallback = null;

    $(rootSelector + " #fvpOk").click(function () {
        var changes = fvpstate.CommitSelection();
        if (successCallback) successCallback(changes);
        $(rootSelector).dialog('close');
    });

    $(rootSelector + " #fvpCancel").click(function () {
        $(rootSelector).dialog('close');
    });

    $(document).on('click', rootSelector + ' a.sw-fvp-delete', function (e) {
        fvpstate.UnselectByOrdinal(parseInt($(e.target).attr('sw:sid'), 10));
    });

    $(rootSelector + " .sw-fvp-intro").text(options.introtext);

    //

    this.Show = function(data, ops) {
        var o = $.extend({ property: "property" }, ops || {});
        successCallback = o.success;

        searchfield.emptyText = String.format(options.searchhint, o.property);
        searchfield.applyEmptyText();
        searchfield.setValue('');

        grid.getSelectionModel().clearSelections(true);

        fvpstate.SetData(data || []);
        searchfield.setDisabled(true);

        $(rootSelector).dialog({
            position: 'center',
            width: options.width,
            height: 'auto',
            modal: true,
            draggable: true,
            title: String.format(options.title, o.property),
            show: { duration: 0 }
        });

        searchfield.setDisabled(false);
        grid.getColumnModel().setColumnHeader(1, o.property);
    }

    function setSelectionPanel(data) {

        var cmp = Ext.getCmp('vfpselectedtext');
        cmp.update(String.format(options.selectedtext, data.length));

        var html = [];
        $.each(data, function (i, v) {
            html.push('<div class="sw-fvp-selected">' +  MakeBreakableText(v.LValue) + '<a class="sw-fvp-delete" sw:sid="' + i + '" href="#" onclick="return false;"></a></div>');
        });

        selected.update(html.join(''));
    }
}
