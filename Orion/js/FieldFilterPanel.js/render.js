﻿var defchunk = /.{1,10}/g;

function MakeBreakableText(text, size) {
    size = size || 10;

    var words = /(?:(?![\s])[^\s]+)/g,
    chunk = size == 10 ? defchunk : new RegExp(".{1," + size + "}", "g");

    return text.replace(words, function (t) {
        return t.length <= size ? t : t.match(chunk).join("\u200B");
    });
}

function RegisterRenderer(fn, id) {
    for (var i = 1, imax = arguments.length; i < imax; i++) {
        renderers['' + id] = fn;
    }
}

function renderDefault(keyid, prop, values) {

    values = values || prop.Values;
    var out = '';

    for (var i = 0, imax = values.length; i < imax; i++) {
        var row = values[i];
        var vals = {
            checked: row.IsInUse ? "checked" : "",
            id: row.Value === undefined ? '' : row.Value,
            DisplayName: row.LValue === undefined ? val : row.LValue,
            DisplayNameBreakable: MakeBreakableText(row.LValue === undefined ? val : row.LValue),
            count: (row.Count === undefined || row.Count === null) ? '' : '(' + row.Count + ')',
            ico: state.Meta(row, 'IconClass'),
            isnull: row.Value === null ? '1' : '0'
        }

        var template = '<tr class="value-row" data-valueid="{:id}" data-isnull="{isnull}"><td class="value-cb"><input type="checkbox" value="{:id}" {checked} /></td>';
        template += (vals.ico ? '<td class="value-icon"><div class="{:ico}"></div></td><td class="value-name">' : '<td class="value-name" colspan="2">');
        template += ((row.Value === null || row.Value === '') ? "<span class='sw-ffp-novalue'>{:DisplayNameBreakable}</span>" : "{:DisplayNameBreakable}");
        template += '<td class="value-count">{count}</td></tr>';
        out += format(template, vals);
    }

    if (values.length == 0) {
        out += '<td class="value-name"><span class="sw-ffp-novalue">' + options.labels.noObjects + '</span></td>';
    }

    return { content: out };
}

function MakeMappedStatusRenderer(fnStatusFetcher) {

    var fnStatus = fnStatusFetcher;

    var ret = function(keyid, prop, values) {

        values = values || prop.Values;
        var out = '';

        for (var i = 0, imax = values.length; i < imax; ++i) {
            var row = values[i];
            var sDetails = fnStatus(row.Value);
            var vals = {
                id: row.Value,
                DisplayName: row.Value === null ? row.LValue : sDetails.ShortDescription,
                DisplayNameBreakable: row.Value === null ? row.LValue : MakeBreakableText(sDetails.ShortDescription),
                checked: row.IsInUse ? "checked" : "",
                IconPostfix: sDetails.IconPostfix,
                count: (row.Count === undefined || row.Count === null) ? '' : '(' + row.Count + ')',
                isnull: row.Value === null ? 1 : 0,
                colspan: row.Value === null ? 2 : 1
            };

            var template = '<tr class="value-row" data-valueid="{id}" data-isnull="{isnull}"><td class="value-cb"><input type="checkbox" value="{id}" {checked} /></td>';
            if (!vals.isnull) template += '<td class="value-icon"><div style="width: 12px; height: 12px;" class="sw-xstatus-s-12-{IconPostfix}"></div></td>';
            template += '<td colspan="{colspan}" class="value-name">';
            template += (vals.isnull ? "<span class='sw-ffp-novalue'>{:DisplayNameBreakable}</span>" : "{:DisplayNameBreakable}");
            template += '</td><td class="value-count">{count}</td></tr>';

            out += format(template, vals);
        }

        if (values.length == 0) {
            out += '<td class="value-name"><span class="sw-ffp-novalue">' + options.labels.noObjects + '</span></td>';
        }

        return { content: out };
    };

    ret.sortImpl = function(prop, values) {
        values = values || prop.Values;

        for (var i = 0, imax = values.length; i < imax; ++i) {
            var row = values[i];
            row.order = fnStatus(row.Value).UiOrder;
        }

        values.sort(function (a, b) { return a.order - b.order; });

        for (var i = 0, imax = values.length; i < imax; ++i) {
            var row = values[i];
            delete row.order;
        }
    };

    return ret;
}

var renderStatus = MakeMappedStatusRenderer(SW.Core.Status.Get);

// -- arbitrary textbox

function renderTextInput(keyid, prop, values) {

    values = prop.Values || []; // ignore the set of values we should display. show them all.

    var out = format(
        '<tr><td colspan="3" style="width:100%"><div style="margin-right:1px;"><table style="width:100%"><tr><td style="width:100%"><div class="value-input-container">' +
        '<input id="textinput_{:0}" type="text" class="value-input" placeholder="{:1}" /></div></td><td><div id="inputadd_{:0}" class="add-input">{:2}</div></td></tr></table></div></td></tr>' +
        '<tr class="input-row anchor-row"><td class="value-name" colspan="3"></td></tr>', keyid, options.labels.search, options.labels.add);

    for (var i = 0, imax = values.length; i < imax; ++i) {
        var row = values[i];
        var vals = {
            checked: row.IsInUse ? "checked" : "",
            id: row.Value === undefined ? '' : row.Value,
            DisplayName: row.LValue === undefined ? val : row.LValue,
            DisplayNameBreakable: MakeBreakableText(row.LValue === undefined ? val : Ext.util.Format.htmlDecode(row.LValue)),
            removeText: options.labels.remove
        }

        out += format( '<tr class="input-row value-row" data-valueid="{:id}"><td class="value-cb"><input type="checkbox" value="{:id}" {checked} /></td>' +
            '<td class="value-name">{:DisplayNameBreakable}</td><td class="input-button"><div class="input-controls"><div id="remove_{:id}" class="input-remove" title="{:removeText}"></div></div></td></tr>', vals);
    }

    return { content: out };
}

function highlightExistingValue(row) {
    row.animate({
        "background-color": "#000000",
        "color": "#ffffff"
    }, 40, function () {
        // Animation complete.
        row.animate({
            "background-color": "#ffffff",
            "color": "#000000"
        }, 800);
    });
}

function isValueNew(keyid, txt) {
    var have = {};

    $("#values_" + keyid).find(".input-row").each(function (key, value) {
        var p = $(value), t = p.find(".value-name").text();
        have[t] = p;
    });

    if (have[txt]) {
        highlightExistingValue(have[txt].closest('tr'));
        return false;
    }

    return true;
}

function addTextInputValue(keyid, txt) {
    if ((txt.length > 0) && (isValueNew(keyid, txt))) {
        var encodedTxt = Ext.util.Format.htmlEncode(txt);
        var out = format('<tr class="input-row value-row" style="display:table-row;" data-valueid="{:0}"><td class="value-cb"><input type="checkbox" value="{:0}" checked /></td>' +
            '<td class="value-name">' + encodedTxt + '</td><td class="input-button"><div class="input-controls"><div id="remove_{:0}" class="input-remove" title="{:1}"></div></div></td></tr>', encodedTxt, options.labels.remove );

        var dom = $("#values_" + keyid);
        dom.find(".input-row:first").before(out);
        state.ValuePrepend(dom[0], { Value: encodedTxt, LValue: encodedTxt, IsInUse: true });
        return true;
    } else {
        return false;
    }
}

$(document).on("keypress", ".value-input", function (e) {
    if (e.which === 13) {
        var wasAdded = addTextInputValue($(this).attr('id').split('_')[1], $(this).val());
        if (wasAdded) {
            $(this).val("");
            $(this).focus();
        }
    }
});

$(document).on("click", ".add-input", function () {
    var inputBox = $(this).parent().parent().find(".value-input");
    var wasAdded = addTextInputValue($(this).attr('id').split('_')[1], inputBox.val());

    if (wasAdded) {
        inputBox.val("");
        inputBox.focus();
    }
});

$(document).on("click", ".input-remove", function (e) {
    e.stopImmediatePropagation();
    state.ValueRemove(this);
    $(this).closest("tr.input-row").remove();
});

$(document).on({
    mouseenter: function () {
        if (!isMouseDown) {
            $(this).find(".input-controls").show();
        }
    },
    mouseleave: function () {
        $(this).find(".input-controls").hide();
    }
}, ".input-row");

// -- hookup

RegisterRenderer(renderTextInput, 'ffp-textinput');
RegisterRenderer(renderDefault, 'ffp-default');
RegisterRenderer(renderStatus, 'ffp-status');
