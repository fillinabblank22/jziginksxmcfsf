﻿(function() {

    var ns = SW.Core.namespace('SW.Core.Widgets.FilterCrumbs');

    var registry = {};

    $(document).on("click", ".sw-ffpcrumb-remove", function () {
        var me = $(this),
            val = me.closest('.sw-ffpcrumb-value'),
            prop = me.closest('.sw-ffpcrumb-prop'),
            id = me.closest('.sw-ffpcrumb').attr('id'),
            crumb = registry[id];

        if (!crumb || !val[0] || !prop[0]) return;

        var valid = val.attr('data-valueid'), isnull = (valid === "null" && val.attr('data-isnull') == "1");

        crumb.trigger('value.remove',{
            crumbid: id,
            propertyid: prop.attr('data-keyid'), 
            valueid: isnull ? null : valid
        });
    });

    $(document).on("click", ".sw-ffpcrumb-clear", function () {
        var me = $(this),
            id = me.closest('.sw-ffpcrumb').attr('id'),
            crumb = registry[id];

        if (crumb) crumb.trigger('value.remove.all', { crumbid: id });
    });

    ns.Init = function (opts) {
        if (!opts || !opts.id) return;
        registry[opts.id] = $.extend(SW.Core.Observer.makeCancellableObserver(), opts);
    };

    ns.Get = function (id) { return registry[id] || null; };

    ns.Redo = function (id, state) {

        var crumb = registry[id], m = [];
        if (!crumb) return;

        state = state || {};

        for (var i = 0, imax = state.length; i < imax; i++) {
            var p = state[i], v = p.Values || [], found = jQuery.map(v, function (vv) { if (vv.IsInUse) return vv; });
            if (found.length < 1) continue;

            var mm = [];

            jQuery.each(found, function(k, o) {
              mm.push(format(crumb.templates.value, {
                id: o.Value === null ? "null" : o.Value,
                DisplayName: Ext.util.Format.htmlDecode(o.LValue),
                nullmarker: o.Value === null ? 'data-isnull="1"' : ''
              }));
            });

            m.push(format(crumb.templates.property, {
                FieldRefId: p.FieldRefId,
                values: mm.join(''),
                DisplayNameCombined: p.DisplayName + (p.OwnerDisplayName ? ' (' + p.OwnerDisplayName + ')' : '')
            }));
        }

        if (m.length) m.push(crumb.templates.clearall);

        $('#' + id).html(m.join(''));
    }

})();