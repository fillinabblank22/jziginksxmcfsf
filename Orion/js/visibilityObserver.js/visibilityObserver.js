var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var Require;
            (function (Require) {
                function notNull(param, paramName) {
                    if (param == null) {
                        throw new ReferenceError("The parameter " + paramName + " can't be null or undefined.");
                    }
                }
                Require.notNull = notNull;
                function notEmptyString(param, paramName) {
                    if (param == null || param.trim() === '') {
                        throw new ReferenceError("The parameter " + paramName + " can't be null, undefined or empty.");
                    }
                }
                Require.notEmptyString = notEmptyString;
            })(Require = VisibilityObservation.Require || (VisibilityObservation.Require = {}));
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="require.ts"/>
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var IntersectionObserverWrap = /** @class */ (function () {
                function IntersectionObserverWrap() {
                    this.toObserve = [];
                }
                IntersectionObserverWrap.prototype.init = function (callback) {
                    VisibilityObservation.Require.notNull(callback, 'callback');
                    if (this.observer == null) {
                        console.log('Initializing the intersection observer.');
                        this.observer = new IntersectionObserver(callback);
                        for (var _i = 0, _a = this.toObserve; _i < _a.length; _i++) {
                            var element = _a[_i];
                            this.observer.observe(element);
                        }
                        this.toObserve = [];
                    }
                };
                IntersectionObserverWrap.prototype.unobserve = function (element) {
                    VisibilityObservation.Require.notNull(element, 'element');
                    if (this.observer != null) {
                        this.observer.unobserve(element);
                    }
                    else {
                        this.toObserve = this.toObserve.filter(function (e) { return e !== element; });
                    }
                };
                IntersectionObserverWrap.prototype.observe = function (element) {
                    VisibilityObservation.Require.notNull(element, 'element');
                    if (this.observer != null) {
                        this.observer.observe(element);
                    }
                    else {
                        this.toObserve.push(element);
                    }
                };
                return IntersectionObserverWrap;
            }());
            VisibilityObservation.IntersectionObserverWrap = IntersectionObserverWrap;
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            /** The main type with resource and load callback data */
            var Resource = /** @class */ (function () {
                function Resource() {
                }
                return Resource;
            }());
            VisibilityObservation.Resource = Resource;
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="comparator.ts"/>
/// <reference path="resource.ts"/>
/// <reference path="require.ts"/>
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var ResourceByRowsComparator = /** @class */ (function () {
                function ResourceByRowsComparator() {
                }
                ResourceByRowsComparator.prototype.compare = function (obj1, obj2) {
                    VisibilityObservation.Require.notNull(obj1, 'obj1');
                    VisibilityObservation.Require.notNull(obj2, 'obj2');
                    if (obj1.rowIndex < obj2.rowIndex) {
                        return -1;
                    }
                    if (obj1.rowIndex === obj2.rowIndex) {
                        return 0;
                    }
                    return 1;
                };
                ResourceByRowsComparator.instance = new ResourceByRowsComparator();
                return ResourceByRowsComparator;
            }());
            VisibilityObservation.ResourceByRowsComparator = ResourceByRowsComparator;
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="resource.ts"/>
/// <reference path="require.ts"/>
/// <reference path="resourceByRowsComparator.ts"/>
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            /** The collection with registered callbacks */
            var Resources = /** @class */ (function () {
                function Resources() {
                    this._items = [];
                }
                Object.defineProperty(Resources.prototype, "items", {
                    get: function () {
                        return this._items;
                    },
                    enumerable: true,
                    configurable: true
                });
                Resources.prototype.getByResourceId = function (resourceId) {
                    VisibilityObservation.Require.notEmptyString(resourceId, 'resourceId');
                    var filtered = this.items.filter(function (e) { return e.resourceId === resourceId; });
                    if (filtered.length === 0) {
                        return null;
                    }
                    return filtered[0];
                };
                /** Returns collection of registered callbacks and resources ordered by position from the top */
                Resources.prototype.getOrdered = function (onlyVisible, withWait) {
                    var ordered = this.items;
                    if (withWait) {
                        ordered = ordered.filter(function (e) { return e.withWait; });
                    }
                    else {
                        ordered = ordered.filter(function (e) { return e.withWait !== true; });
                    }
                    if (onlyVisible) {
                        ordered = ordered.filter(function (e) { return e.isVisible; });
                    }
                    return ordered.sort(VisibilityObservation.ResourceByRowsComparator.instance.compare);
                };
                Resources.prototype.getAllOrdered = function () {
                    return this.items.sort(VisibilityObservation.ResourceByRowsComparator.instance.compare);
                };
                Resources.prototype.add = function (resource) {
                    VisibilityObservation.Require.notNull(resource, 'resource');
                    this.items.push(resource);
                };
                Object.defineProperty(Resources.prototype, "count", {
                    get: function () {
                        return this.items.length;
                    },
                    enumerable: true,
                    configurable: true
                });
                Resources.prototype.removeByResourceId = function (resourceId) {
                    VisibilityObservation.Require.notEmptyString(resourceId, 'resourceId');
                    this._items = this.items.filter(function (v) { return v.resourceId !== resourceId; });
                };
                Resources.prototype.clear = function () {
                    this._items = [];
                };
                return Resources;
            }());
            VisibilityObservation.Resources = Resources;
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var Settings = /** @class */ (function () {
                function Settings() {
                    /** Indicates if the lazy loading is enabled */
                    this.isEnabled = true;
                    /** How long the observer will wait for the load end notification */
                    this.asyncTimeout = 3000;
                    /** Number or maximum resources loading in parallel */
                    this.concurrencyLevel = 3;
                    /** Indicates if lazy loading runs in debug mode */
                    this.debugMode = false;
                }
                return Settings;
            }());
            VisibilityObservation.Settings = Settings;
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="settings.ts"/>
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var WindowWrap = /** @class */ (function () {
                function WindowWrap() {
                }
                WindowWrap.prototype.attachToWindowOnLoad = function (callback) {
                    var anyWindow = window;
                    if (anyWindow.attachEvent) {
                        anyWindow.attachEvent('onload', callback);
                    }
                    else {
                        if (anyWindow.onload) {
                            var currentOnLoad_1 = anyWindow.onload;
                            var newOnLoad = function (evt) {
                                currentOnLoad_1(evt);
                                callback(evt);
                            };
                            anyWindow.onload = newOnLoad;
                        }
                        else {
                            anyWindow.onload = callback;
                        }
                    }
                };
                return WindowWrap;
            }());
            VisibilityObservation.WindowWrap = WindowWrap;
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="settings.ts"/>
/// <reference path="settingsProvider.ts"/>
/// <reference path="intersectionObserverWrap.ts"/>
/// <reference path="resources.ts"/>
/// <reference path="resourceElementParser.ts"/>
/// <reference path="require.ts"/>
/// <reference path="windowWrap.ts"/>
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var VisibilityObserverImpl = /** @class */ (function () {
                function VisibilityObserverImpl(settingsProvider, intersectionObserverWrap, resourceElementParser, windowWrap) {
                    var _this = this;
                    this.globalCallbackCounter = 0;
                    this.toActivate = new VisibilityObservation.Resources();
                    this.currentlyLoading = new VisibilityObservation.Resources();
                    this.pageLoaded = false;
                    this.deferredLoadingFinished = false;
                    this.intersectionObserverInitialized = false;
                    this.globalCallback = function (entries) {
                        try {
                            _this.globalCallbackCounter++;
                            var sessionId_1 = _this.globalCallbackCounter;
                            _this.logFilter(function () { return console.log("IntersectionObserver callback. Session ID: " + sessionId_1); });
                            for (var _i = 0, entries_1 = entries; _i < entries_1.length; _i++) {
                                var entry = entries_1[_i];
                                try {
                                    _this.checkEntry(entry, sessionId_1);
                                }
                                catch (e) {
                                    console.error('Error checking visibility of an entry.', entry, e);
                                }
                            }
                            _this.activateQueues(sessionId_1);
                            if (!_this.intersectionObserverInitialized) {
                                _this.intersectionObserverInitialized = true;
                                _this.checkDeferredLoading();
                            }
                        }
                        catch (e) {
                            console.error('Error handling the intersection observer event.', e);
                        }
                    };
                    this.activateOne = function (sessionId) {
                        var ordered = _this.toActivate.getOrdered(true, false);
                        if (ordered.length > 0) {
                            var first_1 = ordered[0];
                            _this.logFilter(function () {
                                return console.log("Activating one element synchronously. Session ID: " + sessionId, first_1);
                            });
                            _this.initResource(first_1, sessionId);
                        }
                        var activateOneHandle = _this.activateOne;
                        if (ordered.length > 1) {
                            setTimeout(function () {
                                activateOneHandle(sessionId);
                            }.bind(_this), 10);
                        }
                        else if (ordered.length === 1) { // it was the last one visible
                            _this.checkDeferredLoading();
                        }
                    };
                    this.loadingCompleted = function (resourceId) {
                        if (!_this.browserCompatibilityCheck()) {
                            return;
                        }
                        if (_this.settings != null && !_this.settings.isEnabled) {
                            return;
                        }
                        if (resourceId == null) {
                            _this.logFilter(function () {
                                return console.warn('The resource ID can\'t be null, undefined or empty.');
                            });
                            return;
                        }
                        try {
                            var entry_1 = _this.currentlyLoading.getByResourceId(resourceId);
                            if (entry_1 != null) {
                                _this.logFilter(function () {
                                    return console.log("Resource '" + entry_1.title + "' loaded.");
                                });
                                clearTimeout(entry_1.asyncTimeoutHandle);
                                _this.currentlyLoading.removeByResourceId(resourceId);
                                _this.scheduleAsync(entry_1.sessionId);
                            }
                            else {
                                _this.logFilter(function () {
                                    return console.warn("Resource with ID " + resourceId + " is already loaded.");
                                });
                            }
                            _this.checkDeferredLoading();
                        }
                        catch (e) {
                            console.error("Error notifying that loading of resource with ID " + resourceId + " is completed.", e);
                        }
                    };
                    this.loadingElementCompleted = function (element) {
                        if (!_this.browserCompatibilityCheck()) {
                            return;
                        }
                        if (_this.settings != null && !_this.settings.isEnabled && !_this.settings.debugMode) {
                            return;
                        }
                        if (element == null) {
                            _this.logFilter(function () {
                                return console.warn('The element can\'t be null, undefined or empty.');
                            });
                            return;
                        }
                        var resourceId = _this.resourceElementParser.getResourceIdFromElement(element);
                        if (resourceId == null) {
                            _this.logFilter(function () {
                                return console.error('Couldn\'t find the resource ID in the provided element.', element);
                            });
                            return;
                        }
                        _this.loadingCompleted(resourceId);
                    };
                    this.browserCompatibilityCheck = function () {
                        var isCompatibile = false;
                        if (_this.checkIntersectionObserverSupport() && _this.checkSupportForAsyncCalls()) {
                            isCompatibile = true;
                        }
                        return isCompatibile;
                    };
                    this.checkIntersectionObserverSupport = function () {
                        if ('IntersectionObserver' in Window) {
                            _this.logFilter(function () { return console.warn('IntersectionObserver is not supported in this browser.'); });
                            return false;
                        }
                        return true;
                    };
                    this.checkSupportForAsyncCalls = function () {
                        var isAsync = false;
                        try {
                            // tslint:disable-next-line:no-eval
                            eval('async () => {}');
                            isAsync = true;
                        }
                        catch (e) {
                            _this.logFilter(function () { return console.warn('Async calls are forbidden in this browser.'); });
                            if (e instanceof SyntaxError) {
                                _this.logFilter(function () { return console.warn('Please check if your browser support it.'); });
                            }
                            else {
                                _this.logFilter(function () { return console.warn('Please check your Content Security Policy rules.'); });
                                throw e; // throws Content Security Policy error
                            }
                            isAsync = false;
                        }
                        return isAsync;
                    };
                    this.checkDeferredLoading = function () {
                        if (_this.settings != null && _this.settings.isEnabled // Lazy loading is enabled
                            && _this.pageLoaded // and page is loaded
                            && !_this.deferredLoadingFinished // and it wasn't fired
                            && _this.currentlyLoading.count === 0 // and nothing visible is loading
                            && _this.intersectionObserverInitialized) { // and the IntersectionObserver is initialized
                            _this.logFilter(function () {
                                return console.log('Starting deferred loading');
                            });
                            var ordered = _this.toActivate.getAllOrdered();
                            for (var _i = 0, ordered_1 = ordered; _i < ordered_1.length; _i++) {
                                var resource = ordered_1[_i];
                                _this.initResource(resource, -2);
                            }
                            _this.deferredLoadingFinished = true;
                        }
                    };
                    VisibilityObservation.Require.notNull(settingsProvider, 'settingsProvider');
                    VisibilityObservation.Require.notNull(intersectionObserverWrap, 'intersectionObserverWrap');
                    VisibilityObservation.Require.notNull(resourceElementParser, 'resourceElementParser');
                    VisibilityObservation.Require.notNull(windowWrap, 'windowWrap');
                    this.settingsProvider = settingsProvider;
                    this.intersectionObserverWrap = intersectionObserverWrap;
                    this.resourceElementParser = resourceElementParser;
                    this.windowWrap = windowWrap;
                    this.windowWrap.attachToWindowOnLoad(function (c) {
                        _this.pageLoaded = true;
                        _this.checkDeferredLoading();
                    });
                    // initialize with default settings, will be replaced by centralized settings after init()
                    // some methods can be run before init() and settings are required for peoper work
                    this.settings = new SW.Core.VisibilityObservation.Settings();
                }
                VisibilityObserverImpl.prototype.checkEntry = function (entry, sessionId) {
                    var resourceId = this.resourceElementParser.getResourceIdFromElement(entry.target);
                    if (resourceId == null) {
                        this.logFilter(function () { return console.warn('Couldn\'t find any resource ID in the provied DOM element.', entry.target); });
                        return;
                    }
                    var resource = this.toActivate.getByResourceId(resourceId);
                    if (resource != null) {
                        resource.isVisible = entry.intersectionRatio > 0;
                        this.logFilter(function () {
                            return console.log("Visibility of resource '" + resource.title + "': " + resource.isVisible + ". Session ID: " + sessionId);
                        });
                    }
                };
                VisibilityObserverImpl.prototype.activateQueues = function (sessionId) {
                    this.logFilter(function () {
                        return console.log("Activating queues for session ID " + sessionId);
                    });
                    this.scheduleAsync(sessionId);
                    this.activateOne(sessionId);
                };
                VisibilityObserverImpl.prototype.runCallback = function (resource) {
                    try {
                        resource.callback();
                    }
                    catch (e) {
                        console.error("Error running the resource '" + resource.title + "' callback.", e);
                    }
                };
                VisibilityObserverImpl.prototype.initResource = function (resource, sessionId) {
                    this.logFilter(function () {
                        return console.log("Start to load resource '" + resource.title + "'. Session ID: " + sessionId);
                    });
                    this.intersectionObserverWrap.unobserve(resource.element);
                    this.toActivate.removeByResourceId(resource.resourceId);
                    this.runCallback(resource);
                };
                VisibilityObserverImpl.prototype.scheduleAsync = function (sessionId) {
                    var ordered = this.toActivate.getOrdered(true, true);
                    var _loop_1 = function (resource) {
                        if (this_1.currentlyLoading.count < this_1.settings.concurrencyLevel) {
                            this_1.logFilter(function () {
                                return console.log("Scheduling asynchronously resource '" + resource.title + "'", resource);
                            });
                            resource.sessionId = sessionId;
                            var loadedHandle_1 = this_1.loadingCompleted;
                            resource.asyncTimeoutHandle = setTimeout(function () {
                                this.logFilter(function () {
                                    return console.warn("Timeout of loading resource '" + resource.title + "'");
                                });
                                loadedHandle_1(resource.resourceId);
                            }.bind(this_1), this_1.settings.asyncTimeout);
                            this_1.currentlyLoading.add(resource);
                            this_1.initResource(resource, sessionId);
                        }
                    };
                    var this_1 = this;
                    for (var _i = 0, ordered_2 = ordered; _i < ordered_2.length; _i++) {
                        var resource = ordered_2[_i];
                        _loop_1(resource);
                    }
                };
                VisibilityObserverImpl.prototype.buildResource = function (resourceId, element, callback, withWait) {
                    var resource = new VisibilityObservation.Resource();
                    resource.resourceId = resourceId;
                    resource.callback = callback;
                    resource.element = element;
                    resource.isVisible = false;
                    resource.rowIndex = this.resourceElementParser.getRowIndex(element);
                    resource.title = this.resourceElementParser.getTitleFromElement(element);
                    resource.withWait = withWait;
                    return resource;
                };
                // can be called before init()
                VisibilityObserverImpl.prototype.registerByResourceId = function (resourceId, callback, willNotifyOnEnd) {
                    try {
                        VisibilityObservation.Require.notEmptyString(resourceId, 'resourceId');
                        VisibilityObservation.Require.notNull(callback, 'callback');
                        if (!this.browserCompatibilityCheck()) {
                            callback();
                            return;
                        }
                        if (this.settings != null && !this.settings.isEnabled) {
                            callback();
                            return;
                        }
                        if (this.deferredLoadingFinished) {
                            this.logFilter(function () { return console.warn('The deferred loading is finished. Invoking now...'); });
                            callback();
                            return;
                        }
                        this.logFilter(function () { return console.log("Registering resource " + resourceId); });
                        var element = this.resourceElementParser.getElementByResourceId(resourceId);
                        if (element == null) {
                            console.error("Couldn't find the resource with ID " + resourceId);
                            return;
                        }
                        var resource = this.buildResource(resourceId, element, callback, willNotifyOnEnd);
                        this.toActivate.add(resource);
                        this.intersectionObserverWrap.observe(element);
                    }
                    catch (e) {
                        console.error('Error registering a resource for lazy loading.', e);
                    }
                };
                // can be called before init()
                VisibilityObserverImpl.prototype.registerElement = function (element, callback, willNotifyOnEnd) {
                    try {
                        VisibilityObservation.Require.notNull(element, 'element');
                        VisibilityObservation.Require.notNull(callback, 'callback');
                        if (!this.browserCompatibilityCheck()) {
                            callback();
                            return;
                        }
                        if (this.settings != null && !this.settings.isEnabled) {
                            callback();
                            return;
                        }
                        if (this.deferredLoadingFinished) {
                            this.logFilter(function () { return console.warn('The deferred loading is finished. Invoking now...'); });
                            callback();
                            return;
                        }
                        var resourceId_1 = this.resourceElementParser.getResourceIdFromElement(element);
                        if (resourceId_1 == null) {
                            console.error('Couldn\'t find the resource ID in the provided element.', element);
                            return;
                        }
                        this.logFilter(function () { return console.log("Registering element with resource ID " + resourceId_1); });
                        var resource = this.buildResource(resourceId_1, element, callback, willNotifyOnEnd);
                        this.toActivate.add(resource);
                        this.intersectionObserverWrap.observe(element);
                    }
                    catch (e) {
                        console.error('Error registering a resource for lazy loading.', e);
                    }
                };
                VisibilityObserverImpl.prototype.init = function () {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, _i, _b, resource, e_1;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    _c.trys.push([0, 2, , 3]);
                                    _a = this;
                                    return [4 /*yield*/, this.settingsProvider.getSettingsAsync()];
                                case 1:
                                    _a.settings = _c.sent();
                                    this.logFilter(function () { return console.log('Initializing the visibility observer...'); });
                                    if (!this.settings.isEnabled) {
                                        this.logFilter(function () {
                                            return console.warn('The lazy loading is disabled.');
                                        });
                                        for (_i = 0, _b = this.toActivate.items; _i < _b.length; _i++) {
                                            resource = _b[_i];
                                            this.runCallback(resource);
                                        }
                                        this.toActivate.clear();
                                    }
                                    else {
                                        this.intersectionObserverWrap.init(this.globalCallback);
                                    }
                                    return [3 /*break*/, 3];
                                case 2:
                                    e_1 = _c.sent();
                                    console.error('Error initializing the resources lazy loading.', e_1);
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    });
                };
                VisibilityObserverImpl.prototype.logFilter = function (action) {
                    if (this.settings != null && this.settings.debugMode) {
                        action();
                    }
                };
                return VisibilityObserverImpl;
            }());
            VisibilityObservation.VisibilityObserverImpl = VisibilityObserverImpl;
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var Orion;
            (function (Orion) {
                var JQueryXHRWrap = /** @class */ (function () {
                    function JQueryXHRWrap(xhr) {
                        this.xhr = xhr;
                    }
                    JQueryXHRWrap.prototype.fail = function (callback) {
                        var newXhr = this.xhr.fail(callback);
                        return new JQueryXHRWrap(newXhr);
                    };
                    JQueryXHRWrap.prototype.done = function (callback) {
                        var newXhr = this.xhr.done(callback);
                        return new JQueryXHRWrap(newXhr);
                    };
                    return JQueryXHRWrap;
                }());
                var JQueryWrap = /** @class */ (function () {
                    function JQueryWrap() {
                    }
                    JQueryWrap.prototype.ajax = function (settings) {
                        var xhr = $.ajax(settings);
                        return new JQueryXHRWrap(xhr);
                    };
                    return JQueryWrap;
                }());
                Orion.JQueryWrap = JQueryWrap;
            })(Orion = VisibilityObservation.Orion || (VisibilityObservation.Orion = {}));
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="..\settings.ts"/>
/// <reference path="..\settingsProvider.ts"/>
/// <reference path="jqueryWrap.ts"/>
/// <reference path="..\require.ts"/>
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var Orion;
            (function (Orion) {
                Orion.SWIS_CENTRALIZED_SETTINGS_CALL_SETTINGS = {
                    type: 'POST',
                    url: '/Orion/Services/Information.asmx/Query',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                };
                var swqlCentralizedSettings = "\n    SELECT Name, Value\n    FROM Orion.Setting\n    WHERE NAME IN (\n        'VisibilityObserver.IsEnabled',\n        'VisibilityObserver.DebugMode',\n        'VisibilityObserver.AsyncTimeout',\n        'VisibilityObserver.ConcurrencyLevel')";
                var OrionSettingsProvider = /** @class */ (function () {
                    function OrionSettingsProvider(jquery) {
                        VisibilityObservation.Require.notNull(jquery, 'jquery');
                        this.jquery = jquery;
                    }
                    OrionSettingsProvider.prototype.getSettingsAsync = function () {
                        var _this = this;
                        var deferred = $.Deferred();
                        var ajax = this.fetchCentralizedSettings(Orion.SWIS_CENTRALIZED_SETTINGS_CALL_SETTINGS);
                        ajax.done(function (data) {
                            console.log('Got settings from SWIS', data);
                            var setting = _this.parseSettings(data.d.Rows);
                            deferred.resolve(setting);
                        }).fail(function (data) {
                            console.error('Query ajax error', data);
                            deferred.resolve(_this.createDefaultSettings());
                        });
                        return deferred.promise();
                    };
                    OrionSettingsProvider.prototype.fetchCentralizedSettings = function (settings) {
                        settings.data = JSON.stringify({ query: swqlCentralizedSettings });
                        return this.jquery.ajax(settings);
                    };
                    OrionSettingsProvider.prototype.parseSettings = function (rows) {
                        var settings = this.createDefaultSettings();
                        for (var i = 0; i < rows.length; i++) {
                            switch (rows[i][0]) {
                                case 'VisibilityObserver.IsEnabled':
                                    settings.isEnabled = (rows[i][1]).toLowerCase() === 'true';
                                    break;
                                case 'VisibilityObserver.AsyncTimeout':
                                    settings.asyncTimeout = parseInt(rows[i][1], 10);
                                    break;
                                case 'VisibilityObserver.ConcurrencyLevel':
                                    settings.concurrencyLevel = parseInt(rows[i][1], 10);
                                    break;
                                case 'VisibilityObserver.DebugMode':
                                    settings.debugMode = (rows[i][1]).toLowerCase() === 'true';
                                    break;
                            }
                        }
                        return settings;
                    };
                    OrionSettingsProvider.prototype.createDefaultSettings = function () {
                        return new VisibilityObservation.Settings();
                    };
                    return OrionSettingsProvider;
                }());
                Orion.OrionSettingsProvider = OrionSettingsProvider;
            })(Orion = VisibilityObservation.Orion || (VisibilityObservation.Orion = {}));
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="../resourceElementParser.ts"/>
/// <reference path="../require.ts"/>
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        var VisibilityObservation;
        (function (VisibilityObservation) {
            var Orion;
            (function (Orion) {
                Orion.resourceIdAttributeName = 'ResourceId';
                Orion.resourceTitleAttributeName = 'sw-widget-title';
                var OrionResourceElementParser = /** @class */ (function () {
                    function OrionResourceElementParser() {
                    }
                    OrionResourceElementParser.prototype.getResourceIdFromElement = function (element) {
                        VisibilityObservation.Require.notNull(element, 'element');
                        return element.getAttribute(Orion.resourceIdAttributeName);
                    };
                    OrionResourceElementParser.prototype.getElementByResourceId = function (resourceId) {
                        VisibilityObservation.Require.notEmptyString(resourceId, 'resourceId');
                        return document.querySelector("[" + Orion.resourceIdAttributeName + "='" + resourceId + "']");
                    };
                    OrionResourceElementParser.prototype.getTitleFromElement = function (element) {
                        VisibilityObservation.Require.notNull(element, 'element');
                        return element.getAttribute(Orion.resourceTitleAttributeName);
                    };
                    OrionResourceElementParser.prototype.getRowIndex = function (element) {
                        var resourceWrapperParent = element.parentNode;
                        var columnElement = resourceWrapperParent.parentNode;
                        var rows = columnElement.childNodes;
                        var rowIndex = 0;
                        for (rowIndex; rowIndex < rows.length; rowIndex++) {
                            if (rows[rowIndex] === resourceWrapperParent) {
                                break;
                            }
                        }
                        return rowIndex;
                    };
                    return OrionResourceElementParser;
                }());
                Orion.OrionResourceElementParser = OrionResourceElementParser;
            })(Orion = VisibilityObservation.Orion || (VisibilityObservation.Orion = {}));
        })(VisibilityObservation = Core.VisibilityObservation || (Core.VisibilityObservation = {}));
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
/// <reference path="orionSettingsProvider.ts"/>
/// <reference path="orionResourceElementParser.ts"/>
/// <reference path="..\intersectionObserverWrap.ts"/>
/// <reference path="..\visibilityObserver.ts"/>
/// <reference path="jqueryWrap.ts"/>
var SW;
(function (SW) {
    var Core;
    (function (Core) {
        Core.VisibilityObserver = buildOrionVisibilityObserver();
        document.addEventListener('DOMContentLoaded', function () {
            if (Core.VisibilityObserver != null) {
                if (Core.VisibilityObserver.browserCompatibilityCheck()) {
                    Core.VisibilityObserver.init();
                }
                else {
                    console.warn('Lazy loading is disabled due to failed compatibility check with browser.');
                }
            }
        });
        function buildOrionVisibilityObserver() {
            var settingsProvider = new SW.Core.VisibilityObservation.Orion.OrionSettingsProvider(new SW.Core.VisibilityObservation.Orion.JQueryWrap());
            return new SW.Core.VisibilityObservation.VisibilityObserverImpl(settingsProvider, new SW.Core.VisibilityObservation.IntersectionObserverWrap(), new SW.Core.VisibilityObservation.Orion.OrionResourceElementParser(), new SW.Core.VisibilityObservation.WindowWrap());
        }
    })(Core = SW.Core || (SW.Core = {}));
})(SW || (SW = {}));
//# sourceMappingURL=visibilityObserver.js.map