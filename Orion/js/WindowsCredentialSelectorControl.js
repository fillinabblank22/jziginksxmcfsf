﻿SW.Core.namespace("SW.Core").WindowsCredentialSelectorSettings = function () {
    var config, useUserElement, notUserElement;

    credentialElements = getElements(["credName", "userName", "password", "confirmPassword"]);
    credNameElement = getElement("credName");
    userNameElement = getElement("userName");
    passwordElement = getElement("password");
    confirmPasswordElement = getElement("confirmPassword");
    credentialsElement = getElement("credentialsSelect");

    function isUserName(userName) {
        var regex = /^[^\/:*?"<>|]+$/;
        return regex.test(userName);
    }

    function validateCredName(value, elem) {
        var validationResult = true;
        if (!isUserName($.trim(value))) {
            validationResult = false;
            elem.addClass("invalidValue");
            $("#requiredCredentialName").show();
        }
        return validationResult;
    }

    function validateUserName(value, elem) {
        var validationResult = true;
        if (!isUserName($.trim(value))) {
            validationResult = false;
            elem.addClass("invalidValue");
            $("#requiredUserName").show();
        }
        return validationResult;
    }

    function isPassword(password) {
        var validationResult = true;
        if (password.length == 0)
            validationResult = false;
        return validationResult;
    }

    function validateUserPassword(password, passwordElem, confirmPassword, confirmPasswordElem) {
        var validationResult = true;
        if ($.trim(password) != $.trim(confirmPassword)) {
            validationResult = false;
            passwordElem.addClass("invalidValue");
            confirmPasswordElem.addClass("invalidValue");
            $("#compareConfirmPassword").show();
        } else {
            if (!isPassword($.trim(password))) {
                validationResult = false;
                passwordElem.addClass("invalidValue");
                $("#requiredPassword").show();
            }
            if (!isPassword($.trim(confirmPassword))) {
                validationResult = false;
                confirmPasswordElem.addClass("invalidValue");
                $("#requiredConfirmPassword").show();
            }
        }
        return validationResult;
    }

    function getElement(dataKey) {
        return $('[data-form="' + dataKey + '"]');
    }

    function getElements(dataKeys) {

        return $(dataKeys.map(function (x) { return '[data-form="' + x + '"]'; }).join(', '));
    }

    function showHideCredentialsMessage(result) {
        $('#testProgress').hide();
        if (result) {
            $('#testResultSuccess').show();
        } else {
            $('#testResultFailed').show();
        }
    }

    function clearValidationState() {
        credentialElements.removeClass("invalidValue");
        $("#requiredCredentialName, #requiredUserName, #requiredPassword, " +
            "#requiredConfirmPassword, #compareConfirmPassword, #credentialNameAlreadyExists").hide();
    };

    this.init = function () {
        this.radioSelectionChange($('input[type="radio"]:checked'));
        useDefaultUserElement = $("input[id*='useDefaultUser']");
        useDefinedUserElement = $("input[id*='useDefinedUser']");
    };

    this.setConfig = function (configuration) {
        config = configuration;

        if ($("[id$='useDefinedUser']").attr("checked") == "checked") {
            $("[id$='useDefinedUser']").click();
        }
    };

    this.ValidateCredentials = function () {
        var valid = true;
        clearValidationState();
        var credentials = this.getCredentials();
        if (credentials != null) {
            valid = validateCredName(credentials.Name, credNameElement) && valid;
            valid = validateUserName(credentials.Username, userNameElement) && valid;
            valid = validateUserPassword(credentials.Password, passwordElement,
                credentials.ConfirmPassword, confirmPasswordElement) && valid;
        }
        return valid;
    };

    this.getCredentials = function () {
        if (this.isCredentialsDefined()) {
            return {
                ID: credentialsElement.val(),
                Name: $.trim(credNameElement.val()),
                Username: $.trim(userNameElement.val()),
                Password: $.trim(passwordElement.val()),
                ConfirmPassword: $.trim(confirmPasswordElement.val())
            };
        } else {
            return null;
        }
    };

    this.getWindowsCredential = function (credentialId) {
        var cred;
        if (credentialId) {
            SW.Core.Services.callControllerSync("/Orion/Services/CredentialManagerService.asmx/GetWindowsCredential",
                {
                    credentialId: credentialId
                },
                function (result) {
                    cred = result.d;
                });
        };

        return cred;
    };


    this.addUpdateWindowsCredential = function () {
        var creds = this.getCredentials();
        var id = "0";
        if (creds != null) {
            SW.Core.Services.callControllerSync("/Orion/Services/CredentialManagerService.asmx/AddUpdateWindowsCredential",
                {
                    credentials: creds
                },
                function (result) {
                    id = result.d;
                });
        };

        return id;
    };

    this.radioSelectionChange = function (elem) {
        if ($(elem).attr('value') == "1") {
            $("#userCredentials").show();
        }
        else {
            $("#userCredentials").hide();
        };
    };

    this.radioDisabledChange = function (state) {
        useDefaultUserElement.attr("disabled", state);
        useDefinedUserElement.attr("disabled", state);
        if (state) {
            useDefinedUserElement.attr("checked", state);
            $("#userCredentials").hide();
        }
    };

    this.testCredentials = function () {
        var configuration = config.GetConfiguration();
        config.ValidateConfiguration(configuration, function (isValid) {
            $('#testResultSuccess').hide();
            $('#testResultFailed').hide();
            if (isValid) {
                $('#testProgress').show();
                $('#testResult').empty();
                SW.Core.Services.callController(config.TestCredentialsRoute, configuration,
                    // On success
                    function (response) {
                        $('#testProgress').hide();
                        showHideCredentialsMessage(response);
                        clearValidationState();
                    },
                    // On error
                    function (msg) {
                        alert(msg);
                        $('#testProgress').hide();
                    });
            }
        });

    };

    this.ValidateCredentialName = function () {
        var configuration = config.GetConfiguration();

        if (!this.isCredentialsDefined()
            || configuration.Credentials.ID > 0
            || configuration.Credentials.Name == "") {
            return true;
        }

        var isValid = false;

        $('#testProgress').show();
        SW.Core.Services.callControllerSync(config.ValidateCredentialNameRoute, configuration,
            // On success
            function (result) {
                $('#testProgress').hide();
                if (result) {
                    $("#credentialNameAlreadyExists").hide();
                    getElement("credName").removeClass("invalidValue");
                    isValid = true;
                } else {
                    $("#credentialNameAlreadyExists").show();
                    getElement("credName").addClass("invalidValue");
                }
            },
            // On error
            function (msg) {
                alert(msg);
                $('#testProgress').hide();
            });

        return isValid;
    };

    this.isCredentialsDefined = function () {
        return useDefinedUserElement.is(':checked');
    }

    this.credentialChange = function (elem) {
        clearValidationState();

        var credSelectElem = elem;
        var isNewCredential = credSelectElem.value == 0;
        credentialElements.prop("disabled", !isNewCredential);

        if (isNewCredential) {
            credentialElements.removeClass("aspNetDisabled disabled");
        } else {
            credentialElements.addClass("aspNetDisabled disabled");
        }

        var cred;
        if (!isNewCredential) {
            cred = this.getWindowsCredential(credSelectElem.value);
        }

        var useCred = !isNewCredential && cred;
        credNameElement.attr("value", useCred ? cred.Name : "");
        userNameElement.attr("value", useCred ? cred.Username : "");
        passwordElement.attr("value", useCred ? cred.Password : "");
        confirmPasswordElement.attr("value", useCred ? cred.Password : "");
    }
};
