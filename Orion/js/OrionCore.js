﻿var tmp_orion_core_obj = {
    prefix: "_not_set_",

    Prefs: {
        load: function (prefName, defaultValue) {
            return $("#" + ORION.prefix + prefName).val() || defaultValue;
        },
        save: function (prefName, value) {
            var name = ORION.prefix + prefName;
            $("#" + name).val(value);
            ORION.callWebService("/Orion/Services/NodeManagement.asmx",
                                 "SaveUserSetting", { name: name, value: value },
                                 function (result) {
                                     // do nothing.
                                 });
        }
    },

    clearError: function () {
        $("#originalQuery,#test,#stackTrace").text('');
    },

    // obsolete: Use the same method from SW.Core.Services
    handleError: function (xhr, customErrorHandler) {
        if (xhr.status == 401 || xhr.status == 403) {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
            window.location.reload();
        }
        var errorText = xhr.responseText;
        var msg;
        try {
            var error = eval("(" + errorText + ")");
            msg = error.Message;
            $("#stackTrace").text(error.StackTrace);
        }
        catch (err) {
            msg = err.description;
        }
        $("#test").text(msg);
        if (typeof (customErrorHandler) == 'function') {
            customErrorHandler(msg);
        }
    },

    // obsolete: Use the same method from SW.Core.Services
    callWebService: function (serviceName, methodName, param, onSuccess, onError) {
        var paramString = JSON.stringify(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: serviceName + "/" + methodName,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                onSuccess(msg.d);
            },
            error: function (xhr, status, errorThrown) {
                //ORION.handleError(xhr, onError);
                SW.Core.Services.handleError(xhr, onError);
            }
        });
    },

    objectFromDataTable: function (result) {
        var table = [];
        for (var y = 0; y < result.Rows.length; ++y) {
            var row = result.Rows[y];
            var tableRow = {};
            for (var x = 0; x < result.Columns.length; ++x) {
                tableRow[result.Columns[x]] = row[x];
            }
            table.push(tableRow);
        }
        return { Rows: table };
    },
    // obsolete: Use the same method from SW.Core.Services
    postToTarget: function (targetPage, data) {
        var formTag = String.format('<form method="post" action="{0}"></form>', targetPage);
        var form = $(formTag);

        for (var prop in data) {
            var inputTag = String.format('<input type="hidden" name="{0}" id="{0}" />', prop);
            var jsonData = JSON.stringify(data[prop]);
            form.append($(inputTag).val(jsonData));
        }

        $(form).appendTo('body').submit();
    }


};

var GetObjectStatusText = function (statusValue) {
    return SW.Core.Status.Get(statusValue).ShortDescription;
}

if ((typeof Ext) != "undefined") {
    //
    // Simple wrapper for ASP.NET Pageable DataStore.
    //
    // Sample Usage:
    //
    //        var dataStore = new ORION.WebServiceStore(
    //                "/Orion/APM/Services/Applications.asmx/GetApplicationsPaged",
    //                [
    //	                { name: 'ApplicationID', mapping: 0 },
    //	                { name: 'Name', mapping: 1 },
    //	                { name: 'NodeName', mapping: 2 },
    //	                { name: 'TemplateName', mapping: 3 }
    //                ],
    //                "Name"
    //            );
    //
    //
    tmp_orion_core_obj.WebServiceStore = function (url, readerFields, sortOrderField) {
        var c = {};

        ORION.WebServiceStore.superclass.constructor.call(this, Ext.apply(c, {
            proxy: new Ext.data.HttpProxy({
                url: url,
                method: "POST",
                timeout: 90000,
                failure: function (xhr, options) {
                   //ORION.handleError(xhr);
                    SW.Core.Services.handleError(xhr);
                }
            }),
            reader: new Ext.data.JsonReader({
                totalProperty: "d.TotalRows",
                root: "d.DataTable.Rows"
            },
            readerFields
        ),

            remoteSort: true,
            sortInfo: {
                field: sortOrderField,
                direction: "ASC"
            }
        }));
    };
    Ext.extend(tmp_orion_core_obj.WebServiceStore, Ext.data.Store);

    tmp_orion_core_obj.WebApiStore = function (url, readerFields, sortOrderField) {
        var c = {};

        ORION.WebApiStore.superclass.constructor.call(this, Ext.apply(c, {
            proxy: new Ext.data.HttpProxy({
                url: url,
                method: "POST",
                timeout: 900000,
                failure: function (xhr, options) {
                    //ORION.handleError(xhr);
                    SW.Core.Services.handleError(xhr);
                }
            }),
            reader: new Ext.data.JsonReader({
                totalProperty: "TotalRows",
                root: "DataTable.Rows"
            },
            readerFields
        ),

            remoteSort: true,
            sortInfo: {
                field: sortOrderField,
                direction: "ASC"
            }
        }));
    };
    Ext.extend(tmp_orion_core_obj.WebApiStore, Ext.data.Store);

    // Encoding value function
    tmp_orion_core_obj.encodeHTML = function (value) {
        return value == null ? '' : Ext.util.Format.htmlEncode(value);
    };
};

if (typeof (window.ORION) == "undefined") {
    window.ORION = tmp_orion_core_obj;
} else {
    window.ORION = $.extend(window.ORION, tmp_orion_core_obj);
}

