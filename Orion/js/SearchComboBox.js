﻿var searchComboBox = SW.Core.namespace("SW.Core.SearchComboBox");

Ext.form.SearchComboBox = Ext.extend(Ext.form.ComboBox,
{
    clearFilterOnReset: true,
    triggerAction: 'all',
    mode: 'local',
    lazyInit: false,
    setValue: function (value) {
        Ext.form.SearchComboBox.superclass.setValue.call(this, value);
        var field = $('#' + this.id);
        field.attr('ext:qtip', field.val());
    },
    initList: function () {
        var searchId = "sw-combobox-search-" + Ext.id();
        this.tpl = new Ext.XTemplate(
            '<tpl exec="this.clearGroups()"></tpl>',
            '<tpl for=".">',
                '<tpl if="visible==true">',
                    '<tpl exec="this.increaseGroupCount(values.groupName, values.data)"></tpl>',
                '</tpl>',
            '</tpl>',

            '<tpl for=".">',
                '<tpl if="this.groupName != values.groupName && (visible==true || data ==  null)">',
                    '<tpl exec="this.groupName = values.groupName"></tpl>',
                    '<tpl exec="this.rowsCount = this.getGroupCount(groupName)"></tpl>',
                    '<div class="x-combo-group-item <tpl if="xindex == 1">x-combo-group-item-first</tpl>" onclick="if (event && event.stopPropagation) event.stopPropagation(); else if (window.event) window.event.cancelBubble = true; return false;">{groupName} ({this.rowsCount})</div>',
                '</tpl>',

                '<tpl if="values.data == null ">',
                    '<div class="x-combo-group-item x-combo-group-item-first" onclick="if (event && event.stopPropagation) event.stopPropagation(); else if (window.event) window.event.cancelBubble = true; return false;" >{values.displayName}</div>',
                '</tpl>',

                '<div class="x-combo-list-item <tpl if="searchable==true">searchable</tpl> " <tpl if="visible==false || data ==  null">style="display: none;"</tpl> ext:qtip="{values.displayName}">{' + this.displayField + '}</div>',
            '</tpl>',
            {
                compiled: true,
                disableFormats: true,
                groups: {},
                clearGroups: function () { this.groups = {}; },
                increaseGroupCount: function (group, data) {
                    if (this.groups[group]) this.groups[group] = this.groups[group] + 1;
                    else if (data == null) this.groups[group] = 0;
                    else this.groups[group] = 1;
                },
                getGroupCount: function(group) {
                    if (this.groups[group]) return this.groups[group];
                    else return 0;
                }
            }
        );

        Ext.form.SearchComboBox.superclass.initList.call(this);
        if (this.list) {
            $("#" + this.list.dom.id).prepend($('<div class="x-combo-search-item x-form-field-wrap x-form-field-trigger-wrap" id="' + searchId + '">'));
            this.assetHeight += 37; // add 37 pixels to compensate search panel
        }

        var comboEl = this;
        var width = this.width ? this.width - 20 : 180;
        var searchTextboxId = 'search-' + Ext.id();
        this.search = new Ext.form.TwinTriggerField({
            initComponent: function() {
                Ext.form.TwinTriggerField.superclass.initComponent.call(this);
                this.triggerConfig = {
                    tag: 'span',
                    cls: 'x-form-twin-triggers',
                    cn: [
                        { tag: "img", src: "/Orion/Admin/Accounts/images/search/clear_button.gif", cls: "x-form-trigger " + this.trigger1Class },
                        { tag: "img", src: "/Orion/Admin/Accounts/images/search/search_button.gif", cls: "x-form-trigger " + this.trigger2Class }
                    ]
                };
                this.on('specialkey', function(f, e) {
                    if (e.getKey() == e.ENTER) {
                        this.onTrigger2Click();
                    }
                }, this);
                // set search field text styles
                this.on('focus', function() {
                    this.el.applyStyles('font-style:normal; background: #FFE89E;');
                }, this);
                // reset search field text style
                this.on('blur', function() {
                    if (Ext.isEmpty(this.el.dom.value) || this.el.dom.value == this.emptyText) {
                        this.el.applyStyles('font-style:italic;  background: white;');
                    }
                }, this);
                this.on('click', function(event) {
                    if (event && event.stopPropagation) event.stopPropagation();
                    else if (window.event) window.event.cancelBubble = true;
                    return false;
                }, this);
            },
            renderTo: searchId,
            id: searchTextboxId,
            validationEvent: false,
            validateOnBlur: false,
            trigger1Class: 'x-form-clear-trigger',
            trigger2Class: 'x-form-search-trigger',
            style: 'font-style: italic',
            emptyText: '@{R=Core.Strings;K=WEBJS_TM0_53;E=js}',
            hideTrigger1: true,
            width: width,
            hasSearch: false,
            // this is the clear (X) icon button in the search field
            onTrigger1Click: function(event) {
                if (this.hasSearch) {
                    this.el.dom.value = '';
                    if (!comboEl.onSearch) {
                        comboEl.searchItems(this.getRawValue());
                    }
                    this.triggers[0].hide();
                    this.hasSearch = false;
                }
                if (event && event.stopPropagation) event.stopPropagation();
                else if (window.event) window.event.cancelBubble = true;
            },
            //  this is the search icon button in the search field
            onTrigger2Click: function(event) {
                //set the global variable for the SQL query and regex highlighting (renderAccountName)
                var filterText = this.getRawValue();
                if (filterText.length < 1) {
                    this.onTrigger1Click();
                    return;
                }
                comboEl.searchItems(this.getRawValue());
                this.hasSearch = true;
                this.triggers[0].show();
                if (event && event.stopPropagation) event.stopPropagation();
                else if (window.event) window.event.cancelBubble = true;
            }
        });

        if (this.list) {
            $("#" + searchTextboxId).css('height', '16px').click(function(event) {
                if (event && event.stopPropagation) event.stopPropagation();
                else if (window.event) window.event.cancelBubble = true;
            });
        }
    },
    searchItems: function(searchVal) {
        if (this.onSearch)
            this.onSearch(searchVal);
        else
        {
            this.onInternalSearch(searchVal);

            $(this.list.dom).find(".x-combo-list-item").each(function() {
                $(this).html($(this).html().replace(/<\/?span[^>]*>/g, ""));
                if (searchVal != '') {
                    if (searchVal != '') {
                        // set regex pattern
                        var x = '((' + searchVal + ')+)\*';
                        var content = $(this).html(), pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

                        // do regex replace + content gets encoded
                        var fieldValue = Ext.util.Format.htmlEncode(content.replace(pattern, replaceWith));

                        // now replace the literal SPAN markers with the HTML span tags
                        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
                        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');
                        $(this).html(fieldValue);
                    }
                }
            });
        }
    },
    collapse: function() {
        Ext.form.SearchComboBox.superclass.collapse.call(this);
        this.search.onTrigger1Click();
        this.search.blur();
    }
});

searchComboBox.GetNewId = function() {
    return Ext.id();
};


searchComboBox.Init = function (config) {
    // HELPERS METHODS

    // Add recently usage fields to DataStorage if enableRecentlyUsed = true
    var addRecentlyUsedToConfigData = function () {
        if (config.recentlyUsedConfig.enableRecentlyUsed) {
            var recentlyUsed = JSON.parse(sessionStorage.getItem(config.sessionKey));
            if (recentlyUsed != null) {
                if (recentlyUsed.length >= 0) {

                    // Remove Recently used items, need for correct refresh
                    config.data = jQuery.grep(config.data, function (value) {
                        return value.groupName != config.recentlyUsedConfig.recentlyUsedCaption;
                    });

                    for (var i = 0; i < recentlyUsed.length; i++) {
                            config.data.push(recentlyUsed[i]);
                    }
                }
            } else {
                // if sessionStorage is empty add Recently Used section config data on first init (FB343987)
                config.data.push({ data: null, displayName: "@{R=Core.Strings.2;K=WEBJS_AY0_6;E=js}", groupName: config.recentlyUsedConfig.recentlyUsedCaption, visible: true, searchable: false }); //None yet...
            }
        }
    };

    var internalOnSearch = function(searchVal) {
        if (searchVal != '') {
                
            for (var i = 0; i < this.store.data.items.length; i++) {
                this.store.data.items[i].data.visible = !this.store.data.items[i].data.searchable || this.store.data.items[i].data.displayName.toLowerCase().indexOf(searchVal.toLowerCase()) > -1;
            }
            this.bindStore(this.store);
        } else {
            this.updateStore(config.data);
        }
    }

    //END HELPERS METHODS

    addRecentlyUsedToConfigData(config);
    this.combobox = new Ext.form.SearchComboBox(
    {
        store: new Ext.data.ArrayStore({
            fields: [
                { name: 'data', mapping: 'data' },
                { name: 'displayName', mapping: 'displayName' },
                { name: 'groupName', mapping: 'groupName' },
                { name: 'visible', mapping: 'visible' },
                { name: 'searchable', mapping: 'searchable' }
            ],
            data: config.data
        }),
        displayField: 'displayName',
        valueField: 'data',
        renderTo: config.containerId,
        id: '_searchComboBox' + Ext.id(),
        emptyText: config.emptyText,
        width: config.width ? config.width : 200,
        editable: config.editable == false ? false : true,
        onSearch: config.onSearch,
        onInternalSearch: internalOnSearch,
        updateStore: function (data) {
            config.data = data;
            this.store = new Ext.data.ArrayStore({
                fields: [
                    { name: 'data', mapping: 'data' },
                    { name: 'displayName', mapping: 'displayName' },
                    { name: 'groupName', mapping: 'groupName' },
                    { name: 'visible', mapping: 'visible' },
                    { name: 'searchable', mapping: 'searchable' }
                ],
                data: data
            });
            this.bindStore(this.store);
        }
    });

    this.combobox.on('select', function () {
        if (config.onSelect) {
            config.onSelect();
        }
        if (config.recentlyUsedConfig.enableRecentlyUsed) {
            if (config.recentlyUsedConfig.stopAutoUpdateRecentlyUsed == true) {
                config.recentlyUsedConfig.stopAutoUpdateRecentlyUsed = false;
            }
            else {
                this.saveRecentlyUsed(this.value);
            }
        }
    });

    this.combobox.insertIfNotExists = function (field, compareDataOnly) {
        var existsInConfig = false;
        var existsInStore = false;
        for (var i = 0; i < config.data.length; i++) {
            if (compareDataOnly) {
                if (field.data == config.data[i].data && config.data[i].visible) {
                    existsInConfig = true;
                    break;
                }
            } else {
                if (field.data == config.data[i].data &&
                    field.displayName == config.data[i].displayName &&
                    field.groupName == config.data[i].groupName &&
                    field.visible == config.data[i].visible &&
                    field.searchable == config.data[i].searchable) {
                    existsInConfig = true;
                    break;
                }
            }
        }

        for (var i = 0; i < this.store.data.length; i++) {
            if (compareDataOnly) {
                if (field.data == this.store.data.items[i].data.data && this.store.data.items[i].data.visible) {
                    existsInStore = true;
                    break;
                }
            } else {
                if (field.data == this.store.data.items[i].data.data &&
                    field.displayName == this.store.data.items[i].data.displayName &&
                    field.groupName == this.store.data.items[i].data.groupName &&
                    field.visible == this.store.data.items[i].data.visible &&
                    field.searchable == this.store.data.items[i].data.searchable) {
                    existsInStore = true;
                    break;
                }
            }
        }

        if (!existsInConfig) {
            config.data.push(field);
        }

        if (!existsInStore) {
            var fld = {};
            fld["data"] = field.data;
            fld["displayName"] = field.displayName;
            fld["groupName"] = field.groupName;
            fld["visible"] = field.visible;
            fld["searchable"] = field.searchable;
            this.store.add(new this.store.recordType(fld, field.displayName));
            this.bindStore(this.store);
        }
    };

    // Add recenly used items to sessionStorage if enableRecentlyUsed = true;
    this.combobox.saveRecentlyUsed = function (selectedItem) {
        if (config.recentlyUsedConfig.enableRecentlyUsed) {
            var field = null;
            var isExists = false;
            var recentlyUsed = JSON.parse(sessionStorage.getItem(config.sessionKey));
            if (recentlyUsed == null)
                   recentlyUsed = [];

            for (var i = 0; i < this.store.data.length; i++) {
                if (selectedItem == this.store.data.items[i].data.data) {
                    field = config.data[i];
                    break;
                }
            }

            // Check if exists field in Recently used sections
            for (var i = 0; i < recentlyUsed.length; i++) {
                if (field.data == recentlyUsed[i].data && recentlyUsed[i].visible) {
                        isExists = true;
                    }
                }

            // Add to Recently used if there field not exists
            if (!isExists) {
                if (recentlyUsed.length == config.recentlyUsedConfig.ItemCounts) {
                    recentlyUsed.pop();
                }
                recentlyUsed.unshift({ data: field.data, displayName: field.displayName, groupName: config.recentlyUsedConfig.recentlyUsedCaption, visible: true, searchable: true });
            }

            sessionStorage.setItem(config.sessionKey, JSON.stringify(recentlyUsed));
            addRecentlyUsedToConfigData();
            this.updateStore(config.data);
        }
        return;
    };

    this.combobox.stopAutoUpdateRecentlyUsed = function () {
         config.recentlyUsedConfig.stopAutoUpdateRecentlyUsed = true;
     };

    this.combobox.updateSessionKey = function(sessionKey) {
        config.sessionKey = sessionKey;
    };

    this.combobox.updateRecentlyUsedData = function (fields) {
         config.data = fields;
         addRecentlyUsedToConfigData();
         return config.data;
     };

    return this.combobox;
};

