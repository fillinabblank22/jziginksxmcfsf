﻿// Pre processing ...
(function() {
    

})();

// Post processing ...
$(function() {
    
    // Adds zebra stripping to report tables.
    $('table.sw-rpt-tbl').each(function() {
        var $table = $(this);
        $('tbody tr:odd', $table).addClass('sw-rpt-row-alt');

    });
});