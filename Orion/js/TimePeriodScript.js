﻿function CheckTimePeriods() {

	if( !window.sampleTimePeriodControl || !window.sampleTimePeriodControl ) return;

          var oldSelectedValue = sampleTimePeriodControl.getElementsByTagName("option")[sampleTimePeriodControl.selectedIndex].value;
          var sampleTimeSelected = sampleTimeRelations[timePeriodControl.selectedIndex];
          var options = sampleTimePeriodControl.getElementsByTagName("option");
          
          while (options.length>0)
          {
             sampleTimePeriodControl.removeChild(options[options.length-1]);
          }
          for (i = 0; i<sampleTimeSelected.length; i++)
          {
            var sampleTimePeriodValue = sampleTimeValue[jQuery.inArray(sampleTimeSelected[i], sampleTimeIDS)];
            sampleTimePeriodControl.options[sampleTimePeriodControl.length] = new Option(sampleTimePeriodValue, sampleTimeSelected[i]);
          }
          for (i = 0; i<options.length; i++)
          {
            if (options[i].value == oldSelectedValue)
            {
                options[i].selected = true;
            }
          }
}

$(window).ready(CheckTimePeriods);
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
