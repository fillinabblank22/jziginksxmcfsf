Ext.namespace('SW');
Ext.namespace('SW.ESI');
Ext.QuickTips.init();
SW.ESI.ManageESIInstances = function () {
	DontHideTooltips();
	AddTooltipMenuItemToExtJs();

	var selectorModel;
	var pageSizeBox;
	var sortOrder;
	var dataStore;
	var stateStore;
	var menuStateStore;
	var integrationTypeStore;
	var grid;
	var userPageSize;
	var selectedColumns;
	var $container;
	var title;

	var instanceCreatedHint;
	var isDemo = function () {
		return $("#isOrionDemoMode").length != 0;
	}

	var updateToolbarButtons = function () {
		var selCount = grid.getSelectionModel().getCount();
		var map = grid.getTopToolbar().items.map;
		var needsToDisable = (selCount === 0);

		if (isDemo()) {
			map.Edit.setDisabled(selCount != 1);
			map.Delete.setDisabled(true);
			map.ChangeState.setDisabled(true);
		}
		else {
			map.Edit.setDisabled(selCount != 1);
			map.Delete.setDisabled(needsToDisable);
			map.ChangeState.setDisabled(needsToDisable);
		}

		var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

		if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
			hd.addClass('x-grid3-hd-checker-on');
		} else {
			hd.removeClass('x-grid3-hd-checker-on');
		}
	};

	function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
		elem.store.removeAll();

		elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: "" };

		if (limit)
			elem.store.load({ params: { start: start, limit: limit }, callback: callback });
		else
			elem.store.load({ callback: callback });
	};

	function setMainGridHeight(pageSize) {
		window.setTimeout(function () {
			var mainGrid = Ext.getCmp('mainGrid');
			var maxHeight = calculateMaxGridHeight(mainGrid);
			var calculated = calculateGridHeight(pageSize);
			var height = (calculated > 0) ? Math.min(calculated, maxHeight) : maxHeight;
			setHeightForGrids(height);

			//we need this to add height if horizontal scroll bar is present, with low window width and in chrome
			if (grid.el.child(".x-grid3-scroller").dom.scrollWidth != grid.el.child(".x-grid3-scroller").dom.clientWidth) {
				height = Math.min(height + 20, maxHeight);
				setHeightForGrids(height);
			}
		}, 0);
	}


	function getInstanceRowNumber(sId, succeeded) {
		if (!sId) {
			succeeded({ ind: -1 });
			return;
		}

		var sort = "";
		var dir = "";

		if (sortOrder != null) {
			sort = sortOrder[0];
			dir = sortOrder[1];
		}

		SW.Core.Services.callWebServiceSync('/Orion/ESI/Admin/ManageESIInstances.asmx', 'GetESIInstanceNumber', { serverId: sId, sort: sort, direction: dir },
			function (result) {
				if (succeeded != null) {
					succeeded({ ind: result });
					return;
				}
				// not found
				succeeded({ ind: -1 });
				return;
			});
	}

	function setHeightForGrids(height) {
		var mainGrid = Ext.getCmp('mainGrid');

		mainGrid.setHeight(Math.max(height, 300));
		mainGrid.doLayout();
	}

	function calculateGridHeight(numberOfRows) {
		if (grid.store.getCount() == 0)
			return 0;
		var rowsHeight = Ext.fly(grid.getView().getRow(0)).getHeight() * (numberOfRows + 1);
		return grid.getHeight() - grid.getInnerHeight() + rowsHeight + 7;
	}

	function calculateMaxGridHeight(gridPanel) {
		var gridTop = gridPanel.getPosition()[1];
		return $(window).height() - gridTop - $('#footer').height() - 25;
	}

	var addESIInstance = function (context) {
		SW.Core.Services.postToTarget(context.url, { InstanceID: null });
	};

	var editESIInstance = function () {
		var selectedItem = grid.getSelectionModel().selections.items[0];
		var integrationTypeStoreIndex = Ext.StoreMgr.lookup(integrationTypeStore).findExact('typeId', selectedItem.data.InstanceType);
		var integrationTypeStoreRec = Ext.StoreMgr.lookup(integrationTypeStore).getAt(integrationTypeStoreIndex);
		SW.Core.Services.postToTarget(integrationTypeStoreRec.data.typeEditUrl, { InstanceID: selectedItem.data.InstanceID });
	};

	var changeStateESIInstance = function (type) {
		var toEdit = getSelectedESIInstancesIDs(grid.getSelectionModel().getSelections(), type);
		switch (type.value) {
			//change to Disabled
			case 0:
				{
					Ext.Msg.show({
						title: "@{R=ESI.Strings;K=ManageInstances_OperationStateChange_Title; E=js}",
						msg: "@{R=ESI.Strings;K=ManageInstances_OperationStateChange_Confirmation; E=js}",
						buttons: { yes: '@{R=ESI.Strings;K=CommonButtonType_Change; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
						icon: Ext.MessageBox.QUESTION,
						fn: function (btn) {
							if (btn == 'yes') {
								changeState(toEdit, type.value);
							}
						}
					});
				}
				break;
			//change to Restricted
			case 1:
				{
					Ext.Msg.show({
						title: "@{R=ESI.Strings;K=ManageInstances_OperationStateChange_Title; E=js}",
						msg: "@{R=ESI.Strings;K=ManageInstances_OperationStateChange_Confirmation_Restricted; E=js}",
						buttons: { yes: '@{R=ESI.Strings;K=CommonButtonType_Change; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
						icon: Ext.MessageBox.QUESTION,
						fn: function (btn) {
							if (btn == 'yes') {
								changeState(toEdit, type.value);
							}
						}
					});
				}
				break;
			//change to Enabled
			default:
				{
					changeState(toEdit, type.value);
				}

		}
	}

	var changeState = function (ids, state) {
		ORION.callWebService("/Orion/ESI/Admin/ManageESIInstances.asmx",
			"changeStateESIInstance", { ids: ids, state: state },
			function (result) {

				if (result.Success)
					refreshGrid();
				else {
					refreshGrid();
					Ext.Msg.show({ title: "Error", msg: result.Message, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR })
				}
			});
	}

	var refreshGrid = function () {
		refreshGridObjects(0, userPageSize, grid, "", "", "", "", function () { });
	};

	var getSelectedESIInstancesIDs = function (items, type) {
		var ids = [];
		Ext.each(items, function (item) {
			ids.push(item.data.InstanceID)
		});
		return ids;
	};

	var deleteSelectedItems = function (items, onSuccess) {
		var toDelete = getSelectedESIInstancesIDs(items);
		var waitMsg;

		ORION.callWebService("/Orion/ESI/Admin/ManageESIInstances.asmx",
			"GetUsedESIInstances", { ids: toDelete },
			function (result) {

				result = (result.d || result).result;

				var ESIInstancesNameList;


				if (result.length > 0) {
					ESIInstancesNameList = '@{R=ESI.Strings;K=ManageInstances_RemoveInstance_Text_1; E=js}';
					for (var i = 0; i < result.length; i++) {
						var item = result[i];
						ESIInstancesNameList += String.format('@{R=ESI.Strings;K=ManageInstances_RemoveInstance_Text_2; E=js}', item.InstanceName, item.ActionCount);
					}
					Ext.Msg.show({
						title: "@{R=ESI.Strings;K=ManageInstances_RemoveInstance_Title; E=js}",
						msg: ESIInstancesNameList,
						minWidth: 500,
						buttons: { yes: '@{R=ESI.Strings;K=CommonButtonType_Remove; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
						icon: Ext.MessageBox.QUESTION,
						fn: function (btn) {
							if (btn == 'yes') {
								waitMsg = Ext.Msg.wait("@{R=ESI.Strings;K=ManageInstances_RemoveInstance_DeletingMessage; E=js}");
								deleteESIInstances(
									toDelete,
									function (deleteResult) // onSucess
									{
										waitMsg.hide();
										onSuccess(deleteResult.Success);
									},
									function (deleteResult) // onFail
									{
										waitMsg.hide();
										Ext.Msg.show({ title: "@{R=ESI.Strings;K=CommonMessageType_Error; E=js}", msg: deleteResult.Message, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR })
									});
							}
						}
					});
				} else if (result.length == 0) {
					Ext.Msg.show({
						title: "@{R=ESI.Strings;K=ManageInstances_RemoveInstance_Title; E=js}",
						msg: "@{R=ESI.Strings;K=ManageInstances_RemoveInstance_Confirmation; E=js}",
						minWidth: 500,
						buttons: { yes: '@{R=ESI.Strings;K=CommonButtonType_Remove; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
						icon: Ext.MessageBox.QUESTION,
						fn: function (btn) {
							if (btn == 'yes') {
								waitMsg = Ext.Msg.wait("@{R=ESI.Strings;K=ManageInstances_RemoveInstance_DeletingMessage; E=js}");
								deleteESIInstances(
									toDelete,
									function (deleteResult) // onSucess
									{
										waitMsg.hide();
										onSuccess(deleteResult.Success);
									},
									function (deleteResult) // onFail
									{
										waitMsg.hide();
										Ext.Msg.show({ title: "@{R=ESI.Strings;K=CommonMessageType_Error; E=js}", msg: deleteResult.Message, buttons: Ext.Msg.OK, icon: Ext.Msg.ERROR })
									});
							}
						}
					});
				}
			});
	};

	function deleteESIInstances(ids, onSuccess, onFail) {
		console.log("calling DeleteESIInstances" + ids);

		ORION.callWebService("/Orion/ESI/Admin/ManageESIInstances.asmx",
			"DeleteESIInstances", { ids: ids },
			function (result) {

				if (result.Success)
					onSuccess(result);
				else
					onFail(result);
			});
	}

	var renderServerName = function (value, meta, record) {
		return String.format('<a href="{0}" target="_blank">{0}</a>', value);
	};

	function GetStateMenu(store) {
		var stateMenu = new Ext.menu.Menu;

		for (var i = 0; i < store.getCount(); ++i) {
			var rec = store.getAt(i);

			var item = new Ext.menu.Item({
				text: rec.data.status,
				value: rec.data.id,
				icon: rec.data.img,
				tooltip: rec.data.statusHint,
				handler: changeStateESIInstance
			});

			stateMenu.add(item);
		}

		return stateMenu;
	}

	function GetAddInstanceMenu(store) {
		var addInstanceMenu = new Ext.menu.Menu;

		for (var i = 0; i < store.getCount(); ++i) {
			var rec = store.getAt(i);

			var item = new Ext.menu.Item({
				text: rec.data.typeName,
				value: rec.data.typeId,
				icon: rec.data.typeImg,
				url: rec.data.typeEditUrl,
				handler: addESIInstance
			});

			addInstanceMenu.add(item);
		}

		return addInstanceMenu;
	}

	ORION.prefix = 'ManageESIInstances_';
	return {
		ControlReady: function () {
			showDialog();
		},
		init: function () {
			instanceCreatedHint = $('#InstanceSuccessfullyCreated');
			instanceCreatedHint.find('a.sw-suggestion-btn-close').click(function () {
				instanceCreatedHint.hide();
			});


			userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));

			selectorModel = new Ext.grid.CheckboxSelectionModel();
			selectorModel.on("selectionchange", updateToolbarButtons);
			sortOrder = ORION.Prefs.load('SortOrder', 'InstanceType, ASC').split(',');

			selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');

			dataStore = new ORION.WebServiceStore(
				"/Orion/ESI/Admin/ManageESIInstances.asmx/GetESIInstances",
				[
					{ name: 'InstanceID', mapping: 0 },
					{ name: 'InstanceType', mapping: 1 },
					{ name: 'InstanceName', mapping: 2 },
					{ name: 'InstanceURL', mapping: 3 },
					{ name: 'OperationalState', mapping: 5 },
					{ name: 'Status', mapping: 6 }

				]);

			var statesData = [
				[0, '@{R=ESI.Strings;K=ManageInstances_OperationamState_Disable; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationamState_Disabled; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationalState_Disabled_Hint; E=js}', '/Orion/ESI/images/disable.png'],
				[1, '@{R=ESI.Strings;K=ManageInstances_OperationamState_Restrict; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationamState_Restricted; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationalState_Restricted_Hint; E=js}', '/Orion/ESI/images/ignore.png'],
				[2, '@{R=ESI.Strings;K=ManageInstances_OperationamState_Enable; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationamState_Enabled; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationalState_Enabled_Hint; E=js}', '/Orion/ESI/images/check.png'],
				[3, '@{R=ESI.Strings;K=ManageInstances_OperationamState_RestrictedBySystem; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationamState_RestrictedBySystem; E=js}', '@{R=ESI.Strings;K=ManageInstances_OperationalState_RestrictedBySystem_Hint; E=js}', '/Orion/ESI/images/ignore.png']
			];

			stateStore = new Ext.data.SimpleStore(
				{
					fields: ['id', 'status', 'statusAdjective', 'statusHint', 'img'],
					data: statesData

				});

			var statusData = [
				[0, '@{R=ESI.Strings;K=ManageInstances_Status_Unknown; E=js}', '/Orion/images/StatusIcons/Small-Unknown.gif'],
				[1, '@{R=ESI.Strings;K=ManageInstances_Status_Up; E=js}', '/Orion/images/StatusIcons/Small-Up.gif'],
				[2, '@{R=ESI.Strings;K=ManageInstances_Status_Down; E=js}', '/Orion/images/StatusIcons/Small-Down.gif'],
			];

			statusStore = new Ext.data.SimpleStore(
				{
					fields: ['id', 'status', 'img'],
					data: statusData

				});

			var integrationTypeData = [
                ["ServiceDesk", 'Service Desk', '/Orion/ESI/images/service_desk.png', '/orion/esi/admin/editservicedeskinstance.aspx'],
                ["ServiceNow", 'ServiceNow', '/Orion/ESI/images/servicenow.png', '/orion/esi/admin/editservicenowinstance.aspx']
			];

			integrationTypeStore = new Ext.data.SimpleStore(
				{
					fields: ['typeId', 'typeName', 'typeImg', 'typeEditUrl'],
					data: integrationTypeData
				});

			menuStateStore = new Ext.data.SimpleStore(
				{
					fields: ['id', 'status', 'statusAdjective', 'statusHint', 'img'],
					data: statesData.filter(function (item) { return item[0] != 3; })
				});

			// dataStore.proxy.conn.jsonData = {};
			dataStore.sortInfo = { field: sortOrder[0], direction: sortOrder[1] };

			pageSizeBox = new Ext.form.NumberField({
				id: 'PageSizeField',
				enableKeyEvents: true,
				allowNegative: false,
				width: 40,
				allowBlank: false,
				minValue: 1,
				maxValue: 100,
				value: userPageSize,
				listeners: {
					scope: this,
					'keydown': function (f, e) {
						var k = e.getKey();
						if (k == e.RETURN) {
							e.stopEvent();
							var v = parseInt(f.getValue());
							if (!isNaN(v) && v > 0 && v <= 100) {
								userPageSize = v;
								pagingToolbar.pageSize = userPageSize;
								ORION.Prefs.save('PageSize', userPageSize);
								pagingToolbar.doLoad(0);
								setMainGridHeight(userPageSize);
							}
							else {
								Ext.Msg.show({
									title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
									msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
									icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
								});
								return;
							}
						}
					},
					'focus': function (field) {
						field.el.dom.select();
					},
					'change': function (f, numbox, o) {
						var pSize = parseInt(f.getValue());
						if (isNaN(pSize) || pSize < 1 || pSize > 100) {
							Ext.Msg.show({
								title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
								msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
								icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
							});
							return;
						}
						if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
							pagingToolbar.pageSize = pSize;
							userPageSize = pSize;

							ORION.Prefs.save('PageSize', userPageSize);
							pagingToolbar.doLoad(0);
							setMainGridHeight(userPageSize);
						}
					}
				}
			});

			var pagingToolbar = new Ext.PagingToolbar(
				{
					id: 'gridPaging',
					store: dataStore,
					pageSize: userPageSize,
					displayInfo: true,
					displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
					emptyMsg: "@{R=ESI.Strings;K=ManageInstances_Empty_Grid_Info; E=js}",
					beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
					afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
					firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
					prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
					nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
					lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
					refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
					items: [
						'-',
						new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
						pageSizeBox
					]
				}
			);

			grid = new Ext.grid.GridPanel({
				region: 'center',
				store: dataStore,
				baseParams: { start: 0, limit: userPageSize },
				split: true,
				columns: [selectorModel,
					{
						header: '@{R=ESI.Strings;K=ManageInstances_Column_Status; E=js}', width: 100, sortable: true, dataIndex: 'Status', renderer: function (value) {

							// Render just supported statuses
							if (value > 2)
								value = 0;

							var index = Ext.StoreMgr.lookup(statusStore).findExact('id', value);
							var rec = Ext.StoreMgr.lookup(statusStore).getAt(index);
							return '<img style="vertical-align: text-bottom;" src="' + rec.data.img + '"> &nbsp;' + rec.data.status;
						}
					},
					{ header: 'Instance ID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'InstanceID' },
					{
						header: '@{R=ESI.Strings;K=ManageInstances_Column_InstanceType; E=js}', width: 125, sortable: true, hideable: false, dataIndex: 'InstanceType', renderer: function (value) {
							var index = Ext.StoreMgr.lookup(integrationTypeStore).findExact('typeId', value);
							var rec = Ext.StoreMgr.lookup(integrationTypeStore).getAt(index);
							return '<img style="vertical-align: text-bottom; heigth: 16px; width: 16px;" src="' + rec.data.typeImg + '"> &nbsp;' + rec.data.typeName + '</img>';
						}
					},
					{ header: '@{R=ESI.Strings;K=ManageInstances_Column_InstanceName; E=js}', width: 250, sortable: true, hideable: false, dataIndex: 'InstanceName' },
					{ header: '@{R=ESI.Strings;K=ManageInstances_Column_InstanceURL; E=js}', width: 250, sortable: true, dataIndex: 'InstanceURL', renderer: renderServerName },
					{
						header: '@{R=ESI.Strings;K=ManageInstances_Column_OpState; E=js}', width: 250, sortable: true, dataIndex: 'OperationalState', renderer: function (value) {
							var index = Ext.StoreMgr.lookup(stateStore).findExact('id', value);
							var rec = Ext.StoreMgr.lookup(stateStore).getAt(index);
							return '<span ext:qtip="' + rec.data.statusHint + '"><img style="vertical-align: text-bottom;" src="' + rec.data.img + '"> &nbsp;' + rec.data.statusAdjective + '</span>';
						}
					}
				],
				sm: selectorModel, autoScroll: 'true', loadMask: true, width: 750, height: 350,
				tbar: [
					{ id: 'Add', text: '@{R=ESI.Strings;K=ManageInstances_AddInstance; E=js}', tooltip: '@{R=ESI.Strings;K=ManageInstances_AddInstance_TT; E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', menu: GetAddInstanceMenu(integrationTypeStore) }, '-',
					{ id: 'Edit', text: '@{R=ESI.Strings;K=ManageInstances_EditInstance; E=js}', tooltip: '@{R=ESI.Strings;K=ManageInstances_EditInstance_TT; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler: editESIInstance }, '-',
					{ id: 'ChangeState', text: '@{R=ESI.Strings;K=ManageInstances_ChangeOperationalState; E=js}', tooltip: '@{R=ESI.Strings;K=ManageInstances_ChangeOperationalState_TT; E=js}', icon: '/Orion/ESI/images/ChangeOperationalState_icon.png', cls: 'x-btn-text-icon', menu: GetStateMenu(menuStateStore) }, '-',
					{
						id: 'Delete', text: '@{R=ESI.Strings;K=ManageInstances_RemoveInstance; E=js}', tooltip: '@{R=ESI.Strings;K=ManageInstances_RemoveInstance_TT; E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', cls: 'x-btn-text-icon',
						handler: function () {
							if ($("#isOrionDemoMode").length != 0) {
								demoAction("Core_ManageESIInstances_DeleteESIInstance", this);
								return;
							}
							deleteSelectedItems(grid.getSelectionModel().getSelections(), function () {
								refreshGridObjects(0, userPageSize, grid, "", "", "", "", function () { });
							});
						}
					}

				],
				bbar: pagingToolbar,
				listeners: {
					cellclick: function (grid, rowIndex, columnIndex) {
						var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name


					}
				}
			});

			for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
				if (selectedColumns.indexOf(grid.getColumnModel().getDataIndex(i)) > -1 && i > 0) {
					grid.getColumnModel().setHidden(i, false);
				} else {
					grid.getColumnModel().setHidden(i, true);
				}
			}

			grid.on('sortchange', function (grid, option) {
				var sort = option.field;
				var dir = option.direction;

				if (sort && dir) {
					sortOrder[0] = sort;
					sortOrder[1] = dir;
					ORION.Prefs.save('SortOrder', sort + ',' + dir);
				}
			});

			grid.store.on('load', function () {
				grid.getEl().unmask();
				setMainGridHeight(userPageSize);

			});

			grid.getColumnModel().on('hiddenchange', function () {
				var cols = '';
				for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
					if (!grid.getColumnModel().isHidden(i)) {
						cols += grid.getColumnModel().getDataIndex(i) + ',';
					}
				}
				ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
			});

			var mainGridPanel = new Ext.Panel({ id: 'mainGrid', region: 'center', split: true, height: 380, layout: 'border', collapsible: false, items: [grid], cls: 'no-border' });
			mainGridPanel.render("ManageESIInstancesGrid");
			refreshGridObjects(0, userPageSize, grid, "", "", "", "", function () { });

			$(window).resize(function () {
				setMainGridHeight(userPageSize);
				mainGridPanel.doLayout();
			});
			updateToolbarButtons();
		}
	};
}();
Ext.onReady(SW.ESI.ManageESIInstances.init, SW.ESI.ManageESIInstances);

function DontHideTooltips() {
	Ext.override(Ext.QuickTip,
		{
			dismissDelay: 0
		});

	Ext.override(Ext.ToolTip,
		{
			dismissDelay: 0
		});
}

function AddTooltipMenuItemToExtJs() {
	// Creates a menu that supports tooltip specs for it's items. Just add "tooltip: 'txt'" to the menu item config
	Ext.override(Ext.menu.Item, {
		onRender: function (container, position) {
			if (!this.itemTpl) {
				this.itemTpl = Ext.menu.Item.prototype.itemTpl = new Ext.XTemplate(
					'<a id="{id}" class="{cls}" hidefocus="true" unselectable="on" href="{href}"',
					'<tpl if="hrefTarget">',
					' target="{hrefTarget}"',
					'</tpl>',
					'>',
					'<img src="{icon}" class="x-menu-item-icon {iconCls}"/>',
					'<span class="x-menu-item-text">{text}</span>',
					'</a>'
				);
			}
			var a = this.getTemplateArgs();
			this.el = position ? this.itemTpl.insertBefore(position, a, true) : this.itemTpl.append(container, a, true);
			this.iconEl = this.el.child('img.x-menu-item-icon');
			this.textEl = this.el.child('.x-menu-item-text');
			if (this.tooltip) {
				this.tooltip = new Ext.ToolTip(Ext.apply({
					target: this.el
				}, Ext.isObject(this.tooltip) ? this.toolTip : { html: this.tooltip }));
			}
			Ext.menu.Item.superclass.onRender.call(this, container, position);
		}
	});
}