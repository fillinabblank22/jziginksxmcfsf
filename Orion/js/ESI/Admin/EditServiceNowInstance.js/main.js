﻿(function (window) {
    function isDemo() {
        return $("#isOrionDemoMode").length != 0;
    }

    function modelToInstance(model) {
        return {
            id: model.id || 0,
            name: model.name,
            url: model.url,
            login: model.login,
            password: model.passwordIsChanged ? model.password : "",
            passwordIsChanged: model.passwordIsChanged
        };
    }

    function main(scope, bind, services) {

        var service = services("EditServiceNowInstance.asmx", {
            load: function (response) {

                if (response.error) {

                    model.showLoading = false;
                    model.showErrorMessage = true;
                    model.errorMessage = response.error.Message;
                    return;
                }
                model.showLoadingModal = false;

                var result = response.result;

                model.id = result.id;
                model.name = result.name;
                model.url = result.url;

                model.loginPlaceholder =
                model.login = result.login;
                
                model.passwordPlaceholder =
                model.password = typeof result.passwordLength !== "undefined" ? Array(result.passwordLength + 1).join('●') : undefined;
                model.passwordIsChanged = result.id ? false : true;
                model.credentialsValid = typeof result.passwordLength !== "undefined";
            },

            save: function (response) {

                model.showSaving = false;

                if (response.error) {

                    model.showErrorMessage = true;
                    model.errorMessage = response.error.Message;
                    return;
                }

                window.location = "/Orion/ESI/Admin/ManageESIInstances.aspx";

            },
            test: function (response) {
                model.showTestInProgress = false;

                if (response.status === "abort") {
                    model.showTestCredentialsButton = true;
                    this.showTestInProgress = false;
                    return;
                }

                if (response.error) {
                    model.testConnection = false;
                    model.showErrorMessage = true;
                    model.errorMessage = response.error.Message;
                    model.shouldSave = false;
                    return;
                }

                if (!response.result.valid) {
                    model.testFailedMessage = response.result.message;
                    model.testFailedDetail = response.result.detail;
                }

                model.shouldSave = model.shouldSave && response.result.valid;

                if (model.shouldSave && response.result.valid === true) {
                    model.testConnection = false;
                    model.showSaving = true;
                    service.save(modelToInstance(model));
                    return;
                }

                model.credentialsValid =
                model.showCredentialsOkMessage = response.result.valid;
                
                model.showTestFailedMesssage =
                model.showTestFailedDetail = !response.result.valid;
                model.showTestCredentialsButton = true;


                if (model.testFailedDetail === null)
                    model.showTestFailedDetail = false;
            }
        });

        var model = {
            nameIsEmpty: false,
            urlIsEmpty: false,
            urlIsInvalid: false,
            loginIsEmpty: false,
            passwordIsEmpty: false,            
            showLoadingModal: true,
            showLoading: true,
            showSaving: false,
            showErrorMessage: false,
            showTestInProgress: false,
            showTestFailedDetail: false,
            showTestFailedMesssage: false,
            showTestCredentialsButton: true,
            showCredentialsOkMessage: false,
            credentialsValid: false,
            proxyEnabled: false,
            
            showDetails: function () {
                
                Ext.Msg.show({
                    title: "Error details", //@{R=Core.Strings;K=WEBJS_SO0_25; E=js}
                    msg: model.testFailedDetail,
                    minWidth: 300,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            },

            save: function () {
                if (this.validateAll()) {
                    if (isDemo()) {
                        demoAction("Core_ManageESIInstances_addESIInstance", this);
                        return;
                    }

                    if (this.credentialsValid) {

                        this.showSaving = true;
                        service.save(modelToInstance(model));
                        return;

                    } else {
                        this.shouldSave = true;
                        this.test();
                    }
                }
            },
            test: function () {

                if (this.validateForTest()) {
                    if (isDemo()) {
                        demoAction("Core_ManageESIInstances_addESIInstance", this);
                        return;
                    }

                    this.showTestInProgress = true;
                    this.showTestCredentialsButton = false;
                    this.showTestFailedMesssage = false;
                    this.showCredentialsOkMessage = false;

                    this.request = service.test({
                        id: this.id || 0,
                        url: this.url,
                        login: this.login,
                        password: this.passwordIsChanged ? this.password : "",
                        passwordIsChanged: this.passwordIsChanged
                    });
                }
            },

            cancelTest: function () {
                if (typeof this.request !== "undefined") {
                    this.request.abort();
                    this.request = undefined;

                }
            },

            validateAll: function () {
                this.validate(this.name, "name");
                this.validate(this.url, "url");
                this.validate(this.login, "login");


                if (this.passwordIsChanged) {
                    this.validate(this.password, "password");
                }

                return !(this.nameIsEmpty || this.urlIsEmpty || this.urlIsInvalid || this.loginIsEmpty
                         || (this.passwordIsChanged && this.passwordIsEmpty));
            },

            validateForTest: function () {
                this.validate(this.url, "url");
                this.validate(this.login, "login");
                if (this.passwordIsChanged) {
                    this.validate(this.password, "password");
                }

                return !(this.urlIsEmpty || this.urlIsInvalid || this.loginIsEmpty || (this.passwordIsChanged && this.passwordIsEmpty));
            },

            validate: function (value, prop) {

                validateProp = function (v, p) {
                    return typeof value === "undefined"
                              || (typeof value === "string" && value.trim() === "" && value !== p)
                              || value === null;
                }

                if (prop === "password") {
                    this.passwordIsEmpty = validateProp(value, this.passwordIsEmpty);

                    if (this.passwordPlaceholder !== value) {
                        // we need to give our web service a hint that it should update password
                        this.passwordIsChanged = true;
                    }
                }
                else {
                    this[prop + "IsEmpty"] = typeof value === "undefined" || (typeof value === "string" && value.trim() === "") || value === null;
                }


                if (prop === "url") {
                    // check if it's a valid url
                    var expr = /^(https)\:\/\/[A-Za-z0-9]+[A-Za-z0-9\/\.\-\?\%]*(?:[^.])$/i;

                    this.urlIsInvalid = !this.urlIsEmpty && !(expr.test(value));
                }

                return true;
            },

            connectionChanged: function () {
                this.showTestCredentialsButton = true;
                this.showCredentialsOkMessage =
                this.credentialsValid = false;
            },

            passwordChanged: function () {
                this.connectionChanged();

                this.password = this.password.replace(new RegExp('●', 'g'), "");
            },

            closeModal: function () {

                this.showLoadingModal = false;
            },

            discard: function () {
                return window.location = "/Orion/ESI/Admin/ManageESIInstances.aspx";
            }
        };

        bind(scope, model, function () {

            model.$update("instanceId", function () {

                service.load({ instanceId: model.instanceId || null });
            });

        });

        model.showLoading = true;
    }

    $(window.document).ready(function () {
        var bind = use("SW.Core.Bind");
        var services = use("SW.Core.Services");
        var scope = $('[esi-scope="EditServiceNowInstance"]')[0];

        main(scope, bind, services);
    });

})(window);