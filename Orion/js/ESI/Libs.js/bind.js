﻿(function (window) {

    //var scopedWatchLists = [];
    var updateQueue = [];

    function updateFunction(elem, target, prop, item) {

        return function () {
            var canUpdateValue = true;
            var handler = elem.attr("esi-validate");
            var elemValue = elem.is(":checkbox") ? elem.prop("checked") : elem.val();
            if (handler) {

                canUpdateValue = target[handler](elemValue, prop);

            }
            if (canUpdateValue) {
                var shouldCallChangeHandler = target[prop] != elemValue;

                item.value = target[prop] = elemValue;

                var changeHandler = elem.attr("esi-change");
                if (shouldCallChangeHandler && typeof changeHandler !== "undefined" && typeof target[changeHandler] === "function") {

                    target[changeHandler].call(target, prop);
                }
            }
        }
    }

    function updateGridFunction(target, prop) {

        return function () {
            var targetElem = $(this);
            var tr = targetElem.closest("tr");
            var dataIndex = tr.attr("data-index");

            if (dataIndex && targetElem.hasClass("delete-row-btn")) {
                target[prop] = target[prop].filter(function (obj) {
                    return obj.index != dataIndex;
                });
            } else {
                var table = $("table[esi-grid=" + prop + "][esi-grid-editable]");

                if (table.length > 0) {
                    $.each(target[prop],
                        function (objIndex, obj) {
                            var tr = $("tbody tr.editable-row[data-index=" + obj.index + "]", table);
                            if (tr.length > 0) {
                                $.each(obj,
                                    function (propName, propValue) {
                                        if (propName !== "index") {
                                            obj[propName] = htmlEncode($("textarea." + propName + "", tr).val());
                                        }
                                    });
                            }
                        });
                }

                return validateGridValues(table, target);
            }
        };
    }

    function renderGridFunction() {

        var items = this.source[this.property];

        if (this.elem.prop("tagName") === "TABLE") {
            var table = this.elem;
            var editable = table[0].hasAttribute("esi-grid-editable");
            var columnNames = parseJsonString(table.attr("esi-grid-column-names"));
            table.empty();
            if (typeof items === "object" && items.length > 0) {
                table.html(getTableBody(Object.keys(items[0]), columnNames));
                var tbody = $("tbody", table);
                $("tr.empty", tbody).hide();

                // create grid data rows
                for (var index = 0; index < items.length; index++) {
                    var item = items[index];
                    tbody.append(editable ? getCustomAttrRow(this.property, item) : getEntityAttrRow(item));
                }

                if (editable) {
                    setTextAreasHeightAndAttachEvents(this.property, ".editable-table-section-header");
                    validateGridValues(table, this.source);
                }
            } else {
                table.append("<tbody><tr class=\"empty\"><td>@{R=ESI.Strings;K=ESI_Action_Grid_No_Data_Message; E=js}</td></tr></tbody>");
            }
        }
    }

    function validateGridValues(table, model) {

        model.customAttrRequiredError = false;
        model.customAttrUniqueError = false;
        model.customAttrLengthError = false;

        if (!table ||
            table.length === 0 ||
            (table && table.length > 0 && !table[0].hasAttribute("esi-grid-editable"))) {
            return;
        }

        $("tbody tr", table).each(function () {
            var tr = $(this);
            $(".data-column", tr).each(function () {
                var col = $(this);
                var textarea = $("textarea", col);
                if (textarea.length > 0) {
                    var value = $.trim(textarea.val());
                    if (value === "") {
                        model.customAttrRequiredError = true;
                        textarea.addClass("esi-error");
                    } else if (textarea[0].hasAttribute("validation-unique") && !isValueUnique($("tbody tr textarea." + textarea.attr("validation-unique"), table), value)) {
                        model.customAttrUniqueError = true;
                        textarea.addClass("esi-error");
                    } else if (textarea[0].hasAttribute("validation-length") && textarea.val().length > parseInt(textarea.attr("validation-length"))) {
                        model.customAttrLengthError = true;
                        textarea.addClass("esi-error");
                    } else if (textarea.hasClass("esi-error")) {
                        textarea.removeClass("esi-error");
                    }
                }
            });
        });
    }

    function isValueUnique(elements, value) {
        var count = 0;
        elements.each(function () {
            if ($(this).val() === value) {
                count++;
            }
        });
        if (count > 1) {
            return false;
        }
        return true;
    }

    function renderItemsFunction() {

        var items = this.source[this.property];
        var spec = this.spec;
        if (typeof items === "object" && items.length > 0) {
            // render items
            if (this.elem.prop("tagName") === "SELECT") {
                this.elem.empty();
                for (var index = 0; index < items.length; index++) {
                    var item = items[index];
                    this.elem.append('<option value="' + item[spec.value] + '">' + item[spec.display] + "</option>");
                }
            }
        } else {
            // clear items
            this.elem.empty();
        }
    }

    function renderValueFunction() {

        var value = this.source[this.property];
        var tag = this.elem.prop("tagName");
        if (this.elem.is(":checkbox")) {
            if (value === true) {
                this.elem.prop("checked", "checked");
            } else {
                this.elem.removeProp("checked");
            }
        }
        else if (this.elem.is(":text") || this.elem.is(":password") || this.elem.is("select")) {
            this.elem.val(value);
        }
        else if (tag === "A") {
            this.elem.text(value);
            this.elem.attr("href", value);
        }
        else {
            this.elem.text(value);
        }
    }

    function callHandler() {

        var handler = this.source[this.changeHandler];
        if (typeof handler === "function") {
            handler.call(this.source);
        }
    }

    function renderShowFunction() {

        var show = this.source[this.property];
        if (show === true) {
            this.elem.show();
        } else if (show === false) {
            this.elem.hide();
        }
    }

    function renderHideFunction() {

        var show = this.source[this.property];
        if (show === true) {
            this.elem.hide();
        } else if (show === false) {
            this.elem.show();
        }
    }

    function renderDisableFunction() {

        var disable = this.source[this.property];
        this.elem.prop("disabled", disable === true);
        this.elem.find("input,textarea,select").prop("disabled", disable === true);
    }

    function renderEnableFunction() {

        var enable = this.source[this.property];
        this.elem.prop("disabled", enable === false);
        this.elem.find("input,textarea,select").prop("disabled", enable === false);
    }

    function renderModalFunction() {

        var show = this.source[this.property];
        var overlay = $(".ui-overlay")[0];
        var self = this;
        var doc = $(window.document);
        if (show === true) {

            self.elem.css({
                "position": "absolute",
                "z-index": "10010",
                "top": "50%",
                "left": "50%",
                "margin-top": -self.elem.outerHeight() / 2,
                "margin-left": -self.elem.outerWidth() / 2,
                "float": "left"
            });


            if (typeof overlay === "undefined") {
                overlay = $('<div class="ui-overlay"><div class="ui-widget-overlay"></div></div>').hide()
                    .css({
                        "position": "absolute",
                        "z-index": "10000",
                        "top": "0",
                        "left": "0",
                        "width": "100%",
                        "height": "100%",
                    });
                $(self.elem.attr("esi-modal-append-to")).css("position", "relative")
                    .append(overlay);
                overlay.fadeIn();
            }

            self.elem.show();
        } else {
            if (typeof overlay !== "undefined") {
                if (typeof overlay.remove === "undefined") {
                    overlay = $(overlay);
                }
                overlay.remove();
            }
            self.elem.hide();
        }
    }

    function renderCollapseFunction() {

        var collapse = this.source[this.property];
        if (collapse === true) {
            this.elem.hide();
        } else if (collapse === false) {
            this.elem.show();
        }
    }

    function bind(scope, model, callback) {

        var wrappedScope = $(scope);
        var watchList = [];

        wrappedScope.find("[esi-items]").each(function (index, elem) {
            var wrapped = $(elem);
            var prop = wrapped.attr("esi-items");
            var spec = JSON.parse(wrapped.attr("esi-items-spec"));
            var item = { source: model, property: prop, elem: wrapped, process: renderItemsFunction, spec: spec };
            watchList.push(item);
        });

        wrappedScope.find("[esi-bind]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-bind");


            var item = { source: model, property: prop, elem: wrapped, process: renderValueFunction };
            var update = updateFunction(wrapped, model, prop, item);
            item.update = update;
            //var value = wrapped.val();
            //if (typeof value != "undefined" && value !== "")
            //{
            //    model[prop] = value;

            //}

            watchList.push(item);
            wrapped.bind("change paste keyup blur", update);
        });

        wrappedScope.find("[esi-show]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-show");
            var value = model[prop];
            var item = { source: model, property: prop, elem: wrapped, process: renderShowFunction };
            watchList.push(item);
        });

        wrappedScope.find("[esi-hide]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-hide");
            var value = model[prop];
            var item = { source: model, property: prop, elem: wrapped, process: renderHideFunction };
            watchList.push(item);
        });

        wrappedScope.find("[esi-collapse]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-collapse");
            var value = model[prop];
            var item = { source: model, property: prop, elem: wrapped, process: renderCollapseFunction };
            watchList.push(item);
        });

        wrappedScope.find("[esi-disable]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-disable");
            var value = model[prop];
            var item = { source: model, property: prop, elem: wrapped, process: renderDisableFunction };
            watchList.push(item);
        });

        wrappedScope.find("[esi-enable]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-enable");
            var value = model[prop];
            var item = { source: model, property: prop, elem: wrapped, process: renderEnableFunction };
            watchList.push(item);
        });

        wrappedScope.find("[esi-click]").each(function (index, elem) {

            var wrapped = $(elem);
            var handler = wrapped.attr("esi-click");
            wrapped.click(function () { model[handler].call(model); });
        });

        wrappedScope.find("[esi-modal]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-modal");
            var value = model[prop];
            var item = { source: model, property: prop, elem: wrapped, process: renderModalFunction };
            watchList.push(item);
        });

        wrappedScope.find("[esi-grid]").each(function (index, elem) {

            var wrapped = $(elem);
            var prop = wrapped.attr("esi-grid");

            if (!model[prop]) {
                model[prop] = [];
            }

            var value = model[prop];
            var item = { source: model, property: prop, elem: wrapped, process: renderGridFunction };

            var update = updateGridFunction(model, prop);
            item.update = update;

            watchList.push(item);

            wrapped.on("input", "textarea", update);
            wrapped.on("click", ".delete-row-btn", update);
        });

        var removeCloak = true;
        var shouldNotify = typeof callback === "function";

        scope.$watchList = watchList;

        scope.$handle = setInterval(function () {

            var watchList = scope.$watchList;

            for (var index = 0; index < watchList.length; index++) {
                var item = watchList[index];
                var value = item.source[item.property];

                if (value !== item.value) {
                    item.value = value;
                    item.process.call(item);
                }
            }

            var update = updateQueue.pop();

            if (typeof update !== "undefined") {

                for (var index = 0; index < watchList.length; index++) {
                    var item = watchList[index];
                    if (typeof item.update === "function" && (typeof update.property === "undefined" || update.property === item.property)) {
                        item.update();
                    }
                }

                if (typeof update.handler === "function") {
                    update.handler();
                }
            }

            if (removeCloak) {
                wrappedScope.find(".esi-cloak").each(function (index, elem) {
                    $(elem).removeClass("esi-cloak");
                });
                wrappedScope.find(".esi-cloak-collapse").each(function (index, elem) {
                    $(elem).removeClass("esi-cloak-collapse");
                });
                removeCloak = false;
            }

            if (shouldNotify) {
                shouldNotify = false;
                updateQueue.push({ property: '*', handler: callback });


            }

        }, 100);

        model.$update = function (property, handler) {
            if (typeof property === "function") {
                handler = property;
                property = undefined;
            }

            updateQueue.push({ property: property, handler: handler });
        }

        model.$refresh = function (property) {
            for (var i = 0; i < watchList.length; i++) {
                var item = watchList[i];
                if (typeof property === "undefined") {
                    item.process.call(item);
                } else if (item.property === property) {
                    item.process.call(item);
                }
            }
        }

        scope.$snapshot = function () {
            var snapshot = {};
            wrappedScope.find("[esi-bind]").each(function (index, elem) {

                var wrapped = $(elem);
                var prop = wrapped.attr("esi-bind");
                snapshot[prop] = wrapped.val();

            });
            return snapshot;
        }

        scope.$release = function () {
            clearInterval(scope.$handle);
            scope.$watchList = [];
        };
    }

    $(window.document).ready(function () {
        (use("SW.Core.Module"))("SW.Core.Bind", bind);
    });

})(window);

function htmlEncode(value) {

    return $('<textarea/>').text(value).html().replace(/\"/g, '\'');
}

function parseJsonString(jsonStr) {

    if (jsonStr) {
        return JSON.parse(jsonStr.replace(/\'/g, '"'));
    }
}

function setTextAreasHeightAndAttachEvents(property, sectionClass) {

    SW.Core.MacroVariablePickerController.EnableInsertVariableButtons();

    var selector = "table[esi-grid='" + property + "'] textarea";

    $(selector).each(setTextAreaHeight);

    $(document)
        .off("input", selector)
        .on("input", selector, setTextAreaHeight);

    // set the correct size of textareas when section is clicked
    $(document)
        .off("click", sectionClass)
        .on("click", sectionClass,
            function () {
                $(selector).each(setTextAreaHeight);
            });
}

function setTextAreaHeight() {

    if (this.scrollHeight > 0) {
        $(this)
            .height(21)
            .css("padding-bottom", "3px")
            .attr('rows', 1)
            .height(this.scrollHeight);
    }
};

function getTableBody(tableColumns, columnNames) {

    var tableBody = "<thead>" +
        "<tr class=\"table-header\">";

    $.each(tableColumns,
        function (index, item) {
            if (item !== "index" && item !== "Macro") {
                tableBody += "<th class=\"title-column\">" + (columnNames ? columnNames[item] : item) + "</th>";
            }
        });

    tableBody +=
        "<th class=\"button-column\"></th>" +
        "<th class=\"button-column\"></th>" +
        "</tr>" +
        "</thead>" +
        "<tbody>" +
        "</tbody>";

    return tableBody;
}

function getEntityAttrRow(rowData) {

    return "<tr class=\"data-row\" data-index=\"" + rowData.index + "\">" +
        "<td class=\"data-column DisplayName\">" + htmlEncode(rowData.DisplayName) + "</td>" +
        "<td class=\"data-column Name\" title=\"" + rowData.Macro + "\">" + htmlEncode(rowData.Name) + "</td>" +
        "<td></td>" +
        "<td><img class=\"btn delete-row-btn\" alt=\"\" src=\"/Orion/images/delete_icon_gray_16x16.png\" title=\"@{R=ESI.Strings;K=ESI_Action_Grid_Delete_Button_Title; E=js}\"></td>" +
        "</tr>";
}

function getCustomAttrRow(property, rowData) {

    var rowId = property + "_" + rowData.index;
    return "<tr class=\"data-row editable-row\" data-index=\"" + rowData.index + "\">" +
        "<td class=\"data-column\"><textarea class=\"Name no-new-lines\" validation-unique=\"Name\" validation-length=\"255\">" + rowData.Name + "</textarea></td>" +
        "<td class=\"data-column\"><textarea class=\"Value\" data-form=\"" + rowId + "\">" + rowData.Value + "</textarea></td>" +
        "<td><img class=\"btn add-macro-row-btn update-attr-button\" data-macro=\"" + rowId + "\" alt=\"\" src=\"/Orion/images/add_16x16.gif\" title=\"@{R=ESI.Strings;K=ESI_Action_Grid_Add_Variable_Button_Title; E=js}\"></td>" +
        "<td><img class=\"btn delete-row-btn\" alt=\"\" src=\"/Orion/images/delete_icon_gray_16x16.png\" title=\"@{R=ESI.Strings;K=ESI_Action_Grid_Delete_Button_Title; E=js}\"></td>" +
        "</tr>";
}