﻿(function (window) {
    var repository = {};

    if (typeof use !== "undefined") {
        throw new Error("Global naming conflict: 'use' is already defined.");
    }

    var module = createNamespace("SW.Core.Module", repository);
    module.exports = function (namespace, exports) {
        var module = createNamespace(namespace, repository);
        module.exports = exports;
        return module;
    }
    


    use = function (namespace) {
        var ns = traverse(namespace, repository);
        if (typeof ns === "object" && typeof ns.exports === "function") {
            return ns.exports;
        }
        throw new Error(namespace + " module is not loaded or does not define any exports.");
    }
    
    function traverse(path, target) {
        var parts = path.split(".");
        var current = target || {};
        for (var i in parts) {
            var item = parts[i];
            if (current.hasOwnProperty(item)) {
                var current = current[item];

                if (typeof current === "object") {
                    continue;
                }
            }
            break;
        }
        return current;
    }

    function createNamespace(namespace, target) {
        var parts = namespace.split(".");
        var current = target || {};

        for (var i in parts) {
            var item = parts[i];
            var ns = current[item] || {};
            current[item] = ns;
            current = ns;
        }
        return current;
    }

})(window);