﻿(function (window) {

    function makeApiCall(baseUrl, proto, member, handler) {
        return function (data) {
            return $.ajax(baseUrl + "/" + member, {
                type: "POST",
                success: function (response) {
                    handler.call(proto, response.d || response);
                },
                error: function (xhr, status, error) {

                    var errorText = error;
                    if (typeof xhr.responseText !== "undefined") {
                        try {
                            errorText = JSON.parse(xhr.responseText);
                        } catch (e) {
                        }
                    }

                    handler.call(proto, { error: errorText, status: status });
                },
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json;charset=utf-8"
            });
        }
    }

    function service(baseUrl, proto) {

        var api = {};

        for (var member in proto) {
            if (proto.hasOwnProperty(member)) {
                var handler = proto[member];
                if (typeof handler === "function") {

                    api[member] = makeApiCall(baseUrl, proto, member, handler);
                }
            }
        }

        return api;
    }

    $(window.document).ready(function () {
        (use("SW.Core.Module"))("SW.Core.Services", service);
    });

})(window);