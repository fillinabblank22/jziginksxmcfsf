﻿(function (window) {

    var ExecutionMode = {
        TRIGGER: 0,
        RESET : 1
    }; 

    // Action properties specification
    //we need it to translate model properties to action properties
    var spec = {
        InstanceID: "instanceId",
        Settings: "result",
        VisibleProperties: "visibleProperties"
    };

    function _pack(obj) {

        var result = [];
        for (var member in spec) {
            if (obj.hasOwnProperty(spec[member])) {
                result.push({ PropertyName: member, PropertyValue: obj[spec[member]] });
            }
        }

        return result;
    }

    function pack(obj, fields) {
        var settings = {}
        var visibleProps = [];
        for (var index = 0; index < fields.length; index++) {
            var field = fields[index];
            if (obj.hasOwnProperty(field.name)) {
                settings[field.name] = obj[field.name];
            }

            if (obj.hasOwnProperty(field.name + '_hidden')) {
                if (obj[field.name + '_hidden'] === false) {
                    visibleProps.push(field.name);
                }
            }
        }

        return _pack({ instanceId: obj.instanceId, visibleProperties: visibleProps.join(','), result: JSON.stringify(settings) });
    }

    function _unpack(arr, target) {
        var result = target || {};
        for (var index = 0; index < arr.length; index++) {
            var pair = arr[index];

            if (spec.hasOwnProperty(pair.PropertyName)) {
                result[spec[pair.PropertyName]] = pair.PropertyValue;
            }
        }
        return result;
    }

    function shallowCopy(source, target) {
        for (var key in source) {
            if (source.hasOwnProperty(key)) {
                target[key] = source[key];
            }
        }
    }

    function unpack(arr, target) {
        target = target || {};

        var actionDefinition = _unpack(arr);
        if (typeof actionDefinition.result !== "undefined") {

            var settings = JSON.parse(actionDefinition.result);
            shallowCopy(settings, target);
        }

        target.instanceId = actionDefinition.instanceId;
        return target;
    }

    function setupVisibility(config, fields) {
        var actionDefinition = _unpack(config);

        if (typeof actionDefinition.result !== "undefined") {
            // Use existing settings
            var settings = JSON.parse(actionDefinition.result);

            if (actionDefinition.visibleProperties) {
                var visibleProps = actionDefinition.visibleProperties.split(',');
                var visible = {};

                for (var i = 0; i < visibleProps.length; i++) {
                    visible[visibleProps[i]] = true;
                }

                for (var i = 0; i < fields.length; i++) {
                    if (visible[fields[i].name] === true) {
                        fields[i].hidden = false;
                    }
                    else {
                        fields[i].hidden = true;
                    }
                }
            }
        }
        else {
            // Setup from default settings
            for (var i = 0; i < fields.length; i++) {
                fields[i].hidden = !fields[i].defaultField;
            }
        }
    }


    var severityMapping = {
        "Notice": ["3", "3"],
        "Informational": ["3", "3"],
        "Warning": ["2", "2"],
        "Serious": ["1", "1"],
        "Critical": ["1", "1"]
    };

    function getUrgency(severity) {
        return severityMapping[severity][1];
    }

    function getImpact(severity) {
        return severityMapping[severity][0];
    }


    function main(bind, services, config) {

        var scope = {};
        var serviceNowData = {};

        var plugin = {
            validateSectionAsync: function (section, callback) {
                if (typeof callback === "function") {
                    callback(!model.getDataError);
                }

            },
            getActionDefinitionAsync: function (callback) {

                //Make sure we get everything from controls, even if didn't change anything
                model.$update(function () {

                    var templateFields = serviceNowData.fields;
                    templateFields.push({ name: "_closeCode" });
                    templateFields.push({ name: "_resetState" });
                    templateFields.push({ name: "_reopenState" });
                    templateFields.push({ name: "_acknowledgeState" });
                    templateFields.push({ name: "_closeNote" });
                    templateFields.push({ name: "_resetComment" });

                    config.actionDefinition.ActionProperties = pack(model, templateFields);

                    scope.$release();

                    callback(config);
                });

            },
            getUpdatedActionProperties: function (callback) {

                model.$update(function () {
                    config.actionDefinition.ActionProperties = pack(model, serviceNowData.fields);
                    callback(config);
                });

            }
        };

        if (!config.isActionSupported) {

            $("#createActionDefinitionBtn").hide();
            $("#nextSectionInActionDefinitionBtn").hide();
            $("#addActionDefinitionBtn").hide();
            $('#_basicActionPlugins_').remove();
            $('#_topActionPlugins_').remove();
            $('#_timeOfDayActionPlugins_').remove();
            return config.onReadyCallback(plugin);
        }
        var serviceEndpoint = "/api/ServiceNow"; // "/Orion/ESI/Actions/Controls/CreateTicketView.asmx"
        var service = services(serviceEndpoint, {
            getInstances: function (response) {
                if (response.error) {
                    //handle error

                    return;
                }

                model.instances = response.result || [];
                model.editing = typeof model.instanceId != "undefined";
                model.showEditingInfo = model.editing && !model.getDataError;

                // If there is no instance selected (i.e. new action) and there is something to select from
                // select the first one in the list.
                if (typeof model.instanceId == "undefined" && model.instances.length > 0) {
                    model.instanceId = model.instances[0].id;
                }

                if (typeof model.instanceId !== "undefined") {
                    service.getData({ instanceId: model.instanceId });

                } else {
                    notifyPluginReady();
                }
            },
            getData: function (response) {
                if (response.error) {
                    //handle error

                    model.getDataError = true;

                    serviceNowData = {
                        fields: []
                    };

                } else {


                    model.getDataError = false;

                    serviceNowData = response.result;
                }

                setupUI();
                model.loading = false;
            }
        });

        var model = {
            changeHandler: function (property) {
                var self = this;
                var field = _.find(serviceNowData.fields, function (item) { return item.dependsOn === property });

                if (typeof field === "undefined") {
                    return;
                }

                var dependency = _.find(serviceNowData.fields, function (item) { return item.name === property });

                if (typeof dependency === "undefined") {
                    return;
                }

                var dependencyValue = self[property];
                self[field.dataSource] = _.filter(serviceNowData[field.dataSource], function (item) { return item[field.dependsOn] === dependencyValue });
            },

            instanceChanged: function () {
                var self = this;
                if (typeof self.instanceId !== "undefined") {
                    this.loading = true;
                    $("#CreateTicketActionFields :input").prop("disabled", true);
                    service.getData({ instanceId: self.instanceId });
                }
            },

            showNotification: false,
            getDataError: false,
            multiedit: true,
            editing: false,
            loading: false,
            showEditingInfo: false,
            hasIntegrationAction: config.viewContext.ExecutionMode === ExecutionMode.TRIGGER && config.hasIntegrationAction(),
            resetTab: config.viewContext.ExecutionMode === ExecutionMode.RESET
        };

        //Hack, find and bind scope after plugin is shown
        model.urgency = getUrgency(config.severity); //default value when it's new
        model.impact = getImpact(config.severity); //default value when it's new

        unpack(config.actionDefinition.ActionProperties, model);
        model.actionID = config.actionDefinition.ID;
        service.getInstances();

        function getMacroPickerDialogHnadler() {
            var macroElements = $('[data-macro]');
            
            if (macroElements.length == 0)
                return null;
                
            var events = macroElements.data("events");
            
            if (typeof events === "undefined")
                return null;

            if (events.click.length == 0)
                return null;
            
            return events.click[0].handler;
        }

        function setupUI() {
            //Create UI based on available fields
            var contentHtml = "";
            var refreshList = []; // list of properties which should be refreshed when binding is complete
            var updateList = [];
            var macroPickerHandler = getMacroPickerDialogHnadler();

            serviceNowData.fields = serviceNowData.fields || [];

            setupVisibility(config.actionDefinition.ActionProperties, serviceNowData.fields);

            for (var index = 0; index < serviceNowData.fields.length; index++) {
                var field = serviceNowData.fields[index];

                if (!model.hasOwnProperty(field.name + '_hidden')) {
                    model[field.name + '_hidden'] = field.hidden;
                }

                if (field.type === "choice") {

                    if (!model.hasOwnProperty(field.name) && field.defaultValue != null) {
                        model[field.name] = field.defaultValue;
                    }

                    contentHtml += '<div class="esi-control-group esi-condensed" esi-hide="' + field.name + '_hidden">'
                        + '<label automation="CreateTicketAction_IncidentProperties' + field.name + '">' + field.display + '</label>'
                        + '<select esi-items="' + field.dataSource + '" ' + "esi-items-spec='" + '{"value":"value","display":"name"}' + "' "
                        + 'esi-bind="' + field.name + '" esi-change="changeHandler" automation="CreateTicketAction_IncidentProperties_' + field.name + '_Select"></select></div>';

                    if (field.dependsOn === "") {
                        model[field.dataSource] = serviceNowData[field.dataSource];

                    } else {
                        model[field.dataSource] = _.filter(serviceNowData[field.dataSource], function (item) { return item[field.dependsOn] === model[field.dependsOn] });
                        updateList.push(field.dependsOn);
                        refreshList.push(field.name);
                    }
                    continue;
                }

                if (field.type === "line" || field.type === "text" || field.type === "reference") {
                    contentHtml += makeTextSnippet(field);
                }
            }

            contentHtml += "<div class='esi-control-group esi-condensed'>" +
                           "<a id='add_property_button' class='esi-btn-small esi-btn-secondary' automation='add_property' href='#'>@{R=SNI.Strings;K=SelectProperties;E=js}</a>" +
                           "</div>";

            // Replace the html content
            var container = $("#CreateTicketActionFields");
            container.html(contentHtml);

            // Fix the macro pickers
            if (macroPickerHandler != null) {
                $("[sni-macro-picker]").unbind("click");
                $("[sni-macro-picker]").click(macroPickerHandler);
            }

            // Bind add property onclick event
            $("#add_property_button").click(function () {
                var properties = [];

                for (var index = 0; index < serviceNowData.fields.length; index++) {
                    var field = serviceNowData.fields[index];
                    properties.push({
                        id: serviceNowData.fields[index].name,
                        caption: field.display,
                        checked: !model[field.name + '_hidden']
                    });
                }

                SW.ESI.PropertyDialog.show(properties, function (submit, selection) {
                    if (!submit)
                        return;

                    for (var index = 0; index < selection.length; index++) {
                        model[selection[index].id + '_hidden'] = !selection[index].checked;
                    }
                });
            });

            model.statuses = serviceNowData.statuses;
            model.closeCodes = serviceNowData.closeCodes;

            model._resetState = typeof model._resetState === "undefined" ? serviceNowData.defaultState : model._resetState;
            model._reopenState = typeof model._reopenState === "undefined" ? serviceNowData.defaultReopenState : model._reopenState;
            model._closeCode = typeof model._closeCode === "undefined" ? serviceNowData.defaultCloseCode : model._closeCode;
            model._closeNote = model._closeNote || serviceNowData.defaultCloseNote;

            var state = {};
            var savedState = _unpack(config.actionDefinition.ActionProperties);
            if (typeof savedState.result !== "undefined") {
                state = JSON.parse(savedState.result);
            }

            scope = $('[esi-scope="CreateTicketAction"]')[0];
            bind(scope, model, function () {
                // on binding fully completed check if some of the values are not bound to the proper values and show a notification in this case

                var snapshot = scope.$snapshot();

                for (var key in state) {
                    if (state.hasOwnProperty(key)) {

                        if (state[key] !== snapshot[key]) {

                            model.$update(key);

                            if (!model.getDataError) {
                                model.showNotification = true;
                            }
                        }
                    }
                }

                if (config.actionDefinition.ActionProperties.length == 0) {
                    // new instance, need to update model with UI state
                    for (var index = 0; index < updateList.length ; index++) {
                        model.$update(updateList[index]);
                    }

                } else {
                    // loaded instance, need to update UI with model state
                    for (var index = 0; index < refreshList.length ; index++) {
                        model.$refresh(refreshList[index]);
                    }
                }

                notifyPluginReady();

                // If editing, display the incident properties section
                if (model.editing && !model.getDataError && !model.showNotification) {
                    $('#incidentPropertiesSection').trigger('click');
                }
            });
        }

        var pluginReady = false;
        function notifyPluginReady() {
            if (!pluginReady) {
                pluginReady = true;
                config.onReadyCallback(plugin);
            }

        }

        function makeTextSnippet(field) {
            if (field.type === "reference") {
                var html = '<div class="esi-control-group esi-condensed" esi-hide="' + field.name + '_hidden">'
                        + '<label automation="CreateTicketAction_IncidentProperties' + field.name + '">' + field.display
                        + '<img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" alt="" style="vertical-align: text-top;margin-left:10px">'
                        + '<span> @{R=SNI.Strings;K=ThisIsReferenceField;E=js}'
                        + '<a target="_blank" href="http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&amp;topic=orioncoreag-alertstriggeractionservicenowincident.htm"> @{R=SNI.Strings;K=HowCanIUseIt;E=js}'
                        + '</a></span></label>';                
            } else {
                var html = '<div class="esi-control-group esi-condensed" esi-hide="' + field.name + '_hidden">'
                        + '<label automation="CreateTicketAction_IncidentProperties' + field.name + '">' + field.display + '</label>';
            }
            

            if (field.type === "line" || field.type === "reference") {
                html += '<input type="text" esi-bind="' + field.name + '" data-form="' + field.name + '"  id="' + field.name + '" automation="CreateTicketAction_IncidentProperties_' + field.name
                        + '_TextBox" value="' + field.defaultValue + '">';
            } else if (field.type === "text") {
                html += '<textarea rows="3" cols="66" esi-bind="' + field.name + '" data-form="' + field.name + '"  id="' + field.name + '" automation="CreateTicketAction_IncidentProperties_' + field.name
                        + '_TextBox" >' + field.defaultValue + '</textarea>';
            }

            html += '<a href="#" class=\"esi-btn-small esi-btn-secondary insert_variable\" '
                    + 'id="CreateTicketAction_IncidentProperties_InsertVar_' + field.name + '" sni-macro-picker="true" data-macro="' + field.name + '">'
                    + config.insertVariableLabel + '</a></div>';

            return html;
        }
    }

    $(window.document).ready(function () {
        SW.Core.namespace("SW.Core.Actions").CreateTicketController = function (config) {
            var bind = use("SW.Core.Bind");
            var services = use("SW.Core.Services");
            main(bind, services, config);

        };

    });

})(window);


