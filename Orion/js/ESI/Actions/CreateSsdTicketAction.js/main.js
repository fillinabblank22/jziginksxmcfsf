﻿(function (window) {
    var currentModelProperty;
    var currentModelPropertyItem;

    var ExecutionMode = {
        TRIGGER: 0,
        RESET: 1
    };

    // Action properties specification
    // we need it to translate model properties to action properties
    var spec = {
        InstanceID: "instanceId",
        EntityAttributes: "entityAttributes",
        CustomAttributes: "customAttributes"
    };

    function _pack(obj) {
        var result = [];
        for (var member in spec) {
            if (obj.hasOwnProperty(spec[member])) {
                result.push({ PropertyName: member, PropertyValue: obj[spec[member]] });
            }
        }
        return result;
    }

    function pack(obj) {
        return _pack(
            {
                instanceId: obj.instanceId,
                entityAttributes: JSON.stringify(deindexObjects(obj.entityAttributes)),
                customAttributes: JSON.stringify(deindexObjects(obj.customAttributes))
            });
    }

    function _unpack(arr, target) {
        var result = target || {};
        for (var index = 0; index < arr.length; index++) {
            var pair = arr[index];

            if (spec.hasOwnProperty(pair.PropertyName)) {
                result[spec[pair.PropertyName]] = pair.PropertyValue;
            }
        }
        return result;
    }

    function unpack(arr, target) {
        target = target || {};

        var actionDefinition = _unpack(arr);

        if (typeof actionDefinition.entityAttributes !== "undefined") {
            target.entityAttributes = JSON.parse(actionDefinition.entityAttributes);
            indexObjects(target.entityAttributes);
        }

        if (typeof actionDefinition.customAttributes !== "undefined") {
            target.customAttributes = JSON.parse(actionDefinition.customAttributes);
            indexObjects(target.customAttributes);
        }

        target.instanceId = actionDefinition.instanceId;
        return target;
    }

    function indexObjects(array) {
        $.each(array,
            function (index, item) {
                item["index"] = index;
            });
        return array;
    }

    function deindexObjects(array) {
        $.each(array,
            function (index, item) {
                delete item["index"];
            });
        return array;
    }

    function main(bind, services, config) {
        var isValid = false;
        var pluginReady = false;
        var instancesLoaded = false;
        var schemaLoaded = false;
        var scope = {};

        var plugin = {
            validateSectionAsync: function (section, callback) {
                if (typeof callback === "function") {
                    isValid = section !== "ssdCustomAttributesSection"
                        || (!model.customAttrRequiredError && !model.customAttrUniqueError && !model.customAttrLengthError);
                    callback(isValid);
                }
            },
            getActionDefinitionAsync: function (callback) {
                //Make sure we get everything from controls, even if didn't change anything
                model.$update(function () {
                    config.actionDefinition.ActionProperties = pack(model);
                    scope.$release();
                    callback(config);
                });
            },
            getUpdatedActionProperties: function (callback) {
                model.$update(function () {
                    config.actionDefinition.ActionProperties = pack(model);
                    callback(config);
                });
            }
        };

        if (!config.isActionSupported) {
           
            return config.onReadyCallback(plugin);
        }

        var serviceEndpoint = "/api/ServiceDesk";
        var service = services(serviceEndpoint, {
            getInstances: function (response) {
                if (response.error) {
                    //handle error
                    return;
                }

                model.instances = response.result || [];

                // If there is no instance selected (i.e. new action) and there is something to select from
                // select the first one in the list.
                if (typeof model.instanceId == "undefined" && model.instances.length > 0) {
                    model.instanceId = model.instances[0].id;
                }

                instancesLoaded = true;
                notifyPluginReady();
            },
            getEntitySchema: function (response) {
                if (response.error) {
                    //handle error
                    return;
                }

                if (response.result) {
                    model.entityAttributes = response.result;
                    indexObjects(model.entityAttributes);
                }

                schemaLoaded = true;
                notifyPluginReady();
            }
        });

        var model = {
            customAttrRequiredError: false,
            customAttrUniqueError: false,
            customAttrLengthError: false,
            editing: false,
            showEditingInfo: false,
            hasIntegrationAction: config.hasIntegrationAction(),
            resetTab: config.viewContext.ExecutionMode === ExecutionMode.RESET
        };

        unpack(config.actionDefinition.ActionProperties, model);
        model.actionID = config.actionDefinition.ID;
        model.showEditingInfo = model.editing = typeof model.instanceId != "undefined";

        scope = $('[esi-scope="CreateSsdTicketAction"]')[0];
        bind(scope, model, function() {
            service.getInstances();
            if (!model.editing) {
                service.getEntitySchema({ entityType: config.alertSwisObjectType });
                addDefaultCustomAttributes();
            }
        });

        $(".add-attr-button[data-macro]").on("click", function () {
            currentModelProperty = $(this).data("macro");
        });

        $(document).on("click", ".update-attr-button[data-macro]", function () {
            currentModelPropertyItem = $(this).data("macro");
        });

        $(document)
            .one("click", "#createActionDefinitionBtn, #addActionDefinitionBtn, #cancelActionDefinitionBtn",
                function () {
                    if (isValid) {
                        SW.Core.MacroVariablePickerController.SetOnInsertHandler(undefined);
                        $(document).off("dialogclose", "div.action-plugin-definition-dialog");
                        $(document).off("dialogclose", "div.macro-variable-picker-dialog");
                        $(document).off("click", "#selectVariableButtons a");
                    }
                });

        $(document)
            .one("dialogclose", "div.action-plugin-definition-dialog",
                function () {
                    if (isValid) {
                        SW.Core.MacroVariablePickerController.SetOnInsertHandler(undefined);
                        $(document).off("click",
                            "#createActionDefinitionBtn, #addActionDefinitionBtn, #cancelActionDefinitionBtn");
                        $(document).off("dialogclose", "div.macro-variable-picker-dialog");
                        $(document).off("click", "#selectVariableButtons a");
                    }
                });

        /* Hide 'Define SQL/SWQL Variable' checkbox in macrovariable picker dialog for entity attributes*/
        $(document)
            .off("click", "#add_entity_attr_button")
            .on("click",
                "#add_entity_attr_button",
                function () {
                    $("span[data-form=\"enableSqlVariable\"]").hide();
                });

        $(document)
            .on("click", "#selectVariableButtons a",
                function () {
                    $(document).off("dialogclose", "div.macro-variable-picker-dialog");
                    $("span[data-form=\"enableSqlVariable\"]").show();
                });

        $(document)
            .on("dialogclose", "div.macro-variable-picker-dialog",
                function () {
                    $(document).off("click", "#selectVariableButtons a");
                    $("span[data-form=\"enableSqlVariable\"]").show();
                });

        $(document)
            .off("keydown", "table[esi-grid] textarea.no-new-lines")
            .on("keydown", "table[esi-grid] textarea.no-new-lines", function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                }
            });

        $(document)
            .off("paste", "table[esi-grid] textarea.no-new-lines")
            .on("paste", "table[esi-grid] textarea.no-new-lines", function (e) {
                var elem = $(this);
                setTimeout(function () {
                    var newVal = elem.val().replace(/\s\s+/g, " ");
                    elem.val($.trim(newVal));
                    if (elem[0].scrollHeight > 0) {
                        $(elem[0])
                            .height(21)
                            .css("padding-bottom", "3px")
                            .attr('rows', 1)
                            .height(elem[0].scrollHeight);
                    }
                }, 100);
            });

        SW.Core.MacroVariablePickerController.SetOnInsertHandler(function (variables) {
            if (currentModelProperty) {

                if (currentModelProperty === "entityAttributes") {
                    handleEntityAttributes(currentModelProperty, variables);
                } else if (currentModelProperty === "customAttributes") {
                    handleCustomAttributes(currentModelProperty, variables);
                }

                model.$refresh(currentModelProperty);
                currentModelProperty = undefined;
            } else if (currentModelPropertyItem) {
                var splitted = currentModelPropertyItem.split("_");
                var propName = splitted[0];
                var propItemIndex = splitted[1];
                var modelItem = model[propName].find(x => x["index"] === parseInt(propItemIndex));

                $.each(variables,
                    function (index, item) {
                        modelItem.Value += item.Value;
                    });
                $("table[esi-grid='propName'] textarea[data-form='" + currentModelPropertyItem + "']").trigger("input");
                model.$refresh(propName);
                currentModelPropertyItem = undefined;
            }
        });
        
        function notifyPluginReady() {
            if (!pluginReady
                && instancesLoaded
                && (model.editing || schemaLoaded)) {
                pluginReady = true;
                config.onReadyCallback(plugin);
            }
        }

        function addDefaultCustomAttributes() {
            model.customAttributes = [
                { Name: "Title", Value: "${N=Alerting;M=AlertName}" },
                { Name: "Description", Value: "${N=Alerting;M=AlertMessage}" }
            ];
            indexObjects(model.customAttributes);
        }

        function getLastIndexFromGrid(currentGridProperty) {
            var lastIndex = 0;
            if (model[currentGridProperty].length > 0) {
                lastIndex = Math.max.apply(Math,
                    model[currentGridProperty].map(function (obj) { return obj.index; }));
            }
            return lastIndex;
        }

        function handleEntityAttributes(currentModelProperty, variables) {
            var lastIndex = getLastIndexFromGrid(currentModelProperty);

            $.each(variables,
                function (index, item) {
                    var objWithSameName = getObjWithSameName(model[currentModelProperty], "DisplayName", item.Name);

                    if (objWithSameName) {
                        item.Name = getUniqueName(model[currentModelProperty], "DisplayName", item.Name);
                    }

                    model[currentModelProperty].push({ DisplayName: item.Name, Name: parseVariableFromMacroString(item.Value), Macro: item.Value, index: ++lastIndex });
                });
        }

        function handleCustomAttributes(currentModelProperty, variables) {
            var lastIndex = getLastIndexFromGrid(currentModelProperty);

            var attrName = "Attribute";

            if (variables[0].Name === "") {
                variables[0].Name = attrName;
            }

            if (variables.length === 1) {
                var objWithSameName = getObjWithSameName(model[currentModelProperty], "Name", variables[0].Name);

                if (objWithSameName) {
                    variables[0].Name = getUniqueName(model[currentModelProperty], "Name", variables[0].Name);
                }

                model[currentModelProperty].push({ Name: variables[0].Name, Value: variables[0].Value, index: ++lastIndex });
                return;
            }

            var objWithSameName = getObjWithSameName(model[currentModelProperty], "Name", attrName);

            if (objWithSameName) {
                attrName = getUniqueName(model[currentModelProperty], "Name", attrName);
            }

            var attrValue = "";

            $.each(variables,
                function (index, item) {
                    attrValue += item.Value;
                });

            model[currentModelProperty].push({ Name: attrName, Value: attrValue, index: ++lastIndex });
        }

        function getUniqueName(array, prop, value) {
            var num = 2;

            while (true) {
                var newName = value + " " + num++;
                var objWithSameName = getObjWithSameName(array, prop, newName);
                if (!objWithSameName) {
                    return newName;
                }
            }
        }

        function getObjWithSameName(array, prop, value) {
            return array.find(x => x[prop] === value);
        }

        function parseVariableFromMacroString(macroString) {
            var result = macroString;
            var str = trimChar(macroString.trim(), "${");
            var strArr = trimChar(str, "}").split(";");
            $.each(strArr, function (index, item) {
                if (item.startsWith("M=")) {
                    result = item.substring(2);
                    return false;
                }
            });
            return result;
        }

        function trimChar(str, char) {
            if (char === "]") char = "\\]";
            if (char === "\\") char = "\\\\";
            return str.replace(new RegExp("^[" + char + "]+|[" + char + "]+$", "g"), "");
        }
    }

    $(window.document).ready(function () {
        SW.Core.namespace("SW.Core.Actions").CreateSsdTicketController = function (config) {
            var bind = use("SW.Core.Bind");
            var services = use("SW.Core.Services");
            main(bind, services, config);
        };
    });
})(window);


