﻿(function () {

    var ui = SW.Core.namespace('SW.Core.WorldMap.Viewer.UI');
    var pane = ui.SelectionPane = {};
    var texts = {
        'loading': '@{R=Core.Strings;K=WEBJS_PCC_WM_LOADING; E=js}'
    };

    var reload_pane = function (mapstate, swdata) {
        $('#poipane .sw-wmap-selected').text(texts['loading']);
        $('#poipane .sw-wmap-details').text('');

        SW.Core.Loader.Control('#poipane', {
            Control: '/Orion/WorldMap/SelectionPane.ascx',
            'config.Latitude': swdata.Latitude,
            'config.Longitude': swdata.Longitude,
            'config.Zoom': mapstate.map.getZoom(),
            'config.ResourceId': mapstate.config.resourceId || '',
            'config.Undo': mapstate.config.undomode ? 'true' : 'false'
        }, {}, 'replace', function (e) {
            if (e == 'session.timeout')
                window.location = '/orion/login.aspx?sessionTimeout=yes&ReturnUrl=' +
                        encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + '&';
        });
    };

    var internal_init = function (config) {

        vol = SW.Core.namespace('SW.Core.Volatile.WorldMaps');
        var map = vol['0'] = SW.Core.WorldMap.Editor.Create('worldmap0', config);

        // -- selection pane

        map.on('refresh.found', function (mapstate, mark) {
            if (! $('body').hasClass('sw-wmap-showpane'))
                reload_pane(mapstate, mark.swdata);
        });

        map.on('selected', function (mapstate, item) {
            if (!item) pane.Hide(false);
            else reload_pane(mapstate, item);
        });

        ui.ResizeSync();
        map.Refresh({ Longitude: config.lng, Latitude: config.lat });
    };

    pane.Show = function (force) { $('body').addClass('sw-wmap-showpane'); pane.ResizeSync(); };
    pane.Hide = function (force) { $('body').removeClass('sw-wmap-showpane'); pane.ResizeSync(); };
    pane.Total = function (n) { };

    pane.ResizeSync = function () {
        var mapdom = $('#worldmap0'), detdom = $('.sw-wmap-details');
        if (mapdom[0] && detdom[0]) {
            var my = mapdom.offset().top, mh = mapdom.height(), dy = detdom.offset().top, dh = (my + mh) - (dy);
            detdom.height(dh);
        }

        /*
        var map = SW.Core.Volatile.WorldMaps['0'];
        if (map) {
        var m = map.leaflet();
        m.invalidateSize(false);
        }*/
    };

    ui.Init = function (config) {

        SW.Core.namespace('SW.Core.Volatile.WorldMaps');
        $('body').click(SW.Core.WorldMap.Editor.UI.UnselectOnBodyClick);
        $(window).resize(ui.ResizeSync);
        $(window).load(function () { internal_init(config); });
    };

    ui.RefreshCount = function () {

        var base = SW.Core.WorldMap._debug;
        var mapstate = base.getstate(0);

        SW.Core.Loader.Control('#poiselection', {
            Control: '/Orion/WorldMap/SelectionSummary.ascx',
            'config.ResourceId': mapstate.config.resourceid || ''
        }, {}, 'replace', function (e) {
            if (e == 'session.timeout')
                window.location = '/orion/login.aspx?sessionTimeout=yes&ReturnUrl=' +
                        encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + '&';
        });
    };

    ui.Refresh = function () {
        var map = SW.Core.Volatile.WorldMaps['0'];
        map.Refresh();
        ui.RefreshCount();
    };

    ui.ResizeSync = function () {

        var footer = $('#footer');
        var fH = footer.is(':visible') ? $('#footer').height() + 20 : 6; // 20px to cover the 'footermark'
        var hH = $('#maptop').offset().top;
        var wH = $(window).height();
        var bbH = $('#mapbtnbar').outerHeight();

        if (wH < 600)
            wH = 600;

        $('#worldmap0').height(wH - fH - bbH - hH);
        pane.ResizeSync();

        var map = SW.Core.Volatile.WorldMaps['0'];

        if (map) {
            var m = map.leaflet();
            m.invalidateSize(false);
        }
    };

    ui.FullScreen = function (enter) {

        if (enter) {
            $('#pageHeader').hide();
            $('#footermark').hide();
            $('#footer').hide();
        } else {
            $('#pageHeader').show();
            $('#footermark').show();
            $('#footer').show();
        }

        ui.ResizeSync();
    };

})();
