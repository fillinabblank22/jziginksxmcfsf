﻿(function () {
    var ui = SW.Core.namespace('SW.Core.WorldMap.Editor.UI');
    var pane = ui.SelectionPane = {};
    var supportedEntities = [];
    var ext = window.Ext4 || window.Ext;
    var dlgstate = { lat: 0, lng: 0, addmode: false };
    var undo = ui.Undo = { pending: 0, ignore: false };

    var texts = {
        'uxhint.clickmap': '@{R=Core.Strings;K=WEBJS_VL0_30; E=js}',
        'uxhint.clickaddnodes': '@{R=Core.Strings;K=WEBJS_VL0_31; E=js}',
        'loading': '@{R=Core.Strings;K=WEBJS_PCC_WM_LOADING; E=js}',
        'toolbar.placenodes': '@{R=Core.Strings;K=WEBJS_VL0_29; E=js}',
        'toolbar.editlocation': '@{R=Core.Strings;K=WEBJS_PCC_WM_TB_EDITLOCATION; E=js}',
        'toolbar.remove': '@{R=Core.Strings;K=WEBJS_PCC_WM_TB_REMOVE; E=js}',
        'dialog.remove.title': '@{R=Core.Strings;K=WEBJS_PCC_WM_REMOVETITLE; E=js}',
        'confirm.leavemap': '@{R=Core.Strings;K=WEBJS_PCC_WM_CONRIMLEAVE; E=js}',
        'button.placeonmap': '@{R=Core.Strings;K=WEBJS_PCC_WM_BTN_PLACE; E=js}',
        'helpful.groupselection': '@{R=Core.Strings;K=WEBJS_PCC_WM_HELPFUL_GROUPSEL; E=js}',
        'toomany.title': '@{R=Core.Strings;K=WEBJS_PCC_8; E=js}',
        'toomany.text': '@{R=Core.Strings;K=WEBJS_PCC_7; E=js}',
        'dialog.placenodes.title': '@{R=Core.Strings;K=WEBJS_VL0_29; E=js}',
        'dialog.editlocation.title': '@{R=Core.Strings;K=WEBJS_PCC_WM_DLG_EDITLOC_TITLE; E=js}',
        'button.savechanges': '@{R=Core.Strings;K=WEBJS_PCC_WM_BTN_SAVECHANGES; E=js}',
        'dialog.editselection.title': '@{R=Core.Strings;K=WEBJS_PCC_WM_DLG_EDITSEL_TITLE; E=js}',
        'undo.warning.1': '@{R=Core.Strings;K=WEBJS_WM_UNDO_WARNING_1; E=js}',
        'undo.warning.many': '@{R=Core.Strings;K=WEBJS_WM_UNDO_WARNING_MANY; E=js}'
    };

    var trunc = function (n) {
        // we're going to chop to 13 digits.
        var t = '' + n;
        var dot = t.indexOf('.');
        if (dot < 0) return n;
        var digits = t.length - dot - 1;
        if (digits < 13) return n;
        t = t.substring(0, dot + 13);
        return parseFloat(t);
    };

    var reload_pane = function (mapstate, swdata) {
        $('#poipane .sw-wmap-selected').text(texts['loading']);
        $('#poipane .sw-wmap-details').text('');

        SW.Core.Loader.Control('#poipane', {
            Control: '/Orion/WorldMap/SelectionPane.ascx',
            'config.Latitude': swdata.Latitude,
            'config.Longitude': swdata.Longitude,
            'config.Zoom': mapstate.map.getZoom(),
            'config.ResourceId': mapstate.config.resourceId || '',
            'config.Undo': mapstate.config.undomode ? 'true' : 'false'
        }, {}, 'replace', function (e) {
            if (e == 'session.timeout')
                window.location = '/orion/login.aspx?sessionTimeout=yes&ReturnUrl=' +
                        encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + '&';
        });
    };

    var setLocationName = function (location) {
        $('#location').val(location);
    };
    var getLocationName = function () {
        return $('#location').val();
    };

    var getSupportedEntities = function () {
        $.ajax({
            type: 'POST',
            url: '/orion/services/worldmap.aspx?op=get-entities',
            async: false, // on purpose
            dataType: 'json',
            success: function (result) {
                for (i = 0; i < result.data.length; i++) {
                    supportedEntities.push({ MasterEntity: result.data[i], SelectionMode: 'Multiple' });
                }
            }
        });
    };

    function EnableDisableGeolocation(val) {
        var $geolocation = $("#AutoGeolocation");
        setTimeout(function () {
            var name = 'AutomaticGeolocation-Enable';
            ORION.callWebService("/Orion/Services/NodeManagement.asmx",
                    "SaveBoolSetting", { name: name, value: val },
                    function (success) {
                        $("#Manage_AutoGeolocation").val(val ? 1 : 0);
                        if (val) {
                            $geolocation.prop("checked", true);
                        } else {
                            $geolocation.removeAttr("checked");
                        }
                    },
                    function (failure) { });
        });
    }

    function initMoreMenu() {
        $(".sw-hdr-links table td.sw-preference").on("click", function (event) {
            $(this).toggleClass("sw-preference-open");
            $(".sw-preference-more").toggleClass("sw-preference-more-open");
            $(this).find("div.sw-pref-options").toggleClass("sw-pref-options-hidden");
            $(this).closest("tr").toggleClass("sw-preference-row-open");
        });

        $("body").mousedown(function (event) {
            if ($(event.target).closest(".sw-pref-options").length == 0
                    && !$(event.target).hasClass("sw-preference")
                    && !$(event.target).hasClass("sw-preference-more")) {
                $("div.sw-pref-options").addClass("sw-pref-options-hidden");
                $(".sw-hdr-links table td.sw-preference").removeClass("sw-preference-open");
                $(".sw-preference-more").removeClass("sw-preference-more-open");
                $(".sw-preference-row-open").removeClass("sw-preference-row-open");
            }
        });

        var geo = $("#Manage_AutoGeolocation").val();
        if (geo == "1") {
            $("#AutoGeolocation").prop("checked", true);
        }

        $("#AutoGeolocation").on("change", function (event) {
            EnableDisableGeolocation($(this).prop("checked"));
        });

        if ($("html").hasClass("sw-is-msie")) {
            $("#adminContent .sw-preference").css("background-position-y", "2px");
            $(".sw-preference-more").css("padding-top", "0px");
        }

        // Setup left position of more menu
        var marginLeft = -149; // $(".sw-pref-options").css("margin-left");
        var $preferenceTextElem = $(".sw-preference span:first");
        var tmpPreferenceText = $preferenceTextElem.text();
        var preferenceCurWidth = $preferenceTextElem.width();
        var preferenceWithMoreTextWidth = $preferenceTextElem.text("More").width();
        $preferenceTextElem.text(tmpPreferenceText);
        var marginLeftNew = marginLeft - (preferenceWithMoreTextWidth - preferenceCurWidth);
        $(".sw-pref-options").css("margin-left", SW.Core.String.Format("{0}px", marginLeftNew));
    }

    var internal_init = function (config) {
        var lat = config.lat, lng = config.lng, zoom = config.zoom, editmode = !!config.editmode;

        vol = SW.Core.namespace('SW.Core.Volatile.WorldMaps');
        var map = vol['0'] = SW.Core.WorldMap.Editor.Create('worldmap0', config);

        // -- selection pane

        $(window).on('beforeunload', undo.Hook);

        map.on('refresh.found', function (mapstate, mark) {
            if (!$('body').hasClass('sw-wmap-showpane'))
                reload_pane(mapstate, mark.swdata);

            ext.getCmp('maptool-edit').enable();
            ext.getCmp('maptool-remove').enable();
        });

        map.on('selected', function (mapstate, item) {

            if (!item) {
                ext.getCmp('maptool-edit').disable();
                ext.getCmp('maptool-remove').disable();
                pane.Hide(false);
            } else {

                ext.getCmp('maptool-edit').enable();
                ext.getCmp('maptool-remove').enable();
            }

            $('#poipane .sw-wmap-selected').text(texts['loading']);
            $('#poipane .sw-wmap-details').text('');

            if (item == null) {
                return;
            }

            SW.Core.Loader.Control('#poipane', {
                Control: '/Orion/WorldMap/SelectionPane.ascx',
                'config.Latitude': item.Latitude,
                'config.Longitude': item.Longitude,
                'config.Zoom': mapstate.map.getZoom(),
                'config.ResourceId': mapstate.config.resourceId || '',
                'config.Undo': mapstate.config.undomode ? 'true' : 'false'
            }, {}, 'replace', function (e) {
                if (e == 'session.timeout')
                    window.location = '/orion/login.aspx?sessionTimeout=yes&ReturnUrl=' +
                        encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + '&';
            });
        });

        map.on('addmode.enter', function () {
            ui.ShowTip('location', true);
        });

        map.on('addmode.leave', function () {
            ext.getCmp('maptool-add').toggle(false);
            ui.ShowTip('location', false);
        });

        map.on('drag.finish', ui.RefreshCount);

        map.on('location.add', function (mapstate, lat, lng) { ui.AddLocation(lat, lng); });

        ui.ResizeSync();
        map.Refresh();
    };

    pane.Show = function (force) { $('body').addClass('sw-wmap-showpane'); pane.ResizeSync(); };
    pane.Hide = function (force) { $('body').removeClass('sw-wmap-showpane'); pane.ResizeSync(); };

    pane.Total = function (exist, changes) {

        undo.pending = changes;
        undo.ignore = false;

        if (!exist && !changes) ui.ShowTip('begin', true);
        else ui.ShowTip('begin', false);
    };

    pane.ResizeSync = function () {
        var mapdom = $('#worldmap0'), detdom = $('.sw-wmap-details');
        if (mapdom[0] && detdom[0]) {
            var my = mapdom.offset().top, mh = mapdom.height(), dy = detdom.offset().top, dh = (my + mh) - (dy);
            detdom.height(dh);
        }

        /*
        var map = SW.Core.Volatile.WorldMaps['0'];
        if (map) {
        var m = map.leaflet();
        m.invalidateSize(false);
        }*/
    };

    // -- undo

    undo.Ignore = function (e) {
        undo.ignore = true;
    };

    undo.Hook = function (e) {
        if (undo.ignore || !undo.pending) return;
        return String.format(texts['undo.warning.' + (undo.pending == 1 ? '1' : 'many')], undo.pending);
    };

    var tipdata = {
        location: { html: texts['uxhint.clickmap'], want: false },
        begin: { html: texts['uxhint.clickaddnodes'], want: false }
    };

    ui.ShowTip = function (tiptag, want) {
        var tip = tipdata[tiptag];
        if (tip) tip.want = want;

        var found = null;

        $.each(['location', 'begin'], function (k, o) {
            var tip = tipdata[o];
            if (tip.want) {
                found = tip;
                return false;
            }
        });

        var tipdom = $('#newtip');

        if (!found) {
            tipdom.hide();
            return;
        }

        if (!tipdom[0]) {
            $('body').append("<div id='newtip' style='max-width: 400px; position: absolute;'><span class='sw-suggestion'><span class='sw-suggestion-icon'></span>" + found.html + "</div>");
            tipdom = $('#newtip');
        } else {
            $('#newtip').html("<span class='sw-suggestion'><span class='sw-suggestion-icon'></span>" + found.html + "</div>");
        }

        tipdom.show();
        ui.TipResizeSync();
    };

    ui.TipResizeSync = function () {

        var wm = $('#worldmap0');
        var tip = $('#newtip');
        var off = wm.offset();
        var top = off.top;
        var left = off.left;
        var m_width = wm.width();
        var t_width = tip.width();
        var l_off = (m_width - t_width) / 2;

        $('#newtip').css({ 'left': (left + l_off) + 'px', 'top': top + 10 + 'px' });
    };

    ui.UnselectOnBodyClick = function (e) {

        var rules = ['#maptop', '#tool', '.sw-btn', '#newtip', 'a', '.ui-dialog', '.ui-widget-overlay', 'button'];

        var t = $(e.target);
        for (var i = 0, imax = rules.length; i < imax; ++i) if (t.filter(rules[i]).length) return;

        var p = t.parents();
        for (var i = 0, imax = rules.length; i < imax; ++i) if (p.filter(rules[i]).length) return;

        var map = SW.Core.Volatile.WorldMaps['0'];
        map.SetPOI(null);
    };

    ui.Init = function (config) {

        SW.Core.namespace('SW.Core.Volatile.WorldMaps');

        $('body').click(ui.UnselectOnBodyClick);

        var tb = new window.Ext.Toolbar({
            // Ext4.create('Ext.toolbar.Toolbar', {
            id: 'maptool',
            renderTo: 'tool',
            width: 500,
            listeners: { afterrender: function () {
                ui.ResizeSync();
            }
            },
            items: [{
                id: 'maptool-add',
                text: texts['toolbar.placenodes'],
                icon: '/orion/images/add_16x16.gif',
                enableToggle: true,
                pressed: false,
                handler: function () { SW.Core.Volatile.WorldMaps['0'].SetAddMode(this.pressed); }
            },
            '-',
            {
                id: 'maptool-edit',
                text: texts['toolbar.editlocation'],
                disabled: true,
                icon: '/orion/images/edit_16x16.gif',
                handler: function () { SW.Core.WorldMap.Editor.UI.EditLocation(); }
            },
            '-',
            {
                id: 'maptool-remove',
                text: texts['toolbar.remove'],
                disabled: true,
                icon: '/orion/images/delete_16x16.gif',
                handler: function () {

                    $('#removeconfirm').dialog({
                        position: 'center',
                        width: 320,
                        height: 'auto',
                        modal: true,
                        title: texts['dialog.remove.title'],
                        minHeight: 30,
                        show: { duration: 0 }
                    });
                }
            }
            /*,'->',
            {
            id: 'maptool-fullscreen',
            text: 'Full screen',
            disabled: false,
            enableToggle: true,
            pressed: false,
            handler: function () { ui.FullScreen(this.pressed); }
            }*/]
        });

        getSupportedEntities();
        $(window).resize(ui.ResizeSync);
        $(window).load(function () { initMoreMenu(); internal_init(config); });
    };

    ui.ResizeSync = function () {

        if (ext) {
            var toolbar = ext.get('maptool');
            var hW = $('#pageHeader').width();
            if (hW < 800) hW = 800;
            toolbar.setWidth(hW - 20); // 20px to cover left & right padding.
        };

        var footer = $('#footer');
        var fH = footer.is(':visible') ? $('#footer').height() + 20 : 6; // 20px to cover the 'footermark'
        var hH = $('#maptop').offset().top;
        var wH = $(window).height();
        var bbH = $('#mapbtnbar').outerHeight();

        if (wH < 600)
            wH = 600;

        $('#worldmap0').height(wH - fH - bbH - hH);
        pane.ResizeSync();
        ui.TipResizeSync();

        var map = SW.Core.Volatile.WorldMaps['0'];

        if (map) {
            var m = map.leaflet();
            m.invalidateSize(false);
        }
    };

    ui.ConfirmLeave = function () {
        return confirm(texts['confirm.leavemap']);
    };

    ui.FullScreen = function (enter) {

        if (enter) {
            $('#pageHeader').hide();
            $('#footermark').hide();
            $('#footer').hide();
        } else {
            $('#pageHeader').show();
            $('#footermark').show();
            $('#footer').show();
        }

        ui.ResizeSync();
    };

    ui.SelectorDialogFinish = function () {
        if (!dlgstate) return;
        var fn = dlgstate.addmode ? ui.AddLocationFinish : ui.EditSelectionFinish;
        fn();
    };

    ui.AddLocationFinish = function () {

        var ds = dlgstate;
        dlgstate = null;

        if (!ds || !ds.addmode) // not supporting edit mode just yet.
            return;

        var src = SW.Orion.NetObjectPicker.GetSelectedEntities() || [];

        if (!src.length) {
            $('#selectordlg').dialog('close');
            return;
        }

        var items = [];
        $.each(SW.Orion.NetObjectPicker.GetSelectedEntitiesWithInstanceSiteId() || [], function (i, o) { items.push({ Uri: o.Uri, InstanceSiteId: o.InstanceSiteId }); });
        var locationName = getLocationName();

        $.ajax({
            type: 'POST',
            url: '/orion/services/worldmap.aspx?op=set-poi',
            async: false, // on purpose
            dataType: 'json',
            data: JSON.stringify({
                changes: [{ Longitude: ds.lng, Latitude: ds.lat, LocationName: locationName, Items: items}]
            }),
            success: function () {
                var map = SW.Core.Volatile.WorldMaps['0'];
                map.Refresh({ Longitude: ds.lng, Latitude: ds.lat });
                ui.RefreshCount();
                $('#selectordlg').dialog('close');
            }
        });
    };

    ui.AddLocation = function (lat, lng) {

        $('#selectordlgprimary span.sw-btn-t').text(texts['button.placeonmap']);

        var map = SW.Core.Volatile.WorldMaps['0'];

        dlgstate = { lat: trunc(lat), lng: trunc(lng), addmode: true };

        SW.Orion.NetObjectPicker.SetUpEntities(supportedEntities, 'Orion.Nodes', [], 'multiple', false, true);
        SW.Orion.NetObjectPicker.SetText('toomany.title', texts['toomany.title']);
        SW.Orion.NetObjectPicker.SetText('toomany.text', texts['toomany.text']);

        setLocationName(null);

        var width = 890;

        $('#selectordlg').dialog({
            position: 'center',
            width: width,
            height: 'auto',
            modal: true,
            draggable: true,
            title: texts['dialog.placenodes.title'],
            show: { duration: 0 },
            close: function () {
                dlgstate = {};
                ext.getCmp('maptool-add').toggle(false);
                ui.ShowTip('begin', false);
                map.SetAddMode(false); // clear the pointer!
            }
        });
    };

    ui.Cancel = function () {
        $('#selectordlg').dialog('close');
    };

    ui.ClearSelection = function () {
        var swdata = SW.Core.Volatile.WorldMaps['0'].selected();
        if (!swdata) return;

        $.ajax({
            type: 'POST',
            url: '/orion/services/worldmap.aspx?op=set-poi',
            async: false, // on purpose
            dataType: 'json',
            data: JSON.stringify({
                changes: [{ Longitude: -1000.0, Latitude: -1000.0, Items: swdata.Items}]
            }),
            success: function () {
                var map = SW.Core.Volatile.WorldMaps['0'];
                map.SetPOI();
                map.Refresh();
                ui.RefreshCount();
            }
        });
    };

    ui.RefreshCount = function () {

        var base = SW.Core.WorldMap._debug;
        var mapstate = base.getstate(0);

        SW.Core.Loader.Control('#poiselection', {
            Control: '/Orion/WorldMap/SelectionSummary.ascx',
            'config.ResourceId': mapstate.config.resourceid || '',
            'config.Undo': mapstate.config.undomode ? 'true' : 'false'
        }, {}, 'replace', function (e) {
            if (e == 'session.timeout')
                window.location = '/orion/login.aspx?sessionTimeout=yes&ReturnUrl=' +
                        encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + '&';
        });
    };

    ui.EditLocationFinish = function() {
        var base = SW.Core.WorldMap._debug;
        var state = base.getstate(0);
        var map = state.map;
        var selected = state.selected;

        if (!selected)
            return;

            var lat = parseFloat(String($('#locationdlglat').val()).replace(Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator, "."));
            var lng = parseFloat(String($('#locationdlglng').val()).replace(Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator, "."));
            var locationName = getLocationName();

            if (isNaN(lat) || (lat < -90) || (lat > 90) ||
            isNaN(lng) || (lng < -180) || (lng > 180)) {
                $('#locationdlgerr').show();
                return;
            }

            lat = trunc(lat);
            lng = trunc(lng);

            $.ajax({
                type: 'POST',
                url: '/orion/services/worldmap.aspx?op=set-poi',
                async: false, // on purpose
                dataType: 'json',
                data: JSON.stringify({
                    changes: [{ Latitude: lat, Longitude: lng, Items: selected.swdata.Items, LocationName: locationName}]
                }),
                success: function () {
                    var map = SW.Core.Volatile.WorldMaps['0'];
                    map.Refresh({ Longitude: lng, Latitude: lat });
                    ui.RefreshCount();
                    $('#locationdlg').dialog('close');
                }
            });
        };

    ui.SaveAll = function () {
        $.ajax({
            type: 'POST',
            url: '/orion/services/worldmap.aspx?op=commit',
            dataType: 'json',
            data: '',
            success: ui.RefreshCount
        });
    };

    ui.EditLocation = function () {
        var base = SW.Core.WorldMap._debug;
        var state = base.getstate(0);
        var map = state.map;
        var selected = state.selected;

        if (!selected)
            return;

        var showPopup = function() {
            $('#locationdlglat').val(String(trunc(selected.swdata.Latitude)).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator));
            $('#locationdlglng').val(String(trunc(selected.swdata.Longitude)).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator));
            $('#locationdlgerr').hide();

            setLocationName(selected.swdata.LocationName);

            $('#locationdlg').dialog({
                position: 'center',
                width: 420,
                height: 'auto',
                modal: true,
                title: String.format(texts['dialog.editlocation.title'], selected.swdata.EditLabel),
                minHeight: 30,
                show: { duration: 0 }
            });
        };

        if (selected.swdata.AutoAdded) {
            Ext.MessageBox.show({
                title: '@{R=Core.Strings;K=WEBJS_SO0_96; E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_SO0_97; E=js}',
                buttons: { yes: '@{R=Core.Strings;K=WEBJS_SO0_98; E=js}', no: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                width: 550,
                fn: function(btn) {
                    if (btn == 'yes') {
                        showPopup();
                    } else {
                        return;
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        } else
            showPopup();
    };

    ui.EditSelectionFinish = function () {

        var savestate = dlgstate;
        $('#selectordlg').dialog('close');
        dlgstate = savestate;

        var all = SW.Orion.NetObjectPicker.GetSelectedEntitiesWithInstanceSiteId() || [], found = {}, removed = [], lng = dlgstate.lng, lat = dlgstate.lat;

        $.each(all, function (i, o) { found['' + o.Uri] = true; });
        $.each(dlgstate.items || [], function (i, o) { if (found[o.Uri]) return; removed.push({ Uri: o.Uri, InstanceSiteId: o.InstanceSiteId }); });
        var locationName = getLocationName();

        var changes = [];
        changes.push({ Latitude: lat, Longitude: lng, LocationName: locationName, Items: [] });
        changes.push({ Latitude: -1000.0, Longitude: -1000.0, LocationName: locationName, Items: [] });

        $.each(all, function (i, o) { changes[0].Items.push({ Uri: o.Uri, InstanceSiteId: o.InstanceSiteId }); });
        $.each(removed, function (i, o) { changes[1].Items.push({ Uri: o.Uri, InstanceSiteId: o.InstanceSiteId }); });

        $.ajax({
            type: 'POST',
            url: '/orion/services/worldmap.aspx?op=set-poi',
            async: false, // on purpose
            dataType: 'json',
            data: JSON.stringify({ changes: changes }),
            success: function () {
                var map = SW.Core.Volatile.WorldMaps['0'];
                map.Refresh({ Longitude: lng, Latitude: lat });
                ui.RefreshCount();
                $('#selectordlg').dialog('close');
            }
        });
    };

    ui.EditSelection = function () {

        $('#selectordlgprimary span.sw-btn-t').text(texts['button.savechanges']);

        var base = SW.Core.WorldMap._debug;
        var state = base.getstate(0);
        var map = state.map;
        var selected = state.selected;

        if (!selected)
            return;

        dlgstate = { lat: selected.swdata.Latitude, lng: selected.swdata.Longitude, items: selected.swdata.Items };

        var items = [];
        $.each(selected.swdata.Items || [], function (i, o) { items.push(o.Uri); });
        SW.Orion.NetObjectPicker.SetUpEntities(supportedEntities, selected.swdata.EntityType, items, 'multiple', false, true);
        SW.Orion.NetObjectPicker.SetText('toomany.title', texts['toomany.title']);
        SW.Orion.NetObjectPicker.SetText('toomany.text', texts['toomany.text']);

        setLocationName(selected.swdata.LocationName);

        var width = 890;

        $('#selectordlg').dialog({
            position: 'center',
            width: width,
            height: 'auto',
            modal: true,
            draggable: true,
            title: texts['dialog.editselection.title'],
            show: { duration: 0 }
        });
    };

    ui.ExpandGroup = function (expandElem) {
        var containerId = $('#' + expandElem.id).attr("containerid");
        var instanceSiteId = $('#' + expandElem.id).attr("instanceSiteId");
        var treeContainer = $(expandElem).parent().parent().children(".nodeLine");

        expandElem.src = '/Orion/images/Button.Collapse.gif';
        $(expandElem).attr("onclick", 'SW.Core.WorldMap.Editor.UI.Collapse(this)');

        $.ajax({
            type: 'POST',
            url: '/orion/services/worldmap.aspx?op=get-group-objects',
            async: false, // on purpose
            dataType: 'json',
            data: JSON.stringify({ containerId: containerId, instanceSiteId: instanceSiteId }),
            success: function (result) {

                for (var i = 0; i < result.data.length; i++) {
                    var isLocalEntity = result.data[i].IsLocalEntity;
                    var linkPrefix = isLocalEntity ? '' : String.format('/Server/{0}', instanceSiteId);
                    var imagePrefix = isLocalEntity ? '' : String.format('/Server/{0}/ImageHandler', instanceSiteId);
                    var subContainerId = result.data[i].NodeId.split('-')[1];
                    var src = imagePrefix + '/Orion/StatusIcon.ashx?size=small&entity=' + result.data[i].Entity + '&id=' + subContainerId + '&status=' + result.data[i].Status;

                    if (result.data[i].Entity == "Orion.Groups") {
                        treeContainer.append("<div class=\"sw-wmap-line\">" +
                            "<a href=\"javascript:void(0);\"> <img src=\"/Orion/images/Button.Expand.gif\" id=\"expImg_" + subContainerId + "_" + instanceSiteId + "\" onclick='return SW.Core.WorldMap.Editor.UI.ExpandGroup(this)' containerId=" + subContainerId + " instanceSiteId=" + instanceSiteId + "> </a>" +
                            "<a href=\"" + linkPrefix + result.data[i].DrillDownArray[0] + "\" onclick='return SW.Core.WorldMap.Editor.UI.ConfirmLeave();'><img src=\"" + src + "\" class=\"StatusIcon\">" + result.data[i].Caption + 
                            "</a><div style=\"padding-left: 34px\" class=\"nodeLine\"></div></div>");
                    }
                    else {
                        treeContainer.append("<div><a href=\"javascript:void(0);\"><img src=" + src + "></a>" + result.data[i].DrillDown + "</div>");
                    }
                }
            }
        });
    };

    ui.Collapse = function (expandElem) {
        $(expandElem).parent().parent().children(".nodeLine").html('');
        expandElem.src = '/Orion/images/Button.Expand.gif';
        $(expandElem).attr("onclick", 'SW.Core.WorldMap.Editor.UI.ExpandGroup(this)');
    };
})();