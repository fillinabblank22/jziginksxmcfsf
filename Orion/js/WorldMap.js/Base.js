﻿(function () {
    //Override '_recursivelyAddChildrenToMap()' for set correct opacity for marks
    L.MarkerCluster = L.MarkerCluster.extend({
        _recursivelyAddChildrenToMap: function (startPos, zoomLevel, bounds) {
            this._recursively(bounds, -1, zoomLevel,
			function (c) {
			    if (zoomLevel === c._zoom) {
			        return;
			    }
			    //Add our child markers at startPos (so they can be animated out)
			    for (var i = c._markers.length - 1; i >= 0; i--) {
			        var nm = c._markers[i];

			        if (!bounds.contains(nm._latlng)) {
			            continue;
			        }
			        if (startPos) {
			            nm._backupLatlng = nm.getLatLng();
			            nm.setLatLng(startPos);
			            if (nm.setOpacity) {
			                nm.setOpacity(1);
			            }
			        }

			        c._group._featureGroup.addLayer(nm);
			    }
			},
			function (c) {
			    c._addToMap(startPos);
			}
		);
        }
    });

    var base = { _states: { nextid: 0 }, _imgroot: '/orion/images/worldmap/', _service: '/orion/services/worldmap.aspx' };
    var editor = { events: { marker: {}} };
    var viewer = { events: { marker: {}} };
    var texts = {
        'tileset.attrib.normal': '@{R=Core.Strings;K=WEBJS_PCC_TILESETATTRIB_NORMAL; E=js}',
        'tileset.attrib.sat': '@{R=Core.Strings;K=WEBJS_PCC_TILESETATTRIB_SAT; E=js}',
        'zoom.in': '@{R=Core.Strings;K=WEBJS_PCC_WM_ZOOMIN; E=js}',
        'zoom.out': '@{R=Core.Strings;K=WEBJS_PCC_WM_ZOOMOUT; E=js}',
        'locationText': '@{R=Core.Strings;K=WEBJS_WM_LOCATIONTEXT; E=js}',
        'defaultLocationName': '@{R=Core.Strings;K=WEBJS_WM_DEFAULTLOCATIONNAME; E=js}'
    };

    var setClusterIcon = function (cluster, mapstate) {
            var statuses = [];
            $.each(cluster.getAllChildMarkers(), function (i, mark) {
                statuses.push(mark.swdata.StatusInfo);
            });
            return (base.geticon('aggregation.' + rollupStatusWorst(statuses).IconPostfix, mapstate) || base.geticon('aggregation.up', mapstate));
    };

    function removeDuplicates(statuses) {
        var duplicates = {};
        var singles = [];
        $.each(statuses, function (i, el) {
            if (!duplicates[el.StatusId]) {
                duplicates[el.StatusId] = true;
                singles.push(el);
            }
        });
        return singles;
    }

    function rollupStatusWorst(statuses) {
        var worstStatus = null;

        $.each(removeDuplicates(statuses), function (i, statusInfo) {
            if (worstStatus == null || worstStatus.Ranking > statusInfo.Ranking) {
                worstStatus = statusInfo;
                // DownStatusId = 2
                if (worstStatus.StatusId == 2) {
                    return;
                }
            }
        });

        return worstStatus;
    }

    var createMarkerClusterGroup = function (mapstate) {
        return L.markerClusterGroup({
            maxClusterRadius: 20,
            iconCreateFunction: function (cluster) {
                return setClusterIcon(cluster, mapstate);
            }
        }).on('clustermouseover', viewer.events.marker.clustermouseover);
    };

    var curry = function (fn, scope) {
        var fnk = fn, s = scope || window, args = [];

        for (var i = 2, len = arguments.length; i < len; ++i) { args.push(arguments[i]); };
        return function () {
            var a2 = args;
            if (arguments.length > 0)
                a2 = args.concat(Array.prototype.slice.call(arguments, 0));
            return fnk.apply(s, a2);
        };
    };

    var trunc = function (n) {
        // we're going to chop to 13 digits.
        var t = '' + n;
        var dot = t.indexOf('.');
        if (dot < 0) return n;
        var digits = t.length - dot - 1;
        if (digits < 13) return n;
        t = t.substring(0, dot + 13);
        return parseFloat(t);
    };

    var samecoords = function (a, b) { return a.Longitude == b.Longitude && a.Latitude == b.Latitude; };

    // -- normalize

    var normalize_lng = function (lng) {
        if (lng < -180) {
            var o = lng + 180;
            var a = Math.abs(o) % 360;
            if (!a) return -180;
            if (a < 180) return 180 - a;
            return 0 - (a % 180);
        }
        if (lng > 180) {
            var o = lng - 180;
            var a = o % 360;
            if (!a) return 180;
            if (a > 180) return a % 180;
            return a - 180;
        }
        return lng;
    };

    var denormalize_lng = function (lng, left, right) {
        var mid = left + (right - left) / 2, nmid = normalize_lng(mid), dist = lng - nmid, ret = mid + dist;

        if (ret > left) {
            if (ret < right) return ret;
            return ret - 360;
        } else {
            return ret + 360;
        }
    };

    // -- state

    var createstate = function () {

        var ret = { id: base._states.nextid++, callbacks: {} };

        ret.on = function (name, fn) { ret.callbacks[name] = fn; };

        ret.trigger = function (name) {
            var cb, args = [];
            if (!(cb = ret.callbacks[name])) return;
            for (var i = 1, len = arguments.length; i < len; ++i) { args.push(arguments[i]); };
            return cb.apply(null, args);
        };

        return ret;
    };

    // --- base

    base.initicons = function () {

        if (base.icons)
            return;

        var iconType = {
            'sing': 'single',
            'mult': 'multiple',
            'aggr': 'aggregation'
        };

        var mkicon = function (icoType, txt, offset) {
            return L.icon({
                iconUrl: base._imgroot + 'marker-' + icoType + '-' + txt + '.png',
                iconRetinaUrl: base._imgroot + 'marker-' + icoType + '-' + txt + '-2x.png',
                shadowUrl: base._imgroot + 'marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41],
                zIndexOffset: offset || 0
            });
        };

        base.icons = {
            'single.up': mkicon(iconType.sing, 'up', 0),
            'single.warning': mkicon(iconType.sing, 'warning', 200),
            'single.down': mkicon(iconType.sing, 'down', 300),
            'single.unknown': mkicon(iconType.sing, 'unknown', 100),
            'single.selected': mkicon(iconType.sing, 'selected', 0),
            'single.editor': mkicon(iconType.sing, 'editmode', 0),

            'multiple.up': mkicon(iconType.mult, 'up', 0),
            'multiple.warning': mkicon(iconType.mult, 'warning', 200),
            'multiple.down': mkicon(iconType.mult, 'down', 300),
            'multiple.unknown': mkicon(iconType.mult, 'unknown', 100),
            'multiple.selected': mkicon(iconType.mult, 'selected', 0),
            'multiple.editor': mkicon(iconType.mult, 'editmode', 0),

            'aggregation.up': mkicon(iconType.aggr, 'up', 0),
            'aggregation.warning': mkicon(iconType.aggr, 'warning', 200),
            'aggregation.down': mkicon(iconType.aggr, 'down', 300),
            'aggregation.unknown': mkicon(iconType.aggr, 'unknown', 100),
            'aggregation.selected': mkicon(iconType.aggr, 'selected', 0),
            'aggregation.editor': mkicon(iconType.aggr, 'editmode', 0)
        };
    };

    base.initstatuses = function () {
        var tmp;

        $.ajax({
            type: 'POST',
            url: base._service + '?op=get-statuses-info',
            dataType: 'json',
            success: function (data) {
                tmp = data;
            },
            error: function () {
            }
        });
    };

    base.getstate = function (mapid) { return base._states[mapid]; };
    base.newstate = function () { var mapstate = createstate(); base._states[mapstate.id] = mapstate; return mapstate; };
    base.geticon = function (id, mapstate) { if (mapstate && mapstate.config.nostatusicons) id = id.replace(/[.](up|warning|down|unknown)/i, '.editor'); return base.icons[(id || '').toLowerCase()]; };
    base.getlocation = function (mapstate) { return { Center: mapstate.map.getCenter(), Zoom: mapstate.map.getZoom(), Bounds: mapstate.map.getBounds() }; };
    base.getpoi = function (mapstate, lat, lng) {
        var data = mapstate.data, coords = { Latitude: lat, Longitude: lng };
        for (var i = 0, imax = data.length; i < imax; ++i) {
            var o = data[i];
            if (samecoords(o, coords)) return o;
        }
        return null;
    };

    base.reflow = function (mapstate) {
        var bounds = mapstate.map.getBounds(), left = bounds.getSouthWest().lng, right = bounds.getNorthEast().lng;
        var layer = mapstate.icolayer;
        var mods = [];
        var nonmods = [];
        var markerCluster;

        layer.eachLayer(function (o) {
            var ll = o.getLatLng();
            if (bounds.contains(ll)) {
                nonmods.push(o);
                return;
            }
            var ll2 = new L.LatLng(ll.lat, denormalize_lng(o.swdata.Longitude, left, right));
            if (bounds.contains(ll2) == false) return;

            o.setLatLng(ll2);
            mods.push(o);
        });

        if (mods.length > 0) {
            $.each(mods, function (i, o) { o.update(); });

            //Normalize placement for cluster marks on map after  map scaling and moving 
            markerCluster = createMarkerClusterGroup(mapstate);
            markerCluster.addLayers(mods);
            markerCluster.addLayers(nonmods);

            mapstate.icolayer.clearLayers();
            mapstate.icolayer = markerCluster;
            mapstate.icolayer.addTo(mapstate.map);
        }
    };

    base.loadmap = function (mapstate, data) {

        var markevts = (mapstate.editmode ? editor : viewer).events.marker;
        var selectcoords = mapstate.selected && mapstate.selected.swdata;
        var found = selectcoords ? null : {};
        var quietselect = !found && !mapstate.selected.selectonload;
        var config = mapstate.config;
        var bounds = mapstate.map.getBounds(), left = bounds.getSouthWest().lng, right = bounds.getNorthEast().lng;

        mapstate.data = data || [];

        var markerCluster = createMarkerClusterGroup(mapstate);

        $.each(mapstate.data, function (i, v) {

            v.iconid = v.IconClass.toLowerCase();
            var ico;
            if (base.geticon(v.iconid, mapstate)) {
                ico = base.geticon(v.iconid, mapstate);
            } else {
                ico = /^single/i.test(v.iconid) ? base.geticon('single.unknown', mapstate) : base.geticon('multiple.unknown', mapstate);
            }
            
            var mark = L.marker([v.Latitude, denormalize_lng(v.Longitude, left, right)], {
                draggable: config.dragging,
                riseOnHover: true,
                icon: ico,
                zIndexOffset: ico.options.zIndexOffset
            });
            markerCluster.addLayer(mark);
            mark.swdata = v;

            if (markevts.click) mark.on('click', markevts.click);

            if (config.dragging) {
                if (markevts.dragstart) mark.on('dragstart', markevts.dragstart);
                if (markevts.dragend) mark.on('dragend', markevts.dragend);
            }

            if (config.hover)
                mark.on('mouseover', viewer.events.marker.mouseover);

            found = found || (samecoords(selectcoords, v) ? mark : null);
        });

        if (mapstate.icolayer) {
            mapstate.icolayer.clearLayers();
            mapstate.map.removeLayer(mapstate.icolayer);
        }

        mapstate.icolayer = markerCluster;
        mapstate.icolayer.addTo(mapstate.map);

        if (found && found.swdata) {
            editor.select(found, false, quietselect);
            mapstate.trigger('refresh.found', mapstate, found);
        }
    };

    base.tilesets = {};

    var httpsRevise = function (s) {
        if (location.protocol.toLowerCase() != 'https:') return s;
        if (/^http:\/\/\{s\}\.mqcdn\.com/i.test(s)) s = s.replace(/\.mqcdn\.com/i, "-s.mqcdn.com");
        s = s.replace(/^http:/i, 'https:');
        return s;
    };

    base.tilesets.normal = function () {
        return [MQ.mapLayer()];
    };

    base.tilesets.satclean = function () {
        return [MQ.satelliteLayer()];
    };

    base.tilesets.sathyb = function () {
        return [MQ.hybridLayer()];
    };

    base.addtiles = function (map, tileset) {
        var tilesetfn = base.tilesets[tileset] || base.tilesets['normal'];
        var layers = tilesetfn();
        for (var i = 0, imax = layers.length; i < imax; ++i) layers[i].addTo(map);
    };

    base.initmap = function (domid, config, data) {
        var lat = config.lat, lng = config.lng, zoom = config.zoom, tileset = config.tileset || 'normal', editmode = !!config.editmode;

        config.dragging = typeof (config.dragging) != 'undefined' ? config.dragging : editmode;
        config.hover = typeof (config.hover) != 'undefined' ? config.hover : !editmode;

        base.initicons();
        base.initstatuses();

        var opts = editmode ? { trackResize: true} : {};

        var map = L.map(domid, opts);
        var state = base.newstate();
        var mapid = map.mapid = state.id;

        if (lat == "0" && lng == "0") map.fitWorld();
        else map.setView([lat, lng], zoom);

        state.config = config;
        state.data = data;
        state.editmode = editmode;
        state.selected = null;
        state.map = map;

        map.attributionControl.options.prefix = null;

        $('.leaflet-control-zoom-in').attr('title', texts['zoom.in']);
        $('.leaflet-control-zoom-out').attr('title', texts['zoom.out']);
        if (domid != 'mapselector') {
            $('<div class="leaflet-control-zoom leaflet-bar leaflet-control"><a href="#" class="reset-location-zoom leaflet-bar-part"></a></div>').insertAfter($(String.format('#{0} .leaflet-control-zoom-out', domid)).parent());

            $('.reset-location-zoom').click(function () {
                if (lat == "0" && lng == "0") map.fitWorld();
                else map.setView([lat, lng], zoom);
                return false;
            });
        }

        base.addtiles(map, tileset);

        var codesource = (editmode ? editor : viewer);
        var fnKeyup = codesource.events.keyup;

        if (fnKeyup) $(document).keyup(function (e) { return fnKeyup(e, mapid); });
        if (codesource.events.click) map.on('click', codesource.events.click);
        map.on('moveend', function () { base.reflow(state); });

        base.loadmap(state, data);

        return {
            GetPOI: curry(base.getpoi, state, state),
            Refresh: curry(codesource.refresh, state, mapid),
            SetAddMode: curry(codesource.addmode, state, mapid),
            Location: function () { return base.getlocation(state); },
            SetPOI: curry(editor.selecticon, state, state),
            on: curry(state.on, state),
            trigger: curry(state.trigger, state),
            leaflet: function () { return state.map; },
            selected: function () { return state.selected ? state.selected.swdata : null; }
        };
    };

    // --- editor

    editor.create = function (domid, config, data) {
        return base.initmap(domid, config, data);
    };

    editor.events.click = function (e) {
        var t = e.target;
        var mapstate = base.getstate(t.mapid);

        if (mapstate.addmode)
            mapstate.trigger('location.add', mapstate, e.latlng.lat, e.latlng.lng);
    };

    editor.events.keyup = function (e, mapid) {
        if (e.keyCode == 27) {
            var mapstate = base.getstate(mapid);
            editor.unselect(mapstate);
            editor.addmode(mapid, false);
        }
    };

    editor.unselect = function (mapstate, quiet) {

        if (mapstate.selected != null) {

            if (mapstate.selected.layer != null) {
                var resetid = mapstate.selected.swdata.iconid;
                mapstate.selected.layer.setIcon(base.geticon(resetid, mapstate) || base.geticon('single.unknown', mapstate));
                mapstate.selected = null;
            }

            if (!quiet) // drag is using this to properly deselect, avoiding a double bounce.
                mapstate.trigger('selected', mapstate, null);

            return;
        }
    };

    editor.select = function (t, toggle, quiet) {

        var mapstate = base.getstate(t._map.mapid);

        if (mapstate.selected != null && mapstate.selected.layer) {

            var repeat = samecoords(mapstate.selected.swdata, t.swdata);

            if (repeat && !toggle)
                return;

            editor.unselect(mapstate, !repeat);

            if (repeat)
                return;
        }

        mapstate.selected = { swdata: t.swdata, layer: t };

        var icon = /^single/i.test(t.swdata.iconid) ? base.geticon('single.selected', mapstate) : base.geticon('multiple.selected', mapstate);
        t.setIcon(icon);

        if (!quiet)
            mapstate.trigger('selected', mapstate, t.swdata);
    };

    editor.addmode = function (mapid, enter) {
        var mapstate = base.getstate(mapid);

        if (enter) {

            $('body').addClass('sw-map-pointermode');
            mapstate.addmode = true;
            mapstate.map.dragging.disable();
            mapstate.trigger('addmode.enter', mapstate);

        } else {

            if (!mapstate.addmode)
                return;

            $('body').removeClass('sw-map-pointermode');
            mapstate.addmode = false;
            mapstate.map.dragging.enable();
            mapstate.trigger('addmode.leave', null);
        }
    };

    editor.events.marker.click = function (e) {

        var t = e.target;
        var mapstate = base.getstate(t._map.mapid);

        editor.select(t, true);

        if (mapstate.addmode)
            mapstate.trigger('location.add', mapstate, t.swdata.Latitude, t.swdata.Longitude);
    };

    editor.events.marker.dragstart = function (e) {
        // sadly, set-icon on the marker will torch the drag doing this outside the api.

        var t = e.target;
        var mapstate = base.getstate(t._map.mapid);
        mapstate.indrag = true;
        mapstate.inhover = false;

        var issame = mapstate.selected && samecoords(e.target.swdata, mapstate.selected.swdata);
        var noreselect = issame;

        if (mapstate.selected && !issame)
            editor.unselect(mapstate, true);

        var icon = /^single/i.test(t.swdata.iconid) ? base.geticon('single.selected', mapstate) : base.geticon('multiple.selected', mapstate);
        t._icon.src = icon.options.iconUrl;

        if (mapstate.config.hover && SW.Core.NetObjectTips.HasHover(t._icon)) {
            mapstate.inhover = true;
            SW.Core.NetObjectTips.CancelHover(t._icon);
        }

        mapstate.selected = { swdata: t.swdata, layer: t };
        if (!noreselect) mapstate.trigger('selected', mapstate, t.swdata);

        mapstate.trigger('drag.start', t.swdata);
    };

    editor.events.marker.dragend = function (e) {
        var t = e.target;

        var coords = t.getLatLng();
        var oldLat = t.swdata.Latitude;
        var oldLng = t.swdata.Longitude;

        t.swdata.Longitude = trunc(coords.lng);
        t.swdata.Latitude = trunc(coords.lat);

        var revert = function () {
            t.setLatLng({ lat: oldLat, lng: oldLng });
            t.swdata.Latitude = oldLat;
            t.swdata.Longitude = oldLng;
        };

        var mapstate = base.getstate(t._map.mapid);
        mapstate.indrag = false;

        if (mapstate.inhover) {
            mapstate.inhover = false;
            viewer.events.marker.mouseover({ target: t });
        }

        var updateBaseValues = function () {
            $.ajax({
                type: 'POST',
                url: base._service + '?op=set-poi',
                dataType: 'json',
                data: JSON.stringify({
                    changes: [{
                        Longitude: t.swdata.Longitude,
                        Latitude: t.swdata.Latitude,
                        Items: t.swdata.Items,
                        LocationName: t.swdata.LocationName
                    }]
                }),
                success: function (data) {
                    if (!data || !data.success)
                        revert();
                    mapstate.trigger('drag.finish', t.swdata);
                },
                error: function () {
                    revert();
                    mapstate.trigger('drag.error', t.swdata);
                }
            });

            editor.select(t, false);
        };

        if (t.swdata.AutoAdded) {
            Ext.MessageBox.show({
                title: '@{R=Core.Strings;K=WEBJS_SO0_96; E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_SO0_97; E=js}',
                buttons: { yes: '@{R=Core.Strings;K=WEBJS_SO0_98; E=js}', no: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                width: 550,
                fn: function(btn) {
                    if (btn == 'yes') {
                        t.swdata.AutoAdded = false;
                        updateBaseValues();
                    } else {
                        revert();
                        editor.select(t, false);
                        return;
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        } else
            updateBaseValues();
    };

    editor.refresh = function (mapid, swdata) {
        var mapstate = base.getstate(mapid);

        return viewer.refresh(mapid, swdata);
    };

    editor.selecticon = function (mapstate, lat, lng, zoom) {
        if (typeof (lat) == 'undefined' || lat === null) {
            editor.unselect(mapstate);
            return;
        }

        var coords = { lat: lat, lng: lng, zoom: zoom };
        var found;

        mapstate.icolayer.eachLayer(function (o) {
            if (!o || !o.swdata || !samecoords(o.swdata, coords)) return true;
            if (!found) found = o;
            return false;
        });

        if (found) {
            editor.select(found);
        }
    };

    // --- viewer

    viewer.create = function (domid, config, data) {
        return base.initmap(domid, config, data);
    };

    viewer.events.marker.click = function (e) {
        var t = e.target;
        if (!t || !t.swdata) return;
        if (/\/worldmap\/default\.aspx/i.test(t.swdata.DrillDown)) {

            var mapstate = base.getstate(t._map.mapid);
            var zoom = mapstate.map.getZoom();
            var loc = t.swdata.DrillDown;
            if (!/[&]$/.test(loc)) loc += "&";
            loc += "Zoom=" + zoom;

            window.location = loc;

        } else {
            window.location = t.swdata.DrillDown;
        }
    };

    viewer.events.marker.mouseover = function (e) {
        var t = e.target;

        if (!t || !t.swdata || SW.Core.NetObjectTips.HasHover(t._icon)) return;

        var mapstate = base.getstate(t._map.mapid);

        if (mapstate.indrag) return;

        var opts = {};
        if (t.swdata.Items.length > 1) {
            var p = [];
            p.push('?ResourceId=' + (mapstate.config.resourceid || ''));
            p.push('&Latitude=' + t.swdata.Latitude);
            p.push('&Longitude=' + t.swdata.Longitude);
            if (mapstate.config.undomode) p.push('&Undo=true');
            p.push('&');
            opts.url = '/orion/worldmap/GroupLocationPopup.aspx' + p.join('');
        }
        else {
            opts.netobject = t.swdata.DrillDown;
            opts.locationData = {
                Name: t.swdata.LocationName ? t.swdata.LocationName : texts['defaultLocationName'],
                Text: texts['locationText']
            };
        }

        SW.Core.NetObjectTips.ForceHover(t._icon, opts);
    };

    viewer.events.marker.clustermouseover = function (e) {
        var clusterBounds = e.layer._bounds;
        var t = e.target;

        if (!t || SW.Core.NetObjectTips.HasHover(e.layer._icon)) return;
        var mapstate = base.getstate(t._map.mapid);

        if (mapstate.indrag) return;

        var opts = {};

        if (clusterBounds) {
            var p = [];
            p.push('?ResourceId=' + (mapstate.config.resourceid || ''));
            p.push('&LatitudeNE=' + clusterBounds._northEast.lat);
            p.push('&LongitudeNE=' + clusterBounds._northEast.lng);
            p.push('&LatitudeSW=' + clusterBounds._southWest.lat);
            p.push('&LongitudeSW=' + clusterBounds._southWest.lng);
            if (mapstate.config.undomode) p.push('&Undo=true');
            p.push('&');
            opts.url = '/orion/worldmap/ClusterLocationPopup.aspx' + p.join('');
            SW.Core.NetObjectTips.ForceHover(e.layer._icon, opts);
        }
    };

    viewer.refresh = function (mapid, swdata, data) {
        var mapstate = base.getstate(mapid);

        if (mapstate.config.editmode) {
            if (swdata) {
                editor.unselect(mapstate); // force a non-selection.
                mapstate.selected = { swdata: swdata, layer: null, selectonload: true };
            } else if (mapstate.selected) {
                mapstate.selected = { swdata: mapstate.selected.swdata, layer: null, selectonload: false };
            }
        }

        if (data) {
            base.loadmap(mapstate, data);
            return;
        }

        mapstate.trigger('refresh.start', null);

        var latlng = mapstate.map.getCenter(),
            zoom = mapstate.map.getZoom(),
            pxsize = mapstate.map.getSize(),
            id = mapid,
            p = [];

        p.push("?op=get-poi");
        p.push("&longitude=" + latlng.lng);
        p.push("&latitude=" + latlng.lat);
        p.push("&width=" + pxsize.x);
        p.push("&height=" + pxsize.y);
        p.push("&zoom=" + zoom);

        if (mapstate.config.undomode)
            p.push("&undo=true");

        if (mapstate.config.optimize)
            p.push("&optimize=true");

        if (mapstate.config.resourceid)
            p.push("&resourceid=" + mapstate.config.resourceid);

        $.ajax({
            type: 'GET',
            url: base._service + p.join(''),
            dataType: 'json',
            success: function (result) {
                if (result && result.success && result.data)
                    viewer.refresh(id, null, result.data.WorldPOI);
                if (result.data.PartialErrors != null) {
                    $("#worldmappartialerrors").html(result.data.PartialErrors);
                }
                mapstate.trigger('refresh.finish');
            },
            error: function () {
                mapstate.trigger('refresh.error');
            }
        });
    };

    //

    var mapNS = SW.Core.namespace("SW.Core.WorldMap");

    mapNS.Viewer = { Create: viewer.create, NormalizeLng: normalize_lng, DeNormalizeLng: denormalize_lng };
    mapNS.Editor = { Create: editor.create };

    // debug

    mapNS.Viewer._debug = viewer;
    mapNS.Editor._debug = editor;
    mapNS._debug = base;

})();
