﻿SW.Core.namespace("SW.Core").UsernamePasswordCredentialsControlSettings = function () {
    var testCredentialsHandler;
    var userNameValidator;
    var controlId;
    var testInProgress;
    var lastValidationResult;
    var validationResultStateEnum = {
        Ok: 0,
        Warning: 1,
        Error: 2
    };
    /**
    * @typedef UsernamePasswordCredentialsControlSettings~ValidationResult
    * @type {object}
    * @property {boolean} isValid - true if value is correct; otherwise false.
    * @property {string} errorMessage - message which describe validation error.
    */

    /**
     * Default handler for user name validation
     * @param {string} userName
     * @return {UsernamePasswordCredentialsControlSettings~ValidationResult}
     */
    function userNameDefaultValidator(userName) {
        var regex = /^[^\/:*?"<>|]+$/;

        userName = $.trim(userName);

        var validationResult = true;
        var errorMessage = undefined;

        if (userName.length === 0) {
            validationResult = false;
            errorMessage = "@{R=Core.Strings.3; K=WEBJS_PS0_1; E=js}";
        } else if (!regex.test(userName)) {
            validationResult = false;
            errorMessage = "@{R=Core.Strings.3; K=WEBJS_PS0_2; E=js}";
        }

        return { isValid: validationResult, errorMessage: errorMessage };
    }

    /**
      * Validate user entered username and display error status if any was found.
      * @param {string} value 
      * @param {jQuery} inputElement 
      * @param {jQuery} errorElement 
      * @returns {undefined} 
      */
    function validateUserName(value, inputElement, errorElement) {
        var validationResult = userNameValidator(value);
        setValidationForInput(inputElement, errorElement, validationResult);
        return validationResult;
    }

    /**
     * Default handler for password validation - do not allow empty password
     * @param {string} password
     * @return {UsernamePasswordCredentialsControlSettings~ValidationResult} - result of validation
     */
    function passwordDefaultValidator(password) {
        var validationResult = true;
        var errorMessage = undefined;
        if (password.length === 0) {
            validationResult = false;
            errorMessage = "@{R=Core.Strings.3; K=WEBJS_PS0_3; E=js}";
        }
        return { isValid: validationResult, errorMessage: errorMessage };
    }

    /**
      * Valide user entered password and display error status if any was found.
      * @param {string} value 
      * @param {jQuery} inputElement 
      * @param {jQuery} errorElement 
      * @returns {undefined} 
      */
    function validateUserPassword(value, inputElement, errorElement) {
        var validationResult = passwordDefaultValidator(value);
        setValidationForInput(inputElement, errorElement, validationResult);
        return validationResult;
    }

    function setValidationForInput(inputElement, errorElement, validationResult) {
        if (validationResult.isValid) {
            inputElement.removeClass("invalidValue");
            errorElement.text("");
        } else {
            inputElement.addClass("invalidValue");
            errorElement.text(validationResult.errorMessage);
        }
    }

    /**
     * Will return reference to element whose attribut data-form has value reprezented by @param {dataKey}.
     * @param {string} dataKey
     * @return {jQuery}
     */
    function getDataFormElement(dataKey) {
        return $("#usernamePasswordCredentials-" + controlId + " [data-form='" + dataKey + "']");
    }

    /**
      * Will return reference to element whose id has value reprezented by @param {basicId}
      * @param {string} basicId - uniqueidentifier of DOM html element
      * @returns {jQuery} 
      */
    function getHtmlElement(basicId) {
        return $("#" + basicId + "-" + controlId);
    }

    /**
     * @typedef UsernamePasswordCredentialsControlSettings~CredentialValidationResult
     * @type {object}
     * @property {boolean} IsValid - true if credentials are valid; otherwise false.
     * @property {string} ErrorMessage - Message which is typically filed when property IsValid is false and describe reason, why validation wasn't successfull.
     * @property {string} DebugDetails - Filled when property IsValid is false. Typically serialized callstack.
     */



    /**
     * @typedef UsernamePasswordCredentialsControlSettings~Credentials
     * @type {object}
     * @property {string} Username - name of user
     * @property {string} Password - password
     * @property {number} CredentialsId - uniqueidentifier of Orion.Credentials entity
     * @property {string} Usage - usage of credentials (e.g. CiscoApic)
     */

    /**
     * @function UsernamePasswordCredentialsControlSettings~CredentialValidationCallback
     * @param {UsernamePasswordCredentialsControlSettings~CredentialValidationResult} validationResult - information about result of credential validation
     * @return undefined
     */

    /**
     * This credential validation function will be called everytime, when will not be redefined by calling SetTestCredentialsHandler method.
     * @param {UsernamePasswordCredentialsControlSettings~Credentials} credentials - by user entered credentials to validate.
     * @param {UsernamePasswordCredentialsControlSettings~CredentialValidationCallback} callback - function which is called after validation of test credential is finished.
     * @return undefined
     */
    function defaultTestCredentialsHandler(credentials, callback) {
        var validationResult = {
            IsValid: false,
            ErrorMessage: String.format("@{R=Core.Strings.3; K=WEBJS_PS0_4; E=js}", controlId),
            DebugDetails: String.format("@{R=Core.Strings.3; K=WEBJS_PS0_5; E=js}", controlId, "function(credentials, callback) { <Your code that invokes callback(validationResult) at the end>; }")
        };

        callback(validationResult);
    }

    /**
     * Invoked from backend to re-bind validation and restore visual state after control postback is finished.
     * @return undefined
     */
    this.OnPostBackFinished = function () {
        this.bindValidation();
        // Restore visual state
        if (lastValidationResult) {
            setValidationMessageBox(lastValidationResult, getHtmlElement("validationMessageBox"));
        } else if (testInProgress) {
            getHtmlElement("testProgress").show();
        } else if (this.userNameOrPasswordHasValidationError) {
            this.ValidateCredentials();
        }
    }

    /**
     * Will attach to username and password text control validation hooks which will during editing then display validation errors (such as empty field, invalid characters etc.).
     * @return undefined
     */
    this.bindValidation = function () {
        var userNameValidateCallback = $.proxy(function () {
            setTimeout(function () {
                validateUserName(getDataFormElement("userName").val(),
                    getDataFormElement("userName"),
                    getHtmlElement("userNameError"));
            },
                800);
        },
            this);
        getDataFormElement("userName").on("change paste keyup", userNameValidateCallback);

        var passwordValidateCallback = $.proxy(function () {
            setTimeout(function () {
                validateUserPassword(getDataFormElement("password").val(),
                    getDataFormElement("password"),
                    getHtmlElement("passwordError"));
            },
                800);
        },
            this);
        getDataFormElement("password").on("change paste keyup", passwordValidateCallback);
    };

    /**
      * This validation is invoked when user will try to submit form, when control has setup property PreventSubmitWhenValidationFails to true.
      * @param {AspNetClientControlReference} sender 
      * @param {AspNetClientValidateEventArgs} args 
      * @returns {undefined} 
      */
    this.CustomValidationNetValidateUserName = function (sender, args) {
        var valResult = validateUserName(getDataFormElement("userName").val(), getDataFormElement("userName"), getHtmlElement("userNameError"));
        args.IsValid = valResult.isValid;
    };

    /**
      * This validation is invoked when user will try to submit form, when control has setup property PreventSubmitWhenValidationFails to true.
      * @param {AspNetClientControlReference} sender 
      * @param {AspNetClientValidateEventArgs} args 
      * @returns {undefined} 
      */
    this.CustomValidationNetValidatePassword = function (sender, args) {
        var valResult = validateUserPassword(getDataFormElement("password").val(), getDataFormElement("password"), getHtmlElement("passwordError"));
        args.IsValid = valResult.isValid;
    };

    /**
     * Initialization of this component
     * @param {string} uniqueControlId - uniqueidentifier of component
     * @return undefined
     */
    this.init = function (uniqueControlId) {
        controlId = uniqueControlId;

        userNameValidator = userNameDefaultValidator;
        passwordValidator = passwordDefaultValidator;

        testCredentialsHandler = defaultTestCredentialsHandler;

        this.bindValidation();
    };

    /**
     * @function UsernamePasswordCredentialsControlSettings~TestCredentialHandler
     * @param {UsernamePasswordCredentialsControlSettings~Credentials} credentials - Credentials to validate
     * @param {UsernamePasswordCredentialsControlSettings~CredentialValidationCallback} callback - function which is invoked when validation finished and into which is passed validation result.
     * @return undefined
     */

    /**
     * Set javascript function which will perform validation of username/password credentials
     * @param {UsernamePasswordCredentialsControlSettings~TestCredentialHandler} handler - Javascript function which will take care about correct validation of credentials.
     * @return undefined
     */
    this.SetTestCredentialsHandler = function (handler) {
        testCredentialsHandler = handler;
    };

    /**
     * @function {UsernamePasswordCredentialsControlSettings~UsernameValidator}
     * @param {string} username - name of user to validate
     * @return {UsernamePasswordCredentialsControlSettings~ValidationResult} - result of validation
     */

    /**
     * Set javascript function which will perform validation of username field. Very often validation if is not empty or doesn't contain invalid characters.
     * @param {UsernamePasswordCredentialsControlSettings~UsernameValidator} validator
     * @return undefined
     */
    this.SetUserNameValidator = function (validator) {
        userNameValidator = validator;
    }

    /**
     * Set javascript function which will perform validation of password field.
     * @param {UsernamePasswordCredentialsControlSettings~UsernameValidator} validator
     * @return undefined
     */
    this.SetPasswordValidator = function (validator) {
        passwordValidator = validator;
    }

    /**
     * Method will validate format of entered credentials.
     * @return {UsernamePasswordCredentialsControlSettings~ValidationResult} - result of validation
     */
    this.ValidateCredentials = function () {
        var credentials = this.getCredentials();

        var userNameValid = validateUserName(credentials.Username, getDataFormElement("userName"), getHtmlElement("userNameError"));
        var passwordValid = validateUserPassword(credentials.Password, getDataFormElement("password"), getHtmlElement("passwordError"));

        var bothValid = userNameValid.isValid && passwordValid.isValid;
        this.userNameOrPasswordHasValidationError = !bothValid;
        return bothValid;
    };

    /**
     * Method will construct credential object form available fields (username, password etc.).
     * @return {UsernamePasswordCredentialsControlSettings~Credentials} - credentials for validation
     */
    this.getCredentials = function () {
        var credentials = {
            Username: getDataFormElement("userName").val(),
            Password: getDataFormElement("password").val(),
            CredentialsId: getDataFormElement("credentialsId").val(),
            Usage: getDataFormElement("credentialsRelationUse").val()
        };
        return credentials;
    };

    /**
     * Method will do full credential validation and display in UI result of validation (success or failure).
     * @return undefined
     */
    this.testCredentials = function () {
        getHtmlElement("testProgress").hide();
        var messageBoxElement = getHtmlElement("validationMessageBox");
        messageBoxElement.hide();

        var credentials = this.getCredentials();

        if (this.ValidateCredentials()) {
            getHtmlElement("testProgress").show();
            testInProgress = true;
            lastValidationResult = undefined;

            testCredentialsHandler(credentials, function (validationResult) {
                lastValidationResult = validationResult;
                testInProgress = false;
                getHtmlElement("testProgress").hide();
                if (validationResult.Message) {
                    setValidationMessageBox(validationResult, messageBoxElement);
                }
                if (validationResult.CustomData) {
                    $("[id$=hfCustomData]").val(validationResult.CustomData);
                }
            });
        }
    };

    /**
    * @param {UsernamePasswordCredentialsControlSettings~CredentialValidationResult} validationResult
    * @param {jQuery} messageBoxElement
    * @return undefined
    */
    function setValidationMessageBox(validationResult, messageBoxElement) {
        initValidationMessageBox(validationResult, messageBoxElement);
        initValidationMessageBoxButton(validationResult, messageBoxElement);
    }

    /**
    * @param {UsernamePasswordCredentialsControlSettings~CredentialValidationResult} validationResult
    * @param {jQuery} messageBoxElement
    * @return undefined
    */
    function initValidationMessageBox(validationResult, messageBoxElement) {
        messageBoxElement.show();
        messageBoxElement.removeClass();
        getHtmlElement("validationMessageBoxText").text(validationResult.Message);
        if (validationResult.ValidationResultState === validationResultStateEnum.Ok) {
            messageBoxElement.addClass("sw-suggestion sw-suggestion-pass");
        } else if (validationResult.ValidationResultState === validationResultStateEnum.Warning) {
            messageBoxElement.addClass("sw-suggestion sw-suggestion-warning");
        } else if (validationResult.ValidationResultState === validationResultStateEnum.Error) {
            messageBoxElement.addClass("sw-suggestion sw-suggestion-fail");
        }
    }

    /**
    * @param {UsernamePasswordCredentialsControlSettings~CredentialValidationResult} validationResult
    * @param {jQuery} messageBoxElement
    * @return undefined
    */
    function initValidationMessageBoxButton(validationResult, messageBoxElement) {

        if (validationResult.MessageButtonAction) {
            $("[id$=buttonActionKey]", messageBoxElement).val(validationResult.MessageButtonAction);
            $("[id$=validationMessageBoxButton]", messageBoxElement).text(validationResult.MessageButtonText);
            $("[id$=validationMessageBoxButton]", messageBoxElement).show();
            $("[id$=validationMessageBoxButton]", messageBoxElement).on("click",
                function () {
                    lastValidationResult = undefined;
                    messageBoxElement.hide();
                });
        } else {
            $("[id$=validationMessageBoxButton]", messageBoxElement).hide();
        }
    }
};