﻿(function (Core) {
    var ResourceContainer = Core.ResourceContainer = Core.ResourceContainer || {};
    var MIN_COLUMN_WIDTH = 200;
    var VIEW_COLUMN_PADDINGS = 10;

    var slideShowPaused = false;
    var viewsColumnInfo = {};
    var isDemoMode = false;
    var isNocView = false;
    var isReadOnlyMode = false;
    var nocRotationInterval = 0;

    var mouseY = 0;
    var isResizing = false;
    var toolTipTop = 0;

    var refreshFirstSlide = function (interval) {
        if (slideShowPaused) return;
        SW.Core.View.RefreshNocSlide($("table.ResourceContainer")[0]);
        if (interval > 0) setTimeout(function () { refreshFirstSlide(interval); }, interval);
    };

    var originResourceColumn,
        originResourceColumnId,
        originResourcePosition,
        origintResourceViewID;

    var fixResizableHandle = function () {
        $(this).find("div.ui-resizable-handle.ui-resizable-e").css({
            'z-index': 999
        });
        $(".ResourceColumn").hover(function () {
            if ($("div.ui-resizable-handle.ui-resizable-e").height() !== $(this).height())
                $("div.ui-resizable-handle.ui-resizable-e").css("height", $(this).height());
        });
    };

    var showResizeTooltip = function (resizeInfo) {
        var resizeTooltipId = "resizeHandleTooltip";
        var resizeTooltip = $("body").find("#" + resizeTooltipId);
        if (resizeTooltip.length !== 0) {
            resizeTooltip.remove();
        }

        var displayXValue = Math.floor(resizeInfo.relativeLeft + resizeInfo.width);
        var tooltipHtml = "<div id='" + resizeTooltipId + "' class='resizeable-tooltip-container'>";
        tooltipHtml += "<div class='resizeable-tooltip-line'>@{R=Core.Strings;K=Drag_And_Drop_Resize_Handle_Tooltip_XSize; E=js}:&nbsp;";
        tooltipHtml += "<div class='resizeable-tooltip-emphasis'>" + displayXValue + "</div>";
        tooltipHtml += " px</div>";
        tooltipHtml += "<div class='resizeable-tooltip-line'>@{R=Core.Strings;K=Drag_And_Drop_Resize_Handle_Tooltip_ColWidth; E=js}:&nbsp;";
        tooltipHtml += "<div class='resizeable-tooltip-emphasis'>" + resizeInfo.width + "</div>";
        tooltipHtml += " px</div>";
        tooltipHtml += "</div>";
        $("body").append(tooltipHtml);

        resizeTooltip = $("body").find("#" + resizeTooltipId);
        var margin = 15;
        var xPos = Math.floor(resizeInfo.left + resizeInfo.width);
        var toolTipLeft = xPos + margin - $(window).scrollLeft();
        var toolTipRight = toolTipLeft + resizeTooltip.width() + margin;
        // adjust for left bounds
        if (toolTipLeft < VIEW_COLUMN_PADDINGS) {
            toolTipLeft = VIEW_COLUMN_PADDINGS;
        } else if ($(window).width() < toolTipRight) { // adjust for right bounds
            var flipAdjust = resizeTooltip.width() + margin * 2 + $("div.ui-resizable-handle.ui-resizable-e").width();
            var rightBounds = $(window).width() - resizeTooltip.width() - margin - $("div.ui-resizable-handle.ui-resizable-e").width();
            toolTipLeft = Math.min(toolTipLeft - flipAdjust, rightBounds);
        }
        toolTipTop = (isResizing === false) ? mouseY - $(window).scrollTop() : toolTipTop;
        resizeTooltip.css({
            top: toolTipTop,
            left: toolTipLeft,
            position: "fixed",
            opacity: 1
        });
    };

    var hideResizeTooltip = function (delayMs) {
        delayMs = delayMs || 0;
        var resizeTooltipId = "resizeHandleTooltip";
        var tooltip = $("#" + resizeTooltipId);
        tooltip.fadeOut({
            duration: delayMs,
            complete: function () {
                tooltip.remove();
            }
        });
    };

    var configureResizeOverlay = function (resizeInfo) {
        var resizeOverlay = $("body").find(".resource-colresize-helper");
        resizeOverlay.css({
            left: resizeInfo.left,
            width: resizeInfo.width
        });
    };

    var emitNgEventSetPlaceholderHeight = function () {
        angular.element(document.body).injector().get("$rootScope")
            .$emit("JQ_SORTABLE_START-OVER",
                {
                    placeholderId: ".ResourceColumn-New .ResourceColumn-Center",
                    scaleRatio: 1,
                    bottomLimitingElementId: "#footer"
                }
            );
    }

    ResourceContainer.sortableDefinition = {
        items: "div.ResourceWrapperPanel",
        revert: 200,
        opacity: .7,
        connectWith: ".ResourceColumn",
        handle: ".sw-widget-drag-element",
        cursor: "move",
        forcePlaceholderSize: true,
        tolerance: 'pointer',
        delay: 100,
        distance: 5,
        placeholder: "resource-dragdrop-placeholder",
        over: function (event, ui) {
            var parentColumn = ui.placeholder.parent('.ResourceColumn');
            if (!parentColumn || parentColumn.length === 0) return;

            if (parentColumn.hasClass('ResourceColumn-New')) {
                parentColumn.addClass('sw-widget-drawer--jqsortable-over');
            }

            var siblingWidth = parentColumn.find(".ResourceWrapper").css("width");
            ui.placeholder.css("width", siblingWidth);
            if (window.angular) {
                emitNgEventSetPlaceholderHeight();
            }
        },
        out: function (event, ui) {
            var parentColumn = ui.placeholder.parent('.ResourceColumn');
            if (parentColumn.hasClass('ResourceColumn-New')) {
                parentColumn.removeClass('sw-widget-drawer--jqsortable-over');
            }
        },
        start: function (event, ui) {
            var resource = ui.item.find("div.ResourceWrapper");
            if (!resource.length) return;

            $(".ResourceColumn").prepend("<div class='collWidth' style='min-width:10px; width:100%; height:2px;></div>");
            ui.item.closest("div.ResourceWrapperPanel").addClass("sw-widget__dragging");
            $(".ResourceColumn-New .ui-resizable-handle").css("display", "none");

            if (window.angular) {
                $(document.body).addClass('sw-widget-drawer--dropping');

                emitNgEventSetPlaceholderHeight();
            }

            var parentColumn = ui.item.parent(".ResourceColumn");
            if (!parentColumn || parentColumn.length === 0) return;

            originResourceColumn = parentColumn;
            originResourcePosition = parentColumn.children("div.ResourceWrapperPanel").index(ui.item);
            originResourceColumnId = parentColumn.attr("ColumnID");
            origintResourceViewID = parentColumn.attr("ViewID");
        },
        stop: function (event, ui) {
            var resource = ui.item.find("div.ResourceWrapper");
            if (!resource.length) return;

            var resourceId = resource.attr("ResourceID");
            var parentColumn = ui.item.parent(".ResourceColumn");
            var columnId = parentColumn.attr("ColumnID");
            var viewId = parentColumn.attr("ViewID");
            var position = parentColumn.children("div.ResourceWrapperPanel").index(ui.item);

            $(".collWidth").remove();
            ui.item.closest("div.ResourceWrapperPanel").removeClass("sw-widget__dragging");
            $(document.body).removeClass('sw-widget-drawer--dropping');
            parentColumn.removeClass('sw-widget-drawer--jqsortable-over');
            $(".ResourceColumn-New .ui-resizable-handle").css("display", "block");

            var updateResourceWidth = function(columnToAddTo) {
                // try to find and set up new resource width
                // 2px which we're adding within the ResourceWrapper.ascx
                var width = viewsColumnInfo[viewId][columnToAddTo] + 2;
                ui.item.find("div.ResourceWrapper").width(width).trigger("resized");
                $(".ResourceColumn").css('width', 'auto');
            }

            // tell html5 Drag&Drop about JQ Sortable stop event
            if (window.angular && parentColumn.hasClass("ResourceColumn-New")) {
                var newColumnID = $(".ResourceColumn.ui-sortable").length;
                viewsColumnInfo[viewId][newColumnID] = 450;
                updateResourceWidth(newColumnID);

                angular.element(document.body).injector().get("$rootScope").$emit("JQ_SORTABLE_STOP",
                    {
                        repositionData: { viewId: parseInt(viewId, 10), resourceId: parseInt(resourceId, 10), columnNumber: newColumnID, position: 1 },
                        draggingElement: ui.item
                    }
                );
            }

            //this will exit on column = 7 (adding new column).  The actual column add is in the apollo-website project
            if (!parentColumn || parentColumn.length === 0 || viewsColumnInfo[viewId][columnId] === undefined) return;

            updateResourceWidth(columnId);

            // don't store new resource position in case of demo mode
            if (isDemoMode) return;

            // update resource position
            if (originResourcePosition !== position || originResourceColumnId !== columnId || origintResourceViewID !== viewId) {
                var repositionData = { viewId: viewId, resourceId: parseInt(resourceId, 10), column: parseInt(columnId, 10), position: position + 1 };
                SW.Core.Services.callWebService("/Orion/Services/WebAdmin.asmx", "UpdateResourcePosition", repositionData, function () { });
            }
        }
    };

    ResourceContainer.resizableDefinition = {
        handles: "e",
        grid: [6, 6],
        minWidth: MIN_COLUMN_WIDTH,
        helper: "resource-colresize-helper",
        create: function () { fixResizableHandle(); },
        resize: function (event, ui) {
            var cw = ui.size.width - VIEW_COLUMN_PADDINGS;
            var colId = parseInt(ui.element.attr("ColumnID"));
            var viewId = parseInt(ui.element.attr("ViewID"));
            viewsColumnInfo[viewId][colId] = cw;

            var resizeInfo = {
                top: ui.position.top,
                left: ui.position.left,
                relativeLeft: ui.originalElement.position().left,
                width: ui.size.width,
                height: ui.size.height
            };
            configureResizeOverlay(resizeInfo);
            showResizeTooltip(resizeInfo);
            ui.element.find("div.ui-resizable-handle.ui-resizable-e").css({ display: "none" });
            isResizing = true;
        },
        stop: function (event, ui) {
            var cw = ui.size.width - VIEW_COLUMN_PADDINGS;
            var colId = parseInt(ui.element.attr("ColumnID"));
            var viewId = parseInt(ui.element.attr("ViewID"));

            ui.element.css("width", cw + "px");
            ui.element.find(".ResourceWrapper")
                .css("width", cw + "px")
                .trigger("resized", [cw]);
            ui.element.find(".ResourceWrapper:not(.XuiResourceWrapper)")
                .css("overflow", "hidden");
            if (!isDemoMode) {
                SW.Core.Services.callWebService("/Orion/Services/WebAdmin.asmx", "UpdateColumnWidth", { viewId: viewId, column: colId, width: cw }, function() {});
            }
            hideResizeTooltip();
            fixResizableHandle();
            ui.element.find("div.ui-resizable-handle.ui-resizable-e").css({ display: "block" });
            isResizing = false;
        }
    };

    ResourceContainer.Init = function (config) {
        viewsColumnInfo = config.viewsColumnInfo;
        isDemoMode = config.isDemoMode;
        isNocView = config.isNocView;
        isReadOnlyMode = config.isReadOnlyMode;
        nocRotationInterval = config.viewsRotationInterval;
        // if user allowed to customize view then init drag&drop
        if ((config.allowCustomize || isDemoMode) && !isReadOnlyMode) {
            // init drag & drop
            $(".ResourceColumn").sortable(ResourceContainer.sortableDefinition);

            // Init jQuery Resize
            $(".ResourceColumn").resizable(ResourceContainer.resizableDefinition);

            $("div.ui-resizable-handle.ui-resizable-e").hover(
                function (event) {
                    var colEl = $(event.target).closest(".ResourceColumn");
                    var pos = {
                        top: colEl.offset().top,
                        left: colEl.offset().left,
                        relativeLeft: colEl.position().left,
                        width: colEl.width() + VIEW_COLUMN_PADDINGS,
                        height: colEl.height()
                    };
                    showResizeTooltip(pos);
                },
                function () {
                    hideResizeTooltip();
                }
            );

            $(window).scroll(function () {
                hideResizeTooltip();
            });

            $(document).mousemove(function (e) {
                mouseY = e.pageY;
            }).mouseover(); // call the handler immediately

            // disable resizable for now
            $('div.ui-resizable-handle').css('display', 'none');
        }

        if (config.isNocView) {
            // jquery cycle requires >= 2 slides 
            // in case we have only 1 slide we need to refresh it automatically
            if ($("table.ResourceContainer").length === 1) {
                refreshFirstSlide(nocRotationInterval);
            }
            else {
                $('.slideshow').cycle({
                    fx: config.viewsRotationInterval > 0 ? 'fade' : 'none',
                    slideExpr: '> table.ResourceContainer',
                    pager: '.noc-slides-navigation',
                    pagerAnchorBuilder: function () { return "<span class='noc-slides-navigation-item'>&nbsp;</span>"; },
                    before: function (prev, curr) {
                        $('.slideshow').css('height', 'auto');
                        prev.style.position = 'absolute';
                        curr.style.position = 'relative';
                        SW.Core.View.RefreshNocSlide(curr);
                    },
                    timeout: config.viewsRotationInterval, //timeout is in miliseconds
                    slideResize: 0
                });
            }
        }

        if ((config.allowCustomize || isDemoMode) && !config.isNocView)
            setDragAndDropMode(true);
    };

    var setDragAndDropMode = function (enabled) {
        $('div.ui-resizable-handle').css('display', (enabled) ? 'block' : 'none');

        if (enabled)
            $('.ResourceContainer').addClass('drag-and-drop-enabled');
        else if ($('.ResourceContainer').hasClass('drag-and-drop-enabled'))
            $('.ResourceContainer').removeClass('drag-and-drop-enabled');
    };

    ResourceContainer.ToggleSlideShowPaused = function (elem) {
        var paused = elem.getAttribute('data-paused') === "false";
        elem.setAttribute('data-paused', paused.toString());
        elem.innerHTML = (paused) ? '@{R=Core.Strings;K=WEBJS_SO0_85; E=js}' : '@{R=Core.Strings;K=WEBJS_SO0_86; E=js}';
    
		if (paused) {
			$('.slideshow').cycle('pause');
			slideShowPaused = true;
		} else {
			$('.slideshow').cycle('resume');
			slideShowPaused = false;
			if (nocRotationInterval > 0) {
				setTimeout(function () { refreshFirstSlide(nocRotationInterval); }, nocRotationInterval);
			}
		}
    };
})(SW.Core);
