﻿Ext.namespace("SW.Core");


/* Object is responsible for building resource cache and showing Resource Picker in dialog. 
 *
 * @param {Object} config
 *  config.dialogElement {String} id of the dialog element.
 *  config.dialogTitle {String} localized text which will be used for dialog title.
 *  config.onCreated {Function} callback function which will be called when is new loader object created. To callback is passed instance of this object.
 *  config.contentElement {String} id of the element where will be rendered resource picker.
 *  config.loadingElement {String} id of the element which contains loading message. Will be visible before cache is build. 
 *  config.onSelected  {Function} callback function which will be called when is selection of resources is done. To callback is passed instance of this object.
 *  config.btnDialogOk {String} id of the ok button.
 *  config.btnDialogCancel {String} id of the cancel button.
 */
SW.Core.ResourcePickerLoader = function (config) {
    var self = this,
        selectedColumn = 1,
        resourcePicker = config.resourcePicker,
        $resourcePickerDialog = $(config.dialogElement);
        
      
   
    function initDialogButtons() {
        $(config.btnDialogOk).click(selected);
        $(config.btnDialogCancel).click(hideDialog);
    }

    function selected() {
        if (jQuery.isFunction(config.onSelected)) {
            config.onSelected(self);
        }

        hideDialog();
    }

    function hideDialog() {
        $resourcePickerDialog.dialog('close');
    }

    function buildCache() {

        // HACK: When dialog is shown it executes script inside it again. It creates new resource picker instance (data stores).  
        // Moving the scripts out of the markup of ResourcePicker.ascx will fix the need for this hack.
        $resourcePickerDialog.find("script").remove();

        SW.Core.ResourcePicker.buildCache( function () {
            //Once we know cache is built render resource picker. (dialog may be open or closed depending on cache build time)
            //Content is off page to allow ExtJs to render properly in case dialog is closed.
            if (resourcePicker) {
                resourcePicker.Render();
            }
            $(config.loadingElement).hide();
            $(config.contentElement).removeClass('notVisible');
        });
    }
    
    function created() {
        if (jQuery.isFunction(config.onCreated)) {
            config.onCreated(self);
        }
    }

    this.AddResources = function (column) {
        selectedColumn = column;
        
        // Force clearing selection in grid.
        resourcePicker.RealoadResources();

        $resourcePickerDialog
            .removeClass('offPage')
            .dialog({
                width:  'auto',
                height: 'auto',
                title: config.dialogTitle,
                modal: true,
                close: function() {
                    resourcePicker.ClearSelection();
                },
                disableAutoFocus : true
            });
    };

    this.GetSelectedResources = function () {
        return {
            selectedColumn: selectedColumn,
            resources: resourcePicker.GetSelectedResources()
        };
    };

    buildCache();
    
    initDialogButtons();

    created();
};
