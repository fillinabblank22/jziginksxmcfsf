Ext.namespace("SW.Core");

/* Object is responsible for manage selected resources in Resource Picker. 
 * @param {Object} config 
 * config.selectedResourcesTo {String} id of the element where will be rendered selected resources.
 */
SW.Core.ResourcePickerMultipleResourcesSelector = function(config) {
    var resourceItemTemplate = "" +
        "<td><div ext:qtitle=\"{{qTitle}}\" ext:qtip='{{tooltip}}'>{{title}}</div></td>" +
        "<td style='width:20px; cursor:pointer'><img data-resource-path='{{path}}'  src='/Orion/images/Reports/delete_icon16x16.png'/></td>";

    var selectedResourcesTable = $("#" + config.selectedResourcesTo + " table")[0],
        selectedResources = [], // { path: '', title: ''}
        selectedTr = [], // { path: tr: refTr }
        $selectedResourcesEmptyEl = $("#" + config.selectedResourcesTo + " #selectedResourcesEmpty"),
        selectorModel;

    // PRIVATE METHODS:

    function createSelectionModel() {
       selectorModel = new Ext.grid.CheckboxSelectionModel();
       selectorModel.on("beforerowselect", resourceRowClick);
       selectorModel.on("rowdeselect", gridResourcesRowDeselect);
    }

    function addResourceToSelected(path, title, tooltip) {
        var tableBody = $("#" + config.selectedResourcesTo + " table > tbody")[0];
        var encodedTitle = ORION.encodeHTML(title);

        var tr = document.createElement("tr");
        tableBody.appendChild(tr);

        var $tr = $(tr);
        // Use double encoding for qTitle because of polyglot execution
        var innerHtml = _.template(resourceItemTemplate, { path: path, title: encodedTitle, qTitle: ORION.encodeHTML(encodedTitle), tooltip: tooltip });
        $tr.html(innerHtml);

        $tr.find("[data-resource-path]").on('click', function () {
            var rpath = $(this).data('resource-path');
            removeResourceFromSelected(rpath);
        });

        selectedTr.push({ path: path, refTr: tr });
    };

    // Cancel selection from grid and also remove row in right selection table
    function removeResourceFromSelected(path, dontUpdateUiGrid) {
        var index = 0;
        var found = false;

        // removing from array described ui
        for (index = 0; index < selectedTr.length; index++) {
            if (selectedTr[index].path == path) {
                found = true;
                // Removes row from table tbody.
                selectedResourcesTable.firstChild.removeChild(selectedTr[index].refTr);
                break;
            }
        }

        if (found) {
            selectedTr.splice(index, 1);
            selectedResources.splice(index, 1);
        }

        // updating grid ui
        if (!dontUpdateUiGrid) {
            var store = selectorModel.grid.getStore();
            for (index = 0; index < store.getCount(); index++) {
                var record = store.getAt(index);
                if (record.data['Path'] == path) {
                    selectorModel.deselectRow(index, false);
                    break;
                }
            }
        }

        if (selectedResources.length == 0) {
            $selectedResourcesEmptyEl.show();
        }
    };

    function gridResourcesRowSelect(selMod, rowIndex, record) {
        // here will be adding records into array in the case that will not be found in selectedResources field
        var path = record.data['Path'];
        var title = record.data['Title'];
        var tooltip = record.data['Tooltip'];
        
        var canAdd = true;
        for (var i = 0; i < selectedResources.length; i++) {
            if (selectedResources[i].path == path) {
                canAdd = false;
                break;
            }
        }

        if (canAdd) {
            addResourceToSelected(path, title, tooltip);
            selectedResources.push({ path: path, title: title });
        }

        if (selectedResources.length > 0) {
            $selectedResourcesEmptyEl.hide();
        }
    }

    function gridResourcesRowDeselect(selMod, rowIndex, record) {
        removeResourceFromSelected(record.data['Path'], true);
    }

    function resourceRowClick(selectionModel, rowIndex, keepExistingSelection, record) {
        // Because we are calling selection/deselection rows manually we need suspend events for a while to avoid recursion error. 
        selectionModel.suspendEvents();

        var isRowSelected = selectionModel.isSelected(rowIndex);
        if (isRowSelected) {
            selectionModel.deselectRow(rowIndex);
            gridResourcesRowDeselect(selectionModel, rowIndex, record);
        } else {
            selectionModel.selectRow(rowIndex, true);

           
            gridResourcesRowSelect(selectionModel, rowIndex, record);
        }

        selectionModel.resumeEvents();

        // Prevents default action on grid.
        return false;
    }

    // PUBLIC METHODS:
    this.ClearSelection = function () {       
        selectedResources = [];
        selectedTr = [];
        selectorModel.clearSelections();
        $("#" + config.selectedResourcesTo + " table > tbody").html('');       
    };

    this.GetSelectionModel = function() {
        if (!selectorModel) {
            createSelectionModel();
        }
        return selectorModel;
    };

    this.GetSelectedResources = function() {
        return selectedResources;
    };
    
    this.UpdateGridAccordingToSelection = function() {
        var store = selectorModel.grid.getStore();
        for (var index = 0; index < store.getCount(); index++) {
            var record = store.getAt(index);
            for (var i = 0; i < selectedResources.length; i++) {
                if (record.data['Path'] == selectedResources[i].path) {
                    selectorModel.selectRow(index, true, false);
                }
            }
        }
    };

};
