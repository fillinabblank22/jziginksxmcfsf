﻿Ext.namespace("SW.Core");

/* Object is responsible for searching in resouces templates.
 *
 * @param {Object} config
 *  config.searchInput {String} id of the search input element.
 *  config.searchButtonHolder {String} id of the element where will be placed search button.
 *  config.viewType {String} the type of view where is resource picker used. 
 *  config.limit {Number} determines how many items will be shown in autocomplete.
 *  config.onSearched {Function} callback will be called on searchin. Will be passed search term to this callback.
 */
SW.Core.ResourcePickerSearcher = function (config) {
    var watermarkText = '@{R=Core.Strings; K=WEBJS_ZT0_2; E=js}',
        defaultLimit = 5,
        self = this,
        $searchInput = $(config.searchInput),
        autoCompleteSearchMoreText = '@{R=Core.Strings; K=WEBJS_OJ0_2; E=js}';
    
    createLocalizableSearchButton();
    createWatermarkTextbox();
    createAutocompleteTextbox();
    addSearchingOnEnterPressed();

    created();

    // PRIVATE METHODS:

    function createLocalizableSearchButton() {
        var searchButton = $(SW.Core.Widgets.Button('@{R=Core.Strings; K=WEBJS_ZT0_1; E=js}', {
            type: 'secondary'
        }));

        searchButton.appendTo(config.searchButtonHolder);
        searchButton.click(searched);
    }

    function createWatermarkTextbox() {
        var watermark = new SW.Core.Widgets.WatermarkTextbox(config.searchInput, watermarkText);
    }

    function createAutocompleteTextbox() {
        var searchTerm;
        
        var autocomplete = $searchInput.autocomplete({
            source: function (request, respond) {
                searchTerm = request.term;
                SW.Core.Services.callWebService(
                    "/Orion/Services/ResourcePickerWebServices.asmx",
                    "Autocomplete",
                    { request: { Term: request.term, ViewType: config.viewType || '', Limit: config.limit || defaultLimit} },
                    function (response) {
                        response.push(autoCompleteSearchMoreText);
                        //Call JQuery autocomplete respond function
                        respond(response);
                    }
                );
            },
            minLength: 3,
            select: function (event, ui) {
                var autoCompletedValue = ui.item.value;

                if (autoCompletedValue !== autoCompleteSearchMoreText) {
                    setSearchTerm(autoCompletedValue);
                } else {
                    setSearchTerm(searchTerm);
                }
                searched();

                return false;//Prevent the search textbox from being overwritten with the AutoComplete value.  Important for "Search More"
            }
        });
    }

    function addSearchingOnEnterPressed() {
        var ENTER_KEY_CODE = 13;

        $searchInput.keypress(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            // Enter keycode
            if (code == ENTER_KEY_CODE) {
                searched();
            }
        });
    }

    function getSearchTerm() {
        return $searchInput.val();
    }
    function setSearchTerm(value) {
        return $searchInput.val(value);
    }

    function created() {
        if (config.onCreated && jQuery.isFunction(config.onCreated)) {
            config.onCreated(self);
        }
    }

    function searched() {
        var searchTerm = getSearchTerm();

        if (searchTerm && (watermarkText !== searchTerm) && config.onSearched && jQuery.isFunction(config.onSearched)) {
            config.onSearched(searchTerm);
        }
    }
};