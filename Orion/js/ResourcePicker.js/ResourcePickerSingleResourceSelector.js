﻿Ext.namespace("SW.Core");

/* Object is responsible for manage selected resource in Resource Picker. 
 * @param {Object} config 
 */
SW.Core.ResourcePickerSingleResourceSelector = function (config) {

    var selectedResource, // { path: '', title: ''}
        selectorModel;

    // PRIVATE METHODS:

    function createSelectionModel() {
        selectorModel = new Ext.sw.grid.RadioSelectionModel();
        selectorModel.hideable = false;
        selectorModel.menuDisabled = true;
        selectorModel.on("rowselect", resourceRowClick);
        selectorModel.deselectRow = function(index) {
            // call parent
            Ext.sw.grid.RadioSelectionModel.superclass.deselectRow.apply(this, arguments);

            // Property can be set in beforeRowSelect event, which is called by above parent call.
            // but we don't get this information from parent call, so we have check this property to cancel selection in radio.
            if (this.selectionDisabled === true) {
                return;
            }

            // check radio
            var row = Ext.fly(this.grid.view.getRow(index));
            if (row) {
                row.child('input[type=radio]').dom.checked = false;
            }
        };
    }

    function resourceRowClick(selectionModel, rowIndex, record) {
        var path = record.data['Path'];
        var title = record.data['Title'];

        selectedResource = { path: path, title: title };
    }

    // PUBLIC METHODS:
    this.ClearSelection = function () {
        selectedResource = [];
        selectorModel.clearSelections();
    };

    this.GetSelectionModel = function () {
        if (!selectorModel) {
            createSelectionModel();
        }
        return selectorModel;
    };

    this.GetSelectedResources = function () {
        return [selectedResource];
    };

    this.UpdateGridAccordingToSelection = function () {
        if (!selectedResource) return;

        var store = selectorModel.grid.getStore();
        for (var index = 0, max = store.getCount(); index < max; index++) {
            var record = store.getAt(index);
            if (record.data['Path'] === selectedResource.path) {
                selectorModel.selectRow(index, true, false);
                break;
            }
        }
    };

};
