﻿SW.Core.namespace("SW.Core.Pickers");

SW.Core.Pickers.RESOURCE_PICKER_WEB_SERVICE_URL = '/Orion/Services/ResourcePickerWebServices.asmx';

/* Object is responsible for rendering Resource Picker UI. 
 *
 * @param {Object} config 
 *  config.renderTo {String} id of the element where will be resource picker rendered.
 *  config.onCreated {Function} callback function which will be called when is resource picker created. To callback is passed instance of picker.
 *  config.selectedResourcesTo {String} id of the element where will be rendered selected resources.
 *  config.viewType {String} the type of view where is resource picker used. 
 *  config.selectionMode {String} - determine how many items can be selected. Valid options are: Single | Multiple 
 */
SW.Core.ResourcePicker = function (config) {
    var self = this;
    var userPageSize = 20;
    var selectedColumns;
    var firstTimeLoad = true;
    var isReportsAddContent = config.viewType == "Reporting";
    ORION.prefix = "ResourcePicker_";

    // == DataStore objects
    var groupsDataStore, // serve for combobox group by
        categoriesDataStore, // serve for left panel GridPanel (list of group)
        resourceGridStore;

    // == UI Data containers objects
    var groupsComboArray,
        groupListGrid,
        pagingToolbar,
        gridResources;

    var selector,
        selectedGroup = '', // will be e.g. Type, Owner, ClassicCategory or empty string in the case No Grouping will be selected
        selectedCategory = ''; // will be e.g. [All Resources], Alerts, Charts, Events

    var mainGridPanel; // main layout control
    var searchTerm;

    var getSearchTerm = function () {
        return searchTerm || "";
    };

    var refreshGroupListGrid = function () {
        setGetAvailableGroupsRequestJsonData();
        categoriesDataStore.load();
    };

    var refreshResourceGrid = function () {
        if (resourceGridStore != null) {
            setGetResourcesRequestJsonData();
            resourceGridStore.load({ params: { start: 0, limit: userPageSize} });
        }
    };

    var initResourceGridSelectedColumns = function () {
        for (var i = 1; i < gridResources.getColumnModel().getColumnCount(); i++) {
            if (selectedColumns.indexOf(gridResources.getColumnModel().getDataIndex(i)) > -1) {
                gridResources.getColumnModel().setHidden(i, false);
            } else {
                gridResources.getColumnModel().setHidden(i, true);
            }
        }
    };

    var setGetResourcesRequestJsonData = function () {
        resourceGridStore.proxy.conn.jsonData = {
            request: {
                SearchTerm: getSearchTerm() || "",
                GroupName: selectedGroup || "",
                CategoryName: selectedCategory || "",
                ViewType: config.viewType || ""
            }
        };
    };

    var setGetAvailableGroupsRequestJsonData = function () {
        categoriesDataStore.proxy.conn.jsonData = {
            request: {
                GroupName: selectedGroup || "",
                ViewType: config.viewType || ""
            }
        };
    };

    var createAndInitializeDataStores = function () {
        // this will be in future retrieved from WebService
        groupsDataStore = new Ext.data.SimpleStore(
            {
                fields: ['Name', 'Value'],
                data: [
                    ['@{R=Core.Strings; K=WEBJS_VB0_76; E=js}', ''],
                    ['@{R=Core.Strings; K=WEBJS_PS0_23; E=js}', 'Type'],
                    ['@{R=Core.Strings; K=WEBJS_PS0_24; E=js}', 'Owner'],
                    ['@{R=Core.Strings; K=WEBJS_PS0_25; E=js}', 'ClassicCategory']
                ]
            });

        // Will need to rewrite to my webservice
        categoriesDataStore = new ORION.WebServiceStore(
            SW.Core.Pickers.RESOURCE_PICKER_WEB_SERVICE_URL + '/GetGroupCategories',
            [
                { name: 'GroupName', mapping: 0 },
                { name: 'CategoryName', mapping: 1 },
                { name: 'CategorySubname', mapping: 2 }
            ],
            "CategoryName");

        resourceGridStore = new ORION.WebServiceStore(
            SW.Core.Pickers.RESOURCE_PICKER_WEB_SERVICE_URL + '/GetListResources',
            [
                { name: 'Path', mapping: 0 },
                { name: 'Title', mapping: 1 },
                { name: 'Tooltip', mapping: 2 },
                { name: 'Category', mapping: 3 }
            ]
        );
    };

    var createAndInitializeDataUIControls = function () {
        groupsComboArray = new Ext.form.ComboBox(
            {
                id: 'groupBy',
                mode: 'local',
                fieldLabel: 'Name',
                displayField: 'Name',
                valueField: 'Value',
                store: groupsDataStore,
                triggerAction: 'all',
                value: '@{R=Core.Strings; K=WEBJS_VB0_76; E=js}',
                typeAhead: true,
                forceSelection: true,
                selectOnFocus: false,
                multiSelect: false,
                editable: false,
                width: 175,
                listeners: {
                    'select': function () {
                        selectedGroup = this.store.data.items[this.selectedIndex].data.Value;
                        selectedCategory = '';
                        setToGroupingMode();
                        refreshGroupListGrid();
                        firstTimeLoad = false;
                    }
                }
            });

        groupListGrid = new Ext.grid.GridPanel({
            id: 'groupingGrid',
            store: categoriesDataStore,
            cls: 'hide-header',
            columns: [
                { dataIndex: 'GroupName', id: 'Value', editable: false, sortable: false, hidden: true },
                { dataIndex: 'CategoryName', width: '100%', editable: false, sortable: false, renderer: renderTooltipForCategory }
            ],
            selModel: new Ext.grid.RowSelectionModel(),
            autoScroll: true,
            loadMask: true,
            listeners: {
                cellclick: function (mygrid, row, cell, e) {
                    selectedCategory = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                    selectedGroup = mygrid.store.data.items[row].data['GroupName'];
                    setToGroupingMode();
                    refreshResourceGrid();
                }
            },
            anchor: '0 0',
            viewConfig: { forceFit: true },
            split: false,
            autoExpandColumn: 'Value'
        });

        groupListGrid.store.on('beforeload', function () { groupListGrid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading'); });
        groupListGrid.store.on('load', function () {
            if (groupListGrid.getStore().getCount() == 0) {
                refreshResourceGrid();
            }

            setTimeout(function () { // let UI thread to do it's work before selecting first row in groupListGrid
                // Set default selected row to "Reports" for first Load
                if (selectedGroup == 'Type' && firstTimeLoad) {
                    var tempStore = groupListGrid.store.data.items;
                    for (var i = 0; i < tempStore.length; i++) {
                        if (tempStore[i].data["CategoryName"] == '@{R=Core.Strings; K=CoreMetadataTypeValues_Reports; E=js}') {
                            groupListGrid.getSelectionModel().selectRow(i);
                            break;
                        }
                    }
                } else {
                    groupListGrid.getSelectionModel().selectFirstRow();
                }
                groupListGrid.getEl().unmask();
            }, 0);
        });

        groupListGrid.getSelectionModel().on("rowselect", function (selMod, rowIndex, record) {
            selectedCategory = record.data["CategoryName"];
            refreshResourceGrid();
        });

        groupListGrid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
            if (response.responseText) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
            groupListGrid.getEl().unmask();
            refreshResourceGrid();
        });

        pagingToolbar = new Ext.PagingToolbar(
            {
                store: resourceGridStore,
                pageSize: userPageSize,
                dock: 'bottom',
                viewConfig: { forceFit: true },
                split: false,
                displayInfo: true,
                displayMsg: "@{R=Core.Strings;K=WEBJS_SO0_2; E=js}",
                emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}",
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                listeners:
                {
                    afterrender: function () {
                        this.refresh.hide();
                    }
                }
            }
        );

        var resourcesSelectionModel = selector.GetSelectionModel();

        gridResources = new Ext.grid.GridPanel({
            id: 'gridResources',
            region: 'center',
            viewConfig: { forceFit: true, emptyText: "@{R=Core.Strings;K=WEBJS_ZT0_8; E=js}" },
            store: resourceGridStore,
            baseParams: { start: 0, limit: userPageSize },
            stripeRows: true,
            trackMouseOver: false,
            columns: [resourcesSelectionModel,
                { header: 'ResourcePath', width: 0, hidden: true, hideable: false, dataIndex: 'Path' },
                { id: 'Title', header: '@{R=Core.Strings;K=WEBJS_ZT0_9; E=js}', width: isSingleMode() ? 450 : 250, sortable: true, hideable: false, dataIndex: 'Title', renderer: renderTooltipForResource },
                { id: 'Category', header: '@{R=Core.Strings;K=XMLDATA_SO0_25; E=js}', width: isSingleMode() ? 370 : 170, sortable: true, dataIndex: 'Category', renderer: renderTooltipForResource }
            ],
            sm: resourcesSelectionModel,
            autoScroll: true,
            autoExpandColumn: 'Category',
            loadMask: true,
            width: isSingleMode() ? 820 : 420,
            height: 510,
            bbar: pagingToolbar,
            enableColumnMove: false,
            cls: 'resourcesGrid'
        });

        selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');
        initResourceGridSelectedColumns();

        gridResources.store.on('beforeload', function () { gridResources.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading'); });
        gridResources.store.on('load', function () {
            selector.UpdateGridAccordingToSelection();
            gridResources.getEl().unmask();
        });

        gridResources.store.on("exception", function (dataProxy, type, action, options, response, arg) {
            if (response.responseText) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
            gridResources.getEl().unmask();
        });

        gridResources.getColumnModel().on('hiddenchange', function () {
            var cols = '';
            for (var j = 1; j < gridResources.getColumnModel().getColumnCount(); j++) {
                if (!gridResources.getColumnModel().isHidden(j)) {
                    cols += gridResources.getColumnModel().getDataIndex(j) + ',';
                }
            }
            ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
        });
    };

    var createAndInitializeUILayoutControls = function () {
        // Here is creating layour controls
        var groupByTopPanel = new Ext.Panel({
            region: 'centre',
            split: false,
            heigth: 50,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), groupsComboArray],
            cls: 'panel-no-border panel-bg-gradient'
        });

        var navPanel = new Ext.Panel({
            region: 'west',
            split: true,
            anchor: '0 0',
            width: 200,
            heigth: 500,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [groupByTopPanel, groupListGrid],
            cls: 'panel-no-border'
        });

        navPanel.on("bodyresize", function () {
            groupListGrid.setSize(navPanel.getSize().width, 450);
        });

        mainGridPanel = new Ext.Panel({
            region: 'center',
            split: true,
            layout: 'table',
            collapsible: false,
            items: [navPanel, gridResources]
        });
    };

    function initilizeSelectionMode() {
        if (isSingleMode()) {

            selector = new SW.Core.ResourcePickerSingleResourceSelector({});

            // Hides selected recources columns.
            $('#' + config.selectedResourcesTo)
                .parents('table.resourcePicker')
                .find('.selectedResourcesColumn')
                .hide();

        } else { // Multiple selectoion mode by default.
            selector = new SW.Core.ResourcePickerMultipleResourcesSelector({
                selectedResourcesTo: config.selectedResourcesTo
            });
        }
    }

    function renderTooltipForCategory(value, metadata, record) {
        var groubSubname = record.get('CategorySubname');
        if (groubSubname) {
            return '<span ext:qtitle="' + value + '" ext:qtip="' + groubSubname + '">' + value + '</span>';
        }
        return value;
    }

    function renderTooltipForResource(value, metadata, record) {
        var encodedValue = ORION.encodeHTML(value);

        var tooltip = record.get('Tooltip');
        if (tooltip) {
            // use double encoding for qtitle because of polyglot execution
            return metadata.id == "Title" ? '<span ext:qtitle="' + ORION.encodeHTML(encodedValue) + '" ext:qtip="' + tooltip + '">' + encodedValue + '</span>'
                                          : '<span ext:qtitle="' + value + '" ext:qtip="' + tooltip + '">' + value + '</span>';
        }
        return encodedValue;
    }

    function setToGroupingMode() {
        searchTerm = '';
    }

    function setToSearchingMode() {
        selectedCategory = '';
        selectedGroup = '';
    }

    function isSingleMode() {
        return config.selectionMode === 'Single';
    }

    function created() {
        if (config.onCreated && jQuery.isFunction(config.onCreated)) {
            config.onCreated(self);
        }
    }

    // This method will create and initialize all ResourcePicker controls and render into page.
    this.Render = function () {
        initilizeSelectionMode();
        createAndInitializeDataStores();
        createAndInitializeDataUIControls();
        createAndInitializeUILayoutControls();

        mainGridPanel.render(config.renderTo);

        if (isReportsAddContent) {
            groupsComboArray.setValue('Type');
            selectedGroup = groupsComboArray.value;
        }
        refreshGroupListGrid();
    };

    this.Search = function (term) {
        searchTerm = term;
        setToSearchingMode();
        refreshResourceGrid();
    };

    // Method will return list of selected resources as array with object in form { path, title }
    this.GetSelectedResources = function () {
        if (selector) {
            return selector.GetSelectedResources();
        }
    };

    this.ClearSelection = function () {
        if (selector) {
            selector.ClearSelection();
        }
    };

    this.RealoadResources = function () {
        refreshResourceGrid();
    };

    Ext.QuickTips.init();

    Ext.apply(Ext.QuickTips.getQuickTip(), {
        showDelay: 0,
        trackMouse: false
    });

    created();
};

SW.Core.ResourcePicker.buildCache = function(callback) {
    SW.Core.Services.callWebService(SW.Core.Pickers.RESOURCE_PICKER_WEB_SERVICE_URL, 'BuildCache', {}, callback);
};
