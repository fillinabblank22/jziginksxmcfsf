﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

SW.Orion.NotePageRecipientsSelector = function () {
    var $recipientsInput = $("#recipientsSelection input[type='hidden']");
    var selectedRecipients = new Array();
    var availableRecipientsGridStore;
    var sendToRecipientsGridStore;
    function bindSeletionToGrids() {
        var jsonData = new Array();
        if ($recipientsInput.val() != "") {
            $.each(JSON.parse($("#recipientsSelection input[type='hidden']").val()), function () {
                jsonData.push({ name: this.toString() });
            });
        }
        if (selectedRecipients.length != 0)
            sendToRecipientsGridStore.loadData({ records: selectedRecipients }, false);
        else
            sendToRecipientsGridStore.removeAll();

        var recipientsleft = $.grep(jsonData, function (n, i) {
            return selectedRecipients.map(function (e) { return e.name; }).indexOf(n.name) == -1;
        });
        availableRecipientsGridStore.loadData({ records: recipientsleft }, false);
    }
    return {
        getSelectedRecipients: function () {
            return sendToRecipientsGridStore.data.items;
        },
        setSelectedRecipients: function (val) {
            selectedRecipients = val;
            bindSeletionToGrids();
        },
        init: function () {
            var jsonData = new Array();
            if ($recipientsInput.val() != "") {
                $.each(JSON.parse($("#recipientsSelection input[type='hidden']").val()), function () {
                    jsonData.push({ name: this });
                });
            }
            var recipientsData = {
                records: jsonData
            };

            var fields = [
                { name: 'name', mapping: 'name' }
            ];

            availableRecipientsGridStore = new Ext.data.JsonStore({
                fields: fields,
                data: recipientsData,
                root: 'records'
            });

            var cols = [
                { id: 'name', header: '@{R=Core.Strings;K=WEBJS_AB0_62;E=js}', width: 120, sortable: true, dataIndex: 'name' }
            ];

            var availableRecipientsGrid = new Ext.grid.GridPanel({
                ddGroup: 'secondGridDDGroup',
                store: availableRecipientsGridStore,
                columns: cols,
                enableDragDrop: true,
                stripeRows: true,
                autoExpandColumn: 'name',
                title: '@{R=Core.Strings;K=WEBJS_AB0_60;E=js}'
            });

            sendToRecipientsGridStore = new Ext.data.JsonStore({
                fields: fields,
                root: 'records'
            });

            var sendToRecipientsGrid = new Ext.grid.GridPanel({
                ddGroup: 'firstGridDDGroup',
                store: sendToRecipientsGridStore,
                columns: cols,
                enableDragDrop: true,
                stripeRows: true,
                autoExpandColumn: 'name',
                title: '@{R=Core.Strings;K=WEBJS_AB0_61;E=js}'
            });

            var displayPanel = new Ext.Panel({
                width: 400,
                height: 300,
                layout: 'hbox',
                renderTo: 'recipientsSelection',
                defaults: { flex: 1 },
                layoutConfig: { align: 'stretch' },
                items: [
                    availableRecipientsGrid,
                    sendToRecipientsGrid
                ],
                bbar: [
                    '->',
                    {
                        text: '@{R=Core.Strings;K=WEBJS_VB0_38;E=js}',
                        handler: function () {
                            availableRecipientsGridStore.loadData(recipientsData);
                            sendToRecipientsGridStore.removeAll();
                        }
                    },
                    '->',
                    {
                        text: '@{R=Core.Strings;K=WEBJS_VB0_36;E=js}',
                        handler: function () {
                            availableRecipientsGridStore.removeAll();
                            sendToRecipientsGridStore.loadData(recipientsData);
                        }
                    }
                ]
            });

                var firstGridDropTargetEl = availableRecipientsGrid.getView().scroller.dom;
            var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                ddGroup: 'firstGridDDGroup',
                notifyDrop: function (ddSource, e, data) {
                    var records = ddSource.dragData.selections;
                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                    availableRecipientsGrid.store.add(records);
                    availableRecipientsGrid.store.sort('name', 'ASC');
                    return true;
                }
            });

            var secondGridDropTargetEl = sendToRecipientsGrid.getView().scroller.dom;
            var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                ddGroup: 'secondGridDDGroup',
                notifyDrop: function (ddSource, e, data) {
                    var records = ddSource.dragData.selections;
                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                    sendToRecipientsGrid.store.add(records);
                    sendToRecipientsGrid.store.sort('name', 'ASC');
                    return true;
                }
            });
        }
    };
} ();
