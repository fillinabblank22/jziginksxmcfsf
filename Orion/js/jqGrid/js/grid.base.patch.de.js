;(function($){
/**
 * jqGrid English Translation
 * Tony Tomov tony@trirand.com
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = {};

$.jgrid.defaults = {
	recordtext: "@{R=Core.Strings;K=WEBJS_TM0_52;E=js}",
	loadtext: "@{R=Core.Strings;K=WEBJS_VB0_1;E=js}",
	pgtext : "/"
};
$.jgrid.search = {
    caption: "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}",
    Find: "@{R=Core.Strings;K=WEBJS_TM0_54;E=js}",
    Reset: "@{R=Core.Strings;K=WEBJS_TM0_55;E=js}",
    odata : ['@{R=Core.Strings;K=WEBJS_TM0_77;E=js}', '@{R=Core.Strings;K=WEBJS_TM0_78;E=js}', '@{R=Core.Strings;K=WEBJS_TM0_79;E=js}', '@{R=Core.Strings;K=WEBJS_TM0_80;E=js}','@{R=Core.Strings;K=WEBJS_TM0_81;E=js}','@{R=Core.Strings;K=WEBJS_TM0_82;E=js}', '@{R=Core.Strings;K=LIBCODE_VB0_21;E=js}','@{R=Core.Strings;K=LIBCODE_VB0_20;E=js}','@{R=Core.Strings;K=LIBCODE_VB0_22;E=js}' ]
};
$.jgrid.edit = {
    addCaption: "@{R=Core.Strings;K=WEBJS_TM0_56;E=js}",
    editCaption: "@{R=Core.Strings;K=WEBJS_TM0_57;E=js}",
    bSubmit: "@{R=Core.Strings;K=CommonButtonType_Submit;E=js}",
    bCancel: "@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}",
	bClose: "@{R=Core.Strings;K=WEBJS_PCC_4;E=js}",
    processData: "@{R=Core.Strings;K=WEBJS_TM0_58;E=js}",
    msg: {
        required:"@{R=Core.Strings;K=WEBJS_TM0_59;E=js}",
        number:"@{R=Core.Strings;K=WEBJS_TM0_60;E=js}",
        minValue:"@{R=Core.Strings;K=WEBJS_TM0_61;E=js}",
        maxValue:"@{R=Core.Strings;K=WEBJS_TM0_62;E=js}",
        email: "@{R=Core.Strings;K=WEBJS_TM0_63;E=js}",
        integer: "@{R=Core.Strings;K=WEBJS_TM0_64;E=js}",
		date: "@{R=Core.Strings;K=WEBJS_TM0_65;E=js}"
    }
};
$.jgrid.del = {
    caption: "@{R=Core.Strings;K=CommonButtonType_Delete;E=js}",
    msg: "@{R=Core.Strings;K=WEBJS_TM0_66;E=js}",
    bSubmit: "@{R=Core.Strings;K=CommonButtonType_Delete;E=js}",
    bCancel: "@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}",
    processData: "@{R=Core.Strings;K=WEBJS_TM0_58;E=js}"
};
$.jgrid.nav = {
	edittext: " ",
    edittitle: "@{R=Core.Strings;K=WEBJS_TM0_67;E=js}",
	addtext:" ",
    addtitle: "@{R=Core.Strings;K=WEBJS_TM0_68;E=js}",
    deltext: " ",
    deltitle: "@{R=Core.Strings;K=WEBJS_TM0_69;E=js}",
    searchtext: " ",
    searchtitle: "@{R=Core.Strings;K=WEBJS_TM0_70;E=js}",
    refreshtext: "",
    refreshtitle: "@{R=Core.Strings;K=WEBJS_TM0_71;E=js}",
    alertcap: "@{R=Core.Strings;K=StatusDesc_Warning;E=js}",
    alerttext: "@{R=Core.Strings;K=WEBJS_TM0_72;E=js}"
};
// setcolumns module
$.jgrid.col ={
    caption: "@{R=Core.Strings;K=WEBJS_TM0_73;E=js}",
    bSubmit: "@{R=Core.Strings;K=CommonButtonType_Submit;E=js}",
    bCancel: "@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}"	
};
$.jgrid.errors = {
	errcap : "@{R=Core.Strings;K=WEBJS_TM0_74;E=js}",
	nourl : "@{R=Core.Strings;K=WEBJS_TM0_75;E=js}",
	norecords: "@{R=Core.Strings;K=WEBJS_TM0_76;E=js}"
};
})(jQuery);
