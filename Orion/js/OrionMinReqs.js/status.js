﻿(function() {
  var status = SW.Core.namespace('SW.Core.Status');
  var table = status.Table = @{R=Live.Web;K=Core.StatusInfoTable};
  var lookup = {}, lookup2 = {};

  status.Get = function GetOrionStatus(v) { v = (v === undefined ? '0' : ''+v); return lookup[v] || lookup2[v] || lookup2[v.toLowerCase()] || lookup['0']; }

  for (var i = 0, imax = table.length; i < imax; ++i) {
      var v = table[i];
      lookup[''+v.StatusId] = v;
      lookup2[(''+v.StatusName).toLowerCase()] = v;
      try { v.DisplayProperties = JSON.parse(v.DisplayProperties); } catch (e) { v.DisplayProperties = null; }
  }
})();
