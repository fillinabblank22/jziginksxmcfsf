﻿// Underscore templates use <% %>, <%= %>, and <%- %> as delimiters in templates.  This conflicts with
// ASP.Net's delimiters.  We'll change underscore to use {# #}, {{ }}, and {{{ }}} instead
_.templateSettings = {
    evaluate: /\{#([\s\S]+?)#\}/g,
    interpolate : /\{\{(.+?)\}\}/g,
    escape: /\{\{\{(.+?)\}\}\}/g
};
