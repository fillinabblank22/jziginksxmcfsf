﻿// Extension of Core.DateHelper object

/* global SW:true */ // JSHint

(function (helper) {

    var config = {
        strings: {
            justNow: '@{R=Core.Strings.2;K=TimeAgoFormat_justNow;E=js}', // just now
            second: '@{R=Core.Strings.2;K=TimeAgoFormat_second;E=js}', // about a second ago
            seconds: '@{R=Core.Strings.2;K=TimeAgoFormat_seconds;E=js}', // {0} seconds ago
            minute: '@{R=Core.Strings.2;K=TimeAgoFormat_minute;E=js}', // about a minute ago
            minutes: '@{R=Core.Strings.2;K=TimeAgoFormat_minutes;E=js}', // {0} minutes ago
            hour: '@{R=Core.Strings.2;K=TimeAgoFormat_hour;E=js}', // about a hour ago
            hours: '@{R=Core.Strings.2;K=TimeAgoFormat_hours;E=js}', // {0} hours ago
            day: '@{R=Core.Strings.2;K=TimeAgoFormat_day;E=js}', // about a day ago
            days: '@{R=Core.Strings.2;K=TimeAgoFormat_days;E=js}', // {0} days ago
            month: '@{R=Core.Strings.2;K=TimeAgoFormat_month;E=js}', // about a month ago
            months: '@{R=Core.Strings.2;K=TimeAgoFormat_months;E=js}', // {0} months ago
            year: '@{R=Core.Strings.2;K=TimeAgoFormat_year;E=js}', // about a year ago
            years: '@{R=Core.Strings.2;K=TimeAgoFormat_years;E=js}' // {0} years ago
        }
    };

    function formatFromUtc(eventDate, nowOverride, stringsOverride) {
        return formatFrom(eventDate, true, nowOverride, stringsOverride);
    }

    function formatFromLocal(eventDate, nowOverride, stringsOverride) {
        return formatFrom(eventDate, false, nowOverride, stringsOverride);
    }

    // function being using to map our old time frames to apollo format
    function GetPerfStackTimeFrame(timeFrame) {
        //string[] periods = { "Today", "Yesterday", "Last 7 Days", "Last 30 Days", "This Month", "Last Month", "This Year" };
        //startTime=2018-07-16T10:51:00.000Z&endTime=2018-07-30T10:51:27.877Z
        var now = new Date();
        switch (timeFrame) {
        case "Past Hour":
            return "presetTime=lastHour"; 
        case "Last 2 Hours":
            return "presetTime=last2Hours"; 
        case "Today":
            return "startTime=" + (new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0)).toISOString() + "&endTime=" + now.toISOString(); 
        case "Yesterday":
            now.setDate(now.getDate() - 1);
            return "startTime=" + (new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0)).toISOString() + "&endTime=" + (new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59)).toISOString();
        case "Last 7 Days":
            return "presetTime=last7Days"; //+
        case "Last 30 Days":
            return "presetTime=last30Days"; //+
        case "This Month":
            return "startTime=" + (new Date(now.getFullYear(), now.getMonth(), 1, 0, 0)).toISOString() + "&endTime=" + now.toISOString(); //+
        case "Last Month":
            var endDate = new Date(new Date(now.getFullYear(), now.getMonth(), 1, 0, 0));
            endDate.setDate(now.getDate() - 1);
            now.setDate(now.getMonth() - 1);
            return "startTime=" + (new Date(now.getFullYear(), now.getMonth(), 1, 0, 0)).toISOString() + "&endTime=" + (new Date(now.getFullYear(), now.getMonth(), endDate.getDate(), 23, 59)).toISOString();
        case "This Year":
            return "startTime=" + (new Date(now.getFullYear(), 0, 1, 0, 0)).toISOString() + "&endTime=" + now.toISOString(); //+
        default:
            return "presetTime=last12Hours";
        }
    }

    // This function will take a Date object and
    // transform it into 'time ago' string representation
    // e.g. 'This Node was discovered 4 minutes ago'.
    // PARAMS:
    // eventDate: date of the event
    // isUtc: uh.. flag
    // nowOverride: Optional override of LOCAL 'now' (for testing)
    // stringsOverride: Optional object with strings (for testing)
    function formatFrom(eventDate, isUtc, nowOverride, stringsOverride) {
        if (eventDate && eventDate instanceof Date) {

            var event;
            var now;
            var diff;
            var strings;

            // The 'now' can be overriden
            // because I need to work with
            // tiny diffences in unit tests.
            if (nowOverride && nowOverride instanceof Date)
                now = nowOverride;
            else
                now = new Date();

            // Default strings can be also
            // overriden for testing purposes
            if (stringsOverride)
                strings = stringsOverride;
            else
                strings = config.strings;

            // Get the actual diff between
            // the dates so I can extract
            // the time fragments.
            event = isUtc ? SW.Core.DateHelper.utcToLocal(eventDate) : eventDate;
            diff = now - event;

            return stringifyTheDiff(diff, strings);

        } else {
            throw 'You need to pass a date object.';
        }
    }

    // The conversion to text format is being done here.
    // PARAMS
    // diff: Difference between two dates in miliseonds
    // test: Object with strings
    function stringifyTheDiff(diff, text) {

        var totalSeconds;
        var totalMinutes;
        var totalHours;
        var totalDays;

        // JS doesn't have 'getTotalSeconds'
        // like C#, so these values has to be
        // extracted from miliseconds that are
        // returned when subtracting two dates.
        totalSeconds = Math.floor(diff / 1000) || 0;
        totalMinutes = Math.floor(totalSeconds / 60);
        totalHours = Math.floor(totalMinutes / 60);
        totalDays = Math.floor(totalHours / 24);

        if (totalSeconds === 0)
            return text.justNow;
        if (totalSeconds === 1)
            return text.second;
        if (totalSeconds < 45)
            return String.format(text.seconds, totalSeconds);
        if (totalSeconds < 90)
            return text.minute;
        if (totalMinutes < 45)
            return String.format(text.minutes, totalMinutes);
        if (totalMinutes < 90)
            return text.hour;
        if (totalHours < 24)
            return String.format(text.hours, totalHours);
        if (totalHours < 36)
            return text.day;
        if (totalDays < 30)
            return String.format(text.days, totalDays);
        if (totalDays < 50) // This is not very accurate..
            return text.month;
        if (totalDays < 345)
            return String.format(text.months, Math.floor(totalDays / 30));
        if (totalDays < 500)
            return text.year;

        return String.format(text.years, Math.floor(totalDays / 365));

    }

    // Converts Date object to string using with date & time using short format
    // PARAMS
    // dateTime: Date object
    function formatShortDateTime(dateTime) {
        if (dateTime && dateTime instanceof Date) {
            return dateTime.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + " " +
                   dateTime.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortTimePattern);
        } else {
            throw "dateTime argument has to be Date object.";
        }
    }

    // Augment the existing DateHelper object
    helper.utcToTimeAgo = formatFromUtc;
    helper.localToTimeAgo = formatFromLocal;
    helper.formatShortDateTime = formatShortDateTime;
    helper.getPerfStackTimeFrame = GetPerfStackTimeFrame;

}(SW.Core.DateHelper));