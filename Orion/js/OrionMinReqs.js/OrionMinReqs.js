var SW = window.SW = window.SW || {}; SW.Core = SW.Core || {};
(function(Core) {

// store only javascript infrastrucutre init items.
// after modifying, load this file by itself in a blank page to ensure no errors.

    var _String = Core.String = Core.String || {},
        Cookie = Core.Cookie = Core.Cookie || {},
        Loader = Core.Loader = Core.Loader || {},
        Widgets = Core.Widgets = Core.Widgets || {},
        _Date = Core.Date = Core.Date || {},
        Services = Core.Services = Core.Services || {};

/*
 * Method for creating custom namespace
 * Parameters: 
 *  @namespaceString {String} full name of namespace to created.
 * Example: var myNS = SW.Core.namespace('SW.Core.Reporting.Charts');
 */
    Core.namespace = function(namespaceString) {

        var parts = namespaceString.split('.'),
            parent = window,
            currentPart = '';

        for (var i = 0, length = parts.length; i < length; i++) {
            currentPart = parts[i];
            parent[currentPart] = parent[currentPart] || {};
            parent = parent[currentPart];
        }

        return parent;
    };

    _Date._init = function(testing, serverOffset) {
        //TODO: FB 177935 - implement body
        if (testing != 0) {
            _Date.ConvertToDisplayDate = function(date) {
                var rv = new Date(date.getTime());
                rv.setFullYear(rv.getFullYear() + testing);
                return rv;
            }

            _Date.ConvertFromDisplayDate = function(date) {
                var rv = new Date(date.getTime());
                rv.setFullYear(rv.getFullYear() - testing);
                return rv;
            }
        } else {
            _Date.ConvertToDisplayDate = function(date) { return date; }
            _Date.ConvertFromDisplayDate = function(date) { return date; }
        }

        if (typeof(serverOffset) != "undefined") {
            _Date.ServerTimeOffset = serverOffset;
        }

        _Date.ToServerTime = function(date) {
            date.setTime(date.getTime() + _Date.ServerTimeOffset);
            return date;
        };
    }

    _Date._init(0); // initial configuration.

// -- framework conflicts

    _String.Format = function() {
        var rx = /\{\{|\}\}|\{\s*([0-9]+)\s*\}/g, args = arguments || [], s = args[0] || '';
        return s.replace(rx, function(str, ord) {
            return (str == '{{' || str == '}}') ? '' + str[0] : args[parseInt(ord, 10) + 1];
        });
    };

// Some pages (specifically the Login.aspx page do not include a ScriptManager control which means MicrosoftAjax.js is not included.  
// This means String.format is not defined.  We'll define it here just to make sure we can use String.format everywhere
    if (String.format === undefined) String.format = _String.Format;

    _String.HtmlEncode = function(s) {
        return (s || '').replace(/[<>"'&]/g, function(str) {
            switch (str) {
            case '<':
                return '&lt;';
            case '>':
                return '&gt;';
            case '&':
                return '&amp;';
            case '"':
                return '&#34;';
            default:
                return '&#39;'; // '
            }
        });
    };

// -- specific services

    var _lastid = 1;
    Core.Id = function(model, prefix) {
        prefix = prefix || "swgenid-";
        model = model || {};
        model.id = model.id || (prefix + (_lastid++));
        return model.id;
    };

// -- loader

    var _extloaded = false;

    Loader._cbLoaded = function(framework, opts) {
        if (framework == 'msajax') {
            if (_extloaded) {
                // FB56491: override format function that comes from ScriptManager by Ext format.
                // why here? so we have first slot to queue this fix.
                $(function() { if (Date.prototype.dateFormat) Date.prototype.format = Date.prototype.dateFormat; });
            }
        };

        if (framework == 'extjs') {
            _extloaded = true;
        };
    };

// -- buttons (core must render)

    var btn_cls = { resource: 'sw-btn-resource', small: 'sw-btn-small', primary: 'sw-btn-primary', secondary: '' };

    Widgets.Button = function(text, options) {
        var options = options || {},
            type = btn_cls[options.type] || '',
            markup = '<a class="{0}" id="{1}" automation="{2}" href="{3}"{4}><span class="sw-btn-c"><span class="sw-btn-t">{2}</span></span></a>',
            id = options.id || Core.Id(),
            href = options.href || 'javascript:void(0);',
            cls = options.cls || '',
            target = options.target ? _String.Format(' target="{0}"', options.target) : '';

        return _String.Format(markup, ' sw-btn ' + type + ' ' + cls, id, _String.HtmlEncode(text), _String.HtmlEncode(href), target);
    };

// Create watermark text for a given input. 
// Parameter:  
//  textbox -- jQuery selector of textbox
//  text -- text which will be show as watermark
// Example: new SW.Core.Widgets.WatermarkTextbox('#myTextboxId', 'Search ....'); 
    Widgets.WatermarkTextbox = function(textbox, text) {
        var $textbox = $(textbox);

        $textbox
            .click(textboxClickHandler)
            .focus(textboxClickHandler)
            .keyup(textboxClickHandler)
            .blur(textboxBlurHandler);

        textboxBlurHandler();

        function textboxClickHandler() {
            var val = $textbox.val();
            if (val === text) {
                $textbox.val('').css('color', 'black');
            }
        }

        function textboxBlurHandler() {
            var val = $textbox.val();
            if (val === '') {
                $textbox.val(text).css('color', '#888888');
            }
        }

    };
    
    // Simple menu
    // Parameter: config
    //    - menuButtonId: string - ID of menu button
    //    - menuItems: Array of { onClickHandler: function(), title: string }
    
    Widgets.SimpleMenu = function (config) {
        var $menuPlaceholder;
        var $menuButton;
        var $menu;

        this.init = function () {
            $menuPlaceholder = $('<div class="sw-simplemenu-placeholder sw-simplemenu-placeholder-bottom">');
            $menu = $('<div class="sw-simplemenu-items">');

            if (!$.isEmptyObject(config.menuItems)) {
                var itemCount = 0;
                for (var i = 0; i < config.menuItems.length; i++) {
                    var item = config.menuItems[i];

                    if (!item.id)
                        item.id = "simpleMenuItem" + itemCount++;

                    var $rowDiv = $(String.format('<div class="sw-simplemenu-item" data-form="{0}">{1}</div>', item.id, item.title));
                    $rowDiv.click(self.hide);
                    if (!$.isEmptyObject(item.onClickHandler)) {
                        $rowDiv.click(item.onClickHandler);
                    }

                    $menu.append($rowDiv);
                }
            }
            $menuPlaceholder.append($menu);

            $menuButton = $('#' + config.menuButtonId);
            $menuButton.prepend($menuPlaceholder);
        };

        this.show = function () {
            $menuPlaceholder.css('display', 'inline-block');

            // to hide menu once clicked outside of the menu
            $('body').on('click', lostFocusHandler);
        };

        this.hide = function () {
            $menuPlaceholder.css('display', 'none');

            $('body').off('click', lostFocusHandler);
        };

        this.toggle = function () {
            $menuPlaceholder.css('display') === 'none' ? self.show() : self.hide();
        };

        var lostFocusHandler = function (e) {
            if ($(e.target).parents('.sw-simplemenu-placeholder').length == 0
                && $(e.target).parents('#' + config.menuButtonId).length == 0) {
                self.hide();
            }
        };

        // constructor
        var self = this;
    };


// -- cookies

    window.setCookie = // legacy
        Cookie.Set = function(cookieName, cookieValue, periodType, offset) {
            var expDate = new Date();
            offset = offset / 1;

            var type = periodType;
            switch (type.toLowerCase()) {
            case 'years':
                var year = expDate.getYear();
                expDate.setYear(year + offset);
                break;
            case 'months':
                expDate.setMonth(expDate.getMonth() + offset);
                break;
            case 'days':
                expDate.setDate(expDate.getDate() + offset);
                break;
            case 'hours':
                expDate.setHours(expDate.getHours() + offset);
                break;
            case 'minutes':
                expDate.setMinutes(expDate.getMinutes() + offset);
                break;
            default:
                expDate.setDate(expDate.getDate() + offset);
                break;
            }

            document.cookie = escape(cookieName) + '=' + escape(cookieValue) + '; expires=' + expDate.toGMTString() + '; path=/';
        };

    window.checkCookie = // legacy
        Cookie.Check = function() {
            document.cookie = 'test=true';
            if (getCookie('test') == 'true')
                return true;
            else
                return false;
        };

    window.getCookie = // legacy
        Cookie.Get = function(cookieName) {
            var exp = new RegExp(escape(cookieName) + '=([^;]+)');
            if (exp.test(document.cookie + ';')) {
                exp.exec(document.cookie + ';');
                return unescape(RegExp.$1);
            } else return false;
        };

    var isSessionExpired = false;
    Services.handleError = function(xhr, customErrorHandler) {
        if (xhr.status == 401 || xhr.status == 403) {
            // login cookie expired. reload the page so we get bounced to the login page.
            if (!isSessionExpired) {
                isSessionExpired = true;
                alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
                window.location.reload();
            }
           
        }
        var errorText = xhr.responseText;
        var msg;
        try {
            var error = eval("(" + errorText + ")");
            msg = error.Message;
            $("#stackTrace").text(error.StackTrace);
        } catch (err) {
            msg = err.description;
        }
        $("#test").text(msg);
        if (typeof(customErrorHandler) == 'function') {
            customErrorHandler(msg);
        }
    };

    Services.callWebService = function(serviceName, methodName, param, onSuccess, onError) {
        var paramString = JSON.stringify(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: serviceName + "/" + methodName,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(msg) {
                onSuccess(msg.d);
            },
            error: function(xhr, status, errorThrown) {
                Services.handleError(xhr, onError);
            }
        });
    };

    Services.callWebServiceSync = function(serviceName, methodName, param, onSuccess, onError) {
        var paramString = JSON.stringify(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            async: false,
            url: serviceName + "/" + methodName,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(msg) {
                onSuccess(msg.d);
            },
            error: function(xhr, status, errorThrown) {
                Services.handleError(xhr, onError);
            }
        });
    };

    Services.callControllerAction = function(route, actionName, param, onSuccess, onError) {
        if (route.indexOf('/', route.length - 1) === -1) {
            route = route + '/';
        }
        Services.callController(route + actionName, param, onSuccess, onError);
    };

    Services.callController = function(route, param, onSuccess, onError, requestType) {
        var paramString = JSON.stringify(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: requestType || "POST",
            url: route,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: { "ViewLimitationID" : (Core.View && Core.View.LimitationID) ? Core.View.LimitationID : 0 },
            success: function(msg) {
                onSuccess(msg);
            },
            error: function(xhr, status, errorThrown) {
                Services.handleError(xhr, onError);
            }
        });
    };

    Services.callControllerActionSync = function (route, actionName, param, onSuccess, onError) {
        if (route.indexOf('/', route.length - 1) === -1) {
            route = route + '/';
        }
        Services.callControllerSync(route + actionName, param, onSuccess, onError);
    };

    Services.callControllerSync = function(route, param, onSuccess, onError) {
        var paramString = JSON.stringify(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: route,
            async: false,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: { "ViewLimitationID" : (Core.View && Core.View.LimitationID) ? Core.View.LimitationID : 0 },
            success: function(msg) {
                onSuccess(msg);
            },
            error: function(xhr) {
                Services.handleError(xhr, onError);
            }
        });
    };

    Services.generateNewGuid = function() {
        var guid;
        Core.Services.callWebServiceSync('/Orion/Services/WebAdmin.asmx', 'GetNewGuid', {}, function(res) {
            guid = res;
            return;
        });
        return guid;
    };

    Services.postToTarget = function(targetPage, data, stringifyParams, newPage) {
        var formTag = String.format('<form method="post" action="{0}" {1}></form>', targetPage, newPage === true ? "target='_blank'" : "");
        var form = $(formTag);
        for (var prop in data) {
            var inputTag = String.format('<input type="hidden" name="{0}" id="{0}" />', prop);
            var jsonData = (stringifyParams === true) ? JSON.stringify(data[prop]) : data[prop];
            form.append($(inputTag).val(jsonData));
        }

        $(form).appendTo('body').submit();
    };

    Services.downloadFileAsync = function (targetPage, dataToPost, fileName) {
        $.ajax({
            type: "POST",
            url: targetPage,
            data: dataToPost,
            contentType: "application/json; charset=utf-8",
            headers: { "ViewLimitationID": (Core.View && Core.View.LimitationID) ? Core.View.LimitationID : 0 },
            error: function (xhr) {
                Services.handleError(xhr, onError);
            }
        }).done(function (data) {
            var link = document.createElement('a');
            var blob = new Blob([data], { type: 'text/plain' });
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
        });
    }
    
    Services.getXsrfTokenValue = function() {
        return $("#@{R=Live.Web;K=Core.AntiXsrfTokenInputKey}").val();
    };

/*
 * Object implements observer (publisher/subsriber) pattern. 
 * Custom object can inherit it for support publishing events.  
 * Example: 
 
    // Custom object constructor
    var Person = function(name){
        var this = self;
        
        // Inherit Observer
        SW.Core.Observer.makeObserver(self);
    
        // Raise event on any action
        $('#btnEdit').click(function() {

            // Publish event with custom key and arguments.
            var customData = { a: 'a', b: 'b'};
            self.trigger('person.edit', customData);
        });
    }
    
    // Create custom object instance and subscribe to event.
    var myPerson = new Person('Zdenek');
    myPerson.on('person.edit', function (customData) { alert('zdenek edited ' + customData.a); });
 */
    Core.Observer = {
        on: function(eventName, callback, context) {
            /// <summary>Subscribe/bind to event.</summary>
            /// <param name="eventName" type="String">The name of the event to subscribe.</param>
            /// <param name="callback" type="Function">The function to be called when the event is published.</param>
            /// <param name="context" type="Object" optional="true">The context to execute the callback. By default it is ecexuted in global context.</param>

            // Create _callbacks object, unless it already exists.
            var calls = this._callbacks || (this._callbacks = {});

            // Create or use an array for the given event name.
            var eventCalls = calls[eventName] || (calls[eventName] = []);

            eventCalls.push({
                callback: callback,
                context: context
            });
        },

        off: function(eventName, callback) {
            /// <summary>Unsubscribe/unbind from event.</summary>
            /// <param name="eventName" type="String">The name of the event to unsubscribe.</param>
            /// <param name="callback" type="Function">The reference to function which should be unsubscribe. Does not support anonymous functions.</param>


            // Return if there is not any callbacks for event.
            var calls = this._callbacks;
            if (!calls || !calls[eventName]) {
                return;
            }

            // Invoke the event callbacks
            var eventCallbacks = calls[eventName];

            for (var i = 0, max = eventCallbacks.length; i < max; ++i) {
                if (eventCallbacks[i].callback === callback) {
                    eventCallbacks.splice(i, 1);
                }
            }
        },

        trigger: function(eventName, data) {
            /// <summary>Publish/trigger event.</summary>
            /// <param name="eventName" type="String">The name of the event to publish.</param>
            /// <param name="data" type="Object">Any data to be passed to the event handler. Custom to the event.</param>

            // Used Array method for transform arguments object into a real array
            var args = Array.prototype.slice.call(arguments, 0);

            // Extract the first argument (eventName)
            var eventName = args.shift();

            // Return if there is not any callbacks for event
            var calls = this._callbacks;
            if (!calls || !calls[eventName]) {
                return;
            }

            // Invoke the event callbacks
            var eventCallbacks = calls[eventName];

            var asyncCallback = function(callbackInfo) {
                return function() {
                    var context = callbackInfo.context || this;
                    callbackInfo.callback.apply(context, args);
                };
            };

            for (var i = 0, max = eventCallbacks.length; i < max; ++i) {
                setTimeout(asyncCallback(eventCallbacks[i]), 0);
            }

        },

        makeObserver: function(obj) {
            /// <summary>Makes from a given object observer.</summary>
            /// <param name="obj" type="Object">A object which will be used as observer.</param>

            for (var prop in Core.Observer) {
                if (Core.Observer.hasOwnProperty(prop) && typeof Core.Observer[prop] === "function") {
                    obj[prop] = Core.Observer[prop];
                }
            }
        },

        makeCancellableObserver: function(obj) {
            /// <summary>Makes from a given object observer, where observers can stop processing based on order of registration (return false).</summary>
            /// <param name="obj" type="Object">A object which will be used as observer or one will be created.</param>

            var callbacks = {};
            var objref = obj || {};

            if (objref.on || objref.off || objref.trigger) return objref;

            objref.on = function (name, fn, ctx) {
                var cbs = callbacks[name], ca = cbs || (callbacks[name] = []);
                for (var i = 0, imax = ca.length; i < imax; ++i) {
                    if (ca[i].fn === fn) return objref;
                }
                ca.push({ fn: fn, ctx: ctx });
                return objref;
            };

            objref.off = function (name, fn) {
                var ca = callbacks[name];
                if (!ca) return objref;
                for (var i = 0, imax = ca.length; i < imax; ++i) {
                    if (ca[i].fn === fn) {
                        ca.splice(i, 1);
                        return objref;
                    }
                }
                return objref;
            };

            objref.trigger = function (name) {
                var args = Array.prototype.slice.call(arguments, 1), ca = callbacks[name], cb, r, ret;
                if (!ca) return;
                for (var i = 0, imax = ca.length; i < imax; ++i) {
                    if (!(cb = ca[i].fn)) continue;
                    r = cb.apply(ca[i].context || this, args);
                    if (jQuery.isPlainObject(r)) {
                        ret = r.result === undefined ? r : r.result;
                        if (r.halt) return ret;
                    }
                    else ret = r === undefined ? ret : r;
                }
                return ret;
            };

            return objref;
        }
    };


/*
Helper for Adding/Updating/Modifing querystring params from javascript.
ex.
// add/update parameters
navigateWithURIParams({ foo: 'bar', boz: 42 });

// remove parameter
navigateWithURIParams({ foo: null });

// submit the given form by adding/replacing URI parameters (with jQuery)
$('.filter-form').submit(function(e) {
  e.preventDefault();
  navigateWithURIParams(decodeURIParams($(this).serialize()));
});
*/
    Core.UriHelper = {
        decodeURIParams: function(query) {
            ///<summary>Get a dictionary of querystring params based on the query or current url</summary>
            ///<param name="query" type="String">The </param>

            if (query == null)
                query = window.location.search;
            if (query[0] == '?')
                query = query.substring(1);

            var params = query.split('&');
            var result = {};
            for (var i = 0; i < params.length; i++) {
                var param = params[i];
                var pos = param.indexOf('=');
                if (pos >= 0) {
                    var key = decodeURIComponent(param.substring(0, pos));
                    var val = decodeURIComponent(param.substring(pos + 1));
                    result[key] = val;
                } else {
                    var key = decodeURIComponent(param);
                    result[key] = true;
                }
            }
            return result;
        },

        encodeURIParams: function(params, addQuestionMark) {
            var pairs = [];
            for (var key in params)
                if (params.hasOwnProperty(key)) {
                    var value = params[key];
                    if (value != null) /* matches null and undefined */ {
                        pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
                    }
                }
            if (pairs.length == 0)
                return '';
            return (addQuestionMark ? '?' : '') + pairs.join('&');
        },

        navigateWithURIParams: function(newParams) {
            window.location.search = this.encodeURIParams($.extend(this.decodeURIParams(), newParams), true);
        }
    };

// Helper class to java script Date object operations
    Core.DateHelper = {
        // converts local date to UTC time zone i.e. adds the time zone offset
        localToUtc: function(localDate) {
            var offset = localDate.getTimezoneOffset() * 60 * 1000
            return new Date(localDate.getTime() + offset);
        },

        // converts UTC date to local time zone i.e. substracts the time zone offset
        utcToLocal: function(utcDate) {
            var offset = utcDate.getTimezoneOffset() * 60 * 1000
            return new Date(utcDate - offset);
        }
    };

    Core.KnowledgebaseHelper = {
        getKBUrl: function(kbNumber, kbLanguage) {
            return String.format('@{R=Live.Web;K=Core.KBLinkTemplate}', kbLanguage, kbNumber);
        },
        getKBUrlWithLang: function(kbNumber) {
            return String.format('@{R=Live.Web;K=Core.KBLinkTemplate}', '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}', kbNumber);
        }
    };

    Core.HelpDocumentationHelper = {
        getHelpLink: function (linkName) {
            return String.format('@{R=Live.Web;K=Core.HelpLoaderLinkTemplate}', linkName);
        }
    };

// -- global JS patches for shipped modules

window.APM = window.APM || {}; // FB80856. obsolete: remove when APM 4.3 is min version.

// testing only - demo will override this method to provide full functionality
window.demoAction = function() {
        alert('@{R=Core.Strings;K=WEBJS_JP1_1;E=js}');
};

})(SW.Core);