(function (Core) {
    var xsrfCookieName = 'XSRF-TOKEN';
    var xsrfHeaderName = 'X-XSRF-TOKEN';
    if (!Core.Cookie.Get(xsrfCookieName)) return;

    var originalCall = Core.Loader._cbLoaded;
    Core.Loader._cbLoaded = function (framework, opts) {

        if (framework == 'msajax') {
            Sys.Net.WebRequestManager.add_invokingRequest(function (sender, networkRequestEventArgs) {
                var request = networkRequestEventArgs.get_webRequest();
                var headers = request.get_headers();
                headers[xsrfHeaderName] = Core.Cookie.Get(xsrfCookieName);
            });
        }

        if (framework == 'jquery') {
            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    xhr.setRequestHeader(xsrfHeaderName, Core.Cookie.Get(xsrfCookieName));
                }
            });
        }
		
		var extendExtHeader = function(extObj) {
            extObj.Ajax.defaultHeaders = extObj.Ajax.defaultHeaders || {};
	        extObj.Ajax.on("beforerequest", function(){
				extObj.Ajax.defaultHeaders[xsrfHeaderName] = Core.Cookie.Get(xsrfCookieName);
			});
		}

        if (framework == 'extjs') {
            if (typeof (Ext) !== 'undefined') {
                extendExtHeader(Ext);
            }
            if (typeof (Ext4) !== 'undefined') {
                extendExtHeader(Ext4);
            }
			if (typeof (Ext42) !== 'undefined') {
                extendExtHeader(Ext42);
            }
        }

        originalCall(framework, opts);
    };

})(SW.Core);
