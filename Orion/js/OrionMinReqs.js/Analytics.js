﻿var _gaq = _gaq || [];

(function (Core) {

    // wrapper for the Google Analytics code, in case that user didn't opt in to SolarWinds Improvement Program this code will do nothing
    // because the ga command queueing method will not be defined (so the value will be dumped to empty array)

    var Analytics = Core.Analytics = Core.Analytics || {};

    var debug = false;

    Analytics.TrackEvent = function (category, action, opt_label, opt_value, opt_noninteraction) {
        
        if (debug) {
            ga = function () { window.console && console.log(arguments); };
        }

        try {
            ga('send', 'event', {
                eventCategory: category,
                eventAction: action,
                eventLabel: opt_label,
                eventValue: opt_value,
                nonInteraction: opt_noninteraction || true}
            );
            
        } catch (e) { } 
    };
    //only sets the value on tracker, will be sent to GA any time a send method is called (pageview, event,...) before page unload
    //function signature kept but name and scope has to be defined in a custom dimension/metric in GA Admin screen before usage
    Analytics.TrackValue = function(index, name, value, opt_scope) {

        if (debug) {
            ga = function () { window.console && console.log(arguments); };
        }

        try {
            ga('set', 'dimension'+index, value);
        } catch (e) { }
    };
})(SW.Core);
