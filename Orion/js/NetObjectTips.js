/*eslint-env browser, jquery*/
/*globals SW*/
(function (Core) {

    var NetObjectTips = Core.NetObjectTips = Core.NetObjectTips || {};

    // orphan tracking

    var actives = [];

    function addActive(control) {
        if (!control) return;
        for (var i = 0, imax = actives.length; i < imax; ++i) {
            if (actives[i] === control) {
                return;
            }
        }
        actives.push(control);
    }

    function removeActive(control) {
        if (!control) return;
        for (var i = 0, imax = actives.length; i < imax; ++i) {
            if (actives[i] === control) {
                actives.splice(i, 1);
                return;
            }
        }
    }

    NetObjectTips.ActiveHovers = function() {
        return actives.slice(0);
    };

    //

    var markProcessed = {
        propName: "tooltip",
        propValue: "processed"
    };

    NetObjectTips.HasHover = function (control) {
        return $(control).prop(markProcessed.propName) === markProcessed.propValue;
    };

    function triggerPopover($target, netobject) {
        $target
            .attr("netobject", netobject)
            .trigger("mouseover");
    }

    NetObjectTips.ForceHover = function (controlOrSelector, opts) {
        var ctrl = $(controlOrSelector);

        if (!ctrl.length) {
            return false;
        }

        if (opts) {
            var cache = ctrl.data();
            if (opts.locationData) {
                cache.locationData = opts.locationData;
            }
            if (opts.netobject) {
                cache.netobject = /([a-z-]+)(:|%3a)(([^&]|%20)+)/i.exec(opts.netobject);
            } else if (opts.url) {
                cache.url = opts.url;
            } else {
                return false;
            }
        }

        $.swtooltip(ctrl[0])
            .pipe(function (processed) {
                if (processed) {
                    ctrl
                        .prop(markProcessed.propName, markProcessed.propValue)
                        .trigger("mouseover");
                } else {
                    // hack for triggering apollo popovers manually
                    var angular = window.angular;
                    if (angular) {
                        cache.angular = angular.element(ctrl[0]);
                        triggerPopover(cache.angular, opts.netobject);
                    }
                }
            });

        return true;
    };

    NetObjectTips.CancelHover = function (controlOrSelector) {
        var ctrl = $(controlOrSelector);
        if (!ctrl.length) {
            return;
        }

        var control = ctrl[0];
        var cache = ctrl.data();
        cache.netobject = null;
        cache.url = null;
        cache.locationData = null;

        if (cache.angular) {
            cache.angular.trigger("mouseleave");
            cache.angular = null;
        }

        var mout = control.onmouseout;
        if (mout && actives[actives.length - 1] === control) {
            mout();
        }

        control.onmouseout = null;
        control.onmouseover = null;
        control.tooltip = "";
    };

    function parseDict(dict) {
        var map = {};
        for (var i = 0; i < dict.length; ++i) {
            map[dict[i].Key] = dict[i].Value;
        }
        return map;
    }

    NetObjectTips.Init = function (config) {

        var ViewLimitationID = config.ViewLimitationID;
        var NetObjectTypeToTipPagePath = parseDict(config.NetObjectTypeToTipPagePathRaw);

        $(document).mousemove(function (e) {
            if ($.browser.msie && e.clientX != null) {
                var html = document.documentElement;
                var body = document.body;
                e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
                e.pageX -= html.clientLeft || 0;
                e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0);
                e.pageY -= html.clientTop || 0;
            }
            cursor_position.x = e.pageX;
            cursor_position.y = e.pageY;
        });

        var $tooltip = $("\
            <div class='tooltip-outer'>\
                <div class='tooltip-wrapper'>\
                    <div class='tooltip-shadow' style='left: 1px; top: 1px;'></div>\
                    <div class='tooltip-shadow' style='left: 2px; top: 2px;'></div>\
                    <div class='tooltip-shadow' style='left: 3px; top: 3px;'></div>\
                    <div class='tooltip-shadow' style='left: 4px; top: 4px;'></div>\
                    <div class='tooltip-shadow' style='left: 5px; top: 5px;'></div>\
                    <div class='tooltip-shadow' style='left: 6px; top: 6px;'></div>\
                    <div class='cluetip-orion'>\
                        <table class='cluetip-orion-table' cellpadding='0' cellspacing='0'>\
                            <tr>\
                                <td class='cluetip-orion-inner-content' style=''></td>\
                            </tr>\
                        </table>\
                    </div>\
                </div>\
                <div class='tooltip-stem'></div>\
            </div>\
        ");

        var $tooltip_inner = $("td.cluetip-orion-inner-content", $tooltip);
        var $tooltip_stem = $("div.tooltip-stem", $tooltip);
        var $tooltip_wrapper = $("div.tooltip-wrapper", $tooltip);
        var $tooltip_shadow = $("div.tooltip-shadow", $tooltip).css({ opacity: 0.1 });
        var $tooltip_divs = $tooltip.children().hide();
        var $tooltip_timer;
        var $tooltip_request;
        var $iframe = $("<iframe class='tooltip-iframe' scrolling='no' frameborder='0'></iframe>");

        var is_tooltip_available = false;
        var cursor_position = { x: 0, y: 0 };

        var rxGetId = /netobject=([a-z0-9-]+)(:|%3a)(([^&]|%20)+)/i;

        var self = this;

        var getObjectsToExcludeDeferred;

        function getObjectsToExclude(control) {
            if ($(control).closest(".forceLegacyTooltips").length > 0) {
                return $.Deferred().resolve([]).promise();
            } 
            if (!getObjectsToExcludeDeferred) {
                getObjectsToExcludeDeferred = $.Deferred();
                SW.Core.Services.callController(
                    "/api2/tooltip/types/enabled/",
                    null,
                    function (response) {
                        getObjectsToExcludeDeferred.resolve(response);
                    },
                    function () {
                        getObjectsToExcludeDeferred.resolve([]);
                    },
                    "GET"
                );
            }
            return getObjectsToExcludeDeferred.promise();
        }

        $.swtooltip = function (control, tipText) {
            return getObjectsToExclude(control)
                .pipe(function (objectsToExclude) {
                    var tooltipHelper = new $tooltipHelper(control, tipText);
                    var id = tooltipHelper.netobject();

                    if (id != null && $.inArray(id[1], objectsToExclude) > -1) {
                        // legacy tooltip ignored
                        return false;
                    }

                    var $control = $(control);

                    control.onmouseover = function () {
                        $tooltip_timer = setTimeout(function () {
                            if ($tooltip_request) {
                                $tooltip_request.abort();
                                $tooltip_request = null;
                            }
                            if (!$control.swtooltip) {
                                $control.swtooltip = new $tooltipHelper(control, tipText);
                            }
                            $control.swtooltip.show();
                        }, 200);
                    };

                    control.onmouseout = function () {
                        if ($tooltip_timer) {
                            clearTimeout($tooltip_timer);
                            $tooltip_timer = null;
                        }
                        if ($tooltip_request) {
                            $tooltip_request.abort();
                            $tooltip_request = null;
                        }
                        
                        if (!$control.swtooltip) {
                            $control.swtooltip = new $tooltipHelper(control, tipText);
                        }
                        $control.swtooltip.hide();
                    };

                    // legacy tooltip processed
                    return true;
                });
        };

        function $showErrorTooltip() {
            $tooltip_inner.html(String.format(
                "<table style='width:270px;'><tr><td style='height:40px;vertical-align:middle;'><i>{0}</i><td><tr><table>",
                "@{R=Core.Strings;K=WEBJS_VB1_4; E=js}"
            ));
            $tooltip_wrapper.css("top", 0);
        }

        function $appendLocationName(locationData) {
            if (locationData) {
                var $tipTitle = $("h3:first-child", $tooltip_inner);
                $tipTitle.text(String.format(locationData.Text, $tipTitle.text(), locationData.Name));
            }
        }

        function $tooltipHelper(control, tipText) {
            this.control = control;
            this.tipText = tipText;
            this.cache = $(control).data() || {};

            if (tipText || this.url()) {
                $("img[alt]", control).removeAttr("alt");
                if (!is_tooltip_available) {
                    is_tooltip_available = true;
                    $("body").append($tooltip);
                }
            }
        }

        $.extend($tooltipHelper.prototype, {
            static_cache: {},

            hide: function () {
                $tooltip.hide();
                $tooltip_divs.hide();
                removeActive(this.control);
            },

            show: function () {
                if (this.tipText) {
                    $tooltip_inner.html(this.tipText);
                    this.adjustPosition();
                }
                else {
                    if (this.ajaxSettings().url != null) {
                        $tooltip_request = $.ajax(this.ajaxSettings());
                    }
                }
                addActive(this.control);
            },

            adjustPosition: function () {
                $tooltip.removeClass("stem-bottom-top stem-top-right stem-bottom-left stem-bottom-right");
                $tooltip_divs.show();
                $tooltip.show();
                var v = viewport();
                var p = position($tooltip_wrapper);
                var r = (v.x + v.cx < p.x + $tooltip_inner.width());
                var b = (v.y + v.cy < p.y + $tooltip_inner.height());

                if (b && r) {
                    $tooltip.addClass("stem-bottom-right");
                    $tooltip.css("top", p.y - 20);
                    $tooltip.css("left", p.x - $tooltip_inner.width() - 40);
                    $tooltip_stem.css("left", $tooltip_inner.width() + 1);
                }
                else if (b) {
                    $tooltip.addClass("stem-bottom-left");
                    $tooltip.css("top", p.y - 20);
                    $tooltip.css("left", p.x + 15);
                    $tooltip_stem.css("left", 0);
                }
                else if (r) {
                    $tooltip.addClass("stem-top-right");
                    $tooltip.css("top", p.y);
                    $tooltip.css("left", p.x - $tooltip_inner.width() - 40);
                    $tooltip_stem.css("left", $tooltip_inner.width() + 1);
                }
                else {
                    $tooltip.addClass("stem-top-left");
                    $tooltip.css("top", p.y);
                    $tooltip.css("left", p.x + 15);
                    $tooltip_stem.css("left", 0);
                }

                if ((p.cy + p.y) < $tooltip_stem.height()) {
                    $tooltip_wrapper.css("top", "0");
                }

                $tooltip_shadow.css({
                    width: $tooltip_inner.width(),
                    height: $tooltip_inner.height()
                });

            },

            ajaxSettings: function () {
                var _this = this;

                return $.extend({
                    url: this.url(),
                    complete: function () {
                        if (self.canDisplayTooltip) {
                            $appendLocationName(_this.getlocationData());
                            _this.adjustPosition();
                        }
                    },
                    hide: function () {
                        _this.hide();
                    }
                }, this.ajaxSettingsBase);
            },

            ajaxSettingsBase: {
                dataType: "html",
                cache: false,
                error: function () {
                    $tooltip_request = null;
                    $showErrorTooltip();
                },
                success: function (data) {
                    $tooltip_wrapper[0].style.top = "";
                    $tooltip_request = null;
                    self.canDisplayTooltip = true;
                    // check if data is net object tooltip
                    if (data.indexOf("NetObjectTipBody") > -1) {
                        data = data
                            .replace(/<s(cript|tyle)(.|\s)*?\/s(cript|tyle)>/g, "")
                            .replace(/<(link|title)(.|\s)*?\/(link|title)>/g, "");
                        $tooltip_inner.html(data);
                    }
                    else if (data == "NoTip") {
                        this.hide();
                        self.canDisplayTooltip = false;
                        this.complete = function () { };
                    }
                    else {
                        $showErrorTooltip();
                    }
                }
            },

            width: function () {
                return this.static_cache.width || (this.static_cache.width = this.getWidth());
            },

            getWidth: function () {
                var left = 0;
                var right = 0;

                $tooltip.children().each(function () {
                    var p = position($(this));

                    if (p.x < left)
                        left = p.x;

                    if (p.x + p.cx > right)
                        right = p.x + p.cx;
                });

                return right - left;
            },

            netobject: function () {
                return this.cache.netobject || (this.cache.netobject = this.getNetobject());
            },

            getlocationData: function () {
                return this.cache.locationData;
            },

            getNetobject: function () {
                return rxGetId.exec(this.control.href);
            },

            url: function () {
                return this.cache.url || (this.cache.url = this.getUrl());
            },

            getUrl: function () {
                var id = this.netobject();
                var tipURL = id && NetObjectTypeToTipPagePath[id[1]];
                var forceTip = $(this.control).hasClass("ForceTip");

                if (!tipURL) {
                    return null;
                }

                if (!forceTip) {
                    if (this.currentPageId && (id[1] == this.currentPageId[1]) && (id[3] == this.currentPageId[3])) {
                        return null;
                    }
                }

                tipURL += ((tipURL.indexOf("?") < 0) ? "?" : "&") + "NetObject=" + id[1] + ":" + id[3];
                if (ViewLimitationID) {
                    tipURL += "&viewlim=" + ViewLimitationID;
                }

                return tipURL;
            },

            currentPageId: rxGetId.exec(document.location)
        });

        function viewport() {
            var $window = $(window);
            return {
                x: $window.scrollLeft(),
                y: $window.scrollTop(),
                cx: $window.width(),
                cy: $window.height()
            };
        }

        function position(control) {
            return {
                x: cursor_position.x,
                y: cursor_position.y,
                cx: control.width,
                cy: control.heigth
            };
        }

        // do not show tooltips if the link has a class "NoTip" and if it has not been processed yet
        var selectorToProcess = "a[" + markProcessed.propName + "!='" + markProcessed.propValue + "'][href*='NetObject=']:not([href*='NoTip']):not(.NoTip)";
        $(selectorToProcess)
            .livequery(function () {
                $(this).attr(markProcessed.propName, markProcessed.propValue);
                $.swtooltip(this);
            });
    };

})(SW.Core);
