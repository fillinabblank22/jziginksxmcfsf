﻿//The model prototype, we simply recreate the Oid class from the server side.
Ext42.define("Oid",
    {
        extend: 'Ext.data.Model',
        fields: [
            { name: 'description', type: 'string' },
            { name: 'id', type: 'string' },
            { name: 'text', type: 'string' },
            { name: 'mib', type: 'string' },
            { name: 'mibPath', type: 'string' },
            { name: 'accessType', type: 'string' },
            { name: 'status', type: 'string' },
            { name: 'variableType', type: 'string' },
            { name: 'stringType', type: 'string' },
            { name: 'isTabular', type: 'string' },
            { name: 'name', type: 'string' },
            { name: 'tableName', type: 'string' },
            { name: 'calculatedOid', type: 'string' },
            { name: 'calculatedName', type: 'string' },
            { name: 'calculatedIsTabular', type: 'string' }
        ]
    });

//The proxy, enhanced with json capabilities.
Ext42.define('Ext.JsonProxy', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'jsonProxy',
    //asmx services play well with JSON only when using POST.
    actionMethods: {
        read: 'POST'
    },
    type: 'ajax',
    //We need to include the content-type header, otherwise we'll get back
    //XML data from the asmx service.
    headers: {
        'Content-Type': 'application/json'
    },
    //The content-type header by itself is not enough, ExtJs will send the POST parameters
    //as application/x-www-form-urlencoded, but the service excepts JSON data, so we need
    //to encode it by overriding this function.
    doRequest: function (operation, callback, scope) {
        var writer = this.getWriter(),
            request = this.buildRequest(operation, callback, scope);

        if (operation.allowWrite()) {
            request = writer.write(request);
        }

        Ext42.apply(request, {
            headers: this.headers,
            timeout: this.timeout,
            scope: this,
            callback: this.createRequestCallback(request, operation, callback, scope),
            method: this.getMethod(request),
            jsonData: this.jsonData,
            disableCaching: false
        });
        Ext42.Ajax.request(request);
        return request;
    },
    reader: {
        type: 'json',
        //asmx services wrap the JSON response in a root object called 'd'.
        root: 'd'
    }
});

//Subclass treePanel to make it easier to define it.
//  if (Ext42.ClassManager.isCreated('SW.widget.OidPicker') === false) {
Ext42.define("SW.widget.OidPicker", {
    extend: 'Ext.tree.Panel',
    alias: 'oidpicker',
    columns: [{
        xtype: 'treecolumn',
        dataIndex: 'text',
        text: '@{R=Core.Strings;K=WEBJS_GK0_02; E=js}',
        width: '100%'
    }],
    useArrows: true,
    singleExpand: false,
    rootVisible: false,
    autoScroll: false,
    border: false,
    viewConfig: {
        componentCls: 'scroll'
    },
    root: {
        text: 'Root',
        id: 1,
        expanded: true,
        nodeType: 'async'
    },
});

//IE fun
function isArray(object) {
    return Object.prototype.toString.call(object) === '[object Array]';
}