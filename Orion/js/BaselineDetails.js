﻿var bls = SW.Core.namespace("SW.Core.Baselines");

bls.showBaselineDetailsDialog = function (id, thresholdName, thresholdDisplayName, operator, returnFunction) {
    /// <signature>
    /// <summary>Displays the dialog</summary>
    /// <param name="config" type="Object">Required. Dialog configuration hash. Refer to SW.Core.Baselines.showBaselineDetailsDialog.defaultConfig 
    /// field for a list of parameters. Supports more options than the other overload. Use to provide custom formatter function for statistics table.</param>
    /// </signature>
    /// <signature>
    /// <summary>Displays the dialog</summary>
    /// <param name="id" type="string">Required. Instance ID of the entity (e.g. Node) to display dialog for.</param>
    /// <param name="thresholdName" type="string">Required. Internal unique name of the threshold data to display e.g. Nodes.Stats.PercentMemoryUsed.</param>
    /// <param name="thresholdDisplayName" type="string">Required. Display name for the threshold data to display to the user e.g. "Response Time".</param>
    /// <param name="operator" type="string">Required. Number value for threshold data operator from ThresholdOperatorEnum e.g. 0. 
    /// Only 0 (Greater), 1 (GreaterOrEqual), 3 (LessOrEqual) and 4 (Less) are supported.</param>
    /// <param name="returnFunction" type="string">Required. Dialog close callback function that recieves success flag, warning and critical levels.</param>
    /// </signature>

    var config;

    //the function has 2 overloads, check which one is being called
    if (arguments.length == 1) {
        config = $.extend(true, {}, bls.showBaselineDetailsDialog.defaultConfig, id);
    } else {
        config = $.extend(true,
            {},
            bls.showBaselineDetailsDialog.defaultConfig, {
                id: id,
                thresholdName: thresholdName,
                thresholdDisplayName: thresholdDisplayName,
                operator: operator,
                returnFunction: returnFunction
            }
        );
    }

    new SW.Core.Baselines.BaselineDetails().showLatestBaselineDetailsDialog(config);
};

bls.showBaselineDetailsDialog.defaultConfig = {
    /// <field type='String'>Required. Instance ID of the entity (e.g. Node) to display dialog for.</field>
    id: '',
    /// <field type='String'>Required. Internal unique name of the threshold data to display e.g. Nodes.Stats.PercentMemoryUsed.</field>
    thresholdName: '',
    /// <field type='String'>Required. Display name for the threshold data to display to the user e.g. "Response Time".</field>
    thresholdDisplayName: '',
    /// <field type='String'>Required. Number value for threshold data operator from ThresholdOperatorEnum e.g. 0. 
    /// Only 0 (Greater), 1 (GreaterOrEqual), 3 (LessOrEqual) and 4 (Less) are supported.</field>
    operator: '',
    /// <field type='Function'>Required. Dialog close callback function that e.g. could apply the selected threshold values.</field>
    returnFunction: function (applyValues, warningLevel, criticalLevel) {
        /// <signature>
        /// <summary>Callback function</summary>
        /// <param name="applyValues" type="Boolean">A flag that indicates if to apply the selected Warning and Critical threshold values or not.</param>
        /// <param name="warningLevel" type="String">Selected Warning threshold level.</param>
        /// <param name="criticalLevel" type="String">Selected Critical threshold level.</param>
        /// </signature>
    },
    /// <field type='Function'>Optional. Custom formatter function to use instead of the default one.</field>
    statisticsFormatter: null,
    /// <field type='Function'>Default formatter.</field>
    defaultStatisticsFormatter: function (value, type, isInteger) {
        /// <signature>
        /// <summary>Formats incoming value into a string. Default implementation preserves Integers and rounds Floats to 2 decimal places.</summary>
        /// <param name="value" type="Number">Threshold value to be formatted into a string.</param>
        /// <param name="type" type="String">The type of the value being formatted e.g. 'Min' or 'StdDev'.</param>
        /// <returns type="String">Formatted threshold value e.g. for input value of 12345.678 the result could be 12.3K or 12 346.</returns>
        /// </signature>

        var preciseRound = function (num, decimals) {
            return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
        }

        var decimals = isInteger ? 0 : 2;
        var formatted = preciseRound(value, decimals);

        return formatted;
    }
};

bls.BaselineDetails = function() {

    var loadAreaChart = function (instanceId, thresholdName, thresholdDisplayName, chartWidth) {
        // function created inside chart control
        var param = [instanceId, thresholdName, thresholdDisplayName, chartWidth, 0];
        LoadccArea(param);
    };

    var loadHistoricalChart = function (instanceId, thresholdName, thresholdDisplayName, chartWidth) {

        var param = {            
          ThresholdName : thresholdName,
          InstanceId : instanceId
        };

        SW.Core.Services.callController("/api/Thresholds/GetStatisticalDataChartConfig", param,
            function(response) {
                if (response.IsValid) {
                    // function created inside chart control
                    LoadccHistorical([instanceId, thresholdName, thresholdDisplayName, chartWidth, response]);
                }
                else {
                    $('#LatestBaselineDetails #Tabs #tabMetricOverTime').html('').append("<div class='noChartMessage'>@{R=Core.Strings;K=WEBJS_TK0_34;E=js}</div>");
                }
            });

    };
    
    var destroyChart = function (parentDiv) {
        var chart = parentDiv.data();

        try {
            if (chart) {
                chart.destroy();
            }
        }
        catch (ex) { };
    };
    
    var resizeChart = function (parentDiv) {
        var chart = parentDiv.data();

        try {
            if (chart) {
                chart.setSize(parentDiv.width(), chart.chartHeight, false);
            }
        }
        catch (ex) { };
    };

    var resizeCharts = function (activeTab, id, thresholdName, thresholdDisplayName) {
        var newWidth = 0;

        // Area chart - setSize() method does not work for highcharts in core chart framework -> reloading is needed
        if (activeTab === 0) {

            newWidth = $('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first().width();
            loadAreaChart(id, thresholdName, thresholdDisplayName, newWidth);
        }
        // MinMax chart
        if (activeTab === 1) {

            var parentDiv = $('#LatestBaselineDetails #Tabs #tabMetricOverTime').find('.hasChart').first();
            newWidth = parentDiv.width();
            resizeChart(parentDiv);
        }

        $('#LatestBaselineDetails #Tabs #ChartsWidth').val(newWidth);
    };

    this.redrawXAxis = function(title, unit) {
        var chart = $('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first().data();

        chart.xAxis[0].setTitle({ text: title });

        if (unit) {
            $('g.highcharts-axis-labels:first tspan').append(' ' + unit);
        }
    };

    this.redrawPlotBandsInCharts = function() {
        new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInChart($('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first(), 'x');
        new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInChart($('#LatestBaselineDetails #Tabs #tabMetricOverTime').find('.hasChart').first(), 'y');
    };
    
    this.redrawPlotBandsInChart = function(parentDiv, axis) {
        var warningLevel = $('#tblStatistics div.warning').text();
        var criticalLevel = $('#tblStatistics div.critical').text();
        var operator = $('#LatestBaselineDetails #ThresholdOperator').val();
        
        var chart = parentDiv.data();

        try {
            if (chart && chart.Thresholds) {

                var param = {
                    WarningValue: parseFloat(warningLevel),
                    CriticalValue: parseFloat(criticalLevel),
                    ThresholdOperator: parseInt(operator),
                    ShowLabel : false
                };

                chart.Thresholds.showThresholds(param, axis);
            }
        }
        catch(ex) {};   
    };

    this.redrawPlotLineInCharts = function(valueToDisplay, clear) {
        var activeTab = $("#LatestBaselineDetails #Tabs").tabs('option', 'selected');

        // Area chart
        if (activeTab === 0) {
            new SW.Core.Baselines.BaselineDetails().redrawPlotLineInChart($('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first(), valueToDisplay, clear, 'x');
        }
        // MinMax chart
        if (activeTab === 1) {
            new SW.Core.Baselines.BaselineDetails().redrawPlotLineInChart($('#LatestBaselineDetails #Tabs #tabMetricOverTime').find('.hasChart').first(), valueToDisplay, clear, 'y');
        }
    };

    this.redrawPlotLineInChart = function(parentDiv, valueToDisplay, clear, axis) {
        var chart = parentDiv.data();

        try {
            if (chart) {
                var axisChart = (axis === 'x') ? chart.xAxis[0] : chart.yAxis[0];

                if (axisChart) {
                    axisChart.removePlotLine('thresholdPreview');

                    if (!clear) {
                        axisChart.addPlotLine({ id: 'thresholdPreview', value: valueToDisplay, width: 1, color: '#000000', dashStyle: 'dash', zIndex: 10 });
                    }
                }
            }
        } catch(ex) {
        }
        ;
    }; 
     
    this.showLatestBaselineDetailsDialog = function (id, thresholdName, thresholdDisplayName, operator, returnFunction) {
        /// <signature>
        /// <summary>Displays the dialog</summary>
        /// <param name="config" type="Object">Required. Dialog configuration hash. Refer to SW.Core.Baselines.showBaselineDetailsDialog.defaultConfig 
        /// field for a list of parameters. Supports more options than the other overload. Use to provide custom formatter functions for statistics table.</param>
        /// </signature>
        /// <signature>
        /// <summary>Displays the dialog</summary>
        /// <param name="id" type="string">Required. Instance ID of the entity (e.g. Node) to display dialog for.</param>
        /// <param name="thresholdName" type="string">Required. Internal unique name of the threshold data to display e.g. Nodes.Stats.PercentMemoryUsed.</param>
        /// <param name="thresholdDisplayName" type="string">Required. Display name for the threshold data to display to the user e.g. "Response Time".</param>
        /// <param name="operator" type="string">Required. Number value for threshold data operator from ThresholdOperatorEnum e.g. 0. 
        /// Only 0 (Greater), 1 (GreaterOrEqual), 3 (LessOrEqual) and 4 (Less) are supported.</param>
        /// <param name="returnFunction" type="string">Required. Dialog close callback function that recieves success flag, warning and critical levels.</param>
        /// </signature>

        var config;

        // the function has 2 overloads, check which one is being called
        if (arguments.length == 1) {
            config = $.extend(true, {}, bls.showBaselineDetailsDialog.defaultConfig, id);
        } else {
            config = $.extend(true,
                {},
                bls.showBaselineDetailsDialog.defaultConfig, {
                    id: id,
                    thresholdName: thresholdName,
                    thresholdDisplayName: thresholdDisplayName,
                    operator: operator,
                    returnFunction: returnFunction
                }
            );
        }

        // adding threshold name
        $('#LatestBaselineDetails span.thresholdName').text('(' + config.thresholdDisplayName + ')');
        $('#LatestBaselineDetails #ThresholdOperator').val(config.operator);

        // set baseline data for current thresholds div
        setBaselineDataForCurrentThresholdsDiv(null, null, null);

        // set latest baseline statistics and details -> load area chart       
        setBaselineStatisticsAndDetails(config);

        // set 'Use Recommended Thresholds' button
        //$('#LatestBaselineDetails #UseRecommendedThresholds').unbind('click').bind('click', function () {
        //    getLatestBaselineThresholds(basename, id, columnSchemaId, thresholdName, baselineAvailableIn, baselineStatsFromTicks, true);
        //});

        // open the dialog window
        openThresholdsDetailsDialog(config);
    };
    
    var setBaselineDataForCurrentThresholdsDiv = function (baselineAppliedTicks, baselineFromTicks, baselineToTicks) {
        if (baselineAppliedTicks !== null && baselineFromTicks !== null && baselineToTicks !== null) {
            $('#LatestBaselineDetails #DataForCurrentThresholds').show();
            $('#LatestBaselineDetails img.baseline').css('visibility', 'visible');

            if (baselineAppliedTicks === 0) {
                $('#LatestBaselineDetails #DataForCurrentThresholds span.calculatedOn').text(getCalculatedOnDate(SW.Core.Date.ToServerTime(new Date())));
            } else {                
                $('#LatestBaselineDetails #DataForCurrentThresholds span.calculatedOn').text(getCalculatedOnDate(SW.Core.Date.ToServerTime(convertTicksToDate(baselineAppliedTicks))));
            }
            
            var dataUsedText = getBaselineUsedDate(SW.Core.Date.ToServerTime(convertTicksToDate(baselineFromTicks))) + ' @{R=Core.Strings;K=WEBJS_TK0_5;E=js} ' + getBaselineUsedDate(SW.Core.Date.ToServerTime(convertTicksToDate(baselineToTicks)));
            $('#LatestBaselineDetails #DataForCurrentThresholds span.dataUsed').text(dataUsedText);
        } else {
            $('#LatestBaselineDetails #DataForCurrentThresholds').hide();
            $('#LatestBaselineDetails img.baseline').css('visibility', 'hidden');
        }
    };
    
    var getCalculatedOnDate = function (date) {
        var daysSinceText = ' ';
        var timeSince = getDateTimeSince(date);
        if (timeSince.days === 0)
            daysSinceText = ' (@{R=Core.Strings;K=WEBJS_TK0_6;E=js})';
        else if (timeSince.days === 1)
            daysSinceText = ' (@{R=Core.Strings;K=WEBJS_TK0_7;E=js})';
        else if (timeSince.days > 1) {
            if (timeSince.years > 0)
                timeSince.days = timeSince.years * 365 + timeSince.days;
            daysSinceText = ' (' + timeSince.days + ' @{R=Core.Strings;K=WEBJS_TK0_8;E=js})';
        }

        var dateFormat = "@{R=Core.Strings;K=WEBJS_PS0_67;E=js}";
        dateFormat = dateFormat.replace("{DAYNAME}", getDayName(date));
        dateFormat = dateFormat.replace("{MONTHNAME}", getMonthName(date));
        dateFormat = dateFormat.replace("{DAY}", date.getUTCDate());
        dateFormat = dateFormat.replace("{YEAR}", date.getUTCFullYear());
        dateFormat = dateFormat.replace("{DAYSSinceText}", daysSinceText);
        dateFormat = dateFormat.replace("{DAYTIME}", getTime(date.getUTCHours(), date.getUTCMinutes()));
        return dateFormat;
    };

    var getBaselineUsedDate = function (date) {
        var dateFormat = "@{R=Core.Strings;K=WEBJS_PS0_68;E=js}";
        dateFormat = dateFormat.replace("{MONTHNAME}", getMonthName(date));
        dateFormat = dateFormat.replace("{DAY}", date.getUTCDate());
        dateFormat = dateFormat.replace("{YEAR}", date.getUTCFullYear());
        dateFormat = dateFormat.replace("{DAYTIME}", getTime(date.getUTCHours(), date.getUTCMinutes()));
        return dateFormat;
    };

    var getDateTimeSince = function (date) { // this ignores months
        var obj = {};
        obj._milliseconds = (new Date()).valueOf() - date.valueOf();
        obj.milliseconds = obj._milliseconds % 1000;
        obj._seconds = (obj._milliseconds - obj.milliseconds) / 1000;
        obj.seconds = obj._seconds % 60;
        obj._minutes = (obj._seconds - obj.seconds) / 60;
        obj.minutes = obj._minutes % 60;
        obj._hours = (obj._minutes - obj.minutes) / 60;
        obj.hours = obj._hours % 24;
        obj._days = (obj._hours - obj.hours) / 24;
        obj.days = obj._days % 365;
        // finally
        obj.years = (obj._days - obj.days) / 365;

        return obj;
    };
    
    var isInteger = function (value) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
        return typeof value === "number" &&
                      isFinite(value) &&
                      value > -9007199254740992 &&
                      value < 9007199254740992 &&
                      Math.floor(value) === value;
    };

    var getDayName = function (date) {
        var weekday = new Array(7);
        weekday[0] = "@{R=Core.Strings;K=WEBJS_TK0_10;E=js}"; // Sunday
        weekday[1] = "@{R=Core.Strings;K=WEBJS_TK0_11;E=js}"; // Monday
        weekday[2] = "@{R=Core.Strings;K=WEBJS_TK0_12;E=js}"; // Tuesday
        weekday[3] = "@{R=Core.Strings;K=WEBJS_TK0_13;E=js}"; // Wednesday
        weekday[4] = "@{R=Core.Strings;K=WEBJS_TK0_14;E=js}"; // Thursday
        weekday[5] = "@{R=Core.Strings;K=WEBJS_TK0_15;E=js}"; // Friday
        weekday[6] = "@{R=Core.Strings;K=WEBJS_TK0_16;E=js}"; // Saturday

        return weekday[date.getUTCDay()];
    };

    var getMonthName = function (date) {
        var month = new Array(12);
        month[0] = "@{R=Core.Strings;K=WEBJS_TK0_17;E=js}"; // January
        month[1] = "@{R=Core.Strings;K=WEBJS_TK0_18;E=js}"; // February
        month[2] = "@{R=Core.Strings;K=WEBJS_TK0_19;E=js}"; // March
        month[3] = "@{R=Core.Strings;K=WEBJS_TK0_20;E=js}"; // April
        month[4] = "@{R=Core.Strings;K=WEBJS_TK0_21;E=js}"; // May
        month[5] = "@{R=Core.Strings;K=WEBJS_TK0_22;E=js}"; // Juny
        month[6] = "@{R=Core.Strings;K=WEBJS_TK0_23;E=js}"; // July
        month[7] = "@{R=Core.Strings;K=WEBJS_TK0_24;E=js}"; // August
        month[8] = "@{R=Core.Strings;K=WEBJS_TK0_25;E=js}"; // September
        month[9] = "@{R=Core.Strings;K=WEBJS_TK0_26;E=js}"; // October
        month[10] = "@{R=Core.Strings;K=WEBJS_TK0_27;E=js}"; // November
        month[11] = "@{R=Core.Strings;K=WEBJS_TK0_28;E=js}"; // December

        return month[date.getUTCMonth()];
    };

    var getTime = function (hours, minutes) {
        var ampm = hours >= 12 ? '@{R=Core.Strings;K=WEBJS_TK0_30;E=js}' : '@{R=Core.Strings;K=WEBJS_TK0_29;E=js}';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;

        return hours + ':' + minutes + ' ' + ampm;;
    };
    
    var fillStatisticValues = function (config) {
        var integer = isInteger(config.values.Min) && isInteger(config.values.Max);
        var formatter = config.customFormatter || function(value, type) {
            return config.defaultFormatter(value, type, integer);
        };

        var result = config.html.replace('{CLASS}', config.className);
        result = result.replace('{PERIOD}', config.label);

        result = result.replace('{MIN-V}', config.values.Min);
        result = result.replace('{MIN}', formatter(config.values.Min, 'Min'));

        result = result.replace('{MAX-V}', config.values.Max);
        result = result.replace('{MAX}', formatter(config.values.Max, 'Max'));

        result = result.replace('{σ-V}', config.values.StdDev);
        result = result.replace('{σ}', formatter(config.values.StdDev, 'StdDev'));

        result = result.replace('{-3σ-V}', config.values.MeanMinus3StdDev);
        result = result.replace('{-3σ}', formatter(config.values.MeanMinus3StdDev, 'MeanMinus3StdDev'));

        result = result.replace('{-2σ-V}', config.values.MeanMinus2StdDev);
        result = result.replace('{-2σ}', formatter(config.values.MeanMinus2StdDev, 'MeanMinus2StdDev'));

        result = result.replace('{-1σ-V}', config.values.MeanMinusStdDev);
        result = result.replace('{-1σ}', formatter(config.values.MeanMinusStdDev, 'MeanMinusStdDev'));

        result = result.replace('{MEAN-V}', config.values.Mean);
        result = result.replace('{MEAN}', formatter(config.values.Mean, 'Mean'));

        result = result.replace('{1σ-V}', config.values.MeanPlusStdDev);
        result = result.replace('{1σ}', formatter(config.values.MeanPlusStdDev, 'MeanPlusStdDev'));

        result = result.replace('{2σ-V}', config.values.MeanPlus2StdDev);
        result = result.replace('{2σ}', formatter(config.values.MeanPlus2StdDev, 'MeanPlus2StdDev'));

        result = result.replace('{3σ-V}', config.values.MeanPlus3StdDev);
        result = result.replace('{3σ}', formatter(config.values.MeanPlus3StdDev, 'MeanPlus3StdDev'));

        return result;
    };

    // return date from ticks
    var convertTicksToDate = function(pTicks) {
        try {
            var dateTimeObject;
            dateTimeObject = new Date((pTicks - 621355968000000000) / 10000);
            return dateTimeObject;
        } catch(error) {
            return pTicks + ' @{R=Core.Strings;K=WEBJS_TK0_31;E=js}.';
        }
    };

    // IE7/8 workaround (old version are not able to process ISO date formats
    var getDateTimeFromISO = function (s) {
        var day, tz,
        rx = /^(\d{4}\-\d\d\-\d\d([tT ][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/,
        p = rx.exec(s) || [];
        if (p[1]) {
            day = p[1].split(/\D/);
            for (var i = 0, L = day.length; i < L; i++) {
                day[i] = parseInt(day[i], 10) || 0;
            };
            day[1] -= 1;
            day = new Date(Date.UTC.apply(Date, day));
            if (!day.getDate()) return NaN;
            if (p[5]) {
                tz = (parseInt(p[5], 10) * 60);
                if (p[6]) tz += parseInt(p[6], 10);
                if (p[4] == '+') tz *= -1;
                if (tz) day.setUTCMinutes(day.getUTCMinutes() + tz);
            }
            return day;
        }
        return NaN;
    }

    // return date from json string parameter
    var convertStringToDate = function (dateString) {
        
        try
        {
            var date = new Date(dateString);
            if (isNaN(date))
                return getDateTimeFromISO(dateString);
            else
                return date;

        } catch(error) {
            return dateString + ' @{R=Core.Strings;K=WEBJS_TK0_31;E=js}.';
        }
    };

    this.highlightRecommendedThresholds = function(operator) {
        $('#tblStatistics div.warning').removeClass('warning');
        $('#tblStatistics div.critical').removeClass('critical');
        $('#tblStatistics div.recommendedWarning').removeClass('recommendedWarning');
        $('#tblStatistics div.recommendedCritical').removeClass('recommendedCritical');

        var tblStatistics = $('#LatestBaselineDetails #tblStatistics tbody');

        if (operator === 0 || operator === 1) {
            tblStatistics.find('tr.all td:nth-child(10) div.statisticValue').addClass('warning').addClass('recommendedWarning');
            tblStatistics.find('tr.all td:nth-child(11) div.statisticValue').addClass('critical').addClass('recommendedCritical');
        } else if (operator === 3 || operator === 4) {
            tblStatistics.find('tr.all td:nth-child(5) div.statisticValue').addClass('critical').addClass('recommendedCritical');
            tblStatistics.find('tr.all td:nth-child(6) div.statisticValue').addClass('warning').addClass('recommendedWarning');
        }

        var warning = $('#tblStatistics div.recommendedWarning');
        $('#SelectedWarningThreshold')
            .text(warning.text())
            .closest('td').data('value', warning.closest('td').data('value'));

        var critical = $('#tblStatistics div.recommendedCritical');
        $('#SelectedCriticalThreshold')
            .text(critical.text())
            .closest('td').data('value', critical.closest('td').data('value'));

        new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts();
    };
    
    // parse a threshold's statistics
    var parseStatistics = function (statistics, operator, customFormatter, defaultFormatter) {
        var all, day, night;

        for (var i = 0; i < statistics.Model.length; i++) {
            if (statistics.Model[i].TimeFrameName === 'Core_All')
                all = statistics.Model[i];
            if (statistics.Model[i].TimeFrameName === 'Core_WorkHours')
                day = statistics.Model[i];
            if (statistics.Model[i].TimeFrameName === 'Core_NightAndWeekend')
                night = statistics.Model[i];
        }

        var tblStatistics = $("#LatestBaselineDetails #tblStatistics tbody");

        // clear statistics details
        tblStatistics.html('');
        $('#LatestBaselineDetails #BaselineDetails span.calculatedOn').html('');
        $('#LatestBaselineDetails #BaselineDetails span.dataUsed').html('');

        var rowPattern = "<tr class='{CLASS}'>" +
            "<td>{PERIOD}</td>" +
            "<td data-value='{MIN-V}'><div class='statisticValue'>{MIN}</div></td>" +
            "<td data-value='{MAX-V}'><div class='statisticValue'>{MAX}</div></td>" +
            "<td data-value='{σ-V}'>{σ}</td>" +
            "<td data-value='{-3σ-V}'><div class='statisticValue'>{-3σ}</div></td>" +
            "<td data-value='{-2σ-V}'><div class='statisticValue'>{-2σ}</div></td>" +
            "<td data-value='{-1σ-V}'><div class='statisticValue'>{-1σ}</div></td>" +
            "<td data-value='{MEAN-V}'><div class='statisticValue'>{MEAN}</div></td>" +
            "<td data-value='{1σ-V}'><div class='statisticValue'>{1σ}</div></td>" +
            "<td data-value='{2σ-V}'><div class='statisticValue'>{2σ}</div></td>" +
            "<td data-value='{3σ-V}'><div class='statisticValue'>{3σ}</div></td>" +
        "</tr>";
        
        // empty statistics
        if (statistics.length == 0) {
            tblStatistics.append("<tr style='height:66px'><td colspan='11'>@{R=Core.Strings;K=WEBJS_TK0_3;E=js}</td></tr>"); // Statistics are not available.            
        }        
            // standard deviation is zero
        else if ((!day || day.Min === day.Max) && (!night || night.Min === night.Max) && (!all || all.Min === all.Max)) {
            tblStatistics.append("<tr style='height:66px'><td colspan='11'>@{R=Core.Strings;K=WEBJS_TK0_3;E=js} @{R=Core.Strings;K=WEBJS_TK0_4;E=js}</td></tr>"); // Statistics are not available. Standard deviation is zero.            
        }
        else {            
            // fill day row
            if (day) {
                var rowDay = fillStatisticValues({
                    html: rowPattern,
                    className: 'day',
                    label: '<img class="workdays" alt="sun_icon" src="/Orion/images/baselineDay.png" />',
                    values: day,
                    customFormatter: customFormatter,
                    defaultFormatter: defaultFormatter
                });
                tblStatistics.append(rowDay);
            }

            // fill night row
            if (night) {
                var rowNight = fillStatisticValues({
                    html: rowPattern,
                    className: 'night',
                    label: '<img class="offdays" alt="moon_icon" src="/Orion/images/baselineNight.png" />',
                    values: night,
                    customFormatter: customFormatter,
                    defaultFormatter: defaultFormatter
                });
                tblStatistics.append(rowNight);
            }

            // fill all hours row and latest baseline details div
            if (all) {
                var rowAll = fillStatisticValues({
                    html: rowPattern, 
                    className: 'all', 
                    label: 'All hours', 
                    values: all, 
                    customFormatter: customFormatter,
                    defaultFormatter: defaultFormatter
                });
                tblStatistics.append(rowAll);

                tblStatistics.find('tr.all td:first').css('font-size', '9px');

                new SW.Core.Baselines.BaselineDetails().highlightRecommendedThresholds(operator);
               

                var calculatedOnText = getCalculatedOnDate(SW.Core.Date.ToServerTime(convertStringToDate(all.Timestamp)));
                $('#LatestBaselineDetails #BaselineDetails span.calculatedOn').text(calculatedOnText);

                var dataUsedText = getBaselineUsedDate(SW.Core.Date.ToServerTime(convertStringToDate(all.MinDateTime))) + ' - ' + getBaselineUsedDate(SW.Core.Date.ToServerTime(convertStringToDate(all.MaxDateTime)));
                $('#LatestBaselineDetails #BaselineDetails span.dataUsed').text(dataUsedText);
            }            
        }
    };
    
    var setBaselineStatisticsAndDetails = function (config) {
        // show loading masks        
        $("#LatestBaselineDetails #tblStatistics tbody").html('').append("<tr style='height:66px'><td colspan='11'><img src='/Orion/images/loading_gen_16x16.gif' />&nbsp;@{R=Core.Strings;K=WEBJS_TK0_1;E=js}</td></tr>");
        $('#LatestBaselineDetails #BaselineDetails span.calculatedOn').html('').append("<img src='/Orion/images/loading_gen_16x16.gif' />&nbsp;@{R=Core.Strings;K=WEBJS_TK0_1;E=js}");        
        $('#LatestBaselineDetails #BaselineDetails span.dataUsed').html('').append("<img src='/Orion/images/loading_gen_16x16.gif' />&nbsp;@{R=Core.Strings;K=WEBJS_TK0_1;E=js}");
        
        var loadBaselineDataFailed = function (error) {
            $('#BaselineDetailsErrorDialog span.message').html('@{R=Core.Strings;K=WEBJS_TK0_2;E=js}<br/><br/>' + error); //Statistics could not be loaded.
            $('#BaselineDetailsErrorDialog').dialog({
                width: 350,
                modal: true,
                close: function (event, ui) {
                    $("#LatestBaselineDetails #tblStatistics tbody").html('').append("<tr style='height:66px'><td colspan='11'>@{R=Core.Strings;K=WEBJS_TK0_2;E=js}</td></tr>");

                    // load a chart on the first tab (function created inside a chart control) 
                    loadAreaChart(config.id, config.thresholdName, config.thresholdDisplayName, null);
                }
            });
        };

        // Reset selected values
        $('#SelectedWarningThreshold').text('').closest('td').data('value', null);
        $('#SelectedCriticalThreshold').text('').closest('td').data('value', null);

        var GetBaselineDetailsParams = {
            InstanceId: config.id,
            ThresholdName: config.thresholdName
        };

        SW.Core.Services.callController("/api/Thresholds/GetBaselineValues", GetBaselineDetailsParams,
            function(statistics) {
                if (statistics != null && statistics.HasValues) {
                    // parse statistics
                    parseStatistics(statistics, config.operator, config.statisticsFormatter, config.defaultStatisticsFormatter);

                    // load a chart on the first tab (function created inside a chart control) 
                    loadAreaChart(config.id, config.thresholdName, config.thresholdDisplayName, null);

                    // table's tooltip events
                    $('div.statisticValue').unbind('mouseover').bind('mouseover', function() {
                        $('#StastisticsTooltipValue').text($(this).text());
                        $('#StastisticsTooltip .recommended').hide();
                        $('#StastisticsTooltip a').removeClass('disabled');

                        $('#tblStatistics div.inTooltip').removeClass('inTooltip');
                        $(this).addClass('inTooltip');

                        var tooltip = $('#StastisticsTooltip');
                        if ($(this).hasClass('warning')) {
                            $('#StastisticsTooltip #lnkSetWarning').addClass('disabled');
                        }
                        if ($(this).hasClass('critical')) {
                            $('#StastisticsTooltip #lnkSetCritical').addClass('disabled');
                        }
                        if ($(this).hasClass('recommendedWarning')) {
                            $('#StastisticsTooltip #lblRecomendedWarning').show();
                        }
                        if ($(this).hasClass('recommendedCritical')) {
                            $('#StastisticsTooltip #lblRecomendedCritical').show();
                        }
                        tooltip.show();
                        tooltip.position({ at: 'right bottom', of: $(this), my: 'left bottom' });

                        new SW.Core.Baselines.BaselineDetails().redrawPlotLineInCharts($(this).text(), false);
                    });

                    $('#StastisticsTooltip').unbind('mouseleave').bind('mouseleave', function() {
                        $(this).hide();
                        new SW.Core.Baselines.BaselineDetails().redrawPlotLineInCharts(null, true);
                    });

                    $('#tblStatistics').unbind('mouseleave').bind('mouseleave', function() {
                        $('#StastisticsTooltip').hide();
                        new SW.Core.Baselines.BaselineDetails().redrawPlotLineInCharts(null, true);
                    });

                    $('#StastisticsTooltip #lnkSetWarning').unbind('click').bind('click', function() {
                        if ($(this).hasClass('disabled'))
                            return false;
                        $('#StastisticsTooltip').hide();
                        $('#tblStatistics div.warning').removeClass('warning');
                        var cell = $('#tblStatistics div.inTooltip').addClass('warning');
                        $('#SelectedWarningThreshold')
                            .text( cell.text() )
                            .closest('td').data('value', cell.closest('td').data('value'));

                        new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts();
                    });

                    $('#StastisticsTooltip #lnkSetCritical').unbind('click').bind('click', function() {
                        if ($(this).hasClass('disabled'))
                            return false;
                        $('#StastisticsTooltip').hide();
                        $('#tblStatistics div.critical').removeClass('critical');
                        var cell = $('#tblStatistics div.inTooltip').addClass('critical');
                        $('#SelectedCriticalThreshold')
                            .text( cell.text() )
                            .closest('td').data('value', cell.closest('td').data('value'));

                        new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts();
                    });

                    // time frames tooltip events
                    $('img.workdays').unbind('mouseover').bind('mouseover', function() {
                        $('div.Core_WorkHours').show().position({ at: 'right bottom', of: $(this), my: 'left bottom' });
                        $('#StastisticsTooltip').hide();
                    });

                    $('img.offdays').unbind('mouseover').bind('mouseover', function() {
                        $('div.Core_NightAndWeekend').show().position({ at: 'right bottom', of: $(this), my: 'left bottom' });
                        $('#StastisticsTooltip').hide();
                    });

                    $('img.workdays').unbind('mouseout').bind('mouseout', function() {
                        $('div.Core_WorkHours').hide();
                    });

                    $('img.offdays').unbind('mouseout').bind('mouseout', function() {
                        $('div.Core_NightAndWeekend').hide();
                    });
                }
                else {
                    loadBaselineDataFailed('');
                }
           },
           function (error) {
               loadBaselineDataFailed(error);
           }
        );        
    };
    
    var openThresholdsDetailsDialog = function(config) {
        $('#LatestBaselineDetails').dialog({
            title: '@{R=Core.Strings;K=WEBJS_TK0_33;E=js} (' + config.thresholdDisplayName + ')',
            modal: true,
            minHeight: 660,
            maxHeight: 660,
            resizable: true,
            minWidth: 850,
            width: 850,
            open: function(event, ui) {
                $("#LatestBaselineDetails #Tabs").tabs({
                    selected: 0
                });

                destroyChart($('#LatestBaselineDetails #Tabs #tabOccurrences').find('.hasChart').first());
                $('#LatestBaselineDetails #Tabs #tabOccurrences table.chartLegend').html('');

                $("#LatestBaselineDetails #Tabs").unbind('tabsselect').bind('tabsselect', function(event2, ui2) {
                    var chartWidth = $('#LatestBaselineDetails #Tabs #ChartsWidth').val();

                    if (ui2.index === 0) {
                        loadAreaChart(config.id, config.thresholdName, config.thresholdDisplayName, chartWidth);
                    }

                    if (ui2.index === 1) {
                        loadHistoricalChart(config.id, config.thresholdName, config.thresholdDisplayName, chartWidth);
                    }
                });
                
                // reset to recommended thresholds link
                $('#LatestBaselineDetails #ResetToRecommendedThresholds').unbind('click').bind('click', function () {
                    $('#tblStatistics div.warning').removeClass('warning');
                    $('#tblStatistics div.critical').removeClass('critical');
                    $('#tblStatistics div.recommendedWarning').addClass('warning');
                    $('#tblStatistics div.recommendedCritical').addClass('critical');

                    var warning = $('#tblStatistics div.recommendedWarning');
                    $('#SelectedWarningThreshold')
                        .text(warning.text())
                        .closest('td').data('value', warning.closest('td').data('value') );

                    var critical = $('#tblStatistics div.recommendedCritical');
                    $('#SelectedCriticalThreshold')
                        .text(critical.text())
                        .closest('td').data('value', critical.closest('td').data('value'));

                    new SW.Core.Baselines.BaselineDetails().redrawPlotBandsInCharts();
                });

                // Adding help link                
                $('div.ui-dialog-titlebar:first').append('<a class=\"HelpButton\" target=\"_blank\" href=\"' + $('#HelpLinkUrl').text() + '\"><img src=\"/Orion/images/baselineDialogHelp.png\" alt=\"@{R=Core.Strings;K=WEBJS_TK0_32;E=js}\" /></a>');
            },

            resizeStop: function(event, ui) {
                resizeCharts($("#LatestBaselineDetails #Tabs").tabs('option', 'selected'), config.id, config.thresholdName, config.thresholdDisplayName);
            },

            close: function(event, ui) {
				$('div.ui-dialog-titlebar .HelpButton').remove();
                if ($('#LatestBaselineDetails #ApplyThresholds').attr('checked')) {
                    var dialogWarningLevel = $('#SelectedWarningThreshold').closest('td').data('value');
                    var dialogCriticalLevel = $('#SelectedCriticalThreshold').closest('td').data('value');

                    config.returnFunction(true, dialogWarningLevel, dialogCriticalLevel);
                    $('#LatestBaselineDetails #ApplyThresholds').attr('checked', false);
                } else {
                    config.returnFunction(false, null, null);
                }

                $('#LatestBaselineDetails #Tabs #ChartsWidth').val('');
            }
        });
    };
};