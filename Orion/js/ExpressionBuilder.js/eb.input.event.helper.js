/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.models.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Event) {
                        var Helper = (function () {
                            function Helper() {
                            }
                            Helper.GetConstant = function (eventParam, element) {
                                var value = null;
                                switch (eventParam) {
                                    case ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND:
                                        //call asp.net ajax fw
                                        var b = Boolean;
                                        value = b.parse(element.val());
                                        break;
                                    case ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_TYPE:
                                    case ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_COUNT:
                                        value = parseInt(element.val());
                                        break;
                                }

                                var expr = {
                                    NodeType: ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT,
                                    Value: value,
                                    Child: []
                                };

                                return expr;
                            };

                            Helper.GetDefaultValueForEventParam = function (paramID) {
                                switch (paramID) {
                                    case ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND:
                                        return true;
                                    case ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_TYPE:
                                        return 0;
                                    case ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_COUNT:
                                        return 5;
                                }

                                $.error('GetDefaultValueForEventParam method does now know default value for ' + paramID);
                            };

                            Helper.GetEventProperty = function (event, paramID) {
                                if (event.NodeType != ExpressionBuilder.Constants.EXPRESSION_TYPE.EVENT) {
                                    $.error('GetEventProperty method expects Event node as input');
                                    return;
                                }

                                if (event.Child.length < paramID + 1 || event.Child[paramID].Value == undefined || event.Child[paramID].Value == null) {
                                    return Helper.GetDefaultValueForEventParam(paramID);
                                }

                                var result = event.Child[paramID].Value;

                                if (paramID == ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND) {
                                    if (typeof result == "string") {
                                        var b = Boolean;
                                        return b.parse(result);
                                    }
                                }

                                return result;
                            };

                            Helper.GetEventFilter = function (event) {
                                if (event.NodeType != ExpressionBuilder.Constants.EXPRESSION_TYPE.EVENT) {
                                    $.error('GetEventProperty method expects Event node as input');
                                    return;
                                }

                                if (event.Child.length < ExpressionBuilder.Constants.EVENT_PARAMS.FILTER_EXPRESSIONS + 1) {
                                    return null;
                                }

                                return event.Child[ExpressionBuilder.Constants.EVENT_PARAMS.FILTER_EXPRESSIONS];
                            };
                            return Helper;
                        })();
                        Event.Helper = Helper;
                    })(Input.Event || (Input.Event = {}));
                    var Event = Input.Event;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
