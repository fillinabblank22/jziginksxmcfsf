﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var Bool = (function (_super) {
                            __extends(Bool, _super);
                            function Bool(config) {
                                var _this = this;
                                _super.call(this, config);

                                this._input = $('<select />').addClass('neb-styled-combo');
                                this._createItem(this._input, "@{R=Core.Strings;K=WEBJS_JP2_0;E=js}", "1");
                                this._createItem(this._input, "@{R=Core.Strings;K=WEBJS_JP2_1;E=js}", "0");

                                this._input.val((this._expr.Value == "0" || this._expr.Value == "false" || this._expr.Value == "False") ? "0" : "1");

                                this._input.prop('disabled', !(this._expr.Value || this._options.enabledWhenEmpty));
                                this._options.renderTo.append(this._input);

                                var func = function () {
                                    if (_this._input.parents(".neb-event").length == 0) {
                                        _this._input.customSelect();
                                    }
                                };

                                setTimeout(func, 0);
                            }
                            Bool.prototype._createItem = function (select, text, value) {
                                var o = new Option(text, value);
                                $(o).html(text);
                                select.append(o);
                            };
                            return Bool;
                        })(Constant.BaseConstant);
                        Constant.Bool = Bool;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
