var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                ExpressionBuilder.Constants = {
                    EXPRESSION_TYPE: {
                        OPERATOR: 0,
                        FIELD: 1,
                        CONSTANT: 2,
                        EVENT: 3
                    },
                    EVENT_PARAMS: {
                        MUST_HAPPEND: 0,
                        MUST_HAPPEND_TYPE: 1,
                        MUST_HAPPEND_COUNT: 2,
                        FILTER_EXPRESSIONS: 3
                    },
                    OPERATOR_TYPE: {
                        UNARY: 0,
                        BINARY: 1,
                        NARY: 2
                    },
                    RULE_TYPE: {
                        UNKNOWN: 0,
                        FIELD_TO_CONSTANT: 1,
                        FIELD_TO_FIELD: 2,
                        CONSTANT_TO_CONSTANT: 3,
                        CONSTANT_TO_FIELD: 4,
                        EVENT: 5
                    },
                    EXPR_SIDE: {
                        UNKNOWN: 0,
                        LEFT: 1,
                        RIGHT: 2
                    },
                    FIELD_TYPE: {
                        PROPERTY: 0,
                        EVENT: 1,
                        INSTANCE: 2
                    },
                    OBJECT_PICKER_TYPE: {
                        OBJECT: 0,
                        GROUP: 1
                    }
                };
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
