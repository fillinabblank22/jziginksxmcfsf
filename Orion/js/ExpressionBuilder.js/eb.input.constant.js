﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var BaseConstant = (function () {
                            function BaseConstant(config) {
                                this._units = null;
                                this._options = config;
                                this._expr = this._options.expr;
                            }
                            BaseConstant.prototype.getExpr = function () {
                                this._expr.Value = this._input.val();
                                return this._expr;
                            };

                            BaseConstant.prototype.lostFocus = function () {
                                var isValid = this.validate();

                                if (!isValid) {
                                    this._input.addClass('sw-validation-input-error');
                                    this._input.attr('title', '@{R=Core.Strings;K=WEBJS_TK0_36;E=js}'); //Value is invalid. (Can\'t be empty or must be the same type as field)
                                } else {
                                    this._input.removeClass('sw-validation-input-error');
                                    this._input.attr('title', '');
                                }
                            };

                            BaseConstant.prototype.validate = function () {
                                return true;
                            };

                            BaseConstant.prototype.setFilter = function (masterField) {
                                this._input.prop('disabled', false);

                                if (this._units != null) {
                                    if (masterField.DataTypeInfo.Units && masterField.DataTypeInfo.Units !== '') {
                                        this._units.html(masterField.DataTypeInfo.Units);
                                    }
                                }
                            };
                            return BaseConstant;
                        })();
                        Constant.BaseConstant = BaseConstant;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
