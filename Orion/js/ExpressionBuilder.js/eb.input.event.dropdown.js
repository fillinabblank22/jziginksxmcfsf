﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Event) {
                        var DropDown = (function () {
                            function DropDown(config) {
                                // CONSTANTS
                                this._DEFAULT_CONFIG = {
                                    enabledWhenEmpty: false,
                                    dataTypeFilter: 0
                                };
                                this._propagateSet = function () {
                                    var field;
                                    for (var i = 0; i < this._fields.length; i++) {
                                        if (this._fields[i].RefID.Data == this._expr.Value) {
                                            field = this._fields[i];
                                            break;
                                        }
                                    }

                                    if (field && typeof (this._options.onSet) === 'function') {
                                        this._options.onSet(field);
                                    }
                                };
                                this._options = $.extend({}, this._DEFAULT_CONFIG, config);
                                this._fields = [];
                                this._expr = this._options.expr;
                                this._ds = this._options.globalOptions.dataSource;
                                this._event = this._options.event;

                                this._dropdown = $("<select />");
                                this._dropdown.on("change", $.proxy(this._onChange, this));
                                this._dropdown.prop('disabled', !(this._expr.Value || this._options.enabledWhenEmpty));
                                this._options.renderTo.append(this._dropdown);

                                return this;
                            }
                            DropDown.prototype.load = function () {
                                this._fields = this._event.DataTypeInfo.Properties;
                                this._showFilteredItems();
                                this._propagateSet();
                            };

                            // PUBLIC METHODS
                            DropDown.prototype.getExpr = function () {
                                return this._expr;
                            };

                            DropDown.prototype.validate = function () {
                                return !!this._expr.Value;
                            };

                            DropDown.prototype.setFilter = function (masterField) {
                                var dataTypeFilter = masterField.DataTypeInfo.DeclType;
                                this._options.dataTypeFilter = dataTypeFilter;
                                this._dropdown.prop('disabled', false);
                                this._showFilteredItems();
                            };

                            // PRIVATE METHODS
                            DropDown.prototype._showFilteredItems = function () {
                                var prevValue = this._expr.Value;
                                var prevValueFound = false;
                                this._dropdown.empty();
                                if (!this._expr.Value) {
                                    this._createItem(this._dropdown, "@{R=Core.Strings;K=WEBJS_JP2_2;E=js}", ""); // Select Field ...
                                }

                                for (var i = 0; i < this._fields.length; i++) {
                                    if (!this._fields[i].DataTypeInfo.IsCustomProperty) {
                                        this._createItem(this._dropdown, this._fields[i].DisplayName, this._fields[i].RefID.Data);
                                        if (this._fields[i].RefID.Data == prevValue) {
                                            prevValueFound = true;
                                        }
                                    }
                                }

                                // custom properties
                                var optgroup = null;
                                for (var i = 0; i < this._fields.length; i++) {
                                    if (this._fields[i].DataTypeInfo.IsCustomProperty) {
                                        if (!optgroup)
                                            optgroup = $("<optgroup label='@{R=Core.Strings.2;K=WEBJS_TK0_1;E=js}'></optgroup>").appendTo(this._dropdown);

                                        this._createItem(optgroup, this._fields[i].DisplayName, this._fields[i].RefID.Data);
                                        if (this._fields[i].RefID.Data == prevValue) {
                                            prevValueFound = true;
                                        }
                                    }
                                }

                                // set previously selected field if it's still visible with new filter
                                if (prevValueFound) {
                                    this._dropdown.val(prevValue);
                                }
                            };

                            DropDown.prototype._onChange = function () {
                                var prevValue = this._expr.Value;
                                var newValue = this._dropdown.val();
                                this._expr.Value = newValue;
                                if (!prevValue && newValue) {
                                    //refresh fields dropdown to remove "Select Field..."
                                    this._showFilteredItems();
                                }

                                this._propagateSet();
                            };

                            DropDown.prototype._createItem = function (select, text, value) {
                                var o = new Option(text, value);
                                $(o).html(text);
                                select.append(o);
                            };
                            return DropDown;
                        })();
                        Event.DropDown = DropDown;
                    })(Input.Event || (Input.Event = {}));
                    var Event = Input.Event;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
