﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var EventType = (function () {
                            function EventType(config) {
                                this._propagateSet = function () {
                                    if (typeof (this._options.onSet) === 'function') {
                                        this._options.onSet(this._input.find("option:selected").val());
                                    }
                                };
                                this._options = $.extend({}, config);
                                this._expr = this._options.expr;
                                this._input = $('<select />').addClass('neb-styled-combo');

                                this._createItem(this._input, '@{R=Core.Strings.2;K=WEBJS_ZS0_12;E=js}', 'custom');
                                this._fields = config.event.DataTypeInfo.Properties;

                                for (var i = 0; i < this._fields.length; i++) {
                                    this._createItem(this._input, this._fields[i].DisplayName, this._fields[i].RefID.Data);
                                }

                                this._input.on("change", $.proxy(this._onChange, this));
                                this._input.prop('disabled', !(this._expr.Value));
                                this._input.val(this._expr.Value && this._expr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD ? this._expr.Value : "custom");
                                this._options.renderTo.append(this._input);
                                this._options.renderTo.toggle(!(this._fields.length > 2));
                                return this;
                            }
                            // PUBLIC METHODS
                            EventType.prototype.leftValueChanged = function (field) {
                                if (field) {
                                    this._input.prop('disabled', false);
                                }
                            };

                            EventType.prototype.getExpr = function () {
                                if (!this._input.prop('disabled') && this._input.children('option').length > 0) {
                                    if (this._input.val() == "custom") {
                                        if (typeof (this._options.onGetExpr) === 'function') {
                                            this._expr = this._options.onGetExpr();
                                        }
                                    } else {
                                        this._expr.Value = this._input.val();
                                    }
                                }
                                return this._expr;
                            };

                            EventType.prototype.validate = function () {
                                if (!this._input.prop('disabled') && this._input.children('option').length > 0) {
                                    if (this._input.val() == "custom") {
                                        if (typeof (this._options.onValidate) === 'function') {
                                            return this._options.onValidate();
                                        }
                                    }
                                }
                                return !!this._expr.Value;
                            };

                            EventType.prototype.operatorChanged = function (operator) {
                                if (operator && (operator.SupportedTypes.indexOf(SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD)) < 0) {
                                    this._input.val("custom");
                                    this._options.renderTo.toggle(false);
                                    this._propagateSet();
                                } else {
                                    this._options.renderTo.toggle(!(this._fields.length > 2));
                                    this._propagateSet();
                                }
                            };

                            // PRIVATE METHODS
                            EventType.prototype._onChange = function () {
                                this._expr.Value = this._input.val();
                                this._expr.NodeType = this._input.val() == "custom" ? SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT : SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD;
                                this._propagateSet();
                            };

                            EventType.prototype._createItem = function (select, text, value) {
                                var o = new Option(text, value);
                                $(o).html(text);
                                select.append(o);
                            };
                            return EventType;
                        })();
                        Constant.EventType = EventType;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
