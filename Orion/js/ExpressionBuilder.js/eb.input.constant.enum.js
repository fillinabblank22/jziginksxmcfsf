﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var Enum = (function (_super) {
                            __extends(Enum, _super);
                            function Enum(config) {
                                _super.call(this, config);
                                this.operatorsTemplate = "STARTWITH NOTSTARTWITH ENDWITH NOTENDWITH CONTAINS NOTCONTAINS";

                                this._ds = this._options.globalOptions.dataSource;
                                this._extraRequestParams = this._options.globalOptions.extraRequestParams;
                                this._firstTime = true && !!this._expr.Value; // I know. I just want to point out what I really mean
                                this._input = $('<select />').addClass('neb-styled-combo');
                                this._input.addClass("comboEnum");
                                this._inputText = $('<input type="text"/>').addClass("textEnum");
                                this._input.prop('disabled', !(this._expr.Value || this._options.enabledWhenEmpty));
                                this._options.renderTo.append(this._inputText, this._input);
                                this.operatorRefId = "";
                            }
                            Enum.prototype.setFilter = function (masterField) {
                                var _this = this;
                                this._input.prop('disabled', true);

                                if (masterField) {
                                    var param = {
                                        FieldRefID: masterField.RefID.Data,
                                        ObjectType: this._options.globalOptions.dataSource.ObjectType
                                    };

                                    SW.Core.Services.callControllerAction(this._options.globalOptions.dataProviderUrl, "GetFieldEnumValues", param, function (res) {
                                        _this._input.empty();
                                        $.each(res, function (index, item) {
                                            _this._createItem(_this._input, item.DisplayName, item.RefId);
                                        });

                                        if (_this._firstTime) {
                                            _this._firstTime = false;
                                            _this._inputText.val(_this._expr.Value);
                                            _this._input.val(_this._expr.Value);
                                        }

                                        _this._input.prop('disabled', false);

                                        var func = function () {
                                            if ((_this.operatorsTemplate.indexOf(_this.operatorRefId) < 0) && _this._input.parents(".neb-event").length == 0) {
                                                _this._input.customSelect();
                                            }
                                        };

                                        setTimeout(func, 0);
                                    }, function () {
                                        $.error("Can't load dropdown data");
                                    });
                                }
                            };

                            Enum.prototype.operatorChanged = function (operator) {
                                this.operatorRefId = operator.RefID;
                                if (this.operatorsTemplate.indexOf(operator.RefID) > -1) {
                                    this._options.renderTo.find(".comboEnum").hide();
                                    this._options.renderTo.find(".textEnum").show();
                                } else {
                                    this._options.renderTo.find(".textEnum").hide();
                                    this._options.renderTo.find(".comboEnum").show();
                                    this._options.renderTo.find(".comboEnum").css('display', '');
                                    if (this._input.parents(".neb-event").length == 0) {
                                        this._input.customSelect();
                                    }
                                }
                            };

                            Enum.prototype.getExpr = function () {
                                if (!this._input.prop('disabled') && this._input.children('option').length > 0) {
                                    if (this.operatorsTemplate.indexOf(this.operatorRefId) > -1) {
                                        this._expr.Value = this._inputText.val();
                                    } else {
                                        this._expr.Value = this._input.val();
                                    }
                                }
                                return this._expr;
                            };

                            Enum.prototype._createItem = function (select, text, value) {
                                var o = new Option(text, value);
                                $(o).html(text);
                                select.append(o);
                            };
                            return Enum;
                        })(Constant.BaseConstant);
                        Constant.Enum = Enum;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
