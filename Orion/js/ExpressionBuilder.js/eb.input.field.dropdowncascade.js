﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Field) {
                        var DropDownCascade = (function () {
                            function DropDownCascade(config) {
                                var _this = this;
                                this._fieldsComboBoxId = "";
                                this.const_BROWSE_ALL_FIELDS = "BROWSE_ALL_FIELDS";
                                this.const_BROWSE_ALL_EVENTS = "BROWSE_ALL_EVENTS";
                                this.const_USE_MACRO_VALUE = "USE_MACRO_VALUE";
                                this.const_INSTANCE_TYPE = "<this>.";
                                this.const_AggregateDefaultValue = "NotSpecified";
                                // CONSTANTS
                                this._DEFAULT_CONFIG = {
                                    enabledWhenEmpty: false,
                                    dataTypeFilter: 0
                                };
                                this._checkAggregationSupport = function () {
                                    //Check support aggregation for selected field
                                    var isAggregationSupported = false;
                                    if (this._expr.Child) {
                                        for (var i = 0; i < this._properties.length; i++) {
                                            if (this._properties[i].RefID.Data == this._expr.Value) {
                                                isAggregationSupported = true;
                                                var selectedField = this._properties[i];
                                                if (selectedField.DataTypeInfo.SupportedAggregations) {
                                                    this._expr.Child[0].Value = this._getAggregationName(selectedField.DataTypeInfo.SupportedAggregations, selectedField.DataTypeInfo.DefaultAggregation);
                                                } else {
                                                    this._expr.Child = null;
                                                }
                                            }
                                        }
                                        if (!isAggregationSupported)
                                            this._expr.Child = null;
                                    }
                                };
                                this._updateFieldAggregation = function (field) {
                                    var _this = this;
                                    var aggregationContainer = this._fieldsComboBoxId + 'aggregation';
                                    $('#' + aggregationContainer).remove();
                                    if (!field) {
                                        return;
                                    }

                                    if (field.DataTypeInfo.SupportedAggregations) {
                                        if (!this._expr.Child) {
                                            var aggregate = {
                                                Child: [],
                                                NodeType: SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT,
                                                Value: this._getAggregationName(field.DataTypeInfo.SupportedAggregations, field.DataTypeInfo.DefaultAggregation)
                                            };
                                            this._expr.Child = [];
                                            this._expr.Child.push(aggregate);
                                        }

                                        var html = Core.String.Format('<div class="neb-aggr" id="{0}">', aggregationContainer);
                                        html += '<div class="neb-aggr-options" style="display: none">';
                                        html += '<table>';
                                        html += '<tr><td><span class="caption">data aggregation</span></td></tr>';
                                        for (var i = 0; i < field.DataTypeInfo.SupportedAggregations.length; i++) {
                                            var aggregateDetailed = field.DataTypeInfo.SupportedAggregations[i];
                                            var isSelected = this._expr.Child[0] && this._expr.Child[0].Value === aggregateDetailed.Aggregate;
                                            if (isSelected) {
                                                if (aggregateDetailed.Aggregate != this.const_AggregateDefaultValue) {
                                                    this._fieldsComboBox.setRawValue(Core.String.Format('@{R=Core.Strings.2; K=WEBJS_VL0_10; E=js}', aggregateDetailed.DisplayName, field.DisplayName));
                                                } else {
                                                    this._fieldsComboBox.setRawValue(field.DisplayName);
                                                }
                                            }
                                            html += Core.String.Format('<tr><td><label><input type="radio" name="{0}aggroption" value="{1}" {2}>{3}</label></td></tr>', this._fieldsComboBoxId, field.DataTypeInfo.SupportedAggregations[i].Aggregate, isSelected ? 'checked' : '', field.DataTypeInfo.SupportedAggregations[i].DisplayName);
                                        }
                                        html += '</table>';
                                        html += '</div></div>';
                                        $('#' + this._fieldsComboBoxId).after($(html));
                                        $('#' + aggregationContainer + ' :radio').change(function (e) {
                                            var numberOfAggregation = e.target.value;
                                            if (numberOfAggregation != _this.const_AggregateDefaultValue) {
                                                for (var i = 0; i < field.DataTypeInfo.SupportedAggregations.length; i++) {
                                                    var suportedAggregateDetailed = field.DataTypeInfo.SupportedAggregations[i];
                                                    if (numberOfAggregation === suportedAggregateDetailed.Aggregate)
                                                        _this._fieldsComboBox.setRawValue(Core.String.Format('@{R=Core.Strings.2; K=WEBJS_VL0_10; E=js}', suportedAggregateDetailed.DisplayName, field.DisplayName));
                                                }
                                            } else {
                                                _this._fieldsComboBox.setRawValue(field.DisplayName);
                                            }
                                            _this._expr.Child[0].Value = numberOfAggregation;
                                            $('.neb-aggr-options').hide();
                                            $('.neb-aggr').removeClass('neb-aggr-down');
                                        });
                                        $('#' + aggregationContainer).click(function (event) {
                                            $(event.target).closest('.neb-aggr').addClass('neb-aggr-down');
                                            $($(event.target).children()[0]).show();
                                        });
                                    }
                                };
                                this._propagateSet = function (field) {
                                    if (typeof (this._options.onSet) === 'function') {
                                        this._options.onSet(field);
                                    }
                                };
                                this._options = $.extend({}, this._DEFAULT_CONFIG, config);
                                this._properties = [];
                                this._events = [];
                                this._groups = [];
                                this._expr = this._options.expr;
                                this._container = this._options.exprItem;

                                this._ds = this._options.globalOptions.dataSource;
                                this._extraRequestParams = config.globalOptions.extraRequestParams;
                                this._picker = SW.Core.Pickers.FieldPicker.Instances[config.globalOptions.fieldPickerInstanceName];
                                this._picker.SetDataSource(this._ds);
                                this._picker.SetDataTypeFilter(this._options.dataTypeFilter);

                                this._options.renderTo.css('white-space', 'nowrap');

                                this._dropdownEntity = $("<select />").addClass("neb-styled-combo");
                                this._dropdownEntity.on("change", $.proxy(this._dropdownEntityOnChange, this));
                                this._dropdownEntity.prop('disabled', !(this._expr.Value || this._options.enabledWhenEmpty));
                                this._options.renderTo.append(this._dropdownEntity);

                                this._fieldsComboBoxId = 'dropdownFields' + SW.Core.SearchComboBox.GetNewId();
                                this._dropdownFields = $("<span id='" + this._fieldsComboBoxId + "' class='sw-search-combo-box'></span>").css("padding", "0 0 0 5px").css("display", "inline-block");
                                this._options.renderTo.append(this._dropdownFields);

                                var param = {
                                    ObjectType: this._options.globalOptions.dataSource.ObjectType,
                                    OnlyDirectGroups: true
                                };

                                SW.Core.Services.callControllerAction(this._options.globalOptions.dataProviderUrl, "GetGroups", param, function (response) {
                                    if (response != null) {
                                        for (var i = 0; i < response.length; i++) {
                                            _this._groups.push(response[i]);
                                        }

                                        _this._commonGroups = $("<optgroup label='@{R=Core.Strings.2;K=WEBJS_TK0_17;E=js}'/>");
                                        _this._dropdownEntity.append(_this._commonGroups);

                                        for (var i = 0; i < _this._groups.length; i++) {
                                            if (_this._groups[i].IsVisible) {
                                                _this._createGroupItem(_this._commonGroups, _this._groups[i].DisplayName, _this._groups[i].GroupID);
                                            }
                                        }

                                        var $allGroup = $("<optgroup label='@{R=Core.Strings.2;K=WEBJS_TK0_16;E=js}'/>");
                                        _this._createGroupItem($allGroup, "@{R=Core.Strings.2;K=WEBJS_TK0_18;E=js}", _this.const_BROWSE_ALL_FIELDS);
                                        _this._dropdownEntity.append($allGroup);

                                        var entity = (!_this._expr.Value) ? _this._dropdownEntity.val() : _this._expr.Value;

                                        if (_this._expr.Value && _this._dropdownEntity.find("option[value='" + _this._expr.Value + "']").length == 0) {
                                            entity = _this._getField(_this._expr.Value).GroupId;
                                        }

                                        _this._setEntityAndLoadFields(entity);

                                        var func = function () {
                                            _this._dropdownEntity.customSelect();
                                        };

                                        setTimeout(func, 0);
                                    }
                                }, function () {
                                    $.error("Can't load dropdown data");
                                });

                                // handle mousedown outside of aggregation menu
                                $("body").mousedown(function (event) {
                                    if (!($(event.target).closest('.neb-aggr').length)) {
                                        $('.neb-aggr-options').hide();
                                        $('.neb-aggr').removeClass('neb-aggr-down');
                                    }
                                });

                                return this;
                            }
                            // PUBLIC METHODS
                            DropDownCascade.prototype.getExpr = function () {
                                return this._expr;
                            };

                            DropDownCascade.prototype.validate = function () {
                                return !!this._expr.Value;
                            };

                            DropDownCascade.prototype._dropdownEntityOnChange = function () {
                                var selectedEntity = this._dropdownEntity.val();

                                if (selectedEntity == this.const_BROWSE_ALL_FIELDS) {
                                    this._dropdownEntity.val(this._dropdownEntity.find("option:first").val());
                                    this._openFieldPicker(ExpressionBuilder.Constants.FIELD_TYPE.PROPERTY);
                                    return;
                                }

                                this._options.expr.Value = null;
                                this._loadFieldsForGroup(this._options.globalOptions.dataSource.ObjectType, selectedEntity);
                            };

                            DropDownCascade.prototype.setFilter = function (masterField) {
                                if (masterField != null) {
                                    var dataTypeFilter = masterField.DataTypeInfo.DeclType;
                                    this._options.dataTypeFilter = dataTypeFilter;

                                    this._showFilteredItems();

                                    this._dropdownEntity.next().removeClass("customSelectDisabled");
                                    this._dropdownEntity.prop('disabled', false);
                                    this._dropdownFields.prop('disabled', false);
                                }
                            };

                            // PRIVATE METHODS
                            DropDownCascade.prototype._setEntityAndLoadFields = function (fieldRefId) {
                                var entityName = fieldRefId.split('|')[0];
                                if (this._dropdownEntity.find("option[value='" + entityName + "']").length > 0) {
                                    this._dropdownEntity.val(entityName).resize();
                                } else {
                                    for (var i = 0; i < this._groups.length; i++) {
                                        if (this._groups[i].GroupID == entityName && this._groups[i].IsVisible == false) {
                                            this._createGroupItem(this._commonGroups, this._groups[i].DisplayName, this._groups[i].GroupID);
                                            this._dropdownEntity.val(entityName).resize();
                                            break;
                                        }
                                    }
                                }

                                this._loadFieldsForGroup(this._options.globalOptions.dataSource.ObjectType, entityName);
                            };

                            DropDownCascade.prototype._loadFieldsForGroup = function (objectType, groupId) {
                                var _this = this;
                                var paramGroup = {
                                    ObjectType: objectType,
                                    GroupID: groupId
                                };

                                SW.Core.Services.callControllerAction(this._options.globalOptions.dataProviderUrl, 'GetFieldsForGroup', paramGroup, function (res) {
                                    var r = res;
                                    _this._properties = [];
                                    _this._events = [];
                                    _this._instanceOf = null;

                                    for (var i = 0; i < r.length; i++) {
                                        if (r[i].FieldType == ExpressionBuilder.Constants.FIELD_TYPE.EVENT) {
                                            if (!_this._options.globalOptions.showOnlyNonTransientFields)
                                                _this._events.push(r[i]);

                                            continue;
                                        } else if (r[i].FieldType == ExpressionBuilder.Constants.FIELD_TYPE.INSTANCE) {
                                            _this._instanceOf = r[i];
                                        } else {
                                            if (_this._options.globalOptions.showOnlyNonTransientFields && r[i].DataTypeInfo.IsTransient)
                                                continue;

                                            _this._properties.push(r[i]);
                                        }
                                    }
                                    _this._showFilteredItems();
                                }, function () {
                                    $.error("Can't load column data");
                                });
                            };

                            DropDownCascade.prototype._showFilteredItems = function () {
                                var _this = this;
                                var prevValue = this._expr.Value;
                                var prevValueFound = false;

                                var fields = [];

                                if (this._properties.length > 0) {
                                    for (var i = 0; i < this._properties.length; i++) {
                                        if (this._properties[i].DataTypeInfo.IsFavorite && !this._properties[i].DataTypeInfo.IsCustomProperty && (!this._options.dataTypeFilter || this._properties[i].DataTypeInfo.DeclType == this._options.dataTypeFilter)) {
                                            fields.push({ data: this._properties[i].RefID.Data, displayName: this._properties[i].DisplayName, groupName: "<img src = \"/Orion/images/Reports/Star-small.png\" style=\" padding-right:3px;\" >" + "@{R=Core.Strings.2;K=WEBJS_TK0_2;E=js}", visible: true, searchable: true }); //Favorite Fields
                                            if (this._properties[i].RefID.Data == prevValue) {
                                                prevValueFound = true;
                                            }
                                        }
                                    }
                                }

                                if (this._events.length > 0 && this._options.side == ExpressionBuilder.Constants.EXPR_SIDE.LEFT) {
                                    for (var i = 0; i < this._events.length; i++) {
                                        if (this._events[i].DataTypeInfo.IsFavorite) {
                                            fields.push({ data: this._events[i].RefID.Data, displayName: this._events[i].DisplayName, groupName: "<img src = \"/Orion/images/Reports/Star-small.png\" style=\" padding-right:3px;\" >" + "@{R=Core.Strings.2;K=WEBJS_TK0_3;E=js}", visible: true, searchable: true }); // Favorite Events
                                        }
                                    }
                                }

                                if (this._properties.length > 0) {
                                    for (var i = 0; i < this._properties.length; i++) {
                                        if (this._properties[i].DataTypeInfo.IsCustomProperty && this._properties[i].DataTypeInfo.IsFavorite && (!this._options.dataTypeFilter || this._properties[i].DataTypeInfo.DeclType == this._options.dataTypeFilter)) {
                                            fields.push({ data: this._properties[i].RefID.Data, displayName: this._properties[i].DisplayName, groupName: "<img src = \"/Orion/images/Reports/Star-small.png\" style=\" padding-right:3px;\" >" + "@{R=Core.Strings.2;K=WEBJS_AY0_4;E=js}", visible: true, searchable: true }); //Favorite custom properties
                                            if (this._properties[i].RefID.Data == prevValue) {
                                                prevValueFound = true;
                                            }
                                        }
                                    }
                                }

                                if (this._properties.length > 0) {
                                    for (var i = 0; i < this._properties.length; i++) {
                                        if (this._properties[i].DataTypeInfo.IsRecommendedForAlerting && !this._properties[i].DataTypeInfo.IsCustomProperty && (!this._options.dataTypeFilter || this._properties[i].DataTypeInfo.DeclType == this._options.dataTypeFilter)) {
                                            fields.push({ data: this._properties[i].RefID.Data, displayName: this._properties[i].DisplayName, groupName: "@{R=Core.Strings;K=WEBJS_TK0_37;E=js}", visible: true, searchable: true }); //Recommended Fields
                                            if (this._properties[i].RefID.Data == prevValue) {
                                                prevValueFound = true;
                                            }
                                        }
                                    }
                                }

                                if (this._properties.length > 0) {
                                    for (var i = 0; i < this._properties.length; i++) {
                                        if (this._properties[i].DataTypeInfo.IsCustomProperty && this._properties[i].DataTypeInfo.IsRecommendedForAlerting && (!this._options.dataTypeFilter || this._properties[i].DataTypeInfo.DeclType == this._options.dataTypeFilter)) {
                                            fields.push({ data: this._properties[i].RefID.Data, displayName: this._properties[i].DisplayName, groupName: "@{R=Core.Strings.2;K=WEBJS_TK0_1;E=js}", visible: true, searchable: true }); //Custom properties
                                            if (this._properties[i].RefID.Data == prevValue) {
                                                prevValueFound = true;
                                            }
                                        }
                                    }
                                }

                                if (this._events.length > 0 && this._options.side == ExpressionBuilder.Constants.EXPR_SIDE.LEFT) {
                                    for (var i = 0; i < this._events.length; i++) {
                                        if (this._events[i].DataTypeInfo.IsRecommendedForAlerting) {
                                            fields.push({ data: this._events[i].RefID.Data, displayName: this._events[i].DisplayName, groupName: "@{R=Core.Strings;K=WEBJS_TK0_38;E=js}", visible: true, searchable: true }); // Recommended Events
                                        }
                                    }
                                }

                                for (var i = 0; i < this._properties.length; i++) {
                                    if ((!this._options.dataTypeFilter || this._properties[i].DataTypeInfo.DeclType == this._options.dataTypeFilter)) {
                                        var name = this._properties[i].DisplayName;
                                        if (this._properties[i].DataTypeInfo.IsCustomProperty && !this._properties[i].DataTypeInfo.IsRecommendedForAlerting) {
                                            name += ' (@{R=Core.Strings.2;K=WEBJS_TK0_1;E=js})';
                                        }

                                        fields.push({ data: this._properties[i].RefID.Data, displayName: name, groupName: "@{R=Core.Strings.2;K=WEBJS_AY0_1;E=js}", visible: false, searchable: true }); //All Fields
                                    }
                                }

                                for (var i = 0; i < this._events.length; i++) {
                                    fields.push({ data: this._events[i].RefID.Data, displayName: this._events[i].DisplayName, groupName: "@{R=Core.Strings.2;K=WEBJS_AY0_2;E=js}", visible: false, searchable: true }); // All Events
                                }

                                fields.push({ data: this.const_BROWSE_ALL_FIELDS, displayName: "<span style='color: #336699'>&raquo; @{R=Core.Strings;K=WEBJS_TK0_42;E=js}<span>", groupName: "@{R=Core.Strings;K=WEBJS_TK0_40;E=js}", visible: true, searchable: false }); //Browse all fields... , All

                                if (this._options.side == ExpressionBuilder.Constants.EXPR_SIDE.LEFT && !this._options.globalOptions.showOnlyNonTransientFields)
                                    fields.push({ data: this.const_BROWSE_ALL_EVENTS, displayName: "<span style='color: #336699'>&raquo; @{R=Core.Strings;K=WEBJS_TK0_43;E=js}</span>", groupName: "@{R=Core.Strings;K=WEBJS_TK0_40;E=js}", visible: true, searchable: false }); //Browse all events... ,All

                                if (this._options.side == ExpressionBuilder.Constants.EXPR_SIDE.LEFT && !this._options.globalOptions.showOnlyNonTransientFields) {
                                    fields.push({ data: this.const_USE_MACRO_VALUE, displayName: "@{R=Core.Strings;K=WEBJS_TK0_44;E=js}", groupName: "@{R=Core.Strings;K=WEBJS_TK0_41;E=js}", visible: true, searchable: true }); // Use value/macro, Advanced
                                }

                                if (this._options.side == ExpressionBuilder.Constants.EXPR_SIDE.LEFT && this._instanceOf) {
                                    fields.push({ data: this._instanceOf.RefID.Data, displayName: "@{R=Core.Strings.2;K=WEBJS_AY0_5;E=js}", groupName: "@{R=Core.Strings;K=WEBJS_TK0_41;E=js}", visible: true, searchable: true }); //Instance
                                }

                                if (!this._fieldsComboBox) {
                                    this._fieldsComboBox = SW.Core.SearchComboBox.Init({
                                        containerId: this._fieldsComboBoxId,
                                        emptyText: '@{R=Core.Strings;K=WEBJS_JP2_2;E=js}',
                                        data: fields,
                                        editable: false,
                                        sessionKey: this._dropdownEntity.val(),
                                        recentlyUsedConfig: {
                                            enableRecentlyUsed: true,
                                            ItemCounts: 5,
                                            recentlyUsedCaption: "@{R=Core.Strings;K=WEBJS_TK0_39;E=js}"
                                        },
                                        onSelect: function () {
                                            _this._dropdownFieldsOnChange();
                                            _this._validateInputValue();
                                        },
                                        onSearch: function (searchTerm) {
                                            _this._fieldsComboBox.collapse();
                                            _this._openFieldPicker(ExpressionBuilder.Constants.FIELD_TYPE.PROPERTY, searchTerm);
                                        }
                                    });
                                } else {
                                    this._validateInputValue();
                                    this._fieldsComboBox.updateSessionKey(this._dropdownEntity.val());
                                    var updatedFields = this._fieldsComboBox.updateRecentlyUsedData(fields);
                                    this._fieldsComboBox.updateStore(updatedFields);
                                }

                                // set previously selected field if it's still visible with new filter
                                if (prevValueFound) {
                                    this._fieldsComboBox.setValue(prevValue);
                                } else {
                                    if (prevValue != null && prevValue != "") {
                                        var field = this._getField(prevValue);
                                        this._properties.push(field);
                                        this._fieldsComboBox.insertIfNotExists({ data: prevValue, displayName: field.DisplayName + ' (' + field.OwnerDisplayName + ')', groupName: "@{R=Core.Strings.2;K=WEBJS_AY0_1;E=js}", visible: true, searchable: true }, true); //All Field
                                        this._fieldsComboBox.setValue(prevValue);
                                    } else {
                                        this._fieldsComboBox.setValue("");
                                    }
                                }

                                var selectedField;
                                for (var i = 0; i < this._properties.length; i++) {
                                    if (this._properties[i].RefID.Data == this._expr.Value) {
                                        selectedField = this._properties[i];
                                        break;
                                    }
                                }

                                if (selectedField == null || selectedField == 'undefined') {
                                    if (this._expr.Value) {
                                        field = this._getField(this._expr.Value);
                                    }
                                }

                                this._updateFieldAggregation(selectedField);

                                this._propagateSet(selectedField);
                            };

                            DropDownCascade.prototype._validateInputValue = function () {
                                var input = this._container.find(".neb-rhs input");
                                if (input != null)
                                    input.trigger("onSelectField");
                            };

                            DropDownCascade.prototype._getField = function (fieldRefId) {
                                var param = {
                                    ObjectType: this._options.globalOptions.dataSource.ObjectType,
                                    FieldRefID: fieldRefId
                                };

                                var field;

                                SW.Core.Services.callControllerActionSync(this._options.globalOptions.dataProviderUrl, 'GetField', param, function (r) {
                                    field = r;
                                }, function () {
                                    $.error("Can't load column data");
                                });

                                return field;
                            };

                            DropDownCascade.prototype._dropdownFieldsOnChange = function () {
                                var prevValue = this._expr.Value;
                                var newValue = this._fieldsComboBox.getValue();

                                if (newValue == this.const_BROWSE_ALL_FIELDS) {
                                    this._fieldsComboBox.setValue(prevValue);
                                    this._openFieldPicker(ExpressionBuilder.Constants.FIELD_TYPE.PROPERTY);
                                } else if (newValue == this.const_BROWSE_ALL_EVENTS) {
                                    this._fieldsComboBox.setValue(prevValue);
                                    this._openFieldPicker(ExpressionBuilder.Constants.FIELD_TYPE.EVENT);
                                } else if (newValue == this.const_USE_MACRO_VALUE) {
                                    this._options.transformTo(ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT, null);
                                } else if (newValue.indexOf('[updateEvent]') == 0 || newValue.indexOf('[createEvent]') == 0 || newValue.indexOf('[deleteEvent]') == 0) {
                                    this._options.transformTo(ExpressionBuilder.Constants.RULE_TYPE.EVENT, newValue);
                                    this._picker.SetFieldTypeValue(ExpressionBuilder.Constants.FIELD_TYPE.EVENT, !this._options.globalOptions.showOnlyNonTransientFields);
                                } else if (newValue.indexOf(this.const_INSTANCE_TYPE) == 0) {
                                    this._expr.Value = newValue;
                                    this._picker.SetFieldTypeValue(ExpressionBuilder.Constants.FIELD_TYPE.PROPERTY, !this._options.globalOptions.showOnlyNonTransientFields);
                                    this._expr.Child = null;
                                    this._showFilteredItems();
                                } else {
                                    this._expr.Value = newValue;
                                    this._expr.NodeType = ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD;
                                    this._picker.SetFieldTypeValue(ExpressionBuilder.Constants.FIELD_TYPE.PROPERTY, !this._options.globalOptions.showOnlyNonTransientFields);
                                    this._checkAggregationSupport();
                                    this._showFilteredItems();
                                }

                                // Add field to Recently Used
                                if (newValue == this.const_BROWSE_ALL_FIELDS || newValue == this.const_BROWSE_ALL_EVENTS || newValue == this.const_USE_MACRO_VALUE) {
                                    this._fieldsComboBox.stopAutoUpdateRecentlyUsed();
                                }
                            };

                            DropDownCascade.prototype._getAggregationName = function (aggregations, aggregationValue) {
                                for (var i = 0; i < aggregations.length; i++) {
                                    if (aggregations[i].AggregateValue === aggregationValue)
                                        return aggregations[i].Aggregate;
                                }
                            };

                            DropDownCascade.prototype._createGroupItem = function (select, text, value) {
                                var o = new Option(text, value);
                                $(o).html(text);
                                select.append(o);
                            };

                            DropDownCascade.prototype._openFieldPicker = function (type, searchTerm) {
                                this._picker.SetOnSelected($.proxy(this._onSetPicker, this));
                                this._picker.SetGroupValue(this._dropdownEntity.val());

                                var currentFieldID = null;

                                if (searchTerm) {
                                    this._picker.SetSearchTerm(searchTerm);
                                    var picker = this._picker;
                                    this._picker.SetOnFieldsLoaded(function () {
                                        picker.SetSerchingMode();
                                        picker.SetOnFieldsLoaded(null);
                                    });
                                    // TODO :
                                    // if searchTerm is filled, then FieldPicker will be opened with focus in search field(which will contain searchTerm)
                                    // (after FieldPicker is opened, search should be applied)
                                } else {
                                    if (this._expr && this._expr.Value) {
                                        currentFieldID = this._expr.Value;
                                    }
                                }

                                this._picker.SetFieldTypeValue(type, !this._options.globalOptions.showOnlyNonTransientFields);

                                var context = this;
                                this._picker.ComputeAndSetNavigationPathFilterAsync(this._options.getAllFields(), currentFieldID, function () {
                                    context._picker.ShowInDialog();
                                    context._picker.Reload();
                                });

                                return false;
                            };

                            DropDownCascade.prototype._isFieldPresentInDropDown = function (fieldRefId) {
                                var _this = this;
                                var foundInCurrentlyDisplayed = false;
                                this._dropdownFields.find("option").each(function (index, elem) {
                                    if ($(elem).val() == _this._expr.Value) {
                                        foundInCurrentlyDisplayed = true;
                                    }
                                });
                                return foundInCurrentlyDisplayed;
                            };

                            DropDownCascade.prototype._onSetPicker = function (picker) {
                                if (picker.GetSelectedFields()[0]) {
                                    var field = picker.GetSelectedFields()[0].Field;
                                    this._expr.Value = field.RefID.Data;

                                    this._expr.Child = [];
                                    if (field.FieldType == ExpressionBuilder.Constants.FIELD_TYPE.PROPERTY) {
                                        if (field.DataTypeInfo.hasOwnProperty('DefinedAggregation')) {
                                            var aggregate = {
                                                Child: [],
                                                NodeType: SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT,
                                                Value: this._getAggregationName(field.DataTypeInfo.SupportedAggregations, field.DataTypeInfo.DefinedAggregation)
                                            };

                                            this._expr.Child.push(aggregate);
                                        }

                                        this._setEntityAndLoadFields(field.GroupId);

                                        this._fieldsComboBox.insertIfNotExists({ data: field.RefID.Data, displayName: field.DisplayName + ' (' + field.OwnerDisplayName + ')', groupName: "@{R=Core.Strings.2; K=WEBJS_AY0_1; E=js}", visible: true, searchable: true }, false); //All Fields
                                        this._fieldsComboBox.saveRecentlyUsed(field.RefID.Data);
                                        this._fieldsComboBox.setValue(field.RefID.Data);
                                    } else if (field.FieldType == ExpressionBuilder.Constants.FIELD_TYPE.EVENT) {
                                        this._fieldsComboBox.insertIfNotExists({ data: field.RefID.Data, displayName: field.DisplayName + ' (' + field.OwnerDisplayName + ')', groupName: "@{R=Core.Strings.2; K=WEBJS_AY0_2; E=js}", visible: true, searchable: true }, false); //All Events
                                        this._fieldsComboBox.saveRecentlyUsed(field.RefID.Data);
                                        this._options.transformTo(ExpressionBuilder.Constants.RULE_TYPE.EVENT, field.RefID.Data);
                                    } else if (field.FieldType == ExpressionBuilder.Constants.FIELD_TYPE.INSTANCE) {
                                        this._fieldsComboBox.insertIfNotExists({ data: field.RefID.Data, displayName: field.DisplayName, groupName: "@{R=Core.Strings.2; K=WEBJS_AY0_1; E=js}", visible: true, searchable: true }, false); //All Fields
                                        this._fieldsComboBox.saveRecentlyUsed(field.RefID.Data);
                                        this._fieldsComboBox.setValue(field.RefID.Data);
                                    }

                                    if (typeof (this._options.onSet) === 'function') {
                                        this._options.onSet(field);
                                    }
                                } else {
                                    // we need reload fields in dropdown because of update of favorites
                                    this._setEntityAndLoadFields(this._dropdownEntity.val());
                                }
                            };
                            return DropDownCascade;
                        })();
                        Field.DropDownCascade = DropDownCascade;
                    })(Input.Field || (Input.Field = {}));
                    var Field = Input.Field;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
