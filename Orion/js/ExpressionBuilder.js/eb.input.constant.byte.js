/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var Byte = (function (_super) {
                            __extends(Byte, _super);
                            function Byte(config) {
                                _super.call(this, config);
                                this._input = $('<input type="text" />');

                                this._units = $("<span />");
                                this._units.addClass('neb-units');

                                this._input.blur($.proxy(this.lostFocus, this));
                                this._input.on("onSelectField", $.proxy(this.lostFocus, this));
                                this._input.val(this._expr.Value || '');
                                this._input.prop('disabled', !(this._expr.Value || this._options.enabledWhenEmpty));
                                this._options.renderTo.append(this._input);
                                this._input.after(this._units);
                            }
                            Byte.prototype.validate = function () {
                                return this._isByte(this._input.val());
                            };

                            Byte.prototype._isByte = function (n) {
                                return !isNaN(parseInt(n)) && isFinite(n) && n >= 0 && n <= 255;
                            };
                            return Byte;
                        })(Constant.BaseConstant);
                        Constant.Byte = Byte;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
