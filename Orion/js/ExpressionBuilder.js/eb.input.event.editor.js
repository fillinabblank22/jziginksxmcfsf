/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.models.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Event) {
                        var Editor = (function () {
                            function Editor(config) {
                                this._DEFAULT_CONFIG = {};
                                this._mustMatchFilterChanged = function (instance) {
                                    return function () {
                                        instance._divFilterItemContainer.toggle(this.checked);
                                    };
                                };
                                this._eventItemAddHandler = function (instance) {
                                    return function () {
                                        $(this).off('click');
                                        $(this).removeClass("neb-event-filter-item-add");
                                        var container = $(this).data('container');
                                        var addButtonTd = instance._renderEventFilterItem(container, Input.Helper.GetRuleExpr(ExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT, instance._params.globalOptions));
                                        addButtonTd.addClass("neb-event-filter-item-add");
                                        addButtonTd.on("click", instance._eventItemAddHandler(instance));
                                    };
                                };
                                this._eventItemRemoveHandler = function (instance) {
                                    return function () {
                                        var trItem = $(this).data('container');
                                        var table = trItem.parent();

                                        if (table.children("tr").size() > 1) {
                                            if (trItem.children("td .neb-event-filter-item-add").size() > 0) {
                                                var addItem = trItem.prev().children().last();
                                                addItem.addClass('neb-event-filter-item-add');
                                                addItem.on("click", instance._eventItemAddHandler(instance));
                                            }
                                            trItem.remove();
                                        }
                                    };
                                };
                                this._params = $.extend({}, this._DEFAULT_CONFIG, config);
                                this._expr = config.expr;

                                this._ds = this._params.globalOptions.dataSource;
                                this._extraRequestParams = config.globalOptions.extraRequestParams;
                                this._picker = SW.Core.Pickers.FieldPicker.Instances[config.globalOptions.fieldPickerInstanceName];
                                this._picker.SetDataSource(this._ds);

                                this._loadAndRenderEvent();

                                return this;
                            }
                            Editor.prototype._loadAndRenderEvent = function () {
                                var _this = this;
                                var param = {
                                    ObjectType: this._params.globalOptions.dataSource.ObjectType,
                                    FieldRefID: this._params.eventRefId
                                };

                                this._renderEventEditorDetailButton();

                                SW.Core.Services.callControllerAction(this._params.globalOptions.dataProviderUrl, "GetField", param, function (response) {
                                    _this._event = response;
                                    _this._renderEventEditorDetail();
                                    _this._renderEventEditorTitle();

                                    if ((_this._params.firstLoad != undefined && _this._params.firstLoad != null && _this._params.firstLoad == true) || _this._checkBoxMustMatch.is(':checked')) {
                                        _this._detailButton.click();
                                    }
                                }, function () {
                                    $.error("Can't load event data");
                                });
                            };

                            Editor.prototype._openFieldPicker = function () {
                                this._picker.SetOnSelected($.proxy(this._onSetPicker, this));
                                this._picker.SetGroupValue(this._params.globalOptions.dataSource.MasterEntity);
                                this._picker.SetFieldTypeValue(ExpressionBuilder.Constants.FIELD_TYPE.EVENT, !this._params.globalOptions.showOnlyNonTransientFields);
                                var currentFieldID = null;
                                if (this._expr && this._expr.Value) {
                                    currentFieldID = this._expr.Value;
                                }

                                var context = this;
                                this._picker.ComputeAndSetNavigationPathFilterAsync(this._params.getAllFields(), currentFieldID, function () {
                                    context._picker.ShowInDialog();
                                    context._picker.Reload();
                                });

                                return false;
                            };

                            Editor.prototype._onSetPicker = function (picker) {
                                if (picker.GetSelectedFields()[0]) {
                                    var field = picker.GetSelectedFields()[0].Field;

                                    //this._expr.Value = field.RefID.Data;
                                    if (field.FieldType == ExpressionBuilder.Constants.FIELD_TYPE.PROPERTY) {
                                        this._params.transformTo(ExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT, field.RefID.Data);
                                    } else if (field.FieldType == ExpressionBuilder.Constants.FIELD_TYPE.EVENT) {
                                        this._params.transformTo(ExpressionBuilder.Constants.RULE_TYPE.EVENT, field.RefID.Data);
                                    }
                                }
                            };

                            Editor.prototype._renderEventEditorTitle = function () {
                                var spanEventName, spanEventType, spanEventDesc;
                                spanEventName = $("<span class='neb-event-name'></span>");
                                spanEventName.text(this._event.DisplayName);
                                spanEventName.on('click', $.proxy(this._openFieldPicker, this));
                                spanEventType = $("<span class='neb-event-info'></span>");
                                spanEventType.text(" (" + this._event.OwnerDisplayName + " @{R=Core.Strings;K=WEBJS_TK0_45;E=js})"); //Event
                                this._spanEventDesc = $("<span class='neb-event-desc'></span>");
                                this._spanEventDesc.html(this._getEventDescription());

                                spanEventName.appendTo(this._params.renderTitleTo);
                                spanEventType.appendTo(this._params.renderTitleTo);
                                this._spanEventDesc.appendTo(this._params.renderTitleTo);
                            };

                            Editor.prototype._renderEventEditorDetailButton = function () {
                                var thiss = this;
                                this._detailButton = $("<span style='padding-left:25px;padding-right:10px;'>@{R=Core.Strings;K=WEBJS_TK0_46;E=js}</span>"); //Details
                                this._detailButton.on('click', $.proxy(this._showAndHideEditorDetail, this));
                                this._detailButton.appendTo(this._params.renderButtonTo);
                            };

                            Editor.prototype._showAndHideEditorDetail = function (event) {
                                var parent = this._params.renderButtonTo;
                                if (parent.hasClass('neb-btn-expanded')) {
                                    parent.removeClass('neb-btn-expanded');
                                    parent.addClass('neb-btn-collapsed');
                                    this._params.renderDetailTo.parent().hide();
                                    this._spanEventDesc.html(this._getEventDescription());
                                } else {
                                    parent.addClass('neb-btn-expanded');
                                    parent.removeClass('neb-btn-collapsed');
                                    this._params.renderDetailTo.parent().show();
                                    this._spanEventDesc.html('');
                                }
                            };

                            Editor.prototype._renderEventEditorDetail = function () {
                                var table, trMustHappend, tdMustHappend, trMustMatchFilter, tdMustMatchFilter;

                                table = $('<table style="margin-top:5px;margin-left:15px"></table>');

                                trMustHappend = $('<tr class="neb-event-detail-mhr"></tr>').appendTo(table);

                                if (!this._params.globalOptions.correlationEngine) {
                                    trMustHappend.hide();
                                }

                                tdMustHappend = $('<td></td>').appendTo(trMustHappend);
                                this._tdMustHappendType = $('<td></td>').appendTo(trMustHappend);
                                this._tdMustHappendCount = $('<td></td>').appendTo(trMustHappend);
                                this._tdMustHappendUnit = $('<td><span style="padding-left:5px;">@{R=Core.Strings;K=WEBJS_TK0_52;E=js}</span></td>').appendTo(trMustHappend); //times

                                this._renderSelectMustHappend(tdMustHappend);
                                this._renderSelectMustHappendType(this._tdMustHappendType);
                                this._renderTextBoxMustHappendCount(this._tdMustHappendCount);

                                trMustMatchFilter = $('<tr class="neb-event-detail-mmf"></tr>').appendTo(table);
                                tdMustMatchFilter = $('<td colspan="4"></td>').appendTo(trMustMatchFilter);

                                this._renderCheckBoxMustMatchFilter(tdMustMatchFilter);
                                table.appendTo(this._params.renderDetailTo);

                                this._divFilterItemContainer = $("<div class='neb-event-filter-container'></div>").appendTo(this._params.renderDetailTo);
                                if (!this._checkBoxMustMatch.is(':checked'))
                                    this._divFilterItemContainer.hide();

                                this._renderMustMatchFilter(this._divFilterItemContainer);
                            };

                            Editor.prototype._renderSelectMustHappend = function (renderTo) {
                                this._selectMustHappend = $('<select class="neb-event-musthappend">').appendTo(renderTo);
                                this._selectMustHappend.change($.proxy(this._selectMustHappendChanged, this));

                                $('<option value="true" ' + (this._params.mustHappend == true ? 'selected="selected"' : '') + '>@{R=Core.Strings;K=WEBJS_TK0_48;E=js}</option>').appendTo(this._selectMustHappend); //Event must occur
                                $('<option value="false" ' + (this._params.mustHappend == false ? 'selected="selected"' : '') + '>@{R=Core.Strings;K=WEBJS_TK0_49;E=js}</option>').appendTo(this._selectMustHappend); //Event must NOT occur

                                this._selectMustHappendChanged(null);
                            };

                            Editor.prototype._selectMustHappendChanged = function (event) {
                                if (this._selectMustHappend.val() == "true") {
                                    this._tdMustHappendType.show();
                                    this._tdMustHappendCount.show();
                                    this._tdMustHappendUnit.show();
                                } else {
                                    this._tdMustHappendType.hide();
                                    this._tdMustHappendCount.hide();
                                    this._tdMustHappendUnit.hide();
                                }
                            };

                            Editor.prototype._renderSelectMustHappendType = function (renderTo) {
                                this._selectMustHappendType = $('<select class="neb-event-musthappendtype">').appendTo(renderTo);

                                $('<option value="0" ' + ((this._params.mustHappendType == 0) ? 'selected="selected"' : '') + '>@{R=Core.Strings;K=WEBJS_TK0_50;E=js}</option>').appendTo(this._selectMustHappendType); //More than
                                $('<option value="1" ' + ((this._params.mustHappendType == 1) ? 'selected="selected"' : '') + '>@{R=Core.Strings;K=WEBJS_TK0_51;E=js}</option>').appendTo(this._selectMustHappendType); //Less than
                            };

                            Editor.prototype._renderTextBoxMustHappendCount = function (renderTo) {
                                this._textBoxMustHappendCount = $('<input type="text" class="neb-event-musthappendcount" />').appendTo(renderTo);
                                this._textBoxMustHappendCount.blur(this._textBoxMustHappendCountBlur);

                                //
                                //  Hack:
                                //  if "at least" is selected on UI, we need to convert it to "more than"
                                //  it means we need to add 1 from "at least" count.
                                //
                                var temp = this._params.mustHappendCount;
                                if (this._params.mustHappendType == 0) {
                                    this._textBoxMustHappendCount.val((parseInt(temp) + 1).toString());
                                } else {
                                    this._textBoxMustHappendCount.val(this._params.mustHappendCount.toString());
                                }
                            };

                            Editor.prototype._textBoxMustHappendCountBlur = function () {
                                var n = $(this).val();
                                if (!isNaN(parseInt(n)) && isFinite(n) && n >= 0) {
                                    $(this).removeClass('sw-validation-input-error');
                                    $(this).attr('title', '');
                                } else {
                                    $(this).addClass('sw-validation-input-error');
                                    $(this).attr('title', '@{R=Core.Strings;K=WEBJS_TK0_56;E=js}'); //Value must be a number greater or equal to 0
                                }
                            };

                            Editor.prototype._renderCheckBoxMustMatchFilter = function (renderTo) {
                                var checkedLiteral = '';

                                var filterExpr = Input.Event.Helper.GetEventFilter(this._params.expr);
                                if (filterExpr != null && filterExpr.Child[0].Child[0].Value != null && filterExpr.Child[0].Child[0].Value != '')
                                    checkedLiteral = 'checked="checked"';

                                this._checkBoxMustMatch = $('<input type="checkbox" ' + checkedLiteral + ' class="neb-event-mustmatchfilter" />').appendTo(renderTo);
                                this._checkBoxMustMatch.on("change", this._mustMatchFilterChanged(this));
                                var textSpan = $('<span style="margin-left:5px">@{R=Core.Strings;K=WEBJS_TK0_53;E=js}</span>').appendTo(renderTo);
                            };

                            Editor.prototype._renderMustMatchFilter = function (renderTo) {
                                var tableFilter = $('<table class="neb-event-filter"></table>').appendTo(renderTo);
                                this._tableFilterItems = $('<table class="neb-event-filter-items"></table>').appendTo(renderTo);
                                var trFilterType, tdFilterType, trItem;

                                trFilterType = $('<tr></tr>').appendTo(tableFilter);
                                tdFilterType = $('<td colspan="5" class="neb-event-mustmatchfiltertype"></td>').appendTo(trFilterType);

                                this._renderSelectMustMatchFilterType(tdFilterType);
                                var mainOperator = Input.Event.Helper.GetEventFilter(this._expr);

                                var addButtonTd = null;

                                if (mainOperator == null) {
                                    addButtonTd = this._renderEventFilterItem(this._tableFilterItems, Input.Helper.GetRuleExpr(ExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT, this._params.globalOptions));
                                } else {
                                    for (var i = 0; i < mainOperator.Child.length; i++) {
                                        addButtonTd = this._renderEventFilterItem(this._tableFilterItems, mainOperator.Child[i]);
                                    }
                                }

                                addButtonTd.addClass("neb-event-filter-item-add");
                                addButtonTd.on("click", this._eventItemAddHandler(this));

                                var hide = $('<div class="neb-event-detail-hide"><span>@{R=Core.Strings;K=WEBJS_TK0_47;E=js}</span></div>').appendTo(renderTo);
                                hide.on('click', $.proxy(this._showAndHideEditorDetail, this));
                            };

                            Editor.prototype._renderEventFilterItem = function (renderTo, expr) {
                                var tr, td, addButtonTd, spanLeft, spanOp, spanRight, rhs, op, thisExpr, customExpr;
                                var lhs, customrhs, tdRightValueType, tdRightCustomValue;
                                expr = $.extend(true, {}, expr); // deep copy

                                if (expr.Child.length == 1) {
                                    //create temp rhs when operator is unary
                                    expr.Child[1] = {
                                        NodeType: SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT
                                    };
                                }

                                tr = $('<tr class="neb-event-filter-item"></tr>').appendTo(renderTo);

                                td = $('<td></td>').appendTo(tr);
                                spanLeft = $('<span class="neb-event-filter-lhs"></span>').appendTo(td);

                                td = $('<td></td>').appendTo(tr);
                                spanOp = $('<span class="neb-event-filter-op"></span>').appendTo(td);

                                td = $('<td class="neb-event-filter-rhs-column"></td>').appendTo(tr);
                                spanRight = $('<span class="neb-event-filter-rhs"></span>').appendTo(td);
                                tdRightValueType = $('<td class="neb-event-filter-rhs-value-type"></td>').appendTo(spanRight);
                                tdRightCustomValue = $('<td class="neb-event-filter-rhs-custom-value"></td>').appendTo(spanRight);

                                // delete action
                                td = $('<td class="neb-event-filter-item-close"></td>').appendTo(tr);
                                td.data('container', tr);
                                td.click(this._eventItemRemoveHandler(this));

                                // add action
                                addButtonTd = $('<td></td>').appendTo(tr);
                                addButtonTd.data('container', renderTo);

                                // 30px spacer cell on the end for quick automatic layout
                                //'<td style="width: 30px; max-width: 30px; min-width: 30px;">&nbsp;</td>'
                                var ruleType = Input.Helper.GetRuleType(expr);

                                lhs = new Input.Event.DropDown({
                                    renderTo: spanLeft,
                                    expr: expr.Child[0],
                                    exprItem: tr,
                                    event: this._event,
                                    enabledWhenEmpty: true,
                                    globalOptions: this._params.globalOptions,
                                    onSet: function (field) {
                                        op.setFilter(field);
                                        customrhs.setFilter(field);
                                        rhs.leftValueChanged(field);
                                    },
                                    side: SW.Core.Controls.ExpressionBuilder.Constants.EXPR_SIDE.LEFT
                                });

                                rhs = new Input.Constant.EventType({
                                    renderTo: tdRightValueType,
                                    event: this._event,
                                    expr: expr.Child[1],
                                    onSet: function (field) {
                                        tdRightCustomValue.toggle(field == "custom");
                                    },
                                    onGetExpr: function () {
                                        return customrhs.getExpr();
                                    },
                                    onValidate: function () {
                                        return customrhs.validate();
                                    }
                                });

                                customExpr = $.extend({}, expr.Child[1]);
                                if (customExpr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD) {
                                    customExpr.NodeType = SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT;
                                    customExpr.Value = null;
                                }
                                customrhs = Input.Helper.CreateInput({
                                    renderTo: tdRightCustomValue,
                                    exprItem: tr,
                                    event: this._event,
                                    expr: customExpr,
                                    globalOptions: this._params.globalOptions,
                                    onSet: function () {
                                    },
                                    side: SW.Core.Controls.ExpressionBuilder.Constants.EXPR_SIDE.RIGHT
                                }, this._params.globalOptions);

                                thisExpr = $.extend({}, expr); // shallow copy
                                thisExpr.Child = []; // do not pass children
                                op = new this._params.globalOptions.operatorInput({
                                    renderTo: spanOp,
                                    globalOptions: this._params.globalOptions,
                                    expr: thisExpr,
                                    leftSide: lhs,
                                    rightSide: rhs,
                                    ruleType: ruleType,
                                    onSet: function (selectedOp) {
                                        if (customrhs.operatorChanged && typeof customrhs.operatorChanged == 'function') {
                                            customrhs.operatorChanged(selectedOp);
                                        }
                                        if (rhs.operatorChanged && typeof rhs.operatorChanged == 'function') {
                                            rhs.operatorChanged(selectedOp);
                                        }
                                        spanRight.toggle(selectedOp.Type != SW.Core.Controls.ExpressionBuilder.Constants.OPERATOR_TYPE.UNARY);
                                    }
                                });

                                lhs.load();

                                if (Input.Helper.IsConstantOnTheLeft(ruleType)) {
                                    // For constant to constant/field rule we hardcode datatype to TEXT.
                                    var textField = {
                                        DataTypeInfo: { DeclType: 5 }
                                    };

                                    op.setFilter(textField);
                                    lhs.setFilter(textField);
                                    customrhs.setFilter(textField);
                                }

                                tr.data('op', op);

                                return addButtonTd;
                            };

                            Editor.prototype._renderSelectMustMatchFilterType = function (renderTo) {
                                this._selectMustMatchType = $('<select class="neb-event-mustmatchtype"></select>').appendTo(renderTo);

                                var MustMatchFilterAND = 'selected="selected"';
                                var MustMatchFilterOR = '';
                                if (this._params.filterExpression != null && this._params.filterExpression.Value == 'OR') {
                                    MustMatchFilterAND = '';
                                    MustMatchFilterOR = 'selected="selected"';
                                }

                                $('<option value="AND" ' + MustMatchFilterAND + '>@{R=Core.Strings;K=WEBJS_TK0_54;E=js}</option>').appendTo(this._selectMustMatchType); //All of the following must be true (AND)
                                $('<option value="OR" ' + MustMatchFilterOR + '>@{R=Core.Strings;K=WEBJS_TK0_55;E=js}</option>').appendTo(this._selectMustMatchType); //At least one of the following must be true (OR)
                            };

                            Editor.prototype._getEventDescription = function () {
                                var mustHappend = this._selectMustHappend.find("option:selected").text();
                                var mustHappendType = this._selectMustHappendType.find("option:selected").text().toLowerCase();
                                var mustHappendCount = this._textBoxMustHappendCount.val();

                                var desc = " - " + mustHappend + ' ' + mustHappendType + ' ' + mustHappendCount + 'x';

                                if (this._checkBoxMustMatch.is(':checked')) {
                                    this._tableFilterItems.find(".neb-event-filter-item").each(function (index) {
                                        var filterItem = '';

                                        var childExpr = $(this).data('op').getExpr();

                                        var fieldName = $(this).find(".neb-event-filter-lhs").find("option:selected").text();

                                        var operator = childExpr.Value;

                                        if (operator.length > 2) {
                                            operator = $(this).find(".neb-event-filter-op").find("option:selected").text();
                                        }

                                        var value;
                                        var valuetext = '';

                                        $(this).find(".neb-event-filter-rhs-value-type").find("select").each(function (index) {
                                            value = $(this).find("option:selected");
                                            valuetext = value.text();
                                        });

                                        if (value.val() == 'custom') {
                                            $(this).find(".neb-event-filter-rhs-custom-value").find("select").each(function (index) {
                                                value = $(this).find("option:selected");
                                                valuetext = value.text();
                                            });
                                            if ((value.val() == 'custom' || valuetext == '') && childExpr.Child.length == 2)
                                                valuetext = childExpr.Child[1].Value;
                                        }

                                        filterItem = fieldName + ' ' + operator + ' ' + valuetext;

                                        desc += ', <span style="white-space:nowrap">' + filterItem + '</span>';
                                    });
                                }

                                return desc;
                            };

                            Editor.prototype.getExpr = function () {
                                var newExpr = {
                                    NodeType: ExpressionBuilder.Constants.EXPRESSION_TYPE.EVENT,
                                    Value: this._expr.Value,
                                    Child: []
                                };

                                newExpr.Child[0] = Input.Event.Helper.GetConstant(ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND, this._selectMustHappend);
                                newExpr.Child[1] = Input.Event.Helper.GetConstant(ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_TYPE, this._selectMustHappendType);

                                //
                                //  Hack:
                                //  if "at least" is selected on UI, we need to convert it to "more than"
                                //  it means we need to substract 1 from "at least" count.
                                //
                                var prevValue = this._textBoxMustHappendCount.val();

                                if (this._selectMustHappendType.val() == '0') {
                                    var count = parseInt(this._textBoxMustHappendCount.val());
                                    this._textBoxMustHappendCount.val((count - 1).toString());
                                }

                                newExpr.Child[2] = Input.Event.Helper.GetConstant(ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_COUNT, this._textBoxMustHappendCount);

                                this._textBoxMustHappendCount.val(prevValue);

                                if (this._checkBoxMustMatch.is(':checked')) {
                                    var expr = {
                                        NodeType: ExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR,
                                        Value: this._selectMustMatchType.val(),
                                        Child: []
                                    };

                                    newExpr.Child[3] = expr;

                                    this._tableFilterItems.find(".neb-event-filter-item").each(function (index) {
                                        expr.Child[index] = $(this).data('op').getExpr();
                                    });
                                }

                                this._expr = newExpr;

                                return this._expr;
                            };

                            Editor.prototype.isEmpty = function () {
                                return false;
                            };

                            Editor.prototype.validate = function () {
                                return !!this._expr.Value;
                            };
                            return Editor;
                        })();
                        Event.Editor = Editor;
                    })(Input.Event || (Input.Event = {}));
                    var Event = Input.Event;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
