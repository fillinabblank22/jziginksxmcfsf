/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />

var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Variable) {
                        var Picker = (function () {
                            function Picker(config) {
                                var _this = this;
                                this._LINK_TEXT = "@{R=Core.Strings.2;K=WEBJS_TK0_8;E=js}";
                                this._DEFAULT_CONFIG = {
                                    enabledWhenEmpty: false,
                                    dataTypeFilter: 0
                                };
                                this._options = $.extend({}, this._DEFAULT_CONFIG, config);

                                SW.Core.Services.callControllerAction(this._options.globalOptions.dataProviderUrl, 'GetGenericContext', null, function (res) {
                                    SW.Core.MacroVariablePickerController.SetMacroContexts(JSON.parse(res));
                                }, function () {
                                    $.error("Can't load column data");
                                });

                                this._uniqueId = Ext.id();

                                this._input = $('<input style="width:180px" data-form="' + this._uniqueId + '" type="text" />');
                                this._expr = this._options.expr;
                                this._link = $('<a class="neb-variablepicker" data-macro="' + this._uniqueId + '">' + this._LINK_TEXT + '</a>').on('click', function () {
                                    _this._input.val("");
                                });

                                this._options.renderTo.append(this._link);
                                this._options.renderTo.append(this._input);

                                if (this._expr.Value) {
                                    this._input.val(this._expr.Value);
                                }

                                setTimeout(function () {
                                    SW.Core.MacroVariablePickerController.EnableInsertVariableButtons();
                                }, 0);

                                return this;
                            }
                            Picker.prototype.getExpr = function () {
                                this._expr.Value = this._input.val();
                                return this._expr;
                            };

                            Picker.prototype.validate = function () {
                                return !!this._expr.Value;
                            };

                            Picker.prototype.setFilter = function (masterField) {
                            };
                            return Picker;
                        })();
                        Variable.Picker = Picker;
                    })(Input.Variable || (Input.Variable = {}));
                    var Variable = Input.Variable;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
