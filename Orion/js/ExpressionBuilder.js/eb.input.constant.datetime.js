/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\jqueryui.d.ts" />
/// <reference path="..\..\typescripts\typings\jquery.timepicker.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\underscore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};

var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var DateTime = (function (_super) {
                            __extends(DateTime, _super);
                            function DateTime(config) {
                                _super.call(this, config);

                                this._ds = this._options.globalOptions.dataSource;
                                this._field;
                                this._regionalSettings;

                                this._dateInput = $('<input type="text" class="datePicker disposable" style="width: 100px" />');
                                this._timeInput = $('<input type="text" class="timePicker disposable" style="width: 80px" />');

                                var disabled = !(this._expr.Value || this._options.enabledWhenEmpty);
                                this._dateInput.prop('disabled', disabled);
                                this._timeInput.prop('disabled', disabled);

                                this._options.renderTo.css("white-space", "nowrap");
                                this._options.renderTo.append(this._dateInput, this._timeInput);

                                this._loadRegionalSettings();
                            }
                            DateTime.prototype.getExpr = function () {
                                var dateValue = this._dateInput.val();
                                if (dateValue) {
                                    var datepart = $.datepicker.parseDate(this._regionalSettings.dateFormat, dateValue);
                                    var timepart = this._getTimepicker().getTime() || datepart;

                                    var localDate = new window.Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());

                                    var localOffset = (new window.Date().getTimezoneOffset() * 60 * 1000);

                                    this._expr.Value = new window.Date(localDate.getTime() - SW.Core.Date.ServerTimeOffset - localOffset).toISOString();
                                } else if (this._expr.Value != null && this._expr.Value != '') {
                                    return this._expr;
                                } else {
                                    this._expr.Value = '';
                                }

                                return this._expr;
                            };

                            DateTime.prototype.validate = function () {
                                return !!this._dateInput.val();
                            };

                            DateTime.prototype.setFilter = function (masterField) {
                                this._dateInput.prop('disabled', false);
                                this._timeInput.prop('disabled', false);
                            };

                            DateTime.prototype._loadRegionalSettings = function () {
                                var _this = this;
                                SW.Core.Controls.ExpressionBuilder.Input.Constant.DateTime.RegionalSettingsLoader.GetRegionalSettingsAsync(this._options.globalOptions.dataProviderUrl, function (result) {
                                    _this._regionalSettings = result;
                                    _this._initPlugins();
                                    _this._setDateTime();
                                });
                            };

                            DateTime.prototype._initPlugins = function () {
                                $.datepicker.setDefaults(this._regionalSettings);
                                this._dateInput.datepicker({ mandatory: true, closeAtTop: false });
                                this._timeInput.timePicker({ separator: this._regionalSettings.timeSeparator, show24Hours: this._regionalSettings.show24Hours });
                            };

                            DateTime.prototype._setDateTime = function () {
                                try  {
                                    if (this._expr.Value) {
                                        var date = dateFromISO8601(this._expr.Value);

                                        // Datetime is saved in UTC, we need convert it to server time.
                                        var serverDate = new window.Date(date.getTime() + SW.Core.Date.ServerTimeOffset);

                                        this._dateInput.val($.datepicker.formatDate(this._regionalSettings.dateFormat, serverDate));
                                        this._getTimepicker().setTime(serverDate);
                                    }
                                } catch (e) {
                                    // When we change field in AQB with different type we can get in expr.Value non-datetime value
                                    // In that case we just leave empty value in date/time inputs.
                                }
                            };

                            DateTime.prototype._getTimepicker = function () {
                                return $('.timePicker', this._options.renderTo)[0].timePicker;
                            };
                            return DateTime;
                        })(Constant.BaseConstant);
                        Constant.DateTime = DateTime;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));

var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        (function (DateTime) {
                            var RegionalSettingsLoader = (function () {
                                function RegionalSettingsLoader() {
                                }
                                RegionalSettingsLoader._getDefaultRegionalSettings = function () {
                                    return {
                                        dateFormat: "m/d/yy",
                                        show24Hours: false,
                                        timeSeparator: ":"
                                    };
                                };

                                RegionalSettingsLoader._loadRegionalSetting = function (dataProviderUrl) {
                                    SW.Core.Services.callControllerAction(dataProviderUrl, "GetDatePickerRegionalSettings", {}, function (res) {
                                        RegionalSettingsLoader._regionalSettings = res ? JSON.parse(res) : RegionalSettingsLoader._getDefaultRegionalSettings();
                                        RegionalSettingsLoader._pulseWaiters();
                                    }, function () {
                                        // OnError we set default regional settings.
                                        RegionalSettingsLoader._regionalSettings = RegionalSettingsLoader._getDefaultRegionalSettings();
                                        RegionalSettingsLoader._pulseWaiters();
                                    });
                                };

                                RegionalSettingsLoader._pulseWaiters = function () {
                                    _.each(RegionalSettingsLoader._waiters, function (waiter) {
                                        waiter(RegionalSettingsLoader._regionalSettings);
                                    });

                                    RegionalSettingsLoader._waiters = [];
                                };

                                RegionalSettingsLoader._getRegionalSettingsAsync = function (callback) {
                                    // Already loaded ?
                                    if (RegionalSettingsLoader._regionalSettings) {
                                        callback(RegionalSettingsLoader._regionalSettings);
                                    } else {
                                        // No, wait for server response.
                                        RegionalSettingsLoader._waiters.push(callback);
                                    }
                                };

                                RegionalSettingsLoader.GetRegionalSettingsAsync = function (dataProviderUrl, callback) {
                                    // Call function on document ready.
                                    if (!RegionalSettingsLoader._regionalSettings && RegionalSettingsLoader._alreadyRequestedLoading == false) {
                                        var func = function () {
                                            RegionalSettingsLoader._loadRegionalSetting(dataProviderUrl);
                                        };
                                        setTimeout(func, 0);

                                        RegionalSettingsLoader._alreadyRequestedLoading = true;
                                    }
                                    RegionalSettingsLoader._getRegionalSettingsAsync(callback);
                                };
                                RegionalSettingsLoader._waiters = [];
                                RegionalSettingsLoader._alreadyRequestedLoading = false;
                                return RegionalSettingsLoader;
                            })();
                            DateTime.RegionalSettingsLoader = RegionalSettingsLoader;
                        })(Constant.DateTime || (Constant.DateTime = {}));
                        var DateTime = Constant.DateTime;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));

if (!Date.prototype.toISOString) {
    (function () {
        function pad(num) {
            var r = String(num);
            if (r.length === 1) {
                r = '0' + r;
            }
            return r;
        }

        Date.prototype.toISOString = function () {
            return this.getUTCFullYear() + '-' + pad(this.getUTCMonth() + 1) + '-' + pad(this.getUTCDate()) + 'T' + pad(this.getUTCHours()) + ':' + pad(this.getUTCMinutes()) + ':' + pad(this.getUTCSeconds()) + '.' + String((this.getUTCMilliseconds() / 1000.0).toFixed(3)).slice(2, 5) + 'Z';
        };
    }());
}

function dateFromISO8601(isostr) {
    var parts = isostr.match(/\d+/g);
    return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
}
