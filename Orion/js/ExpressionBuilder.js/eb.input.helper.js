/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    var Helper = (function () {
                        function Helper() {
                        }
                        Helper.GetRuleExpr = function (ruleType, settings) {
                            for (var i = 0; i < settings.ruleTypes.length; i++) {
                                if (settings.ruleTypes[i].ruleType == ruleType)
                                    return $.extend(true, {}, settings.ruleTypes[i].expr);
                            }
                            return null;
                        };

                        Helper.CreateInput = function (config, settings) {
                            var input;

                            if (config.expr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD) {
                                input = new settings.fieldInput(config);
                            } else if (config.expr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT) {
                                input = new settings.constantInput(config);
                            } else if (config.expr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.EVENT) {
                                input = new settings.eventInput(config);
                            } else {
                                $.error('Unexpected expression type');
                            }

                            return input;
                        };

                        Helper.IsConstantOnTheLeft = function (ruleType) {
                            return ruleType === SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_FIELD || ruleType === SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT;
                        };

                        Helper.IsConstantOnTheRight = function (ruleType) {
                            return ruleType === SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT || ruleType === SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT;
                        };

                        Helper.GetRuleType = function (expr) {
                            if (expr.Child.length === 2 && (expr.Child[0].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT) && (expr.Child[1].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT)) {
                                return SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT;
                            }

                            if (expr.Child.length === 2 && (expr.Child[0].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD) && (expr.Child[1].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT)) {
                                return SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT;
                            }

                            if (expr.Child.length === 2 && (expr.Child[0].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT) && (expr.Child[1].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD)) {
                                return SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_FIELD;
                            }

                            if (expr.Child.length === 2 && (expr.Child[0].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD) && (expr.Child[1].NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD)) {
                                return SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_FIELD;
                            }

                            return SW.Core.Controls.ExpressionBuilder.Constants.RULE_TYPE.UNKNOWN;
                        };
                        return Helper;
                    })();
                    Input.Helper = Helper;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
