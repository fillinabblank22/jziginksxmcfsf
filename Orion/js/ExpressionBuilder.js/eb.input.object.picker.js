/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />

var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Object) {
                        var Picker = (function () {
                            function Picker(config) {
                                var _this = this;
                                this._entityType = null;
                                this._LINK_TEXT = "@{R=Core.Strings.2;K=WEBJS_TK0_4;E=js}";
                                this._LINK_TEXT_OBJECT = "@{R=Core.Strings.2;K=WEBJS_TK0_6;E=js}";
                                this._LINK_TEXT_OBJECTS = "@{R=Core.Strings.2;K=WEBJS_TK0_5;E=js}";
                                this._DEFAULT_CONFIG = {
                                    enabledWhenEmpty: false,
                                    dataTypeFilter: 0
                                };
                                this.setFilter = function (masterField) {
                                    var prevAData = this._expr.AdditionalData;

                                    if (prevAData && prevAData != masterField.RefID.Data) {
                                        this._expr.Child = [];
                                        this._link.text(this._LINK_TEXT + '...');
                                    }

                                    this._filter = masterField;
                                    this._options.dataTypeFilter = masterField.DataTypeInfo.DeclType;

                                    this._expr.AdditionalData = masterField.RefID.Data;

                                    if (masterField) {
                                        var refId = masterField.RefID.Data.replace('<this>.', '');
                                        var index = refId.indexOf('|');
                                        this._entityType = refId.substring(0, index);
                                    }
                                };
                                $('#btnNetObjectPickerChange').off('click').on('click', function (e) {
                                    e.preventDefault();
                                    _this._changeNetObject();
                                    _this._closeNetObjectPicker();
                                });

                                $('#btnNetObjectPickerCancel').off('click').on('click', function (e) {
                                    e.preventDefault();
                                    _this._closeNetObjectPicker();
                                });

                                this._options = $.extend({}, this._DEFAULT_CONFIG, config);
                                this._expr = this._options.expr;
                                this._ds = this._options.globalOptions.dataSource;
                                this._link = $('<a class="neb-fieldpicker"></a>').on('click', $.proxy(this._openObjectPicker, this));

                                if (this._entityType == null)
                                    this._entityType = this._options.globalOptions.dataSource.MasterEntity;

                                this._options.renderTo.append(this._link);

                                if (this._expr.Child && this._expr.Child.length > 0) {
                                    this._link.text(this._expr.Child.length + ' ' + ((this._expr.Child.length == 1) ? this._LINK_TEXT_OBJECT : this._LINK_TEXT_OBJECTS));
                                } else {
                                    this._link.text(this._LINK_TEXT + '...');
                                }

                                return this;
                            }
                            Picker.prototype.getExpr = function () {
                                return this._expr;
                            };

                            Picker.prototype.validate = function () {
                                return true;
                            };

                            Picker.prototype.operatorChanged = function (operator) {
                                var previousMode = this._mode;
                                if (operator.RefID == "ISMEMBEROF" || operator.RefID == "ISNOTMEMBEROF")
                                    this._mode = ExpressionBuilder.Constants.OBJECT_PICKER_TYPE.GROUP;
                                else {
                                    this._mode = ExpressionBuilder.Constants.OBJECT_PICKER_TYPE.OBJECT;
                                }

                                if (previousMode != undefined && this._mode != previousMode) {
                                    this._expr.Child = [];
                                    this._link.text(this._LINK_TEXT + '...');
                                }
                            };

                            Picker.prototype.getFilter = function () {
                                return this._filter;
                            };

                            // PRIVATE METHODS
                            Picker.prototype._openObjectPicker = function (event) {
                                var _this = this;
                                event.stopPropagation();
                                event.preventDefault();

                                $('#btnNetObjectPickerChange').off('click').on('click', function (e) {
                                    e.preventDefault();
                                    _this._changeNetObject();
                                    _this._closeNetObjectPicker();
                                });

                                var entityType;
                                if (this._mode == ExpressionBuilder.Constants.OBJECT_PICKER_TYPE.OBJECT) {
                                    entityType = this._entityType;
                                } else {
                                    entityType = "Orion.Groups";
                                }

                                SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: entityType, SelectionMode: 'Multiple' }], entityType, this._getAllEntities(), 'Multiple', true, false);

                                //$('#netObjectPickerPlaceHolder').remove();
                                $('#netObjectPickerPlaceHolder').prependTo('#insertNetObjectPickerDlg');

                                $('#insertNetObjectPickerDlg').dialog({
                                    position: ['middle', 100],
                                    width: 700,
                                    height: 'auto',
                                    modal: true,
                                    draggable: true,
                                    title: '@{R=Core.Strings.2;K=WEBJS_IT0_3; E=js}',
                                    show: { duration: 0 }
                                });

                                return false;
                            };

                            Picker.prototype._getAllEntities = function () {
                                var result = [];
                                if (this._expr.Child && this._expr.Child.length > 0) {
                                    for (var i = 0; i < this._expr.Child.length; i++) {
                                        result.push(this._expr.Child[i].Value);
                                    }
                                }

                                return result;
                            };

                            Picker.prototype._changeNetObject = function () {
                                var selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntitiesDefinitions();
                                if (selectedEntities.length > 0) {
                                    this._expr.Child = [];

                                    for (var i = 0; i < selectedEntities.length; i++) {
                                        var newChild = {
                                            Child: [],
                                            NodeType: SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT,
                                            Value: selectedEntities[i].uri
                                        };

                                        this._expr.Child.push(newChild);
                                    }

                                    this._link.text(this._expr.Child.length + ' ' + ((this._expr.Child.length == 1) ? this._LINK_TEXT_OBJECT : this._LINK_TEXT_OBJECTS));
                                } else {
                                    this._expr.Child = [];
                                    this._link.text(this._LINK_TEXT + '...');
                                }
                            };

                            Picker.prototype._closeNetObjectPicker = function () {
                                $('#insertNetObjectPickerDlg').dialog('close');
                            };
                            return Picker;
                        })();
                        Object.Picker = Picker;
                    })(Input.Object || (Input.Object = {}));
                    var Object = Input.Object;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
