/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Field) {
                        var Picker = (function () {
                            function Picker(config) {
                                var _this = this;
                                this._LINK_TEXT = "@{R=Core.Strings;K=WEBJS_JP2_2;E=js}";
                                this._DEFAULT_CONFIG = {
                                    enabledWhenEmpty: false,
                                    dataTypeFilter: 0
                                };
                                this.setFilter = function (masterField) {
                                    this._filter = masterField;
                                    var dataTypeFilter = masterField.DataTypeInfo.DeclType;
                                    this._options.dataTypeFilter = dataTypeFilter;
                                    this._picker.SetDataTypeFilter(this._options.dataTypeFilter);
                                    this._link.removeClass('neb-fieldpicker-disabled');
                                    if (this._field) {
                                        //check if current field conforms the new filter
                                        if (this._field.DataTypeInfo.DeclType != this._options.dataTypeFilter) {
                                            this._field = null;
                                            this._expr.Value = '';
                                            this._link.text(this._LINK_TEXT);
                                        }
                                    }
                                };
                                this._options = $.extend({}, this._DEFAULT_CONFIG, config);
                                this._expr = this._options.expr;
                                this._ds = this._options.globalOptions.dataSource;
                                this._extraRequestParams = config.globalOptions.extraRequestParams;
                                this._picker = SW.Core.Pickers.FieldPicker.Instances[config.globalOptions.fieldPickerInstanceName];
                                this._picker.SetDataSource(this._ds);
                                this._picker.SetDataTypeFilter(this._options.dataTypeFilter);
                                this._link = $('<a class="neb-fieldpicker"></a>').on('click', $.proxy(this._openFieldPicker, this));
                                this._link.toggleClass('neb-fieldpicker-disabled', !(this._expr.Value || this._options.enabledWhenEmpty));
                                this._options.renderTo.append(this._link);

                                if (this._expr.Value) {
                                    // loading existing data, need to get full Field to get DisplayName
                                    var param = {
                                        ObjectType: this._options.globalOptions.dataSource.ObjectType,
                                        FieldRefID: this._expr.Value
                                    };

                                    SW.Core.Services.callControllerAction(this._options.globalOptions.dataProviderUrl, 'GetField', param, function (res) {
                                        _this._field = res;
                                        _this._onChange();
                                    }, function () {
                                        $.error("Can't load column data");
                                    });
                                } else {
                                    this._link.text(this._LINK_TEXT);
                                }

                                return this;
                            }
                            Picker.prototype.getExpr = function () {
                                return this._expr;
                            };

                            Picker.prototype.validate = function () {
                                return !!this._expr.Value;
                            };

                            Picker.prototype.getFilter = function () {
                                return this._filter;
                            };

                            // PRIVATE METHODS
                            Picker.prototype._onChange = function () {
                                this._expr.Value = this._field.RefID.Data;
                                this._link.text(this._field.DisplayName);
                                if (typeof (this._options.onSet) === 'function') {
                                    this._options.onSet(this._field);
                                }
                            };

                            Picker.prototype._openFieldPicker = function (event) {
                                event.stopPropagation();
                                event.preventDefault();

                                if (!this._link.hasClass('neb-fieldpicker-disabled')) {
                                    this._picker.SetOnSelected($.proxy(this._onSetPicker, this));

                                    var currentFieldID = null;
                                    if (this._expr && this._expr.Value) {
                                        currentFieldID = this._expr.Value;
                                    }

                                    var context = this;
                                    this._picker.ComputeAndSetNavigationPathFilterAsync(this._options.getAllFields(), currentFieldID, function () {
                                        context._picker.ShowInDialog();
                                        context._picker.Reload();
                                    });
                                }
                                return false;
                            };

                            Picker.prototype._onSetPicker = function (picker) {
                                if (picker.GetSelectedFields()[0]) {
                                    this._field = picker.GetSelectedFields()[0].Field;
                                    this._onChange();
                                }
                            };
                            return Picker;
                        })();
                        Field.Picker = Picker;
                    })(Input.Field || (Input.Field = {}));
                    var Field = Input.Field;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
