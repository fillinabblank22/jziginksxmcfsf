/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    var Wrapper = (function () {
                        function Wrapper(config) {
                            // This class represents a wrapper for any input field. When the filter is changed, new input is created
                            // CONSTANTS
                            this._DEFAULT_CONFIG = {
                                enabledWhenEmpty: false,
                                dataTypeFilter: 0
                            };
                            this._DEFAULT_MAPPPING = {
                                // values per SolarWinds.Reporting.Models.Data.DataDeclarationType
                                // values per SolarWinds.Reporting.Models.Data.DataDeclarationType
                                0: SW.Core.Controls.ExpressionBuilder.Input.Constant.Unknown,
                                1: SW.Core.Controls.ExpressionBuilder.Input.Constant.DateTime,
                                2: SW.Core.Controls.ExpressionBuilder.Input.Constant.Bool,
                                3: SW.Core.Controls.ExpressionBuilder.Input.Constant.Int,
                                4: SW.Core.Controls.ExpressionBuilder.Input.Constant.Float,
                                5: SW.Core.Controls.ExpressionBuilder.Input.Constant.Text,
                                6: SW.Core.Controls.ExpressionBuilder.Input.Constant.Text,
                                7: SW.Core.Controls.ExpressionBuilder.Input.Constant.Text,
                                8: SW.Core.Controls.ExpressionBuilder.Input.Constant.Enum,
                                9: SW.Core.Controls.ExpressionBuilder.Input.Constant.Text,
                                10: SW.Core.Controls.ExpressionBuilder.Input.Constant.Byte,
                                11: SW.Core.Controls.ExpressionBuilder.Input.Object.Picker
                            };
                            this._options = $.extend({}, this._DEFAULT_CONFIG, config);
                            this._options.mapping = $.extend({}, this._options.globalOptions.constantMapping, this._DEFAULT_MAPPPING);
                            this._input = this._createConstant();
                            this._filter = null;
                        }
                        Wrapper.prototype.getExpr = function () {
                            return this._input.getExpr();
                        };

                        Wrapper.prototype.validate = function () {
                            return this._input.validate();
                        };

                        Wrapper.prototype.setFilter = function (masterField) {
                            if (masterField == null) {
                                this._options.renderTo.empty();
                                this._options.dataTypeFilter = 5;
                                this._input = this._createConstant();
                                return;
                            }

                            if (masterField.FieldType == ExpressionBuilder.Constants.FIELD_TYPE.EVENT)
                                return;

                            this._filter = masterField;

                            var prevFilter = this._options.dataTypeFilter;
                            var newFilter = masterField.DataTypeInfo.DeclType;
                            if (prevFilter != newFilter) {
                                this._options.dataTypeFilter = newFilter;
                                this._options.renderTo.empty();

                                // If we changed datatype from any other datatype we clear value.
                                if (prevFilter !== 0) {
                                    this._options.expr.Value = '';
                                }

                                this._input = this._createConstant();
                            }

                            this._input.setFilter(masterField);
                        };

                        Wrapper.prototype.operatorChanged = function (operator) {
                            if (this._input != null && typeof this._input.operatorChanged === 'function') {
                                this._input.operatorChanged(operator);
                            }
                        };

                        Wrapper.prototype.getFilter = function () {
                            return this._filter;
                        };

                        Wrapper.prototype.getErrorMessage = function () {
                            if (this._input.errorMessage && typeof this._input.errorMessage !== "undefined")
                                return this._input.errorMessage;
                            return null;
                        };

                        Wrapper.prototype._createConstant = function () {
                            var className = this._options.mapping[this._options.dataTypeFilter];
                            if (this._options.side == ExpressionBuilder.Constants.EXPR_SIDE.LEFT) {
                                className = SW.Core.Controls.ExpressionBuilder.Input.Variable.Picker;
                            }

                            return new className(this._options);
                        };
                        return Wrapper;
                    })();
                    Input.Wrapper = Wrapper;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
