/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var Text = (function (_super) {
                            __extends(Text, _super);
                            function Text(config) {
                                var _this = this;
                                _super.call(this, config);
                                this._texts = {
                                    'watermark-text': "@{R=Core.Strings;K=WEBJS_ZT0_35;E=js}"
                                };
                                this.errorMessage = "@{R=Core.Strings;K=WEBJS_PD0_02;E=js}";

                                this._ds = this._options.globalOptions.dataSource;
                                this._extraRequestParams = this._options.globalOptions.extraRequestParams;
                                this._field;
                                this._input = $('<input type="text" />');

                                this._units = $("<span />");
                                this._units.addClass('neb-units');

                                this._input.blur($.proxy(this.lostFocus, this));
                                this._input.on("onSelectField", $.proxy(this.lostFocus, this));

                                this._input.val(this._expr.Value || '');
                                this._input.prop('disabled', !(this._expr.Value || this._options.enabledWhenEmpty));

                                if (this._options.globalOptions.enableWatermark) {
                                    this._watermark = new SW.Core.Widgets.WatermarkTextbox(this._input, this._texts['watermark-text']);
                                }

                                // See FB367239 and http://stackoverflow.com/a/10507766/342581 for details about getting
                                // autocomplete to work on focus, without requiring a character to be typed first.
                                if (!this._options.globalOptions.disableAutocomplete) {
                                    var inputControl = this._input;
                                    inputControl.autocomplete({
                                        disabled: true,
                                        delay: 500,
                                        minLength: 0,
                                        source: function (request, response) {
                                            var _FieldRefID = '';
                                            if (_this._field && _this._field.RefID) {
                                                _FieldRefID = _this._field.RefID.Data;
                                            }

                                            var param = {
                                                ObjectType: _this._options.globalOptions.dataSource.ObjectType,
                                                FieldRefID: _FieldRefID,
                                                SearchTerm: request.term
                                            };

                                            SW.Core.Services.callControllerAction(_this._options.globalOptions.dataProviderUrl, "GetFieldAutoCompleteValues", param, function (res) {
                                                if (res != null) {
                                                    response(res);
                                                } else {
                                                    response([]);
                                                }
                                            }, function () {
                                                response([]);
                                                $.error("Can't load autocomplete data");
                                            });
                                        },
                                        select: function (event, data) {
                                            inputControl.autocomplete('disable');
                                        }
                                    }).blur(function () {
                                        inputControl.autocomplete('enable');
                                    }).focus(function () {
                                        inputControl.autocomplete("search", $(this).val());
                                    });
                                }

                                this._options.renderTo.append(this._input);
                                this._input.after(this._units);
                            }
                            Text.prototype._getVal = function () {
                                var val = this._input.val();

                                if (val == this._texts['watermark-text']) {
                                    val = '';
                                }

                                return val;
                            };

                            Text.prototype.getExpr = function () {
                                this._expr.Value = this._getVal();
                                return this._expr;
                            };

                            Text.prototype.validate = function () {
                                return true;
                            };

                            Text.prototype.setFilter = function (masterField) {
                                this._field = masterField;
                                this._input.prop('disabled', false);
                                this._input.autocomplete('enable');

                                if (masterField.DataTypeInfo.Units && masterField.DataTypeInfo.Units !== '') {
                                    this._units.html(masterField.DataTypeInfo.Units);
                                }
                            };
                            return Text;
                        })(Constant.BaseConstant);
                        Constant.Text = Text;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
