﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.models.ts" />
/// <reference path="eb.input.event.helper.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                var Instance = (function () {
                    function Instance() {
                    }
                    return Instance;
                })();
                ExpressionBuilder.Instance = Instance;

                var Control = (function () {
                    function Control(config) {
                        var _this = this;
                        this._getAllFields = function () {
                            function visit(expr, fields) {
                                if (expr && expr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD && expr.Value) {
                                    fields.push(expr.Value);
                                } else if (expr && expr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR && expr.Child.length > 0) {
                                    for (var i = 0; i < expr.Child.length; i++) {
                                        visit(expr.Child[i], fields);
                                    }
                                }
                                return fields;
                            }

                            return visit(_this.getExpression(), []);
                        };
                        var _self = this;
                        this._config = config;
                        this._settings = $.extend({}, this._getDefaults(), config);
                        this._isEmpty = !this._settings.expr;

                        if (this._isEmpty) {
                            this._settings.expr = this._getDefaultGroupExpr();
                        }

                        this._elem = this._settings.renderTo;
                        this._elem.addClass(this._settings.containerClass);

                        this._render(this._settings.expr);

                        this._lastChangeTimestamp = 0;

                        // bind events
                        this._elem.unbind("onValidationError").bind("onValidationError", $.proxy(this._settings.onValidationError, this));
                        $("body").off("mouseup").on("mouseup", $.proxy(this._expressionBuilderHasChanged, this));
                        $("body").off("keyup").on("keyup", $.proxy(this._expressionBuilderHasChanged, this));
                        setTimeout($.proxy(this._expressionBuilderHasChanged, this), 0);

                        this._updateHandlers();

                        // set up handler for add/remove actions
                        var selector = '.neb-action';
                        this._elem.off("click", selector).on("click", selector, function () {
                            var container = $(this).data("container");
                            var expr = $(this).data("expr");
                            if (expr) {
                                container.append(_self._createExprBlock(expr));
                            } else {
                                // delete action
                                container.remove();
                                _self._updateIsEmpty(_self._getIsEmpty());
                            }
                            _self._updateHandlers();
                        });

                        // handle mousedown outside of add item menu
                        $("body").mousedown(function (event) {
                            if (!($(event.target).hasClass('neb-action'))) {
                                $('.neb-item-add-options').hide();
                                $('.neb-item-add-down').removeClass('neb-item-add-down');
                            }
                            if (!($(event.target).hasClass('neb-pref-item'))) {
                                $('.neb-pref-options').hide();
                                $('.neb-preference').removeClass('neb-preference-open');
                            }
                        });

                        this._settings.onEmpty(this._isEmpty);
                    }
                    // PUBLIC METHODS
                    Control.prototype.refresh = function () {
                        return this._render(this._settings.expr);
                    };

                    Control.prototype.setOnChangeInterval = function (interval) {
                        this._settings.onChangeInterval = interval;
                    };

                    Control.prototype.setOnChangeHandler = function (handler) {
                        this._settings.onChangeHandler = handler;
                    };

                    Control.prototype.clearInfoPanel = function () {
                        this._informationContainer.empty();
                    };

                    Control.prototype.isCorrelationEngineEnabled = function () {
                        return this._settings.correlationEngine;
                    };

                    Control.prototype.removeFromInfoPanel = function (id) {
                        this._elem.find(".neb-information-panel-item[data-id='" + id + "']").remove();
                    };

                    Control.prototype.addToInfoPanel = function (id, text, imagePath) {
                        var renderedHtml = '';
                        if (imagePath && imagePath != '') {
                            renderedHtml = "<div class='neb-information-panel-item' data-id='" + id + "'><img src='" + imagePath + "'><span class='neb-information-panel-item-text'>" + text + "</span></div>";
                        } else {
                            renderedHtml = "<div class='neb-information-panel-item' data-id='" + id + "'><span class='neb-information-panel-item-text'>" + text + "</span></div>";
                        }

                        $(renderedHtml).appendTo(this._informationContainer);
                    };

                    Control.prototype.getExpression = function () {
                        if (this._getIsEmpty()) {
                            return null;
                        }
                        return this._getExpr($(".neb-top-node", this._elem));
                    };

                    Control.prototype.validate = function () {
                        return true;
                    };

                    Control.prototype.validateConstantValues = function () {
                        if (this._getIsEmpty()) {
                            return true;
                        }
                        return this._doValidation($(".neb-top-node", this._elem), true);
                    };

                    // PRIVATE METHODS
                    Control.prototype._expressionBuilderHasChanged = function () {
                        var _this = this;
                        function contains(a, b) {
                            return a.contains ? a != b && a.contains(b) : !!(a.compareDocumentPosition(b) & 16);
                        }

                        var currentTimestamp = new window.Date().getTime();

                        if (currentTimestamp - this._lastChangeTimestamp > this._settings.onChangeInterval) {
                            var callback = function () {
                                // callback may fire on a control that's not ready yet and isn't part of the document tree
                                // that can lead to errors so check here
                                if (_this._settings.onChangeHandler && contains(document.body, _this._elem[0])) {
                                    _this._settings.onChangeHandler(_this);
                                }
                            };
                            setTimeout(callback, this._settings.onChangeInterval);
                        }

                        this._lastChangeTimestamp = currentTimestamp;

                        // Internals
                        setTimeout($.proxy(this._setGroupOperatorLabels, this), 0);
                    };

                    Control.prototype._setGroupOperatorLabels = function () {
                        this._elem.find(".neb-condition-info").remove();
                        this._elem.find("ul.neb-condition-list").each(function (index, element) {
                            var type = $(element).parent().parent().parent().parent().parent().parent().prev().find("select.neb-group-condition option:selected").attr("data-shortname");
                            if (type == undefined)
                                type = $(element).parent().parent().prev().find("select.neb-group-condition option:selected").attr("data-shortname");

                            if (type !== undefined) {
                                var $allitems = $(element).children("li.neb-node");
                                var count = $allitems.length;
                                if (count > 1) {
                                    $allitems.each(function (index, el) {
                                        if (index != 0) {
                                            $(el).before($("<li class='neb-condition-info'>" + type + "</li>"));
                                        }
                                    });
                                }
                            }
                        });
                    };

                    Control.prototype._getDefaults = function () {
                        return {
                            // text to display for the root node element (upper left corner), "Where" by default
                            rootNodeText: "@{R=Core.Strings;K=WEBJS_JP2_3;E=js}",
                            // background colors to use
                            bgColorAnd: "#ffe89e",
                            bgColorOr: "#daf7cc",
                            bgColorChild: "#ebebeb",
                            bgColorItemValidationError: "#F58CB6",
                            // global settings / switches
                            allowGroups: false,
                            htmlentities: false,
                            decimal_separator: ".",
                            // css style classes
                            containerClass: "neb-rules-container",
                            rulesListLiErrorClass: "neb-error-li",
                            rulesListLiAppliedClass: "neb-applied-li",
                            filterInputTextClass: "neb-rhs-text",
                            filterInputNumberClass: "neb-rhs-number",
                            filterInputDateClass: "neb-rhs-date",
                            filterInputCheckboxClass: "neb-rhs-checkbox",
                            filterInputRadioClass: "neb-rhs-radio",
                            filterSelectClass: "neb-rhs-select",
                            filterGroupListClass: "neb-rhs-group-list",
                            filterGroupListItemHorizontalClass: "neb-rhs-group-list-item-horizontal",
                            filterGroupListItemVerticalClass: "neb-rhs-group-list-item-vertical",
                            // function hooks
                            onValidationError: function () {
                            },
                            onEmpty: function (isEmpty) {
                            },
                            fieldInput: SW.Core.Controls.ExpressionBuilder.Input.Field.DropDownCascade,
                            operatorInput: SW.Core.Controls.ExpressionBuilder.Input.Operator,
                            constantInput: SW.Core.Controls.ExpressionBuilder.Input.Wrapper,
                            eventInput: SW.Core.Controls.ExpressionBuilder.Input.Event.Editor,
                            disableAutocomplete: false,
                            constantMapping: {}
                        };
                    };

                    Control.prototype._getDefaultGroupExpr = function () {
                        return {
                            NodeType: SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR,
                            Value: this._settings.groupOperators[0].RefID,
                            Child: [
                                this._getRuleExpr(0)
                            ]
                        };
                    };

                    Control.prototype._getRuleExpr = function (ruleId) {
                        return this._settings.ruleTypes[ruleId].expr;
                    };

                    // create condition dropdown (AND/OR)
                    Control.prototype._createGroupConditionDropdown = function (value) {
                        var ops = this._settings.groupOperators;
                        var span = $('<span class="neb-group-condition-container"></span>');
                        var select = $('<select class="neb-group-condition"></select>').appendTo(span);

                        for (var i = 0; i < ops.length; i++) {
                            var o = new Option(ops[i].DisplayName, ops[i].RefID);
                            $(o).html(ops[i].DisplayName);
                            $(o).attr("data-shortname", ops[i].ShortDisplayName);
                            select.append(o);
                        }

                        if (value) {
                            select.val(value);
                        }

                        return span;
                    };

                    // create Group Actions dropdown (add child/add block)
                    Control.prototype._createGroupActionList = function (condition_list_container) {
                        var div, div2, table, tr, td;

                        div = $('<div class="neb-item-add-wrap"></div>');
                        if (this._settings.allowGroups || this._settings.ruleTypes.length > 1) {
                            $('<div class="neb-item-add"></div>').appendTo(div);
                            div2 = $('<div class="neb-item-add-options"></div>').appendTo(div);
                            table = $('<table></table>').appendTo(div2);
                            for (var i = 0; i < this._settings.ruleTypes.length; i++) {
                                if (!this._settings.ruleTypes[i].visible)
                                    continue;

                                tr = $('<tr></tr>').appendTo(table);
                                td = $('<td class="neb-action"></td>').appendTo(tr);
                                td.text(this._settings.ruleTypes[i].displayName);
                                td.data('expr', this._getRuleExpr(i));
                                td.data('container', condition_list_container);
                                if (this._settings.ruleTypes[i].hint) {
                                    td.attr('ext:qtip', this._settings.ruleTypes[i].hint);
                                }
                            }
                            if (this._settings.allowGroups) {
                                tr = $('<tr></tr>').appendTo(table);
                                td = $('<td class="neb-action"></td>').appendTo(tr);
                                td.text("@{R=Core.Strings;K=WEBJS_JP2_4;E=js}"); // Add And/Or block
                                td.data('expr', this._getDefaultGroupExpr());
                                td.data('container', condition_list_container);
                                td.attr('ext:qtip', '@{R=Core.Strings.2;K=WEBJS_BV0_0001;E=js}');
                            }
                        } else {
                            table = $('<table></table>').appendTo(div);
                            tr = $('<tr><td class="neb-simple-elbow"><img src="/Orion/images/NestedExpressionBuilder/tree_elbow.png"></img></td></tr>').appendTo(table);

                            td = $('<td class="neb-action neb-add-simple"></td>').appendTo(tr);
                            td.text(this._settings.ruleTypes[0].displayName);
                            td.data('expr', this._getRuleExpr(0));
                            td.data('container', condition_list_container);
                        }

                        return div;
                    };

                    Control.prototype._setHandlerForPreferenceOption = function (item, callBackFunc, actionId) {
                        item.on("mousedown", function () {
                            callBackFunc(actionId);
                        });
                    };

                    Control.prototype._createPreferencepActionList = function () {
                        var div, div2, table, tr, td, span;

                        if (this._settings.preferenceOptions.length > 0) {
                            div = $('<div class="neb-pref-options"></div>');
                            table = $('<table></table>').appendTo(div);
                            for (var i = 0; i < this._settings.preferenceOptions.length; i++) {
                                tr = $('<tr class="neb-pref-item"></tr>').appendTo(table);
                                var tdElement = '<td class="neb-pref-action" id="pA-' + this._settings.preferenceOptions[i].actionId + '"></td>';
                                td = $(tdElement).appendTo(tr);
                                var actionId = this._settings.preferenceOptions[i].actionId;
                                var callBackFunc = this._settings.preferenceOptions[i].callback;
                                this._setHandlerForPreferenceOption(td, callBackFunc, actionId);

                                //span = $('<span class="neb-pref-action"></span>').appendTo(td);
                                //td.on("mousedown", () => { alert('buf');});
                                td.text(this._settings.preferenceOptions[i].displayName);
                            }
                        }
                        return div;
                    };

                    Control.prototype._createExprBlock = function (expr, firstLoad) {
                        $('.neb-item-add-down').removeClass('neb-item-add-down');

                        if (expr.NodeType == SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.EVENT) {
                            return this._createLeafConditionEventBlock(expr, firstLoad);
                        }

                        if (expr.NodeType != SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR) {
                            $.error('Incorrect expression. Expecting Operator(0), got type: ' + expr.NodeType);
                        }
                        for (var i = 0; i < this._settings.groupOperators.length; i++) {
                            if (this._settings.groupOperators[i].RefID == expr.Value) {
                                return this._createGroupConditionBlock(expr);
                            }
                        }
                        return this._createLeafConditionExpressionBlock(expr);
                    };

                    Control.prototype._createMainGroupConditionBlock = function (expr) {
                        var _this = this;
                        var root, table, tr, td, ul;
                        var value = expr.Value;

                        root = $('<div></div>').addClass('neb');

                        // workaround jquery 1.7 bug
                        table = $('<table cellspacing="0"></table>').appendTo(root);
                        tr = $('<tr></tr>').appendTo(table);
                        td = $('<td class="neb-condition-list-container"></td>').appendTo(tr);
                        ul = $('<ul class="neb-node neb-top-node neb-condition-list neb-group"></ul>').appendTo(td);
                        ul.data('value', value);

                        table = $('<table cellspacing="0"></table>').appendTo(ul);

                        // ROW 1 - is the condition dropdown (and/or)
                        tr = $('<tr class="neb-condition neb-condition-' + value.toLowerCase() + '"></tr>').appendTo(table);

                        if (this._settings.rootNodeText) {
                            $('<td class="neb-root-text" style="width:115px; white-space: nowrap;">' + this._settings.rootNodeText + '</td><td style="background-color: transparent;width: 9px; max-width: 9px;"><div class="neb-root-line"></div></td>').appendTo(tr);
                        } else {
                            $('<td>&nbsp;</td>').appendTo(tr);
                        }

                        $('<td class="neb-group-dropdown"></td>').append(this._createGroupConditionDropdown(value)).appendTo(tr);
                        td = $('<td class="neb-preference" style="width: 44px;min-width:44px;max-width:44px;">&nbsp;</td>').appendTo(tr);
                        td.append(this._createPreferencepActionList());

                        // ROW 2 - is an unordered list of children
                        tr = $('<tr></tr>').appendTo(table);
                        this._informationContainer = $('<td rowspan="2" class="neb-information-panel"></td>').appendTo(tr);

                        td = $('<td colspan="3" style="vertical-align:top"></td>').appendTo(tr);

                        var innerTable = $('<table></table>').appendTo(td);
                        tr = $('<tr></tr>').appendTo(innerTable);

                        td = $('<td class="neb-condition-list-container" colspan="2"></td>').appendTo(tr);

                        if (this._settings.rootNodeText) {
                            td.css("padding-left", "5px");
                        }

                        ul = $('<ul class="neb-node neb-condition-list"></ul>').appendTo(td);
                        $.each(expr.Child, function (index, child) {
                            //if (child.Value != null) {
                            ul.append(_this._createExprBlock(child));
                            // }
                        });

                        // ROW 3 - is the group action button (add new children / condition blocks)
                        tr = $('<tr></tr>').appendTo(innerTable);
                        td = $('<td colspan="2"></td>').appendTo(tr);

                        if (this._settings.rootNodeText) {
                            td.css("padding-left", "5px");
                        }

                        td.append(this._createGroupActionList(ul));

                        return root;
                    };

                    Control.prototype._createGroupConditionBlock = function (expr) {
                        var _this = this;
                        var table, tr, td, ul, li;
                        var value = expr.Value;

                        li = $('<li class="neb-node neb-group"></li>');
                        li.data('value', value);
                        table = $('<table cellspacing="0"></table>').appendTo(li);

                        // ROW 1 - is the condition dropdown (and/or)
                        tr = $('<tr class="neb-condition neb-condition-' + value.toLowerCase() + '"></tr>').appendTo(table);
                        $('<td style="background-color: transparent;width: 9px; max-width: 9px;"><div class="neb-root-line"></div></td>').appendTo(tr);
                        $('<td class="neb-drag-handle"></td>').appendTo(tr);
                        $('<td></td>').append(this._createGroupConditionDropdown(value)).appendTo(tr);

                        // delete action
                        td = $('<td class="neb-item-close neb-action"></td>').appendTo(tr);
                        td.data('container', li);

                        // ROW 2 - is an unordered list of children
                        tr = $('<tr></tr>').appendTo(table);
                        td = $('<td class="neb-condition-list-container" colspan="3"></td>').appendTo(tr);
                        ul = $('<ul class="neb-node neb-condition-list"></ul>').appendTo(td);
                        $.each(expr.Child, function (index, child) {
                            ul.append(_this._createExprBlock(child));
                        });

                        // ROW 3 - is the group action button (add new children / condition blocks)
                        tr = $('<tr></tr>').appendTo(table);
                        td = $('<td colspan="100"></td>').appendTo(tr);
                        td.append(this._createGroupActionList(ul));

                        return li;
                    };

                    Control.prototype._createLeafConditionEventBlock = function (expr, firstLoad) {
                        var _this = this;
                        var li, table, tr, trDetailRow, tdEventDetail, tdEvent, tdEventButton, eventEditor;
                        var bgCol = this._settings.bgColorChild;
                        expr = $.extend(true, {}, expr); // deep copy

                        li = $('<li class="neb-node"></li>');
                        table = $('<table></table>').appendTo(li);
                        tr = $('<tr class="neb-item"></tr>').appendTo(table);
                        trDetailRow = $('<tr class="neb-event" style="display:none"><td colspan="2"></td></tr>').appendTo(table);
                        tdEventDetail = $('<td class="neb-event-detail"></td>').appendTo(trDetailRow);
                        $('<td style="background-color:#DEEBFA"></td>').appendTo(trDetailRow);
                        $('<td></td>').appendTo(trDetailRow);

                        $('<td style="background-color: transparent;width: 9px; max-width: 9px;"><div class="neb-root-line"></div></td>').appendTo(tr);
                        $('<td class="neb-drag-handle"></td>').css('background-color', bgCol).appendTo(tr);

                        tdEvent = $('<td class="neb-event"></td>').css('background-color', bgCol).appendTo(tr);
                        tdEventButton = $('<td class="neb-event-button neb-btn-collapsed"></td>').appendTo(tr);

                        var eventParams = {
                            mustHappend: ExpressionBuilder.Input.Event.Helper.GetEventProperty(expr, ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND),
                            mustHappendType: ExpressionBuilder.Input.Event.Helper.GetEventProperty(expr, ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_TYPE),
                            mustHappendCount: ExpressionBuilder.Input.Event.Helper.GetEventProperty(expr, ExpressionBuilder.Constants.EVENT_PARAMS.MUST_HAPPEND_COUNT),
                            filterExpression: ExpressionBuilder.Input.Event.Helper.GetEventFilter(expr),
                            globalOptions: this._settings,
                            renderTitleTo: tdEvent,
                            renderButtonTo: tdEventButton,
                            firstLoad: firstLoad,
                            renderDetailTo: tdEventDetail,
                            eventRefId: expr.Value,
                            expr: expr,
                            getAllFields: this._getAllFields,
                            transformTo: function (type, data) {
                                _this._blockTransformation(type, data, li);
                            }
                        };

                        eventEditor = ExpressionBuilder.Input.Helper.CreateInput(eventParams, this._settings);

                        // delete action
                        tdEvent = $('<td class="neb-item-close neb-action"></td>').css('background-color', bgCol).appendTo(tr);
                        tdEvent.data('container', li);

                        // 30px spacer cell on the end for quick automatic layout
                        //'<td style="width: 30px; max-width: 30px; min-width: 30px;">&nbsp;</td>'
                        li.data('op', eventEditor);

                        return li;
                    };

                    Control.prototype._blockTransformation = function (type, data, parentItem) {
                        var parentContainer = parentItem.parent();

                        parentItem.remove();
                        var expr = ExpressionBuilder.Input.Helper.GetRuleExpr(type, this._settings);
                        if (data) {
                            if (type == ExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT) {
                                expr.Child[0].Value = data;
                            } else if (type == ExpressionBuilder.Constants.RULE_TYPE.EVENT) {
                                expr.Value = data;
                            }
                        }

                        parentContainer.append(this._createExprBlock(expr, (type == ExpressionBuilder.Constants.RULE_TYPE.EVENT) ? true : false));
                    };

                    Control.prototype._createLeafConditionExpressionBlock = function (expr) {
                        var _this = this;
                        var li, table, tr, td, spanLeft, spanOp, spanRight, lhs, rhs, op, thisExpr;
                        var bgCol = this._settings.bgColorChild;
                        expr = $.extend(true, {}, expr); // deep copy

                        if (expr.Child.length == 1) {
                            //create temp rhs when operator is unary
                            expr.Child[1] = {
                                NodeType: SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT
                            };
                        }

                        var ruleType = ExpressionBuilder.Input.Helper.GetRuleType(expr);

                        // convert { value|operator|field } to { field|operator|value } - from old alerting's compatibility reason
                        if (ruleType == ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_FIELD) {
                            var tmp = expr.Child[0];
                            expr.Child[0] = expr.Child[1];
                            expr.Child[1] = tmp;
                        }

                        li = $('<li class="neb-node"></li>');
                        table = $('<table></table>').appendTo(li);
                        tr = $('<tr class="neb-item"></tr>').appendTo(table);
                        $('<td class="neb-ignore" style="background-color: transparent;width: 9px; max-width: 9px;"><div class="neb-root-line"></div></td>').appendTo(tr);
                        $('<td class="neb-drag-handle"></td>').css('background-color', bgCol).appendTo(tr);

                        td = $('<td></td>').css('background-color', bgCol).appendTo(tr);
                        spanLeft = $('<span class="neb-lhs"></span>').appendTo(td);

                        td = $('<td></td>').css('background-color', bgCol).appendTo(tr);
                        spanOp = $('<span class="neb-op"></span>').appendTo(td);

                        td = $('<td></td>').css('background-color', bgCol).appendTo(tr);
                        spanRight = $('<span class="neb-rhs"></span>').appendTo(td);

                        // delete action
                        td = $('<td class="neb-item-close neb-action"></td>').css('background-color', bgCol).appendTo(tr);
                        td.data('container', li);

                        lhs = ExpressionBuilder.Input.Helper.CreateInput({
                            renderTo: spanLeft,
                            expr: expr.Child[0],
                            exprItem: li,
                            enabledWhenEmpty: true,
                            globalOptions: this._settings,
                            onSet: function (field) {
                                op.setFilter(field);
                                rhs.setFilter(field);
                                _this._updateIsEmpty(false);
                            },
                            transformTo: function (type, data) {
                                _this._blockTransformation(type, data, li);
                            },
                            getAllFields: this._getAllFields,
                            side: SW.Core.Controls.ExpressionBuilder.Constants.EXPR_SIDE.LEFT
                        }, this._settings);

                        rhs = ExpressionBuilder.Input.Helper.CreateInput({
                            renderTo: spanRight,
                            exprItem: li,
                            expr: expr.Child[1],
                            globalOptions: this._settings,
                            onSet: function (field) {
                            },
                            getAllFields: this._getAllFields,
                            side: SW.Core.Controls.ExpressionBuilder.Constants.EXPR_SIDE.RIGHT
                        }, this._settings);

                        thisExpr = $.extend({}, expr); // shallow copy
                        thisExpr.Child = []; // do not pass children
                        op = new this._settings.operatorInput({
                            renderTo: spanOp,
                            globalOptions: this._settings,
                            expr: thisExpr,
                            leftSide: lhs,
                            rightSide: rhs,
                            ruleType: ruleType,
                            transformTo: function (type, data) {
                                _this._blockTransformation(type, data, li);
                            },
                            onSet: function (selectedOp) {
                                if (rhs.operatorChanged && typeof rhs.operatorChanged == 'function') {
                                    rhs.operatorChanged(selectedOp);
                                }

                                spanRight.toggle(selectedOp.Type != SW.Core.Controls.ExpressionBuilder.Constants.OPERATOR_TYPE.UNARY);
                            }
                        });

                        if (ExpressionBuilder.Input.Helper.IsConstantOnTheLeft(ruleType)) {
                            // For constant to constant/field rule we hardcode datatype to TEXT.
                            var textField = {
                                DataTypeInfo: { DeclType: 5 }
                            };

                            op.setFilter(textField);
                            lhs.setFilter(textField);
                            rhs.setFilter(textField);
                        }

                        li.data('op', op);

                        return li;
                    };

                    Control.prototype._getExpr = function (conditionBlock) {
                        var _this = this;
                        var expr;
                        if (conditionBlock.hasClass('neb-group')) {
                            expr = {
                                NodeType: SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR,
                                Value: conditionBlock.data('value'),
                                Child: []
                            };
                            var container = conditionBlock.find(".neb-condition-list:first");
                            container.children(".neb-node").each(function (index, item) {
                                expr.Child.push(_this._getExpr($(item)));
                            });
                        } else {
                            expr = conditionBlock.data('op').getExpr();
                        }
                        return expr;
                    };

                    Control.prototype._getIsEmpty = function (conditionBlock) {
                        var _this = this;
                        if (!conditionBlock) {
                            return this._getIsEmpty($(".neb-top-node", this._elem));
                        }
                        if (conditionBlock.hasClass('neb-group')) {
                            var container = conditionBlock.find(".neb-condition-list:first");
                            var atLeastOneNotEmpty = false;
                            container.children(".neb-node").each(function (index, item) {
                                if (!_this._getIsEmpty($(item))) {
                                    atLeastOneNotEmpty = true;
                                    return false;
                                }
                            });
                            return !atLeastOneNotEmpty;
                        } else {
                            return conditionBlock.data('op').isEmpty();
                        }
                    };

                    Control.prototype._render = function (expr) {
                        this._elem.empty();
                        this._elem.append(this._createMainGroupConditionBlock(expr));
                        return this._elem;
                    };

                    Control.prototype._updateHandlers = function () {
                        var _this = this;
                        // update drag handlers
                        var bTemp;
                        $(".neb-condition-list", this._elem).sortable({
                            handle: ".neb-drag-handle",
                            placeholder: "neb-drag-target",
                            connectWith: "#" + this._elem.attr("id") + " .neb-condition-list:not('.neb-top-node')",
                            start: function (evt, ui) {
                                bTemp = ui.item.css("border-left");
                                ui.item.css("border-left", "0px");
                            },
                            stop: function (evt, ui) {
                                ui.item.css("border-left", bTemp);
                            }
                        });

                        // update group action handlers
                        $(".neb-item-add:not(.bound)", this._elem).addClass("bound").bind("click", function () {
                            var action_btn = $(this);
                            var action_list = $(this).next();
                            if (action_list.is(":visible")) {
                                action_list.css('display', 'none');
                                action_btn.removeClass('neb-item-add-down');
                            } else {
                                action_list.css('display', 'inline-block');
                                action_btn.addClass('neb-item-add-down');
                            }
                        });

                        // update pref action handlers
                        $(".neb-preference:not(.bound)", this._elem).addClass("bound").bind("click", function () {
                            var action_btn = $(this);
                            var action_list = $($(this).children()[0]);
                            if (action_list.is(":visible")) {
                                action_list.css('display', 'none');
                                action_btn.removeClass('neb-preference-open');
                            } else {
                                action_list.css('display', 'inline-block');
                                action_btn.addClass('neb-preference-open');
                            }
                        });

                        $(".neb-item-add-options").hide();
                        $(".neb-pref-options").hide();

                        // update condition select change
                        $(".neb-group-condition:not(.bound)", this._elem).addClass("bound").bind("change", function () {
                            var newValue = $(this).val();
                            $(this).closest("tr").attr("class", "neb-condition neb-condition-" + newValue.toLowerCase());
                            $(this).closest(".neb-node").data('value', newValue);
                        });

                        $(".neb-lhs").one("change", function () {
                            if (!_this._getIsEmpty() && _this._config.updateSourceNameDelegate) {
                                _this._config.updateSourceNameDelegate(false);
                            }
                        });

                        if (this._getIsEmpty() && this._config.updateSourceNameDelegate) {
                            this._config.updateSourceNameDelegate(true);
                        }
                    };

                    Control.prototype._updateIsEmpty = function (newState) {
                        if (this._isEmpty != newState) {
                            this._isEmpty = newState;
                            this._settings.onEmpty(this._isEmpty);
                        }
                    };

                    Control.prototype._doValidation = function (conditionBlock, item) {
                        var _this = this;
                        var iconCell = $("<td class='neb-validation-icon'></td>");
                        var rowCells, op, row;
                        var validationState = true;

                        if (conditionBlock.hasClass("neb-group")) {
                            var container = conditionBlock.find(".neb-condition-list:first");
                            container.children(".neb-node").each(function (index, item) {
                                validationState &= _this._doValidation($(item));
                            });
                        } else {
                            op = conditionBlock.data("op");
                            row = conditionBlock.find(".neb-item");
                            rowCells = row.children(":not(:first-child)");

                            // cleanup
                            row.find("td.neb-validation-icon").remove();
                            rowCells.removeClass("neb-validation-error");

                            // if there is a incorrect value add error class to cells and append the error icon
                            if (!op.validate()) {
                                rowCells.addClass("neb-validation-error");
                                iconCell.attr("title", op.getErrorMessage()).appendTo(row);
                                return false;
                            }
                        }
                        return validationState;
                    };
                    return Control;
                })();
                ExpressionBuilder.Control = Control;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
