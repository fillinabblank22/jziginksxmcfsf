/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var Float = (function (_super) {
                            __extends(Float, _super);
                            function Float(config) {
                                _super.call(this, config);
                                this.errorMessage = "@{R=Core.Strings;K=WEBJS_PD0_01;E=js}";
                                this._input = $('<input type="text" />');

                                this._units = $("<span />");
                                this._units.addClass('neb-units');

                                this._input.on("onSelectField", $.proxy(this.lostFocus, this));
                                this._input.blur($.proxy(this.lostFocus, this));
                                this._input.val((this._expr.Value || '').replace(".", this._options.globalOptions.decimal_separator));
                                this._input.prop('disabled', !(this._expr.Value || this._options.enabledWhenEmpty));
                                this._options.renderTo.append(this._input);
                                this._input.after(this._units);
                            }
                            Float.prototype.validate = function () {
                                return this._isFloat(this._input.val());
                            };

                            Float.prototype.getExpr = function () {
                                this._expr.Value = this._input.val().replace(this._options.globalOptions.decimal_separator, ".");
                                ;
                                return this._expr;
                            };

                            Float.prototype._isFloat = function (n) {
                                var val = n.replace(this._options.globalOptions.decimal_separator, ".");
                                var numberVal = Number(val);
                                if (numberVal > 3.40282347E+38 || numberVal < -3.40282347E+38 || isNaN(numberVal))
                                    return false;
                                return (this._options.globalOptions.decimal_separator == '.') ? /^-?\d+(\.\d{0,7})?([eE][-+]?[0-9]+)?$/.test(n) : /^-?\d+(\,\d{0,7})?([eE][-+]?[0-9]+)?$/.test(n);
                            };
                            return Float;
                        })(Constant.BaseConstant);
                        Constant.Float = Float;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
