﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="..\..\typescripts\typings\underscore.d.ts" />
/// <reference path="eb.constants.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var FieldSwitch = (function () {
                            function FieldSwitch(config) {
                                this._texts = {
                                    'menu-use-field': "@{R=Core.Strings;K=WEBJS_ZT0_36;E=js}",
                                    'menu-use-constant': "@{R=Core.Strings;K=WEBJS_ZT0_37;E=js}"
                                };
                                this._config = config;
                                this._disabled = !(config.expr.Value || config.enabledWhenEmpty);

                                if (config.expr.NodeType === SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT) {
                                    this._inputType = 'Constant';
                                    this._renderInput(SW.Core.Controls.ExpressionBuilder.Input.Wrapper);
                                } else {
                                    this._inputType = 'Field';
                                    this._renderInput(SW.Core.Controls.ExpressionBuilder.Input.Field.Picker);
                                }

                                this._renderSwitchButton(this._disabled);
                            }
                            FieldSwitch.prototype.getExpr = function () {
                                return this._input.getExpr();
                            };

                            FieldSwitch.prototype.validate = function () {
                                return this._input.validate();
                            };

                            FieldSwitch.prototype.setFilter = function (masterField) {
                                this._input.setFilter(masterField);

                                this._renderSwitchButton(false);
                            };

                            FieldSwitch.prototype._renderInput = function (inputConstuctor) {
                                this._config.renderTo.empty();

                                this._input = new inputConstuctor(this._config);

                                return this._input;
                            };

                            FieldSwitch.prototype._renderSwitchButton = function (disabled) {
                                this._config.renderTo.find('.neb-constant-field-switch').remove();

                                if (this._config.expr.ConditionType !== 'Simple') {
                                    disabled = !!disabled;

                                    if (disabled) {
                                        this._config.renderTo.append("<span class='neb-constant-field-switch disabled'>&nbsp;</span>");
                                    } else {
                                        var template = "<span class='neb-constant-field-switch'>&nbsp;&nbsp;&nbsp;<span class='neb-constant-field-switch-menu'>{{text}}</span></span>";
                                        var text = (this._inputType === 'Constant') ? this._texts['menu-use-field'] : this._texts['menu-use-constant'];
                                        var html = $(_.template(template, { text: text }));

                                        this._config.renderTo.append(html);
                                        this._config.renderTo.find('.neb-constant-field-switch-menu').on('click', this._switchInput);
                                    }
                                }
                            };

                            FieldSwitch.prototype._switchInput = function () {
                                var previousInput = this._input;

                                this._config.expr.Value = null;

                                if (this._inputType === 'Field') {
                                    this._renderInput(SW.Core.Controls.ExpressionBuilder.Input.Wrapper);
                                    this._config.expr.NodeType = SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT;
                                    this._inputType = 'Constant';
                                } else {
                                    this._config.expr.NodeType = SW.Core.Controls.ExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD;
                                    this._renderInput(SW.Core.Controls.ExpressionBuilder.Input.Field.Picker);
                                    this._inputType = 'Field';
                                }

                                // If was set filter for right side, we will preserve it.
                                if (this._config.side === SW.Core.Controls.ExpressionBuilder.Constants.EXPR_SIDE.RIGHT) {
                                    if ($.isFunction(previousInput.getFilter)) {
                                        var filter = previousInput.getFilter();

                                        this._input.setFilter(filter);
                                    }
                                } else if (this._inputType === 'Constant' && this._config.side === SW.Core.Controls.ExpressionBuilder.Constants.EXPR_SIDE.LEFT) {
                                    var textField = {
                                        DataTypeInfo: { DeclType: 5 }
                                    };
                                    this._input.setFilter(textField);

                                    if ($.isFunction(this._config.onSet)) {
                                        this._config.onSet(textField);
                                    }
                                }

                                this._renderSwitchButton();
                            };
                            return FieldSwitch;
                        })();
                        Constant.FieldSwitch = FieldSwitch;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
