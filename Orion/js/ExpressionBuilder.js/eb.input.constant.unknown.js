﻿/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
/// <reference path="eb.input.constant.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    (function (Constant) {
                        var Unknown = (function (_super) {
                            __extends(Unknown, _super);
                            function Unknown(config) {
                                _super.call(this, config);

                                this._ds = this._options.globalOptions.dataSource;
                                this._input = $('<input type="text" />');
                                this._input.prop('disabled', true);
                                this._input.blur($.proxy(this.lostFocus, this));
                                this._input.on("onSelectField", $.proxy(this.lostFocus, this));
                                this._options.renderTo.append(this._input);
                            }
                            Unknown.prototype.getExpr = function () {
                                return this._expr;
                            };

                            Unknown.prototype.setFilter = function (masterField) {
                            };
                            return Unknown;
                        })(Constant.BaseConstant);
                        Constant.Unknown = Unknown;
                    })(Input.Constant || (Input.Constant = {}));
                    var Constant = Input.Constant;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
