/// <reference path="..\..\typescripts\typings\jquery.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionCore.d.ts" />
/// <reference path="..\..\typescripts\typings\OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Controls) {
            (function (ExpressionBuilder) {
                (function (Input) {
                    var Operator = (function () {
                        function Operator(config) {
                            var _this = this;
                            this._DEFAULT_CONFIG = {
                                dataTypeFilter: ''
                            };
                            this.isEmpty = function () {
                                return !this._options.leftSide.getExpr().Value;
                            };
                            this._onChange = function () {
                                _this._expr.Value = _this._dropdown.val();

                                if (_this._expr.Value == "HASCHANGED") {
                                    _this._options.transformTo(ExpressionBuilder.Constants.RULE_TYPE.EVENT, '[updateEvent].' + _this._fieldOnTheLeftSide.RefID.Data);
                                    return;
                                }

                                for (var i = 0; i < _this._operators.length; i++) {
                                    if (_this._operators[i].RefID == _this._expr.Value) {
                                        _this._operator = _this._operators[i];
                                        break;
                                    }
                                }

                                if (_this._operator && typeof (_this._options.onSet) === 'function') {
                                    _this._options.onSet(_this._operator);
                                }

                                _this._dropdown.resize(); // trick to redraw customSelect
                            };
                            this._options = $.extend({}, this._DEFAULT_CONFIG, config);
                            this._operators = this._options.globalOptions.logicalOperators;
                            this._expr = this._options.expr;

                            this._dropdown = $("<select></select>").addClass("neb-styled-combo");

                            this._dropdown.on("change", this._onChange);
                            this._dropdown.prop('disabled', !this._expr.Value);
                            this._options.renderTo.append(this._dropdown);
                            this._showFilteredItems();

                            var func = function () {
                                if (_this._dropdown.parents(".neb-event").length == 0) {
                                    _this._dropdown.customSelect();
                                }
                            };

                            setTimeout(func, 0);

                            return this;
                        }
                        Operator.prototype.getExpr = function () {
                            var leftExpr = this._options.leftSide.getExpr();
                            this._expr.Child = [leftExpr];
                            if (this._operator.Type != SW.Core.Controls.ExpressionBuilder.Constants.OPERATOR_TYPE.UNARY) {
                                var rightExpr = this._options.rightSide.getExpr();

                                //if (rightExpr.NodeType == Constants.EXPRESSION_TYPE.CONSTANT) {
                                //    rightExpr.Child = [];
                                //}
                                this._expr.Child.push(rightExpr);
                            }

                            return this._expr;
                        };

                        Operator.prototype.validate = function () {
                            var result = this._options.leftSide.validate();
                            if (this._operator.Type != SW.Core.Controls.ExpressionBuilder.Constants.OPERATOR_TYPE.UNARY) {
                                result = result && this._options.rightSide.validate();
                            }
                            return result;
                        };

                        Operator.prototype.setFilter = function (masterField) {
                            if (!masterField) {
                                this._dropdown.prop('disabled', true);
                                var customSelect = this._dropdown.next();
                                if (!customSelect.hasClass('customSelectDisabled')) {
                                    this._dropdown.next().addClass("customSelectDisabled");
                                }
                            } else {
                                this._fieldOnTheLeftSide = masterField;
                                var dataTypeFilter = masterField.DataTypeInfo.DeclType;
                                this._options.dataTypeFilter = dataTypeFilter;
                                this._dropdown.prop('disabled', false);
                                this._dropdown.next().removeClass("customSelectDisabled");
                                this._showFilteredItems();
                            }
                        };

                        Operator.prototype.getErrorMessage = function () {
                            if (this._operator.Type != SW.Core.Controls.ExpressionBuilder.Constants.OPERATOR_TYPE.UNARY) {
                                return this._options.rightSide.getErrorMessage();
                            }
                            return null;
                        };

                        // PRIVATE METHODS
                        Operator.prototype._isConstantOnTheLeftSide = function () {
                            return !(this._options.ruleType != ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT && this._options.ruleType != ExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_FIELD);
                        };

                        Operator.prototype._showFilteredItems = function () {
                            var _this = this;
                            var prevValue = this._expr.Value;
                            var prevValueFound = false;

                            if (this._isConstantOnTheLeftSide() || this._fieldOnTheLeftSide == null) {
                                this._dropdown.empty();

                                for (var i = 0; i < this._operators.length; i++) {
                                    if (!this._options.dataTypeFilter || this._isOperatorCompatible(this._operators[i], this._options.dataTypeFilter)) {
                                        this._createItem(this._dropdown, this._operators[i].DisplayName, this._operators[i].RefID);
                                        if (this._operators[i].RefID == prevValue) {
                                            prevValueFound = true;
                                        }
                                    }
                                }

                                if (prevValueFound) {
                                    this._dropdown.val(prevValue);
                                }
                                this._onChange();
                            } else {
                                var param = {
                                    ObjectType: this._options.globalOptions.dataSource.ObjectType,
                                    FieldRefID: this._fieldOnTheLeftSide.RefID.Data
                                };

                                SW.Core.Services.callControllerAction(this._options.globalOptions.dataProviderUrl, "GetOperatorIDsForField", param, function (response) {
                                    _this._dropdown.empty();
                                    for (var i = 0; i < _this._operators.length; i++) {
                                        if (response.indexOf(_this._operators[i].RefID) > -1) {
                                            _this._createItem(_this._dropdown, _this._operators[i].DisplayName, _this._operators[i].RefID);
                                            if (_this._operators[i].RefID == prevValue) {
                                                prevValueFound = true;
                                            }
                                        }
                                    }

                                    if (prevValueFound) {
                                        _this._dropdown.val(prevValue);
                                    }
                                    _this._onChange();
                                }, function () {
                                    $.error("Can't load column data");
                                });
                            }
                        };

                        Operator.prototype._createItem = function (select, text, value) {
                            var o = new Option(text, value);
                            $(o).html(text);
                            select.append(o);
                        };

                        Operator.prototype._isOperatorCompatible = function (op, filter) {
                            if (op.SupportedTypes.length == 0) {
                                return true;
                            }
                            for (var i = 0; i < op.SupportedTypes.length; i++) {
                                if (op.RefID != 'HASCHANGED' && op.SupportedTypes[i] == filter) {
                                    return true;
                                }
                            }
                            return false;
                        };
                        return Operator;
                    })();
                    Input.Operator = Operator;
                })(ExpressionBuilder.Input || (ExpressionBuilder.Input = {}));
                var Input = ExpressionBuilder.Input;
            })(Controls.ExpressionBuilder || (Controls.ExpressionBuilder = {}));
            var ExpressionBuilder = Controls.ExpressionBuilder;
        })(Core.Controls || (Core.Controls = {}));
        var Controls = Core.Controls;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
