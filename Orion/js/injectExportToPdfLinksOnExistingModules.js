jQuery(document).ready(

function() {

    //If the export to PDF link already exists, then exit.
    var exportToPdfElement = document.getElementById('ExportToPdf');
    if (exportToPdfElement)
    {
        return;
    }

    var allDivTags = document.getElementsByTagName('div');

    for (i = 0; i < allDivTags.length; i++) 
    {
        var div = allDivTags[i];
        if (div.className.match('dateArea$') == 'dateArea') 
        {
            var aSibling = div.firstChild;

            var scriptNode = document.createElement('SCRIPT');
            scriptNode.type = 'text/javascript';
            scriptNode.src = '/Orion/js/ExportToPdf.js';

            div.insertBefore(scriptNode, aSibling);



            var newAnchor = document.createElement('a');
            newAnchor.id = 'ExportToPdf';
            newAnchor.className = 'exportToPdf';
            newAnchor.href = '#';
            newAnchor.onclick = function() { ExportToPDF(); return false; };

            var isAPM = (div.className.match('apm') == 'apm');
            if (isAPM)
            {
                newAnchor.style.verticalAlign = 'top';
            }         

            var anchorText = document.createTextNode('Export to PDF');
            newAnchor.appendChild(anchorText);

            if (isAPM)
            {
               //APM requires an outer <span> or the icon won't be hot.
               var newSpan = document.createElement('span');
               newSpan.className = 'apm_IconHelpButton';
               newSpan.appendChild(newAnchor);

               div.insertBefore(newSpan, aSibling);
               return;
            }


            div.insertBefore(newAnchor, aSibling);
        }
    }
}
);

