SW.Core.Formatters = SW.Core.Formatters || {};

(function (ns) {
    /*
    This function translates number of days following this rules:
    days < 2: "1 day"
    days < 14: "{0} days", days
    days < 60: "{0} weeks", days / 7
    days < 366: "{0} months", days / 30
    else: "> 1 year"
    days < 0: exact date and set class from parameter
    */
    ns.capacityForecastDateFormat = function (daysLeft,threshold, className) {
        var cl = '';
        var retVal = '&nbsp;';
        if (daysLeft || (daysLeft === 0)) {
            if (daysLeft <= 0) {
                retVal = '@{R=Core.Strings;K=LIBCODE_VB1_46;E=js}';
                cl = className;
            }
            else {
                if (daysLeft < 2)
                    retVal = '@{R=Core.Strings;K=WEBDATA_LF_day;E=js}';
                else
                    if (daysLeft < 14)
                        retVal = daysLeft + ' @{R=Core.Strings;K=WEBDATA_LF_days;E=js}';
                    else
                        if (daysLeft < 60)
                            retVal = Math.floor(daysLeft / 7) + ' @{R=Core.Strings;K=WEBDATA_LF_weeks;E=js}';
                        else
                            if (daysLeft < 366)
                                retVal = Math.floor(daysLeft / 30) + ' @{R=Core.Strings;K=WEBDATA_LF_months;E=js}';
                            else
                                retVal = '@{R=Core.Strings;K=WEBDATA_LF_moreyear;E=js}';
            }
        }
        return '<span class="sw-forecast-span ' + cl + '"> ' + String.format('@{R=Core.Strings;K=WEBDATA_LF0_17;E=js}', threshold) + ' <br>' + retVal + '</span>';
    }

    ns.capacityForecastDateFormatWOThreshold = function (daysLeft) {
        var retVal = '&nbsp;';
        if (daysLeft || (daysLeft === 0)) {
            if (daysLeft <= 0) {
                retVal = '@{R=Core.Strings;K=LIBCODE_VB1_46;E=js}';
            }
            else {
                if (daysLeft < 2)
                    retVal = '@{R=Core.Strings;K=WEBDATA_LF_day;E=js}';
                else
                    if (daysLeft < 14)
                        retVal = daysLeft + ' @{R=Core.Strings;K=WEBDATA_LF_days;E=js}';
                    else
                        if (daysLeft < 60)
                            retVal = Math.floor(daysLeft / 7) + ' @{R=Core.Strings;K=WEBDATA_LF_weeks;E=js}';
                        else
                            if (daysLeft < 366)
                                retVal = Math.floor(daysLeft / 30) + ' @{R=Core.Strings;K=WEBDATA_LF_months;E=js}';
                            else
                                retVal = '<span class="sw-forecast-morethanyear">' + '@{R=Core.Strings;K=WEBDATA_LF_moreyear;E=js}' + '</span>';
            }
        }
        return '<span class="sw-forecast-span"> ' + retVal + '</span>';
    }

	ns.capacityForecastCellFormat = function (daysLeft,tdjObj, className) {
		var cl = '';
		
		if (daysLeft || (daysLeft === 0)) {
            if (daysLeft <= 0) {
                cl = className;
            }
		}
		tdjObj.addClass(cl);
	}

    ns.capacityForecastDateFormatTable = function (daysLeft, threshold, className) {
        var cl = '';
        var retVal = '&nbsp;';
        if (daysLeft || (daysLeft === 0)) {
            if (daysLeft <= 0) {
                retVal = '@{R=Core.Strings;K=LIBCODE_VB1_46;E=js}';
                cl = className;
            }
            else {
            cl = 'sw-forecast-bold';
                if (daysLeft < 2)
                    retVal = '@{R=Core.Strings;K=WEBDATA_LF_day;E=js}';
                else
                    if (daysLeft < 14)
                        retVal = daysLeft + ' @{R=Core.Strings;K=WEBDATA_LF_days;E=js}';
                    else
                        if (daysLeft < 60)
                            retVal = Math.floor(daysLeft / 7) + ' @{R=Core.Strings;K=WEBDATA_LF_weeks;E=js}';
                        else
                            if (daysLeft < 366)
                                retVal = Math.floor(daysLeft / 30) + ' @{R=Core.Strings;K=WEBDATA_LF_months;E=js}';
                            else
                             {
                                retVal = '@{R=Core.Strings;K=WEBDATA_LF_moreyear;E=js}';
                                cl = '';
                                }
            }
        }
        return '<td style="width:80px;" class="sw-forecast-span ' + cl + '">' +  String.format('@{R=Core.Strings;K=WEBDATA_LF0_17;E=js}', threshold) + ' <br>' +  retVal + '</td>';
    }

    ns.capacityForecastMakeBreakable = function (t) {
        return ns.makeBreakableText(t, 6);
    }

    ns.makeBreakableText = function (text, chunk, br) {
        return ns.insertPerChunks(text, chunk || 10, br || '\u200B');
    };

    ns.formatMacAddress = function (address) {
        return ns.insertPerChunks(address, 4, '-');
    };

    ns.insertPerChunks = function (text, chunk, substring) {
        var pos = chunk;
        while (pos + chunk <= text.length) {
            text = text.substring(0, pos) + substring + text.substring(pos, text.length);
            pos = pos + chunk + substring.length;
        }

        return text;
    };

})(SW.Core.Formatters);