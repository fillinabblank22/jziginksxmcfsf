﻿SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Field").DropDown = function (config) {
    // CONSTANTS
    var DEFAULT_CONFIG = {
        enabledWhenEmpty: false,
        dataTypeFilter: 0
    };

    // PUBLIC METHODS

    this.getExpr = function () {
        return expr;
    };

    this.validate = function () {
        return !!expr.Value;
    };

    this.setFilter = function (masterField) {
        dataTypeFilter = masterField.DataTypeInfo.DeclType;
        options.dataTypeFilter = dataTypeFilter;
        dropdown.prop('disabled', false);
        showFilteredItems();
    };

    // PRIVATE METHODS

    var showFilteredItems = function () {
        var prevValue = expr.Value;
        var prevValueFound = false;
        dropdown.empty();
        if (!expr.Value) {
            createItem(dropdown, "@{R=Core.Strings;K=WEBJS_JP2_2;E=js}", "");       // Select Field ...
        }
        for (i = 0; i < fields.length; i++) {
            if (!options.dataTypeFilter || fields[i].DataTypeInfo.DeclType == options.dataTypeFilter) {
                createItem(dropdown, fields[i].DisplayName, fields[i].RefID.Data);
                if (fields[i].RefID.Data == prevValue) {
                    prevValueFound = true;
                }
            }
        }
        // set previously selected field if it's still visible with new filter
        if (prevValueFound) {
            dropdown.val(prevValue);
        }
    };

    var onChange = function () {
        var prevValue = expr.Value;
        var newValue = dropdown.val();
        expr.Value = newValue;
        if (!prevValue && newValue) {
            //refresh fields dropdown to remove "Select Field..."
            showFilteredItems();
        }

        propagateSet();
    };

    var propagateSet = function () {
        var field;
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].RefID.Data == expr.Value) {
                field = fields[i];
                break;
            }
        }

        if (field && typeof (options.onSet) === 'function') {
            options.onSet(field);
        }
    }

    var createItem = function (select, text, value) {
        var o = new Option(text, value);
        $(o).html(text);
        select.append(o);
    };

    // CONSTRUCTOR
    var self = this;
    var options = $.extend({}, DEFAULT_CONFIG, config);
    var fields = [];
    var expr = options.expr;
    var ds = options.globalOptions.dataSource;
   
    var dropdown = $("<select />");
    dropdown.on("change", onChange);
    dropdown.prop('disabled', !(expr.Value || options.enabledWhenEmpty));
    options.renderTo.append(dropdown);

     SW.Core.Services.callWebService('/Orion/Services/FieldPicker.asmx', 'GetColumns',
        { request: { DataSource: ds, GroupID: '', CategoryID: ds.MasterEntity} },
        function (r) {
            fields = [];
            var rows = r.DataTable.Rows;
            for (var i = 0; i < rows.length; i++) {
                var field = JSON.parse(rows[i][2]);      // get the Field instance
                if (field.DataTypeInfo.IsFilterBy || field.DataTypeInfo.IsGroupBy) {     // get only the basic fields
                    fields.push(field);
                }
            }
            showFilteredItems();
            propagateSet();
        },
        function () { $.error("Can't load column data"); });

    return this;
};