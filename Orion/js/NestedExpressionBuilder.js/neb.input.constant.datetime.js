SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").DateTime = function(config) {

    // PUBLIC METHODS
    this.getExpr = function () {
        switch (options.SelectedOperator.RefID) {
            case "DAYOFWEEKIS":
                expr.Value = dayOfWeeekInput.val();
                break;
            default:
                var dateValue = dateInput.val();
                var datepart;
                if (options.SelectedOperator.RefID == 'TIMEOFDAYISAFTER' || options.SelectedOperator.RefID == 'TIMEOFDAYISBEFORE') {
                    datepart = new Date();
                } else {
                    datepart = $.datepicker.parseDate(regionalSettings.dateFormat, dateValue);
                }

                if (datepart) {
                    var timepart = getTimepicker().getTime() || datepart;

                    var localDate = new Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());

                    var localOffset = (new Date().getTimezoneOffset() * 60 * 1000);

                    expr.Value = new Date(localDate.getTime() - SW.Core.Date.ServerTimeOffset - localOffset).toISOString();

                } else {
                    expr.Value = '';
                }
                break;
        }

        return expr;
    };

    this.validate = function () {
        return !!dateInput.val();
    };

    this.setFilter = function (masterField) {
        dateInput.prop('disabled', false);
        timeInput.prop('disabled', false);
    };

    this.operatorChanged = function (selectedOp) {
        options.SelectedOperator = selectedOp;
        setUpDateTimeInput();
    };

    var setUpDateTimeInput = function() {
        switch (options.SelectedOperator.RefID) {
            case "DAYOFWEEKIS":
                dateInput.hide();
                timeInput.hide();
                dayOfWeeekInput.show();
                break;
            case "TIMEOFDAYISAFTER":
            case "TIMEOFDAYISBEFORE":
                dateInput.hide();
                timeInput.show();
                dayOfWeeekInput.hide();
                break;
            default:
                dateInput.show();
                timeInput.show();
                dayOfWeeekInput.hide();
        }
    };

    var loadRegionalSettings = function() {
        SW.Core.NestedExpressionBuilder.Input.Constant.DateTime.RegionalSettingsLoader.GetRegionalSettingsAsync(function(result) {
            regionalSettings = result;
            initPlugins();
            setDateTime();   
        });
    };

    var initPlugins = function() {
        $.datepicker.setDefaults(regionalSettings);
        dateInput.datepicker({ mandatory: true, closeAtTop: false });
        timeInput.timePicker({ separator: regionalSettings.timeSeparator, show24Hours: regionalSettings.show24Hours });
        // add week day according to regional settings 
        var currentDay = regionalSettings.firstDay;
        for (var i = 0; i < weekDayNames.length; i++) {
            dayOfWeeekInput.append('<option value="' + currentDay + '">' + weekDayNames[currentDay] + '</option>');
            currentDay = (currentDay == weekDayNames.length - 1) ? 0 : currentDay + 1;
        }
    };

    var weekDayNames = [
        '@{R=Core.Strings;K=Charts_DOW_YK0_1;E=js}',
        '@{R=Core.Strings;K=Charts_DOW_YK0_2;E=js}',
        '@{R=Core.Strings;K=Charts_DOW_YK0_3;E=js}',
        '@{R=Core.Strings;K=Charts_DOW_YK0_4;E=js}',
        '@{R=Core.Strings;K=Charts_DOW_YK0_5;E=js}',
        '@{R=Core.Strings;K=Charts_DOW_YK0_6;E=js}',
        '@{R=Core.Strings;K=Charts_DOW_YK0_7;E=js}',
    ];

    var setDateTime = function () {
        setUpDateTimeInput();
        switch (options.SelectedOperator.RefID) {
        case "DAYOFWEEKIS":
            dayOfWeeekInput.val(expr.Value);
            break;
        default:
            try {
                if (expr.Value) {
                    var date = dateFromISO8601(expr.Value);
                    // Datetime is saved in UTC, we need convert it to server time.
                    var serverDate = new Date(date.getTime() + SW.Core.Date.ServerTimeOffset);

                    if (options.SelectedOperator.RefID != 'TIMEOFDAYISAFTER' && options.SelectedOperator.RefID != 'TIMEOFDAYISBEFORE')
                        dateInput.val($.datepicker.formatDate(regionalSettings.dateFormat, serverDate));
                    getTimepicker().setTime(serverDate);
                }

            } catch (e) {
                // When we change field in AQB with different type we can get in expr.Value non-datetime value 
                // In that case we just leave empty value in date/time inputs.
            }
            break;
        }
    };

    var getTimepicker = function() {
        return $('.timePicker', options.renderTo)[0].timePicker;
    };

    // CONSTRUCTOR
    var self = this;
    var options = config;
    var expr = options.expr;
    var ds = options.globalOptions.dataSource;
    var field;
    var regionalSettings;
    var selectedOpRefId;

    var dateInput = $('<input type="text" class="datePicker disposable" style="width: 100px" />');
    var timeInput = $('<input type="text" class="timePicker disposable" style="width: 80px" />');
    var dayOfWeeekInput = $('<select style="display:none;"></select>');

    var disabled = !(expr.Value || options.enabledWhenEmpty);
    dateInput.prop('disabled', disabled);
    timeInput.prop('disabled', disabled);

    options.renderTo.append(dateInput, timeInput, dayOfWeeekInput);

    loadRegionalSettings();

    return this;
};

SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").DateTime.RegionalSettingsLoader = (function() {
    var getDefaultRegionalSettings = function() {
        return {
            dateFormat: "m/d/yy",
            show24Hours: false,
            timeSeparator: ":"
        };
    };

    var loadRegionalSetting = function() {
        SW.Core.Services.callWebService('/Orion/Services/ExpressionBuilderDataProvider.asmx', 'GetDatePickerRegionalSettings', {},
            function(res) {
                regionalSettings = res ? JSON.parse(res) : getDefaultRegionalSettings();
                pulseWaiters();
            },
            function() {
                // OnError we set default regional settings.
                regionalSettings = getDefaultRegionalSettings();
                pulseWaiters();
            });
    };

    var pulseWaiters = function() {
        _.each(waiters, function(waiter) {
            waiter(regionalSettings);
        });

        waiters = [];
    };

    var getRegionalSettingsAsync = function(callback) {
        // Already loaded ?
        if (regionalSettings) {
            callback(regionalSettings);
        } else {
            // No, wait for server response.
            waiters.push(callback);
        }
    };

    // CONSTRUCTOR
    var regionalSettings = null;
    var waiters = [];

    // Call function on document ready.
    $(loadRegionalSetting);

    return {
        GetRegionalSettingsAsync: getRegionalSettingsAsync
    };

}());

if ( !Date.prototype.toISOString ) {
  ( function() {
    
    function pad(number) {
      var r = String(number);
      if ( r.length === 1 ) {
        r = '0' + r;
      }
      return r;
    }
 
    Date.prototype.toISOString = function() {
      return this.getUTCFullYear()
        + '-' + pad( this.getUTCMonth() + 1 )
        + '-' + pad( this.getUTCDate() )
        + 'T' + pad( this.getUTCHours() )
        + ':' + pad( this.getUTCMinutes() )
        + ':' + pad( this.getUTCSeconds() )
        + '.' + String( (this.getUTCMilliseconds()/1000).toFixed(3) ).slice( 2, 5 )
        + 'Z';
    };
  
  }() );
}

function dateFromISO8601(isostr) {
	var parts = isostr.match(/\d+/g);
	return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
}