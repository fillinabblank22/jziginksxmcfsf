SW.Core.namespace("SW.Core.NestedExpressionBuilder").Control = function (config) {

    // PUBLIC METHODS        

    this.refresh = function () {
        return render(settings.expr);
    };

    this.getExpression = function () {
        if (getIsEmpty()) {
            return null;
        }
        return getExpr($(".neb-top-node", elem));
    };

    this.validate = function () {
        return true;
    };
	
    this.validateConstantValues = function () {
        if (getIsEmpty()) {
            return true;
        }
        return doValidation($(".neb-top-node", elem), true);
    };

    // PRIVATE METHODS
    
    var getDefaults = function () {
        return {
            // text to display for the root node element (upper left corner), "Where" by default
            rootNodeText: "@{R=Core.Strings;K=WEBJS_JP2_3;E=js}",

            // background colors to use
            bgColorAnd: "#ffe89e",
            bgColorOr: "#daf7cc",
            bgColorChild: "#ebebeb",

            // global settings / switches
            allowGroups: false,
            htmlentities: false,
            decimal_separator: ".",

            // css style classes
            containerClass: "neb-rules-container",
            rulesListLiErrorClass: "neb-error-li",
            rulesListLiAppliedClass: "neb-applied-li",

            filterInputTextClass: "neb-rhs-text",
            filterInputNumberClass: "neb-rhs-number",
            filterInputDateClass: "neb-rhs-date",
            filterInputCheckboxClass: "neb-rhs-checkbox",
            filterInputRadioClass: "neb-rhs-radio",
            filterSelectClass: "neb-rhs-select",
            filterGroupListClass: "neb-rhs-group-list",
            filterGroupListItemHorizontalClass: "neb-rhs-group-list-item-horizontal",
            filterGroupListItemVerticalClass: "neb-rhs-group-list-item-vertical",

            // function hooks
            onValidationError: function () { },
            onEmpty: function (isEmpty) { },

            fieldInput: SW.Core.NestedExpressionBuilder.Input.Field.Picker,
            operatorInput: SW.Core.NestedExpressionBuilder.Input.Operator,
            constantInput: SW.Core.NestedExpressionBuilder.Input.Constant.Wrapper,
            disableAutocomplete: false,

            constantMapping: {}
        };
    };

    var getDefaultGroupExpr = function () {
        return {
            NodeType: SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR,
            Value: settings.groupOperators[0].RefID,
            Child: [
                getRuleExpr(0)
            ]
        };
    };

    var getRuleExpr = function (ruleId) {
        return settings.ruleTypes[ruleId].expr;
    };

    var getAllFields = function () {
        function visit(expr, fields) {
            if (expr && expr.NodeType == SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD && expr.Value) {
                fields.push(expr.Value);
            } else if (expr && expr.NodeType == SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR && expr.Child.length > 0) {
                for (var i = 0; i < expr.Child.length; i++) {
                    visit(expr.Child[i], fields);
                }
            }
            return fields;
        }

        return visit(self.getExpression(), []);
    };

    var getRuleType = function(expr) {
        if (expr.Child.length === 2
            && (expr.Child[0].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT)
            && (expr.Child[1].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT)) {

            return SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT;
        }
        
        if (expr.Child.length === 2
            && (expr.Child[0].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD)
            && (expr.Child[1].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT)) {
              return SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT;    
        }

         if (expr.Child.length === 2
            && (expr.Child[0].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT)
            && (expr.Child[1].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD)) {
              return SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_FIELD;    
        }
  
        if (expr.Child.length === 2
            && (expr.Child[0].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD)
            && (expr.Child[1].NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD)) {
              return SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_FIELD;    
        }

        return SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.UNKNOWN;
    };

    var isConstantOnTheLeft = function(ruleType) {
        return ruleType === SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_FIELD
            || ruleType === SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT;
    };
    

     var isConstantOnTheRight = function(ruleType) {
        return ruleType === SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.FIELD_TO_CONSTANT
            || ruleType === SW.Core.NestedExpressionBuilder.Constants.RULE_TYPE.CONSTANT_TO_CONSTANT;
    };
	
    // create condition dropdown (AND/OR)
    var createGroupConditionDropdown = function (value) {
        var ops = settings.groupOperators;
        var span = $('<span class="neb-group-condition-container"></span>');
        var select = $('<select class="neb-group-condition"></select>').appendTo(span);

        for (var i = 0; i < ops.length; i++) {
            var o = new Option(ops[i].DisplayName, ops[i].RefID);
            $(o).html(ops[i].DisplayName);
            select.append(o);
        }

        if (value) {
            select.val(value);
        }

        return span;
    };

    // create Group Actions dropdown (add child/add block)
    var createGroupActionList = function (condition_list_container) {
        var div, div2, table, tr, td;

        div = $('<div class="neb-item-add-wrap"></div>');
        if (settings.allowGroups || settings.ruleTypes.length > 1) {
            $('<div class="neb-item-add"></div>').appendTo(div);
            div2 = $('<div class="neb-item-add-options"></div>').appendTo(div);
            table = $('<table></table>').appendTo(div2);
            for (var i = 0; i < settings.ruleTypes.length; i++) {
                tr = $('<tr></tr>').appendTo(table);
                td = $('<td class="neb-action"></td>').appendTo(tr);
                td.text(settings.ruleTypes[i].displayName);
                td.data('expr', getRuleExpr(i));
                td.data('container', condition_list_container);
                if (settings.ruleTypes[i].hint) {
                    var tooltip = $('<div class="sw-suggestion sw-suggestion-noicon neb-action-hint">' + settings.ruleTypes[i].hint + '</div>').appendTo(td);
                    td.data('tooltip', tooltip);
                    td.on('mouseover', function () {
                        var w = $(this).width();
                        var o = $(this).offset();
                        t = $(this).data('tooltip');
                        t.show();
                        t.offset({ top: o.top, left: o.left + w + 20 });
                    });
                    td.on('mouseout', function () {
                        t = $(this).data('tooltip');
                        t.hide();
                    });
                }
            }
            if (settings.allowGroups) {
                tr = $('<tr></tr>').appendTo(table);
                td = $('<td class="neb-action"></td>').appendTo(tr);
                td.text("@{R=Core.Strings;K=WEBJS_JP2_4;E=js}");                            // Add And/Or block
                td.data('expr', getDefaultGroupExpr());
                td.data('container', condition_list_container);
            }
        }
        else {
            table = $('<table></table>').appendTo(div);
            tr = $('<tr><td class="neb-simple-elbow"><img src="/Orion/images/NestedExpressionBuilder/tree_elbow.png"></img></td></tr>').appendTo(table);

            td = $('<td class="neb-action neb-add-simple"></td>').appendTo(tr);
            td.text(settings.ruleTypes[0].displayName);
            td.data('expr', getRuleExpr(0));
            td.data('container', condition_list_container);
        }

        return div;
    };

    var createExprBlock = function (expr) {
        $('.neb-item-add-down').removeClass('neb-item-add-down');

        if (expr.NodeType != SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR) {
            $.error('Incorrect expression. Expecting Operator(0), got type: ' + expr.NodeType);
        }
        for (var i = 0; i < settings.groupOperators.length; i++) {
            if (settings.groupOperators[i].RefID == expr.Value) {
                return createGroupConditionBlock(expr);
            }
        }
        return createLeafConditionBlock(expr);
    };

    var createMainGroupConditionBlock = function (expr) {
        var root, table, tr, td, ul;
        var value = expr.Value;

        root = $('<div></div>').addClass('neb');
        // workaround jquery 1.7 bug
        table = $('<table cellspacing="0"></table>').appendTo(root);
        tr = $('<tr></tr>').appendTo(table);
        td = $('<td class="neb-condition-list-container"></td>').appendTo(tr);
        ul = $('<ul class="neb-node neb-top-node neb-condition-list neb-group"></ul>').appendTo(td);
        ul.data('value', value);

        table = $('<table cellspacing="0"></table>').appendTo(ul);

        // ROW 1 - is the condition dropdown (and/or)
        tr = $('<tr class="neb-condition neb-condition-' + value.toLowerCase() + '"></tr>').appendTo(table);

        if (settings.rootNodeText) {
            $('<td class="neb-root-text" style="width:30px;">' + settings.rootNodeText + '</td><td style="background-color: transparent;width: 9px; max-width: 9px;"><div class="neb-root-line"></div></td>').appendTo(tr);
        }
        else {
            $('<td>&nbsp;</td>').appendTo(tr);
        }

        $('<td class="neb-group-dropdown"></td>').append(createGroupConditionDropdown(value)).appendTo(tr);
        td = $('<td class="neb-preference" style="width: 44px;min-width:44px;max-width:44px;">&nbsp;</td>').appendTo(tr);
        td.append(createPreferencepActionList());

        // ROW 2 - is an unordered list of children
        tr = $('<tr></tr>').appendTo(table);
        td = $('<td class="neb-condition-list-container" colspan="3"></td>').appendTo(tr);

        if (settings.rootNodeText) {
            td.css("padding-left", "51px");
        }

        ul = $('<ul class="neb-node neb-condition-list"></ul>').appendTo(td);
        $.each(expr.Child, function (index, child) {
            //if (child.Value != null) {
            ul.append(createExprBlock(child));
           // }
        });

        // ROW 3 - is the group action button (add new children / condition blocks)
        tr = $('<tr></tr>').appendTo(table);
        td = $('<td colspan="3"></td>').appendTo(tr);

        if (settings.rootNodeText) {
            td.css("padding-left", "51px");
        }

        td.append(createGroupActionList(ul));

        return root;
    };

    var _setHandlerForPreferenceOption = function(item,  callBackFunc, actionId) {
        item.on("mousedown", function () { callBackFunc(actionId); });
    };

    var createPreferencepActionList = function () {
        var div, div2, table, tr, td, span;

        if (settings.preferenceOptions.length > 0) {

            div = $('<div class="neb-pref-options"></div>');
            table = $('<table></table>').appendTo(div);
            for (var i = 0; i < settings.preferenceOptions.length; i++) {
                tr = $('<tr class="neb-pref-item"></tr>').appendTo(table);
                var tdElement = '<td class="neb-pref-action" id="pA-' + settings.preferenceOptions[i].actionId + '"></td>';
                td = $(tdElement).appendTo(tr);
                var actionId = settings.preferenceOptions[i].actionId;
                var callBackFunc = settings.preferenceOptions[i].callback;
                _setHandlerForPreferenceOption(td, callBackFunc, actionId);
                td.text(settings.preferenceOptions[i].displayName);
            }
        }
        return div;
    };

    var createGroupConditionBlock = function (expr) {
        var table, tr, td, ul, li;
        var value = expr.Value;

        li = $('<li class="neb-node neb-group"></li>');
        li.data('value', value);
        table = $('<table cellspacing="0"></table>').appendTo(li);

        // ROW 1 - is the condition dropdown (and/or)
        tr = $('<tr class="neb-condition neb-condition-' + value.toLowerCase() + '"></tr>').appendTo(table);
        $('<td style="background-color: transparent;width: 9px; max-width: 9px;"><div class="neb-root-line"></div></td>').appendTo(tr);
        $('<td class="neb-drag-handle"></td>').appendTo(tr);
        $('<td></td>').append(createGroupConditionDropdown(value)).appendTo(tr);
        // delete action
        td = $('<td class="neb-item-close neb-action"></td>').appendTo(tr);
        td.data('container', li);

        // ROW 2 - is an unordered list of children
        tr = $('<tr></tr>').appendTo(table);
        td = $('<td class="neb-condition-list-container" colspan="3"></td>').appendTo(tr);
        ul = $('<ul class="neb-node neb-condition-list"></ul>').appendTo(td);
        $.each(expr.Child, function (index, child) {
            ul.append(createExprBlock(child));
        });

        // ROW 3 - is the group action button (add new children / condition blocks)
        tr = $('<tr></tr>').appendTo(table);
        td = $('<td colspan="100"></td>').appendTo(tr);
        td.append(createGroupActionList(ul));

        return li;
    };

    var createLeafConditionBlock = function (expr) {
        var li, table, tr, td, spanLeft, spanOp, spanRight, lhs, rhs, op, thisExpr;
        var bgCol = settings.bgColorChild;
        expr = $.extend(true, {}, expr); // deep copy

        if (expr.Child.length == 1) {
            //create temp rhs when operator is unary
            expr.Child[1] = {
                NodeType: SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT
            };
        }

        li = $('<li class="neb-node"></li>');
        table = $('<table></table>').appendTo(li);
        tr = $('<tr class="neb-item"></tr>').appendTo(table);
        $('<td style="background-color: transparent;width: 9px; max-width: 9px;"><div class="neb-root-line"></div></td>').appendTo(tr);
        $('<td class="neb-drag-handle"></td>').css('background-color', bgCol).appendTo(tr);

        td = $('<td></td>').css('background-color', bgCol).appendTo(tr);
        spanLeft = $('<span class="neb-lhs"></span>').appendTo(td);

        td = $('<td></td>').css('background-color', bgCol).appendTo(tr);
        spanOp = $('<span class="neb-op"></span>').appendTo(td);

        td = $('<td></td>').css('background-color', bgCol).appendTo(tr);
        spanRight = $('<span class="neb-rhs"></span>').appendTo(td);

        // delete action
        td = $('<td class="neb-item-close neb-action"></td>').css('background-color', bgCol).appendTo(tr);
        td.data('container', li);

        // 30px spacer cell on the end for quick automatic layout
        //'<td style="width: 30px; max-width: 30px; min-width: 30px;">&nbsp;</td>'

        var ruleType = getRuleType(expr);

        lhs = createInput({
            renderTo: spanLeft,
            expr: expr.Child[0],
            enabledWhenEmpty: true,
            globalOptions: settings,
            onSet: function(field) {
                op.setFilter(field);
                rhs.setFilter(field);
                updateIsEmpty(false);
            },
            getAllFields: getAllFields,
            side: SW.Core.NestedExpressionBuilder.Constants.EXPR_SIDE.LEFT
        });
        
        rhs = createInput({
            renderTo: spanRight,
            expr: expr.Child[1],
            globalOptions: settings,
            onSet: function (field) {

            },
            getAllFields: getAllFields,
            side: SW.Core.NestedExpressionBuilder.Constants.EXPR_SIDE.RIGHT
        });

        thisExpr = $.extend({}, expr);      // shallow copy
        thisExpr.Child = [];                // do not pass children
        op = new settings.operatorInput({
            renderTo: spanOp,
            globalOptions: settings,
            expr: thisExpr,
            leftSide: lhs,
            rightSide: rhs,
            onSet: function (selectedOp) {
                spanRight.toggle(selectedOp.Type != SW.Core.NestedExpressionBuilder.Constants.OPERATOR_TYPE.UNARY);
                if (typeof (this.rightSide.operatorChanged) === "function") {
                    rhs.SelectedOperator = selectedOp;
                    this.rightSide.operatorChanged(selectedOp);
                }
            }
        });

      
        if (isConstantOnTheLeft(ruleType)) {
            // For constant to constant/field rule we hardcode datatype to TEXT.
            var textField = { 
                DataTypeInfo: { DeclType: 5 } // TEXT
            }; 
                    
            op.setFilter(textField);
            lhs.setFilter(textField);
            rhs.setFilter(textField);

            //if (isConstantOnTheRight(ruleType)) {
            //     rhs.setFilter(textField);
            //}

            
        }
        
          
        li.data('op', op);

        return li;
    };

    var createInput = function (config) {
        var input;

        if (config.expr.NodeType == SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD) {
            input = new settings.fieldInput(config);
        }
        else if (config.expr.NodeType == SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT) {
            input = new settings.constantInput(config);
        }
        else {
            $.error('Unexpected expression type');
        }

        return input;
    };

    var getExpr = function (conditionBlock) {
        var expr;
        if (conditionBlock.hasClass('neb-group')) {
            expr = {
                NodeType: SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.OPERATOR,
                Value: conditionBlock.data('value'),
                Child: []
            };
            var container = conditionBlock.find(".neb-condition-list:first");
            container.children().each(function (index, item) {
                expr.Child.push(getExpr($(item)));
            });
        }
        else {
            expr = conditionBlock.data('op').getExpr();
        }
        return expr;
    };
    
    var getIsEmpty = function (conditionBlock) {
        if (!conditionBlock) {
            return getIsEmpty($(".neb-top-node", elem));
        }
        if (conditionBlock.hasClass('neb-group')) {
            var container = conditionBlock.find(".neb-condition-list:first");
            var atLeastOneNotEmpty = false;
            container.children().each(function (index, item) {
                if (!getIsEmpty($(item))) {
                    atLeastOneNotEmpty = true;
                    return false;   //break
                }
            });
            return !atLeastOneNotEmpty;
        }
        else {
            return conditionBlock.data('op').isEmpty();
        }
    };
  
    var render = function (expr) {
        elem.empty();
        elem.append(createMainGroupConditionBlock(expr));
        return elem;
    };

    var updateHandlers = function () {
        // update drag handlers
        var bTemp;
        $(".neb-condition-list", elem).sortable({
            handle: ".neb-drag-handle",
            placeholder: "neb-drag-target",
            connectWith: ".neb-condition-list:not('.neb-top-node')",
            start: function (evt, ui) {
                bTemp = ui.item.css("border-left");
                ui.item.css("border-left", "0px");
            },
            stop: function (evt, ui) {
                ui.item.css("border-left", bTemp);
            }
        });

        // update group action handlers
        $(".neb-item-add:not(.bound)", elem).addClass("bound").bind("click", function () {
            var action_btn = $(this);
            var action_list = $(this).next();
            if (action_list.is(":visible")) {
                action_list.css('display', 'none');
                action_btn.removeClass('neb-item-add-down');
            }
            else {
                action_list.css('display', 'inline-block');
                action_btn.addClass('neb-item-add-down');
            }
        });

        // update pref action handlers
        $(".neb-preference:not(.bound)", this._elem).addClass("bound").bind("click", function () {
            var action_btn = $(this);
            var action_list = $($(this).children()[0]);
            if (action_list.is(":visible")) {
                action_list.css('display', 'none');
                action_btn.removeClass('neb-preference-open');
            } else {
                action_list.css('display', 'inline-block');
                action_btn.addClass('neb-preference-open');
            }
        });

        $(".neb-item-add-options").hide();
        $(".neb-pref-options").hide();

        // update condition select change
        $(".neb-group-condition:not(.bound)", elem).addClass("bound").bind("change", function () {
            var newValue = $(this).val();
            $(this).closest("tr").attr("class", "neb-condition neb-condition-" + newValue.toLowerCase());
            $(this).closest(".neb-node").data('value', newValue);
        });

        $(".neb-lhs").one("change", function () {
            if (!getIsEmpty() && config.updateSourceNameDelegate) {
                config.updateSourceNameDelegate(false);
            }
        });

        if (getIsEmpty() && config.updateSourceNameDelegate) {
            config.updateSourceNameDelegate(true);
        }

    };
   
    var updateIsEmpty = function (newState) {
        if (isEmpty != newState) {
          isEmpty = newState;
          settings.onEmpty(isEmpty);
        } 
    };

    var doValidation = function (conditionBlock) {
        var iconCell = $("<td class='neb-validation-icon'></td>");
        var rowCells, op, row;
        var validationState = true;
        
        if (conditionBlock.hasClass("neb-group")) {
            var container = conditionBlock.find(".neb-condition-list:first");
            container.children().each(function (index, item) {
                validationState &= doValidation($(item));
            });
        }
        else {
            op = conditionBlock.data("op");
            row = conditionBlock.find(".neb-item");
            rowCells = row.children(":not(:first-child)");
            
            // cleanup
            row.find("td.neb-validation-icon").remove();
            rowCells.removeClass("neb-validation-error");
            
            // if there is a incorrect value add error class to cells and append the error icon
            if (!op.validate()) {
                rowCells.addClass("neb-validation-error");
                iconCell.attr("title",op.getErrorMessage()).appendTo(row);
                return false;
            }
        }
        return validationState;
    };
    
    // CONSTRUCTOR

    var self = this;
    var settings = $.extend({}, getDefaults(), config);
    var isEmpty = !settings.expr;
    if (isEmpty) {
        settings.expr = getDefaultGroupExpr();
    }
    var elem = settings.renderTo;
    elem.addClass(settings.containerClass);
    
	render(settings.expr);

    // bind events
    elem.unbind("onValidationError").bind("onValidationError", settings.onValidationError);

    updateHandlers();

    // set up handler for add/remove actions
    var selector = '.neb-action';
    elem.off("click", selector).on("click", selector, function () {
        var container = $(this).data("container");
        var expr = $(this).data("expr");
        if (expr) {
            container.append(createExprBlock(expr));
        }
        else {
            // delete action
            container.remove();
            updateIsEmpty(getIsEmpty());
        }
        updateHandlers();
    });


    // handle mousedown outside of add item menu
    $(".neb").mousedown(function (event) {
        if (!($(event.target).hasClass('neb-action'))) {
            $('.neb-item-add-options').hide();
            $('.neb-item-add-down').removeClass('neb-item-add-down');
        }
        if (!($(event.target).hasClass('neb-pref-item'))) {
            $('.neb-pref-options').hide();
            $('.neb-preference').removeClass('neb-preference-open');
        }
    });

    settings.onEmpty(isEmpty);

    return self;
};
