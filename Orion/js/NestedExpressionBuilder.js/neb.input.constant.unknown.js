﻿SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").Unknown = function (config) {

    // PUBLIC METHODS

    this.getExpr = function () {
        expr.Value = '';
        return expr;
    };

    this.validate = function () {
        return true;
    };

    this.setFilter = function (masterField) {
        
    };

    // CONSTRUCTOR
    var self = this;
    var options = config;
    var expr = options.expr;
    var ds = options.globalOptions.dataSource;
    var field;
   
    var input = $('<input type="text" />');
    input.prop('disabled', true);

    options.renderTo.append(input);

    return this;
}