(function ($) {

    var pluginName = "nested_expression_builder";

    $.fn[pluginName] = function (config) {

        // OPTIONAL
        if (this.size() != 1) {
            var err_msg = "You must use the jQuery plugin (" + pluginName + ") with a unique element <div id=my_query_builder></div>";
            this.html('<span style="color: red;">' + 'ERROR: ' + err_msg + '</span>');
            $.error(err_msg);
        }

        var method = config;
        var isConfig = !!config && !!config.ruleTypes;

        var neb = this.data(pluginName);
        // initialize if it isn't yet
        if (!neb || isConfig) {
            if (!config || !config.ruleTypes) {
                $.error('Configuration is not well defined. At least one rule must be defined in ruleTypes array.');
            }
            $.extend(config, { renderTo: this });
            neb = new SW.Core.NestedExpressionBuilder.Control(config);
            this.data(pluginName, neb);
            return this;
        }
      
        if (method == 'destroy') {
            return this.removeData(pluginName);
        }
        else if (neb[method]) {
            return neb[method].apply(neb, Array.prototype.slice.call(arguments, 1));
        }
        else {
            $.error('Method ' + method + ' does not exist on jQuery.' + pluginName);
        }
    };

})(jQuery);