SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Field").Picker = function (config) {
    // CONSTANTS
    var FIELD_PICKER_INSTANCE = 'fieldPickerInput_Instance';
    var LINK_TEXT = "@{R=Core.Strings;K=WEBJS_JP2_2;E=js}";  // Select Field ...
    var DEFAULT_CONFIG = {
        enabledWhenEmpty: false,
        dataTypeFilter: 0
    };
    var extraRequestParams = config.globalOptions.extraRequestParams;

    // PUBLIC METHODS

    this.getExpr = function () {
        return expr;
    };

    this.validate = function () {
        return !!expr.Value;
    };

    this.setFilter = function (masterField) {

        filter = masterField;
        dataTypeFilter = masterField.DataTypeInfo.DeclType;
        options.dataTypeFilter = dataTypeFilter;
        link.removeClass('neb-fieldpicker-disabled');
        if (field) {
            //check if current field conforms the new filter
            if (field.DataTypeInfo.DeclType != options.dataTypeFilter) {
                field = null;
                expr.Value = '';
                link.text(LINK_TEXT);
            }
        }
    };
    
     this.getFilter = function() {      
        return filter;
    };

    // PRIVATE METHODS

     var convertToDeclarationType = function (dType) {
        if (dType == "System.String"){
             return pickers.DataDeclarationType.Text;
		}
        if (dType == "System.Int64" || dType == "System.Int32" || dType == "System.Int16"){
             return pickers.DataDeclarationType.Integer;
		}
        if (dType == "System.Double" || dType == "System.Single"){
             return pickers.DataDeclarationType.Float;
		}
        if (dType == "System.Boolean"){
             return pickers.DataDeclarationType.Boolean;
		}
        if (dType == "System.Char"){
             return pickers.DataDeclarationType.Char;
		}
        if (dType == "System.Guid"){
             return pickers.DataDeclarationType.Uuid;
		}
        if (dType == "System.DateTime"){
             return pickers.DataDeclarationType.DateTime;
		}
        if (dType == "System.Byte"){
             return pickers.DataDeclarationType.Byte;
        }
        return pickers.DataDeclarationType.NotSpecified;
     };

    var onChange = function () {
        expr.Value = field.RefID.Data;
        link.text(field.DisplayName);
        if (typeof (options.onSet) === 'function') {
            options.onSet(field);
        }
    };

    var openFieldPicker = function (event) {
        event.stopPropagation();
        event.preventDefault();

        if (!link.hasClass('neb-fieldpicker-disabled')) {
            picker.SetOnSelected(onSetPicker);
            if (filter && filter.DataTypeInfo && filter.DataTypeInfo.DataType) {
                picker.SetDataTypesFilters(options.dataTypeFilter, convertToDeclarationType(filter.DataTypeInfo.DataType.Data));
            } else {
                picker.SetDataTypeFilter(options.dataTypeFilter);
            }            
            var currentFieldID = null;
            if (expr && expr.Value) {
                currentFieldID = expr.Value;
            }
            picker.ComputeAndSetNavigationPathFilterAsync(options.getAllFields(), currentFieldID, function () {
                picker.ShowInDialog();
                picker.Reload();
            });       
        }
        return false;
    };

    var onSetPicker = function (picker) {
        if (picker.GetSelectedFields()[0]) {
        field = picker.GetSelectedFields()[0].Field;
        onChange();
    }
    };

    // CONSTRUCTOR
    var self = this;
    var options = $.extend({}, DEFAULT_CONFIG, config);
    var expr = options.expr;
    var ds = options.globalOptions.dataSource;
    var field;
    var filter = null;
    var picker = SW.Core.Pickers.FieldPicker.Instances[FIELD_PICKER_INSTANCE];
    picker.SetDataSource(ds);
    picker.SetDataTypeFilter(options.dataTypeFilter);
    var link = $('<a class="neb-fieldpicker"></a>').on('click', openFieldPicker);
    link.toggleClass('neb-fieldpicker-disabled', !(expr.Value || options.enabledWhenEmpty));
    options.renderTo.append(link);

    if (expr.Value) {
        // loading existing data, need to get full Field to get DisplayName
        SW.Core.Services.callWebService(config.globalOptions.dataProviderUrl || '/Orion/Services/ExpressionBuilderDataProvider.asmx', 'GetField',
             { dataSource: ds, fieldRefId: expr.Value, extraRequestParams : extraRequestParams },
            function (res) {
                field = res;
                onChange();
            },
            function () { $.error("Can't load column data"); });
    }
    else {
        link.text(LINK_TEXT);
    }

    return this;
};

