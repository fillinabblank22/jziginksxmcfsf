SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").Text = function (config) {

     var texts = {
        'watermark-text': "@{R=Core.Strings;K=WEBJS_ZT0_35;E=js}"
    };


    var getVal = function() {
        var val = input.val();
        
        if (val == texts[ 'watermark-text']) {
            val = '';
        }

        return val;
    };


    // PUBLIC METHODS

    this.getExpr = function () {
        expr.Value = getVal();
        return expr;
    };

    this.validate = function () {
        return !!getVal();
    };

    this.setFilter = function (masterField) {
        field = masterField;
        input.prop('disabled', false);
        input.autocomplete('enable');
    };
    
    // PUBLIC PROPERTIES
    
    this.errorMessage = "@{R=Core.Strings;K=WEBJS_PD0_02;E=js}";
	
	// CONSTRUCTOR
    var self = this;
    var options = config;
    var expr = options.expr;
    var ds = options.globalOptions.dataSource;
    var extraRequestParams = options.globalOptions.extraRequestParams;
    var field;
    var input = $('<input type="text" />');
    var watermark;

    input.val(expr.Value || '');
    input.prop('disabled', !(expr.Value || options.enabledWhenEmpty));


    if (options.globalOptions.enableWatermark) {
        watermark = new SW.Core.Widgets.WatermarkTextbox(input, texts[ 'watermark-text']);
    }

    if(!options.globalOptions.disableAutocomplete) {
        input.autocomplete({
            disabled: true,
            delay: 500,
            minLength: 0,
            source: function (request, response) {
                var params = {fieldRefId: '', dataSource: ds, searchTerm: request.term, extraRequestParams: extraRequestParams };
                
                if (field && field.RefID) {
                    params.fieldRefId = field.RefID.Data;
                }
                              
                SW.Core.Services.callWebService(  options.globalOptions.dataProviderUrl || '/Orion/Services/ExpressionBuilderDataProvider.asmx', 'GetFieldAutocompleteValues', params,
                function (res) {
                    response(res);
                },
                function () {
                    response([]);
                    $.error("Can't load autocomplete data");
                });
    
            }
        });
    }
    
    options.renderTo.append(input);

    return this;
};