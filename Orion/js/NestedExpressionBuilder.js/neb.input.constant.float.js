SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").Float = function (config) {
    // PUBLIC METHODS

    this.getExpr = function () {
        expr.Value = input.val().replace(options.globalOptions.decimal_separator, ".");
        return expr;
    };

    this.validate = function () {
        return isFloat(input.val());
    };

    this.setFilter = function (masterField) {
        input.prop('disabled', false);
    };

    // PUBLIC PROPERTIES

    this.errorMessage = "@{R=Core.Strings;K=WEBJS_PD0_01;E=js}";


    // PRIVATE METHODS

    function isFloat(n) {
        var val = String(n).replace(options.globalOptions.decimal_separator, ".");
        var numberVal = Number(val);
        if (numberVal > 3.40282347E+38 || numberVal < -3.40282347E+38 || isNaN(numberVal))
            return false;
        return (options.globalOptions.decimal_separator == '.') ? /^-?\d+(\.\d{0,7})?([eE][-+]?[0-9]+)?$/.test(n) : /^-?\d+(\,\d{0,7})?([eE][-+]?[0-9]+)?$/.test(n);
    };

    // CONSTRUCTOR
    var self = this;
    var options = $.extend({}, config);
    var expr = options.expr;
    var input = $('<input type="text" />');
    input.val((expr.Value || '').replace(".", options.globalOptions.decimal_separator));
    input.prop('disabled', !(expr.Value || options.enabledWhenEmpty));
    options.renderTo.append(input);

    return this;
};