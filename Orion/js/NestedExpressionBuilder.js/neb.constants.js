SW.Core.namespace("SW.Core.NestedExpressionBuilder").Constants = {
    EXPRESSION_TYPE: {
        OPERATOR: 0,
        FIELD: 1,
        CONSTANT: 2
    },

    OPERATOR_TYPE: {
        UNARY: 0,
        BINARY: 1,
        NARY: 2
    },
    
    RULE_TYPE: {
        UNKNOWN: 0,
        FIELD_TO_CONSTANT: 1,
        FIELD_TO_FIELD: 2,
        CONSTANT_TO_CONSTANT: 3,
        CONSTANT_TO_FIELD: 4
    },
    
    EXPR_SIDE: {
        UNKNOWN: 0,
        LEFT: 1,
        RIGHT: 2
    }
} 