SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input").Operator = function (config) {
    
    // CONSTANTS
    
    var DEFAULT_CONFIG = {
        dataTypeFilter: ''
    };

    // PUBLIC METHODS

    this.getExpr = function () {
        expr.Child = [options.leftSide.getExpr()];
        if (operator.Type != SW.Core.NestedExpressionBuilder.Constants.OPERATOR_TYPE.UNARY) {
            expr.Child.push(options.rightSide.getExpr());
        }
        return expr;
    };

    this.validate = function () {
        var result = options.leftSide.validate();
        if (operator.Type != SW.Core.NestedExpressionBuilder.Constants.OPERATOR_TYPE.UNARY) {
            result = result && options.rightSide.validate();
        }
        return result;
    };

    this.setFilter = function (masterField) {
        dataTypeFilter = masterField.DataTypeInfo.DeclType;
        options.dataTypeFilter = dataTypeFilter;
        dropdown.prop('disabled', false);
        showFilteredItems();
    };
    
    this.isEmpty = function () {
        return !options.leftSide.getExpr().Value;
    };
	
    this.getErrorMessage = function() {
        if (operator.Type != SW.Core.NestedExpressionBuilder.Constants.OPERATOR_TYPE.UNARY) {
            return options.rightSide.getErrorMessage();
        }
        return null;
    };

    // PRIVATE METHODS

    var showFilteredItems = function () {
        var prevValue = expr.Value;
        var prevValueFound = false;
        dropdown.empty();
        for (i = 0; i < operators.length; i++) {
            if (!options.dataTypeFilter || isOperatorCompatible(operators[i], options.dataTypeFilter)) {
                createItem(dropdown, operators[i].DisplayName, operators[i].RefID);
                if (operators[i].RefID == prevValue) {
                    prevValueFound = true;
                }
            }
        }
        // set previously selected field if it's still visible with new filter
        if (prevValueFound) {
            dropdown.val(prevValue);
        }
        onChange();
    };

    var onChange = function () {
        expr.Value = dropdown.val();
        for (i = 0; i < operators.length; i++) {
            if (operators[i].RefID == expr.Value) {
                operator = operators[i];
                break;
            }
        }

        if (operator && typeof (options.onSet) === 'function') {
            options.onSet(operator);
        }
    };

    var createItem = function (select, text, value) {
        var o = new Option(text, value);
        $(o).html(text);
        select.append(o);
    };

    var isOperatorCompatible = function (op, filter) {
        if (op.SupportedTypes.length == 0) {
            return true;
        }
        for (var i = 0; i < op.SupportedTypes.length; i++) {
            if (op.SupportedTypes[i] == filter) {
                return true;
            }
        }
        return false;
    };

    // CONSTRUCTOR

    var self = this;
    var options = $.extend({}, DEFAULT_CONFIG, config);
    var operators = options.globalOptions.logicalOperators;
    var expr = options.expr;
    var operator;
    var dropdown = $("<select>");
    dropdown.on("change", onChange);
    dropdown.prop('disabled', !expr.Value);
    options.renderTo.append(dropdown);
    showFilteredItems();

    return this;
};

