SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").Int = function (config) {

    // PUBLIC METHODS

    this.getExpr = function () {
        expr.Value = input.val();
        return expr;
    };

    this.validate = function () {
        return isInt(input.val());
    };

    this.setFilter = function (masterField) {
        input.prop('disabled', false);
    };

    // PRIVATE METHODS

    function isInt(n) {
        return !isNaN(parseInt(n)) && isFinite(n);
    }

    // CONSTRUCTOR
    var self = this;
    var options = config;
    var expr = options.expr;
    var input = $('<input type="text" />');
    input.val(expr.Value || '');
    input.prop('disabled', !(expr.Value || options.enabledWhenEmpty));
    options.renderTo.append(input);

    return this;
}