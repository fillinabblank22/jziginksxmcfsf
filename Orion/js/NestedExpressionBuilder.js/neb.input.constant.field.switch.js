﻿SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input").ConstantFieldSwitch = function(config) {

    var texts = {
        'menu-use-field': "@{R=Core.Strings;K=WEBJS_ZT0_36;E=js}",
        'menu-use-constant':  "@{R=Core.Strings;K=WEBJS_ZT0_37;E=js}"
    };

    this.getExpr = function () {
        return input.getExpr();
    };

    this.validate = function () {
        return input.validate();
    };

    this.setFilter = function (masterField) {
        input.setFilter(masterField);

        renderSwitchButton(false);
    };

    var renderInput = function(inputConstuctor) {
        config.renderTo.empty();

        input = new inputConstuctor(config);

        return input;
    };

    var renderSwitchButton = function(disabled) {

        config.renderTo.find('.neb-constant-field-switch').remove();

        
        if (config.expr.ConditionType !== 'Simple') {
            disabled = !!disabled;

            if (disabled) {               
                 config.renderTo.append("<span class='neb-constant-field-switch disabled'>&nbsp;</span>");

            } else {
                var template = "<span class='neb-constant-field-switch'>&nbsp;&nbsp;&nbsp;<span class='neb-constant-field-switch-menu'>{{text}}</span></span>";
                var text = (inputType === 'Constant') ? texts['menu-use-field'] : texts['menu-use-constant'];
                var html = $(_.template(template, {text: text } ));

                config.renderTo.append(html);
                config.renderTo.find('.neb-constant-field-switch-menu').on('click', switchInput);
            }

       }
    };

    var switchInput = function() {

        var previousInput = input;

        config.expr.Value = null;

        if (inputType === 'Field') {
            renderInput(SW.Core.NestedExpressionBuilder.Input.Constant.Wrapper);
            config.expr.NodeType = SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT;
            inputType = 'Constant';
        } else {
            config.expr.NodeType = SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.FIELD;
            renderInput(SW.Core.NestedExpressionBuilder.Input.Field.Picker);       
            inputType = 'Field';
        }
    
        // If was set filter for right side, we will preserve it.
        if (config.side === SW.Core.NestedExpressionBuilder.Constants.EXPR_SIDE.RIGHT) {
           
            if ($.isFunction(previousInput.getFilter)) {

                var filter = previousInput.getFilter();

                input.setFilter(filter);
            }
            
        } else if(inputType === 'Constant' && config.side === SW.Core.NestedExpressionBuilder.Constants.EXPR_SIDE.LEFT) {
             var textField = { 
                DataTypeInfo: { DeclType: 5 } // TEXT
            };
            input.setFilter(textField);
            
            if ($.isFunction(config.onSet)) {
                config.onSet(textField);
            }

        }

        renderSwitchButton();
    };


    var self = this;
    var input;
    var inputType;
    var disabled = !(config.expr.Value || config.enabledWhenEmpty);

    if (config.expr.NodeType === SW.Core.NestedExpressionBuilder.Constants.EXPRESSION_TYPE.CONSTANT) {
        inputType = 'Constant';
        renderInput(SW.Core.NestedExpressionBuilder.Input.Constant.Wrapper);      
    } else {
        inputType = 'Field';
        renderInput(SW.Core.NestedExpressionBuilder.Input.Field.Picker); 
    }

    renderSwitchButton(disabled);


}