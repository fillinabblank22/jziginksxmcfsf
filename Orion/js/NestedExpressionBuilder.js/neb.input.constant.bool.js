﻿SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").Bool = function (config) {

    // PUBLIC METHODS

    this.getExpr = function () {
        expr.Value = input.val();
        return expr;
    };

    this.validate = function () {
        return true;
    };

    this.setFilter = function (masterField) {
        input.prop('disabled', false);
    };

    // PRIVATE METHODS

    var createItem = function (select, text, value) {
        var o = new Option(text, value);
        $(o).html(text);
        select.append(o);
    };

    // CONSTRUCTOR
    var self = this;
    var options = config;
    var expr = options.expr;

    var input = $('<select />');
    createItem(input, "@{R=Core.Strings;K=WEBJS_JP2_0;E=js}", "true");
    createItem(input, "@{R=Core.Strings;K=WEBJS_JP2_1;E=js}", "false");

    input.val(expr.Value == "true" ? "true" : "false");

    input.prop('disabled', !(expr.Value || options.enabledWhenEmpty));
    options.renderTo.append(input);

    return this;
}
