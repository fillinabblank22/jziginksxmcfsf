﻿SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").Enum = function (config) {

    // PUBLIC METHODS

    this.getExpr = function () {
        expr.Value = input.val();
        return expr;
    };

    this.validate = function () {
        return true;
    };

    this.setFilter = function (masterField) {
        input.prop('disabled', true);

        if (masterField) {
            SW.Core.Services.callWebService( options.globalOptions.dataProviderUrl || '/Orion/Services/ExpressionBuilderDataProvider.asmx', 'GetFieldEnumValues',
             { dataSource: ds, fieldRefId: masterField.RefID.Data,  extraRequestParams : extraRequestParams  },
            function (res) {
                input.empty();
                $.each(res, function (index, item) {
                    createItem(input, item.DisplayName, item.RefId);
                });
                input.prop('disabled', false);
                if (firstTime) {
                    firstTime = false;
                    input.val(expr.Value);
                }
            },
            function () { $.error("Can't load column data"); });
        }
    };

    // PRIVATE METHODS
    var createItem = function (select, text, value) {
        var o = new Option(text, value);
        $(o).html(text);
        select.append(o);
    };

    // CONSTRUCTOR
    var self = this;
    var options = config;
    var expr = options.expr;
    var ds = options.globalOptions.dataSource;
    var extraRequestParams = options.globalOptions.extraRequestParams;
    var firstTime = true && !!expr.Value; // I know. I just want to point out what I really mean
    var input = $('<select />');
    input.prop('disabled', !(expr.Value || options.enabledWhenEmpty));
    options.renderTo.append(input);

    return this;
}