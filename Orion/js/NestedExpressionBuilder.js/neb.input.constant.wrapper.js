SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Constant").Wrapper = function (config) {
    // This class represents a wrapper for any input constant field. When the filter is changed, new input is created

    // CONSTANTS
    var DEFAULT_CONFIG = {
        enabledWhenEmpty: false,
        dataTypeFilter: 0
    };

    var DEFAULT_MAPPPING = {
        // values per SolarWinds.Reporting.Models.Data.DataDeclarationType
        0: SW.Core.NestedExpressionBuilder.Input.Constant.Unknown,               // NotSpecified
        1: SW.Core.NestedExpressionBuilder.Input.Constant.DateTime,              // DateTime
        2: SW.Core.NestedExpressionBuilder.Input.Constant.Bool,                  // Boolean
        3: SW.Core.NestedExpressionBuilder.Input.Constant.Int,                   // Integer
        4: SW.Core.NestedExpressionBuilder.Input.Constant.Float,                 // Float
        5: SW.Core.NestedExpressionBuilder.Input.Constant.Text,                  // Text
        6: SW.Core.NestedExpressionBuilder.Input.Constant.Text,                  // Char
        7: SW.Core.NestedExpressionBuilder.Input.Constant.Text,                  // Uuid
        8: SW.Core.NestedExpressionBuilder.Input.Constant.Enum,                  // Enumerated
        9: SW.Core.NestedExpressionBuilder.Input.Constant.Text,                  // CustomTransform
        10: SW.Core.NestedExpressionBuilder.Input.Constant.Byte                  // Byte
    };

    // PUBLIC METHODS

    this.getExpr = function () {
        return input.getExpr();
    };

    this.validate = function () {
        return input.validate();
    };

    this.setFilter = function (masterField) {
        filter = masterField;
        
        var prevFilter = options.dataTypeFilter;
        var newFilter = masterField.DataTypeInfo.DeclType;
        if (prevFilter != newFilter) {
            options.dataTypeFilter = newFilter;
            options.renderTo.empty();
            
            // If we changed datatype from any other datatype we clear value.
            if (prevFilter !== 0) {
                options.expr.Value = '';
            }
            
            input = createConstant();
        }

        input.setFilter(masterField);
    };

    this.getFilter = function() {      
        return filter;
    };
	
    this.getErrorMessage = function(){
        if (input.errorMessage && typeof input.errorMessage !== "undefined") return input.errorMessage;
        return null;
    };

    this.operatorChanged = function (selectedOp) {
        if (input) {
            options.SelectedOperator = this.SelectedOperator;
            if (typeof (input.operatorChanged) === 'function') {
                input.operatorChanged(selectedOp);
            }
        }
    };

    // PRIVATE METHODS

    var createConstant = function () {
        var className = options.mapping[options.dataTypeFilter];
        return new className(options);
    };

    // CONSTRUCTOR

    var self = this;
    var options = $.extend({}, DEFAULT_CONFIG, config);
    options.mapping = $.extend({}, options.globalOptions.constantMapping, DEFAULT_MAPPPING);
    var input = createConstant();
    var filter = null;
    
    return this;
};