﻿/*!
 * Ext JS Library 3.2.0
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
/**
 * List compiled by mystix on the extjs.com forums.
 * Thank you Mystix!
 *
 * English Translations
 * updated to 2.2 by Condor (8 Aug 2008)
 */

Ext.UpdateManager.defaults.indicatorText = '<div class="loading-indicator">創Loading...末</div>';

if(Ext.data.Types){
    Ext.data.Types.stripRe = /[\$,%]/g;
}

if(Ext.DataView){
  Ext.DataView.prototype.emptyText = "";
}

if(Ext.grid.GridPanel){
  Ext.grid.GridPanel.prototype.ddText = "創{0} selected row{1}末";
}

if(Ext.LoadMask){
  Ext.LoadMask.prototype.msg = "創Loading...末";
}

Date.monthNames = [
  "創January末",
  "創February末",
  "創March末",
  "創April末",
  "創May末",
  "創June末",
  "創July末",
  "創August末",
  "創September末",
  "創October末",
  "創November末",
  "創December末"
];

Date.getShortMonthName = function(month) {
  return Date.monthNames[month].substring(0, 3);
};

Date.monthNumbers = {
  Jan : 0,
  Feb : 1,
  Mar : 2,
  Apr : 3,
  May : 4,
  Jun : 5,
  Jul : 6,
  Aug : 7,
  Sep : 8,
  Oct : 9,
  Nov : 10,
  Dec : 11
};

Date.getMonthNumber = function(name) {
  return Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
};

Date.dayNames = [
  "創Sunday末",
  "創Monday末",
  "創Tuesday末",
  "創Wednesday末",
  "創Thursday末",
  "創Friday末",
  "創Saturday末"
];

Date.getShortDayName = function(day) {
  return Date.dayNames[day].substring(0, 3);
};

Date.parseCodes.S.s = "(?:創st末|創nd末|創rd末|創th末)";

if(Ext.MessageBox){
  Ext.MessageBox.buttonText = {
    ok     : "創OK末",
    cancel : "創Cancel末",
    yes    : "創Yes末",
    no     : "創No末"
  };
}

if(Ext.util.Format){
  Ext.util.Format.date = function(v, format){
    if(!v) return "";
    if(!(v instanceof Date)) v = new Date(Date.parse(v));
    return v.dateFormat(format || "m/d/Y");
  };
}

if(Ext.DatePicker){
  Ext.apply(Ext.DatePicker.prototype, {
    todayText         : "創Today末",
    minText           : "創This date is before the minimum date末",
    maxText           : "創This date is after the maximum date末",
    disabledDaysText  : "",
    disabledDatesText : "",
    monthNames        : Date.monthNames,
    dayNames          : Date.dayNames,
    nextText          : '創Next Month (Control+Right)末',
    prevText          : '創Previous Month (Control+Left)末',
    monthYearText     : '創Choose a month (Control+Up/Down to move years)末',
    todayTip          : "創{0} (Spacebar)末",
    format            : "m/d/y",
    okText            : "&#160;創OK末&#160;",
    cancelText        : "創Cancel末",
    startDay          : 0
  });
}

if(Ext.PagingToolbar){
  Ext.apply(Ext.PagingToolbar.prototype, {
    beforePageText : "創Page末",
    afterPageText  : "創of {0}末",
    firstText      : "創First Page末",
    prevText       : "創Previous Page末",
    nextText       : "創Next Page末",
    lastText       : "創Last Page末",
    refreshText    : "創Refresh末",
    displayMsg     : "創Displaying {0} - {1} of {2}末",
    emptyMsg       : '創No data to display末'
  });
}

if(Ext.form.BasicForm){
    Ext.form.BasicForm.prototype.waitTitle = "創Please Wait...末"
}

if(Ext.form.Field){
  Ext.form.Field.prototype.invalidText = "創The value in this field is invalid末";
}

if(Ext.form.TextField){
  Ext.apply(Ext.form.TextField.prototype, {
    minLengthText : "創The minimum length for this field is {0}末",
    maxLengthText : "創The maximum length for this field is {0}末",
    blankText     : "創This field is required末",
    regexText     : "",
    emptyText     : null
  });
}

if(Ext.form.NumberField){
  Ext.apply(Ext.form.NumberField.prototype, {
    decimalSeparator : ".",
    decimalPrecision : 2,
    minText : "創The minimum value for this field is {0}末",
    maxText : "創The maximum value for this field is {0}末",
    nanText : "創{0} is not a valid number末"
  });
}

if(Ext.form.DateField){
  Ext.apply(Ext.form.DateField.prototype, {
    disabledDaysText  : "創Disabled末",
    disabledDatesText : "創Disabled末",
    minText           : "創The date in this field must be after {0}末",
    maxText           : "創The date in this field must be before {0}末",
    invalidText       : "創{0} is not a valid date - it must be in the format {1}末",
    format            : "m/d/y",
    altFormats        : "m/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d"
  });
}

if(Ext.form.ComboBox){
  Ext.apply(Ext.form.ComboBox.prototype, {
    loadingText       : "創Loading...末",
    valueNotFoundText : undefined
  });
}

if(Ext.form.VTypes){
  Ext.apply(Ext.form.VTypes, {
    emailText    : '創This field should be an e-mail address in the format "user@example.com"末',
    urlText      : '創This field should be a URL in the format "http:/'+'/www.example.com"末',
    alphaText    : '創This field should only contain letters and _末',
    alphanumText : '創This field should only contain letters, numbers and _末'
  });
}

if(Ext.form.HtmlEditor){
  Ext.apply(Ext.form.HtmlEditor.prototype, {
    createLinkText : '創Please enter the URL for the link:末',
    buttonTips : {
      bold : {
        title: '創Bold (Ctrl+B)末',
        text: '創Make the selected text bold.末',
        cls: 'x-html-editor-tip'
      },
      italic : {
        title: '創Italic (Ctrl+I)末',
        text: '創Make the selected text italic.末',
        cls: 'x-html-editor-tip'
      },
      underline : {
        title: '創Underline (Ctrl+U)末',
        text: '創Underline the selected text.末',
        cls: 'x-html-editor-tip'
      },
      increasefontsize : {
        title: '創Grow Text末',
        text: '創Increase the font size.末',
        cls: 'x-html-editor-tip'
      },
      decreasefontsize : {
        title: '創Shrink Text末',
        text: '創Decrease the font size.末',
        cls: 'x-html-editor-tip'
      },
      backcolor : {
        title: '創Text Highlight Color末',
        text: '創Change the background color of the selected text.末',
        cls: 'x-html-editor-tip'
      },
      forecolor : {
        title: '創Font Color末',
        text: '創Change the color of the selected text.末',
        cls: 'x-html-editor-tip'
      },
      justifyleft : {
        title: '創Align Text Left末',
        text: '創Align text to the left.末',
        cls: 'x-html-editor-tip'
      },
      justifycenter : {
        title: '創Center Text末',
        text: '創Center text in the editor.末',
        cls: 'x-html-editor-tip'
      },
      justifyright : {
        title: '創Align Text Right末',
        text: '創Align text to the right.末',
        cls: 'x-html-editor-tip'
      },
      insertunorderedlist : {
        title: '創Bullet List末',
        text: '創Start a bulleted list.末',
        cls: 'x-html-editor-tip'
      },
      insertorderedlist : {
        title: '創Numbered List末',
        text: '創Start a numbered list.末',
        cls: 'x-html-editor-tip'
      },
      createlink : {
        title: '創Hyperlink末',
        text: '創Make the selected text a hyperlink.末',
        cls: 'x-html-editor-tip'
      },
      sourceedit : {
        title: '創Source Edit末',
        text: '創Switch to source editing mode.末',
        cls: 'x-html-editor-tip'
      }
    }
  });
}

if(Ext.grid.GridView){
  Ext.apply(Ext.grid.GridView.prototype, {
    sortAscText  : "創Sort Ascending末",
    sortDescText : "創Sort Descending末",
    columnsText  : "Columnsv"
  });
}

if(Ext.grid.GroupingView){
  Ext.apply(Ext.grid.GroupingView.prototype, {
    emptyGroupText : '創(None)末',
    groupByText    : '創Group By This Field末',
    showGroupsText : '創Show in Groups末'
  });
}

if(Ext.grid.PropertyColumnModel){
  Ext.apply(Ext.grid.PropertyColumnModel.prototype, {
    nameText   : "創Name末",
    valueText  : "創Value末",
    dateFormat : "m/j/Y",
    trueText: "true",
    falseText: "false"
  });
}

if(Ext.grid.BooleanColumn){
   Ext.apply(Ext.grid.BooleanColumn.prototype, {
      trueText  : "true",
      falseText : "false",
      undefinedText: '&#160;'
   });
}

if(Ext.grid.NumberColumn){
    Ext.apply(Ext.grid.NumberColumn.prototype, {
        format : '0,000.00'
    });
}

if(Ext.grid.DateColumn){
    Ext.apply(Ext.grid.DateColumn.prototype, {
        format : 'm/d/Y'
    });
}

if(Ext.layout.BorderLayout && Ext.layout.BorderLayout.SplitRegion){
  Ext.apply(Ext.layout.BorderLayout.SplitRegion.prototype, {
    splitTip            : "創Drag to resize.末",
    collapsibleSplitTip : "創Drag to resize. Double click to hide.末"
  });
}

if(Ext.form.TimeField){
  Ext.apply(Ext.form.TimeField.prototype, {
    minText : "創The time in this field must be equal to or after {0}末",
    maxText : "創The time in this field must be equal to or before {0}末",
    invalidText : "創{0} is not a valid time末",
    format : "g:i A",
    altFormats : "g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H"
  });
}

if(Ext.form.CheckboxGroup){
  Ext.apply(Ext.form.CheckboxGroup.prototype, {
    blankText : "創You must select at least one item in this group末"
  });
}

if(Ext.form.RadioGroup){
  Ext.apply(Ext.form.RadioGroup.prototype, {
    blankText : "創You must select one item in this group末"
  });
}
