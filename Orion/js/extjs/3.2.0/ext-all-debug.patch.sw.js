/*!
 * Ext JS Library 3.2.0
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
/**
 * List compiled by mystix on the extjs.com forums.
 * Thank you Mystix!
 *
 * English Translations
 * updated to 2.2 by Condor (8 Aug 2008)
 */

Ext.UpdateManager.defaults.indicatorText = '<div class="loading-indicator">創...ƃuıpɐol末</div>';

if(Ext.data.Types){
    Ext.data.Types.stripRe = /[\$,%]/g;
}

if(Ext.DataView){
  Ext.DataView.prototype.emptyText = "";
}

if(Ext.grid.GridPanel){
  Ext.grid.GridPanel.prototype.ddText = "創{1}ʍoɹ pǝʇɔǝlǝs {0}末";
}

if(Ext.LoadMask){
  Ext.LoadMask.prototype.msg = "創...ƃuıpɐol末";
}

Date.monthNames = [
  "創ʎɹɐnuɐɾ末",
  "創ʎɹɐnɹqǝɟ末",
  "創ɥɔɹɐɯ末",
  "創lıɹdɐ末",
  "創ʎɐɯ末",
  "創ǝunɾ末",
  "創ʎlnɾ末",
  "創ʇsnƃnɐ末",
  "創ɹǝqɯǝʇdǝs末",
  "創ɹǝqoʇɔo末",
  "創ɹǝqɯǝʌou末",
  "創ɹǝqɯǝɔǝp末"
];

Date.getShortMonthName = function(month) {
  return Date.monthNames[month].substring(0, 3);
};

Date.monthNumbers = {
  Jan : 0,
  Feb : 1,
  Mar : 2,
  Apr : 3,
  May : 4,
  Jun : 5,
  Jul : 6,
  Aug : 7,
  Sep : 8,
  Oct : 9,
  Nov : 10,
  Dec : 11
};

Date.getMonthNumber = function(name) {
  return Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
};

Date.dayNames = [
  "創ʎɐpuns末",
  "創ʎɐpuoɯ末",
  "創ʎɐpsǝnʇ末",
  "創ʎɐpsǝupǝʍ末",
  "創ʎɐpsɹnɥʇ末",
  "創ʎɐpıɹɟ末",
  "創ʎɐpɹnʇɐs末"
];

Date.getShortDayName = function(day) {
  return Date.dayNames[day].substring(0, 3);
};

Date.parseCodes.S.s = "(?:創ʇs末|創pu末|創pɹ末|創ɥʇ末)";

if(Ext.MessageBox){
  Ext.MessageBox.buttonText = {
    ok     : "創ʞo末",
    cancel : "創lǝɔuɐɔ末",
    yes    : "創sǝʎ末",
    no     : "創ou末"
  };
}

if(Ext.util.Format){
  Ext.util.Format.date = function(v, format){
    if(!v) return "";
    if(!(v instanceof Date)) v = new Date(Date.parse(v));
    return v.dateFormat(format || "m/d/Y");
  };
}

if(Ext.DatePicker){
  Ext.apply(Ext.DatePicker.prototype, {
    todayText         : "創ʎɐpoʇ末",
    minText           : "創ǝʇɐp ɯnɯıuıɯ ǝɥʇ ǝɹoɟǝq sı ǝʇɐp sıɥʇ末",
    maxText           : "創ǝʇɐp ɯnɯıxɐɯ ǝɥʇ ɹǝʇɟɐ sı ǝʇɐp sıɥʇ末",
    disabledDaysText  : "",
    disabledDatesText : "",
    monthNames        : Date.monthNames,
    dayNames          : Date.dayNames,
    nextText          : '創)ʇɥƃıɹ+loɹʇuoɔ( ɥʇuoɯ ʇxǝu末',
    prevText          : '創)ʇɟǝl+loɹʇuoɔ( ɥʇuoɯ snoıʌǝɹd末',
    monthYearText     : '創)sɹɐǝʎ ǝʌoɯ oʇ uʍop/dn+loɹʇuoɔ( ɥʇuoɯ ɐ ǝsooɥɔ末',
    todayTip          : "創)ɹɐqǝɔɐds( {0}末",
    format            : "m/d/y",
    okText            : "&#160;創ʞo末&#160;",
    cancelText        : "創lǝɔuɐɔ末",
    startDay          : 0
  });
}

if(Ext.PagingToolbar){
  Ext.apply(Ext.PagingToolbar.prototype, {
    beforePageText : "創ǝƃɐd末",
    afterPageText  : "創{0} ɟo末",
    firstText      : "創ǝƃɐd ʇsɹıɟ末",
    prevText       : "創ǝƃɐd snoıʌǝɹd末",
    nextText       : "創ǝƃɐd ʇxǝu末",
    lastText       : "創ǝƃɐd ʇsɐl末",
    refreshText    : "創ɥsǝɹɟǝɹ末",
    displayMsg     : "創{2} ɟo {1} - {0} ƃuıʎɐldsıp末",
    emptyMsg       : '創ʎɐldsıp oʇ ɐʇɐp ou末'
  });
}

if(Ext.form.BasicForm){
    Ext.form.BasicForm.prototype.waitTitle = "創...ʇıɐʍ ǝsɐǝld末"
}

if(Ext.form.Field){
  Ext.form.Field.prototype.invalidText = "創pılɐʌuı sı plǝıɟ sıɥʇ uı ǝnlɐʌ ǝɥʇ末";
}

if(Ext.form.TextField){
  Ext.apply(Ext.form.TextField.prototype, {
    minLengthText : "創{0} sı plǝıɟ sıɥʇ ɹoɟ ɥʇƃuǝl ɯnɯıuıɯ ǝɥʇ末",
    maxLengthText : "創{0} sı plǝıɟ sıɥʇ ɹoɟ ɥʇƃuǝl ɯnɯıxɐɯ ǝɥʇ末",
    blankText     : "創pǝɹınbǝɹ sı plǝıɟ sıɥʇ末",
    regexText     : "",
    emptyText     : null
  });
}

if(Ext.form.NumberField){
  Ext.apply(Ext.form.NumberField.prototype, {
    decimalSeparator : ".",
    decimalPrecision : 2,
    minText : "創{0} sı plǝıɟ sıɥʇ ɹoɟ ǝnlɐʌ ɯnɯıuıɯ ǝɥʇ末",
    maxText : "創{0} sı plǝıɟ sıɥʇ ɹoɟ ǝnlɐʌ ɯnɯıxɐɯ ǝɥʇ末",
    nanText : "創ɹǝqɯnu pılɐʌ ɐ ʇou sı {0}末"
  });
}

if(Ext.form.DateField){
  Ext.apply(Ext.form.DateField.prototype, {
    disabledDaysText  : "創pǝlqɐsıp末",
    disabledDatesText : "創pǝlqɐsıp末",
    minText           : "創{0} ɹǝʇɟɐ ǝq ʇsnɯ plǝıɟ sıɥʇ uı ǝʇɐp ǝɥʇ末",
    maxText           : "創{0} ǝɹoɟǝq ǝq ʇsnɯ plǝıɟ sıɥʇ uı ǝʇɐp ǝɥʇ末",
    invalidText       : "創{1} ʇɐɯɹoɟ ǝɥʇ uı ǝq ʇsnɯ ʇı - ǝʇɐp pılɐʌ ɐ ʇou sı {0}末",
    format            : "m/d/y",
    altFormats        : "m/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d"
  });
}

if(Ext.form.ComboBox){
  Ext.apply(Ext.form.ComboBox.prototype, {
    loadingText       : "創...ƃuıpɐol末",
    valueNotFoundText : undefined
  });
}

if(Ext.form.VTypes){
  Ext.apply(Ext.form.VTypes, {
    emailText    : '創"ɯoɔ.ǝldɯɐxǝ@ɹǝsn" ʇɐɯɹoɟ ǝɥʇ uı ssǝɹppɐ lıɐɯ-ǝ uɐ ǝq plnoɥs plǝıɟ sıɥʇ末',
    urlText      : '創"ɯoɔ.ǝldɯɐxǝ.ʍʍʍ/'+'/:dʇʇɥ" ʇɐɯɹoɟ ǝɥʇ uı lɹn ɐ ǝq plnoɥs plǝıɟ sıɥʇ末',
    alphaText    : '創_ puɐ sɹǝʇʇǝl uıɐʇuoɔ ʎluo plnoɥs plǝıɟ sıɥʇ末',
    alphanumText : '創_ puɐ sɹǝqɯnu ,sɹǝʇʇǝl uıɐʇuoɔ ʎluo plnoɥs plǝıɟ sıɥʇ末'
  });
}

if(Ext.form.HtmlEditor){
  Ext.apply(Ext.form.HtmlEditor.prototype, {
    createLinkText : '創:ʞuıl ǝɥʇ ɹoɟ lɹn ǝɥʇ ɹǝʇuǝ ǝsɐǝld末',
    buttonTips : {
      bold : {
        title: '創)q+lɹʇɔ( ploq末',
        text: '創.ploq ʇxǝʇ pǝʇɔǝlǝs ǝɥʇ ǝʞɐɯ末',
        cls: 'x-html-editor-tip'
      },
      italic : {
        title: '創)ı+lɹʇɔ( ɔılɐʇı末',
        text: '創.ɔılɐʇı ʇxǝʇ pǝʇɔǝlǝs ǝɥʇ ǝʞɐɯ末',
        cls: 'x-html-editor-tip'
      },
      underline : {
        title: '創)n+lɹʇɔ( ǝuılɹǝpun末',
        text: '創.ʇxǝʇ pǝʇɔǝlǝs ǝɥʇ ǝuılɹǝpun末',
        cls: 'x-html-editor-tip'
      },
      increasefontsize : {
        title: '創ʇxǝʇ ʍoɹƃ末',
        text: '創.ǝzıs ʇuoɟ ǝɥʇ ǝsɐǝɹɔuı末',
        cls: 'x-html-editor-tip'
      },
      decreasefontsize : {
        title: '創ʇxǝʇ ʞuıɹɥs末',
        text: '創.ǝzıs ʇuoɟ ǝɥʇ ǝsɐǝɹɔǝp末',
        cls: 'x-html-editor-tip'
      },
      backcolor : {
        title: '創ɹoloɔ ʇɥƃılɥƃıɥ ʇxǝʇ末',
        text: '創.ʇxǝʇ pǝʇɔǝlǝs ǝɥʇ ɟo ɹoloɔ punoɹƃʞɔɐq ǝɥʇ ǝƃuɐɥɔ末',
        cls: 'x-html-editor-tip'
      },
      forecolor : {
        title: '創ɹoloɔ ʇuoɟ末',
        text: '創.ʇxǝʇ pǝʇɔǝlǝs ǝɥʇ ɟo ɹoloɔ ǝɥʇ ǝƃuɐɥɔ末',
        cls: 'x-html-editor-tip'
      },
      justifyleft : {
        title: '創ʇɟǝl ʇxǝʇ uƃılɐ末',
        text: '創.ʇɟǝl ǝɥʇ oʇ ʇxǝʇ uƃılɐ末',
        cls: 'x-html-editor-tip'
      },
      justifycenter : {
        title: '創ʇxǝʇ ɹǝʇuǝɔ末',
        text: '創.ɹoʇıpǝ ǝɥʇ uı ʇxǝʇ ɹǝʇuǝɔ末',
        cls: 'x-html-editor-tip'
      },
      justifyright : {
        title: '創ʇɥƃıɹ ʇxǝʇ uƃılɐ末',
        text: '創.ʇɥƃıɹ ǝɥʇ oʇ ʇxǝʇ uƃılɐ末',
        cls: 'x-html-editor-tip'
      },
      insertunorderedlist : {
        title: '創ʇsıl ʇǝllnq末',
        text: '創.ʇsıl pǝʇǝllnq ɐ ʇɹɐʇs末',
        cls: 'x-html-editor-tip'
      },
      insertorderedlist : {
        title: '創ʇsıl pǝɹǝqɯnu末',
        text: '創.ʇsıl pǝɹǝqɯnu ɐ ʇɹɐʇs末',
        cls: 'x-html-editor-tip'
      },
      createlink : {
        title: '創ʞuılɹǝdʎɥ末',
        text: '創.ʞuılɹǝdʎɥ ɐ ʇxǝʇ pǝʇɔǝlǝs ǝɥʇ ǝʞɐɯ末',
        cls: 'x-html-editor-tip'
      },
      sourceedit : {
        title: '創ʇıpǝ ǝɔɹnos末',
        text: '創.ǝpoɯ ƃuıʇıpǝ ǝɔɹnos oʇ ɥɔʇıʍs末',
        cls: 'x-html-editor-tip'
      }
    }
  });
}

if(Ext.grid.GridView){
  Ext.apply(Ext.grid.GridView.prototype, {
    sortAscText  : "創ƃuıpuǝɔsɐ ʇɹos末",
    sortDescText : "創ƃuıpuǝɔsǝp ʇɹos末",
    columnsText  : "Columnsv"
  });
}

if(Ext.grid.GroupingView){
  Ext.apply(Ext.grid.GroupingView.prototype, {
    emptyGroupText : '創)ǝuou(末',
    groupByText    : '創plǝıɟ sıɥʇ ʎq dnoɹƃ末',
    showGroupsText : '創sdnoɹƃ uı ʍoɥs末'
  });
}

if(Ext.grid.PropertyColumnModel){
  Ext.apply(Ext.grid.PropertyColumnModel.prototype, {
    nameText   : "創ǝɯɐu末",
    valueText  : "創ǝnlɐʌ末",
    dateFormat : "m/j/Y",
    trueText: "true",
    falseText: "false"
  });
}

if(Ext.grid.BooleanColumn){
   Ext.apply(Ext.grid.BooleanColumn.prototype, {
      trueText  : "true",
      falseText : "false",
      undefinedText: '&#160;'
   });
}

if(Ext.grid.NumberColumn){
    Ext.apply(Ext.grid.NumberColumn.prototype, {
        format : '0,000.00'
    });
}

if(Ext.grid.DateColumn){
    Ext.apply(Ext.grid.DateColumn.prototype, {
        format : 'm/d/Y'
    });
}

if(Ext.layout.BorderLayout && Ext.layout.BorderLayout.SplitRegion){
  Ext.apply(Ext.layout.BorderLayout.SplitRegion.prototype, {
    splitTip            : "創.ǝzısǝɹ oʇ ƃɐɹp末",
    collapsibleSplitTip : "創.ǝpıɥ oʇ ʞɔılɔ ǝlqnop .ǝzısǝɹ oʇ ƃɐɹp末"
  });
}

if(Ext.form.TimeField){
  Ext.apply(Ext.form.TimeField.prototype, {
    minText : "創{0} ɹǝʇɟɐ ɹo oʇ lɐnbǝ ǝq ʇsnɯ plǝıɟ sıɥʇ uı ǝɯıʇ ǝɥʇ末",
    maxText : "創{0} ǝɹoɟǝq ɹo oʇ lɐnbǝ ǝq ʇsnɯ plǝıɟ sıɥʇ uı ǝɯıʇ ǝɥʇ末",
    invalidText : "創ǝɯıʇ pılɐʌ ɐ ʇou sı {0}末",
    format : "g:i A",
    altFormats : "g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H"
  });
}

if(Ext.form.CheckboxGroup){
  Ext.apply(Ext.form.CheckboxGroup.prototype, {
    blankText : "創dnoɹƃ sıɥʇ uı ɯǝʇı ǝuo ʇsɐǝl ʇɐ ʇɔǝlǝs ʇsnɯ noʎ末"
  });
}

if(Ext.form.RadioGroup){
  Ext.apply(Ext.form.RadioGroup.prototype, {
    blankText : "創dnoɹƃ sıɥʇ uı ɯǝʇı ǝuo ʇɔǝlǝs ʇsnɯ noʎ末"
  });
}
