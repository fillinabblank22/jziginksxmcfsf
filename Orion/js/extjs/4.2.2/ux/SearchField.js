﻿
var filterText = ''; //global search value

var filterPropertyParam = '';
var filterTypeParam = '';
var filterValueParam = '';

//Ext.ux.form.SearchField = Ext.extend(Ext.form.TwinTriggerField, {
Ext42.define('Ext.ux.form.SearchField', {
    extend: 'Ext.form.field.Trigger',
    //extend: 'Ext.form.TwinTriggerField',
    alias: 'widget.searchfield',

    trigger1Cls: Ext42.baseCSSPrefix + 'form-clear-trigger',

    trigger2Cls: Ext42.baseCSSPrefix + 'form-search-trigger',

    // hasSearch: false,

    initComponent: function() {
        //Ext.ux.form.SearchField.superclass.initComponent.call(this);
        var me = this;
        me.callParent(arguments);

        /*this.triggerConfig = {
            tag: 'span', cls: 'x-form-twin-triggers', cn: [
            {id: "clearbutton", tag:  "img", src: "/Orion/images/clear_button.gif", cls: "x-form-trigger " + this.trigger1Class },
            {id: "searchbutton", tag: "img", src: "/Orion/images/search_button.gif", cls: "x-form-trigger " + this.trigger2Class }
        ]
        }; */

        this.on('specialkey', function(f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        // set search field text styles
        this.on('focus', function() {
            this.el.applyStyles('font-style:normal;');
            //console.log(this.getValue() + " " + this.emptyText);
            if (Ext42.isEmpty(this.getValue()) || this.getValue() == this.emptyText) {
                this.setValue('');
            }
        }, this);

        // reset search field text style
        this.on('blur', function() {
            //if (Ext42.isEmpty(this.el.dom.value) || this.el.dom.value == this.emptyText) {
            if (Ext42.isEmpty(this.getValue()) || this.getValue() == this.emptyText) {
                this.el.applyStyles('font-style:italic;');
            }
        }, this);

        this.on('render', function () {
           // me.triggerEl.show();
            setTimeout(function() {
                me.hideTrigger1();
            }, 0);
            // hack
          /*  setTimeout(function () {
                me.inputEl.dom.style.width = "109%";
            }, 700);
            setTimeout(function () {
                me.inputEl.dom.style.width = "109%";
            }, 1000);*/
        }, this);
    },

    id: 'searchfield',
    validationEvent: false,
    validateOnBlur: false,

    trigger1Class: 'x-form-clear-trigger',
    trigger2Class: 'x-form-search-trigger',

    // default search field text style
    style: 'font-style: italic',
    emptyText: '@{R=Core.Strings;K=WEBJS_TM0_53;E=js}',

    //hideTrigger1: true,
    width: 180,
    hasSearch: false,

    hideTrigger1: function() {
        this.triggerCell.item(0).setDisplayed(false);
        this.updateLayout();
    },
    showTrigger1: function() {
        this.triggerCell.item(0).setDisplayed(true);
        this.updateLayout();
    },
    // this is the clear (X) icon button in the search field
    onTrigger1Click: function () {
        if (this.hasSearch) {
            //this.el.dom.value = '';
            this.setValue('');
            filterText = this.getRawValue();
            this.store.proxy.conn.jsonData = { property: filterPropertyParam, type: filterTypeParam, value: filterValueParam, search: '' };
            var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
            this.store.load({ params: o });
            this.hideTrigger1();
            
            this.hasSearch = false;
        }
    },

    //  this is the search icon button in the search field
    onTrigger2Click: function () {

        //set the global variable for the SQL query and regex highlighting
        filterText = this.getRawValue();

        if (filterText.length < 1) {
            this.onTrigger1Click();
            return;
        }

        // convert search string to SQL format (i.e. replace * with %)
        var tmpfilterText = "";
        tmpfilterText = filterText.replace(/\*/g, "%");

        if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
            tmpfilterText = "";
        }
        
        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        // provide search string for Id param when loading the page
        this.store.proxy.conn.jsonData = { property: filterPropertyParam, type: filterTypeParam, value: filterValueParam, search: tmpfilterText };


	    var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
        this.store.load({ params: o });

        this.hasSearch = true;
        this.showTrigger1();
    }
});