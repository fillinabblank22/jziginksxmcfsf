/**
* Ext.sw.grid.RadioSelectionModel
*/

Ext.ns('Ext.sw.grid');

/**
* @class Ext.sw.grid.RadioSelectionModel
* @extends Ext.grid.RowSelectionModel
* @constructor
* @param {Object} config The configuration options
*/
Ext.sw.grid.RadioSelectionModel = function (config) {

    // call parent
    Ext.sw.grid.RadioSelectionModel.superclass.constructor.call(this, config);

    // force renderer to run in this scope
    this.renderer = function (v, p, record) {
        var checked = record === this.selections.get(0);
        var retval =
        '<div style="position:relative;left:-1px;top:0px;height:13px;" class="x-grid3-row-radio"><input type="radio" name="' + this.id + '"'
        + (checked ? 'checked="checked"' : '')
        + '></div>';
        return retval;
    }.createDelegate(this);

}; // end of constructor

Ext.extend(Ext.sw.grid.RadioSelectionModel, Ext.grid.RowSelectionModel, {
    header: '<div>&#160;</div>'
, width: 20
, sortable: false

    // private
, fixed: true
, dataIndex: ''
, id: 'radio'
, singleSelect: true

, selectRow: function (index) {
    // call parent
    Ext.sw.grid.RadioSelectionModel.superclass.selectRow.apply(this, arguments);

    // Property can be set in beforeRowSelect event, which is called by above parent call.
    // but we don't get this information from parent call, so we have check this property to cancel selection in radio.
    if (this.selectionDisabled === true) {
        return;
    }

    // check radio
    var row = Ext.fly(this.grid.view.getRow(index));
    if (row) {
        row.child('input[type=radio]').dom.checked = true;
    }
}});