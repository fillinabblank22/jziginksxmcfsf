/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
/** 
 * @class Ext.ux.grid.filter.NumericFilter
 * @extends Ext.ux.grid.filter.Filter
 * Filters using an Ext.ux.menu.RangeMenu.
 * <p><b><u>Example Usage:</u></b></p>
 * <pre><code>    
var filters = new Ext.ux.grid.GridFilters({
    ...
    filters: [{
        type: 'numeric',
        dataIndex: 'price'
    }]
});
 * </code></pre> 
 */
Ext.ux.grid.filter.NumericFilter = Ext.extend(Ext.ux.grid.filter.Filter, {

    /**
    * @cfg {Object} fieldCls
    * The Class to use to construct each field item within this menu
    * Defaults to:<pre>
    * fieldCls : Ext.form.NumberField
    * </pre>
    */
    fieldCls: Ext.form.NumberField,
    /**
    * @cfg {Object} fieldCfg
    * The default configuration options for any field item unless superseded
    * by the <code>{@link #fields}</code> configuration.
    * Defaults to:<pre>
    * fieldCfg : {}
    * </pre>
    * Example usage:
    * <pre><code>
    fieldCfg : {
    width: 150,
    },
    * </code></pre>
    */
    /**
    * @cfg {Object} fields
    * The field items may be configured individually
    * Defaults to <tt>undefined</tt>.
    * Example usage:
    * <pre><code>
    fields : {
    gt: { // override fieldCfg options
    width: 200,
    fieldCls: Ext.ux.form.CustomNumberField // to override default {@link #fieldCls}
    }
    },
    * </code></pre>
    */
    /**
    * @cfg {Object} iconCls
    * The iconCls to be applied to each comparator field item.
    * Defaults to:<pre>
    iconCls : {
    gt : 'ux-rangemenu-gt',
    lt : 'ux-rangemenu-lt',
    eq : 'ux-rangemenu-eq'
    }
    * </pre>
    */
    iconCls: {
        gt: 'ux-rangemenu-gt',
        lt: 'ux-rangemenu-lt',
        eq: 'ux-rangemenu-eq'
    },

    /**
    * @cfg {Object} menuItemCfgs
    * Default configuration options for each menu item
    * Defaults to:<pre>
    menuItemCfgs : {
    emptyText: 'Enter Filter Text...',
    selectOnFocus: true,
    width: 125
    }
    * </pre>
    */
    menuItemCfgs: {
        emptyText: 'Enter Filter Text...',
        selectOnFocus: true,
        width: 125
    },

    /**
    * @cfg {Array} menuItems
    * The items to be shown in this menu.  Items are added to the menu
    * according to their position within this array. Defaults to:<pre>
    * menuItems : ['lt','gt','-','eq']
    * </pre>
    */
    menuItems: ['lt', 'gt', '-', 'eq'],

    /**  
    * @private
    * Template method that is to initialize the filter and install required menu items.
    */
    init: function (config) {
        // if a menu already existed, do clean up first
        if (this.menu) {
            this.menu.destroy();
        }
        this.menu = new Ext.ux.menu.RangeMenu(Ext.apply(config, {
            // pass along filter configs to the menu
            fieldCfg: this.fieldCfg || {},
            fieldCls: this.fieldCls,
            fields: this.fields || {},
            iconCls: this.iconCls,
            menuItemCfgs: this.menuItemCfgs,
            menuItems: this.menuItems,
            updateBuffer: this.updateBuffer
        }));

        this.initializeEmptyValueCheckbox(this);
        
        this.menu.add(new Ext.menu.Separator());
        this.menu.add(this.checkboxItem);

        this.checkboxItem.on('checkchange', this.onCheckChange, this);

        // relay the event fired by the menu
        this.menu.on('update', this.onUpdate, this);

        this.updateTask = new Ext.util.DelayedTask(this.fireUpdate, this);
    },

    disableFilter: function (comparison) {
        switch (comparison) {
            case "lt":
                this.menu.fields.lt.setValue("");
                break;
            case "gt":
                this.menu.fields.gt.setValue("");
                break;
            case "eq":
                this.menu.fields.eq.setValue("");
                break;
        }
        if (Ext.isEmpty(comparison)) {
            this.checkboxItem.enable();
            this.checkboxItem.setChecked(false);
            this.active = false;
        } else {
            if (this.getSerialArgs() == 0) {
                this.checkboxItem.enable();
                this.active = false;
            }
        }
    },

    onUpdate: function () {
        if (this.getSerialArgs() != 0)
            this.checkboxItem.disable();
        else
            this.checkboxItem.enable();
        this.fireUpdate(this);
    },

    onCheckChange: function () {
        if (this.checkboxItem.checked) {
            this.menu.fields.lt.disable();
            this.menu.fields.gt.disable();
            this.menu.fields.eq.disable();
        } else {
            this.menu.fields.lt.enable();
            this.menu.fields.gt.enable();
            this.menu.fields.eq.enable();
        }
        this.updateTask.delay(this.updateBuffer);
    },
    /**
    * @private
    * Template method that is to get and return the value of the filter.
    * @return {String} The value of this filter
    */
    getValue: function () {
        return this.menu.getValue();
    },

    /**
    * @private
    * Template method that is to set the value of the filter.
    * @param {Object} value The value to set the filter
    */
    setValue: function (value) {
        this.menu.setValue(value);
    },

    /**
    * @private
    * Template method that is to return <tt>true</tt> if the filter
    * has enough configuration information to be activated.
    * @return {Boolean}
    */
    isActivatable: function () {
        if (this.checkboxItem.checked)
            return true;
        var values = this.getValue();
        for (key in values) {
            if (values[key] !== undefined) {
                return true;
            }
        }
        return false;
    },

    /**
    * @private
    * Template method that is to get and return serialized filter data for
    * transmission to the server.
    * @return {Object/Array} An object or collection of objects containing
    * key value pairs representing the current configuration of the filter.
    */
    getSerialArgs: function () {
        if (this.checkboxItem.checked)
            return { type: 'numeric', value: '' };
        var key,
            args = [],
            values = this.menu.getValue();
        for (key in values) {
            args.push({
                type: 'numeric',
                comparison: key,
                value: values[key]
            });
        }
        return args;
    },

    /**
    * Template method that is to validate the provided Ext.data.Record
    * against the filters configuration.
    * @param {Ext.data.Record} record The record to validate
    * @return {Boolean} true if the record is valid within the bounds
    * of the filter, false otherwise.
    */
    validateRecord: function (record) {
        var val = record.get(this.dataIndex),
            values = this.getValue();
        if (values.eq !== undefined && val != values.eq) {
            return false;
        }
        if (values.lt !== undefined && val >= values.lt) {
            return false;
        }
        if (values.gt !== undefined && val <= values.gt) {
            return false;
        }
        return true;
    }
});