/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
/** 
 * @class Ext.ux.grid.filter.ListFilter
 * @extends Ext.ux.grid.filter.Filter
 * <p>List filters are able to be preloaded/backed by an Ext.data.Store to load
 * their options the first time they are shown. ListFilter utilizes the
 * {@link Ext.ux.menu.ListMenu} component.</p>
 * <p>Although not shown here, this class accepts all configuration options
 * for {@link Ext.ux.menu.ListMenu}.</p>
 * 
 * <p><b><u>Example Usage:</u></b></p>
 * <pre><code>    
var filters = new Ext.ux.grid.GridFilters({
    ...
    filters: [{
        type: 'list',
        dataIndex: 'size',
        phpMode: true,
        // options will be used as data to implicitly creates an ArrayStore
        options: ['extra small', 'small', 'medium', 'large', 'extra large']
    }]
});
 * </code></pre>
 * 
 */
Ext.ux.grid.filter.ListFilter = Ext.extend(Ext.ux.grid.filter.Filter, {

    /**
    * @cfg {Array} options
    * <p><code>data</code> to be used to implicitly create a data store
    * to back this list when the data source is <b>local</b>. If the
    * data for the list is remote, use the <code>{@link #store}</code>
    * config instead.</p>
    * <br><p>Each item within the provided array may be in one of the
    * following formats:</p>
    * <div class="mdetail-params"><ul>
    * <li><b>Array</b> :
    * <pre><code>
    options: [
    [11, 'extra small'], 
    [18, 'small'],
    [22, 'medium'],
    [35, 'large'],
    [44, 'extra large']
    ]
    * </code></pre>
    * </li>
    * <li><b>Object</b> :
    * <pre><code>
    labelField: 'name', // override default of 'text'
    options: [
    {id: 11, name:'extra small'}, 
    {id: 18, name:'small'}, 
    {id: 22, name:'medium'}, 
    {id: 35, name:'large'}, 
    {id: 44, name:'extra large'} 
    ]
    * </code></pre>
    * </li>
    * <li><b>String</b> :
    * <pre><code>
    * options: ['extra small', 'small', 'medium', 'large', 'extra large']
    * </code></pre>
    * </li>
    */
    /**
    * @cfg {Boolean} phpMode
    * <p>Adjust the format of this filter. Defaults to false.</p>
    * <br><p>When GridFilters <code>@cfg encode = false</code> (default):</p>
    * <pre><code>
    // phpMode == false (default):
    filter[0][data][type] list
    filter[0][data][value] value1
    filter[0][data][value] value2
    filter[0][field] prod 

    // phpMode == true:
    filter[0][data][type] list
    filter[0][data][value] value1, value2
    filter[0][field] prod 
    * </code></pre>
    * When GridFilters <code>@cfg encode = true</code>:
    * <pre><code>
    // phpMode == false (default):
    filter : [{"type":"list","value":["small","medium"],"field":"size"}]

    // phpMode == true:
    filter : [{"type":"list","value":"small,medium","field":"size"}]
    * </code></pre>
    */
    phpMode: false,
    /**
    * @cfg {Ext.data.Store} store
    * The {@link Ext.data.Store} this list should use as its data source
    * when the data source is <b>remote</b>. If the data for the list
    * is local, use the <code>{@link #options}</code> config instead.
    */
    separator: null,

    /**  
    * @private
    * Template method that is to initialize the filter and install required menu items.
    * @param {Object} config
    */
    init: function (config) {
        this.dt = new Ext.util.DelayedTask(this.fireUpdate, this);

        // if a menu already existed, do clean up first
        if (this.menu) {
            this.menu.destroy();
        }
        this.menu = new Ext.ux.menu.ListMenu(config);

        this.initializeEmptyValueCheckbox(this);

        this.separator = new Ext.menu.Separator();
        this.menu.add(this.separator);
        this.menu.add(this.checkboxItem);

        this.menu.on('checkchange', this.onCheckChange, this);

        this.checkboxItem.on('checkchange', this.onCheckChange, this);
    },

    disableFilter: function () {
        if (this.checkboxItem.checked != true) {
            var values = this.getValue();
            for (var i = 0; i < values.length; i++) {
                this.menu.items.get(values[i]).setChecked(false, true);
            }

            //Clears previous selected items
            this.menu.setSelected([]);
            this.checkboxItem.enable();
        } else {
            this.checkboxItem.enable();
            this.checkboxItem.setChecked(false);
        }
        this.active = false;
    },

    reconfigureFilters: function (newValue) {
        var i = this.menu.items.indexOf(this.separator);

        var item = new Ext.menu.CheckItem({
            text: Ext.util.Format.htmlEncode(newValue),
            itemId: newValue,
            group: this.menu.single ? Ext.id() : null,
            checked: this.menu.selected.indexOf(newValue) > -1
        });

        item.on('checkchange', this.menu.checkChange, this.menu);

        this.menu.insert(i, item);
    },

    /**
    * @private
    * Template method that is to get and return the value of the filter.
    * @return {String} The value of this filter
    */
    getValue: function () {
        if (this.checkboxItem.checked) {
            //ListMenu requires to return array and don't have correct behaviour
            //for items added directly like in case of Ext.menu.CheckItem
            return [''];
        }
        return this.menu.getSelected();
    },
    /**
    * @private
    * Template method that is to set the value of the filter.
    * @param {Object} value The value to set the filter
    */
    setValue: function (value) {
        this.menu.setSelected(value);
        this.fireEvent('update', this);
    },

    /**
    * @private
    * Template method that is to return <tt>true</tt> if the filter
    * has enough configuration information to be activated.
    * @return {Boolean}
    */
    isActivatable: function () {
        if (this.checkboxItem.checked)
            return true;
        return this.getValue().length > 0;
    },

    /**
    * @private
    * Template method that is to get and return serialized filter data for
    * transmission to the server.
    * @return {Object/Array} An object or collection of objects containing
    * key value pairs representing the current configuration of the filter.
    */
    getSerialArgs: function () {
        if (this.checkboxItem.checked)
            return { type: 'list', value: '' };
        var args = { type: 'list', value: this.phpMode ? this.getValue().join(',') : this.getValue() };
        return args;
    },

    /** @private */
    onCheckChange: function (item, checked) {
        if (item == this.checkboxItem) {
            //if empty values checkbox was changed disable/enable other checkboxes
            for (var i = 0; i < this.menu.items.length; i++) {
                //if not empty values checkbox then go into
                if (this.menu.items.items[i] != this.checkboxItem)
                //if checked == true then disable, if == false the enable
                    if (checked) {
                        this.menu.items.items[i].disable();
                    } else {
                        this.menu.items.items[i].enable();
                    }
            }
        }
        //if was checked not empty values checkbox
        else if (checked) {
            this.checkboxItem.disable();
        }
        //if was unchecked not empty values checkbox
        //and there is no other checked checkboxes
        else if (this.getCountOfCheckedItems() == 0)
            this.checkboxItem.enable();
        this.dt.delay(this.updateBuffer);
    },

    getCountOfCheckedItems: function () {
        var count = 0;
        for (var j = 0; j < this.menu.items.length; j++) {
            if (this.menu.items.items[j] != this.checkboxItem && this.menu.items.items[j].checked) {
                count++;
            }
        }
        return count;
    },

    /**
    * Template method that is to validate the provided Ext.data.Record
    * against the filters configuration.
    * @param {Ext.data.Record} record The record to validate
    * @return {Boolean} true if the record is valid within the bounds
    * of the filter, false otherwise.
    */
    validateRecord: function (record) {
        return this.getValue().indexOf(record.get(this.dataIndex)) > -1;
    }
});
