﻿
/**
Timepicker component. ExtJS 3.4 doesn't own timepicker as ExtJS 4.0, therefore we need to introduce own.
*/
Ext.ux.grid.TimePicker = Ext.extend(Ext.grid.GridPanel, {

    increment: 15,
    timeFormat: 'g:i A',
    initDate: [2008, 0, 1],

    /**
    Add into passed date time value choosen in this picker and return resulted datetime value.
    * @param {Object} date
    * @return Date object
    */
    getDateTime: function (date) {
        var selectedRows = this.getSelectionModel().getSelections();
        if (selectedRows.length > 0) {
            var timeRawValue = selectedRows[0].data.date;
            date.setHours(timeRawValue.getHours());
            date.setMinutes(timeRawValue.getMinutes());
        }

        return date;
    },

    /**
     * @private
     * Creates the internal {@link Ext.data.Store} that contains the available times. The store
     * is loaded with all possible times, and it is later filtered to hide those times outside
     * the minValue/maxValue.
     */
    createStore: function () {
        var me = this;
        var times = [];
        var min = me.absMin;
        var max = me.absMax;

        while (min <= max) {
            times.push([Ext.util.Format.date(min, me.timeFormat), min]);

            min = min.add('mi', me.increment); 
        }

        return new Ext.data.ArrayStore({
            fields: [{ name: 'disp' }, { name: 'date' }],
            data: times
        });
    },
    
    initComponent: function () {
        var me = this;
        
        var initDate = me.initialConfig.initDate || me.initDate;

        me.absMin = (new Date(initDate[0], initDate[1], initDate[2])).clearTime();
        me.absMax = (new Date(initDate[0], initDate[1], initDate[2])).clearTime().add('mi', (24 * 60) - 1);
        var isMsie = false;
        if ($("html").hasClass("sw-is-msie") && !$("html").hasClass("sw-is-msie-8"))
            isMsie = true;

        var config = {
            multiSelect: false,
            store: me.createStore(),
            width: 100,
            height: (isMsie) ? 189 : 200,
            hideHeader: true,
            cls: 'hide-header',
            autoScroll: true,
            viewConfig: { forceFit: true },
            layout: 'fit',
            columns: [
            {
                header: 'Time',
                width: 80,
                dataIndex: 'disp'
            }],
        }

        Ext.apply(this, config);
        Ext.apply(this.initialConfig, config);

        Ext.ux.grid.TimePicker.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('timepicker', Ext.ux.grid.TimePicker);

/**
 * Filter by a configurable Ext.picker.DatePicker menu
 *
 * This filter allows for the following configurations:
 *
 * - Any of the normal configs will be passed through to either component.
 * - There can be a docked config.
 * - The timepicker can be on the right or left (datepicker, too, of course).
 * - Choose which component will initiate the filtering, i.e., the event can be
 *   configured to be bound to either the datepicker or the timepicker, or if
 *   there is a docked config it be automatically have the handler bound to it.
 *
 * Although not shown here, this class accepts all configuration options
 * for {@link Ext.picker.Date} and {@link Ext.picker.Time}.
 *
 * In the case that a custom dockedItems config is passed in, the
 * class will handle binding the default listener to it so the
 * developer need not worry about having to do it.
 *
 * The default dockedItems position and the toolbar's
 * button text can be passed a config for convenience, i.e.,:
 *
 *     dock: {
 *        buttonText: 'Click to Filter',
 *        dock: 'left'
 *     }
 *
 * Or, pass in a full dockedItems config:
 *
 *     dock: {
 *        dockedItems: {
 *            xtype: 'toolbar',
 *            dock: 'bottom',
 *            ...
 *        }
 *     }
 *
 * Or, give a value of `true` to accept dock defaults:
 *
 *     dock: true
 *
 * But, it must be one or the other.
 *
 * Example Usage:
 *
 *     var filters = new Ext.ux.grid.GridFilters({
 *         ...
 *         filters: [{
 *             // required configs
 *             type: 'dateTime',
 *             dataIndex: 'date',
 *
 *             // optional configs
 *             positionDatepickerFirst: false,
 *             //selectDateToFilter: false, // this is overridden b/c of the presence of the dock cfg object
 *
 *             date: {
 *                 format: 'm/d/Y',
 *             },
 *
 *             time: {
 *                 format: 'H:i:s A',
 *                 increment: 1
 *             },
 *
 *             dock: {
 *                 buttonText: 'Click to Filter',
 *                 dock: 'left'
 *
 *                 // allows for custom dockedItems cfg
 *                 //dockedItems: {}
 *             }
 *         }]
 *     });
 *
 * In the above example, note that the filter is being passed a {@link #date} config object,
 * a {@link #time} config object and a {@link #dock} config. These are all optional.
 *
 * As for positioning, the datepicker will be on the right, the timepicker on the left
 * and the docked items will be docked on the left. In addition, since there's a {@link #dock}
 * config, clicking the button in the dock will trigger the filtering.
 */

Ext.ux.grid.filter.DateTimeFilter = Ext.extend(Ext.ux.grid.filter.DateFilter, {
    /**
     * @private
     */
    dateDefaults: {
        xtype: 'datepicker',
        format: 'm/d/Y',
        width: 200
    },

    /**
     * @private
     */
    timeDefaults: {
        xtype: 'timepicker', 
        width: 100,
        format: 'g:i A'
    },

    /**
     * @private
     */
    dockDefaults: {
        dock: 'top',
        buttonText: 'Filter'
    },

    /**
     * @cfg {Object} date
     * A {@link Ext.picker.Date} can be configured here.
     * Uses {@link #dateDefaults} by default.
     */

    /**
     * @cfg {Object} time
     * A {@link Ext.picker.Time} can be configured here.
     * Uses {@link #timeDefaults} by default.
     */

    /**
     * @cfg {Boolean/Object} dock
     * A {@link Ext.panel.AbstractPanel#cfg-dockedItems} can be configured here.
     * A `true` value will use the {@link #dockDefaults} default configuration.
     * If present, the button in the docked items will initiate the filtering.
     */

    /**
     * @cfg {Boolean} [selectDateToFilter=true]
     * By default, the datepicker has the default event listener bound to it.
     * Setting to `false` will bind it to the timepicker.
     *
     * The config will be ignored if there is a `dock` config.
     */
    selectDateToFilter: true,

    /**
     * @cfg {Boolean} [positionDatepickerFirst=true]
     * Positions the datepicker within its container.
     * A `true` value will place it on the left in the container.
     * Set to `false` if the timepicker should be placed on the left.
     * Defaults to `true`.
     */
    positionDatepickerFirst: true,

    reTime: /\s(am|pm)/i,
    reItemId: /\w*-(\w*)$/,
    
    /**
     * @private
     * Template method that is to initialize the filter and install required menu items.
     */
    init: function (config) {
        var me = this;

        if ($("html").hasClass("sw-is-msie"))
            me.dateDefaults.width = 208;
        
        if ($("html").hasClass("sw-is-msie-8"))
            me.dateDefaults.width = 206;

        var dateCfg = Ext.applyIf(me.date || {}, me.dateDefaults);
        var timeCfg = Ext.applyIf(me.time || {}, me.timeDefaults);
        var dockCfg = me.dock; // should not default to empty object
        var defaultListeners = {
                click: {
                    scope: me,
                    click: me.onMenuSelect
                },
                select: {
                    scope: me,
                    select: me.onMenuSelect
                }
            };

        var pickerCtnCfg, i, len, item, cfg;
        
        var items = [dateCfg, timeCfg];

            // we need to know the datepicker's position in the items array
            // for when the itemId name is bound to it before adding to the menu
          var  datepickerPosition = 0;

        if (!me.positionDatepickerFirst) {
            items = items.reverse();
            datepickerPosition = 1;
        }

        pickerCtnCfg = Ext.apply(me.pickerOpts, {
            xtype: !dockCfg ? 'container' : 'panel',
            layout: 'hbox',
            width: 300,
            items: items
        });

        // If there's no dock config then bind the default listener to the desired picker.
        if (!dockCfg) {
            if (me.selectDateToFilter) {
                dateCfg.listeners = defaultListeners.select;
            } else {
                timeCfg.listeners = defaultListeners.select;
            }
        } else if (dockCfg) {
            me.selectDateToFilter = null;

            if (dockCfg.dockedItems) {
                pickerCtnCfg.dockedItems = dockCfg.dockedItems;

                // TODO: allow config that will tell which item to bind the listener to
                // right now, it's using the first item
                pickerCtnCfg.dockedItems.items[dockCfg.bindToItem || 0].listeners = defaultListeners.click;
            } else {
                // dockCfg can be `true` if button text and dock position defaults are wanted
                if (Ext.isBoolean(dockCfg)) {
                    dockCfg = {};
                }
                dockCfg = Ext.applyIf(dockCfg, me.dockDefaults);
                pickerCtnCfg.dockedItems = {
                    xtype: 'toolbar',
                    dock: dockCfg.dock,
                    items: [
                        {
                            xtype: 'button',
                            text: dockCfg.buttonText,
                            flex: 1,
                            listeners: defaultListeners.click
                        }
                    ]
                };
            }
        }

        me.fields = {};
        for (i = 0, len = me.menuItems.length; i < len; i++) {
            item = me.menuItems[i];
            if (item !== '-') {
                pickerCtnCfg.items[datepickerPosition].itemId = item;

                cfg = {
                    itemId: 'range-' + item,
                    text: me[item + 'Text'],
                    menu: new Ext.menu.Menu({
                        items: pickerCtnCfg
                    }),
                    listeners: {
                        scope: me,
                        checkchange: me.onCheckChange
                    }
                };
                item = me.fields[item] = new Ext.menu.CheckItem(cfg);
            }
            me.menu.add(item);
        }
        me.values = {};

        me.initializeEmptyValueCheckbox(this);
        me.menu.add(new Ext.menu.Separator());
        me.menu.add(this.checkboxItem);
        me.checkboxItem.on('checkchange', me.onEmptyCheckChange, me);
        me.updateTask = new Ext.util.DelayedTask(me.fireUpdate, me);
    },

    onEmptyCheckChange: function() {
        var me = this;
        var fields = this.fields;
        if (me.checkboxItem.checked) {
            fields.before.disable();
            fields.after.disable();
        } else {
            fields.before.enable();
            fields.after.enable();
        }

        me.updateTask.delay(me.updateBuffer);
    },

    /**
     * @private
     */
    onCheckChange: function (item, checked) {
        var me = this;
        var menu = item.menu;
        
        var timepicker = menu.items.items[0].items.get(1);
        var datepicker = menu.items.items[0].items.get(0);
        var itemId = datepicker.id;
        var values = me.values;

        if (checked) {
            values[itemId] = timepicker.getDateTime(datepicker.value);
            me.checkboxItem.disable();
        } else {
            delete values[itemId];
            if (me.getCountOfCheckedItems() == 0)
                me.checkboxItem.enable();
        }

        me.setActive(me.isActivatable());
        me.fireEvent('update', me);
    },

    /** 
     * Handler for when the DatePicker for a field fires the 'select' event
     * @param {Ext.picker.Date} picker
     * @param {Object} date
     */
    //onMenuSelect: function (picker, date) {
    onMenuSelect: function (menuItem, value, picker) {
        // NOTE: we need to redefine the picker.
        var me = this;
        var fields = this.fields;
        var field = this.fields[menuItem.itemId];

        field.setChecked(true);

        var beforeValue = (typeof fields.before.menu.items != "undefined") 
                          && (typeof fields.before.menu.items.items != "undefined")
                          && (typeof fields.before.menu.items.items[0] != "undefined")
                          && (typeof fields.before.menu.items.items[0].items.get(0) != "undefined")
                          ? fields.before.menu.items.items[0].items.get(1).getDateTime(fields.before.menu.items.items[0].items.get(0).value.clearTime())
                          : value;

        var afterValue = (typeof fields.after.menu.items != "undefined")
                          && (typeof fields.after.menu.items.items != "undefined")
                          && (typeof fields.after.menu.items.items[0] != "undefined")
                          && (typeof fields.after.menu.items.items[0].items.get(0) != "undefined")
                          ? fields.after.menu.items.items[0].items.get(1).getDateTime(fields.after.menu.items.items[0].items.get(0).value.clearTime())
                          : value;

        if (field == fields.on) {
            fields.before.setChecked(false, true);
            fields.after.setChecked(false, true);
        } else {
            if (typeof fields.on != "undefined") {
                fields.on.setChecked(false, true);
            }

            if (field == fields.after && beforeValue < afterValue) {
                fields.before.setChecked(false, true);
            } else if (field == fields.before && afterValue > beforeValue) {
                fields.after.setChecked(false, true);
            }
        }

        me.fireEvent('update', me);

        // The timepicker's getBubbleTarget() returns the boundlist's implementation,
        // so it doesn't look up ownerCt chain (it looks up this.pickerField).
        // This is a problem :)
        // This can be fixed by just walking up the ownerCt chain
        // (same thing, but confusing without comment).
        menuItem.ownerCt.ownerCt.hide();
    },

    /**
     * @private
     * Template method that is to get and return serialized filter data for
     * transmission to the server.
     * @return {Object/Array} An object or collection of objects containing
     * key value pairs representing the current configuration of the filter.
     */
    getSerialArgs: function () {
        var me = this;
        var key;
        var fields = me.fields;
        var args = [];

        if (me.checkboxItem.checked)
            return { type: 'datetime', value: '' };

        for (key in fields) {
            if (fields[key].checked) {
                args.push({
                    type: 'datetime',
                    comparison: me.compareMap[key],
                    value: me.getFieldValue(key).format((me.dateformat || me.dateDefaults.format) + ' ' + me.timeDefaults.format)
                });
            }
        }
        return args;
    },

    getFieldValue: function(item) {
        var datepicker = this.fields[item].menu.items.items[0].get(0);
        var timepicker = this.fields[item].menu.items.items[0].get(1);
        return timepicker.getDateTime(datepicker.value);
    },

    /**
     * @private
     * Template method that is to set the value of the filter.
     * @param {Object} value The value to set the filter
     * @param {Boolean} preserve true to preserve the checked status
     * of the other fields.  Defaults to false, unchecking the
     * other fields
     */
    setValue: function (value, preserve) {
        var me = this;
        var fields = me.fields;
        var key;
        var val;
        var datepicker;

        for (key in fields) {
            val = value[key];
            if (val) {
                datepicker = Ext.getCmp(key).items.items[0].get(0);

                // Note that calling the Ext.picker.Date:setValue() calls Ext.Date.clearTime(),
                // which we don't want, so just call update() instead and set the value on the component.
                datepicker.update(val);
                datepicker.value = val;

                fields[key].setChecked(true);
            } else if (!preserve) {
                fields[key].setChecked(false);
            }
        }
        me.fireEvent('update', me);
    },

    getValue: function() {
        var key, result = {};
        for (key in this.fields) {
            if (this.fields[key].checked) {
                var picker = this.fields[key].menu.items.items[0].get(0);
                result[key] = picker.getValue();
            }
        }
        return result;
    },

    /**
     * Template method that is to validate the provided Ext.data.Record
     * against the filters configuration.
     * @param {Ext.data.Record} record The record to validate
     * @return {Boolean} true if the record is valid within the bounds
     * of the filter, false otherwise.
     */
    validateRecord: function (record) {
        // remove calls to Ext.Date.clearTime
        var me = this,
            key,
            pickerValue,
            val = record.get(me.dataIndex);

        if (!Ext.isDate(val)) {
            return false;
        }

        val = val.getTime();

        for (key in me.fields) {
            if (me.fields[key].checked) {
                pickerValue = me.getFieldValue(key).getTime();
                if (key == 'before' && pickerValue <= val) {
                    return false;
                }
                if (key == 'after' && pickerValue >= val) {
                    return false;
                }
                if (key == 'on' && pickerValue != val) {
                    return false;
                }
            }
        }
        return true;
    }

});