Ext.override(Ext.BoxComponent, {
   setWidth : function(width){
        if (isNaN(width) && (typeof width != 'object') && $("html").hasClass("sw-is-msie"))
	  width = "auto";
	return this.setSize(width);
    }
});