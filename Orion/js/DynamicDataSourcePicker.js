﻿var pickers = SW.Core.namespace("SW.Core.Pickers");
pickers.DYNAMIC_SELECTION_TYPE = {
  UNDEFINED : 0, 
  BASIC: 1,
  ADVANCED: 2
};
pickers.EXPR_BUILDER_WEB_SERVICE_URL = '/Orion/Services/ExpressionBuilderDataProvider.asmx';

pickers.DynamicDataSourcePicker = function (config) {
    /// <summary>Picker for selecting dynamic query.</summary>

    /// <param name="config" type="JSON">Configuration object.</param>
    /// <param name="config.wrapperID" type="String">Identifier of html element where will be picker rendered.</param>

    var changesLostConfirmMsg = '@{R=Core.Strings;K=WEBJS_ZT0_14;E=js}';


    var htmlTemplate =
        "<div class='dynamicDataSourcePicker' data-form='wrapper'>" +
            "<div class='selectorTypes'><table><tr>" +
                "<td><input type='radio' name='selectorType' value='{{BasicSelectorTypeValue}}' checked='checked' data-form='basicSelectorType' /></td>" +
                "<td class='text'>{{BasicSelectorTypeText}}</td>" +
                "<td><input type='radio' name='selectorType' value='{{AdvancesSelectorTypeValue}}' data-form='advancedSelectorType' /></td>" +
                "<td class='text'>{{AdvancedSelectorTypeText}}</td>" +
            "</tr></table></div>" +
            "<div class='masterEntities'>{{MasterEntityText}}  " +
            "<select data-form='entity'>" +
                "{# _.each(Entities, function (entity, index) {  #}" +
                    "<option value='{{entity.Name}}' data-plural='{{entity.DisplayNamePlural}}'>{{entity.DisplayName}}</option>" +
                "{# }); #}" +
            "</select>" +
            "</div>" +
            "<div class='exprBuilderEmptyMsg'></div>" +
            "<div id='exprBuilder' ></div>" +
        "</div>";


    // PRIVATE FIELDS:
    var self = this,
        $wrapper = null,
        masterEntitiesCache = null,
        dataSource = null,
        showNeb = false;

    init();

    // PUBLIC METHODS:

    this.render = function () {
        loadEntitiesAsync(function (entities) {
            masterEntitiesCache = entities;
            renderItself();
        });
    };


    this.getDataSource = function () {
        var dt = {
            Type: pickers.DATASOURCE_TYPE_ENUM.DYNAMIC,
            MasterEntity: getSelectedMasterEntity(),
            DynamicSelectionType: getSelectionType(),
            Filter: getExpr()
        };

        return dt;
    };

    this.setDataSource = function (value) {
        dataSource = value;

        if (value) {
            if (value.MasterEntity) {
                setMasterEntity(value.MasterEntity);
            }

            setSelectionType(value.DynamicSelectionType);

            redrawExprBuilder();
        }
    };
    this.updateSourceNameDelegate = null;
    this.updateSourceNameFnc = function (isEmpty) {
            if (isEmpty) {
                var pluralDisplayName = getMasterEntityElement().children(':selected').data('plural');
                $(".exprBuilderEmptyMsg").html('<div class="sw-suggestion">' +
                                                    '<span class="sw-suggestion-icon"></span>' +
                                                    '<span id="noConditionsHint">' +
                                                        String.format('@{R=Core.Strings;K=WEBJS_JP2_9;E=js}', pluralDisplayName) + 
                                                    '</span>' +
                                                '</div>');
                if (self.updateSourceNameDelegate) {
                    self.updateSourceNameDelegate(String.format('@{R=Core.Strings;K=WEBJS_AB0_5;E=js}', pluralDisplayName));
                }
            } else {
                $(".exprBuilderEmptyMsg").html('');
                if (self.updateSourceNameDelegate) {
                    self.updateSourceNameDelegate(null);
                }
            }
    };

    this.isValid = function () {
        return (showNeb) ? getExprBuilderElement().nested_expression_builder("validate") || true : true;
    };

    this.setFilter = function (value) {
        if (dataSource) {
            dataSource.Filter = value;
        }
    };

    this.setShowNeb = function(value) {
        showNeb = value;
    };

    // PRIVATE METHODS:

    function loadEntitiesAsync(callback) {

        // We alredy have entities loaded from server.
        if (masterEntitiesCache) {
            callback(masterEntitiesCache);
            // Else load it from server.
        } else {
            pickers.DynamicDataSourcePicker.getMasterEntities(function (entities) {
                callback(entities);
            });
        }
    }

    function renderItself() {
        var entitiesLowercase = masterEntitiesCache.Entities;
        for (var i = 0; i < entitiesLowercase.length; i++) {
            entitiesLowercase[i].Name = entitiesLowercase[i].Name.toLowerCase();
        }
        
        var templateData = {
            BasicSelectorTypeValue: pickers.DYNAMIC_SELECTION_TYPE.BASIC,
            BasicSelectorTypeText: '@{R=Core.Strings;K=WEBJS_ZT0_11;E=js}',
            AdvancesSelectorTypeValue: pickers.DYNAMIC_SELECTION_TYPE.ADVANCED,
            AdvancedSelectorTypeText: '@{R=Core.Strings;K=WEBJS_ZT0_12;E=js}',
            Entities: entitiesLowercase,
            MasterEntityText: '@{R=Core.Strings;K=WEBJS_ZT0_13;E=js}'
        };

        $wrapper = $('#' + config.wrapperID);

        // Removes previous instance.
        $wrapper.find('.dynamicDataSourcePicker').remove();

        // Adds new instance.
        var html = _.template(htmlTemplate, templateData);
        $wrapper.prepend(html);

      
        $wrapper.find('.selectorTypes').change(function (event) {
            var proceed = selectionTypeChanged();
            if (!proceed) {
                // Restore the previous value.
                setSelectionType(getSelectionTypeElement().data('previousValue'));
            }
            return proceed;
        });


        getMasterEntityElement().val('orion.nodes')
            .on('change', function () {
                var proceed = masterEntityChanged();
                if (!proceed) {
                    setMasterEntity(getMasterEntityElement().data('previousValue'));

                }
            });
        
        redrawExprBuilder();
        getSelectionTypeElement().data('previousValue', getSelectionType());
        getMasterEntityElement().data('previousValue', getSelectedMasterEntity());

    }

    function selectionTypeChanged() {
        var reset = false;
        var newSelectionType = getSelectionType();

        if (newSelectionType == pickers.DYNAMIC_SELECTION_TYPE.BASIC) {
            if (!showChangesLostConfirm()) {
                return false;
            }
            reset = true;
        } else {
            // For advanced selector we always show NEB
            showNeb = true;
        }

        getSelectionTypeElement().data('previousValue', newSelectionType);

        if (dataSource) {
            dataSource.Filter = reset ? null : getExpr();
        }
        
        redrawExprBuilder();

        return true;
    }

    function masterEntityChanged() {
        var newMasterEntity = getSelectedMasterEntity();
        var proceed = showChangesLostConfirm();

        if (proceed) {
            getMasterEntityElement().data('previousValue', newMasterEntity);
            if (dataSource) {
                dataSource.Filter = null;
            }
            redrawExprBuilder();
        }
    }

    function showChangesLostConfirm() {
        var currentExpState = getExpr();
        if (currentExpState) {
            if (confirm(changesLostConfirmMsg)) {
                return true;
            } else {
                return false;
            }
        }
        else {
            return true;
        }
    }

    function getExprBuilderElement() {
        return $wrapper.find('#exprBuilder');
    }

    function getExpr() {
        return (showNeb) ? getExprBuilderElement().nested_expression_builder("getExpression") : null;
    }

    function redrawExprBuilder() {
        if (showNeb) {
            var exprBuilderConfig = createExprBuilderConfig();

            getExprBuilderElement().nested_expression_builder(exprBuilderConfig);
        } else {
            createAddConditionBlock();
            self.updateSourceNameFnc(true);
        }
       
    };

    function createExprBuilderConfig() {
        var emptyDataSource = {
            "CommandText": "",
            "MasterEntity": "",
            "Filter": null,
            "EntityUri": []
        };

        dataSource = _.extend(emptyDataSource, dataSource, {
            Type: pickers.DATASOURCE_TYPE_ENUM.DYNAMIC,
            DynamicSelectionType: getSelectionType(),
            MasterEntity: getSelectedMasterEntity()
        });

        var baseConfig = (getSelectionType() == pickers.DYNAMIC_SELECTION_TYPE.BASIC) ?
                                                    pickers.DynamicDataSourcePicker.exprBuilderConfigSimple :
                                                    pickers.DynamicDataSourcePicker.exprBuilderConfigAdvanced;
        
        baseConfig.preferenceOptions = [
            {
                displayName: '@{R=Core.Strings;K=WEBJS_PS0_56;E=js}',
                actionId: "CLEAR",
                callback: function () {
                    
                    dataSource.Filter = null;
                    var exprBuilderConfig = createExprBuilderConfig();
                    getExprBuilderElement().nested_expression_builder(exprBuilderConfig);
                    redrawExprBuilder();
                }
            },
            {
                displayName: '@{R=Core.Strings.2;K=WEBJS_SO0_15;E=js}',
                actionId: "SHOW_SWQL",
                callback: function () {
                    var query = $.fn.dataSources.getSWQLquery();

                    $("#ShowSWQLDialog").dialog({
                        width: 430,
                        modal: true,
                        title: "@{R=Core.Strings.2;K=WEBJS_TK0_15;E=js}",
                        autoOpen: true
                    });

                    $("#ShowSWQLDialog span.closeButtonPlaceHolder").empty();
                    $(SW.Core.Widgets.Button('@{R=Core.Strings.2;K=WEBJS_PCO_005;E=js}', { type: 'secondary' })).appendTo($("#ShowSWQLDialog span.closeButtonPlaceHolder")).click(function () {
                        $(this).closest('.ui-dialog-content').dialog('close');
                    });

                    $("#showSWQLArea").val(query);
                }
            } // Show SWQL query
        ];
        // clone datasource and remove filter, so we're not sending it to the web methods
        var expr = dataSource.Filter;
        var ds = $.extend({}, dataSource);
        ds.Filter = null;

        var exprConfig = $.extend(true, {}, baseConfig, {
            dataSource: ds,
            expr: expr,
            onEmpty: function (isEmpty) {
                self.updateSourceNameFnc(isEmpty);
            },
            updateSourceNameDelegate: self.updateSourceNameFnc
        });

        return exprConfig;
    }
  
    function createAddConditionBlock() {
        getExprBuilderElement()
            .html("<table><tr><td data-name='showNEB' class='neb-add-simple'>@{R=Core.Strings;K=WEBJS_AB0_3;E=js}</td></tr></table>")
            .find('[data-name="showNEB"]')
            .click(function() {
                showNeb = true;
                redrawExprBuilder();
            });
    };

    function getMasterEntityElement() {
        if (!$wrapper) {
            $wrapper = $('#' + config.wrapperID);
        }
        return $wrapper.find("[data-form='entity']");
    }

    function setMasterEntity(value) {
        var valueLower = value.toLowerCase();
        getMasterEntityElement().val(valueLower);
    }

    function getSelectedMasterEntity() {
        return getMasterEntityElement().val();
    }

    function getSelectionTypeElement() {
        return $wrapper.find("input[type='radio']");
    }

    function getSelectionType() {
        return getSelectionTypeElement().filter(':checked').val();
    };

    function setSelectionType(value) {
        getSelectionTypeElement()
          .data('previousValue', value)
            .filter("[value='" + value + "']").attr("checked", "checked");
    }

    function init() {
        pickers.DynamicDataSourcePicker.getMasterEntities(function (entities) {
            masterEntitiesCache = entities;
        });
    };
};

pickers.DynamicDataSourcePicker.exprBuilderConfig = {};

pickers.DynamicDataSourcePicker.getMasterEntities = function(onSuccess, onError) {
    var requestParam = {};
    
    SW.Core.Services.callWebService(pickers.EXPR_BUILDER_WEB_SERVICE_URL, "GetMasterEntities", requestParam, 
        // On success
        function(response) {
            if (jQuery.isFunction(onSuccess)) {
                onSuccess(response);
            }
        }, 
       // On error
       function(msg) {
            if (jQuery.isFunction(onError)) {
                onError(msg, response);
            }     
       });
    };

