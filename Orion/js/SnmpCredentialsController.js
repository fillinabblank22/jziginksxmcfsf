﻿SW.Core.namespace("SW.Core").SnmpCredentials = function (config) {
    "use strict";
	
	const SnmpAuthMethod = {
		MD5: 0,
		SHA1: 1,
		None: 2
	}
	
	const SnmpPrivMethod = {
		DES56: 0,
		AES128: 1,
		AES192: 2,
		AES256: 3,
		None: 4
	}

    // Private methods
    function validateUserName(value, elem) {
        var validationResult = true;
        if (!isUserName($.trim(value))) {
            validationResult = false;
            elem.addClass("invalid");
        }
        return validationResult;
    }
    function isUserName(userName) {
        var regex = /^[^\/:*?"<>|]+$/;
        return regex.test(userName);
    }

    function validateConfiguration(configuration) {
        var isValid = true;
        $container.find("[data-form='snmpCredUserName']").removeClass("invalid");
        if (configuration.Version == 3) {
            if (!validateUserName(configuration.UserName, $container.find("[data-form='snmpCredUserName']"))) {
                isValid = false;
            }
        }
        return isValid;
    }

    var createConfiguration = function () {
        var configuration = {
            Version: $container.find("[data-form='SNMPVersion']").prop("selectedIndex") + 1,
            //community string
            Name: $container.find("[data-form='communityString']").val()
        };
        if (configuration.Version == 3) {
            configuration.UserName = $container.find("[data-form='snmpCredUserName']").val();
            configuration.PrivMethod = SnmpPrivMethod[$container.find("[data-form='privMethodType']").prop("value")];
            configuration.AuthMethod = SnmpAuthMethod[$container.find("[data-form='authMethodType']").prop("value")];
            configuration.PrivIsKey = $container.find("[data-form='privPasswordIsKey']").find("input:checked").length == 1;
            configuration.AuthIsKey = $container.find("[data-form='authPasswordIsKey']").find("input:checked").length == 1;
            configuration.AuthPassword = $container.find("[data-form='authPassword'").val();
            configuration.PrivPassword = $container.find("[data-form='privPassword'").val();
        }
        return configuration;
    };

    function versionChanged(elem) {
        if ($(elem).val() != "SNMP3") {
            $container.find("#credEditV1V2Controls").show();
            $container.find("#credEditV3Controls").hide();
        } else {
            $container.find("#credEditV1V2Controls").hide();
            $container.find("#credEditV3Controls").show();
        }
    }

    // Public methods
    this.init = function () {
        $container = $('#' + config.containerID);
        $container.find("[data-form='SNMPVersion']").change(function () {
            versionChanged(this);
        });
        $container.find("[data-form='SNMPVersion']").change();
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback();
        }
    };

    this.validate = function () {
        var configuration = createConfiguration();
        return validateConfiguration(configuration);
    };

    this.getSnmpCredentialsAsync = function () {
        return createConfiguration();
    };

    this.getSnmpVersion = function () {
        return $container.find("[data-form='SNMPVersion']").val();
    };

    // Constructor

    var self = this;
    var $container = null;
}
