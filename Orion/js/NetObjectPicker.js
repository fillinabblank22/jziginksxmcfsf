﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');

SW.Orion.NetObjectPicker = function () {
    ORION.prefix = "Orion_NetObjectPicker_";
    var initialized;
    var tree;
    var treePanel;
    var rightPanelTitle = '@{R=Core.Strings;K=WEBJS_IB0_35;E=js}';
    var leftPanelTitle = '@{R=Core.Strings;K=WEBJS_IB0_34;E=js}';
    var entityName = '';
    var supportedInterfaces = [];
    var selectionMode;
    var refreshTreeOnLoad = false;
    var sqlFilter = '';
    var selectedObjects = [];
    var gridItemsFieldClientID = '';
    var unresponsiveServersNotifierClientID = '';
    var maxAllowedCount;
    var blockCheckAllItems = false;
    var groupByChanged = false;
    var defaultGroupingProperties = {};
    var isMultipleObjects = false;
    var texts = {
        'toomany.text': '@{R=Core.Strings;K=WEBJS_IB0_41; E=js}',
        'toomany.title': '@{R=Core.Strings;K=WEBJS_AK0_74; E=js}'
    };

    Ext.override(Ext.tree.TreeEventModel, {
        delegateClick: function (e, t) {
            if (this.beforeEvent(e)) {
                if (e.getTarget('input[type=checkbox]', 1) || e.getTarget('input[type=radio]')) {
                    this.onCheckboxClick(e, this.getNode(e));
                } else if (e.getTarget('.x-tree-ec-icon', 1)) {
                    this.onIconClick(e, this.getNode(e));
                } else if (this.getNodeTarget(e)) {
                    var selectedNode = this.getNode(e);
                    this.onNodeClick(e, selectedNode);
                    if (selectedNode.leaf === true)
                        this.onNodeDblClick(e, selectedNode);
                }
            } else {
                this.checkContainerEvent(e, 'click');
            }
        }
    });

    Ext.override(Ext.tree.TreeNodeUI, {
        renderElements: function (n, a, targetNode, bulkRender) {

            this.indentMarkup = n.parentNode ? n.parentNode.ui.getChildIndent() : '';

            var cb = Ext.isBoolean(a.checked),
                nel,
                t = n.getOwnerTree(),
                href = this.getHref(a.href),
                buf = ['<li class="x-tree-node"><div ext:tree-node-id="', Ext.util.Format.htmlEncode(n.id), '" class="x-tree-node-el x-tree-node-leaf x-unselectable ', a.cls, '" unselectable="on">',
                    '<span class="x-tree-node-indent">', this.indentMarkup, "</span>",
                    '<img alt="" src="', this.emptyIcon, '" class="x-tree-ec-icon x-tree-elbow" />',
                    '<img alt="" src="', a.icon || this.emptyIcon, '" class="x-tree-node-icon', (a.icon ? " x-tree-node-inline-icon" : ""), (a.iconCls ? " " + a.iconCls : ""), '" unselectable="on" />',
                    (selectionMode == 'Single') ? (cb ? ('<input class="x-tree-node-cb" type="radio" name="' + t.id + 'radionode" ' + (this.node.isExpandable() ? 'style="display: none;!important" ' : '') + (a.checked ? 'checked="checked" />' : '/>')) : '') :
                        (cb ? ('<input class="x-tree-node-cb" type="checkbox" ' + (a.checked ? 'checked="checked" />' : '/>')) : ''),
                    '<a hidefocus="on" class="x-tree-node-anchor" href="', href, '" tabIndex="1" ',
                    a.hrefTarget ? ' target="' + a.hrefTarget + '"' : "", '><span unselectable="on">', n.text, "</span></a></div>",
                    '<ul class="x-tree-node-ct" style="display:none;"></ul>',
                    "</li>"].join('');

            if (bulkRender !== true && n.nextSibling && (nel = n.nextSibling.ui.getEl())) {
                this.wrap = Ext.DomHelper.insertHtml("beforeBegin", nel, buf);
            } else {
                this.wrap = Ext.DomHelper.insertHtml("beforeEnd", targetNode, buf);
            }

            this.elNode = this.wrap.childNodes[0];
            this.ctNode = this.wrap.childNodes[1];
            var cs = this.elNode.childNodes;
            this.indentNode = cs[0];
            this.ecNode = cs[1];
            this.iconNode = cs[2];
            var index = 3;
            if (cb) {
                this.checkbox = cs[3];

                this.checkbox.defaultChecked = this.checkbox.checked;
                index++;
            }
            this.anchor = cs[index];
            this.textNode = cs[index].firstChild;
        }
    });

    var isAllowedToAdd = function () {
        if (selectedObjects.length >= maxAllowedCount) {
            Ext.Msg.show({
                title: texts['toomany.title'],
                cls: 'warn-popup-message',
                msg: String.format(texts['toomany.text'], maxAllowedCount),
                width: 500,
                buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
                icon: Ext.Msg.WARNING
            });
            return false;
        }
        return true;
    };

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        if (query != null) {
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
        }
        return "";
    }

    function createDataTreeLoader(config) {
        var defaultConfig = {
            handleResponse: function (response) {
                this.transId = false;
                var a = response.argument;
                var responseObject = Ext.decode(response.responseText);

                if (responseObject.errorControl && responseObject.errorControl != '') {
                    showSwisError(responseObject.errorControl);
                }

                response.responseData = responseObject.data;

                this.processResponse(response, a.node, a.callback, a.scope);
                this.fireEvent("load", this, a.node, response);
            }
        };

        var treeLoader = new Ext.tree.TreeLoader($.extend({}, config, defaultConfig));

        return treeLoader;
    }

    var showSwisError = function (errorControlHtml) {
        $('.swis-error-wrapper').show();
        if ($('.swis-error-wrapper').length > 0) {
            $('.swis-error-wrapper').html(errorControlHtml);
        }
    };

    var hideSwisError = function () {
        $('.swis-error-wrapper').hide();
    };

    LoadDefaultGroupingProperties = function () {
        SW.Core.Services.callWebServiceSync("/Orion/Services/AddRemoveObjects.asmx", "GetDefaultGroupByProperties", {}, function (result) {
            defaultGroupingProperties = result;
        });
    };

    LoadEntities = function (itemType, entityIds) {
        if (entityIds.length > 0) {
            ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "LoadEntitiesByUris", { entityType: itemType, uris: entityIds }, function (result) {
                for (var i = 0; i < result.length; i++) {
                    addToSelected(
                        {
                            attributes: {
                                id: result[i].ID,
                                status: result[i].Status,
                                statusIconHint: result[i].StatusIconHint,
                                uri: result[i].Uri,
                                text: result[i].FullName,
                                fullName: result[i].FullName,
                                entity: result[i].ItemType,
                                instanceSiteId: result[i].InstanceSiteId
                            }
                        });
                }
            });
        }
    };

    LoadMultipleEntities = function (entityIds) {
        if (entityIds.length > 0) {
            ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "LoadMultipleEntitiesByUris", { uris: entityIds }, function (result) {
                for (var i = 0; i < result.length; i++) {
                    addToSelected(
                        {
                            attributes: {
                                id: result[i].ID,
                                status: result[i].Status,
                                statusIconHint: result[i].StatusIconHint,
                                uri: result[i].Uri,
                                text: result[i].FullName,
                                fullName: result[i].FullName,
                                entity: result[i].ItemType,
                                instanceSiteId: result[i].InstanceSiteId
                            }
                        });
                }
            });
        }
    };

    GetGroupByProperties = function (itemType, onSuccess) {
        ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "GetGroupByProperties", { entityType: itemType }, onSuccess);
    };

    LoadAvailableEntities = function (entityToSelect, callback) {
        var entityList = [];
        for (var j = 0; j < supportedInterfaces.length; j++) {
            entityList.push(supportedInterfaces[j].MasterEntity);
        }

        ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "GetAvailableEntityNames", { entities: entityList }, function (result) {
            var entitiesSelect = $("#entitiesSelect");
            entitiesSelect.empty();
            for (var i = 0; i < result.length; i++) {
                var item = result.sort(function (a, b) { return a.DisplayName.localeCompare(b.DisplayName); })[i];
                var selected = (item.Entity == entityToSelect) ? 'selected="selected"' : '';

                entitiesSelect.append('<option value="' + item.Entity + '" ' + selected + ' >' + item.DisplayName + '</option>');
            }

            entitiesSelect.change(function () {
                var newEntityName = GetSelectedEntityName();
                var newEntityDisplayName = GetSelectedEntityDisplayName();
                var newSelectionMode = GetEntitySelectionMode();
                if (newEntityName != entityName || newSelectionMode != selectionMode || refreshTreeOnLoad) {
                    CancelSearch(false);
                    if (!isMultipleObjects) {
                        SW.Orion.NetObjectPicker.ClearCheckedEntities();
                        $('#remove-all-button').css('display', 'none');
                    }
                    entityName = newEntityName;
                    selectionMode = GetEntitySelectionMode();
                    $('#label-available').html(String.format(leftPanelTitle, newEntityDisplayName));
                    $('#label-selected').html(String.format(rightPanelTitle, newEntityDisplayName, ''));
                    refreshData();
                    refreshTreeOnLoad = false;
                }
            });
            entitiesSelect.change();
            selectionMode = GetEntitySelectionMode();

            var pluralDisplayName = GetSelectedEntityDisplayName();
            var entityCount = entitiesSelect.children('option').length;

            if (entityCount == 1) {
                entitiesSelect.closest('td').hide();
            } else {
                entitiesSelect.closest('td').show();
            }
            if (SW.Orion.NetObjectPicker.SetupHintForSpecificObjectEntity)
                SW.Orion.NetObjectPicker.SetupHintForSpecificObjectEntity(entityCount, result[0].DisplayNameSingular, pluralDisplayName, selectionMode);

            if (callback)
                callback();
        });

    };

    LoadSolarwindsServers = function (entityToSelect, callback) {
        ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "GetSolarwindsServers", { enabledServersFilter: true }, function (result) {
            var solarwindsServersSelect = $("#solarwindsServersSelect");

            solarwindsServersSelect.empty();

            result = result.sort(function (a, b) { return a.Name.localeCompare(b.Name); });

            var item;
            var downServersPresent = false;
            for (var i = 0; i < result.length; i++) {
                item = result[i];

                solarwindsServersSelect.append('<option value="' + item.ServerID + '"' + (item.Down ? 'class="server-select-down-status"' : '') + '>' + Ext.util.Format.htmlEncode(item.Name) + '</option>');

                if (item.Down) {
                    downServersPresent = true;
                }
            }

            solarwindsServersSelect.prepend('<option value="-1"' + (downServersPresent ? 'class="server-select-down-status"' : '') + '>@{R=Core.Strings;K=WorldMapView_SWServerFilter_Default;E=js}</option>');

            refreshTreeOnLoad = true;
            solarwindsServersSelect.change(function () {
                var serverId = GetSelectedServerId();
                var selectedServersDown = GetSelectedServerIsDown();
                var downServerNames;
                if (serverId == '-1') {
                    sqlFilter = '';

                    if (selectedServersDown) {
                        downServerNames = GetAllDownServerNames();
                    }

                } else {
                    sqlFilter = 'InstanceSiteID=' + serverId;

                    if (selectedServersDown) {
                        downServerNames = [GetSelectedServerName()];
                    }
                }

                var unresponsiveServersNotifier = new SW.Orion.UnresponsiveServersNotifier(unresponsiveServersNotifierClientID);
                if (selectedServersDown) {
                    unresponsiveServersNotifier.show(downServerNames);
                } else {
                    unresponsiveServersNotifier.hide();
                }

                LoadAvailableEntities((entityToSelect) ? entityToSelect : "Orion.Nodes", function () {
                    SW.Orion.NetObjectPicker.SetSelectedEntities(entities);
                    if (mode != undefined)
                        selectionMode = mode;
                });
                LoadGroups();
            });
            solarwindsServersSelect.change();
            refreshTreeOnLoad = true;
        });
    };

    GetAllDownServerNames = function () {
        var downServerNames = [];

        $('#solarwindsServersSelect option.server-select-down-status').each(function () {
            if ($(this).val() != '-1') {
                downServerNames.push($(this).text());
            }
        });

        return downServerNames;
    }

    GetSelectedServerName = function () {
        return $("#solarwindsServersSelect option:selected").text();
    }

    LoadGroupByProperties = function (defaultGroupBy) {
        CancelSearch(false);
        var itemType = entityName;

        if (itemType) {
            GetGroupByProperties(itemType, function (result) {
                var groupBySelect = $("#groupBySelect");
                groupBySelect.empty();

                for (var i = 0; i < result.length; i++) {
                    var item = result[i];
                    var selected = (item.Column == defaultGroupBy) ? 'selected="selected"' : '';

                    groupBySelect.append('<option value="' + item.Column + '" type="' + item.Type + '" ' + selected + ' >' + item.DisplayName + '</option>');
                }
                groupBySelect.click(function () {
                    groupByChanged = true;
                });
                CancelSearch(false);
                LoadGroups();
                groupBySelect.change(function () {
                    if (groupByChanged) {
                        CancelSearch(false);
                        LoadGroups();
                        groupByChanged = false;
                    }
                });
            });
        }
    };

    GetGroups = function (itemType, groupBy, searchValue, onSuccess) {
        ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "GetObjectPickerFilteredEntityGroups", { entityType: itemType, groupByProperty: groupBy, searchValue: searchValue, viewId: getQueryVariable("ViewID"), filter: sqlFilter }, onSuccess);
    };

    // Changes icon on gived TreeNode in runtime
    SetNodeIcon = function (node, icon) {
        if ((node.ui) && (node.ui.iconNode)) {
            node.ui.iconNode.src = icon;
        }
    };

    CancelSearch = function (reloadTree) {
        $('#searchBox').val('');

        if (reloadTree)
            LoadGroups();
    };

    DoSearch = function () {
        LoadGroups();
    };

    GetLabelForNode = function (name, count) {
        return String.format("{0} ({1})", Ext.util.Format.htmlEncode(name), count);
    };

    UpdateGroupItemsCount = function (node, newCount) {
        node.attributes.groupCount = newCount;
        node.setText(GetLabelForNode(node.attributes.groupName, node.attributes.groupCount));
    };

    function expandAndUpdate(node) {
        hideSwisError();
        var propCheck = node.attributes.propagateCheck;
        if (selectionMode == "Single") return;
        var loader = createDataTreeLoader({
            dataUrl: '/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, n) {
                        this.baseParams.entityType = entityName;
                        this.baseParams.groupBy = GetGroupBy();
                        this.baseParams.groupByType = GetGroupByType();
                        this.baseParams.searchValue = GetSearchValue();
                        this.baseParams.value = n.attributes.value;
                        this.baseParams.excludeDefinitions = [];
                        this.baseParams.filter = sqlFilter;
                        this.baseParams.viewId = getQueryVariable("ViewID");
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, n, response) {
                        if (blockCheckAllItems && n.attributes.checked && !propCheck) return;
                        else blockCheckAllItems = false;
                        var result = response.responseData;
                        for (var i = 0; i < result.length; i++) {
                            //check if it isn't "There are XX more objects" message
                            if (result[i].uri != '0') {
                                var child = new Ext.tree.TreeNode(result[i]);
                                if (n.attributes.checked) {
                                    if (!isAllowedToAdd()) { blockCheckAllItems = !propCheck; return; }
                                    addToSelected(child);
                                }
                                else {
                                    removeFromSelectedByUri(child.attributes.uri);
                                }
                            }
                        }
                    }
                },
                // after load event if the network request failed
                loadexception: {
                    fn: function () {
                        Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: '@{R=Core.Strings;K=WEBJS_TM0_139;E=js}', minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    }
                }
            }
        });
        loader.load(node, null, null);
    }

    function isSelected(node) {
        for (var i = 0; i < selectedObjects.length; i++) {
            if (selectedObjects[i].uri == node.attributes.uri) {
                return true;
            }
        }
        return false;
    }

    function addToSelected(node) {
        if (isSelected(node)) return;
        $('#selectedObjectsEmpty').css('display', 'none');
        $('#remove-all-button').css('display', 'block');
        var tableBody = $("#selected-objects-panel > div > table > tbody:last")[0];
        var tr = document.createElement("tr");
        tableBody.appendChild(tr);

        if (node.attributes.status != '-1') {
            var td = document.createElement("td");
            td.style.width = "20px";
            var statusIcon = document.createElement("img");
            statusIcon.src = String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small{2}', node.attributes.entity, node.attributes.status, (!node.attributes.statusIconHint) ? "" : "&hint=" + node.attributes.statusIconHint);
            td.appendChild(statusIcon);
            tr.appendChild(td);
        }

        td = document.createElement("td");
        td.innerHTML = node.attributes.text;
        tr.appendChild(td);

        td = document.createElement("td");
        td.style.width = "20px";
        var img = document.createElement("img");

        img.src = "/Orion/images/Reports/delete_netobject.png";
        img.onclick = function () { removeFromSelectedByUri(node.attributes.uri); };
        img.style.cursor = "pointer";
        td.appendChild(img);
        tr.appendChild(td);
        selectedObjects.push({ id: node.attributes.id, entity: node.attributes.entity, uri: node.attributes.uri, status: node.attributes.status, fullName: node.attributes.fullName, refTr: tr, instanceSiteId: node.attributes.instanceSiteId });
        $('#label-selected').html(String.format(rightPanelTitle, GetSelectedEntityDisplayName(), selectedObjects.length));

        if (SW.Orion.NetObjectPicker.updateSourceNameDelegate) {
            SW.Orion.NetObjectPicker.updateSourceNameDelegate((selectedObjects.length == 1) ? $.trim(node.attributes.fullName) || $.trim(node.attributes.text) : null);
        }
    }

    function removeFromSelectedByUri(uri) {
        // uncheck tree node
        var checkedNodes = tree.getChecked();
        for (var i = 0; i < checkedNodes.length; i++) {
            if (checkedNodes[i].attributes.uri == uri) {
                checkedNodes[i].ui.toggleCheck(false);
                break;
            }
        }
        removeFromSelectedList(uri);
    }

    function removeFromSelectedList(uri) {
        var index;
        var found = false;
        // removing from array described ui
        for (index = 0; index < selectedObjects.length; index++) {
            if (selectedObjects[index].uri == uri) {
                found = true;
                // Removes row from table tbody.
                $("#selected-objects-panel > div > table > tbody:last")[0].removeChild(selectedObjects[index].refTr);
                break;
            }
        }

        if (found)
            selectedObjects.splice(index, 1);

        if (selectedObjects.length == 0) {
            $('#selectedObjectsEmpty').css('display', 'block');
            $('#remove-all-button').css('display', 'none');
        }
        if (SW.Orion.NetObjectPicker.updateSourceNameDelegate) {
            if (jQuery.browser.version <= 8.0) {
                SW.Orion.NetObjectPicker.updateSourceNameDelegate(selectedObjects.length == 1 ? $.trim(selectedObjects[0].refTr.outerText) : null);
            }
            else {
                SW.Orion.NetObjectPicker.updateSourceNameDelegate(selectedObjects.length == 1 ? selectedObjects[0].refTr.textContent : null);
            }
        }

        $('#label-selected').html(String.format(rightPanelTitle, GetSelectedEntityDisplayName(), (selectedObjects.length > 0) ? selectedObjects.length : ''));
    }

    RemoveAllFromSelected = function () {
        for (var i = selectedObjects.length - 1; i >= 0; i--) {
            removeFromSelectedByUri(selectedObjects[i].uri);
        }
    };

    LoadGroups = function () {
        hideSwisError();
        var itemType = entityName;
        var groupBy = GetGroupBy();
        var groupByType = GetGroupByType();
        var searchValue = GetSearchValue();
        var loadText = "@{R=Core.Strings;K=WEBJS_VB0_1; E=js}";

        if (searchValue) {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchCancel.gif');
            $('#searchButton').unbind('click');
            $('#searchButton').click(function () { CancelSearch(true); });
            loadText = "@{R=Core.Strings;K=WEBJS_TM0_34; E=js}";
        } else {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchIcon.gif');
            $('#searchButton').unbind('click');
            $('#searchButton').click(DoSearch);
        }

        ClearTree();
        tree.root.appendChild({ text: loadText, icon: '/Orion/images/AJAX-Loader.gif', expandable: false, leaf: true });

        if ((itemType) && (groupBy)) {
            GetGroups(itemType, groupBy, searchValue, function (result) {
                ClearTree();
                for (var i = 0; i < result.length; i++) {
                    var item = result[i];

                    // if if groupBy is 'Status' or ends with '.Status' then value should be item.Status
                    var displayedName = item.Name == '[Unknown]' ? '@{R=Core.Strings;K=WEBJS_VB0_70; E=js}' : item.Name;

                    var itemValue = item.GroupValue;
                    if (item.Count > 0) {
                        var node = new Ext.tree.AsyncTreeNode({
                            id: 'tree-node-' + (i),
                            text: GetLabelForNode(displayedName, item.Count),
                            value: itemValue,
                            groupCount: item.Count,
                            groupName: item.Name,
                            icon: item.Status != '-1' ? String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small{2}', itemType, item.Status, (!item.StatusIconHint) ? "" : "&hint=" + item.StatusIconHint) : Ext.BLANK_IMAGE_URL,
                            allowDrag: true,
                            isGroup: true,
                            leaf: false,
                            checked: false,
                            propagateCheck: true,
                            listeners: {
                                'checkchange': function (selnode, checked) {
                                    if (!checked) blockCheckAllItems = false;
                                    if (selnode.attributes.propagateCheck) {
                                        selnode.eachChild(function (n) {
                                            n.attributes.propagateCheck = false;
                                            n.checked = checked;
                                            n.getUI().toggleCheck(checked);
                                            n.attributes.propagateCheck = true;
                                        });
                                    }
                                    if (!selnode.expanded) expandAndUpdate(selnode);
                                    if (checked) {
                                        selnode.getUI().addClass('x-tree-selected');
                                    } else {
                                        selnode.getUI().removeClass('x-tree-selected');
                                    }
                                }
                            },
                            loader: createDataTreeLoader({
                                dataUrl: '/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsTreeProvider.ashx',
                                listeners: {
                                    // init load event handler
                                    beforeload: {
                                        fn: function (treeLoader, node) {
                                            this.baseParams.entityType = itemType;
                                            this.baseParams.groupBy = groupBy;
                                            this.baseParams.groupByType = groupByType;
                                            this.baseParams.searchValue = searchValue;
                                            this.baseParams.value = node.attributes.value;
                                            this.baseParams.excludeDefinitions = [];
                                            this.baseParams.filter = sqlFilter;
                                            this.baseParams.viewId = getQueryVariable("ViewID");

                                            SetNodeIcon(node, '/Orion/images/AJAX-Loader.gif', false);
                                        }
                                    },
                                    // after load event handler
                                    load: {
                                        fn: function (treeLoader, node) {
                                            node.expanded = true;
                                            SetNodeIcon(node, node.attributes.icon);
                                        }
                                    },
                                    // after load event if the network request failed
                                    loadexception: {
                                        fn: function (treeLoader, n, response) {
                                            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: '@{R=Core.Strings;K=WEBJS_TM0_139;E=js}', minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                                        }
                                    }
                                }
                            })
                        });

                        node.attributes.loader.baseParams.icon = node.attributes.icon;
                        tree.root.appendChild(node);
                    }
                    else {
                        tree.root.appendChild(new Ext.tree.TreeNode({
                            text: item.Name,
                            allowDrag: false,
                            icon: '/Orion/StatusIcon.ashx?entity=&status=&size=small'
                        }));
                    }
                }
            });
        } else if (itemType) {
            // no grouping - load entites directly
            LoadEntitiesNonGrouped(itemType, searchValue);
        }
    };

    LoadEntitiesNonGrouped = function (itemType, searchValue) {
        hideSwisError();
        var loader = createDataTreeLoader({
            dataUrl: '/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, node) {
                        this.baseParams.entityType = itemType;
                        this.baseParams.searchValue = searchValue;
                        this.baseParams.excludeDefinitions = [];
                        this.baseParams.filter = sqlFilter;
                        this.baseParams.viewId = getQueryVariable("ViewID");
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, node, response) {
                        ClearTree();
                        var result = response.responseData;
                        for (var i = 0; i < result.length; i++) {
                            var child = new Ext.tree.TreeNode(result[i]);
                            tree.root.appendChild(child);
                        }
                    }
                },
                // after load event if the network request failed
                loadexception: {
                    fn: function (treeLoader, n, response) {
                        Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: '@{R=Core.Strings;K=WEBJS_TM0_139;E=js}', minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    }
                }
            }
        });
        loader.load(new Ext.tree.TreeNode(), null, null);
    };

    updateSourceNameDelegate = null;

    ClearTree = function () {
        while (tree.root.firstChild) {
            tree.root.removeChild(tree.root.firstChild);
        }
        tree.root.removeAll();
    };

    ReloadTree = function () {
        var itemType = entityName;
        var groupBy = GetGroupBy();

        if (groupBy) {
            $.each(tree.root.childNodes, function (index, node) {
                if (node.loaded) {
                    node.attributes.loader.baseParams.expandAfterLoad = node.expanded;
                    node.reload(function () { }, node);
                }
            });
        } else {
            LoadEntitiesNonGrouped(itemType);
        }
    };

    GetSelectedServerId = function () {
        var item = $("#solarwindsServersSelect option:selected");
        return item.val();
    };

    GetSelectedServerIsDown = function () {
        return $("#solarwindsServersSelect option:selected").hasClass("server-select-down-status");
    };

    GetSelectedEntityName = function () {
        var item = $("#entitiesSelect option:selected");
        return item.val();
    };

    GetSelectedEntityDisplayName = function () {
        var item = $("#entitiesSelect option:selected");

        if (isMultipleObjects) {
            return '@{R=Core.Strings;K=WEBJS_AY0_30; E=js}';
        }
        return item.text();
    };

    GetGroupBy = function () {
        var item = $("#groupBySelect option:selected");
        $("#groupByType").val(item.attr("type")); // save the type of group by selection
        return item.val();
    };

    GetGroupByType = function () {
        return $("#groupByType").val();
    };

    GetSearchValue = function () {
        return $("#searchBox").val();
    };

    CheckChange = function (node, checked) {
        if (checked) {
            if (blockCheckAllItems && !node.attributes.propagateCheck) return;
            else blockCheckAllItems = false;
            if (selectionMode == "Single") {
                for (var i = 0; i < selectedObjects.length; i++) {
                    removeFromSelectedByUri(selectedObjects[i].uri);
                }
            }
            if (!isAllowedToAdd()) { blockCheckAllItems = !node.attributes.propagateCheck; return; }
            addToSelected(node);
            node.getUI().addClass('x-tree-selected');
        } else {
            removeFromSelectedList(node.attributes.uri);
            node.getUI().removeClass('x-tree-selected');
            if ((node.parentNode) && (node.attributes.propagateCheck)) {
                node.parentNode.attributes.propagateCheck = false;
                node.parentNode.getUI().removeClass('x-tree-selected');
                node.parentNode.getUI().toggleCheck(false);
                node.parentNode.attributes.propagateCheck = true;
            }
            blockCheckAllItems = false;
        }
    };

    ToggleAllCheckboxes = function (value) {
        tree.root.eachChild(function (n) {
            n.attributes.propagateCheck = false;
            n.checked = value;
            n.getUI().toggleCheck(value);
            n.attributes.propagateCheck = true;

            if (n.attributes.isGroup) {
                n.eachChild(function (child) {
                    child.attributes.propagateCheck = false;
                    child.checked = value;
                    child.getUI().toggleCheck(value);
                    child.attributes.propagateCheck = true;
                });
            }
        });
    };

    EmptySelectedObjects = function () {
        $("#selected-objects-panel > div > table > tbody:last").empty();
        selectedObjects = [];

        $('#selectedObjectsEmpty').css('display', 'block');
    };

    InitTree = function () {
        var selectPanel = new Ext.Container({
            applyTo: 'GroupItemsSelector',
            region: 'north',
            height: 100
        });

        tree = new Ext.tree.TreePanel({
            id: 'TreeExtPanel',
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDrag: true,
            height: 277, // border layout somehow doesn't work so we have to set height manually
            region: 'center',
            root: {
                text: '',
                id: 'root'
            },
            rootVisible: false,
            ddGroup: "treeDDGroup"
        });

        treePanel = new Ext.Panel({
            renderTo: 'netobject-tree-panel',
            //layout: 'fit',
            region: 'west',
            split: true,
            items: [
                selectPanel,
                tree
            ]
        });
    };

    function GetEntitySelectionMode() {
        // check if there is multiple selection for this entity
        for (var i = 0; i < supportedInterfaces.length; i++) {
            if (supportedInterfaces[i].SelectionMode == 'Any' || supportedInterfaces[i].MasterEntity == entityName && supportedInterfaces[i].SelectionMode == 'Multiple') {
                $('.selectAllButton').css('opacity', '1').attr("disabled", false);
                return 'Multiple';
            }
        }
        $('.selectAllButton').css('opacity', '0.5').attr("disabled", true);
        return 'Single';
    }

    function refreshData() {
        var defaultGroupBy = "";
        if (defaultGroupingProperties[entityName])
            defaultGroupBy = defaultGroupingProperties[entityName];
        LoadGroupByProperties(defaultGroupBy);
    }

    return {
        SetText: function (hint, text) {
            if (texts[hint]) texts[hint] = text;
        },
        SetTitles: function (leftPanel, rightPanel) {
            leftPanelTitle = leftPanel;
            rightPanelTitle = rightPanel;
        },
        SetUpEntities: function (supInterfaces, selectedEntity, entities, mode, refreshOnLoad, isMultipleObject) {
            if (supInterfaces.length == 0) return;
            supportedInterfaces = supInterfaces;
            isMultipleObjects = isMultipleObject;
            refreshTreeOnLoad = refreshOnLoad == true;
            var isFederationEnabled = $("#isFederationEnabled").val().toLowerCase();
            if (isFederationEnabled == 'true') {
                LoadSolarwindsServers(selectedEntity);
            }
            LoadAvailableEntities((selectedEntity) ? selectedEntity : "Orion.Nodes", function () {
                SW.Orion.NetObjectPicker.SetSelectedEntities(entities);
                if (mode != undefined)
                    selectionMode = mode;
            });
        },
        GetItemType: function () {
            return entityName;
        },
        SetFilter: function (filter) {
            sqlFilter = filter;
        },
        SetGridItemsFieldClientID: function (id) {
            gridItemsFieldClientID = id;
        },
        SetUnresponsiveServersNotifierClientID: function (id) {
            unresponsiveServersNotifierClientID = id;
        },
        GetSelectedEntities: function () {
            var entities = [];
            for (var i = 0; i < selectedObjects.length; i++) {
                entities.push(selectedObjects[i].uri);
            }
            return entities;
        },
        GetSelectedEntitiesWithInstanceSiteId: function () {
            var entities = [];
            for (var i = 0; i < selectedObjects.length; i++) {
                entities.push({ Uri: selectedObjects[i].uri, InstanceSiteId: selectedObjects[i].instanceSiteId });
            }
            return entities;
        },
        GetSelectedEntitiesForTestAction: function () {

            var entities = [];
            for (var i = 0; i < selectedObjects.length; i++) {
                entities.push(selectedObjects[i].uri);
            }

            if (GetSelectedEntityName() == "Orion.ContainerMembers") {
                SW.Core.Services.callWebServiceSync("/Orion/Services/AddRemoveObjects.asmx", "GetContainerMemberUris", { memberUris: entities }, function (result) {
                    entities = result;
                });
            }

            return entities;
        },
        GetSelectedEntitiesDefinitions: function () {
            return selectedObjects;
        },
        SetSelectedEntities: function (entities) {
            EmptySelectedObjects();
            if (isMultipleObjects) {
                LoadMultipleEntities(entities);
            }
            else {
                LoadEntities(entityName, entities);
            }
        },
        ClearCheckedEntities: function () {
            EmptySelectedObjects();
            var checkedNodes = tree.getChecked();
            //clear checked
            for (var i = 0; i < checkedNodes.length; i++) {
                checkedNodes[i].ui.toggleCheck(false);
            }
        },
        SetupHintForSpecificObjectEntity: null,
        init: function () {
            if (initialized)
                return;
            maxAllowedCount = parseInt(ORION.Prefs.load('MaxSelectedCount', '300'));
            initialized = true;
            LoadDefaultGroupingProperties();
            InitTree();
            $('#remove-all-button').click(RemoveAllFromSelected);
            $('#searchButton').click(DoSearch);
            $('#searchBox').keydown(function (event) {
                if (event.keyCode == '13') {
                    event.preventDefault();
                    DoSearch();
                } else {
                    $('#searchButton img').attr('src', '/Orion/images/Button.SearchIcon.gif');
                    $('#searchButton').unbind('click');
                    $('#searchButton').click(DoSearch);
                }
            });
        }
    };
}();

Ext.onReady(SW.Orion.NetObjectPicker.init, SW.Orion.NetObjectPicker);
