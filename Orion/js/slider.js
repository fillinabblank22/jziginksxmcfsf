﻿var CreateSlider = function(sliderName, textBoxName, validatorName, value, rangeFrom, rangeTo, step) {
    $('#' + sliderName).slider({
        range: "min",
        value: value,
        min: rangeFrom,
        max: rangeTo,
        stepping: step,
        slide: function(e, ui) { SetSliderText(textBoxName, validatorName, ui.value); }
    });

    $('#' + textBoxName).keypress(function() {
        setTimeout(function() { SetSliderValue(sliderName, textBoxName); }, 0);
    });
};

var SetSliderText = function(textBoxName, validatorName, value) {
    if (!$('#' + textBoxName).data('updating')) {
        $('#' + textBoxName).val(value);
        if (validatorName) ValidatorValidate($('#' + validatorName).get(0));
    }
};

var SetSliderValue = function (sliderName, textBoxName) {
    $('#' + textBoxName).data('updating', true);
    $('#' + sliderName).slider('value', $('#' + textBoxName).val());
    $('#' + textBoxName).data('updating', false);
}