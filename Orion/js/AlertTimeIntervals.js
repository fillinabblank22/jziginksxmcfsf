﻿SW.Core.namespace("SW.Core.Controls").AlertTimeIntervals = function(config) {
    this.enable = function(enable) {
        $alertTimeValue.prop('disabled', !enable);
        $alertTimeType.prop('disabled', !enable);
        
        if (typeof config.alertTimeValueValidatorId !== 'undefined') {
            var $alertTimeValueValidatorId = $('#' + config.alertTimeValueValidatorId);
            // enable/disable ASP.NET validator
            if ($alertTimeValueValidatorId.length > 0) {
                if (enable) {
                    $alertTimeValueValidatorId[0].enabled = true;
                } else {
                    ValidatorEnable($alertTimeValueValidatorId[0], enable);
                    $('#' + config.alertTimeValueId).val('');
                }
            }
        }
    };

    this.validateTimeValue = function () {
        if ($alertTimeValue.prop('disabled')) // do not validate disabled time control
            return true;
        
        var result = true;
        var timeValue = $alertTimeValue.val();
        var timeValueInt = parseInt(timeValue);
        var $timeKind = $alertTimeType.find('option:selected');
        var timeKind = parseInt($timeKind.attr('value'));

        var errorMessage = '';
        if (timeValueInt.toString() !== timeValue || $.trim(timeValue) === '')
        {
            errorMessage = '@{R=Core.Strings.2;K=WEBJS_JK0_01;E=js}';
            result = false;
        }
        else if (timeKind * timeValueInt < config.minTimeInSeconds) {
            errorMessage = String.format('@{R=Core.Strings.2;K=WEBJS_JK0_02;E=js}', config.minTimeInSeconds, config.minTimeUnit);
            result = false;
        }
        else if (timeKind * timeValueInt > config.maxTimeInSeconds) {
            errorMessage = String.format('@{R=Core.Strings.2;K=WEBJS_JK0_03;E=js}', Math.floor(config.maxTimeInSeconds / timeKind), $timeKind.text());
            result = false;
        }

        showInvalidInput($alertTimeValue, !result, errorMessage);
        return result;
    };

    this.getTimeInSeconds = function() {
        var timeValue = $alertTimeValue.val();
        var timeValueInt = parseInt(timeValue);
        var $timeKind = $alertTimeType.find('option:selected');
        var timeKind = parseInt($timeKind.attr('value'));

        return timeKind * timeValueInt;
    };

    this.setTimeInterval = function (timeIntervalInSeconds) {
        if (timeIntervalInSeconds == 0) {
            $alertTimeValue.val('0');
            $alertTimeType[0].selectedIndex = 0;
            return;
        }

        if (timeIntervalInSeconds / 3600 > 0 && timeIntervalInSeconds % 3600 == 0) {
            $alertTimeValue.val(timeIntervalInSeconds / 3600);
            $alertTimeType[0].selectedIndex = 2;
            return;
        }

        if (timeIntervalInSeconds / 60 > 0 && timeIntervalInSeconds % 60 == 0) {
            $alertTimeValue.val(timeIntervalInSeconds / 60);
            $alertTimeType[0].selectedIndex = 1;
            return;
        }

        $alertTimeValue.val(timeIntervalInSeconds);
        $alertTimeType[0].selectedIndex = 0;
    };

    this.clientValidate = function(source, args) {
        args.IsValid = self.validateTimeValue();
    };

    this.getJQueryElement = function() {
        var $result = $alertTimeValue.add($alertTimeType);
        return $result;
    };
    
    //
    // private variables
    //
    var $alertTimeValue;
    var $alertTimeType;
    var defaultTimeKindIndex;
        
    //
    // private functions
    //
    
    function showInvalidInput($element, show, message) {
        if (show) {
            $element.addClass('sw-validation-input-error');
            $element.attr('title', message);
            $("#validatorMessageField").css("visibility", "visible").text(message);
        } else {
            $element.removeClass('sw-validation-input-error');
            $element.removeAttr('title');
            $("#validatorMessageField").css("visibility", "hidden");;
        }
    }
    
    function registerEvents() {
        $($alertTimeValue)
            .change(function () { self.validateTimeValue(); })
            .keyup(function () { self.validateTimeValue(); return true; });
        $($alertTimeType).change(function () { self.validateTimeValue(); });
    }
    
    //
    // constructor
    //
    var self = this;

    if (typeof (config.defaultTimeKindIndex) === 'undefined') {
        defaultTimeKindIndex = 0;
    } else {
        defaultTimeKindIndex = parseInt(config.defaultTimeKindIndex);
    }

    // client/javascript mode
    if (config.onlyClientMode === true) {
        $alertTimeValue = $('<input type="text" style="width:30px; margin-right: .4em">');
        var select = '<select>' +
            '<option value="1">@{R=Core.Strings.2;K=AlertTimeIntervals_Seconds;E=js}</option>' +  // seconds
            '<option value="60">@{R=Core.Strings.2;K=AlertTimeIntervals_Minutes;E=js}</option>' + // minutes
            '<option value="3600">@{R=Core.Strings.2;K=AlertTimeIntervals_Hours;E=js}</option>' + // hours
            '</select>';
        $alertTimeType = $(select);
        $alertTimeType[0].selectedIndex = defaultTimeKindIndex;
        registerEvents();
    } else {    // user control mode
        $alertTimeValue = $('#' + config.alertTimeValueId);
        $alertTimeType = $('#' + config.alertTimeTypeId);
        registerEvents();
    }
};