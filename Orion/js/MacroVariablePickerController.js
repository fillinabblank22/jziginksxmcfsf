﻿SW.Core.namespace("SW.Core").MacroVariablePicker = function (config) {
    "use strict";

    // Private methods
    function renderFavorite(value) {
        if (value) {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-full.png"/></a>');
        } else {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-empty.png"/></a>');
        }
    }

    function renderAddIcon() {
        return '<a href="javascript:void(0)"><img src="/Orion/Nodes/images/icons/icon_add.gif"/></a>';
    }

    // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }
    
    function onCloseDialog() {
        grid.store.removeAll();
        groupGrid.store.removeAll();
        clearAllVariableSelection();
        updateSelectedVariablesCount();
        clearFileterValues();
        groupingComboArray.setValue('@{R=Core.Strings.2;K=LIBCODE_IT0_1; E=js}');
        groupingComboArray.selectedIndex = -1;
        groupPropertiesArray = {};

        $container.find('[data-form="enableSqlVariable"] input[type=checkbox]').prop('checked', false);
        isSqlMode = false;
        isSwqlMode = false;
        changePickerModeControlsState(false);
    };

    function closeDialog() {
        filterText = "";
        $container.dialog('close');
    }

    var insertVariable = function() {
        var variablesToInsert = '';

        if (isSqlMode || isSwqlMode) {
            if (!/^\s*select\s/.test($sqlVariable.val().toLocaleLowerCase())) {
                Ext.Msg.show({
                    title: '@{R=Core.Strings.2;K=WEBJS_IT0_5; E=js}',
                    msg: '@{R=Core.Strings.2;K=WEBJS_IT0_6; E=js}',
                    modal: true,
                    closable: false,
                    minWidth: 500,
                    buttons: { ok: 'OK' },
                    icon: Ext.MessageBox.WARNING,
                    fn: function() {}
                });
                return;
            }
        }

        if (typeof(onInsertVariables) == "function") {
            var selected = [];
            if (isSqlMode) {
                selected.push({ Name: "", Value: String.format(SQL_MACRO_FORMAT_PATTERN, $sqlVariable.val()) });
            } else if (isSwqlMode) {
                selected.push({ Name: "", Value: String.format(SWQL_MACRO_FORMAT_PATTERN, $sqlVariable.val()) });
            } else {
                for (var i = 0; i < selectedVariables.length; i++) {
                    selected.push({ Name: selectedVariables[i].DisplayName, Value: formatMacroFromVariable(selectedVariables[i]) });
                }
            }
            onInsertVariables(selected);
            closeDialog();
            return;
        }

        if (isSqlMode) {
            variablesToInsert = String.format(SQL_MACRO_FORMAT_PATTERN, $sqlVariable.val());
        } else if (isSwqlMode) {
            variablesToInsert = String.format(SWQL_MACRO_FORMAT_PATTERN, $sqlVariable.val());
        } else {
            $.each(selectedVariables, function(index, value) {
                variablesToInsert += formatMacroFromVariable(value);
            });
        }

        if ($.inArray($macroToInsertPlace.prop("tagName"), ["INPUT", "SELECT", "OPTION", "TEXTAREA", "BUTTON"]) != -1) {
            var currentText = $macroToInsertPlace.val();
            $macroToInsertPlace.val(currentText.substring(0, insertPos) + variablesToInsert.trim() + currentText.substring(insertPos));
        } else {
            var currentText = $macroToInsertPlace.text();
            $macroToInsertPlace.text(currentText.substring(0, insertPos) + variablesToInsert.trim() + currentText.substring(insertPos));
        }
        closeDialog();
    };

    function formatMacroFromVariable(variable) {
        if (variable.DefaultFormatter) {
            return String.format(LONG_MACRO_FORMAT_PATTERN, variable.PluginId, variable.ID, variable.DefaultFormatter) + ' ';
        } else {
            return String.format(SHORT_MACRO_FORMAT_PATTERN, variable.PluginId, variable.ID) + ' ';
        }
    }

    function getCaret(el) {
        if (el.selectionStart) {
            return el.selectionStart;
        } else if (document.selection) {
            el.focus();

            var r = document.selection.createRange();
            if (r == null) {
                return 0;
            }

            var re = el.createTextRange(),
                rc = re.duplicate();
            re.moveToBookmark(r.getBookmark());
            rc.setEndPoint('EndToStart', re);

            return rc.text.length;
        }
        return 0;
    }

    function setCaretPosition(el, pos) {
        if (el.setSelectionRange) {
            el.focus();
            el.setSelectionRange(pos, pos);
        }
        else if (el.createTextRange) {
            var range = el.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    function refreshGrid(filterProperty, filterPropertyValue) {
        if (setRequest(filterProperty, filterPropertyValue)) {
            dataStore.load();
        }
    }

    function refreshVatiablesTypesCombo() {
        var firstValue = variablesTypesComboArray.getStore().getAt(1);
        if (firstValue) {
            variablesTypesComboArray.setValue(firstValue.data.Value);
            updateDefaultNetObjectIfNeed();
        }
    }

    function setRequest(filterProperty, filterPropertyValue) {
        var tmpfilterText = filterText.replace(/\*/g, "%");
        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;
            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }
        var context = variablesTypesComboArray.getValue();
        if (context) {
            if (context === ALL_NETWORK_OBJECTS_VALUE || tmpfilterText.length > 0) {
                dataStore.proxy.conn.jsonData = { value: filterValueParam, search: tmpfilterText, property: filterProperty, propertyValue: filterPropertyValue };
            } else {
                dataStore.proxy.conn.jsonData = { value: [context], search: tmpfilterText, property: filterProperty, propertyValue: filterPropertyValue };
            }
            return true;
        }
        return false;
    }

    function renderSelectedVariable(selectedVariable) {
        if (selectedVariable) {
            var template;
            template = $('#variableTemplate').html();
            var html = $(_.template(template, {
                selectedVariable: selectedVariable,
                formatMacroFromVariable: formatMacroFromVariable
            }));

            html.delegate(".delete", "click", function () {
                deleteVariable($(this).closest('.selectedVariable').data('variableid'));
                refreshGridSelection();
            });
            html.delegate(".preView a", "click", function (e) {
                e.preventDefault();
                expandEditView($(this));
            });
            html.delegate(".editView a", "click", function () {
                var sender = $(this);
                collapseEditView(sender);
                updateVariableFormatValue(sender);
            });

            html.delegate(".editView select", "change", function () {
                updateVariablePreviewValue($(this));
            });

            $container.find('#selectedVariablesPlaceHolder').append(html);

            if (selectedVariable.Formatters.length > 0 && !selectedVariable.DefaultFormatter) {
                SW.Core.Services.callController(API_CONTROLLER_URL + '/FormatValue', { Formatter: selectedVariable.Formatters[0].FormatId, Value: selectedVariable.OriginalValue, DataType: selectedVariable.OutputDataType }, function (result) {
                    html.find('.previewPlaceHolder').text(result);
                });
            }
        }
    };

    function expandEditView(clickedLink) {
        clickedLink.hide();
        var variableToExpand = clickedLink.closest('.selectedVariable');
        variableToExpand.addClass('expanded');
        variableToExpand.find('.editView').show();
    };

    function collapseEditView(clickedLink) {
        var variableToCollapse = clickedLink.closest('.selectedVariable');
        variableToCollapse.removeClass('expanded');
        variableToCollapse.find('.preView a').show();
        variableToCollapse.find('.editView').hide();
    };

    function deleteVariable(variableId) {
        var variableToDelete = _.find(selectedVariables, function (obj) { return obj.ID === variableId; });
        var variableIndex = _.indexOf(selectedVariables, variableToDelete);
        selectedVariables.splice(variableIndex, 1);

        var itemToRemove = $container.find('#selectedVariablesPlaceHolder').find('[data-variableid=\'' + variableId + '\']');
        itemToRemove.next('.empty').remove();
        itemToRemove.remove();
    };

    function addVariable(variable) {
        selectedVariables.push(variable);
        renderSelectedVariable(variable);
    };

    function refreshGridSelection() {
        var rowsIds = new Array();
        $.each(selectedVariables, function (ind, obj) {
            rowsIds.push(getVariableRowIndexById(obj.ID));
        });
        grid.getSelectionModel().selectRows(rowsIds);
        updateSelectedVariablesCount();
    }

    function updateSelectedVariablesCount() {
        $container.find('#selectedVariablesCountHolder').text(String.format('@{R=Core.Strings.2;K=WEBJS_IT0_2; E=js}', selectedVariables.length));
    }

    function getVariableRowIndexById(id) {
        for (var i = 0; i < grid.store.data.items.length; i++) {
            if (grid.getStore().getAt(i).data.ID == id) {
                return i;
            }
        }
        return null;
    }

    function isVariableAlreadySelected(variable) {
        return _.any(selectedVariables, function (obj) { return obj.ID === variable.ID; });
    }

    function clearAllVariableSelection() {
        selectedVariables.length = 0;
        $container.find('#selectedVariablesPlaceHolder').empty();
    }

    function updateVariablePreviewValue(sender) {
        var newFormater = sender.find(':selected').val();
        var selectedVariableElem = sender.closest('.selectedVariable');
        var currentVariableId = selectedVariableElem.data('variableid');
        var currentVariable = _.find(selectedVariables, function (variable) {
            return variable.ID === currentVariableId;
        });

        if (newFormater != ORIGINAL_VALUE_MACRO_FORMATTER) {
            SW.Core.Services.callController(API_CONTROLLER_URL + '/FormatValue', { Formatter: newFormater, Value: currentVariable.OriginalValue, DataType: currentVariable.OutputDataType }, function(result) {
                selectedVariableElem.find('.previewPlaceHolder').text(result);
            });
        } else {
            selectedVariableElem.find('.previewPlaceHolder').text(currentVariable.OriginalValue);
        }
    }

    function updateVariableFormatValue(sender) {
        var selectedVariableElem = sender.closest('.selectedVariable');
        var currnetVariableId = selectedVariableElem.data('variableid');
        var selectedFormat = selectedVariableElem.find('select').find(':selected').val();
        var currentVariable = _.find(selectedVariables, function (variable) {
            return variable.ID === currnetVariableId;
        });
        if (currentVariable && currentVariable.DefaultFormatter !== selectedFormat) {
            currentVariable.DefaultFormatter = selectedFormat;
            selectedVariableElem.find('.variableFormatPlaceHolder').text(formatMacroFromVariable(currentVariable));

            currentVariable.FormattedValue = selectedVariableElem.find('.previewPlaceHolder').text();
            selectedVariableElem.find('.formattedValuePlaceHolder').text(currentVariable.FormattedValue);
        }
    }

    function showNetObjectPicker() {
        SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: currentNetObject.entityType }], currentNetObject.entityType, [currentNetObject.uri], 'Single');
        $('#netObjectPickerPlaceHolder').prependTo('#insertVariableNetObjectPickerDlg');

        $('#insertVariableNetObjectPickerDlg').dialog({
            position: ['middle', 100],
            width: 700,
            height: 'auto',
            modal: true,
            draggable: true,
            title: '@{R=Core.Strings.2;K=WEBJS_IT0_3; E=js}',
            show: { duration: 0 }
        });
    }

    function changeNetObject() {
        var selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntitiesDefinitions();
        if (selectedEntities.length > 0 && selectedEntities[0].uri !== currentNetObject.uri) {
            currentNetObject = selectedEntities[0];

            var context = variablesTypesComboArray.getValue();
            if (context.hasOwnProperty('EntityUri')) {
                context.EntityUri = currentNetObject.uri;
                currentNetObject.entityType = context.EntityType;
            }

            renderCurrentNetObject();
            refreshGrid(currentFilterProperty, currentFilterPropertyValue);
        }
    }

    function renderCurrentNetObject() {
        var htmlNetObject = String.format('<img style="vertical-align:middle" src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;status={0}&amp;size=small"/><span>{1}</span>', currentNetObject.status, currentNetObject.fullName);
        $('#currentNetObjectPlaceHolder').html(htmlNetObject);
    }

    function closeNetObjectPicker() {
        $('#insertVariableNetObjectPickerDlg').dialog('close');
    };

    function updateDefaultNetObjectIfNeed() {
		var context = variablesTypesComboArray.getValue();
		if (context.hasOwnProperty('EntityUri')) {
		    if (currentNetObject && currentNetObject.entityType == context.EntityType) {
		        context.EntityUri = currentNetObject.uri;
		    } else {
				LoadTopOneEntity(context.EntityType, function(result) {
					currentNetObject = { uri: result.Uri, status: result.Status, fullName: result.FullName, entityType : context.EntityType };
		            context.EntityUri = result.Uri;
		            renderCurrentNetObject();
				}, function() {
					$('#changeNetObjectLink').hide();
					$('#currentNetObjectPlaceHolder').html(String.format('@{R=Core.Strings.2;K=WEBJS_VL0_6; E=js}', context.DisplayName));
				});
		    }
		    $('#changeNetObjectSection').show();
		} else {
		    $('#changeNetObjectSection').hide();
		}
    }
	
	function LoadTopOneEntity(entType, onSuccess, onFailure) {
		ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "LoadTopOneEntityByEntityType", { entityType: entType}, function(result) {
            if (result) {
				if (typeof onSuccess === "function")
					onSuccess(result);
            } else {
				if (typeof onFailure === "function")
					onFailure();
            }
		});
	}

    function renderGroup(value, meta, record) {
        if (groupingComboArray.getStore().data.items[groupingComboArray.selectedIndex].data.GroupProperty == 'IsFavorite') {
            return '<img src = \"/Orion/images/Reports/Star-small.png\" style=\" padding-right:3px;\" >' + '@{R=Core.Strings.2;K=WEBJS_OM0_02; E=js}' + ' (' + record.data.Cnt + ')';
        }

        var displayString = record.data.DisplayName + ' (' + record.data.Cnt + ')';

        return '<span ext:qtitle="" ext:qtip="' + encodeHTML(displayString) + '">' + encodeHTML(displayString) + '</span>';
    }

    function refreshGroupGrid(reloadGrid) {
        repearGroupComboSelection();
        var propertyValue, property;
        var groupItem = groupingComboArray.getStore().data.items[groupingComboArray.selectedIndex];
        if (groupItem) {
            propertyValue = groupItem.data.GroupValue;
            property = groupItem.data.GroupProperty;

            var context = variablesTypesComboArray.getValue();
            if (context === ALL_NETWORK_OBJECTS_VALUE) {
                context = filterValueParam;
            } else {
                context = [context];
            }

            var groupGridElement = Ext.getCmp('macroGroupingGrid');
            groupGridElement.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
            if (groupPropertiesArray[property]) {
                populateGrouppingGrid(context, property, propertyValue);
            } else {
            	// Load all grouping values for current property  
            	SW.Core.Services.callController(API_CONTROLLER_URL + '/GetVariablesGroupValues', { value: context, property: property, propertyValue: propertyValue }, function (result) {
                    groupPropertiesArray[property] = [];
                    $.each(result.DataTable.Rows, function (index, obj) {
                        groupPropertiesArray[property].push([obj[0], obj[1]]);
                    });
		            populateGrouppingGrid_OnSuccess(result, reloadGrid);
                }, function () { groupGridElement.getEl().unmask(); });
            }
        }
    }

    function repearGroupComboSelection() {
        if (groupingComboArray.selectedIndex === -1) {
            var value = groupingComboArray.getValue();
            var record = groupingComboArray.findRecord(groupingComboArray.displayField, value);
            var recordIndex = groupingComboArray.store.indexOf(record);
            if (recordIndex > 0) {
                groupingComboArray.setValue(record.data[groupingComboArray.valueField]);
                groupingComboArray.setRawValue(record.data[groupingComboArray.displayField]);
                groupingComboArray.selectedIndex = recordIndex;
            }
        }
    }

    function populateGrouppingGrid(context, property, propertyValue) {
	    SW.Core.Services.callController(API_CONTROLLER_URL + '/GetVariablesGroupValues', { value: context, property: property, propertyValue: propertyValue },
		    function(result) { populateGrouppingGrid_OnSuccess(result, true); },
			function () { Ext.getCmp('macroGroupingGrid').getEl().unmask(); });
    }

    function populateGrouppingGrid_OnSuccess(result, reloadGrid) {
        var groupGridElement = Ext.getCmp('macroGroupingGrid');
        grouppingGridStoreRecords.length = 0;
        $.each(result.DataTable.Rows, function (index, obj) {
            grouppingGridStoreRecords.push([obj[0], obj[1], obj[2]]);
        });

        groupingGridStore.loadData(grouppingGridStoreRecords);

	    if (groupGrid.store.data.items.length > 0) {
		    // Select current filter property
		    var indexToSelect = -1;
		    for (var i = 0; i < groupGrid.store.data.items.length; i++) {
			    if (groupGrid.getStore().getAt(i).data.Value == currentFilterPropertyValue) {
				    indexToSelect = i;
				    break;
			    }
		    }

		    if (indexToSelect != -1) {
		        currentFilterPropertyValue = groupGrid.getStore().getAt(indexToSelect).data.Value;
		        groupGrid.getSelectionModel().selectRows([indexToSelect]);
		    }
	    }

		if (reloadGrid) {
			currentFilterProperty = groupingComboArray.getStore().data.items[groupingComboArray.selectedIndex].data.GroupProperty;
			refreshGrid(currentFilterProperty, currentFilterPropertyValue);
	    }
	    groupGridElement.getEl().unmask();
    }

    function clearFileterValues() {
        currentFilterProperty = '';
        currentFilterPropertyValue = '';
    }

    function changePickerModeControlsState(state) {
        if (state) {
            $('#queryMode').show();
            $('#simpleMode').hide();
            grid.getColumnModel().setHidden(0, true);
            grid.getColumnModel().setHidden(8, false);
        } else {
            $('#simpleMode').show();
            $('#queryMode').hide();
            grid.getColumnModel().setHidden(8, true);
            grid.getColumnModel().setHidden(0, false);
        }
    }

    function setMacroContexts(contexts) {
        var variablesTypesData = [];
        variablesTypesData.push([ALL_NETWORK_OBJECTS_DISPLAY_NAME, ALL_NETWORK_OBJECTS_VALUE]);
        $.each(contexts, function (counter, contextObj) {
            if (this.hasOwnProperty("EntityType") && !config.entityType)
                config.entityType = this.EntityType;
			
			if (this.hasOwnProperty("EntityType") && this.hasOwnProperty("EntityUri") && !this.EntityUri) {
                LoadTopOneEntity(this.EntityType, function(result) {
					currentNetObject = { uri: result.Uri, status: result.Status, fullName: result.FullName, entityType : config.entityType };
		            contextObj.EntityUri = result.Uri;
				});
			}
			
            variablesTypesData.push([this.DisplayName, this]);
        });

        if (variablessTypesDataStore) {
            variablessTypesDataStore.loadData(variablesTypesData);
        } else {
            variablessTypesDataStore = new Ext.data.SimpleStore({
                fields: ['Name', 'Value'],
                data: variablesTypesData
            });
        }
        filterValueParam = contexts;
    };

    function init() {
        $sqlVariable = $container.find('[data-form="sqlVariable"]');
        $('#' + config.cancelButtonID).on('click', closeDialog);
        $('#' + config.insertVariableButtonID).on('click', insertVariable);
        $container.find('#removeAllSelections').on('click', function(e) {
            e.preventDefault();
            clearAllVariableSelection();
            refreshGridSelection();
        });

        $container.find('#changeNetObjectLink').on('click', function (e) {
            e.preventDefault();
            showNetObjectPicker();
        });

        $container.find('#btnNetObjectChange').on('click', function (e) {
            e.preventDefault();
            changeNetObject();
            closeNetObjectPicker();
        });

        $container.find('#btnNetObjectCancel').on('click', function (e) {
            e.preventDefault();
            closeNetObjectPicker();
        });

        $container.find('[data-form="enableSqlVariable"] input[type=checkbox]').change(function () {
            var state = this.checked;
            changePickerModeControlsState(state);
            if (state) {
                isSqlMode = $("#sqlModeSelector")[0].checked;
                isSwqlMode = !isSqlMode;
                grid.getSelectionModel().clearSelections();
                if ($sqlVariable.val() === SQL_VARIABLE_DEFAULT) {
                    setCaretPosition($sqlVariable.get(0), SQL_VARIABLE_DEFAULT.length);
                }
            } else {
                isSqlMode = false;
                isSwqlMode = false;
                refreshGridSelection();
            }
        });
        
        $container.find('#queryMode input[type=radio]').change(function () {
            isSqlMode = $("#sqlModeSelector")[0].checked;
            isSwqlMode = !isSqlMode;
        });

        selectorModel = new Ext.grid.CheckboxSelectionModel({ checkOnly: true, header: '<div id="checker" class="x-grid3-hd-checker" style="display:none">&nbsp;</div>' });

        var gridColumnsModel = [
            selectorModel,
            { header: 'ID', hidden: true, hideable: false, sortable: false, dataIndex: 'ID' },
            { header: 'PluginId', hidden: true, hideable: false, sortable: false, dataIndex: 'PluginId' },
            { header: 'Formatters', hidden: true, hideable: false, sortable: false, dataIndex: 'Formatters' },
            { header: 'DefaultFormatter', hidden: true, hideable: false, sortable: false, dataIndex: 'DefaultFormatter' },
            { header: 'OriginalValue', hidden: true, hideable: false, sortable: false, dataIndex: 'OriginalValue' },
            { header: 'OutputDataType', hidden: true, hideable: false, sortable: false, dataIndex: 'OutputDataType' },
            { id: 'isFavorite', header: '<img src="/Orion/images/Reports/Star-empty.png"/>', width: 28, sortable: true, dataIndex: 'IsFavorite', renderer: renderFavorite },
            { header: '@{R=Core.Strings;K=WEBJS_PS0_1; E=js}', hidden: true, width: 32, sortable: false, dataIndex: 'AddInSqlQuery', renderer: renderAddIcon, },
            { header: '@{R=Core.Strings.2;K=WEBJS_VL0_2; E=js}', hideable: false, width: 235, sortable: true, dataIndex: 'DisplayName', renderer: renderString },
            { header: '@{R=Core.Strings.2;K=WEBJS_VL0_3; E=js}', hideable: false, width: 235, sortable: true, dataIndex: 'FormattedValue', renderer: renderString }
        ];
        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'DisplayName', mapping: 1 },
            { name: 'PluginId', mapping: 2 },
            { name: 'Formatters', mapping: 3 },
            { name: 'FormattedValue', mapping: 4 },
            { name: 'DefaultFormatter', mapping: 5 },
            { name: 'OriginalValue', mapping: 6 },
            { name: 'OutputDataType', mapping: 7 },
            { name: 'IsFavorite', mapping: 8 }
        ];
        
        dataStore = new ORION.WebApiStore(API_CONTROLLER_URL + '/GetAvailableMacroVariables', gridStoreColumns, 'DisplayName');
        grid = new Ext.grid.GridPanel({
            id: 'macroVariableGrid',
            region: 'center',
            viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings.2;K=WEBJS_VL0_5; E=js}" },
            store: dataStore,
            columns: gridColumnsModel,
            baseParams: { start: 0, limit: 20 },
            sm: selectorModel,
            enableHdMenu: false,
            autoScroll: true,
            loadMask: true,
            height: 360,
            listeners: {
                cellmousedown: function (macroGrid, rowIndex, columnIndex) {
                    var fieldName = macroGrid.getColumnModel().getDataIndex(columnIndex);
                    var selectedVariable = macroGrid.getStore().getAt(rowIndex).data;
                    if (fieldName == "") {
                        if (isVariableAlreadySelected(selectedVariable)) {
                            deleteVariable(selectedVariable.ID);
                        } else {
                            addVariable(selectedVariable);
                        }
                        refreshGridSelection();
                    }
                    if (fieldName == 'IsFavorite') {
                        var record = macroGrid.getStore().getAt(rowIndex);
                        SW.Core.Services.callController(API_CONTROLLER_URL + '/ChangeFavoriteState',
                        {
                            VariableId: selectedVariable.ID,
                            PluginId: selectedVariable.PluginId,
                            IsFavorite: selectedVariable.IsFavorite
                        },function (result) {
                            if (result) {
                                record.set(fieldName, !selectedVariable.IsFavorite);
                                record.commit();
                            }
                        });
                    }
                    if (fieldName == 'AddInSqlQuery') {
                        var currentSqlMacro = $sqlVariable.val();
                        var variableToInsertPosition = getCaret($sqlVariable.get(0));
                        var variableToInsert = formatMacroFromVariable(selectedVariable);
                        $sqlVariable.val(currentSqlMacro.substring(0, variableToInsertPosition) + variableToInsert + currentSqlMacro.substring(variableToInsertPosition));
                        setCaretPosition($sqlVariable.get(0), variableToInsertPosition + variableToInsert.length);
                    }
                    return false;
                }
            },
            cls: 'panel-no-border'
        });

        grid.store.on('load', function () {
            if (!isSqlMode)
                refreshGridSelection();
            $("#checker").click(function () {
                if (grid.getSelectionModel().selections.items.length == 0) {
                    $.each(grid.store.data.items, function () {
                        var item = this;
                        if (isVariableAlreadySelected(item.data)) {
                            deleteVariable(item.data.ID);
                        }
                    });
                } else {
                    $.each(grid.getSelectionModel().selections.items, function () {
                        var item = this;
                        if (!isVariableAlreadySelected(item.data)) {
                            addVariable(item.data);
                        }
                    });
                }
                if (!isSqlMode)
                    refreshGridSelection();
            });
        });

        if (config.macroContexts)
            setMacroContexts(config.macroContexts);

        variablesTypesComboArray = new Ext.form.ComboBox(
        {
            id: 'context' + Ext.id(),
            fieldLabel: 'Value',
            displayField: 'Name',
            valueField: 'Value',
            store: variablessTypesDataStore,
            mode: 'local',
            triggerAction: 'all',
            typeAhead: true,
            forceSelection: true,
            autoSelect: true,
            multiSelect: false,
            editable: false,
            allowBlank: false,
            width: 175,
            listeners: {
            	'select': function () {
                    updateDefaultNetObjectIfNeed();
                    refreshGroupGrid(true);
                }
            }
        });

        groupingDataStore = new ORION.WebApiStore(
            API_CONTROLLER_URL + '/GetVariablesGroupProperties',
            [
                { name: 'GroupProperty', mapping: 0 },
                { name: 'DisplayName', mapping: 1 },
                { name: 'GroupValue', mapping: 2 }
            ]);

        groupingDataStore.on('load', function () {
            refreshGroupGrid(false);
        });
        groupingDataStore.load();
       
        groupingComboArray = new Ext.form.ComboBox({
            id: 'macroGroupCombo',
            mode: 'local',
            fieldLabel: 'DisplayName',
            displayField: 'DisplayName',
            valueField: 'GroupProperty',
            store: groupingDataStore,
            triggerAction: 'all',
            value: '@{R=Core.Strings.2;K=LIBCODE_IT0_1; E=js}',
            typeAhead: true,
            forceSelection: true,
            selectOnFocus: false,
            multiSelect: false,
            editable: false,
            width: 175,
            listeners: {
                'select': function () {
                    clearFileterValues();
                    refreshGroupGrid(true);
                }
            }
        });

        groupingGridStore = new Ext.data.SimpleStore({
            fields: ['Value', 'DisplayName', 'Cnt'],
            data: grouppingGridStoreRecords
        });

        groupGrid = new Ext.grid.GridPanel({
            id: 'macroGroupingGrid',
            store: groupingGridStore,
            cls: 'hide-header',
            columns: [
                { id: 'Value', width: 185, editable: false, sortable: false, dataIndex: 'Value', renderer: renderGroup }
            ],
            selModel: new Ext.grid.RowSelectionModel(),
            autoScroll: true,
            heigth: 300,
            loadMask: true,
            layout: 'fit',
            listeners: {
            	cellclick: function (senderGrid, rowIndex, cell, e) {
                    currentFilterProperty = groupingComboArray.getStore().data.items[groupingComboArray.selectedIndex].data.GroupProperty;
                    currentFilterPropertyValue = senderGrid.store.getAt(rowIndex).data.Value;
                    refreshGrid(currentFilterProperty, currentFilterPropertyValue);
                }
            }
        });

        var groupByTopPanel = new Ext.Panel({
            id: 'macroGroupByTopPanel',
            region: 'center',
            split: false,
            heigth: 50,
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [
                new Ext.form.Label({ text: "@{R=Core.Strings.2;K=WEBJS_VL0_1; E=js}" }),
                variablesTypesComboArray,
                new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }),
                groupingComboArray],
            cls: 'panel-no-border panel-bg-gradient'
        });

        var navPanel = new Ext.Panel({
            id: 'macroNavPanel',
            width: 190,
            height: 360,
            region: 'west',
            style: {
                borderRight: '1px solid #d0d0d0'
            },
            split: false,
            anchor: '0 0',
            collapsible: false,
            viewConfig: { forceFit: true },
            items: [groupByTopPanel, groupGrid],
            cls: 'panel-no-border'
        });

        navPanel.on("bodyresize", function () {
            groupGrid.setSize(navPanel.getSize().width, 290);
        });


        var mainGridPanel = new Ext.Panel({
            id: 'macroMainGrid',
            height: 360,
            region: 'center',
            split: true,
            layout: 'border',
            collapsible: false,
            items: [navPanel, grid]
        });

        mainGridPanel.render(config.renderTo);

        var serch = new Ext.ux.form.SearchField({
            id: 'macroSearchField',
            emptyText: "@{R=Core.Strings.2;K=WEBJS_VL0_4; E=js}",
            store: dataStore,
            width: 280
        });
        var searchField = new Ext.Panel({
            id: 'macroSearchFieldPanel',
            region: 'center',
            split: false,
            heigth: 50,
            collapsible: false,
            layout: 'table',
            layoutConfig: {
                columns: 1
            },
            items: [serch],
            cls: 'panel-no-border'
        });
        searchField.render('macroSearchPanel');
    };

    var onInsertVariables;

    // Public methods
    this.BindPicker = function () {
    };

    this.DisableInsertVariableButtons = function () {
        $('[data-macro]').addClass('sw-btn-disabled');
        $('[data-macro]').off('click', self.showDialog);
    };

    this.SetOnInsertHandler = function(handler) {
        onInsertVariables = handler;
    };
    
    this.EnableInsertVariableButtons = function () {
        $('[data-macro]').on('click', self.showDialog);
    };

    this.showDialog = function () {
        $macroToInsertPlace = $('[data-form=\'' + $(this).data('macro') + '\']');
        insertPos = getCaret($macroToInsertPlace.get(0));
        $container.dialog({
            dialogClass: "macro-variable-picker-dialog",
            modal: true,
            position: ['middle', 100],
            title: '@{R=Core.Strings.2;K=WEBJS_IT0_1; E=js}',
            width: 740,
            heigh: 'auto',
            minWidth: 740,
            close: onCloseDialog
        });
        if ($container.find("#" + config.renderTo).children().length == 0) {
            init();
        }

        refreshVatiablesTypesCombo();
        refreshGrid('', '');
        refreshGroupGrid(false);

        $sqlVariable.val(SQL_VARIABLE_DEFAULT);
    };

    this.SetMacroContexts = function (contexts) {
        config.macroContexts = contexts;
        setMacroContexts(contexts);
    };

    this.ParseMacroVariable = function (template) {
        var result;
        SW.Core.Services.callControllerSync(API_CONTROLLER_URL + '/Parse',
            {
                "context": config.macroContexts,
                "template": template
            },
            function (response) {
                result = response;
            });
        return result;
    };

    // Constructor

    var self = this;
    var $container = $('#' + config.containerID);
    var $macroToInsertPlace = null;
    var insertPos = 0;
    var selectedVariables = [];
    var selectorModel;
    var variablessTypesDataStore;
    var variablesTypesComboArray;

    var groupingComboArray;
    var groupingDataStore;

    var groupGrid;
    var groupingGridStore;
    var currentFilterProperty = '';
    var currentFilterPropertyValue = '';
    var groupPropertiesArray = {};
    var grouppingGridStoreRecords = [];

    var isSqlMode = false;
    var isSwqlMode = false;
    var $sqlVariable = null;

    var grid;
    var dataStore;
    var currentNetObject = null;

    var SHORT_MACRO_FORMAT_PATTERN = '${{N={0};M={1}}}';
    var LONG_MACRO_FORMAT_PATTERN = '${{N={0};M={1};F={2}}}';
    var API_CONTROLLER_URL = '/api/MacroVariablePicker';
    var ALL_NETWORK_OBJECTS_DISPLAY_NAME = '@{R=Core.Strings.2;K=WEBJS_IT0_4; E=js}';
    var ALL_NETWORK_OBJECTS_VALUE = 'allNetworkObjects';
    var SQL_VARIABLE_DEFAULT = 'SELECT ';
    var SQL_MACRO_FORMAT_PATTERN = '${{SQL: {0}}}';
    var SWQL_MACRO_FORMAT_PATTERN = '${{N=SWQL;M={0}}}';
    var ORIGINAL_VALUE_MACRO_FORMATTER = 'OriginalValue';
}
