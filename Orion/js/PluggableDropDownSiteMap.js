/// <reference path="~/Orion/js/jquery.js" />
$(function () {
    window.isBreadCrumbInitialized = true;
    var isClicked = false;
    var submenuHeight = 0;
    var $iframe = $('<iframe src="" class="tooltip-iframe" scroll="none" frameborder="0"></iframe>');

    var Images = Object.freeze({
        arrow: "/Orion/images/breadcrumb_arrow.gif",
        dropDownBg: "/Orion/images/breadcrumb_dd_bg.gif"
    });

    var TemplateSibling = Object.freeze({
        IconUrl: "data:image/gif;base64,",
        ElementId: "TemplateSiblingElementId",
        Text: "TemplateSiblingText",
        Title: "TemplateSiblingTitle",
        Url: "TemplateSiblingUrl",
    });

    $("img.bc_itemimage").hover(function () {
        $(this).attr({ src: Images.dropDownBg });
    },
    function () {
        if (!isClicked) {
            $(this).attr({ src: Images.arrow });
        }
    });

    $("img.bc_itemimage").click(function (e) {
        e.stopPropagation();

        var theImage = $(this);
        var submenu = theImage.next("ul.bc_submenu");

        $("ul.bc_submenu").each(function (idx) {
            if (this.id != submenu.attr("id")) {
                $(this).hide();
                $(this).prev("img.bc_itemimage").attr({ src: Images.arrow });
            }
        });

        if (submenu.is(":visible")) {
            theImage.attr({ src: Images.arrow });
            theImage.parent().removeClass("bc_itemselected");
            theImage.prev("a").removeClass("bc_refselected");
            submenu.hide();
            
            isClicked = false;
        }
        else {
            function showDropDown() {
                theImage.attr({ src: Images.dropDownBg });
                theImage.parent().addClass("bc_itemselected");
                theImage.prev("a").addClass("bc_refselected");
                isClicked = true;
                submenuHeight = submenu.height();

                submenu.show();
            };
            var template = $("#dp-submenu-template");
            var submenuLoaded = submenu.attr("loaded");
            if (template.length > 0 && !submenuLoaded) {
                var siteMapKey = template.attr("siteMapKey");
                var siteMapIndex = template.attr("siteMapIndex");
                var siteMapProviderName = template.attr("siteMapProviderName");
                var currentNodeKey = template.attr("currentNodeKey");
                var currentDataId = template.attr("currentDataId");

                var parentNodeKey = submenu.attr("parentNodeKey");
                var parentNodeDataId = submenu.attr("parentNodeDataId");
                var childNodeDataId = submenu.attr("childNodeDataId");


                var data = "{siteMapKey:'" + siteMapKey
                + "', siteMapIndex:'" + siteMapIndex
                + "', siteMapProviderName:'" + siteMapProviderName
                + "', currentPageNodeKey:'" + currentNodeKey
                + "', currentPageNodeDataId:'" + currentDataId
                + "', parentNodeKey:'" + parentNodeKey
                + "', parentNodeDataId:'" + parentNodeDataId
                + "', childNodeDataId:'" + childNodeDataId + "'}";

                var htmlTemplate = template.html();

                function onGetSiblingsSuccess(result) {
                    for (var i = 0; i < result.d.length; i++) {
                        var sibling = result.d[i];
                        if (sibling.ElementId == null) {
                            sibling.ElementId = String.format("{0}-{1}", submenu[0].id, i);
                        }
                        if (sibling.Title == null) {
                            sibling.Title = sibling.Text;
                        }
                        var html = htmlTemplate.trim();
                        html = html.replace(TemplateSibling.ElementId, sibling.ElementId);
                        html = html.replace(TemplateSibling.IconUrl, sibling.IconUrl);
                        html = html.replace(TemplateSibling.Text, sibling.Text);
                        html = html.replace(TemplateSibling.Title, sibling.Title);
                        html = html.replace(TemplateSibling.Url, sibling.Url);
                        submenu.append(html);
                    }
                    submenu.attr("loaded", "true");
                    showDropDown();
                }

                function onGetSiblingsError(error) {
                    console.error(error);
                }

                $.ajax({
                    type: 'Post',
                    url: '/Orion/Services/SiteMapSiblings.asmx/GetSiblings',
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: onGetSiblingsSuccess,
                    error: onGetSiblingsError
                });
            }
            else {
                showDropDown();
            }
        }
    });

    $(this).click(function () {
        $("li.bc_item").removeClass("bc_itemselected");
        $("a").removeClass("bc_refselected");
        
        $("ul.bc_submenu").hide();
        $("img.bc_itemimage").attr({ src: Images.arrow });
        isClicked = false;
    });

});
