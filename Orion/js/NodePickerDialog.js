﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');


SW.Orion.NodePickerDialog = function () {
    ORION.prefix = "Orion_NodePickerDialog_";

    var self = this,
        $nodePickerDialog = $('#nodePickerDialog'),
        $okButton = null,
        $cancelButton = null,
        dialogResponse;

    jQuery.extend(self, {
        openDialog: openDialog,
        selectedNode: null,
        filter: null
    });

    activate();

    return self;

    function activate() {
        // OK button
        $okButton = $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_IB0_31;E=js}', { type: 'primary', id: 'selectNodeOk' }))
            .appendTo("#nodePickerDialogButtons")
            .addClass('sw-btn-disabled')
            .click(function() {
                if (!self.selectedNode) {
                    Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: '@{R=Core.Strings;K=WEBJS_IB0_33;E=js}', minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                dialogResponse.resolve(self.selectedNode);
                hideDialog();
            });

        // Cancel button
        $cancelButton = $(SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}', { type: 'secondary', id: 'selectNodeCancel' }))
            .appendTo("#nodePickerDialogButtons")
            .click(function() {
                self.selectedNode = null;
                dialogResponse.reject();
                hideDialog();
            });
    }

    function hideDialog() {
        destroy();
    }

    function openDialog() {
        var node = self.selectedNode,
            lastSelectedUri = node && node.uri && node.uri != '' ? [node.uri] : [];

        setUpNetObjectPicker(
            lastSelectedUri,
            function (n) {
                self.selectedNode = n;
                $okButton[n && n.valid ? 'removeClass' : 'addClass']('sw-btn-disabled');
            },
            self.filter
        );

        return $.Deferred(function(dfd) {
            dialogResponse = dfd;

            $nodePickerDialog
                .removeClass('offPage')
                .dialog({
                    closeOnEscape: false,
                    position: 'center',
                    width: '890',
                    height: 'auto',
                    title: 'res:Select a Node',
                    modal: true,
                    close: hideDialog
                });
        });
    }

    function setUpNetObjectPicker(selected, onSelect, filter) {
        SW.Orion.NetObjectPicker.SetText('toomany.title', 'res:Too many title');
        SW.Orion.NetObjectPicker.SetText('toomany.text', 'res:Too many text');

        if (filter && filter != '') {
            SW.Orion.NetObjectPicker.SetFilter(filter);
        }

        SW.Orion.NetObjectPicker.updateSourceNameDelegate = function(name) {
            var netObject = SW.Orion.NetObjectPicker.GetSelectedEntities() || [];

            if (netObject.length) {
                onSelect({ name: name, uri: netObject[0], valid: true });
            } else {
                onSelect(null);
            }
        };

        var sel = selected && $.isArray(selected) ? selected : [];
        SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: 'Orion.Nodes' }], 'Orion.Nodes', sel, 'Single');
    }

    function destroy () {
        SW.Orion.NetObjectPicker.SetSelectedEntities([]);
        SW.Orion.NetObjectPicker.updateSourceNameDelegate = null;
        $nodePickerDialog.dialog('destroy');
    };
};
