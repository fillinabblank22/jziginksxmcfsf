﻿SW = SW || {};

SW.OidPicker = function () {
    //Extjs components
    var tree,
        selectionHolder,
        mainPanel,
        oidPickerStore,
        oidPickerProxy;

    //Service endpoints
    var oidPickerWizardUrl,
        testOidUrl,
        loadNodeUrl,
        oidPickerServiceUrl,
        searchUrl,
        getChildOidsUrl,
        cancelSearchUrl;

    //Result and validation
    var oidPickerSelection = [];
    var isValidOid = false;
    var oidPickerPropertyIsTable = false;
    var firstXRowsCount = 20;

    //Initialization values
    var oidPickerNodeId;
    var oidPickerRenderTo;

    //Variables for searching
    var oidPickerSearchBarEmptyText = '@{R=Core.Strings;K=WEBJS_GK0_05; E=js}';
    var oidPickerIsSearching = false;
    var searchCriteria;

    //Reference for making the object observable
    var self;

    function xinit(options) {
        //Clear previous seleciton (if any)
        oidPickerSelection.length = 0;
        SW.Core.Observer.makeObserver(this);
        //Store reference to be able to trigger the observer
        self = this;
        //Initialization of service endpoints
        oidPickerServiceUrl = options.serviceUrl;
        oidPickerWizardUrl = options.wizardUrl;
        searchUrl = oidPickerServiceUrl + '/Search';
        getChildOidsUrl = oidPickerServiceUrl + '/GetChildren';
        cancelSearchUrl = oidPickerServiceUrl + '/AbortQuery';
        testOidUrl = oidPickerWizardUrl + '/TestOID';
        loadNodeUrl = oidPickerWizardUrl + '/LoadNode';

        oidPickerNodeId = options.nodeId;
        oidPickerRenderTo = options.renderTo;
        oidPickerPropertyIsTable = options.propertyIsTable;

        Ext42.require([
            'Ext.data.*',
            'Ext.grid.*',
            'Ext.tree.*'
        ]);

        //The proxy, enhanced with json capabilities.
        oidPickerProxy = Ext42.create('jsonProxy', {
            url: getChildOidsUrl
        });

        //The store, loads data using the proxy and forwards it to the tree widget.
        oidPickerStore = Ext42.create('Ext.data.TreeStore', {
            model: 'Oid',
            proxy: oidPickerProxy,
            listeners: {
                //Needed for propert asmx JSON communication. Here we create a JSON object for
                //handling pagination/searching and selection, and use it in the proxy to send
                //it back to the service.
                beforeload: function (store, operation, eOpts) {
                    store.proxy.jsonData = {
                        "node": operation.node.data.id,
                        "searchCriteria": searchCriteria
                    };
                },
                //By default we use this URL to load children when clicking on nodes. When we search,
                //we set the proxy's URL to another service endpoint which serves the search result.
                //The load event fires after the data has been fetched from the server (after the search
                //is finished). We reset the proxy's URL to load child nodes once again.
                load: function (me, node, records) {
                    oidPickerStore.proxy.url = getChildOidsUrl;
                }
            }
        });

        tree = Ext42.create('oidpicker', {
            id: 'oidPickerTree',
            width: 600,
            height: 600,
            store: oidPickerStore,
            //The toolbar of the panel, we have multiple rows, thus the lengthy definition.
            tbar: new Ext42.create('Ext.Panel',
                {
                    cls: 'oidPickerToolbar',
                    items: [
                        {
                            height: 25,
                            xtype: 'toolbar',
                            items: [
                                {
                                    xtype: 'label',
                                    text: '@{R=Core.Strings;K=WEBJS_GK0_31; E=js}',
                                    cls: 'oidPickerLabel'
                                },
                                '->',
                                {
                                    xtype: 'textfield',
                                    emptyText: oidPickerSearchBarEmptyText,
                                    id: 'searchBar',
                                    width: 300,
                                    style: {
                                        //Interference when showing in dialog
                                        zIndex: 1000000
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: '<img src="/Orion/Images/OidPicker/Clear_button.gif" alt="@{R=Core.Strings;K=WEBJS_GK0_04; E=js}"/>',
                                    id: 'clearSearchButton',
                                    cls: 'clearSearchButton',
                                    hidden: true,
                                    listeners: {
                                        click: function () {
                                            Ext42.getCmp('clearSearchButton').hide();
                                            //The same as for the search button, except that we reset the whole tree this time.
                                            oidPickerIsSearching = false;
                                            Ext42.getCmp('searchResultToolbar').hide();
                                            oidPickerStore.getRootNode().removeAll();
                                            $("#searchBar").find("input").val('');
                                            oidPickerProxy.url = getChildOidsUrl;
                                            oidPickerStore.load();
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: '<img src="/Orion/Images/OidPicker/Search_button.gif" alt="@{R=Core.Strings;K=WEBJS_GK0_03; E=js}"/>',
                                    id: 'searchButton',
                                    cls: 'searchButton',
                                    listeners: {
                                        click: function (button) {
                                            searchOid(getSearchText());
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'toolbar',
                            id: 'searchResultToolbar',
                            hidden: true,
                            items: [
                                {
                                    xtype: 'label',
                                    width: '100%',
                                    id: 'searchResultLabel'
                                }
                            ]
                        }
                    ]
                }),
            listeners: {
                afterrender: function () {
                    var view = tree.getView();
                    view.getHeaderCt().setHeight(0);
                    setTimeout(function () {
                        if (view.getTreeStore() && view.getTreeStore().loading) {
                            tree.setLoading({
                                msg: '<p><strong>@{R=Core.Strings;K=WEBJS_GK0_21; E=js}</strong></p><p>@{R=Core.Strings;K=WEBJS_GK0_30; E=js}</p>'
                            });
                        }
                    }, 50);
                },
                itemclick: function (tree, record, item) {
                    //Only allow selection of nodes which have checkboxes
                    var checkable = record.get('checked') !== null;
                    if (checkable) {
                        self.trigger('OIDPicker.SelectionChange');
                        oidPickerSelection.length = 0;
                        if (!record.get('checked')) {
                            record.set('checked', true);
                            clearSelections(record);
                            oidPickerSelection.push(record.data);
                            testOid(record.data);
                        } else {
                            record.set('checked', false);
                            clearRightPane();
                        }
                    } else if (record.isExpanded()) {
                        record.collapse();
                    } else {
                        record.expand();
                    }
                },
                //Raised when a checkbox status is changed, we enforce single selection
                //here if needed, and dealing with the selected items.
                checkchange: function (node, checked) {
                    //Empty the selection
                    self.trigger('OIDPicker.SelectionChange');
                    oidPickerSelection.length = 0;
                    if (checked) {
                        clearSelections(node);
                        oidPickerSelection.push(node.data);
                        testOid(node.data);
                    } else {
                        clearRightPane();
                    }
                },
                //Nodes that doesn't have checkboxes (e.g. folders, tables, entries) should not be selectable.
                beforeselect: function (tree, record) {
                    return record.get('checked') !== null;
                },
                beforeload: function () {
                    var searchText;
                    var inputText = getSearchText();

                    if (inputText !== '' && inputText !== oidPickerSearchBarEmptyText) {
                        searchText = String.format('@{R=Core.Strings;K=WEBJS_GK0_20; E=js}', _.escape(inputText));
                    } else {
                        searchText = '@{R=Core.Strings;K=WEBJS_GK0_21; E=js}';
                    }
                    this.setLoading({
                        msg: '<p><strong>' + searchText + '</strong></p><p>@{R=Core.Strings;K=WEBJS_GK0_30; E=js}</p><input type="button" id="oidPickerCancelSearch" value="@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}"/>'
                    });
                },
                load: function (tree, node, records, successful, eOpts) {
                    this.setLoading(false);
                    var inputText = getSearchText();
                    var itemCount = oidPickerStore.getRootNode().childNodes.length;
                    if (isNaN(itemCount) || itemCount === 0 && oidPickerIsSearching) {
                        Ext42.getCmp('searchResultToolbar').show();
                        Ext42.getCmp('searchResultLabel').update(getEmptyText());
                        return;
                    } else if (oidPickerIsSearching) {
                        Ext42.getCmp('searchResultToolbar').show();
                        var resultHtml;
                        //For one result we show "1 result", for more, we show "X results"
                        if (itemCount === 1) {
                            resultHtml = String.format('<div id="oidPickerEmptyText"><p class="oidPickerNoResult">@{R=Core.Strings;K=WEBJS_GK0_35; E=js}</p></div>', itemCount, _.escape(inputText));
                        } else {
                            resultHtml = String.format('<div id="oidPickerEmptyText"><p class="oidPickerNoResult">@{R=Core.Strings;K=WEBJS_GK0_34; E=js}</p></div>', itemCount, _.escape(inputText));
                        }
                        Ext42.getCmp('searchResultLabel').update(resultHtml);
                    }
                    oidPickerIsSearching = false;
                }
            }
        });

        selectionHolder = Ext42.create('Ext.panel.Panel', {
            id: 'selectionHolder',
            width: 269,
            height: 600,
            frame: false,
            title: '@{R=Core.Strings;K=WEBJS_ZB0_03; E=js}',
            autoScroll: true
        });

        mainPanel = Ext42.create('Ext.panel.Panel', {
            renderTo: oidPickerRenderTo,
            layout: {
                type: 'table',
                columns: 2,
                rows: 1,
                tdAttrs: {
                    valign: 'top'
                }
            },
            items: [
                tree, selectionHolder
            ]
        });

        function clearSelections(node) {
            var nodeId = node.data.id;
            //Find the root of the tree from the current node.
            var rootNode = null;
            while (node.parentNode !== null) {
                node = node.parentNode;
            }
            rootNode = node;
            //Clear all selections except the newly selected current node.
            rootNode.cascadeBy(function () {
                if (this.get('id') !== nodeId) {
                    //Setting checked reloads the node's icon. With only unchecking what is
                    //checked (which would be one node) we can save a lot of image loading.
                    if (this.get('checked')) {
                        this.set('checked', false);
                    }
                }
            });
        }

        function getSearchText() {
            return $("#searchBar").find("input").val();
        }

        function getEmptyText() {
            var html = '';
            html += '<div id="oidPickerEmptyText"><p class="oidPickerNoResult">@{R=Core.Strings;K=WEBJS_GK0_22; E=js}' + _.escape(getSearchText()) + '@{R=Core.Strings;K=WEBJS_GK0_23; E=js}</p>';
            html += '<p>@{R=Core.Strings;K=WEBJS_GK0_24; E=js}</p>';
            html += '<ul>';
            html += '<li>@{R=Core.Strings;K=WEBJS_GK0_25; E=js}</li>';
            html += '<li>@{R=Core.Strings;K=WEBJS_GK0_26; E=js}</li>';
            html += '<li><a href="@{R=Core.Strings;K=WEBJS_GK0_28; E=js}">@{R=Core.Strings;K=WEBJS_GK0_27; E=js}</a></li>';
            html += '</ul>';
            html += '</div>';
            return html;
        }

        function fillTestResultWithError(error) {
            $('#oidPickerPolledValue').text('@{R=Core.Strings;K=WEBJS_GK0_17; E=js}');
        }

        function clearRightPane() {
            Ext42.getCmp('selectionHolder').update('');
        }

        function testOid(data) {

            // when nothing selected, just skip the oid test
            if (oidPickerSelection.length === 0) {
                return;
            }
            var currentValue = oidPickerSelection[0].id;
            var oidIsTabular = oidPickerSelection[0].isTabular === 'true';
            var pollingSNMPType = oidIsTabular ? 2 : 1;

            if (SW.MIBBrowser.IsDemo) {
                fillMIBInformation(data, oidIsTabular);
                fillDemoMessage();
                return;
            }

            if (oidPickerWizardUrl !== undefined && oidPickerNodeId !== undefined) {
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: testOidUrl,
                    data: JSON.stringify({ nodeID: oidPickerNodeId, oid: currentValue, pollingType: pollingSNMPType }),
                    success: function (result) {
                        data.result = result.d;
                        fillRightPane(data, oidIsTabular);
                    },
                    error: function (xhr) {
                        var err = eval("(" + xhr.responseText + ")");
                        fillTestResultWithError(err.Message);
                        isValidOid = false;
                    },
                    dataType: 'json'
                });
            }
        }

        function searchOid(criteria) {
            searchCriteria = criteria;
            //Search only if there's search text
            if (criteria !== '' && criteria !== oidPickerSearchBarEmptyText) {
                Ext42.getCmp('clearSearchButton').show();
                oidPickerIsSearching = true;
                //Clear the tree (we need to clear it this way, store.remove() doesn't work
                //as excpected, it launches a request for every single node in the tree, thus
                //choking businesslayer.
                oidPickerStore.getRootNode().removeAll();
                //Set the proxy URL to the search endpoint.
                oidPickerProxy.url = searchUrl;
                //Launch a request.
                oidPickerStore.load();
            }
        }

        //The ExtJs event runs too late, and the browsers submits back on enter anyways.
        //JQuery can handle it.
        $('#searchBar-inputEl').keydown(function (e) {
            //On enter start searching. Return false not to submit the form.
            if (e.keyCode === 13) {
                searchOid($(this).val());
                $(this).blur();
                return false;
            }
        });
        return this;
    }

    function fillMIBInformation(data, oidIsTabular) {
        var html = '<div class="oidPickerRightPane">';

        var nodeText = '<span><a target="_blank" href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + oidPickerNodeId + '"><span class="oidPickerNode"></span></a></span>';
        var onNodeText = String.format('@{R=Core.Strings;K=LIBCODE_AB0_17; E=js}', nodeText);

        if (oidIsTabular) {
            var firstXRowsText = '@{R=Core.Strings;K=WEBJS_GK0_61; E=js}';
            firstXRowsText = firstXRowsText.replace('{0}', '<strong>' + data.name + '</strong>');
            firstXRowsText = firstXRowsText.replace('{1}', nodeText);

            html += '<table id="oidPickerPolledValueHolder" class="oidPickerPolledValueHolder">';
            html += '<thead><tr><td>' + firstXRowsText + '</td></tr></thead>';
        } else {
            html += '<table id="oidPickerPolledValueHolder">';
            html += '<thead><tr><td><b>' + data.name + '</b> ' + onNodeText + '</td></tr></thead>';
        }
        html += '</table>';
        if (data.description !== undefined) {
            html += '<p><b>@{R=Core.Strings;K=WEBJS_GK0_10; E=js} </b><p>';
            html += '<p>' + data.description + '</p>';
        }
        if (data.id !== undefined) {
            html += '<p><b>@{R=Core.Strings;K=WEBJS_GK0_08; E=js} </b><span>' + data.id + '</span> </p>';
        }
        if (data.mib !== undefined) {
            html += '<p><b>@{R=Core.Strings;K=WEBJS_GK0_09; E=js} </b><span>' + data.mib + '</span> </p>';
        }
        html += '<p><b>@{R=Core.Strings;K=WEBJS_GK0_15; E=js} </b><span id="oidPickerPolledType"></span></p>';
        if (data.accessType !== undefined) {
            html += '<p><b>@{R=Core.Strings;K=WEBJS_GK0_11; E=js} </b><span>' + data.accessType + '</span> </p>';
        }
        if (data.variableType !== undefined) {
            html += '<p><b>@{R=Core.Strings;K=WEBJS_GK0_12; E=js} </b><span>' + data.variableType + '</span> </p>';
        }
        if (data.status !== undefined) {
            html += '<p><b>@{R=Core.Strings;K=WEBJS_GK0_13; E=js} </b><span>' + data.status + '</span> </p>';
        }
        html += '</div>';
        Ext42.getCmp('selectionHolder').update(html);
        $.ajax({
            url: loadNodeUrl,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({
                nodeId: oidPickerNodeId
            }),
            success: function (result) {
                $('.oidPickerNode').html(result.d);
            }
        });
    };

    function fillRightPane(data, oidIsTabular) {
        fillMIBInformation(data, oidIsTabular);
        fillTestResult(data.result, oidIsTabular);
    }

    function fillDemoMessage() {
        var message = '@{R=Core.Strings.2;K=WEBJS_LH0_4; E=js}';
        var submessage = '@{R=Core.Strings.2;K=WEBJS_LH0_5; E=js}';

        var template = "<tr><td><div class='sw-demo-message'>{0}<div class='sw-demo-submessage'>{1}</div></div></td></tr>";
        var html = SW.Core.String.Format(template, message, submessage);
        $('#oidPickerPolledValueHolder').append(html);
    }

    function fillTestResult(data, oidIsTabular) {
        var type;
        var oids = [];
        isValidOid = true;
        $(data).each(function (index, oid) {
            oids.push(oid.Value);
            type = oid.Type;
            if (type === 'UNSUPPORTED') {
                isValidOid = false;
            }
        });
        if (!isValidOid) {
            $('#oidPickerPolledValueHolder').append(buildUnsupportedTip());
        } else {
            var resultHtml = buildTestResult(oids, oidIsTabular);
            $('#oidPickerPolledValueHolder').append(resultHtml);

            // single value is expected, not table
            if (oidIsTabular && !oidPickerPropertyIsTable) {
                $('#oidPickerPolledValueHolder').append(buildMultipleResultsTip());
            }
        }

        $('#oidPickerPolledType').text(type);
    }

    function buildUnsupportedTip() {
        return '<div class="sw-suggestion sw-suggestion-fail">' +
		'<div class="sw-suggestion-icon"></div>' +
		'<span id="oidPickerPolledValue">' +
		'<span>@{R=Core.Strings;K=WEBJS_GK0_60; E=js}</span>' +
		' >><a target="_blank" rel="noopener noreferrer" href="@{R=Core.Strings;K=WEBJS_GK0_55; E=js}">@{R=Core.Strings;K=WEBJS_GK0_54; E=js}</a>' +
		'</span></div>';
    }

    function buildMultipleResultsTip() {
        return '<div class="sw-suggestion"><div class="sw-suggestion-icon"></div>' +
            '<span class="sw-suggestion-title">@{R=Core.Strings;K=WEBJS_GK0_56; E=js}</span>' +
            '<p>@{R=Core.Strings;K=WEBJS_GK0_57; E=js}</p>' +
            '</div>';
    }

    function buildTestResult(oids, oidIsTabular) {
        var html = '';
        if (oidIsTabular) {
            for (var i = 0; i < firstXRowsCount; i++) {
                if (i < oids.length) {
                    html += '<tr><td>' + oids[i] + '</td></tr>';
                } else {
                    html += '<tr><td class="oidPickerGrayText">[@{R=Core.Strings;K=WEBJS_ZB0_02; E=js}]</td></tr>';
                }
            }
        } else {
            html = '<tr><td>' + oids[0] + '</td></tr>';
        }
        return html;
    }

    function convertToSelection(oid) {
        return {
            oid: oid.id,
            name: oid.name,
            isTabular: oid.isTabular,
            tableName: oid.tableName
        };
    }

    $('#oidPickerCancelSearch').live('click', function () {
        oidPickerIsSearching = false;
        tree.setLoading({
            msg: '@{R=Core.Strings;K=WEBJS_GK0_29; E=js}'
        });
        $.ajax({
            url: cancelSearchUrl,
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        });
    });

    Ext42.onReady(function () {
        //ExtJs has a 30 seconds ajax timeout by default, however, searching OIDs by description
        //could take a minute or more. We increase the timeout because of this to 10 minutes.
        Ext42.override(Ext42.data.proxy.Ajax, { timeout: 600000 });
        //Initialize the QucikTips module to enable the popups on hover.
        Ext42.QuickTips.init({ showDelay: 500 });
        //ExtJs fall behind the HTML5 specification, and uses an unsupported placeholder mechanism.
        //The problem is that the placeholder text in the search field doesn't disappear, so we have
        //to turn off HTML5 placeholder support to make it work.
        Ext42.supports.Placeholder = false;
    });

    return {
        init: function (options) {
            return xinit(options);
        },

        getSelection: function () {
            var result = [];
            for (i = 0; i < oidPickerSelection.length; i++) {
                result.push(convertToSelection(oidPickerSelection[i]));
            }
            return result;
        },

        clearSelection: function () {
            var record;
            for (i = 0; i < oidPickerSelection.length; i++) {
                record = oidPickerStore.getNodeById(oidPickerSelection[i].id);
                if (record && typeof record !== 'undefined') {
                    record.set('checked', false);
                }
            }
            oidPickerSelection = [];
            selectionHolder.update('');
        },

        fillManualResult: function (name, oid, data, isTabular) {
            fillRightPane(
                {
                    name: name,
                    id: oid,
                    result: data
                }, isTabular);
        },

        getFailureReason: function () {
            if (oidPickerSelection.length === 0) {
                return '@{R=Core.Strings;K=WEBJS_GK0_51; E=js} >><a target="_blank" rel="noopener noreferrer" href="@{R=Core.Strings;K=WEBJS_GK0_55; E=js}">@{R=Core.Strings;K=WEBJS_GK0_54; E=js}</a>';
            }
            else if (isValidOid === false) {
                return '@{R=Core.Strings;K=WEBJS_GK0_52; E=js} >><a target="_blank" rel="noopener noreferrer" href="@{R=Core.Strings;K=WEBJS_GK0_55; E=js}">@{R=Core.Strings;K=WEBJS_GK0_54; E=js}</a>';
            }
        },

        hasValidOid: function () {
            return oidPickerSelection.length !== 0 && isValidOid;
        },

        reset: function () {
            mainPanel.removeAll();
        }
    }
}();
