﻿/// <reference path="~/Orion/js/jquery/jquery-1.7.1-vsdoc.js" />

SW.Core.WSAsyncExecuteTasks = function () {
    var clientMethodIndex = 0;
    var clientObjectIndex = 1;
    var serverMethodIndex = 2;
    var parametersIndex = 3;
    var graphObjectIndex = 4;
    var graphDataIndex = 5;

    return {

        LoadResource: function (taskObjects, ControlLifeCycle) {
            var results = new Array();
            results["NoMessage"] = results["Message"] = results["JustMessage"] = 0;
            $("#" + taskObjects.ContentContainer).show();
            $("#" + taskObjects.MessageContainer).empty();

            if (ControlLifeCycle.length > 0) {
                var task_timer = setTimeout(function () {
                    $("#" + taskObjects.Loader).show();
                }, 200);

                SW.Core.WSAsyncExecuteTasks.Execute(task_timer, taskObjects, ControlLifeCycle, 0, results);
            }
        },

        Execute: function (task_timer, taskObjects, ControlLifeCycle, index, results) {
            if (ControlLifeCycle[index][serverMethodIndex] == null) {
                alert("ProcessData");
                SW.Core.WSAsyncExecuteTasks.ProcessData({ "Result": null, "DataResultState": "", "Message": null }, task_timer, taskObjects, ControlLifeCycle, index, results);
            } else {
                SW.Core.WSAsyncExecuteTasks.callWebService("/Orion/Services/WSAsyncExecuteTasks.asmx",
                                     "GetTaskResult",
                                     {
                                         delegateServerMethod: ControlLifeCycle[index][serverMethodIndex],
                                         parameters: ControlLifeCycle[index][parametersIndex]
                                     },
                                     function (data) { SW.Core.WSAsyncExecuteTasks.ProcessData(data, task_timer, taskObjects, ControlLifeCycle, index, results); });
            }
        },
        
        handleError: function(xhr, customErrorHandler) {
            if (xhr.status == 401 || xhr.status == 403) {
                // login cookie expired. reload the page so we get bounced to the login page.
                alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
                window.location.reload();
            }
            var errorText = xhr.responseText;
            var msg;
            try {
                var error = eval("(" + errorText + ")");
                msg = error.Message;
                $("#stackTrace").text(error.StackTrace);
            }
            catch (err) {
                msg = err.description;
            }
            $("#test").text(msg);
            if (typeof (customErrorHandler) == 'function') {
                customErrorHandler(msg);
            }
        },
        
        callWebService: function (serviceName, methodName, param, onSuccess, onError) {
            var paramString = JSON.stringify(param);

            if (paramString.length === 0) {
                paramString = "{}";
            }

            $.ajax({
                type: "POST",
                url: serviceName + "/" + methodName,
                data: paramString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    onSuccess(msg.d);
                },
                error: function (xhr, status, errorThrown) {
                    SW.Core.WSAsyncExecuteTasks.handleError(xhr, onError);
                }
            });
        },

        ProcessData: function (data, task_timer, taskObjects, ControlLifeCycle, index, results) {
            results[data.DataResultState == null ? "NoMessage" : data.DataResultState]++;

            if (ControlLifeCycle[index][graphObjectIndex] != null) {
                $("#" + ControlLifeCycle[index][graphObjectIndex]).attr("src", ControlLifeCycle[index][graphDataIndex] + "&__=" + Number(new Date()));
                $("#" + ControlLifeCycle[index][graphObjectIndex]).show('slow');
            }

            if (ControlLifeCycle[index][clientMethodIndex] == null) {
                if (ControlLifeCycle[index][clientObjectIndex] != null) {
                    var dataDiv = $("#" + ControlLifeCycle[index][clientObjectIndex]);

                    dataDiv.empty();
                    dataDiv.append(data.Result);
                    dataDiv.show('slow');
                }
            } else {
                if (ControlLifeCycle[index][clientMethodIndex] == "SW.Core.WSAsyncExecuteTasks.DoRefreshPage") {
                    if (data.Result != null) {
                        window.location = data.Result;
                        return;
                    } else {
                        if (ControlLifeCycle[index][clientObjectIndex] != null)
                            $("#" + ControlLifeCycle[index][clientObjectIndex]).show();
                    }
                } else {
                    if (ControlLifeCycle[index][serverMethodIndex] == null && data.Result == null) {
                        //no server callback... just user defined JS method with user defined parameters
                        eval(ControlLifeCycle[index][clientMethodIndex]);
                    } else {
                        //server callback with result into the user defined client JS method
                        eval(ControlLifeCycle[index][clientMethodIndex] + "(data.Result, ControlLifeCycle[index][clientObjectIndex]);");
                    }
                }
            }

            if ((results["Message"] + results["JustMessage"] != 0) && data.Message != null) {
                //show error message only if servers that we get data from are available
                //if no server is available the error message is not so important... 
                //in that case unreachable flag is more important
                var errorDiv = $("#" + taskObjects.MessageContainer);

                errorDiv.empty();
                errorDiv.append(data.Message);
                errorDiv.show('slow');

                if (results["JustMessage"] != 0)
                    $("#" + taskObjects.ContentContainer).hide();
            }

            if (ControlLifeCycle.length > index + 1) {
                SW.Core.WSAsyncExecuteTasks.Execute(task_timer, taskObjects, ControlLifeCycle, index + 1, results);
            } else {
                if (task_timer) {
                    clearTimeout(task_timer);
                    task_timer = null;
                }
                $("#" + taskObjects.Loader).hide();
            }
        }

    };
} ();