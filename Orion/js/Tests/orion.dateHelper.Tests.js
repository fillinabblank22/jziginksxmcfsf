﻿/// <reference path="../OrionMinReqs.js/json2.js"/>
/// <reference path="../OrionMinReqs.js/OrionMinReqs.js"/>
/// <reference path="../OrionMinReqs.js/underscore-min.js"/>
/// <reference path="../OrionMinReqs.js/underscore-customizations.js"/>
/// <reference path="../OrionMinReqs.js/status.js"/>
/// <reference path="../OrionMinReqs.js/orion.dateHelper.js"/>

/* global SW:true, module, test */ // JSHint

module('Orion.DateHelper', {
    setup: function () {
        this.helper = SW.Core.DateHelper;
        this.strings = {
            justNow: 'TEST just now',
            second: 'TEST about a second ago',
            seconds: 'TEST {0} seconds ago',
            minute: 'TEST about a minute ago',
            minutes: 'TEST {0} minutes ago',
            hour: 'TEST about a hour ago',
            hours: 'TEST {0} hours ago',
            day: 'TEST about a day ago',
            days: 'TEST {0} days ago',
            month: 'TEST about a month ago',
            months: 'TEST {0} months ago',
            year: 'TEST about a year ago',
            years: 'TEST {0} years ago'
        };
    },
    teardown: function () {
        delete this.helper;
        delete this.strings;
    }
});

test('has utcToTimeAgo method.', function (assert) {
    assert.equal(typeof this.helper.utcToTimeAgo, 'function', 'utcToTimeAgo method is defined.');
});

test('has localToTimeAgo method.', function (assert) {
    assert.equal(typeof this.helper.localToTimeAgo, 'function', 'localToTimeAgo method is defined.');
});

test('xToTimeAgo methods require date object as argument', function (assert) {
    assert.throws(function () {
        this.helper.utcToTimeAgo('2014-01-01T11:11:11');
    }, "utcToTimeAgo call with string argument fails.");
    assert.throws(function () {
        this.helper.localToTimeAgo('2014-01-01T11:11:11');
    }, "localtoTimeAgo call with string argument fails.");
});

test('utcToTimeAgo returns correct texts for different dates', function (assert) {
    var event = new Date('2014-01-01T08:00:00'); // in utc time
    var now = new Date('2014-01-01T09:00:00'); // in local time

    now.setSeconds(1);
    var r0 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r0, 'TEST about a second ago', r0);

    now.setSeconds(2);
    var r1 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r1, 'TEST 2 seconds ago', r1);

    now.setSeconds(50);
    var r2 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r2, 'TEST about a minute ago', r2);

    now.setMinutes(10);
    var r3 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r3, 'TEST 10 minutes ago', r3);

    now.setMinutes(50);
    var r4 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r4, 'TEST about a hour ago', r4);

    now.setHours(14);
    var r5 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r5, 'TEST 5 hours ago', r5);

    now.setDate(2);
    var r6 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r6, 'TEST about a day ago', r6);

    now.setDate(15);
    var r7 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r7, 'TEST 14 days ago', r7);

    now.setMonth(1);
    var r8 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r8, 'TEST about a month ago', r8);

    now.setMonth(3);
    var r9 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r9, 'TEST 3 months ago', r9);

    now.setMonth(12);
    var r10 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r10, 'TEST about a year ago', r10);

    now.setYear(2018);
    var r11 = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r11, 'TEST 4 years ago', r11);
});

test('utcToTimeAgo returns "just now" if the event happened "now"', function (assert) {
    var event = new Date('2014-01-01 11:11:11');
    var now = event;
    var r = this.helper.utcToTimeAgo(event, now, this.strings);
    assert.equal(r, 'TEST just now', r);
});

test('localToTimeAgo returns correct texts', function(assert) {
    var event = new Date();
    var now = new Date();
    var r0 = this.helper.localToTimeAgo(event, now, this.strings);
    assert.equal(r0, 'TEST just now', r0);

    now.setMinutes(now.getMinutes()+6);
    var r1 = this.helper.localToTimeAgo(event, now, this.strings);
    assert.equal(r1, 'TEST 6 minutes ago', r1);

    // Doing just a few tests, as the core functionality is same as for UTC
});