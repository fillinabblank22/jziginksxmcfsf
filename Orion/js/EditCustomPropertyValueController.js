﻿SW.Core.namespace("SW.Core").EditCustomPropertyValue = function (config) {
    "use strict";

    var initDateTimePickers = function () {
        $.fn.orionGetDate = function () {
            var datepicker = $container.find('.datePicker');
            var timepicker = $container.find('.timePicker');
            var datepart = $.datepicker.parseDate(config.regionalSettings.dateFormat, datepicker.val());
            var timepart = timepicker[0].timePicker.getTime();
            return new Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());
        };
        $.fn.orionSetDate = function (date) {
            $container.find('.datePicker').val($.datepicker.formatDate(config.regionalSettings.dateFormat, date));
            var time = date.getTime(); // timePicker.setTime screws up the object. preserve it for our poor caller
            $container.find('.timePicker')[0].timePicker.setTime(date);
            date.setTime(time);
        };
        $(function () {
            $.datepicker.setDefaults(config.regionalSettings);
            $container.find('.datePicker').datepicker({ mandatory: true, closeAtTop: false });
            $container.find('.timePicker').timePicker({ separator: config.regionalSettings.timeSeparator, show24Hours: config.regionalSettings.show24Hours });
        });
    };

    function isInteger(str) {
        var n = ~ ~Number(str);
        return String(n) === str;
    }

    function isNumber(str) {
        var decimalSeparator = window.Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator;
        var num = str.replace(decimalSeparator, '.');
        return !isNaN(parseFloat(num)) && isFinite(num);
    }

    function isDate(configuration, callback) {
        ORION.callWebService("/Orion/Services/EditCustomPropertyValue.asmx", "ValidateDate", { date: configuration.DateValue, time: configuration.TimeValue },
        // On success
            function (response) {
                if (response.date === false) {
                    $container.find('.datePicker[data-edited="EditorValue"]').addClass('invalidValue');
                }
                if (response.time === false) {
                    $container.find('.timePicker[data-edited="EditorValue"]').addClass('invalidValue');
                }
                if ($.isFunction(callback)) {
                    callback(response.date && response.time);
                }
            },
        // On fails
            function () {
                if ($.isFunction(callback)) {
                    callback(false);
                }
                $container.find('.datePicker[data-edited="EditorValue"]').addClass('invalidValue');
            }
        );
    }
    var macroRegExp = /\$\{[^\}]*\}/; 
    var validateConfiguration = function (configuration, callback, isMultiedit) {
        var macroTemplateResult;
        if (isMultiedit) {
            if (configuration.Value.match(macroRegExp)) {
                $container.find('[data-edited="EditorValue"]').addClass("invalidValue");
                callback(false);
                return;
            } else {
                macroTemplateResult = configuration.Value;
            }
        } else {
            macroTemplateResult = SW.Core.MacroVariablePickerController.ParseMacroVariable(configuration.Value);
        }


        $container.find('[data-edited="EditorValue"]').removeClass("invalidValue");

        if (macroTemplateResult == '') {
            if ($.isFunction(callback)) {
                callback(true);
            }
            return;
        }

        if (config.datatype === 'string' || config.datatype === 'bool' || config.datatype === 'enum') {
            if (macroTemplateResult === '') {
                $container.find('[data-edited="EditorValue"]').addClass("invalidValue");
                if ($.isFunction(callback)) {
                    callback(false);
                }
            } else {
                callback(true);
            }
        } else if (config.datatype === 'int') {
            if (isInteger(macroTemplateResult) === false && !macroTemplateResult.match(macroRegExp)) {
                $container.find('[data-edited="EditorValue"]').addClass("invalidValue");
                if ($.isFunction(callback)) {
                    callback(false);
                }
            } else {
                callback(true);
            }
        } else if (config.datatype === 'float' || config.datatype === 'double') {
            if (isNumber(macroTemplateResult) === false && !macroTemplateResult.match(macroRegExp)) {
                $container.find('[data-edited="EditorValue"]').addClass("invalidValue");
                if ($.isFunction(callback)) {
                    callback(false);
                }
            } else {
                callback(true);
            }
        } else if (config.datatype === 'datetime') {
            if (configuration.DateValue === '' || configuration.TimeValue === '') {
                if (configuration.DateValue === '') {
                    $container.find('.datePicker[data-edited="EditorValue"]').addClass('invalidValue');
                }
                if (configuration.TimeValue === '') {
                    $container.find('.timePicker[data-edited="EditorValue"]').addClass('invalidValue');
                }
                if ($.isFunction(callback)) {
                    callback(false);
                }
            } else {
                isDate(configuration, callback);
            }
        }
    };

    // Constructor
    this.init = function () {
        $container = $('#' + config.containerID);
        initDateTimePickers();
    };

    // Public
    this.createConfiguration = function () {
        var value = '';
        var dateValue = '';
        var timeValue = '';
        if (config.datatype === 'datetime') {
            dateValue = $container.find('.datePicker[data-edited="EditorValue"]').val();
            timeValue = $container.find('.timePicker[data-edited="EditorValue"]').val();
            value = dateValue + ' ' + timeValue;
        } else {
            value = $container.find('[data-edited="EditorValue"]').val();
        }
        var configuration = {
            Value: value.trim(),
            DateValue: dateValue.trim(),
            TimeValue: timeValue.trim()
        };
        return configuration;
    };

    this.validateSectionAsync = function (sectionID, callback, isMultiEdit) {
        validateConfiguration(this.createConfiguration(), callback, isMultiEdit);
    };

    var self = this;
    var $container = null;
}