﻿(function (Core) {

    var URLfooter = 'https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=documentation_licensemanager';
    var URLblog = 'https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=thwackproductblog';
    var URLestimatePurchase = 'https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=online_quote&CMP=PRD-IPS-Settings-EvalPopup-X-NOIP-X';
    var URLmaintenanceBenefits = 'https://www.solarwinds.com/embedded_in_products/productLink.aspx?id=maintenance_card';
    var URLLicenseManager = '/ui/licensing/license-manager';

    var URLcustomerPortalMaintenance = '/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_renew&CMP=PRD-IPS-Settings-MaintPopup-X-NOIP-X';
    var URLmaintenanceDetails = '/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_renew&CMP=PRD-IPS-Settings-UpgrdPopupMaint-X-NOIP-X';
    var URLcustomerPortalPua = '/Orion/Admin/WebIntegrationProxy.aspx?linkId=cp_pua&CMP=PRD-IPS-UpdateNotification-ProductUpgradeAdvisor-X-X-X';

    var SalesTrigger = Core.SalesTrigger = Core.SalesTrigger || {};

    var dialogTemplate = '<div id="sw-salestrigger-popup">' +
                    '    <div class="sw-salestrigger-wrapper">' +
                    '        <div class="sw-salestrigger-content">' +
                    '            <p></p>' +
                    '            <table>' +
                    '                <thead>' +
                    '                    <tr></tr>' +
                    '                </thead>' +
                    '                <tbody></tbody>' +
                    '            </table>' +
                    '        </div>' +
                    '        <div class="sw-salestrigger-links sw-salestrigger-adminonly"></div>' +
                    '    </div>' +

                    '    <div id="sw-salestrigger-footer" class="sw-salestrigger-adminonly">' +
                    '    </div>' +
                    '</div>';

    var linkBlockTemplate = '<div class="sw-salestrigger-linkwrapper"><span>{0}</span><span class="sw-text-helpful">{1}</span></div>';

    var popupFooter = '<div class="sw-salestrigger-left">' +
            '       <div class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_2;E=js}</div>' +
            '       <ul>' +
            '           <li><a href="mailto:customersales@solarwinds.com" class="sw-link">@{R=Core.Strings.2;K=WEBJS_LC0_17;E=js}</a></li>' +
            '           <li><span class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_4;E=js}</span> <a href="tel:866 530 8100">866 530 8100</a></li>' +
            '           <li><span class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_5;E=js}</span> <a href="tel:+353 21 5002900">+353 21 5002900</a></li>' +
            '           <li><span class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_6;E=js}</span> <a href="tel:+61 2 8412 4900">+61 2 8412 4900</a></li>' +
            '       </ul>' +
            '   </div>' +
            '   <div class="sw-salestrigger-right">' +
            '       <div class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_7;E=js}</div>' +
            '       <p>@{R=Core.Strings.2;K=WEBJS_LC0_8;E=js}</p>' +
            '        <a href="' + URLLicenseManager + '" class="manage-orion-licenses-button">' +
            '           <div class="button-frame"></div>' +
            '           <div  class="button-text">' +
            '               <span>@{R=Core.Strings.2;K=WEBJS_LC0_19;E=js}</span>' +
            '           </div>' +
            '       </a>' +
            '   </div>';

    var maintenanceFooter = '<div class="sw-salestrigger-left">' +
        '      <div class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_2;E=js}</div>' +
        '      <ul>' +
        '           <li><a href="mailto:renewals@solarwinds.com" class="sw-link">@{R=Core.Strings.2;K=WEBJS_JK0_04;E=js}</a></li>' +
        '           <li><span class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_4;E=js}</span> <a href="tel:866 530 8100">866 530 8100</a></li>' +
        '           <li><span class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_5;E=js}</span> <a href="tel:+353 21 5002900">+353 21 5002900</a></li>' +
        '           <li><span class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_LC0_6;E=js}</span> <a href="tel:+61 2 8412 4900">+61 2 8412 4900</a></li>' +
        '       </ul>' +
        '   </div>' +
        '   <div class="sw-salestrigger-right">' +
        '       <div class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_SH0_1;E=js}</div>' +
        '       <p>' +
        '           <a class="sw-link" target="_blank" rel="noopener noreferrer" href="' + URLmaintenanceBenefits + '">&#0187; @{R=Core.Strings.2;K=WEBJS_SH0_2;E=js}</a><br />' +
        '           <a class="sw-link" target="_blank" rel="noopener noreferrer" href="' + URLblog + '">&#0187; @{R=Core.Strings.2;K=WEBJS_SH0_3;E=js}</a>' +
        '       </p>' +
        '       <div class="sw-salestrigger-label">@{R=Core.Strings.2;K=WEBJS_SH0_4;E=js}</div>' +
        '       <p>@{R=Core.Strings.2;K=WEBJS_LC0_8;E=js}</p>' +
        '   </div>';

    var prepareOutgoingLink = function (url) {
        if (SW.Core.Info.OIPEnabled) {
            return url.replace('NOIP', 'OIP');
        }
        return url;
    };

    SalesTrigger.DontRemindMeAgain = function (notificationType) {
        SW.Core.Services.callWebService('/Orion/Services/SalesTrigger.asmx', 'DontRemindMeAgain', { notificationType: notificationType }, function () { location.reload(); });
    };

    SalesTrigger.ShowEvalPopupAsync = function () {
        SW.Core.Services.callWebService('/Orion/Services/SalesTrigger.asmx', 'GetEvalModuleInfo', {}, function (r) {
            SalesTrigger.ShowEvalPopup(r);
        });
    };

    // Eval Banner Handling.
    SalesTrigger.ShowEvalPopup = function (evalDetails) {

        var template = $(dialogTemplate);

        $('.sw-salestrigger-wrapper', template).addClass('sw-salestrigger-evaluation');

        // set the content of the paragraph at the top of the dialog
        $('.sw-salestrigger-content p:first', template).html('@{R=Core.Strings.2;K=WEBJS_LC0_9;E=js}');

        // set the content of the table header row
        $('.sw-salestrigger-content table:first thead tr', template).html(
            '<th>@{R=Core.Strings.2;K=WEBJS_LC0_10;E=js}</th>' +
            '<th>@{R=Core.Strings.2;K=WEBJS_LC0_11;E=js}</th>');

        // set the table body
        // there are classes available for the row styling (sw-salestrigger-expired, sw-salestrigger-expiring)

        var content = '';
        $.each(evalDetails, function () {

            var formatString = this.IsExpired
                ? '@{R=Core.Strings.2;K=WEBJS_LC0_12;E=js}'
                : this.DaysRemaining == 1
                    ? '@{R=Core.Strings.2;K=WEBJS_LC0_13;E=js}'
                    : '@{R=Core.Strings.2;K=WEBJS_LC0_14;E=js}';

            var row = '<tr class="sw-salestrigger' + (this.IsExpired ? '-expired"' : '-expiring') + '">' +
                '<td><label for="sw-sales-' + this.ModuleName + '">' + this.DisplayName + '</label></td>' +
                '<td>' + SW.Core.String.Format(formatString, this.DaysRemaining, this.Expiration) + '</td></tr>';

            content += row;
        });

        $('.sw-salestrigger-content table:first tbody', template).html(content);

        // hide links for non-admin user
        if (!SW.Core.Info.AllowAdmin) {
            $('.sw-salestrigger-adminonly', template).hide();
        } else {
            // set links section
            $('.sw-salestrigger-links', template).html(
                SW.Core.String.Format(linkBlockTemplate, SalesTrigger.PopupButton({ label: '@{R=Core.Strings.2;K=WEBJS_JD0_3;E=js}', url: prepareOutgoingLink(URLestimatePurchase), cls: 'sw-salestrigger-button-eval' }), '@{R=Core.Strings.2;K=WEBJS_LC0_16;E=js}') +
                SW.Core.String.Format(linkBlockTemplate, '<a class="sw-link" href="javascript:SW.Core.SalesTrigger.DontRemindMeAgain(\'Eval\');">&#0187;&nbsp;@{R=Core.Strings;K=WEBJS_ZS0_10;E=js}</a>', '&nbsp;')
            );

            // set footer
            $('#sw-salestrigger-footer', template).html(SW.Core.String.Format(popupFooter, URLfooter));
        }

        // check all functionality

        // fire up the dialog
        $(template).dialog({
            title: '@{R=Core.Strings.2;K=WEBJS_LC0_1;E=js}',
            width: 700,
            modal: true,
            close: function (event, ui) {
                $(this).dialog('destroy').remove();
            }
        });
    };

    SalesTrigger.PopupButton = function (options) {
        var defaults = {
            type: 'primary',
            label: '@{R=Core.Strings.2;K=WEBJS_LC0_15;E=js}',
            cls: '',
            url: '#'
        };

        var opt = $.extend({}, defaults, options || {});

        var html;

        if (opt.type == 'link') {
            html = String.Format('<a href="javascript:void();" class="{1}" id="{2}">{0}</a>', opt.label, 'sw-salestrigger-productusage-link' + ' sw-link ' + opt.cls, opt.id || SW.Core.Id());
        } else {
            html = SW.Core.Widgets.Button(opt.label, {
                type: opt.type,
                id: opt.id || SW.Core.Id(),
                cls: 'sw-salestrigger-productusage-link ' + opt.cls,
                target: "_blank",
                href: opt.url
            });
        }

        return html;
    };

    SalesTrigger.ShowMaintenancePopupAsync = function () {
        SW.Core.Services.callWebService('/Orion/Services/SalesTrigger.asmx', 'GetMaintenanceModuleInfo', {}, function (r) {
            SalesTrigger.ShowMaintenancePopup(r);
        });
    };

    // Maintenance Banner Handling.
    SalesTrigger.ShowMaintenancePopup = function (expirationDetails) {

        var template = $(dialogTemplate);

        $('.sw-salestrigger-wrapper', template).addClass('sw-salestrigger-maintenance');

        // set the content of the paragraph at the top of the dialog
        $('.sw-salestrigger-content p:first', template).html('@{R=Core.Strings.2;K=WEBJS_SH0_5;E=js}');

        // set the content of the table header row
        $('.sw-salestrigger-content table:first thead tr', template).html(
            '<th>@{R=Core.Strings.2;K=WEBJS_LC0_10;E=js}</th>' +
            '<th class="sw-salestrigger-maintenance-lastcol">@{R=Core.Strings.2;K=WEBJS_LC0_11;E=js}</th>');

        // set the table body
        // there are classes available for the row styling (sw-salestrigger-expired, sw-salestrigger-expiring)

        var title = '@{R=Core.Strings.2;K=WEBJS_SH0_7;E=js}';
        var content = '';
        $.each(expirationDetails, function () {

            if (this.IsExpired) {
                title = '@{R=Core.Strings.2;K=WEBJS_SH0_14;E=js}';
            }

            var formatString = this.IsExpired
                ? '@{R=Core.Strings.2;K=WEBJS_LC0_12;E=js}'
                : this.DaysRemaining <= 1
                    ? '@{R=Core.Strings.2;K=WEBJS_LC0_13;E=js}'
                    : '@{R=Core.Strings.2;K=WEBJS_LC0_14;E=js}';

            var row = '<tr class="sw-salestrigger' + (this.IsExpired ? '-expired"' : '-expiring') + '">' +
                '<td><label for="sw-sales-' + this.ModuleName + '">' + this.DisplayName + '</label></td>' +
                '<td>' + SW.Core.String.Format(formatString, this.DaysRemaining, this.Expiration) + '</td></tr>';

            content += row;
        });

        $('.sw-salestrigger-content table:first tbody', template).html(content);

        // hide links for non-admin user
        if (!SW.Core.Info.AllowAdmin) {
            $('.sw-salestrigger-adminonly', template).hide();
        } else {

            $('.sw-salestrigger-links', template).html(
                SW.Core.String.Format(linkBlockTemplate, SalesTrigger.PopupButton({ label: '@{R=Core.Strings.2;K=WEBJS_JD0_1;E=js}', cls: 'sw-salestrigger-button-maintenance', url: prepareOutgoingLink(URLcustomerPortalMaintenance), id: 'sw-salestrigger-button-maintenance-id' }), '@{R=Core.Strings.2;K=WEBJS_LC0_16;E=js}') +
                SW.Core.String.Format(linkBlockTemplate, '<a class="sw-link" href="javascript:SW.Core.SalesTrigger.DontRemindMeAgain(\'Maintenance\');">&#0187;&nbsp;@{R=Core.Strings;K=WEBJS_ZS0_10;E=js}</a>', '&nbsp;'));

            // set footer
            $('#sw-salestrigger-footer', template).html(SW.Core.String.Format(maintenanceFooter, URLfooter));
        }

        // fire up the dialog
        $(template).dialog({
            title: title,
            width: 700,
            modal: true,
            close: function (event, ui) {
                $(this).dialog('destroy').remove();
            }
        });
    };

    SalesTrigger.ShowPollerLimitPopupAsync = function () {
        SW.Core.Services.callWebService('/Orion/Services/SalesTrigger.asmx', 'GetPollerLimitInfo', {}, function (r) {
            SalesTrigger.ShowPollerLimitPopup(r);
        });
    };

    // Poller Limit Banner Handling.
    SalesTrigger.ShowPollerLimitPopup = function (limitDetails) {

        var template = $(dialogTemplate);

        $('.sw-salestrigger-wrapper', template).addClass('sw-salestrigger-polling');

        // set the content of the paragraph at the top of the dialog
        $('.sw-salestrigger-content p:first', template).html(
            '@{R=Core.Strings.2;K=WEBJS_SH0_11;E=js}<br />' +
            '<a class="sw-link sw-salestrigger-adminonly" href="/Orion/Admin/Details/Engines.aspx">&#0187;&nbsp;@{R=Core.Strings.2;K=WEBJS_SH0_13;E=js}</a>');

        // set the content of the table header row
        $('.sw-salestrigger-content table:first thead tr', template).html(
            '<th>@{R=Core.Strings.2;K=WEBJS_SH0_8;E=js}</th>' +
            '<th>@{R=Core.Strings.2;K=WEBJS_SH0_9;E=js}</th>');

        // set the table body
        // there are classes available for the row styling (sw-salestrigger-expired, sw-salestrigger-expiring)

        var content = '';

        $.each(limitDetails, function () {

            var row = '<tr class="sw-salestrigger' + (this.IsPollerLimitReached ? '-expired"' : '-expiring') + '">' +
                '<td><label for="sw-sales-' + this.EngineName + '">@{R=Core.Strings.2;K=WEBJS_SH0_6;E=js} ' + this.EngineName + '</label></td>' +
                '<td><span class="sw-salestrigger-status"><span>' + this.CurrentUsage + ' %</span></span></td></tr>';

            content += row;
        });

        $('.sw-salestrigger-content table:first tbody', template).html(content);

        // hide links for non-admin user
        if (!SW.Core.Info.AllowAdmin) {
            $('.sw-salestrigger-adminonly', template).hide();
        } else {
            // set links section
            var links =
                SW.Core.String.Format(linkBlockTemplate, SalesTrigger.PopupButton({ cls: "sw-salestrigger-button-poller" }), '@{R=Core.Strings.2;K=WEBJS_LC0_16;E=js}') +
                SW.Core.String.Format(linkBlockTemplate, '<a class="sw-link" href="javascript:SW.Core.SalesTrigger.DontRemindMeAgain(\'Polling\');">&#0187;&nbsp;@{R=Core.Strings;K=WEBJS_ZS0_10;E=js}</a>', '&nbsp;');

            $('.sw-salestrigger-links', template).html(links);

            // set footer
            $('#sw-salestrigger-footer', template).html(SW.Core.String.Format(popupFooter, URLfooter));
        }

        // check all functionality
        //setupCheckBoxes(template, '.sw-salestrigger-poller-checkbox-all', '.sw-salestrigger-poller-checkbox');

        // fire up the dialog
        $(template).dialog({
            title: '@{R=Core.Strings.2;K=WEBJS_SH0_10;E=js}',
            width: 700,
            modal: true,
            close: function (event, ui) {
                $(this).dialog('destroy').remove();
            }
        });
    };

    SalesTrigger.ShowUpgradePopupAsync = function () {
        SW.Core.Services.callWebService('/Orion/Services/SalesTrigger.asmx', 'GetUpgradeModuleInfo', {}, function (r) {
            SalesTrigger.ShowUpgradePopup(r);
        });
    };

    // Upgrade notification Banner Handling.
    SalesTrigger.ShowUpgradePopup = function (upgradeDetails) {

        var template = $(dialogTemplate);
        // set the content of the paragraph at the top of the dialog
        $('.sw-salestrigger-content p:first', template).html('@{R=Core.Strings.2;K=WEBJS_IB0_3;E=js}');

        // set the content of the table header row
        $('.sw-salestrigger-content table:first thead tr', template).html('<th></th><th>@{R=Core.Strings;K=WEBJS_ZS0_3;E=js}</th><th>@{R=Core.Strings.2;K=WEBJS_IB0_4;E=js}</th>');

        $('.sw-salestrigger-wrapper', template).addClass('sw-salestrigger-upgrade');

        var maintenanceExpiredMessage = '<tr class="sw-salestrigger-message"><td colspan=3>' +
            '<div class="sw-suggestion sw-suggestion-fail"> <span class="sw-suggestion-icon"></span>' +
            '<b class="sw-salestrigger-warning-message-text">' + SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_ZS0_5;E=js}', '') + '</b><br>' +
            '<a class="sw-suggestion-link sw-salestrigger-redLink" href="{0}" id="sw-salestrigger-message-row-id">&#0187;&nbsp;@{R=Core.Strings;K=WEBJS_ZS0_7;E=js}</a></div></td></tr>';

        // set the table body
        // there are classes available for the row styling (sw-salestrigger-expired, sw-salestrigger-expiring)

        var content = '';
        $.each(upgradeDetails.ugradeItems, function () {
            var row = '<tr class="sw-salestrigger' + (this.IsVersionDifference ? '-product-blue-bold' : '-product-blue') + (this.IsMaintenanceExpired ? 'sw-salestrigger-product-gray' : '') + '">' +
                '<td class="sw-salestrigger-checkbox"></td>' +
                '<td><b>' + this.ProductDisplayName + '</b><br/><a class="sw-link" href="' + this.ReleaseNotes + '" target="_blank">@{R=Core.Strings.2;K=WEBJS_IB0_1;E=js}&nbsp;&#0187;</a></td>' +
                '<td><span class="sw-salestrigger-status' + (this.IsMaintenanceExpired ? '-gray' : '-blue') + '">' + this.Version + '</span></td></tr>' + (this.IsMaintenanceExpired ? SW.Core.String.Format(maintenanceExpiredMessage, prepareOutgoingLink(URLmaintenanceDetails)) : '');
            content += row;
        });

        $('.sw-salestrigger-content table:first tbody', template).html(content);

        var btnDefaults = {
            id: SW.Core.Id(),
            type: 'primary',
            label: '@{R=Core.Strings.2;K=WEBJS_IB0_2;E=js}',
            cls: 'sw-salestrigger-download-updates'
        };

        // set links section
        $('.sw-salestrigger-links', template).html(
            SW.Core.String.Format(linkBlockTemplate, SW.Core.Widgets.Button(btnDefaults.label, { type: btnDefaults.type, id: btnDefaults.id, cls: btnDefaults.cls }), '') +
            SW.Core.String.Format(linkBlockTemplate, '<a class="sw-link" href="javascript:SW.Core.SalesTrigger.DontRemindMeAgain(\'Upgrade\');">&nbsp;@{R=Core.Strings;K=WEBJS_ZS0_10;E=js}</a>', ''));

        // fire up the dialog
        $(template).dialog({
            dialogClass: 'sw-upgrade-dialog',
            title: upgradeDetails.title,
            width: 700,
            modal: true,
            close: function (event, ui) {
                $(this).dialog('destroy').remove();
            }
        });
        $('#' + btnDefaults.id).click(function () {
            SW.Core.Services.postToTarget(
                URLcustomerPortalPua,
                { pua_data: upgradeDetails.upgradeRequestData }, true, true);
        });
    };

    SalesTrigger.ShowLicensePopupAsync = function () {
        SW.Core.Services.callWebService('/Orion/Services/SalesTrigger.asmx', 'GetLicenseModuleInfo', {}, function (r) {
            SalesTrigger.ShowLicensePopup(r);
        });
    };
    // License popup Handling.
    SalesTrigger.ShowLicensePopup = function (licenseDetails) {

        var template = $(dialogTemplate);

        $('.sw-salestrigger-wrapper', template).addClass('sw-salestrigger-license');

        // set the content of the paragraph at the top of the dialog
        $('.sw-salestrigger-content p:first', template).html('@{R=Core.Strings;K=WEBJS_ZS0_13;E=js}');

        // set the content of the table header row
        $('.sw-salestrigger-content table:first thead tr', template).html(
            '<th colspan=2>@{R=Core.Strings.2;K=WEBJS_LC0_10;E=js}</th>' +
            '<th>@{R=Core.Strings;K=WEBJS_ZS0_11;E=js}</th>');

        // set the table body
        var content = '';
        $.each(licenseDetails, function () {
            var subrows = '';
            var isLimitReached = false;
            //subrows
            $.each(this.ElementList, function () {
                if (this.IsLimitReached) {
                    isLimitReached = true;
                }

                var subRow = '<tr class="sw-salestrigger' + (this.IsLimitReached ? '-expired' : (this.Saturation > 79) ? '-expiring' : '') + ' sw-salestrigger-message">' +
                '<td></td>' +
                '<td>&nbsp;&nbsp;' + this.ElementType + '</label></td>' +
                '<td>' + SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_ZS0_14;E=js}', this.Saturation, this.Count, this.MaxCount) + '</td></tr>';

                subrows += subRow;
            });

            var row = '<tr class="sw-salestrigger' + (isLimitReached ? '-expiring-bold"' : '-expiring') + '">' +
                '<td colspan=3><label for="sw-sales-' + this.ModuleName + '">' + this.ProductDisplayName + '</label></td></tr>';

            content += row;
            content += subrows;
        });

        $('.sw-salestrigger-content table:first tbody', template).html(content);

        // hide links for non-admin user
        if (!SW.Core.Info.AllowAdmin) {
            $('.sw-salestrigger-adminonly', template).hide();
        } else {
            // set links section         
            $('.sw-salestrigger-links', template).html(
                SW.Core.String.Format(linkBlockTemplate, '<a class="sw-link" href="javascript:SW.Core.SalesTrigger.DontRemindMeAgain(\'Licensing\');">&#0187;&nbsp;@{R=Core.Strings;K=WEBJS_ZS0_10;E=js}</a>', '&nbsp;'));

            // set footer
            $('#sw-salestrigger-footer', template).html(SW.Core.String.Format(popupFooter, URLfooter));
        }

        //fire up the dialog
        $(template).dialog({
            title: '@{R=Core.Strings;K=LIBCODE_PCC_24;E=js}',
            width: 700,
            modal: true,
            close: function (event, ui) {
                $(this).dialog('destroy').remove();
            }
        });
    };

    var linkHandlerRegisteredUnlimitedLicense = false;

    SalesTrigger.UnlimitedLicenseButton = function (options) {
        var defaults = {
            type: 'secondary',
            label: '@{R=Core.Strings;K=WEBJS_ZS0_12;E=js}',
            cls: 'sw-salestrigger-unlimited-licenses'
        };

        var opt = $.extend({}, defaults, options || {});

        var html;
        html = SW.Core.Widgets.Button(opt.label, {
            type: opt.type,
            id: opt.id || SW.Core.Id(),
            cls: opt.cls
        });

        if (!linkHandlerRegisteredUnlimitedLicense) {
            $('body').on('click', '.sw-salestrigger-unlimited-licenses', function () {
                // todo, this is the place where the actual stuff will happen
            });

            linkHandlerRegisteredUnlimitedLicense = true;
        }

        return html;
    };

})(SW.Core);
