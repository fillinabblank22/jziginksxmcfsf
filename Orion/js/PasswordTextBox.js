﻿
function clearPasswordBox(passwordBoxId) {
    $("#" + passwordBoxId).val("");
}

function setPasswordBoxValue(passwordBoxId, mask) {
    if ($("#" + passwordBoxId).val().length === 0) {
        $("#" + passwordBoxId).val(mask);
    }
}

function clearPasswordKey(passwordBoxId, message) {
    if ($("#" + passwordBoxId).val().length > 0) {
        var res = confirm(message);
        if (res) {
            $("#" + passwordBoxId).val("");
        }
    }
    return false;
}