﻿/// <reference path="../typescripts/typings/jquery.d.ts" />
// TypeScript compiler adds "Core." to new Date() below if it's called in SW.Core namespace. This is a workaround.
var FileUploadDateType = Date;

var SW;
(function (SW) {
    (function (Core) {
        var AsyncFileUpload = (function () {
            function AsyncFileUpload() {
            }
            AsyncFileUpload.prototype.uploadFile = function (options, success) {
                var _this = this;
                var id = new FileUploadDateType().getTime();
                var formElement = $('<form name="uploadingForm' + id + '" id="uploadingForm' + id + '" style="position:absolute; top:-1200px; left:-1200px"></form>');
                formElement.attr({
                    'action': options.url,
                    'method': 'POST',
                    'target': 'uploadingFrame' + id,
                    'enctype': 'multipart/form-data'
                });

                var originalFileElement = $('#' + options.fileElementId);
                var uploadingPlaceholder = $('<div class="sw-asyncFileUpload-uploading"><img src="/Orion/images/loading_gen_small.gif" alt="uploading..." /> @{R=Core.Strings;K=AsyncFileUpload_UploadingFileLabel;E=js}</div>');

                // move file element to hidden form so that we can upload the file
                originalFileElement.after(uploadingPlaceholder).appendTo(formElement);

                formElement.appendTo('body');

                this.addExtraData(options, formElement);

                var iframeElement = this.createUploadIframe(id);

                formElement.submit();

                // move file element back so that it's still visible to user
                originalFileElement.insertBefore(uploadingPlaceholder);

                setTimeout(function () {
                    return _this.waitForUploadResponse(formElement, iframeElement, uploadingPlaceholder, success);
                }, 1000);
            };

            AsyncFileUpload.prototype.waitForUploadResponse = function (formElement, iframeElement, uploadingPlaceholder, success) {
                var _this = this;
                var iframe = iframeElement.get(0);
                var response = iframe.contentWindow.document.body.innerText;

                if (response === AsyncFileUpload.FreshIframeContent) {
                    setTimeout(function () {
                        return _this.waitForUploadResponse(formElement, iframeElement, uploadingPlaceholder, success);
                    }, 1000);
                    return;
                }

                iframeElement.remove();
                formElement.remove();

                uploadingPlaceholder.remove();

                success(JSON.parse(response));
            };

            AsyncFileUpload.prototype.createUploadIframe = function (id) {
                var iframeElement = $('<iframe id="uploadingFrame' + id + '" name="uploadingFrame' + id + '" style="position:absolute; top:-9999px; left:-9999px;" />');
                iframeElement.appendTo('body').contents().find('body').append(AsyncFileUpload.FreshIframeContent);
                return iframeElement;
            };

            AsyncFileUpload.prototype.addExtraData = function (options, formElement) {
                if (options.extraData) {
                    for (var key in options.extraData) {
                        if (!options.extraData.hasOwnProperty(key))
                            continue;

                        var dataElement = $('<input type="hidden" name="' + key + '" value="' + options.extraData[key] + '" />');
                        dataElement.appendTo(formElement);
                    }
                }
            };
            AsyncFileUpload.FreshIframeContent = "@FRESH_IFRAME@";
            return AsyncFileUpload;
        })();
        Core.AsyncFileUpload = AsyncFileUpload;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
