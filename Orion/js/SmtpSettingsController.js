SW.Core.namespace("SW.Core").SMTPServerSettings = function (config) {
    "use strict";

    var validHostnameOrIpRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^(?:(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){6})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:::(?:(?:(?:[0-9a-fA-F]{1,4})):){5})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){4})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,1}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){3})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,2}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){2})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,3}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:[0-9a-fA-F]{1,4})):)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,4}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,5}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,6}(?:(?:[0-9a-fA-F]{1,4})))?::))))$";

    var numericStringRegex = /^\d+$/;
    var addressForEdit;

    // Private methods

    function validateConfiguration(configuration) {
        var isValid = true;
        $container.find('[data-form="smtpServerIP"]').removeClass("invalidValue");
        $container.find('[data-form="smtpServerPort"]').removeClass("invalidValue");
        $container.find('[data-form="smtpServerUsername"]').removeClass("invalidValue");
        $container.find('[data-form="smtpServerPassword"]').removeClass("invalidValue");
        $container.find('[data-form="smtpServerPasswordConfirm"]').removeClass("invalidValue");
        $container.find('.hostnameIpExist').hide();
        if (configuration.Address != "") {
            if (configuration.Address != addressForEdit)
                $.each($('[data-form="smtpBackupServerID"]').find("option"), function () {
                    if ($(this).text() == configuration.Address) {
                        isValid = false;
                        $container.find('[data-form="smtpServerIP"]').addClass("invalidValue");
                        $container.find('.hostnameIpExist').show();
                    }
                });
            if (configuration.Address.match(validHostnameOrIpRegex) == null) {
                isValid = false;
                $container.find('[data-form="smtpServerIP"]').addClass("invalidValue");
            }
        } else {
            isValid = false;
            $container.find('[data-form="smtpServerIP"]').addClass("invalidValue");
        }
        if (configuration.Port != "" && parseInt(configuration.Port, 10) != "NaN" && parseInt(configuration.Port, 10) <= 65535) {
            if (!numericStringRegex.test(configuration.Port)) {
                isValid = false;
                $container.find('[data-form="smtpServerPort"]').addClass("invalidValue");
            }
        } else {
            isValid = false;
            $container.find('[data-form="smtpServerPort"]').addClass("invalidValue");
        }
        if (configuration.Credentials) {
            if (!configuration.Credentials.Username) {
                isValid = false;
                $container.find('[data-form="smtpServerUsername"]').addClass("invalidValue");
            }
            if (!configuration.Credentials.Password) {
                isValid = false;
                $container.find('[data-form="smtpServerPassword"]').addClass("invalidValue");
                $container.find('[data-form="smtpServerPasswordConfirm"]').addClass("invalidValue");
            }
            else if (configuration.Credentials.Password != $container.find('[data-form="smtpServerPasswordConfirm"]').val()) {
                isValid = false;
                $container.find('[data-form="smtpServerPasswordConfirm"]').addClass("invalidValue");
            }
        }
        if (isValid) {
            SW.Core.Services.callWebServiceSync("/Orion/Services/SMTPServers.asmx", "ValidateSmtpServer", { config: configuration }, function (result) {
                for (var key in result) {
                    if (!result[key] && key != 'smtpServerExist') {
                        $container.find('[data-form="' + key + '"]').addClass("invalidValue");
                        isValid = false;
                    }
                }
                if (config.mode != 'AlertSettingsEdit') {
                    if (result['smtpServerExist'] && configuration.Address != addressForEdit) {
                        isValid = false;
                        $container.find('[data-form="smtpServerIP"]').addClass("invalidValue");
                        $container.find('.hostnameIpExist').show();
                    }
                }
            });
        }
        return isValid;
    }

    function isEmail(email) {
        var regex = new RegExp('^[A-Za-z0-9\._+%-~/&\*\'\?\!\#\^\{\}]+@[A-Za-z0-9\.-]+\.[A-Za-z]{2,4}(;\\n*[A-Za-z0-9\._%-~/&\*\'\?\!\#\^\{\}]+@[A-Za-z0-9\.-]+\.[A-Za-z]{2,4})*;?$');
        return regex.test(email);
    }

    var createConfiguration = function () {
        var configuration = {
            ServerID: $container.find('[data-form="smtpServerID"]').val(),
            Address: $container.find('[data-form="smtpServerIP"]').val(),
            BackupServerID: $container.find('[data-form="smtpBackupServerID"]').val(),
            Port: $container.find('[data-form="smtpServerPort"]').val(),
            EnableSSL: $container.find('[data-form="smtpServerEnableSSL"] input[type=checkbox]').is(':checked'),
            IsDefault: $container.find('[data-form="isDefault"]').val()
        };
        if ($container.find('[data-form="smtpServerUseCredentials"] input[type=checkbox]').is(':checked')) {
            configuration.Credentials = {
                Username: $container.find('[data-form="smtpServerUsername"]').val(),
                Password: $container.find('[data-form="smtpServerPassword"]').val(),
                Name: $container.find('[data-form="smtpServerIP"]').val()
            };
            $container.find('[data-form="isPswChanged"]').val("true");
        }

        return configuration;
    };

    var createrEmailConfiguration = function (emailFrom, emailTo, emailCC, emailBCC) {
        var smtpServer = createConfiguration();
        var configuration = {
            EmailFrom: emailFrom,
            EmailTo: emailTo,
            EmailCC: emailCC,
            EmailBCC: emailBCC,
            SmtpServer: smtpServer
        };
        return configuration;
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        $('#testResult').empty();
        $('#testProgress').show();

        SW.Core.Services.callController("/api/Email/" + action, param,
            // On success
            function (response) {
                $('#testProgress').hide();
                if (response == true) {
                    $('#testResult').html('<div class="sw-suggestion sw-suggestion-pass" style="margin-bottom: 10px;margin-left: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings;K=WEBJS_AY0_6;E=js}</div>');
                } else {
                    $('#testResult').html('<div class="sw-suggestion sw-suggestion-fail" style="margin-bottom: 10px;margin-left: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings;K=WEBJS_AY0_7;E=js}</div>');
                }

                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
            // On error
            function (msg) {
                $('#testProgress').hide();
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    function validateSendTestEmailConfiguration(configuration) {
        var isValid = validateConfiguration(configuration.SmtpServer);
        if (!isValid) { return isValid; }
        if (config.mode === 'Inline' || config.mode === 'AlertSettingsEdit') {
            if (!isEmail($.trim(configuration.EmailTo))) {
                isValid = false;
            }
            if (!isEmail($.trim(configuration.EmailFrom))) {
                isValid = false;
            }

            if (configuration.EmailCC && configuration.EmailCC.length != 0 && !isEmail($.trim(configuration.EmailCC))) {
                isValid = false;
            }
            if (configuration.EmailBCC && configuration.EmailBCC.length != 0 && !isEmail($.trim(configuration.EmailBCC))) {
                isValid = false;
            }
            if (!isValid) {
                Ext.Msg.show({
                    title: "@{R=Core.Strings;K=WEBJS_PS0_7; E=js}",
                    msg: "@{R=Core.Strings;K=WEBJS_VM0_6; E=js}",
                    icon: Ext.Msg.WARNING, buttons: Ext.Msg.OK
                });
            }
        }
        if (configuration.EmailFrom === undefined || configuration.EmailTo === undefined) {
            isValid = false;
            if (config.mode === 'New' || config.mode === 'Edit') {
                $smtpSettingsMissingFromToDialog.dialog('open');

                $smtpSettingsMissingFromToDialog.find('.smtp-settings-send-btn').on('click', function () {
                    var emailConfig = createrEmailConfiguration();
                    if (validateSendTestEmailDialogConfiguration(emailConfig)) {
                        callActionAndExecuteCallback('TestSmtpCredentials', emailConfig, null);
                        $smtpSettingsMissingFromToDialog.dialog('close');
                    }
                });

                $smtpSettingsMissingFromToDialog.find('.smtp-settings-cancel-btn').on('click', function () {
					$('[data-form="smtpSettingsEmailTo"]').siblings('.emailInputField').find(".emailItem img").click();
					$('[data-form="smtpSettingsEmailFrom"]').siblings('.emailInputField').find(".emailItem img").click();
                    $smtpSettingsMissingFromToDialog.dialog('close');
                });
            }
        }
        return isValid;
    }

    function validateSendTestEmailDialogConfiguration(configuration) {
        var isValid = validateConfiguration(configuration.SmtpServer);;

        $('[data-form="smtpSettingsEmailTo"]').siblings('div').removeClass("invalidValue");
        $('[data-form="smtpSettingsEmailFrom"]').siblings('div').removeClass("invalidValue");

        if (!isEmail($.trim($('[data-form="smtpSettingsEmailTo"]').val()))) {
            $('[data-form="smtpSettingsEmailTo"]').siblings('div').addClass("invalidValue");
            isValid = false;
        }
        if (!isEmail($.trim($('[data-form="smtpSettingsEmailFrom"]').val()))) {
            $('[data-form="smtpSettingsEmailFrom"]').siblings('div').addClass("invalidValue");
            isValid = false;
        }
        if (isValid) {
            configuration.EmailTo = $('[data-form="smtpSettingsEmailTo"]').val();
            configuration.EmailFrom = $('[data-form="smtpSettingsEmailFrom"]').val();
        }
        return isValid;
    }

    function createSmtpSettingsMissingEmailFromToDlg() {
        return $("#smtpSettingsEmailFromToDialog").dialog({
            title: '@{R=Core.Strings;K=WEBJS_VM0_5; E=js}',
            autoOpen: false,
            modal: true,
            width: 400,
            close: function () {
                $smtpSettingsMissingFromToDialog.find('.smtp-settings-send-btn').off('click');
                $smtpSettingsMissingFromToDialog.find('.smtp-settings-cancel-btn').off('click');
            }
        });
    }

    // Public methods

    this.init = function () {
        $container = $('#' + config.containerID);
        addressForEdit = $container.find('[data-form="smtpServerIP"]').val();
        $container.find('[data-form="smtpServerID"]').change(function () {
            if ($container.find('[data-form="smtpServerID"]').val() == -1)
                $container.find('[data-form="smtpServerSettings"]').show();
            else
                $container.find('[data-form="smtpServerSettings"]').hide();
        });
        $container.find('[data-form="smtpServerEnableSSL"] input[type=checkbox]').change(function () {
            if ($container.find('[data-form="smtpServerEnableSSL"] input[type=checkbox]').is(':checked'))
                $container.find('[data-form="smtpServerPort"]').val('465');
            else
                $container.find('[data-form="smtpServerPort"]').val('25');
        });
        $container.find('[data-form="smtpServerUseCredentials"] input[type=checkbox]').change(function () {
            if ($container.find('[data-form="smtpServerUseCredentials"] input[type=checkbox]').is(':checked')) {
                $container.find('[data-form="smtpServerCredsContainer"]').show();
                $container.find('[data-form="isPswChanged"]').val("true");
            }
            else
                $container.find('[data-form="smtpServerCredsContainer"]').hide();
        });
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback();
        }

        $smtpSettingsMissingFromToDialog = createSmtpSettingsMissingEmailFromToDlg();
    };

    this.validate = function () {
        var configuration = createConfiguration();
        return validateConfiguration(configuration);
    };

    this.getSmtpServerAsync = function () {
        return createConfiguration();
    };

    this.testSmtpCreadentials = function (emailFrom, emailTo, emailCC, emailBCC) {
        var configuration = createrEmailConfiguration(emailFrom, emailTo, emailCC, emailBCC);
        if (validateSendTestEmailConfiguration(configuration))
            callActionAndExecuteCallback("TestSmtpCredentials", configuration, null);
    };

    this.refreshSmtpServersList = function (ddSmtpServerlist) {

        var isPrimarySmtpServerDd = ddSmtpServerlist.attributes['data-form'].value == "smtpServerID";

        // Refresh if selected "Refres this list" item
        if (ddSmtpServerlist.value === "-2") {
            SW.Core.Services.callWebServiceSync("/Orion/Services/SMTPServers.asmx",
                "GetSmtpServersForDropDown", { isPrimarySmtpServerDd: isPrimarySmtpServerDd },
                function (result) {
                    //clear dropdown data source
                    ddSmtpServerlist.options.length = 0;

                    //bind refreshed smtp server list
                    $.each(result, function (key, value) {
                        var option = document.createElement('option');
                        option.value = value.id;
                        option.innerHTML = value.name;
                        ddSmtpServerlist.options.add(option);
                    });
                });
        }
    };

    this.createSMTPServer = function () {
        var configuration = createConfiguration();
        var id;
        SW.Core.Services.callWebServiceSync("/Orion/Services/SMTPServers.asmx",
            "AddSmtpServer", { smtpServer: configuration },
            function (result) {
                id = result;
            });
        return id;
    };

    // Constructor

    var self = this;
    var $container = null;
    var $smtpSettingsMissingFromToDialog = null;
}
