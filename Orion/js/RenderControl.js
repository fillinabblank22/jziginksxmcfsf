﻿(function (core) {
    //
    // NOTE! requires changes to SolarWinds.Orion.Web.UI.Localizer.LocalizerHttpHandler.ProcessFile
    // after: PatchFile(writer, physcalPath, extension, true);
    // add: if (extension == ".js") writer.WriteLine("/**/{0}if(window.SW&&SW.Core&&SW.Core.Loader)SW.Core.Loader._cbLoaded('jsfile',{{url:'{0}'}});", Environment.NewLine, resource);
    //        
    
    var loader = core.Loader,
        hookee = null,
        listeners = {},
        loadedScripts = [],
        loadingScripts = [],
        isUrlRooted = /^(\w+:|\/).*$/,
        rootUrl = function(url) {
            if (!isUrlRooted.test(url)) {
                url = '/orion/' + url;
            }
            
            return url;
        },
        geturl = function(url) {
            var rx = /^(?:https?:\/\/[^\/]+)?(\/.*?)(?:\.i18n\.ashx|$|[?])/i,
                m = url.match(rx);
            return (m && m[1] || url).toLowerCase();
        },
        geturlaxd = function(url) {
            var rx = /^(?:https?:\/\/[^\/]+)?(\/.*)/i,
                m = url.match(rx);
            return (m && m[1] || url).toLowerCase();
        },
        onJsLoadFinished = function(url) {
            var id = geturl(url);

            var found = listeners[id];
            if (!found) return;

            var idx = loadingScripts.indexOf(url);
            if (idx >= 0) {
                loadingScripts.splice(idx, 1);
            }

            $('script[src*="' + url + '"]').attr('loading', 'false'); // js file loaded, so turn off the loading flag
            listeners[id] = null;
            for (var i = 0, imax = found.length; i < imax; ++i)
                found[i]();
        },
        newfile = function(framework, opts) {
            hookee(framework, opts);
                
            if (framework !== 'jsfile') return;
            onJsLoadFinished(opts.url);
        },

        hook = function() {
            if (hookee) return;
            hookee = loader._cbLoaded;
            loader._cbLoaded = newfile;
        },

        listen = function(url, resume) {
            if (!resume) return;
            hook();
            var id = geturl(url), item = listeners[id] || (listeners[id] = []);
            item.push(resume);
        };

    // -- public
    loader.Css = function (url, resume) {
        var found = null;
        var seek = geturl(url);

        $('link[rel*="style"]').each(function () {
            if (seek === geturl(this.href)) {
                found = this;
                return false;
            }
            return true;
        });

        if (!found) {
            // todo: fetch the stylesheet ordinal
            // we cannot use jQuery for modifiing head since it doesn't work with IE7,8, see http://bugs.jquery.com/ticket/6105
            var headElement = document.getElementsByTagName("head")[0];
            var cssNode = document.createElement('link');
            cssNode.type = 'text/css';
            cssNode.rel = 'stylesheet';
            cssNode.href = url;
            headElement.appendChild(cssNode);
            // todo: wait for the css file to load (look for # of stylesheet rules)
        }

        if (resume)
            resume();
    };

    loader.Js = function(url, resume) {
        var found = false;
        
        // check if the url is rooted (either absolute, or starts with /)
        // since the rendered urls are relative to RenderControl.aspx (and not the baseurl of this page),
        // we need to make them absolute (so, prepend /orion/)
        url = rootUrl(url);
        
        var seek = geturl(url);
        if (/\.axd$/.test(seek)) {
            seek = geturlaxd(url);
            
            $('script[src*=".axd"]').each(function() {
                if (seek === geturlaxd(this.src)) {
                    found = true;
                    return false;
                }
                return true;
            });
        } else {
            $('script[src*=".js"]').each(function() {
                if (seek === geturl(this.src)) {
                    found = true;
                    return false;
                }
                return true;
            });
        }

        if (!found) {
            found = loadedScripts.indexOf(seek) >= 0;
        }

        if (!found) {
            if (loadingScripts.indexOf(seek) >= 0) {
                if (resume) {
                    listen(geturl(url), resume);
                    return;
                }
            }

            loadingScripts.push(seek);
            LoadCachedVersion(url).then(function() {
                loadedScripts.push(seek);
                onJsLoadFinished(url);
                if (resume) resume();
            });
        } else if (resume) {
            resume();
        }
    };

    function LoadCachedVersion(url, options) {
        // Allow user to set any option except for dataType, cache, and url
        options = $.extend(options || {}, {
            dataType: "script",
            cache: true,
            url: url
        });

        // Use $.ajax() since it is more flexible than $.getScript
        // Return the jqXHR object so we can chain callbacks
        return jQuery.ajax(options);
    }

    loader.Control = function (target, params, data, mode, errorHandler, callbackFunction) {

        var tgtselector = target,
            src,
            txtmode = {
                replace: 'replace',
                append: 'append',
                prepend: 'prepend',
                after: 'after'
            }[mode] || 'replace';
            
        // fix all relative, non-rooted URLs in the control
        var fixUrls = function(items) {
            
            $.each(items.find('a'), function(index, anchor) {
                var href = anchor.getAttribute('href');
                if (href) {
                    anchor.setAttribute('href', rootUrl(href));
                }
            });
        }

        var loadcontent = function () {
            var tgt = $(tgtselector);
            var js = src.find('script[type="text/sw-javascript"]');
            var viewstate = src.find('script[type="text/sw-viewstate"]');
            var viewstateGenerator = src.find('script[type="text/sw-viewstate-generator"]');
            var items = src.children().not(js).not(viewstate).not(viewstateGenerator);
            
            var newResourceWrapper = items.find('.ResourceWrapper');
            newResourceWrapper.data('sw-viewstate', viewstate.text());
            newResourceWrapper.data('sw-viewstate-generator', viewstateGenerator.text());
            
            fixUrls(items);

            if (txtmode === 'replace') {
                if (window.angular && window.angular.element(tgt)) {
                    // If target was compiled against $new scope, we need to destroy it, when doing the replace.
                    // Otherwise it introduces memory leak
                    var initialScope = tgt.data("initialScope");
                    if (initialScope) {
                        initialScope.$destroy();
                    }

                    // use angular to destroy previous content to properly trigger angular $destroy events
                    window.angular.element(tgt).empty();
                }
                tgt.replaceWith(items);
            }
            else if (txtmode === 'after') tgt.after(items);
            else if (txtmode === 'prepend') items.prependTo(tgt);
            else items.appendTo(tgt);

            var pos;
            var posmax = js.length;

            for (pos = 0; pos < posmax; ++pos) {
                var scriptHtml = $(js[pos]).html().trim();
                
                // ok - IE11 does not survive if the js for eval is in '<!-- -->' (it survives it when directly inserted into page. bugs me.)
                // so, remove it.
                
                // i can't use startsWith()/endsWith() in ie.. 
                if (scriptHtml.substring(0, 4) == "<!--")
                    scriptHtml = scriptHtml.substring(4);
                    
                if (scriptHtml.substring(scriptHtml.length - 3, scriptHtml.length) == "-->")
                    scriptHtml = scriptHtml.substring(0, scriptHtml.length - 3);
                
                $.globalEval(scriptHtml);
            }
        };

        var loadjs = function () {
            var $js = src.find('script[type="text/sw-javascript"]');

            for (var pos = 0; pos < $js.length; ++pos) {
                var $item = $js.eq(pos);
                var xsrc = $item.attr('swsrc');
                if (!xsrc) continue;

                $item.remove();
                loader.Js(xsrc, arguments.callee);
                return;
            }

            loadcontent();
        };

        var loadcss = function () {
            var $css = src.find('script[type="text/sw-stylesheet"]');

            if ($css.length) {
                var $item = $css.eq(0);
                var xsrc = $item.attr('swsrc');
                $item.remove();
                loader.Css(xsrc, arguments.callee);
                return;
            }

            loadjs();
        };
        
        var dataSerialized = data;
        if (typeof dataSerialized !== 'string') // i haven't found anyone sending anything meaningful here, just empty object, but to be sure.
            dataSerialized = JSON.stringify(data);

        // we need to grab the xhr for checks for redirect
        var _originalXhr = $.ajaxSettings.xhr;
        var ajaxXhr;
        
        $.ajax({
            type: 'POST',
            url: '/orion/rendercontrol.aspx?' + $.param(params),
            data: dataSerialized,
            dataType: 'html',
            success: function (results) {
                // [pc] if the session has timed out, the entire page is dead. let's give the user a way to fix that.
                if (results.indexOf('loginsig(09470EE361E54F3DB730000176BA16B2)') >= 0) {
                    if (typeof errorHandler != 'undefined')
                        errorHandler('session.timeout');
                    return;
                }
                
                var $resourceContainer = $('<div />');

                // If angular is available $compile the html result.
                if (window.angular && window.angular.element(document.body).injector()) {                   
                    window.angular.element(document.body)
                        .injector()
                        .invoke(["$compile", "$rootScope", function ($compile, $rootScope) {
                            var childScope = $rootScope.$new();
                            var compiledResults = $compile(results)(childScope);
                            $resourceContainer.append(compiledResults);
                            // Saving scope, to be properly destroyed later
                            $resourceContainer.children(".ResourceWrapper").data("initialScope", childScope);
                        }]);
                } else {
                    $resourceContainer.html(results);
                }

                src = $resourceContainer;
                loadcss();

                if (callbackFunction)
                    callbackFunction();
            },
            error: function () {
                if (errorHandler)
                    errorHandler();
            }
        });
    };
})(SW.Core);

