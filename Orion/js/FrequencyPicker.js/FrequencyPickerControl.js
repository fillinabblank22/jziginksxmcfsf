Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
SW.Orion.FrequencyPickerControl = function (config) {
    var self;

    return {
        init: function () {
            self = this;
            SW.Orion.ScheduleConfigurationController = SW.Orion.ScheduleConfigurationControl({
                useTimePeriodMode: config.useTimePeriodMode,
                frequencyTypes: config.frequencyTypes
            });
            SW.Orion.ScheduleConfigurationController.init();
        },

        ShowMigrationWarning: function (value) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_AB0_27; E=js}', msg: String.format('@{R=Core.Strings;K=WEBJS_AB0_28; E=js}', value), minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING });
        },

        CollectDateTimeJson: function (elem) {
            var dateArray = new Array();
            var intervalStartTimeArray = new Array();
            var intervalEndTimeArray = new Array();
            var resultJson = new Array();
            var intervalEndJson = new Array();
            var freqIdsJson = new Array();

            if (!config.useTimePeriodMode) {
                $.each($(elem).find(".timePickers").find(".datePicker"), function () {
                    if ($(this).is(":visible"))
                        dateArray.push($(this).val());
                    else {
                        $(this).datepicker('setDate', new Date());
                        dateArray.push($(this).val());
                    }
                });
                $.each($(elem).find(".timePickers").find(".timePicker"), function () {
                    intervalStartTimeArray.push($(this).val());
                    freqIdsJson.push($(this).closest(".timeSelector").attr("frequency_Id"));
                });
                for (var i = 0; i < dateArray.length; i++) {
                    resultJson.push({ Key: i, Value: dateArray[i] + " " + intervalStartTimeArray[i] });
                }
                $($(elem).find(".timePickers input[type='hidden']")[0]).val(JSON.stringify(resultJson));
                $($(elem).find(".timePickers input[type='hidden']")[2]).val(JSON.stringify(freqIdsJson));
            } else {
                $.each($(elem).find(".timeSelector"), function () {
                    dateArray.push(new Date());
                    intervalStartTimeArray.push($($(this).find(".timePicker")[0]).val());
                    intervalEndTimeArray.push($($(this).find(".timePicker")[1]).val());
                    freqIdsJson.push($(this).attr("frequency_Id"));
                });
                for (var j = 0; j < dateArray.length; j++) {
                    resultJson.push({ Key: j, Value: dateArray[j].toDateString() + " " + intervalStartTimeArray[j] });
                    intervalEndJson.push({ Key: j, Value: dateArray[j].toDateString() + " " + intervalEndTimeArray[j] });
                }

                $($(elem).find(".timePickers input[type='hidden']")[0]).val(JSON.stringify(resultJson));
                $($(elem).find(".timePickers input[type='hidden']")[1]).val(JSON.stringify(intervalEndJson));
                $($(elem).find(".timePickers input[type='hidden']")[2]).val(JSON.stringify(freqIdsJson));
            } 
        },

        GetFrequenciesObject: function(elem) {
            
            var scheduleObject = SW.Orion.ScheduleConfigurationController.GetSchedulesObject(elem);
            return scheduleObject;
        },

        Validate: function () {
            var result = true;
            if ($(".noFrequenciesBtnContainer").is('visible')) {
                Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_AB0_55; E=js}', msg: '@{R=Core.Strings;K=WEBJS_AB0_54; E=js}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                result = false;
            }
            if (result) {
                $.each($("div.frequencySelection"), function () {
                    var currentFriquency = this;
                    result = result && SW.Orion.ScheduleConfigurationController.ValidateSchedule(currentFriquency);
                });
            }
            return result;
        }
    };
}