Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
SW.Orion.ScheduleConfigurationControl = function (config) {

    var self = this;
    var RIGHT_NOW_INDEX = "0";
    var SPECIFIC_DAY_INDEX = "1";
    
    function getFrequencyTypeClientId(elem, type) {
        var typeDiv = $(elem).parent().find(".frequencyBoxes div");
        var div;
        switch (type) {
            case config.frequencyTypes.once:
                div = $(typeDiv)[0];
                break;
            case config.frequencyTypes.daily:
                div = $(typeDiv)[1];
                break;
            case config.frequencyTypes.weekly:
                div = $(typeDiv)[2];
                break;
            case config.frequencyTypes.monthly:
                div = $(typeDiv)[3];
                break;
        }
        return div;
    }

    function validateDateTimes(elem) {
        var result = true;
        var $timeControls = $(elem).find(".timePicker");
        var $dateControls = $(elem).find(".datePicker");
        $.each($dateControls, function () {
            if ($(this).is(":visible")) {
                if (!$.fn.validateDateField(this)) {
                    result = false;
                    return false;
                }
            }
        });
        if (!result) {
            return false;
        }
        if (JSON.parse($('#timeFormat').val()).show24Hours) {
            setDafultTimes(elem, "00:01", ".specStartDateTime");
            setDafultTimes(elem, "23:59", ".endTimePicker");
        } else {
            setDafultTimes(elem, "12:01 AM", ".specStartDateTime");
            setDafultTimes(elem, "11:59 PM", ".endTimePicker");
        }

        $.each($timeControls, function () {
            if ($(this).is(":visible")) {
                if (!$.fn.validateTimeField(this)) {
                    result = false;
                    return false;
                }
            }
        });
        return result;
    }

    function setDafultTimes(elem, defaultTime, elementClass) {
        if ($(elem).closest(".frequencySelection").find(elementClass).find('.timePicker').attr("customplaceholder") ==
            $(elem).closest(".frequencySelection").find(elementClass).find('.timePicker').val()) {

            $(elem).closest(".frequencySelection").find(elementClass).find('.timePicker').val(defaultTime);
        }
    }

    function extractDateTimesFromJson(elem) {
        if ($($(elem).find(".timePickers input[type='hidden']")[0]).val() != "") {
            var dateTimeJson = JSON.parse($($(elem).find(".timePickers input[type='hidden']")[0]).val());
            var freqIds = JSON.parse($($(elem).find(".timePickers input[type='hidden']")[2]).val());
            for (var i = 0; i < dateTimeJson.length; i++) {
                callAddTimeControl(dateTimeJson[i].Value, $(elem).closest(".schedulePickerControl"), freqIds[i] ? freqIds[i] : 0);
            }
        }
    }
    
    function extractTimeIntervalFromJson(elem) {
        if (($($(elem).find(".timePickers input[type='hidden']")[0]).val() != "") && ($($(elem).find(".timePickers input[type='hidden']")[1]).val() != "")) {
            var dateTimeJson = JSON.parse($($(elem).find(".timePickers input[type='hidden']")[0]).val());
            var intervalEndTimeJson = JSON.parse($($(elem).find(".timePickers input[type='hidden']")[1]).val());
            var freqIds = JSON.parse($($(elem).find(".timePickers input[type='hidden']")[2]).val());
            if (intervalEndTimeJson.length == dateTimeJson.length) {
                for (var i = 0; i < intervalEndTimeJson.length; i++) {
                    callAddTimePeriodControl(dateTimeJson[i].Value, intervalEndTimeJson[i].Value, $(elem).closest(".schedulePickerControl"), freqIds[i] ? freqIds[i] : 0);
                }
            }
        }
    }
    var uniqId = 0;

    function callAddTimePeriodControl(intervalStartTime, intervalEndTime, elem, freqId) {
        if (!freqId) {
            freqId = 0;
        }
        if (!intervalStartTime) {
            intervalStartTime = new Date().toLocaleString();
        }
        if (!intervalEndTime) {
            intervalEndTime = new Date().toLocaleString();
        }
        if (!$(elem).hasClass("schedulePickerControl")) {
            elem = $(elem).closest(".schedulePickerControl");
        }
        var containerDataIndex = $(elem).attr("data-index");
        var selector = String.format(".{0}[data-index='{1}'] {2}", $(elem).attr("class").split(' ')[0], containerDataIndex, ".TimeControl");
        SW.Core.Loader.Control(selector, {
            Control: "Controls/TimePeriodSelectionControl.ascx",
            NamingContainer: "timeCont" + uniqId
        }, {
            'config': {
                "Time": intervalStartTime,
                "IntervalEndTime": intervalEndTime,
                "FrequencyId": freqId
            }
        },
            'append', function () {
            }, function () {
                var allDaySection = $(selector).find(".timeSelector").find(".allDaySection");
                if ($(selector).find(".timeSelector").length == 1) {
                    allDaySection.show();
                    $(selector).find(".timeSelector").attr("disable", false);
                    $(selector).find(".timeSelector").find(".crossButton").hide();
                } else {
                    allDaySection.hide();
                    $(selector).find(".timeSelector").find(".crossButton").show();
                    $(selector).find(".timeSelector").first().find(".timePicker").attr("disabled", false);
                }
                $.fn.attachDateTimeValidation($(selector).find(".timeSelector").last());
            });
        uniqId++;
    };

    function callAddTimeControl(time, elem, freqId) {
        if (!freqId) {
            freqId = 0;
        }
        if (!time) {
            time = new Date().toLocaleString();
        }
        if (!$(elem).hasClass("schedulePickerControl")) {
            elem = $(elem).closest(".schedulePickerControl");
        }
        var containerDataIndex = $(elem).attr("data-index");
        var selector = String.format(".{0}[data-index='{1}'] {2}", $(elem).attr("class").split(' ')[0], containerDataIndex, ".TimeControl");
        var isOnce = $(elem).find(".friqSelect option:selected").val() == "Once";
        SW.Core.Loader.Control(selector, {
            Control: "Controls/TimeSelectionControl.ascx",
            NamingContainer: "timeCont" + uniqId
        }, {
            'config': {
                "Time": time,
                "IsOnce": isOnce,
                "FrequencyId": freqId
            }
        },
            'append', function () {
            }, function () {
                if ($(selector).find(".timeSelector").length == 1) {
                    $(selector).find(".timeSelector").find(".crossButton").hide();
                } else {
                    $(selector).find(".timeSelector").find(".crossButton").show();
                }
                $.fn.attachDateTimeValidation($(selector).find(".timeSelector").last());
            });
        uniqId++;
    };
    
    return {
        init: function () {
            self = this;
        },
        ValidateSchedule: function (currentFriquency) {
            var result = true;
            $(currentFriquency).find(".invalidDayValidationMessage").hide();
            result = result && validateDateTimes(currentFriquency);
            if (result) {
                var type = $(currentFriquency).find(".friqSelect").val();
                switch (type) {
                    case config.frequencyTypes.once:
                        result = result && true;
                        break;
                    case config.frequencyTypes.daily:
                        result = result && true;
                        break;
                    case config.frequencyTypes.weekly:
                        if ($($(currentFriquency).find(".cbl")[0]).find('input:checkbox:checked').length == 0) {
                            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_AB0_24; E=js}', msg: '@{R=Core.Strings;K=WEBJS_AB0_22; E=js}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            result = false;
                        } else {
                            result = result && true;
                        }
                        break;
                    case config.frequencyTypes.monthly:
                        if ($($(currentFriquency).find(".cbl")[1]).find('input:checkbox:checked').length == 0) {
                            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_AB0_25; E=js}', msg: '@{R=Core.Strings;K=WEBJS_AB0_23; E=js}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            result = false;
                        } else {
                            if ($(currentFriquency).find(".monthlyCondionsTable").find("input:radio:first:checked").length == 1) {
                                var errorPattern = '@{R=Core.Strings;K=WEBJS_AB0_52; E=js}';
                                var monthsNamesArray = new Array();
                                var array = new Array();
                                $.each($($(currentFriquency).find(".cbl")[1]).find('input:checkbox:checked'), function () {
                                    var lastDay = new Date(new Date().getFullYear(), $(this).val(), 0).getDate();
                                    var pickedDay = $(currentFriquency).find(".invalidDayValidationMessage").parent().find("select").val();
                                    if (lastDay < pickedDay)
                                        monthsNamesArray.push($(this).parent().text());
                                    array.push(lastDay);
                                });
                                if (monthsNamesArray.length > 0) {
                                    $(currentFriquency).find(".invalidDayValidationMessage").text(String.format(errorPattern, monthsNamesArray.join())).show();
                                    result = false;
                                } else {
                                    $(currentFriquency).find(".invalidDayValidationMessage").hide();
                                    result = result && true;
                                }
                            }
                        }
                        break;
                }
            }
            return result;
        },

        CallAddTimePeriodControl: function (intervalStartTime, intervalEndTime, elem) {
            callAddTimePeriodControl(intervalStartTime, intervalEndTime, elem);
        },
        CallAddTimeControl: function (time, elem) {
            callAddTimeControl(time, elem);
        },
        UpdateControlWithSelections: function () {
            $.each($('.frequencySelection'), function () {
                $.each($(this).find('.startTimePicker'), function () { self.StartTimeSelectionChange(this); });
                $.each($(this).find('.endTimeCheckBox input:checkbox'), function () { self.CheckEndTimeSpec(this); });
                $.each($(this).find('.friqSelect'), function () { self.DropdownSelectionChange(this); });
                $.each($(this).find(".monthlyCondionsTable").find("input:radio:checked"), function () { self.RadioButtonStateChange(this); });
                if (config.useTimePeriodMode) {
                    extractTimeIntervalFromJson(this);
                } else {
                    extractDateTimesFromJson(this);
                }
            });
        },

        ShowMigrationWarning: function (value) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_AB0_27; E=js}', msg: String.format('@{R=Core.Strings;K=WEBJS_AB0_28; E=js}', value), minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING });
        },

        CheckALL: function (elem) {
            $(elem).closest(".chackboxTable").find("input:checkbox:not(:checked)").click(); //.attr('checked', true);
        },

        CheckNone: function (elem) {
            $(elem).closest(".chackboxTable").find("input:checkbox:checked").click(); //.attr('checked', false);
        },
        DropdownSelectionChange: function (elem) {
            var selectedValue = $(elem).find("option:selected").val();
            var divElem = getFrequencyTypeClientId(elem, selectedValue);
            $(divElem).show();
            if (selectedValue == "Once") {
                $(divElem).closest(".frequencySelection").find(".startEndTimePicker").hide();
                $.each($(divElem).closest(".frequencySelection").find(".forOnceDateField"), function () { $(this).show(); });
            } else {
                $(divElem).closest(".frequencySelection").find(".startEndTimePicker").show();
                $.each($(divElem).closest(".frequencySelection").find(".forOnceDateField"), function () { $(this).hide(); });
            }
            if ((selectedValue == "Daily" || selectedValue == "Monthly") && $(divElem).find("input:radio:checked").length == 0)
                $($(divElem).find("input:radio")[0]).attr('checked', 'checked');
            var deselectedValues = $(elem).find("option:not(:selected)");
            $.each(deselectedValues, function () {
                $(getFrequencyTypeClientId(elem, $(this).val())).hide();
            });
            
            if ($(divElem).closest(".frequencySelection").find('.timeGrid tr').length > 3) {
                $(divElem).closest(".frequencySelection").find('.bigButton').css('margin-right', '-5px');
            }
        },
        RadioButtonStateChange: function (elem) {
            $.each($(elem).closest(".monthlyCondionsTable").find("input:radio"), function () {
                $(this).attr('checked', false);
            });
            $(elem).attr('checked', 'checked');
            $($(elem).closest(".monthlyCondionsTable").find("select")[1]).click(function () {
                if ($(this).val() == "Fifth") {
                    $($(elem).closest(".monthlyCondionsTable").find("select")[2]).click(function () {
                        $(elem).closest(".monthlyCondionsTable").find(".weekRiskMessage").text(String.format('@{R=Core.Strings;K=WEBJS_AB0_53; E=js}', $(this).find("option:selected").text()));
                        $(elem).closest(".monthlyCondionsTable").find(".weekRiskMessage").show();
                    });
                    $($(elem).closest(".monthlyCondionsTable").find("select")[2]).click();
                } else {
                    $($(elem).closest(".monthlyCondionsTable").find("select")[2]).unbind("click");
                    $(elem).closest(".monthlyCondionsTable").find(".weekRiskMessage").hide();
                }
            });
            $($(elem).closest(".monthlyCondionsTable").find("select")[1]).click();
        },
        StartTimeSelectionChange: function (elem) {
            var selectedValue = $(elem).find("option:selected").val();
            switch (selectedValue) {
                case "RunNow":
                    $(elem).parent().find(".specStartDateTime").hide();
                    break;
                default:
                    $(elem).parent().find(".specStartDateTime").show();
            }
        },
        CheckEndTimeSpec: function (elem) {
            if ($(elem).attr('checked')) {
                $(elem).closest(".frequencySelection").find(".endTimePicker").show();
            } else {
                $(elem).closest(".frequencySelection").find(".endTimePicker").hide();
            }
        },

        GetSchedulesObject: function(elem) {
            var result = {};
            
            result.frequenciesIdsJson = new Array();
            result.dateTimeJson = new Array();

            result.frequenciesIdsJson = $($(elem).find(".timePickers input[type='hidden']")[2]).val();
            result.dateTimeJson = $($(elem).find(".timePickers input[type='hidden']")[0]).val();
            if (config.useTimePeriodMode) {
                result.intervalEndTimeJson = JSON.parse($($(elem).find(".timePickers input[type='hidden']")[1]).val());
            }
            result.UseTimePeriodMode = config.useTimePeriodMode;
            result.selectFrequency = $(elem).find('.friqSelect option:selected').val();
            result.StartDateDropdown = $(elem).find('.startTimePicker option:selected').index();
            result.endTimeIsSpecified = $(elem).find('.endTimeCheckBox input:checkbox').is(":checked");

            result.frequencyName = $(elem).find(".frequencyName input").val();
            result.impactFrequency = $(elem).find(".impactFrequency input:checked").val();

            result.workDayOfWeek = $(elem).find(".workDayOfWeek input").is(":checked");
            result.daySelectionDaily = $(elem).find(".frequencyHeader select option:selected").val();

            var checkBoxSelectedItems = new Array();
            var checkBoxList = $(elem).find('.cblWeekDays input');
            for (var i = 0; i < checkBoxList.length; i++) {
                if (checkBoxList[i].checked) {
                    checkBoxSelectedItems.push(i);
                }
            }
            result.cblWeekDays = checkBoxSelectedItems;
            
            checkBoxSelectedItems = new Array();
            checkBoxList = $(elem).find('.cblMonths input');
            for (var i = 0; i < checkBoxList.length; i++) {
                if (checkBoxList[i].checked) {
                    checkBoxSelectedItems.push(checkBoxList[i].value);
                }
            }
            result.cblMonths = checkBoxSelectedItems;
            result.SpecificDayRadioButton = $(".SpecificDayRadioButton input").is(":checked");
            result.daySelection = $(".specificDaySelection option:selected").val();
            result.advancedDaySelection = $(".advancedDaySelection input").is(":checked");;
            result.weekNumberSelection = $(elem).find('.weekNumberSelection option:selected').index();
            result.dayOfWeekSelection = $(elem).find('.dayOfWeekSelection option:selected').index();
            if (result.StartDateDropdown == RIGHT_NOW_INDEX) {
                if (result.endTimeIsSpecified == true) {
                    result.specStartDate = $(elem).find(".specStartDateTime").find(".timePicker").val();
                    result.specEndDate = $(elem).find(".endTimePicker").find(".datePicker").val() + " " + $(elem).find(".endTimePicker").find(".timePicker").val();

                } else {
                    result.specStartDate = $(elem).find(".specStartDateTime").find(".timePicker").val();
                }

            }
            if (result.StartDateDropdown == SPECIFIC_DAY_INDEX) {
                result.specStartDate = $(elem).find(".specStartDateTime").find(".datePicker").val() + " " + $(elem).find(".specStartDateTime").find(".timePicker").val();
                if (result.endTimeIsSpecified == true) {
                    result.specEndDate = $(elem).find(".endTimePicker").find(".datePicker").val() + " " + $(elem).find(".endTimePicker").find(".timePicker").val();
                }
            }
            return result;
        },
    };
}
