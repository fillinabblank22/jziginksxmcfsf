$(function () {
    var $header = $('#pageHeader'),
        $menu = $header.find('#mega-tabs .mega-submenu.full-menu'),
        $menuList = $menu.find('.mega-submenu-container > ul'),
        $menuItems = $menuList.find('> li'),
        $menuToggle = $menu.find('.mega-menu-config > .menu-style-toggle'),
        $notificationsContainer = $('#notifications-container'),
        $navNotifications = $('#nav-notifications'),
        $evalBar = $('#evalMsg'),
        $form = $('#aspnetForm'),
        $navScrollContainer = $('#swNavScroll'),
        $navSection = $('#swNav'),
        $userTextSection = $('.nav-usertext'),
        $userSection = $('#userName'),
        userSectionWidth = $userSection.width(),
        maxUserTextWidth = 108, //maximum width of the user name @ 1024 page width
        minWidth = 1024, // minimum supported width
        forceScroll = false,
        maxScroll = 0,
        menus = { primary: 'menu-primary', secondary: 'menu-secondary' },
        menuPinned = false,
        notificationsOpen = false,
        timeout,
        openTime,
        resizeThrottle,
        scrollThrottle;

    // setup a smartresize function that debounces the resize events
    function smartResizeHandler(e) {
        if (window.innerWidth < 1024) { // minimum width
            maxScroll = Math.abs(window.innerWidth - minWidth);
            $userSection.css('right', (-1 * maxScroll) + 'px');
            $userTextSection.css('max-width', maxUserTextWidth + 'px');
            forceScroll = true;
        } else {
            $userSection.css('right', '0');
            $userTextSection.css('max-width', window.innerWidth - minWidth + maxUserTextWidth + 'px');
            forceScroll = false;
        }
    }
    // anonymous function handles throttled resize events
    resizeThrottle = _.throttle(function (e) {
        smartResizeHandler(e); // call the actual handler
    }, 100);
    // bind the resize event handler
    window.addEventListener('resize', resizeThrottle, false);
    smartResizeHandler(); // trigger this once on page load

    // same thing for scrolling
    function smartScrollHandler(e) {
        if (!forceScroll) { return; }
        if (window.pageXOffset > 0) {
            var offset = -1 * Math.min(maxScroll, window.pageXOffset);
            $navScrollContainer.css('left', offset + 'px');
        } else {
            $navScrollContainer.css('left', '0');
        }
    }
    scrollThrottle = _.throttle(function (e) {
        smartScrollHandler(e);
    }, 25);
    window.addEventListener('scroll', scrollThrottle, false);
    smartScrollHandler();

    // jQuery menu-aim plugin for handling mouse movement over the compact menu.
    // https://github.com/kamens/jQuery-menu-aim
    $menuList.menuAim({
        activate: function (row) {
            $(row).addClass('menu-active');
        },
        deactivate: function (row) {
            $(row).removeClass('menu-active');
        },
        exitMenu: function () { return true; }
    });

    // Helper function to handle user interaction outside a given element.
    function clickOutside($element) {
        var deferred = $.Deferred();
        var $body = $('body');

        $body.on('click', function (e) {
            if ($(e.target).closest($element).length === 0) {
                $body.off('click');
                deferred.resolve();
                return false;
            }
        });

        return deferred.promise();
    }

    // Allow menus to be closed with the escape key.
    $('body').keydown(function (e) {
        if (e.which === 27) {
            unpinMenu($('#mega-tabs > ul > li.menu-open'));
        }
    });

    function setHeights() {
        // Set the maximum height of the menu based on the window height.
        $menuList = $menu.find('.mega-submenu-container > ul');
        $menuItems = $menuList.find('> li');
        var windowHeight = $(window).height();
        var menuHeight = Math.round(windowHeight * 0.7);
        $menuList.css('max-height', menuHeight);
        $menuList.css('overflow-y', 'auto');

        if (MenuSelector.isMenuStyleSelected(menus.secondary)) {
            $menuItems.css('height', 'auto');
        } else {
            // Align the heights of the rows to the list with the largest height.
            var columns = Math.floor($menuList.width() / $menuItems.width());
            if (columns == null || columns < 2) return;

            for (var i = 0, j = $menuItems.length; i < j; i += columns) {
                var maxHeight = 0,
                    $rows = $menuItems.slice(i, i + columns);

                $rows.each(function() {
                    var itemHeight = parseInt($(this).outerHeight());
                    if (itemHeight > maxHeight) maxHeight = itemHeight;
                });
                $rows.css('height', maxHeight);

                if (maxHeight >= menuHeight) {
                    $menuList.css('overflow-y', 'scroll');
                }
            }
        }
    }

    var MenuSelector = {
        init: function () {
            $menuToggle.click(function (e) {
                e.preventDefault();
                MenuSelector.toggleMenu();
            });

            // If the module count is 1 then use the expanded version and hide the toggle,
            // greater than 4 then we want to make the secondary menu the default.
            var moduleCount = $menuItems.length;
            if (moduleCount === 1) {
                if (!this.isMenuStyleSelected(menus.secondary)) {
                    // Hide the toggle if there is 1 module and the seondary menu is not specified.
                    $menuToggle.hide();
                }
            }

            if (moduleCount > 4) {
                // During init if the primary menu class is specified then it means the user has
                // a stored setting for this account so leave it as it is. Otherwise, toggle the
                // secondary menu.
                if (!this.isMenuStyleSelected(menus.primary)) {
                    $menu.addClass(menus.secondary);
                }
            }
        },
        isMenuStyleSelected: function (style) {
            return $menu.hasClass(style);
        },
        toggleMenu: function () {
            // We need to use opacity rather than display so the size of the menu is available for setHeights().
            $menu.animate({ opacity: 0.0 }, 200, function () {
                pinMenu($('#mega-tabs > ul > li.menu-open'));

                $menu.toggleClass(menus.secondary);

                setHeights();

                var onComplete = function () {
                    $menu.animate({ opacity: 1.0 }, 600);
                };

                var selectedMenu = $menu.hasClass(menus.secondary) ?
                    menus.secondary : menus.primary;

                // Persist the user's setting.
                SW.Core.Services.callWebService(
                    "/Orion/Services/NodeManagement.asmx", "SaveUserSetting",
                    { name: 'MegaMenu-Style', value: selectedMenu },
                    onComplete, onComplete);
            });
        }
    };
    MenuSelector.init();

    function openMenu($menuItem) {
        if (!$menuItem.length) return;
        openTime = Date.now();

        $menuItem
            .addClass('menu-open')
            .find('> .mega-submenu')
            .fadeIn(300);
    }

    function closeMenu($menuItem) {
        if (!$menuItem.length) return;

        $menuItem
            .removeClass('menu-open')
            .find('> .mega-submenu')
            .fadeOut(100);
    }

    function unpinMenu($menuItem) {
        menuPinned = false;
        closeMenu($menuItem);
    }

    function pinMenu($menuItem) {
        menuPinned = true;
        closeMenu($('#mega-tabs > ul > li.menu-open'));
        openMenu($menuItem);

        clickOutside($menuItem).then(function() {
            unpinMenu($menuItem);
        });
    }

    // Close the menu when a menu item is clicked.
    $('.sub-menu-container > ul > li > a').click(function() {
        closeMenu($('#mega-tabs > ul > li.menu-open'));
    });

    // Handle mouseenter/mouseleave/click events on the top level menus.
    $('#mega-tabs > ul > li')
        .mouseenter(function () {
            closeNotifications();

            var $menuItem = $(this);
            if ($menuItem.is('.menu-open')) return;

            timeout = setTimeout(function () {
                // Close any menus that might be open.
                unpinMenu($('#mega-tabs > ul > li.menu-open'));

                openMenu($menuItem);

                setHeights();
            }, 300);
        })
        .mouseleave(function () {
            if (menuPinned) return;

            clearTimeout(timeout);
            var $menuItem = $(this);

            setTimeout(function () {
                if (!$menuItem.is(':hover')) {
                    closeMenu($menuItem);
                }
            }, 200);
        })
        .find('> a').click(function (e) {
            clearTimeout(timeout);

            closeNotifications();

            var $menuItem = $(this).parent();

            var isOpen = $menuItem.is(".menu-open");
            if (isOpen && (Math.abs(Date.now() - openTime) < 400)) {
                return;
            }

            if (isOpen) {
                unpinMenu($menuItem);
            } else {
                pinMenu($menuItem);
            }

            return false;
        });

    // OGS search
    $('#nav-search').click(function() {
        var $el = $(this);
        $el.addClass('open');

        var $searchForm = $('#nav-search-form');

        $searchForm.fadeIn(300, function() {
            $searchForm.find('.search-input').focus();
            clickOutside($searchForm.find('.nav-search-input-container')).then(function() {
                $el.removeClass('open');
                $searchForm.fadeOut(100);
            });
        });
    });

    // Workaround to fix "GS-840 Joe the IT guy wants to have working Top search bar when Grid is present so he could search"
    var resetSearchFormSubmit = function () { $('#nav-search-form').unbind('submit'); };
    if (typeof Ext != 'undefined' && Ext != null) {
        Ext.onReady(resetSearchFormSubmit);
    } else {
        resetSearchFormSubmit();
    }

    // Notifications
    $navNotifications.on('click', openNotifications);

    function openNotifications() {
        if (notificationsOpen) return false;

        // Close any menus that might be open.
        unpinMenu($('#mega-tabs > ul > li.menu-open'));

        notificationsOpen = true;
        $navNotifications.off('click');
        $notificationsContainer.fadeIn(300, function() {
            clickOutside($notificationsContainer).then(function() {
                closeNotifications();
            });
        });

        return false;
    }

    function closeNotifications() {
        if (!notificationsOpen) return;
        var $notificationCount = $('#notifications-count').hide();
        $notificationCount.show();
        $notificationsContainer.fadeOut(100, function () {
            $navNotifications.on('click', openNotifications);
        });
        notificationsOpen = false;
    }
});
