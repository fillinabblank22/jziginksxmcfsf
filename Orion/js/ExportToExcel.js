﻿(function () {
    var ns = SW.Core.namespace("SW.Core.Export.Excel");
    var registry = {};

    var defaultAny = function () {
        var exports = $('.sw-exportdata');
        return !!exports[0];
    };

    var defaultCollect = function () {

        var ret = [];

        $('.sw-exportdata').each(function (idx, value) {
            var s = $(value).html();
            if (s.trim() == "") return;
            ret.push(s);
        });

        return ret.length > 0 ? ret : null;
    };

    // ---

    ns.RefreshLink = function () {
        var any = false;

        $.each(registry, function (key, value) {

            if (value.detect()) {
                any = true;
                return false;
            }
        });

        var link = $('#ExportToExcel');
        if (any) link.show();
        else link.hide();
    };

    ns.Register = function (name, fnCollect, fnAny) {
        if (name === 'defaults') {
            fnCollect = fnCollect || defaultCollect;
            fnAny = fnAny || defaultAny;
        }

        if (!fnCollect) {
            fnAny = null;
        }
        else if (!fnAny) {
            var f = fnCollect;
            fnAny = function () { var ctx = f(); return ctx && ctx.length > 0; };
        }

        registry[name] = { detect: fnAny, collect: fnCollect };
    };

    ns.Start = function (filename) {

        var items = [];

        filename = filename || ns._filename || document.title;

        $.each(registry, function (key, value) {
            if (!value || !value.collect) return;

            var v = value.collect();

            if (v) {
                if ($.isArray(v)) items = $.merge(items, v);
                else items.push(v);
            }
        });

        if (items.length < 0) {
            if (console && console.log)
                console.log('No exportable items detected. export button should not be visible.');
            return;
        }

        for (var i = 0, imax = items.length; i < imax; ++i) {
            var v = items[i];
            if (typeof (v) !== "string")
                v = JSON.stringify(v);
            items[i] = fixVulnerableSymbols(v);
        }

        var txt = "[" + items.join(',') + "]";

        // [pc] yes, this is syncronous. Why? because we need the anchor click to start the download to get past IE's default settings.

        $.ajax('/Orion/Reports/AsExcel.aspx?filename=' + encodeURIComponent(filename), {
            async: false,
            cache: false,
            type: 'post',
            data: txt,
            contentType: 'application/json; charset=UTF8',
            dataType: 'json',
            success: function (d) {
                if (d && d.url)
                    window.location = d.url;
            }
        });
    };

    function fixVulnerableSymbols (text) {
        text = sanitizeText(text, "Title");
        text = sanitizeText(text, "Subtitle");

        return text;
    };

    function sanitizeText(text, inputType){        
        const vulnerableSymbols = ["=", "-", "+", "@"];

        var indexOfInput = text.indexOf(inputType);
        // the distance from indexOfInput to the first letter of the actual input, e.g. "Title":"Traffic Rate"...
        var distanceToFirstLetter = inputType.length + 3;
        var isInputVulnerable = indexOfInput !== -1 && vulnerableSymbols.includes(text.charAt(indexOfInput + distanceToFirstLetter));
        
        if (isInputVulnerable){
            var index = indexOfInput + distanceToFirstLetter;
            text = text.slice(0, index) + "\'" + text.slice(index, text.length);
        }

        return text;
    }

})();