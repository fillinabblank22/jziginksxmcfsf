﻿(function(Core) {
    var AdvancedTextBox = Core.AdvancedTextBox = Core.AdvancedTextBox || {};

    AdvancedTextBox.CheckForOverflow = function checkForOverflow(clientId) {
        var textbox = $(String.format("#{0}", clientId));
        var textboxContent = Ext.util.Format.htmlEncode(textbox.val());
        var editButton = $(String.format("#{0}_editButton", clientId));

        var loopStep = 60;
        for (var i = 0; i < textboxContent.length; i += loopStep) {
            var textToCheck = textboxContent.substring(0, i + loopStep);

            var fakeEl = $("<span />").html(textToCheck);
            fakeEl.appendTo(document.body).hide();
            var textWidth = fakeEl.width();
            fakeEl.remove();

            if (textWidth > textbox.width()) {
                editButton.show();
                return;
            }
        }

        editButton.hide();
    };

    AdvancedTextBox.ShowEditDialog = function(clientId) {
        var editor = new SW.Core.EditorDialog(clientId);
        editor.show();
    };
})(SW.Core);