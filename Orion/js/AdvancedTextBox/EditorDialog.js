﻿SW.Core.namespace("SW.Core").EditorDialog = function (textboxId) {
    var mDlg = null,
        textAreaId = 'textAreaId',
        textbox = $(String.format('#{0}', textboxId));

    this.show = function() {
        var dlgConfig = {
            title: "@{R=Core.Strings.2;K=SettingEditorDialog_Title;E=js}",
            height: 480,
            width: 640,
            padding: 20,
            modal: true,
            border: false,
            constrain: true,
            constrainHeader: true,
            resizable: true,
            layout: 'fit',
            flex: 1,
            buttons: [
                {
                    text: "@{R=Core.Strings.2;K=SettingEditorDialog_OK;E=js}",
                    handler: onSubmit
                },
                {
                    text: "@{R=Core.Strings.2;K=SettingEditorDialog_Cancel;E=js}",
                    handler: onClose
                }
            ],
            items: [{
                xtype: 'textarea',
                id: textAreaId,
                value: textbox.val(),
                setAutoScroll: true
            }]
        };

        mDlg = new Ext.Window(dlgConfig);
        mDlg.show();
    };
        
    function onClose() {
        mDlg.close();
        mDlg.destroy();
        mDlg = null;
    };
        
    function onSubmit() {
        textbox.val(Ext.getCmp(textAreaId).getValue());
        textbox.trigger("input");
        onClose();
    }
};