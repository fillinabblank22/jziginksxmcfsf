﻿Ext.namespace("SW.Core");
SW.Core.AddContentLoader = function (config) {
    var self = this,
        selectedColumn = 1,
        resourcePicker = config.resourcePicker,
        $resourcePickerDialog = config.dialogElement,
        $contentDialogAccordion = config.accordionElement,
        $resourceHeader = config.resourceHeader,
        $datasourceHeader = config.dataSourceHeader,
        configsSessionName = 'add_content_loader_config_data',
        addResButton,
        addToLayoutButton,
        selectedResource = {},
        selectedResourceConfigData = {},
        updateDataSourceHandler,
        notSupportedGuid = 'ffffffff-ffff-ffff-ffff-ffffffffffff',
        emptyGuid = '00000000-0000-0000-0000-000000000000';

    buildCache();

    function selectResource(skipActivate) {
        if (skipActivate != true) {
            $contentDialogAccordion.accordion('activate', 0);
        }
        if ($resourceHeader.hasClass('resourceHeaderInActive')) {
            $resourceHeader.removeClass('resourceHeaderInActive');
        }
        var resource = getSelectedResource();
        if (resource != null) {
            if ($datasourceHeader.hasClass('datasourceHeaderDisabled'))
                $datasourceHeader.removeClass('datasourceHeaderDisabled');
            if ($datasourceHeader.hasClass("ui-state-disabled"))
                $datasourceHeader.removeClass("ui-state-disabled");
            $datasourceHeader.addClass('datasourceHeaderInActive');
        } else {
            $datasourceHeader.addClass('datasourceHeaderDisabled');
        }
        $('#add-content-select-resourceText').html('@{R=Core.Strings;K=WEBJS_IB0_27;E=js}');
        $('#add-content-select-resourceName').empty();
        $('#add-content-select-resourceDescription').empty();

        $('#add-content-select-datasourceName').html('@{R=Core.Strings;K=WEBJS_IB0_28;E=js}');
        addResButton.show();
        addToLayoutButton.hide();
    }

    function getSelectedResource() {
        var resources = resourcePicker.GetSelectedResources();
        if (resources != undefined && resources != null) {
            var resource = resources[0];
            if (resource != undefined && resource != null && resource.path != undefined && resource.path != null)
                return resource;
        }
        return null;
    }

    function selectDataSource(resFile, frameId, source, callback) {
        var location = '';
        var resource = getSelectedResource();
        if (resFile)
            location = resFile;
        else if (resource != null)
            location = resource.path;
        else return;
        ORION.callWebService('/Orion/Services/ReportManager.asmx', 'LoadResourceConfigOptions', { location: location },
            function (result) {
                if (result == null) return;

                selectedResourceConfigData = {
                    supportDateTimeFilter: result.SupportDtFilter, requiresEditOnAdd: result.RequiresEditOnAdd, supportedInterfaces: result.SupportedInterfaces, iconPath: result.IconPath, editButtonText: result.EditButtonText, path: result.Path, title: result.Title
                };

                selectedResource = {
                    RefId: result.RefId,
                    ConfigId: result.ConfigId,
                    TimeframeRefId: result.SupportDtFilter ? emptyGuid : notSupportedGuid,
                    DataSelectionRefId: emptyGuid,
                    RenderProvider: result.RenderProvider,
                    DisplayName: result.Title,
                    Config: null
                };

                if (result.SupportedInterfaces.length == 0) {
                    //in case selected resource doesn't support data sources then just return selected resource
                    selected();
                    hideDialog();
                    return;
                }
                var defaultMasterEntities = function () {
                    var entity = result.SupportedInterfaces[0].MasterEntity;
                    for (var i = 0; i < result.SupportedInterfaces.length; i++) {
                        if (result.SupportedInterfaces[i].MasterEntity == "Orion.Nodes") {
                            entity = result.SupportedInterfaces[i].MasterEntity;
                            break;
                        }
                    }
                    return entity;
                } ();
                var dataSource = (source) ? source : { RefId: emptyGuid, Name: '', Type: 4, CommandText: '', MasterEntity: defaultMasterEntities, Filter: null, EntityUri: [] };
                if (callback)
                    callback(1);
                else {
                    $contentDialogAccordion.accordion('activate', 1);
                }
                $.fn.dataSources.setUpForm((frameId) ? frameId : result.RefId, dataSource, result.SupportedInterfaces);

                $('#add-content-select-resourceText').html('@{R=Core.Strings;K=WEBJS_IB0_29;E=js}');
                $('#add-content-select-resourceName').html(selectedResource.DisplayName);
                $('#add-content-select-resourceDescription').html(String.format('@{R=Core.Strings;K=WEBJS_RB0_9;E=js}', result.Category));
                $resourceHeader.addClass('resourceHeaderInActive');
                if ($datasourceHeader.hasClass('datasourceHeaderDisabled'))
                    $datasourceHeader.removeClass('datasourceHeaderDisabled');
                if ($datasourceHeader.hasClass("ui-state-disabled"))
                    $datasourceHeader.removeClass("ui-state-disabled");
                if ($datasourceHeader.hasClass('datasourceHeaderInActive'))
                    $datasourceHeader.removeClass('datasourceHeaderInActive');

                $('#add-content-select-datasourceName').html('@{R=Core.Strings;K=WEBJS_IB0_30;E=js}');

                addResButton.hide();
                addToLayoutButton.show();
            },
            function (errorMsg) {
                Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: errorMsg, minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                hideDialog();
            });
    }

    initButtons();

    function initButtons() {
        addResButton = $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_IB0_31;E=js}', { type: 'primary', id: 'addContentSelectResource' }));
        addResButton.appendTo("#report-content-dialog-btn-ph").click(function () {
            var selectedRes = resourcePicker.GetSelectedResources()[0];
            if (selectedRes == undefined || selectedRes.length == 0) {
                Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: '@{R=Core.Strings;K=WEBJS_IB0_33;E=js}', minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }
            selectDataSource();
        });

        addToLayoutButton = $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_SO0_66;E=js}', { type: 'primary', id: 'addContentToLayout' }));
        addToLayoutButton.appendTo("#report-content-dialog-btn-ph").click(function () {
            if (!$.fn.dataSources.validateAndSyncData()) return;
            if (updateDataSourceHandler) {
                updateDataSourceHandler();
                hideDialog();
            }
            else {
                //update data source id
                selectedResource.DataSelectionRefId = $.fn.dataSources.getSelectedDataSourceId();
                selected();
            }
            $resourcePickerDialog.dialog('destroy');
        }).hide();
        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}', { type: 'secondary', id: 'addContentCancel' })).appendTo("#report-content-dialog-btn-ph").click(function () { hideDialog(); });

        $resourceHeader.click(function () {
            if (!$(this).hasClass("ui-state-disabled"))
                selectResource();
        });

        $datasourceHeader.click(function () {
            if (!$(this).hasClass("ui-state-disabled")) selectDataSource();
        });
        $datasourceHeader.addClass("ui-state-disabled datasourceHeaderDisabled");
    }


    function selected() {
        var resource = getSelectedResource();
        var selfObj = self;
        if (resource != null) {
            ORION.callWebService('/Orion/Services/ReportManager.asmx', 'CreateConfig', { location: resource.path, configsSessionName: configsSessionName, configId: selectedResource.ConfigId },
                function () {
                    if (jQuery.isFunction(config.onSelected)) {
                        config.onSelected(selfObj);
                    }
                    hideDialog();
                },
                function (errorMsg) {
                    Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: errorMsg, minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    hideDialog();
                });
        }
        else {
            hideDialog();
        }
    }

    function hideDialog() {
        $resourcePickerDialog.dialog('destroy');
        $contentDialogAccordion.accordion('destroy');
        resourcePicker.ClearSelection();
        // clear datasourcepicker in case user close dialog pressing Cancel or X button
        $.fn.dataSources.clear();
    }

    function buildCache() {

        // HACK: When dialog is shown it executes script inside it again. It creates new resource picker instance (data stores).  
        // Moving the scripts out of the markup of ResourcePicker.ascx will fix the need for this hack.
        $resourcePickerDialog.find("script").remove();

        SW.Core.ResourcePicker.buildCache(function () {
            //Once we know cache is built render resource picker. (dialog may be open or closed depending on cache build time)
            //Content is off page to allow ExtJs to render properly in case dialog is closed.
            if (resourcePicker) {
                resourcePicker.Render();
            }
            $('#resourcePickerLoader-loading-ph').hide();
            $('#resourcePickerLoader-content-ph').removeClass('notVisible');
        });
    }

    function created() {
        if (jQuery.isFunction(config.onCreated)) {
            config.onCreated(self);
        }
    }
    function openDialog(activeTab) {
        $contentDialogAccordion.accordion({ autoHeight: false, active: activeTab ? activeTab : 0 });
        $resourcePickerDialog
            .removeClass('offPage')
            .dialog({
                closeOnEscape: false, // bug jQuery: closing non-top window on Esc
                width:  'auto',
                height: 'auto',
                title: '@{R=Core.Strings;K=WEBJS_SO0_57;E=js}',
                modal: true,
                close: function () {
                    hideDialog();
                }
            });
    };

    $.fn.dataSources.openEditDialogDelegate = function (resFile, frameId, dataSource, updateHandler) {
        $resourceHeader.addClass("ui-state-disabled");
        $resourceHeader.click(function (event) {
            event.stopImmediatePropagation();
            return false;
        });
        if ($datasourceHeader.hasClass("ui-state-disabled"))
            $datasourceHeader.removeClass("ui-state-disabled datasourceHeaderDisabled");
        updateDataSourceHandler = updateHandler;
        selectDataSource(resFile, frameId, dataSource, function (activeTab) { openDialog(activeTab); });
    };

    this.setConfigSessionName = function (sessionName) {
        configsSessionName = sessionName;
    };

    this.AddContent = function (column) {
        if ($resourceHeader.hasClass("ui-state-disabled"))
            $resourceHeader.removeClass("ui-state-disabled");
        $datasourceHeader.addClass("ui-state-disabled datasourceHeaderDisabled");
        updateDataSourceHandler = undefined;

        selectedColumn = column;
        $datasourceHeader.click(function (event) {
            event.stopImmediatePropagation();
            return false;
        });
        // accordion is disabled when open dialog;
        selectResource(true);
        openDialog();
    };


    this.GetSelectedResource = function () {
        return {
            selectedColumn: selectedColumn,
            resource: selectedResource,
            resourceConfigData: selectedResourceConfigData
        };
    };

    created();
};
