﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');

SW.Orion.UnresponsiveServersNotifier = function(container) {
    this.messageContainer = $('#' + container);
    this.titleRenderTo = this.messageContainer.find('.sw-unresponsive-servers-notifier-title');
    this.messageRenderTo = this.messageContainer.find('.sw-unresponsive-servers-notifier-msg');
}

SW.Orion.UnresponsiveServersNotifier.prototype.hide = function () {
    return this.messageContainer.hide();
};

SW.Orion.UnresponsiveServersNotifier.prototype.show = function (downServersList) {
    var title = '@{R=Core.Strings;K=UnresponsiveSolarWindsSitesSelectedMessage_Title;E=js}';
    var message = '@{R=Core.Strings;K=UnresponsiveSolarWindsSitesSelectedMessage_Body;E=js}'.replace('{0}', '<b>' + downServersList.join(", ") + '</b>');

    this.titleRenderTo.text(title);
    this.messageRenderTo.html(message);

    this.messageContainer.show();
};