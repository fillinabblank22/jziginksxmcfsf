(function (Core) {

    //
    // This page is included on Orion's Master Pages just before the closing body tag.
    //

    var Header = Core.Header = Core.Header || {},
        Footer = Core.Footer = Core.Footer || {},
        MessageBox = Core.MessageBox = Core.MessageBox || {},
        Alerting = Core.Alerting = Core.Alerting || {},
        View = Core.View = Core.View || {};

    Alerting.EnableAlert = function (alertDefId) {
        $.ajax({
            type: 'Post',
            url: '/Orion/Services/AlertsAdmin.asmx/EnableAdvancedAlert',
            data: "{'alertDefId': '" + alertDefId + "'}",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.d > 0)
                    alert('@{R=Core.Strings;K=WEBJS_TM1_ENABLE_ALERT;E=js}');
                else
                    alert('@{R=Core.Strings;K=WEBJS_TM1_ENABLE_ALERT_FAILED;E=js}');
            },
            error: function (ex) {
                var r = jQuery.parseJSON(ex.responseText);
                alert(String.format('@{R=Core.Strings;K=WEBJS_TM1_ENABLE_ALERT_ERROR;E=js}', r.Message));
            }
        });
    };

    MessageBox.Dialog = function (config) {
        var html = '<div>' +
               '  <div style="padding: 3px 10px;">' +
                    config.html +
               '    <div style="bottom:10px; text-align:center;" >';

        if (config.buttons != null && config.buttons != undefined) {
            for (i = 0; i < config.buttons.length; i++) {
                html += config.buttons[i].html + '<span>&nbsp;</span>';
            }
        }

        html += '    </div>' +
               '  </div>' +
               '</div>';

        var dlg = $(html);

        dlg.show().dialog({
            title: config.title || '',
            dialogClass: "sw-message-box",
            modal: !!config.modal,
            width: config.width || 400,
            height: config.height || 'auto'
        });

        if (config.buttons != null && config.buttons != undefined) {
            for (i = 0; i < config.buttons.length; i++) {
                $('#' + config.buttons[i].id, dlg).click(function () {
                    for (j = 0; j < config.buttons.length; j++) {
                        if (config.buttons[j].id == this.id)
                            config.buttons[j].handler(dlg);
                    }
                });
            }
        }
    }

    // Eval Banner Handling.
    Header.EvalInit = function (EvalBannerDetails) {

        $('#evalMsg a').click(function () {
            SW.Core.SalesTrigger.ShowEvalPopup(EvalBannerDetails);
        });
    };

    // RC panel Handling
    Header.RCInit = function (RCBannerDetails, HelpUrl) {

        $('#rcMsg a').click(function () {
            var html = '<div id="rcDetails">' +
               '  <div style="padding: 3px 10px;">' +
               '    <div>' +
               '       <p>@{R=Core.Strings;K=WEBJS_PSR_1;E=js} <span style="font-size: 8.5pt; cursor: pointer;">&nbsp;' +
               '            &raquo; <a href="' + HelpUrl + '" ' +
               '              target="_blank">@{R=Core.Strings;k=WEBJS_PSR_2;E=js}</a></span>' +
               '       <ul id="rcDetailsList"/>' +
               '    </div>' +
               '    <div style="position:absolute;bottom:10px;right:10px;padding-right:5px;" >' +

               SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_PCC_4;E=js}', { type: 'primary', id: 'rcDetailsClose' }) +

               '    </div>' +
               '  </div>' +
               '</div>';
            var dlg = $(html);

            $('#rcDetailsClose', dlg).click(function () {
                dlg.dialog('close');
            });

            var fmt = Core.String.Format;

            var items = '';
            $.each(RCBannerDetails, function () {
                var countTxt = this.IsExpired ? '@{R=Core.Strings;K=StatusDesc_Expired;E=js}' : fmt(this.DaysRemaining == 1 ? '@{R=Core.Strings;K=WEBJS_PCC_2;E=js}' : '@{R=Core.Strings;K=WEBJS_PCC_3;E=js}', this.DaysRemaining);
                items += '<li><b>' + this.DisplayName + '</b> - ' + countTxt + '</li>';
            });
            $('#rcDetailsList', dlg).empty().append(items);

            dlg.dialog({
                title: '@{R=Core.Strings;K=RCBanner_Title;E=js}',
                width: 600,
                modal: true
            });
        });

    };

    // if( navigator.userAgent.indexOf("Trident") == -1) => version of IE > 7
    //VERSION     TRIDENT
    // IE9 Standard MSIE 9.0    Trident/5.0
    // IE9 Cv       MSIE 7.0    Trident/5.0
    // IE8 Standard MSIE 8.0    Trident/4.0
    // IE8 CV       MSIE 7.0    Trident/4.0
    // IE7          MSIE 7.0    No Trident token
    // IE6          MSIE 6.0    No Trident token
    Header.WarnIfOldBrowser = function () {
        if ($.browser.msie && (navigator.userAgent.indexOf("Trident") == -1)
            && location.href.toLowerCase().indexOf('inlinerendering=true') == -1
            && location.href.toLowerCase() != 'about:blank'
            && !SW.Core.Cookie.Get("SW_UnsupportedBrowser_HideWarning")) {
            Core.MessageBox.Dialog({
                title: '@{R=Core.Strings;K=WEBJS_IB0_2;E=js}',
                width: 600,
                modal: true,
                html: '<table style="width: 100%;">' +
                  ' <tr>' +
                  '  <td style="vertical-align: top; padding:0px 10px 0px 2px;">' +
                  '   <img src="/orion/images/info_32x32.gif" />' +
                  '  </td>' +
                  '  <td>' +
                  '   @{R=Core.Strings;K=WEBJS_IB0_3;E=js} ' +
                '     <br/>&#0187; <a href="' + SW.Core.KnowledgebaseHelper.getKBUrlWithLang('2997') + '" target="_blank" rel="noopener noreferrer" class="coloredLink">@{R=Core.Strings;K=LIBCODE_PCC_7;E=js}</a> ' +
                  '     <br/><br/>' +
                  '     <table>' +
                  '        <tr><td width="20px"><input id="notShowAgain" type="checkbox" style="width:20px!important;"/></td>' +
                  "            <td class='cb-label'>@{R=Core.Strings;K=WEBJS_IB0_4;E=js}</td><tr>" +
                  '     </table>' +
                  '  </td>' +
                  ' </tr>' +
                  '</table>',
                buttons: [
                {
                    id: 'updateBrowserWarningBtn',
                    html: SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Ok;E=js}', { type: 'primary', id: 'updateBrowserWarningBtn' }),
                    handler: function (msbox) {
                        if ($('#notShowAgain').get(0).checked) {
                            SW.Core.Cookie.Set('SW_UnsupportedBrowser_HideWarning', '1', 'years', 5);
                        }
                        else {
                            SW.Core.Cookie.Set('SW_UnsupportedBrowser_HideWarning', null, '', null);
                        }
                        msbox.dialog('close');
                    }
                }
            ]
            });

            // fix i18n button's styles for IE6 browser
            // due to this message shows only for IE6 browser
            $("#updateBrowserWarningBtn").find('span.sw-btn-m').css('overflow', 'hidden');
            $("#updateBrowserWarningBtn").find('span.sw-btn-br').css('background-position', '-10px -108px');
            $("#updateBrowserWarningBtn").find('span.sw-btn-bl').css('background-position', '0px -108px');
            $("#updateBrowserWarningBtn").find('span.sw-btn-b').css('background-position', '0px -6px');
            $("#updateBrowserWarningBtn").find('span.sw-btn-c').css('padding-bottom', '6px');
        }
    }

    Header.TabsInit = function (OrionTabSettings) {

        var url = window.location + "";
        if (url.toLowerCase().search("login.aspx") > 0) {
            return true;
        }

        var tabCount = $('div#tabs div.sw-mainnav-bars a').length;

        if (tabCount == 0) {
            return true;
        }

        var $currentTab = '';
        var $selectedTab = '';
        var overTimeout = 0;
        var outTimeout = 0;
        var sensitiv = 1;

        sensitiv = OrionTabSettings.TabsSensitivity;
        overTimeout = OrionTabSettings.TabsMouseOverTimeout;
        outTimeout = OrionTabSettings.TabsMouseOutTimeout;

        var tabContainers = $('div#tabs div.sw-mainnav-bars ul');
        var hash;
        var allowSetActiveTabIdCallback = true;

        var isAdmin = window.IsOrionAdminPage || (window.location.pathname.match(new RegExp('Orion[/a-zA-Z]+Admin')) != null);
        var tabsConfig = {
            sensitivity: sensitiv,
            interval: 0,
            over: function (e) { },
            timeout: outTimeout,
            out: function (e) {
                // if e doesn't exist, get it.
                if (!e) var e = window.event;
                // this is the element we've moved to
                var reltg = (e.relatedTarget) ? e.relatedTarget : e.toElement;
                // if reltg is null - cursor is out of browser window
                if (reltg != null && reltg != undefined) {
                    while (reltg.id != 'tabs' && reltg.tagName != 'BODY' && reltg.tagName != 'HTML')
                        reltg = reltg.parentNode;
                    if (reltg.id == 'tabs') {
                        // we found tg as a parent. So this is not really leaving the element
                        return;
                    }
                }
                // return to current tab
                if (!isAdmin) {
                    $("ul[id*='tabs-empty']").css('display', 'none');
                    allowSetActiveTabIdCallback = false;
                    $("a[href='" + $currentTab + "']").click();
                    allowSetActiveTabIdCallback = true;
                }
                else {
                    $('div#tabs div.sw-mainnav-tabs li').removeClass('selected');
                    $("ul[id*='tabs-']").css('display', 'none');
                    $("ul[id*='tabs-empty']").css('display', 'block');
                }
            }
        };

        var tabConfig = {
            sensitivity: sensitiv,
            interval: overTimeout,
            over: function (e) {
                allowSetActiveTabIdCallback = false;
                $(this).click();
                allowSetActiveTabIdCallback = true;
            },
            timeout: 0,
            out: function (e) { }
        };

        var setActiveTabId = function (tabId, success, error) {
            if (!success)
                success = function () { };
            SW.Core.Services.callWebServiceSync('/Orion/Services/TabsManager.asmx', 'SetActiveTabId', { 'tabId': tabId }, success, error);
        };

        $("div#tabs").hoverIntent(tabsConfig);
        $('div#tabs div.sw-mainnav-tabs a').hoverIntent(tabConfig);

        $('div#tabs div.sw-mainnav-tabs a').click(function () {
            tabContainers.hide().filter(this.hash).show();
            $('div#tabs div.sw-mainnav-tabs li').removeClass('selected');
            $(this).parent('li').addClass('selected');

            hash = this.hash;
            $selectedTab = hash;
            var tabId = this.hash.replace('#tabs-', '');
            
            setCookie('SelectedTab', tabId, 'hours', 2);

            if (allowSetActiveTabIdCallback) {
                $currentTab = $selectedTab;
                setActiveTabId(tabId, function () {
                    isAdmin = false;
                    window.location.href = tabContainers.filter(hash).find('a').filter(':first')[0].href;
                });
            }

            return false;
        });

        $('div#tabs .sw-mainnav-bars a').click(function (evt) {
            if (evt.ctrlKey || evt.which == 2) return true; // FB153626: allow the default action to occur.
            if (this.target != '_blank') {
                var href = this.href;
                setActiveTabId($selectedTab.replace('#tabs-', ''), function () {
                    $currentTab = $selectedTab;
                    isAdmin = false;
                    window.location.href = href;
                });
                return false;
            }
            else {
                window.open(this.href);
                return false;
            }
        });

        // Set the current tab...
        if (!isAdmin) {
            allowSetActiveTabIdCallback = false;
            $("a[href='#tabs-" + OrionTabSettings.CurrentTabId + "']").click();
            allowSetActiveTabIdCallback = true;
        }

        $currentTab = '#tabs-' + OrionTabSettings.CurrentTabId;
        $selectedTab = '#tabs-' + OrionTabSettings.CurrentTabId;
    };

    Header.SyncTo = function (cssselector) {

        // default to syncing with web resources.
        var selector = cssselector || '.ResourceContainer', appregionid, syncid,

    sync_region = function () {
        if (!syncid) {
            appregionid = Core.Id($('.sw-app-region')[0]);
            var p = $(selector)[0];
            while (p && p.parentNode) {
                if ((p.parentNode.tagName == 'FORM') || (p.parentNode.id == appregionid)) break;
                p = p.parentNode;
            }
            syncid = Core.Id(p);
        }

        //var docw = $(window).width(),
        //    winw = $(window).width(),
        //    res = $('#' + syncid),
        //    off = res.offset().left,
        //    rm = parseInt(res.css('margin-right'), 10),
        //    resw = rm + off + res.outerWidth(),
        //    w = Math.max(winw, resw);

        //w = (w == winw) ? '100%' : '' + w + 'px';
        //$('#pageHeader,#footer,#' + appregionid).css('width', w);
    };

        $(sync_region);
        $(window).resize(sync_region);
        $(window).load(sync_region);
    };

    // Footer (revised from original 'ie6' version)

    Footer.Init = function (opts) {
        var options = opts || {};
        $(function () {
            var domid = options.ismobile ? '#mobileFooter' : '#footer', _w, _d, sync = function () {
                var w = $(window).height(), m = $('#footermark'), d = m.offset().top;
                if ((w != _w) || (d != _d)) {
                    var footer = $(domid);
                    footer.css('position', (w >= (d + footer.height())) ? 'fixed' : 'static');
                    _w = w;
                    _d = d;
                }
            };

            if (options.ismobile) {
                $(sync);
                $(window).resize(sync);
                $(window).load(sync);
            }
        });
    };

    Header.WarnIfOldBrowser();

    // FB100779: Export to pdf: Maps and charts are split into two pages
    // Check whether the page is being rendered by EO.Pdf. If yes then add 'RenderPdf' class to Body
    if (typeof (eopdf) === 'object') $('html').addClass('RenderPdf');

    window.EnableAlert = Core.Alerting.EnableAlert;

})(SW.Core);