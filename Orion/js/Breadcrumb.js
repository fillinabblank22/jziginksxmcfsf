$(function() {
    if(window.isBreadCrumbInitialized)
    {
       // if these events are handled, probably it is done by PluggableDropDownSiteMap.js
       return;
    }
    
    var isClicked = false;
    var submenuHeight = 0;
    var $iframe = $('<iframe src="" class="tooltip-iframe" scroll="none" frameborder="0"></iframe>');
    var $customizeDiv = $('<div id="customizeDialog" style="display:none;"> \
									<div id="inside"> <table width="100%"> <tr><td id="customizeCaption"> </td> </tr> <tr> <td> <select id="groupByProperty" style="width: 60%;"> \
														 </td> </tr> </table> </div> \
									<div class="sw-btn-bar-wizard" style="margin-right:5px;">' +
										SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Submit; E=js}', { type: 'primary', id: 'customizeSubmit' }) +
								    SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}', { type: 'secondary', id: 'customizeCancel' }) +
									'</div> \
								</div>');
    $('body').append($customizeDiv);
    var $cancelButton = $customizeDiv.find("#customizeCancel");
    var $submitButton = $customizeDiv.find("#customizeSubmit");
    var $dropDownProp = $customizeDiv.find("#groupByProperty");
    var $caption = $customizeDiv.find("#customizeCaption");
    var customizeMode = "Nodes";

    $("img.bc_itemimage").hover(function() {
        $(this).attr({ src: "/Orion/images/breadcrumb_dd_bg.gif" });
    },
    function() {

        if (!isClicked) {
            $(this).attr({ src: "/Orion/images/breadcrumb_arrow.gif" });
        }
    });
    
    $("img.bc_itemimage").click(function(e) {
        e.stopPropagation();

        var theImage = $(this);
        var submenu = theImage.next("ul.bc_submenu");

        $("ul.bc_submenu").each(function(idx) {
            if (this.id != submenu.attr("id")) {
                $(this).hide();
                $(this).prev("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });
            }
        });

        if (submenu.is(":visible")) {
            theImage.attr({ src: "/Orion/images/breadcrumb_arrow.gif" });
            theImage.parent().removeClass("bc_itemselected");
            theImage.prev("a").removeClass("bc_refselected");
            submenu.hide();
            isClicked = false;
        }
        else {
            theImage.attr({ src: "/Orion/images/breadcrumb_dd_bg.gif" });
            theImage.parent().addClass("bc_itemselected");
            theImage.prev("a").addClass("bc_refselected");
            isClicked = true;
            submenuHeight = submenu.height();

            submenu.show();
        }
    });

    $(this).click(function() {
        $("li.bc_item").removeClass("bc_itemselected");
        $("a").removeClass("bc_refselected");
        $("ul.bc_submenu").hide();
        $("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });

        isClicked = false;
    });

    setStyles = function() {
        $("li.bc_item").removeClass("bc_itemselected");
        $("a").removeClass("bc_refselected");
        $("ul.bc_submenu").hide();
        $("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });

        isClicked = false;
    };
    showCustomizeDialog = function(objectName) {
        $dropDownProp.children().remove();
        customizeMode = objectName;
        var objectTitle;
        switch (objectName) {
            case 'Nodes':
                $caption[0].innerHTML = "<b>" + "@{R=Core.Strings;K=WEBJS_VB0_122; E=js}" + "</b> <br>";
                objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_125; E=js}";
                break;
            case 'Interfaces':
                $caption[0].innerHTML = "<b>" + "@{R=Core.Strings;K=WEBJS_VB0_123; E=js}" + "</b> <br>";
                objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_126; E=js}";
                break;
            case 'Volumes':
                $caption[0].innerHTML = "<b>" + "@{R=Core.Strings;K=WEBJS_VB0_124; E=js}"+"</b> <br>";
                objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_127; E=js}";
                break;
            default:
                objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_125; E=js}";
                break;
        }

        $.ajax({
            type: 'Post',
            url: '/Orion/Services/CustomizeBreadcrumb.asmx/GetListOfOptions',
            data: "{'objectName': '" + objectName + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                $dropDownProp.append(result.d);
                var dialog = $customizeDiv.dialog({
                    width: 480, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: objectTitle
                });
                dialog.show();
            },
            error: function(error) {
                switch (objectName) {
                    case 'Nodes':
                        $dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_128; E=js}</option>");
                        break;
                    case 'Interfaces':
                        $dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_129; E=js}</option>");
                        break;
                    case 'Volumes':
                        $dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_130; E=js}</option>");
                        break;
                    default:
                        $dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_128; E=js}</option>");
                        break;
                }

                var dialog = $customizeDiv.dialog({
                    width: 480, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: objectTitle
                });
                dialog.show();
            }
        });
    };

    $("#customizeNodeList").click(function() {
        showCustomizeDialog("Nodes");
        setStyles();
        return false;
    });

    $("#customizeInterfaceList").click(function() {
        showCustomizeDialog("Interfaces");
        setStyles();
        return false;
    });

    $("#customizeVolumeList").click(function() {
        showCustomizeDialog("Volumes");
        setStyles();
        return false;
    });

    $cancelButton.click(function() {
        $customizeDiv.dialog("close");
    });

    $submitButton.click(function() {
        $.ajax({
            type: 'Post',
            url: '/Orion/Services/CustomizeBreadcrumb.asmx/SaveBcCustomization',
            data: "{'customizationValue': '" + $dropDownProp.val() + "', 'objectName': '" + customizeMode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });
        $customizeDiv.dialog("close");
        location.reload();
    });
});
