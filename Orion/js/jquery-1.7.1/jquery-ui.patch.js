
// default behavior of previous version of jQuery was not to have resizable dialogs by default
// we want to keep it 

$.extend($.ui.dialog.prototype.options, { resizable: false });

// Patch code for jQuery UI dialog, that scrollbars didn't worked in Firefox and Chrome
if(window.jQuery && window.jQuery.ui && window.jQuery.ui.dialog)
(function ($) {
	$.ui.dialog.overlay.events = $.map('focus,keydown,keypress'.split(','), function (event) { return event + '.dialog-overlay'; }).join(' ');

	$.ui.dialog.prototype.open = function () {

		if (this._isOpen) { return; }

		var self = this,
		options = self.options,
		uiDialog = self.uiDialog;

		self.overlay = options.modal ? new $.ui.dialog.overlay(self) : null;
		self._size();
		self._position(options.position);
		uiDialog.show(options.show);
		self.moveToTop(true);

		// prevent tabbing out of modal dialogs
		if (options.modal) {
			uiDialog.bind('keypress.ui-dialog', function (event) {
				if (event.keyCode !== $.ui.keyCode.TAB) {
					return;
				}

				var tabbables = $(':tabbable', this),
				first = tabbables.filter(':first'),
				last = tabbables.filter(':last');

				if (event.target === last[0] && !event.shiftKey) {
					first.focus(1);
					return false;
				} else if (event.target === first[0] && event.shiftKey) {
					last.focus(1);
					return false;
				}
			});
		}

		// NEW PATCH CODE:
		// We add support for disabling auto focus, because it can take long time.
		if (!options.disableAutoFocus) {
			
			// ORIGINAL CODE:
			// set focus to the first tabbable element in the content area or the first button
			// if there are no tabbable elements, set focus on the dialog itself
			$(self.element.find(':tabbable').get().concat(uiDialog.find('.ui-dialog-buttonpane :tabbable').get().concat(uiDialog.get()))).eq(0).focus();

		}


		self._isOpen = true;
		self._trigger('open');

		return self;
	}

	var $proto = $.ui.dialog.prototype,
		encodeTitle = function(title) { 
			return (typeof title === "string") ? $('<div/>').text(title).html() : title;
		};
	
	// OADP-454 .. Vulnerability: CVE-2010-5312
	$proto.__setOption = $proto._setOption;
	$proto._setOption = function (key, value) {
		if ( key === "title" ) value = encodeTitle(value);
		return this.__setOption(key, value);
	}

	$proto.__create = $proto._create;
	$proto._create = function () {
		if ( this.options ) this.options["title"] = encodeTitle(this.options["title"]);
		this.__create(); 
	}
}(jQuery));
