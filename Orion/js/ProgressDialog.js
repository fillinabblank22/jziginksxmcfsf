﻿/** Usage example:
SW.Core.MessageBox.ProgressDialog({
            title: "Poll initiated",
            items: ids,
            serverMethod: NodeManagement.PollNetObjNow,
            getArg: function(id) { return id },
            sucessMessage: "Poll successfully completed",
            failMessage: "Poll failed",
            afterSucceeded: function(id) {
                setTimeout(function() {
                window.location.reload(true); }, 100); }
        });
 @param {Object} options - wrapped parameters into object according which will be dialog displayed
 @param {String} options.title - dialog title
 @param {String array} options.items - list of items which are processed
 @param {Function} options.serverMethod - reference to method which mostly call via ajax web service and takes 2 function parameters (first one which is callback in the success, second one in the case of failure)
 @param {Function} options.getArg -
 @param {String} options.sucessMessage - Message text which is displayed in the case of success (for each item)
 @param {String} options.failMessage - Message text which is displayed in the case of failure (for each item)
 @param {Function} options.afterSucceeded - callback which is called in the case of success serverMethod (for each item)
 @param {Bool} options.showOnlySummary - When set to true, only summary of number of processed and failed items is displayed instead of single message success/error for each processed item
 @param {Function} options.afterFinished(numSucceeded) - callback which is called when all items were processed
*/
SW.Core.namespace("SW.Core.MessageBox");

SW.Core.MessageBox.ProgressDialog = function (options) {
    var width = 600;
    var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
    var statusContent = $('<div></div>').appendTo(statusBox);
    var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
    var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
    var progressText = $('<div></div>').addClass('ProgressText').appendTo(statusBox);
    var statusText = $('<div></div>').addClass('NetObjectText').appendTo(statusContent);

    var bulkSize = options.bulkSize || 1;
    var totalItems = options.totalItems || options.items.length;

    $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_VB1_5;E=js}', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('close').remove(); });
    $('body').append(statusBox);
    var numFinished = 0, numSucceeded = 0;
    var onFinishedItem = function (item, succeeded) {
        progressBarInner.width((++numFinished / options.items.length) * 100.0 + '%');

        if (succeeded && ++numSucceeded >= options.items.length) {
            setTimeout("$('.StatusDialog').fadeOut('slow', function() { $('.StatusDialog').dialog('destroy').remove(); })", 1000);
        }

        if (options.showOnlySummary) {
            if (numFinished === numSucceeded) {
                progressText.text("@{R=Core.Strings;K=WEBJS_AK0_51;E=js}"
                    .format([Math.min(numFinished * bulkSize, totalItems), totalItems]));
            } else {
                progressText.text("@{R=Core.Strings;K=WEBJS_AK0_52;E=js}"
                    .format([Math.min(numFinished * bulkSize, totalItems), totalItems, numFinished - numSucceeded]));
            }
        } else {
            var cssClass = succeeded ? "success" : "failed";
            var message = succeeded ? options.sucessMessage : options.failMessage;
            statusText.append('<span class="iconLink ' + cssClass + '">' + message + '</span>');
            statusText.append('&nbsp;&nbsp;');
        }

        if (options.afterFinished && options.items.length === numFinished) {
            options.afterFinished(numSucceeded);
        }

        if (succeeded && options.afterSucceeded) {
            options.afterSucceeded(item);
        }
    };

    statusBox.dialog({
        width: width, height: statusContent.height() + 135, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
    });

    $.each(options.items, function () {
        var item = this;
        options.serverMethod(options.getArg(item), function () {
            onFinishedItem(item, true);
        }, function () {
            onFinishedItem(item, false);
        });
    });
};
