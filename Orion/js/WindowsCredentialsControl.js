﻿SW.Core.namespace("SW.Core").WindowsCredentialsSettings = function() {
    var config, useUserElement, notUserElement;
    function isUserName(userName) {
        var regex = /^[^\/:*?"<>|]+$/ ;
        return regex.test(userName);
    }

    function validateUserName(value, elem) {
        var validationResult = true;
        if (!isUserName($.trim(value))) {
            validationResult = false;
            elem.addClass("invalidValue");
        }
        return validationResult;
    }

    function isPassword(password) {
        var validationResult = true;
        if (password.length == 0)
            validationResult = false;
        return validationResult;
    }

    function validateUserPassword(value, elem) {
        var validationResult = true;
        if (!isPassword($.trim(value))) {
            validationResult = false;
            elem.addClass("invalidValue");
        }
        return validationResult;
    }

    function getElement(dataKey) {
        return $('[data-form="' + dataKey + '"]');
    }

    function showHideCredentialsMessage(isValid) {
        $('#testProgress').hide();
        if (isValid) {
            $('#testResultSuccess').show();
        } else {
            $('#testResultFailed').show();
        }
    }

    this.init = function() {
        this.radioSelectionChange($('input[type="radio"]:checked'));
        useUserElement = $("input[id*='useUser']");
        notUserElement = $("input[id*='notUseUser']");
    };

    this.setConfig = function(configuration) {
        config = configuration;

        if ($("[id$='useUser']").attr("checked") == "checked") {
            $("[id$='useUser']").click();
        }
    };

    this.ValidateCredentials = function() {
        var valid = true;
        getElement("userName").removeClass("invalidValue");
        getElement("password").removeClass("invalidValue");
        var credentials = this.getCredentials();
        if (credentials != null) {
            valid = validateUserName(credentials.Username, getElement("userName")) && valid;
            valid = validateUserPassword(credentials.Password, getElement("password")) && valid;
        }
        return valid;
    };

    this.getCredentials = function() {
        if (this.isCredentialsDefined()) {
            return {
                Username: getElement("userName").val(),
                Password: getElement("password").val()
            };
        } else {
            return null;
        }
    };

    this.addUpdateWindowsCredential = function() {
        var creds = this.getCredentials();
        var id = "0";
        if (creds != null) {
            SW.Core.Services.callControllerSync("/Orion/Services/CredentialManagerService.asmx/AddUpdateWindowsCredential",
                {
                    credentials: creds
                },
                function(result) {
                    id = result.d;
                });
        };

        return id;
    };

    this.radioSelectionChange = function(elem) {
        if ($(elem).attr('value') == "1") {
            $("#userCredentials").show();
        }
        else {
            $("#userCredentials").hide();
        };
    };

    this.radioDisabledChange = function(state) {
        useUserElement.attr("disabled", state);
        notUserElement.attr("disabled", state);
        if (state) {
            notUserElement.attr("checked", state);
            $("#userCredentials").hide();
        }
    };

    this.testCredentials = function() {
        var configuration = config.GetConfiguration();
        config.ValidateConfiguration(configuration, function(isValid) {
            $('#testResultSuccess').hide();
            $('#testResultFailed').hide();
            if (isValid) {
                $('#testProgress').show();
                $('#testResult').empty();
                SW.Core.Services.callController(config.TestCredentialsRoute, configuration,
                // On success
                    function(response) {
                        $('#testProgress').hide();
                        showHideCredentialsMessage(response);
                        getElement("userName").removeClass("invalidValue");
                        getElement("password").removeClass("invalidValue");
                    },
                // On error
                    function(msg) {
                        alert(msg);
                        $('#testProgress').hide();
                    });
            }
        });

    };

    this.isCredentialsDefined = function () {
        return useUserElement.is(':checked');
    }
};