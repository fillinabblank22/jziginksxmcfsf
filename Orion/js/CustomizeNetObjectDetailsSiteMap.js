$(function () {
	var $customizeDiv = $('<div id="customizeDialog" style="display:none;"> \
									<div id="inside"> <table width="100%"> <tr><td id="customizeCaption"> </td> </tr> <tr> <td> <select id="groupByProperty" style="width: 60%;"> \
														 </td> </tr> </table> </div> \
									<div class="sw-btn-bar-wizard" style="margin-right:5px;">' +
                                    SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Submit; E=js}', { type: 'primary', id: 'customizeSubmit' }) +
								    SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}', { type: 'secondary', id: 'customizeCancel' }) +
									'</div> \
								</div>');
	$('body').append($customizeDiv);
	var $cancelButton = $customizeDiv.find("#customizeCancel");
	var $submitButton = $customizeDiv.find("#customizeSubmit");
	var $dropDownProp = $customizeDiv.find("#groupByProperty");
	var $caption = $customizeDiv.find("#customizeCaption");
	var customizeMode = "Nodes";
	showCustomizeDialog = function (objectName) {
		$dropDownProp.children().remove();
		customizeMode = objectName;
		var objectTitle;
		switch (objectName) {
			case 'Nodes':
				$caption[0].innerHTML = "<b>" + "@{R=Core.Strings;K=WEBJS_VB0_122; E=js}" + "</b> <br>";
				objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_125; E=js}";
				break;
			case 'Interfaces':
				$caption[0].innerHTML = "<b>" + "@{R=Core.Strings;K=WEBJS_VB0_123; E=js}" + "</b> <br>";
				objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_126; E=js}";
				break;
			case 'Volumes':
				$caption[0].innerHTML = "<b>" + "@{R=Core.Strings;K=WEBJS_VB0_124; E=js}" + "</b> <br>";
				objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_127; E=js}";
				break;
		    case 'ActiveAlerts':
		        $caption[0].innerHTML = SW.Core.String.Format("<b>{0}</b><br/>", "@{R=Core.Strings;K=WEBJS_PS0_63;E=js}");
		        objectTitle = "@{R=Core.Strings;K=WEBJS_PS0_61;E=js}";
		        break;
			default:
				objectTitle = "@{R=Core.Strings;K=WEBJS_VB0_125; E=js}";
				break;
		}

		$.ajax({
			type: 'Post',
			url: '/Orion/Services/CustomizeBreadcrumb.asmx/GetListOfOptions',
			data: "{'objectName': '" + objectName + "'}",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (result) {
				$dropDownProp.append(result.d);
				var dialog = $customizeDiv.dialog({
					width: 480, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: objectTitle,
					open: function (event, ui) {
						var $dialogError = $(this).find('.dialogError');
						if ($dialogError.length != 0) {
							$dialogError.remove();
						}
					}
				});
				dialog.show();
			},
			error: function (error) {
				switch (objectName) {
					case 'Nodes':
						$dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_128; E=js}</option>");
						break;
					case 'Interfaces':
						$dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_129; E=js}</option>");
						break;
					case 'Volumes':
						$dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_130; E=js}</option>");
						break;
				    case 'ActiveAlerts':
				        $dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_PS0_60; E=js}</option>");
				        break;
					default:
						$dropDownProp.append("<option value=''>@{R=Core.Strings;K=WEBJS_VB0_128; E=js}</option>");
						break;
				}

				var dialog = $customizeDiv.dialog({
					width: 480, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: objectTitle
				});
				dialog.show();
			}
		});
	};

	$("#customizeNodeList").click(function () {
		showCustomizeDialog("Nodes");
		setStyles();
		return false;
	});

	$("#customizeInterfaceList").click(function () {
		showCustomizeDialog("Interfaces");
		setStyles();
		return false;
	});

	$("#customizeVolumeList").click(function () {
		showCustomizeDialog("Volumes");
		setStyles();
		return false;
	});

	$("#customizeActiveAlertList").click(function () {
	    showCustomizeDialog("ActiveAlerts");
	    setStyles();
	    return false;
	});

	$cancelButton.click(function () {
		$customizeDiv.dialog("close");
	});

	$submitButton.click(function () {
		$.ajax({
			type: 'Post',
			url: '/Orion/Services/CustomizeBreadcrumb.asmx/SaveBcCustomization',
			data: "{'customizationValue': '" + $dropDownProp.val() + "', 'objectName': '" + customizeMode + "'}",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (result) {
				$customizeDiv.dialog("close");
				location.reload();
			},
			error: function (xhr, status, error) {
				var msg = JSON.parse(xhr.responseText);
				if ($customizeDiv.find('.dialogError').length == 0)
					$customizeDiv.find('#inside table').after(String.format('<span class="dialogError" style="color:red;font-size: small;">{0}</span>', msg.Message));
			}
		});
	});

	setStyles = function () {
		$("ul.bc_submenu").hide();
		$("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });

		isClicked = false;
	};
});
