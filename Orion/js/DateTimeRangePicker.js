﻿$(function () {
    var containerClass = ".sw-dateTimeRangePicker";

    $(".sw-dateTimeRangePicker-startTimeTypeSelect").on("change", function () {
        var self = $(this);
        var startingOnPicker = self.closest(containerClass).find(".sw-dateTimeRangePicker-startingOn-picker");

        var isSelected = $(this).val() == "SpecificDate";
        if (isSelected) {
            startingOnPicker.show();
        } else {
            startingOnPicker.hide();
        }

        startingOnPicker.data('shown', isSelected);
    }).change();

    $(".sw-dateTimeRangePicker-endingOnCheckbox input[name$=endingOnEnable]").on("change", function () {
        var self = $(this);
        var parent = self.closest(containerClass);
        var endingOnPicker = parent.find(".sw-dateTimeRangePicker-endingOn-picker");

        var isChecked = $(this).is(":checked");
        if (isChecked) {
            endingOnPicker.show();
        } else {
            endingOnPicker.hide();
        }

        endingOnPicker.data('shown', isChecked);
        parent.trigger("endingOnStatusChanged", [isChecked]);
    }).change();


    // define methods
    (function ($) {

        if (typeof $.fn.getDateTimeRange !== "function") {
            $.fn.getDateTimeRange = function(defaultFrom, defaultTo) {
                var startPicker = $(".sw-dateTimeRangePicker-startingOn-picker", this);
                var endPicker = $(".sw-dateTimeRangePicker-endingOn-picker", this);
                var start = startPicker.children().last();
                var end = endPicker.children().last();

                if (!start.length || !end.length) {
                    return false;
                }

                return {
                    start: startPicker.data('shown') ? start.orionGetDate() : defaultFrom,
                    end: endPicker.data('shown') ? end.orionGetDate() : defaultTo
                }
            };
        }

        if (typeof $.fn.setDateTimeRangeFrom !== "function") {
            $.fn.setDateTimeRangeFrom = function(date) {
                var start = $(".sw-dateTimeRangePicker-startingOn-picker", this).children().last();
                if (!start.length) {
                    return false;
                }

                start.orionSetDate(date);
                start.find('input').change();
                return this;
            };
        }

        if (typeof $.fn.setDateTimeRangeTo !== "function") {
            $.fn.setDateTimeRangeTo = function(date) {
                var end = $(".sw-dateTimeRangePicker-endingOn-picker", this).children().last();
                if (!end.length) {
                    return false;
                }

                end.orionSetDate(date);
                end.find('input').change();
                return this;
            };
        }

    }(jQuery));

});