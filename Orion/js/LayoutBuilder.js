﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

var splitterWidth = 4;
var minColumnWidth = 300;
var AddResourceDemoLimitation = 10;
// fix for FB218060 there is an issue with Ext.dd.DragDropMgr
// in case you have some nested layout with lots of splitbars they might not working
// found solution there http://www.sencha.com/forum/archive/index.php/t-167936.html?s=6d03f967f921b5f994ea853e38e10473
Ext.dd.DragDropMgr.getLocation = function (oDD) {
    if (!this.isTypeOfDD(oDD)) {
        return null;
    }
    var el = oDD.getEl(), pos, x1, x2, y1, y2, t, r, b, l;
    try {
        pos = Ext.lib.Dom.getXY(el);
    } catch (e) { }

    if (!pos) {
        return null;
    }
    x1 = pos[0];
    x2 = x1 + el.offsetWidth;
    y1 = pos[1];
    y2 = y1 + el.offsetHeight;

    t = y1 - oDD.padding[0];
    r = x2 + oDD.padding[1];
    b = y2 + oDD.padding[2];
    l = x1 - oDD.padding[3];
    return new Ext.lib.Region(t, r, b, l);
};
// end of fix for FB218060

// partial fix for FB216338 when section heigth is bigger then browser window heigth - splitter move used to cause scrolling to top
//in case this.config.scroll is undefined the scrool option shound be false 
Ext.override(Ext.dd.DD, {
    applyConfig: function () {
        Ext.dd.DD.superclass.applyConfig.call(this);
        this.scroll = (this.config.scroll !== false && this.config.scroll !== undefined);
    }
});
// end of fix for FB216338

SW.Orion.LayoutBuilder = function () {
    var mainLayoutPanel;
    var originalWidth;
    var mainWidth;
    var mainWidthWhithoutborders;
    var maxLayoutWidth = 0;
    var wideWidthForLayoutBuilder = 1400;
    var percentValueToSatisfyResizing = 0.25;
    var sectionsData = new Array();
    var layoutTemplates;
    var sectionsSessionName;
    var configsSessionName;
    var noResourcesMessage;
    var resourcePickerLoader;
    var dragAndDropItemConfig;
    var draggingBetweenTwoColumns = false;
    var returnUrl = window.location;
    var posX;
    var dragging = false;
    var portletItemWidth;
    var resourceConfigDictionary = {};
    var maxAvailableColumnCount = 9;
    var emptyRefId = '00000000-0000-0000-0000-000000000000';
    var notSupportedGuid = 'ffffffff-ffff-ffff-ffff-ffffffffffff';
    var self = this;

    Ext.override(Ext.layout.BorderLayout.SplitRegion, {
        render: function (ct, p) {
            Ext.layout.BorderLayout.SplitRegion.superclass.render.call(this, ct, p);

            var ps = this.position;

            this.splitEl = ct.createChild({
                cls: "x-layout-split x-layout-split-" + ps, html: "&#160;",
                id: this.panel.id + '-xsplit'
            });

            if (this.collapseMode == 'mini') {
                this.miniSplitEl = this.splitEl.createChild({
                    cls: "x-layout-mini x-layout-mini-" + ps, html: "&#160;"
                });
                this.miniSplitEl.addClassOnOver('x-layout-mini-over');
                this.miniSplitEl.on('click', this.onCollapseClick, this, { stopEvent: true });
            }

            var s = this.splitSettings[ps];

            this.split = new Ext.SplitBar(this.splitEl.dom, p.el, s.orientation);
            this.split.tickSize = this.tickSize;
            this.split.placement = s.placement;
            this.split.getMaximumSize = this[s.maxFn].createDelegate(this);
            // this fixes issue with dynamic minWidth
            if (s.orientation == Ext.SplitBar.HORIZONTAL) {
                this.split.getMinimumSize = this['getHMinSize'].createDelegate(this);
            }
            this.split.minSize = this.minSize || this[s.minProp];
            this.split.on("beforeapply", this.onSplitMove, this);
            this.split.useShim = this.useShim === true;
            this.maxSize = this.maxSize || this[s.maxProp];

            if (p.hidden) {
                this.splitEl.hide();
            }

            if (this.useSplitTips) {
                this.splitEl.dom.title = this.collapsible ? this.collapsibleSplitTip : this.splitTip;
            }
            if (this.collapsible) {
                this.splitEl.on("dblclick", this.onCollapseClick, this);
            }
        },
        getHMinSize: function () {
            var westWidth = 0;
            var westItems = this.layout.west.items;
            for (var i = 0; i < westItems.length; i++) {
                if (westItems[i].region == 'west') {
                    westWidth = westItems[i].getWidth() + splitterWidth;
                }
            }
            return westWidth + minColumnWidth;
        },
        onSplitMove: function (split, newSize) {
            var s = this.panel.getSize();
            this.lastSplitSize = newSize;
            if (this.position == 'north' || this.position == 'south') {
                this.panel.setSize(s.width, newSize);
                this.state.height = newSize;
            } else {
                this.panel.setSize(newSize, s.height);
                this.state.width = newSize;
            }
            this.layout.layout();
            this.panel.saveState();

            // this is fix for FB226824 - we need to refresh section height after splitter was moved
            var itemId = findSectionElementId($('#' + this.panel.id));
            if (!Ext.isEmpty(itemId)) {
                var section = mainLayoutPanel.find('id', itemId);
                if (!Ext.isEmpty(section) && !Ext.isEmpty(section[0])) {
                    section[0].doLayout();
                    refreshLayoutHeigth(itemId);
                    syncSectionsData();
                }
            }
            // end of fix
            return false;
        }
    });

    $(function () {
        originalWidth = $('#layoutBuilder').width();
        mainWidth = originalWidth - 40;                 // width of Groupbox class minus 40px for paddings
        mainWidthWhithoutborders = mainWidth - 2;       // -2 due to without borders
        updateMainWidth();
    });

    function updateContainerWidth(layoutBuilderWidth, bodyWidth, containerWidth, onSuccess) {
        // default value for layout builder width
        var defaulContainertWidth = layoutBuilderWidth + 200;
        var newWidth;

        if (layoutBuilderWidth == originalWidth) {
            newWidth = bodyWidth;
        } else {
            if (layoutBuilderWidth > bodyWidth) {
                newWidth = defaulContainertWidth;
            } else {
                newWidth = bodyWidth;
            }
        }
        // condition for resizing window
        if (defaulContainertWidth > bodyWidth)
            newWidth = defaulContainertWidth;
        else if (containerWidth < bodyWidth) {
            newWidth = bodyWidth;
        }

        if (onSuccess && containerWidth != newWidth)
            onSuccess(newWidth);
    }

    function updateMainWidth(newWidth) {
        var $layoutBuilder = $('#layoutBuilder');
        //update required only for container width
        if (!((typeof newWidth === "undefined") || newWidth === "undefined")) {
            if ($layoutBuilder.width() != newWidth) {
                $layoutBuilder.width(newWidth);
                mainWidth = newWidth - 40;                              // width minus 40px for paddings
                mainWidthWhithoutborders = mainWidth - 2;               // -2 due to without borders
            }
        }

        var container = $('body #container');
        if (Ext.isEmpty(container))
            return;

        var bodyWidth = $('body').width();
        var layoutBuilderWidh = $layoutBuilder.width();

        updateContainerWidth(layoutBuilderWidh, bodyWidth, container.width(), function (newContainerWidth) {
            container.width(newContainerWidth);
        });
    }

    function recalculateMainWidth(itemCount) {
        //increase layout width by multiplying column count on minimum column width and on 120%
        var newWidth = minColumnWidth * itemCount * 1.2;
        var defaultWidth = originalWidth;
        if (itemCount != 1 && minColumnWidth > newWidth * percentValueToSatisfyResizing) {
            defaultWidth = wideWidthForLayoutBuilder;
        } else if (itemCount == 1) {
            defaultWidth = originalWidth;
        }
        return defaultWidth > newWidth ? defaultWidth : newWidth;
    }

    function getSinglePlaceHolder(region, width, sectionId, columnId) {
        var templateHtml = String.format(
                '<table style="width: 100%" cellpadding="0" cellspacing="0">' +
                    '    <tr><td><div style="height: 30px;background-color: #f8f8f8;" class="layout-percent-indicator"><div class="percentSquare">100%</div></div></td></tr>' +
                        '    <tr><td><div id="layout-resources-{0}" class="columnItem">{1}</div></td></tr>' +
                            '<table>',
                sectionId + '__' + columnId, noResourcesMessage);
        var addContentBtnId = 'addResBtn_' + Ext.id();
        return new Ext.Panel({
            region: region, layout: 'column', cls: 'no-border layout-column', autoHeight: true, border: false, width: width, split: true, stateful: false, minWidth: minColumnWidth, id: sectionId + '__' + columnId,
            items: [
                        { html: templateHtml, border: false },
                        new Ext.Panel({ id: Ext.id(), layout: 'column', autoHeight: true, cls: 'no-border layout-add-more addResourcesbtn', border: false,
                            items: [{
                                html: SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_SO0_57; E=js}', { type: 'secondary', id: addContentBtnId, cls: 'addContentBtn' }),
                                border: false,
                                listeners: {
                                    afterrender: function () {
                                        $('#' + addContentBtnId).click({ param1: sectionId, param2: columnId }, function (event) {
                                            var params = [event.data.param1, event.data.param2];
                                            resourcePickerLoader.AddContent(params);
                                        });
                                    }
                                }
                            }]
                        })],
            listeners: {
                resize: function (el, adjWidth) {
                    var parts = el.id.split('__');
                    var percentVal = 100;
                    for (var j = 0; j < sectionsData.length; j++) {
                        if (sectionsData[j].RefId == parts[0]) {
                            percentVal = Math.round(adjWidth / (mainWidthWhithoutborders - (sectionsData[j].Columns.length - 1) * splitterWidth) * 100);
                            for (var i = 0; i < sectionsData[j].Columns.length; i++) {
                                if (sectionsData[j].Columns[i].RefId == parts[1]) {
                                    sectionsData[j].Columns[i].PercentWidth = percentVal;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    $('#' + el.id).find('.percentSquare').each(function () {
                        this.innerHTML = percentVal + '%';
                    });
                }
            }
        });
    }

    function findSectionElementId(element) {
        var parents = $(element).parents();
        for (var i = 0; i < parents.length; i++) {
            if (parents[i].id.indexOf('layout-section-') > -1)
                return parents[i].id;
        }
        return null;
    }

    function selectSectionElement(sectionId, findHandler) {
        var secId = sectionId.replace('layout-section-', '');
        for (var i = 0; i < sectionsData.length; i++) {
            if (secId && secId == sectionsData[i].RefId) {
                findHandler(sectionsData[i], i);
                return;
            }
        }
    }

    function selectColumnElement(columnId, findHandler, sectionId) {
        for (var i = 0; i < sectionsData.length; i++) {
            if (!sectionId || sectionsData[i].RefId == sectionId) {
                for (var j = 0; j < sectionsData[i].Columns.length; j++) {
                    if (!columnId || sectionsData[i].Columns[j].RefId == columnId) {
                        findHandler(sectionsData[i].Columns[j]);
                        return;
                    }
                }
            }
        }
    }

    function selectCellElement(cellId, findHandler, sectionId, columnId) {
        var cellRefId = cellId.replace('selection-cell-', '');
        for (var i = 0; i < sectionsData.length; i++) {

            if (!sectionId || sectionsData[i].RefId == sectionId) {
                for (var j = 0; j < sectionsData[i].Columns.length; j++) {

                    if (!columnId || sectionsData[i].Columns[j].RefId == columnId) {
                        for (var k = 0; k < sectionsData[i].Columns[j].Cells.length; k++) {
                            if (sectionsData[i].Columns[j].Cells[k].RefId == cellRefId) {
                                findHandler(sectionsData[i].Columns[j].Cells[k]);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    function refreshLayoutHeigth(id) {
        var items = mainLayoutPanel.find('id', id);
        for (var i = 0; i < items.length; i++) {
            fixChildPanelsHeight(items[i]);
        }
    }

    // Panels with border layout doesn't support autoHeigth - this method set parent panel heigth accourding to content
    function fixChildPanelsHeight(item) {
        var childItems = item.items.items;
        var maxHeight = 0;
        for (var i = 0; i < childItems.length; i++) {
            if (childItems[i].layout.type == 'border') {
                var height = fixChildPanelsHeight(childItems[i]);
                childItems[i].setHeight(height);
                childItems[i].syncHeight();
                childItems[i].expand();
                childItems[i].doLayout();
                maxHeight = Math.max(maxHeight, height);
            }
            else {
                if (childItems[i].getHeight) {
                    maxHeight = Math.max(maxHeight, childItems[i].getHeight() + 1); // add one pixel to avoid any border hiding (seems getHeigth returns incorrect value) 
                }
            }
        }
        return maxHeight;
    }

    function getParentPlaceHolder(region, items, width) {
        return new Ext.Panel({
            id: Ext.id(), region: region, layout: 'border', height: 60, cls: 'no-border layout-item-parent-place-holder', border: false, items: items, split: true, stateful: false, width: width
        });
    }

    function getLayoutTemplate(sectionId) {
        return new Ext.Panel({ id: 'layout-section-' + sectionId, layout: 'column', autoHeight: true, cls: 'no-border layout-item', border: false, autoWidth: true, items: [] });
    }

    function getNumericTemplate() {
        return new Ext.form.NumberField({
            id: 'spinner_' + Ext.id(),
            width: 50,
            enableKeyEvents: true,
            allowBlank: false,
            allowDecimals: false,
            allowNegative: false,
            maxValue: 9,
            minValue: 1,
            listeners: {
                specialkey: function (field, e) {
                    // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
                    // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
                    if (e.getKey() == e.ENTER) {
                        this.blur();
                    }
                }
            }
        });
    }

    function getRegionByIndex(index, count) {
        if (index == 0 && count > 1) return 'west';
        return 'center';
    }

    function getConfiguredColumns(layoutId, columnsConfig, row) {
        // initialize initial columns array
        var itemsArray = new Array();
        for (var i = 0; i < columnsConfig.length; i++) {
            var layoutColumns = sectionsData[row].Columns;
            var columnId = columnsConfig[i].RefId ? columnsConfig[i].RefId : SW.Core.Services.generateNewGuid();
            // if cell already within the config then reinitialize cell properties, else add new cell
            var percentWidth = Math.round(columnsConfig[i].width / (mainWidthWhithoutborders - (columnsConfig.length - 1) * splitterWidth) * 100);
            if (layoutColumns[i] != null || layoutColumns[i] != undefined)
                layoutColumns[i] = { RefId: columnId, PixelWidth: null, PercentWidth: percentWidth, BorderStyle: '', Title: '', Subtitle: '', Cells: [] };
            else {
                sectionsData[row].Columns.push({ RefId: columnId, PixelWidth: null, PercentWidth: percentWidth, BorderStyle: '', Title: '', Subtitle: '', Cells: [] });
            }
            itemsArray.push(getSinglePlaceHolder(getRegionByIndex(i, columnsConfig.length), columnsConfig[i].width, layoutId, columnId));
        }
        // group columns array to parent panels (each parent item should have max 3 child)
        return configureItemsArray(itemsArray);
    }

    function rebuildLayoutItem(layoutId, columnsConfig, row) {
        var layoutItem = mainLayoutPanel.find('id', layoutId)[0];
        // clear existing items
        layoutItem.removeAll();
        layoutItem.setWidth(mainWidth);
        layoutItem.doLayout();

        var configuredItems = getConfiguredColumns(layoutId.replace('layout-section-', ''), columnsConfig, row);
        for (var j = 0; j < configuredItems.length; j++) {
            layoutItem.add(configuredItems[j]);
        }
        // refresh layout item after update
        layoutItem.doLayout();
        // initialize drag&drop feature
        initDragAndDrop();
    }

    function syncSectionsData() {
        SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'SetSessionData', { name: sectionsSessionName, value: Ext.util.JSON.encode(sectionsData) }, function () { });
    }

    function syncTimeFrameSectionsData(refId, timeFrameId) {
        setTimeFrameId(refId, timeFrameId);
        syncSectionsData();
    }

    function syncDataSourceSectionsData(refId, dataSourceId) {
        setDataSourceId(refId, dataSourceId);
        syncSectionsData();
    }

    function setTimeFrameId(refId, timeFrameId) {
        for (var i = 0; i < sectionsData.length; i++) {
            for (var j = 0; j < sectionsData[i].Columns.length; j++) {
                for (var k = 0; k < sectionsData[i].Columns[j].Cells.length; k++) {
                    if (sectionsData[i].Columns[j].Cells[k].RefId == refId) {
                        sectionsData[i].Columns[j].Cells[k].TimeframeRefId = timeFrameId;
                        return;
                    }

                }
            }
        }
    }

    function setDataSourceId(refId, dataSourceId) {
        for (var i = 0; i < sectionsData.length; i++) {
            for (var j = 0; j < sectionsData[i].Columns.length; j++) {
                for (var k = 0; k < sectionsData[i].Columns[j].Cells.length; k++) {
                    if (sectionsData[i].Columns[j].Cells[k].RefId == refId) {
                        sectionsData[i].Columns[j].Cells[k].DataSelectionRefId = dataSourceId;
                        return;
                    }

                }
            }
        }
    }

    // this recursive method groups items into parent panels, due to 'border' layout supports only 'center', 'west' and 'east' horizontal regions.
    // so in case we have many columns we need to grop them into parent panels to have maximum 3 childs
    function configureItemsArray(array) {
        // if array is empty of have only one element then no need to group anything
        if (array.length <= 1)
            return array;

        var parentArray = new Array();
        var childArray = new Array();
        // calculate how many parent items we're expectiang in this iteration
        var parentArrayCount = array.length - 1;

        var width = 0;
        var leftPanelExists = false;
        for (var i = 0; i < array.length; i++) {
            if (!leftPanelExists) {
                // set up child items array (max 3 child)
                childArray.push(array[i]);
                //calculate parent panal width
                width += array[i].width;
                // unite first two child
                if (i == 1) {
                    leftPanelExists = true;
                    width += splitterWidth; // adda extraspace for splitter
                    parentArray.push(getParentPlaceHolder(getRegionByIndex(parentArray.length, parentArrayCount), childArray, width));
                }
            }
            else {
                parentArray.push(array[i]);
            }
        }
        return configureItemsArray(parentArray);
    }

    function redirectToEdit(param1, param2) {
        var supportedInterfaces = resourceConfigDictionary[param1].supportedInterfaces;
        var dsGuid = '';
        var sectionCellRefId;
        var dataSourceId;

        selectCellElement(param2, function (cell) { dataSourceId = cell.DataSelectionRefId; sectionCellRefId = cell.RefId; });
        if (supportedInterfaces.length > 0) {
            if (dataSourceId == emptyRefId) {
                Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_SO0_69; E=js}', msg: '@{R=Core.Strings;K=WEBJS_SO0_68; E=js}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return false;
            }
            dsGuid = '&DatasourceID=' + dataSourceId;
        }
        var storageGuid = $('#HiddenReportWizardGuid').val();
        $('#RedirectToUrlHiddenField').val('/Orion/Reports/EditReportResource.aspx?SessionId=' + configsSessionName + '&RefId=' + param1 + dsGuid + '&ReportStorageGuid=' + storageGuid +
            '&SectionsSessionId=' + sectionsSessionName + '&SectionCellRefId=' + sectionCellRefId + '&ReturnTo=' + returnUrl);
        __doPostBack();
        return false;
    }

    function createResButtons(resId, editButtonText) {
        var editBtnMarckup = '<div class="buttons">' +
                                '<a href="" class="greyBtn">' +
                                        '<img src="/Orion/images/Reports/edit_16x16.png" alt=""/>' +
                                              editButtonText.toUpperCase() +
                               '</a>' +
                          '</div>';
        $("#editTableBtnPlaceHolder_" + resId).append(editBtnMarckup).click(function (event) {
            var refId = this.id.replace('editTableBtnPlaceHolder_', '');
            selectCellElement(refId, function (cell) {
                redirectToEdit(cell.ConfigId, cell.RefId);
            });
            event.preventDefault();
        });
    }

    function addResourcesToColumn(layoutId, column, resources, positionOfOriginalResource, refreshHeightNeeded) {
        if (($("#isOrionDemoMode").length != 0) && $('.portletItem').length >= AddResourceDemoLimitation) {
            demoAction("Core_Reporting_AddResourcesLimit", this, AddResourceDemoLimitation);
            return;
        }
        var sectionGuid = layoutId.replace('layout-section-', '');
        var columnItem = $('#layout-resources-' + sectionGuid + '__' + column);
        if (columnItem.length > 0) {
            if (resources.length > 0)
                $(columnItem).find('div.layout-empty-message').remove();
            for (var i = 0; i < resources.length; i++) {
                // get resource options from dictionary
                var supportDtFilter = resourceConfigDictionary[resources[i].ConfigId].supportDateTimeFilter;
                var supportedInterfaces = resourceConfigDictionary[resources[i].ConfigId].supportedInterfaces;
                var resourceIconPath = resourceConfigDictionary[resources[i].ConfigId].iconPath;
                var editButtonText = resourceConfigDictionary[resources[i].ConfigId].editButtonText;
                var showDataSource = ((typeof supportedInterfaces === "undefined") ? (resources[i].DataSelectionRefId != emptyRefId) : (supportedInterfaces.length > 0)) && (resources[i].DataSelectionRefId != notSupportedGuid);
                var showDtControl = (supportDtFilter || resources[i].TimeframeRefId != emptyRefId) && (resources[i].TimeframeRefId != notSupportedGuid);
                var displayTitle = (resourceConfigDictionary[resources[i].ConfigId] == null) ? resources[i].DisplayName : resourceConfigDictionary[resources[i].ConfigId].title;
                var isPostionDefined = (typeof positionOfOriginalResource === "undefined") || positionOfOriginalResource === "undefined" ? false : true;

                var menuHtml = '';

                var dataSourceHtml = '';
                if (showDataSource) {
                    dataSourceHtml = String.format('<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;width: auto;display: inline;float: left;text-align:right;">' +
                '<tr><td style="text-align: right; width: 45px; font-weight: normal;">{0}</td>' +
                '<td>{1}</td></tr></table>',
                 '@{R=Core.Strings;K=WEBJS_IB0_26;E=js}',
                 '<div id="dataSource_' + resources[i].RefId + '" ></div>');
                }

                var dtHtml = '';
                if (showDtControl) {
                    dtHtml = String.format('<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;width: auto;display: inline;float: left;text-align:right;">' +
                '<tr><td style="text-align: right; width: 45px;font-weight: normal;">{0}</td><td style="text-align:left;">{1}</td></tr></table>',
                 '@{R=Core.Strings;K=Reports_YK0_1;E=js}',
                 '<div id="timeFrame_' + resources[i].RefId + '" ></div>'
                 );
                }

                if (showDtControl || showDataSource) {
                    menuHtml = '<table cellspacing="0" cellpadding="0">' +
                        '<tr>' +
                            '<td>' +
                                '<div style="clear:both;">' + dataSourceHtml + dtHtml + '</div>' +
                            '</td>' +
                        '</tr>' +
                     '</table>';
                }

                var newPortletItem = '<div class="portletItem shadow" id="selection-cell-' + resources[i].RefId + '">' +
                    '<div class="portlet-header">' +
                        '<table cellspacing="0" cellpadding="0">' +
                            '<tr>' +
                                '<td style="width: 18px;padding-right: 10px;vertical-align: top;padding-top: 0;">' +
                                        '<div style="float: right;margin-left: 10px;display:inline;vertical-align: bottom;">' +
                                            '<img style="padding-top: 10px;" src="/Orion/images/LayoutBuilder/ddpickres.png"/>' +
                                        '</div>' +
                                '</td>' +
                                '<td style="padding-top: 0px;">' +
                                    '<div style="display: inline; float: left; padding-top: 7px; padding-right: 25px;">' +
                                        '<img style="margin-right: 5px;" src="' + resourceIconPath + '"/>' +
                                        '<span style="position: relative;bottom: 3px;">' + Ext.util.Format.htmlEncode(displayTitle) + '</span>' +
                                    '</div>' +
                                    '<div id="editTableBtnPlaceHolder_' + resources[i].RefId + '" style="display:inline;float: left;padding-right:10px;padding-top: 7px;"></div>' +
                                '</td>' +
                                '<td style="vertical-align: top;">' +
                                    '<div style="text-align: right;white-space:nowrap;">' +
                                        '<span id="selection-cell-' + resources[i].RefId + '-duplicate' + '" style="cursor:pointer;' + '">' +
                                            '<img style="margin-right: 5px;" class="manageBtns" class="resource-manage-btn" title="@{R=Core.Strings;K=WEBJS_PS0_3; E=js}" alt="@{R=Core.Strings;K=WEBJS_PS0_3; E=js}" src="/Orion/images/LayoutBuilder/DuplicateResource_16x16.png"/>' +
                                            '<span style="white-space: nowrap;padding-right: 15px;vertical-align: top;font-size: 10px;position: relative;top: 2px;">' +
                                                '@{R=Core.Strings;K=WEBJS_RB0_5; E=js}' +
                                            '</span>' +
                                        '</span>' +
                                        '<img class="manageBtns" id="selection-cell-' + resources[i].RefId + '-remove' + '" class="resource-manage-btn" title="@{R=Core.Strings;K=WEBJS_PS0_4; E=js}" alt="@{R=Core.Strings;K=WEBJS_PS0_4; E=js}" src="/Orion/images/LayoutBuilder/deleteres.png"/>' +
                                    '</div>' +
                                '</td>' +
                            '</tr>' +
                        '</table>' +
                        menuHtml +
                    '</div></div>';
                if (isPostionDefined) {
                    var portletItem = $(columnItem).children(String.format(":nth-child({0})", positionOfOriginalResource + 1));
                    portletItem.after(newPortletItem);

                    for (var k = 0; k < sectionsData.length; k++) {
                        if (sectionsData[k].RefId == sectionGuid) {
                            for (var h = 0; h < sectionsData[k].Columns.length; h++) {
                                if (sectionsData[k].Columns[h].RefId == column) { sectionsData[k].Columns[h].Cells.splice(positionOfOriginalResource + 1, 0, resources[i]); break; }
                            }
                            break;
                        }
                    }
                } else {
                    $(columnItem).append(newPortletItem);

                    for (var j = 0; j < sectionsData.length; j++) {
                        if (sectionsData[j].RefId == sectionGuid) {
                            for (var m = 0; m < sectionsData[j].Columns.length; m++) {
                                if (sectionsData[j].Columns[m].RefId == column) { sectionsData[j].Columns[m].Cells.push(resources[i]); break; }
                            }
                            break;
                        }
                    }
                }
                createResButtons(resources[i].RefId, editButtonText);

                if (showDtControl) {
                    $('#timeFrame_' + resources[i].RefId).timePeriods.settings.controlId = "timeFrame_" + resources[i].RefId;
                    $('#timeFrame_' + resources[i].RefId).timePeriods.syncTimeFrameIdDelegate = syncTimeFrameSectionsData;
                    $('#timeFrame_' + resources[i].RefId).timePeriods('InitTimeFrames', resources[i].TimeframeRefId);
                }

                if (showDataSource) {
                    var resFile = resourceConfigDictionary[resources[i].ConfigId].path;
                    $('#dataSource_' + resources[i].RefId).dataSources.settings.controlId = "dataSource_" + resources[i].RefId;
                    $('#dataSource_' + resources[i].RefId).dataSources.syncDataSourceIdDelegate = syncDataSourceSectionsData;
                    $('#dataSource_' + resources[i].RefId).dataSources('InitDataSources', resources[i].DataSelectionRefId, supportedInterfaces, resFile);
                }

                $('#selection-cell-' + resources[i].RefId + '-edit').click({ param1: resources[i].ConfigId, param2: resources[i].RefId }, redirectToEdit);

                $('#selection-cell-' + resources[i].RefId + '-duplicate').click(function () {
                    var resourceId = this.id.replace('-duplicate', '');
                    SW.Orion.LayoutBuilder.duplicateResource(this, resourceId);
                    return false;
                });
                $('#selection-cell-' + resources[i].RefId + '-remove').click(function () {
                    var resourceId = this.id.replace('-remove', '');
                    SW.Orion.LayoutBuilder.removeResourceFromColumn(this, resourceId, false);
                    return false;
                });
            }
            initDragAndDrop();
            if (refreshHeightNeeded)
                refreshLayoutHeigth('layout-section-' + sectionGuid);
        }
    }

    function moveResourceIntoColumn(column, item) {
        var index = item.parent().children().index(item);
        var cellId = item[0].id;
        var sectionId = findSectionElementId(item[0]);
        var positionInfo = column.id.replace('layout-resources-', '').split('__');
        if (dragAndDropItemConfig != null && dragAndDropItemConfig.RefId == cellId.replace('selection-cell-', '')) {
            for (var j = 0; j < sectionsData.length; j++) {
                if (sectionsData[j].RefId == sectionId.replace('layout-section-', '')) {
                    for (var i = 0; i < sectionsData[j].Columns.length; i++) {
                        if (sectionsData[j].Columns[i].RefId == positionInfo[1]) {
                            sectionsData[j].Columns[i].Cells.splice(index, 0, dragAndDropItemConfig);
                            break;
                        }
                    }
                    break;
                }
            }
        }
        $(column).find('div.layout-empty-message').remove();
    }

    // this method do initialization for drag&drop
    function initDragAndDrop() {
        var previousPlaceHolderId;
        $(".portletItem").mousemove(function (e) {
            // Calculate offset only when there is no dragging items
            if (!dragging) {
                // Offset from left border to cursor position
                posX = e.pageX - $(this).offset().left;
            }
        });

        $(".columnItem").sortable({ items: "> .portletItem", connectWith: ".columnItem",
            handle: ".portlet-header",
            helper: 'clone',
            appendTo: 'body',
            zIndex: 10000,
            tolerance: 'pointer',
            start: function (event, ui) {
                if (Ext.isIE7)
                    ui.helper.width(400);
                else
                    ui.helper.addClass('portletDrag');
                ui.helper.removeClass('shadow');
                portletItemWidth = ui.helper.width();
                dragging = true;
                previousPlaceHolderId = ui.placeholder[0].parentNode.id;
                // decrease placeholder height due to borders
                var phHeight = ui.item.height() - 2;
                // consider shadow height in IE7 && IE8
                if (Ext.isIE7 || Ext.isIE8) {
                    ui.placeholder.removeClass('shadow');
                    phHeight -= 4;
                }
                ui.placeholder.height(phHeight);
            },
            sort: function (event, ui) {
                // Offset from left border to cursor position minus half width of portletItem
                var portletItemOffset = ui.position.left + posX - portletItemWidth / 2;
                ui.helper.css({ 'left': portletItemOffset + 'px' });
            },
            change: function (event, ui) {
                var sectionCurrentId = findSectionElementId(ui.placeholder[0]);
                var sectionPreviousId = findSectionElementId($('#' + previousPlaceHolderId));

                var currentPlaceholderId = ui.placeholder.context.parentNode.id;
                var $prevPlaceholderEmptyMessage = $('#' + previousPlaceHolderId + ' .layout-empty-message');

                // if placeholders are different
                // this check is needed because change event fires twice
                if (previousPlaceHolderId != currentPlaceholderId) {
                    $('#' + currentPlaceholderId + ' .layout-empty-message').hide();
                    // condition to check if there is need to show empty message
                    // there are two scenarios when dragging
                    // 1. there can be no portletItems
                    // 2. there can be portletItem but exactly that we are dragging at the moment
                    var $previousPlaceHolderPortletItems = $('#' + previousPlaceHolderId + ' .portletItem');
                    if ($previousPlaceHolderPortletItems.length == 0 ||
                        ($previousPlaceHolderPortletItems.length == 1 && !$previousPlaceHolderPortletItems.is(':visible'))) {
                        if ($prevPlaceholderEmptyMessage.length != 0)
                            $prevPlaceholderEmptyMessage.show();
                        else {
                            $('#' + previousPlaceHolderId).prepend(noResourcesMessage);
                        }
                    }
                    // refresh section height
                    if (sectionCurrentId != sectionPreviousId)
                        refreshLayoutHeigth(sectionPreviousId);
                    refreshLayoutHeigth(sectionCurrentId);
                }
                previousPlaceHolderId = ui.placeholder[0].parentNode.id;
            },
            remove: function (event, ui) {
                selectCellElement(ui.item[0].id, function (cell) { dragAndDropItemConfig = cell; }); SW.Orion.LayoutBuilder.removeResourceFromColumn(this, ui.item[0].id, true);
            },
            receive: function (event, ui) {
                draggingBetweenTwoColumns = true;
                moveResourceIntoColumn(this, ui.item);
            },
            stop: function (event, ui) {
                if (!draggingBetweenTwoColumns) {
                    updateColumn(this, ui.item);
                }
                draggingBetweenTwoColumns = false;
                syncSectionsData();
                dragging = false;
                ui.item.addClass('shadow');

                if (!Ext.isIE7)
                    ui.item.removeClass('portletDrag');
                var sectionId = findSectionElementId(ui.item[0]);
                refreshLayoutHeigth(sectionId);
            }
        });
        $(".columnItem").disableSelection();

        var panelId = "#" + mainLayoutPanel.getId();
        $(panelId).sortable({ helper: 'clone', items: "div.portletSection", connectWith: panelId, tolerance: 'pointer', handle: ".layout-item-header", containment: 'document', placeholder: "sortableSectionPlaceholder",
            start: function (event, ui) { ui.helper.css('border', 'solid 1px #006699'); },
            stop: function (event, ui) { reconfigureSections(this, ui.item); syncSectionsData(); }
        });
    }

    function reconfigureSections(column, item) {
        var positionWhereToPaste = item.parent().children().index(item);
        var sectionId = item.find(".layout-item")[0].id.replace('layout-section-', '');
        var sectionCopy = {};
        for (var i = sectionsData.length - 1; i >= 0; i--) {
            if (sectionId == sectionsData[i].RefId) {
                sectionCopy = sectionsData[i];
                sectionsData.splice(i, 1);
                break;
            }
        }
        sectionsData.splice(positionWhereToPaste, 0, sectionCopy);
    }

    function updateColumn(column, item) {
        var positionInfo = column.id.replace('layout-resources-', '').split('__');
        var sectionId = findSectionElementId(item[0]);
        var sectId = sectionId.replace('layout-section-', '');
        var index = item.parent().children().index(item);
        var cellId = item[0].id.replace('selection-cell-', '');

        var itemConfig;
        selectCellElement(item[0].id, function (cell) { itemConfig = cell; }, sectId, positionInfo[1]);

        for (var j = 0; j < sectionsData.length; j++) {
            if (sectionsData[j].RefId == sectId) {
                for (var k = 0; k < sectionsData[j].Columns.length; k++) {
                    if (sectionsData[j].Columns[k].RefId == positionInfo[1]) {
                        for (var i = 0; i < sectionsData[j].Columns[k].Cells.length; i++) {
                            if (cellId == sectionsData[j].Columns[k].Cells[i].RefId) {
                                sectionsData[j].Columns[k].Cells.splice(i, 1);
                            }
                        }
                        if (itemConfig != null && itemConfig.RefId == cellId) {
                            sectionsData[j].Columns[k].Cells.splice(index, 0, itemConfig);
                        }

                        $(column).find('div.layout-empty-message').remove();
                        refreshLayoutHeigth(sectionId);
                        break;
                    }
                }
                break;
            }
        }
    }

    function checkIfAllSectionsContainsOnlyOneColumn(columnsConfig) {
        for (var i = 0; i < columnsConfig.length; i++) {
            if (columnsConfig[i].columns.length != 1)
                return false;
        }
        return true;
    }

    function updateLayoutTemplate(columnsConfig, layoutIcon, insertResourcesIntoFirstColumn) {
        var sectionCells = [];
        maxLayoutWidth = 0;
        if (checkIfAllSectionsContainsOnlyOneColumn(columnsConfig))
            updateMainWidth(originalWidth);
        else
            updateMainWidth(wideWidthForLayoutBuilder);

        if (insertResourcesIntoFirstColumn != false) {
            for (var m = 0; m < sectionsData.length; m++) {
                for (var k = 0; k < sectionsData[m].Columns.length; k++) {
                    for (var l = 0; l < sectionsData[m].Columns[k].Cells.length; l++) {
                        sectionCells.push(sectionsData[m].Columns[k].Cells[l]);
                    }
                }
            }
        }
        sectionsData = [];

        for (var j = mainLayoutPanel.items.length - 2; j >= 0; j--) {
            mainLayoutPanel.remove(mainLayoutPanel.items.items[j], true);
        }
        layoutTemplates.setText(String.format('<img src="{0}"/>', layoutIcon));
        for (var i = 0; i < columnsConfig.length; i++) {
            // add new layout section if needed;
            SW.Orion.LayoutBuilder.addNewLayoutTemplate(false, columnsConfig[i].RefId, 'undefined', columnsConfig[i].columns);
            var layoutId = 'layout-section-' + sectionsData[i].RefId;
            if (insertResourcesIntoFirstColumn != false)
                if (i == 0) addResourcesToColumn(layoutId, sectionsData[i].Columns[0].RefId, sectionCells, 'undefined', true);
        }
        mainLayoutPanel.doLayout();
    }

    var layoutArray;
    var layoutImage = '/Orion/images/LayoutBuilder/l01.png';

    function initializeLayoutTemplates() {
        layoutArray = [{ columns: [{ width: 1}]}];
        layoutTemplates = new Ext.Toolbar.SplitButton({
            id: 'layoutTemplates',
            text: '<img src="/Orion/images/LayoutBuilder/l01.png"/>',
            height: 25,
            cls: 'layout-templates',
            menu: {
                id: 'layoutTemplatesMenu',
                items: [{
                    text: '<img id="l01" src="/Orion/images/LayoutBuilder/l01.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 1}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l01.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l02" src="/Orion/images/LayoutBuilder/l02.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 0.5 }, { width: 0.5}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l02.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l03" src="/Orion/images/LayoutBuilder/l04.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 0.25 }, { width: 0.75}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l04.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l04" src="/Orion/images/LayoutBuilder/l05.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 0.75 }, { width: 0.25}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l05.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l05" src="/Orion/images/LayoutBuilder/l03.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 0.33 }, { width: 0.33 }, { width: 0.34}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l03.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l06" src="/Orion/images/LayoutBuilder/l06.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 1}] }, { columns: [{ width: 0.5 }, { width: 0.5}] }, { columns: [{ width: 1}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l06.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l07" src="/Orion/images/LayoutBuilder/l08.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 1}] }, { columns: [{ width: 0.25 }, { width: 0.75}] }, { columns: [{ width: 1}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l08.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l08" src="/Orion/images/LayoutBuilder/l09.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 1}] }, { columns: [{ width: 0.75 }, { width: 0.25}] }, { columns: [{ width: 1}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l09.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l09" src="/Orion/images/LayoutBuilder/l07.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 1}] }, { columns: [{ width: 0.33 }, { width: 0.33 }, { width: 0.34}] }, { columns: [{ width: 1}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l07.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }, {
                    text: '<img id="l10" src="/Orion/images/LayoutBuilder/l10.png"/>',
                    handler: function () {
                        layoutArray = [{ columns: [{ width: 1}] }, { columns: [{ width: 0.25 }, { width: 0.5 }, { width: 0.25}] }, { columns: [{ width: 1}]}];
                        layoutImage = '/Orion/images/LayoutBuilder/l10.png';
                        updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                    }
                }]
            },
            listeners: {
                scope: this,
                'click': function (field) {
                    updateLayoutTemplate(layoutArray, layoutImage); syncSectionsData();
                }
            }
        });

        layoutTemplates.render("layoutSelect");
    }

    function findParentLayoutId(elem) {
        var layoutId = '';
        if (elem.id.indexOf("layout-resources-") > -1) {
            layoutId = elem.id;
        }
        else {
            //find parent layout item
            var parents = $(elem).parents();
            for (var j = 0; j < parents.length; j++) {
                if (parents[j].id.indexOf("layout-resources-") > -1) {
                    layoutId = parents[j].id;
                    break;
                }
            }
        }
        return layoutId;
    }

    function correctConfigPercentage(columnConfigs, usePercentWidthInsteadOfWidth, onItemProcess, onFinish) {
        var itemPercentWidth;
        var itemWidth;
        var mainLayoutWidth = mainWidthWhithoutborders - (columnConfigs.length - 1) * splitterWidth; // (count - 1) * splitterWidth place for splitter
        for (var i = 0; i < columnConfigs.length; i++) {
            if (usePercentWidthInsteadOfWidth)
                itemPercentWidth = columnConfigs[i].PercentWidth / 100;
            else
                itemPercentWidth = columnConfigs[i].width;

            itemWidth = parseInt(mainLayoutWidth * itemPercentWidth);
            // width of resource must be equal minColumnWidth
            // this is caused by not exact multiplication of mainLayoutWidth * itemPercentWidth
            if ((minColumnWidth - 5 <= itemWidth) && (itemWidth <= minColumnWidth + 5))
                itemWidth = minColumnWidth;

            if (onItemProcess)
                onItemProcess(i, itemWidth, itemPercentWidth, mainLayoutWidth);
        }
        if (onFinish)
            onFinish();
    }

    function calculateDefaultWidths(sectionGuid, count) {
        var mainLayoutWidth = mainWidthWhithoutborders - (count - 1) * splitterWidth; // (count - 1) * splitterWidth place for splitter
        var itemPercentWidth = parseInt(100 / count);
        var itemWidth = parseInt(mainLayoutWidth * itemPercentWidth / 100);
        var configArray = new Array();
        // calculate columns width
        // needed for adding resource after page refresh
        selectSectionElement(sectionGuid, function (section, index) {
            for (var i = 0; i < count; i++) {
                if (i == count - 1) {
                    //if last element then add mod to the width
                    itemWidth += parseInt(mainLayoutWidth % itemWidth);
                }
                configArray.push({ width: itemWidth, RefId: Ext.isEmpty(sectionsData[index].Columns[i]) ? null : sectionsData[index].Columns[i].RefId });
            }
        });
        return configArray;
    }

    function prepareConfigArray(sectionGuid, columnConfigs, columnCount) {
        var configArray = new Array();
        var areDefaultWidthsRequired = false;
        if (Ext.isEmpty(columnConfigs)) {
            configArray = calculateDefaultWidths(sectionGuid, columnCount);
        } else {
            correctConfigPercentage(columnConfigs, true, function (index, itemWidth) {
                if (itemWidth < minColumnWidth) {
                    areDefaultWidthsRequired = true;
                    configArray = calculateDefaultWidths(sectionGuid, columnCount);
                    return;
                }
            }, function () {
                if (!areDefaultWidthsRequired) {
                    // needed for adding resource after page refresh
                    selectSectionElement(sectionGuid, function (section, index) {
                        configArray = prepareConfigArrayWithSelectedPercentage(columnConfigs, true);
                    });
                }
            });
        }
        return configArray;
    }

    function updateAllSections(sectionGuid, count) {
        for (var i = 0; i < sectionsData.length; i++) {
            if (sectionsData[i].RefId == sectionGuid)
            // if no changes were made to appropriate section and we need to update all sections from outside 
                if (sectionsData[i].Columns.length == count)
                    updateSectionLayout(sectionsData[i].RefId, sectionsData[i].Columns.length, sectionsData[i].Columns, true);
                else
                    updateSectionLayout(sectionsData[i].RefId, count, [], false);
            else
                updateSectionLayout(sectionsData[i].RefId, sectionsData[i].Columns.length, sectionsData[i].Columns, true);
        }
        syncSectionsData();
    }

    function updateSectionLayout(sectionGuid, count, columnConfigs, restoreResourcesAtPrevPositions) {
        var layoutId = 'layout-section-' + sectionGuid;
        var configArray = prepareConfigArray(sectionGuid, columnConfigs, count);

        var sectionCells = [];
        selectSectionElement(sectionGuid, function (section, index) {
            for (var k = 0; k < section.Columns.length; k++) {
                if (!Ext.isEmpty(section.Columns[k].Cells))
                    sectionCells.push({ colId: k, cells: section.Columns[k].Cells });
            }
            var columnId = SW.Core.Services.generateNewGuid();
            sectionsData[index] = { BorderStyle: '', RefId: sectionGuid, Columns: [{ RefId: columnId, PixelWidth: null, PercentWidth: 100, BorderStyle: '', Title: '', Subtitle: '', Cells: []}] };
            //add new columns into existing layout
            rebuildLayoutItem(layoutId, configArray, index);
            if (!Ext.isEmpty(sectionCells)) {
                if (restoreResourcesAtPrevPositions) {
                    for (var i = 0; i < sectionCells.length; i++)
                        addResourcesToColumn(sectionGuid, section.Columns[sectionCells[i].colId].RefId, sectionCells[i].cells, 'undefined', true);
                } else {
                    var tempStore = [];
                    for (var i = 0; i < sectionCells.length; i++)
                        for (var j = 0; j < sectionCells[i].cells.length; j++)
                            tempStore.push(sectionCells[i].cells[j]);
                    addResourcesToColumn(sectionGuid, section.Columns[0].RefId, tempStore, 'undefined', true);
                }
            } else {
                refreshLayoutHeigth(layoutId);
            }
        });
    }

    function updateMaxLayoutWidth(mainLayoutWidth) {
        if (maxLayoutWidth < mainLayoutWidth)
            maxLayoutWidth = mainLayoutWidth;
    }

    function increaseSectionWidth(sectionGuid, count) {
        selectSectionElement(sectionGuid, function (section, index) {
            updateSectionLayout(section.RefId, count, section.Columns, true);
        });
    }

    function increaseLayoutWidth(sectionGuid, count) {
        var newWidth = recalculateMainWidth(count);
        updateMainWidth(newWidth);

        var mainLayoutWidth = mainWidthWhithoutborders - (count - 1) * splitterWidth;
        updateMaxLayoutWidth(mainLayoutWidth);

        // update all layouts
        updateAllSections(sectionGuid, count);
    }

    function validateDecreaseLayoutWidth(sectionGuid, count) {
        var tempMaxLayoutWidth = 0;
        var returnOriginalWidth = true;
        for (var ii = 0; ii < sectionsData.length; ii++) {
            var proposedWidth;
            var columnCount;
            if (sectionsData[ii].RefId == sectionGuid)
                columnCount = count;
            else
                columnCount = sectionsData[ii].Columns.length;

            proposedWidth = recalculateMainWidth(columnCount);
            if (tempMaxLayoutWidth < proposedWidth)
                tempMaxLayoutWidth = proposedWidth;

            // check if returning original width(=1024px) is needed
            if (minColumnWidth > (originalWidth / columnCount) || minColumnWidth > originalWidth * percentValueToSatisfyResizing) {
                returnOriginalWidth = false;
            }
        }
        //check if decrementing main width is needed or returning original width
        if (returnOriginalWidth || tempMaxLayoutWidth < maxLayoutWidth) {
            var widthToUpdate;
            if (returnOriginalWidth)
                widthToUpdate = originalWidth;
            else
                widthToUpdate = tempMaxLayoutWidth;

            updateMainWidth(widthToUpdate);
            maxLayoutWidth = mainWidthWhithoutborders - (count - 1) * splitterWidth;
            updateAllSections(sectionGuid, count);
            return true;
        }
        return false;
    }

    function restorePreviousColumnsWidths(initialConfig) {
        var mainLayoutWidth;
        for (var m = 0; m < initialConfig.length; m++) {
            var count = initialConfig[m].Columns.length;
            if (minColumnWidth >= mainWidth / count) {
                var newWidth = recalculateMainWidth(count);
                updateMainWidth(newWidth);
                mainLayoutWidth = mainWidthWhithoutborders - (count - 1) * splitterWidth;
                updateMaxLayoutWidth(mainLayoutWidth);
            }
        }
    }

    function updateDeleteSectionBtn() {
        // remove delete section button if there are only 1 section
        if (sectionsData.length > 1) {
            $('.delete-section-button').show();
        } else {
            $('.delete-section-button').hide();
        }
    }

    function prepareConfigArrayWithSelectedPercentage(columnConfigs, usePercentWidthInsteadOfWidth) {
        var additionalPixels = 0;
        var maxWidthInArray = 0;
        var posOfMaxWidthInArray = 0;
        var configArray = new Array();

        correctConfigPercentage(columnConfigs, usePercentWidthInsteadOfWidth,
            function (index, itemWidth, itemPercentWidth, mainLayoutWidth) {
                if (maxWidthInArray < itemWidth) {
                    maxWidthInArray = itemWidth;
                    posOfMaxWidthInArray = index;
                }
                // can be negative
                additionalPixels += (mainLayoutWidth * itemPercentWidth) - itemWidth;
                configArray.push({ width: itemWidth, RefId: Ext.isEmpty(columnConfigs[index]) ? null : columnConfigs[index].RefId });
            },
            function () {
                // add additional pixels to the item with max width
                configArray[posOfMaxWidthInArray].width += additionalPixels;
            });
        return configArray;
    }

    function removeResourceFromSection(section) {
        var configIds = new Array();
        for (var i = 0; i < section.Columns.length; i++) {
            for (var j = 0; j < section.Columns[i].Cells.length; j++) {
                configIds.push(section.Columns[i].Cells[j].ConfigId);
            }
        }
        if (!Ext.isEmpty(configIds))
            SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'RemoveConfigs', { configsSessionName: configsSessionName, configIds: configIds }, function() { });
    }

    function prepareConfigForSelectedLayout(sectionGuid, columnConfigs) {
        var columnsCount = columnConfigs.length;

        if (minColumnWidth >= mainWidth / columnsCount) {
            var newWidth = recalculateMainWidth(columnsCount);
            updateMainWidth(newWidth);
            for (var m = 0; m < sectionsData.length; m++) {
                if (sectionsData[m].RefId != sectionGuid)
                    increaseSectionWidth(sectionsData[m].RefId, sectionsData[m].Columns.length);
            }
        }

        var mainLayoutWidth = mainWidthWhithoutborders - (columnsCount - 1) * splitterWidth; // (count - 1) * splitterWidth place for splitter
        maxLayoutWidth = mainLayoutWidth;

        var configArray = prepareConfigArrayWithSelectedPercentage(columnConfigs, false);
        return configArray;
    }

    return {
        duplicateResource: function (column, cellId) {
            var layoutId = findParentLayoutId(column);
            var positionInfo = layoutId.replace('layout-resources-', '').split('__');
            var positionOfOriginalResource = 0;
            var sectionId = findSectionElementId($('#' + cellId)[0]);
            for (var j = 0; j < sectionsData.length; j++) {
                if (sectionsData[j].RefId == sectionId.replace('layout-section-', '')) {
                    var columnResources = null;
                    for (var m = 0; m < sectionsData[j].Columns.length; m++) {
                        if (sectionsData[j].Columns[m].RefId == positionInfo[1]) {
                            columnResources = sectionsData[j].Columns[m].Cells;
                        }
                    }
                    if (columnResources == null) return;
                    var resConfig = {};
                    for (var i = 0; i < columnResources.length; i++) {
                        if (columnResources[i].RefId == cellId.replace('selection-cell-', '')) {
                            var refId = SW.Core.Services.generateNewGuid();
                            var configId = SW.Core.Services.generateNewGuid();
                            resConfig = {
                                RefId: refId, ConfigId: configId, TimeframeRefId: columnResources[i].TimeframeRefId, DataSelectionRefId: columnResources[i].DataSelectionRefId,
                                RenderProvider: columnResources[i].RenderProvider, DisplayName: columnResources[i].DisplayName,
                                Config: columnResources[i].Config
                            };
                            // add new config into the dictionary;
                            resourceConfigDictionary[configId] = resourceConfigDictionary[columnResources[i].ConfigId];
                            positionOfOriginalResource = i;
                            // add new config item
                            SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'DuplicateConfig', { configsSessionName: configsSessionName, configId: columnResources[i].ConfigId, newConfigId: configId }, function () { });
                            break;
                        }
                    }
                    addResourcesToColumn(sectionId, positionInfo[1], [resConfig], positionOfOriginalResource, true);
                    break;
                }
            }
            syncSectionsData();
        },
        removeResourceFromColumn: function (column, cellId, drag) {
            var layoutId = findParentLayoutId(column);
            var positionInfo = layoutId.replace('layout-resources-', '').split('__');
            var sectionId = findSectionElementId($(column));
            var sectId = sectionId.replace('layout-section-', '');
            for (var j = 0; j < sectionsData.length; j++) {
                if (sectionsData[j].RefId == sectId) {
                    var columnConfig = null;
                    selectColumnElement(positionInfo[1], function (col) { columnConfig = col; }, sectId);
                    if (columnConfig == null) return;
                    var columnResources = columnConfig.Cells;
                    for (var i = 0; i < columnResources.length; i++) {
                        if (columnResources[i].RefId == cellId.replace('selection-cell-', '')) {
                            var configId = columnResources[i].ConfigId;
                            columnResources.splice(i, 1);
                            // remove redudant config data in case we're removing resource at all
                            if (!drag) {
                                SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'RemoveConfigs', { configsSessionName: configsSessionName, configIds: [configId] }, function () { });
                            }
                            break;
                        }
                    }
                    if (columnResources.length == 0) {
                        $('#' + layoutId)[0].innerHTML = noResourcesMessage;
                    }
                    $('#' + layoutId).find('#' + cellId + '').remove();
                    refreshLayoutHeigth(sectionId);
                    syncSectionsData();
                    break;
                }
            }
        },
        // add new layout template into layout builder at the last position
        addNewLayoutTemplate: function (syncSectionData, sectionGUID, columnGUID, columnConfigs) {
            var index = mainLayoutPanel.items.length - 1;
            var sectionGuid = sectionGUID ? sectionGUID : SW.Core.Services.generateNewGuid();

            if (Ext.isEmpty(columnConfigs))
                columnConfigs = [{ RefId: Ext.isEmpty(columnGUID) || columnGUID === "undefined" ? SW.Core.Services.generateNewGuid() : columnGUID, width: 1}];
            var columnsCount = columnConfigs.length;
            var layoutId = 'layout-section-' + sectionGuid;

            // add new row into layout config
            sectionsData.push({ BorderStyle: '', RefId: sectionGuid, Columns: [] });
            var layoutTemplate = getLayoutTemplate(sectionGuid);

            var configArray = prepareConfigForSelectedLayout(sectionGuid, columnConfigs);
            var configuredItems = getConfiguredColumns(sectionGuid, configArray, index);
            for (var i = 0; i < configuredItems.length; i++) {
                layoutTemplate.add(configuredItems[i]);
            }
            var numericTemplate = getNumericTemplate();
            //set numberfield value selected
            numericTemplate.setValue(columnsCount);

            // insert  new empty layout at the last position but before Add layout section button
            mainLayoutPanel.items.insert(index, new Ext.Panel({
                id: Ext.id(), layout: 'fit', autoHeight: true, autoWidth: true, border: false, cls: 'portletSection no-border layout-container',
                items: [
                        new Ext.Panel({
                            layout: 'column',
                            autoHeight: true,
                            autoWidth: true,
                            cls: 'no-border layout-item-header',
                            border: false,
                            items: [
                                    { html: '<table style="border-collapse: collapse;"><tr><td style="width: 15px;padding:0px !important;"><img src="/Orion/images/LayoutBuilder/ddpicksection.png"/></td><td style="padding-left: 0px !important;">' + '@{R=Core.Strings;K=WEBJS_SO0_58; E=js}' + '</td></tr></table>', border: false },
                                    numericTemplate,
                                { html: String.format('<div><img style="padding-left: 5px;vertical-align: bottom;cursor:pointer;position:relative;" id="decrColumnCount-{0}" src="/Orion/images/LayoutBuilder/dwnarrow.png" /><img style="padding-left: 5px;vertical-align: bottom;cursor:pointer;position:relative;" id="incrColumnCount-{0}" src="/Orion/images/LayoutBuilder/uparrow.png" /></div>', sectionGuid), border: false },
                                    { html: String.format('<img id="delete-section-{0}" src="/Orion/images/LayoutBuilder/section_delete.png" />', sectionGuid), border: false, cls: "delete-section-button", style: "display: none;" }
                                ]
                        }),
                        layoutTemplate
                    ]
            }));

            // when select items count was changed we need to rebuild columns count
            numericTemplate.on('change', function () {
                if (numericTemplate.isValid()) {
                    var count = this.getValue();
                    if ((minColumnWidth >= mainWidth / count) || (count != 1 && minColumnWidth > mainWidth * percentValueToSatisfyResizing)) {
                        increaseLayoutWidth(sectionGuid, count);
                        return;
                    }
                    // decrease mainwidth only if performed condition
                    else if ((maxLayoutWidth > originalWidth - 4 * 10 - 4 * 1) && (minColumnWidth <= mainWidth * percentValueToSatisfyResizing)) {
                        if (validateDecreaseLayoutWidth(sectionGuid, count))
                            return;
                    }
                    selectSectionElement(sectionGuid, function (section, index) {
                        // if no changes were made to appropriate section and we need to update all sections from outside 
                        if (section.Columns.length == count)
                            updateSectionLayout(sectionsData[index].RefId, sectionsData[index].Columns.length, sectionsData[index].Columns, true);
                        else
                            updateSectionLayout(sectionsData[index].RefId, count, [], false);
                    });
                    syncSectionsData();
                }
            });
            numericTemplate.un('blur');
            // call this to refreah control items
            mainLayoutPanel.doLayout();
            refreshLayoutHeigth(layoutId);

            // remove delete section button if there are only 1 section
            updateDeleteSectionBtn();

            $('#incrColumnCount-' + sectionGuid).click(function () {
                if (numericTemplate.isValid() && numericTemplate.getValue() != maxAvailableColumnCount) {
                    var newVal = numericTemplate.getValue() + 1;
                    numericTemplate.setValue(newVal);
                    numericTemplate.fireEvent('change');
                }
            });
            $('#decrColumnCount-' + sectionGuid).click(function () {
                if (numericTemplate.isValid() && numericTemplate.getValue() != 1) {
                    var newVal = numericTemplate.getValue() - 1;
                    numericTemplate.setValue(newVal);
                    numericTemplate.fireEvent('change');
                }
            });

            $('#delete-section-' + sectionGuid).click(function () {
                var sectionId = this.id.replace('delete-section-', '');
                SW.Orion.LayoutBuilder.removeSection(sectionId);
                return false;
            });

            // initialize drag&drop after adding new layout
            initDragAndDrop();
            if (syncSectionData)
                syncSectionsData();
        },
        ResourcePickerLoaderCreated: function (loader) {
            loader.setConfigSessionName($('[id*=configsSessionIdentifier]')[0].value);
            resourcePickerLoader = loader;
        },
        SaveSelectedResources: function (loader) {
            var selectedResource = loader.GetSelectedResource();
            var sectionId = selectedResource.selectedColumn[0];
            var columnId = selectedResource.selectedColumn[1];
            var requiresEditOnAdd = selectedResource.resourceConfigData.requiresEditOnAdd;
            resourceConfigDictionary[selectedResource.resource.ConfigId] = selectedResource.resourceConfigData;
            addResourcesToColumn(sectionId, columnId, [selectedResource.resource], 'undefined', true);

            syncSectionsData();

            if (requiresEditOnAdd) {
                var btnId = '#editTableBtnPlaceHolder_' + selectedResource.resource.RefId;
                $(btnId).click();
            }
        },
        removeSection: function (sectionId) {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_24; E=js}', msg: '@{R=Core.Strings;K=WEBJS_IB0_25; E=js}', minWidth: 300, buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' }, icon: Ext.MessageBox.ERROR,
                fn: function (btn) {
                    if (btn == 'yes') {
                        selectSectionElement(sectionId, function (section, index) {
                            sectionsData.splice(index, 1);
                            var sectionElement = mainLayoutPanel.find('id', 'layout-section-' + sectionId);
                            if (sectionElement != null && sectionElement != undefined && sectionElement.length > 0) {
                                mainLayoutPanel.remove(sectionElement[0].ownerCt);
                                mainLayoutPanel.doLayout();

                                // remove delete section button if there are only 1 section
                                updateDeleteSectionBtn();
                            }
                            removeResourceFromSection(section);
                            syncSectionsData();
                            // fire 'change' event for 
                            Ext.getCmp(mainLayoutPanel.id).find('id', $('[id^=spinner]')[0].id)[0].fireEvent('change'); // [0] because find returns array
                        });
                    }
                }
            });
        },
        wizardRedirectFunction: function (url) {
            $('#RedirectToUrlHiddenField').val(url);
            __doPostBack();
        },
        setInitialConfig: function (config) {
            self.config = config;
        },
        init: function () {
            //initialize sections data
            sectionsData = [];

            initializeLayoutTemplates();
            sectionsSessionName = $('[id*=sectionsSessionIdentifier]')[0].value;
            configsSessionName = $('[id*=configsSessionIdentifier]')[0].value;

            returnUrl = $('#ReturnToUrl').val();
            noResourcesMessage = '<div class="layout-empty-message"><div><img style="vertical-align: bottom;" src="/Orion/images/LayoutBuilder/u109_normal.png"/>' + $('#emptyMessageText').val() + '<img style="vertical-align: bottom;" src="/Orion/images/LayoutBuilder/u107_normal.png"/></div></div>';
            mainLayoutPanel = new Ext.Panel({
                id: Ext.id(), layout: 'form', autoHeight: true, border: false, cls: 'no-border',
                items: [
                        new Ext.Panel({
                            id: Ext.id(), region: 'center', autoHeight: true, autoWidth: true, cls: 'no-border layout-add-new', border: false,
                            items: [{ html: '<div class="layout-add-link"><div>' + SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_SO0_71; E=js}', { type: 'secondary', id: 'addLink', href: 'javascript:SW.Orion.LayoutBuilder.addNewLayoutTemplate(true);', cls: 'sw-btn-small' }) + '</div></div>', border: false
                            }]
                        })]
            });
            mainLayoutPanel.render("layoutContainer");

            SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'GetLayoutInfo', { sectionsSessionName: sectionsSessionName, configsSessionName: configsSessionName }, function (result) {
                if (result != null && result.Sections != null) {
                    for (var j = 0; j < result.ResourceOptions.length; j++) {
                        resourceConfigDictionary[result.ResourceOptions[j].ConfigId] = { supportDateTimeFilter: result.ResourceOptions[j].SupportDtFilter, requiresEditOnAdd: result.ResourceOptions[j].RequiresEditOnAdd, supportedInterfaces: result.ResourceOptions[j].SupportedInterfaces, iconPath: result.ResourceOptions[j].IconPath, editButtonText: result.ResourceOptions[j].EditButtonText, path: result.ResourceOptions[j].Path, title: result.ResourceOptions[j].Title };
                    }
                    // set up initial sections config
                    var initialConfig = result.Sections;
                    //restore previous columns widths
                    restorePreviousColumnsWidths(initialConfig);

                    var layoutConfigInitial = [];
                    for (var i = 0; i < initialConfig.length; i++) {
                        layoutConfigInitial.push({ RefId: initialConfig[i].RefId, columns: [] });
                        for (j = 0; j < initialConfig[i].Columns.length; j++) {
                            layoutConfigInitial[i].columns.push({ RefId: initialConfig[i].Columns[j].RefId, width: initialConfig[i].Columns[j].PercentWidth / 100 });
                        }
                    }
                    sectionsData = initialConfig;
                    updateLayoutTemplate(layoutConfigInitial, '/Orion/images/LayoutBuilder/l01.png', false);
                    for (var k = 0; k < initialConfig.length; k++) {
                        for (var l = 0; l < initialConfig[k].Columns.length; l++) {
                            if (initialConfig[k].Columns[l].Cells.length > 0) {
                                addResourcesToColumn(initialConfig[k].RefId, initialConfig[k].Columns[l].RefId, initialConfig[k].Columns[l].Cells, 'undefined', true);
                            }
                        }
                    }
                    if (SW.Orion.LayoutBuilder.reportErrors)
                        SW.Orion.LayoutBuilder.reportErrors();
                }
                else {
                    // add empty layout at the begining
                    SW.Orion.LayoutBuilder.addNewLayoutTemplate(true);

                    // If What is new dialog wasn't explicitly hidden by user we will show it on page.
                    if (self.config.WhatIsNewReportDialog && !self.config.WhatIsNewReportDialog.WasDialogHidden()) {
                        self.config.WhatIsNewReportDialog.ShowDialog(function () {
                            // call add resource!
                            resourcePickerLoader.AddContent([sectionsData[0].RefId, sectionsData[0].Columns[0].RefId]);
                        });
                    }
                    else {
                        // call add resource!
                        resourcePickerLoader.AddContent([sectionsData[0].RefId, sectionsData[0].Columns[0].RefId]);
                    }
                }
                $(window).resize(function () {
                    updateMainWidth();
                });

            },
                function (errorMsg) {
                    Ext.Msg.show({ title: '@{R=Core.Strings;K=WEBJS_IB0_32;E=js}', msg: errorMsg, minWidth: 200, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                }
            );
            $('#RedirectToUrlHiddenField').val('');
        }
    };
} ();

// addLinkToEditPage: if true then {0} in errorMessage is replaced by link to the resource edit page
SW.Orion.LayoutBuilder.showErrorInSectionCell = function (sectionCellRefId, errorMessage, addLinkToEditPage) {
    if (addLinkToEditPage) {
        var linkText = $(String.format('#editTableBtnPlaceHolder_{0} a', sectionCellRefId)).text();
        var editLink = $(String.format('<a href="javascript:$(\'#editTableBtnPlaceHolder_{0}\').click();" class="sw-link" style="cursor: pointer;">{1}</a>', sectionCellRefId, linkText));
        errorMessage = String.format(errorMessage, editLink[0].outerHTML);
    }
    
    var template = $('#sectionCellErrorMessageTemplate').html();
    var newElement = _.template(template, { errorMessage: errorMessage });
    
    $(String.format('#selection-cell-{0} table', sectionCellRefId)).first().after(newElement);
};

// it can be redefined to report errors in layout builder
SW.Orion.LayoutBuilder.reportErrors = function() {
};

Ext.onReady(SW.Orion.LayoutBuilder.init, SW.Orion.LayoutBuilder);
