Ext.namespace('SW');
Ext.namespace('SW.AgentManagement');

function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
}

SW.AgentManagement.AgentsGrid = function () {
    var manageAgentsMode = 'ManageAgents';
    var manageRemoteCollectorsMode = 'ManageRemoteCollectors';
    var maxRemoteCollectorPollingElements = 1000.0;

    var agentGridMode = manageAgentsMode;
    var isManageAgentsPage = true;
    var isManageRemoteCollectorsPage = false;

    var deleteTitleSingle = '@{R=AgentManagement.Strings;K=DeleteAgentTitle;E=js}';//"Delete Agent";
    var deleteTitleMultiple = '@{R=AgentManagement.Strings;K=DeleteAgentsTitle;E=js}';//"Delete Agents";
    var deletePromptSingle = '@{R=AgentManagement.Strings;K=DeleteAgentPrompt;E=js}';//"Do you really want to delete selected agent?";
    var deletePromptMultiple = '@{R=AgentManagement.Strings;K=DeleteAgentsPrompt;E=js}';//"Do you really want to delete selected agents?";
    var modeActive = '@{R=AgentManagement.Strings;K=AgentInitiatedCommunicationLabel;E=js}';//"Agent initiated communication";
    var modePassive = '@{R=AgentManagement.Strings;K=ServerInitiatedCommunicationLabel;E=js}';//"Server initiated communication"
    var modeAuto = '@{R=AgentManagement.Strings;K=AutodetectCommunicationLabel;E=js}';//"Automatic detection"
    var perPageLabel = '@{R=AgentManagement.Strings;K=ItemsPerPageLabel;E=js}';//"Per Page: ";
    var errorTitle = '@{R=AgentManagement.Strings;K=ErrorDialogTitle;E=js}';//"Error";
    var invalidPageSizeText = '@{R=AgentManagement.Strings;K=InvalidPageSize;E=js}';//"Invalid page size";
    var waitingAgentsSingularMessageFormat = '@{R=AgentManagement.Strings;K=WaitingAgentMessageFormat;E=js}';
    var waitingAgentsPluralMessageFormat = '@{R=AgentManagement.Strings;K=WaitingAgentsMessageFormat;E=js}';
    var waitingAgentsSingularNotificationFormat =
        '<a class="sw-link AgentManagement_EnabledLink" href="javascript:void(0);" onclick="SW.AgentManagement.AgentsGrid.ShowNotificationRow()">@{R=AgentManagement.Strings;K=OneAgentWaitingForAdd;E=js}</a>';
    var waitingAgentsPluralNotificationFormat =
        '<a class="sw-link AgentManagement_EnabledLink" href="javascript:void(0);" onclick="SW.AgentManagement.AgentsGrid.ShowNotificationRow()">@{R=AgentManagement.Strings;K=MultipleAgentsWaitingForAdd;E=js}</a>';

    var forceUpdateWaitMsg = '@{R=AgentManagement.Strings;K=ForcingAgentsUpdateWaitMessage;E=js}';//'Forcing update of Agents ...';
    var forceUpdatePromptTitle = '@{R=AgentManagement.Strings;K=ForceAutomaticUpdatePromptTitle;E=js}';//'Force automatic update';
    var forceUpdateButtonText = '@{R=AgentManagement.Strings;K=ForceAgentUpdateButtonLabel;E=js}';//'Force Agent Update';
    var forceUpdateButtonTooltip = '@{R=AgentManagement.Strings;K=ForceAgentUpdateButtonTooltip;E=js}';//'Forces the Agent to update.';
    var forceUpdateMsgSingular = '@{R=AgentManagement.Strings;K=ForceAgentUpdateQuestionSingular;E=js}';//'Do you really want to force an update of the selected Agent?';
    var forceUpdateMsgPlural = '@{R=AgentManagement.Strings;K=ForceAgentUpdateQuestionPlural;E=js}';//'Do you really want to force an update of the selected Agents?';

    var forceRedeployWaitDialogText = '@{R=AgentManagement.Strings;K=ForceAgentDeployWaitMessage;E=js}';//'Forcing deployment of Agents ...';
    var forceRedeployWindowTitle = '@{R=AgentManagement.Strings;K=ForceAgentDeployPromptTitle;E=js}';//'Force Agent Deployment - Assign Credential';

    var LoadingDataLabel = '@{R=AgentManagement.Strings;K=LoadingDataWaitMessage;E=js}';//'Loading data...';

    var addAgentButtonText = '@{R=AgentManagement.Strings;K=AddAgentButtonText;E=js}';//'Add Agent'
    var editAgentButtonText = '@{R=AgentManagement.Strings;K=EditAgentButtonText;E=js}';//'Edit Setting'
    var deleteAgentButtonText = '@{R=AgentManagement.Strings;K=DeleteAgentButtonText;E=js}';//'Delete'
    var chooseResourcesButtonText = '@{R=AgentManagement.Strings;K=ChooseResourcesButtonText;E=js}'; // 'Choose resources'
    var moreActionsText = '@{R=AgentManagement.Strings;K=MoreActionsButtonText;E=js}';
    var viewInstalledAgentPluginsButtonText = '@{R=AgentManagement.Strings;K=ViewInstalledAgentPluginsText;E=js}';
    var manageAsNodeButtonText = '@{R=AgentManagement.Strings;K=ManageAsNodeButtonText;E=js}';
    var retryAgentInstallationButtonText = '@{R=AgentManagement.Strings;K=AgentsGrid_RetryAgentInstallationButtonText;E=js}';

    var rebootButtonText = '@{R=AgentManagement.Strings;K=RebootAgentButtonText;E=js}';//'Reboot'
    var rebootButtonTooltip = '@{R=AgentManagement.Strings;K=RebootAgentButtonTooltip;E=js}';//'Reboot selected agents'
    var rebootPromptTitle = '@{R=AgentManagement.Strings;K=RebootAgentPromptTitle;E=js}';//'Reboot selected agents?'
    var rebootMsgPlural = '@{R=AgentManagement.Strings;K=RebootAgentsQuestionPlural;E=js}';//'Do you really want to reboot machines with selected agents?'
    var rebootMsgSingular = '@{R=AgentManagement.Strings;K=RebootAgentQuestionSingular;E=js}';//'Do you really want to reboot machine with selected agent?'
    var sendingRebootMessageWaitMsg = '@{R=AgentManagement.Strings;K=SendingRebootCommandWaitMsg;E=js}';//'Sending reboot command to selected agents.'

    var restartButtonText = '@{R=AgentManagement.Strings;K=RestartAgentButtonText;E=js}';//'Restart'
    var restartButtonTooltip = '@{R=AgentManagement.Strings;K=RestartAgentButtonTooltip;E=js}';//'Restart Orion Agent Service'
    var restartPromptTitle = '@{R=AgentManagement.Strings;K=RestartAgentPromptTitle;E=js}';//'Restart Orion Agent Service on selected agents?'
    var restartMsgPlural = '@{R=AgentManagement.Strings;K=RestartAgentsQuestionPlural;E=js}';//'Do you really want to restart Orion Agent Service on selected agents?'
    var restartMsgSingular = '@{R=AgentManagement.Strings;K=RestartAgentQuestionSingular;E=js}';//'Do you really want to restart Orion Agent Service on selected agent?'
    var sendingRestartMessageWaitMsg = '@{R=AgentManagement.Strings;K=SendingRestartCommandWaitMsg;E=js}';//'Sending restart command to selected agents.'

    var promoteButtonText = '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentButtonText;E=js}'; // 'Promote Agent'; 
    var promoteErrorDialogTitle = '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorDialogTitle;E=js}';
    var promoteAgentErrorDialogMessages = {
        1: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentNotFound;E=js}',
        2: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentHostNameIsEmpty;E=js}',
        3: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentInstalledOnOrionServer;E=js}',
        4: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_CreateOrUpdateEngineOperationFailed;E=js}',
        5: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentInstalledOnUnsupportedOperatingSystem;E=js}',
        6: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentInstalledOnUnsupportedOperatingSystemVersion;E=js}',
        7: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentHostnameAlreadyUsedAsEngineServerName;E=js}',
        8: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentInstalledOnSystemWithInsufficientDotNetFramework;E=js}',
        9: '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentErrorMessage_AgentInPassiveMode;E=js}'

    };

    var promoteAgentDialogTitle = '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentDialogTitle;E=js}';
    var promoteAgentDialogOfflineAgentsWarning = '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentDialogOfflineAgentsWarning;E=js}';
    var promoteAgentDialogYesButton = '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentDialogYesButton;E=js}';
    var promoteAgentDialogNoButton = '@{R=AgentManagement.Strings;K=AgentsGrid_PromoteAgentDialogNoButton;E=js}';
    var promoteAgentToOrionRemoteCollectorIcon = '/Orion/AgentManagement/Images/Icon.PromoteToOrionRemoteCollector.png';
    
    var updatingAgentsWaitMsg = '@{R=AgentManagement.Strings;K=SendingUpdateCommandToAgentsWaitMsg;E=js}';//'Sending update command to selected agents.'
    var updateButtonText = '@{R=AgentManagement.Strings;K=UpdateAgentButtonText;E=js}';//'Update'
    var updateButtonTooltip = '@{R=AgentManagement.Strings;K=UpdateAgentButtonTooltip;E=js}';//'Update selected agents'
    var updatePromptTitle = '@{R=AgentManagement.Strings;K=UpdateAgentPromptTitle;E=js}';//'Update selected agents?'
    var updateMsgPlural = '@{R=AgentManagement.Strings;K=UpdateAgentsQuestionPlural;E=js}';//'Do you really want to update selected agents?'
    var updateMsgSingular = '@{R=AgentManagement.Strings;K=UpdateAgentQuestionSingular;E=js}';//'Do you really want to update selected agent?'
    var rebootOrionEnginePromptTitle = '@{R=AgentManagement.Strings;K=RebootOrionEnginePromptTitle;E=js}';
    var rebootOrionEnginePromptFormat = '@{R=AgentManagement.Strings;K=RebootOrionEnginePromptFormat;E=js}';

    var installedAgentPluginsDialogNodeText = '@{R=AgentManagement.Strings;K=InstalledAgentPluginsDialog_Node;E=js}';
    var installedAgentPluginsDialogAgentStatusText = '@{R=AgentManagement.Strings;K=InstalledAgentPluginsDialog_AgentStatus;E=js}';
    var installedAgentPluginsDialogConnectionStatusText = '@{R=AgentManagement.Strings;K=InstalledAgentPluginsDialog_ConnectionStatus;E=js}';
    var installedAgentPluginsDialogTitle = '@{R=AgentManagement.Strings;K=InstalledAgentPluginsWindow_Title;E=js}';
    var deletingAgentsText = '@{R=AgentManagement.Strings;K=AgentsGrid_DeletingAgentsText;E=js}';
    var deletingAndUninstallingAgentsSingularText = '@{R=AgentManagement.Strings;K=AgentsGrid_DeletingAgentsMessageSingular;E=js}';
    var deletingAndUninstallingAgentsPluralText = '@{R=AgentManagement.Strings;K=AgentsGrid_DeletingAgentsMessagePlural;E=js}';
    var fieldMustNotBeEmptyText = '@{R=AgentManagement.Strings;K=AgentsGrid_FieldMustNotBeEmptyText;E=js}';
    var submitButtonText = '@{R=AgentManagement.Strings;K=AgentsGrid_SubmitButtonText;E=js}';// "Submit";
    var cancelButtonText = '@{R=AgentManagement.Strings;K=AgentGrid_AddEdit_CancelLabelText;E=js}';// "Cancel";
    var noGroupingLabelText = '@{R=AgentManagement.Strings;K=AgentsGrid_NoGroupingLabelText;E=js}';// '[No grouping]';
    var communicationTypeLabelText = '@{R=AgentManagement.Strings;K=AgentsGrid_CommunicationTypeLabelText;E=js}';// 'Communication Type';
    var agentStatusLabelText = '@{R=AgentManagement.Strings;K=AgentsGrid_AgentStatusLabelText;E=js}';//'Agent Status';
    var connectionStatusLabelText = '@{R=AgentManagement.Strings;K=AgentsGrid_ConnectionStatusLabelText;E=js}'; //'Connection Status'; 
    var orionPollerLabelText = '@{R=AgentManagement.Strings;K=AgentsGrid_OrionPollerLabelText;E=js}'; // 'Orion Poller'
    var isRemoteCollectorLabelText = '@{R=AgentManagement.Strings;K=AgentsGrid_IsRemoteCollectorLabelText;E=js}'; // 'Type';
    var displayingAgentsText = '@{R=AgentManagement.Strings;K=AgentsGrid_DisplayingAgentsText;E=js}';  //'Displaying agents {0} - {1} of {2}';
    var displayingRemoteCollectorsText = '@{R=AgentManagement.Strings;K=AgentsGrid_DisplayingRemoteCollectorsText;E=js}';  //'Displaying Remote Collectors {0} - {1} of {2}';
    var noAgentsToDisplayText = '@{R=AgentManagement.Strings;K=AgentsGrid_NoAgentsToDisplayText;E=js}'; //"No agents to display";
    var noRemoteCollectorsToDisplayText = '@{R=AgentManagement.Strings;K=AgentsGrid_NoRemoteCollectorsToDisplayText;E=js}'; //"No agents to display";
    var thereAreNoAgentsAvailableText = '@{R=AgentManagement.Strings;K=AgentsGrid_ThereAreNoAgentsAvailableText;E=js}';
    var thereAreNoRemoteCollectorsAvailableText = '@{R=AgentManagement.Strings;K=AgentsGrid_ThereAreNoRemoteCollectorsAvailableText;E=js}';
    var searchAllAgentsDefaultText = '@{R=AgentManagement.Strings;K=AgentsGrid_SearchAllAgentsDefaultText;E=js}';// 'Search all agents ...'
    var searchAllRemoteCollectorsDefaultText = '@{R=AgentManagement.Strings;K=AgentsGrid_SearchAllRemoteCollectorsDefaultText;E=js}';// 'Search all Remote Collectors ...'
    var beforePageTextGridPaging = '@{R=AgentManagement.Strings;K=BeforePageTextGridPaging;E=js}'; //'Page';
    var afterPageTextGridPaging = '@{R=AgentManagement.Strings;K=AfterPageTextGridPaging;E=js}'; //'of {0}';

    var deleteAgentWindowTitleText = '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteAgentWindowTitleText;E=js}';
    var deleteAgentsWindowTitleText = '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteAgentsWindowTitleText;E=js}';
    var deleteAgentWindowOfflineAgentsWarningPluralFormat = '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteAgent_OfflineAgentsWarningPluralFormat;E=js}';
    var deleteAgentWindowOfflineAgentsWarningSingularFormat = '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteAgent_OfflineAgentsWarningSingularFormat;E=js}';
    var deleteAgentWindowYesButtonText = '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteAgent_YesButtonText;E=js}';
    var deleteAgentWindowNoButtonText = '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteAgent_NoButtonText;E=js}';

    var reconnectAgentButtonText = '@{R=AgentManagement.Strings;K=ReconnectAgentButtonLabel;E=js}';//'Reconnect to Agent';
    var reconnectAgentButtonTooltip = '@{R=AgentManagement.Strings;K=ReconnectAgentButtonTooltip;E=js}';//'FReconnect to Agent';
    var reconnectAgentIcon = '/Orion/AgentManagement/Images/Icon.ReconnectAgent.png';

    var loadingText = '@{R=AgentManagement.Strings;K=LoadingText;E=js}';

    var retryAgentInstallationIcon = '/Orion/AgentManagement/Images/Icon.RetryAgentInstallation.png';
    var rebootIcon = '/Orion/AgentManagement/Images/Icon.Reboot.png';
    var restartIcon = '/Orion/AgentManagement/Images/Icon.Restart.png';
    var updateIcon = '/Orion/AgentManagement/Images/Icon.Update.png';
    var viewInstalledAgentPluginsIcon = '/Orion/AgentManagement/Images/Icon.ViewInstalledAgentPlugins.png';
    var viewInstalledModulesReportIcon = '/Orion/AgentManagement/Images/Icon.ViewModulesReport.png';
    var viewInstalledModulesReportButtonText = '@{R=AgentManagement.Strings;K=AgentsGrid_ViewInstalledModulesReportButtonText;E=js}';
    var newAgentLabelText = '@{R=AgentManagement.Strings;K=AgentsGrid_NewAgentLabel;E=js}';

    var pollingCapacityIsUnknown = '@{R=AgentManagement.Strings;K=AgentsGrid_PollingCapacityIsUnknown;E=js}';

    var idGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_IdGridColumnHeader;E=js}';//'ID';
    var agentNodeGridColumnHeader = escapeHtml('@{R=AgentManagement.Strings;K=AgentsGrid_AgentNodeGridColumnHeader;E=js}');//'Agent/Node';
    var remoteCollectorGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_RemoteCollectorGridColumnHeader;E=js}';//'RemoteCollector';
    var ipAddressOrHostnameGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_IpAddressOrHostnameGridColumnHeader;E=js}';//'IP Address or Hostname';
    var ipAddressGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_IpAddressGridColumnHeader;E=js}';//'IP Address';
    var hostnameGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_HostnameGridColumnHeader;E=js}';//'Hostname';
    var dnsNameGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_DnsNameGridColumnHeader;E=js}';//'DNS Name';
    var agentStatusGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_AgentStatusGridColumnHeader;E=js}';//'Agent Status';
    var connectionStatusGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_ConnectionStatusGridColumnHeader;E=js}';//'Connection Status';
    var isRemoteCollectorGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_IsRemoteCollectorGridColumnHeader;E=js}';//'Type';
    var registeredOnGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_RegisteredOnGridColumnHeader;E=js}';//'Registered On';
    var pollingEngineGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_PollingEngineGridColumnHeader;E=js}';//'Polling Engine';
    var masterEngineGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_MasterEngineGridColumnHeader;E=js}';//'Master engine';
    var modeGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_ModeGridColumnHeader;E=js}';//'Mode';
    var agentModeGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_AgentModeGridColumnHeader;E=js}';//'Agent Mode';
    var versionGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_VersionGridColumnHeader;E=js}';//'Version';
    var agentSoftwareVersionGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_AgentSoftwareVersionGridColumnHeader;E=js}';//'Agent Software Version';
    var guidGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_GuidGridColumnHeader;E=js}';//'Guid';
    var operatingSystemColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_OperatingSystemGridColumnHeader;E=js}';//'Operating System';
    var pollingCompletionColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_PollingCompletionGridColumnHeader;E=js}';//'Polling Completion';
    var pollingCapacityColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_PollingCapacityGridColumnHeader;E=js}';//'Polling Capacity';
    var elementCountColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_ElementCountGridColumnHeader;E=js}';//'Elements';
    var nodeCountColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_NodeCountGridColumnHeader;E=js}';//'Nodes';
    var interfaceCountColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_InterfaceCountGridColumnHeader;E=js}';//'Interfaces';
    var volumeCountColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_VolumeCountGridColumnHeader;E=js}';//'Volumes';
    var osVersionGridColumnHeader = '@{R=AgentManagement.Strings;K=AgentsGrid_OperatingSystemVersionGridColumnHeader;E=js}';//'Operating System Version';

    var columnsSelectionText = '@{R=AgentManagement.Strings;K=ExtjsGrid_ColumnsSelectionText;E=js}';//'Columns';
    var sortAscendingText = '@{R=AgentManagement.Strings;K=ExtjsGrid_SortAscendingText;E=js}';//'Sort Ascending';
    var sortDescendingText = '@{R=AgentManagement.Strings;K=ExtjsGrid_SortDescendingText;E=js}';//'Sort Descending';

    var pluginErrorDetailsLinkText = '@{R=AgentManagement.Strings;K=AgentsGrid_PluginErrorDetailsLinkText;E=js}';//'View plug-ins';
    var redeployPluginLinkText = '@{R=AgentManagement.Strings;K=AgentsGrid_RedeployPluginLinkText;E=js}';//'Reinstall plug-in';
    var pluginIsBeingReinstalledText = '@{R=AgentManagement.Strings;K=AgentsGrid_PluginIsBeingReinstalledText;E=js}';//'Plug-in is being reinstalled. It may take up to few minutes.';
    var tryAgainLinkText = '@{R=AgentManagement.Strings;K=AgentsGrid_TryAgainLinkText;E=js}';//'Try again';
    var moreLinkText = '@{R=AgentManagement.Strings;K=AgentsGrid_MoreLinkText;E=js}';//'More';

    var pastePrivateKeyManuallyTitle = '@{R=AgentManagement.Strings;K=DeploymentWizard_PastePrivateKeyManuallyTitle;E=js}';
    var pastePrivateKeyManuallyLabel = '@{R=AgentManagement.Strings;K=DeploymentWizard_PastePrivateKeyManuallyLabel;E=js}';
    var missingPrivateKeyError = '@{R=AgentManagement.Strings;K=DeploymentWizard_MissingPrivateKeyError;E=js}';

    var privateKeyCredentialType = "privateKey";
    var usernameAndPasswordCredentialType = "usernameAndPassword";
    var newCredentialId = -1;

    var windowsOsType = "windows";
    var linuxOsType = "linux";

    var ConnectionStatusUnknown = 0;
    var ConnectionStatusOk = 1;
    var ConnectionStatusServiceNotResponding = 2;
    var ConnectionStatusDeploymentPending = 3;
    var ConnectionStatusDeploymentInProgress = 4;
    var ConnectionStatusDeploymentFailed = 5;
    var ConnectionStatusInvalidResponse = 6;
    var ConnectionStatusWaitingForConnection = 7;
    var ConnectionStatusConnecting = 8;

    var AgentStatusUnknown = 0;
    var AgentStatusOk = 1;
    var AgentStatusUpdateAvailable = 2;
    var AgentStatusUpdateInProgress = 3;
    var AgentStatusUpdateFailed = 4;
    var AgentStatusRebootRequired = 5;
    var AgentStatusRebootInProgress = 6;
    var AgentStatusRebootFailed = 7;
    var AgentStatusPluginUpdatePending = 8;
    var AgentStatusUninstallInProgress = 9;
    var AgentStatusAgentUninstalled = 10;
    var AgentStatusPluginErrorOccurred = 11;
    var AgentStatusAgentRestartInProgress = 12;
    var AgentStatusAgentRestartFailed = 13;


    var PluginStatusInstalationPending = 3;
    var PluginStatusInstallationInProgress = 6;
    var PluginStatusReinstalationPending = 13;
    var inProgressPluginStatuses = [PluginStatusInstalationPending, PluginStatusInstallationInProgress, PluginStatusReinstalationPending];
    var failedPluginStatuses = [];
    
    var groupBy;
    var groupByItem;
    var groupByCombo;
    var groupByStore;
    var groupByList;
    var groupingPanel;

    var reportUrl;
    var isOrcFeatureEnabled;
    var howLongIsAgentDisplayedAsNewInMiliseconds;

    var selectorModel;
    var dataStore;
    var initialPage = 1;
    var pagingToolbar;
    var grid;
    var searchField;
    var initialized;
    var excludedAgents = new Array();
    var unsupportedAgents = new Array();
    var pageSizeBox;
    var forceUpdateAllowed = false;
    var forceRedeployAllowed = false;

    var selectedAgentsFieldClientID;

    // active agents service lock
    var activeAgentsCallInProgress = false;

    // force deploy stuff
    var forceDeployWindow;
    
    var credentialsCombo;
    var credentialsList;
    var sshPrivateKeyCredentialsList;
    var selectedCredential = -1;
    var selectedSshPrivateKeyCredential = -1;

    var usernameField;
    var passwordField;
    var privateKeyPasswordField;
    var credentialNameField;

    var certificatePastedManually = false;
    var certificateData;

    var additionalCredentialsCombo;
    var additionalCredentialNameField;
    var additionalUsernameField;
    var additionalPasswordField;
    var selectedAdditionalCredential = -1;
    var useSudoCheckbox;

    var installedAgentPluginsWindow;
    var deleteAndUninstallAgentWindow;
    var promoteAgentWindow;

    var pageSize = 1;
    var userName = "";
    var defaultCookiePrefix = 'AgentManagement_' + agentGridMode + '_AgentsGrid_';
    var cookiePrefix = defaultCookiePrefix;

    var isEditGrid = true;

    var loadingMask = null;

    var returnUrl = null;

    var loadWarningThreshold = 80;
    var loadErrorThreshold = 100;
    var NotificationRecordId = -1;

    var task;
    var runner;

    deleteAndUninstallAgentWindow = {
        defaultHtml: '',
        show: function () {
            if (this.defaultHtml == '') {
                this.defaultHtml = $('#AgentManagement_DeleteAndUninstallDialog').html();
            }

            $('#AgentManagement_DeleteAndUninstallDialog').html(this.defaultHtml);
            var selectedAgentsCount = grid.getSelectionModel().getCount();
            var dialogContent = $('.AgentManagement_Text');
            var multiSelect = selectedAgentsCount > 1;
            var hasAnyAssociatedAgentNode = false;
            var items = grid.getSelectionModel().getSelections(); 
            var agentIds = [];
            var offlineAgentIds = [];
            Ext.each(items, function (item) {                
                if (!hasAnyAssociatedAgentNode && isAgentNode(item)) {
                    hasAnyAssociatedAgentNode = true;
                }
                if(!isAgentOffline(item)) {
                    agentIds.push(item.data.ID);
                }
                else {
                    offlineAgentIds.push(item.data.ID);
                }
            });
            
            // select template based on selection
            var contentSelector = '.AgentManagement_DeleteAndUninstallDialogBody ';
            if (!multiSelect) {
                // only one agent selected
                var item = items[0];
                if (hasAnyAssociatedAgentNode) {
                    contentSelector += '.AgentManagement_OneSelectWithNodes';
                    $(contentSelector).show();
                    // render node with status icon from grid
                    $('.AgentManagement_Table tr td:last-child').html(formatNameValue("", item));
                } else {
                    contentSelector += '.AgentManagement_OneSelectWithoutNodes';
                    $(contentSelector).show();
                }
                // bind 'Installed Plugins dialog' on click on link
                $('.AgentManagement_InstalledPluginsButton').click(function() {
                    installedAgentPluginsWindow.show();
                });
            } else {
                // more agents selected
                if (hasAnyAssociatedAgentNode) {
                    contentSelector += '.AgentManagement_MultiSelectWithNodes';
                    $(contentSelector).show();
                } else {
                    contentSelector += '.AgentManagement_MultiSelectWithoutNodes';
                    $(contentSelector).show();
                    $('.numberOfAgents').html(selectedAgentsCount);
                }
            }

            // show warning when some of selected agents are offline
            if (offlineAgentIds.length > 0) {               
                var text;
                if (offlineAgentIds.length == 1) {
                    text = String.format(deleteAgentWindowOfflineAgentsWarningSingularFormat, offlineAgentIds.length);
                } else {
                    text = String.format(deleteAgentWindowOfflineAgentsWarningPluralFormat, offlineAgentIds.length);
                }

                $(contentSelector + " .AgentManagement_DialogOfflineAgentsWarning span:last-child").html(text);
                $(contentSelector + " .AgentManagement_DialogOfflineAgentsWarning").show();
            } else {
                $(contentSelector + " .AgentManagement_DialogOfflineAgentsWarning").hide();
            }

            // bind edit properties link, when any agent has node
            if (hasAnyAssociatedAgentNode) {
                $('.AgentManagement_EditNodeButton').click(function () {
                    return goToEditNodesPage();
                });
            }

            var dialogOptions = {
                width: 420,
                height: 'auto',
                modal: true,
                overlay: { "background-color": "black", opacity: 0.4 },
                title: multiSelect ? deleteAgentsWindowTitleText : deleteAgentWindowTitleText,
                dialogClass: 'AgentManagement_DeleteAndUninstallDialog',
                close: function () {
                    $('#AgentManagement_DeleteAndUninstallDialog').html('');
                },
                buttons: [
                    {
                        text: deleteAgentWindowYesButtonText,
                        click: function () {
                            DeleteAndUninstallSelectedItems();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: deleteAgentWindowNoButtonText,
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ],
                resizable: false
            };

            // show dialog
            var dialog = $('#AgentManagement_DeleteAndUninstallDialog')
                .dialog(dialogOptions).css('overflow', 'hidden');
        }
    };
    installedAgentPluginsWindow = {
        defaultHtml: '',
        helpUrl:'',
        show: function () {
            if (this.defaultHtml == '') {
                this.defaultHtml = $('#AgentManagement_InstalledAgentPluginsDialog').html();
            }
            if (this.helpUrl == '') {
                this.helpUrl = $('#AgentManagement_InstalledAgentPluginsDialog').data('help-url');
            }

            var item = grid.getSelectionModel().getSelected();
            var agentId = item.data.ID;
            $('#AgentManagement_InstalledAgentPluginsDialog').html(this.defaultHtml);
            var tableBody = $('#AgentManagement_InstalledAgentPluginsDialog .AgentManagement_PluginsTable table tbody');
            var statusesBody = $('#AgentManagement_InstalledAgentPluginsDialog .AgentManagement_Statuses table tbody');

            var dialogOptions = {
                width: 600,
                height: 'auto',
                dialogClass: 'AgentManagement_InstalledAgentPluginsDialog',
                modal: true,
                overlay: { "background-color": "black", opacity: 0.4 },
                title: installedAgentPluginsDialogTitle,
                close: function() {
                    $('#AgentManagement_InstalledAgentPluginsDialog').html('');
                },
                resizable: false
            };
            var createTable = function (rows) {
                // clean table           
                tableBody.html('');

                // append rows to table
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    var tr = $('<tr/>');

                    if (i % 2 == 1)
                        tr.addClass('AgentManagement_GreyBg');

                    // first column
                    $('<td/>').html(row.Name != null && row.Name != "" ? row.Name : row.Id).appendTo(tr);

                    // second column
                    var td = $('<td/>');

                    td.append(CreatePluginStatusCellContent(agentId, row));
                    
                    td.appendTo(tr);

                    // third column
                    $('<td/>').text(row.Version).appendTo(tr);

                    tr.appendTo(tableBody);
                }
            };
            var createAgentStatuses = function () {
                statusesBody.html('');

                var tr1 = $('<tr/>');
                $('<td/>').text(installedAgentPluginsDialogNodeText + ':').appendTo(tr1);
                var tr1td2 = $('<td/>');
                tr1td2.html(formatNameValue("", item));
                tr1td2.appendTo(tr1);
                tr1.appendTo(statusesBody);

                var tr2 = $('<tr/>');
                $('<td/>').text(installedAgentPluginsDialogAgentStatusText + ':').appendTo(tr2);
                var tr2td2 = $('<td/>');
                tr2td2.html(formatAgentStatusValue("", item, false));
                tr2td2.appendTo(tr2);
                tr2.appendTo(statusesBody);

                var tr3 = $('<tr/>');
                $('<td/>').text(installedAgentPluginsDialogConnectionStatusText + ':').appendTo(tr3);
                var tr3td2 = $('<td/>');
                tr3td2.html(formatConnectionStatusValue("", item));
                tr3td2.appendTo(tr3);
                tr3.appendTo(statusesBody);
            };
            var showLoadingTableMessage = function()
            {
                tableBody.html('<center>' + loadingText + ' ...</center>');
            };
            var hideLoadingTableMessage = function () {
                tableBody.html('');
            };
   
            createAgentStatuses();

            // show dialog
            var dialog = $('#AgentManagement_InstalledAgentPluginsDialog')
                .dialog(dialogOptions).css('overflow', 'hidden');

            // fetch data for table
            showLoadingTableMessage();
            LoadAgentPlugins(agentId,
                             function (result) {
                                 hideLoadingTableMessage();
                                 createTable(result);
                                 $('#AgentManagement_InstalledAgentPluginsDialog').dialog("option", "position", "center");
                             }, function (error) {
                                 tableBody.html('Error: ' + error);
                             });

            $('button#AgentManagement_InstalledAgentPluginsDialogCloseButton').click(function () {
                $('#AgentManagement_InstalledAgentPluginsDialog').dialog("close");
            });
            
            // add help button 
            $('<a/>').attr('href', this.helpUrl).attr('target', '_blank').attr('rel', 'noopener noreferrer').addClass('AgentManagement_HelpButton').insertBefore('.ui-dialog-titlebar-close');
        }
    };
    promoteAgentWindow = {
        defaultHtml: '',
        show: function () {
            if (this.defaultHtml == '') {
                this.defaultHtml = $('#AgentManagement_PromoteAgentDialog').html();
            }

            $('#AgentManagement_PromoteAgentDialog').html(this.defaultHtml);
            var hasAssociatedAgentNode = false;
            var item = grid.getSelectionModel().getSelections()[0];

            if (!hasAssociatedAgentNode && isAgentNode(item)) {
                hasAssociatedAgentNode = true;
            }

            // select template based on selection 
            var contentSelector = '.AgentManagement_PromoteAgentDialogBody ';
            // Agent with node / Agent without node 
            if (hasAssociatedAgentNode) {
                contentSelector += '.AgentManagement_SelectWithNodes';
                $(contentSelector).show();
                // render node with status icon from grid 
                $('.AgentManagement_Table tr td:last-child').html(formatNameValue("", item));
            } else {
                contentSelector += '.AgentManagement_SelectWithoutNodes';
                $(contentSelector).show();
            }

            // show warning when elected agent is offline 
            if (isAgentOffline(item)) {
                var text = String.format(promoteAgentDialogOfflineAgentsWarning);
                $(contentSelector + " .AgentManagement_DialogOfflineAgentsWarning span:last-child").html(text);
                $(contentSelector + " .AgentManagement_DialogOfflineAgentsWarning").show();
            } else {
                $(contentSelector + " .AgentManagement_DialogOfflineAgentsWarning").hide();
            }

            var dialogOptions = {
                width: 450,
                height: 'auto',
                modal: true,
                overlay: { "background-color": "black", opacity: 0.4 },
                title: promoteAgentDialogTitle,
                dialogClass: 'AgentManagement_PromoteAgentDialog',
                close: function () {
                    $('#AgentManagement_PromoteAgentDialog').html('');
                },
                buttons: [
                    {
                        text: promoteAgentDialogYesButton,
                        click: function () {
                            $(this).dialog("close");
                            PromoteAgentToRemoteCollector(item);
                        }
                    },
                    {
                        text: promoteAgentDialogNoButton,
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                resizable: false
            };

            // show dialog 
            $('#AgentManagement_PromoteAgentDialog').dialog(dialogOptions).css('overflow', 'hidden');
        }
    }; 

    LoadAgentPlugins = function(agentId, successCallback, errorCallback) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                             "GetPlugins", { agentId: agentId },
                             function (result) {
                                 successCallback(result);
                             }, function (error) {
                                 errorCallback(error);
                             });
    }

    CreatePluginStatusCellContent = function (agentId, row) {
        var content = $(String.format('<span id="{0}"></span>', GetPluginStatusCellContentId(agentId, row.Id)));

        if (row.StatusIcon != null && row.StatusIcon != '') {
            $('<img />').attr('src', row.StatusIcon).appendTo(content);
        }
        if (row.StatusText != null) {
            content.append(row.StatusText);
        }
        if (row.StatusMessage != null && row.StatusMessage != '') {
            content.append('<br /><span class="sw-AgentManagement-grid-help-text">' + row.StatusMessage + '</span>');
        }
        if ($.inArray(row.Status, failedPluginStatuses) != -1) {
            content.append('<br />');

            var statusAdditionalTextContainer = $(String.format('<span id="{0}"></span>', GetPluginFixLinkId(agentId, row.Id)));

            var redeployLink = $(String.format('<span id="{0}" class="AgentManagement_HelpLink">&raquo; {1}</span>',
                GetPluginFixLinkId(agentId, row.Id), redeployPluginLinkText));
            redeployLink.on('click', function () {
                RedeployPlugin(agentId, row.Id);
            });

            statusAdditionalTextContainer.append(redeployLink);
            content.append(statusAdditionalTextContainer);
        }
        return content;
    }
    
    AddAgent = function () {
        window.location = '/Orion/AgentManagement/Admin/AddAgent.aspx?returnUrl=' + returnUrl;
    };

    EditAgent = function (item) {
        window.location = '/Orion/AgentManagement/Admin/EditAgent.aspx?id=' + item.data.ID + '&returnUrl=' + returnUrl;
    };

    GetPageSize = function () {
        var pSize = parseInt(getCookie(cookiePrefix + 'PageSize'), 10);
        return isNaN(pSize) ? pageSize : pSize;
    };

    RefreshObjects = function (initializeSelection) {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: LoadingDataLabel
            });
        }
        
        var records = groupByList.getSelectedRecords();
        var groupByItem = '';
        if (records.length > 0)
            groupByItem = records[0].data.Value;

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {
            groupByCategory: groupByCombo.getValue(),
            groupByItem: groupByItem
        };
        grid.store.baseParams = { start: (initialPage - 1) * GetPageSize(), limit: GetPageSize() };
        grid.store.on('load', function () {
            loadingMask.hide();
            if (initializeSelection)
                UpdateSelection();
        });
        grid.store.load();
    };

    RefreshGroupByList = function(groupByCategory, handler) {
        groupByStore.proxy.conn.jsonData = { groupByCategory: groupByCategory };
        if (handler) {
            var internalHandler = function() {
                handler();
                groupByStore.un('load', internalHandler);
            };
            groupByStore.on('load', internalHandler);
        }

        groupByStore.load();
    };

    UpdateAgentsStatuses = function () {
        dataStore.each(function (record) {
            var recordCopy = record;
            if (record.data.ID <= 0)
                return;
            
            GetAgent(record.data.ID, function (result, context) {
                if (result != null) {
                    if (result.AgentStatus == AgentStatusUpdateFailed) { // if update failed show force button
                        forceUpdateAllowed = true;
                        context.record.set("ForceUpdateSupported", true);
                        UpdateToolbarButtons();
                    }
                    if (result.ConnectionStatus == ConnectionStatusDeploymentFailed) { // if deployment failed show force button
                        forceRedeployAllowed = true;
                        context.record.set("RedeploySupported", true);
                        UpdateToolbarButtons();
                    }
                    context.record.set("ConnectionStatusMessage", result.ConnectionStatusMessage);
                    context.record.set("ConnectionStatus", result.ConnectionStatus);
                    context.record.set("AgentStatusMessage", result.AgentStatusMessage);
                    context.record.set("AgentStatus", result.AgentStatus);
                    context.record.set("AgentVersion", result.AgentVersion.Major + '.' + result.AgentVersion.Minor + '.' + result.AgentVersion.Build + '.' + result.AgentVersion.Revision);
                } else {
                    grid.store.remove(context.record);
                }
            }, { 'record': recordCopy });
        });
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;
        var moreActionsDropdown = map.MoreActionsDropdownButton.menu.items.map;
        var anyAgentWithUninstallInProgressStatus = false;

        Ext.each(selItems.getSelections(), function (item) {
            if (item.data.AgentStatus == AgentStatusUninstallInProgress) {
                anyAgentWithUninstallInProgressStatus = true;
                return false;   // break
            }
        });

        if (!isEditGrid) {
            map.EditButton.hide();
            map.DeleteButton.hide();
            moreActionsDropdown.ViewInstalledAgentPluginsButton.hide();
            map.ChooseResourcesButton.hide();
            map.ManageAsNodeButton.hide();
            moreActionsDropdown.PromoteAgentToRemoteCollector.hide();
        }

        if (forceRedeployAllowed && isEditGrid) {
            moreActionsDropdown.DeployAgentButton.setVisible(true);

            var disable = false;
            // enable redeploy button only if all selected agents are redeployable
            Ext.each(selItems.getSelections(), function (item) {
                if (!item.data.RedeploySupported) {
                    disable = true;
                    return false;   // break
                }
            });

            moreActionsDropdown.DeployAgentButton.setDisabled(count > 0 ? disable : true);
        }
        
        if (isEditGrid) {
            var disable = true;
            // enable reboot button only if all selected agents are in reboot pending state
            Ext.each(selItems.getSelections(), function (item) {
                if (item.data.AgentStatus == AgentStatusRebootRequired) {
                    disable = false;
                    return false;   // break
                }
            });
            moreActionsDropdown.RebootAgentButton.setDisabled(count > 0 ? disable : true);
            
            disable = true;
            // enable update button only if all selected agents are in update available state
            Ext.each(selItems.getSelections(), function (item) {
                if (item.data.AgentStatus == AgentStatusUpdateAvailable || item.data.AgentStatus == AgentStatusPluginUpdatePending) {
                    disable = false;
                    return false;   // break
                }
            });
            moreActionsDropdown.UpdateAgentButton.setDisabled(count > 0 ? disable : true);

            //PromoteAgentToRemoteCollector action button disable/enable control            
            if (isManageAgentsPage) {
                moreActionsDropdown.PromoteAgentToRemoteCollector.setDisabled(count === 0 || count > 1);
            }
        }

        map.AddButton.setDisabled(false);
        map.EditButton.setDisabled(true);
        map.DeleteButton.setDisabled(true);
        moreActionsDropdown.ViewInstalledAgentPluginsButton.setDisabled(true);
        map.ChooseResourcesButton.setDisabled(true);
        map.ManageAsNodeButton.setDisabled(true);
        moreActionsDropdown.ReconnectAgentButton.setDisabled(true);

        moreActionsDropdown.RestartAgentButton.setDisabled(count > 0 ? false : true);

        if (count === 1) {
            map.EditButton.setDisabled(false);
            map.DeleteButton.setDisabled(false);
            moreActionsDropdown.ViewInstalledAgentPluginsButton.setDisabled(false);
            if (isAgentNode(selItems.getSelected())) {
                map.ChooseResourcesButton.setDisabled(false);
            } else {
                map.ManageAsNodeButton.setDisabled(false);
            }
            //enable Reconnect button only if passive agent selected
            Ext.each(selItems.getSelections(), function (item) {
                if (item.data.AgentStatus == AgentStatusAgentUninstalled &&
                    item.data.Mode === 2 /*==PassiveMode*/) {
                    moreActionsDropdown.ReconnectAgentButton.setDisabled(false);
                    return false;   // break
                }
            });
        } else if (count > 1) {
            if (AllSelectedAgentsHaveNode() && !anyAgentWithUninstallInProgressStatus) {
                map.ChooseResourcesButton.setDisabled(false);
            }
            map.DeleteButton.setDisabled(false);
        }
    };

    var isAgentNode = function(item) {
        return item.data.NodeID > 0 && item.data.NodeObjectSubType == 'Agent';
    };
    
    AllSelectedAgentsHaveNode = function() {
        var allHaveNode = true;
        grid.getSelectionModel().each(function (item) {
            if (!isAgentNode(item))
                allHaveNode = false;
        });
        return allHaveNode;
    };

    SetSelectedAgentsToField = function () {
        var selectedAgents = '';
        grid.getSelectionModel().each(function (item) {
            if (selectedAgents != '')
                selectedAgents += ',';

            selectedAgents += item.data.ID;
        });

        var field = $('#' + selectedAgentsFieldClientID);
        field.val(selectedAgents);
    };

    UpdateSelection = function () {
        var field = $('#' + selectedAgentsFieldClientID);
        var selectedAgents = field.val();
        var ids = selectedAgents.split(',');
        var selectionModel = grid.getSelectionModel();

        // count only valid records (value > 0)
        var excludedAgentsLen = 0;
        $.each(excludedAgents, function (index, value) {
            if (value > 0)
                excludedAgentsLen++;
        });

        if (ids.length > 0 && ids[0]) {
            selectionModel.clearSelections();
            Ext.each(ids, function (agentId) {
                var recordIndex = dataStore.findBy(function (record, id) {
                    return record.data.ID == agentId;
                });

                selectionModel.selectRow(recordIndex, true);
            });
        } else if (!isEditGrid && (dataStore.getTotalCount() - excludedAgentsLen == 1)) {
            // preselect single enabled record in the grid
            dataStore.each(function (record) {
                if (record.data.ID > 0 && $.inArray(record.data.ID, excludedAgents) == -1) {
                    var index = dataStore.find("ID", record.data.ID);
                    if (index > -1) {
                        selectionModel.selectRow(index, true);
                        return false;
                    }
                }
            });
        }
    };

    DeployWithExistingCredential = function (items, credentialId) {
        var toAssign = [];

        Ext.each(items, function (item) {
            toAssign.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(forceRedeployWaitDialogText);

        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "DeployWithExistingCredential", { agentIds: toAssign, credentialId: credentialId },
                         function (result) {
                             waitMsg.hide();
                             grid.store.reload();
                         });
    };

    DeployWithNewCredential = function (items, credentials) {
        var toAssign = [];

        Ext.each(items, function (item) {
            toAssign.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(forceRedeployWaitDialogText);

        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                        "DeployWithNewCredential", { agentIds: toAssign, credentials: credentials },
                        function (result) {
                            waitMsg.hide();
                            grid.store.reload();
                        });
    };

    GetAgent = function (id, onSuccess, context) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                        "GetAgent", { agentId: id },
                        function (result) {
                            onSuccess(result, context);
                        });
    };

    CallDeleteAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "DeleteAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         },
                         function (msg) {
                             activeAgentsCallInProgress = false; // reset flag if call failed
                         });
    };

    CallDeleteAndUninstallAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "DeleteAndUninstallAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         },
                         function (msg) {
                             activeAgentsCallInProgress = false; // reset flag if call failed
                         });
    };

    CallGetActiveAgentsWaitingForAdd = function (onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                        "GetActiveAgentsWaitingForAdd", {},
                        function (result) {
                            onSuccess(result);
                        });
    };

    CallReconnectAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "ReconnectAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         });

    }

    DeleteAndUninstallSelectedItems = function () {
        var items =  grid.getSelectionModel().getSelections(); 
        var toDelete = [];

        Ext.each(items, function (item) {
            toDelete.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait((toDelete.length > 1 ? deletingAndUninstallingAgentsPluralText : deletingAndUninstallingAgentsSingularText) + " ...");

        CallDeleteAndUninstallAgents(toDelete, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    DeleteSelectedItems = function (items) {
        var toDelete = [];

        Ext.each(items, function (item) {
            toDelete.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(deletingAgentsText + " ...");

        CallDeleteAgents(toDelete, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    ForceUpdateSelectedItems = function (items) {
        var toUpdate = [];

        Ext.each(items, function (item) {
            if (item.data.ForceUpdateSupported)
                toUpdate.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(forceUpdateWaitMsg);

        ForceUpdateOfAgents(toUpdate, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    ForceUpdateOfAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "UpdateAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };
    
    RebootSelectedAgents = function (items) {
        var toReboot = [];

        Ext.each(items, function (item) {
            toReboot.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(sendingRebootMessageWaitMsg);

        RebootAgents(toReboot, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    RebootAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "RebootAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };
    
    RestartSelectedAgents = function (items) {
        var toRestart = [];

        Ext.each(items, function (item) {
            toRestart.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(sendingRestartMessageWaitMsg);

        RestartAgents(toRestart, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    PromoteAgentToRemoteCollector = function (item) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
            "PromoteAgentToRemoteCollector", { agentId: item.data.ID },
            function (result) {
                if (result.Status === WebApiResponseStatus.Error) {
                    var errorMsg = promoteAgentErrorDialogMessages[result.ErrorCode];
                    if (errorMsg === undefined) {
                        errorMsg = result.Message;
                    } else {
                        errorMsg = String.format(errorMsg, item.data.Hostname);
                    }
                    Ext.Msg.show({
                        title: promoteErrorDialogTitle,
                        msg: errorMsg,
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR,
                        fn: function() {
                        }
                    });
                    return; // do not reload after error
                }

                grid.store.reload();
                RefreshObjects();
            },
            function () {
            });
    };

    RestartAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "RestartAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    UpdateSelectedAgents = function (items) {
        var toUpdate = [];

        Ext.each(items, function (item) {
            toUpdate.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(updatingAgentsWaitMsg);

        UpdateAgents(toUpdate, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    UpdateAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                         "UpdateAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    FilterNotificationItems = function () {
        agentsNotificationPanel.update('');

        if (getCookie(cookiePrefix + 'HideActiveAgentsNotification') == 1) {
            grid.store.filterBy(function (record, id) {
                if (record.data.ID == -1) {
                    if (record.data.Name == 1)
                        agentsNotificationPanel.update(String.format(waitingAgentsSingularNotificationFormat, record.data.Name));
                    else
                        agentsNotificationPanel.update(String.format(waitingAgentsPluralNotificationFormat, record.data.Name));

                    return false;
                }

                return true;
            });
        } else {
            grid.store.clearFilter();
        }
    };

    ChooseResources = function () {
        var items = grid.getSelectionModel().getSelections();
        if (items.length == 1) {
            var item = items[0];
            var targetUrl = String.format('/Orion/Nodes/ListResources.aspx?Nodes={0}&ReturnTo={1}&SubType=Agent', item.data.NodeID, returnUrl);
            window.location = targetUrl;
        } else if (items.length > 1) {
            runAgentDiscoveryForSelectedAgents(items);
        }
    };

    SelectRowByAgentId = function(agentId) {
        var recordIndex = dataStore.findBy(function (record, id) {
            return record.data.ID == agentId;
        });

        grid.getSelectionModel().selectRow(recordIndex, true);
    }

    ShowPluginDetails = function (agentId) {
        SelectRowByAgentId(agentId);
        installedAgentPluginsWindow.show();
    };

    RedeployPlugin = function (agentId, pluginId) {
        var pluginStatusCellContent = $('#' + GetPluginStatusCellContentId(agentId, pluginId));

        // replace "reinstall" link with "peding" message
        var fixLink = $('#' + GetPluginFixLinkId(agentId, pluginId));
        fixLink.empty();
        fixLink.append('<img src="/Orion/images/loading_gen_16x16.gif" /><span class="sw-AgentManagement-grid-help-text">' + pluginIsBeingReinstalledText + '</span>')
        
        ORION.callWebService("/Orion/Services/Information.asmx",
                             "Invoke", { entity: "Orion.AgentManagement.Agent", verb: 'RedeployPlugin', param: [agentId, pluginId] });

        // refresh plugin status
        var pluginRefreshInterval = setInterval(function() {
            LoadAgentPlugins(agentId,
                function(rows) {
                    for (var i = 0; i < rows.length; i++) {
                        var row = rows[i];
                        if (row.Id != pluginId) {
                            continue;
                        }

                        // if plugin is no more in in-progress status, show new status
                        if ($.inArray(row.Status, inProgressPluginStatuses) == -1) {
                            pluginStatusCellContent.replaceWith(CreatePluginStatusCellContent(agentId, row));
                            clearInterval(pluginRefreshInterval);
                        }
                    }
                });
        }, 5000);
    }

    GetPluginFixLinkId = function(agentId, pluginId) {
        return String.format('PluginFixLink-{0}-{1}', agentId, pluginId);
    }

    GetPluginStatusCellContentId = function (agentId, pluginId) {
        return String.format('PluginStatus-{0}-{1}', agentId, pluginId);
    }

    RetryAgentDeploymentForAgent = function (agentId) {
        SelectRowByAgentId(agentId);
        RetryAgentDeployment();
    }

    RetryAgentDeployment = function() {
        if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

        forceDeployWindow.show();
    }

    function formatNameValue(value, record) {
        if (record.data.ID == -1) { // this record contains number of waiting agents
            if (value == 1)
                value = String.format(waitingAgentsSingularMessageFormat, value);
            else
                value = String.format(waitingAgentsPluralMessageFormat, value);

            // wrap into span and make it relative so that it can span over multiple columns
            value = '<span style="position: absolute; top: 3px; left: 25px;">' + value + '</span>';
        } else {
            value = record.data.Name;

            var index1 = $.inArray(record.data.ID, unsupportedAgents);
            var index2 = $.inArray(record.data.ID, excludedAgents);
            if (index1 != -1) {
                value = value + AgentManagement_HoverHelpLinkHtml({
                    helpLinkText: unsupportedAgentTextShort,
                    html: unsupportedAgentText
                });
            } else if (index2 != -1) {
                value = value + AgentManagement_HoverHelpLinkHtml({
                    helpLinkText: alreadyAssignedTextShort,
                    html: alreadyAssignedText
                });
            }
            else if (isAgentNode(record)) {
                value = '<a href="/Orion/View.aspx?NetObject=N:' + record.data.NodeID + '"><img class="StatusIcon" src="/Orion/images/StatusIcons/Small-' + record.data.NodeIcon + '" netobject="N:' + record.data.NodeID + '"> ' + escapeHtml(record.data.NodeCaption) + "</a>";
            } else {
                value = '<img class="StatusIcon" src="/Orion/AgentManagement/Images/Icon.Agent_16x16.png"> ' + value;
            }
        }

        return value;
    }

    function isAgentOffline(item) {
        return item.data.ConnectionStatus != ConnectionStatusOk || item.data.ConnectionStatus == AgentStatusUnknown;
    }

    function goToEditNodesPage() {
        // reused from NodeManagementPaging.js
        function guidGenerator() {
            var guidPiece = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };
            return (guidPiece() + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + guidPiece() + guidPiece());
        }

        // get EngineNetObjectIds from selected agents
        var netobjectIds = [];
        var items = grid.getSelectionModel().getSelections();
        var agentIds = [];
        Ext.each(items, function (item) {
            agentIds.push(item.data.ID);
            if (isAgentNode(item)) {
                netobjectIds.push(String.format("N:{0}:{1}", item.data.NodeID, item.data.PollingEngineId));
            }
        });
       
        var actionUrl = String.format('/Orion/Nodes/NodeProperties.aspx?returnUrl={0}', returnUrl);
        var params = netobjectIds.join(',');
        var guid = guidGenerator();
        var stringForm = "<form id='AgentManagement_SelectedNetObjects' action='{0}' method='POST'> \
                            <input type='hidden' name='Nodes' value='{1}'/> \
                            <input type='hidden' name='ReturnTo' value='{2}'/> \
                             <input type='hidden' name='GuidID' value='{3}'/> \
                         </form>";

        var postForm = $(String.format(stringForm, actionUrl, params, returnUrl, guid));
        $('body').append(postForm);
        $('#AgentManagement_SelectedNetObjects').submit();

        return false;
    }

    function runAgentDiscoveryForSelectedAgents(items) {
        if (items.length == 0)
            return;

        var agentGuids = [];
        Ext.each(items, function (item) {
            agentGuids.push(item.data.AgentGuid);
        });

        var actionUrl = String.format('/Orion/Discovery/Default.aspx?returnUrl={0}', returnUrl);
        var params = agentGuids.join(',');
        var stringForm = "<form id='AgentManagement_SelectedNetObjects' action='{0}' method='POST'> \
                            <input type='hidden' name='action' value='discoverAgents'/> \
                            <input type='hidden' name='agentAddresses' value='{1}'/> \
                            <input type='hidden' name='returnTo' value='{2}'/> \
                         </form>";

        var postForm = $(String.format(stringForm, actionUrl, params, returnUrl));
        $('body').append(postForm);
        $('#AgentManagement_SelectedNetObjects').submit();
    }

    function renderName(value, meta, record, rowIndex) {
        value = formatNameValue(value, record);
        return renderDefault(value, meta, record);
    }

    function renderPollingCapacity(value, meta, record, rowIndex) {
        var utilizationBarWidth = 55;

        var percent;
        if (value == null) {
            // Fallback to calculate percentage by hardcoded maximum polling capacity
            percent = record.data.ElementCount / maxRemoteCollectorPollingElements;
            value = Math.ceil(percent * 100);
        } else {
            percent = value / 100.0;
        }
        
        var cssClass;
        if (percent >= 1.0) {
            cssClass = "critical";
        } else if (percent >= 0.8) {
            cssClass = "warning";
        } else {
            cssClass = "normal";
        }

        var progressWidth = Math.ceil(Math.min(percent, 1.0) * utilizationBarWidth);

        var html = "<div class='AgentManagement_PollingCapacityBar utilization-bar " + cssClass + "' style='width:" + utilizationBarWidth + "px'>" +
            "<div class='progress' style='width:" + progressWidth + "px;'></div>" +
            "</div>";

        value = html + " " + value + " %";

        return renderDefault(value, meta, record);
    }

    function renderPollingCompletion(value, meta, record, rowIndex) {
        value = Math.min(Math.max(0, value), 100) + " %";
        return renderDefault(value, meta, record);
    }

    ShowMoreDetailsMessage = function (title, detailedMessageText) {
        Ext.Msg.show({
            title : title,
            msg: detailedMessageText,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.INFO,
            minWidth : 250
        });
    }

    function formatConnectionStatusValue(value, record, addHelpLinkIfNeeded) {
        if (record.data.ID < 0)
            return;

        if (value == '') {
            value = record.data.ConnectionStatus;
        }

        var subMessages = record.data.ConnectionStatusMessage.split(".");
        var mainMessage = '';
        var detailedMessage = '';
        if (subMessages.length == 0) {
            mainMessage = record.data.ConnectionStatusMessage;
        }
        else {
            mainMessage = subMessages[0].trim();
            subMessages.shift();
            detailedMessage = subMessages.join(".").trim();
        }

        var icon;
        var cls = '';
        var clsDetailed = '';
        var additionalLink = '';

        switch (value) {
            case ConnectionStatusOk:
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-Up.gif';
                break;
            case ConnectionStatusConnecting:
            case ConnectionStatusDeploymentPending:
            case ConnectionStatusDeploymentInProgress:
            case ConnectionStatusWaitingForConnection:
                icon = '/Orion/images/loading_gen_small.gif';
                break;
            case ConnectionStatusDeploymentFailed:
                cls += ' Error';
                clsDetailed += ' sw-text-helpful';
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-Down.gif';
                if (addHelpLinkIfNeeded) {
                    additionalLink = '<span class="AgentManagement_HelpLink" onclick="SW.AgentManagement.AgentsGrid.RetryAgentDeploymentForAgent(' + record.data.ID + ');">'
                                   + '&raquo; ' + tryAgainLinkText + '</span>'
                }
                break;

            default: icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-Down.gif';
        }

        if (detailedMessage.length == 0) {
            value = String.format('<span title="{2}" class="{3}"><img src="{0}" /> {1}</span>', icon, mainMessage, Ext.util.Format.stripTags(mainMessage), cls);
            if (additionalLink) {
                value += ' ' + additionalLink;
            }
        } else {
            var moreLink = String.format("<span class='AgentManagement_HelpLink' onclick='SW.AgentManagement.AgentsGrid.ShowMoreDetailsMessage(\"{1}\", \"{2}\");'>"
                                       + "&raquo; {0}</span>", moreLinkText, mainMessage, detailedMessage);

            value = String.format('<span title="{2}" class="{3}"><img src="{0}" /> {1} </span>{6} </br>'
                                + '<span title="{2}" class="{4}"><img src="{0}" style="visibility: hidden;"/> {5}</span>',
                                   icon, mainMessage, Ext.util.Format.stripTags(mainMessage), cls, clsDetailed, detailedMessage, additionalLink);

            value += ' ' + moreLink;
        }

        return value;
    };

    function renderConnectionStatus(value, meta, record) {
        value = formatConnectionStatusValue(value, record, true);

        //wrap long text
        meta.attr = 'style="white-space:normal"';

        return renderDefault(
        value,
        meta,
        record);
    }

    function formatAgentStatusValue(value, record, addHelpLinkIfNeeded) {
        if (record.data.ID < 0)
            return;

        if (value == '') {
            value = record.data.AgentStatus;
        }

        var message = record.data.AgentStatusMessage;
        var icon;
        var cls = '';
        var clickEvent = '';
        var additionalLink = '';

        switch (value) {
            case AgentStatusOk:
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-Up.gif';
                break;
            case AgentStatusUpdateInProgress:
            case AgentStatusRebootInProgress:
            case AgentStatusAgentRestartInProgress:
            case AgentStatusUninstallInProgress:
                icon = '/Orion/images/loading_gen_small.gif';
                break;
            case AgentStatusUpdateFailed:
            case AgentStatusRebootFailed:
                cls += ' Error';
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-UpdateFailed.png';
                break;
            case AgentStatusUpdateAvailable:
            case AgentStatusPluginUpdatePending:
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-UpdateAvailable.png';
                break;
            case AgentStatusRebootRequired:
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-RebootRequired.gif';
                break;
            case AgentStatusPluginErrorOccurred:
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-PluginErrorOccurred.gif';
                if (addHelpLinkIfNeeded) {
                    additionalLink = '<span class="AgentManagement_HelpLink" onclick="SW.AgentManagement.AgentsGrid.ShowPluginDetails(' + record.data.ID + ');">'
                                   + '&raquo; ' + pluginErrorDetailsLinkText + '</span>'
                }
                break;
            case AgentStatusAgentRestartFailed:
                cls += ' Error';
                icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-RestartFailed.png';
                break;

            default: icon = '/Orion/AgentManagement/Images/StatusIcons/Small-Agent-Down.gif';
        }

        value = String.format('<span title="{2}" class="{3}"><img src="{0}" /> {1}</span>', icon, message, Ext.util.Format.stripTags(message), cls);
        if (additionalLink) {
            value += ' ' + additionalLink;
        }
        return value;
    }

    function isAgentNew (record) {
        if (record.data.ID == NotificationRecordId || !isManageAgentsPage)
            return false;
            
        var currentTimeInMiliseconds = new Date().getTime();
        var agentRegisteredInMiliseconds = new Date(parseInt(record.data.RegisteredOn.substr(6))).getTime();
        var difference = currentTimeInMiliseconds - agentRegisteredInMiliseconds;
        return difference > 0 && difference < howLongIsAgentDisplayedAsNewInMiliseconds;
    };

    function renderAgentStatus(value, meta, record) {
        value = formatAgentStatusValue(value, record, true);

        // add 'New' icon
        if (isAgentNew(record)) {
            value = String.format("<span class='AgentManagement_NewAgentIcon'>{0}</span>{1}", newAgentLabelText, value);
        }

        return renderDefault(
        value,
        meta,
        record);
    }

    function renderMode(value, meta, record) {
        if (record.data.ID == NotificationRecordId)
            return '';
        switch (value) {
            case 1:
                return renderDefault(modeActive, meta, record);
            case 2:
                return renderDefault(modePassive, meta, record);
            default:
                return renderDefault(modeAuto, meta, record);
        }
    }
    
    function renderUtcDateTime(value, meta, record) {
        if (record.data.ID == NotificationRecordId)
            return '';
            
        var tmp = new Date(parseInt(value.substr(6)));
        return tmp.toLocaleString();
    }

    function renderIsRemoteCollector(value, meta, record) {
        return renderDefault(value ? '@{R=AgentManagement.Strings;K=RemoteCollectorLabel;E=js}' : '@{R=AgentManagement.Strings;K=NotRemoteCollectorLabel;E=js}', meta, record);
    }

    function renderBoolean(value, meta, record) {
        return renderDefault(value ? '@{R=AgentManagement.Strings;K=Yes;E=js}' : '@{R=AgentManagement.Strings;K=No;E=js}', meta, record);
    }

    function renderDefault(value, meta, record) {
        if (typeof (value) == 'undefined')
            return '';

        var index = $.inArray(record.data.ID, excludedAgents);
        if (index != -1)
            return String.format('<span class="AgentManagement_DisabledText">{0}</span>', value);

        return value;
    }

    function textFieldsValidator(value) {
        if (selectedCredential != -1)
            return true;

        if (value == null || value == '') {
            return fieldMustNotBeEmptyText;
        }
        return true;
    }

    IsCredentialNameUniqueCall = function (credential, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
                         "IsCredentialNameUnique", { credential: credential },
                         function (result) {
                             onSuccess(result);
                         });
    };

    GetSelectedCredentialType = function () {
        if ($('#CredentialTypeCertificateRadio').is(':checked')) {
            return privateKeyCredentialType;
        }
        return usernameAndPasswordCredentialType;
    }

    SetSelectedCredentialType = function (type) {
        if (type == privateKeyCredentialType) {
            $('#CredentialTypeCertificateRadio').prop('checked', 'checked');
        } else {
            $('#CredentialTypeUsernamePasswordRadio').prop('checked', 'checked');
        }
    }

    GetSelectedOsType = function () {
        if ($('#OSTypeLinuxRadio').is(':checked')) {
            return linuxOsType;
        }
        return windowsOsType;
    }

    SetSelectedOsType = function (type) {
        if (type == linuxOsType) {
            $('#OSTypeLinuxRadio').prop('checked', 'checked');
        } else {
            $('#OSTypeWindowsRadio').prop('checked', 'checked');
        }
    }

    GetPrivateKeyFromFile = function (success) {
        var uploader = new AgentManagement.AsyncFileUpload();
        uploader.uploadFile({
            url: "/api/AgentManagementDeployment/GetFileContentAsText",
            fileElementId: 'CredentialPrivateKeyFile'
        }, function (fileContent) {
            success(fileContent);
        });
    }

    GetCredentialFromDialog = function (credentialsReady) {
        var credential = {};
        credential.credentialId = newCredentialId;
        credential.credentialName = credentialNameField.getValue();

        if (GetSelectedCredentialType() == privateKeyCredentialType) {
            credential.type = privateKeyCredentialType;
            credential.credentialId = selectedSshPrivateKeyCredential;
            credential.username = usernameField.getValue();
            credential.privateKey = certificateData;
            credential.privateKeyPassword = privateKeyPasswordField.getValue();
            if (credential.credentialId > 0) {
                credential.credentialName = GetExistingCredentialName(credential.credentialId, sshPrivateKeyCredentialsList);
            }

        } else {
            credential.type = usernameAndPasswordCredentialType;
            credential.credentialId = selectedCredential;
            credential.username = usernameField.getValue();
            credential.password = passwordField.getValue();
            if (credential.credentialId > 0) {
                credential.credentialName = GetExistingCredentialName(credential.credentialId, credentialsList);
            }
        }

        if (useSudoCheckbox.getValue()) {
            credential.additionalCredential = {
                type: usernameAndPasswordCredentialType,
                credentialId: selectedAdditionalCredential,
                credentialName: additionalCredentialNameField.getValue(),
                username: additionalUsernameField.getValue(),
                password: additionalPasswordField.getValue()
            };

            if (credential.additionalCredential.credentialId > 0) {
                credential.additionalCredential.credentialName = GetExistingCredentialName(credential.additionalCredential.credentialId, credentialsList);
            }
        }

        if (credential.credentialId != newCredentialId || certificateData || GetSelectedCredentialType() != privateKeyCredentialType) {
            credentialsReady(credential);
        } else if (certificateFileField.get(0).files.length > 0) {
            GetPrivateKeyFromFile(function (privateKey) {
                certificateData = privateKey;
                credential.privateKey = certificateData;
                credentialsReady(credential);
            });
        } else {
            Ext.Msg.show({
                title: errorTitle,
                msg: missingPrivateKeyError,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    }

    GetExistingCredentialName = function (credentialId, credentialCollection) {
        var matches = credentialCollection.filter(function (value) {
            return value.Key == credentialId;
        });
        return matches.length > 0 ? matches[0].Value : '';
    }

    AreCredentialFieldsValid = function () {
        var valid = true;

        if (GetSelectedCredentialType() == privateKeyCredentialType) {
            if (selectedSshPrivateKeyCredential == newCredentialId) {
                valid = credentialNameField.isValid() && valid;
                valid = usernameField.isValid() && valid;
                if (!certificateData && certificateFileField.get(0).files.length == 0) {
                    valid = false;
                    Ext.Msg.show({
                        title: errorTitle,
                        msg: missingPrivateKeyError,
                        icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                    });
                }
            }
        } else {
            if (selectedCredential == newCredentialId) {
                valid = credentialNameField.isValid() && valid;
                valid = usernameField.isValid() && valid;
                valid = passwordField.isValid() && valid;
            }
        }

        if (useSudoCheckbox.getValue()) {
            if (selectedAdditionalCredential == newCredentialId) {
                // additional creentials may contain just password
                valid = additionalPasswordField.isValid() && valid;
            }
        }
        return valid;
    };

    ShowPrivateKeyDialog = function () {
        var textField = new Ext.form.TextArea({
            name: 'privateKeyData',
            fieldLabel: pastePrivateKeyManuallyLabel,
            width: '100%',
            height: 200,
            allowBlank: false
        });
        var formPanel = new Ext.FormPanel({
            height: 200,
            labelAlign: 'top',
            items: [textField]
        });
        var privateKeyWindow = new Ext.Window({
            title: pastePrivateKeyManuallyTitle,
            width: 400,
            cls: 'sw-AgentManagement-CertificatePasteDialog',
            autoHeight: true,
            closable: false,
            modal: true,
            resizable: false,

            items: [formPanel],

            buttons: [
                {
                    text: "@{R=AgentManagement.Strings;K=SubmitButtonLabel;E=js}",
                    disabled: false,
                    handler: function () {
                        certificateData = textField.getValue();
                        certificatePastedManually = true;
                        privateKeyWindow.hide();
                        RefreshCredentialsFormVisibility();
                    }
                },
                {
                    text: "@{R=AgentManagement.Strings;K=CancelButtonLabel;E=js}",
                    handler: function () {
                        privateKeyWindow.hide();
                    }
                }],
        });
        privateKeyWindow.show();
    }

    ResetPrivateKey = function () {
        certificateData = "";
        certificatePastedManually = false;
        RefreshCredentialsFormVisibility();
    }

    RefreshCredentialsFormVisibility = function () {
        if (GetSelectedOsType() == linuxOsType) {
            $('.sw-AgentManagement-CredentialTypeRow').show();
            $('.sw-AgentManagement-SudoSelection').show();
        } else {
            $('.sw-AgentManagement-CredentialTypeRow').hide();
            $('.sw-AgentManagement-SudoSelection').hide();
        }

        if (credentialsCombo.getValue() == newCredentialId && sshPrivateKeyCredentialsCombo.getValue() == newCredentialId) {
            $('.sw-AgentManagement-PrimaryCustomCredential').show();

            if (GetSelectedCredentialType() == privateKeyCredentialType) {
                $('.sw-AgentManagement-UsernamePasswordItem').hide();
                $('.sw-AgentManagement-PrivateKeyItem').show();
            } else {
                $('.sw-AgentManagement-UsernamePasswordItem').show();
                $('.sw-AgentManagement-PrivateKeyItem').hide();
            }
        } else {
            if (GetSelectedCredentialType() == privateKeyCredentialType) {
                $('.sw-AgentManagement-UsernamePasswordItem').hide();
                $('.sw-AgentManagement-PrivateKeyItem').show();
            } else {
                $('.sw-AgentManagement-UsernamePasswordItem').show();
                $('.sw-AgentManagement-PrivateKeyItem').hide();
            }

            $('.sw-AgentManagement-PrimaryCustomCredential').hide();
        }

        if ($('#IncludeSudoCredentials').is(':checked') && GetSelectedOsType() == linuxOsType) {
            $('.sw-AgentManagement-SudoSection').show();

            if (additionalCredentialsCombo.getValue() == newCredentialId) {
                $('.sw-AgentManagement-AdditionalCustomCredential').show();
            } else {
                $('.sw-AgentManagement-AdditionalCustomCredential').hide();
            }
        } else {
            $('.sw-AgentManagement-SudoSection').hide();
        }

        if (certificatePastedManually) {
            $('#CertificateViaFileBlock').hide();
            $('#CertificateManualBlock').show();
        } else {
            $('#CertificateViaFileBlock').show();
            $('#CertificateManualBlock').hide();
        }

        forceDeployWindow.center();
        forceDeployWindow.syncShadow();
    };

    OnCredentialTypeChange = function () {
        credentialsCombo.setValue(newCredentialId);
        sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
        selectedCredential = newCredentialId;
        selectedSshPrivateKeyCredential = newCredentialId;

        ClearCredentialFields();

        RefreshCredentialsFormVisibility();
    };

    OnOsTypeChange = function () {
        credentialsCombo.setValue(newCredentialId);
        sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
        additionalCredentialsCombo.setValue(newCredentialId);
        selectedCredential = newCredentialId;
        selectedSshPrivateKeyCredential = newCredentialId;
        selectedAdditionalCredential = newCredentialId;

        if (GetSelectedOsType() != linuxOsType) {
            SetSelectedCredentialType(usernameAndPasswordCredentialType);
            $('#IncludeSudoCredentials').prop('checked', false);
            $('#OSTypeWindowsRadioLabel').addClass('active');
            $('#OSTypeLinuxRadioLabel').removeClass('active');
        } else {
            $('#OSTypeWindowsRadioLabel').removeClass('active');
            $('#OSTypeLinuxRadioLabel').addClass('active');
        }

        ClearCredentialFields();
        ClearAdditionalCredentialFields();

        RefreshCredentialsFormVisibility();
    };

    ClearCredentialFields = function () {
        credentialNameField.setValue('');
        credentialNameField.clearInvalid();
        usernameField.setValue('');
        usernameField.clearInvalid();
        passwordField.setValue('');
        passwordField.clearInvalid();
        privateKeyPasswordField.setValue('');
        privateKeyPasswordField.clearInvalid();
        certificateData = '';
        $('#testResults').hide();
    };

    ClearAdditionalCredentialFields = function () {
        additionalCredentialNameField.setValue('');
        additionalCredentialNameField.clearInvalid();
        additionalUsernameField.setValue('');
        additionalUsernameField.clearInvalid();
        additionalPasswordField.setValue('');
        additionalPasswordField.clearInvalid();
    };

    InitAdditionalCredentials = function () {
        useSudoCheckbox = new Ext.form.Checkbox({
            applyTo: "IncludeSudoCredentials",
            boxLabel: ''
        });

        additionalCredentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: credentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'AdditionalCredentialNamesCombo',
            hiddenId: 'AdditionalCredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedAdditionalCredential = index.data.Key;
                    additionalCredentialNameField.validate();
                    additionalUsernameField.validate();
                    additionalPasswordField.validate();

                    if (selectedCredential == newCredentialId) {
                        ClearAdditionalCredentialFields();
                    }

                    RefreshCredentialsFormVisibility();
                }
            }
        });

        additionalCredentialsCombo.setValue(selectedAdditionalCredential);

        additionalCredentialNameField = new Ext.form.TextField({
            applyTo: "AdditionalCredentialName",
            width: 230,
            validator: textFieldsValidator
        });

        additionalUsernameField = new Ext.form.TextField({
            applyTo: "AdditionalCredentialUsername",
            width: 230,
            validator: textFieldsValidator
        });

        additionalPasswordField = new Ext.form.TextField({
            applyTo: "AdditionalCredentialPassword",
            width: 230,
            validator: textFieldsValidator
        });
    }

    InitForceDeployWindow = function () {
        credentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: credentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'CredentialNamesCombo',
            hiddenId: 'CredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedCredential = index.data.Key;
                    credentialNameField.validate();
                    usernameField.validate();
                    passwordField.validate();

                    if (selectedCredential == newCredentialId) {
                        ClearCredentialFields();
                    }

                    RefreshCredentialsFormVisibility();
                }
            }
        });
        credentialsCombo.setValue(selectedCredential);

        sshPrivateKeyCredentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: sshPrivateKeyCredentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'SshCredentialNamesCombo',
            hiddenId: 'SshCredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedSshPrivateKeyCredential = index.data.Key;
                    credentialNameField.validate();
                    usernameField.validate();

                    if (selectedCredential == newCredentialId) {
                        ClearCredentialFields();
                    }

                    RefreshCredentialsFormVisibility();
                }
            }
        });
        sshPrivateKeyCredentialsCombo.setValue(selectedSshPrivateKeyCredential);

        credentialNameField = new Ext.form.TextField({
            applyTo: "CredentialName",
            width: 230,
            validator: textFieldsValidator
        });

        usernameField = new Ext.form.TextField({
            applyTo: "CredentialUsername",
            width: 230,
            validator: textFieldsValidator
        });

        passwordField = new Ext.form.TextField({
            applyTo: "CredentialPassword",
            width: 230,
            validator: textFieldsValidator
        });

        certificateFileField = $("#CredentialPrivateKeyFile");

        privateKeyPasswordField = new Ext.form.TextField({
            applyTo: "CredentialPrivateKeyPassword",
            buttonOnly: true,
            width: 230,
        });

        InitAdditionalCredentials();

        forceDeployWindow = new Ext.Window({
            title: "@{R=AgentManagement.Strings;K=RedeployDialogTitle;E=js}",
            width: 550,
            cls: 'sw-AgentManagement-credential-window-outer',
            autoHeight: true,
            closable: false,
            modal: true,
            resizable: false,

            items: [
                    new Ext.Panel({
                        applyTo: 'ForceDeployWindow',
                        border: false
                    })
            ],

            buttons: [
                {
                    text: "@{R=AgentManagement.Strings;K=RedeployButtonLabel;E=js}",
                    disabled: false,
                    width: 150,
                    cls: 'sw-btn-primary',
                    handler: function () {
                        if (!AreCredentialFieldsValid())
                            return;

                        GetCredentialFromDialog(function (credentials) {
                            IsCredentialNameUniqueCall(credentials, function (isUnique) {
                                if (isUnique) {
                                    DeployWithNewCredential(grid.getSelectionModel().getSelections(), credentials);
                                    forceDeployWindow.hide();
                                } else {
                                    Ext.Msg.show({
                                        title: errorTitle,
                                        msg: "@{R=AgentManagement.Strings;K=CredentialNameAlreadyExists;E=js}",
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK
                                    });
                                }
                            });
                        });
                    }
                },
                {
                    text: "@{R=AgentManagement.Strings;K=CancelButtonLabel;E=js}",
                    handler: function () {
                        forceDeployWindow.hide();
                        credentialNameField.setValue('');
                        usernameField.setValue('');
                        passwordField.setValue('');
                    }
                }],
            listeners: {
                beforeshow: function () {
                    lastDialogCredentialTestResults = [];

                    $('#ForceDeployWindow').show();

                    if (grid.getSelectionModel().getCount() != 1) {
                        sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
                        credentialsCombo.setValue(newCredentialId);
                        additionalCredentialsCombo.setValue(newCredentialId);
                        selectedSshPrivateKeyCredential = newCredentialId;
                        selectedCredential = newCredentialId;
                    } else {
                        var record = grid.getSelectionModel().getSelected();

                        SetSelectedOsType(record.data.OsType ? record.data.OsType : windowsOsType);

                        if (record.data.CredentialId > 0) {
                            if (record.data.CredentialType == privateKeyCredentialType) {
                                SetSelectedCredentialType(privateKeyCredentialType);
                                sshPrivateKeyCredentialsCombo.setValue(record.data.CredentialId);
                                credentialsCombo.setValue(newCredentialId);
                                selectedSshPrivateKeyCredential = record.data.CredentialId;
                                selectedCredential = newCredentialId;
                            } else {
                                GetSelectedCredentialType() == usernameAndPasswordCredentialType;
                                sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
                                credentialsCombo.setValue(record.data.CredentialId);
                                selectedSshPrivateKeyCredential = newCredentialId;
                                selectedCredential = record.data.CredentialId;
                            }
                        } else {
                            sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
                            credentialsCombo.setValue(newCredentialId);
                            selectedSshPrivateKeyCredential = newCredentialId;
                            selectedCredential = newCredentialId;
                        }

                        if (record.data.AdditionalCredentialId > 0) {
                            useSudoCheckbox.setValue(true);
                            additionalCredentialsCombo.setValue(record.data.AdditionalCredentialId);
                            selectedAdditionalCredential = record.data.AdditionalCredentialId;
                        } else {
                            useSudoCheckbox.setValue(false);
                            additionalCredentialsCombo.setValue(newCredentialId);
                            selectedAdditionalCredential = newCredentialId;
                        }
                    }

                    Ext.each(grid.getSelectionModel().getSelected(), function (record) {
                        record.data.CredentialTested = false;
                    });

                    ClearCredentialFields();
                    ClearAdditionalCredentialFields();

                    RefreshCredentialsFormVisibility();

                    OnOsTypeChange();

                    var testResults = $('#testResults');
                    testResults.html('');
                    testResults.hide();
                }
            }
        });
    };

    function updateCookiePrefix() {
        defaultCookiePrefix = 'AgentManagement_' + agentGridMode + '_AgentsGrid_';
        if (userName) {
            cookiePrefix = userName + '_' + defaultCookiePrefix;
        } else {
            cookiePrefix = defaultCookiePrefix;
        }
    }

    InitMode = function () {
        agentGridMode = manageAgentsMode;
        if ($("#switchAgentsGridToManageRemoteCollectorsMode").length > 0) {
            agentGridMode = manageRemoteCollectorsMode;
        }
        updateCookiePrefix();

        isManageRemoteCollectorsPage = agentGridMode === manageRemoteCollectorsMode;
        isManageAgentsPage = agentGridMode === manageAgentsMode;
    }

    InitGroupingPanel = function () {
        var groupByGroups = [
            ['none', noGroupingLabelText],
            ['mode', communicationTypeLabelText],
            ['status', agentStatusLabelText],
            ['connection', connectionStatusLabelText],
            ['poller', orionPollerLabelText]
        ];

        if (isManageAgentsPage) {
            groupByGroups.push(['isremotecollector', isRemoteCollectorLabelText]);
        }

        groupByCombo = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: groupByGroups
            }),
            valueField: 'itemValue',
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            width: 150,
            editable: false,
            transform: 'groupBySelect',
            hiddenId: 'groupBySelect',
            listeners: {
                select: function(record, index) {
                    RefreshGroupByList(record.value);

                    if (record.value == 'none')
                        RefreshObjects();
                }
            }
        });
        groupByCombo.setValue('none');

        var selectPanel = new Ext.Container({
            applyTo: 'AgentManagement_GroupingPanel',
            region: 'north',
            height: 50,
            layout: 'form'
        });

        var tpl = new Ext.XTemplate(
            '<tpl for=".">',
            '<div class="AgentManagement_GroupByItem" id="{Value}">',
            '{Label}</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        );

        groupByStore = new ORION.WebServiceStore(
            "/Orion/AgentManagement/Services/AgentsGrid.asmx/GetGroupByItems",
            [
                { name: 'Value', mapping: 0 },
                { name: 'Label', mapping: 1 },
                { name: 'Type', mapping: 2 },
            ],
            "Label");

        groupByList = new Ext.DataView({
            cls: 'AgentManagement_GroupByList',
            border: true,
            store: groupByStore,
            tpl: tpl,
            height: AgentManagement_GetManagementGridHeight() - selectPanel.getSize().height,
            autoHeight: false,
            autoScroll: true,
            singleSelect: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            itemSelector: 'div.AgentManagement_GroupByItem',
            listeners: {
                selectionchange: function(view, selections) {
                    if (selections.length > 0)
                        RefreshObjects();
                }
            }
        });

        groupingPanel = new Ext.Panel({
            region: 'west',
            width: 200,
            split: true,
            items: [
                selectPanel,
                groupByList
            ]
        });

        groupingPanel.on('bodyresize', function() {
            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        });
    };

    InitLayout = function() {
        var panel = new Ext.Container({
            renderTo: 'AgentsGrid',
            height: AgentManagement_GetManagementGridHeight(),
            layout: 'border',
            items: [groupingPanel, grid]
        });
        Ext.EventManager.onWindowResize(function() {
            panel.setHeight(AgentManagement_GetManagementGridHeight());
            panel.doLayout();
        }, panel);

        $('#IncludeSudoCredentials').on('change', RefreshCredentialsFormVisibility);
        $('#CredentialTypeCertificateRadio').on('change', OnCredentialTypeChange);
        $('#CredentialTypeUsernamePasswordRadio').on('change', OnCredentialTypeChange);
        $('#OSTypeWindowsRadio').on('change', OnOsTypeChange);
        $('#OSTypeLinuxRadio').on('change', OnOsTypeChange);
    };

    InitGrid = function () {
        selectorModel = new Ext.sw.AgentManagement.grid.CheckboxSelectionModel(
            {
                disabledRows: excludedAgents
            });

        selectorModel.on("selectionchange", function() {
            UpdateToolbarButtons();
            SetSelectedAgentsToField();
        });

        if (isManageAgentsPage) {
            dataStore = new ORION.WebServiceStore(
                "/Orion/AgentManagement/Services/AgentsGrid.asmx/GetAgentsPaged",
                [
                    { name: 'ID', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'ConnectionStatus', mapping: 2 },
                    { name: 'ConnectionStatusMessage', mapping: 3 },
                    { name: 'AgentStatus', mapping: 4 },
                    { name: 'AgentStatusMessage', mapping: 5 },
                    { name: 'PollingEngineId', mapping: 6 },
                    { name: 'PollingEngineName', mapping: 7 },
                    { name: 'Mode', mapping: 8 },
                    { name: 'AgentGuid', mapping: 9 },
                    { name: 'Hostname', mapping: 10 },
                    { name: 'DNSName', mapping: 11 },
                    { name: 'IP', mapping: 12 },
                    { name: 'AgentVersion', mapping: 13 },
                    { name: 'ForceUpdateSupported', mapping: 14 },
                    { name: 'RedeploySupported', mapping: 15 },
                    { name: 'AutoUpdateEnabled', mapping: 16 },
                    { name: 'MatchingEngineId', mapping: 17 },
                    { name: 'IpOrHostname', mapping: 18 },
                    { name: 'NodeID', mapping: 19 },
                    { name: 'NodeIcon', mapping: 20 },
                    { name: 'NodeCaption', mapping: 21 },
                    { name: 'NodeObjectSubType', mapping: 22 },
                    { name: 'IsRemoteCollector', mapping: 24 },
                    { name: 'RegisteredOn', mapping: 25 }
                ],
                "Name");
        }
        if (isManageRemoteCollectorsPage) {
            dataStore = new ORION.WebServiceStore(
                "/Orion/AgentManagement/Services/AgentsGrid.asmx/GetRemoteCollectorsPaged",
                [
                    { name: 'ID', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'ConnectionStatus', mapping: 2 },
                    { name: 'ConnectionStatusMessage', mapping: 3 },
                    { name: 'AgentStatus', mapping: 4 },
                    { name: 'AgentStatusMessage', mapping: 5 },
                    { name: 'PollingEngineId', mapping: 6 },
                    { name: 'PollingEngineName', mapping: 7 },
                    { name: 'Mode', mapping: 8 },
                    { name: 'AgentGuid', mapping: 9 },
                    { name: 'Hostname', mapping: 10 },
                    { name: 'DNSName', mapping: 11 },
                    { name: 'IP', mapping: 12 },
                    { name: 'AgentVersion', mapping: 13 },
                    { name: 'ForceUpdateSupported', mapping: 14 },
                    { name: 'RedeploySupported', mapping: 15 },
                    { name: 'AutoUpdateEnabled', mapping: 16 },
                    { name: 'MatchingEngineId', mapping: 17 },
                    { name: 'IpOrHostname', mapping: 18 },
                    { name: 'NodeID', mapping: 19 },
                    { name: 'NodeIcon', mapping: 20 },
                    { name: 'NodeCaption', mapping: 21 },
                    { name: 'NodeObjectSubType', mapping: 22 },
                    { name: 'IsRemoteCollector', mapping: 24 },
                    { name: 'OSVersion', mapping: 25 },
                    { name: 'PollingCompletion', mapping: 26 },
                    { name: 'ElementCount', mapping: 27 },
                    { name: 'NodeCount', mapping: 28 },
                    { name: 'InterfaceCount', mapping: 29 },
                    { name: 'VolumeCount', mapping: 30 },
                    { name: 'PollingCapacity', mapping: 31 },
                    { name: 'OperatingSystem', mapping: 32 },
                    { name: 'RegisteredOn', mapping: 33 }
                ],
                "Name");
        }

        dataStore.addListener("exception", function(dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: "Error",
                msg: error.Message,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        dataStore.addListener("add", function(store, records, options) {
            FilterNotificationItems();
        });

        dataStore.addListener("load", function(store, records, options) {
            if (isEditGrid && !activeAgentsCallInProgress) {
                // add notification record for active agents
                if (getCookie(cookiePrefix + 'HideActiveAgentsNotification') != 1) {
                    activeAgentsCallInProgress = true; // allow only one service call at time
                    CallGetActiveAgentsWaitingForAdd(function(agents) {
                        if (agents.length > 0) {
                            NotificationRecord = Ext.data.Record.create([
                                { name: "ID", type: "string" },
                                { name: "Name", type: "string" }
                            ]);
                            var record = new NotificationRecord({
                                ID: NotificationRecordId,
                                Name: agents.length
                            });

                            store.insert(0, record);
                        }

                        activeAgentsCallInProgress = false;
                    });
                }
            }
            if (isEditGrid) {
                // check if there is at least one record with agent which can be force-updated or force-redeployed, if it is then show appropriate button(s)
                forceUpdateAllowed = false;
                forceRedeployAllowed = false;
                Ext.each(records, function(record) {
                    if (record.data.ForceUpdateSupported) {
                        forceUpdateAllowed = true;
                    }
                    if (record.data.RedeploySupported) {
                        forceRedeployAllowed = true;
                    }
                    if (forceUpdateAllowed && forceRedeployAllowed) {
                        return false; // break;
                    }
                });

                UpdateToolbarButtons();
            }
        });

        agentsNotificationPanel = new Ext.Panel({
            html: '',
            border: false,
            bodyCssClass: 'AgentManagement_ActiveAgentsNotificationLabel'
        });
        searchField = new Ext.ux.form.SearchField({
            store: dataStore,
            width: 200,
            emptyText: (isManageRemoteCollectorsPage) ? searchAllRemoteCollectorsDefaultText : searchAllAgentsDefaultText,
            paramName: 'search'
        });

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 1000,
            value: GetPageSize()
        });

        pageSizeBox.on('change', function(f, numbox, o) {
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                Ext.Msg.show({
                    title: errorTitle,
                    msg: invalidPageSizeText,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });

                return;
            }

            if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                pagingToolbar.pageSize = pSize;
                setCookie(cookiePrefix + 'PageSize', pSize, 'months', 1);
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }

        });

        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: GetPageSize(),
            displayInfo: true,
            displayMsg: (isManageRemoteCollectorsPage) ? displayingRemoteCollectorsText : displayingAgentsText,
            beforePageText: beforePageTextGridPaging,
            afterPageText: afterPageTextGridPaging,
            emptyMsg: (isManageRemoteCollectorsPage) ? noRemoteCollectorsToDisplayText : noAgentsToDisplayText,
            items: [
                '-',
                perPageLabel,
                pageSizeBox
            ]
        });

        var addressColumnHidden = getCookie(cookiePrefix + 'IpOrHostname');
        var connectionStatusColumnHidden = getCookie(cookiePrefix + 'ConnectionStatus');
        var agentStatusColumnHidden = getCookie(cookiePrefix + 'AgentStatus');
        var pollerColumnHidden = getCookie(cookiePrefix + 'PollingEngineName');
        var modeColumnHidden = getCookie(cookiePrefix + 'Mode');
        var guidColumnHidden = getCookie(cookiePrefix + 'AgentGuid');
        var hostnameColumnHidden = getCookie(cookiePrefix + 'Hostname');
        var dnsnameColumnHidden = getCookie(cookiePrefix + 'DNSName');
        var ipColumnHidden = getCookie(cookiePrefix + 'IP');
        var versionColumnHidden = getCookie(cookiePrefix + 'AgentVersion');
        var autoUpdateColumnHidden = getCookie(cookiePrefix + 'AutoUpdateEnabled');
        //var lastContactedColumnHidden = getCookie(cookiePrefix + 'ConnectionStatusTimestamp');
        var registeredColumnHidden = getCookie(cookiePrefix + 'RegisteredOn');
        var isRemoteCollectorColumnHidden = getCookie(cookiePrefix + 'IsRemoteCollector');

        var tableColumns;
        if (isManageAgentsPage) {
            tableColumns = [
                selectorModel,
                { header: idGridColumnHeader, width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                { header: agentNodeGridColumnHeader, width: 200, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                { header: ipAddressOrHostnameGridColumnHeader, hidden: (addressColumnHidden == 'H') || !addressColumnHidden, width: 200, sortable: true, dataIndex: 'IpOrHostname', renderer: renderDefault },
                { header: ipAddressGridColumnHeader, hidden: (ipColumnHidden == 'H') || !ipColumnHidden, width: 180, sortable: true, dataIndex: 'IP', renderer: renderDefault },
                { header: hostnameGridColumnHeader, hidden: (hostnameColumnHidden == 'H') || !hostnameColumnHidden, width: 200, sortable: true, dataIndex: 'Hostname', renderer: renderDefault },
                { header: dnsNameGridColumnHeader, hidden: (dnsnameColumnHidden == 'H') || !dnsnameColumnHidden, width: 200, sortable: true, dataIndex: 'DNSName', renderer: renderDefault },
                { header: agentStatusGridColumnHeader, hidden: agentStatusColumnHidden == 'H', width: 220, sortable: true, dataIndex: 'AgentStatus', renderer: renderAgentStatus },
                { header: connectionStatusGridColumnHeader, hidden: connectionStatusColumnHidden == 'H', width: 300, sortable: true, dataIndex: 'ConnectionStatus', renderer: renderConnectionStatus },
                { header: isRemoteCollectorGridColumnHeader, hidden: isRemoteCollectorColumnHidden == 'H', width: 220, sortable: true, dataIndex: 'IsRemoteCollector', renderer: renderIsRemoteCollector },
                { header: registeredOnGridColumnHeader, hidden: registeredColumnHidden == 'H', width: 180, sortable: true, dataIndex: 'RegisteredOn', renderer: renderUtcDateTime },
                { header: pollingEngineGridColumnHeader, hidden: (pollerColumnHidden == 'H') || !pollerColumnHidden, width: 150, sortable: true, dataIndex: 'PollingEngineName', renderer: renderDefault },
                { header: modeGridColumnHeader, hidden: (modeColumnHidden == 'H'), width: 200, sortable: true, dataIndex: 'Mode', renderer: renderMode },
                { header: versionGridColumnHeader, hidden: (versionColumnHidden == 'H'), width: 90, sortable: true, dataIndex: 'AgentVersion', renderer: renderDefault },
                { header: guidGridColumnHeader, hidden: (guidColumnHidden == 'H') || !guidColumnHidden, width: 250, sortable: true, dataIndex: 'AgentGuid', renderer: renderDefault },
                { header: '@{R=AgentManagement.Strings;K=AutoUpdateEnabledAgentGridColumnHeader;E=js}', hidden: (autoUpdateColumnHidden == 'H') || !autoUpdateColumnHidden, width: 180, sortable: true, dataIndex: 'AutoUpdateEnabled', renderer: renderBoolean },
                { id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 }
            ];
        }
        if (isManageRemoteCollectorsPage) {
            var operatingSystemColumnHidden = getCookie(cookiePrefix + 'OperatingSystem');
            var osVersionColumnHidden = getCookie(cookiePrefix + 'OSVersion');
            var pollingCompletionColumnHidden = getCookie(cookiePrefix + 'PollingCompletion');
            var elementCountColumnHidden = getCookie(cookiePrefix + 'ElementCount');
            var nodeCountColumnHidden = getCookie(cookiePrefix + 'NodeCount');
            var interfaceCountColumnHidden = getCookie(cookiePrefix + 'InterfaceCount');
            var volumeCountColumnHidden = getCookie(cookiePrefix + 'VolumeCount');

            tableColumns = [
                selectorModel,
                { header: idGridColumnHeader, width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                { header: remoteCollectorGridColumnHeader, width: 200, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                { header: ipAddressOrHostnameGridColumnHeader, hidden: (addressColumnHidden == 'H') || !addressColumnHidden, width: 200, sortable: true, dataIndex: 'IpOrHostname', renderer: renderDefault },
                { header: ipAddressGridColumnHeader, hidden: (ipColumnHidden == 'H') || !ipColumnHidden, width: 180, sortable: true, dataIndex: 'IP', renderer: renderDefault },
                { header: hostnameGridColumnHeader, hidden: (hostnameColumnHidden == 'H') || !hostnameColumnHidden, width: 200, sortable: true, dataIndex: 'Hostname', renderer: renderDefault },
                { header: dnsNameGridColumnHeader, hidden: (dnsnameColumnHidden == 'H') || !dnsnameColumnHidden, width: 200, sortable: true, dataIndex: 'DNSName', renderer: renderDefault },
                { header: agentStatusGridColumnHeader, hidden: (agentStatusColumnHidden == 'H'), width: 220, sortable: true, dataIndex: 'AgentStatus', renderer: renderAgentStatus },
                { header: connectionStatusGridColumnHeader, hidden: (connectionStatusColumnHidden == 'H'), width: 220, sortable: true, dataIndex: 'ConnectionStatus', renderer: renderConnectionStatus },
                { header: masterEngineGridColumnHeader, hidden: (pollerColumnHidden == 'H'), width: 150, sortable: true, dataIndex: 'PollingEngineName', renderer: renderDefault },
                { header: agentModeGridColumnHeader, hidden: (modeColumnHidden == 'H'), width: 200, sortable: true, dataIndex: 'Mode', renderer: renderMode },
                { header: agentSoftwareVersionGridColumnHeader, hidden: (versionColumnHidden == 'H'), width: 90, sortable: true, dataIndex: 'AgentVersion', renderer: renderDefault },
                { header: pollingCompletionColumnHeader, hidden: (pollingCompletionColumnHidden == 'H'), width: 120, sortable: true, dataIndex: 'PollingCompletion', renderer: renderPollingCompletion },
                { header: pollingCapacityColumnHeader, hidden: (elementCountColumnHidden == 'H'), width: 120, sortable: true, dataIndex: 'PollingCapacity', renderer: renderPollingCapacity},
                { header: elementCountColumnHeader, hidden: (elementCountColumnHidden == 'H'), width: 70, sortable: true, dataIndex: 'ElementCount', renderer: renderDefault },
                { header: nodeCountColumnHeader, hidden: (nodeCountColumnHidden == 'H' || !nodeCountColumnHidden), width: 70, sortable: true, dataIndex: 'NodeCount', renderer: renderDefault },
                { header: interfaceCountColumnHeader, hidden: (interfaceCountColumnHidden == 'H' || !interfaceCountColumnHidden), width: 70, sortable: true, dataIndex: 'InterfaceCount', renderer: renderDefault },
                { header: volumeCountColumnHeader, hidden: (volumeCountColumnHidden == 'H' || !volumeCountColumnHidden), width: 70, sortable: true, dataIndex: 'VolumeCount', renderer: renderDefault, tooltip: 'abc' },
                { header: operatingSystemColumnHeader, hidden: (operatingSystemColumnHidden == 'H'), width: 170, sortable: true, dataIndex: 'OperatingSystem', renderer: renderDefault },
                { header: osVersionGridColumnHeader, hidden: (osVersionColumnHidden == 'H' || !osVersionColumnHidden), width: 170, sortable: true, dataIndex: 'OSVersion', renderer: renderDefault },
                { header: guidGridColumnHeader, hidden: (guidColumnHidden == 'H') || !guidColumnHidden, width: 250, sortable: true, dataIndex: 'AgentGuid', renderer: renderDefault },
                { id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 }
            ];
        }

        var toolbarSeparator = {
            xtype: 'tbseparator'
        };

        var editAgentsGridSeparator = {
            hidden: !(isEditGrid && isManageAgentsPage),
            xtype: 'tbseparator'
        };

        var menuSeparator = {
            xtype: 'menuseparator'
        };
        
        grid = new Ext.grid.GridPanel({
            store: dataStore,

            columns: tableColumns,

            sm: selectorModel,

            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            autoExpandColumn: 'filler',
            viewConfig: {
                emptyText: (isManageRemoteCollectorsPage) ? thereAreNoRemoteCollectorsAvailableText : thereAreNoAgentsAvailableText,
                columnsText: columnsSelectionText,
                sortAscText: sortAscendingText,
                sortDescText: sortDescendingText,
                getRowClass: function(record, index) {
                    if (record.data.ID == -1)
                        return "AgentManagement_NotificationGridRow";
                },
                markDirty: false
            },

            tbar: [
                {
                    id: 'AddButton',
                    text: addAgentButtonText,
                    iconCls: 'AgentManagement_AddAgentButton',
                    hidden: isManageRemoteCollectorsPage,
                    handler: function() {
                        AddAgent();
                    }
                },
                editAgentsGridSeparator,
                {
                    id: 'EditButton',
                    text: editAgentButtonText,
                    iconCls: 'AgentManagement_EditAgentButton',
                    hidden: false,
                    handler: function() {
                        if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();
                        EditAgent(grid.getSelectionModel().getSelected());
                    }
                },
                toolbarSeparator,
                {
                    id: 'DeleteButton',
                    text: deleteAgentButtonText,
                    iconCls: 'AgentManagement_DeleteAgentButton',
                    hidden: false,
                    handler: function () {
                        if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

                        var ids = [];
                        Ext.each(grid.getSelectionModel().getSelections(), function (item) {
                            ids.push(item.data.ID);
                        });

                        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                            "GetAllRemoteCollectorAgentIdsFromSelected", { agentIds: ids },
                            function (result) {
                                if (result.length > 0) {
                                    Ext.Msg.show({
                                        cls: 'AgentManagement_ErrorDialogDeleteRemoteCollector',
                                        title: '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteRemoteCollector_Title;E=js}',
                                        msg: '@{R=AgentManagement.Strings;K=AgentsGrid_DeleteRemoteCollector_Message;E=js}',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.ERROR,
                                        fn: function() {
                                        }
                                    });
                                } else {
                                    deleteAndUninstallAgentWindow.show();
                                }
                            });
                    }
                },
                editAgentsGridSeparator,
                {
                    id: 'ChooseResourcesButton',
                    text: chooseResourcesButtonText,
                    iconCls: 'AgentManagement_ChooseResourcesButton',
                    disabled: true,
                    hidden: isManageRemoteCollectorsPage,
                    handler: function () {
                        if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();
                        ChooseResources();
                    }
                },
                toolbarSeparator,
                {
                    id: 'ManageAsNodeButton',
                    text: manageAsNodeButtonText,
                    iconCls: 'AgentManagement_ManageAsNodeButton',
                    disabled: true,
                    hidden: false,
                    handler: function () {
                        var item = grid.getSelectionModel().getSelected();
                        var targetUrl;
                        if (item.data.NodeID > 0) {
                            // agent associated with node managed by other method than Agent
                            targetUrl = String.format('/Orion/Nodes/NodeProperties.aspx?Nodes={0}&ReturnTo={1}', item.data.NodeID, returnUrl);
                        } else {
                            targetUrl = String.format('/Orion/AgentManagement/Admin/ManageAgents.aspx?manageAsNodeClick=1&agentId={0}', item.data.ID);
                        }
                        window.location = targetUrl;
                    }
                },
                toolbarSeparator, 
                {
                    id: 'MoreActionsDropdownButton',
                    text: moreActionsText,
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                id: 'ViewInstalledAgentPluginsButton',
                                text: viewInstalledAgentPluginsButtonText,
                                icon: viewInstalledAgentPluginsIcon,
                                hidden: false,
                                handler: function () {
                                    installedAgentPluginsWindow.show();
                                }
                            },
                            {
                                id: 'ViewInstalledModulesReportButton',
                                text: viewInstalledModulesReportButtonText,
                                icon: viewInstalledModulesReportIcon,
                                hidden: false,
                                handler: function() {
                                    window.location = reportUrl;
                                }
                            },
                            menuSeparator,
                            {
                                id: 'PromoteAgentToRemoteCollector',
                                text: promoteButtonText,
                                tooltip: null,
                                icon: promoteAgentToOrionRemoteCollectorIcon,
                                hidden: isManageRemoteCollectorsPage || !isOrcFeatureEnabled,
                                disabled: true,
                                handler: function () {
                                    if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();
                                    promoteAgentWindow.show();
                                }
                            },
                            {
                                id: 'DeployAgentButton',
                                text: retryAgentInstallationButtonText,
                                icon: retryAgentInstallationIcon,
                                hidden: true,
                                handler: function () {
                                    RetryAgentDeployment();
                                }
                            },
                            {
                                id: 'RebootAgentButton',
                                text: rebootButtonText,
                                tooltip: rebootButtonTooltip,
                                icon: rebootIcon,
                                hidden: false,
                                disabled: true,
                                handler: function () {
                                    if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

                                    var rebootingOrionServer = null;
                                    // check if we are going to reboot some Orion server
                                    Ext.each(grid.getSelectionModel().getSelections(), function (item) {
                                        if (item.data.MatchingEngineId > 0) {
                                            rebootingOrionServer = item;
                                        }
                                    });

                                    var multiSelect = grid.getSelectionModel().getCount() > 1;

                                    // Some agent is installed on some Orion Engine. Show extra warning.
                                    Ext.Msg.show({
                                        title: rebootPromptTitle,
                                        msg: multiSelect ? rebootMsgPlural : rebootMsgSingular,
                                        buttons: Ext.Msg.YESNO,
                                        icon: Ext.Msg.WARNING,
                                        fn: function (btn, text) {
                                            if (btn == 'yes') {
                                                if (rebootingOrionServer != null) {
                                                    // Some agent is installed on some Orion Engine. Show extra warning.
                                                    Ext.Msg.show({
                                                        title: rebootOrionEnginePromptTitle,
                                                        msg: String.format(rebootOrionEnginePromptFormat, rebootingOrionServer.data.Hostname, rebootingOrionServer.data.IP),
                                                        buttons: Ext.Msg.YESNO,
                                                        icon: Ext.Msg.WARNING,
                                                        fn: function (btn, text) {
                                                            if (btn == 'yes') {
                                                                RebootSelectedAgents(grid.getSelectionModel().getSelections());
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    RebootSelectedAgents(grid.getSelectionModel().getSelections());
                                                }
                                            }
                                        }
                                    });
                                }
                            },
                            {
                                id: 'UpdateAgentButton',
                                text: updateButtonText,
                                tooltip: updateButtonTooltip,
                                icon: updateIcon,
                                hidden: false,
                                disabled: true,
                                handler: function () {
                                    if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

                                    var multiSelect = grid.getSelectionModel().getCount() > 1;

                                    var dlg = Ext.Msg.show({
                                        title: updatePromptTitle,
                                        msg: multiSelect ? updateMsgPlural : updateMsgSingular,
                                        buttons: Ext.Msg.YESNO,
                                        icon: Ext.Msg.WARNING,
                                        fn: function (btn, text) {
                                            if (btn == 'yes') {
                                                UpdateSelectedAgents(grid.getSelectionModel().getSelections());
                                            }
                                        }
                                    }).getDialog();
                                }
                            },
                            {
                                id: 'ForceUpdateAgentButton',
                                text: forceUpdateButtonText,
                                tooltip: forceUpdateButtonTooltip,
                                icon: updateIcon,
                                hidden: true,
                                handler: function () {
                                    if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

                                    var multiSelect = grid.getSelectionModel().getCount() > 1;

                                    var dlg = Ext.Msg.show({
                                        title: forceUpdatePromptTitle,
                                        msg: multiSelect ? forceUpdateMsgPlural : forceUpdateMsgSingular,
                                        buttons: Ext.Msg.YESNO,
                                        icon: Ext.Msg.WARNING,
                                        fn: function (btn, text) {
                                            if (btn == 'yes') {
                                                ForceUpdateSelectedItems(grid.getSelectionModel().getSelections());
                                            }
                                        }
                                    }).getDialog();
                                }
                            },
                            menuSeparator,
                            {
                                id: 'ReconnectAgentButton',
                                text: reconnectAgentButtonText,
                                tooltip: reconnectAgentButtonTooltip,
                                icon: reconnectAgentIcon,
                                hidden: false,
                                disabled: true,
                                handler: function () {
                                    if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

                                    var items = grid.getSelectionModel().getSelected();
                                    var agentIds = [];
                                    Ext.each(items, function (item) {
                                        agentIds.push(item.data.ID);
                                    });

                                    var waitMsg = Ext.Msg.wait("Reconnecting...");

                                    CallReconnectAgents(agentIds, function () {
                                        waitMsg.hide();
                                        RefreshObjects();
                                    });
                                }
                            },
                            {
                                id: 'RestartAgentButton',
                                text: restartButtonText,
                                tooltip: restartButtonTooltip,
                                icon: restartIcon,
                                hidden: false,
                                disabled: true,
                                handler: function () {
                                    if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

                                    var multiSelect = grid.getSelectionModel().getCount() > 1;

                                    // Some agent is installed on some Orion Engine. Show extra warning.
                                    Ext.Msg.show({
                                        title: restartPromptTitle,
                                        msg: multiSelect ? restartMsgPlural : restartMsgSingular,
                                        buttons: Ext.Msg.YESNO,
                                        icon: Ext.Msg.WARNING,
                                        fn: function (btn, text) {
                                            if (btn == 'yes') {
                                                RestartSelectedAgents(grid.getSelectionModel().getSelections());
                                            }
                                        }
                                    });
                                }
                            }                            
                        ]
                    }
                },
                '->',
                agentsNotificationPanel,
                searchField
            ],

            bbar: pagingToolbar
        });

        grid.getColumnModel().on('hiddenchange', function(cm, index, hidden) {
            setCookie(cookiePrefix + '' + cm.getDataIndex(index), hidden ? 'H' : 'V', 'months', 1);
        });

        InitForceDeployWindow();

        task = {
            run: function() {
                UpdateAgentsStatuses();
            },
            interval: 30000 // 30 seconds update interval
        };
        runner = new Ext.util.TaskRunner();
        runner.start(task);
    };

    ORION.prefix = "AgentManagement_AgentsGrid_";

    return {
        SetPageSize: function (size) {
            pageSize = size;
        },
        SetEditGrid: function (editGrid) {
            isEditGrid = editGrid;
        },
        SetGroupBy: function (value) {
            groupBy = value;
        },
        SetGroupByItem: function (value) {
            groupByItem = value;
        },
        SetUserName: function (user) {
            if (user) {
                userName = user;
                updateCookiePrefix();
            }
        },
        SetSelectedAgentsFieldClientID: function (id) {
            selectedAgentsFieldClientID = id;
        },
        SetExcludedAgents: function (idsString) {
            var ids = idsString.split(',');
            excludedAgents.length = 0;
            if (ids.length > 0 && ids[0]) {
                Ext.each(ids, function (agentId) {
                    excludedAgents.push(parseInt(agentId));
                });
            }
        },
        SetUnsupportedAgents: function (idsString) {
            var ids = idsString.split(',');
            unsupportedAgents.length = 0;
            if (ids.length > 0 && ids[0]) {
                Ext.each(ids, function (agentId) {
                    unsupportedAgents.push(parseInt(agentId));
                    excludedAgents.push(parseInt(agentId));
                });
            }
        },
        HideNotificationRow: function () {
            setCookie(cookiePrefix + 'HideActiveAgentsNotification', 1, 'months', 1);
            FilterNotificationItems();
        },
        ShowNotificationRow: function () {
            setCookie(cookiePrefix + 'HideActiveAgentsNotification', 0, 'months', 1);
            FilterNotificationItems();
        },
        SetReturnUrl: function (url) {
            returnUrl = url;
        },
        SetAgentLoadWarningThreshold: function (threshold) {
            loadWarningThreshold = threshold;
        },
        SetAgentLoadErrorThreshold: function (threshold) {
            loadErrorThreshold = threshold;
        },
        SetReportUrl: function(url) {
            reportUrl = url;
        },
        SetIsOrcFeatureEnabled: function (isFeatureEnabled) {
            isOrcFeatureEnabled = isFeatureEnabled;
        },
        SetHowLongIsAgentDisplayedAsNewInHours: function (hours) {
            howLongIsAgentDisplayedAsNewInMiliseconds = hours * 60 * 60 * 1000;
        },
        SetFailedPluginStatuses: function (statuses) {
            failedPluginStatuses = statuses;
        },
        ShowPluginDetails: ShowPluginDetails,
        RedeployPlugin: RedeployPlugin,
        RetryAgentDeploymentForAgent: RetryAgentDeploymentForAgent,
        ShowMoreDetailsMessage: ShowMoreDetailsMessage,
        SetCredentialsList: function (list) {
            credentialsList = list;
        },
        SetSshPrivateKeyCredentialsList: function (list) {
            sshPrivateKeyCredentialsList = list;
        },
        ShowPrivateKeyDialog: function () {
            ShowPrivateKeyDialog();
        },
        ResetPrivateKey: function () {
            ResetPrivateKey();
        },
        init: function () {
            
            if (initialized)
                return;

            initialized = true;

            InitMode();
            InitGroupingPanel();
            InitGrid();
            InitLayout();

            if (groupBy && groupByItem) {
                groupByCombo.setValue(groupBy);
                RefreshGroupByList(groupBy, function () {
                    groupByList.select(groupByItem);
                });
            }

            RefreshObjects();

            UpdateToolbarButtons();        
        }
    };
} ();

Ext.onReady(SW.AgentManagement.AgentsGrid.init, SW.AgentManagement.AgentsGrid);
