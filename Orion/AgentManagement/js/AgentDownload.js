﻿Ext.namespace('SW');
Ext.namespace('SW.AgentManagement');
Ext.namespace('SW.AgentManagement.Download');

SW.AgentManagement.Download.connectionDetailsErrorTitle = '';
SW.AgentManagement.Download.connectionDetailsErrorText = '';
SW.AgentManagement.Download.engineDropdownId = '';
SW.AgentManagement.Download.downloadActiveAgentRadioId = '';
SW.AgentManagement.Download.downloadPassiveAgentRadioId = '';
SW.AgentManagement.Download.advancedSettingsCheckboxId = '';
SW.AgentManagement.Download.useProxyAuthenticationCheckboxId = '';
SW.AgentManagement.Download.massDeploymentRadioId = '';
SW.AgentManagement.Download.goldMasterImageRadioId = '';
SW.AgentManagement.Download.manualProxySettingErrorTitle = '';
SW.AgentManagement.Download.manualProxySettingErrorText = '';

SW.AgentManagement.Download.ValidateIP = function (ipAddress, onSuccess) {
    SW.Core.Services.callWebServiceSync("/Orion/AgentManagement/Services/ValidationHelper.asmx",
        "ValidateIPAddress", { ipaddr: ipAddress },
        function (result) {
            if (!result) {
                Ext.MessageBox.show({
                    title: SW.AgentManagement.Download.invalidIPAddressErrorTitle,
                    msg: SW.AgentManagement.Download.invalidIPAddressErrorText,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            } else {
                onSuccess();
            }
        });
};

SW.AgentManagement.Download.IsNumeric = function(input) {
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
}

SW.AgentManagement.Download.ValidatePort = function (port) {
    if (!SW.AgentManagement.Download.IsNumeric(port))
    {
        Ext.MessageBox.show({
            title: SW.AgentManagement.Download.invalidPortErrorTitle,
            msg: SW.AgentManagement.Download.portWrongFormatError,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR
        });
        return false;
    }
    var i_port = parseInt(port);
    if (i_port < 1 || i_port > 65535)
    {
        Ext.MessageBox.show({
            title: SW.AgentManagement.Download.invalidPortErrorTitle,
            msg: SW.AgentManagement.Download.portRangeError,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR
        });
        return false;
    }
    return true;
}

SW.AgentManagement.Download.ShowHidePollingEngineSet = function () {
    var is_active_agent = $('#' + SW.AgentManagement.Download.downloadActiveAgentRadioId).is(':checked');
    if (is_active_agent) {
        $('#selectPollingEngineSet').show();
        $('#specifyAgentCommunicationPort').hide();
    } else {
        $('#selectPollingEngineSet').hide();
        $('#specifyAgentCommunicationPort').show();
    }
}

SW.AgentManagement.Download.ShowHideMassDeploymentSection = function () {
    var is_mass_deployment = $('#' + SW.AgentManagement.Download.massDeploymentRadioId).is(':checked');
    if (is_mass_deployment) {
        $('#massDeploymentDownloadSection').show();
        $('#goldMasterImageDownloadSection').hide();
    } else {
        $('#massDeploymentDownloadSection').hide();
        $('#goldMasterImageDownloadSection').show();
    }
}

SW.AgentManagement.Download.ShowCorrectDeploymentSection = function () {
    if ($('#connectionDetailsTypeSelect').val() == 1) {
        $('#pollingEngineSelectionTable').hide();
        $('#manualSelectionTable').show();
    } else {
        $('#manualSelectionTable').hide();
        $('#pollingEngineSelectionTable').show();
    }
};

SW.AgentManagement.Download.ShowCorrectProxyAccessType = function () {
    if ($('#proxyAccessTypeTypeSelect').val() == 0) {	    //use_default
        $('#AgentManagement_ProxyHostnamePortSettings').hide();
        $('#AgentManagement_ProxyAuthenticationSettingsGroup').show();
    }
    else if ($('#proxyAccessTypeTypeSelect').val() == 1) {  //auto-discovery
        $('#AgentManagement_ProxyHostnamePortSettings').hide();
        $('#AgentManagement_ProxyAuthenticationSettingsGroup').hide();
    }
    else if ($('#proxyAccessTypeTypeSelect').val() == 2) {  //disabled
        $('#AgentManagement_ProxyHostnamePortSettings').hide();
        $('#AgentManagement_ProxyAuthenticationSettingsGroup').hide();
    }
    else if ($('#proxyAccessTypeTypeSelect').val() == 3) {  //user_defined
        $('#AgentManagement_ProxyHostnamePortSettings').show();
        $('#AgentManagement_ProxyAuthenticationSettingsGroup').show();
    }
};

SW.AgentManagement.Download.DownloadMST = function () {
    SW.AgentManagement.Download.HandleDownloadClick('/Orion/AgentManagement/Admin/MstGenerator.ashx');
};

SW.AgentManagement.Download.DownloadOfflineInstaller = function () {
    SW.AgentManagement.Download.HandleDownloadClick('/Orion/AgentManagement/Admin/OfflineInstaller.ashx');
};

SW.AgentManagement.Download.HandleDownloadClick = function (relativeUri) {
    if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

    var is_active_agent = $('#' + SW.AgentManagement.Download.downloadActiveAgentRadioId).is(':checked');

    var proxyAccessType = $('#proxyAccessTypeTypeSelect').val();
    var proxyHostname = $('#deploymentProxyHostname').val();
    var proxyPort = $('#deploymentProxyPort').val();

    if ((proxyAccessType == "3" /*user_defined*/) && (proxyHostname == '' || proxyPort == '')) {
    	Ext.MessageBox.show({
    		title: SW.AgentManagement.Download.manualProxySettingErrorTitle,
    		msg: SW.AgentManagement.Download.manualProxySettingErrorText,
    		buttons: Ext.MessageBox.OK,
    		icon: Ext.MessageBox.ERROR
    	});
    	return;
    }

    var proxyUsername = $('#deploymentProxyUsername').val();
    var proxyPassword = $('#deploymentProxyPassword').val();

    var extraParams = '';
    if (is_active_agent) {
        extraParams = '&is_active_agent=' + is_active_agent +
                      '&proxy_access_type=' + proxyAccessType +
                      '&proxy_hostname=' + proxyHostname +
                      '&proxy_port=' + proxyPort +
                      '&proxy_username=' + proxyUsername +
                      '&proxy_password=' + proxyPassword;
    } else {
        extraParams = '&is_active_agent=' + is_active_agent;
    }

    //add communication port
    if (!is_active_agent)
    {
        var agentCommunicationPort = $('#' + SW.AgentManagement.Download.agentCommunicationPortId).val();
        if(!SW.AgentManagement.Download.ValidatePort(agentCommunicationPort))
        {
            return;
        }
        extraParams += '&port=' + agentCommunicationPort;
    }

    if ($('#connectionDetailsTypeSelect').val() == 1) {
        var hostname = $('#hostnameTextBox').val();
        var ipAddress = $('#ipTextBox').val();

        if (hostname == '' || ipAddress == '') {
            Ext.MessageBox.show({
                title: SW.AgentManagement.Download.connectionDetailsErrorTile,
                msg: SW.AgentManagement.Download.connectionDetailsErrorText,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
            return;
        }

        SW.AgentManagement.Download.ValidateIP(ipAddress, function () {
            window.location = relativeUri + '?ip=' + encodeURIComponent(ipAddress)
                + '&hostname=' + encodeURIComponent(hostname)
                + extraParams;
        });
    } else {
        var pollingEngineId = $('#' + SW.AgentManagement.Download.engineDropdownId).val();
        window.location = relativeUri + '?pollingEngineId=' + pollingEngineId
                + extraParams;
    }
};

SW.AgentManagement.Download.RefreshEngineIP = function () {
    var selectedEngineOption = $('#' + SW.AgentManagement.Download.engineDropdownId).find(":selected");
    $('#engineIPAddress').text(selectedEngineOption.attr('ipAddress'));
};

SW.AgentManagement.Download.InitExpandCollapse = function () {
    $('.AgentManagement_ExpandCollapse').each(function () {
        $(this).find('.AgentManagement_ExpandCollapseHeader').click(function () {
            var content = $(this).parent().find('.AgentManagement_ExpandCollapseContent');
            content.toggle();

            if (content.is(':visible')) {
                $(this).find('.AgentManagement_ExpandButton').html('&ndash;');
            } else {
                $(this).find('.AgentManagement_ExpandButton').text('+');
            }
        });
    });
};

SW.AgentManagement.Download.RefreshAdvancedSettings = function () {
    var isChecked = $('#'+SW.AgentManagement.Download.advancedSettingsCheckboxId).is(':checked');

    if (isChecked) {
        $('#AgentManagement_AdvancedDeploymentSettings').show();
        $('#CollapseDeploymentImg').attr('src', '/Orion/Images/Button.Collapse.gif');
    }
    else {
        $('#AgentManagement_AdvancedDeploymentSettings').hide();
        $('#CollapseDeploymentImg').attr('src', '/Orion/Images/Button.Expand.gif');
    }
}

SW.AgentManagement.Download.RefreshProxySettings = function () {
    var isChecked = $('#'+SW.AgentManagement.Download.useProxyAuthenticationCheckboxId).is(':checked');

    if (isChecked) {
        $('#AgentManagement_ProxyAuthenticationSettings').show();
    }
    else {
        $('#AgentManagement_ProxyAuthenticationSettings').hide();
    }    
}

$(document).ready(function () {
    $('#' + SW.AgentManagement.Download.engineDropdownId).change(function () {
        SW.AgentManagement.Download.RefreshEngineIP();
    });

    $('#' + SW.AgentManagement.Download.downloadActiveAgentRadioId).change(function () {
        SW.AgentManagement.Download.ShowHidePollingEngineSet();
    });

    $('#' + SW.AgentManagement.Download.downloadPassiveAgentRadioId).change(function () {
        SW.AgentManagement.Download.ShowHidePollingEngineSet();
    });

    $('#' + SW.AgentManagement.Download.advancedSettingsCheckboxId).click(function (e) {
        e.stopPropagation();
    });

    $('#AgentManagement_ToggleAdvancedDeploymentDiv').click(function () {
        $('#'+SW.AgentManagement.Download.advancedSettingsCheckboxId).click();
        SW.AgentManagement.Download.RefreshAdvancedSettings();
    });

    $('#' + SW.AgentManagement.Download.useProxyAuthenticationCheckboxId).click(function () {
        SW.AgentManagement.Download.RefreshProxySettings();
    });

    $('#' + SW.AgentManagement.Download.massDeploymentRadioId).change(function () {
        SW.AgentManagement.Download.ShowHideMassDeploymentSection();
    });

    $('#' + SW.AgentManagement.Download.goldMasterImageRadioId).change(function () {
        SW.AgentManagement.Download.ShowHideMassDeploymentSection();
    });

    SW.AgentManagement.Download.RefreshEngineIP();
    SW.AgentManagement.Download.InitExpandCollapse();
});
