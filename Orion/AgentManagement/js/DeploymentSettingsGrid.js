﻿Ext.namespace('SW');
Ext.namespace('SW.AgentManagement');

SW.AgentManagement.DeploymentSettingsGrid = function () {
    var errorTitle = '@{R=AgentManagement.Strings;K=ErrorTitle;E=js}';
    var notManagedByOrionText = '@{R=AgentManagement.Strings;K=NotManagedByOrion;E=js}';
    var assignCredentialsButtonText = '@{R=AgentManagement.Strings;K=AssignCredentialsButtonText;E=js}';
    var testCredentialsButtonText = '@{R=AgentManagement.Strings;K=TestCredentialsButtonText;E=js}';
    var removeAgentButtonText = '@{R=AgentManagement.Strings;K=DeleteAgentButtonText;E=js}';
    var removingAgentsText = '@{R=AgentManagement.Strings;K=DeletingAgentText;E=js}';
    var removePromptSingle = '@{R=AgentManagement.Strings;K=DeleteAgentPromptSingle;E=js}';
    var removePromptMultiple = '@{R=AgentManagement.Strings;K=DeleteAgentsPromptMultiple;E=js}';
    var assigningCredentialsText = '@{R=AgentManagement.Strings;K=AssigningCredentialsText;E=js}';
    var unassignedText = '@{R=AgentManagement.Strings;K=Unassigned;E=js}';
    var testingCredentialsText = '@{R=AgentManagement.Strings;K=TestingCredentials;E=js}';
    var notTestedText = '@{R=AgentManagement.Strings;K=NotTested;E=js}';
    var testSuccessfulText = '@{R=AgentManagement.Strings;K=CredentialTestSuccessful;E=js}';
    var testFailedText = '@{R=AgentManagement.Strings;K=CredentialTestFailed;E=js}';
    var testFailedHelpLinkText = '@{R=AgentManagement.Strings;K=CredentialTestFailedHelpLinkText;E=js}';
    var testFailedHelpKB = "http://www.solarwinds.com/documentation/kbloader.aspx?kb=5574&lang=@{R=Core.Strings;K=CurrentHelpLanguage;E=js}";
    var unsupportedOsHelpLinkText = '@{R=AgentManagement.Strings;K=CredentialTestUnsupportedOsHelpLinkText;E=js}';
    var thereIsExistingAgentForDiffrentServerText = '@{R=AgentManagement.Strings;K=ThereIsExistingAgentForDiffrentServer;E=js}';
    var thereIsExistingAgentForThisServerText = '@{R=AgentManagement.Strings;K=ThereIsExistingAgentForThisServer;E=js}';
    var pastePrivateKeyManuallyTitle = '@{R=AgentManagement.Strings;K=DeploymentWizard_PastePrivateKeyManuallyTitle;E=js}';
    var pastePrivateKeyManuallyLabel = '@{R=AgentManagement.Strings;K=DeploymentWizard_PastePrivateKeyManuallyLabel;E=js}';
    var missingPrivateKeyError = '@{R=AgentManagement.Strings;K=DeploymentWizard_MissingPrivateKeyError;E=js}';
    var unsupportedOsHelpUrl = '';

    var privateKeyCredentialType = "privateKey";
    var usernameAndPasswordCredentialType = "usernameAndPassword";
    var newCredentialId = -1;

    var windowsOsType = "windows";
    var linuxOsType = "linux";

    var initialized;

    var grid;
    var selectorModel;
    var dataStore;

    var loadingMask = null;

    var assignCredentialWindow;
    var credentialsCombo;
    var credentialsList;
    var sshPrivateKeyCredentialsList;
    var selectedCredential = -1;
    var selectedSshPrivateKeyCredential = -1;
    var autodetectAgentMode = 0;
    var activeAgentMode = 1;
    var passiveAgentMode = 2;
    var installPackageCombo;

    var pollerComboBoxEditor;
    var pollerList;
    var pollerStore;

    var usernameField;
    var passwordField;
    var privateKeyPasswordField;
    var credentialNameField;

    var certificatePastedManually = false;
    var certificateData;

    var additionalCredentialsCombo;
    var additionalCredentialNameField;
    var additionalUsernameField;
    var additionalPasswordField;
    var selectedAdditionalCredential = -1;
    var useSudoCheckbox;

    var agentModeList;
    var agentModeStore;
    var agentModeComboBoxEditor;

    var installPackageStore;
    var installPackageComboBoxEditor;

    var lastDialogCredentialTestResults = [];

    var credentialsTestResultsValues = {      //keep corresponding values in sync with DeploymentHost.credentialsTestResultsValues enum
        CredentialsNotTested: 0,        // 0 indicates that credentials haven't been tested yet. ('undefined' might have the same meaning)
        Ok: 1,                          //positive values indicate successfull test
        ExistingAgentWillBeReplaced: 2,
        CredentialsInvalid: -1,         // negative values indicate error
        CredentialsMissing: -2,
        UnsupportedOS: -3,
        CredentialsTestInProgress: 99,  // special value for test in progress
    };

    var deploymentType = {
        Unknown: 0,
        RIS: 1,
        SSH: 2,
        Auto: 3
    };

    var failureReasonValues = {
        UnsupportedLinuxFailure: 13
    };

    UpdateAgentSettings = function (hostname, pollerId, mode, installPackageId, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
                         "UpdateAgentSettings", { hostName: hostname, pollerId: pollerId, agentMode: mode, installPackageId: installPackageId },
                         function (result) {
                             onSuccess();
                         });
    };

    RemoveAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
                         "RemoveAgent", { hostnames: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    AssignCredentialsCall = function (ids, credentials, credentialsTestsResults, onSuccess) {
        Ext.each(credentialsTestsResults, function (result) {
            PropagateCredentialTestDataToRecord(result, result.Item);

            UpdateAgentSettings(result.Item.data.ID,
                    result.Item.data.PollerId,
                    result.Item.data.Mode,
                    result.Item.data.InstallPackageId, function() {});
        });

        ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
                         "AssignCredentials", { hostnames: ids, credentials: credentials },
                         function (result) {
                             onSuccess(result);
                         });
    };

    AssignCredentials = function (items, credentials, credentialsTestsResults) {
        var toAssign = [];

        Ext.each(items, function (item) {
            toAssign.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(assigningCredentialsText);

        AssignCredentialsCall(toAssign, credentials, credentialsTestsResults, function (result) {
            waitMsg.hide();
            if (result.Success) {
                var allTested = true;
                Ext.each(items, function (item) {
                    item.data.CredentialId = credentials.credentialId;
                    item.data.CredentialName = credentials.credentialName;
                    item.data.CredentialType = credentials.type;
                    item.data.AdditionalCredentialId = credentials.additionalCredential != null ? credentials.additionalCredential.credentialId : 0;
                    item.data.OsType = GetSelectedOsType();
                    item.commit();
                    if (!item.data.CredentialTested) {
                        allTested = false;
                    }
                });

                if (!allTested) {
                    TestCredentials(items);
                }
            } else {
                Ext.each(items, function (item) {
                    item.data.CredentialsTestResult = credentialsTestResultsValues.CredentialsInvalid;
                    item.data.CredentialsTestMessage = result.Message;
                    item.data.DeploymentType = deploymentType.Auto;
                    item.commit();
                });
            }
        });
    };

    IsCredentialNameUniqueCall = function (credential, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
                         "IsCredentialNameUnique", { credential: credential },
                         function (result) {
                             onSuccess(result);
                         });
    };

    RemoveSelectedItems = function (items) {
        var toDelete = [];

        Ext.each(items, function (item) {
            toDelete.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(removingAgentsText);

        RemoveAgents(toDelete, function () {
            waitMsg.hide();
            grid.store.reload();
        });
    };

    TestCredentials = function (items) {
        Ext.each(items, function (item) {
            item.data.CredentialsTestResult = credentialsTestResultsValues.CredentialsTestInProgress; // test in progress
            item.commit();

            ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
                "StartTestingCredentials", { hostname: item.data.ID, credentialId: 0, credentialName: null, username: null, password: null },
                function (result) {
                    if (result.Success) {
                        item.data.CredentialsTestProgressId = result.ProgressId;
                        setTimeout(function () { GetTestCredentialsResult(item); }, 5000);
                    } else {
                        item.data.CredentialsTestResult = credentialsTestResultsValues.CredentialsInvalid;
                        item.data.CredentialsTestMessage = result.Message;
                        item.data.DeploymentType = result.DeploymentType;
                        item.commit();
                    }
                });
        });
    };

    GetTestCredentialsResult = function (item) {
        ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
            "GetTestCredentialsResult", { hostname: item.data.ID, progressId: item.data.CredentialsTestProgressId },
            function (result) {
                if (result == null || result.InProgress) {
                    setTimeout(function () { GetTestCredentialsResult(item); }, 5000);
                    return;
                }

                PropagateCredentialTestDataToRecord(result, item);
            });
    };

    PropagateCredentialTestDataToRecord = function(result, item) {
        item.data.CredentialsTestResult = GetCredentialTestResult(result);
        if (!item.data.HasUnsupportedOS) { // once we detect unsupported OS then we always say it's there
            item.data.HasUnsupportedOS = item.data.CredentialsTestResult == credentialsTestResultsValues.UnsupportedOS;
        }
        item.data.CredentialsTestMessage = result.Message;
        item.data.DeploymentType = result.DeploymentType;
        item.data.DetectionInfo = result.DetectionInfo;
        // handle agent mode autodetection
        if (item.data.Mode == autodetectAgentMode && result.CanUseActiveMode != null) {
            item.data.Mode = result.CanUseActiveMode ? activeAgentMode : passiveAgentMode;
        }

        if (typeof (result['InstallPackageFallbackId']) !== 'undefined') {
            item.data.InstallPackageId = result.InstallPackageFallbackId;
        } else {
            item.data.InstallPackageId = item.data.InstallPackageId || '';
        }

        item.commit();

        // show manual linux package selection column if required
        if (item.data.HasUnsupportedOS) {
            var columnModel = grid.getColumnModel();
            columnModel.setHidden(columnModel.getIndexById('installPackageIdColumn'), false);
        }
    }

    GetCredentialTestResult = function(result) {
        return result.Success
                  ? credentialsTestResultsValues.Ok
                  : result.FailureReason === failureReasonValues.UnsupportedLinuxFailure ? credentialsTestResultsValues.UnsupportedOS : credentialsTestResultsValues.CredentialsInvalid;
    }

    TestCredentialsInPopupInternal = function (items) {
        if (!AreCredentialFieldsValid())
            return;

        GetCredentialFromDialog(function(credentials) {;
            var testResults = $('#testResults');
            testResults.html('');
            testResults.show();

            var installPackageFallbackId = installPackageCombo.getValue();

            lastDialogCredentialTestResults = [];

            Ext.each(items, function(item) {
                var localItem = item;

                testResults.append(String.format('<li>{0}</li>', getCredentialTestResultMarkup('popup' + item.id, credentialsTestResultsValues.CredentialsTestInProgress, '', 0, localItem.data.Name)));
                setTimeout(function() { assignCredentialWindow.syncShadow(); }, 100);

                ORION.callWebService("/Orion/AgentManagement/Services/DeploymentWizard.asmx",
                    "TestCredentials", { hostname: localItem.data.ID, credentials: credentials },
                    function(result) {
                        $('#' + getCredentialTestResultId('popup' + localItem.id)).remove();

                        localItem.data.CredentialTested = true;
                        result.Item = localItem;

                        lastDialogCredentialTestResults.push(result);

                        SetDialogTestResults(lastDialogCredentialTestResults);
                    });
            });
        });
    };

    SetDialogTestResults = function(results) {
        var testResults = $('#testResults');
        testResults.html('');
        testResults.show();
        Ext.each(results, function(result) {
            var resultStatus = GetCredentialTestResult(result);

            if (resultStatus == credentialsTestResultsValues.UnsupportedOS) {
                $('.sw-AgentManagement-InstallPackageSelection').show();
                installPackageCombo.setValue('');
            }

            testResults.append(
                getCredentialTestResultMarkup(
                    'popup' + result.Item.id,
                    resultStatus,
                    result.Message,
                    result.DetectionInfo,
                    result.Item.data.Name));
        });
        assignCredentialWindow.syncShadow();
    }

    GetSelectedCredentialType = function() {
        if ($('#CredentialTypeCertificateRadio').is(':checked')) {
            return privateKeyCredentialType;
        }
        return usernameAndPasswordCredentialType;
    }

    SetSelectedCredentialType = function (type) {
        if (type == privateKeyCredentialType) {
            $('#CredentialTypeCertificateRadio').prop('checked', 'checked');
        } else {
            $('#CredentialTypeUsernamePasswordRadio').prop('checked', 'checked');
        }
    }

    GetSelectedOsType = function () {
        if ($('#OSTypeLinuxRadio').is(':checked')) {
            return linuxOsType;
        }
        return windowsOsType;
    }

    SetSelectedOsType = function (type) {
        if (type == linuxOsType) {
            $('#OSTypeLinuxRadio').prop('checked', 'checked');
        } else {
            $('#OSTypeWindowsRadio').prop('checked', 'checked');
        }
    }

    GetPrivateKeyFromFile = function (success) {

        var upload = () => {
            var uploader = new AgentManagement.AsyncFileUpload();
            uploader.uploadFile({
                url: "/api/AgentManagementDeployment/GetFileContentAsText",
                fileElementId: 'CredentialPrivateKeyFile'
            }, function (fileContent) {
                success(fileContent);
            });
        };

        try {
            var fileWithKey = document.getElementById("CredentialPrivateKeyFile").files[0];
            if (fileWithKey) {
                var reader = new FileReader();
                reader.readAsText(fileWithKey, "UTF-8");
                reader.onload = function (evt) {
                    success(evt.target.result.toString());
                }
                reader.onerror = function () {
                    upload();
                }
            }
        } catch (e) {
            console.log(e);
            upload();
        }
    }

    GetCredentialFromDialog = function(credentialsReady) {
        var credential = {};
        credential.credentialId = newCredentialId;
        credential.credentialName = credentialNameField.getValue();

        if (GetSelectedCredentialType() == privateKeyCredentialType) {
            credential.type = privateKeyCredentialType;
            credential.credentialId = selectedSshPrivateKeyCredential;
            credential.username = usernameField.getValue();
            credential.privateKey = certificateData;
            credential.privateKeyPassword = privateKeyPasswordField.getValue();
            if (credential.credentialId > 0) {
                credential.credentialName = GetExistingCredentialName(credential.credentialId, sshPrivateKeyCredentialsList);
            }

        } else {
            credential.type = usernameAndPasswordCredentialType;
            credential.credentialId = selectedCredential;
            credential.username = usernameField.getValue();
            credential.password = passwordField.getValue();
            if (credential.credentialId > 0) {
                credential.credentialName = GetExistingCredentialName(credential.credentialId, credentialsList);
            }
        }

        if (useSudoCheckbox.getValue()) {
            credential.additionalCredential = {
                type: usernameAndPasswordCredentialType,
                credentialId: selectedAdditionalCredential,
                credentialName: additionalCredentialNameField.getValue(),
                username: additionalUsernameField.getValue(),
                password: additionalPasswordField.getValue()
            };

            if (credential.additionalCredential.credentialId > 0) {
                credential.additionalCredential.credentialName = GetExistingCredentialName(credential.additionalCredential.credentialId, credentialsList);
            }
        }

        if (credential.credentialId != newCredentialId || certificateData || GetSelectedCredentialType() != privateKeyCredentialType) {
            credentialsReady(credential);
        } else if (certificateFileField.get(0).files.length > 0) {
            GetPrivateKeyFromFile(function(privateKey) {
                certificateData = privateKey;
                credential.privateKey = certificateData;
                credentialsReady(credential);
            });
        } else {
            Ext.Msg.show({
                title: errorTitle,
                msg: missingPrivateKeyError,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    }

    GetExistingCredentialName = function(credentialId, credentialCollection) {
        var matches = credentialCollection.filter(function(value) {
            return value.Key == credentialId;
        });
        return matches.length > 0 ? matches[0].Value : '';
    }

    AreCredentialFieldsValid = function() {
        var valid = true;

        if (GetSelectedCredentialType() == privateKeyCredentialType) {
            if (selectedSshPrivateKeyCredential == newCredentialId) {
                valid = credentialNameField.isValid() && valid;
                valid = usernameField.isValid() && valid;
                if (!certificateData && certificateFileField.get(0).files.length == 0) {
                    valid = false;
                    Ext.Msg.show({
                        title: errorTitle,
                        msg: missingPrivateKeyError,
                        icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                    });
                }
            }
        } else {
            if (selectedCredential == newCredentialId) {
                valid = credentialNameField.isValid() && valid;
                valid = usernameField.isValid() && valid;
                valid = passwordField.isValid() && valid;
            }
        }

        if (useSudoCheckbox.getValue()) {
            if (selectedAdditionalCredential == newCredentialId) {
                // additional creentials may contain just password
                valid = additionalPasswordField.isValid() && valid;
            }
        }
        return valid;
    };

    AssignCredentialsToSingleItem = function (itemId) {
        selectorModel.clearSelections();

        var recordIndex = dataStore.findBy(function (record, id) {
            return record.data.ID == itemId;
        });

        selectorModel.selectRow(recordIndex, true);

        assignCredentialWindow.show();
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        map.AssignCredentials.setDisabled(true);
        map.RemoveAgent.setDisabled(true);
        map.TestCredentials.setDisabled(true);

        if (count >= 1) {
            map.AssignCredentials.setDisabled(false);
            map.RemoveAgent.setDisabled(false);
            map.TestCredentials.setDisabled(false);
        }
    };

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: '@{R=AgentManagement.Strings;K=LoadingAgents;E=js}'
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {};
        grid.store.baseParams = {};
        grid.store.on('load', function () {
            loadingMask.hide();
            TestNotTestedCredentials();
        });
        grid.store.load();
    };

    AreAllCredentialsValidInternal = function () {
        var result = credentialsTestResultsValues.Ok;
        var isThereExistingAgent = false;
        dataStore.each(function (item) {
            if (item.data.CredentialId <= 0 && item.data.CredentialName == '') {
                result = credentialsTestResultsValues.CredentialsMissing;
                return false;
            }
            if (item.data.CredentialsTestResult === credentialsTestResultsValues.CredentialsInvalid) {
                result = credentialsTestResultsValues.CredentialsInvalid;
                return false;
            }
            if (item.data.CredentialsTestResult === credentialsTestResultsValues.CredentialsTestInProgress) {
                result = credentialsTestResultsValues.CredentialsTestInProgress;
                return false;
            }
            
            if (item.data.CredentialsTestResult === credentialsTestResultsValues.UnsupportedOS && (item.data.InstallPackageId == null || item.data.InstallPackageId == '')) {
                result = credentialsTestResultsValues.UnsupportedOS;
                return false;
            }
            if (item.data.DetectionInfo === 1 || item.data.DetectionInfo === 2) {
                isThereExistingAgent = true;
            }
        });

        if (result == credentialsTestResultsValues.Ok && isThereExistingAgent) {
            result = credentialsTestResultsValues.ExistingAgentWillBeReplaced;
        }
        return result;
    };

    TestNotTestedCredentials = function () {
        var itemsToTest = [];
        dataStore.each(function (item) {
            if ((typeof (item.data.CredentialsTestResult) == 'undefined' || item.data.CredentialsTestResult == credentialsTestResultsValues.CredentialsNotTested) && (item.data.CredentialId > 0 || item.data.CredentialName != '')) {
                itemsToTest.push(item);
            }
        });
        TestCredentials(itemsToTest);
    }

    function renderDefault(value, meta, record) {
        return value;
    }

    function renderName(value, meta, record) {
        if (record.data.NodeId > 0) {
            return renderDefault(
                    String.format('<img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;status={0}&amp;size=small" /> {1}', record.data.Status, value),
                    meta, record);
        }
        return renderDefault(value, meta, record);
    }

    function renderStatus(value, meta, record) {
        if (value == null || value == "")
            return renderDefault(String.format('<span class="sw-text-helpful sw-AgentManagement-grid-help-text">{0}</a>', notManagedByOrionText), meta, record);
        else
            return renderDefault(value, meta, record);
    }

    function renderCredentialsName(value, meta, record) {
        if (record.data.CredentialId == newCredentialId && record.data.CredentialName == '')
            return renderDefault(
                String.format('<span class="AgentManagement_UnassignedCredentials">{0}</span> ' +
                    '<span class="AgentManagement_HelpLink" onclick="SW.AgentManagement.DeploymentSettingsGrid.AssignCredentials(\'{1}\');">' +
                    '@{R=AgentManagement.Strings;K=AssignCredentialsButtonText;E=js}</span>',
                unassignedText, record.data.ID), meta, record);
        else
            return renderDefault(Ext.util.Format.htmlEncode(value), meta, record);
    }

    function renderCredentialsTestResult(value, meta, record) {
        if (record.data.CredentialsTestResult == credentialsTestResultsValues.UnsupportedOS && record.data.InstallPackageId) {
            // user selected install package manually, let's show that everything is OK
            return renderDefault(getCredentialTestResultMarkup(record.id, credentialsTestResultsValues.Ok, '', record.data.DetectionInfo, null), meta, record);
        } else {
            // make credentials test column larger if the message is long
            if ((record.data.CredentialsTestMessage && record.data.CredentialsTestMessage.length > 70)
            || record.data.DetectionInfo > 0) {
                var columnModel = grid.getColumnModel();
                var colIndex = columnModel.getIndexById('credential-test');
                if (columnModel.getColumnWidth(colIndex) < 1500) {
                    columnModel.setColumnWidth(colIndex, 1500);
                    setTimeout(function () { grid.getView().refresh(); }, 200);
                }
            }
            return renderDefault(getCredentialTestResultMarkup(record.id, value, record.data.CredentialsTestMessage, record.data.DetectionInfo, null), meta, record);
        }
    }

    function renderPoller(value, meta, record) {
        meta.css += ' x-small-editor x-form-field-wrap x-form-field-trigger-wrap sw-AgentManagement-combobox-cell';
        return renderDefault(
            String.format('<input type="text" autocomplete="off" class="x-form-text x-form-field x-trigger-noedit" value="{0}" readonly>' +
                '<img src="/Orion/js/extjs/resources/images/default/s.gif" alt="" class="x-form-trigger x-form-arrow-trigger">',
            pollerStore.getById(value).data.Value), meta, record);
    }

    function renderMode(value, meta, record) {
        meta.css += ' x-small-editor x-form-field-wrap x-form-field-trigger-wrap sw-AgentManagement-combobox-cell';
        return renderDefault(
            String.format('<input type="text" autocomplete="off" class="x-form-text x-form-field x-trigger-noedit" value="{0}" readonly>' +
                '<img src="/Orion/js/extjs/resources/images/default/s.gif" alt="" class="x-form-trigger x-form-arrow-trigger">',
            agentModeStore.getById(value).data.Value), meta, record);
    }

    function renderInstallPackage(value, meta, record) {
        meta.css += ' x-small-editor x-form-field-wrap x-form-field-trigger-wrap sw-AgentManagement-combobox-cell';
        if (!value) {
            value = '';
        }
        if (!record.data.HasUnsupportedOS) {
          return '@{R=AgentManagement.Strings;K=SettingsGridAgentInstallPackage_Autodetect;E=js}'
        } else {
          return renderDefault(
              String.format('<input type="text" autocomplete="off" class="x-form-text x-form-field x-trigger-noedit" value="{0}" readonly>' +
                  '<img src="/Orion/js/extjs/resources/images/default/s.gif" alt="" class="x-form-trigger x-form-arrow-trigger">',
              installPackageStore.data.find(function (x) { return x.data.PackageId == value; }).data.Label), meta, record);
        }
    }

    function getCredentialTestResultId(itemId) {
        return "test-result-" + itemId;
    }

    function getCredentialTestResultMarkup(itemId, resultValue, resultMessage, detectionInfo, itemName) {
        if (typeof (itemName) == 'undefined' || itemName === null)
            itemName = '';
        else
            itemName = itemName + ' - ';

        if (typeof (resultValue) == 'undefined' || resultValue == credentialsTestResultsValues.CredentialsNotTested) {
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-warn" automation="warn" title="{2}"><span class="sw-suggestion-icon"></span>{1}{2}</div>',
                getCredentialTestResultId(itemId), itemName, notTestedText);
        } else if (resultValue == credentialsTestResultsValues.CredentialsTestInProgress) {
            return String.format('<div id="{0}" title="{2}"><img src="/Orion/images/loading_gen_small.gif" alt="loading..." /> {1}{2}</div>',
                getCredentialTestResultId(itemId), itemName, testingCredentialsText);
        } else if (resultValue == credentialsTestResultsValues.UnsupportedOS) {
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-fail" automation="fail" title="{2}"><span class="sw-suggestion-icon"></span>{1}{2} <a href="{4}" target="_blank" rel="noopener noreferrer" class="AgentManagement_HelpLink">{3}</a></div>',
                getCredentialTestResultId(itemId), itemName, resultMessage, unsupportedOsHelpLinkText, unsupportedOsHelpUrl);
        } else if (resultValue > 0) {
            var markup = String.format('<div id="{0}" class="sw-suggestion sw-suggestion-pass" automation="pass" title="{2}"><span class="sw-suggestion-icon"></span>{1}{2}</div>',
            getCredentialTestResultId(itemId), itemName, testSuccessfulText);
            if (detectionInfo == 1) {
                markup += String.format('<div id="{0}-existing-agent" class="sw-suggestion sw-suggestion-warn" automation="warn" title="{1}"><span class="sw-suggestion-icon"></span>{1}{2}</div>',
                    getCredentialTestResultId(itemId), thereIsExistingAgentForThisServerText);
            } else if (detectionInfo == 2) {
                markup += String.format('<div id="{0}-existing-agent" class="sw-suggestion sw-suggestion-warn" automation="warn" title="{1}"><span class="sw-suggestion-icon"></span>{1}{2}</div>',
                    getCredentialTestResultId(itemId), thereIsExistingAgentForDiffrentServerText);
            }
            return markup;
        } else {
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-fail" automation="fail" title="{3}"><span class="sw-suggestion-icon"></span>{1}{2} - {3} <a href="{5}" target="_blank" rel="noopener noreferrer" class="AgentManagement_HelpLink">{4}</a></div>',
                getCredentialTestResultId(itemId), itemName, testFailedText, resultMessage, testFailedHelpLinkText, testFailedHelpKB);
        }
    }

    function textFieldsValidator(value) {
        if (value == null || value == '')
            return '@{R=AgentManagement.Strings;K=FieldMustNotBeEmpty;E=js}';

        return true;
    }

    InitLayout = function () {
        var panel = new Ext.Container({
            renderTo: 'SettingsGrid',
            height: AgentManagement_GetManagementGridHeight(),
            layout: 'border',
            items: [grid]
        });
        Ext.EventManager.onWindowResize(function () {
            panel.setHeight(AgentManagement_GetManagementGridHeight());
            panel.doLayout();
        }, panel);

        $('#IncludeSudoCredentials').on('change', RefreshCredentialsFormVisibility);
        $('#CredentialTypeCertificateRadio').on('change', OnCredentialTypeChange);
        $('#CredentialTypeUsernamePasswordRadio').on('change', OnCredentialTypeChange);
        $('#OSTypeWindowsRadio').on('change', OnOsTypeChange);
        $('#OSTypeLinuxRadio').on('change', OnOsTypeChange);
    };

    InitGrid = function () {
        selectorModel = new Ext.grid.CheckboxSelectionModel();

        selectorModel.on("selectionchange", function () {
            UpdateToolbarButtons();
        });

        dataStore = new ORION.WebServiceStore(
                            "/Orion/AgentManagement/Services/DeploymentWizard.asmx/GetAgentsForSettingsGrid",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'CredentialId', mapping: 2 },
                                { name: 'CredentialName', mapping: 3 },
                                { name: 'CredentialType', mapping: 4 },
                                { name: 'OsType', mapping: 5 },
                                { name: 'AdditionalCredentialId', mapping: 6 },
                                { name: 'StatusName', mapping: 7 },
                                { name: 'PollerId', mapping: 8 },
                                { name: 'NodeId', mapping: 9 },
                                { name: 'Status', mapping: 10 },
                                { name: 'Mode', mapping: 11, defaultValue: '1' },
                                { name: 'CredentialsTestResult', mapping: 12, defaultValue: undefined },
                                { name: 'CredentialsTestMessage', defaultValue: undefined },
                                { name: 'DetectionInfo', mapping: 13, defaultValue: undefined },
                                { name: 'CredentialsTestProgressId', defaultValue: undefined },
                                { name: 'DeploymentType', defaultValue: deploymentType.Auto },
                                { name: 'InstallPackageId', mapping: 14, defaultValue: '' },
                            ], "Name");

        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        dataStore.addListener("update", function (store, record, operation) {
            if (operation == Ext.data.Record.EDIT) {
                UpdateAgentSettings(
                    record.data.ID,
                    record.data.PollerId,
                    record.data.Mode,
                    record.data.InstallPackageId,
                    function () {
                    });
            }
        });
        
        dataStore.addListener("load", function (store, records, options) {
            Ext.each(records, function (record) {
                if (record.data.HasUnsupportedOS) {
                  var columnModel = grid.getColumnModel();
                  columnModel.setHidden(columnModel.getIndexById('installPackageIdColumn'), false);
              }
          });
        });

        pollerStore = new Ext.data.JsonStore({
            fields: ['Key', 'Value'],
            data: pollerList,
            idProperty: 'Key'
        });

        pollerComboBoxEditor = new Ext.form.ComboBox({
            store: pollerStore,
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            editable: false,
            lazyInit: false,
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    }
                    , scope: this
                }
            }
        });

        agentModeStore = new Ext.data.JsonStore({
            fields: ['Key', 'Value'],
            data: agentModeList,
            idProperty: 'Key'
        });

        agentModeComboBoxEditor = new Ext.form.ComboBox({
            store: agentModeStore,
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            editable: false,
            lazyInit: false,
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    }
                    , scope: this
                }
            }
        });

        installPackageStore = new ORION.WebServiceStore(
                            "/Orion/AgentManagement/Services/DeploymentWizard.asmx/GetInstallPackages",
                            [
                                { name: 'PackageId', mapping: 0 },
                                { name: 'Label', mapping: 1 },
                            ], "Label");
        installPackageStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });
        installPackageStore.proxy.conn.jsonData = {}; // force JSON
        installPackageStore.load();

        installPackageComboBoxEditor = new Ext.form.ComboBox({
            store: installPackageStore,
            valueField: 'PackageId',
            displayField: 'Label',
            triggerAction: 'all',
            mode: 'local',
            editable: false,
            lazyInit: false,
            listWidth: 300,
            listeners: {
                'focus': {
                    fn: function (comboField) {
                        comboField.doQuery(comboField.allQuery, true);
                        comboField.expand();
                    }
                    , scope: this
                },
                select: {
                    fn: function (comboField) {
                        // propagate value to record immediately, don't wait for real blur
                        comboField.fireEvent('blur', comboField);
                    }
                    , scope: this
                }
            }
        });

        grid = new Ext.grid.EditorGridPanel({

            store: dataStore,

            columns: [
                    selectorModel,
                    { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                    { header: '@{R=AgentManagement.Strings;K=SettingsGridAgentHostnameOrIP;E=js}', width: 250, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                    { header: '@{R=AgentManagement.Strings;K=SettingsGridPollingEngine;E=js}', width: 150, hideable: false, hidden: true, sortable: false, dataIndex: 'PollerId', renderer: renderPoller, editor: pollerComboBoxEditor },
                    { header: '@{R=AgentManagement.Strings;K=SettingsGridAgentStatus;E=js}', width: 150, hideable: false, sortable: false, dataIndex: 'StatusName', renderer: renderStatus },
                    { header: '@{R=AgentManagement.Strings;K=SettingsGridAgentMode;E=js}', width: 150, hideable: false, sortable: false, dataIndex: 'Mode', renderer: renderMode, editor: agentModeComboBoxEditor },
                    { header: '@{R=AgentManagement.Strings;K=SettingsGridAgentInstallPackage;E=js}', id: 'installPackageIdColumn', width: 150, hidden: true, hideable: true, sortable: false, dataIndex: 'InstallPackageId', renderer: renderInstallPackage, editor: installPackageComboBoxEditor },
                    { header: '@{R=AgentManagement.Strings;K=SettingsGridCredentialName;E=js}', width: 200, hideable: false, sortable: false, dataIndex: 'CredentialName', renderer: renderCredentialsName },
                    { id: 'credential-test', header: '@{R=AgentManagement.Strings;K=SettingsGridCredentialTestResults;E=js}', width: 600, hideable: false, sortable: false, dataIndex: 'CredentialsTestResult', renderer: renderCredentialsTestResult },
                    { id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 }
            ],

            sm: selectorModel,

            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            clicksToEdit: 1,
            autoExpandColumn: 'filler',
            viewConfig: {
                emptyText: '@{R=AgentManagement.Strings;K=ThereAreNoAgents;E=js}'
            },
            listeners: {
                'beforeedit': {
                    fn: function (e) {
                        if (e.field == 'InstallPackageId') {
                            // allow selection of install package only in case of failed autodetection
                            if (!e.record.data.HasUnsupportedOS) {
                                e.cancel = true;
                            }
                        }
                    }
                    , scope: this
                }
            },

            tbar: [
					{
					    id: 'AssignCredentials',
					    text: assignCredentialsButtonText,
					    iconCls: 'AgentManagement_AssignCredentialsButton',
					    handler: function () {
					        assignCredentialWindow.show();
					    }

					}, '-', {
					    id: 'TestCredentials',
					    text: testCredentialsButtonText,
					    iconCls: 'AgentManagement_TestCredentialsButton',
					    handler: function () {
					        TestCredentials(grid.getSelectionModel().getSelections());
					    }

					}, '-', {
					    id: 'RemoveAgent',
					    text: removeAgentButtonText,
					    iconCls: 'AgentManagement_DeleteAgentButton',
					    handler: function () {
					        var multiSelect = grid.getSelectionModel().getCount() > 1;

					        Ext.Msg.confirm(multiSelect ? "@{R=AgentManagement.Strings;K=RemoveAgentsDialogTitle;E=js}" : "@{R=AgentManagement.Strings;K=RemoveAgentDialogTitle;E=js}",
								multiSelect ? removePromptMultiple : removePromptSingle,
								function (btn, text) {
								    if (btn == 'yes') {
								        RemoveSelectedItems(grid.getSelectionModel().getSelections());
								    }
								});
					    }
					}
            ]

        });

        // hide poller column if only one poller is available
        grid.getColumnModel().setHidden(3, pollerStore.getCount() == 1);
    };

    ShowPrivateKeyDialog = function () {
        var textField = new Ext.form.TextArea({
            name: 'privateKeyData',
            fieldLabel: pastePrivateKeyManuallyLabel,
            width: '100%',
            height: 200,
            allowBlank: false
        });
        var formPanel = new Ext.FormPanel({
            height: 200,
            labelAlign: 'top',
            items: [textField]
        });
        var privateKeyWindow = new Ext.Window({
            title: pastePrivateKeyManuallyTitle,
            width: 400,
            cls: 'sw-AgentManagement-CertificatePasteDialog',
            autoHeight: true,
            closable: false,
            modal: true,
            resizable: false,

            items: [formPanel],

            buttons: [
                {
                    text: "@{R=AgentManagement.Strings;K=SubmitButtonLabel;E=js}",
                    disabled: false,
                    handler: function () {
                        certificateData = textField.getValue();
                        certificatePastedManually = true;
                        privateKeyWindow.hide();
                        RefreshCredentialsFormVisibility();
                    }
                },
                {
                    text: "@{R=AgentManagement.Strings;K=CancelButtonLabel;E=js}",
                    handler: function () {
                        privateKeyWindow.hide();
                    }
                }],
        });
        privateKeyWindow.show();
    }

    ResetPrivateKey = function () {
        certificateData = "";
        certificatePastedManually = false;
        RefreshCredentialsFormVisibility();
    }

    RefreshCredentialsFormVisibility = function () {
        $('.sw-AgentManagement-InstallPackageSelection').hide();

        if (GetSelectedOsType() == linuxOsType) {
            $('.sw-AgentManagement-CredentialTypeRow').show();
            $('.sw-AgentManagement-SudoSelection').show();
        } else {
            $('.sw-AgentManagement-CredentialTypeRow').hide();
            $('.sw-AgentManagement-SudoSelection').hide();
        }

        if (credentialsCombo.getValue() == newCredentialId && sshPrivateKeyCredentialsCombo.getValue() == newCredentialId) {
            $('.sw-AgentManagement-PrimaryCustomCredential').show();

            if (GetSelectedCredentialType() == privateKeyCredentialType) {
                $('.sw-AgentManagement-UsernamePasswordItem').hide();
                $('.sw-AgentManagement-PrivateKeyItem').show();
            } else {
                $('.sw-AgentManagement-UsernamePasswordItem').show();
                $('.sw-AgentManagement-PrivateKeyItem').hide();
            }
        } else {
            if (GetSelectedCredentialType() == privateKeyCredentialType) {
                $('.sw-AgentManagement-UsernamePasswordItem').hide();
                $('.sw-AgentManagement-PrivateKeyItem').show();
            } else {
                $('.sw-AgentManagement-UsernamePasswordItem').show();
                $('.sw-AgentManagement-PrivateKeyItem').hide();
            }

            $('.sw-AgentManagement-PrimaryCustomCredential').hide();
        }

        if ($('#IncludeSudoCredentials').is(':checked') && GetSelectedOsType() == linuxOsType) {
            $('.sw-AgentManagement-SudoSection').show();
            
            if (additionalCredentialsCombo.getValue() == newCredentialId) {
                $('.sw-AgentManagement-AdditionalCustomCredential').show();
            } else {
                $('.sw-AgentManagement-AdditionalCustomCredential').hide();
            }
        } else {
            $('.sw-AgentManagement-SudoSection').hide();
        }

        if (certificatePastedManually) {
            $('#CertificateViaFileBlock').hide();
            $('#CertificateManualBlock').show();
        } else {
            $('#CertificateViaFileBlock').show();
            $('#CertificateManualBlock').hide();
        }

        assignCredentialWindow.center();
        assignCredentialWindow.syncShadow();
    };

    OnCredentialTypeChange = function () {
        credentialsCombo.setValue(newCredentialId);
        sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
        selectedCredential = newCredentialId;
        selectedSshPrivateKeyCredential = newCredentialId;

        ClearCredentialFields();

        RefreshCredentialsFormVisibility();
    };

    OnOsTypeChange = function () {
        credentialsCombo.setValue(newCredentialId);
        sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
        additionalCredentialsCombo.setValue(newCredentialId);
        selectedCredential = newCredentialId;
        selectedSshPrivateKeyCredential = newCredentialId;
        selectedAdditionalCredential = newCredentialId;

        if (GetSelectedOsType() != linuxOsType) {
            SetSelectedCredentialType(usernameAndPasswordCredentialType);
            $('#IncludeSudoCredentials').prop('checked', false);
            $('#OSTypeWindowsRadioLabel').addClass('active');
            $('#OSTypeLinuxRadioLabel').removeClass('active');
        } else {
            $('#OSTypeWindowsRadioLabel').removeClass('active');
            $('#OSTypeLinuxRadioLabel').addClass('active');
        }

        ClearCredentialFields();
        ClearAdditionalCredentialFields();

        RefreshCredentialsFormVisibility();
    };

    ClearCredentialFields = function() {
        credentialNameField.setValue('');
        credentialNameField.clearInvalid();
        usernameField.setValue('');
        usernameField.clearInvalid();
        passwordField.setValue('');
        passwordField.clearInvalid();
        privateKeyPasswordField.setValue('');
        privateKeyPasswordField.clearInvalid();
        certificateData = '';
        $('#testResults').hide();
    };

    ClearAdditionalCredentialFields = function() {
        additionalCredentialNameField.setValue('');
        additionalCredentialNameField.clearInvalid();
        additionalUsernameField.setValue('');
        additionalUsernameField.clearInvalid();
        additionalPasswordField.setValue('');
        additionalPasswordField.clearInvalid();
    };

    InitAdditionalCredentials = function () {
        useSudoCheckbox = new Ext.form.Checkbox({
            applyTo: "IncludeSudoCredentials",
            boxLabel: ''
        });

        additionalCredentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: credentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'AdditionalCredentialNamesCombo',
            hiddenId: 'AdditionalCredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedAdditionalCredential = index.data.Key;
                    additionalCredentialNameField.validate();
                    additionalUsernameField.validate();
                    additionalPasswordField.validate();
                    
                    if (selectedCredential == newCredentialId) {
                      ClearAdditionalCredentialFields();
                    }

                    RefreshCredentialsFormVisibility();
                }
            }
        });

        additionalCredentialsCombo.setValue(selectedAdditionalCredential);

        additionalCredentialNameField = new Ext.form.TextField({
            applyTo: "AdditionalCredentialName",
            width: 230,
            validator: textFieldsValidator
        });

        additionalUsernameField = new Ext.form.TextField({
            applyTo: "AdditionalCredentialUsername",
            width: 230,
            validator: textFieldsValidator
        });

        additionalPasswordField = new Ext.form.TextField({
            applyTo: "AdditionalCredentialPassword",
            width: 230,
            validator: textFieldsValidator
        });
    }

    InitAssignCredentialWindow = function () {
        credentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: credentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'CredentialNamesCombo',
            hiddenId: 'CredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedCredential = index.data.Key;
                    credentialNameField.validate();
                    usernameField.validate();
                    passwordField.validate();
                    
                    if (selectedCredential == newCredentialId) {
                      ClearCredentialFields();
                    }
                    
                    RefreshCredentialsFormVisibility();
                }
            }
        });
        credentialsCombo.setValue(selectedCredential);

        sshPrivateKeyCredentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: sshPrivateKeyCredentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'SshCredentialNamesCombo',
            hiddenId: 'SshCredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedSshPrivateKeyCredential = index.data.Key;
                    credentialNameField.validate();
                    usernameField.validate();
                    
                    if (selectedCredential == newCredentialId) {
                      ClearCredentialFields();
                    }
                    
                    RefreshCredentialsFormVisibility();
                }
            }
        });
        sshPrivateKeyCredentialsCombo.setValue(selectedSshPrivateKeyCredential);

        credentialNameField = new Ext.form.TextField({
            applyTo: "CredentialName",
            width: 230,
            validator: textFieldsValidator
        });

        usernameField = new Ext.form.TextField({
            applyTo: "CredentialUsername",
            width: 230,
            validator: textFieldsValidator
        });

        passwordField = new Ext.form.TextField({
            applyTo: "CredentialPassword",
            width: 230,
            validator: textFieldsValidator
        });

        certificateFileField = $("#CredentialPrivateKeyFile");

        privateKeyPasswordField = new Ext.form.TextField({
            applyTo: "CredentialPrivateKeyPassword",
            buttonOnly: true,
            width: 230,
        });

        installPackageCombo = new Ext.form.ComboBox({
            store: installPackageStore,
            valueField: 'PackageId',
            displayField: 'Label',
            triggerAction: 'all',
            mode: 'local',
            width: 300,
            editable: false,
            lazyInit: false,
            transform: 'InstallPackageCombo',
            hiddenId: 'InstallPackageCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Label:htmlEncode}</div></tpl>',
            listeners: {
                select: function (combo, record, index) {
                    if (record.data.PackageId != '') {
                        var newResults = [];
                        Ext.each(lastDialogCredentialTestResults, function (result) {
                            result.InstallPackageFallbackId = record.data.PackageId;
                            // make new "everything is fine" result because user selected install package
                            newResults.push({
                                Item: result.Item,
                                Success: true,
                                Message: '',
                                FailureReason: result.FailureReason,
                                DetectionInfo: result.DetectionInfo
                            })
                        });
                        SetDialogTestResults(newResults);
                    } else {
                        Ext.each(lastDialogCredentialTestResults, function(result) {
                            result.InstallPackageFallbackId = record.data.PackageId;
                        });
                        SetDialogTestResults(lastDialogCredentialTestResults);
                    }
                }
            }
        });

        InitAdditionalCredentials();

        assignCredentialWindow = new Ext.Window({
            title: "@{R=AgentManagement.Strings;K=AssignCredentialDialogTitle;E=js}",
            width: 550,
            cls: 'sw-AgentManagement-credential-window-outer',
            autoHeight: true,
            closable: false,
            modal: true,
            resizable: false,

            items: [
                    new Ext.Panel({
                        applyTo: 'AssignCredentialWindow',
                        border: false
                    })
            ],

            buttons: [
                {
                    text: "@{R=AgentManagement.Strings;K=AssignButtonLabel;E=js}",
                    disabled: false,
                    cls: 'sw-btn-primary',
                    handler: function () {
                        if (!AreCredentialFieldsValid())
                            return;

                        GetCredentialFromDialog(function(credentials) {
                            IsCredentialNameUniqueCall(credentials, function(isUnique) {
                                if (isUnique) {
                                    AssignCredentials(grid.getSelectionModel().getSelections(), credentials, lastDialogCredentialTestResults);
                                    assignCredentialWindow.hide();
                                } else {
                                    Ext.Msg.show({
                                        title: errorTitle,
                                        msg: "@{R=AgentManagement.Strings;K=CredentialNameAlreadyExists;E=js}",
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK
                                    });
                                }
                            });
                        });
                    }
                },
                {
                    text: "@{R=AgentManagement.Strings;K=CancelButtonLabel;E=js}",
                    handler: function () {
                        assignCredentialWindow.hide();
                        credentialNameField.setValue('');
                        usernameField.setValue('');
                        passwordField.setValue('');
                    }
                }],
            listeners: {
                beforeshow: function () {
                    lastDialogCredentialTestResults = [];

                    $('#AssignCredentialWindow').show();

                    if (grid.getSelectionModel().getCount() != 1) {
                        sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
                        credentialsCombo.setValue(newCredentialId);
                        additionalCredentialsCombo.setValue(newCredentialId);
                        selectedSshPrivateKeyCredential = newCredentialId;
                        selectedCredential = newCredentialId;
                    } else {
                        var record = grid.getSelectionModel().getSelected();
                        
                        SetSelectedOsType(record.data.OsType ? record.data.OsType : windowsOsType);

                        if (record.data.CredentialId > 0) {
                            if (record.data.CredentialType == privateKeyCredentialType) {
                                SetSelectedCredentialType(privateKeyCredentialType);
                                sshPrivateKeyCredentialsCombo.setValue(record.data.CredentialId);
                                credentialsCombo.setValue(newCredentialId);
                                selectedSshPrivateKeyCredential = record.data.CredentialId;
                                selectedCredential = newCredentialId;
                            } else {
                                GetSelectedCredentialType() == usernameAndPasswordCredentialType;
                                sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
                                credentialsCombo.setValue(record.data.CredentialId);
                                selectedSshPrivateKeyCredential = newCredentialId;
                                selectedCredential = record.data.CredentialId;
                            }
                        } else {
                            sshPrivateKeyCredentialsCombo.setValue(newCredentialId);
                            credentialsCombo.setValue(newCredentialId);
                            selectedSshPrivateKeyCredential = newCredentialId;
                            selectedCredential = newCredentialId;
                        }

                        if (record.data.AdditionalCredentialId > 0) {
                            useSudoCheckbox.setValue(true);
                            additionalCredentialsCombo.setValue(record.data.AdditionalCredentialId);
                            selectedAdditionalCredential = record.data.AdditionalCredentialId;
                        } else {
                            useSudoCheckbox.setValue(false);
                            additionalCredentialsCombo.setValue(newCredentialId);
                            selectedAdditionalCredential = newCredentialId;
                        }
                    }

                    Ext.each(grid.getSelectionModel().getSelected(), function (record) {
                        record.data.CredentialTested = false;
                    });

                    ClearCredentialFields();
                    ClearAdditionalCredentialFields();

                    RefreshCredentialsFormVisibility();

                    OnOsTypeChange();

                    var testResults = $('#testResults');
                    testResults.html('');
                    testResults.hide();
                }
            }
        });
    };

    ORION.prefix = "AgentManagement_DeploymentSettingsGrid_";

    return {
        SetCredentialsList: function (list) {
            credentialsList = list;
        },
        SetSshPrivateKeyCredentialsList: function (list) {
            sshPrivateKeyCredentialsList = list;
        },
        SetPollerList: function (list) {
            pollerList = list;
        },
        SetAgentModeList: function (list) {
            agentModeList = list;
        },
        SetUnsupportedOsHelpUrl: function (url) {
            unsupportedOsHelpUrl = url;
        },
        TestCredentialsInPopup: function () {
            TestCredentialsInPopupInternal(grid.getSelectionModel().getSelections());
        },
        AssignCredentials: function (itemId) {
            AssignCredentialsToSingleItem(itemId);
        },
        AreAllCredentialsValid: function () {
            return AreAllCredentialsValidInternal();
        },
        ShowPrivateKeyDialog: function () {
            ShowPrivateKeyDialog();
        },
        ResetPrivateKey: function () {
            ResetPrivateKey();
        },
        UploadFile: function() {
            UploadFile();
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            InitGrid();
            InitLayout();
            InitAssignCredentialWindow();

            grid.getColumnModel().setHidden(3, pollerStore.getCount() == 1);

            RefreshObjects();
            UpdateToolbarButtons();
        },
        credentialsTestResultsValues: credentialsTestResultsValues
    };
}();

Ext.onReady(SW.AgentManagement.DeploymentSettingsGrid.init, SW.AgentManagement.DeploymentSettingsGrid);
