﻿Ext.namespace('SW');
Ext.namespace('SW.AgentManagement');
Ext.namespace('SW.AgentManagement.EditAgent');

SW.AgentManagement.EditAgent = function () {
    var diagnosticsRunningMessage = '@{R=AgentManagement.Strings;K=EditAgent_DiagnosticsRunningMessage;E=js}';//"Collecting new diagnostics. Started at {0}. Please wait...";
    var noDiagnosticsMessage = '@{R=AgentManagement.Strings;K=EditAgent_NoDiagnosticsMessage;E=js}'; //"No diagnostics available";
    var diagnosticsAvailableFormat = '@{R=AgentManagement.Strings;K=EditAgent_DiagnosticsAvailableFormat;E=js}'; //"Available diagnostics from {0}";
    var downloadText = '@{R=AgentManagement.Strings;K=EditAgent_DownloadText;E=js}'; //"Download";
    var saveWithFailedTestMessageTitle = '@{R=AgentManagement.Strings;K=EditAgent_DownloadText;E=js}'; //"Save current values";
    var saveWithFailedTestMessageFormat = '@{R=AgentManagement.Strings;K=EditAgent_SaveWithFailedTestMessageFormat;E=js}';
    var failedTestPollerDownMessageFormat = '@{R=AgentManagement.Strings;K=EditAgent_FailedTestPollerDownMessageFormat;E=js}';
    var updateRemoteSettingsMessageTitle = '@{R=AgentManagement.Strings;K=EditAgent_UpdateRemoteSettingsMessageTitle;E=js}';
    var updateRemoteSettingsWithFailedTestMessageFormat = '@{R=AgentManagement.Strings;K=EditAgent_UpdateRemoteSettingsWithFailedTestMessageFormat;E=js}';
    var testingMessage = '@{R=AgentManagement.Strings;K=EditAgent_TestingMessage;E=js}';
    var noAgentAvailableText = '@{R=AgentManagement.Strings;K=EditAgent_NoAgentAvailableText;E=js}'; //
    var loadingAgentsText = '@{R=AgentManagement.Strings;K=EditAgent_LoadingAgentsText;E=js}'; // EditAgent_LoadingAgentsText
    var errorText = '@{R=AgentManagement.Strings;K=ErrorTitle;E=js}';  //'Error';
    var testFailedHelpKB = "http://www.solarwinds.com/documentation/kbloader.aspx?kb=5574&lang=@{R=Core.Strings;K=CurrentHelpLanguage;E=js}";

    var useProxyCheckboxId;
    var passiveRadioId;
    var activeRadioId;
    var agentIdBoxId;
    var agentDnsNameBoxId;
    var agentIpBoxId;
    var nameBoxId;
    var passwordPassiveBoxId;
    var passiveAgentPortBoxId;
    var proxyUrlBoxId;
    var useProxyAuthenticationCheckboxId;
    var proxyUserNameBoxId;
    var proxyPasswordBoxId;
    var guidBoxId;
    var originalGuidBoxId;
    var passiveAgentHostnameBoxId;
    var originalAgentHostnameBoxId;
    var originalModeBoxId;
    var logLevelComboId;
    var pollingEngineActiveId;
    var pollingEnginePassiveId;
    var advancedPassiveCheckboxId;
    var advancedActiveCheckboxId;
    var updateRemoteSettingsCheckboxId;
    var agentStatusMessageBoxId;
    var agentStatusBoxId;
    var originalAgentIpBoxId;
    var originalAgentDnsNameBoxId;
    var originalAgentPollingEngineIdId;
    var savePostBack;
    var testMask;
    var previousDiagnosticsRunStatus = null;
    var agentVersionFromTestId;
    var testConnectionWindow;

    var proxySettingsListId;

    var modeActive = 1;
    var modePassive = 2;

    var activeAgents = {};

    Init = function () {
        $('#' + advancedPassiveCheckboxId).click(function (e) {
            e.stopPropagation();
        });

        $('#' + advancedActiveCheckboxId).click(function (e) {
            e.stopPropagation();
        });

        $('#AgentManagement_ToggleAdvancedPassiveDiv').click(function () {
            $('#' + advancedPassiveCheckboxId).click();

            RefreshAdvancedSections();
        });

        $('#AgentManagement_ToggleAdvancedActiveDiv').click(function () {
            $('#' + advancedActiveCheckboxId).click();

            RefreshAdvancedSections();
        });

        $('#ToggleTroubleshootDiv').click(function () {
            if ($('#TroubleshootCollapseImg').attr('src') == '/Orion/Images/Button.Expand.gif') {
                $('#TroubleshootDivLoading').show();
                $('#AgentOfflineError').hide();
                $('#TroubleshootDiv').hide();
                $('#TroubleshootCollapseImg').attr('src', '/Orion/Images/Button.Collapse.gif');

                RefreshDiagnosticsStatus();
            }
            else {
                $('#TroubleshootDiv').hide();
                $('#AgentOfflineError').hide();
                $('#TroubleshootDivLoading').hide();
                $('#TroubleshootCollapseImg').attr('src', '/Orion/Images/Button.Expand.gif');
            }
        });

        $('#GenerateDiagnosticsLink').click(
            function () {
                $('#GenerateDiagnosticsLink').hide();
                $('#AgentManagement_DiagnosticsStatus').html('<img src="/Orion/images/loading_gen_small.gif" /> ' + String.format(diagnosticsRunningMessage, new Date().toLocaleTimeString()));
                var agent = GetAgentFromForm(false);
                CallGenerateDiagnostics(agent.AgentId,
                    function () {
                        setTimeout(RefreshDiagnosticsStatus, 1000);
                    });
                return false;
            });

        $('#' + useProxyCheckboxId).click(function () {
            RefreshProxySection();
        });

        $('#' + useProxyAuthenticationCheckboxId).click(function () {
            RefreshProxyAuthenticationSection();
        });

        $('#' + passiveRadioId).click(function () {
            RefreshSetingsSections();

            if ($('#' + passiveRadioId).is(':checked')) {
                var agent = GetAgentFromForm(false);
                if (agent.PassiveAgentHostname == '')
                    $('#' + passiveAgentHostnameBoxId).val(agent.DNSName);
            }
        });

        $('#' + activeRadioId).click(function () {
            RefreshSetingsSections();
        });

        $('#activeAgentsCombo').change(function () {
            UpdateSelectedActiveAgent();
        });

        if (updateRemoteSettingsCheckboxId) {
            if ($('#' + updateRemoteSettingsCheckboxId).is(':checked'))
                DisableControlsForRemoteSettings();

            $('#' + updateRemoteSettingsCheckboxId).click(function () {
                var isChecked = $(this).is(':checked');
                if (isChecked)
                    DisableControlsForRemoteSettings();
                else
                    EnableControlsForRemoteSettings();
            });
        }

        $('#' + proxySettingsListId).change(function() {
            UpdateSelectedProxySettings();
        });

        RefreshSetingsSections();
        RefreshAdvancedSections();
        RefreshProxySection();
        RefreshProxyAuthenticationSection();
        RefreshActiveAgentsList();

        //update proxy settings as we may have wrong values after page reload
        UpdateSelectedProxySettings();
    };

    CallGetDiagnosticsStatus = function (agentId, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                             "GetDiagnosticsStatus", { agentId: agentId },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    CallGenerateDiagnostics = function (agentId, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                             "CollectDiagnostics", { agentId: agentId },
                             function (result) {
                                 onSuccess();
                             });
    };

    CallCheckAgentNameExists = function (agentId, agentName, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                            "CheckAgentNameExists", { agentId: agentId, agentName: agentName },
                            function (result) {
                                onSuccess(result);
                            });
    };

    CallCheckAgentLocationExists = function (agentId, agentUrl, agentPort, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                            "CheckAgentLocationExists", { agentId: agentId, url: agentUrl, port: agentPort },
                            function (result) {
                                onSuccess(result);
                            });
    };

    CallTestAgent = function (agent, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                             "TestAgent", { agent: agent, updateRemoteSettings: updateRemoteSettingsCheckboxId ? $('#' + updateRemoteSettingsCheckboxId).is(':checked') : false },
                             function (result) {
                                 onSuccess(result);
                             });
    };
    
    CallGetActiveAgentsWaitingForAdd = function (onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                             "GetActiveAgentsWaitingForAdd", {},
                             function (result) {
                                 onSuccess(result);
                             });
    };

    CallGetProxySettings = function (proxyId, onSuccess) {
        ORION.callWebService("/Orion/AgentManagement/Services/AgentsGrid.asmx",
                             "GetProxySettings", {proxyId : proxyId},
                             function (result) {
                                 onSuccess(result);
                             });
    };

    RefreshActiveAgentsList = function () {
        $('#activeAgentsCombo').html("");

        // Get original agent
        var originalAgent = GetAgentFromForm(true);
        // Get agent from form
        var agentFromForm = GetAgentFromForm(false);

        var currentAgentSelected = false;
        var originalAgentAdded = false;
        var agentFromFormAdded = false;

        // If there is an original agent and it is different that selected agent, display it
        if (originalAgent.IP != '' && originalAgent.IP != agentFromForm.IP) {
            $('#activeAgentsCombo').append('<option value="' + originalAgent.AgentGuid + '">' + originalAgent.DNSName + ' (' + originalAgent.IP + ')</option>');
            originalAgentAdded = true;
        }
        if (agentFromForm.IP != '') {
            $('#activeAgentsCombo').append('<option value="' + agentFromForm.AgentGuid + '" selected="selected">' + agentFromForm.DNSName + ' (' + agentFromForm.IP + ')</option>');
            agentFromFormAdded = true;
            currentAgentSelected = true;
        }

        $('#activeAgentsCombo').append('<option value="loadingAgents">' + loadingAgentsText + '</option>');

        CallGetActiveAgentsWaitingForAdd(function (agents) {
            $('#activeAgentsCombo option[value="loadingAgents"]').remove();

            if (agents.length == 0 && !originalAgentAdded && !agentFromFormAdded) {
                $('#activeAgentsCombo').append('<option value="" selected="selected">' + noAgentAvailableText + '</option>');
            }

            for (var i = 0; i < agents.length; i++) {
                // add agent to agent list for later use
                activeAgents[agents[i].AgentGuid] = agents[i];

                // skip current agent, it's already there
                if ((agents[i].AgentGuid == agentFromForm.AgentGuid && agentFromFormAdded)
                    || (agents[i].AgentGuid == originalAgent.AgentGuid && originalAgentAdded))
                    continue;

                if (i == 0 && !currentAgentSelected)
                    $('#activeAgentsCombo').append('<option value="' + agents[i].AgentGuid + '" selected="selected">' + agents[i].DNSName + ' (' + agents[i].IP + ')</option>');
                else
                    $('#activeAgentsCombo').append('<option value="' + agents[i].AgentGuid + '">' + agents[i].DNSName + ' (' + agents[i].IP + ')</option>');
            }

            UpdateSelectedActiveAgent();
        });
    };

    UpdateSelectedActiveAgent = function () {
        var guid = $('#activeAgentsCombo option:selected').val();
        var originalGuid = $('#' + originalGuidBoxId).val();

        // TODO: Move agentFromForm and originalAgent to agents collection and select always from that collection
        if (guid != '' && typeof (activeAgents[guid]) != 'undefined') {
            $('#' + guidBoxId).val(guid);
            $('#' + agentDnsNameBoxId).val(activeAgents[guid].DNSName);
            $('#' + agentIpBoxId).val(activeAgents[guid].IP);
            $('#' + pollingEngineActiveId).val(activeAgents[guid].PollingEngineId);
        }
        else if (originalGuid != '' && originalGuid == guid) {
            $('#' + guidBoxId).val(originalGuid);
            $('#' + agentDnsNameBoxId).val($('#' + originalAgentDnsNameBoxId).val());
            $('#' + agentIpBoxId).val($('#' + originalAgentIpBoxId).val());
            $('#' + pollingEngineActiveId).val($('#' + originalAgentPollingEngineIdId).val());
        }
    };

    RefreshAdvancedSections = function () {
        var isChecked = $('#' + advancedPassiveCheckboxId).is(':checked');

        if (isChecked) {
            $('#AgentManagement_AgentAdvancedPassiveSettings').show();
            $('#CollapsePassiveImg').attr('src', '/Orion/Images/Button.Collapse.gif');
        }
        else {
            $('#AgentManagement_AgentAdvancedPassiveSettings').hide();
            $('#CollapsePassiveImg').attr('src', '/Orion/Images/Button.Expand.gif');
        }

        isChecked = $('#' + advancedActiveCheckboxId).is(':checked');

        if (isChecked) {
            $('#AgentManagement_AgentAdvancedActiveSettings').show();
            $('#CollapseActiveImg').attr('src', '/Orion/Images/Button.Collapse.gif');
        }
        else {
            $('#AgentManagement_AgentAdvancedActiveSettings').hide();
            $('#CollapseActiveImg').attr('src', '/Orion/Images/Button.Expand.gif');
        }
    };

    RefreshProxySection = function () {
        var isChecked = $('#' + useProxyCheckboxId).is(':checked');

        if (isChecked) {
            $('#AgentManagement_ProxySettings').show();
        }
        else {
            $('#AgentManagement_ProxySettings').hide();
            $('#' + useProxyAuthenticationCheckboxId).attr('checked', false);
        }
        RefreshProxyAuthenticationSection();
    };

    RefreshProxyAuthenticationSection = function () {
        var isChecked = $('#' + useProxyAuthenticationCheckboxId).is(':checked');

        if (isChecked) {
            $('#AgentManagement_ProxyAuthenticationSettings').show();
        }
        else {
            $('#AgentManagement_ProxyAuthenticationSettings').hide();
        }
    };

    RefreshSetingsSections = function () {
        var isChecked = $('#' + passiveRadioId).is(':checked');

        if (isChecked) {
            $('#AgentManagement_ActiveSettings').hide();
            $('#AgentManagement_PassiveSettings').show();
        }
        else {
            $('#AgentManagement_PassiveSettings').hide();
            $('#AgentManagement_ActiveSettings').show();
        }
    };

    SetTestResults = function (message, detail, className, showHelp) {
        $('#AgentManagement_TestProgress').hide();

        var testResults = $('#AgentManagement_TestResults');
        testResults.removeAttr('class');
        $('#AgentManagement_TestResults .sw-suggestion-title').text(message);

        testResults.addClass('sw-suggestion');
        if (className != '')
            testResults.addClass(className);

        var testDetail = $('#AgentManagement_TestDetail');
        if (detail != null) {
            if (showHelp)
                testDetail.html(detail + testDetail.html());
            else
                testDetail.text(detail);
        } else {
            testDetail.text("");
        }

        $('#AgentManagement_TestResults').show();
    }

    TestAgentSettings = function (onSuccess) {
        var agent = GetAgentFromForm(false);
        if (agent == null)
            return;

        $('#AgentManagement_TestResults').hide();
        $('#AgentManagement_TestProgress').show();
        CallTestAgent(agent, function (results) {
            var className = results.Success == '1' ? 'sw-suggestion-pass' : 'sw-suggestion-fail';
            SetTestResults(results.Message, results.Detail, className, (results.Status == 0 || results.Status == 2));

            if (onSuccess)
                onSuccess(results);
        });
    }

    GetAgentFromForm = function (useOriginal) {
        var agent = {};
        agent.AgentId = $('#' + agentIdBoxId).val();
        if (useOriginal) {
            agent.DNSName = $('#' + originalAgentDnsNameBoxId).val();
            agent.IP = $('#' + originalAgentIpBoxId).val();
        } else {
            agent.DNSName = $('#' + agentDnsNameBoxId).val();
            agent.IP = $('#' + agentIpBoxId).val();
        }
        agent.Name = $('#' + nameBoxId).val();
        agent.Mode = $('#' + activeRadioId).is(':checked') ? modeActive : modePassive;
        agent.PassiveAgentHostname = $('#' + passiveAgentHostnameBoxId).val();

        if (agent.Mode == modeActive) {
            agent.PollingEngineId = $('#' + pollingEngineActiveId).val();
            agent.PollingEngineName = $('#' + pollingEngineActiveId + ' option:selected').text();
        } else {
            agent.PollingEngineId = $('#' + pollingEnginePassiveId).val();
            agent.PollingEngineName = $('#' + pollingEnginePassiveId + ' option:selected').text();
            agent.PassiveAgentPort = $('#' + passiveAgentPortBoxId).val() || 0;
        }

        agent.UseProxy = $('#' + useProxyCheckboxId).is(':checked');
        agent.ProxyUrl = $('#' + proxyUrlBoxId).val();
        agent.UseProxyAuthentication = $('#' + useProxyAuthenticationCheckboxId).is(':checked');
        agent.ProxyUserName = $('#' + proxyUserNameBoxId).val();
        agent.ProxyPassword = $('#' + proxyPasswordBoxId).val();
        if (useOriginal) {
            agent.AgentGuid = $('#' + originalGuidBoxId).val();
        } else {
            if ($('#' + guidBoxId).val() != '') {
                agent.AgentGuid = $('#' + guidBoxId).val();
            }
        }

        if (logLevelComboId)
            agent.LogLevel = $('#' + logLevelComboId + ' option:selected').text();

        return agent;
    }

    RefreshDiagnosticsStatus = function () {
        var agent = GetAgentFromForm(false);

        if (!agent.AgentId || agent.AgentId == 0) {
            $('#ToggleTroubleshootDiv').hide();
            return;
        }

        $('#ToggleTroubleshootDiv').show();

        CallGetDiagnosticsStatus(agent.AgentId,
            function (result) {
                if (!result) {
                    $('#TroubleshootDivLoading').hide();
                    $('#TroubleshootDiv').hide();
                    $('#AgentOfflineError').show();
                    return;
                }

                // stop refresh if completed
                if (!result.Running) {
                    var html = '';
                    if (result.Failed) {
                        html += '<span class="AgentManagement_DiagnosticsError">' + result.ErrorMessage + '</span><br />';
                    }
                    
                    if (result.Exists) {
                        html += String.format(diagnosticsAvailableFormat, result.DateTime) + ' <a  class="sw-link" href="/Orion/AgentManagement/Admin/AgentDiagnosticsDownloader.ashx?agentId=' + agent.AgentId + '" target="_blank">' + downloadText + '</a>';
                    }
                    else {
                        html += noDiagnosticsMessage;
                    }
                    $('#AgentManagement_DiagnosticsStatus').html(html);
                    $('#GenerateDiagnosticsLink').show();
                }
                else {
                    // set loading icon only if running status changed to prevent lags in animation
                    var d = new Date();
                    d.to
                    if (previousDiagnosticsRunStatus != result.Running) {
                        $('#AgentManagement_DiagnosticsStatus').html('<img src="/Orion/images/loading_gen_small.gif" /> ' + String.format(diagnosticsRunningMessage, new Date().toLocaleTimeString()));
                    }
                    $('#GenerateDiagnosticsLink').hide();
                }
                
                $('#TroubleshootDivLoading').hide();
                $('#TroubleshootDiv').show();
                
                previousDiagnosticsRunStatus = result.Running;
                
                // some controls are not valid if agent is offline
                if (!result.AgentConnected) {
                    $('#TroubleshootDiv .AgentManagemnt_RequiresOnlineAgent').hide();
                    $('#AgentOfflineError').show();
                } else {
                    $('#' + logLevelComboId).val(result.CurrentLogLevel);
                    $('#TroubleshootDiv .AgentManagemnt_RequiresOnlineAgent').show();
                    $('#AgentOfflineError').hide();
                }
                
                if (result.Running)
                    setTimeout(RefreshDiagnosticsStatus, 1000);
            });
    };

    ShowError = function (msg, title) {
        Ext.Msg.show({
            title: title || errorText,
            msg: msg,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR
        });
    };

    DisableControlsForRemoteSettings = function () {
        // reset changes except port and password
        $('#' + guidBoxId).val($('#' + originalGuidBoxId).val());
        $('#' + passiveAgentHostnameBoxId).val($('#' + originalAgentHostnameBoxId).val());

        if ($('#' + originalModeBoxId).val() == 'active') {
            $('#' + activeRadioId).click();
            $('#' + activeRadioId).trigger('click');
        } else {
            $('#' + passiveRadioId).click();
            $('#' + passiveRadioId).trigger('click');
        }

        // disable mode selection
        $('#' + nameBoxId).attr('disabled', true);
        $('#' + activeRadioId).attr('disabled', true);
        $('#' + passiveRadioId).attr('disabled', true);
        $('#' + guidBoxId).attr('disabled', true);
        $('#' + passiveAgentHostnameBoxId).attr('disabled', true);
        $('#' + useProxyCheckboxId).attr('disabled', true);
        $('#' + proxyUrlBoxId).attr('disabled', true);
        $('#' + useProxyAuthenticationCheckboxId).attr('disabled', true);
        $('#' + proxyUserNameBoxId).attr('disabled', true);
        $('#' + proxyPasswordBoxId).attr('disabled', true);
    }

    EnableControlsForRemoteSettings = function () {
        $('#' + nameBoxId).removeAttr('disabled');
        $('#' + activeRadioId).removeAttr('disabled');
        $('#' + passiveRadioId).removeAttr('disabled');
        $('#' + guidBoxId).removeAttr('disabled');
        $('#' + passiveAgentHostnameBoxId).removeAttr('disabled');
        $('#' + useProxyCheckboxId).removeAttr('disabled');
        $('#' + proxyUrlBoxId).removeAttr('disabled');
        $('#' + useProxyAuthenticationCheckboxId).removeAttr('disabled');
        $('#' + proxyUserNameBoxId).removeAttr('disabled');
        $('#' + proxyPasswordBoxId).removeAttr('disabled');
    }

    UpdateSelectedProxySettings = function() {
        var proxyId = $('#' + proxySettingsListId).val();
        CallGetProxySettings(proxyId,
            function (result) {
                if (result.ProxyId == 0) { //empty fields
                    $('#AgentManagement_ProxyAuthenticationSettings').hide();
                    $('#' + proxyUrlBoxId).removeAttr('disabled');
                    $('#' + useProxyAuthenticationCheckboxId).removeAttr('disabled');
                    $('#' + proxyUserNameBoxId).removeAttr('disabled');
                    $('#' + proxyPasswordBoxId).removeAttr('disabled');

                    $('#' + useProxyAuthenticationCheckboxId).attr('checked', false);

                    $('#' + proxyUrlBoxId).val('');
                    $('#' + proxyUserNameBoxId).val('');
                    $('#' + proxyPasswordBoxId).val('');
                } else {
                    $('#' + proxyUrlBoxId).attr('disabled', true);
                    $('#' + useProxyAuthenticationCheckboxId).attr('disabled', true);
                    $('#' + proxyUserNameBoxId).attr('disabled', true);
                    $('#' + proxyPasswordBoxId).attr('disabled', true);

                    $('#' + proxyUrlBoxId).val(result.ProxyUrl);
                    if (result.UseProxyAuthentication) {
                        $('#AgentManagement_ProxyAuthenticationSettings').show();
                        $('#' + useProxyAuthenticationCheckboxId).attr('checked', true);
                        //credentials
                        $('#' + proxyUserNameBoxId).val(result.ProxyUsername);
                        $('#' + proxyPasswordBoxId).val('********');
                    } else {
                        $('#AgentManagement_ProxyAuthenticationSettings').hide();
                        $('#' + useProxyAuthenticationCheckboxId).attr('checked', false);
                    }
                }                
        });
    }


    var originalHandleError = ORION.handleError;
    ORION.handleError = function (xhr) {
        originalHandleError(xhr);

        var msg = $('#test').text();
        if (msg) {
            ShowError(msg);
        }
    };

    return {

        Init: function () {
            Init();
        },

        TestAgentSettings: function () {
            TestAgentSettings();
        },
        TestAgentSettingsAndSave: function () {
            if (testMask == null)
                testMask = new Ext.LoadMask(Ext.getBody(), { msg: testingMessage });

            testMask.show();

            TestAgentSettings(function (results) {
                $('#' + agentStatusBoxId).val(results.Status);
                $('#' + agentStatusMessageBoxId).val(results.Message);

                var version = results.AgentVersion;
                if (typeof version !== 'undefined' && version != null) {
                    $('#' + agentVersionFromTestId).val(version.Major + '.' + version.Minor + '.' + version.Build + '.' + version.Revision);
                }

                if (results.Status == 1) {
                    savePostBack();
                } else {
                    testMask.hide();

                    if ($('#' + updateRemoteSettingsCheckboxId).is(':checked')) {
                        Ext.Msg.show({
                            title: updateRemoteSettingsMessageTitle,
                            msg: String.format(updateRemoteSettingsWithFailedTestMessageFormat, results.Message),
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    } else {
                        if (results.IsPollerDown) {
                            Ext.Msg.show({
                                title: saveWithFailedTestMessageTitle,
                                msg: String.format(failedTestPollerDownMessageFormat, results.Message),
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        } else {
                            Ext.Msg.show({
                                title: saveWithFailedTestMessageTitle,
                                msg: String.format(saveWithFailedTestMessageFormat, results.Message),
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.MessageBox.WARNING,
                                fn: function (buttonId) {
                                    if (buttonId == 'yes') {
                                        testMask.show();
                                        savePostBack();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        },

        SetUseProxyCheckboxId: function (id) { useProxyCheckboxId = id; },
        SetPassiveRadioId: function (id) { passiveRadioId = id; },
        SetActiveRadioId: function (id) { activeRadioId = id; },
        SetAgentIdBoxId: function (id) { agentIdBoxId = id; },
        SetAgentDNSNameBoxId: function (id) { agentDnsNameBoxId = id; },
        SetAgentIPBoxId: function (id) { agentIpBoxId = id; },
        SetNameBoxId: function (id) { nameBoxId = id; },
        SetPasswordPassiveBoxId: function (id) { passwordPassiveBoxId = id; },
        SetPassiveAgentPortBoxId: function (id) { passiveAgentPortBoxId = id; },
        SetProxyUrlBoxId: function (id) { proxyUrlBoxId = id; },
        SetUseProxyAuthenticationCheckboxId: function (id) { useProxyAuthenticationCheckboxId = id; },
        SetProxyUserNameBoxId: function (id) { proxyUserNameBoxId = id; },
        SetProxyPasswordBoxId: function (id) { proxyPasswordBoxId = id; },
        SetGuidBoxId: function (id) { guidBoxId = id; },
        SetOriginalGuidBoxId: function (id) { originalGuidBoxId = id; },
        SetPassiveAgentHostnameBoxId: function (id) { passiveAgentHostnameBoxId = id; },
        SetOriginalModeBoxId: function (id) { originalModeBoxId = id; },
        SetLogLevelComboId: function (id) { logLevelComboId = id; },
        SetPollingEngineActiveId: function (id) { pollingEngineActiveId = id; },
        SetPollingEnginePassiveId: function (id) { pollingEnginePassiveId = id; },
        SetAdvancedPassiveCheckboxId: function (id) { advancedPassiveCheckboxId = id; },
        SetAdvancedActiveCheckboxId: function (id) { advancedActiveCheckboxId = id; },
        SetUpdateRemoteSettingsCheckboxId: function (id) { updateRemoteSettingsCheckboxId = id; },
        SetAgentStatusBoxId: function (id) { agentStatusBoxId = id; },
        SetAgentStatusMessageBoxId: function (id) { agentStatusMessageBoxId = id; },
        SetSavePostBack: function (postback) { savePostBack = postback; },
        SetOriginalAgentIpBoxId: function (id) { originalAgentIpBoxId = id; },
        SetOriginalAgentDnsNameBoxId: function (id) { originalAgentDnsNameBoxId = id; },
        SetOriginalAgentPollingEngineIdId: function (id) { originalAgentPollingEngineIdId = id; },
        SetAgentVersionFromTestId: function (id) { agentVersionFromTestId = id; },
        SetProxySettingsListId: function (id) { proxySettingsListId = id; }
    };
} ();
