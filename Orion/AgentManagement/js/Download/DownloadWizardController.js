/// <reference path="../../../typescripts/typings/orionminregs.d.ts" />
/// <reference path="../../../typescripts/typings/jquery.d.ts" />
/// <reference path="dynamicvaluebinder.ts" />
var AgentManagement;
(function (AgentManagement) {
    var DownloadWizardController = (function () {
        function DownloadWizardController() {
            var _this = this;
            this.agentModeBinding = 'agentMode';
            this.connectionSpecBinding = 'connectionSpec';
            this.manualHostnameBinding = 'manualHostname';
            this.manualIpBinding = 'manualIp';
            this.proxyTypeBinding = 'proxyType';
            this.agentProxyHostnameBinding = 'agentProxyHostname';
            this.agentProxyPortBinding = 'agentProxyPort';
            this.agentProxyUsernameBinding = 'agentProxyUsername';
            this.agentProxyPasswordBinding = 'agentProxyPassword';
            this.agentCommunicationPortBinding = 'agentCommunicationPort';
            this.pollingEngineIpBinding = 'pollingEngineIp';
            this.msiInstallerVersionBinding = 'msiInstallerVersion';
            this.machineTypeBinding = 'machineType';
            this.deploymentMethodBinding = 'deploymentMethod';
            this.linuxManualInstallCommandBinding = 'linuxManualInstallCommand';
            this.linuxPackageBinding = 'linuxPackage';
            this.linuxPackageVersionBinding = 'linuxPackageVersion';
            this.linuxPackageAddRepositoryCommandBinding = 'linuxPackageAddRepositoryCommand';
            this.linuxPackageInstallCommandBinding = 'linuxPackageInstallCommand';
            this.linuxPackageDropdownId = 'AMS_LinuxPackageDropdown';
            this.enginesDropdownId = 'AMS_EnginesDropdown';
            this.missingProxyHostnameError = '@{R=AgentManagement.Strings;K=DownloadAgent_MissingProxyHostnameError;E=js}';
            this.invalidProxyPortError = '@{R=AgentManagement.Strings;K=DownloadAgent_InvalidProxyPortError;E=js}';
            this.missingPollerHostnameError = '@{R=AgentManagement.Strings;K=DownloadAgent_MissingPollerHostnameError;E=js}';
            this.missingPollerIpError = '@{R=AgentManagement.Strings;K=DownloadAgent_MissingPollerIpError;E=js}';
            this.wrongAgentPortValueError = '@{R=AgentManagement.Strings;K=DownloadAgent_WrongPortValueError;E=js}';
            this.errorMessageTitle = '@{R=AgentManagement.Strings;K=DownloadAgent_ErrorMessageTitle;E=js}';
            this.incorrectInputValuesError = '@{R=AgentManagement.Strings;K=DownloadAgent_IncorrectInputValuesError;E=js}';
            this.errorGeneratingCommandError = '@{R=AgentManagement.Strings;K=DownloadAgent_ErrorGeneratingCommand;E=js}';
            this.demoModeMessage = '@{R=AgentManagement.Strings;K=AgentManagement_FunctionalityDisabledInDemoText;E=js}';
            this.apiRelativePath = '/api/AgentManagementDownloadAgent/';
            this.getDownloadPageDataAction = 'GetDownloadPageData';
            this.downloadMstAction = 'DownloadMstFile';
            this.downloadWindowsOfflineInstallerAction = 'DownloadWindowsOfflineInstaller';
            this.generateLinuxOnlineInstallCommandAction = 'GenerateLinuxOnlineInstallCommand';
            this.downloadLinuxOfflineInstallerAction = 'DownloadLinuxOfflineInstaller';
            this.generateLinuxPackageCommandsAction = 'GenerateLinuxPackageCommands';
            this.loadingValue = 'loading';
            this.linuxPackageDeploymentType = 'LinuxPackage';
            this.valueBinder = new AgentManagement.DynamicValueBinder();
            this.valueBinder.registerBindingChangeCallback(this.machineTypeBinding, function (bindingName, oldValue, newValue) { return _this.onMachineTypeChanged(oldValue, newValue); });
            this.valueBinder.registerBindingChangeCallback(this.deploymentMethodBinding, function (bindingName, oldValue, newValue) { return _this.onDeploymentMethodChanged(oldValue, newValue); });
            this.valueBinder.registerBindingChangeCallback(this.linuxPackageBinding, function (bindingName, oldValue, newValue) { return _this.onLinuxOsChanged(oldValue, newValue); });
            this.valueBinder.registerBindingChangeCallback('', function (bindingName, oldValue, newValue) { return _this.clearGeneratedCommands(); });
            this.valueBinder.setBindingValue('MachineType', 'Windows');
            this.loadInitialData();
        }
        DownloadWizardController.prototype.downloadMstFile = function () {
            this.downloadFile(this.downloadMstAction);
        };
        DownloadWizardController.prototype.downloadWindowsOfflineInstaller = function () {
            this.downloadFile(this.downloadWindowsOfflineInstallerAction);
        };
        DownloadWizardController.prototype.downloadFile = function (actionName) {
            if (this.checkDemoAndShowMessage())
                return;
            var request = this.getDownloadRequestData();
            if (!this.validate(request))
                return;
            // File download can't be done via AJAX, we need to navigate to the download URL. That means that we can pass data only via URL.
            // Because passing complex types to WebApi from URL is not easy let's just pass JSON string and deserialize it on the other side.
            window.location = (this.getApiUrl(actionName) + '?requestJson=' + encodeURIComponent(JSON.stringify(request)));
        };
        DownloadWizardController.prototype.generateLinuxOnlineInstallCommand = function () {
            var _this = this;
            if (this.checkDemoAndShowMessage())
                return;
            var request = this.getDownloadRequestData();
            if (!this.validate(request)) {
                this.valueBinder.setBindingValue(this.linuxManualInstallCommandBinding, '');
                this.valueBinder.setBindingError(this.linuxManualInstallCommandBinding, this.incorrectInputValuesError);
                return;
            }
            // special command value to show loading spinner
            this.valueBinder.setBindingValue(this.linuxManualInstallCommandBinding, this.loadingValue);
            SW.Core.Services.callController(this.getApiUrl(this.generateLinuxOnlineInstallCommandAction), request, function (result) {
                var data = result;
                _this.linuxInstallScriptUrl = data.ScriptUrl;
                _this.valueBinder.setBindingValue(_this.linuxManualInstallCommandBinding, data.Command);
                _this.valueBinder.setBindingValue(_this.linuxPackageVersionBinding, data.AgentVersion);
            }, function (errorResult) {
                if (typeof (errorResult) === 'undefined') {
                    _this.valueBinder.setBindingError(_this.linuxManualInstallCommandBinding, _this.errorGeneratingCommandError);
                }
                else {
                    _this.valueBinder.setBindingError(_this.linuxManualInstallCommandBinding, errorResult);
                }
            });
        };
        DownloadWizardController.prototype.showLinuxInstallScript = function () {
            if (this.checkDemoAndShowMessage())
                return;
            window.location = this.linuxInstallScriptUrl;
        };
        DownloadWizardController.prototype.downloadLinuxOfflineInstaller = function () {
            this.downloadFile(this.downloadLinuxOfflineInstallerAction);
        };
        DownloadWizardController.prototype.generateLinuxPackageCommands = function () {
            var _this = this;
            if (this.isDemoMode()) {
                this.valueBinder.setBindingValue(this.linuxPackageAddRepositoryCommandBinding, this.demoModeMessage);
                this.valueBinder.setBindingValue(this.linuxPackageInstallCommandBinding, this.demoModeMessage);
                return;
            }
            var request = this.getDownloadRequestData();
            // special command value to show loading spinner
            this.valueBinder.setBindingValue(this.linuxPackageAddRepositoryCommandBinding, this.loadingValue);
            this.valueBinder.setBindingValue(this.linuxPackageInstallCommandBinding, this.loadingValue);
            SW.Core.Services.callController(this.getApiUrl(this.generateLinuxPackageCommandsAction), request.LinuxPackageId, function (result) {
                var data = result;
                _this.valueBinder.setBindingValue(_this.linuxPackageAddRepositoryCommandBinding, data.AddRepositoryCommand);
                _this.valueBinder.setBindingValue(_this.linuxPackageInstallCommandBinding, data.InstallCommand);
            }, function (errorResult) {
                if (typeof (errorResult) === 'undefined') {
                    _this.valueBinder.setBindingError(_this.linuxPackageAddRepositoryCommandBinding, _this.errorGeneratingCommandError);
                    _this.valueBinder.setBindingError(_this.linuxPackageInstallCommandBinding, _this.errorGeneratingCommandError);
                }
                else {
                    _this.valueBinder.setBindingError(_this.linuxPackageAddRepositoryCommandBinding, errorResult);
                    _this.valueBinder.setBindingError(_this.linuxPackageInstallCommandBinding, errorResult);
                }
            });
        };
        DownloadWizardController.prototype.clearGeneratedCommands = function () {
            this.valueBinder.setBindingValue(this.linuxManualInstallCommandBinding, '');
        };
        DownloadWizardController.prototype.isActiveAgent = function () {
            return this.valueBinder.getBindingValue(this.agentModeBinding) == 'active';
        };
        DownloadWizardController.prototype.usesExistingEngine = function () {
            return this.valueBinder.getBindingValue(this.connectionSpecBinding) == 'engine';
        };
        DownloadWizardController.prototype.manualHostname = function () {
            return this.valueBinder.getBindingValue(this.manualHostnameBinding);
        };
        DownloadWizardController.prototype.setManualHostnameError = function (error) {
            this.valueBinder.setBindingError(this.manualHostnameBinding, error);
        };
        DownloadWizardController.prototype.manualIp = function () {
            return this.valueBinder.getBindingValue(this.manualIpBinding);
        };
        DownloadWizardController.prototype.setManualIpError = function (error) {
            this.valueBinder.setBindingError(this.manualIpBinding, error);
        };
        DownloadWizardController.prototype.proxyType = function () {
            return this.valueBinder.getBindingValue(this.proxyTypeBinding);
        };
        DownloadWizardController.prototype.proxyHostname = function () {
            return this.valueBinder.getBindingValue(this.agentProxyHostnameBinding);
        };
        DownloadWizardController.prototype.setProxyHostnameError = function (error) {
            this.valueBinder.setBindingError(this.agentProxyHostnameBinding, error);
        };
        DownloadWizardController.prototype.proxyPort = function () {
            return parseInt(this.valueBinder.getBindingValue(this.agentProxyPortBinding));
        };
        DownloadWizardController.prototype.setProxyPortError = function (error) {
            this.valueBinder.setBindingError(this.agentProxyPortBinding, error);
        };
        DownloadWizardController.prototype.proxyUsername = function () {
            return this.valueBinder.getBindingValue(this.agentProxyUsernameBinding);
        };
        DownloadWizardController.prototype.proxyPassword = function () {
            return this.valueBinder.getBindingValue(this.agentProxyPasswordBinding);
        };
        DownloadWizardController.prototype.agentCommunicationPort = function () {
            return parseInt(this.valueBinder.getBindingValue(this.agentCommunicationPortBinding));
        };
        DownloadWizardController.prototype.setAgentCommunicationPortError = function (error) {
            this.valueBinder.setBindingError(this.agentCommunicationPortBinding, error);
        };
        DownloadWizardController.prototype.linuxPackageId = function () {
            return this.valueBinder.getBindingValue(this.linuxPackageBinding);
        };
        DownloadWizardController.prototype.onMachineTypeChanged = function (oldMachineType, newMachineType) {
            // preselect proper deployment method based on machine type
            if (newMachineType.indexOf('Linux') > -1) {
                this.valueBinder.setBindingValue(this.deploymentMethodBinding, 'LinuxManual');
            }
            else {
                this.valueBinder.setBindingValue(this.deploymentMethodBinding, 'WindowsManual');
            }
        };
        DownloadWizardController.prototype.onDeploymentMethodChanged = function (oldMethod, newMethod) {
            // refresh commands for Linux package deployment
            if (newMethod === this.linuxPackageDeploymentType) {
                this.generateLinuxPackageCommands();
            }
        };
        DownloadWizardController.prototype.onLinuxOsChanged = function (oldMethod, newMethod) {
            // refresh commands for Linux package deployment
            if (this.valueBinder.getBindingValue(this.deploymentMethodBinding) === this.linuxPackageDeploymentType) {
                this.generateLinuxPackageCommands();
            }
        };
        DownloadWizardController.prototype.getDownloadRequestData = function () {
            var request = new FileDownloadRequest();
            request.IsActiveAgent = this.isActiveAgent();
            if (request.IsActiveAgent) {
                if (this.usesExistingEngine()) {
                    var selectedEngine = this.getSelectedEngine();
                    request.Hostname = selectedEngine.Name;
                    request.IpAddress = selectedEngine.IpAddress;
                }
                else {
                    request.Hostname = this.manualHostname();
                    request.IpAddress = this.manualIp();
                }
                request.ProxyInfo.AccessType = this.proxyType();
                if (request.ProxyInfo.AccessType == ProxyAccessType.UserProvided) {
                    request.ProxyInfo.HostnameOrIp = this.proxyHostname();
                    request.ProxyInfo.Port = this.proxyPort();
                }
                request.ProxyInfo.Username = this.proxyUsername();
                request.ProxyInfo.Password = this.proxyPassword();
            }
            else {
                request.AgentCommunicationPort = this.agentCommunicationPort();
                // passive agent does not in fact need poller but all code expects it
                var selectedEngine = this.getSelectedEngine();
                request.Hostname = selectedEngine.Name;
                request.IpAddress = selectedEngine.IpAddress;
            }
            request.LinuxPackageId = this.linuxPackageId();
            return request;
        };
        DownloadWizardController.prototype.validate = function (request) {
            var errors = [];
            if (request.IsActiveAgent) {
                if (request.Hostname == '') {
                    errors.push(this.missingPollerHostnameError);
                    this.setManualHostnameError(this.missingPollerHostnameError);
                }
                if (request.IpAddress == '') {
                    errors.push(this.missingPollerIpError);
                    this.setManualIpError(this.missingPollerIpError);
                }
                if (request.ProxyInfo.AccessType == ProxyAccessType.UserProvided) {
                    if (request.ProxyInfo.HostnameOrIp == '') {
                        errors.push(this.missingProxyHostnameError);
                        this.setProxyHostnameError(this.missingProxyHostnameError);
                    }
                    if (request.ProxyInfo.Port < 1 || request.ProxyInfo.Port > 65535 || isNaN(request.ProxyInfo.Port)) {
                        errors.push(this.invalidProxyPortError);
                        this.setProxyPortError(this.invalidProxyPortError);
                    }
                }
            }
            else {
                if (request.AgentCommunicationPort == 0 || isNaN(request.AgentCommunicationPort)) {
                    errors.push(this.wrongAgentPortValueError);
                    this.setAgentCommunicationPortError(this.wrongAgentPortValueError);
                }
            }
            if (errors.length > 0) {
                this.showErrorDialog(errors.join('<br />'));
            }
            return errors.length == 0;
        };
        DownloadWizardController.prototype.showErrorDialog = function (error) {
            Ext.MessageBox.show({
                title: this.errorMessageTitle,
                msg: error,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
        };
        DownloadWizardController.prototype.onEngineChanged = function () {
            this.valueBinder.setBindingValue(this.pollingEngineIpBinding, this.getSelectedEngine().IpAddress);
        };
        DownloadWizardController.prototype.getSelectedEngine = function () {
            return ($("#" + this.enginesDropdownId + " option:selected").data('engine'));
        };
        DownloadWizardController.prototype.getSelectedLinuxPackage = function () {
            return ($("#" + this.linuxPackageDropdownId + " option:selected").data('installPackage'));
        };
        DownloadWizardController.prototype.updateEnginesDropdown = function (engines) {
            var _this = this;
            var dropdown = $("#" + this.enginesDropdownId);
            $.each(engines, function (index, engine) {
                var option = $('<option></option>');
                option.val(engine.ID);
                option.text(engine.Name);
                option.data('engine', engine);
                dropdown.append(option);
            });
            // preselect first engine
            dropdown.val(dropdown.find('option:first').val());
            dropdown.change(function () { return _this.onEngineChanged(); });
            dropdown.change();
        };
        DownloadWizardController.prototype.updateLinuxPackagesDropdown = function (packages) {
            var dropdown = $("#" + this.linuxPackageDropdownId);
            $.each(packages, function (index, item) {
                var option = $('<option></option>');
                option.val(item.PackageId);
                option.text(item.Label);
                option.data('installPackage', item);
                dropdown.append(option);
            });
            // preselect first item
            dropdown.val(dropdown.find('option:first').val());
            dropdown.change();
        };
        DownloadWizardController.prototype.loadInitialData = function () {
            var _this = this;
            SW.Core.Services.callController(this.getApiUrl(this.getDownloadPageDataAction), {}, function (result) {
                var data = result;
                _this.valueBinder.setBindingValue(_this.msiInstallerVersionBinding, data.WindowsInstallerVersion);
                _this.updateEnginesDropdown(data.Engines);
                _this.updateLinuxPackagesDropdown(data.LinuxDistributions);
            }, function (errorResult) {
                _this.showErrorDialog(errorResult);
            });
        };
        DownloadWizardController.prototype.getApiUrl = function (actioName) {
            return this.apiRelativePath + actioName;
        };
        DownloadWizardController.prototype.isDemoMode = function () {
            return $("#isOrionDemoMode").length > 0;
        };
        DownloadWizardController.prototype.checkDemoAndShowMessage = function () {
            if (!this.isDemoMode())
                return false;
            alert(this.demoModeMessage);
            return true;
        };
        return DownloadWizardController;
    })();
    AgentManagement.DownloadWizardController = DownloadWizardController;
    var FileDownloadRequest = (function () {
        function FileDownloadRequest() {
            this.ProxyInfo = new ProxyInfo();
        }
        return FileDownloadRequest;
    })();
    var ProxyInfo = (function () {
        function ProxyInfo() {
        }
        return ProxyInfo;
    })();
    var LinuxInstallCommandResponse = (function () {
        function LinuxInstallCommandResponse() {
        }
        return LinuxInstallCommandResponse;
    })();
    var LinuxPackageCommandsResponse = (function () {
        function LinuxPackageCommandsResponse() {
        }
        return LinuxPackageCommandsResponse;
    })();
    // must be same as ProxyAccessType in ProxyInfo.cs
    var ProxyAccessType;
    (function (ProxyAccessType) {
        ProxyAccessType[ProxyAccessType["Disabled"] = 0] = "Disabled";
        ProxyAccessType[ProxyAccessType["UseDefault"] = 1] = "UseDefault";
        ProxyAccessType[ProxyAccessType["UseAutoDiscovery"] = 2] = "UseAutoDiscovery";
        ProxyAccessType[ProxyAccessType["UserProvided"] = 3] = "UserProvided";
    })(ProxyAccessType || (ProxyAccessType = {}));
    ;
})(AgentManagement || (AgentManagement = {}));
