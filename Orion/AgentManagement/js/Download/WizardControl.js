/// <reference path="../../../typescripts/typings/jquery.d.ts" />
var AgentManagement;
(function (AgentManagement) {
    var Wizard = (function () {
        function Wizard(wizardContainer, backButtonId, nextButtonId, cancelButtonId, doneButtonId) {
            var _this = this;
            this.wizardContainer = wizardContainer;
            this.wizardSteps = [];
            this.currentIndex = 0;
            this.backButton = $('#' + backButtonId);
            this.backButton.click(function () { return _this.back(); });
            this.nextButton = $('#' + nextButtonId);
            this.nextButton.click(function () { return _this.next(); });
            this.cancelButton = $('#' + cancelButtonId);
            this.cancelButton.click(function () { return _this.cancel(); });
            this.doneButton = $('#' + doneButtonId);
            this.doneButton.click(function () { return _this.cancel(); });
            wizardContainer.find('.AgentManagement_WizardStep').each(function (index, element) {
                var step = new WizardStep($(element));
                step.hide();
                _this.wizardSteps.push(step);
            });
            if (this.wizardSteps.length == 0)
                return;
            this.wizardSteps[0].show();
            this.progressIndicatorElement = $('<div></div>').addClass('ProgressIndicator');
            wizardContainer.prepend(this.progressIndicatorElement);
            this.refreshSteps();
        }
        Wizard.prototype.refreshProgressIndicator = function () {
            var _this = this;
            var separatorOnOff = $('<img src="/Orion/Images/ProgressIndicator/PI_sep_on_off_sm.gif" />');
            var separatorOffOff = $('<img src="/Orion/Images/ProgressIndicator/PI_sep_off_off_sm.gif" />');
            var separatorOffOn = $('<img src="/Orion/Images/ProgressIndicator/PI_sep_off_on_sm.gif" />');
            this.progressIndicatorElement.empty();
            this.wizardSteps.forEach(function (step, index) {
                _this.progressIndicatorElement.append(step.progressIndicatorElement);
                var nextStep = (index < _this.wizardSteps.length - 1) ? _this.wizardSteps[index + 1] : WizardStep.NoStep;
                if (step.isVisible() && !nextStep.isVisible()) {
                    _this.progressIndicatorElement.append(separatorOnOff.clone());
                }
                else if (!step.isVisible() && nextStep.isVisible()) {
                    _this.progressIndicatorElement.append(separatorOffOn.clone());
                }
                else {
                    _this.progressIndicatorElement.append(separatorOffOff.clone());
                }
            });
        };
        Wizard.prototype.back = function () {
            if (this.currentIndex <= 0)
                return;
            this.currentIndex--;
            this.refreshSteps();
        };
        Wizard.prototype.next = function () {
            if (this.currentIndex >= this.wizardSteps.length - 1)
                return;
            this.currentIndex++;
            this.refreshSteps();
        };
        Wizard.prototype.cancel = function () {
            location.reload();
        };
        Wizard.prototype.refreshSteps = function () {
            var _this = this;
            this.wizardSteps.forEach(function (step, index) {
                if (_this.currentIndex == index) {
                    step.show();
                }
                else {
                    step.hide();
                }
            });
            this.refreshProgressIndicator();
            this.refreshButtons();
        };
        Wizard.prototype.refreshButtons = function () {
            (this.currentIndex == 0) ? this.backButton.hide() : this.backButton.show();
            if (this.currentIndex == this.wizardSteps.length - 1) {
                this.nextButton.hide();
                this.cancelButton.hide();
                this.doneButton.show();
            }
            else {
                this.nextButton.show();
                this.cancelButton.show();
                this.doneButton.hide();
            }
        };
        return Wizard;
    })();
    AgentManagement.Wizard = Wizard;
    var WizardStep = (function () {
        function WizardStep(element) {
            this.element = element;
            var titleElement = element.find('.AgentManagement_WizardStepTitle');
            titleElement.hide();
            this.progressIndicatorElement = $('<div></div>').html('&nbsp;' + titleElement.text());
        }
        WizardStep.prototype.show = function () {
            this.progressIndicatorElement.removeClass('PI_off');
            this.progressIndicatorElement.addClass('PI_on');
            this.element.show();
        };
        WizardStep.prototype.hide = function () {
            this.progressIndicatorElement.removeClass('PI_on');
            this.progressIndicatorElement.addClass('PI_off');
            this.element.hide();
        };
        WizardStep.prototype.isVisible = function () {
            return this.element.is(':visible');
        };
        WizardStep.NoStep = new WizardStep($('<div></div>').hide());
        return WizardStep;
    })();
})(AgentManagement || (AgentManagement = {}));
