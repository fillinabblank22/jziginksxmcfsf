/// <reference path="../../../typescripts/typings/jquery.d.ts" />
var AgentManagement;
(function (AgentManagement) {
    /**
     * This class implements very simple binding system to help with binding of model and view.
     *
     * Binding value source can be any form element with custom attribute: data-ams-binding-source="BINDING_NAME"
     * When value of this element changes, the binding with name BINDING_NAME is updated to contain that value.
     *
     * Binding can be used to control element visibility. Any element can have custom attribute in format:
     *   data-ams-BINDING_NAME-show="BINDING_VALUES" or data-ams-BINDING_NAME-hide="BINDING_VALUES"
     *
     * When binding value changes, all elements marked with "show" attribute and matching BINDING_VALUES in it will be displayed.
     * When binding value changes, all elements marked with "hide" attribute and matching BINDING_VALUES in it will be hidden.
     * BINDING_VALUES can be single value or multiple values separated by |.
     *
     * Usage example:
     *   <input type="radio" name="group" data-ams-binding-source="myValue" value="yes" />
     *   <input type="radio" name="group" data-ams-binding-source="myValue" value="no" />
     *
     *   <dic data-ams-myvalue-show="yes">This will be visible when YES radio is selected.</div>
     *   <dic data-ams-myvalue-show="no">This will be visible when NO radio is selected.</div>
     *   <dic data-ams-myvalue-hide="no">This will be hidden when NO radio is selected.</div>
     */
    var DynamicValueBinder = (function () {
        function DynamicValueBinder() {
            this.values = {};
            this.changeCallbacks = {};
            this.processingChangeCallback = {};
            this.allBindingsKey = '$ALL_BINDINGS$';
            this.registerBindings();
        }
        DynamicValueBinder.prototype.getBindingValue = function (bindingName) {
            return this.values[bindingName];
        };
        DynamicValueBinder.prototype.setBindingValue = function (bindingName, value) {
            this.values[bindingName] = value;
            var valueSources = $("[data-ams-binding-source='" + bindingName + "']");
            valueSources.each(function (index, element) {
                var jqElement = $(element);
                if (jqElement.is('input[type="text"],textarea,select')) {
                    jqElement.val(value);
                }
                else if (jqElement.is('input[type="radio"],input[type="checkbox"]') && jqElement.val() == value) {
                    jqElement.prop('checked', 'checked');
                }
            });
            this.refreshBinding(bindingName);
        };
        DynamicValueBinder.prototype.setBindingError = function (bindingName, error) {
            $('[data-ams-' + bindingName + '-error]').each(function (index, element) {
                if (error == '') {
                    $(element).text('');
                    $(element).hide();
                }
                else {
                    $(element).text(error);
                    $(element).show();
                }
            });
        };
        /**
         * Register callback for particular binding change. If empty binging name is passed the callback is for all changes in all bindings.
         */
        DynamicValueBinder.prototype.registerBindingChangeCallback = function (bindingName, callback) {
            if (bindingName == '') {
                bindingName = this.allBindingsKey;
            }
            this.changeCallbacks[bindingName] = callback;
        };
        DynamicValueBinder.prototype.refreshBinding = function (bindingName) {
            if (typeof (bindingName) === 'undefined' || bindingName == null)
                return;
            var oldValue = this.values[bindingName];
            this.loadBindingValue(bindingName);
            this.setBindingError(bindingName, '');
            this.updateBoundVisibility(bindingName);
            this.updateBoundValues(bindingName);
            if (oldValue != this.values[bindingName] && !this.processingChangeCallback[bindingName]) {
                this.processingChangeCallback[bindingName] = true;
                if (typeof (this.changeCallbacks[bindingName]) === "function") {
                    this.changeCallbacks[bindingName](bindingName, oldValue, this.values[bindingName]);
                }
                if (typeof (this.changeCallbacks[this.allBindingsKey]) === "function") {
                    this.changeCallbacks[this.allBindingsKey](bindingName, oldValue, this.values[bindingName]);
                }
                this.processingChangeCallback[bindingName] = false;
            }
        };
        DynamicValueBinder.prototype.registerBindings = function () {
            var _this = this;
            $('[data-ams-binding-source]').each(function (index, element) {
                var jqElement = $(element);
                var bindingName = jqElement.attr('data-ams-binding-source');
                _this.values[bindingName] = null;
                jqElement.change(function () { return _this.refreshBinding(bindingName); });
                _this.refreshBinding(bindingName);
            });
        };
        DynamicValueBinder.prototype.loadBindingValue = function (bindingName) {
            var _this = this;
            var valueSources = $("[data-ams-binding-source='" + bindingName + "']");
            if (valueSources.length == 0)
                return;
            this.values[bindingName] = null;
            valueSources.each(function (index, element) {
                var jqElement = $(element);
                if (_this.isBoundElementProvidingValue(jqElement)) {
                    _this.values[bindingName] = jqElement.val();
                }
            });
        };
        DynamicValueBinder.prototype.isBoundElementProvidingValue = function (element) {
            // some elements provide direct value, others as checkboxes and radios need to be checked to provide value
            return element.is('textarea,select,input[type="text"],input[type="password"],:checked');
        };
        DynamicValueBinder.prototype.updateBoundVisibility = function (bindingName) {
            var _this = this;
            $('[data-ams-' + bindingName + '-show]').each(function (index, element) {
                var jqElement = $(element);
                var showVal = jqElement.attr('data-ams-' + bindingName + '-show');
                _this.doIfValuesStringContainsValue(showVal, _this.values[bindingName], function () { return jqElement.show(); }, function () { return jqElement.hide(); });
            });
            $('[data-ams-' + bindingName + '-hide]').each(function (index, element) {
                var jqElement = $(element);
                var hideVal = jqElement.attr('data-ams-' + bindingName + '-hide');
                _this.doIfValuesStringContainsValue(hideVal, _this.values[bindingName], function () { return jqElement.hide(); }, function () { return jqElement.show(); });
            });
        };
        DynamicValueBinder.prototype.updateBoundValues = function (bindingName) {
            var _this = this;
            $('[data-ams-' + bindingName + '-value]').each(function (index, element) {
                $(element).text(_this.values[bindingName]);
            });
        };
        /**
         * Check if "valuesString" contains search value in discrete form.
         *
         * @param valuesString String containing one value or multiple values separated by |
         * @param searchValue Value to search for
         * @param ifMatchesCallback Callback executed if valuesString contains search value as discrete value
         * @param ifNotMatchesCallback Callback executed if valuesString does not contain search value as discrete value
         */
        DynamicValueBinder.prototype.doIfValuesStringContainsValue = function (valuesString, searchValue, ifMatchesCallback, ifNotMatchesCallback) {
            var values = valuesString.split('|');
            var found = false;
            values.forEach(function (val) {
                if (searchValue == val) {
                    ifMatchesCallback();
                    found = true;
                }
            });
            if (!found) {
                ifNotMatchesCallback();
            }
        };
        return DynamicValueBinder;
    })();
    AgentManagement.DynamicValueBinder = DynamicValueBinder;
})(AgentManagement || (AgentManagement = {}));
