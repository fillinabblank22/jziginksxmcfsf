﻿using SolarWinds.AgentManagement.Common;
using SolarWinds.AgentManagement.Common.Models;
using SolarWinds.AgentManagement.Web.Exceptions;
using SolarWinds.Orion.Web;
using System;
using System.Web;
using System.Web.UI.WebControls;

public partial class Orion_AgentManagement_Admin_Settings : System.Web.UI.Page, IBypassAccessLimitation
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    private void LoadData()
    {
        using (var proxy = ControlServiceFactory.Create())
        {
            var settings = proxy.GetAgentManagementSettings();
            this.chbAllowAutoregistration.Checked =
                settings.GetSettingValue<bool>(AgentManagementSettings.SettingKeys.AutoApproveAgents);
            this.chbAllowAutoregistration.Attributes["defaultValue"] =
                settings.GetDefaultSettingValue<bool>(AgentManagementSettings.SettingKeys.AutoApproveAgents).ToString();

            this.chbAllowAutomaticUpdates.Checked =
                settings.GetSettingValue<bool>(AgentManagementSettings.SettingKeys.AutoUpdateAgents);
            this.chbAllowAutomaticUpdates.Attributes["defaultValue"] =
                settings.GetDefaultSettingValue<bool>(AgentManagementSettings.SettingKeys.AutoUpdateAgents).ToString();

            this.chbAllowAutoNodeCreate.Checked =
                settings.GetSettingValue<bool>(AgentManagementSettings.SettingKeys.AutoCreateNode);
            this.chbAllowAutoNodeCreate.Attributes["defaultValue"] =
                settings.GetDefaultSettingValue<bool>(AgentManagementSettings.SettingKeys.AutoCreateNode).ToString();

            this.ibHowLongIsAgentDisplayedAsNew.Value = settings.HowLongIsAgentDisplayedAsNew.ToString();
            this.ibHowLongIsAgentDisplayedAsNew.Attributes["defaultValue"] =
                settings.HowLongIsAgentDisplayedAsNew.ToString();

            ibHowLongIsAgentDisplayedAsNewRangeValidator.Type = ValidationDataType.Integer;
            ibHowLongIsAgentDisplayedAsNewRangeValidator.MinimumValue = "0";
            ibHowLongIsAgentDisplayedAsNewRangeValidator.MaximumValue = "168";
            ibHowLongIsAgentDisplayedAsNewRangeValidator.EnableClientScript = true;
            ibHowLongIsAgentDisplayedAsNewRangeValidator.Text = String.Format(Resources.AgentManagementWebContent.HowLongIsAgentDisplayedAsNewSetting_RangeErrorMessage,
                ibHowLongIsAgentDisplayedAsNewRangeValidator.MinimumValue,
                ibHowLongIsAgentDisplayedAsNewRangeValidator.MaximumValue);

            ibHowLongIsAgentDisplayedAsNewRequiredFieldValidator.Text =
                Resources.AgentManagementWebContent.HowLongIsAgentDisplayedAsNewSetting_RequiredFieldErrorMessage;
        }
    }

    protected void ImgbtnSubmitClick(object sender, EventArgs e)
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowAdmin)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                throw new AgentManagementUnauthorizedAccessException();
            else
                return;
        }

        if (!Page.IsValid)
            return;

        using (var proxy = ControlServiceFactory.Create())
        {
            var settings = new AgentManagementSettings(new[]
                    {
                        new AgentManagementSetting(AgentManagementSettings.SettingKeys.AutoUpdateAgents, chbAllowAutomaticUpdates.Checked ? 1 : 0),
                        new AgentManagementSetting(AgentManagementSettings.SettingKeys.AutoApproveAgents, chbAllowAutoregistration.Checked ? 1 : 0),
                        new AgentManagementSetting(AgentManagementSettings.SettingKeys.AutoCreateNode, chbAllowAutoNodeCreate.Checked ? 1 : 0),
                        new AgentManagementSetting(AgentManagementSettings.SettingKeys.HowLongIsAgentDisplayedAsNew, int.Parse(ibHowLongIsAgentDisplayedAsNew.Value)),
                    });
            proxy.UpdateAgentManagementSettings(settings);
        }

        GoBack();
    }

    protected void ImgbtnCancelClick(object sender, EventArgs e)
    {
        GoBack();
    }

    private void GoBack()
    {
        Response.Redirect("/Orion/AgentManagement/Admin");
    }
}