using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.AgentManagement.Contract;
using SolarWinds.AgentManagement.Contract.Models;
using SolarWinds.AgentManagement.Web.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.AgentManagement.Common;
using SolarWinds.AgentManagement.Web.DAL;
using SolarWinds.AgentManagement.Common.Models;
using System.Linq;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using System.Security;

// TODO: [JT] This page is a mess now because it combines Active and Passive agents at they were in WPM. 
// However, current version of Agent Management has only active agent and with limited functionality compared to WPM.
// I leave the stuff commented or unused here by purpose so that it can be used once we add passive agent.
// If it won't be needed, it can be removed later.

public partial class Orion_AgentManagement_Admin_EditAgent : System.Web.UI.Page, IBypassAccessLimitation
{
    private static readonly Log log = new Log();
    private readonly CredentialManager _credentialManager = new CredentialManager();

    protected readonly string AddPageTitle = Resources.AgentManagementWebContent.AddAgentPageTitle;
    protected readonly string EditPageTitle = Resources.AgentManagementWebContent.EditAgentPageTitle;
    protected readonly string MainDescription = Resources.AgentManagementWebContent.AddEditAgentPageDescription;

    protected readonly string NameLabel = Resources.AgentManagementWebContent.AddEditAgentName;
    protected readonly string CommunicationTypeLabel = Resources.AgentManagementWebContent.AgentCommunicationTypeLabel;

    protected readonly string PassiveRadioLabel = Resources.AgentManagementWebContent.PassiveRadioLabel;
    protected readonly string ActiveRadioLabel = Resources.AgentManagementWebContent.ActiveRadioLabel;
    protected readonly string PassiveHelpText = Resources.AgentManagementWebContent.EditAgent_PassiveHelpText;
    // TODO: Define port number
    protected readonly string ActiveHelpText = Resources.AgentManagementWebContent.ActiveHelpText;
    protected readonly string AgentPasswordHelpText = Resources.AgentManagementWebContent.AgentPasswordHelpText;

    protected readonly string IPLabel = Resources.AgentManagementWebContent.EditAgent_IPLabel;
    protected readonly string AgentPasswordLabel = Resources.AgentManagementWebContent.EditAgent_AgentPasswordLabel;
    protected readonly string ProxyUrlLabel = Resources.AgentManagementWebContent.EditAgent_ProxyUrlLabel;

    protected readonly string AdvancedLabel = Resources.AgentManagementWebContent.EditAgent_AdvancedLabel;
    protected readonly string PortLabel = Resources.AgentManagementWebContent.EditAgent_PortLabel;
    protected readonly string PasswordLabel = Resources.AgentManagementWebContent.EditAgent_PasswordLabel;
    protected readonly string WhatIsThisLabel = Resources.AgentManagementWebContent.EditAgent_WhatIsThisLabel;
    protected readonly string WhatIsProxyLabel = Resources.AgentManagementWebContent.EditAgent_WhatIsProxyLabel;
    protected readonly string TroubleshootingLabel = Resources.AgentManagementWebContent.TroubleshootingLabel;
    protected readonly string LoadingText = Resources.AgentManagementWebContent.LoadingText;
    protected readonly string TestingText = Resources.AgentManagementWebContent.TestingText;
    protected readonly string LogLevelLabel = Resources.AgentManagementWebContent.LogLevelLabel;
    protected readonly string DiagnosticsLabel = Resources.AgentManagementWebContent.DiagnosticsLabel;
    protected readonly string GenerateNewDiagnosticsLabel = Resources.AgentManagementWebContent.GenerateNewDiagnosticsLabel;
    protected readonly string LocationNotRespondingError = Resources.AgentManagementWebContent.AgentNotRespondingError;
    protected readonly string AssignToPollerLabel = Resources.AgentManagementWebContent.EditAgent_AssignToPollerLabel;
    protected readonly string AssignedToPollerLabel = Resources.AgentManagementWebContent.EditAgent_AssignedToPollerLabel;
    protected readonly string AgentNotRespondingError = Resources.AgentManagementWebContent.EditAgent_AgentNotRespondingError;
    protected readonly string LearnMoreLabel = Resources.AgentManagementWebContent.EditAgent_LearnMoreLabel;
    protected readonly string ActiveAgentLabel = Resources.AgentManagementWebContent.EditAgent_ActiveAgentLabel;
    protected readonly string AgentGuidLabel = Resources.AgentManagementWebContent.EditAgent_AgentGuidLabel;
    protected readonly string UseProxyLabel = Resources.AgentManagementWebContent.EditAgent_UseProxyLabel;
    protected readonly string NewProxyItemText = Resources.AgentManagementWebContent.NewProxyItemText;

    protected readonly string AgentNameEmptyError = Resources.AgentManagementWebContent.EditAgent_AgentNameEmptyError;

    // GUID validator is for hidden guid field. User selects agent from combo box so validation message must refer to that box and not to GUID.
    protected readonly string AgentGuidEmptyError = Resources.AgentManagementWebContent.EditAgent_AgentGuidEmptyError;
    protected readonly string AgentGuidFormatError = Resources.AgentManagementWebContent.EditAgent_AgentGuidFormatError;

    protected readonly string PollingEngineEmptyError = Resources.AgentManagementWebContent.EditAgent_PollingEngineEmptyError;
    protected readonly string PollingEngineFormatError = Resources.AgentManagementWebContent.EditAgent_PollingEngineFormatError;
    protected readonly string AgentHostnameEmptyError = Resources.AgentManagementWebContent.EditAgent_AgentNameEmptyError;
    protected readonly string AgentHostnameFormatError = String.Format("{0}: ", Resources.AgentManagementWebContent.EditAgent_AgentHostnameFormatError);
    protected readonly string AgentPortError = Resources.AgentManagementWebContent.EditAgent_AgentPortError;
    protected readonly string AgentProxyUrlEmptyError = Resources.AgentManagementWebContent.EditAgent_AgentProxyUrlEmptyError;
    protected readonly string AgentProxyUserNameEmptyError = Resources.AgentManagementWebContent.EditAgent_AgentProxyUserNameEmptyError;
    protected readonly string AgentLocationExistsError = Resources.AgentManagementWebContent.EditAgent_AgentLocationExistsError;
    protected readonly string AdditionalPollerAssignedToLocalhostError = Resources.AgentManagementWebContent.EditAgent_AdditionalPollerAssignedToLocalhostError;
    
    protected readonly string TestNewPollerConnectionError = Resources.AgentManagementWebContent.TestNewPollerConnectionError;

    protected readonly string UseProxyAuthenticationLabel = Resources.AgentManagementWebContent.EditAgent_UseProxyAuthenticationLabel;
    protected readonly string ProxyUserNameLabel = Resources.AgentManagementWebContent.EditAgent_ProxyUserNameLabel;
    protected readonly string ProxyPasswordLabel = Resources.AgentManagementWebContent.EditAgent_ProxyPasswordLabel;

    protected readonly string TestPassiveAgentConnectionHelpText = Resources.AgentManagementWebContent.EditAgent_TestPassiveAgentConnectionHelpText;
    protected readonly string TestPassiveAgentLabel = Resources.AgentManagementWebContent.EditAgent_TestPassiveAgentLabel;

    protected readonly string PassiveAgentTroubleshootingHelpContext = "&nbsp;<a href='{0}' class='sw-link' target='_blank' rel='noopener noreferrer'><span class='LinkArrow'>&#0187;</span>{1}</a>"; 

    private const string savePostBackArgument = "saveAgent";

    //proxy
    private Boolean useProxyForConnectionToPassiveAgent = false;
    private ProxySetting formProxySetting = new ProxySetting();
    private string proxyUserName = String.Empty;
    private string proxyPassword = String.Empty;
    //!proxy

    private AgentRecord agent;
    private string returnUrl = null;
    // Main poller ID (Engines table).
    private static int mainPollerEngineId = 0;

    internal static Func<String, SecureString> DecryptPassword = 
        (sspwd) => new SolarWinds.Orion.Security.CryptoHelper().DecryptSecure(sspwd);

    internal static Func<SecureString, String> EncryptPassword =
        (ss) => new SolarWinds.Orion.Security.CryptoHelper().Encrypt(ss);

    SecureString agentConnectPassword = new SecureString();

    private void CheckPermissions()
    {
        if (!IsAdmin)
        {
            throw new AgentManagementUnauthorizedAccessException();
        }
    }

    private bool IsAdmin
    {
        get { return ((ProfileCommon)HttpContext.Current.Profile).AllowAdmin; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_AgentManagement_Admin_AgentManagementAdminPage)this.Master).HelpFragment = "OrionAgentEditAgent";

        if (!string.IsNullOrEmpty(Request["returnUrl"]))
            returnUrl = UrlHelper.FromSafeUrlParameter(Request["returnUrl"]);

        SavePostBack = Page.ClientScript.GetPostBackEventReference(this, savePostBackArgument);

        int agentId = 0;
        if (Int32.TryParse(Request["id"], out agentId))
        {
            CheckPermissions(); // in demo user shouldn't be able to edit Agent
            Page.Title = EditPageTitle;
            IsEditPage = true;
        }
        else
        {
            Page.Title = AddPageTitle;
            IsEditPage = false;
        }

        RunTest = false;
        RunTestAndSave = false;

        // setup validators
        nameValidator.ServerValidate += nameValidator_ServerValidate;
        nameValidator.ValidateEmptyText = true;
        nameValidator.CssClass = "AgentManagement_ValidationError";
        guidValidator.ServerValidate += guidValidator_ServerValidate;
        guidValidator.ValidateEmptyText = true;
        guidValidator.CssClass = "AgentManagement_ValidationError";
        passivePollerValidator.ServerValidate += pollerValidator_ServerValidate;
        passivePollerValidator.ValidateEmptyText = true;
        passivePollerValidator.CssClass = "AgentManagement_ValidationError";
        activePollerValidator.ServerValidate += pollerValidator_ServerValidate;
        activePollerValidator.ValidateEmptyText = true;
        activePollerValidator.CssClass = "AgentManagement_ValidationError";
        passiveAgentHostnameValidator.ServerValidate += passiveAgent_HostnameValidator_ServerValidate;
        passiveAgentHostnameValidator.ValidateEmptyText = true;
        passiveAgentHostnameValidator.CssClass = "AgentManagement_ValidationError";
        passiveAgentPortValidator.ServerValidate += passiveAgentPortValidator_ServerValidate;
        passiveAgentPortValidator.ValidateEmptyText = true;
        passiveAgentPortValidator.CssClass = "AgentManagement_ValidationError";
        proxyValidator.ServerValidate += proxyValidator_ServerValidate;
        proxyValidator.ValidateEmptyText = true;
        proxyValidator.CssClass = "AgentManagement_ValidationError";

        if (!Page.IsPostBack)
        {
            // Fill poller engines combo
            var pollingEngines = new AgentsDAL().GetAvailablePollingEngines();
            var engines = pollingEngines as IList<KeyValuePair<int, string>> ?? pollingEngines.ToList();
            if (!engines.Any())
                throw new InvalidOperationException("There is no available polling engine.");

            // Main poller is always the first in the list
            mainPollerEngineId = engines.ElementAt(0).Key;
            foreach (var kvp in engines)
            {
                pollingEnginePassive.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
                pollingEngineActive.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
            }

            //proxy settings
            proxySettingsList.Items.Add(new ListItem(NewProxyItemText, "0")); //add new item
            var proxySettings = new ProxyDAL().GetProxySettings();
            foreach (var p in proxySettings)
            {
                if (p.ProxyCredentialId != 0)
                {
                    //skip credentials that are in DB but no credentials in store
                    var creds = _credentialManager.GetCredential<UsernamePasswordCredential>(p.ProxyCredentialId);
                    if (creds != null) 
                    {
                        proxySettingsList.Items.Add(new ListItem(String.Format("{0} - {1}", p.ProxyUrl, creds.Username), p.ProxyId.ToString()));
                    }
                    else
                    {
                        //TODO : should we delete records from DB ?????
                    }
                }
                else
                {
                    proxySettingsList.Items.Add(new ListItem(String.Format("{0} - {1}", p.ProxyUrl, Resources.AgentManagementWebContent.EditAgent_NoCredentialsLabel), p.ProxyId.ToString()));
                }
            }

            if (agentId != 0)
            {
                using (var proxy = ControlServiceFactory.Create())
                {
                    agent = proxy.AgentGet(agentId);
                }

                if (agent == null)
                    throw new ArgumentException("Specified agent was not found.");
            }
            else
            {
                agent = new AgentRecord {Mode = AgentMode.Active};
            }

            LoadAgentIntoForm();

            originalAgentDnsNameBox.Value = agent.DNSName;
            originalAgentIpBox.Value = agent.IP;
            originalAgentPollingEngineId.Value = agent.PollingEngineId.ToString();
            originalAgentPollingEngineName.Value = agent.PollingEngineName;

            if (Request["mode"] != null)
            {
                passiveRadio.Checked = Request["mode"].Equals("passive", StringComparison.OrdinalIgnoreCase);
            }

            if (IsEditPage)
            {
                passiveRadio.Enabled = false;
                activeRadio.Enabled = false;
            }
        }
        else
        {
            agent = GetAgentFromForm();

            activePollerValidator.Enabled = agent.Mode == AgentMode.Active;
            passivePollerValidator.Enabled = !activePollerValidator.Enabled;

            if (Request["__EVENTARGUMENT"] == savePostBackArgument)
            {
                Page.Validate();
                if (Page.IsValid && IsAdmin)
                {
                    SaveAgent();
                }
            }
        }

        // we are showing polling engine when there is more than one engine
        // or when agent-engine relation is broken (agent is assigned to non-existing engine)
        ShowPollerDropDown = pollingEngineActive.Items.Count != 1 ||
                             (agent != null && agent.AgentId > 0 && string.IsNullOrEmpty(originalAgentPollingEngineName.Value));

        //if (originalAgentModeBox.Value == "active")
        //    updateRemoteSettingsCheckbox.InputAttributes.Add("disabled", "disabled");
    }

    protected string SavePostBack { get; set; }

    protected bool IsEditPage { get; set; }

    protected bool ShowPollerDropDown { get; set; }

    void proxyValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        //if (updateRemoteSettingsCheckbox.Checked)
        //    return;

        if (agent.Mode == AgentMode.Active)
            return;

        if (!agent.IsProxyEnabled())
            return;

        //skip if proxyID is not preselected
        if (formProxySetting.ProxyId != 0)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            AdvancedSettingsValidationError(validator, args, AgentProxyUrlEmptyError);
        }
    }

    void passiveAgentPortValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        if (agent.Mode == AgentMode.Active)
            return;

        int port;
        if (!Int32.TryParse(args.Value, out port))
        {
            AdvancedSettingsValidationError(validator, args, AgentPortError);
        }
    }

    void passiveAgent_HostnameValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        //if (updateRemoteSettingsCheckbox.Checked)
        //    return;

        if (agent.Mode == AgentMode.Active)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim(' ')))
        {
            validator.ErrorMessage = AgentHostnameEmptyError;
            args.IsValid = false;
            return;
        }

        try
        {
            //try resolve IP/hostname
            if (string.IsNullOrEmpty(agent.PassiveAgentHostname))
            {
                IPHostEntry hostInfo = Dns.GetHostEntry(args.Value.Trim(' '));
                if (hostInfo.AddressList.Length == 0)
                {
                    log.DebugFormat("Specified host couldn't be resolved : {0}", args.Value);
                    agent.PassiveAgentHostname = args.Value.Trim(' ');
                }
                else
                {
                    agent.PassiveAgentHostname = hostInfo.HostName;
                }               
            }

            //Let's save if we created new proxy record.
            //If ping to agent failed, then let's delete proxy settings from DAL and CredentialManager
            bool isCreatedNewProxyId = false;
            if (useProxyForConnectionToPassiveAgent)
            {
                //add new proxy record to DB if it has empty ID
                isCreatedNewProxyId = UpdateAgentProxySettings(ref formProxySetting, ref agent);
            }
            else
            {
                agent.ProxyId = 0;
            }

            AgentsDAL agentsDal = new AgentsDAL();
            var pingResult = agentsDal.TestPassiveAgentConnection(agent);
            if (!pingResult.Success)
            {
                //cleanup : avoid having credentials that are invalid
                //if we failed to connect to passive agent, then delete proxy entries in DAL and CredentialManager
                if (isCreatedNewProxyId)
                {
                    try
                    {
                        if (agent.ProxyId != 0)
                        {
                            agentsDal.DeleteProxy(agent.PollingEngineId, agent.ProxyId);
                        }

                        if (formProxySetting.ProxyCredentialId != 0)
                        {
                            _credentialManager.DeleteCredential(ProxySetting.ProxyCredentialOwner,
                                formProxySetting.ProxyCredentialId);
                        }
                        formProxySetting.ProxyId = 0;
                        formProxySetting.ProxyCredentialId = 0;
                    }
                    catch (Exception ex)
                    {
                        //ignore exception if we are deleting non-existing ID from CredentialManager
                    }
                }
                validator.ErrorMessage = String.Format(Resources.AgentManagementWebContent.EditAgent_AgentNotReachableError, agent.PassiveAgentHostname, agent.PassiveAgentPort.ToString(), pingResult.ErrorMessage);
                if (!String.IsNullOrEmpty(pingResult.ErrorMessage))
                {
                    validator.ErrorMessage += String.Format(Resources.AgentManagementWebContent.EditAgent_AgentNotReachableExtraError, pingResult.ErrorMessage);
                }
                validator.ErrorMessage += String.Format(PassiveAgentTroubleshootingHelpContext, PassiveAgentTroubleshootingHelpUrl, Resources.AgentManagementWebContent.PassiveAgentTroubleshootingHelp);

                args.IsValid = false;
            }
        }
        catch (Exception ex)
        {
            log.WarnFormat("Error validating passive agent hostname {0}. {1}", args.Value, ex);
            validator.ErrorMessage = AgentHostnameFormatError + args.Value;
            args.IsValid = false;
        }
    }

    private void AdvancedSettingsValidationError(CustomValidator validator, ServerValidateEventArgs args, string message)
    {
        validator.ErrorMessage = message;
        args.IsValid = false;

        // make appropriate advanced settings visible
        if (activeRadio.Checked)
        {
            advancedActiveCheckbox.Checked = true;
        }
        else
        {
            advancedPassiveCheckbox.Checked = true;
        }
    }

    void pollerValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;
        
        // don't validate when updating remote settings
        //         if (updateRemoteSettingsCheckbox.Checked)
        //             return;

        if (string.IsNullOrEmpty(args.Value))
        {
            AdvancedSettingsValidationError(validator, args, PollingEngineEmptyError);
            return;
        }

        int pollerId;
        if (!Int32.TryParse(args.Value, out pollerId))
        {
            AdvancedSettingsValidationError(validator, args, PollingEngineFormatError);
            return;
        }

        // Additional poller can't be assigned to agent with relative address (localhost/127.0.0.1)
        if (agent.Mode == AgentMode.Passive && 
            pollerId != mainPollerEngineId)
        {
            var host = passiveAgentHostnameBox.Text.Trim(' ');
            if (string.IsNullOrEmpty(host))
                return;

            if (Regex.Match(host, @"^(localhost|127\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})", RegexOptions.IgnoreCase).Success)
            {
                AdvancedSettingsValidationError(validator, args, AdditionalPollerAssignedToLocalhostError);
                return;
            }
        }

        // Poller is not being switched for new agent, it's just being added.
        if (agent.AgentId == 0)
            return;

        // validate possibility to switch active agent poller
        if (agent.Mode == AgentMode.Passive) 
            return;

        try
        {
            var dalAgent = new AgentsDAL().GetAgent(agent.AgentId);
            if (dalAgent == null)
            {
                var msg = String.Format(Resources.AgentManagementWebContent.EditAgent_AgentIsNotFoudErrorMsg, agent.AgentId);
                AdvancedSettingsValidationError(validator, args, msg);
                return;
            }
            
            var engine = new EnginesDAL().GetEngineById(dalAgent.PollingEngineId);
            if (engine == null)
            {
                var msg = String.Format(Resources.AgentManagementWebContent.EditAgent_PollerIsNotFound, dalAgent.PollingEngineId);
                AdvancedSettingsValidationError(validator, args, msg);
                return;
            }

            using (var proxyService = ControlServiceFactory.Create(engine.Name, engine.BusinessLayerPort, true))
            {
                if (!proxyService.TestServerConnection(agent, agent.PollingEngineId))
                {
                    AdvancedSettingsValidationError(validator, args, TestNewPollerConnectionError);
                }
            }
        }
        catch (Exception ex)
        {
            AdvancedSettingsValidationError(validator, args, ex.Message);
        }
    }

    void guidValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        //if (updateRemoteSettingsCheckbox.Checked)
        //    return;

        if (agent.Mode == AgentMode.Passive)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            validator.ErrorMessage = AgentGuidEmptyError;
            args.IsValid = false;
            return;
        }

        try
        {
            Guid guid = new Guid(args.Value);
            if (guid == Guid.Empty)
            {
                validator.ErrorMessage = AgentGuidEmptyError;
                args.IsValid = false;
                return;
            }
        }
        catch (Exception)
        {
            validator.ErrorMessage = AgentGuidFormatError;
            args.IsValid = false;
        }
    }

    void nameValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        //if (updateRemoteSettingsCheckbox.Checked)
        //    return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            validator.ErrorMessage = AgentNameEmptyError;
            args.IsValid = false;
            return;
        }
    }

    private void LoadAgentIntoForm()
    {
        agentIdBox.Value = agent.AgentId.ToString();
        agentDnsNameBox.Value = agent.DNSName;
        agentIpBox.Value = agent.IP;
        nameBox.Text = agent.Name;
        agentVersionFromTest.Value = agent.AgentVersion.ToString();

        // passive
        passiveAgentHostnameBox.Text = agent.PassiveAgentHostname;
        originalAgentHostnameBox.Value = agent.PassiveAgentHostname;
        passiveAgentPortBox.Text = agent.PassiveAgentPort != 0 ? agent.PassiveAgentPort.ToString() : SolarWinds.AgentManagement.Common.Constants.PassiveAgentPort.ToString();
        
        formProxySetting = new ProxyDAL().GetProxySetting(agent.ProxyId);
        if (formProxySetting.ProxyId != 0)
        {
            //disable fields
            proxyUrlBox.Enabled = false;
            useProxyAuthenticationCheckbox.Enabled = false;
            proxyUserNameBox.Enabled = false;
            proxyPasswordBox.Enabled = false;

            proxySettingsList.SelectedValue = formProxySetting.ProxyId.ToString();
            useProxyCheckbox.Checked = true;
            proxyUrlBox.Text = formProxySetting.ProxyUrl;
            useProxyAuthenticationCheckbox.Checked = formProxySetting.UseProxyAuthentication;
            try
            {
                //if no such credential, then we'll goto catch
                var creds = _credentialManager.GetCredential<UsernamePasswordCredential>(formProxySetting.ProxyCredentialId);
                proxyUserNameBox.Text = creds.Username;
                proxyPasswordBox.Attributes["value"] = "********";
            }
            catch (Exception e)
            {
                log.ErrorFormat("Error : {0}", e.Message);
            }
        }
        else
        {
            //enable fields
            proxyUrlBox.Enabled = true;
            useProxyAuthenticationCheckbox.Enabled = true;
            proxyUserNameBox.Enabled = true;
            proxyPasswordBox.Enabled = true;
        }

        //shared secret
        if(!String.IsNullOrEmpty(agent.Password))
        {
            agentConnectPassword = DecryptPassword(agent.Password);
            sharedSecretBox.Attributes["value"] = new System.Net.NetworkCredential(string.Empty, agentConnectPassword).Password;
        }

        // active or passive agent
        activeRadio.Checked = agent.Mode == AgentMode.Active;
        passiveRadio.Checked = !activeRadio.Checked;

        //guid
        guidBox.Text = agent.AgentGuid.ToString();
        originalAgentGuidBox.Value = agent.AgentGuid.ToString();
        
        //poller 
        if (agent.Mode == AgentMode.Active)
        {
            pollingEngineActive.SelectedValue = agent.PollingEngineId.ToString();
        }
        else
        {
            pollingEnginePassive.SelectedValue = agent.PollingEngineId.ToString();
        }
        originalAgentModeBox.Value = agent.Mode == AgentMode.Active ? "active" : "passive";

        // Auto update
        bool isRemoteCollector = agent.Type == AgentType.RemoteCollector;
        enableAutoUpdateCheckbox.Checked = agent.AutoUpdateEnabled;
        enableAutoUpdateCheckbox.Enabled = !isRemoteCollector;
    }

    private void GetProxySettingsFromForm()
    {
        useProxyForConnectionToPassiveAgent = useProxyCheckbox.Checked;
        formProxySetting.ProxyId = Convert.ToInt32(proxySettingsList.SelectedValue);
        if (formProxySetting.ProxyId != 0)
        {
            formProxySetting = new ProxyDAL().GetProxySetting(formProxySetting.ProxyId);
        }
        else
        {
            formProxySetting.ProxyUrl = proxyUrlBox.Text.Trim(' ');
            formProxySetting.UseProxyAuthentication = useProxyAuthenticationCheckbox.Checked;
            proxyUserName = proxyUserNameBox.Text;
            proxyPassword = proxyPasswordBox.Text;           
        }
    }

    private AgentRecord GetAgentFromForm()
    {
        AgentRecord agent = new AgentRecord();

        int agentId;
        Int32.TryParse(agentIdBox.Value, out agentId);
        agent.AgentId = agentId;

        agent.Name = nameBox.Text;
        agent.Mode = activeRadio.Checked ? AgentMode.Active : AgentMode.Passive;
        agent.DNSName = agentDnsNameBox.Value;

        Version version;
        if (Version.TryParse(agentVersionFromTest.Value, out version))
            agent.AgentVersion = version;

        int pollingEngineId;
        if (agent.Mode == AgentMode.Active)
        {
            try
            {
                agent.IP = agentIpBox.Value;
                agent.AgentGuid = new Guid(guidBox.Text);
                if (int.TryParse(pollingEngineActive.SelectedValue, out pollingEngineId))
                {
                    agent.PollingEngineId = pollingEngineId;
                    agent.PollingEngineName = pollingEngineActive.SelectedItem.Text;
                }
            }
            catch (Exception)
            { }
        }
        else
        {
            try
            {
                //address
                agent.PassiveAgentHostname = passiveAgentHostnameBox.Text.Trim(' ');
                agent.IP = agent.PassiveAgentHostname;
                
                //port
                int port;
                Int32.TryParse(passiveAgentPortBox.Text.Trim(' '), out port);
                agent.PassiveAgentPort = port;

                //shared secret
                string pwd = sharedSecretBox.Text;
                if (!String.IsNullOrEmpty(pwd))
                {
                    agentConnectPassword.Clear();
                    foreach (var c in pwd.ToCharArray())
                    {
                        agentConnectPassword.AppendChar(c);
                    }
                    agent.Password = EncryptPassword(agentConnectPassword);
                }

                //read proxy
                GetProxySettingsFromForm();
                agent.ProxyId = formProxySetting.ProxyId;
                
                //polling engine
                if (Int32.TryParse(pollingEnginePassive.SelectedValue, out pollingEngineId))
                {
                    agent.PollingEngineId = pollingEngineId;
                    agent.PollingEngineName = pollingEnginePassive.SelectedItem.Text;
                }
            }
            catch (Exception)
            { }
        }

        agent.LogLevel = logLevelCombo.SelectedValue;
        agent.AutoUpdateEnabled = enableAutoUpdateCheckbox.Checked;

        return agent;
    }

    public void SaveAgent()
    {
        SaveAgentInternal(false);//updateRemoteSettingsCheckbox.Checked);

        if (string.IsNullOrEmpty(returnUrl))
            GoBack();
        else
            Response.Redirect(returnUrl);
    }

    public void SaveAgentInternal(bool updateRemoteSettings)
    {
        AgentRecord newAgent = GetAgentFromForm();
        AgentRecord existingAgent = null;

        try
        {
            AgentsDAL agentsDal = new AgentsDAL();
            EnginesDAL enginesDal = new EnginesDAL();

            if (newAgent.AgentId == 0)
            {
                if (newAgent.Mode == AgentMode.Active)
                {
                    // newly added active client may not be connected right now, inform user that it may take few minutes
                    newAgent.ConnectionStatus = ConnectionStatus.WaitingForConnection;
                    newAgent.AgentStatus = AgentStatus.Unknown;
                }
                else
                {
                    newAgent.ConnectionStatus = ConnectionStatus.Unknown;
                    newAgent.AgentStatus = AgentStatus.Unknown;

                    String hostname = agent.PassiveAgentHostname;
                    try
                    {
                        IPHostEntry host;
                        host = Dns.GetHostEntry(hostname);
                        newAgent.IP = host.AddressList[0].ToString().ToUpper();
                        hostname = host.HostName;
                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Error trying resolve {0} : {1}", passiveAgentHostnameBox.Text, ex.Message);
                    }

                    newAgent.DNSName = hostname;
                    newAgent.Hostname = hostname;
                    newAgent.PassiveAgentHostname = hostname;
                }

                if (useProxyForConnectionToPassiveAgent)
                {
                    //add new proxy record to DB if it has empty ID
                    UpdateAgentProxySettings(ref formProxySetting, ref newAgent);
                }

                agentsDal.AddAgent(newAgent);
            }
            else
            {
                // We don't want to update any of the fields we don't already fill out...
                existingAgent = agentsDal.GetAgent(newAgent.AgentId);

                //make poller switch
                if (newAgent.PollingEngineId != existingAgent.PollingEngineId)
                {
                    try
                    {
                        var existingAgentEngine = enginesDal.GetEngineById(existingAgent.PollingEngineId);
                        if (existingAgentEngine == null)
                        {
                            log.ErrorFormat("SaveAgentInternal : poller {0} for agent {1} is missed. Switching to main poller.", existingAgent.PollingEngineId, existingAgent.AgentId);

                            var primaryPoller = enginesDal.GetPrimaryEngine();
                            if (primaryPoller == null)
                            {
                                log.ErrorFormat("SaveAgentInternal : Main Poller is not found. Rejecting switch for agent {0}.", existingAgent.AgentId);
                                throw new InvalidOperationException("Main Poller is not found. Broken DB.");
                            }

                            existingAgent.PollingEngineId = primaryPoller.ID;
                            existingAgent.PollingEngineName = primaryPoller.Name;
                        }
                        else
                        {
                            using (var p = ControlServiceFactory.Create(existingAgent, true))
                            {
                                if (!p.SetNewServerConnection(existingAgent, newAgent.PollingEngineId))
                                {
                                    log.ErrorFormat("Agent was not switched to the new poller {0} from {1}.",
                                        newAgent.PollingEngineId, existingAgent.PollingEngineId);
                                }
                                else
                                {
                                    existingAgent.PollingEngineId = newAgent.PollingEngineId;
                                    existingAgent.PollingEngineName = newAgent.PollingEngineName;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Agent {0} was not switched to the new poller {1} from {2} properly.\r\n{3}",
                            existingAgent.AgentId, newAgent.PollingEngineId,
                            existingAgent.PollingEngineId, ex);
                    }
                }

                if (existingAgent.NodeId.HasValue && existingAgent.NodeId.Value > 0)
                {
                    using (var coreBlProxy = SolarWinds.Orion.Core.Common.Proxy.BusinessLayer.CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(ex => { }))
                    {
                        coreBlProxy.UpdateNodesPollingEngine(existingAgent.PollingEngineId,
                            new int[] {existingAgent.NodeId.Value});
                    }
                }

                existingAgent.Mode = newAgent.Mode;
                existingAgent.AutoUpdateEnabled = newAgent.AutoUpdateEnabled;
                existingAgent.Name = newAgent.Name;
                existingAgent.AgentGuid = newAgent.AgentGuid;

                if (existingAgent.Mode == AgentMode.Passive)
                {
                    String hostname = newAgent.PassiveAgentHostname;
                    try
                    {
                        var host = Dns.GetHostEntry(hostname);
                        existingAgent.IP = host.AddressList[0].ToString().ToUpper();
                        hostname = host.HostName;
                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Error trying resolve {0} : {1}", passiveAgentHostnameBox.Text, ex.Message);
                    }

                    existingAgent.Hostname = hostname;
                    existingAgent.DNSName = hostname;

                    if (useProxyForConnectionToPassiveAgent)
                    {
                        UpdateAgentProxySettings(ref formProxySetting, ref existingAgent);
                    }
                    else
                    {
                        existingAgent.ProxyId = 0;
                    }

                    existingAgent.Password = newAgent.Password;
                }

                existingAgent.PassiveAgentHostname = newAgent.PassiveAgentHostname;
                existingAgent.PassiveAgentPort = newAgent.PassiveAgentPort;

                agentsDal.UpdateAgent(existingAgent, updateRemoteSettings);
            }

            if (existingAgent != null && !string.IsNullOrEmpty(newAgent.LogLevel))
            {
                using (var proxy = ControlServiceFactory.Create(existingAgent, true))
                {
                    proxy.SetAgentLogLevel(existingAgent.AgentId, newAgent.LogLevel);
                }
            }
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Error adding new agent: Guid: {0}, Hostname: {1}, IP: {2}, PollingEngineId: {3}. {4}",
                newAgent.AgentGuid, newAgent.Hostname, newAgent.IP, newAgent.PollingEngineId, ex);
            throw;
        }
    }

    private bool UpdateAgentProxySettings(ref ProxySetting proxyS, ref AgentRecord agentR)
    {
        //reload formProxySetting
        GetProxySettingsFromForm();

        var isCreatedNewProxyId = false;
        try
        {
            if (proxyS.ProxyId == 0 && IsNewProxySetting(ref formProxySetting)) //do we need need to add new proxy ?
            {
                if (proxyS.UseProxyAuthentication)
                {
                    //try to add new proxy to store/DB
                    var upCreds = new UsernamePasswordCredential()
                    {
                        ID = null,
                        Name = String.Format("{0} - {1}", proxyS.ProxyUrl, proxyUserName),
                        Username = proxyUserName,
                        Password = proxyPassword
                    };
                    _credentialManager.AddCredential(ProxySetting.ProxyCredentialOwner, upCreds);
                    if (upCreds.ID.HasValue)
                    {
                        proxyS.ProxyCredentialId = upCreds.ID.Value;
                    }
                }
            }

            //update agent settings if proxy is enabled
            if (proxyS.ProxyId == 0)
            {
                isCreatedNewProxyId = true;
                AgentsDAL dal = new AgentsDAL();
                proxyS.ProxyId = agentR.ProxyId = dal.AddProxy(agentR.PollingEngineId, proxyS);
            }
            else
            {
                agentR.ProxyId = proxyS.ProxyId;    
            }           
        }
        catch (System.Exception ex)
        {
            log.ErrorFormat("Error updating proxy for agent agentR.ProxyId={0} proxyR.ProxyId={1} : {2}", agentR.ProxyId, proxyS.ProxyId, ex);
        }

        return isCreatedNewProxyId;
    }

    private bool IsNewProxySetting(ref ProxySetting pSearch)
    {
        //Lookup through all records in DB and compare with proxySetting read from Web-Form
        var proxyList = new ProxyDAL().GetProxySettings();
        
        //traverse through each proxy
        foreach (var p in proxyList)
        {
            if (pSearch.ProxyUrl == p.ProxyUrl && 
                pSearch.UseProxyAuthentication == p.UseProxyAuthentication &&
                pSearch.ProxyCredentialId == p.ProxyCredentialId)
            {
                pSearch = p;
                return false;
            }
            else //compare username from store with username entered by user
            {
                try
                {
                    var credsP = _credentialManager.GetCredential<UsernamePasswordCredential>(p.ProxyCredentialId);
                    //enough to compare only url and username as <username,password> is unique for proxy
                    if (pSearch.ProxyUrl == p.ProxyUrl && proxyUserName == credsP.Username)
                    {
                        pSearch = p;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    //no need to catch anything : just ignore and treat as new proxy
                }
            }
        }

        return true;
    }

    protected string InstallingAgentHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionAgentEditAgent");
        }
    }

    protected string LearnMoreActiveUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionAgentManagementPHActivePassiveAgent");
        }
    }

    protected string LearnMorePassiveUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionAgentManagementPHServerIniCommunication");
        }
    }


    protected string WhatIsProxyHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgentManagementPHProxy"); }
    }
    
    protected string PassiveAgentTroubleshootingHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionAgent_Troubleshooting");
        }
    }

    protected bool RunTest { get; set; }
    protected bool RunTestAndSave { get; set; }

    protected void Save_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && IsAdmin)
        {
            //RunTestAndSave = true;
            SaveAgent();
        }
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(returnUrl))
            GoBack();
        else
            Response.Redirect(returnUrl);
    }

    protected void testButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && IsAdmin)
        {
            RunTest = true;
        }
    }

    private void GoBack()
    {
        Response.Redirect("/Orion/AgentManagement/Admin/ManageAgents.aspx");
    }
}
