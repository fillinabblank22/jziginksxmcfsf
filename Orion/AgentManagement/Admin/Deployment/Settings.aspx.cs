﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using SolarWinds.AgentManagement.Contract;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.AgentManagement.Common;
using SolarWinds.AgentManagement.Common.Helpers;
using SolarWinds.AgentManagement.Common.Models;
using SolarWinds.AgentManagement.Web;
using SolarWinds.AgentManagement.Web.DAL;
using SolarWinds.AgentManagement.Web.Models;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_AgentManagement_Admin_Deployment_Settings : DeploymentWizardPageBase, IBypassAccessLimitation
{
    #region Constants

    protected readonly string NewCredentialText = Resources.AgentManagementWebContent.Deployment_Settings_NewCredentialText;

    protected readonly string DeployAgentSingularText = Resources.AgentManagementWebContent.Deployment_Settings_DeployAgentSingularText;
    protected readonly string DeployAgentPluralText = Resources.AgentManagementWebContent.Deployment_Settings_DeployAgentPluralText;

    protected readonly string NoLocationsSelectedError = Resources.AgentManagementWebContent.Deployment_Settings_NoLocationsSelectedError;
    protected readonly string UnassignedCredentialsError = Resources.AgentManagementWebContent.Deployment_Settings_UnassignedCredentialsError;
    protected readonly string UnableToDeployAgentFormat = Resources.AgentManagementWebContent.Deployment_Settings__UnableToDeployAgentFormat;
    protected readonly string InvalidHostnameOrIPFormat = Resources.AgentManagementWebContent.Deployment_Settings_InvalidHostnameOrIPFormat;
    protected readonly string InvalidCredentialsFormat = Resources.AgentManagementWebContent.Deployment_Settings_InvalidCredentialsFormat;
    protected readonly string UnsupportedOSFormat = Resources.AgentManagementWebContent.Deployment_Settings_UnsupportedOSFormat;

    protected readonly string ChooseExistingOrCreateNewOneText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_ChooseExistingOrCreateNewOneText);
    protected readonly string ChooseCredentialText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_ChooseCredentialText);
    protected readonly string ChooseInstallPackageText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_ChooseInstallPackageText);
    protected readonly string UserNameText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_UserNameText);
    protected readonly string PasswordText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_PasswordText);

    protected readonly string SuccessMessageFormat = Resources.AgentManagementWebContent.Deployment_Settings_SuccessMessageFormat;
    protected readonly string ErrorMessageFormat = Resources.AgentManagementWebContent.Deployment_Settings_ErrorMessageFormat;

    #endregion // Constants

    private readonly CredentialManager _credentialManager = new CredentialManager();
    private readonly JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();
    private readonly IList<string> _errorMessages = new List<string>();
    private readonly AgentsDAL _agentsDal = new AgentsDAL();
    private int _successfulCount;
    /// <summary>
    /// Overall error message if this message is set, error messages for single deployments are not shown
    /// </summary>
    private string _overallErrorMessage;

    private bool _isLinuxAgentEnabled;

    protected string CredentialsList
    {
        get
        {
            return
                _jsSerializer.Serialize(
                    (new[] {new KeyValuePair<int, string>(-1, NewCredentialText)}).Concat(
                        _credentialManager.GetCredentialNames<UsernamePasswordCredential>(
                            CoreConstants.CoreCredentialOwner)));
        }
    }

    protected string SshPrivateKeyCredentialsList
    {
        get
        {
            return
                _jsSerializer.Serialize(
                    (new[] { new KeyValuePair<int, string>(-1, NewCredentialText) }).Concat(
                        _credentialManager.GetCredentialNames<SshPrivateKeyCredential>(
                            CoreConstants.CoreCredentialOwner)));
        }
    }

    protected bool ShowSuccess
    {
        get { return _successfulCount > 0; }
    }
    protected string SuccessMessage
    {
        get { return String.Format(SuccessMessageFormat, _successfulCount); }
    }
    protected bool ShowError
    {
        get { return _overallErrorMessage != null || _errorMessages.Any(); }
    }
    protected string ErrorMessage
    {
        get
        {
            // if there is an overall error message set don't show errors for single deployment hosts
            if (_overallErrorMessage != null)
                return _overallErrorMessage;

            var errorList = "<ul>" +
                            _errorMessages.Select(
                                x => String.Format("<li>{0}</li>", x.Replace(Environment.NewLine, "<br />"))).Aggregate(
                                    (a, b) => a + Environment.NewLine + b) + "</ul>";

            return String.Format(ErrorMessageFormat, _errorMessages.Count, errorList);
        }
    }

    protected string PollerList
    {
        get
        {
            var dal = new AgentsDAL();
            return _jsSerializer.Serialize(dal.GetAvailablePollingEngines());
        }
    }

    protected string AgentModeList
    {
        get
        {
            var modeList = new List<KeyValuePair<int, string>>();
            modeList.Add(new KeyValuePair<int, string>((int)AgentMode.Auto, Resources.AgentManagementWebContent.DeploymentSettings_AutodetectAgentModeText));
            modeList.Add(new KeyValuePair<int, string>((int)AgentMode.Active, Resources.AgentManagementWebContent.DeploymentSettings_ActiveAgentModeText));
            modeList.Add(new KeyValuePair<int, string>((int)AgentMode.Passive, Resources.AgentManagementWebContent.DeploymentSettings_PassiveAgentModeText));
            return _jsSerializer.Serialize(modeList);
        }
    }

    public string LinuxAgentVisibilityControlCssClass
    {
        get
        {
            return _isLinuxAgentEnabled ? "" : "hidden";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        _isLinuxAgentEnabled =
                new FeatureHelper(SwisConnectionProxyPool.GetSystemCreator()).IsFeatureEnabled(
                    FeatureHelper.LinuxAgentFeature);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Workflow.SessionInstanceHasExpired)
            GotoFirstStep();

        ((Orion_AgentManagement_Admin_Deployment_DeploymentWizard)this.Master).HelpFragment = "OrionAgentManagementAGDeployAgent";

        imgbNext.Text = Workflow.Hosts.Count() > 1 ? DeployAgentPluralText : DeployAgentSingularText;
        imgbBack.Visible = Workflow.CanGoBack;

        // init poller Id for hosts
        int mainPollerId = _agentsDal.GetAvailablePollingEngines().First().Key;
        foreach (var host in Workflow.Hosts)
        {
            if (host.PollerId == 0)
                host.PollerId = mainPollerId;
        }
    }

    protected override bool Next()
    {
        if (!Workflow.Hosts.Any())
        {
            _overallErrorMessage = NoLocationsSelectedError;
            return false;
        }

        if(Workflow.Hosts.Any(h => h.CredentialId == -1))
        {
            _overallErrorMessage = UnassignedCredentialsError;
            return false;
        }

        var successfullyDeployed = new List<string>();

        using (var proxy = ControlServiceFactory.Create())
        {
            foreach (var host in Workflow.Hosts)
            {
                if (!ValidateAndCompleteHost(host))
                {
                    _log.ErrorFormat("Invalid host with name '{0}'", host.Name);
                    continue;
                }

                try
                {
                    var description = host.GetDeploymentDescription();
                    proxy.DeployAgent(description);
                    successfullyDeployed.Add(host.Hostname);
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Unable to deploy Agent to {0}({1}).", host.Hostname, host.IpAddress), ex);
                    _errorMessages.Add(String.Format(UnableToDeployAgentFormat, host.Name, ex.Message));
                }
            }
        }

        // remove successfully deployed hosts from the list
        foreach (var deployedHost in successfullyDeployed)
        {
            Workflow.Remove(deployedHost);
        }

        _successfulCount = successfullyDeployed.Count;

        // if there are any hosts remaining do not redirect to return url
        if (Workflow.Hosts.Any())
            return false;

        return true;
    }

    private bool ValidateAndCompleteHost(DeploymentHost host)
    {
        var result = host.Validate(_credentialManager);
        switch (result)
        {
            case DeploymentHost.HostValidationResult.InvalidHostnameOrIP:
                _errorMessages.Add(String.Format(InvalidHostnameOrIPFormat, host.Name));
                return false;
            case DeploymentHost.HostValidationResult.InvalidCredentials:
                _errorMessages.Add(String.Format(InvalidCredentialsFormat, host.Name));
                return false;
            case DeploymentHost.HostValidationResult.UnsupportedOS:
                _errorMessages.Add(String.Format(UnsupportedOSFormat, host.Name));
                return false;
            default:
                return true;

        }
    }

    protected string UnsupportedOsHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_SupportedLinuxOS"); }
    }

    protected string SudoCredentialsHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_LinuxCredentials"); }
    }
}