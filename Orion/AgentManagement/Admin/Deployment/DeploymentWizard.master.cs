﻿using System;

public partial class Orion_AgentManagement_Admin_Deployment_DeploymentWizard : System.Web.UI.MasterPage
{
    public string PageTitle = Resources.AgentManagementWebContent.DeploymentWizardPageTitle;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = PageTitle;
    }

    public string HelpFragment
    {
        get
        {
            return ((Orion_AgentManagement_Admin_AgentManagementAdminPage)this.Master).HelpFragment;
        }
        set
        {
            ((Orion_AgentManagement_Admin_AgentManagementAdminPage)this.Master).HelpFragment = value;
        }
    }
}
