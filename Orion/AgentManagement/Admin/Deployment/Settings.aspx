﻿<%@ Page Language="C#" MasterPageFile="~/Orion/AgentManagement/Admin/Deployment/DeploymentWizard.master" AutoEventWireup="true"
    CodeFile="Settings.aspx.cs" Inherits="Orion_AgentManagement_Admin_Deployment_Settings" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" Module="AgentManagement" File="CheckboxSelectionModel.js" />
    <orion:Include runat="server" Module="AgentManagement" File="DeploymentSettingsGrid.js" />
    <orion:Include runat="server" Module="AgentManagement" File="AsyncFileUpload.js" />

    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }
    </script>

    <h2><%= Resources.AgentManagementWebContent.DeploymentSettingsHeading %></h2>

    <div class="sw-AgentManagement-message-block">

        <% if (ShowSuccess)
            { %><div class="sw-suggestion sw-suggestion-pass"><span class="sw-suggestion-icon"></span><%= SuccessMessage %></div>
        <br />
        <% } %>

        <% if (ShowError)
            { %><div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= ErrorMessage %></div>
        <br />
        <% } %>
    </div>

    <div id="SettingsGrid" class="AgentManagement_ExtJsGrid"></div>

    <div class="AgentManagement_WorkflowButtons">
        <orion:LocalizableButton ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" runat="server" />
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="CustomText" DisplayType="Primary" runat="server" OnClientClick="return AreAllCredentialsValid('imgbNext');" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

    <div id="AssignCredentialWindow" style="display: none;">
        <div class="x-panel-body sw-AgentManagement-credential-window">
            <p><%= ChooseExistingOrCreateNewOneText %></p>
            
            <table class="sw-AgentManagement-CredentialsTable">
                <tr class="sw-AgentManagement-OsTypeRow <%= LinuxAgentVisibilityControlCssClass %>">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_OSType %>:</td>
                    <td>
                        <label id="OSTypeWindowsRadioLabel">
                            <input type="radio" id="OSTypeWindowsRadio" name="osType" value="windows" checked="checked" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_OSTypeWindows %>
                        </label>
                        <label id="OSTypeLinuxRadioLabel">
                            <input type="radio" id="OSTypeLinuxRadio" name="osType" value="linux" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_OSTypeLinux %>
                        </label>
                    </td>
                </tr>
                <tr class="<%= LinuxAgentVisibilityControlCssClass %>">
                    <td class="sw-AgentManagement-SpacerRow" colspan="2"></td>
                </tr>

                <tr class="sw-AgentManagement-CredentialTypeRow <%= LinuxAgentVisibilityControlCssClass %>">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_CredentialType %>:</td>
                    <td>
                        <label>
                            <input type="radio" id="CredentialTypeUsernamePasswordRadio" name="credentialType" value="usernameAndPassword" checked="checked" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_UsernamePasswordCredentials %>
                        </label>
                        <label>
                            <input type="radio" id="CredentialTypeCertificateRadio" name="credentialType" value="privateKey" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_CertificateCredentials %>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td class="firstColumn"><%= ChooseCredentialText %></td>
                    <td>
                        <div class="sw-AgentManagement-UsernamePasswordItem">
                            <select id="CredentialNamesCombo">
                                <option value="test">test</option>
                            </select>
                        </div>
                        <div class="sw-AgentManagement-PrivateKeyItem">
                            <select id="SshCredentialNamesCombo">
                                <option value="test">test</option>
                            </select>
                        </div>
                    </td>
                </tr>

                <tr class="sw-AgentManagement-PrimaryCustomCredential">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.CredentialName %>:</td>
                    <td>
                        <input type="text" id="CredentialName" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential">
                    <td class="firstColumn"><%= UserNameText %></td>
                    <td>
                        <input type="text" id="CredentialUsername" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential sw-AgentManagement-UsernamePasswordItem">
                    <td class="firstColumn"><%= PasswordText %></td>
                    <td>
                        <input type="password" id="CredentialPassword" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential sw-AgentManagement-PrivateKeyItem">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_PrivateKey %>:</td>
                    <td>
                        <div id="CertificateViaFileBlock">
                            <input type="file" id="CredentialPrivateKeyFile" name="file" value="" />
                            <br />
                            <a href="javascript:void(0);" onclick="SW.AgentManagement.DeploymentSettingsGrid.ShowPrivateKeyDialog();">
                                <%= Resources.AgentManagementWebContent.Deployment_Settings_EnterPrivateKeyManually %>
                            </a>
                        </div>
                        <div id="CertificateManualBlock">
                            <span id="CredentialPrivateKeyManualLabel"><%= Resources.AgentManagementWebContent.Deployment_Settings_PrivateKeyPastedManually %></span>
                            <br/>
                            <a href="javascript:void(0);" onclick="SW.AgentManagement.DeploymentSettingsGrid.ResetPrivateKey();">
                                <%= Resources.AgentManagementWebContent.Deployment_Settings_UsePrivateKeyInFile %>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential sw-AgentManagement-PrivateKeyItem">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_PrivateKeyPassword %>:</td>
                    <td>
                        <input type="password" id="CredentialPrivateKeyPassword" value="" />
                    </td>
                </tr>

                <tr class="sw-AgentManagement-SudoSelection">
                    <td colspan="2">
                        <label>
                            <input type="checkbox" id="IncludeSudoCredentials" value="1" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_IncludeSudo %>
                        </label>
                        <img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" id="sudoHelpIcon" />
                        <div id="sudoHelpIconTooltipContent" class="hidden">
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_DoINeedToProvideSudoCredentials %><br/><br/>
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_SudoCredentialsHelpfulText %><br/><br/>
                            <a href="<%= SudoCredentialsHelpUrl %>" target="_blank" rel="noopener noreferrer"><%= Resources.AgentManagementWebContent.Deployment_Settings_WhatCredentialsDoINeedForLinux %></a>
                        </div>
                    </td>
                </tr>
                <!-- SUDO -->
                <tr class="sw-AgentManagement-SudoSection">
                    <td colspan="2">
                        <%= Resources.AgentManagementWebContent.Deployment_Settings_SudoCredentialsCaption %>
                    </td>
                </tr>

                <tr class="sw-AgentManagement-SudoSection">
                    <td class="firstColumn"><%= ChooseCredentialText %></td>
                    <td>
                        <select id="AdditionalCredentialNamesCombo">
                            <option value="test">test</option>
                        </select>
                    </td>
                </tr>

                <tr class="sw-AgentManagement-SudoSection sw-AgentManagement-AdditionalCustomCredential">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.CredentialName %>:</td>
                    <td>
                        <input type="text" id="AdditionalCredentialName" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-SudoSection sw-AgentManagement-AdditionalCustomCredential">
                    <td class="firstColumn"><%= UserNameText %></td>
                    <td>
                        <input type="text" id="AdditionalCredentialUsername" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-SudoSection sw-AgentManagement-AdditionalCustomCredential">
                    <td class="firstColumn"><%= PasswordText %></td>
                    <td>
                        <input type="password" id="AdditionalCredentialPassword" value="" /></td>
                </tr>
                
                <tr class="sw-AgentManagement-InstallPackageSelection">
                    <td class="firstColumn"><%= ChooseInstallPackageText %></td>
                    <td>
                        <select id="InstallPackageCombo">
                            <option value="test">test</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <orion:LocalizableButton ID="imgbTest" LocalizedText="Test" DisplayType="Small" runat="server" OnClientClick="SW.AgentManagement.DeploymentSettingsGrid.TestCredentialsInPopup();return false;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul id="testResults" style="display: none;"></ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <script type="text/javascript">
        // <![CDATA[
        $(document).ready(function () {
            var toolTip = new Ext.ToolTip({
                target: 'sudoHelpIcon',
                html: $('#sudoHelpIconTooltipContent').html(),
                anchor: 'left',
                cls: 'sw-AgentManagement-Tooltip',
                dismissDelay: 0,
                showDelay: 0,
                autoHide: false
            });

            toolTip.on('show', function () {

                var timeout;

                toolTip.getEl().on('mouseout', function () {
                    timeout = window.setTimeout(function () {
                        toolTip.hide();
                    }, 500);
                });

                toolTip.getEl().on('mouseover', function () {
                    window.clearTimeout(timeout);
                });

                Ext.get('sudoHelpIcon').on('mouseover', function () {
                    window.clearTimeout(timeout);
                });

                Ext.get('sudoHelpIcon').on('mouseout', function () {
                    timeout = window.setTimeout(function () {
                        toolTip.hide();
                    }, 500);
                });

            });
        });

        var skipCredentialsValidation = false;
        SW.AgentManagement.DeploymentSettingsGrid.SetCredentialsList(<%= CredentialsList %>);
        SW.AgentManagement.DeploymentSettingsGrid.SetSshPrivateKeyCredentialsList(<%= SshPrivateKeyCredentialsList %>);
        SW.AgentManagement.DeploymentSettingsGrid.SetPollerList(<%= PollerList %>);
        SW.AgentManagement.DeploymentSettingsGrid.SetAgentModeList(<%= AgentModeList %>);
        SW.AgentManagement.DeploymentSettingsGrid.SetUnsupportedOsHelpUrl('<%= UnsupportedOsHelpUrl %>');

        function AreAllCredentialsValid(postbackButtonId) {
            if (skipCredentialsValidation)
                return true;

            var credentialsTestResult = SW.AgentManagement.DeploymentSettingsGrid.AreAllCredentialsValid(postbackButtonId);
            if (credentialsTestResult == SW.AgentManagement.DeploymentSettingsGrid.CredentialsTestResults.CredentialsMissing) {
                Ext.Msg.show({
                    title: '<%= Resources.AgentManagementWebContent.ErrorTitle %>',
                    msg: '<%= Resources.AgentManagementWebContent.DeploymentCredentialsAreMissing %>',
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
                return false;
            } else if (credentialsTestResult == SW.AgentManagement.DeploymentSettingsGrid.CredentialsTestResults.CredentialsInvalid) {
                Ext.Msg.show({
                    title: '<%= Resources.AgentManagementWebContent.ErrorTitle %>',
                    msg: '<%= Resources.AgentManagementWebContent.DeploymentCredentialsAreInvalid %>',
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
                return false;
            } else if (credentialsTestResult == SW.AgentManagement.DeploymentSettingsGrid.CredentialsTestResults.UnsupportedOS) {
                Ext.Msg.show({
                    title: '<%= Resources.AgentManagementWebContent.ErrorTitle %>',
                    msg: '<%= Resources.AgentManagementWebContent.Deployment_SomeAgentHasUnsupportedOS %>',
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
                return false;
            } else if (credentialsTestResult == SW.AgentManagement.DeploymentSettingsGrid.CredentialsTestResults.CredentialsTestInProgress) {
                Ext.Msg.show({
                    title: '<%= Resources.AgentManagementWebContent.ErrorTitle %>',
                    msg: '<%= Resources.AgentManagementWebContent.DeploymentCredentialsTestInProgress %>',
                    icon: Ext.Msg.WARNING, buttons: Ext.Msg.OK
                });
                return false;
            } else if (credentialsTestResult == SW.AgentManagement.DeploymentSettingsGrid.CredentialsTestResults.ExistingAgentWillBeReplaced) {
                Ext.Msg.confirm('<%= Resources.AgentManagementWebContent.ThereIsSomeExistingAgentForDiffrentServerTitle %>',
                    '<%= Resources.AgentManagementWebContent.ThereIsSomeExistingAgentForDiffrentServerConfirmation %>',
                    function (btn) {
                        if (btn == 'yes') {
                            skipCredentialsValidation = true;
                            __doPostBack('<%= imgbNext.ClientID.Replace('_','$') %>', '');
                        }
                    });
                return false;
            }
            return true;
        }
// ]]>
    </script>
</asp:Content>

