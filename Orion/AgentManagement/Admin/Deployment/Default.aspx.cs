﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.AgentManagement.Web;
using SolarWinds.AgentManagement.Web.DAL;

public partial class Orion_AgentManagement_Admin_Deployment_Default : DeploymentWizardPageBase, IBypassAccessLimitation
{
    #region Constants

    protected readonly string Heading = Resources.AgentManagementWebContent.DeployAgentLocationSelectionHeading;
    protected readonly string NewLocationText = Resources.AgentManagementWebContent.ManuallyEnterTheLocation;
    protected readonly string EnterIPOrHostnameText = Resources.AgentManagementWebContent.EnterIPOrHostname;
    protected readonly string SelectFromExistingNodesText = Resources.AgentManagementWebContent.SelectFromExistingNodes;
    protected readonly string LocationsSelectedText = Resources.AgentManagementWebContent.LocationsSelected;

    protected readonly string IpAddressOrHostnameIsRequiredError = Resources.AgentManagementWebContent.IpAddressOrHostnameIsRequiredError;
    protected readonly string InvalidHostnameError = Resources.AgentManagementWebContent.InvalidHostnameError;
    protected readonly string HostnameAlreadyInListError = Resources.AgentManagementWebContent.HostnameAlreadyInListError;

    protected readonly string NoLocationsSelectedError = Resources.AgentManagementWebContent.NoLocationsSelectedError;

    protected readonly string AddToListButtonLabel = Resources.AgentManagementWebContent.AddToListButtonLabel;

    protected readonly string LearnMoreText = Resources.AgentManagementWebContent.LearnMore;
    protected readonly string DeploymentInfoText = Resources.AgentManagementWebContent.DeploymentInfoText;

    #endregion // Constants

    private readonly JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();
    private List<DeploymentHost> _hosts;

    protected bool ShowError { get; set; }
    protected string ErrorMessage { get; set; }
    protected bool ShowGrid { get; set; }

    protected string LearnMoreUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgentManagementAGDeployAgent"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_AgentManagement_Admin_Deployment_DeploymentWizard)this.Master).HelpFragment = "OrionAgentManagementAGDeployAgent";

        if (Request["keepData"] == null)
            Workflow.Reset();

        var dal = new NodesDAL();
        ShowGrid = dal.GetNodesCount("", "") != 0;
        
        ShowError = false;

        buttonAddToList.Text = AddToListButtonLabel;

        hostnameRequiredValidator.ErrorMessage = IpAddressOrHostnameIsRequiredError;
        hostnameCustomValidator.ErrorMessage = InvalidHostnameError;

        // load nodes from URL and go directly to settings page if correct parameter is set
        if (!string.IsNullOrEmpty(Request["nodeIds"]))
        {
            _hosts = dal.GetDeploymentHostsById(GetIdsFromQueryParameter(Request["nodeIds"])).ToList();
            if (_hosts.Count > 0)
            {
                Next();
                // We don't want user to go from settings back to node selection in this case
                Workflow.CanGoBack = false;
                GotoNextStep();
                return;
            }
        }

        if(!IsPostBack || string.IsNullOrEmpty(selectedNodes.Value))
        {
            _hosts = new List<DeploymentHost>(Workflow.Hosts);
        }
        else
        {
            _hosts = _jsSerializer.Deserialize<List<DeploymentHost>>(selectedNodes.Value);
        }
    }

    private IEnumerable<int> GetIdsFromQueryParameter(string value)
    {
        foreach (string idStr in value.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
        {
            int id;
            if (int.TryParse(idStr, out id))
                yield return id;
        }
    }

    protected override bool Next()
    {
        // automatically add location if the next is hit and hostname textbox contains valid hostname
    	var hostname = addHostTextbox.Text.Trim(' ');
        if (!string.IsNullOrEmpty(hostname))
        {
            Page.Validate("AddHost");

            if (!Page.IsValid)
                return false;

            _hosts.Add(new DeploymentHost()
            {
                Name = hostname,
                Hostname = hostname,
                StatusName = "",
                CredentialId = -1
            });

            addHostTextbox.Text = string.Empty;
        }

        if (_hosts == null || _hosts.Count == 0)
        {
            ShowError = true;
            ErrorMessage = NoLocationsSelectedError;
            return false;
        }

        // merge selected hosts with values in workflow
        Workflow.Merge(_hosts);

        return true;
    }

    protected void addHostTextbox_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        var hostname = e.Value.Trim(' ');
        var hostnameType = Uri.CheckHostName(hostname);
        if (hostnameType == UriHostNameType.Unknown || hostnameType == UriHostNameType.Basic)
        {
            e.IsValid = false;
        }
        else if(_hosts.FirstOrDefault(h => string.Compare(h.Hostname, hostname, StringComparison.OrdinalIgnoreCase) == 0) == null)
        {
            e.IsValid = true;
        }
        else
        {
            hostnameCustomValidator.ErrorMessage = HostnameAlreadyInListError;
            e.IsValid = false;
        }
    }

    protected void buttonAddToList_OnClick(object sender, EventArgs e)
    {
        var hostname = addHostTextbox.Text.Trim(' ');
        if (hostnameRequiredValidator.IsValid && hostnameCustomValidator.IsValid)
        {
            _hosts.Add(new DeploymentHost()
                           {
                               Name = hostname,
                               Hostname = hostname,
                               StatusName = "",
                               CredentialId = -1
                           });

            addHostTextbox.Text = string.Empty;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if(_hosts != null)
        {
            selectedNodes.Value = _jsSerializer.Serialize(_hosts);
        }
    }
}