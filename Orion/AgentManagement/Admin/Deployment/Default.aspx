﻿<%@ Page Language="C#" MasterPageFile="~/Orion/AgentManagement/Admin/Deployment/DeploymentWizard.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_AgentManagement_Admin_Deployment_Default" %>
<%@ Import Namespace="SolarWinds.AgentManagement.Web" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4"  />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" Module="AgentManagement" File="SearchField.js" />
    <orion:Include runat="server" Module="AgentManagement" File="CheckboxSelectionModel.js" />
    <orion:Include runat="server" Module="AgentManagement" File="NodesBrowser.js" />

    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }
    </script>

    <h2><%= Heading %></h2>

    <span class="sw-text-helpful"><%= DeploymentInfoText%> <a href="<%= LearnMoreUrl %>" class="AgentManagement_HelpLink" target="_blank" rel="noopener noreferrer">&#0187; <%= LearnMoreText %></a></span><br />

    <% if(ShowError) { %><div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= ErrorMessage %></div><p></p><% } %>
    
    <table class="sw-AgentManagement-nodes-table">
        <tr>
            <td class="sw-AgentManagement-first-column">
                <h3><%= NewLocationText %></h3>
                <div class="sw-AgentManagement-enter-host">
                    <%= EnterIPOrHostnameText%> <asp:TextBox ID="addHostTextbox" runat="server" CssClass="AgentManagement_EditBox" /> <orion:LocalizableButton ValidationGroup="AddHost" ID="buttonAddToList" LocalizedText="CustomText" DisplayType="Small" OnClick="buttonAddToList_OnClick" runat="server" />
                    <asp:RequiredFieldValidator ID="hostnameRequiredValidator" ControlToValidate="addHostTextbox" ValidationGroup="AddHost" Display="Dynamic" runat="server" />
                    <asp:CustomValidator ID="hostnameCustomValidator" ControlToValidate="addHostTextbox" OnServerValidate="addHostTextbox_ServerValidate" ValidationGroup="AddHost" Display="Dynamic" runat="server" />
                </div>
                
                <div style="display: <%= ShowGrid ? "block" : "none" %>">

                <h3><%= SelectFromExistingNodesText%></h3>
                <div id="AgentManagement_GroupingPanel" class="AgentManagement_GroupingPanel">
	                <div class="GroupSection">
		                <label for="groupBySelect"><%: Resources.AgentManagementWebContent.Deployment_GroupByLabel %>:</label>
		                <select id="groupBySelect">
			                <option value="test"/>
		                </select>
	                </div>
                    <div id="TestID"></div>
                </div>

                <div id="NodesGrid" class="AgentManagement_ExtJsGrid"></div>

                </div>
            </td>
            <td class="AgentManagement_LeftBorder AgentManagement_DarkBorder sw-AgentManagement-second-column">
                <div class="sw-AgentManagement-locations-header">
                    <a href="javascript:void(0);" id="sw-AgentManagement-expander-link"><img src="/Orion/Images/Button.Collapse.gif" alt="+" /> <b><span id="sw-AgentManagement-locations-count">0</span> <%= LocationsSelectedText %></b></a>
                </div>
                <div id="sw-AgentManagement-hosts-list">
                    <ul></ul>
                </div>
            </td>
        </tr>
    </table>
    
    <div class="AgentManagement_WorkflowButtons">
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" ValidationGroup="WholePage" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

    <asp:HiddenField ID="selectedNodes" runat="server" />
    <asp:HiddenField ID="AgentManagement_CurrentPage" ClientIDMode="Static" runat="server" />

    <script type="text/javascript">
        // <![CDATA[
        SW.AgentManagement.NodesBrowser.SetPageSize(20);
        SW.AgentManagement.NodesBrowser.SetUserName("<%=Context.User.Identity.Name.Trim()%>");
        SW.AgentManagement.NodesBrowser.SelectedNodesFieldId("<%= selectedNodes.ClientID %>");

        $(function () {
            $('#sw-AgentManagement-expander-link').toggle(
                function () {
                    $('img:first', this).attr('src', '/Orion/Images/Button.Expand.gif');
                    $('#sw-AgentManagement-hosts-list').slideUp('fast');
                },
                function () {
                    $('img:first', this).attr('src', '/Orion/Images/Button.Collapse.gif');
                    $('#sw-AgentManagement-hosts-list').slideDown('fast');
                }
            );
        });
        // ]]>
    </script>

</asp:Content>