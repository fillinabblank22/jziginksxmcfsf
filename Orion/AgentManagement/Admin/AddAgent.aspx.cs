using System;
using System.Text;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.AgentManagement.Common.Helpers;
using SolarWinds.AgentManagement.Web.Exceptions;

public partial class Orion_AgentManagement_Admin_AddAgent : System.Web.UI.Page, IBypassAccessLimitation
{
    #region Text constants

    protected readonly string PageTitle = Resources.AgentManagementWebContent.AddAgentPageTitle;
    protected readonly string SelectMethodText = Resources.AgentManagementWebContent.SelectAgentAddMethod;
    protected readonly string DownloadAgentText = Resources.AgentManagementWebContent.DownloadAgentLinkText;

    protected readonly string AddExistingLocationLabelTitle = Resources.AgentManagementWebContent.AddExistingAgent;
    protected readonly string AddExistingLocationLabelDetail = Resources.AgentManagementWebContent.AddExistingAgentDescription;

    protected readonly string InstallLocationOnNetworkLabelTitle = Resources.AgentManagementWebContent.DeployAgentToNetwork;
    protected readonly string InstallLocationOnNetworkLabelDetail = Resources.AgentManagementWebContent.DeployAgentToNetworkDescription;
    protected readonly string InstallLocationOnNetworkLabelFipsError = Resources.AgentManagementWebContent.DeployAgentToNetworkFipsError;

    #endregion // Text constants

    #region Private properties

    private string AddExistingLocationUrl
    {
        get
        {
            return string.IsNullOrEmpty(_returnUrl)
                       ? "/Orion/AgentManagement/Admin/EditAgent.aspx"
                       : "/Orion/AgentManagement/Admin/EditAgent.aspx?returnUrl=" + UrlHelper.ToSafeUrlParameter(_returnUrl);
        }
    }

    private string InstallLocationUrl
    {
        get
        {
            StringBuilder queryString = new StringBuilder();
            if (!string.IsNullOrEmpty(_returnUrl))
            {
                queryString.Append((queryString.Length == 0) ? "?" : "&");
                queryString.AppendFormat("returnUrl={0}", UrlHelper.ToSafeUrlParameter(_returnUrl));
            }
            if (!string.IsNullOrEmpty(_nodeIds))
            {
                queryString.Append((queryString.Length == 0) ? "?" : "&");
                queryString.AppendFormat("nodeIds={0}", _nodeIds);
            }
            return "/Orion/AgentManagement/Admin/Deployment/Default.aspx" + queryString;
        }
    }

    #endregion // Private properties

    private string _returnUrl = null;
    private string _nodeIds;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = PageTitle;

        ((Orion_AgentManagement_Admin_AgentManagementAdminPage)this.Master).HelpFragment = "";    // todo: helplink
        InitialSetRadioButtons();
        ProcessUrlParameters();
    }

    private void InitialSetRadioButtons()
    {
        var isFipsEnabled = EnginesDAL.IsFIPSModeEnabledOnPrimaryEngine();
        installLocation.Enabled = !isFipsEnabled;
        fipsErrorMessage.Visible = isFipsEnabled;
        if (!IsPostBack)
        {
            installLocation.Checked = !isFipsEnabled;
            existingLocation.Checked = isFipsEnabled;
        }
    }

    private void ProcessUrlParameters()
    {
        if (!string.IsNullOrEmpty(Request["returnUrl"]))
            _returnUrl = UrlHelper.FromSafeUrlParameter(Request["returnUrl"]);
        if (!string.IsNullOrEmpty(Request["nodeIds"]))
            _nodeIds = Request["nodeIds"];

        // shortcut to skip initial selection and jump directly to correct wizard
        if (!string.IsNullOrEmpty(Request["action"]))
        {
            switch (Request["action"].ToLowerInvariant())
            {
                case "deploy":
                    Response.Redirect(InstallLocationUrl);
                    break;
                case "addexisting":
                    Response.Redirect(AddExistingLocationUrl);
                    break;
            }
        }
    }

    protected void nextButton_OnClick(object sender, EventArgs e)
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowAdmin)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                throw new AgentManagementUnauthorizedAccessException();
            else
                return;
        }

        // redirect to proper wizard/page
        if(existingLocation.Checked)
        {
            Response.Redirect(AddExistingLocationUrl);
        }
        else if (installLocation.Checked)
        {
            Response.Redirect(InstallLocationUrl);
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(_returnUrl))
            GoBack();
        else
            Response.Redirect(_returnUrl);
    }

    private void GoBack()
    {
        Response.Redirect("/Orion/AgentManagement/Admin/ManageAgents.aspx");
    }
}
