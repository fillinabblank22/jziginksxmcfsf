using System;
using SolarWinds.AgentManagement.Common.Helpers;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_AgentManagement_Admin_DownloadAgent : System.Web.UI.Page, IBypassAccessLimitation
{
    protected readonly string AgentCommunicationPortDescription =
        String.Format(Resources.AgentManagementWebContent.AgentCommunicationPortDescription,
        "<a class=\"sw-link\" href=\"/Orion/AgentManagement/Admin/ManageAgents.aspx\"><b>Settings > Manage Agents</b></a>");

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        deployAgentInfo.Visible = !EnginesDAL.IsFIPSModeEnabledOnPrimaryEngine();

        IsLinuxAgentEnabled =
                new FeatureHelper(SwisConnectionProxyPool.GetSystemCreator()).IsFeatureEnabled(
                    FeatureHelper.LinuxAgentFeature);
    }

    protected bool IsLinuxAgentEnabled { get; set; }

    protected string MassDeploymentHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgentHowToMassDeployAgent"); }
    }

    protected string ManualDeploymentHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgentHowToManuallyInstallAgent"); }
    }

    protected string UsingMstHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgentUsingMSTFiles"); }
    }

    protected string UsingGoldImageLinkUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_DeployGoldMasterImage"); }
    }

    protected string SpecifyPollingEngineHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgentSpecifyingPollingEngineMore"); }
    }

    protected string AddingOrionLinuxRepositoryHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_AddRepository"); }
    }

    protected string LinuxManualDeploymentHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_ManuallyDeployLinuxAgent"); }
    }

    protected string LinuxRepositoryDeploymentHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_DeployAgentfromRepository"); }
    }
}
