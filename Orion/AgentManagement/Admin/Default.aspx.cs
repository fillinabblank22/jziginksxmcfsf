using System;
using SolarWinds.AgentManagement.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;

public partial class Orion_AgentManagement_Admin_Default : System.Web.UI.Page, IBypassAccessLimitation
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_AgentManagement_Admin_AgentManagementAdminPage)Master).HelpFragment = "OrionAgentSettings";
        IsOrcFeatureEnabled = new FeatureManager().IsFeatureEnabled(FeatureConstants.RemoteCollectorFeature);
        if (IsOrcFeatureEnabled)
        {
            Page.Title = Resources.AgentManagementWebContent.MainSettingsPageTitle_WithRemoteCollector;
        } else
        {
            Page.Title = Resources.AgentManagementWebContent.MainSettingsPageTitle;
        }
    }

    protected bool IsOrcFeatureEnabled { get; set; }
}
