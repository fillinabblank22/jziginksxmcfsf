﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_AgentManagement_Admin_Default"  
    MasterPageFile="~/Orion/AgentManagement/Admin/AgentManagementAdminPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <style type="text/css">
        body { background: #ecedee; }
        #adminNav { display: none; }
        #adminContent p { border: none; width: auto; padding: 0; font-size: 12px;}
        #adminContent a:hover {color:orange;}
        .column1of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
        .column2of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <h1 style="padding-bottom:2px;"><%= Page.Title %></h1>
    
	<table class="AgentManagement_AdminContent">
		  <tr>
			<td class="column1of2">
				
                <!-- Begin Management Bucket -->
				<div class="NewContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="AgentManagement_AdminIconCell">
								<img class="BucketIcon" src="/Orion/AgentManagement/Images/Icon.Agent_32x32.png" alt="" />
							</td>
							<td>
								<div class="NewBucketHeader"><%= Resources.AgentManagementWebContent.AgentManagementSettingsBucketTitle %></div>
								<p><%= Resources.AgentManagementWebContent.AgentManagementSettingsBucketDescription %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
							<td class="LinkColumn">
								<p>
								    <span class="LinkArrow">&#0187;</span> 
                                    <a href="/Orion/AgentManagement/Admin/ManageAgents.aspx"><%= Resources.AgentManagementWebContent.ManageAgentsLinkLabel %></a>
								</p>
							</td>
                            <td class="LinkColumn">
								<p>
								    <span class="LinkArrow">&#0187;</span> 
                                    <a href="/Orion/AgentManagement/Admin/DownloadAgent.aspx"><%= Resources.AgentManagementWebContent.DownloadAgentLinkLabel %></a>
								</p>
							</td>
						</tr>
						<% if (IsOrcFeatureEnabled)
							{ %>
                        <tr>
                            <td class="LinkColumn">
                                <p>
                                    <span class="LinkArrow">&#0187;</span> 
                                    <a href="/Orion/AgentManagement/Admin/ManageRemoteCollectors.aspx"><%= Resources.AgentManagementWebContent.ManageRemoteCollectors_LinkLabel %></a>
                                </p>
                            </td>
                            <td class="LinkColumn">
                            </td>
                        </tr>
						<% } %>
					</table>
				</div>
				<!-- End Management Bucket -->

                <!-- Begin Settings Bucket -->
				<div class="NewContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="AgentManagement_AdminIconCell">
								<img class="BucketIcon" src="/Orion/AgentManagement/Images/Icon.Settings_36x36.gif" alt="" />
							</td>
							<td>
								<div class="NewBucketHeader"><%= Resources.AgentManagementWebContent.GlobalAgentsSettingsBucketTitle %></div>
								<p><%= Resources.AgentManagementWebContent.GlobalAgentsSettingsBucketDescription %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
							<td class="LinkColumn">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/AgentManagement/Admin/Settings.aspx"><%= Resources.AgentManagementWebContent.GlobalAgentSettingsLinkLabel %></a></p>
							</td>
						</tr>	
					</table>
				</div>
				<!-- End Settings Bucket -->
				

			</td>
            <td class="column2of2"></td>
		</tr>
	</table>
	<br/>

</asp:Content>