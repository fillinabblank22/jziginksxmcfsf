﻿<%@ Page Language="C#" MasterPageFile="~/Orion/AgentManagement/Admin/AgentManagementAdminPage.master" AutoEventWireup="true" CodeFile="AddAgent.aspx.cs" Inherits="Orion_AgentManagement_Admin_AddAgent" %>
<%@ Import Namespace="SolarWinds.AgentManagement.Web" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <orion:Include runat="server" File="OrionCore.js" />

    <h1><%= Page.Title %></h1>
    <div class="GroupBox">
        <h2><%= SelectMethodText %></h2>
        <table class="sw-AgentManagement-radiolist">
            <tr class="sw-AgentManagement-row">
                <td><asp:RadioButton ID="installLocation" GroupName="LocationType" runat="server" /></td>
                <td><img src="/Orion/AgentManagement/Images/install_location_16x16.png" alt="<%= InstallLocationOnNetworkLabelTitle %>" width="16" height="16" /></td>
                <td>
                    <asp:Label AssociatedControlID="installLocation" runat="server">
                        <span class="sw-AgentManagement-description"><%= InstallLocationOnNetworkLabelTitle %></span><br />
                        <span class="sw-text-helpful"><%= InstallLocationOnNetworkLabelDetail %></span>
                    </asp:Label>
                </td>
                <td>
                    <asp:Label ID="fipsErrorMessage" AssociatedControlID="installLocation" runat="server">
                        <span class="sw-suggestion-fail"><%= InstallLocationOnNetworkLabelFipsError %></span>
                    </asp:Label>
                </td>
            </tr>
            <tr class="sw-AgentManagement-row">
                <td><asp:RadioButton ID="existingLocation" GroupName="LocationType" runat="server" /></td>
                <td><img src="/Orion/images/add_16x16.gif" alt="<%= AddExistingLocationLabelTitle %>" width="16" height="16" /></td>
                <td>
                    <asp:Label AssociatedControlID="existingLocation" runat="server">
                        <span class="sw-AgentManagement-description"><%= AddExistingLocationLabelTitle %></span><br />
                        <span class="sw-text-helpful"><%= AddExistingLocationLabelDetail %></span><br />
                        <a href="/Orion/AgentManagement/Admin/DownloadAgent.aspx" class="AgentManagement_HelpLink" target="_blank">&#0187; <%= Resources.AgentManagementWebContent.DownloadAgentLinkLabel %></a>
                    </asp:Label>
                </td>
            </tr>
        </table>
    </div>
    
    <div class="AgentManagement_WorkflowButtons">
        <orion:LocalizableButton ID="nextButton" LocalizedText="Next" DisplayType="Primary" runat="server" OnClick="nextButton_OnClick" OnClientClick="return AgentManagementDisableInDemo();" />
        <orion:LocalizableButton ID="cancelButton" LocalizedText="Cancel" DisplayType="Secondary" runat="server" CausesValidation="false" OnClick="cancelButton_OnClick" />
    </div>
</asp:Content>
