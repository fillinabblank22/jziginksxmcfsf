﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_AgentManagement_Admin_AgentManagementManagementPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string HelpFragment
    {
        get
        {
            return ((Orion_AgentManagement_Admin_AgentManagementAdminPage)this.Master).HelpFragment;
        }
        set
        {
            ((Orion_AgentManagement_Admin_AgentManagementAdminPage)this.Master).HelpFragment = value;
        }
    }
}
