﻿using System;
using System.Web;
using SolarWinds.AgentManagement.Web.Exceptions;
using SolarWinds.Logging;

public partial class Orion_AgentManagement_Admin_AgentManagementAdminPage : System.Web.UI.MasterPage
{
    private static readonly Log log = new Log();

    protected bool _isOrionDemoMode = false;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!((ProfileCommon)HttpContext.Current.Profile).AllowAdmin)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                _isOrionDemoMode = true;
            else
                throw new AgentManagementUnauthorizedAccessException();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global",
                                                           this.ResolveClientUrl("~/Orion/AgentManagement/js/AgentManagement.js"));

        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }
    }

    public string HelpFragment { get; set; }
}
