﻿<%@ Page Title="<%$ Resources: AgentManagementWebContent, GlobalSettingsPageTitle %>" Language="C#" MasterPageFile="~/Orion/AgentManagement/Admin/AgentManagementAdminPage.master" AutoEventWireup="true" 
    CodeFile="Settings.aspx.cs" Inherits="Orion_AgentManagement_Admin_Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= Page.Title %></h1>
    <table class="AgentManagement_SettingsTable">
        <tr>
            <td>
                <label for="<%= chbAllowAutoregistration.ClientID %>">
                    <input type="checkbox" runat="server" id="chbAllowAutoregistration" />
                    <%= Resources.AgentManagementWebContent.AllowAutoRegistrationLabel %>
                </label>
            </td>
            <td><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" ext:qtip="<%= Resources.AgentManagementWebContent.AllowAutoRegistrationDescription %>" /></td>
        </tr>
        <tr>
            <td>
                <label for="<%= chbAllowAutoNodeCreate.ClientID %>">
                    <input type="checkbox" runat="server" id="chbAllowAutoNodeCreate" />
                    <%= Resources.AgentManagementWebContent.AllowAutoNodeCreateLabel %>
                </label>
                <br/>
                <div class="sw-suggestion" style="width: 380px;margin-left: 10px;margin-top: 4px;">
                    <span class="sw-suggestion-icon"></span>
                    <span id="suggestionText"><%= Resources.AgentManagementWebContent.AllowAutoNodeCreateNote %></span>
                </div>
            </td>
            <td><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" ext:qtip="<%= Resources.AgentManagementWebContent.AllowAutoNodeCreateDescription %>" /></td>
        </tr>
        <tr>
            <td>
                <label for="<%= chbAllowAutomaticUpdates.ClientID %>">
                    <input type="checkbox" runat="server" id="chbAllowAutomaticUpdates" />
                    <%= Resources.AgentManagementWebContent.AllowAutomaticUpdatesLabel %>
                </label>
            </td>
            <td><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" ext:qtip="<%= Resources.AgentManagementWebContent.AllowAutomaticUpdatesDescription %>" /></td>
        </tr>
        <tr class="AgentManagement_SettingDisplayAsNew">
            <td>
                <div><%: Resources.AgentManagementWebContent.Settings_AgentsDisplayAsNewInGridSettingsLabel %></div>
                <input type="text" runat="server" id="ibHowLongIsAgentDisplayedAsNew" />
                <label for="<%= ibHowLongIsAgentDisplayedAsNew.ClientID %>"><%: Resources.AgentManagementWebContent.Settings_AgentsDisplayAsNewInGridSettingsHoursLabel %></label>
                <asp:RangeValidator runat="server" 
                    id="ibHowLongIsAgentDisplayedAsNewRangeValidator"
                    ControlToValidate="ibHowLongIsAgentDisplayedAsNew"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" id="ibHowLongIsAgentDisplayedAsNewRequiredFieldValidator" 
                    ControlToValidate="ibHowLongIsAgentDisplayedAsNew"></asp:RequiredFieldValidator>
            </td>
            <td><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" ext:qtip="<%= Resources.AgentManagementWebContent.Settings_AgentsDisplayAsNewInGridSettingsDescription %>" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <orion:LocalizableButton runat="server" ID="restoreDefaults" Text="<%$ Resources: AgentManagementWebContent, RestoreDefaultsButtonLabel %>" DisplayType="Secondary" OnClientClick="AgentManagement_RestoreDefaults(); return false;" />
            </td>
        </tr>
    </table>
    <orion:LocalizableButton runat="server" ID="imgbtnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="ImgbtnSubmitClick" OnClientClick="return AgentManagementDisableInDemo();"/>
    <orion:LocalizableButton runat="server" ID="imgbtnCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="ImgbtnCancelClick" />
    
    <script type="text/javascript">
        $(document).ready(function() {
            Ext.QuickTips.init();
        });
        
        function AgentManagement_RestoreDefaults() {
            if (AgentManagementIsDemoMode()) return AgentManagementDemoModeMessage();

            $(".AgentManagement_SettingsTable").find("input").each(function() {
                var defaultValue = $(this).attr('defaultValue');

                if (typeof (defaultValue) === 'undefined')
                    return;
                
                if ($(this).is(':checkbox')) {
                    $(this).prop('checked', defaultValue.toLowerCase() == "true");
                } else {
                    $(this).val(defaultValue);
                }
            });
        }
    </script>
</asp:Content>

