﻿<%@ Page Language="C#" MasterPageFile="~/Orion/AgentManagement/Admin/AgentManagementAdminPage.master" AutoEventWireup="true" CodeFile="EditAgent.aspx.cs"
    Inherits="Orion_AgentManagement_Admin_EditAgent" %>

<%@ Import Namespace="SolarWinds.AgentManagement.Web" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Orion/js/OrionCore.js" />
            <asp:ScriptReference Path="~/Orion/AgentManagement/js/EditAgent.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    
    <h1><%= Page.Title %></h1>
    <p>
        <%= MainDescription %>
        <a href="<%= InstallingAgentHelpUrl %>" class="sw-link" target="_blank" rel="noopener noreferrer"><span class="LinkArrow">&#0187;</span> <%= LearnMoreLabel %></a>
        <a href="/Orion/AgentManagement/Admin/DownloadAgent.aspx" class="sw-link" target="_blank"><span class="LinkArrow">&#0187;</span> <%= Resources.AgentManagementWebContent.DownloadAgentLinkLabel %></a>
    </p>
    
    <div class="GroupBox AgentSettings">
        <asp:HiddenField runat="server" ID="originalAgentDnsNameBox" />
        <asp:HiddenField runat="server" ID="originalAgentIpBox" />
        <asp:HiddenField runat="server" ID="originalAgentPollingEngineId" />
        <asp:HiddenField runat="server" ID="originalAgentPollingEngineName" />
        <asp:HiddenField runat="server" ID="agentIdBox" />
        <asp:HiddenField runat="server" ID="agentDnsNameBox" />
        <asp:HiddenField runat="server" ID="agentIpBox" />
        <asp:HiddenField runat="server" ID="originalAgentGuidBox" />
        <asp:HiddenField runat="server" ID="originalAgentHostnameBox" />
        <asp:HiddenField runat="server" ID="originalAgentModeBox" />
        <asp:HiddenField runat="server" ID="agentStatusBox" />
        <asp:HiddenField runat="server" ID="agentStatusMessageBox" />
        <asp:HiddenField runat="server" ID="pollingEngineActiveId" />
        <asp:HiddenField runat="server" ID="agentVersionFromTest" />

        <asp:Label runat="server" AssociatedControlID="nameBox"><b><%= NameLabel %>:</b></asp:Label>
        <asp:TextBox runat="server" ID="nameBox" CssClass="AgentManagement_EditBox" />
        <asp:CustomValidator runat="server" ID="nameValidator" ControlToValidate="nameBox" Display="Dynamic" />

        <span class="AgentManagement_AgentSettingsSectionLabel"><b><%= CommunicationTypeLabel %></b></span>

        <asp:Label runat="server" AssociatedControlID="passiveRadio">
            <asp:RadioButton runat="server" ID="passiveRadio" GroupName="modeGroup" />
            <%= PassiveRadioLabel %>
            <a href="<%= LearnMorePassiveUrl %>" class="AgentManagement_HelpLink" target="_blank" rel="noopener noreferrer">
                <span class="LinkArrow">&#0187;</span>
                <%= LearnMoreLabel %>
            </a>
        </asp:Label>
        <div class="AgentManagement_HelpText"><%= PassiveHelpText %></div>

        <!-- PASSIVE config -->
        <div class="AgentManagement_AgentAdvancedSettings" id="AgentManagement_PassiveSettings" style="display: none;">
            <asp:Label runat="server" AssociatedControlID="passiveAgentHostnameBox"><%= IPLabel %>:</asp:Label>
            <asp:TextBox runat="server" ID="passiveAgentHostnameBox" CssClass="AgentManagement_EditBox" />
            <asp:CustomValidator runat="server" ID="passiveAgentHostnameValidator" ControlToValidate="passiveAgentHostnameBox" Display="Dynamic" />

            <!-- SharedSceret field-->
            <div id="AgentManagement_SharedSecretDix" style="padding-bottom : 1em;">
                <asp:Label runat="server" AssociatedControlID="sharedSecretBox"><%= AgentPasswordLabel %>:</asp:Label>
                <div class="AgentManagement_HelpText"><%= AgentPasswordHelpText %></div>
                <asp:TextBox runat="server" TextMode="Password" ID="sharedSecretBox" CssClass="AgentManagement_EditBox" />
            </div>

            <!-- PASSIVE advanced config -->
            <div id="AgentManagement_ToggleAdvancedPassiveDiv">
                <img id="CollapsePassiveImg" src="/Orion/Images/Button.Expand.gif" />
                <%= AdvancedLabel %>
                <asp:CheckBox ID="advancedPassiveCheckbox" runat="server" Style="display: none;" />
            </div>
            <div id="AgentManagement_AgentAdvancedPassiveSettings" style="display: none;">

                <!-- Port field -->
                <asp:Label runat="server" AssociatedControlID="passiveAgentPortBox"><%= PortLabel%>:</asp:Label>
                <asp:TextBox runat="server" ID="passiveAgentPortBox" CssClass="AgentManagement_EditBox" />
                <asp:CustomValidator runat="server" ID="passiveAgentPortValidator" ControlToValidate="passiveAgentPortBox" Display="Dynamic" />

                <div style="display: <%= ShowPollerDropDown ? "block" : "none" %>;">
                    <asp:Label runat="server" AssociatedControlID="pollingEnginePassive"><%= AssignToPollerLabel %>:</asp:Label>
                    <asp:DropDownList ID="pollingEnginePassive" runat="server" CssClass="AgentManagement_EditBox">
                    </asp:DropDownList>
                    <asp:CustomValidator runat="server" ID="passivePollerValidator" ControlToValidate="pollingEnginePassive" Display="Dynamic" />
                </div>

                <!-- Proxy definition fields -->
                <asp:Label runat="server" AssociatedControlID="useProxyCheckbox">
                    <asp:CheckBox runat="server" ID="useProxyCheckbox" />
                    <%= UseProxyLabel %>
                </asp:Label>
                <div id="AgentManagement_ProxySettings" style="display: none;">                   
                    <!-- Select proxy settings -->	
                    <asp:DropDownList ID="proxySettingsList" runat="server" CssClass="AgentManagement_EditBox" />

                    <asp:Label runat="server" AssociatedControlID="proxyUrlBox"><%= ProxyUrlLabel %>:
                        <span class="LinkArrow">&#0187;</span> <a href="<%= WhatIsProxyHelpUrl %>" class="AgentManagement_HelpLink" target="_blank" rel="noopener noreferrer"><%= WhatIsProxyLabel %></a>
                    </asp:Label>
                    <asp:TextBox runat="server" ID="proxyUrlBox" CssClass="AgentManagement_EditBox" />
                    <asp:CustomValidator runat="server" ID="proxyValidator" ControlToValidate="proxyUrlBox" Display="Dynamic" />


                    <!-- Proxy authentication definition fields -->
                    <asp:Label runat="server" AssociatedControlID="useProxyAuthenticationCheckbox">
                        <asp:CheckBox runat="server" ID="useProxyAuthenticationCheckbox" />
                        <%= UseProxyAuthenticationLabel %></asp:Label>
                    <div id="AgentManagement_ProxyAuthenticationSettings" style="display: none;">
                        <asp:Label runat="server" AssociatedControlID="proxyUserNameBox"><%= ProxyUserNameLabel %>: </asp:Label>
                        <asp:TextBox runat="server" ID="proxyUserNameBox" CssClass="AgentManagement_EditBox" />
                        <asp:Label runat="server" AssociatedControlID="proxyPasswordBox"><%= ProxyPasswordLabel %>: </asp:Label>
                        <asp:TextBox runat="server" TextMode="Password" ID="proxyPasswordBox" CssClass="AgentManagement_EditBox" />
                    </div>
                </div>
            </div>
            <!-- / PASSIVE advanced config -->
        </div>
        <!-- / PASSIVE config -->

        <asp:Label runat="server" AssociatedControlID="activeRadio">
            <asp:RadioButton runat="server" ID="activeRadio" GroupName="modeGroup" Checked="True" Visible="true" />
            <%= ActiveRadioLabel %>
            <a href="<%= LearnMoreActiveUrl %>" class="AgentManagement_HelpLink" target="_blank" rel="noopener noreferrer">
                <span class="LinkArrow">&#0187;</span>
                <%= LearnMoreLabel %>
            </a>
        </asp:Label>
        <div class="AgentManagement_HelpText"><%= ActiveHelpText %></div>

        <!-- ACTIVE config -->
        <div class="AgentManagement_AgentAdvancedSettings" id="AgentManagement_ActiveSettings" style="display: none;">
            <asp:Label runat="server" AssociatedControlID="guidBox"><%= ActiveAgentLabel%>:</asp:Label>
            <select id="activeAgentsCombo" class="AgentManagement_EditBox">
            </select>
            <asp:TextBox runat="server" ID="guidBox" CssClass="AgentManagement_EditBox x-hidden" />
            <asp:CustomValidator runat="server" ID="guidValidator" ControlToValidate="guidBox" Display="Dynamic" />

            <!-- ACTIVE advanced config -->
            <div id="AgentManagement_ToggleAdvancedActiveDiv">
                <div style="display:  <%= ShowPollerDropDown ? "block" : "none" %>;">
                    <img id="CollapseActiveImg" src="/Orion/Images/Button.Expand.gif" />
                    <%= AdvancedLabel %>
                    <asp:CheckBox ID="advancedActiveCheckbox" runat="server" Style="display: none;" />
                </div>
            </div>
            <div id="AgentManagement_AgentAdvancedActiveSettings" style="display: none;">
                <div style="display:  <%= ShowPollerDropDown ? "block" : "none" %>;">
                    <asp:Label runat="server" AssociatedControlID="pollingEngineActive"><%= AssignedToPollerLabel %>:</asp:Label>
                    <asp:DropDownList ID="pollingEngineActive" runat="server" CssClass="AgentManagement_EditBox">
                    </asp:DropDownList>
                    <asp:CustomValidator runat="server" ID="activePollerValidator" ControlToValidate="pollingEngineActive" Display="Dynamic" />
                </div>
            </div>
            <!-- / ACTIVE advanced config -->
        </div>
        <!-- / ACTIVE config -->

        <asp:Label ID="Label1" runat="server" AssociatedControlID="enableAutoUpdateCheckbox">
            <asp:CheckBox runat="server" ID="enableAutoUpdateCheckbox" />
            <%= Resources.AgentManagementWebContent.EditAgentAllowAutomaticUpdateLabel %>
            <img src="/Orion/images/Icon.Lightbulb.gif" class="AgentManagement_Tooltip" title="<%= Resources.AgentManagementWebContent.EditAgentAllowAutomaticUpdateDescription %>" />
        </asp:Label>

        <% if (IsEditPage)
           { %>
        <!-- Remote settings config -->
        <%--<span class="AgentManagement_AgentSettingsSectionLabel"><b><%= RemoteSettingsLabel%>:</b></span>

        <asp:Label runat="server" AssociatedControlID="updateRemoteSettingsCheckbox"><asp:CheckBox runat="server" ID="updateRemoteSettingsCheckbox" /> <%= RemoteSettingsCheckboxLabel%></asp:Label>
        <div class="AgentManagement_HelpText"><%= RemoteSettingsHelpText%></div>--%>

        <!-- Troubleshooting config -->
        <div id="ToggleTroubleshootDiv">
            <img id="TroubleshootCollapseImg" src="/Orion/Images/Button.Expand.gif" />
            <%= TroubleshootingLabel%>
        </div>
        <div id="TroubleshootDivLoading" style="display: none;">
            <img src="/Orion/images/loading_gen_small.gif" />
            <%= LoadingText%> ...
        </div>
        <div id="TroubleshootDiv" style="display: none;">
            <asp:Label AssociatedControlID="logLevelCombo" runat="server" CssClass="AgentManagemnt_RequiresOnlineAgent"><%= LogLevelLabel %>:</asp:Label>
            <asp:DropDownList ID="logLevelCombo" runat="server" CssClass="AgentManagement_EditBox AgentManagemnt_RequiresOnlineAgent">
                <asp:ListItem Value="" Text="<%$ Resources : AgentManagementWebContent, EditAgent_LogLevelCombo_NoChange %>">No change</asp:ListItem>
                <asp:ListItem Value="OFF">OFF</asp:ListItem>
                <asp:ListItem Value="ERROR">ERROR</asp:ListItem>
                <asp:ListItem Value="WARN">WARN</asp:ListItem>
                <asp:ListItem Value="INFO">INFO</asp:ListItem>
                <asp:ListItem Value="DEBUG">DEBUG</asp:ListItem>
                <asp:ListItem Value="VERBOSE">VERBOSE</asp:ListItem>
            </asp:DropDownList>

            <label><%= DiagnosticsLabel%>:</label>
            <span id="AgentManagement_DiagnosticsStatus"></span>
            <br />
            <a href="javascript:void(0);" id="GenerateDiagnosticsLink" class="sw-link AgentManagemnt_RequiresOnlineAgent" target="_blank"><%= GenerateNewDiagnosticsLabel%></a>
        </div>
        <div id="AgentOfflineError" class="sw-suggestion sw-suggestion-fail" style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <%= LocationNotRespondingError%>
        </div>
        <!-- / Troubleshooting config -->
        <%
           } %>

        <div id="AgentManagement_TestProgress" style="display: none;">
            <img src="/Orion/images/loading_gen_small.gif" />
            <%= TestingText %> ...
        </div>
        <div id="AgentManagement_TestResults" class="sw-suggestion" style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <div class="sw-suggestion-title"></div>
            <div id="AgentManagement_TestDetail">
                <br />
                <%= AgentNotRespondingError %> <a href="<%= InstallingAgentHelpUrl %>" class="AgentManagement_HelpLink" target="_blank" rel="noopener noreferrer"><span class="LinkArrow">&#0187;</span> <%= LearnMoreLabel %></a>
            </div>
        </div>

        <%-- 
        <orion:LocalizableButton LocalizedText="Test" DisplayType="Small" runat="server" OnClick="testButton_Click" OnClientClick="return AgentManagementDisableInDemo();" CssClass="AgentManagement_AgentTestButton" />
        --%>

        <div class="AgentManagement_EditAgentButtons">
            <orion:LocalizableButton LocalizedText="Submit" DisplayType="Primary" runat="server" OnClientClick="return AgentManagementDisableInDemo();" OnClick="Save_Click" />
            <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" CausesValidation="false" OnClick="Cancel_Click" />
        </div>
    </div>

    <script type="text/javascript" defer="defer">
        //<![CDATA[
        $(document).ready(function () {
            SW.AgentManagement.EditAgent.SetAgentIdBoxId('<%= agentIdBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetAgentDNSNameBoxId('<%= agentDnsNameBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetAgentIPBoxId('<%= agentIpBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetNameBoxId('<%= nameBox.ClientID %>');

            SW.AgentManagement.EditAgent.SetPassiveAgentPortBoxId('<%= passiveAgentPortBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetUseProxyCheckboxId('<%= useProxyCheckbox.ClientID %>');
            SW.AgentManagement.EditAgent.SetProxyUrlBoxId('<%= proxyUrlBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetUseProxyAuthenticationCheckboxId('<%= useProxyAuthenticationCheckbox.ClientID %>');
            SW.AgentManagement.EditAgent.SetProxyUserNameBoxId('<%= proxyUserNameBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetProxyPasswordBoxId('<%= proxyPasswordBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetPassiveAgentHostnameBoxId('<%= passiveAgentHostnameBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetAdvancedPassiveCheckboxId('<%= advancedPassiveCheckbox.ClientID %>');
            SW.AgentManagement.EditAgent.SetPassiveRadioId('<%= passiveRadio.ClientID %>');

            SW.AgentManagement.EditAgent.SetGuidBoxId('<%= guidBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetOriginalGuidBoxId('<%= originalAgentGuidBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetOriginalModeBoxId('<%= originalAgentModeBox.ClientID %>');

            SW.AgentManagement.EditAgent.SetActiveRadioId('<%= activeRadio.ClientID %>');

            SW.AgentManagement.EditAgent.SetPollingEngineActiveId('<%= pollingEngineActiveId.ClientID %>');
            SW.AgentManagement.EditAgent.SetPollingEnginePassiveId('<%= pollingEnginePassive.ClientID %>');

            SW.AgentManagement.EditAgent.SetProxySettingsListId('<%= proxySettingsList.ClientID %>');

            SW.AgentManagement.EditAgent.SetAdvancedActiveCheckboxId('<%= advancedActiveCheckbox.ClientID %>');
            SW.AgentManagement.EditAgent.SetAgentStatusBoxId('<%= agentStatusBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetAgentStatusMessageBoxId('<%= agentStatusMessageBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetOriginalAgentIpBoxId('<%= originalAgentIpBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetOriginalAgentDnsNameBoxId('<%= originalAgentDnsNameBox.ClientID %>');
            SW.AgentManagement.EditAgent.SetOriginalAgentPollingEngineIdId('<%= originalAgentPollingEngineId.ClientID %>');
            SW.AgentManagement.EditAgent.SetAgentVersionFromTestId('<%= agentVersionFromTest.ClientID %>');
            <% if (IsEditPage)
               {%>
            SW.AgentManagement.EditAgent.SetLogLevelComboId('<%=logLevelCombo.ClientID%>');
            <%--
            SW.AgentManagement.EditAgent.SetUpdateRemoteSettingsCheckboxId('<%=updateRemoteSettingsCheckbox.ClientID%>');--%>
            <%
               }%>
            SW.AgentManagement.EditAgent.SetSavePostBack(function () { <%= SavePostBack %> });
            SW.AgentManagement.EditAgent.Init();

            <% if (RunTest)
               {%>
            SW.AgentManagement.EditAgent.TestAgentSettings();
            <%
               }%>
            <% if (RunTestAndSave)
               {%>
            SW.AgentManagement.EditAgent.TestAgentSettingsAndSave();
            <%
               }%>
        });
        //]]>
    </script>

    <!-- This is used to grab the results from a failed Web Service call -->
    <div id="test" class="x-hidden"></div>
</asp:Content>
