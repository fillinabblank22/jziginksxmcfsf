﻿using System;
using SolarWinds.Orion.Web;

public partial class Orion_AgentManagement_Admin_ManageAgents : System.Web.UI.Page, IBypassAccessLimitation
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_AgentManagement_Admin_AgentManagementManagementPage)this.Master).HelpFragment = "OrionAgentManageAgents";
    }
}
