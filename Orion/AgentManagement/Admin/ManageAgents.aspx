﻿<%@ Page Language="C#" MasterPageFile="~/Orion/AgentManagement/Admin/AgentManagementManagementPage.master" AutoEventWireup="true" 
         CodeFile="ManageAgents.aspx.cs" Inherits="Orion_AgentManagement_Admin_ManageAgents" Title="<%$ Resources: AgentManagementWebContent,ManageAgentsTitle %>" %>

<%@ Register TagPrefix="AgentManagement" TagName="AgentsGrid" Src="~/Orion/AgentManagement/Controls/AgentsGrid.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentHeaderPlaceholder" Runat="Server">
    <h1><%= Page.Title %></h1>
    
    <p>
        <%= Resources.AgentManagementWebContent.ManageAgentsDescriptionText %>
        <a href="/Orion/AgentManagement/Admin/DownloadAgent.aspx" class="AgentManagement_HelpLink" target="_blank">&#0187;&nbsp;<%= Resources.AgentManagementWebContent.DownloadAgentLinkText %></a>
    </p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <AgentManagement:AgentsGrid runat="server" ID="agentsGrid" />
</asp:Content>

