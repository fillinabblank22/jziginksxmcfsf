﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DownloadAgent.aspx.cs" Inherits="Orion_AgentManagement_Admin_DownloadAgent" 
    MasterPageFile="~/Orion/AgentManagement/Admin/AgentManagementAdminPage.master" Title="<%$ Resources: AgentManagementWebContent, DownloadAgentPageTitle %>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <orion:Include runat="server" File="ProgressIndicator.css" />
    <orion:Include runat="server" Module="AgentManagement" File="Download/WizardControl.js" />
    <orion:Include runat="server" Module="AgentManagement" File="Download/DynamicValueBinder.js" />
    <orion:Include runat="server" Module="AgentManagement" File="Download/DownloadWizardController.js" />
    <orion:Include runat="server" Module="AgentManagement" File="Download/clipboard.min.js" />
    
    <%-- 
        This page is using custom binding mechanism implemented in AgentMamanagement/js/Download/DynamicValueBinder.ts 
        Check that file for description of how the binding works.
    --%>

    <script type="text/javascript">
        var controller = null;
    </script>

    <h1><%= Page.Title %></h1>

    <div id="DownloadWizard" class="AgentManagement_DownloadWizard">

        <!-- ========================================================== -->
        <!-- BEGIN Machine type selection step -->
        <!-- ========================================================== -->
        <% if (IsLinuxAgentEnabled)
           { %>
        <div class="AgentManagement_WizardStep GroupBox">
            <div class="AgentManagement_WizardStepTitle"><%= Resources.AgentManagementWebContent.DownloadAgent_MachineType_StepTitle %></div>

            <%--<div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-machinetype-show="Windows">
                System requirements Windows
            </div>

            <div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-machinetype-show="Linux">
                System requirements Linux
            </div>--%>

            <div class="AgentManagement_DownloadWizardQuestion"><%= Resources.AgentManagementWebContent.DownloadAgent_MachineType_WhatMachineQuestion %></div>

            <label class="AgentManagement_RadioListItem">
                <input type="radio" name="machineType" data-ams-binding-source="machineType" value="Windows" checked="checked" />
                <span><%= Resources.AgentManagementWebContent.DownloadAgent_MachineType_WindowsRadioLabel %></span>
            </label>
            <label class="AgentManagement_RadioListItem">
                <input type="radio" name="machineType" data-ams-binding-source="machineType" value="Linux" />
                <span><%= Resources.AgentManagementWebContent.DownloadAgent_MachineType_LinuxRadioLabel %></span>
            </label>
            <hr class="AgentManagement_Cleaner" />
        </div>
        <% } %>
        <!-- ========================================================== -->
        <!-- END Machine type selection step -->
        <!-- ========================================================== -->

        <!-- ========================================================== -->
        <!-- BEGIN Deployment method selection step -->
        <!-- ========================================================== -->
        <div class="AgentManagement_WizardStep GroupBox">
            <div class="AgentManagement_WizardStepTitle"><%= Resources.AgentManagementWebContent.DownloadAgent_DeploymentMethod_StepTitle %></div>

            <div data-ams-machinetype-show="Windows">
                <div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-deploymentmethod-show="WindowsManual">
                    <img src="/Orion/AgentManagement/Images/Download/WindowsManual.png"/>
                </div>

                <div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-deploymentmethod-show="WindowsMass">
                    <img src="/Orion/AgentManagement/Images/Download/WindowsMass.png"/>
                </div>

                <div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-deploymentmethod-show="WindowsGoldenImage">
                    <img src="/Orion/AgentManagement/Images/Download/WindowsGoldenImage.png"/>
                </div>

                <div class="AgentManagement_DownloadWizardQuestion"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethod_HowToDeployWindowsQuestion %></div>

                <label class="AgentManagement_RadioListItem">
                    <input type="radio" name="deployMethod" data-ams-binding-source="deploymentMethod" value="WindowsManual" checked="checked" />
                    <span><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodWindows_ManualRadioLabel %></span>
                    <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodWindows_ManualDescription %></span>
                </label>
                <label class="AgentManagement_RadioListItem">
                    <input type="radio" name="deployMethod" data-ams-binding-source="deploymentMethod" value="WindowsMass" />
                    <span><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodWindows_MassRadioLabel %></span>
                    <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodWindows_MassDescription %></span>
                </label>
                <label class="AgentManagement_RadioListItem">
                    <input type="radio" name="deployMethod" data-ams-binding-source="deploymentMethod" value="WindowsGoldenImage" />
                    <span><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodWindows_GoldenImageRadioLabel %></span>
                    <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodWindows_GoldenImageDescription %></span>
                </label>
                <hr class="AgentManagement_Cleaner" />
            </div>

            <div data-ams-machinetype-show="Linux">
                <div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-deploymentmethod-show="LinuxManual">
                    <img src="/Orion/AgentManagement/Images/Download/LinuxManual.png"/>
                </div>

                <div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-deploymentmethod-show="LinuxPackage">
                    <img src="/Orion/AgentManagement/Images/Download/LinuxPackage.png"/>
                </div>

                <%--<div class="AgentManagement_DownloadWizard_DetailsBox" data-ams-deploymentmethod-show="LinuxMass">
                    Mass deployment image picture
                </div>--%>

                <div class="AgentManagement_DownloadWizardQuestion"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethod_HowToDeployLinuxQuestion %></div>

                <label class="AgentManagement_RadioListItem">
                    <input type="radio" name="deployMethod" data-ams-binding-source="deploymentMethod" value="LinuxManual" />
                    <span><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodLinux_ManualRadioLabel %></span>
                    <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodLinux_ManualDescription %></span>
                </label>
                <label class="AgentManagement_RadioListItem">
                    <input type="radio" name="deployMethod" data-ams-binding-source="deploymentMethod" value="LinuxPackage" />
                    <span><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodLinux_PackageRadioLabel %></span>
                    <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodLinux_PackageDescription %></span>
                </label>
                <%--<label class="AgentManagement_RadioListItem">
                    <input type="radio" name="deployMethod" data-ams-binding-source="deploymentMethod" value="LinuxMass" disabled="disabled" />
                    <span><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodLinux_MassRadioLabel %> - COMING SOON!</span>
                    <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_DeployMethodLinux_MassDescription %></span>
                </label>--%>
                <hr class="AgentManagement_Cleaner" />
            </div>
        </div>

        <!-- ========================================================== -->
        <!-- END Deployment method selection step -->
        <!-- ========================================================== -->

        <!-- ========================================================== -->
        <!-- BEGIN Setting & files step -->
        <!-- ========================================================== -->
        <div class="AgentManagement_WizardStep GroupBox">
            <div class="AgentManagement_WizardStepTitle"><%= Resources.AgentManagementWebContent.DownloadAgent_SettingAndFiles_StepTitle %></div>

            <div class="AgentManagement_DownloadMethodHelpLink">
                <a href="<%= ManualDeploymentHelpUrl %>" class="sw-link" target="_blank" rel="noopener noreferrer" data-ams-deploymentmethod-show="WindowsManual">&#0187; <%= Resources.AgentManagementWebContent.DownloadAgent_LearnMoreAboutManualWindowsInstall %></a>
                <a href="<%= MassDeploymentHelpUrl %>" class="sw-link" target="_blank" rel="noopener noreferrer" data-ams-deploymentmethod-show="WindowsMass">&#0187; <%= Resources.AgentManagementWebContent.DownloadAgent_LearnMoreAboutMassWindowsInstall %></a>
                <a href="<%= UsingGoldImageLinkUrl %>" class="sw-link" target="_blank" rel="noopener noreferrer" data-ams-deploymentmethod-show="WindowsGoldenImage">&#0187; <%= Resources.AgentManagementWebContent.DownloadAgent_LearnMoreAboutGoldenImageWindowsInstall %></a>
                <a href="<%= LinuxManualDeploymentHelpUrl %>" class="sw-link" target="_blank" rel="noopener noreferrer" data-ams-deploymentmethod-show="LinuxManual">&#0187; <%= Resources.AgentManagementWebContent.DownloadAgent_LearnMoreAboutManualLinuxInstall %></a>
                <a href="<%= LinuxRepositoryDeploymentHelpUrl %>" class="sw-link" target="_blank" rel="noopener noreferrer" data-ams-deploymentmethod-show="LinuxPackage">&#0187; <%= Resources.AgentManagementWebContent.DownloadAgent_LearnMoreAboutPackageLinuxInstall %></a>
                <%--<a href="#TODO" class="sw-link" target="_blank" rel="noopener noreferrer" data-ams-deploymentmethod-show="LinuxMass">&#0187; <%= Resources.AgentManagementWebContent.DownloadAgent_LearnMoreAboutMassLinuxInstall %></a>--%>
            </div>
            
            <div class="AgentManagement_DownloadWizardQuestion">
                <span data-ams-deploymentmethod-show="WindowsMass"><%= Resources.AgentManagementWebContent.DownloadAgent_Files_WindowsMassTitle %></span>
                <span data-ams-deploymentmethod-show="WindowsGoldenImage"><%= Resources.AgentManagementWebContent.DownloadAgent_Files_WindowsGoldenImageTitle %></span>
                <span data-ams-deploymentmethod-show="LinuxManual"><%= Resources.AgentManagementWebContent.DownloadAgent_Files_LinuxManualTitle %></span>
                <span data-ams-deploymentmethod-show="LinuxPackage"><%= Resources.AgentManagementWebContent.DownloadAgent_Files_LinuxPackageTitle %></span>
            </div>
            
            <hr class="AgentManagement_Cleaner"/>

            <div data-ams-deploymentmethod-show="WindowsManual">
                <div class="AgentManagement_DownloadFileBox">
                    <a class="AgentManagement_DownloadButton" href="/api/AgentManagementDownloadAgent/DownloadWindowsInstaller" onclick="return AgentManagementDisableInDemo();">
                        <%= Resources.AgentManagementWebContent.DownloadMsiButtonLabel %>
                    </a>
                    <span class="AgentManagement_FileName">SolarWinds-Agent.msi</span>
                    <%= Resources.AgentManagementWebContent.MassDeploymentLatestVersion %>: <span data-ams-msiinstallerversion-value></span>
                </div>
            </div>
            
            <div data-ams-deploymentmethod-show="LinuxManual|LinuxPackage">
                <!-- UNIX/Linux distribution -->
                <div class="AgentManagement_SeparatorHeader"><%= Resources.AgentManagementWebContent.DownloadAgent_LinuxDistributionSectionHeader %></div>
                
                <label for="AMS_LinuxPackageDropdown"><%= Resources.AgentManagementWebContent.DownloadAgent_LinuxDistributionDropdownLabel %>:</label>
                <select id="AMS_LinuxPackageDropdown" data-ams-binding-source="linuxPackage"></select>
            </div>
            
            <div data-ams-deploymentmethod-show="WindowsMass|WindowsGoldenImage|LinuxManual">
                <!-- Agent mode - Active/Passive -->
                <div class="AgentManagement_SeparatorHeader"><%= Resources.AgentManagementWebContent.DownloadAgent_CommunicationModeSectionHeader %></div>
                <table>
                    <tr>
                        <td width="50%">
                            <label class="AgentManagement_RadioListItem">
                                <input type="radio" name="agentMode" data-ams-binding-source="agentMode" value="active" checked="checked" />
                                <span><%= Resources.AgentManagementWebContent.MassDeploymentAgentInitiatedCommunicationLabel %></span>
                                <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_ActiveHelpText %></span>
                            </label>
                        </td>
                        <td width="50%">
                            <label class="AgentManagement_RadioListItem">
                                <input type="radio" name="agentMode" data-ams-binding-source="agentMode" value="passive" />
                                <span><%=Resources.AgentManagementWebContent.MassDeploymentServerInitiatedCommunicationLabel%></span>
                                <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_PassiveHelpText %></span>
                            </label>
                        </td>
                    </tr>
                </table>

                <!-- Connection settings -->
                <div class="AgentManagement_SeparatorHeader"><%= Resources.AgentManagementWebContent.DownloadAgent_ConnectionSettingsSectionHeader %></div>

                <!-- Connection settings - Active -->
                <div data-ams-agentmode-show="active">
                    <div class="sw-suggestion AgentManagement_DownloadAgent_ModeHint">
                        <span class="sw-suggestion-icon"></span>
                        <%= Resources.AgentManagementWebContent.MassDeploymentPollingEngineIPAddressWarning %>
                        <a href="<%= SpecifyPollingEngineHelpUrl %>" class="sw-link">&raquo;&nbsp;<%= Resources.AgentManagementWebContent.More %></a>
                    </div>

                    <select data-ams-binding-source="connectionSpec" class="AgentManagement_DownloadAgent_ConnectionSettingDropdown">
                        <option value="engine" selected="selected"><%= Resources.AgentManagementWebContent.MassDeploymentUseEngineConnectionDetails %></option>
                        <option value="manual"><%= Resources.AgentManagementWebContent.MassDeploymentUseManualConnectionDetails %></option>
                    </select>

                    <table data-ams-connectionspec-show="engine" class="AgentManagement_DownloadAgent_ConnectionSettingsTable">
                        <tr>
                            <td>
                                <label for="AMS_EnginesDropdown"><%= Resources.AgentManagementWebContent.MassDeploymentPollingEngineLabel %>:</label></td>
                            <td>
                                <select id="AMS_EnginesDropdown" data-ams-binding-source="pollingEngineId"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label><%= Resources.AgentManagementWebContent.MassDeploymentPollingEngineIPLabel %>:</label></td>
                            <td><span data-ams-pollingengineip-value></span></td>
                        </tr>
                    </table>

                    <table data-ams-connectionspec-show="manual" class="AgentManagement_DownloadAgent_ConnectionSettingsTable">
                        <tr>
                            <td>
                                <label for="hostnameTextBox"><%= Resources.AgentManagementWebContent.MassDeploymentHostnameLabel %>:</label></td>
                            <td>
                                <input type="text" id="hostnameTextBox" data-ams-binding-source="manualHostname" class="AgentManagement_TextInput" />
                                <span data-ams-manualhostname-error class="AgentManagement_ErrorText" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="ipTextBox"><%= Resources.AgentManagementWebContent.MassDeploymentIPAddressLabel %>:</label></td>
                            <td>
                                <input type="text" id="ipTextBox" data-ams-binding-source="manualIp" class="AgentManagement_TextInput" />
                                <span data-ams-manualip-error class="AgentManagement_ErrorText" />
                            </td>
                        </tr>
                    </table>

                    <!-- Advanced config -->
                    <div class="AgentManagement_DownloadAgent_ConnectionSettingsAdvancedSection">
                        <label class="AgentManagement_DownloadAgent_ConnectionSettingsAdvancedLabel">
                            <input type="checkbox" data-ams-binding-source="advancedSettings" value="1" class="hidden" />
                            <img src="/Orion/Images/Button.Expand.gif" data-ams-advancedsettings-hide="1" />
                            <img src="/Orion/Images/Button.Collapse.gif" data-ams-advancedsettings-show="1" />
                            <%= Resources.AgentManagementWebContent.EditAgent_AdvancedLabel %>
                        </label>
                    
                        <div data-ams-advancedsettings-show="1">
                            <!-- Proxy access type -->
                            <table class="AgentManagement_DownloadAgent_ConnectionSettingsTable">
                                <tr>
                                    <td>
                                        <label><%= Resources.AgentManagementWebContent.MassDeployment_ProxyAccessType_Label %></label>
                                    </td>
                                    <td>
                                        <div>
                                            <select data-ams-binding-source="proxyType">
                                                <option value="0" selected="selected"><%= Resources.AgentManagementWebContent.MassDeployment_ProxyAccessType_NoProxy %></option>
                                                <option value="2"><%= Resources.AgentManagementWebContent.MassDeployment_ProxyAccessType_AutoDetect %></option>
                                                <option value="1"><%= Resources.AgentManagementWebContent.MassDeployment_ProxyAccessType_UseDefault %></option>
                                                <option value="3"><%= Resources.AgentManagementWebContent.MassDeployment_ProxyAccessType_UserDefined %></option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <!-- Proxy hostname and port -->
                            <div data-ams-proxytype-show="3">
                                <table class="AgentManagement_DownloadAgent_ConnectionSettingsTable">
                                    <tr>
                                        <td>
                                            <label for="proxyHostnameTextBox"><%= Resources.AgentManagementWebContent.MassDeployment_ProxyHostname_Label %></label></td>
                                        <td>
                                            <input type="text" id="deploymentProxyHostname" data-ams-binding-source="agentProxyHostname" class="AgentManagement_TextInput" />
                                            <span data-ams-agentproxyhostname-error class="AgentManagement_ErrorText" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="proxyPortTextBox"><%= Resources.AgentManagementWebContent.MassDeployment_ProxyPort_Label %></label></td>
                                        <td>
                                            <input type="text" id="deploymentProxyPort" data-ams-binding-source="agentProxyPort" class="AgentManagement_TextInput" />
                                            <span data-ams-agentproxyport-error class="AgentManagement_ErrorText" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- Proxy authentication definition fields -->
                            <div data-ams-proxytype-show="1|3">
                                <label class="AgentManagement_DownloadAgent_ProxyAuthenticationCheckbox">
                                    <input type="checkbox" value="1" data-ams-binding-source="proxyAuthentication" />
                                    <%= Resources.AgentManagementWebContent.EditAgent_UseProxyAuthenticationLabel %>
                                </label>
                                <div data-ams-proxyauthentication-show="1">
                                    <table class="AgentManagement_DownloadAgent_ConnectionSettingsTable">
                                        <tr>
                                            <td>
                                                <label for="deploymentProxyUsername"><%= Resources.AgentManagementWebContent.DeploymentProxyUsernameLabel %> </label>
                                            </td>
                                            <td>
                                                <input type="text" id="deploymentProxyUsername" data-ams-binding-source="agentProxyUsername" class="AgentManagement_TextInput" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="deploymentProxyPassword"><%= Resources.AgentManagementWebContent.DeploymentProxyPasswordLabel %> </label>
                                            </td>
                                            <td>
                                                <input type="password" id="deploymentProxyPassword" data-ams-binding-source="agentProxyPassword" class="AgentManagement_TextInput" /></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <hr class="AgentManagement_Cleaner"/>
                </div>

                <!-- Connection settings - Passive -->
                <div data-ams-agentmode-show="passive">
                    <div class="sw-suggestion AgentManagement_DownloadAgent_ModeHint">
                        <span class="sw-suggestion-icon"></span>
                        <%= AgentCommunicationPortDescription %>
                    </div>

                    <table class="AgentManagement_DownloadAgent_ConnectionSettingsTable">
                        <tr>
                            <td>
                                <label for="agentCommunicationPort"><%= Resources.AgentManagementWebContent.AgentCommunicationPortLabel.Replace(" ", "&nbsp;") %></label></td>
                            <td>
                                <input type="text" class="AgentManagement_TextInput" data-ams-binding-source="agentCommunicationPort" value="<%= SolarWinds.AgentManagement.Common.Constants.PassiveAgentPort.ToString() %>" />
                                <span data-ams-agentcommunicationport-error class="AgentManagement_ErrorText" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><span class="sw-text-helpful"><%= Resources.AgentManagementWebContent.AgentCommunicationPortHint %></span></td>
                        </tr>
                    </table>
                    
                    <hr class="AgentManagement_Cleaner"/>
                </div>
            </div>

            <div data-ams-deploymentmethod-show="WindowsMass|WindowsGoldenImage">
                <!-- File downloads -->
                <div class="AgentManagement_SeparatorHeader"><%= Resources.AgentManagementWebContent.DownloadAgent_FileDownloadsSectionHeader %></div>

                <!-- MSI -->
                <div class="AgentManagement_DownloadFileBox" data-ams-deploymentmethod-show="WindowsMass">
                    <a class="AgentManagement_DownloadButton" href="/api/AgentManagementDownloadAgent/DownloadWindowsInstaller" onclick="return AgentManagementDisableInDemo();">
                        <%= Resources.AgentManagementWebContent.DownloadMsiButtonLabel %>
                    </a>
                    <span class="AgentManagement_FileName">SolarWinds-Agent.msi</span>
                    <%= Resources.AgentManagementWebContent.MassDeploymentLatestVersion %>: <span data-ams-msiinstallerversion-value></span>
                </div>

                <!-- MST -->
                <div class="AgentManagement_DownloadFileBox" data-ams-deploymentmethod-show="WindowsMass">
                    <a class="AgentManagement_DownloadButton" href="javascript:void(0);" onclick="controller.downloadMstFile();">
                        <%= Resources.AgentManagementWebContent.DownloadMstButtonLabel %>
                    </a>

                    <span class="AgentManagement_FileName">SolarWinds-Agent.mst</span>
                    <%= Resources.AgentManagementWebContent.MassDeploymentLatestVersion %>: <%= Resources.AgentManagementWebContent.GeneratedFile %>
                    <br />
                    <a href="<%= UsingMstHelpUrl %>" target="_blank" rel="noopener noreferrer" class="sw-link">&raquo; <%= Resources.AgentManagementWebContent.UsingMstFilesLinkLabel %></a>
                </div>

                <!-- Golden master image -->
                <div class="AgentManagement_DownloadFileBox" data-ams-deploymentmethod-show="WindowsGoldenImage">
                    <a class="AgentManagement_DownloadButton" href="javascript:void(0);" onclick="controller.downloadWindowsOfflineInstaller();">
                        <%= Resources.AgentManagementWebContent.DownloadOfflineInstallerButtonLabel %>
                    </a>

                    <span class="AgentManagement_FileName"><%= Resources.AgentManagementWebContent.DownloadOfflineInstallerLabel %></span>
                    <%= Resources.AgentManagementWebContent.MassDeploymentLatestVersion %>: <span data-ams-msiinstallerversion-value></span>
                    <br />
                    <a href="<%= UsingGoldImageLinkUrl %>" target="_blank" rel="noopener noreferrer" class="sw-link">&raquo; <%= Resources.AgentManagementWebContent.UsingGoldImageLinkUrlLabel %></a>
                </div>
            </div>
            
            <div data-ams-deploymentmethod-show="LinuxManual">
                <!-- File downloads -->
                <div class="AgentManagement_SeparatorHeader"><%= Resources.AgentManagementWebContent.DownloadAgent_InstallCommandSectionHeader %></div>
                <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_InstallCommandHelpText %></span>
                
                <div class="AgentManagement_BlueBox">
                    <label><%= Resources.AgentManagementWebContent.DownloadAgent_CommandLabel %>:</label>
                    <div data-ams-linuxManualInstallCommand-hide="|loading">
                        <pre id="AgentManagement_CommandText_1" class="AgentManagement_CommandText"><span data-ams-linuxManualInstallCommand-value></span></pre>
                        <a class="sw-btn sw-btn-primary AgentManagement_CopyButton" href="javascript:void(0);" data-clipboard-target="#AgentManagement_CommandText_1">
                            <span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.AgentManagementWebContent.DownloadAgent_CopyLabel %></span></span>
                        </a>                        
                        <a class="AgentManagement_ViewScriptLink" href="javascript:void(0);" onclick="controller.showLinuxInstallScript(); return false;">
                            <%= Resources.AgentManagementWebContent.DownloadAgent_ViewLinuxInstallScript %>
                        </a>
                        <span class="AgentManagement_Light"><%= Resources.AgentManagementWebContent.DownloadAgent_AgentSoftwareVersion %>: <span data-ams-linuxPackageVersion-value></span></span>
                        <hr class="AgentManagement_Cleaner"/>
                    </div>
                    <span data-ams-linuxManualInstallCommand-error class="AgentManagement_ErrorText"></span>
                    <div data-ams-linuxManualInstallCommand-show="">
                        <a class="sw-btn sw-btn-primary" href="javascript:void(0);" onclick="controller.generateLinuxOnlineInstallCommand(); return false;">
                            <span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.AgentManagementWebContent.DownloadAgent_GenerateCommandLabel %></span></span>
                        </a>
                    </div>
                    <div data-ams-linuxManualInstallCommand-show="loading">
                        <img src="/Orion/images/loading_gen_small.gif"/>
                    </div>
                </div>
                
                <div>
                    <a href="javascript:void(0);" onclick="controller.downloadLinuxOfflineInstaller();">
                        <%= Resources.AgentManagementWebContent.DownloadAgent_LinuxManualDownloadLinkLabel %>
                    </a>
                </div>
            </div>
            
            <div data-ams-deploymentmethod-show="LinuxPackage">
                <!-- Adding the repository -->
                <div class="AgentManagement_SeparatorHeader">1. <%= Resources.AgentManagementWebContent.DownloadAgent_AddingRepositorySectionHeader %></div>
                <a href="<%= AddingOrionLinuxRepositoryHelpUrl %>" target="_blank" rel="noopener noreferrer" class="sw-link AgentManagement_RightHelpLink">
                    &raquo; <%= Resources.AgentManagementWebContent.DonwloadAgent_AddingOrionLinuxRepository %>
                </a>
                <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_AddingRepositoryHelpText %></span>
                
                <div class="AgentManagement_BlueBox">
                    <label><%= Resources.AgentManagementWebContent.DownloadAgent_CommandLabel %>:</label>
                    <pre id="AgentManagement_CommandText_2" class="AgentManagement_CommandText"><span data-ams-linuxPackageAddRepositoryCommand-value></span></pre>
                    <a class="sw-btn sw-btn-primary AgentManagement_CopyButton" href="javascript:void(0);" data-clipboard-target="#AgentManagement_CommandText_2">
                        <span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.AgentManagementWebContent.DownloadAgent_CopyLabel %></span></span>
                    </a>     
                    <hr class="AgentManagement_Cleaner"/>
                </div>
                
                <!-- Installing the agent -->
                <div class="AgentManagement_SeparatorHeader">2. <%= Resources.AgentManagementWebContent.DownloadAgent_InstallingAgentSectionHeader %></div>
                <span class="AgentManagement_HelpText"><%= Resources.AgentManagementWebContent.DownloadAgent_InstallingAgentFromRepositoryHelpText %></span>
                
                <div class="AgentManagement_BlueBox">
                    <label><%= Resources.AgentManagementWebContent.DownloadAgent_CommandLabel %>:</label>
                    <pre id="AgentManagement_CommandText_3" class="AgentManagement_CommandText"><span data-ams-linuxPackageInstallCommand-value></span></pre>
                    <a class="sw-btn sw-btn-primary AgentManagement_CopyButton" href="javascript:void(0);" data-clipboard-target="#AgentManagement_CommandText_3">
                        <span class="sw-btn-c"><span class="sw-btn-t"><%= Resources.AgentManagementWebContent.DownloadAgent_CopyLabel %></span></span>
                    </a>  
                    <hr class="AgentManagement_Cleaner"/>
                </div>
            </div>
        </div>
        <!-- ========================================================== -->
        <!-- END Setting & files step -->
        <!-- ========================================================== -->

        <div class="AgentManagement_WorkflowButtons">
            <orion:LocalizableButton runat="server" ID="backButton" LocalizedText="Back" OnClientClick="return false;" />&nbsp;
            <orion:LocalizableButton runat="server" ID="nextButton" LocalizedText="Next" DisplayType="Primary" OnClientClick="return false;" />&nbsp;
            <orion:LocalizableButton runat="server" ID="cancelButton" LocalizedText="Cancel" OnClientClick="return false;" />
            <orion:LocalizableButton runat="server" ID="doneButton" LocalizedText="Done" OnClientClick="return false;" />
        </div>
    </div>
    <asp:Panel id="deployAgentInfo" runat="server">
        <div class="sw-suggestion AgentManagement_DownloadAgent_GlobalHint">
            <span class="sw-suggestion-icon"></span>
            <b><%= Resources.AgentManagementWebContent.DownloadAgent_LookingToPushAgentFromOrionTitle %></b>
            <br/>
            <%= Resources.AgentManagementWebContent.DownloadAgent_LookingToPushAgentFromOrionText %>
            <a href="/Orion/AgentManagement/Admin/Deployment/Default.aspx" class="sw-link"> <%= Resources.AgentManagementWebContent.DownloadAgent_DeployAgentFromOrion %></a>
        </div>
    </asp:Panel>

    <script type="text/javascript">
        $(function () {
            var wizard = new AgentManagement.Wizard($('#DownloadWizard'), '<%= backButton.ClientID %>', '<%= nextButton.ClientID %>', '<%= cancelButton.ClientID %>', '<%= doneButton.ClientID %>');
            controller = new AgentManagement.DownloadWizardController();
            new Clipboard('.AgentManagement_CopyButton');
        });
    </script>
</asp:Content>
