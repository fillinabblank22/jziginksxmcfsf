﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.AgentManagement.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_AgentManagement_Discovery_Controls_AgentDiscoveryPlugin : System.Web.UI.UserControl
{
    private Log _log = new Log();
    private long? _agentsCount;

    protected long AgentsCount
    {
        get
        {
            if (_agentsCount == null)
            {
                var dal = new AgentsDAL();
                try
                {
                    _agentsCount = dal.GetAgentsCount();
                }
                catch (Exception ex)
                {
                    _log.Error("Error on GetAgentsCount", ex);
                }
            }

            return _agentsCount ?? 0;
        }
    }

    protected string ReturnUrl
    {
        get
        {
            var currentUrl = Request.Url.AbsoluteUri;
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(currentUrl);
            return Convert.ToBase64String(plainTextBytes); 
        }
    }

    protected string NAgentsHaveRegisteredLocalizedHtmlText
    {
        get
        {
            var encodedFormatText =
                HttpUtility.HtmlEncode(Resources.AgentManagementWebContent.AgentDiscoveryPlugin_NAgentsHaveRegistered);
            var agentCountHtml = String.Format("<span style='font-weight:bold'>{0}</span>", AgentsCount);

            return String.Format(encodedFormatText, agentCountHtml);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (AgentsCount == 0)
        {
            monitoredObjectsInfo.Attributes["class"] = "noObjectsStyle";
            bulbImage.Visible = true;
            okImage.Visible = false;
        }
    }

    protected void DeployAgentOnMyNetworkButton_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect(String.Format("~/Orion/AgentManagement/Admin/Deployment/Default.aspx?returnUrl={0}", ReturnUrl));
    }

    protected void DownloadAgentInstallationFilesButton_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect(String.Format("~/Orion/AgentManagement/Admin/DownloadAgent.aspx?returnUrl={0}", ReturnUrl));
    }
}