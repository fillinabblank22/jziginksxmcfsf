﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentDiscoveryPlugin.ascx.cs" Inherits="Orion_AgentManagement_Discovery_Controls_AgentDiscoveryPlugin" %>

<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
	<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/AgentManagement/Images/agent.svg"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2><%: Resources.AgentManagementWebContent.AgentDiscoveryPlugin_Title %></h2>
    <div><%: Resources.AgentManagementWebContent.AgentDiscoveryPlugin_Description %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="AgentDiscoveryHelpLink" runat="server" HelpUrlFragment="OrionAgent-DiscoveryCentral"
        HelpDescription="<%$ Resources: AgentManagementWebContent, AgentDiscoveryPlugin_HelpLinkText %>" CssClass="helpLink" />
    <br />
    
     <div class="managedObjInfoWrapper">   
         <span id="monitoredObjectsInfo" runat="server">
             <asp:Image ID="okImage" runat="server" ImageUrl="~/Orion/images/ok_16x16.gif" CssClass="stateImage"></asp:Image>
             <asp:Image ID="bulbImage" runat="server" ImageUrl="~/Orion/images/lightbulb_tip_16x16.gif" Visible="false" CssClass="stateImage"></asp:Image>
             <%= NAgentsHaveRegisteredLocalizedHtmlText%></span>
     </div>
    
    <orion:LocalizableButton ID="deployAgentOnMyNetworkButton" runat="server" LocalizedText="CustomText" OnClick="DeployAgentOnMyNetworkButton_Click"
            Text="<%$ Resources: AgentManagementWebContent, AgentDiscoveryPlugin_DeployAgentOnMyNetworkLabel %>" DisplayType="Secondary"/>
    
    <orion:LocalizableButton ID="downloadAgentInstallationFilesButton" runat="server" LocalizedText="CustomText" OnClick="DownloadAgentInstallationFilesButton_Click"
            Text="<%$ Resources: AgentManagementWebContent, AgentDiscoveryPlugin_DownloadAgentInstallationFilesLabel %>" DisplayType="Secondary"/>

</div>