﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master" AutoEventWireup="true"
    CodeFile="AgentNodesSelection.aspx.cs" Inherits="Orion_Discovery_Config_AgentNodesSelection" Title="<%$ Resources: AgentManagementWebContent, DiscoveryStepTitle %>" %>
<%@ Import Namespace="SolarWinds.AgentManagement.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" Module="AgentManagement" File="AgentManagement.css"/>
     <script type="text/javascript">
        function createAutoCompleteFields() {
            $('.conditionRow').each(function(index, element) {
                var propertyBox = $(element).find('.propertyColumn select');
                var valueBox = $(element).find('.valueColumn input');

                propertyBox.change(function() {
                    $(valueBox).flushCache();
                });

                $(valueBox).autocomplete("/Orion/Admin/Containers/PropertyAutocompleteProvider.ashx",
                {
                    cacheLength: 10,
                    max: 100,
                    matchCase: false,
                    matchContains: true,
                    scroll: true,
                    selectFirst: true,
                    minChars: 0,
                    highlight: function(value, term)
                    {
                        return value.replace(term, '<b>' + term + '</b>');
                    },
                    extraParams: {
                        entityName: function() {
                            return <%= SwisEntities.FullyQualifiedNames.Nodes %>;
                        },
                        propertyName: function() {
                            return propertyBox.val();
                        }
                    }
                });
            });
        }

        function refreshOperators(propertyBox) {
            var operatorBox = $(propertyBox).parents('.conditionRow').find('.operatorColumn select');

            $.ajax({
                url: '/Orion/Admin/Containers/PropertyOperatorProvider.ashx',
                dataType: 'json',
                data: {
                    entityName: <%= SwisEntities.FullyQualifiedNames.Nodes %>,
                    propertyName: $(propertyBox).val()
                },
                success: function (data) {
                    operatorBox.children().remove();
                    for (var i = 0; i < data.length; i++) {
                        operatorBox.append('<option value="' + data[i].value + '">' + data[i].label + '</option>');
                    }
                    operatorBox.width('150px');
                    storeOperatorValue(operatorBox);
                }
            });
        }

        function storeOperatorValue(operatorBox) {
            var operatorValueBox = $(operatorBox).siblings('input[type=hidden]');
            operatorValueBox.val($(operatorBox).val());
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div class="AgentManagement_DiscoveryStep">
        <span class="ActionName"><%= Resources.AgentManagementWebContent.DiscoveryStepHeader %></span>
        <p class="AgentManagement_Description">
            <%= Resources.AgentManagementWebContent.DiscoveryStepDescription %>
            <a href="<%= LearnMoreUrl %>" target="_blank" rel="noopener noreferrer" class="sw-link">&raquo; <%= Resources.AgentManagementWebContent.LearnMore %></a>
        </p>

        <asp:CheckBox runat="server" ID="discoverAgentNodesCheckBox"  />
        <asp:Label runat="server" AssociatedControlID="discoverAgentNodesCheckBox" Text="<%$ Resources: AgentManagementWebContent, DiscoverAgentNodesCheckboxLabel %>" />
        
        <div id="discoveryFilterSection" runat="server" hidden>
            <div id="discoverAgentsRadioSection" runat="server" style="padding-left:24px;">
            <p>
                <asp:RadioButton runat="server" ID="discoverAllAgentNodesRadio" GroupName="DynamicQueryFilterMode" AutoPostBack="true" OnCheckedChanged="discoverAllAgentNodesRadio_CheckedChanged" />
                <asp:Label runat="server" AssociatedControlID="discoverAllAgentNodesRadio" Text="<%$ Resources: AgentManagementWebContent, DiscoverAllAgentNodesRadioLabel %>"  />
            </p>
            <p>
                <asp:RadioButton runat="server" ID="discoverOnlySpecificAgentNodesRadio" GroupName="DynamicQueryFilterMode" AutoPostBack="true" OnCheckedChanged="discoverAllAgentNodesRadio_CheckedChanged" />
                <asp:Label runat="server" AssociatedControlID="discoverOnlySpecificAgentNodesRadio" Text="<%$ Resources: AgentManagementWebContent, DiscoverOnlySpecificAgentNodesRadioLabel %>" />
            </p>            
            
                <div id="QueryBuilderSection" runat="server">   
	            <div class="ContentBucket"> 
	                <div class="dynamicQueryEditorPanel">
	                    <table id="dynamicQueryEditorTable">
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <table width="100%" class="dynamicQueryTable">
                                                <asp:Repeater ID="ConditionsRepeater" runat="server" OnItemDataBound="ConditionsRepeater_ItemDataBound" OnItemCommand="ConditionsRepeater_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr class="conditionRow">
                                                            <td class="propertyColumn">
                                                                <asp:DropDownList ID="propertyBox" runat="server" onchange="refreshOperators(this);" />

                                                            </td>
                                                            <td class="operatorColumn">
                                                                <asp:DropDownList ID="operatorBox" runat="server" onchange="storeOperatorValue(this);" />
                                                                <asp:HiddenField ID="operatorBoxValue" runat="server" />
                                                            </td>
                                                            <td class="valueColumn"><asp:TextBox ID="valueBox" runat="server" /></td>
                                                            <td class="deleteColumn">
                                                                <asp:ImageButton ID="removeConditionButton" ImageUrl="~/Orion/images/Button.RemoveCondition.gif" 
                                                                runat="server" CommandName="Remove" CausesValidation="false" />
                                                            </td>
                                                            <td>
                                                            <asp:CustomValidator ID="ConditionValidator" runat="server" Display="Dynamic" OnServerValidate="ConditionValidation" />
                                                            <span style="padding-left:15px;">&nbsp;</span>
                                                            <%if (this.FirstCondition && this.NoValidatorIsShown)
                                                              { %>
                                                            <asp:Label ID="HintLablel" runat="server" Text="<%$ Resources: AgentManagementWebContent, BuildDynamicQueryCharacterB %>" ></asp:Label>
                                                            <%} %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                 <tr>
                                                    <td class="dynamicQueryBuilderColumn">
                                                        <orion:LocalizableButton ID="addConditionButton" LocalizedText="CustomText" Text="<%$ Resources: AgentManagementWebContent, BuildDynamicQueryAddConditionLabel %>" DisplayType="Small" runat="server" CausesValidation="false" OnClick="AddCondition" />
                                                        <orion:LocalizableButton ID="previewButton" LocalizedText="CustomText" Text="<%$ Resources: AgentManagementWebContent, BuildDynamicQueryPreviewLabel %>" DisplayType="Primary" runat="server" OnClick="Preview" AutoPostBack="true" />
                                                    </td>
                                                    <td class="dynamicQueryBuilderColumn">&nbsp;</td>
                                                    <td class="dynamicQueryBuilderColumn">&nbsp;</td>
                                                </tr>
	                                        </table>
	                                    </ContentTemplate>
	                                </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CustomValidator ID="ConditionsValidator" ErrorMessage="<%$ Resources: AgentManagementWebContent, BuildDynamiQueryConditionCountValidatorHint %>" runat="server" Display="Dynamic" OnServerValidate="ConditionsValidation" />
                                </td>
                            </tr>
                        </table>
	                </div>

                    <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div id="dynamicQueryPreviewPanel" class="dynamicQueryPreviewPanel" runat="server">
                        <asp:Label runat="server" id="MoreItemsHeader" Text="" />
                        <table id="dynamicQueryPreviewTable">
	                    <asp:Repeater ID="PreviewRepeater" runat="server">
                            <HeaderTemplate>
                            </HeaderTemplate>
	                        <ItemTemplate>
	                            <tr  class="ZebraStripe"><td><%# ((AgentNodeDescriptor)this.GetDataItem()).Caption %></td></tr>
	                        </ItemTemplate>
	                        <AlternatingItemTemplate>
	                            <tr><td><%# ((AgentNodeDescriptor)this.GetDataItem()).Caption %></td></tr>
	                        </AlternatingItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
	                    </asp:Repeater>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="MoreItemsLabel" Text="<%$ Resources: AgentManagementWebContent, BuildDynamicQueryAndModeLabel %>" Style="margin-left:10px;"></asp:Label>
                            </td>
                        </tr>
                        </table>
                        </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        </div>

        <br />
        <br />

        <span id="allNodesExplanationLabel" runat="server" class="sw-suggestion sw-suggestion-info">
            <span class="sw-suggestion-icon"></span>
            <%= Resources.AgentManagementWebContent.DiscoveryStepNodesInfo %>
            <a href="<%= LearnMoreUrl %>" target="_blank" rel="noopener noreferrer" class="sw-link">&raquo; <%= Resources.AgentManagementWebContent.LearnMore %></a>
        </span>
    
        <br />
        <br />

        <span class="sw-suggestion AgentManagement_NumAgentNodesInfo"><span class="sw-suggestion-icon"></span><%= string.Format(
              NumAgentNodes == 1 
              ? Resources.AgentManagementWebContent.DiscoveryStepNumAgentNodesPolledSingularLabelFormat
              : Resources.AgentManagementWebContent.DiscoveryStepNumAgentNodesPolledPluralLabelFormat, NumAgentNodes) %></span>

        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary"
                runat="server" ID="imgBack" OnClick="imgbBack_Click" CausesValidation="false" />
            <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary"
                runat="server" ID="imgbNext" OnClick="imgbNext_Click" CssClass="NetworkNextButton"/>
            <orion:LocalizableButton class="CancelButton" LocalizedText ="Cancel" DisplayType="Secondary" runat="server"
                ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
        </div>  
    </div>
</asp:Content>