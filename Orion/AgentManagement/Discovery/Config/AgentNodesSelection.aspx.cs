﻿using System;
using SolarWinds.AgentManagement.Web.DAL;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.Models;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Containers;
using System.Linq;
using System.ServiceModel;
using SolarWinds.InformationService.Contract2;
using SolarWinds.AgentManagement.Common;

public partial class Orion_Discovery_Config_AgentNodesSelection : ConfigWizardBasePage, IStep
{
    const string JAVASCRIPT_INCLUDE_KEY = "AgentNodesSelection";

    private DiscoveryConfiguration _discoveryConfiguration;
    private List<DynamicQueryCondition> _conditions;
    private List<EntityProperty> _entityProperties;


    private bool _firstCondition = true;
    public bool FirstCondition
    {
        get
        {
            if (_firstCondition)
            {
                _firstCondition = false;
                return true;
            }
            return false;
        }
    }

    public bool NoValidatorIsShown { get; set; }


    protected override void OnLoad(EventArgs e)
    {
        InitButtons(imgbCancel);

        _conditions = new List<DynamicQueryCondition>();

        _discoveryConfiguration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        if (_discoveryConfiguration == null)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails
            {
                Error = new Exception(Resources.AgentManagementWebContent.DiscoveryConfigurationMissing),
                Title = Resources.AgentManagementWebContent.DiscoveryConfigurationErrorTitle
            });
            return;
        }

        if (!IsPostBack)
        {
            dynamicQueryPreviewPanel.Visible = false;
            LoadConditionsFromSession();
        }

        
        var metadataDal = new SwisMetadataDAL();
        _entityProperties = metadataDal.GetEntityProperties(SwisEntities.FullyQualifiedNames.Nodes,
                                    SolarWinds.Orion.Web.DAL.ContainersMetadataDAL.PropertyType.FilterBy).ToList();

        AgentsDAL dal = new AgentsDAL();
        NumAgentNodes = dal.GetAgentNodesCount(ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineId);

        if (IsPostBack)
        {
            StoreConditions();
        }
        else
        {
            RefreshConditions();
        }

        base.OnLoad(e);
    }

    private void LoadConditionsFromSession()
    {
        CoreDiscoveryPluginConfiguration configuration = _discoveryConfiguration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        if (configuration != null)
        {
            var tmpConditions = DynamicQueryHelper.GetConditions(configuration.AgentsFilterDefinition ?? string.Empty).ToList();
            if (tmpConditions.Count > 0)
            {
                _conditions.AddRange(tmpConditions);
            }

            RefreshCheckboxes(configuration.DiscoverAgentNodes, tmpConditions.Count > 0);
        }
    }

    /// <summary>
    /// Read conditions from current form and save them
    /// </summary>
    private void StoreConditions()
    {
        foreach (RepeaterItem item in ConditionsRepeater.Items)
        {
            DropDownList propertyBox = item.FindControl("propertyBox") as DropDownList;
            DropDownList operatorBox = item.FindControl("operatorBox") as DropDownList;
            // this is required to allow client side modification of operators
            HiddenField operatorBoxValue = item.FindControl("operatorBoxValue") as HiddenField;
            TextBox valueBox = item.FindControl("valueBox") as TextBox;

            DynamicQueryCondition condition = new DynamicQueryCondition()
            {
                Property = _entityProperties.FirstOrDefault(p => p.FullyQualifiedName == propertyBox.SelectedValue),
                Operator = DynamicQueryOperator.Operators.FirstOrDefault(op => op.Value == operatorBoxValue.Value)
            };
            if (condition.Property != null)
            {
                if (condition.Property.IsDateTime)
                {
                    condition.SetDateTime(valueBox.Text, DateTimeKind.Local);
                }
                else
                {
                    condition.Value = valueBox.Text;
                }

                // set operators to select box according to current condition
                IEnumerable<DynamicQueryOperator> operators = DynamicQueryOperator.Operators;
                if ((condition.Property.IsStatus) || (condition.Property.IsBoolean))
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForEnum);
                else if ((condition.Property.IsNumeric) || (condition.Property.IsDateTime))
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForNumber);
                else if (condition.Property.IsString)
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForString);

                operatorBox.DataSource = operators.ToArray();
                operatorBox.DataBind();
                operatorBox.SelectedValue = condition.Operator;
            }
            _conditions.Add(condition);
        }
    }

    /// <summary>
    /// Refresh property reference, refresh UI repeater with stored conditions
    /// </summary>
    private void RefreshConditions()
    {
        foreach (DynamicQueryCondition condition in _conditions)
        {
            condition.Property = _entityProperties.FirstOrDefault(p => p.FullyQualifiedName == condition.Property.FullyQualifiedName);
        }
        RefreshRepeater();
    }

    private void SaveConfigurationForDiscoveryPlugin()
    {
        if (_discoveryConfiguration == null)
        {
            return;
        }

        CoreDiscoveryPluginConfiguration configuration = _discoveryConfiguration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

        if (configuration == null)
        {
            configuration = new CoreDiscoveryPluginConfiguration();
            _discoveryConfiguration.AddDiscoveryPluginConfiguration(configuration);
        }

        configuration.DiscoverAgentNodes = discoverAgentNodesCheckBox.Checked;
        if (configuration.DiscoverAgentNodes)
        {
            configuration.AgentsFilterDefinition = DynamicQueryHelper.GetFilterDefinition(SwisEntities.FullyQualifiedNames.Nodes, _conditions);
            var conditionsDal = new DynamicConditionsDal();
            configuration.AgentsFilterQuery = conditionsDal.ParseQueryDefinition(configuration.AgentsFilterDefinition);
        }
        else
        {
            configuration.AgentsFilterDefinition = null;
            configuration.AgentsFilterQuery = null;
        }
    }

    protected int NumAgentNodes { get; set; }

    protected void ConditionValidation(object source, ServerValidateEventArgs args)
    {
        // find current validator in repeater and process it
        for (int i = 0; i < ConditionsRepeater.Items.Count; i++)
        {
            CustomValidator validator = ConditionsRepeater.Items[i].FindControl("ConditionValidator") as CustomValidator;
            if (validator != source as CustomValidator)
                continue;

            if (validator == null)
            {
                continue;
            }
            validator.Text = string.Empty;


            if (!DynamicQueryHelper.ValidateCondition(_conditions[i]))
            {
                validator.Text = _conditions[i].Error;
                args.IsValid = false;
                NoValidatorIsShown = false;
                return;
            }
        }
    }

    protected void ConditionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem is DynamicQueryCondition)
        {
            DynamicQueryCondition condition = e.Item.DataItem as DynamicQueryCondition;

            DropDownList propertyBox = e.Item.FindControl("propertyBox") as DropDownList;
            DropDownList operatorBox = e.Item.FindControl("operatorBox") as DropDownList;
            // this is required to allow client side modification of operators
            HiddenField operatorBoxValue = e.Item.FindControl("operatorBoxValue") as HiddenField;
            TextBox valueBox = e.Item.FindControl("valueBox") as TextBox;

            if (propertyBox == null)
                return;

            propertyBox.DataTextField = "DisplayName";
            propertyBox.DataValueField = "FullyQualifiedName";
            propertyBox.DataSource = _entityProperties;
            propertyBox.DataBind();

            IEnumerable<DynamicQueryOperator> operators = DynamicQueryOperator.Operators;

            if (condition.Property == null)
            {
                condition.Property = _entityProperties.FirstOrDefault();
            }

            string value = condition.Value;

            if (condition.Property != null)
            {
                propertyBox.SelectedValue = condition.Property.FullyQualifiedName;

                if ((condition.Property.IsStatus) || (condition.Property.IsBoolean))
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForEnum);
                else if ((condition.Property.IsNumeric) || (condition.Property.IsDateTime))
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForNumber);
                else if (condition.Property.IsString)
                    operators = DynamicQueryOperator.Operators.Where(o => o.IsForString);

                if (condition.Property.IsDateTime)
                {
                    value = condition.GetDateTime(DateTimeKind.Local);
                }
                else if (condition.Property.IsDecimal && value != null)
                {
                    // trim possible .0 that was added to definition for save
                    if (value.EndsWith(".0"))
                        value = value.Substring(0, value.Length - 2);
                }
            }

            operatorBox.DataTextField = "Label";
            operatorBox.DataValueField = "Value";
            operatorBox.DataSource = operators.ToArray();
            operatorBox.DataBind();
            operatorBox.SelectedValue = condition.Operator;
            
            // this is required to allow client side modification of operators
            operatorBoxValue.Value = condition.Operator.Value;

            valueBox.Text = value;
        }
    }

    protected void ConditionsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            _conditions.RemoveAt(e.Item.ItemIndex);
            RefreshRepeater();
        }
    }

    protected void RefreshRepeater()
    {
        ConditionsRepeater.DataSource = _conditions;
        ConditionsRepeater.DataBind();
    }

    protected void AddCondition(object sender, EventArgs e)
    {
        _conditions.Add(new DynamicQueryCondition());
        RefreshRepeater();
    }

    protected void ConditionsValidation(object source, ServerValidateEventArgs args)
    {
        if (discoverOnlySpecificAgentNodesRadio.Checked && _conditions.Count == 0)
        {
            args.IsValid = false;
        }
    }

    protected void Preview(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        dynamicQueryPreviewPanel.Visible = true;

        var dal = new AgentsDAL();
        int maxRecords = dal.GetAgentNodesCount(ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineId);

        String filterDefinition = string.Empty;
        try
        {
            filterDefinition = DynamicQueryHelper.GetFilterDefinition(SwisEntities.FullyQualifiedNames.Nodes, _conditions);
            var filterString = new DynamicConditionsDal().ParseQueryDefinition(filterDefinition);
            var dt = new NodesDAL().GetNodeNamesByFilter(filterString, ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineId).ToList();
            
            MoreItemsHeader.Text = Resources.AgentManagementWebContent.BuildDynamicQueryNodesPreview;
            bool moreItemsVisible = dt.Count > maxRecords;
            if (moreItemsVisible)
            {
                if (dt.Count > 0)
                {
                    dt.RemoveAt(dt.Count - 1);
                }

                MoreItemsLabel.Text = String.Format(Resources.AgentManagementWebContent.BuildDynamicQueryShowingOnlyXLabel, dt.Count);
            }
            MoreItemsLabel.Visible = moreItemsVisible;

            List<AgentNodeDescriptor> members = new List<AgentNodeDescriptor>();

            foreach (var row in dt)
            {
               members.Add(new AgentNodeDescriptor(){Caption = row});
            }

            if (members.Count == 0)
            {
                members.Add(new AgentNodeDescriptor{Caption = Resources.AgentManagementWebContent.BuildDynamicQueryNoResultLabel });
            }

            RefreshPreview(members);
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            log.ErrorFormat("Error displaying dynamic query preview. Parameters: filterDefinition={0}", filterDefinition);
            log.Error(e);

            throw new Exception(String.Format(Resources.AgentManagementWebContent.BuildDynamicQueryUnablePreviewLabel, ex.Detail.Message));
        }
    }

    protected void Save(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        string definition = DynamicQueryHelper.GetFilterDefinition(SwisEntities.FullyQualifiedNames.Nodes, _conditions);

        // add query to session if available
        if (Request["sid"] != null)
        {
            IList<string> members = (IList<string>)Session[Request["sid"]];
            if (members != null)
            {
                members.Add(definition);
                Session[Request["sid"]] = members;
            }
        }
    }

    protected void RefreshPreview(IEnumerable<AgentNodeDescriptor> members)
    {
        PreviewRepeater.DataSource = members.ToList();
        PreviewRepeater.DataBind();
    }


    protected override void Next()
    {
        SaveConfigurationForDiscoveryPlugin();
    }

    public string Step
    {
        get
        {
            return "AgentNodes";
        }
    }

    protected string LearnMoreUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgentDiscovery"); }
    }

    private void RefreshCheckboxes(bool isAgentEnabled, bool containsConditions)
    {
        discoverAgentNodesCheckBox.Checked = isAgentEnabled;
        discoverAllAgentNodesRadio.Checked = !containsConditions;
        discoverOnlySpecificAgentNodesRadio.Checked = !discoverAllAgentNodesRadio.Checked;
        QueryBuilderSection.Visible = discoverOnlySpecificAgentNodesRadio.Checked;
        dynamicQueryPreviewPanel.Visible = false;
    }

    protected void discoverAllAgentNodesRadio_CheckedChanged(object sender, EventArgs e)
    {
        if (discoverOnlySpecificAgentNodesRadio.Checked)
        {
            AddCondition(null, null);
        }
        else
        {
            _conditions.Clear();
            RefreshRepeater();
        }

        RefreshCheckboxes(discoverAgentNodesCheckBox.Checked,discoverOnlySpecificAgentNodesRadio.Checked);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var cs = Page.ClientScript;
        if (!cs.IsClientScriptIncludeRegistered(JAVASCRIPT_INCLUDE_KEY))
        {
            cs.RegisterClientScriptInclude(JAVASCRIPT_INCLUDE_KEY, ResolveUrl("~/Orion/AgentManagement/Discovery/js/AgentNodesSelection.js"));
        }
    }

    public class AgentNodeDescriptor
    {
        public string Caption { get; set; }
    }
}
