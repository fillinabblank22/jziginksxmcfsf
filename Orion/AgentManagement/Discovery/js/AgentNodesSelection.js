﻿$(document).ready(function () {
    let checkBoxName = "[id$='discoverAgentNodesCheckBox']";
    let radioButtonsSection = "[id$='discoveryFilterSection']";

    if ($(checkBoxName)[0].checked) {
        $(radioButtonsSection).show();
    } else {
        $(radioButtonsSection).hide();
    }

    // bind checkbox on change
    $(checkBoxName).change(function (e) {
        let targetValue = e.target.checked;
        if (!targetValue) {
            $(radioButtonsSection).hide();
        } else {
            $(radioButtonsSection).show();
        }
    });
});