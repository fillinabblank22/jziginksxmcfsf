﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentsGrid.ascx.cs" Inherits="Orion_AgentManagement_Controls_AgentsGrid" %>
<%@ Import Namespace="SolarWinds.AgentManagement.Web.Configuration" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="~/Orion/js/OrionCore.js"/>
        <asp:ScriptReference Path="~/Orion/AgentManagement/js/SearchField.js" />
        <asp:ScriptReference Path="~/Orion/AgentManagement/js/CheckboxSelectionModel.js" />
        <asp:ScriptReference Path="~/Orion/AgentManagement/js/AgentsGrid.js" />
    </Scripts>
</asp:ScriptManagerProxy>

<orion:Include runat="server" Module="AgentManagement" File="AsyncFileUpload.js" />

<div id="AgentManagement_GroupingPanel" class="AgentManagement_GroupingPanel">
	<div class="GroupSection">
		<label for="groupBySelect"><%: Resources.AgentManagementWebContent.ManageAgents_GroupBy %>:</label>
		<select id="groupBySelect">
			<option value="test"/>
		</select>
	</div>
    <div id="TestID"></div>
</div>

<div id="AgentsGrid" class="AgentManagement_ExtJsGrid"></div>

<div id="ForceDeployWindow" style="display: none;">
    <div class="x-panel-body sw-AgentManagement-credential-window">
            <p><%= ChooseExistingOrCreateNewOneText %></p>
            
            <table class="sw-AgentManagement-CredentialsTable">
                <tr class="sw-AgentManagement-OsTypeRow">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_OSType %>:</td>
                    <td>
                        <label id="OSTypeWindowsRadioLabel">
                            <input type="radio" id="OSTypeWindowsRadio" name="osType" value="windows" checked="checked" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_OSTypeWindows %>
                        </label>
                        <label id="OSTypeLinuxRadioLabel">
                            <input type="radio" id="OSTypeLinuxRadio" name="osType" value="linux" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_OSTypeLinux %>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td class="sw-AgentManagement-SpacerRow" colspan="2"></td>
                </tr>

                <tr class="sw-AgentManagement-CredentialTypeRow">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_CredentialType %>:</td>
                    <td>
                        <label>
                            <input type="radio" id="CredentialTypeUsernamePasswordRadio" name="credentialType" value="usernameAndPassword" checked="checked" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_UsernamePasswordCredentials %>
                        </label>
                        <label>
                            <input type="radio" id="CredentialTypeCertificateRadio" name="credentialType" value="privateKey" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_CertificateCredentials %>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td class="firstColumn"><%= ChooseCredentialText %></td>
                    <td>
                        <div class="sw-AgentManagement-UsernamePasswordItem">
                            <select id="CredentialNamesCombo">
                                <option value="test">test</option>
                            </select>
                        </div>
                        <div class="sw-AgentManagement-PrivateKeyItem">
                            <select id="SshCredentialNamesCombo">
                                <option value="test">test</option>
                            </select>
                        </div>
                    </td>
                </tr>

                <tr class="sw-AgentManagement-PrimaryCustomCredential">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.CredentialName %>:</td>
                    <td>
                        <input type="text" id="CredentialName" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential">
                    <td class="firstColumn"><%= UserNameText %></td>
                    <td>
                        <input type="text" id="CredentialUsername" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential sw-AgentManagement-UsernamePasswordItem">
                    <td class="firstColumn"><%= PasswordText %></td>
                    <td>
                        <input type="password" id="CredentialPassword" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential sw-AgentManagement-PrivateKeyItem">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_PrivateKey %>:</td>
                    <td>
                        <div id="CertificateViaFileBlock">
                            <input type="file" id="CredentialPrivateKeyFile" name="file" value="" />
                            <br />
                            <a href="javascript:void(0);" onclick="SW.AgentManagement.AgentsGrid.ShowPrivateKeyDialog();">
                                <%= Resources.AgentManagementWebContent.Deployment_Settings_EnterPrivateKeyManually %>
                            </a>
                        </div>
                        <div id="CertificateManualBlock">
                            <span id="CredentialPrivateKeyManualLabel"><%= Resources.AgentManagementWebContent.Deployment_Settings_PrivateKeyPastedManually %></span>
                            <br/>
                            <a href="javascript:void(0);" onclick="SW.AgentManagement.AgentsGrid.ResetPrivateKey();">
                                <%= Resources.AgentManagementWebContent.Deployment_Settings_UsePrivateKeyInFile %>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr class="sw-AgentManagement-PrimaryCustomCredential sw-AgentManagement-PrivateKeyItem">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.Deployment_Settings_PrivateKeyPassword %>:</td>
                    <td>
                        <input type="password" id="CredentialPrivateKeyPassword" value="" />
                    </td>
                </tr>

                <tr class="sw-AgentManagement-SudoSelection">
                    <td colspan="2">
                        <label>
                            <input type="checkbox" id="IncludeSudoCredentials" value="1" />
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_IncludeSudo %>
                        </label>
                        <img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" id="sudoHelpIcon" />
                        <div id="sudoHelpIconTooltipContent" class="hidden">
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_DoINeedToProvideSudoCredentials %><br/><br/>
                            <%= Resources.AgentManagementWebContent.Deployment_Settings_SudoCredentialsHelpfulText %><br/><br/>
                            <a href="<%= SudoCredentialsHelpUrl %>" target="_blank" rel="noopener noreferrer"><%= Resources.AgentManagementWebContent.Deployment_Settings_WhatCredentialsDoINeedForLinux %></a>
                        </div>
                    </td>
                </tr>
                <!-- SUDO -->
                <tr class="sw-AgentManagement-SudoSection">
                    <td colspan="2">
                        <%= Resources.AgentManagementWebContent.Deployment_Settings_SudoCredentialsCaption %>
                    </td>
                </tr>

                <tr class="sw-AgentManagement-SudoSection">
                    <td class="firstColumn"><%= ChooseCredentialText %></td>
                    <td>
                        <select id="AdditionalCredentialNamesCombo">
                            <option value="test">test</option>
                        </select>
                    </td>
                </tr>

                <tr class="sw-AgentManagement-SudoSection sw-AgentManagement-AdditionalCustomCredential">
                    <td class="firstColumn"><%= Resources.AgentManagementWebContent.CredentialName %>:</td>
                    <td>
                        <input type="text" id="AdditionalCredentialName" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-SudoSection sw-AgentManagement-AdditionalCustomCredential">
                    <td class="firstColumn"><%= UserNameText %></td>
                    <td>
                        <input type="text" id="AdditionalCredentialUsername" value="" /></td>
                </tr>
                <tr class="sw-AgentManagement-SudoSection sw-AgentManagement-AdditionalCustomCredential">
                    <td class="firstColumn"><%= PasswordText %></td>
                    <td>
                        <input type="password" id="AdditionalCredentialPassword" value="" /></td>
                </tr>
            </table>
        </div>
</div>


<asp:HiddenField runat="server" ID="selectedAgents"/>

<script type="text/javascript">
    // <![CDATA[
    SW.AgentManagement.AgentsGrid.SetEditGrid(<%= this.IsEditGrid.ToString().ToLower() %>);
    SW.AgentManagement.AgentsGrid.SetSelectedAgentsFieldClientID('<%= this.SelectedAgentsFieldClientID %>');
    SW.AgentManagement.AgentsGrid.SetPageSize(<%= this.PageSize %>);
    SW.AgentManagement.AgentsGrid.SetUserName("<%= HttpUtility.JavaScriptStringEncode(Context.User.Identity.Name.Trim())%>");
    SW.AgentManagement.AgentsGrid.SetExcludedAgents("<%=this.ExcludedAgentsString%>");
    SW.AgentManagement.AgentsGrid.SetUnsupportedAgents("<%=this.UnsupportedAgentsString%>");
    SW.AgentManagement.AgentsGrid.SetReturnUrl("<%= this.ReturnUrl %>");
    SW.AgentManagement.AgentsGrid.SetReportUrl("<%= this.ReportUrl %>");
    SW.AgentManagement.AgentsGrid.SetHowLongIsAgentDisplayedAsNewInHours(<%= this.HowLongIsAgentDisplayedAsNew %>);
    SW.AgentManagement.AgentsGrid.SetFailedPluginStatuses(<%= this.FailedPluginStatusesJSArray %>);
    SW.AgentManagement.AgentsGrid.SetCredentialsList(<%= CredentialsList %>);
    SW.AgentManagement.AgentsGrid.SetSshPrivateKeyCredentialsList(<%= SshPrivateKeyCredentialsList %>);
    SW.AgentManagement.AgentsGrid.SetIsOrcFeatureEnabled(<%= this.IsOrcFeatureEnabled.ToString().ToLower() %>);

    $(document).ready(function () {
        var toolTip = new Ext.ToolTip({
            target: 'sudoHelpIcon',
            html: $('#sudoHelpIconTooltipContent').html(),
            anchor: 'left',
            cls: 'sw-AgentManagement-Tooltip',
            dismissDelay: 0,
            showDelay: 0,
            autoHide: false
        });

        toolTip.on('show', function () {

            var timeout;

            toolTip.getEl().on('mouseout', function () {
                timeout = window.setTimeout(function () {
                    toolTip.hide();
                }, 500);
            });

            toolTip.getEl().on('mouseover', function () {
                window.clearTimeout(timeout);
            });

            Ext.get('sudoHelpIcon').on('mouseover', function () {
                window.clearTimeout(timeout);
            });

            Ext.get('sudoHelpIcon').on('mouseout', function () {
                timeout = window.setTimeout(function () {
                    toolTip.hide();
                }, 500);
            });

        });
    });

        // ]]>
</script>

<!-- This is used to grab the results from a failed Web Service call -->
<div id="test" class="x-hidden"></div>

<!-- div for delete agent dialog -->
<div id="AgentManagement_DeleteAndUninstallDialog" class="AgentManagement_DeleteAndUninstallDialogBody" style="display:none">
    <div class="AgentManagement_WarningIcon"></div>
    <div class="AgentManagement_Text AgentManagement_OneSelectWithNodes" style="display: none">
        <p>
            <%: Resources.AgentManagementWebContent.DeleteAgentDialog_OneAgentWithNodeMsg1 %>
            <a href="#" class="AgentManagement_EditNodeButton AgentManagement_ArrowLink"><%: Resources.AgentManagementWebContent.DeleteAgentDialog_EditNodePropertiesLinkText %></a>
        </p>
        
        <table class="AgentManagement_Table">
            <tbody>
                <tr>
                    <td><%: Resources.AgentManagementWebContent.DeleteAgentDialog_NodeLabel %>:</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        
        <p><%: Resources.AgentManagementWebContent.DeleteAgentDialog_AllAgentSWWillBeUninstalledText %></p>
        <p><a href="#" class="AgentManagement_InstalledPluginsButton AgentManagement_ArrowLink"><%= WhatPluginAreCurrentlyInstalledText %></a></p>
        
        <div class="AgentManagement_DialogOfflineAgentsWarning sw-suggestion" style="display: none">
            <span class="AgentManagement_DialogOfflineAgentsWarningIcon"></span>
            <span></span>
        </div>

        <p>
           <%: Resources.AgentManagementWebContent.DeleteAgentDialog_AreYouSureAgentWithNode %>
        </p>
    </div>
    
    <div class="AgentManagement_Text AgentManagement_OneSelectWithoutNodes" style="display: none">      
        <p><%: Resources.AgentManagementWebContent.DeleteAgentDialog_AllAgentSWWillBeUninstalledText %></p>
        <p><a href="#" class="AgentManagement_InstalledPluginsButton AgentManagement_ArrowLink"><%= WhatPluginAreCurrentlyInstalledText %></a></p>
        
        <div class="AgentManagement_DialogOfflineAgentsWarning sw-suggestion" style="display: none">
            <span class="AgentManagement_DialogOfflineAgentsWarningIcon"></span>
            <span></span>
        </div> 

        <p>
           <%: Resources.AgentManagementWebContent.DeleteAgentDialog_AreYouSureAgentWithoutNode %>
        </p>
    </div>    

    <div class="AgentManagement_Text AgentManagement_MultiSelectWithoutNodes" style="display:none">
        <p>
            <%: Resources.AgentManagementWebContent.DeleteAgentDialog_MultiWithoutNodesMsg1 %>
        </p>    
        
        <div class="AgentManagement_DialogOfflineAgentsWarning sw-suggestion" style="display: none">
            <span class="AgentManagement_DialogOfflineAgentsWarningIcon"></span>
            <span></span>
        </div>

        <p>
           <%= AreYouSureTextMultiSelectWithoutNodesText %>
        </p>
    </div> 

    <div class="AgentManagement_Text AgentManagement_MultiSelectWithNodes" style="display:none">
        <p>
            <%: Resources.AgentManagementWebContent.DeleteAgentDialog_MultiWithNodesMsg1 %>
        </p>
        <p>
            <a href="#" class="AgentManagement_EditNodeButton AgentManagement_ArrowLink"><%: Resources.AgentManagementWebContent.DeleteAgentDialog_EditNodePropertiesLinkText %></a>
        </p>  
        
        <div class="AgentManagement_DialogOfflineAgentsWarning sw-suggestion" style="display: none">
            <span class="AgentManagement_DialogOfflineAgentsWarningIcon"></span>
            <span></span>
        </div>              

        <p>
           <%: Resources.AgentManagementWebContent.DeleteAgentDialog_AreYouSureMultiNodeWithNodes %>
        </p>
    </div>
</div>

<!-- div for installed agent plug-ins dialog -->
<div id="AgentManagement_InstalledAgentPluginsDialog" class="AgentManagement_InstalledAgentPluginsDialogBody" data-help-url="<%: InstalledAgentModulesHelpUrl %>" style="display:none">
    <div class="AgentManagement_Statuses">
        <table>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="AgentManagement_Plugins">
        <p><%: Resources.AgentManagementWebContent.InstalledAgentPluginsDialog_TableTitle %></p>
        <div class="AgentManagement_PluginsTable">
            <table>
                <thead><tr>
                    <th><%: Resources.AgentManagementWebContent.InstalledAgentPluginsDialog_TableColPluginName %></th>
                    <th><%: Resources.AgentManagementWebContent.InstalledAgentPluginsDialog_TableColPluginStatus %></th>
                    <th><%: Resources.AgentManagementWebContent.InstalledAgentPluginsDialog_TableColPluginVersion %></th>
                </tr></thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="AgentManagement_Footer">
        <a href="<%= ReportUrl %>">» <%: Resources.AgentManagementWebContent.InstalledAgentPluginsDialog_ViewInstalledPluginReport %></a>
        <button id="AgentManagement_InstalledAgentPluginsDialogCloseButton"><%: Resources.AgentManagementWebContent.AgentsGrid_CloseButtonText %></button>
    </div>
</div>

<!-- div for promotion agent prompt dialog -->
<div id="AgentManagement_PromoteAgentDialog" class="AgentManagement_PromoteAgentDialogBody" style="display:none">
    <div class="AgentManagement_WarningIcon"></div>
    <div class="AgentManagement_Text AgentManagement_SelectWithNodes" style="display: none">
        <p>
            <%: Resources.AgentManagementWebContent.PromoteAgentDialog_Message %>            
        </p>                
        <table class="AgentManagement_Table">
            <tbody>
                <tr>
                    <td><%: Resources.AgentManagementWebContent.DeleteAgentDialog_NodeLabel %>:</td>
                    <td></td>
                </tr>
            </tbody>
        </table>                                       
        <div class="AgentManagement_DialogOfflineAgentsWarning sw-suggestion" style="display: none">
            <span class="AgentManagement_DialogOfflineAgentsWarningIcon"></span>
            <span></span>
        </div>
        <p>
           <%: Resources.AgentManagementWebContent.PromoteAgentDialog_AreYouSureQuestion %>
        </p>
    </div>
    <div class="AgentManagement_Text AgentManagement_SelectWithoutNodes" style="display: none">      
        <p><%: Resources.AgentManagementWebContent.PromoteAgentDialog_Message %></p>
        
        <div class="AgentManagement_DialogOfflineAgentsWarning sw-suggestion" style="display: none">
            <span class="AgentManagement_DialogOfflineAgentsWarningIcon"></span>
            <span></span>
        </div> 
        <p>
            <%: Resources.AgentManagementWebContent.PromoteAgentDialog_AreYouSureQuestion %>
        </p>
    </div>         
</div>
