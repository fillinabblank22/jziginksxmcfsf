using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using SolarWinds.AgentManagement.Common;
using SolarWinds.AgentManagement.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using System.Web.Script.Serialization;
using SolarWinds.AgentManagement.Contract.Models;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web;

public partial class Orion_AgentManagement_Controls_AgentsGrid : System.Web.UI.UserControl
{
    private static readonly Log _log = new Log();
    protected readonly string NameLabel = Resources.AgentManagementWebContent.AgentsGrid_NameLabel;
    protected readonly string IPLabel = Resources.AgentManagementWebContent.AgentsGrid_IPLabel;
    protected readonly string AdvancedLabel = Resources.AgentManagementWebContent.AgentsGrid_AdvancedLabel;
    protected readonly string PortLabel = Resources.AgentManagementWebContent.AgentsGrid_PortLabel;
    protected readonly string PasswordLabel = Resources.AgentManagementWebContent.AgentsGrid_PasswordLabel;
    protected readonly string WhatIsThisLabel = Resources.AgentManagementWebContent.AgentsGrid_WhatIsThisLabel;
    protected readonly string WhatIsProxyLabel = Resources.AgentManagementWebContent.AgentsGrid_WhatIsProxyLabel;
    protected readonly string TroubleshootingLabel = Resources.AgentManagementWebContent.AgentsGrid_TroubleshootingLabel;
    protected readonly string LoadingText = Resources.AgentManagementWebContent.AgentsGrid_LoadingText;
    protected readonly string LogLevelLabel = Resources.AgentManagementWebContent.AgentsGrid_LogLevelLabel;
    protected readonly string DiagnosticsLabel = Resources.AgentManagementWebContent.AgentsGrid_DiagnosticsLabel;
    protected readonly string GenerateNewDiagnosticsLabel = Resources.AgentManagementWebContent.AgentsGrid_GenerateNewDiagnosticsLabel;
    protected readonly string LocationNotRespondingError = Resources.AgentManagementWebContent.AgentsGrid_LocationNotRespondingError;
    protected readonly string AssignToPollerLabel = Resources.AgentManagementWebContent.AgentsGrid_AssignToPollerLabel;
    protected readonly string AgentNotFoundError = Resources.AgentManagementWebContent.AgentsGrid_AgentNotFoundError;
    protected readonly string LearnMoreLabel = Resources.AgentManagementWebContent.AgentsGrid_LearnMoreLabel;
    protected readonly string AgentGuidLabel = Resources.AgentManagementWebContent.AgentsGrid_AgentGuidLabel;
    protected readonly string UseProxyLabel = Resources.AgentManagementWebContent.AgentsGrid_UseProxyLabel;
    protected readonly string UpdateRemoteSettingsLabel = Resources.AgentManagementWebContent.AgentsGrid_UpdateRemoteSettingsLabel;

    protected readonly string NewCredentialText = Resources.AgentManagementWebContent.Deployment_Settings_NewCredentialText;
    protected readonly string ChooseExistingOrCreateNewOneText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_ChooseExistingOrCreateNewOneText);
    protected readonly string ChooseCredentialText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_ChooseCredentialText);
    protected readonly string ChooseInstallPackageText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_ChooseInstallPackageText);
    protected readonly string UserNameText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_UserNameText);
    protected readonly string PasswordText = String.Format("{0}:", Resources.AgentManagementWebContent.Deployment_Settings_PasswordText);

    private readonly CredentialManager _credentialManager = new CredentialManager();
    private readonly JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();

    protected string AreYouSureTextMultiSelectWithoutNodesText
    {
        get
        {
            var htmlEncodedFormat = HttpUtility.HtmlEncode(Resources.AgentManagementWebContent.DeleteAgentDialog_AreYouSureTextMultiSelectWithoutNodesFormat);
            var text = htmlEncodedFormat
                .Replace("*##", "<span>")
                .Replace("##*", "</span>")
                .Replace("{0}", "<span class='numberOfAgents'></span>");

            return text;
        }
    }

    protected string WhatPluginAreCurrentlyInstalledText
    {
        get
        {
            var htmlEncodedFormat =
                HttpUtility.HtmlEncode(
                    Resources.AgentManagementWebContent.DeleteAgentDialog_WhatPluginAreCurrentlyFormat);
            var text = htmlEncodedFormat
                .Replace("*##", "<span>")
                .Replace("##*", "</span>");

            return text;
        }
    }

    protected string CredentialsList
    {
        get
        {
            return
                _jsSerializer.Serialize(
                    (new[] { new KeyValuePair<int, string>(-1, NewCredentialText) }).Concat(
                        _credentialManager.GetCredentialNames<UsernamePasswordCredential>(
                            CoreConstants.CoreCredentialOwner)));
        }
    }

    protected string SshPrivateKeyCredentialsList
    {
        get
        {
            return
                _jsSerializer.Serialize(
                    (new[] { new KeyValuePair<int, string>(-1, NewCredentialText) }).Concat(
                        _credentialManager.GetCredentialNames<SshPrivateKeyCredential>(
                            CoreConstants.CoreCredentialOwner)));
        }
    }

    private bool isEditGrid = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        ProcessManageAsNodeClick();
        PollingEngines = new AgentsDAL().GetAvailablePollingEngines();
        IsOrcFeatureEnabled = new FeatureManager().IsFeatureEnabled(FeatureConstants.RemoteCollectorFeature);
    }

    protected bool IsOrcFeatureEnabled { get; set; }

    public int PageSize
    {
        // TODO
        get { return 20; }
    }

    public bool IsEditGrid
    {
        get { return isEditGrid; }
        set { isEditGrid = value; }
    }

    protected String SelectedAgentsFieldClientID
    {
        get { return selectedAgents.ClientID; }
    }

    public IEnumerable<Int32> SelectedAgents
    {
        get
        {
            string[] ids = selectedAgents.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string idStr in ids)
            {
                int id = 0;
                if (Int32.TryParse(idStr, out id))
                    yield return id;
            }

            yield break;
        }
        set
        {
            selectedAgents.Value = string.Join(",", value.Select(id => id.ToString()).ToArray());
        }
    }

    protected string ExcludedAgentsString
    {
        get
        {
            // -1 is for "waiting agents" and it should always be disabled
            if (ExcludedAgentsIds == null)
                return "-1";

            return string.Join(",", ExcludedAgentsIds.Concat(new[] { -1 }).Select(id => id.ToString()).ToArray());
        }
    }

    public IEnumerable<Int32> ExcludedAgentsIds
    {
        get; set;
    }

    protected string UnsupportedAgentsString
    {
        get
        {
            if (UnsupportedAgentsIds == null)
                return string.Empty;

            return string.Join(",", UnsupportedAgentsIds.Select(id => id.ToString()).ToArray());
        }
    }

    public IEnumerable<Int32> UnsupportedAgentsIds
    {
        get;
        set;
    }

    public string ReturnUrl
    {
        get
        {
            var currentUrl = Request.Url.AbsoluteUri;
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(currentUrl);
            return Convert.ToBase64String(plainTextBytes); 
        }
    }

    public IEnumerable<KeyValuePair<int, string>> PollingEngines { get; private set; }

    protected string InstallingAgentHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionAgentManagementAGInstallingAgent");
        }
    }

    protected string InstalledAgentModulesHelpUrl
    {
        get
        {
            // (FB358957)
            return HelpHelper.GetHelpUrl("OrionAgent-AgentPluginStatus");
        }
    }

    protected string SudoCredentialsHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_LinuxCredentials"); }
    }

    private int? howLongIsAgentDisplayedAsNew;
    protected int HowLongIsAgentDisplayedAsNew
    {
        get
        {
            if (howLongIsAgentDisplayedAsNew == null)
            {
                try
                {
                    using (var proxy = ControlServiceFactory.Create())
                    {
                        var settings = proxy.GetAgentManagementSettings();
                        howLongIsAgentDisplayedAsNew = settings.HowLongIsAgentDisplayedAsNew;
                    }
                }
                catch (Exception ex)
                {
                    _log.Error("Error on getting value HowLongIsAgentDisplayedAsNew from Settings", ex);
                }
            }

            return howLongIsAgentDisplayedAsNew != null ? (int) howLongIsAgentDisplayedAsNew : default(int);
        }
    }

    private string _reportUrl;
    public string ReportUrl
    {
        get
        {
            if (_reportUrl == null)
            {
                try
                {
                    var dal = new ReportDAL();
					var reportId = dal.GetRepordId(SolarWinds.AgentManagement.Common.Constants.ReportNameForAgents);
                    _reportUrl = String.Format("/Orion/Report.aspx?ReportID={0}", reportId);
                }
                catch (Exception ex)
                {
                    _log.Error("Error on get ReportUrl", ex);
                }

            }

            return _reportUrl;
        }
    }

    protected string FailedPluginStatusesJSArray
    {
        get { return "[" + string.Join(",", StatusHelper.FailedPluginStatuses.Cast<int>()) + "]"; }
    }

    private void PrefillFormForManageAsNode(AgentsDAL agentsDAL, int agentId, AgentRecord agentRecord, Node node)
    {
        // if agent already has node assigned verify that such node really exists
        // as NodesData table primary key increments always there should be no mismatch between two nodes having same NodeID by time and so this is gonna be safe
        if (agentRecord.NodeId.HasValue)
        {
            Node tempNode = NodeDAL.GetNode(agentRecord.NodeId.GetValueOrDefault());
            // if NodeID is assigned but node does not exist let's purge invalid reference to node
            if (tempNode == null)
            {
                // IF this fails THEN just log into OrionWeb.log, Error page is already shown to user
                if (!agentsDAL.UpdateAgent(agentRecord, false))
                {
                    _log.WarnFormat("Removing of Agent to Node reference failed. AgentId={0}; NodeId={1}", agentId, node.ID);
                }
            }
        }
        // prefill form for Add Node page
        node.DNS = agentRecord.DNSName;
        node.Name = !string.IsNullOrEmpty(agentRecord.Hostname) ? agentRecord.Hostname : agentRecord.IP;
        node.Caption = node.Name;
        node.NodeSubType = NodeSubType.Agent;
        node.IpAddress = agentRecord.IP;
        node.EngineID = agentRecord.PollingEngineId;

        // prefill AgentCredential to indicate agent already exists
        NodeWorkflowHelper.AgentCredential = new SolarWinds.Orion.Discovery.Contract.Models.AgentManagementCredential()
        {
            AgentId = agentRecord.AgentId
        };
    }
    private void ProcessManageAsNodeClick()
    {
        if (Request.QueryString["manageAsNodeClick"] == null || Request.QueryString["agentId"] == null)
        {
            return;
        }

        try
        {
            int agentId;
            if (!int.TryParse(Request.QueryString["agentId"], out agentId))
            {
                _log.WarnFormat("agentId:{0} is missing, doing nothing", agentId);
                return;
            }
            var agentsDAL = new AgentsDAL();
            AgentRecord agentRecord = agentsDAL.GetAgent(agentId);
            if (agentRecord == null)
            {
                _log.Warn("agent record is missing, doing nothing");
                return;
            }
            var node = NodeWorkflowHelper.CreateNode();
            PrefillFormForManageAsNode(agentsDAL, agentId, agentRecord, node);
            NodeWorkflowHelper.Node = node;
            Response.Redirect("/Orion/Nodes/Add/Default.aspx?restart=false&targetDisabled=true");
        }
        catch (ThreadAbortException)
        {
            // This exception is expected in call to Response.Redirect 
        }
        catch (Exception ex)
        {
            _log.Error("manageAsNodeClick()", ex);
        }
    }
}
