﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminTabs.ascx.cs" Inherits="Orion_AgentManagement_Controls_AdminTabs" %>
<%
    this.AddTab(Resources.AgentManagementWebContent.ManageAgents_TabLabel, VirtualPathUtility.ToAbsolute("~/Orion/AgentManagement/Admin/ManageAgents.aspx"), IsPageRegexMatch("(^|/)ManageAgents.aspx$"));
    if (IsOrcFeatureEnabled)
    {
        this.AddTab(Resources.AgentManagementWebContent.ManageRemoteCollectors_TabLabel, VirtualPathUtility.ToAbsolute("~/Orion/AgentManagement/Admin/ManageRemoteCollectors.aspx"), IsPageRegexMatch("(^|/)ManageRemoteCollectors.aspx$"));
    }
%>

<script type="text/javascript">
    $(document).ready(function () {
        $(".AgentManagement_AdminTabs li.sw-tab").bind('mouseover', function () { $(this).addClass('x-tab-strip-over'); });
        $(".AgentManagement_AdminTabs li.sw-tab").bind('mouseout', function () { $(this).removeClass('x-tab-strip-over'); });
    });
</script>

<div class="AgentManagement_AdminTabs">
    <div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">
        <div class="x-tab-strip-wrap">
            <ul class="x-tab-strip x-tab-strip-top">
                <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />
                <li class="x-tab-edge"></li>
                <div class="x-clear">
                    <!-- -->
                </div>
            </ul>
            <%--<div class="x-tab-strip-spacer" style="border-bottom: none;">
                <!-- -->
            </div>--%>
        </div>
    </div>
</div>
