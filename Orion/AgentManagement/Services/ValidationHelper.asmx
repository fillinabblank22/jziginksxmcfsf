﻿<%@ WebService Language="C#" Class="Helper" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.AgentManagement.Contract.Models;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.AgentManagement.Common;
using SolarWinds.AgentManagement.Common.Models;
using SolarWinds.AgentManagement.Web;
using SolarWinds.AgentManagement.Web.DAL;
using EnginesDAL = SolarWinds.AgentManagement.Web.DAL.EnginesDAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Helper : WebService
{
    private static readonly Log _log = new Log();
    
    [WebMethod(EnableSession = true)]
    public Boolean ValidateIPAddress(string ipaddr)
    {
        try
        {
            System.Net.IPAddress ip;
            return System.Net.IPAddress.TryParse(ipaddr, out ip);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Error when validating ip {0} : {1}", ipaddr , ex);
            return false;
        }
    }

}

