using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class ViewGroupSubNav : BaseViewGroupControl
{

    protected override void RebuildUI()
    {
        ViewGroupSubNavHolder.Visible = true;

        if (!OrionMinReqsMaster.IsApolloEnabled)
        {
            BindSubNavExpandedState();
        }

        rptTabs.DataSource = this.RelatedViews;
        rptTabs.DataBind();

        pnlScript.Visible = this.IsActive;

        // shift the left-nav tabs to the top if there are no customization menu items
        if (!Profile.AllowCustomize && !subNavContainer.Attributes["class"].Contains("menuItemsHidden"))
        {
            subNavContainer.Attributes["class"] += " menuItemsHidden";
        }
    }

    private void BindSubNavExpandedState()
    {
        bool isSubNavExpanded = true;
        if (Page.Request.Cookies["SWSettings"] != null && this.IsActive == true)
        {
            string cookieValue = System.Web.HttpUtility.UrlDecode(Page.Request.Cookies["SWSettings"].Value);

            //Ideally we would parse out the json and only look at our setting if we want to store more settings in this cookie. but for now this should be fine.
            //If I was to implement I would add the following code to our JsonObjectConverter http://stackoverflow.com/questions/3142495/deserialize-json-into-c-sharp-dynamic-object
            if (cookieValue == "{subNavExpanded:false}")
                isSubNavExpanded = false;
        }


        if (!isSubNavExpanded)
            subNavContainer.Attributes["class"] += " collapsed";
    }

    public string GetTabRowStyleClass(ViewInfo view)
    {
        if (view.ViewID == 0)
        {
            return "sw-dashboard__add-tab";
        }

        return "";
    }

}
