﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WindowsLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected static Regex proxyFinder = new Regex(@"/Orion/Proxy\.aspx\?(PageTitle=[^&]*&)?Path=(?<path>.*)",
    RegexOptions.IgnoreCase | RegexOptions.Compiled);

    protected static Regex credentialsRemover = new Regex(@"[&?]AccountID=[^&]*&Password=[^&]*",
        RegexOptions.IgnoreCase | RegexOptions.Compiled);

    protected static Regex directLinkUrlValidator = new Regex(@"^.*/((Default|Login)\.aspx?)?(\?.*)?$",
        RegexOptions.IgnoreCase | RegexOptions.Compiled);

    protected string ReturnUrl
    {
        get
        {
			string url = string.Empty;
			string[] urls = this.Request.QueryString.GetValues("ReturnUrl");
			if (urls != null && urls.Length > 0)
			{
				url = urls[0];
			}
			
            url = Server.UrlDecode(url);

            Match proxyMatch = proxyFinder.Match(url);
            if (proxyMatch.Success)
                url = proxyMatch.Groups["path"].Value;

            url = credentialsRemover.Replace(url, string.Empty);

            if (string.IsNullOrEmpty(url))
                url = "/Orion/Default.aspx";

            return url;
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
       
        base.OnPreInit(e);



        if( Request.LogonUserIdentity != null &&
                !String.IsNullOrEmpty(Request.LogonUserIdentity.AuthenticationType ) &&
                Request.LogonUserIdentity.IsAuthenticated )
        {

            //We have a valid WindowsIdentity

            //Remember current WindowsIdentity for the rest of the session
            HttpContext.Current.Session["CurrentWindowsIdentity"] = Request.LogonUserIdentity;

            //Set flag saying we've got the identity
            HttpContext.Current.Session["WindowsAuthBounce"] = true;

            //Return to Login (preserve ReturnUrl)
            //Server.Transfer to avoid additional requests from client
            string retUrl = "?returnUrl=" + ReturnUrl;
            Server.Transfer("/Orion/Login.aspx" + retUrl);

        } else {

            // triggers an access denied in the correct way to bypass forms authentication
            Context.Items["Send401"] = true;

            //Set flag saying 401 has been sent, identity should be availble from this point on.
            HttpContext.Current.Session["WindowsAuthBounce"] = true;

        }

    }

   

}
