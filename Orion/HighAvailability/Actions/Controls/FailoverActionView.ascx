<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FailoverActionView.ascx.cs" Inherits="Orion_HighAvailability_Actions_Controls_FailoverActionView" %>

<style type="text/css">
    .failover-action-container label {
        font-weight: bold;
    }

</style>

<div id="container" runat="server" automation="FailoverAction_Container" class="failover-action-container">
    <div ID="ConfigurePoolSection" runat="server" automation="FailoverAction_ConfigurePoolSection" >
            <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.SelectHighAvailabilityPoolSectionTitle) %></h3>
            <div class="item-box">
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.FailoverActionSelectPoolLabel) %></label>
            <asp:DropDownList runat="server" ID="SelectPools" /> </div>

            


    </div>

    <div ID="HAIsNotConfigured" runat="server" class="section" automation="FailoverAction_HAIsNotConfiguredSection">
        <h1 automation="HAIsNotConfiguredSectionTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.HighAvailabilityPoolIsNotConfigured) %></h1>
        <a target="_blank" automation="HAIsNotConfiguredSectionTitle_Link" class="link" href="/ui/ha/summary"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ConfigureHighAvailabilityLinkLabel) %></a>

    </div>

    <div ID="HAIsNotInstalled" runat="server" class="section" automation="FailoverAction_HAIsNotInstalled">
        <h1 automation="HAIsNotInstalledTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.HighAvailabilityIsNotInstalled) %></h1>
        <p automation="HAIsNotInstalledText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.HighAvailabilityIsNotInstalledText) %></p>
    </div>


    <div ID="resetTabWarning" class="section" automation="FailoverAction_ResetTabWarningSection">
        <h1 automation="ResetTabWarningSectionTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.FailoverActionOnResetNotSupportedTitle) %></h1>
        <p automation="ResetTabWarningSectionText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.FailoverActionOnResetNotSupportedText) %></p>
    </div>

</div>

<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {

        var viewContext = <%= ToJson(ViewContext) %>;
        var actionContext =  <%= ToJson(ActionDefinition) %>;
        if (!actionContext) {
            actionContext = <%= CreateActionDefinition() %>;
        } else {
        
            var property = _.find( actionContext.ActionProperties, function(item) { return item.PropertyName == "PoolID"; });
            $('#<%= SelectPools.ClientID %>').val(property.PropertyValue);
        };

        
        var config = {
            actionDefinition : actionContext,
            viewContext :  viewContext ,
            environmentType : <%= ToJson(EnviromentType) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            isActionSupported: <%= IsActionSupported.ToString().ToLower() %>,
        };

        config.onReadyCallback = function(plugin) {
            <%= OnReadyJsCallback %>(plugin);
        }; 

        var plugin = {
            validateSectionAsync: function (section, callback) {
                
                if (!config.isActionSupported) {

                    $("#createActionDefinitionBtn").hide();
                }
                callback(true);
            },
            getActionDefinitionAsync: function (callback) {
                
                if (!config.isActionSupported) {

                    $("#createActionDefinitionBtn").hide();
                }
                
                var props = [];

                props.push({PropertyName: "PoolID", PropertyValue: $('#<%= SelectPools.ClientID %>').val() });
                config.actionDefinition.ActionProperties = props;

                callback(config);

            },
            getUpdatedActionProperties: function (callback) {
                
                if (!config.isActionSupported) {

                    $("#createActionDefinitionBtn").hide();
                }
                callback(config);
            }
        };

        config.onReadyCallback(plugin);

        if (!config.isActionSupported) {

            $("#createActionDefinitionBtn").hide();
            $("#nextSectionInActionDefinitionBtn").hide();
            $("#addActionDefinitionBtn").hide();
            $('#_basicActionPlugins_').remove();
            $('#_topActionPlugins_').remove();
            $('#_timeOfDayActionPlugins_').remove();
        }

        if (viewContext.ExecutionMode === 1)
        {
            $('#<%=HAIsNotConfigured.ClientID %>').hide();
            $('#<%=ConfigurePoolSection.ClientID %>').hide();

            $('#resetTabWarning').show();
            $("#addActionDefinitionBtn").hide();
            $("#nextSectionInActionDefinitionBtn").hide();
            $("#createActionDefinitionBtn").hide();
            $('#_basicActionPlugins_').remove();
            $('#_topActionPlugins_').remove();
            $('#_timeOfDayActionPlugins_').remove();


        } else {
            $('#resetTabWarning').hide();

        }



    })();

</script>