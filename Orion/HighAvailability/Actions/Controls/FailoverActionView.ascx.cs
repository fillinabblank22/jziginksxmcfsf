﻿using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Core.Actions.Impl.Failover;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Newtonsoft.Json;

public partial class Orion_HighAvailability_Actions_Controls_FailoverActionView : ActionPluginBaseView
{
    private static SolarWinds.Logging.Log logger = new SolarWinds.Logging.Log();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(ActionDefinitionJsonString))
            {
                ActionDefinition = JsonConvert.DeserializeObject<ActionDefinition>(ActionDefinitionJsonString);

            }

            var haDeployment = QueryHADeployment();

            ConfigurePoolSection.Visible = haDeployment == HADeployment.Configured;

            if (ConfigurePoolSection.Visible)
            {
                SelectPools.DataSource = LoadPools();
                SelectPools.DataTextField = "name";
                SelectPools.DataValueField = "value";
                SelectPools.DataBind();
            }

            HAIsNotConfigured.Visible = haDeployment == HADeployment.NotConfigured;
            HAIsNotInstalled.Visible = haDeployment == HADeployment.NotInstalled;

            IsActionSupported = ConfigurePoolSection.Visible;


        }
    }


    public string CreateActionDefinition()
    {
        var action = new FailoverPlugin();
        var actionDef = new ActionDefinition
        {
            ActionTypeID = action.ActionTypeID,
            Description = action.Description,
            IconPath = action.IconPath,
            Title = action.Title,
            Enabled = true
        };

        actionDef.Properties["PoolID"] = new ActionProperty("PoolID", "0");

        return ToJson(actionDef);
    }

    HADeployment QueryHADeployment()
    {

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var result = false;
            try
            {
                result = Convert.ToBoolean(swis.Query("SELECT TOP 1 COUNT(FullName) AS IsHAInstalled FROM Metadata.Entity WHERE FullName = 'Orion.HA.Pools'").Rows[0][0]);
                if (!result)
                {
                    return HADeployment.NotInstalled;
                }

                result = Convert.ToBoolean(swis.Query("SELECT TOP 1 COUNT(PoolId) AS HasPool FROM Orion.HA.Pools").Rows[0][0]);
                if (!result)
                {
                    return HADeployment.NotConfigured;
                }

                return HADeployment.Configured;
            }
            catch (Exception e)
            {
                logger.WarnFormat("Unable to determine if High Availablity is properly configured: {0}", e);
            }

            return HADeployment.NotInstalled;
        }
    }

    IEnumerable<object> LoadPools()
    {
        if (String.Equals(AlertingViewContext.EntityType, "Orion.OrionServers", StringComparison.InvariantCulture))
        {
            yield return new { value = 0, name = Resources.CoreWebContent.FailoverAffectedPoolLabel };
        }


        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable result = null;

            try
            {
                result =  swis.Query("SELECT PoolId, DisplayName FROM Orion.HA.Pools");

            }
            catch (Exception e)
            {
                logger.ErrorFormat("While retrieving High Availability Pools: {0}", e);
                yield break;
            }

            foreach (DataRow row in result.Rows)
            {
                yield return new { value = row[0], name = row[1] };
            }
        }
    }

    public override string ActionTypeID
    {
        get { return FailoverConstants.ActionTypeID; }
    }

    enum HADeployment
    {
        NotInstalled,
        NotConfigured,
        Configured
    }
}