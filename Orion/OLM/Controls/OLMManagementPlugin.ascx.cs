using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;
using Node = SolarWinds.Orion.NPM.Web.Node;
using SolarWinds.Orion.LogMgmt.BusinessLayerProxy.Licensing;
using LicenseInfo = SolarWinds.Orion.LogMgmt.BusinessLayerProxy.Licensing.LicenseInfo;

public partial class Orion_OLM_Controls_OLMManagementPlugin : UserControl, IManagementTasksPlugin
{
    private static readonly Log log = new Log();
    private readonly ILicenseInfo licenseInfo;

    public Orion_OLM_Controls_OLMManagementPlugin()
    {
        licenseInfo = LicenseInfo.CreateInstance();
    }

    public IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        try
        {
            var task = GetOLMManagementTask(netObject, returnUrl);

            if (task != null)
            {
                return new[] { task };
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting management task for OLM module.", ex);
        }

        return Enumerable.Empty<ManagementTaskItem>();
    }


    private ManagementTaskItem GetOLMManagementTask(NetObject netObject, string returnUrl)
    {
        var node = netObject as Node;
        if (node == null)
        {
            var netobject = netObject.NetObjectID;
            if (NetObjectFactory.ConvertNetObject(ref netobject, new[] { "N" }))
            {
                if (!string.IsNullOrEmpty(netobject))
                {
                    node = NetObjectFactory.Create(netobject) as Node;
                }
            }
        }

        //check if it is a valid OLM Node
        if (node == null || !licenseInfo.IsFullLicensedNode(node.NodeID))
        {
            return null;
        }

        var taskSectionName = Resources.OLMWebContent.OLM_TaskSectionName;
        return new ManagementTaskItem
                     {
                         Section = taskSectionName,
                         ClientID = "olmNode",
                         LinkInnerHtml = string.Format(
                                                       "<img src='/Orion/OLM/images/icons/olm_launch.svg' height='16' width='16' alt='' />&nbsp;{0}", Resources.OLMWebContent.OLM_TaskName),
                         LinkUrl = string.Format("/ui/orionlog/logviewer/now/1hours/{0}",
                                                 node.NodeID)
                     };
    }
}