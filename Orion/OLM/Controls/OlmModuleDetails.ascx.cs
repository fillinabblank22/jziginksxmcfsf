using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.LogMgmt.BusinessLayerProxy;
using SolarWinds.Orion.LogMgmt.BusinessLayerProxy.Licensing;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_OLM_OlmModuleDetails : System.Web.UI.UserControl
{
    private static readonly Log log = new Log();

    private readonly ILicenseInfo licenseInfo;

    public Orion_OLM_OlmModuleDetails()
    {
        licenseInfo = LicenseInfo.CreateInstance();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                LoadModuleAndLicenseInfo("OLM");
            }
            catch (Exception ex)
            {
                log.Error("Error while displaying details for Log Analyzer module.", ex);
            }
        }
    }

    private void LoadModuleAndLicenseInfo(string moduleName)
    {
        this.Visible = false;

        var module = ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false)
        .FirstOrDefault(m => m.ProductTag.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase));

        if (module == null)
        {
            return;
        }

        var values = new Dictionary<string, string>();
        var primaryPollerEngineId = EngineDAL.GetPrimaryEngineId();
        using (var proxy = LogMgmtBusinessLayerProxyFactory.Instance.Create(primaryPollerEngineId))
        {
            bool isLicenseDetailVisible = licenseInfo.IsFullLicense() || proxy.Api.HasEvaluationExpired();
            if (!isLicenseDetailVisible)
            {
                return;
            }
        }

        values.Add(Resources.OLMWebContent.LicenseDetails_LicenseInfo, !string.IsNullOrEmpty(module.LicenseInfo) ? module.LicenseInfo : Resources.OLMWebContent.License_Expired);
        values.Add(Resources.OLMWebContent.LicenseDetails_ProductName, module.ProductName);
        values.Add(Resources.OLMWebContent.LicenseDetails_Version, module.Version);
        values.Add(Resources.OLMWebContent.LicenseDetails_ServicePack, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.OLMWebContent.LicenseDetails_None : module.HotfixVersion);
        AddOlmLicenseInfo(values);

        OlmDetails.Name = module.ProductDisplayName;
        OlmDetails.DataSource = values;
        this.Visible = true;
    }

    private void AddOlmLicenseInfo(Dictionary<string, string> values)
    {
        try
        {
            AddUsedElementsCount(values);
            AddElementsCount(values);
        }
        catch (Exception ex)
        {
            log.Warn("Cannot get Log Analyzer specific information about license", ex);
        }
    }

    private void AddElementsCount(Dictionary<string, string> values)
    {
        string elementDescription = Resources.OLMWebContent.LicenseDetails_TotalNodes;
        string elementCount = GetMaxElementCountDisplay();

        values.Add(elementDescription, elementCount);
    }

    public string GetMaxElementCountDisplay()
    {
        var count = licenseInfo.GetMaxElementCount();
        return count == int.MaxValue ? Resources.OLMWebContent.License_Unlimited : count.ToString();
    }

    private void AddUsedElementsCount(Dictionary<string, string> values)
    {
        string elementDescription = Resources.OLMWebContent.LicenseDetails_UsedNodes;
        string elementCount = GetUsedElementCountDisplay();

        values.Add(elementDescription, elementCount);
    }

    private string GetUsedElementCountDisplay()
    {
        int? count;
        try
        {
            count = licenseInfo.GetLicensedElementsCount();
        }
        catch (Exception ex)
        {
            log.Error("Reading Log Analyzer licensed elements count failed.", ex);
            count = null;
        }

        return count?.ToString() ?? Resources.OLMWebContent.LicenseDetails_Unavailable;
    }
}