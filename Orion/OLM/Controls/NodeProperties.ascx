<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeProperties.ascx.cs" Inherits=" Orion_OLM_NodeProperties" %>
<orion:Include runat="server" File="OLM/styles/NodeProperties.css" />
<div class="contentBlock" id="mainHolder" runat="server">
    <div runat="server" id="OLMSettingsHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal runat="server" Text="<%$ Resources: OLMWebContent, NodePlugin_Title%>" />
                </td>
            </tr>
        </table>
    </div>
    <div id="manageNodesWithOLMHolder" runat="server" automation="olmSettingsBlock" class="olmNodeProperties">
        <table width="100%" class="blueBox">
            <tr>
                <td class="leftLabelColumn" style="vertical-align: text-top">
                    <asp:CheckBox runat="server" ID="ApplyManageNodesWithOLM" AutoPostBack="true" OnCheckedChanged="ApplyManageNodesWithOLM_CheckedChanged" ClientIDMode="Static" />
                    <asp:Label AssociatedControlID="ApplyManageNodesWithOLM" runat="server" ID="LabelManageNodesWithOLM" Text="<%$ Resources: OLMWebContent, NodePlugin_MonitorNode_Title%>" />
                </td>
                <td class="rightInputColumn">
                    <asp:DropDownList ID="ManageNodesWithOLM" runat="server" ClientIDMode="Static" />
                    <asp:CustomValidator ID="OLMLicenseValidator" runat="server"
                        OnServerValidate="OLMLicenseValidator_ServerValidate"
                        ControlToValidate="ManageNodesWithOLM">
                    </asp:CustomValidator>
                    <asp:CustomValidator ID="OLMLicenseSelectedValidator" runat="server"
                        OnServerValidate="OLMLicenseSelectedValidator_ServerValidate"
                        ControlToValidate="ManageNodesWithOLM"
                        ErrorMessage="<%$ Resources: OLMWebContent, NodePlugin_MonitorNode_ValidationMessage_NoSelectedValue%>">
                    </asp:CustomValidator>
                    <div runat="server" id="OverlicensedNodeMessage" class="overlicensedNodeMessage" visible="false">
                        <div class="sw-suggestion sw-suggestion-warn">
                            <span class="sw-suggestion-icon"></span>
                            <b><%= Resources.OLMWebContent.NodePlugin_OverLicensedNodeMessage_Title%></b><br />
                            <%= Resources.OLMWebContent.NodePlugin_OverLicensedNodeMessage_Body%>
                        </div>
                    </div>
                    <span class="helpfulText" style="padding: 0px;">
                        <asp:Literal ID="DescriptionManageNodesWithOLM" runat="server" Text="<%$ Resources: OLMWebContent, NodePlugin_MonitorNode_Tooltip%>" />
                        <a class="RenewalsLink" href=<%= this.KbLoaderUrl %> target="_blank" rel="noopener noreferrer"><%= Resources.OLMWebContent.OLM_LearnMoreString%></a>
                        <ul>
                            <li id="DescriptionManageNodesWithOLM_OptionDefault" runat="server">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: OLMWebContent, NodePlugin_MonitorNode_Tooltip_OptionDefault%>" />
                            </li>
                            <li>
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: OLMWebContent, NodePlugin_MonitorNode_Tooltip_OptionMonitor%>" />
                            </li>
                            <li>
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources: OLMWebContent, NodePlugin_MonitorNode_Tooltip_OptionDontMonitor%>" />
                            </li>
                        </ul>
                    </span>
                </td>
            </tr>
        </table>
    </div>
</div>
