using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.LogMgmt.BusinessLayerProxy;
using SolarWinds.Orion.LogMgmt.BusinessLayerProxy.Licensing;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.LogMgmt.Contract.Models.Enums;

public partial class Orion_OLM_NodeProperties : UserControl, INodePropertyPlugin, IDisposable
{
    private IList<Node> nodes;
    private const string monitorNodeSettingName = "OLM.MonitorNode";
    protected const string NodePluginMonitor_NoValue = "-1";
    protected const string NodePluginMonitor_Default = "2";
    protected const string NodePluginMonitor_MonitorNode = "1";
    protected const string NodePluginMonitor_DontMonitorNode = "0";
    private static readonly Log log = new Log();
    private NodePropertyPluginExecutionMode mode;
    private readonly ISwisConnectionProxyFactory factory;
    private readonly ILicenseInfo licenseInfo;

    public string KbLoaderUrl { get { return KnowledgebaseLinkHelper.GetSalesForceKBUrl(12267);}}

    private struct DropDownItemConfig
    {
        public int Index { get; }
        public string Text { get; }

        public DropDownItemConfig(int index, string text)
        {
            Index = index;
            Text = text;
        }
    }

    private enum MonitorNodeSettingValue
    {
        NoValue = -1,
        DontMonitor = 0,
        Monitor = 1,
        MonitorDefault = 2
    }

    private readonly Dictionary<string, DropDownItemConfig> dropDownConfiguration = new Dictionary<string, DropDownItemConfig>()
    {
        [NodePluginMonitor_NoValue] = new DropDownItemConfig(0, OLMWebContent.NodePlugin_MonitorNode_PleaseSelectValue),
        [NodePluginMonitor_Default] = new DropDownItemConfig(1, OLMWebContent.NodePlugin_MonitorNode_DefaultValue),
        [NodePluginMonitor_MonitorNode] = new DropDownItemConfig(2, OLMWebContent.NodePlugin_MonitorNode_MonitorValue),
        [NodePluginMonitor_DontMonitorNode] = new DropDownItemConfig(3, OLMWebContent.NodePlugin_MonitorNode_DontMonitorValue),
    };

    public Orion_OLM_NodeProperties()
    {
        factory = new SwisConnectionProxyFactory();
        licenseInfo = LicenseInfo.CreateInstance();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // add values to dropdown list

        foreach (var item in dropDownConfiguration)
        {
            ManageNodesWithOLM.Items.Add(new ListItem(item.Value.Text, item.Key));
        }

        ScriptManager scm = ScriptManager.GetCurrent(this.Page);
        scm.RegisterAsyncPostBackControl(ApplyManageNodesWithOLM);
    }

    /// <summary>
    /// Initializes the node plugin. Runs on every 
    /// </summary>
    /// <param name="nodes">List of nodes which are processed by the node plugin.</param>
    /// <param name="mode">Editing mode of plugin.</param>
    /// <param name="pluginState">Additional state properties which are passed to the plugin.</param>
    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        if (nodes == null)
        {
            throw new ArgumentNullException(nameof(nodes));
        }

        bool isMultiSelect = nodes.Count > 1;
        // make a copy of original list as other plugins might alter it. (see https://jira.solarwinds.com/browse/NPM-9635)
        this.nodes = new List<Node>(nodes);
        this.mode = mode;

        if (!IsPostBack)
        {
            // for single node is checkbox hidden but checked by default. In contrast for many nodes it is always unchecked by default
            ApplyManageNodesWithOLM.Checked = !isMultiSelect;

            if (nodes.Count > 0)
            {
                // load setting from 1st node if there is any
                InitializeDropDown(nodes, mode);
            }
            else
            {
                // if there is no previous value, set default value
                ManageNodesWithOLM.SelectedIndex = 0;
            }
        }
        // show only when editing multiple nodes
        ApplyManageNodesWithOLM.Visible = isMultiSelect;
        // enabled for single node always for many nodes in case 1st checkbox (ApplyManageNodesWithOLM) is checked (thus changes are expected to be made)
        ManageNodesWithOLM.Enabled = !isMultiSelect || ApplyManageNodesWithOLM.Checked;

        // enable validators only when value can be changed
        OLMLicenseValidator.Enabled = ManageNodesWithOLM.Enabled;
        OLMLicenseSelectedValidator.Enabled = isMultiSelect && ApplyManageNodesWithOLM.Checked;
    }

    /// <summary>
    /// Updates the settings for selected nodes based on selected values.
    /// </summary>
    /// <returns>True in case the setting values were successfully processed; otherwise False.</returns>
    public bool Update()
    {
        try
        {
            var processedNodes = nodes;

            // node add node wizard stores node different way
            if (mode == NodePropertyPluginExecutionMode.WizChangeProperties)
            {
                processedNodes = new List<Node> { NodeWorkflowHelper.Node };
            }

            return UpdateMonitorNodeSetting(processedNodes);
        }
        catch (Exception e)
        {
            log.Error("Failed to update settings for OLM node plugin.", e);
            return false;
        }

    }

    public bool Validate()
    {
        return true;
    }

    private void InitializeDropDown(IList<Node> nodes, NodePropertyPluginExecutionMode mode)
    {
        using (var proxy = factory.CreateConnection())
        {
            if (nodes.Count > 1)
            {
                ManageNodesWithOLM.Items[dropDownConfiguration[NodePluginMonitor_NoValue].Index].Enabled = true;
                ManageNodesWithOLM.Items[dropDownConfiguration[NodePluginMonitor_Default].Index].Enabled = nodes.All(n => !n.NodeSettings.ContainsKey("OLM.MonitorNode") || n.NodeSettings["OLM.MonitorNode"] == NodePluginMonitor_Default);
            }
            else
            {
                ManageNodesWithOLM.Items[dropDownConfiguration[NodePluginMonitor_NoValue].Index].Enabled = false;

                // try to findout selected value in edit node properties for single node
                if (mode == NodePropertyPluginExecutionMode.EditProperies)
                {
                    int nodeId = nodes.First().ID;

                    var result = proxy.Query($@"SELECT DISTINCT TOP 1 ns.SettingValue, n.NodeID FROM Orion.Nodes n
                        LEFT JOIN Orion.NodeSettings ns ON ns.SettingName = '{monitorNodeSettingName}' AND n.NodeID = ns.NodeID
                        WHERE n.NodeID = {nodeId}");

                    var row = result?.Rows?.Cast<DataRow>().FirstOrDefault();
                    if ((row != null) && (!row.IsNull(0)))
                    {
                        string monitorValue = row.Field<string>(0);
                        if (dropDownConfiguration.ContainsKey(monitorValue))
                        {
                            if (monitorValue != NodePluginMonitor_Default)
                                HideDefaultOption();

                            // lets do check if node is not everlicensed, in that case we show warn user that node is actually not monitored...
                            OverlicensedNodeMessage.Visible = monitorValue == NodePluginMonitor_MonitorNode && !IsNodeLicensed(nodeId);

                            ManageNodesWithOLM.SelectedIndex = dropDownConfiguration[monitorValue].Index;
                            return;
                        }
                    }
                    ManageNodesWithOLM.SelectedIndex = dropDownConfiguration[NodePluginMonitor_Default].Index;
                }
            }
        }
    }

    private void HideDefaultOption()
    {
        ManageNodesWithOLM.Items[dropDownConfiguration[NodePluginMonitor_Default].Index].Enabled = false;
        DescriptionManageNodesWithOLM_OptionDefault.Visible = false;
    }

    private bool UpdateMonitorNodeSetting(IEnumerable<Node> nodes)
    {
        if (ApplyManageNodesWithOLM.Checked && nodes.Any())
        {
            var nodeIds = nodes.Select(node => node.ID).ToArray();
            var newValue = ParseMonitorNodeValue(ManageNodesWithOLM.SelectedValue);

            using (var proxy = LogMgmtBusinessLayerProxyFactory.Instance.Create())
            {
                var username = Context.User.Identity.Name;

                switch (newValue)
                {
                    case MonitorNodeSettingValue.DontMonitor:
                        proxy.Api.DontMonitorNodes(nodeIds, username);
                        break;
                    case MonitorNodeSettingValue.Monitor:
                    case MonitorNodeSettingValue.MonitorDefault:
                        var monitorNodesOptions = newValue == MonitorNodeSettingValue.Monitor ? MonitorNodesOptions.ReserveLicense : MonitorNodesOptions.None;
                        proxy.Api.MonitorNodes(nodeIds, monitorNodesOptions, username);
                        break;
                    default:
                        var message = $"Invalid value '{newValue}' was selected for OLM node monitoring";
                        log.Error(message);
                        throw new InvalidOperationException(message);
                }
            }
        }

        return true;
    }

    private MonitorNodeSettingValue ParseMonitorNodeValue(string value)
    {
        if (value == null)
        {
            throw new ArgumentNullException(nameof(value));
        }

        MonitorNodeSettingValue parsedValue;
        if (Enum.TryParse(value, out parsedValue))
        {
            return parsedValue;
        }

        log.Warn($"Unable to parse selected value for setting '{monitorNodeSettingName}' therefore returning fallback value '{MonitorNodeSettingValue.Monitor}'");
        return MonitorNodeSettingValue.Monitor;
    }

    protected void ApplyManageNodesWithOLM_CheckedChanged(object sender, EventArgs e)
    {
        var switcher = sender as CheckBox;

        if (switcher != null)
        {
            ManageNodesWithOLM.Enabled = switcher.Checked;
        }
    }

    public override void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
        base.Dispose();
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            factory?.Dispose();
        }
    }

    protected void OLMLicenseValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // validate only when chosen Monitor or Automatic.
        if (args.Value == NodePluginMonitor_MonitorNode || args.Value == NodePluginMonitor_Default)
        {
            int licenseSlotsTotal = licenseInfo.GetMaxElementCount();
            IEnumerable<int> licensedNodeIDs = licenseInfo.GetLicensedNodes().Distinct();
            var licenseSlotsRequested = GetNodeIDsToLicense(nodes.Select(n => n.Id), licensedNodeIDs, args.Value).Count();

            var licenseSlotsAvailable = licenseSlotsTotal - licensedNodeIDs.Count();

            bool isLicenseShortage = false;

            //when add node wizard - we can't query swis as node is not yet added
            if (mode == NodePropertyPluginExecutionMode.WizChangeProperties)
            {
                // skip validation for Default, validate only when chosen Monitor value
                isLicenseShortage = args.Value == NodePluginMonitor_Default ? false : licenseSlotsAvailable < licenseSlotsRequested;
            }
            else
            {
                // we are not in add node wizard - validate always
                isLicenseShortage = licenseSlotsAvailable < licenseSlotsRequested;
            }

            if (isLicenseShortage)
            {
                // not enough available license slots
                var validator = source as CustomValidator;
                if (validator != null)
                {
                    if (licenseSlotsAvailable <= 0)
                    {
                        validator.ErrorMessage = Resources.OLMWebContent.NodePlugin_MonitorNode_ValidationError_OutOfLicenseSlots;
                    }
                    else
                    {
                        validator.ErrorMessage = string.Format(Resources.OLMWebContent.NodePlugin_MonitorNode_ValidationError_NotEnoughLicenseSlots, licenseSlotsRequested, licenseSlotsAvailable);
                    }
                }
                args.IsValid = false;
            }
        }
    }

    protected void OLMLicenseSelectedValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // validate only when chosen no value (first option value when multi-select).
        if (args.Value == NodePluginMonitor_NoValue)
        {
            args.IsValid = false;
        }
    }

    private IEnumerable<int> GetNodeIDsToLicense(IEnumerable<int> nodeIDsToLicense, IEnumerable<int> licensedNodeIDs, string modeValue)
    {
        var nodeIDsWithoutLicense = nodeIDsToLicense.Except(licensedNodeIDs).ToArray();

        if (modeValue == NodePluginMonitor_MonitorNode || nodeIDsWithoutLicense.Length == 0)
            return nodeIDsWithoutLicense;

        // for mode == 2 (MonitorDefault) we need to remove from nodeIDsWithoutLicense all nodes which don't have message source as they don't count to license
        using (var proxy = factory.CreateConnection())
        {
            var nodeIDsString = string.Join(",", nodeIDsWithoutLicense);
            var nodeIDsWithoutMessageSourceTable = proxy.Query($"SELECT NodeID FROM Orion.Nodes n WHERE n.LogNode.NodeID IS NULL AND n.NodeID IN ({nodeIDsString})");
            var nodeIDsWithoutMessageSource = nodeIDsWithoutMessageSourceTable.Rows.Cast<DataRow>().Select(row => row.Field<int>(0));

            return nodeIDsWithoutLicense.Except(nodeIDsWithoutMessageSource).ToArray();
        }
    }

    private bool IsNodeLicensed(int nodeId)
    {
        return licenseInfo.IsLicensedNode(nodeId);
    }
}