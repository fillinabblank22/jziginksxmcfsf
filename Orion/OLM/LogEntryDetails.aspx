﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="LogEntryDetails.aspx.cs" Inherits="Orion_OLM_LogEntryDetails" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.Containers" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle %>
        - 
        <%= NetObject["Message"] %>
        - <%= Resources.OLMWebContent.OLM_LogEntryDetails_On %> 
        <orion:StatusIconControl ID="StatusIconControl" runat="server" />
        <orion:NodeLink ID="NodeLink" runat="server" />
    </h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <orion:ContainerResourceHost ID="ResourceHostControl" runat="server">
        <orion:ResourceContainer runat="server" ID="ResContainer" />
    </orion:ContainerResourceHost>
</asp:Content>
