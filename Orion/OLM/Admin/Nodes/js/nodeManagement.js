﻿(function (OLM, $, undefined) {

    var i18n = {
        addDialog: {
            dialogTitle: "@{R=OLM.Strings;K=Web_NodeManagement_Title;E=js}",
            waitMessage: "@{R=OLM.Strings; K=Web_NodeManagement_AddNodesDialog_WaitMessage; E=js}",
            okMessage: "@{R=OLM.Strings; K=Web_NodeManagement_AddNodesDialog_OkMessage; E=js}",
            nothingChangedMessage: "@{R=OLM.Strings; K=Web_NodeManagement_AddNodesDialog_NothingChangedMessage; E=js}",
            badRequestMessage: "@{R=OLM.Strings; K=Web_NodeManagement_AddNodesDialog_BadRequestMessage; E=js}",
            noAvailableLicenseSlotsMessage: "@{R=OLM.Strings; K=Web_NodeManagement_AddNodesDialog_NoAvailableLicenseMessage; E=js}",
            genericErrorMessage: "@{R=OLM.Strings; K=Web_NodeManagement_AddNodesDialog_GenericErrorMessage; E=js}",
        },
        removeDialog: {
            dialogTitle: "@{R=OLM.Strings;K=Web_NodeManagement_Title;E=js}",
            waitMessage: "@{R=OLM.Strings; K=Web_NodeManagement_RemoveNodesDialog_WaitMessage; E=js}",
            okMessage: "@{R=OLM.Strings; K=Web_NodeManagement_RemoveNodesDialog_OkMessage; E=js}",
            nothingChangedMessage: "@{R=OLM.Strings; K=Web_NodeManagement_RemoveNodesDialog_NothingChangedMessage; E=js}",
            genericErrorMessage: "@{R=OLM.Strings; K=Web_NodeManagement_RemoveNodesDialog_GenericErrorMessage; E=js}",
        },
    };

    var cssClasses = {
        dialog: "olmOperationFinishedDialog",
        successDialog: "olmSuccess",
        failDialog: "olmFail",
    };

    OLM.nodeOperationEnabled = function (grid) {
        var nodeIds = grid.GetSelectedNodeIDs();
        return nodeIds.length > 0;
    };

    OLM.addNodes = function (grid) {
        if ($("#isDemoMode").length !== 0) {
            demoAction('OLM_addNodesCallback');
        }
        else {
            var waitMsg = Ext.Msg.wait(i18n.addDialog.waitMessage);
            callWebMethod('/api2/orionlog/nodemanagement/add', true, { nodeIds: grid.GetSelectedNodeIDs(), filter: grid.GetWhere("n"), allSelected: grid.allSelected },
                function (result) {
                    waitMsg.hide();
                    if (result === true) {
                        showMessage(i18n.addDialog.dialogTitle, i18n.addDialog.okMessage, cssClasses.successDialog);
                        grid.RefreshObjects();
                    } else {
                        showMessage(i18n.addDialog.dialogTitle, i18n.addDialog.nothingChangedMessage, cssClasses.successDialog);
                    }
                },
                function (response) {
                    waitMsg.hide();

                    var title = i18n.addDialog.dialogTitle;
                    var message = i18n.addDialog.genericErrorMessage;

                    switch (response.status) {
                        case 409: // conflict (no available license)
                            message = i18n.addDialog.noAvailableLicenseSlotsMessage;
                            break;
                        case 400: // bad request
                            message = i18n.addDialog.badRequestMessage;
                            break;
                        default:
                            // nothing special, use default messages
                            break;
                    }

                    showMessage(title, message, cssClasses.failDialog);

                });
        }
    };

    OLM.removeNode = function (grid) {
        if ($("#isDemoMode").length !== 0) {
            demoAction('OLM_removeNodesCallback');
        }
        else {
            var waitMsg = Ext.Msg.wait(i18n.removeDialog.waitMessage);
            callWebMethod('/api2/orionlog/nodemanagement/remove', true, { nodeIds: grid.GetSelectedNodeIDs(), filter: grid.GetWhere("n"), allSelected: grid.allSelected },
                function (result) {
                    waitMsg.hide();
                    if (result === true) {
                        showMessage(i18n.removeDialog.dialogTitle, i18n.removeDialog.okMessage, cssClasses.successDialog);
                        grid.RefreshObjects();
                    } else {
                        showMessage(i18n.removeDialog.dialogTitle, i18n.removeDialog.nothingChangedMessage, cssClasses.successDialog);
                    }
                },
                function (response) {
                    waitMsg.hide();

                    var title = i18n.removeDialog.dialogTitle;
                    var message = i18n.removeDialog.genericErrorMessage;

                    showMessage(title, message, cssClasses.failDialog);

                });
        }
    }

    var callWebMethod = function (url, async, data, success, failure) {
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            async: async,
            success: function (result) {
                if (success)
                    success(result);
            },
            error: function (response) {
                if (failure)
                    failure(response);
            }
        });
    }

    var showMessage = function (title, message, dialogClass) {
        Ext.Msg.show({
            title: title,
            msg: message,
            minWidth: 500,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.INFO,
            cls: cssClasses.dialog + " " + dialogClass
        });
    }
}(window.OLM = window.OLM || {}, jQuery));

// node management can't handle functions nested in objects therefore assigning to window object.
var OLM_nodeOperationEnabled = OLM.nodeOperationEnabled;
var OLM_addNode = OLM.addNodes;
var OLM_removeNode = OLM.removeNode;
