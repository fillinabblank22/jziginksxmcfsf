﻿using System;
using SolarWinds.Orion.LogMgmt.OrionWeb.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_OLM_LogEntryDetails : OrionView, ILogMgmtLogEntryProvider
{
    public override string ViewType
    {
        get { return "Log Entry Details"; }
    }

    public LogMgmtLogEntry LogEntry
    {
        get { return (LogMgmtLogEntry)this.NetObject; }
    }

    protected override void OnPreInit(EventArgs e)
    {
        // Set the header "X-UA-Compatible" value to "IE=8"
        ModulePatchHelper.SetIECompatibilityRendering(8);

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ResContainer.DataSource = ViewInfo;
        ResContainer.DataBind();

        Title = ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle;

        string nodeId = LogEntry["NodeID"].ToString();
        NodeLink.NodeID = "N:" + nodeId;
        StatusIconControl.EntityId = nodeId;
    }
}