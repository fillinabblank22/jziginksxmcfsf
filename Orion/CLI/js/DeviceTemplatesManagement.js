﻿Ext.namespace('SW');
Ext.namespace('SW.CLI');
Ext.QuickTips.init();

SW.CLI.DeviceTemplatesManagment = function() {

        ORION.prefix = "CLI_DeviceTemplatesManagement_";

        var gridStore;
        var grid;
        var pageSize;
        var currentSearchTerm;

        var createEditDeviceTemplateViaWizard = function(Id) {
            if (Id) {
                window.location =
                    String.format(
                        "/Orion/NCM/Admin/DeviceTemplate/SelectNode.aspx?StepIndex=0&SetSessionData=true&Id={0}",
                        Id);
            } else {
                window.location = "/Orion/NCM/Admin/DeviceTemplate/SelectNode.aspx?StepIndex=0&SetSessionData=true";
            }
        };

        var deleteDeviceTemplates = function(items) {
            ORION.callWebService("/Orion/CLI/Services/DeviceTemplatesManagement.asmx",
                "DeleteDeviceTemplates",
                { ids: getSelectedTemplateIds(items) },
                function(result) {
                    if (result) {
                        errorHandler('@{R=CLI.Strings;K=DeviceTemplateManagement_ErrorCaption_DeleteTemplate;E=js}',
                            JSON.parse(result));
                    } else {
                        reloadGrid();
                    }
                });
        };

        var enableDisableDeviceTemplates = function(items, enable, onSuccess) {
            var ids = getSelectedTemplateIds(items);
            if (!enable) {
                //confirm before disabling
                Ext.Msg.show({
                    title: '@{R=CLI.Strings;K=DeviceTemplateManagement_MessageBoxCaption_DisableAutoDetect;E=js}',
                    msg: '@{R=CLI.Strings;K=DeviceTemplateManagement_MessageBoxContent_DisableAutoDetect;E=js}',
                    buttons: Ext.Msg.YESNO,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            enableDisableDeviceTemplatesInternal(ids, enable, onSuccess);
                        }
                    }
                });
            } else {
                //enabling, no confirmation needed
                enableDisableDeviceTemplatesInternal(ids, enable, onSuccess);
            }
        };

        var enableDisableDeviceTemplatesInternal = function(ids, enable, onSuccess) {
            ORION.callWebService("/Orion/CLI/Services/DeviceTemplatesManagement.asmx",
                "EnableDisableAutoDetect",
                { ids: ids, enable: enable },
                function(result) {
                    onSuccess(result);
                });
        };

        var addDeviceTemplate = function(name,
            systemOid,
            systemDescriptionRegex,
            useForAutoDetect,
            autoDetectType,
            comments,
            templateXml,
            onSuccess) {
            ORION.callWebService("/Orion/CLI/Services/DeviceTemplatesManagement.asmx",
                "AddDeviceTemplate",
                {
                    name: name,
                    systemOid: systemOid,
                    systemDescriptionRegex: systemDescriptionRegex,
                    useForAutoDetect: useForAutoDetect,
                    autoDetectType: autoDetectType,
                    comments: comments,
                    templateXml: templateXml
                },
                function(result) {
                    onSuccess(result);
                });
        };

        var updateDeviceTemplate = function(id,
            name,
            systemOid,
            systemDescriptionRegex,
            useForAutoDetect,
            autoDetectType,
            comments,
            templateXml,
            onSuccess) {
            ORION.callWebService("/Orion/CLI/Services/DeviceTemplatesManagement.asmx",
                "UpdateDeviceTemplate",
                {
                    id: id,
                    name: name,
                    systemOid: systemOid,
                    systemDescriptionRegex: systemDescriptionRegex,
                    useForAutoDetect: useForAutoDetect,
                    autoDetectType: autoDetectType,
                    comments: comments,
                    templateXml: templateXml
                },
                function(result) {
                    onSuccess(result);
                });
        };

        var getDeviceTemplate = function(id, onSuccess) {
            ORION.callWebService("/Orion/CLI/Services/DeviceTemplatesManagement.asmx",
                "GetDeviceTemplate",
                { id: id },
                function(result) {
                    onSuccess(result);
                });
        };

        var duplicateDeviceTemplate = function(id, copyTemplateName, copySystemOID, onSucces) {
            ORION.callWebService("/Orion/CLI/Services/DeviceTemplatesManagement.asmx",
                "DuplicateDeviceTemplate",
                { id: id, copyTemplateName: copyTemplateName, copySystemOID: copySystemOID },
                function(result) {
                    onSucces(result);
                });
        };

        var importDeviceTemplate = function() {
            window.location = "/Orion/CLI/Admin/DeviceTemplate/ImportDeviceTemplate.aspx";
        };

        var exportDeviceTempateAsFile = function(item) {
            $.download('/Orion/CLI/Admin/DeviceTemplate/DownloadDeviceTemplate.ashx', 'Id=' + item.data.ID);
        };

        var uploadDeviceTemplatesToThwackInternal = function(ids, onSuccess) {
            ORION.callWebService("/Orion/NCM/Services/ThwackDeviceTempates.asmx",
                "UploadDeviceTemplatesToTwack",
                { ids: ids },
                function(result) {
                    onSuccess(result);
                });
        };

        var uploadDeviceTemplatesToThwack = function(items) {
            var ids = getSelectedTemplateIds(items);

            var waitMsg =
                Ext.Msg.wait("@{R=CLI.Strings;K=DeviceTemplateManagement_WaitMessageBox_UploadingTemplates;E=js}");
            uploadDeviceTemplatesToThwackInternal(ids,
                function(result) {
                    waitMsg.hide();

                    var obj = Ext.decode(result);
                    if (!obj.isSuccess) {
                        obj.msg = String.format("<span style='color:#f00;'>{0}</span>", obj.msg);
                        SW.NCM.validateThwackCred(false);
                    } else {
                        SW.NCM.validateThwackCred(true);
                    }

                    function onClose() {
                        dlg.hide();
                        dlg.destroy();
                        dlg = null;
                    }

                    var dlg = new Ext.Window({
                        title:
                            "@{R=CLI.Strings;K=DeviceTemplateManagement_MessageBoxCaption_UploadingTemplatesStatus;E=js}",
                        html: obj.msg,
                        width: 350,
                        autoHeight: true,
                        border: false,
                        region: "center",
                        layout: "fit",
                        modal: true,
                        resizable: false,
                        plain: true,
                        bodyStyle: "padding:5px 0px 0px 10px;",
                        buttons: [
                            { text: "@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonClose;E=js}", handler: onClose }
                        ]
                    });
                    dlg.show();
                });
        };

        var verifyTemplateXml = function(xml) {
            var isValid = true;
            SW.CLI.callWebMethod("/Orion/CLI/Services/DeviceTemplatesManagement.asmx/VerifyTemplateXml",
                false,
                { xml: xml },
                function(result) {
                    if (result.d) {
                        isValid = false;
                        errorHandler(
                            '@{R=CLI.Strings;K=DeviceTemplateManagement_ErrorMessageBoxCaption_XmlValidation;E=js}',
                            JSON.parse(result.d));
                    }
                });
            return isValid;
        };

        var templateNameClick = function(obj) {
            var id = obj.attr("value");
            getDeviceTemplate(id,
                function(result) {
                    if (result) {
                        showPreviewDialog(result.Name, result.TemplateXml);
                    }
                });
        };

        var templateXmlResize = function(window) {
            $("#txtComments").width(window.getWidth() - $("#tdComments").width() - 50);
            $("#txtTemplateXml").width(window.getWidth() - 45);
            var height = $("#createEditDeviceTemplateDialogBody").height() -
                $("#tdTemplateXml").position().top -
                $("#tdTemplateXml").height();
            $("#txtTemplateXml").height(height - 10);
        };

        useForAutoDetectChange = function(value) {
            if (value === "true")
                $("#autoDetectTypeHolder").show();
            else
                $("#autoDetectTypeHolder").hide();

            templateXmlResize(createEditDeviceTemplateDialog);
        };

        var createEditDeviceTemplateDialog;
        var showCreateEditTemplateDialog = function(eventType, Id) {

            var txtTemplateName = $("#txtTemplateName");
            var txtSystemOID = $("#txtSystemOID");
            var txtSystemDescriptionRegex = $("#txtSystemDescriptionRegex");
            var ddlUseForAutoDetect = $("#ddlUseForAutoDetect");
            var rbAutoDetectType = $("#rbAutoDetectType");
            var txtComments = $("#txtComments");
            var txtTemplateXml = $("#txtTemplateXml");

            var rfvTemplateName = $("#rfvTemplateName")[0];
            var rfvSystemOID = $("#rfvSystemOID")[0];
            var revSystemOID = $("#revSystemOID")[0];
            var rfvSystemDescriptionRegex = $("#rfvSystemDescriptionRegex")[0];

            ValidatorEnable(rfvTemplateName, false);
            ValidatorEnable(rfvSystemOID, false);
            ValidatorEnable(revSystemOID, false);
            ValidatorEnable(rfvSystemDescriptionRegex, false);

            if (!createEditDeviceTemplateDialog) {
                createEditDeviceTemplateDialog = new Ext.Window({
                    id: 'createEditDeviceTemplateDialog',
                    applyTo: 'createEditDeviceTemplateDialog',
                    layout: 'fit',
                    width: Ext.getBody().getViewSize().width * 0.5,
                    height: Ext.getBody().getViewSize().height * 0.7,
                    minWidth: 650,
                    minHeight: 400,
                    closeAction: 'hide',
                    plain: true,
                    resizable: true,
                    modal: true,
                    contentEl: 'createEditDeviceTemplateDialogBody',
                    listeners: {
                        show: function(that) {
                            templateXmlResize(that);
                        },
                        resize: function(that, width, height) {
                            templateXmlResize(that);
                        }
                    },
                    buttons: [
                        {
                            id: 'btnSave',
                            text:
                                '@{R=CLI.Strings;K=DeviceTemplateManagement_CreateEditTemplateDialog_SaveButton;E=js}',
                            handler: function() {
                                if (!IsDemoMode) {
                                    var name = txtTemplateName.val();
                                    var systemOid = txtSystemOID.val();
                                    var systemDescriptionRegex = txtSystemDescriptionRegex.val();
                                    var useForAutoDetect = ddlUseForAutoDetect.val();
                                    var autoDetectType = useForAutoDetect === "true"
                                        ? rbAutoDetectType.find(":checked").val()
                                        : 0;
                                    var comments = txtComments.val();
                                    var templateXml = txtTemplateXml.val();

                                    ValidatorEnable(rfvTemplateName, true);
                                    ValidatorEnable(rfvSystemOID, true);
                                    ValidatorEnable(revSystemOID, true);
                                    ValidatorEnable(rfvSystemDescriptionRegex,
                                        useForAutoDetect === "true" && autoDetectType === "1");

                                    var valid = Page_ClientValidate("CreateEditTemplateValidationGroup");
                                    if (valid && verifyTemplateXml(templateXml)) {
                                        if (createEditDeviceTemplateDialog.eventType === "new" ||
                                            createEditDeviceTemplateDialog.eventType === "copy") {
                                            addDeviceTemplate(name,
                                                systemOid,
                                                systemDescriptionRegex,
                                                useForAutoDetect,
                                                autoDetectType,
                                                comments,
                                                templateXml,
                                                function(result) {
                                                    createEditDeviceTemplateDialog.hide();
                                                    reloadGrid();
                                                });
                                        } else {
                                            updateDeviceTemplate(createEditDeviceTemplateDialog.Id,
                                                name,
                                                systemOid,
                                                systemDescriptionRegex,
                                                useForAutoDetect,
                                                autoDetectType,
                                                comments,
                                                templateXml,
                                                function(result) {
                                                    createEditDeviceTemplateDialog.hide();
                                                    reloadGrid();
                                                });
                                        }
                                    }
                                } else {
                                    demoAction('CLI_DeviceTemplate_CreateEdit', this);
                                }
                            }
                        }, {
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonCancel;E=js}',
                            handler: function() {
                                createEditDeviceTemplateDialog.hide();
                                reloadGrid();
                            }
                        }
                    ]
                });
            }

            if (eventType === "edit" || eventType === "copy") {
                //edit or copy existing template
                getDeviceTemplate(Id,
                    function(result) {
                        txtTemplateName.val(eventType === "copy"
                            ? String.format("@{R=CLI.Strings;K=DeviceTemplateManagement_TemplateCopyPattern;E=js}",
                                result.Name)
                            : result.Name);
                        txtSystemOID.val(result.SystemOID);
                        txtSystemDescriptionRegex.val(result.SystemDescriptionRegex);
                        ddlUseForAutoDetect.val(result.UseForAutoDetect + "");
                        useForAutoDetectChange(result.UseForAutoDetect + "");
                        $(String.format('#rbAutoDetectType input[value="{0}"]', result.AutoDetectType))
                            .attr('checked', 'checked');
                        txtComments.val(result.Comments);
                        txtTemplateXml.val(result.TemplateXml);

                        createEditDeviceTemplateDialog.eventType = eventType;
                        createEditDeviceTemplateDialog.Id = Id;
                        createEditDeviceTemplateDialog.setTitle(eventType === "copy"
                            ? "@{R=CLI.Strings;K=DeviceTemplateManagement_CreateEditTemplateDialog_TitleCopy;E=js}"
                            : "@{R=CLI.Strings;K=DeviceTemplateManagement_CreateEditTemplateDialog_TitleEdit;E=js}");
                        createEditDeviceTemplateDialog.alignTo(document.body, "c-c");
                        createEditDeviceTemplateDialog.show();
                    });
            } else {
                //create new template
                var defaultTemplateXml =
                    '<Configuration-Management Device=\"Device Name\" SystemOID=\" 1.3.6.1.4.1.9\">\r\n\t' +
                        '<Commands>\r\n\t\t' +
                        '<Command Name=\"RESET\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"Reboot\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"EnterConfigMode\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"ExitConfigMode\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"Startup\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"Running\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"DownloadConfig\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"UploadConfig\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"DownloadConfigIndirect\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"UploadConfigIndirect\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"DownloadConfigIndirectSCP\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"UploadConfigIndirectSCP\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"EraseConfig\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"SaveConfig\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"Version\" Value=\""/>\r\n\t\t' +
                        '<Command Name=\"Disconnect\" Value=\""/>\r\n\t' +
                        '</Commands>\r\n' +
                        '</Configuration-Management>\r\n';

                txtTemplateName.val('');
                txtSystemOID.val('');
                txtSystemDescriptionRegex.val('');
                ddlUseForAutoDetect.val("false");
                useForAutoDetectChange("false");
                $('#rbAutoDetectType input[value="0"]').attr('checked', 'checked');
                txtComments.val('');
                txtTemplateXml.val(defaultTemplateXml);

                createEditDeviceTemplateDialog.eventType = eventType;
                createEditDeviceTemplateDialog.setTitle(
                    "@{R=CLI.Strings;K=DeviceTemplateManagement_CreateEditTemplateDialog_TitleCreate;E=js}");
                createEditDeviceTemplateDialog.alignTo(document.body, "c-c");
                createEditDeviceTemplateDialog.show();
            }
        };

        var duplicateDefaultTemplateDialog;
        var showDuplicateDefaultTemplateDialog = function(viaWizard) {
            var duplicateTemplateName = $("#txtDuplicateTemplateName");
            var duplicateSystemOID = $("#txtDuplicateSystemOID");
            var originTemplateName = $("#lblOriginTemplateName");

            var id = grid.getSelectionModel().getSelected().data.ID;
            var templateName = grid.getSelectionModel().getSelected().data.TemplateName;
            var templateOID = grid.getSelectionModel().getSelected().data.SystemOID;

            if (!duplicateDefaultTemplateDialog) {
                duplicateDefaultTemplateDialog = new Ext.Window({
                    id: 'DuplicateDefaultTemplateDialog',
                    applyTo: 'DuplicateDefaultTemplateDialog',
                    layout: 'fit',
                    width: 500,
                    height: 350,
                    plain: true,
                    resizable: false,
                    modal: true,
                    closeAction: 'hide',
                    title: '@{R=CLI.Strings;K=DeviceTemplateManagement_Error_CannotEdit;E=js}',
                    contentEl: 'DuplicateDefaultTemplateDialogBody',
                    buttons: [
                        {
                            id: 'Duplicate',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_DuplicateTemplateDialog_Title;E=js}',
                            handler: function() {
                                if (!IsDemoMode) {
                                    var valid = Page_ClientValidate("DuplicateTemplateValidationGroup");
                                    if (valid) {
                                        duplicateDeviceTemplate(duplicateDefaultTemplateDialog.deviceTemplateId,
                                            duplicateTemplateName.val(),
                                            duplicateSystemOID.val(),
                                            function(result) {
                                                if (result > 0) {
                                                    if (duplicateDefaultTemplateDialog.viaWizard) {
                                                        createEditDeviceTemplateViaWizard(result);
                                                    } else {
                                                        showCreateEditTemplateDialog("edit", result);
                                                        duplicateDefaultTemplateDialog.hide();
                                                    }
                                                }
                                            });
                                    }
                                } else {
                                    demoAction('CLI_DeviceTemplate_Duplicate', this);
                                }
                            }
                        }, {
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonCancel;E=js}',
                            handler: function() {
                                duplicateDefaultTemplateDialog.hide();
                            }
                        }
                    ]
                });
            }

            duplicateTemplateName.val(String.format(
                "@{R=CLI.Strings;K=DeviceTemplateManagement_TemplateCopyPattern;E=js}",
                templateName));
            duplicateSystemOID.val(templateOID);
            originTemplateName.html(templateName);

            duplicateDefaultTemplateDialog.deviceTemplateId = id;
            duplicateDefaultTemplateDialog.viaWizard = viaWizard;
            duplicateDefaultTemplateDialog.show();
        };

        var previewDialog;
        var showPreviewDialog = function(templateName, templateXml) {
            previewDialog = new Ext.Window({
                id: 'previewDialog',
                layout: 'anchor',
                width: Ext.getBody().getViewSize().width * 0.5,
                height: Ext.getBody().getViewSize().height * 0.7,
                closeAction: 'close',
                title: Ext.util.Format.htmlEncode(templateName),
                modal: true,
                resizable: true,
                plain: true,
                stateful: false,
                html: '<textarea id="templateXml" readonly="readonly" class="x-form-textarea x-form-field"></textarea>',
                buttons: [
                    {
                        text: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonClose;E=js}',
                        handler: function() {
                            previewDialog.close();
                        }
                    }
                ],
                listeners: {
                    resize: function(that, w, h) {
                        $('#templateXml').width(that.getInnerWidth() - 10);
                        $('#templateXml').height(that.getInnerHeight() - 10);
                    },
                    show: function(that) {
                        that.alignTo(document.body, "c-c");
                        $('#templateXml').text(templateXml);
                    }
                }
            }).show(null);
        };

        var isDefaultTemplateSelected = function() {
            var items = grid.getSelectionModel().getSelections();
            var res = false;
            Ext.each(items,
                function(item) {
                    if (item.data.IsDefault === true)
                        res = true;
                });
            return res;
        };

        var getSelectedTemplateIds = function(items) {
            var ids = [];

            Ext.each(items,
                function(item) {
                    ids.push(item.data.ID);
                });

            return ids;
        };

        var errorHandler = function(source, result, func) {
            if (result != null && result.Error) {
                Ext.Msg.show({
                    title: source,
                    msg: result.Msg,
                    minWidth: 500,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    fn: func
                });
            }
        };

        var replaceAll = function(txt, replace, with_this) {
            return txt.replace(new RegExp(replace, 'g'), with_this);
        };

        var highlightSearchText = function(selector, searchTerm) {
            var quoteForRegExp = function(str) {
                var toQuote = '\\/[](){}?+*|.^$'; // note: backslash must be first
                for (var i = 0; i < toQuote.length; ++i) {
                    str = replaceAll(str, '\\' + toQuote.charAt(i), '\\' + toQuote.charAt(i));
                }
                return str;
            };

            // search term highlighting adapted from http://dossy.org/archives/000338.html
            var re = new RegExp('(' + quoteForRegExp(searchTerm) + ')', 'ig');
            $(selector + " *").each(function() {
                if ($(this).children().size() > 0) return;
                var html = $(this).html();
                var newhtml = html.replace(re, '<span class="search-highlight">$1</span>');
                $(this).html(newhtml);
            });
        };

        var columnIndexByDataIndex = function(columnModel, columnsDataIndex) {
            var ids = [];
            if (!columnModel || !columnModel.config || !columnsDataIndex)
                return [];

            Ext.each(columnsDataIndex,
                function(columnDataIndex) {

                    var res = columnModel.getColumnsBy(function(c) {
                        if (c)
                            return c.dataIndex == columnDataIndex;
                        else
                            return false;
                    });

                    if (res[0])
                        ids.push(res[0].id);
                });

            return ids;
        };

        var doClean = function() {
            var map = grid.getTopToolbar().items.map;

            map.Clean.setVisible(false);
            map.txtSearch.setValue('');
            currentSearchTerm = '';
            reloadStore('');
        };

        var doSearch = function() {
            var map = grid.getTopToolbar().items.map;
            var search = map.txtSearch.getValue();

            if ($.trim(search).length == 0)
                return;

            currentSearchTerm = search;
            reloadStore(search);
            map.Clean.setVisible(true);
        };

        var getGridHeight = function() {
            var top = $('#gridCell').offset().top;
            return $(window).height() - top - $('#footer').height() - 100;
        };

        var gridResize = function() {
            if (grid != null) {
                grid.setWidth(1);
                grid.setWidth($('#gridCell').width());

                grid.setHeight(getGridHeight());
            }
        };

        var windowResize = function() {
            if (createEditDeviceTemplateDialog != null) {
                createEditDeviceTemplateDialog.setSize(Ext.getBody().getViewSize().width * 0.5,
                    Ext.getBody().getViewSize().height * 0.7);
                createEditDeviceTemplateDialog.alignTo(Ext.getBody(), "c-c");
            }

            if (previewDialog != null) {
                previewDialog.setSize(Ext.getBody().getViewSize().width * 0.5,
                    Ext.getBody().getViewSize().height * 0.7);
                previewDialog.alignTo(Ext.getBody(), "c-c");
            }
        };

        var renderDeviceTemplateName = function(value, meta, record) {
            return String.format('<a href="#" style="font-weight:bold;" class="templateName" value="{0}">{1}</a>',
                record.data.ID,
                Ext.util.Format.htmlEncode(value));
        };

        var renderUseForAutoDetect = function(value, meta, record) {
            if (value == true) {
                return (
                    '<a href="#" class="toggleOn">@{R=CLI.Strings;K=DeviceTemplateManagement_UseForAutoDetect_ToggleYes;E=js}</a>'
                );
            } else {
                return (
                    '<a href="#" class="toggleOff">@{R=CLI.Strings;K=DeviceTemplateManagement_UseForAutoDetect_ToggleNo;E=js}</a>'
                );
            }
        };

        var renderAuthor = function(value, meta, record) {
            if (!value || 0 === value.length)
                return '<span></span>';
            else if (record.data.IsDefault)
                return '<span class="author-icon-certified">' + Ext.util.Format.htmlEncode(value) + '</span>';
            else
                return '<span class="author-icon-normal">' + Ext.util.Format.htmlEncode(value) + '</span>';
        };

        var renderEncodeHtml = function(value, meta, record) {
            return Ext.util.Format.htmlEncode(value);
        };

        var reloadStore = function(searchTerm) {
            gridStore.proxy.conn.jsonData = {
                searchTerm: searchTerm
            };
            gridStore.load({ params: { limit: pageSize } });
        };

        var reloadGrid = function() {
            var pagingToolbar = grid.getBottomToolbar();
            var currentPageRecordCount = grid.store.getCount();

            var selectedRecordCount = grid.getSelectionModel().getCount();
            if (currentPageRecordCount > selectedRecordCount) {
                pagingToolbar.doLoad(pagingToolbar.cursor);
            } else {
                pagingToolbar.doLoad(0);
            }
        };

        var updateToolBarButtons = function() {
            var selCount = grid.getSelectionModel().getCount();
            var map = grid.getTopToolbar().items.map;

            var needsOnlyOneSelected = (selCount != 1);
            var needsAtLeastOneSelected = (selCount === 0);

            map.Copy.setDisabled(needsOnlyOneSelected);
            map.EditMenu.setDisabled(needsOnlyOneSelected);
            map.Edit.setDisabled(needsOnlyOneSelected);
            map.Delete.setDisabled(needsAtLeastOneSelected || isDefaultTemplateSelected());
            map.EnableDisable.setDisabled(needsAtLeastOneSelected);
            map.Export.setDisabled(needsOnlyOneSelected);
            map.UploadToThwack.setDisabled(needsAtLeastOneSelected);

            var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
            if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
                hd.addClass('x-grid3-hd-checker-on');
            } else {
                hd.removeClass('x-grid3-hd-checker-on');
            }
        };

        return {
            init: function() {

                $("#mainGrid").click(function(e) {

                    var obj = $(e.target);

                    if (obj.hasClass('templateName')) {
                        templateNameClick(obj);
                        return false;
                    }
                });

                gridStore = new ORION.WebServiceStore(
                    "/Orion/CLI/Services/DeviceTemplatesManagement.asmx/GetDeviceTemplatesPaged",
                    [
                        { name: 'ID', mapping: 0 },
                        { name: 'TemplateName', mapping: 1 },
                        { name: 'SystemOID', mapping: 2 },
                        { name: 'UseForAutoDetect', mapping: 3 },
                        { name: 'Comments', mapping: 4 },
                        { name: 'IsDefault', mapping: 5 },
                        { name: 'Author', mapping: 6 }
                    ],
                    "TemplateName");

                pageSize = parseInt(ORION.Prefs.load('PageSize', '25'));

                var pageSizeBox = new Ext.form.NumberField({
                    id: 'pageSizeBox',
                    enableKeyEvents: true,
                    allowNegative: false,
                    width: 30,
                    allowBlank: false,
                    minValue: 25,
                    maxValue: 100,
                    value: pageSize,
                    listeners: {
                        scope: this,
                        'keydown': function(f, e) {
                            var k = e.getKey();
                            if (k == e.RETURN) {
                                e.stopEvent();
                                var value = parseInt(f.getValue());
                                if (!isNaN(value) && value >= 25 && value <= 100) {
                                    pageSize = value;
                                    var pagingToolbar = grid.getBottomToolbar();
                                    if (pagingToolbar.pageSize != pageSize
                                    ) { // update page size only if it is different

                                        pagingToolbar.pageSize = pageSize;
                                        ORION.Prefs.save('PageSize', pageSize);
                                        pagingToolbar.doLoad(0);
                                    }
                                } else {
                                    return;
                                }
                            }
                        },
                        'focus': function(field) {
                            field.el.dom.select();
                        },
                        'change': function(f) {
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value >= 25 && value <= 100) {
                                pageSize = value;
                                var pagingToolbar = grid.getBottomToolbar();
                                if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                    ORION.Prefs.save('PageSize', pageSize);
                                    pagingToolbar.pageSize = pageSize;
                                    pagingToolbar.doLoad(0);
                                }
                            } else {
                                return;
                            }
                        }
                    }
                });

                var pagingToolbar = new Ext.PagingToolbar({
                    id: 'pagingToolbar',
                    store: gridStore,
                    pageSize: pageSize,
                    displayInfo: true,
                    displayMsg: '@{R=CLI.Strings;K=DeviceTemplateManagement_GridLabel_RowsCount;E=js}',
                    emptyMsg: '@{R=CLI.Strings;K=DeviceTemplateManagement_Label_NoTemplates;E=js}',
                    items: [
                        '-', new Ext.form.Label({
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Label_PageSize;E=js}',
                            style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;'
                        }), pageSizeBox
                    ]

                });

                selectionModel = new Ext.grid.CheckboxSelectionModel();
                selectionModel.addListener("selectionchange", updateToolBarButtons);

                grid = new Ext.grid.GridPanel({
                    id: 'gridControl',
                    store: gridStore,
                    renderTo: 'mainGrid',
                    sm: selectionModel,
                    xtype: grid,
                    disableSelection: true,
                    layout: 'fit',
                    autoScroll: 'true',
                    loadMask: true,
                    height: 500,
                    stripeRows: true,
                    columns: [
                        selectionModel, {
                            header: 'ID',
                            width: 80,
                            hidden: true,
                            hideable: false,
                            sortable: true,
                            dataIndex: 'ID'
                        }, {
                            id: 'TemplateName',
                            header: '@{R=CLI.Strings;K=DeviceTemplateManagement_Name_Column;E=js}',
                            width: 250,
                            hideable: false,
                            sortable: true,
                            dataIndex: 'TemplateName',
                            renderer: renderDeviceTemplateName
                        }, {
                            id: 'SystemOID',
                            header: '@{R=CLI.Strings;K=DeviceTemplateManagement_SystemOID_Column;E=js}',
                            sortable: true,
                            width: 250,
                            dataIndex: 'SystemOID'
                        }, {
                            header: '@{R=CLI.Strings;K=DeviceTemplateManagement_UseForAutoDetect_Column;E=js}',
                            width: 200,
                            sortable: true,
                            dataIndex: 'UseForAutoDetect',
                            renderer: renderUseForAutoDetect
                        }, {
                            id: 'Comments',
                            header: '@{R=CLI.Strings;K=DeviceTemplateManagement_Comments_Column;E=js}',
                            sortable: true,
                            width: 250,
                            dataIndex: 'Comments',
                            renderer: renderEncodeHtml
                        }, {
                            id: 'Author',
                            header: '@{R=CLI.Strings;K=DeviceTemplateManagement_Author_Column;E=js}',
                            sortable: true,
                            width: 150,
                            dataIndex: 'Author',
                            renderer: renderAuthor
                        }
                    ],
                    listeners: {
                        cellclick: function(grid, rowIndex, columnIndex, e) {
                            var fieldName = grid.getColumnModel().getDataIndex(columnIndex);
                            if (fieldName == 'UseForAutoDetect') {
                                if (!IsDemoMode) {
                                    var record = grid.getStore().getAt(rowIndex);
                                    var useForAutoDetect = record.get(fieldName);
                                    enableDisableDeviceTemplates(grid.getSelectionModel().getSelections(),
                                        !useForAutoDetect,
                                        function(result) {
                                            if (result) {
                                                errorHandler('Enable/Disable auto-detect', JSON.parse(result));
                                            } else {
                                                record.set(fieldName, !useForAutoDetect);
                                                record.commit();
                                            }
                                        });
                                } else {
                                    demoAction('CLI_DeviceTemplate_EnableDisableAutoDetect', this);
                                }
                            }
                        }
                    },
                    tbar:
                    [
                        {
                            id: 'CreateMenu',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_AddNew;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_AddNew;E=js}',
                            icon: '/Orion/CLI/images/icon_add.gif',
                            cls: 'x-btn-text-icon',
                            hidden: !IsNcmInstalled,
                            menu: {
                                cls: 'menu-add-edit-template',
                                showSeparator: false,
                                items: [
                                    {
                                        iconCls: 'item-no-icon',
                                        text:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EditButtonSubItem_UsingWizard;E=js}',
                                        handler: function() {
                                            createEditDeviceTemplateViaWizard();
                                        }
                                    }, {
                                        iconCls: 'item-no-icon',
                                        text:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EditButtonSubItem_UsingXmlEditor;E=js}',
                                        handler: function() {
                                            showCreateEditTemplateDialog("new");
                                        }
                                    }
                                ]
                            }
                        }, {
                            id: 'Create',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_AddNew;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_AddNew;E=js}',
                            icon: '/Orion/CLI/images/icon_add.gif',
                            cls: 'x-btn-text-icon',
                            hidden: IsNcmInstalled,
                            iconCls: 'item-no-icon',
                            handler: function() {
                                showCreateEditTemplateDialog("new");
                            }
                        }, '-', {
                            id: 'Copy',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_Copy;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_Copy;E=js}',
                            icon: '/Orion/CLI/images/icon_clone.gif',
                            cls: 'x-btn-text-icon',
                            handler: function() {
                                showCreateEditTemplateDialog("copy", grid.getSelectionModel().getSelected().data.ID);
                            }
                        }, '-', {
                            id: 'EditMenu',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_Edit;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_Edit;E=js}',
                            icon: '/Orion/CLI/images/icon_edit.gif',
                            cls: 'x-btn-text-icon',
                            hidden: !IsNcmInstalled,
                            menu: {
                                cls: 'menu-add-edit-template',
                                showSeparator: false,
                                items: [
                                    {
                                        iconCls: 'item-no-icon',
                                        text:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EditButtonSubItem_UsingWizard;E=js}',
                                        handler: function() {
                                            if (isDefaultTemplateSelected()) {
                                                showDuplicateDefaultTemplateDialog(true);
                                            } else {
                                                createEditDeviceTemplateViaWizard(grid.getSelectionModel().getSelected()
                                                    .data.ID);
                                            }
                                        }
                                    }, {
                                        iconCls: 'item-no-icon',
                                        text:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EditButtonSubItem_UsingXmlEditor;E=js}',
                                        handler: function() {
                                            if (isDefaultTemplateSelected()) {
                                                showDuplicateDefaultTemplateDialog(false);
                                            } else {
                                                showCreateEditTemplateDialog("edit",
                                                    grid.getSelectionModel().getSelected().data.ID);
                                            }
                                        }
                                    }
                                ]
                            }
                        }, {
                            id: 'Edit',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_Edit;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_Edit;E=js}',
                            icon: '/Orion/CLI/images/icon_edit.gif',
                            cls: 'x-btn-text-icon',
                            hidden: IsNcmInstalled,
                            handler: function() {
                                if (isDefaultTemplateSelected()) {
                                    showDuplicateDefaultTemplateDialog(false);
                                } else {
                                    showCreateEditTemplateDialog("edit",
                                        grid.getSelectionModel().getSelected().data.ID);
                                }
                            }
                        }, '-', {
                            id: 'Delete',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_Delete;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_Delete;E=js}',
                            icon: '/Orion/CLI/images/icon_delete.gif',
                            cls: 'x-btn-text-icon',
                            handler: function() {
                                if (!IsDemoMode) {
                                    Ext.Msg.confirm(
                                        '@{R=CLI.Strings;K=DeviceTemplateManagement_MessageBoxTitle_DeleteConfirmation;E=js}',
                                        '@{R=CLI.Strings;K=DeviceTemplateManagement_MessageBoxContent_DeleteConfirmation;E=js}',
                                        function(btn) {
                                            if (btn == 'yes') {
                                                deleteDeviceTemplates(grid.getSelectionModel().getSelections());
                                            }
                                        });
                                } else {
                                    demoAction('CLI_DeviceTemplate_Delete', this);
                                }
                            }
                        }, '-', {
                            id: 'EnableDisable',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_EnableDisable;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_EnableDisable;E=js}',
                            icon: '/Orion/images/enable_disable.png',
                            cls: 'x-btn-text-icon',
                            menu: {
                                xtype: 'menu',
                                items: [
                                    {
                                        id: 'EnableAutoDetect',
                                        text:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EnableDisableButtonSubItem_Enable;E=js}',
                                        tooltip:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EnableDisableButtonSubItemTooptip_Enable;E=js}',
                                        icon: '/Orion/images/Check.Green.gif',
                                        cls: 'x-btn-text-icon',
                                        handler: function() {
                                            if (!IsDemoMode) {
                                                enableDisableDeviceTemplates(grid.getSelectionModel().getSelections(),
                                                    true,
                                                    function(result) {
                                                        if (result) {
                                                            errorHandler(
                                                                '@{R=CLI.Strings;K=DeviceTemplateManagement_EnableAutoDetectTemplate_Confirmation;E=js}',
                                                                JSON.parse(result));
                                                        } else {
                                                            reloadGrid();
                                                        }
                                                    });
                                            } else {
                                                demoAction('CLI_DeviceTemplate_EnableAutoDetect', this);
                                            }
                                        }
                                    }, {
                                        id: 'DisableAutoDetect',
                                        text:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EnableDisableButtonSubItem_Disable;E=js}',
                                        tooltip:
                                            '@{R=CLI.Strings;K=DeviceTemplateManagement_EnableDisableButtonSubItemTooptip_Disable;E=js}',
                                        icon: '/Orion/images/failed_16x16.gif',
                                        cls: 'x-btn-text-icon',
                                        handler: function() {
                                            if (!IsDemoMode) {
                                                enableDisableDeviceTemplates(grid.getSelectionModel().getSelections(),
                                                    false,
                                                    function(result) {
                                                        if (result) {
                                                            errorHandler(
                                                                '@{R=CLI.Strings;K=DeviceTemplateManagement_DisableAutoDetectTemplate_Confirmation;E=js}',
                                                                JSON.parse(result));
                                                        } else {
                                                            reloadGrid();
                                                        }
                                                    });
                                            } else {
                                                demoAction('CLI_DeviceTemplate_DisableAutoDetect', this);
                                            }
                                        }
                                    }
                                ]
                            }
                        }, '-', {
                            id: 'Import',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_Import;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_Import;E=js}',
                            icon: '/Orion/CLI/images/icon_import.gif',
                            cls: 'x-btn-text-icon',
                            handler: function() {
                                importDeviceTemplate();
                            }
                        }, '-', {
                            id: 'Export',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_Export;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_Export;E=js}',
                            icon: '/Orion/CLI/images/icon_export.gif',
                            cls: 'x-btn-text-icon',
                            handler: function() {
                                if (!IsDemoMode) {
                                    exportDeviceTempateAsFile(grid.getSelectionModel().getSelected());
                                } else {
                                    demoAction('CLI_DeviceTemplate_Export', this);
                                }
                            }
                        }, IsNcmInstalled ? '-' : '', {
                            id: 'UploadToThwack',
                            text: '@{R=CLI.Strings;K=DeviceTemplateManagement_Button_ExportToThwack;E=js}',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_ExportToThwack;E=js}',
                            icon: '/Orion/CLI/images/icon_thwack.gif',
                            cls: 'x-btn-text-icon',
                            hidden: !IsNcmInstalled,
                            handler: function() {
                                if (!IsDemoMode) {
                                    SW.NCM.logInToThwack(uploadDeviceTemplatesToThwack,
                                        grid.getSelectionModel().getSelections(),
                                        false);
                                } else {
                                    demoAction('CLI_DeviceTemplate_UploadToThwack', this);
                                }
                            }
                        }, '->', {
                            id: 'txtSearch',
                            xtype: 'textfield',
                            cls: 'search-placeholder',
                            emptyText: '@{R=CLI.Strings;K=DeviceTemplateManagement_SearchEditBox_Watermark;E=js}',
                            width: 200,
                            enableKeyEvents: true,
                            listeners: {
                                keypress: function(obj, evnt) {
                                    if (evnt.keyCode == 13) {
                                        doSearch();
                                    }
                                }
                            }
                        }, {
                            id: 'Clean',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_CleanSearch;E=js}',
                            iconCls: 'clear-button',
                            cls: 'x-btn-icon',
                            hidden: true,
                            handler: function() {
                                doClean();
                            }
                        }, {
                            id: 'Search',
                            tooltip: '@{R=CLI.Strings;K=DeviceTemplateManagement_ButtonTooltip_Search;E=js}',
                            iconCls: 'search-button',
                            cls: 'x-btn-icon',
                            handler: function() {
                                doSearch();
                            }
                        }
                    ],
                    bbar: pagingToolbar
                });

                reloadStore('');

                grid.getView().on("refresh",
                    function() {
                        var search = currentSearchTerm;
                        if ($.trim(search).length == 0) return;

                        var columnsToHighlight =
                            columnIndexByDataIndex(grid.getColumnModel(), ['TemplateName', 'SystemOID']);
                        Ext.each(columnsToHighlight,
                            function(columnNumber) {
                                highlightSearchText("#mainGrid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                            });
                    });

                gridResize();
                $(window).resize(function() {
                    gridResize();
                    windowResize();
                });

                updateToolBarButtons();
            }
        };
    }
    ();
Ext.onReady(SW.CLI.DeviceTemplatesManagment.init, SW.CLI.DeviceTemplatesManagment);

//jquery plugin for file downloading
jQuery.download = function(url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'),
            function() {
                var pair = this.split('=');
                inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
            });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
            .appendTo('body').submit().remove();
    }
};

SW.CLI.callWebMethod = function(url, async, data, success, failure) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: async,
        success: function(result) {
            if (success)
                success(result);
        },
        error: function(response) {
            if (failure)
                failure(response);
            else {
                alert(response);
            }
        }
    });
};