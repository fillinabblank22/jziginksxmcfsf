﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeviceTemplate.ascx.cs" Inherits="Orion_CLI_Controls_DeviceTemplate" %>

<div class="contentBlock">
    <table width="100%">
        <tr>
            <td class="contentBlockHeader">
                <div class="contentBlockHeader" style="padding-top:10px;">
                    <asp:CheckBox runat="server" id="cbCliDeviceTemplate" AutoPostBack="true" OnCheckedChanged="cbCliDeviceTemplate_CheckedChanged" /><%=Resources.CliWebContent.DeviceTemplateSelector_Label_Templates %> 
                </div>
            </td>
            <td style="text-align:right;">
                <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                <a href="/Orion/CLI/Admin/DeviceTemplate/DeviceTemplatesmanagement.aspx" class="RenewalsLink" target="_blank">
                    <%=Resources.CliWebContent.DeviceTemplateSelector_Link_ManageTemplates%> 
                </a> 
            </td>
        </tr>
    </table>
    
    <table class="blueBox" width="100%">
        <tr>
            <td class="leftLabelColumn">
                <%=Resources.CliWebContent.DeviceTemplateSelector_Label_TemplateName %> 
            </td>
            <td class="rightInputColumn">
                <asp:DropDownList ID="ddlDeviceTemplate" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDeviceTemplate_SelectedIndexChanged" Width="225px" />
            </td>
        </tr>
    </table>
</div>