﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Cli.Web;
using SolarWinds.Orion.Cli.Contracts;
using SolarWinds.Orion.Cli.Contracts.DataModel;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Common;

public partial class Orion_CLI_Controls_DeviceTemplate : UserControl, INodePropertyPlugin
{
    private static readonly Log _log = new Log();
    private IList<Node> _nodes;
    private string _pageGuid;
    private Dictionary<string, object> _pluginState;

    private ICoreBusinessLayerProxyCreator CoreBusinessLayerProxyCreator =>
        CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private string SelectedDeviceTemplateId
    {
        set { _pluginState["Cli_SelectedDeviceTemplateId"] = value; }
    }

    private bool IsMultiselected
    {
        get
        {
            IList<int> Ids = null;
            Ids = NodeWorkflowHelper.GetMultiSelectedNodeIds(_pageGuid);
            if (Ids.Count == 0 && NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(_pageGuid) != null)
            {
                return NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(_pageGuid).Count > 1;
            }
            else
            {
                if (Ids == null) return false;
                return (Ids.Count > 1);
            }
        }
    }

    #region INodePropertyPlugin Members

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode,
        Dictionary<string, object> pluginState)
    {
        _nodes = new List<Node>(nodes);
        _pluginState = pluginState;

        HiddenField hf = (ControlHelper.FindControlRecursive(Page, "HiddenFieldGuid") as HiddenField);
        if (hf != null)
        {
            _pageGuid = hf.Value;
        }

        if (!IsPostBack)
        {
            LoadDeviceTemplateDropDownList();
            SelectDeviceTemplateForNodes();

            SelectedDeviceTemplateId = ddlDeviceTemplate.SelectedValue;

            if (IsMultiselected)
            {
                cbCliDeviceTemplate.Visible = true;
                cbCliDeviceTemplate.Checked = false;

                cbCliDeviceTemplate_CheckedChanged(cbCliDeviceTemplate, EventArgs.Empty);
            }
            else
            {
                cbCliDeviceTemplate.Visible = false;
            }
        }
    }

    public bool Update()
    {
        if (OrionConfiguration.IsDemoServer)
        {
            return false;
        }
        if (_nodes == null) return true;

        if (IsMultiselected && !cbCliDeviceTemplate.Checked) return true;

        if (_pageGuid != null)
        {
            //clear nodes retrieved from session without guid
            _nodes.Clear();

            //get nodes using guid
            _nodes = GetNodes(_pageGuid);
        }

        if (_nodes.Count > 0)
        {
            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
            int templateId = Convert.ToInt32(ddlDeviceTemplate.SelectedValue, CultureInfo.InvariantCulture);

            foreach (Node node in _nodes)
            {
                if (node.ID <= 0)
                {
                    var workflowNode = NodeWorkflowHelper.Node;
                    if (workflowNode.ID > 0)
                    {
                        if (templateId > 0)
                            mng.SetTemplateForNode(workflowNode.ID, templateId);
                        else
                            mng.ClearTemplateForNode(workflowNode.ID);
                    }
                }
                else if (node.ID > 0)
                {
                    if (templateId > 0)
                        mng.SetTemplateForNode(node.ID, templateId);
                    else
                        mng.ClearTemplateForNode(node.ID);
                }
            }
        }

        return true;
    }

    public bool Validate()
    {
        return true;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ddlDeviceTemplate_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectedDeviceTemplateId = ddlDeviceTemplate.SelectedValue;
    }

    protected void cbCliDeviceTemplate_CheckedChanged(object sender, EventArgs e)
    {
        ddlDeviceTemplate.Enabled = cbCliDeviceTemplate.Checked;
        ddlDeviceTemplate.CssClass = cbCliDeviceTemplate.Checked ? string.Empty : "disabled";
    }

    private void LoadDeviceTemplateDropDownList()
    {
        var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
        var templates = mng.GetDeviceTemplates(false);

        ddlDeviceTemplate.Items.Clear();
        ddlDeviceTemplate.Items.Add(new ListItem(CliWebContent.DeviceTemplateSelector_Dropdown_AutoDetermine, "0"));
        foreach (var template in templates)
        {
            ddlDeviceTemplate.Items.Add(new ListItem(template.Name, template.Id.ToString()));
        }
    }

    private void SelectDeviceTemplateForNodes()
    {
        string autoDetectTemplateId = "0"; //Auto Determine 
        string currentTemplateId = string.Empty;
        string oldTemplateId = string.Empty;
        string newTemplateId = string.Empty;

        var Ids = new List<int>();
        if (NodeWorkflowHelper.GetMultiSelectedNodeIds(_pageGuid).Count > 0)
        {
            Ids = NodeWorkflowHelper.GetMultiSelectedNodeIds(_pageGuid);
        }
        else if (NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(_pageGuid).Count > 0)
        {
            foreach (string id in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(_pageGuid))
            {
                Ids.Add(NodeWorkflowHelper.GetObjectId(id));
            }
        }

        if (Ids.Count > 0)
        {
            var template = GetDeviceTemplateForNode(Ids[0]);
            oldTemplateId = template != null
                ? template.Id.ToString(CultureInfo.InvariantCulture)
                : autoDetectTemplateId;
            currentTemplateId = oldTemplateId;
        }
        else
        {
            currentTemplateId = autoDetectTemplateId;
        }

        for (var i = 1; i < Ids.Count; i++)
        {
            var template = GetDeviceTemplateForNode(Ids[i]);
            newTemplateId = template != null
                ? template.Id.ToString(CultureInfo.InvariantCulture)
                : autoDetectTemplateId;
            if (!oldTemplateId.Equals(newTemplateId, StringComparison.InvariantCulture))
            {
                currentTemplateId = autoDetectTemplateId;
                break;
            }

            newTemplateId = oldTemplateId;
        }

        ddlDeviceTemplate.SelectedValue = ddlDeviceTemplate.Items.FindByValue(currentTemplateId) != null
            ? currentTemplateId
            : autoDetectTemplateId;
    }

    private DeviceTemplate GetDeviceTemplateForNode(int nodeId)
    {
        DeviceTemplate template = null;
        try
        {
            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
            template = mng.GetTemplateForNode(nodeId, false);
        }
        catch (Exception ex)
        {
            _log.WarnFormat("Unable to get template for node: {0}. {1}", nodeId, ex);
        }

        return template;
    }

    private List<Node> GetNodes(string guid)
    {
        var nodes = new List<Node>();
        using (var coreProxy = CoreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            if (NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count > 0)
            {
                foreach (var nodeId in NodeWorkflowHelper.GetMultiSelectedNodeIds(guid))
                {
                    var engineId = GetEngineIdForNodeId(coreProxy, nodeId);
                    Node node = GetNodeById(nodeId, engineId);
                    nodes.Add(node);
                }
            }
            else if (NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid).Count > 0)
            {
                foreach (string coreNodeIdWithEngineId in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid))
                {
                    int nodeId = Int32.Parse(coreNodeIdWithEngineId.Split(':')[1]);
                    int engineId = Int32.Parse(coreNodeIdWithEngineId.Split(':')[2]);
                    Node node = GetNodeById(nodeId, engineId);
                    nodes.Add(node);
                }
            }
        }

        return nodes;
    }

    private int GetEngineIdForNodeId(ICoreBusinessLayer proxy, int nodeId)
    {
        var engineId = proxy.GetEngineIdForNetObject($"N:{nodeId}");
        return engineId;
    }

    private Node GetNodeById(int nodeId, int engineId)
    {
        using (var coreProxy = CoreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler, engineId, true))
        {
            return coreProxy.GetNodeWithOptions(nodeId, false, false);
        }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        throw new Exception("BusinessLayerException:Cli Device Template");
    }
}