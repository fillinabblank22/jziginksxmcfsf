﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedCiscoAsaMonitoring.ascx.cs" Inherits="Orion_CLI_Controls_AdvancedCiscoAsaMonitoring" %>

<%@ Register Src="~/Orion/Controls/PasswordTextBox.ascx"  TagPrefix="orion" TagName="PasswordTextBox" %>

<script type="text/javascript">
    var hideTestDetailsBar = function() {
        $("#<%=TestDetailsBar.ClientID%>").hide();
    }

    var validateCliCredentials = function() {
        var rfvUsername = $("[id$='_rfvUsername']")[0];
        var rfvPassword = $("[id$='_rfvPassword']")[0];
        var rfvSSHPort = $("[id$='_rfvSSHPort']")[0];
        var cvSSHPort = $("[id$='_cvSSHPort']")[0];

        ValidatorEnable(rfvUsername, true);
        ValidatorEnable(rfvPassword, false);
        ValidatorEnable(rfvSSHPort, true);
        ValidatorEnable(cvSSHPort, true);

        return rfvUsername.isvalid && rfvSSHPort.isvalid && cvSSHPort.isvalid;
    }

    var testButtonClick = function () {
        hideTestDetailsBar();
        return validateCliCredentials();
    }
</script>

<div>
    <table width="100%" style="padding-top:15px;padding-bottom:15px;">
        <tr>
            <td class="contentBlockModuleHeader">
                <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
                <%=Resources.CliWebContent.EditNodePlugin_Label_PollingSettings %> 
            </td>
            <td class="rightInputColumn">
				<asp:CheckBox ID="cbAdvCiscoAsaMonitoring" runat="server" AutoPostBack="True" OnCheckedChanged="cbAdvCiscoAsaMonitoring_CheckedChanged" Text="<%$ Resources: CliWebContent,EditNodePlugin_Checkbox_EnablePolling %>" />
                <div id="AdvCiscoAsaMonitoringForMultipleMode" runat="server">
                    <%=Resources.CliWebContent.EditNodePlugin_Label_EnablePolling %> &nbsp;
                    <asp:DropDownList ID="ddlAdvCiscoAsaMonitoring" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAdvCiscoAsaMonitoring_SelectedIndexChanged">
                        <asp:ListItem Text="<%$ Resources: CliWebContent,EditNodePlugin_DropDown_KeepUnchnaged %>" Value="Keep Unchanged" Enabled="false"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: CliWebContent,EditNodePlugin_DropDown_Yes %>" Value="Yes"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: CliWebContent,EditNodePlugin_DropDown_No %>" Value="No"></asp:ListItem>
                    </asp:DropDownList>
                </div>
				<a class="sw-link" rel="help" href="http://www.solarwinds.com/documentation/helploader.aspx?topic=orionph-cli-polling.htm" target="_blank" tabindex="-1"> <%=Resources.CliWebContent.EditNodePlugin_Link_LearnMore %> </a>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="helpfulText" style="padding-left:5px;">
                <%=Resources.CliWebContent.EditNodePlugin_Label_Description %>
            </td>
        </tr>
    </table>

    <table class="blueBox" width="100%" runat="server" id="AdvCiscoAsaMonitoringBox">
        <tr>
            <td class="leftLabelColumn">
                <%=Resources.CliWebContent.EditNodePlugin_Label_UserName %>
            </td>
            <td class="rightInputColumn" nowrap="nowarp">
                <asp:TextBox ID="txtUsername" runat="server" Width="225px" />
                <asp:RequiredFieldValidator ID="rfvUsername" ControlToValidate="txtUsername" Display="Dynamic" ErrorMessage="<%$ Resources: CliWebContent,EditNodePlugin_ValidationError_UserNameRequired %>" runat="server" style="margin-left:5px;" /> 
                <a class="sw-link" rel="help" href="http://www.solarwinds.com/documentation/kbloader.aspx?kb=MT92237" target="_blank" tabindex="-1"> <%=Resources.CliWebContent.EditNodePlugin_HelpLink_Priviledges %></a>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%=Resources.CliWebContent.EditNodePlugin_Label_Password %>
            </td>
            <td class="rightInputColumn">
                <orion:PasswordTextBox ID="txtPassword" runat="server" Width="225px" IsEnabledPasswordTextBox="True" />
                <asp:CustomValidator ID="rfvPassword" Display="Dynamic" OnServerValidate="ValidatePassword" ErrorMessage="<%$ Resources: CliWebContent,EditNodePlugin_ValidationError_PasswordRequired %>" runat="server" style="margin-left:5px;" /> 
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%=Resources.CliWebContent.EditNodePlugin_Label_EnablePassword%>
            </td>
            <td class="rightInputColumn">
                <orion:PasswordTextBox ID="txtEnablePassword" runat="server" Width="225px" IsEnabledPasswordTextBox="True" />
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%=Resources.CliWebContent.EditNodePlugin_Label_SSHPort %>
            </td>
            <td class="rightInputColumn" nowrap="nowarp">
                <asp:TextBox ID="txtSSHPort" runat="server" Width="225px" />
                <asp:RequiredFieldValidator ID="rfvSSHPort" ControlToValidate="txtSSHPort" Display="Dynamic" ErrorMessage="<%$ Resources: CliWebContent,EditNodePlugin_ValidationError_SSHPortRequired %>" runat="server" style="margin-left:5px;" /> 
                <asp:CompareValidator runat="server" ID="cvSSHPort" ControlToValidate="txtSSHPort" ValueToCompare="0" ErrorMessage="<%$ Resources: CliWebContent,EditNodePlugin_ValidationError_SSHPortValue %>" Operator="GreaterThan" Type="Integer" Display="Dynamic" style="margin-left:5px;" />
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%=Resources.CliWebContent.EditNodePlugin_Label_UseInteractiveAuthentication %>
            </td>
            <td class="rightInputColumn">
                <asp:DropDownList ID="dropDownUseKeyboardInteractiveAuthentication" runat="server">
                    <asp:ListItem Text="<%$ Resources: CliWebContent,EditNodePlugin_DropDown_Yes %>" Value="True"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources: CliWebContent,EditNodePlugin_DropDown_No %>" Value="False" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="TestBar" runat="server">
            <td class="leftLabelColumn">
                &nbsp;
            </td>
            <td class="rightInputColumn" nowrap="nowarp">
                <div id="TestDetailsBar" runat="server" style="display:none;">
                    <div id="TestSuccessBar" runat="server" style="padding-left:5px;background-color:#D6F6C6;" visible="false">
                        <img style="vertical-align:middle;" src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                        <asp:Label ID="TestSuccessMessage" runat="server"></asp:Label>
                    </div>
                    <div id="TestFailedBar" runat="server"  style="padding-left:5px;background-color:#FEEE90;color:Red;" visible="false">
                        <img style="vertical-align:middle;" src="/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                        <asp:Label ID="TestFailedMessage" runat="server"></asp:Label>
                    </div>

                    <div id="TestSuccessDetailsBar" runat="server" style="padding-top:5px;" visible="false">
                        <asp:LinkButton runat="server" ID="btnShowTestDetails" style="vertical-align:middle;" Font-Underline="false" Font-Size="8pt" OnClick="btnShowTestOutput_Click" CausesValidation="false">
                            <img alt="" id="ShowHideTestOutputArrow" style="vertical-align:middle;" runat="server" src="/Orion/CLI/images/arrowright.gif" />
                            <asp:Label ID="ShowHideTestOutputLabel" runat="server" style="vertical-align:middle;padding-left:5px;"></asp:Label>
                        </asp:LinkButton>
                        <div id="TestOutputBar" runat="server" visible="false">
		                    <asp:TextBox ID="TestOutputBox" runat="server" BackColor="#E6E6DD" ForeColor="#5D5C5E" TextMode="MultiLine" Font-Size="9pt" ReadOnly="True" Height="150px" Width="400px" spellcheck="false"></asp:TextBox>
	                    </div>
                    </div>
                </div>
                <div style="padding-top:5px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="btnTestUpdatePanel" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <orion:LocalizableButton ID="btnTest" runat="server" DisplayType="Small" LocalizedText="Test" OnClientClick="return testButtonClick();" OnClick="btnTest_Click" CausesValidation="false"/>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="padding-left:10px;">
                                <asp:UpdateProgress runat="server" ID="testUpdateProgress" AssociatedUpdatePanelID="btnTestUpdatePanel" DynamicLayout="true" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <img src="/Orion/images/animated_loading_sm3_whbg.gif" /><span style="font-weight:bold;"> <%=Resources.CliWebContent.EditNodePlugin_Label_Validating %></span>                  
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>