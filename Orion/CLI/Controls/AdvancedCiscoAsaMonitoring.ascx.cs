﻿using System;
using System.Net;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Cli.Contracts;
using SolarWinds.Orion.Cli.Contracts.DataModel;
using SolarWinds.Orion.Cli.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Common;

public partial class Orion_CLI_Controls_AdvancedCiscoAsaMonitoring : UserControl, INodePropertyPlugin
{
    private Dictionary<string, object> _pluginState;
    private IList<Node> _nodes;
    private NodePropertyPluginExecutionMode _mode;
    private string _pageGuid;

    private const string Oid_System = "1.3.6.1.2.1.1";
    private const string sysObjectID = "2.0";
    private const string sysDescr = "1.0";

    private const string __nodeIp = "__nodeIp";

    private const string Cli_Username = "Cli_Username";
    private const string Cli_Password = "Cli_Password";
    private const string Cli_EnablePassword = "Cli_EnablePassword";
    private const string Cli_SSHPort = "Cli_SSHPort";
    private const string Cli_UseKeyboardInteractiveAuthentication = "Cli_UseKeyboardInteractiveAuthentication";

    private const string Cli_OrionNodeCliCredentials = "Cli_OrionNodeCliCredentials";

    private ICoreBusinessLayerProxyCreator CoreBusinessLayerProxyCreator =>
        CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private bool AdvancedCiscoAsaMonitoringEnabled
    {
        set
        {
            _pluginState["Cli_AdvancedCiscoAsaMonitoringEnabled"] = value;
            ShowCliDeviceTemplatePlugin(value);
        }
    }

    private OrionNodeCliCredentials Credentials
    {
        get
        {
            if (!_pluginState.ContainsKey(Cli_OrionNodeCliCredentials))
            {
                _pluginState[Cli_OrionNodeCliCredentials] = null;
            }

            return (OrionNodeCliCredentials) _pluginState[Cli_OrionNodeCliCredentials];
        }
        set { _pluginState[Cli_OrionNodeCliCredentials] = value; }
    }

    private string Username
    {
        get
        {
            if (!_pluginState.ContainsKey(Cli_Username))
            {
                _pluginState[Cli_Username] = string.Empty;
            }

            return _pluginState[Cli_Username] as string;
        }
        set { _pluginState[Cli_Username] = value; }
    }

    private string Password
    {
        get
        {
            if (!_pluginState.ContainsKey(Cli_Password))
            {
                _pluginState[Cli_Password] = string.Empty;
            }

            return _pluginState[Cli_Password] as string;
        }
        set { _pluginState[Cli_Password] = value; }
    }

    private string EnablePassword
    {
        get
        {
            if (!_pluginState.ContainsKey(Cli_EnablePassword))
            {
                _pluginState[Cli_EnablePassword] = string.Empty;
            }

            return _pluginState[Cli_EnablePassword] as string;
        }
        set { _pluginState[Cli_EnablePassword] = value; }
    }

    private string SSHPort
    {
        get
        {
            if (!_pluginState.ContainsKey(Cli_SSHPort))
            {
                _pluginState[Cli_SSHPort] = "22";
            }

            return _pluginState[Cli_SSHPort] as string;
        }
        set { _pluginState[Cli_SSHPort] = value; }
    }


    private bool UseKeyboardInteractiveAuthentication
    {
        get
        {
            if (!_pluginState.ContainsKey(Cli_UseKeyboardInteractiveAuthentication))
            {
                _pluginState[Cli_UseKeyboardInteractiveAuthentication] = false;
            }

            return (bool) _pluginState[Cli_UseKeyboardInteractiveAuthentication];
        }
        set { _pluginState[Cli_UseKeyboardInteractiveAuthentication] = value; }
    }

    #region INodePropertyPlugin members

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode,
        Dictionary<string, object> pluginState)
    {
        //show only if NPM or UDT installed
        var plugin = NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == "CliAdvancedCiscoAsaMonitoring");
        if (plugin != null && (ModulesCollector.IsModuleInstalled("NPM") &&
                               ModulesCollector.GetModuleVersion("NPM") >= new Version(12, 2)
                               || ModulesCollector.IsModuleInstalled("UDT") &&
                               ModulesCollector.GetModuleVersion("UDT") >= new Version(3, 3)))
        {
            plugin.Visible = true;
        }

        _pluginState = pluginState;
        _nodes = new List<Node>(nodes);
        _mode = mode;

        if (!IsPostBack)
        {
            TestBar.Visible = _nodes.Count() == 1;

            txtUsername.Text = this.Username;
            txtPassword.Text = this.Password;
            txtEnablePassword.Text = this.EnablePassword;
            txtSSHPort.Text = this.SSHPort;
            dropDownUseKeyboardInteractiveAuthentication.SelectedIndex = UseKeyboardInteractiveAuthentication ? 0 : 1;

            switch (_mode)
            {
                case NodePropertyPluginExecutionMode.WizChangeProperties:
                {
                    cbMultipleSelection.Visible = false;
                    cbMultipleSelection.Checked = true;

                    AdvCiscoAsaMonitoringForMultipleMode.Visible = false;

                    cbAdvCiscoAsaMonitoring.Visible = true;
                    cbAdvCiscoAsaMonitoring.Checked = false;
                    cbAdvCiscoAsaMonitoring_CheckedChanged(this, EventArgs.Empty);

                    break;
                }
                case NodePropertyPluginExecutionMode.EditProperies:
                {
                    if (_nodes.Count == 1)
                    {
                        cbMultipleSelection.Visible = false;
                        cbMultipleSelection.Checked = true;

                        AdvCiscoAsaMonitoringForMultipleMode.Visible = false;

                        var credentials = GetCredentialsForNode(_nodes[0].ID);

                        cbAdvCiscoAsaMonitoring.Checked = (credentials != null);
                        cbAdvCiscoAsaMonitoring_CheckedChanged(this, EventArgs.Empty);

                        InitCredentials(credentials);
                    }
                    else if (_nodes.Count > 1)
                    {
                        cbMultipleSelection.Visible = true;
                        cbMultipleSelection.Checked = false;

                        AdvCiscoAsaMonitoringForMultipleMode.Visible = true;
                        ddlAdvCiscoAsaMonitoring.Enabled = false;

                        cbAdvCiscoAsaMonitoring.Visible = false;

                        InitCredentialsForMultipleMode(_nodes);

                        cbMultipleSelection_CheckedChanged(this, EventArgs.Empty);
                        ddlAdvCiscoAsaMonitoring_SelectedIndexChanged(this, EventArgs.Empty);
                    }

                    break;
                }
                default:
                    break;
            }
        }
        else
        {
            //update credentials on non postback events (test etc.)
            UpdateCredentials();
        }

        //on every page reload repopulate the password boxes if we stored a passwords previously
        txtPassword.Text = this.Password;
        txtEnablePassword.Text = this.EnablePassword;
    }

    public bool Update()
    {
        if (OrionConfiguration.IsDemoServer)
        {
            return false;
        }
        switch (_mode)
        {
            case NodePropertyPluginExecutionMode.WizChangeProperties:
            {
                if (cbAdvCiscoAsaMonitoring.Checked)
                {
                    SetCredentialsForNode(NodeWorkflowHelper.Node);
                }

                break;
            }
            case NodePropertyPluginExecutionMode.EditProperies:
            {
                if (!cbMultipleSelection.Checked)
                    return true;

                if (_pageGuid != null)
                {
                    //clear nodes retrieved from session without guid
                    _nodes.Clear();

                    //get nodes using guid
                    _nodes = GetNodes(_pageGuid);
                }

                if (_nodes != null)
                {
                    if (_nodes.Count == 1)
                    {
                        if (cbAdvCiscoAsaMonitoring.Checked)
                        {
                            SetCredentialsForNode(_nodes[0]);
                        }
                        else
                        {
                            DeleteCredentialsForNode(_nodes[0].ID);
                        }
                    }
                    else if (_nodes.Count > 1)
                    {
                        foreach (var node in _nodes)
                        {
                            if (ddlAdvCiscoAsaMonitoring.SelectedValue.Equals("Yes"))
                            {
                                SetCredentialsForNode(node);
                            }
                            else if (ddlAdvCiscoAsaMonitoring.SelectedValue.Equals("No"))
                            {
                                DeleteCredentialsForNode(node.ID);
                            }
                            else if (ddlAdvCiscoAsaMonitoring.SelectedValue.Equals("Keep Unchanged"))
                            {
                                var credentials = GetCredentialsForNode(node.ID);
                                if (credentials != null)
                                {
                                    SetCredentialsForNode(node, credentials);
                                }
                            }
                        }
                    }
                }

                break;
            }
        }

        return true;
    }

    public bool Validate()
    {
        return true;
    }

    #endregion

    private void InitCredentialsForMultipleMode(IList<Node> nodes)
    {
        List<OrionNodeCliCredentials> list = new List<OrionNodeCliCredentials>();
        foreach (var node in nodes)
        {
            var credentials = GetCredentialsForNode(node.ID);
            list.Add(credentials);
        }

        var areAllCredentialsIsNull = list.Count() == list.Count(x => x == null);
        var areAllCredentialsTheSame = !areAllCredentialsIsNull && list.Distinct().Count() == 1;
        var atLeastOneCredentialsIsNull = list.Any(x => x == null);

        if (areAllCredentialsTheSame)
        {
            ddlAdvCiscoAsaMonitoring.SelectedValue = "Yes";

            InitCredentials(list.First());
        }
        else
        {
            if (areAllCredentialsIsNull)
            {
                ddlAdvCiscoAsaMonitoring.SelectedValue = "No";
            }
            else if (atLeastOneCredentialsIsNull)
            {
                ddlAdvCiscoAsaMonitoring.Items.FindByValue("Keep Unchanged").Enabled = true;
                ddlAdvCiscoAsaMonitoring.SelectedValue = "Keep Unchanged";
            }

            InitCredentials();
        }
    }

    private void InitCredentials(OrionNodeCliCredentials credentials = null)
    {
        Credentials = credentials;

        this.Username = credentials != null ? credentials.UserName : string.Empty;
        txtUsername.Text = this.Username;

        this.Password = credentials != null ? credentials.Password : string.Empty;
        txtPassword.Text = this.Password;

        this.EnablePassword = credentials != null ? credentials.EnablePassword : string.Empty;
        txtEnablePassword.Text = this.EnablePassword;

        this.SSHPort =
            credentials != null
                ? Convert.ToString(credentials.Port, CultureInfo.InvariantCulture)
                : "22"; //use 22 as default value for SSH port
        txtSSHPort.Text = this.SSHPort;

        this.UseKeyboardInteractiveAuthentication =
            credentials != null ? credentials.UseKeyboardInteractiveAuthentication : false;
    }

    private void UpdateCredentials()
    {
        this.Username = txtUsername.Text;
        this.Password = txtPassword.Text;
        this.EnablePassword = txtEnablePassword.Text;
        this.SSHPort = txtSSHPort.Text;
        this.UseKeyboardInteractiveAuthentication = dropDownUseKeyboardInteractiveAuthentication.SelectedIndex == 0;
    }

    private OrionNodeCliCredentials GetCredentialsForNode(int nodeId)
    {
        var mng = ServiceLocator.Container.Resolve<ICredentialsReader>();
        return mng.GetCredentialsForNode(nodeId);
    }

    private void SetCredentialsForNode(Node node, OrionNodeCliCredentials otherCredentials = null)
    {
        if (otherCredentials == null)
            otherCredentials = Credentials;

        var orionNodeCliCredentials = CreateOrionNodeCliCredentials(node);
        if (!orionNodeCliCredentials.Equals(otherCredentials))
        {
            var mng = ServiceLocator.Container.Resolve<ICredentialsWriter>();
            mng.SetCredentialsForNode(node.ID, orionNodeCliCredentials);
        }
    }

    private void DeleteCredentialsForNode(int nodeId)
    {
        var mng = ServiceLocator.Container.Resolve<ICredentialsWriter>();
        mng.DeleteCredentialsForNode(nodeId);
    }

    private OrionNodeCliCredentials CreateOrionNodeCliCredentials(Node node)
    {
        return new OrionNodeCliCredentials()
        {
            CredentialsName = $"{HttpContext.Current.Profile.UserName} ({node.Caption})",
            UserName = this.Username,
            Password = this.Password,
            EnablePassword = this.EnablePassword,
            Port = Convert.ToInt32(this.SSHPort, CultureInfo.InvariantCulture),
            UseKeyboardInteractiveAuthentication = this.UseKeyboardInteractiveAuthentication
        };
    }

    private int GetSelectedDeviceTemplateId()
    {
        int selectedDeviceTemplateId = 0;
        var cliDeviceTemplatePlugin =
            NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == "CliDeviceTemplate");
        object value;
        if (cliDeviceTemplatePlugin != null &&
            cliDeviceTemplatePlugin.State.TryGetValue("Cli_SelectedDeviceTemplateId", out value))
        {
            if (!int.TryParse(value.ToString(), out selectedDeviceTemplateId))
                selectedDeviceTemplateId = 0;
        }

        return selectedDeviceTemplateId;
    }

    private void TestInternal(Node node)
    {
        TestDetailsBar.Attributes["style"] = "display:block;padding-top:5px;";
        TestSuccessBar.Visible = false;
        TestFailedBar.Visible = false;
        TestSuccessDetailsBar.Visible = false;

        var ipAddress = GetNodeIpAddress(node);
        var orionNodeCliCredentials = CreateOrionNodeCliCredentials(node);
        var selectedDeviceTemplateId = GetSelectedDeviceTemplateId();
        var engineId = node.EngineID;

        if (selectedDeviceTemplateId == 0) //auto determine device template selected
        {
            var response = NodeSNMPQuery(node, ipAddress);
            selectedDeviceTemplateId = GetAutoDetectDeviceTemplateId(
                response != null ? response[sysObjectID] : string.Empty,
                response != null ? response[sysDescr] : string.Empty);
        }

        var loginValidatorClient = ServiceLocator.Container.Resolve<ILoginValidatorClient>();
        var result =
            loginValidatorClient.ValidateLogin(ipAddress, orionNodeCliCredentials, selectedDeviceTemplateId, engineId);

        if (result.IsError)
        {
            TestFailedBar.Visible = true;
            TestFailedMessage.Text = $"{CliWebContent.EditNodePlugin_Label_TestFailed} {result.Data}";
        }
        else
        {
            TestSuccessBar.Visible = true;
            TestSuccessMessage.Text = CliWebContent.EditNodePlugin_Label_TestSuccessfull;

            TestSuccessDetailsBar.Visible = true;
            TestOutputBar.Visible = true;
            TestOutputBox.Text = result.Data;
            btnShowTestOutput_Click(this, EventArgs.Empty);
        }
    }

    private int GetAutoDetectDeviceTemplateId(string systemOid, string systemDescription)
    {
        try
        {
            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
            var deviceTemplate = mng.AutoDetectTemplate(systemOid, systemDescription);
            return deviceTemplate.Id;
        }
        catch
        {
            return 0;
        }
    }

    private string GetNodeIpAddress(Node node)
    {
        object value;
        if (_pluginState.TryGetValue(__nodeIp, out value))
        {
            string ipAddress = (value == null) ? string.Empty : value.ToString();
            if (!string.IsNullOrEmpty(ipAddress))
                return ipAddress;
        }

        return node.IpAddress;
    }

    private Dictionary<string, string> NodeSNMPQuery(Node node, string ipAddress)
    {
        var nodeForSnmpQuery = node;
        IPAddress oldIpAddress = IPAddress.Parse(node.IpAddress);
        if (!string.IsNullOrEmpty(ipAddress) && !IPAddress.Parse(ipAddress).Equals(oldIpAddress))
        {
            nodeForSnmpQuery.IpAddress = ipAddress;
        }

        Dictionary<string, string> response;
        if (SNMPQuery(nodeForSnmpQuery, Oid_System, "getsubtree", out response))
            return response;

        return null;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField hf = (ControlHelper.FindControlRecursive(Page, "HiddenFieldGuid") as HiddenField);
        if (hf != null)
        {
            _pageGuid = hf.Value;
        }
    }

    protected void ValidatePassword(object source, ServerValidateEventArgs args)
    {
        args.IsValid = !string.IsNullOrWhiteSpace(txtPassword.Text);
    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(txtPassword.Text))
        {
            TestDetailsBar.Attributes["style"] = "display:none;";
            rfvPassword.IsValid = false;
            return;
        }

        TestInternal(_nodes[0]);
    }

    protected void btnShowTestOutput_Click(object sender, EventArgs e)
    {
        bool visible = TestOutputBar.Visible;
        TestOutputBar.Visible = !visible;
        ShowHideTestOutputLabel.Text = visible ? CliWebContent.EditNodePlugin_LinkButton_ShowTestDetails : CliWebContent.EditNodePlugin_LinkButton_HideTestDetails;
        ShowHideTestOutputArrow.Src = visible ? "/Orion/CLI/images/arrowright.gif" : "/Orion/CLI/images/arrowdown.gif";
    }

    protected void ddlAdvCiscoAsaMonitoring_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool advCiscoAsaMonitoringEnabled = (ddlAdvCiscoAsaMonitoring.SelectedValue.Equals("Keep Unchanged") ||
                                             ddlAdvCiscoAsaMonitoring.SelectedValue.Equals("Yes"));
        this.AdvancedCiscoAsaMonitoringEnabled = advCiscoAsaMonitoringEnabled;

        AdvCiscoAsaMonitoringBox.Visible = cbMultipleSelection.Checked && advCiscoAsaMonitoringEnabled;
    }

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        bool advCiscoAsaMonitoringEnabled = (ddlAdvCiscoAsaMonitoring.SelectedValue.Equals("Keep Unchanged") ||
                                             ddlAdvCiscoAsaMonitoring.SelectedValue.Equals("Yes"));

        AdvCiscoAsaMonitoringBox.Visible = cbMultipleSelection.Checked && advCiscoAsaMonitoringEnabled;
        ddlAdvCiscoAsaMonitoring.Enabled = cbMultipleSelection.Checked;
    }

    protected void cbAdvCiscoAsaMonitoring_CheckedChanged(object sender, EventArgs e)
    {
        AdvCiscoAsaMonitoringBox.Visible = cbMultipleSelection.Checked && cbAdvCiscoAsaMonitoring.Checked;
        this.AdvancedCiscoAsaMonitoringEnabled = cbAdvCiscoAsaMonitoring.Checked;
    }

    private void ShowCliDeviceTemplatePlugin(bool advCiscoAsaMonitoringEnabled)
    {
        var cliDeviceTemplatePlugin =
            NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == "CliDeviceTemplate");
        if (cliDeviceTemplatePlugin == null) return;

        try
        {
            bool manageNodesWithNcmEnabled = true;
            var ncmPlugin = NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == "NCMNodeProperties");
            object value;
            if (ncmPlugin != null && ncmPlugin.State.TryGetValue("Ncm_ManageNodesWithNcmEnabled", out value))
            {
                manageNodesWithNcmEnabled = bool.Parse(value.ToString());
            }

            cliDeviceTemplatePlugin.Visible = advCiscoAsaMonitoringEnabled || manageNodesWithNcmEnabled;
        }
        catch (Exception)
        {
            cliDeviceTemplatePlugin.Visible = true;
        }
    }

    private List<Node> GetNodes(string guid)
    {
        var nodes = new List<Node>();
        using (var proxy = CoreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            if (NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count > 0)
            {
                foreach (int nodeId in NodeWorkflowHelper.GetMultiSelectedNodeIds(guid))
                {
                    int engineId = GetEngineIdForNodeId(proxy, nodeId);
                    Node node = GetNodeById(nodeId, engineId);
                    nodes.Add(node);
                }
            }
            else if (NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid).Count > 0)
            {
                foreach (string coreNodeIdWithEngineId in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid))
                {
                    int nodeId = Int32.Parse(coreNodeIdWithEngineId.Split(':')[1]);
                    int engineId = Int32.Parse(coreNodeIdWithEngineId.Split(':')[2]);
                    Node node = GetNodeById(nodeId, engineId);
                    nodes.Add(node);
                }
            }
        }

        return nodes;
    }

    private int GetEngineIdForNodeId(ICoreBusinessLayer proxy, int nodeId)
    {
        var engineId = proxy.GetEngineIdForNetObject($"N:{nodeId}");
        return engineId;
    }

    private Node GetNodeById(int nodeId, int engineId)
    {
        using (var coreProxy = CoreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler, engineId, true))
        {
            return coreProxy.GetNodeWithOptions(nodeId, false, false);
        }
    }

    private bool SNMPQuery(Node node, string oid, string snmpGetType, out Dictionary<string, string> response)
    {
        try
        {
            using (var coreProxy = CoreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID))
            {
                coreProxy.NodeSNMPQuery(node, oid, snmpGetType, out response);
                if (response != null && response.ContainsKey(sysObjectID) && response.ContainsKey(sysDescr))
                {
                    return true;
                }

                return false;
            }
        }
        catch
        {
            response = null;
            return false;
        }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        throw new Exception("BusinessLayerException:Cli Advanced Cisco ASA Monitoring");
    }
}