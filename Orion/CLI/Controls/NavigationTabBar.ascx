<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationTabBar.ascx.cs" Inherits="Orion_CLI_Controls_NavigationTabBar" %>

<%
    this.AddTab(Resources.CliWebContent.Navigation_TabBar_TemplatesManagement, VirtualPathUtility.ToAbsolute("~/Orion/CLI/Admin/DeviceTemplate/DeviceTemplatesManagement.aspx"), IsPageRegexMatch("(^|/)(DeviceTemplatesManagement.aspx)$"), true, !IsPageRegexMatch("(^|/)(DeviceTemplatesManagement)"));
    this.AddTab(Resources.CliWebContent.Navigation_TabBar_SharedTemplatesOnThwack, VirtualPathUtility.ToAbsolute("~/Orion/NCM/Admin/DeviceTemplate/Thwack/ThwackDeviceTemplates.aspx"), IsPageRegexMatch("(^|/)(Thwack/ThwackDeviceTemplates.aspx)$"), true, !IsPageRegexMatch("(^|/)(DeviceTemplatesManagement)"));

    this.AddTab(Resources.CliWebContent.Navigation_TabBar_TemplatesManagement, VirtualPathUtility.ToAbsolute("~/Orion/CLI/Admin/DeviceTemplate/DeviceTemplatesManagement.aspx"), IsPageRegexMatch("(^|/)(DeviceTemplatesManagement.aspx)$"), true, !IsPageRegexMatch("(^|/)(ThwackDeviceTemplates)"));
    this.AddTab(Resources.CliWebContent.Navigation_TabBar_SharedTemplatesOnThwack, VirtualPathUtility.ToAbsolute("~/Orion/NCM/Admin/DeviceTemplate/Thwack/ThwackDeviceTemplates.aspx"), IsPageRegexMatch("(^|/)(Thwack/ThwackDeviceTemplates.aspx)$"), true, !IsPageRegexMatch("(^|/)(ThwackDeviceTemplates)"));

%>

<style type="text/css">
.sw-tabs .x-tab-strip-text { font-size: 13px !important; }
.tab-top 
{
    border: solid 1px #DFDFDE;
    border-top:medium none !important;
    padding:1em;
    background:white url('/Orion/CLI/images/tab.top.gif') repeat-x scroll left top !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $(".sw-tabs li.sw-tab").bind('mouseover', function () { $(this).addClass('x-tab-strip-over'); });
        $(".sw-tabs li.sw-tab").bind('mouseout', function () { $(this).removeClass('x-tab-strip-over'); });
    });
</script>

<div class="sw-tabs">
    <div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">
        <div class="x-tab-strip-wrap">

            <ul class="x-tab-strip x-tab-strip-top" style="width:100%;">

                <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />

                <li class="x-tab-edge"></li>
                <div class="x-clear"><!-- --></div>
            </ul>
        </div>
    </div>
</div>