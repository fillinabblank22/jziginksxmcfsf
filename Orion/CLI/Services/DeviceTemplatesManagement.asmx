﻿<%@ WebService Language="C#" Class="DeviceTemplatesManagement" %>

using System;
using System.Xml;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Web.Script.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Cli.Web;
using SolarWinds.Orion.Cli.Contracts;
using SolarWinds.Orion.Cli.Contracts.DataModel;
using Newtonsoft.Json;
using SolarWinds.Coding.Utils.Logger;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class DeviceTemplatesManagement : WebService
{
    private static readonly string CLI_DEVICE_TEMPLATES_SESSION_KEY = "Cli_Device_Templates";
    private readonly ILogger log;
    private readonly IDeviceTemplatesManager deviceTemplatesManager;

    public DeviceTemplatesManagement()
    {
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        deviceTemplatesManager = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDeviceTemplatesPaged(string searchTerm)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        int totalRows;
        DataTable pageData = GetDeviceTemplatesPaged(sortColumn, sortDirection, searchTerm, startRowNumber, startRowNumber + pageSize, out totalRows);
        return new PageableDataTable(pageData, totalRows);
    }

    [WebMethod(EnableSession = true)]
    public string VerifyTemplateXml(string xml)
    {
        try
        {
            var xmlDoc = new XmlDocument();

            xmlDoc.XmlResolver = null; //NCM-4365 - prevent XXE vulnerability

            xmlDoc.LoadXml(xml);
            var node = xmlDoc.SelectSingleNode("//Commands");
            if (node == null)
            {
                return JsonConvert.SerializeObject(new
                {
                    Msg = "XML content doesn't have tag \"Command\".",
                    Error = true
                });
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in VerifyTemplateXml", ex);
            return JsonConvert.SerializeObject(new
            {
                Msg = ex.Message,
                Error = true
            });
        }
    }

    [WebMethod(EnableSession = true)]
    public int AddDeviceTemplate(string name, string systemOid, string systemDescriptionRegex, bool useForAutoDetect, int autoDetectType, string comments, string templateXml)
    {
        try
        {
            var template = new DeviceTemplate
            {
                Name = name,
                SystemOID = systemOid,
                SystemDescriptionRegex = systemDescriptionRegex,
                UseForAutoDetect = useForAutoDetect,
                AutoDetectType = (eAutoDetectType) autoDetectType,
                Comments = comments,
                TemplateXml = templateXml,
                Author = HttpContext.Current.Profile.UserName
            };
            var id = deviceTemplatesManager.AddDeviceTemplate(template);

            ClearSession();

            return id;
        }
        catch (Exception ex)
        {
            log.Error("Error in AddDeviceTemplate", ex);
            return 0;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool UpdateDeviceTemplate(int id, string name, string systemOid, string systemDescriptionRegex, bool useForAutoDetect, int autoDetectType, string comments, string templateXml)
    {
        try
        {
            var template = deviceTemplatesManager.GetDeviceTemplate(id);
            template.Name = name;
            template.SystemOID = systemOid;
            template.SystemDescriptionRegex = systemDescriptionRegex;
            template.UseForAutoDetect = useForAutoDetect;
            template.AutoDetectType = (eAutoDetectType)autoDetectType;
            template.Comments = comments;
            template.TemplateXml = templateXml;
            template.Author = HttpContext.Current.Profile.UserName;
            deviceTemplatesManager.UpdateDeviceTemplate(template);

            ClearSession();

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error in UpdateDeviceTemplate", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public int DuplicateDeviceTemplate(int id, string copyTemplateName, string copySystemOID)
    {
        try
        {
            var template = deviceTemplatesManager.GetDeviceTemplate(id);
            template.Name = copyTemplateName;
            template.SystemOID = copySystemOID;
            template.Author = HttpContext.Current.Profile.UserName;
            template.IsDefault = false;
            int newId = deviceTemplatesManager.AddDeviceTemplate(template);

            ClearSession();

            return newId;
        }
        catch (Exception ex)
        {
            log.Error("Error in DuplicateDefaultTemplate", ex);
            return 0;
        }
    }

    [WebMethod(EnableSession = true)]
    public DeviceTemplate GetDeviceTemplate(int id)
    {
        try
        {
            return deviceTemplatesManager.GetDeviceTemplate(id);
        }
        catch (Exception ex)
        {
            log.Error("Error in GetDeviceTemplate", ex);
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public string DeleteDeviceTemplates(int[] ids)
    {
        try
        {
            deviceTemplatesManager.DeleteDeviceTemplates(ids.ToList());

            ClearSession();

            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in DeleteDeviceTemplates", ex);
            return JsonConvert.SerializeObject(new
            {
                Msg = ex.Message,
                Error = true
            });
        }
    }

    [WebMethod(EnableSession = true)]
    public string EnableDisableAutoDetect(int[] ids, bool enable)
    {
        try
        {
            foreach (var id in ids)
            {
                var template = deviceTemplatesManager.GetDeviceTemplate(id);
                template.UseForAutoDetect = enable;
                deviceTemplatesManager.UpdateDeviceTemplate(template);
            }

            ClearSession();

            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in EnableDisableAutoDetect", ex);
            return JsonConvert.SerializeObject(new
            {
                Msg = ex.Message,
                Error = true
            });
        }
    }

    private DataTable GetDeviceTemplatesPaged(string sortColumn, string sortDirection, string searchTerm, int startIndex, int endIndex, out int totalRows)
    {
        DataTable dt = new DataTable() { Locale = CultureInfo.InvariantCulture };
        dt.Columns.Add("ID", typeof(int));
        dt.Columns.Add("TemplateName", typeof(string));
        dt.Columns.Add("SystemOID", typeof(string));
        dt.Columns.Add("UseForAutoDetect", typeof(bool));
        dt.Columns.Add("Comments", typeof(string));
        dt.Columns.Add("IsDefault", typeof(bool));
        dt.Columns.Add("Author", typeof(string));

        var templates = GetDeviceTemplates();

        if (!string.IsNullOrEmpty(searchTerm))
        {
            templates = templates.Where(t => t.Name.IndexOf(searchTerm, StringComparison.InvariantCultureIgnoreCase) >= 0 || t.SystemOID.IndexOf(searchTerm, StringComparison.InvariantCultureIgnoreCase) >= 0).ToList();
        }

        Comparison<DeviceTemplate> compareMethod = SortByTemplateName;
        if (String.Equals(sortColumn, "TemplateName", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByTemplateName;
        else if (String.Equals(sortColumn, "SystemOID", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortBySystemOID;
        else if (String.Equals(sortColumn, "UseForAutoDetect", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByUseForAutoDetect;
        else if (String.Equals(sortColumn, "Comments", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByComments;
        else if (String.Equals(sortColumn, "Author", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByAuthor;

        templates.Sort(compareMethod);

        if (String.Equals(sortDirection, "desc", StringComparison.OrdinalIgnoreCase))
        {
            templates.Reverse();
        }

        endIndex = Math.Min(endIndex, templates.Count);

        for (int i = startIndex; i < endIndex; i++)
        {
            var template = templates[i];
            dt.Rows.Add(
                template.Id,
                template.Name,
                template.SystemOID,
                template.UseForAutoDetect,
                template.Comments,
                template.IsDefault,
                template.Author);
        }

        totalRows = templates.Count;

        return dt;
    }

    private List<DeviceTemplate> GetDeviceTemplates()
    {
        List<DeviceTemplate> list = Session[CLI_DEVICE_TEMPLATES_SESSION_KEY] as List<DeviceTemplate>;
        if (list != null)
            return list;

        list = deviceTemplatesManager.GetDeviceTemplates();

        Session[CLI_DEVICE_TEMPLATES_SESSION_KEY] = list;

        return list;
    }

    private void ClearSession()
    {
        Session.Remove(CLI_DEVICE_TEMPLATES_SESSION_KEY);
    }

    private int SortByTemplateName(DeviceTemplate x, DeviceTemplate y)
    {
        return (x.Name ?? string.Empty).CompareTo(y.Name ?? string.Empty);
    }

    private int SortBySystemOID(DeviceTemplate x, DeviceTemplate y)
    {
        return (x.SystemOID ?? string.Empty).CompareTo(y.SystemOID ?? string.Empty);
    }

    private int SortByUseForAutoDetect(DeviceTemplate x, DeviceTemplate y)
    {
        return x.UseForAutoDetect.CompareTo(y.UseForAutoDetect);
    }

    private int SortByComments(DeviceTemplate x, DeviceTemplate y)
    {
        return (x.Comments ?? string.Empty).CompareTo(y.Comments ?? string.Empty);
    }

    private int SortByAuthor(DeviceTemplate x, DeviceTemplate y)
    {
        return (x.Author ?? string.Empty).CompareTo(y.Author ?? string.Empty);
    }
}