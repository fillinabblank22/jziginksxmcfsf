﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="CLISettings.aspx.cs" Inherits="Orion_CLI_Admin_CLISettings" %>

<%@ Register Src="~/Orion/Controls/Slider.ascx" TagPrefix="orion" TagName="Slider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptManagerPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" File="Admin.css" />
    <orion:Include ID="Include2" runat="server" File="Discovery.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/CLI/styles/Admin.css" />
    <style type="text/css" id="antiClickjack">body{display:none !important;}</style>
    <style type="text/css">
        tr.description > td {
            text-align: left;
        }

        tr.slider > td {
            text-align: left;
            white-space: nowrap;
            padding-right: 20px;
            padding: 10px;
        }
    </style>
    
    <script type="text/javascript">
        if (self === top) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
        } else {
            top.location = self.location;
        }
    </script>
    <script type="text/javascript">
        var validateTelnetSshSettings = function (sender, args) {
            var connTimeout = parseInt($('input[id*="_slTelnetConnectionTimeout"]').val());
            var promptTimeout = parseInt($('input[id*="_slTelnetPromptTimeout"]').val());
            args.IsValid = (connTimeout > promptTimeout);
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td style="padding-top:10px;">
                <div class="titleTable">
                    <h1><%=Page.Title%></h1>
                </div>
            </td>
            <td style="padding-top:10px;vertical-align:top;">
                <div class="dateArea">
                </div>
            </td>
        </tr>
		<tr>   
            <td colspan="2" style="padding:10px;">
                <div class="SettingContainer">
                    <div class="SettingBlockHeader"> <%=Resources.CliWebContent.CliSettings_LableTraceFileHeader %></div>
                    <div class="SettingBlockDescription">
                        <%=Resources.CliWebContent.CliSettings_Label_TraceFileDescription %> 
                    </div>
                    <div style="padding-top:20px;">
                        <b><asp:CheckBox ID="chkEnableSessionTracing" runat="server" Text="<%$ Resources: CliWebContent, CliSettings_Checkbox_EnableSessionTracing %>" /></b>
                    </div>
                    <div style="padding-top: 20px;">
                        <%=Resources.CliWebContent.CliSettings_TraceFileLocation %> "<asp:Label ID="lSessionTracePath" runat="server"></asp:Label>"
                    </div>
                
                    <div class="SettingBlockHeader" style="padding-top:50px;"> <%=Resources.CliWebContent.CliSettings_Lable_TelnetSSHSettings %> </div>
                    <div>
                        <table cellpadding="0" cellspacing="0">
                            <tr class="description">
                                <td colspan="4" style="padding-top:5px;padding-bottom:10px;">
                                    <%=Resources.CliWebContent.CliSettings_Label_ConnectionTimeoutsDescription %>  
                                </td>
                            </tr>
                            <tr class="slider" style="background-color:#e4f1f8;padding:10px;">
                                <orion:Slider runat="server" ID="slTelnetConnectionTimeout" TableCellCount="3"
                                    RangeFrom="5" RangeTo="3600" Unit="<%$ Resources: CliWebContent, CliSettings_SliderUnits_Seconds %>"
                                    ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: CliWebContent, CliSettings_ValidationError_TimeoutValues %>" />
                                <td>
                                    <span class="SettingNote"> <%=Resources.CliWebContent.CliSettings_Label_ConnectionTimeoutDefaulValue %>  </span>
                                </td>
                            </tr>
                            <tr class="description">
                                <td colspan="4" style="padding-top:40px; padding-bottom:10px;">
                                    <%=Resources.CliWebContent.CliSettings_Label_PromptTimeoutsDescription %> 
                                </td>
                            </tr>
                            <tr class="slider" style="background-color:#e4f1f8;padding:10px;">
                                <orion:Slider runat="server" ID="slTelnetPromptTimeout" TableCellCount="3"
                                    RangeFrom="5" RangeTo="3600" Unit="<%$ Resources: CliWebContent, CliSettings_SliderUnits_Seconds %>"
                                    ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: CliWebContent, CliSettings_ValidationError_TimeoutValues %>" />
                                <td>
                                    <span class="SettingNote"> <%=Resources.CliWebContent.CliSettings_Label_PromptTimeoutDefaulValue %></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:CustomValidator ID="cvTelnetSshSettings" runat="server" ClientIDMode="Static" ForeColor="Red" Display="Dynamic" ClientValidationFunction="validateTelnetSshSettings" ErrorMessage="<%$ Resources: CliWebContent, CliSettings_ValidationError_BadTimeoutValues %>"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr class="description">
                                <td colspan="4" style="padding-top:40px; padding-bottom:10px;">
                                    <%=Resources.CliWebContent.CliSettings_Label_LoginAttemptsDescription %> 
                                </td>
                            </tr>
                            <tr class="slider" style="background-color:#e4f1f8;padding:10px;">
                                <orion:Slider runat="server" ID="slLoginAttemptsCount" TableCellCount="3"
                                    RangeFrom="1" RangeTo="5" Unit="<%$ Resources: CliWebContent, CliSettings_SliderUnits_Attempts %>"
                                    ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: CliWebContent, CliSettings_ValidationError_LoginAttemptsValue %>" />
                                <td>
                                    <span class="SettingNote"> <%=Resources.CliWebContent.CliSettings_Label_LoginAttemptsDefaultValue %> </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div style="padding-top:20px;">
                    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="btnSubmit_Click" DisplayType="Primary" CausesValidation="true" />
                    <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>