﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="ImportDeviceTemplate.aspx.cs" Inherits="Orion_CLI_Admin_DeviceTemplate_ImportDeviceTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptManagerPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td style="padding-top:10px;">
                <div class="titleTable">
                    <h1><%=Page.Title%></h1>
                </div>
            </td>
        </tr>
		<tr>
			<td style="padding:10px;">
                <div>
                    <br />
                    <asp:FileUpload ID="uploadDeviceTemplate" runat="server" Width="50em" size="100" />
                    <br />
                    <br />
                    <asp:CustomValidator ID="validatorDeviceTemplateFile" runat="server" Display="Dynamic" Font-Size="9pt" />
                    <br />
                    <br />
                    <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="submit" DisplayType="Primary" OnClick="SubmitClick" />
                    <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" CausesValidation="false" />        
                </div>
			</td>
		</tr>
	</table>
</asp:Content>


