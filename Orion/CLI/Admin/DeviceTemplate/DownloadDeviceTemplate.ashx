<%@ WebHandler Language="C#" Class="DownloadDeviceTemplate" %>

using System;
using System.Globalization;
using System.IO;
using System.ServiceModel;
using System.Text;
using System.Web;
using SolarWinds.InformationService.Contract;
using SolarWinds.Orion.Cli.Web;
using SolarWinds.Orion.Cli.Contracts;

public class DownloadDeviceTemplate : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
            int id = Convert.ToInt32(context.Request["Id"], CultureInfo.InvariantCulture);
            var template = mng.GetDeviceTemplate(id);
            string xml = mng.ExportTemplate(template);
            string fileName = string.Format("{0}-{1}", template.Name, template.SystemOID);
            var encodedFileName = EncodeFilenameIfNeeded(context, fileName);
            context.Response.Clear();
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.ContentType = "text/ConfigMgmt-Commands";
            context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", encodedFileName + ".ConfigMgmt-Commands"));
            context.Response.Write(xml);
        }
        catch (Exception ex)
        {
            context.Response.Clear();
            if (ex.GetType() == typeof(FaultException<InfoServiceFaultContract>))
                context.Response.Write((ex as FaultException<InfoServiceFaultContract>).Detail.Message);
            else
                context.Response.Write(ex.Message);
        }
        context.ApplicationInstance.CompleteRequest();
    }

    public bool IsReusable
    {
        get { return false; }
    }

    private static string EncodeFilenameIfNeeded(HttpContext context, string filename)
    {
        const int IEMaxFileNameSize = 23;

        string browserType = context.Request.Browser.Type.ToUpperInvariant();
        filename = ReplaceInvalidFileNameChars(filename);

        if (browserType.StartsWith("IE"))
        {
            //Internet Explorer cuts not ANSI file name from the beginning if it is larger than 23 symbols
            //This code cuts file name from the end.
            if (!IsAnsiString(filename) && (filename.Length > IEMaxFileNameSize))
                filename = filename.Substring(0, IEMaxFileNameSize);

            return HttpUtility.UrlPathEncode(filename);
        }

        return filename;
    }

    private static bool IsAnsiString(string str)
    {
        const char compareCharacter = '\x7F';
        foreach (char c in str)
        {
            if (c > compareCharacter)
                return false;
        }
        return true;
    }

    private static string ReplaceInvalidFileNameChars(string Filename)
    {
        string newFilename = string.Empty;

        foreach (char ch in Filename)
        {
            if (Array.IndexOf(Path.GetInvalidFileNameChars(), ch) >= 0)
            {
                newFilename += '_';
            }
            else
            {
                newFilename += ch;
            }
        }
        newFilename = newFilename.Replace(";", "_");

        return newFilename;
    }
}