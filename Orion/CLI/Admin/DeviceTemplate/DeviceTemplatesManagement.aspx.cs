﻿using System;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;

public partial class Orion_CLI_Admin_DeviceTemplate_DeviceTemplatesManagement : Page
{
    protected string _thwackUserInfo = "{name:'',pass:'',valid:false}";
    private static readonly string CLI_DEVICE_TEMPLATES_SESSION_KEY = "Cli_Device_Templates";

    protected override void OnInit(EventArgs e)
    {
        if (!OrionConfiguration.IsDemoServer && !(Page is IBypassAccessLimitation) && !Profile.AllowNodeManagement)
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", CliWebContent.Security_ErrorMessage_NeedNodeManagementRights));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = CliWebContent.DeviceTemplateManagement_PageTitle;
        if (!IsPostBack)
            Session.Remove(CLI_DEVICE_TEMPLATES_SESSION_KEY);

        NavigationTabBar.Visible = OrionModuleManager.IsInstalled("NCM");
    }
}