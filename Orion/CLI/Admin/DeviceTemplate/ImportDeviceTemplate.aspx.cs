﻿using System;
using System.Xml;
using System.Web;
using System.IO;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Cli.Web;
using SolarWinds.Orion.Cli.Contracts;
using System.Text.RegularExpressions;
using System.Web.UI;

public partial class Orion_CLI_Admin_DeviceTemplate_ImportDeviceTemplate : Page
{
    private readonly Log _log = new Log();

    protected override void OnInit(EventArgs e)
    {
        if (!OrionConfiguration.IsDemoServer && !(this.Page is IBypassAccessLimitation) && !Profile.AllowNodeManagement)
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", CliWebContent.Security_ErrorMessage_NeedNodeManagementRights));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = CliWebContent.ImportDeviceTemplates_PageTitle;

        if (OrionConfiguration.IsDemoServer)
            Submit.OnClientClick = "demoAction('CLI_DeviceTemplate_Import', this); return false;";
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (OrionConfiguration.IsDemoServer) return;

        if (uploadDeviceTemplate.HasFile)
        {
            try
            {
                string error;
                if (VerifyFileFormat(uploadDeviceTemplate.FileName, out error))
                {
                    using (StreamReader streamReader = new StreamReader(uploadDeviceTemplate.FileContent))
                    {
                        var templateXml = streamReader.ReadToEnd();

                        var name = GetTemplateName(uploadDeviceTemplate.FileName);
                        var systemOid = GetSystemOid(uploadDeviceTemplate.FileName, templateXml);

                        if (VerifyFileName(name, systemOid, out error) && VerifyXmlContent(templateXml, out error))
                        {
                            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
                            mng.ImportTemplate(name, systemOid, templateXml, HttpContext.Current.Profile.UserName);

                            Response.Redirect("DeviceTemplatesManagement.aspx");
                        }
                        else
                        {
                            validatorDeviceTemplateFile.ErrorMessage = error;
                            validatorDeviceTemplateFile.IsValid = false;
                        }
                    }
                }
                else
                {
                    validatorDeviceTemplateFile.ErrorMessage = error;
                    validatorDeviceTemplateFile.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                validatorDeviceTemplateFile.ErrorMessage = ex.Message;
                validatorDeviceTemplateFile.IsValid = false;
            }
        }
        else
        {
            validatorDeviceTemplateFile.ErrorMessage = CliWebContent.ImportDeviceTemplates_ErrorMessage_WrongTemplateFormat;
            validatorDeviceTemplateFile.IsValid = false;
        }
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect("DeviceTemplatesManagement.aspx");
    }

    private bool VerifyXmlContent(string fileContent, out string error)
    {
        try
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.XmlResolver = null; //NCM-4365 - prevent XXE vulnerability
            xmlDoc.LoadXml(fileContent);

            var node = xmlDoc.SelectSingleNode("//Commands");
            if (node == null)
            {
                error = CliWebContent.ImportDeviceTemplates_ErrorMessage_NoCommandTag;
                return false;
            }
            error = string.Empty;
            return true;
        }
        catch (Exception ex)
        {
            _log.Error(ex);
            error = ex.Message;
            return false;
        }
    }

    private bool VerifyFileFormat(string fileName, out string error)
    {
        try
        {
            string extension = Path.GetExtension(fileName);
            if (!extension.Equals(".ConfigMgmt-Commands", StringComparison.InvariantCultureIgnoreCase))
            {
                error = CliWebContent.ImportDeviceTemplates_ErrorMessage_IncorrectFileFormat;
                return false;
            }

            error = string.Empty;
            return true;
        }
        catch (Exception ex)
        {
            _log.Error(ex);
            error = ex.Message;
            return false;
        }
    }

    private bool VerifyFileName(string name, string systemOid, out string error)
    {
        if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(systemOid))
        {
            error = CliWebContent.ImportDeviceTemplates_ErrorMessage_IncorrectSystemOIDFormat;
            return false;
        }

        error = string.Empty;
        return true;
    }

    private string GetSystemOid(string fileName, string fileContent)
    {
        string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
        int index = fileNameWithoutExtension.IndexOf("-1.3.6.1", StringComparison.OrdinalIgnoreCase);
        if (index > 0)
        {
            return fileNameWithoutExtension.Substring(index + 1, fileNameWithoutExtension.Length - index - 1);
        }
        else
        {
            var match = Regex.Match(fileContent, "SystemOID=\"(?<systemOid>(.*?))\"");
            return match.Success ? match.Groups["systemOid"].Value : string.Empty;
        }
    }

    private string GetTemplateName(string fileName)
    {
        string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
        int index = fileNameWithoutExtension.IndexOf("-1.3.6.1", StringComparison.OrdinalIgnoreCase);
        if (index > 0)
        {
            return fileNameWithoutExtension.Substring(0, index);
        }
        return fileNameWithoutExtension;
    }
}
