﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="DeviceTemplatesManagement.aspx.cs" Inherits="Orion_CLI_Admin_DeviceTemplate_DeviceTemplatesManagement" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Register Src="~/Orion/CLI/Controls/NavigationTabBar.ascx" TagPrefix="cli" TagName="NavigationTabBar" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptManagerPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/CLI/js/DeviceTemplatesManagement.js" ></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>

    <script type="text/javascript">
        var thwackUserInfo = <%=_thwackUserInfo%>;
        var IsDemoMode = <%=OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
        var IsNcmInstalled = <%=OrionModuleManager.IsInstalled("NCM").ToString().ToLowerInvariant() %>;
    </script>

    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/CLI/styles/DeviceTemplatesManagement.css" />

    <style type="text/css">
        #mainGrid td{ 
            padding: 0px 0px 0px 0px;
            border: 0px;
        }
        .toggleOn, .toggleOff {
            width: initial;
            min-width: 25px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td style="padding-top:10px;">
                <div class="titleTable">
                    <h1><%=Page.Title%></h1>
                </div>
            </td>
            <td style="padding-top:10px;vertical-align:top;">
                <div class="dateArea">
                    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionNCMPHDeviceTemplatesManagement" />
                </div>
            </td>
        </tr>
		<tr>
			<td colspan="2" style="padding:10px;">
                <input type="hidden" name="CLI_DeviceTemplatesManagement_PageSize" id="CLI_DeviceTemplatesManagement_PageSize" value='<%=WebUserSettingsDAL.Get("CLI_DeviceTemplatesManagement_PageSize")%>' />
                <cli:NavigationTabBar ID="NavigationTabBar" runat="server"></cli:NavigationTabBar>
                <div id="tabPanel" class="tab-top">
				    <table id="gridContent" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td id="gridCell">
                                <div id="mainGrid">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
			</td>
		</tr>
	</table>

    <div id="createEditDeviceTemplateDialog" class="x-hidden">
        <div id="createEditDeviceTemplateDialogBody" class="x-panel-body" style="padding:10px;height:100%;overflow:hidden;">
            <div>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="white-space:nowrap;padding-right:5px;padding-bottom:5px;">
                            <%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_TemplateName %> 
                        </td>
                        <td style="padding-bottom:5px;">
                            <asp:TextBox ID="txtTemplateName" ClientIDMode="Static" runat="server" Width="300px" Height="18px" placeholder="<%$ Resources: CliWebContent, DeviceTemplateManagement_EditBoxEmptyText_TypeTemplateName %>"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTemplateName" ClientIDMode="Static" ControlToValidate="txtTemplateName" Display="Dynamic" runat="server" ErrorMessage="<%$ Resources: CliWebContent, DeviceTemplateManagement_ValidationError_TemplateNameRequired %>" ValidationGroup="CreateEditTemplateValidationGroup"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap;padding-right:5px;padding-bottom:5px;">
                            <%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_SystemOID %> 
                        </td>
                        <td style="padding-bottom:5px">
                            <asp:TextBox id="txtSystemOID" ClientIDMode="Static" runat="server" Width="300px" Height="18px" placeholder="<%$ Resources: CliWebContent, DeviceTemplateManagement_EditBoxEmptyText_TypeOID %>"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvSystemOID" ClientIDMode="Static" ControlToValidate="txtSystemOID" Display="Dynamic" runat="server" ErrorMessage="<%$ Resources: CliWebContent, DeviceTemplateManagement_ValidationError_SysOIDRequired %>" ValidationGroup="CreateEditTemplateValidationGroup"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revSystemOID" ClientIDMode="Static" ControlToValidate="txtSystemOID" Display="Dynamic" runat="server" ErrorMessage="<%$ Resources: CliWebContent, DeviceTemplateManagement_ValidationError_IncorrectOIDFormat %>" ValidationExpression="\d([.]\d{1,9}){4,}" ValidationGroup="CreateEditTemplateValidationGroup"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap;padding-right:5px;padding-bottom:5px;">
                            <%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_DescriptionPattern %> 
                        </td>
                        <td style="padding-bottom:5px">
                            <asp:TextBox id="txtSystemDescriptionRegex" ClientIDMode="Static" runat="server" Width="300px" Height="18px" placeholder="<%$ Resources: CliWebContent, DeviceTemplateManagement_EditBoxEmptyText_TypeDescription %>"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvSystemDescriptionRegex" ClientIDMode="Static" ControlToValidate="txtSystemDescriptionRegex" Display="Dynamic" runat="server" ErrorMessage="<%$ Resources: CliWebContent, DeviceTemplateManagement_ValidationError_DescriptionPatternRequired %>" ValidationGroup="CreateEditTemplateValidationGroup"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap;padding-right:5px;padding-bottom:5px;">
                            <%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_UseForAutoDetect %> 
                        </td>
                        <td style="padding-bottom:5px;">
                            <asp:DropDownList ID="ddlUseForAutoDetect" runat="server" ClientIDMode="Static" onchange="useForAutoDetectChange(this.value);">
                                <asp:ListItem Text="<%$ Resources: CliWebContent, DeviceTemplateManagement_DialogDropDown_Yes %>" Value="true"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: CliWebContent, DeviceTemplateManagement_DialogDropDown_No %>" Value="false"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="autoDetectTypeHolder">
                        <td style="white-space:nowrap;padding-right:5px;padding-bottom:5px;">
                            &nbsp;
                        </td>
                        <td style="padding-bottom:5px;">
                            <asp:RadioButtonList runat="server" ID="rbAutoDetectType" ClientIDMode="Static">
                                <asp:ListItem Text="<%$ Resources: CliWebContent, DeviceTemplateManagement_DialogDropDown_MatchByOID %>" Value="0"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: CliWebContent, DeviceTemplateManagement_DialogDropDown_MatchByDescription %>" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdComments" style="white-space:nowrap;padding-right:5px;padding-bottom:5px;vertical-align:top;">
                            <%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_Comments %> 
                        </td>
                        <td style="padding-bottom:5px;">
                           <textarea style="height:50px;width:100%;" id="txtComments" class="x-form-textarea x-form-field"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdTemplateXml" colspan="2" style="white-space:nowrap;">
                            <%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_TemplateXml %> 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea id="txtTemplateXml" class="x-form-textarea x-form-field"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <div id="DuplicateDefaultTemplateDialog" class="x-hidden">
        <div id="DuplicateDefaultTemplateDialogBody" class="x-panel-body" style="padding:10px;height:100%;overflow:hidden;">
            <div>
                <span>
                    <%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_CannotEdit %> 
                </span>
                <br/>
                <br/>
                <span><%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_NameOfOriginalTemplate %> </span>
                <br/>
                <asp:Label ID="lblOriginTemplateName" ClientIDMode="Static" Text="test" runat="server" Font-Bold="True"></asp:Label>
            </div>
            <div style="padding-top:10px">
                <span><%=Resources.CliWebContent.DeviceTemplateManagement_DialogLabel_NewTemplateName %></span>
                <br/>
                <asp:TextBox id="txtDuplicateTemplateName" ClientIDMode="Static" runat="server" Width="200px" Height="18px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCopyTemplateName" ControlToValidate="txtDuplicateTemplateName" Display="Dynamic" runat="server" ErrorMessage="<%$ Resources: CliWebContent, DeviceTemplateManagement_ValidationError_TemplateNameRequired %>" ValidationGroup="DuplicateTemplateValidationGroup"></asp:RequiredFieldValidator>
            </div>
            <div style="padding-top:10px">
                <span><%=Resources.CliWebContent.DeviceTemplateManagement_Label_NewOID %></span><br/>
                <asp:TextBox id="txtDuplicateSystemOID" ClientIDMode="Static" runat="server" Width="200px" Height="18px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvDuplicateSystemOID" ControlToValidate="txtDuplicateSystemOID" Display="Dynamic" runat="server" ErrorMessage="<%$ Resources: CliWebContent, DeviceTemplateManagement_ValidationError_SysOIDRequired %>" ValidationGroup="DuplicateTemplateValidationGroup"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revDuplicateSystemOID" ControlToValidate="txtDuplicateSystemOID" Display="Dynamic" runat="server" ErrorMessage="<%$ Resources: CliWebContent, DeviceTemplateManagement_ValidationError_IncorrectOIDFormat %>" ValidationExpression="\d([.]\d{1,9}){4,}" ValidationGroup="DuplicateTemplateValidationGroup"></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>
</asp:Content>