﻿using System;
using System.Web;
using System.Web.UI;
using Resources;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Cli.Web;
using SolarWinds.Orion.Cli.Contracts;
using SolarWinds.Orion.Cli.Contracts.DataModel;

public partial class Orion_CLI_Admin_CLISettings : Page
{
    private readonly ICliDbSettingsDal cliSettingsDal;
    private readonly ILogger logger;

    public Orion_CLI_Admin_CLISettings()
    {
        cliSettingsDal = ServiceLocator.Container.Resolve<ICliDbSettingsDal>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    protected override void OnInit(EventArgs e)
    {
        try
        {
            if (!OrionConfiguration.IsDemoServer && !(Page is IBypassAccessLimitation) && !Profile.AllowAdmin)
            {
                Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", CliWebContent.Security_ErrorMessage_PageAccessDenied));
            }

            if (OrionConfiguration.IsDemoServer)
            {
                btnSubmit.OnClientClick = "demoAction('{0}', this); return false;";
            }
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the CLI settings page.", ex);
            throw;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.Title = CliWebContent.CliSettings_PageTitle;

            slTelnetConnectionTimeout.Label = string.Format("<b>{0}</b>", CliWebContent.CliSettings_Label_TelnetSSHConnectionTimeout);
            slTelnetPromptTimeout.Label = string.Format("<b>{0}</b>", CliWebContent.CliSettings_Label_TelnetSSHPromptTimeout);
            slLoginAttemptsCount.Label = string.Format("<b>{0}</b>", CliWebContent.CliSettings_Label_LoginAttempts);
            if (!Page.IsPostBack)
            {
                InitSettings();
            }
        }
        catch (Exception ex)
        {
            logger.Error("Error loading the CLI settings page.", ex);
            throw;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            SaveSettings();

            Response.Redirect("/Orion/Admin/");
        }
        catch (Exception ex)
        {
            logger.Error("Error submitting.", ex);
            throw;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("/Orion/Admin/");
        }
        catch (Exception ex)
        {
            logger.Error("Error cancelling.", ex);
            throw;
        }
    }

    private void InitSettings()
    {
        var sessionTraceEnabled = cliSettingsDal.GetDbSetting(CliSettings.SessionTraceEnabledSettingKey, false);
        chkEnableSessionTracing.Checked = sessionTraceEnabled;

        var sessionTracePath = cliSettingsDal.GetDbSetting(CliSettings.SessionTracePathSettingKey, @"%ALLUSERSPROFILE%\Application Data\SolarWinds\Logs\Orion\CLI\Session-Trace");
        lSessionTracePath.Text = HttpUtility.HtmlEncode(sessionTracePath);

        var connectionTimeout = cliSettingsDal.GetDbSetting(CliSettings.ConnectionTimeoutSettingKey, 45);
        slTelnetConnectionTimeout.Value = connectionTimeout;

        var promptTimeout = cliSettingsDal.GetDbSetting(CliSettings.PromptTimeoutSettingKey, 15);
        slTelnetPromptTimeout.Value = promptTimeout;

        var loginAttemptsCount = cliSettingsDal.GetDbSetting(CliSettings.LoginAttemptsCountSettingKey, 1);
        slLoginAttemptsCount.Value = loginAttemptsCount;
    }

    private void SaveSettings()
    {
        var sessionTraceEnabled = chkEnableSessionTracing.Checked;
        cliSettingsDal.SetDbSetting(CliSettings.SessionTraceEnabledSettingKey, sessionTraceEnabled);

        var connectionTimeout = slTelnetConnectionTimeout.Value;
        cliSettingsDal.SetDbSetting(CliSettings.ConnectionTimeoutSettingKey, connectionTimeout);

        var promptTimeout = slTelnetPromptTimeout.Value;
        cliSettingsDal.SetDbSetting(CliSettings.PromptTimeoutSettingKey, promptTimeout);

        var loginAttemptsCount = slLoginAttemptsCount.Value;
        cliSettingsDal.SetDbSetting(CliSettings.LoginAttemptsCountSettingKey, loginAttemptsCount);
    }
}