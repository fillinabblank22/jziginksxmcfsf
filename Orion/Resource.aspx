﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Resource.aspx.cs" Inherits="Orion_Resource" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="orion" TagName="Reposter" Src="~/Orion/Controls/Reposter.ascx" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <orion:Include File="Resources.css" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <form id="form1" runat="server" class="sw-app-region">
        <asp:ScriptManager id="ScriptManager1" runat="server" />
    
        <orion:Reposter ID="Reposter1" runat="server" />
        <div runat="server" id="divContainer">
        </div>
    </form>
</asp:Content>
