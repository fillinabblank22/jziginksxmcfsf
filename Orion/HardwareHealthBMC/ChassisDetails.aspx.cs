﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.HardwareHealth.BMC.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Shared;

namespace Orion.HardwareHealthBMC
{
    public partial class ChassisDetails : OrionView, IChassisProvider
    {
        private const string DetailsLink = "<a href=\"{0}\">{1}</a>";
        private const string StatusIcon = "<img src=\"/Orion/images/StatusIcons/Small-{0}.gif\">";

        public string PageHeader { get; private set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
               
            if (ChassisNetObject == null)
                throw new IndexOutOfRangeException(string.Format(HwHBMCWebContent.ChassisNotFoundErrorMessage));

            Title = string.Format("{0} - {1}", ViewInfo.ViewTitle, ChassisNetObject.Name);
            PageHeader = string.Format("{0} - {1} {2} on {3}",
                ViewInfo.ViewTitle,
                string.Format(StatusIcon, StatusInfo.GetStatus(MapStatusIcon(ChassisNetObject.Data.Status ?? 0))),
                string.Format(DetailsLink, ChassisNetObject.Data.DetailsUrl, ChassisNetObject.Name),
                string.Format(DetailsLink, ChassisNetObject.Data.ControllerDetailsUrl, ChassisNetObject.Data.ControllerName));

            resourceContainer.DataSource = ViewInfo;
            resourceContainer.DataBind();
        }

        public ChassisNetObject ChassisNetObject
        {
            get { return NetObject as ChassisNetObject; }
        }

        public override string ViewType
        {
            get { return "Chassis Details"; }
        }

        protected void SAMSiteMapPath_OnInit(object sender, EventArgs e)
        {
            if (CommonWebHelper.IsBreadcrumbsDisabled)
                return;

            var renderer = new NetObjectsSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("NetObject", Request.QueryString["NetObject"] ?? string.Empty));
            SAMSiteMapPath.SetUpRenderer(renderer);
        }

        private int MapStatusIcon(int status)
        {
            if (status == 17 || status == 27) 
                return 0;

            return status;
        }
    }
}