<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditChassisDetailsView.ascx.cs" Inherits="Orion.HardwareHealthBMC.Admin.EditChassisDetailsView" %>

<asp:ListBox runat="server" ID="lbxChassisDetails" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$Resources:HwHBMCWebContent,ViewSelectionNone%>" Value="0" />
    <asp:ListItem Text="<%$Resources:HwHBMCWebContent,ViewSelectionByDeviceType%>" Value="-1" />
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>