using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

namespace Orion.HardwareHealthBMC.Admin
{
    public partial class EditChassisDetailsView : ProfilePropEditUserControl
    {
        public override string PropertyValue
        {
            get
            {
                return lbxChassisDetails.SelectedValue;
            }
            set
            {
                var item = lbxChassisDetails.Items.FindByValue(value);
                if (null != item)
                {
                    item.Selected = true;
                }
                else
                {
                    // default to "by device type"
                    lbxChassisDetails.Items.FindByValue("-1").Selected = true;
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            foreach (var info in ViewManager.GetViewsByType("Chassis Details"))
            {
                var item = new ListItem(info.ViewTitle, info.ViewID.ToString());
                lbxChassisDetails.Items.Add(item);
            }

            base.OnInit(e);
        }
    }
}
