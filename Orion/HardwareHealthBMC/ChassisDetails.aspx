﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true"
CodeFile="ChassisDetails.aspx.cs" Inherits="Orion.HardwareHealthBMC.ChassisDetails"
Title="Chassis Details"%>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
      { %>
        <br />
        <div style="margin-left: 10px" >
            <orion:FileBasedDropDownMapPath ID="SAMSiteMapPath" runat="server" 
                                            OnInit="SAMSiteMapPath_OnInit"
                                            Provider="HardwareHealthBMCSitemapProvider" 
                                            SiteMapFilePath="/Orion/HardwareHealthBMC/HardwareHealthBMC.sitemap">
                <RootNodeTemplate>
                    <a href="<%# Eval("url") %>" ><u> <%# Eval("title") %></u> </a>
                </RootNodeTemplate>
                <CurrentNodeTemplate>
                    <a href="<%# Eval("url") %>" ><%# Eval("title") %> </a>
                </CurrentNodeTemplate>
            </orion:FileBasedDropDownMapPath>
        </div>
    <%} %>
    <h1>
        <%= PageHeader %>
    </h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <orion:ResourceHostControl ID="resourceHostControl" runat="server">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </orion:ResourceHostControl>
</asp:Content>