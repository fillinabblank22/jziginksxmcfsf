﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BmcControllerCredentials.ascx.cs" Inherits="Orion.HardwareHealthBMC.Controls.BmcControllerCredentials" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>
<div class="contentBlock">
    <div class="contentBlockHeader" runat="server" id="BmcCredentialsHeader">
        <table>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged"/>
                    <asp:Literal runat="server" Text="<%$ Resources:HwHBMCWebContent, ControllerCredentials%>"/>
                </td>
                <td class="rightInputColumn">
                    <asp:CheckBox ID="cbIsBmcNode" runat="server" AutoPostBack="True" OnCheckedChanged="cbIsBmcNode_CheckedChanged" Enabled="false"/>
                    <asp:Literal ID="cbIsBmcDescription" runat="server" Text="<%$ Resources:HwHBMCWebContent,ControllerIsBmcNode%>"/>
                </td>
            </tr>
        </table>
    </div>
    <div class="blueBox" runat="server" id="BmcCredentialsBox">
        <table runat="server" id="myTable">
            <tr>
                <td class="leftLabelColumn">
                    <%= Resources.HwHBMCWebContent.ControllerPort %><%= Resources.HwHBMCWebContent.PunctuationMarkColon %>
                </td>
                <td class="rightInputColumn">

                    <asp:TextBox runat="server" ID="txtPortNumber" Width="250"></asp:TextBox>

                </td>
                <td class="helpfulText">
                    <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources:HwHBMCWebContent,ControllerPortDescription%>"/>
                </td>
            </tr>

            <tr>
                <td class="leftLabelColumn">

                </td>
                <td class="rightInputColumn">

                    <asp:CheckBox runat="server" ID="cbUseSSL"></asp:CheckBox>
                    <%= Resources.HwHBMCWebContent.ControllerUseHttps %>

                </td>
                <td class="helpfulText">

                </td>
            </tr>

            <tr>
                <td class="leftLabelColumn">
                    <%= Resources.HwHBMCWebContent.ControllerUserName %><%= Resources.HwHBMCWebContent.PunctuationMarkColon %>
                </td>
                <td class="rightInputColumn">

                    <asp:TextBox runat="server" ID="txtUserName" Width="250" AutoCompleteType="Disabled"></asp:TextBox>

                </td>
                <td>
                    <asp:Label ID="userNameWarning" runat="server"/>
                </td>
            </tr>

            <tr>
                <td class="leftLabelColumn">
                    <%= Resources.HwHBMCWebContent.ControllerPassword %><%= Resources.HwHBMCWebContent.PunctuationMarkColon %>
                </td>

                <td class="rightInputColumn">

                    <asp:TextBox TextMode="Password" runat="server" ID="txtPassword" Width="250" AutoCompleteType="Disabled"></asp:TextBox>

                </td>
                <td>
                </td>
            </tr>

            <tr>
                <td/>
                <td>
                    <orion:LocalizableButton id="bmcTestButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: HwHBMCWebContent, ControllerTestButton %>" CausesValidation="true" OnClick="TestButton_Click"/>

                    <asp:Label ID="testResultLabel" runat="server"/>
                </td>
                <td>


                </td>
            </tr>
        </table>
        <div runat="server" id="bmcTestSuccess" style="padding-left: 118px; background-color: #D6F6C6; text-align: center" visible="false">
            <img alt="<%$ Resources:HwHBMCWebContent,ControllerTestSuccessfulMessage%>" runat="server" id="BMCValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif"/>
            <%= Resources.HwHBMCWebContent.ControllerTestSuccessfulMessage %>
        </div>
        <div runat="server" id="bmcTestFailure" style="padding-left: 118px; background-color: #FEEE90; text-align: center; color: Red" visible="false">
            <img alt="<%$ Resources:HwHBMCWebContent,ControllerTestFailedMessage%>" runat="server" id="BMCValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif"/>
            <%= Resources.HwHBMCWebContent.ControllerTestFailedMessage %>
        </div>
        <div runat="server" id="multipleCredentialsWarning" style="padding-left: 118px; background-color: #FEEE90; text-align: center; color: Red" visible="false">
            <img alt="<%$ Resources:HwHBMCWebContent,ControllerDifferentCredentails%>" runat="server" id="BMCDiffernetCredentials" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif"/>
            <%= Resources.HwHBMCWebContent.ControllerMultipleChangeCredentialsWarningMessage %>
        </div>
    </div>
</div>