﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using SolarWinds.Orion.Common.Extensions;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.HardwareHealth.BMC.Common;
using SolarWinds.Orion.HardwareHealth.BMC.Common.Models;
using SolarWinds.Orion.Web;

namespace Orion.HardwareHealthBMC.Controls
{
    public partial class BmcControllerCredentials : System.Web.UI.UserControl, INodePropertyPlugin
    {
        //Implementing the extremely cool Add/Edit node stuff
        private Dictionary<string, object> _propertyBag;
        private IList<Node> _nodes;
        private Dictionary<string, BMCConnectionTestResult> _connectionTestResults;
        private NodePropertyPluginExecutionMode _mode;

        private readonly string _validationFailExceptionMessage = Resources.HwHBMCWebContent.CredentailValidationFailedMessage;



        protected enum TestResult
        {
            None,
            Success,
            Failure
        }

        protected string enteredUserName = string.Empty;
        protected SecureString enteredPassword = string.Empty.ToSecureString();
        protected bool enteredSsl = false;
        protected int enteredPort;

        private const string USERNAME_SESSIONID = "BMCControllerCredentials_UserName";
        private const string PASSWORD_SESSIONID = "BMCControllerCredentials_Password";
        private const string SSL_SESSIONID = "BMCControllerCredentials_UseSSL";
        private const string PORT_SESSIONID = "BMCControllerCredentials_Port";
        private const string ISBMC_SESSIONID = "BMCControllerCredentials_IsBmc";

        private const string PASS_CHANGEDID = "BMCControllerCredentials_passwordChanged";

        private const string TESTRESULT_SESSIONID = "BMCControllerCredentials_TestResult";



        #region Properties


        /// <summary>
        /// Defines if plugin "Enable multiple selection" checkbox is visible. Normally property is set from this.Initialize()
        /// </summary>
        public bool MultipleSelectionVisible
        {
            get { return cbMultipleSelection.Visible; }
            set { cbMultipleSelection.Visible = value; }
        }

        /// <summary>
        /// Defines if plugin "Enable" checkbox is visible. Normally property is set from this.Initialize()
        /// </summary>
        public bool IsBmcNode
        {
            get
            {
                if (!_propertyBag.ContainsKey(ISBMC_SESSIONID))
                {
                    _propertyBag[ISBMC_SESSIONID] = false;
                }
                return (bool)_propertyBag[ISBMC_SESSIONID];
            }
            set
            {
                _propertyBag[ISBMC_SESSIONID] = value;
            }
        }


        /// <summary>
        /// Defines if plugin is enabled to edit multiple items. Normally property is set from this.Initialize()
        /// </summary>
        public bool MultipleSelectionSelected
        {
            get { return cbMultipleSelection.Checked; }
            set
            {
                cbMultipleSelection.Checked = value;
                cbMultipleSelection_CheckedChanged(this, EventArgs.Empty);
            }
        }


        public string EnteredUserName
        {
            get
            {
                if (!_propertyBag.ContainsKey(USERNAME_SESSIONID))
                {
                    enteredUserName = String.Empty;
                    _propertyBag[USERNAME_SESSIONID] = enteredUserName;
                }
                return (string)_propertyBag[USERNAME_SESSIONID];
            }
            set
            {
                if (value == null)
                {
                    if (_propertyBag.ContainsKey(USERNAME_SESSIONID))
                    {
                        _propertyBag.Remove(USERNAME_SESSIONID);
                    }
                }
                else
                {
                    enteredUserName = value;
                    _propertyBag[USERNAME_SESSIONID] = value;
                }
            }
        }

        public SecureString EnteredPassword
        {
            get
            {
                if (!_propertyBag.ContainsKey(PASSWORD_SESSIONID) || !(_propertyBag[PASSWORD_SESSIONID] is SecureString))
                {
                    enteredPassword = String.Empty.ToSecureString();
                    _propertyBag[PASSWORD_SESSIONID] = enteredPassword;
                }
                return (SecureString)_propertyBag[PASSWORD_SESSIONID];
            }
            set
            {
                if (value == null)
                {
                    if (_propertyBag.ContainsKey(PASSWORD_SESSIONID))
                    {
                        _propertyBag.Remove(PASSWORD_SESSIONID);
                    }
                }
                else
                {
                    enteredPassword = value;
                    _propertyBag[PASSWORD_SESSIONID] = value;
                }
            }
        }

        public bool EnteredSsl
        {
            get
            {
                if (!_propertyBag.ContainsKey(SSL_SESSIONID))
                {
                    _propertyBag[SSL_SESSIONID] = enteredSsl = false;
                }
                return (bool)_propertyBag[SSL_SESSIONID];
            }
            set
            {
                _propertyBag[SSL_SESSIONID] = enteredSsl = value;
            }
        }

        public int EnteredPort
        {
            get
            {
                if (!_propertyBag.ContainsKey(PORT_SESSIONID))
                {
                    enteredPort = 0;
                    _propertyBag[PORT_SESSIONID] = enteredPort;
                }
                return (int)_propertyBag[PORT_SESSIONID];
            }
            set
            {
                enteredPort = value;
                _propertyBag[PORT_SESSIONID] = value;
            }
        }


        #endregion



        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region Event handlers

        protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
        {
            IsBmcNode = cbIsBmcNode.Checked && cbMultipleSelection.Checked;

            SetControlVisibility(IsBmcNode && MultipleSelectionSelected);
            SetBmcNodeControlEnabled(MultipleSelectionSelected);
        }

        private void SetBmcNodeControlEnabled(bool MultipleSelectionSelected)
        {
            this.cbIsBmcNode.Enabled = MultipleSelectionSelected;
        }

        protected void cbIsBmcNode_CheckedChanged(object sender, EventArgs e)
        {
            IsBmcNode = cbIsBmcNode.Checked && cbMultipleSelection.Checked;
            SetControlVisibility(IsBmcNode && MultipleSelectionSelected);
        }

        protected void TestButton_Click(object sender, EventArgs e)
        {

            ClearTestResultMessage();
            UpdateData();

            ValidateInternal();

            //repopulate the password textbox after test
            if (EnteredPassword != null)
            {
                txtPassword.Attributes["value"] = EnteredPassword.ToSimpleString();
                txtPassword.Text = EnteredPassword.ToSimpleString();
            }

            //4QA: To test 22486, uncomment following line.
            //System.Threading.Thread.Sleep(120000);

        }



        #endregion

        #region Helpers creepers


        private void SetControlVisibility(bool visible)
        {
            BmcCredentialsBox.Visible = IsBmcNode && MultipleSelectionSelected;
        }

        private void ClearTestResultMessage()
        {
            HttpContext.Current.Session[TESTRESULT_SESSIONID] = TestResult.None;
            bmcTestSuccess.Visible = false;
            bmcTestFailure.Visible = false;

        }


        private void ReportTestSuccess()
        {
            HttpContext.Current.Session[TESTRESULT_SESSIONID] = TestResult.Success;
            bmcTestSuccess.Visible = true;
            bmcTestFailure.Visible = false;


        }


        private void ReportTestFailure()
        {
            HttpContext.Current.Session[TESTRESULT_SESSIONID] = TestResult.Failure;
            bmcTestSuccess.Visible = false;
            bmcTestFailure.Visible = true;


        }
        private void UpdateData()
        {

            try
            {

                try
                {
                    EnteredPort = Convert.ToInt32(txtPortNumber.Text);
                }
                catch
                {
                    EnteredPort = 0;
                }

                EnteredSsl = cbUseSSL.Checked;
              
                if (!String.IsNullOrEmpty(txtPassword.Text))
                {
                    EnteredPassword = txtPassword.Text.ToSecureString();
                }
                else
                {
                    EnteredPassword = txtPassword.Attributes["value"].ToSecureString();
                }

                EnteredUserName = txtUserName.Text;

            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

        }


        private BMCConnectionTestResult TestConnection(string nodeIpAddress, int portNumber, string userName, SecureString password, bool ssl, int engineId)
        {

            using (var proxy = BusinessLayerProxyFactory.Instance.Create(engineId))
            {
                try
                {
                    return proxy.Api.TestBmcConnection(nodeIpAddress, portNumber, userName, password.ToSimpleString(), ssl);
                }
                catch (Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }
        }

        private void ClearDataFromSession()
        {
            EnteredPort = 0;
            EnteredUserName = string.Empty;
            EnteredPassword = string.Empty.ToSecureString();
            EnteredSsl = false;
        }

        private void FillFirstTimeValues()
        {
            //Try to get data from session and fill them in

            txtUserName.Text = EnteredUserName;
            cbUseSSL.Checked = EnteredSsl;

            if (EnteredPort > 0)
            {
                txtPortNumber.Text = EnteredPort.ToString();
            }
            else
            {
                txtPortNumber.Text = String.Empty;
            }

            cbIsBmcNode.Checked = IsBmcNode;
            cbIsBmcNode_CheckedChanged(this, EventArgs.Empty);

            ClearTestResultMessage();
        }


        private void UpdateBmcStuffInDbForNode(Node node)
        {
            using (var proxy = BusinessLayerProxyFactory.Instance.Create())
            {
                try
                {
                    proxy.Api.CreateBMCController(new BMCController
                    {
                        NodeID = node.Id,
                        UserName = EnteredUserName,
                        Password = EnteredPassword.ToSimpleString(),
                        Port = EnteredPort,
                        UseSSL = EnteredSsl
                    });

                    // create BMC Poller for node
                    proxy.Api.InsertUpdatePoller("N", node.Id, _connectionTestResults[node.IpAddress].PollerType);
                }
                catch (Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }
        }


        #endregion

        #region BL exception handling

        private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

        protected void BusinessLayerExceptionHandler(Exception ex)
        {
            log.Error(ex);
        }

        #endregion



        #region INodePropertyPlugin members

        public void Initialize(IList<SolarWinds.Orion.Core.Common.Models.Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
        {

            // save localy passed args
            _propertyBag = pluginState;
            _nodes = nodes;
            _connectionTestResults = new Dictionary<string, BMCConnectionTestResult>();
            _mode = mode;


            if (!IsPostBack)
            {
                FillFirstTimeValues();

                //Change timeout setting for scripts to cover long-lasting tests
                System.Web.UI.ScriptManager manager = System.Web.UI.ScriptManager.GetCurrent(this.Page);
                using (var proxy = BusinessLayerProxyFactory.Instance.Create())
                {
                    try
                    {
                        manager.AsyncPostBackTimeout = proxy.Api.GetBMCApiTimeout(_nodes.Count);
                    }
                    catch (Exception ex)
                    {
                        BusinessLayerExceptionHandler(ex);
                        throw;
                    }
                }

                switch (_mode)
                {
                    case NodePropertyPluginExecutionMode.EditProperies:
                    {
                        //Handle Multiple nodes.
                        //The user has to explicitly check the box if they want to edit BMC credentials for multiple nodes.
                        if (_nodes.Count > 1)
                        {
                            MultipleSelectionVisible = true;
                            MultipleSelectionSelected = false;
                        }
                        else
                        {
                            MultipleSelectionVisible = false;
                            MultipleSelectionSelected = true;
                        }

                        //Show us in any case on Edit node page. 
                        foreach (var plugin in NodePropertyPluginManager.Plugins.Where(p => p.Name == "BmcControllerCredentials"))
                        {
                            plugin.Visible = true;
                        }


                        //We are going to edit properties. Load them from DB first.
                        //If there are more nodes with different credentials, display a warning.

                        bool fillCredentials = false;
                        string portToFill = String.Empty;
                        string userNameToFill = String.Empty;
                        SecureString passwordToFill = string.Empty.ToSecureString();
                        bool useSSLToFill = false;

                        //Check if the credentials are the same. If not, issue warning. 
                        bool areAllCredsSame = true;
                        string lastCreds = null;
                        string currentCreds = string.Empty;

                        bool moreThanOneNode = _nodes.Count > 1;

                        using (var proxy = BusinessLayerProxyFactory.Instance.Create())
                        {
                            foreach (var node in _nodes)
                            {
                                BMCController existingController;
                                try
                                {
                                    existingController = proxy.Api.GetBMCControllerWithSensitiveData(node.ID);
                                }
                                catch (Exception ex)
                                {
                                    BusinessLayerExceptionHandler(ex);
                                    throw;
                                }
                                if (existingController != null)
                                {
                                    cbIsBmcNode.Enabled = false;
                                    currentCreds = existingController.Port + existingController.UserName + existingController.Password;

                                    if (lastCreds != null)
                                    {
                                        //this is not the first node
                                        areAllCredsSame &= (currentCreds == lastCreds);

                                    }
                                    else
                                    {
                                        //One-time fill of the credentials. Will be used if all are the same.
                                        portToFill = existingController.Port.ToString();
                                        userNameToFill = existingController.UserName;
                                        passwordToFill = existingController.Password.ToSecureString();
                                        useSSLToFill = existingController.UseSSL;
                                    }

                                    lastCreds = currentCreds;
                                    if (!areAllCredsSame) break; //no need to evaluate evrything if failed already
                                }
                                else
                                {
                                    //BMC controller not found for a node. Back out
                                    areAllCredsSame = false;
                                }
                            }
                        }

                        fillCredentials = areAllCredsSame;


                        if (fillCredentials)
                        {
                            //If we pre-fill credentials for user,
                            //enable the control
                            cbIsBmcNode.Checked = true;
                            cbIsBmcNode_CheckedChanged(this, EventArgs.Empty);

                            txtUserName.Text = userNameToFill;
                            EnteredUserName = userNameToFill;

                            txtPortNumber.Text = portToFill;
                            EnteredPort = Convert.ToInt32(portToFill);


                            EnteredPassword = passwordToFill;
                            txtPassword.Attributes["value"] = passwordToFill.ToSimpleString();
                            txtPassword.Text = passwordToFill.ToSimpleString();

                            cbUseSSL.Checked = useSSLToFill;
                        }
                        else
                        {
                            //Display the warning if
                            //we have more nodes
                            //they don't have the same credentials
                            //and there is at least one set of valid credentials
                            if (moreThanOneNode && !areAllCredsSame && !String.IsNullOrEmpty(portToFill))
                            {
                                multipleCredentialsWarning.Visible = true;
                            }
                        }
                        break;
                    }


                    case NodePropertyPluginExecutionMode.WizDefineNode:
                    {
                        cbIsBmcNode.Checked = true;
                        cbIsBmcNode_CheckedChanged(this, EventArgs.Empty);
                        cbIsBmcNode.Visible = false;
                        cbIsBmcDescription.Visible = false;


                        MultipleSelectionVisible = false;
                        MultipleSelectionSelected = true;
                        break;
                    }
                    default:
                    {
                        MultipleSelectionVisible = false;
                        MultipleSelectionSelected = true;
                        break;
                    }
                }


            }
            else //If postback
            {
                //update data on non postback events (submits etc).
                UpdateData();

            }


            //On every page reload, repopulate the password box if we stored a password previously.
            if (EnteredPassword != null)
            {
                txtPassword.Attributes["value"] = EnteredPassword.ToSimpleString();
                txtPassword.Text = EnteredPassword.ToSimpleString();
            }


            //When the test takes long time, a new page load is issued after it completes. This should 
            //make the test message display properly even in that case.

            if (_propertyBag.ContainsKey(TESTRESULT_SESSIONID))
            {
                TestResult testMessageId = (TestResult)_propertyBag[TESTRESULT_SESSIONID];

                switch (testMessageId)
                {
                    case TestResult.None:
                        ClearTestResultMessage();
                        break;

                    case TestResult.Success:
                        ReportTestSuccess();
                        break;

                    case TestResult.Failure: //Failure
                        ReportTestFailure();
                        break;
                }
            }


        }

        public bool Update()
        {

            if (!IsBmcNode)
            {
                //The control is not enabled - back off
                return true;
            }
            switch (_mode)
            {
                case NodePropertyPluginExecutionMode.WizDefineNode: //Add node first step never calls update

                    return true;

                case NodePropertyPluginExecutionMode.WizChangeProperties: //Add node last step

                    UpdateBmcStuffInDbForNode(NodeWorkflowHelper.Node);
                    ClearDataFromSession();
                    return true;


                case NodePropertyPluginExecutionMode.EditProperies: //Edit node

                    if (_nodes != null)
                    {
                        if (_nodes.Count > 0)
                        {
                            foreach (var node in _nodes)
                            {
                                UpdateBmcStuffInDbForNode(node);
                            }
                        }
                    }

                    ClearDataFromSession();
                    return true;

                default:
                    return false;
            }
        }

        public bool Validate()
        {
            if (!ValidateInternal())
            {
                throw new ArgumentException(_validationFailExceptionMessage);
            }

            return true;
        }

        private bool ValidateInternal()
        {

            if (!IsBmcNode)
            {
                //This means we do not edit anything, so no need to validate
                return true;
            }


            UpdateData();

            bool allOK = true;
            int portNumber = -1;

            allOK &= Int32.TryParse(txtPortNumber.Text, out portNumber);
            allOK &= portNumber > 0;

            _connectionTestResults.Clear();
            //username and password can be empty, so don't test them
            if (allOK)
            {
                foreach (SolarWinds.Orion.Core.Common.Models.Node node in _nodes)
                {
                    string nodeIp = node.IpAddress;
                    if (_nodes.Count == 1 && !string.IsNullOrEmpty(_propertyBag["__nodeIp"] as string))
                    {
                        nodeIp = _propertyBag["__nodeIp"].ToString();
                    }

                    var bmcConnectionTestResult = TestConnection(nodeIp, EnteredPort, EnteredUserName, EnteredPassword, EnteredSsl, node.EngineID);
                    _connectionTestResults[nodeIp] = bmcConnectionTestResult;
                    allOK &= bmcConnectionTestResult.Success;
                }

            }



            if (allOK)
            {
                ReportTestSuccess();
            }
            else
            {
                ReportTestFailure();
            }

            return allOK;

        }

        #endregion
    }
}
