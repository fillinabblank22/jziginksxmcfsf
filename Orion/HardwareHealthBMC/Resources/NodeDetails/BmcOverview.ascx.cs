﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.HardwareHealth.BMC.Common;
using SolarWinds.Orion.HardwareHealth.BMC.Contract.BmcOverview;
using SolarWinds.Orion.HardwareHealth.BMC.Web.ResourceMetadata.Enums;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Shared;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, HardwareHealthBmcMetadataSearchTagsValues.Bmc)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, HardwareHealthBmcMetadataSearchTagsValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, HardwareHealthBmcMetadataSearchTagsValues.Overview)]
public partial class Orion_HardwareHealthBMC_Resources_Details_BMCOverview : ResourceControl
{
    private const string IndentColumn = "<td class=\"PropertyIndent\"></td>";
    private const string ManageLink = "<span tabindex=0 onClick=\"{0} return false;\" style=\"cursor:pointer; color:#0079aa;\">{1}</span>";
    private const string DetailsLink = "<a href=\"{0}\">{1}</a>";
    private const string AddNodeLink = "/Orion/Nodes/Add/Default.aspx?IPAddress={0}&restart=false";
    private const string NodeStatusIcon = "/Orion/Images/StatusIcons/small-{0}.gif";
    private const string EmptyIcon = "";

    private const string SelectNodeInfo = @"
            SELECT n.Status, n.Stats.ResponseTime, n.Stats.PercentLoss, n.DetailsUrl
            FROM Orion.Nodes (nolock=true) n
            WHERE n.NodeID = @NodeID";

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof(INodeProvider)}; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionproductResourceUCSOverview"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.HwHBMCWebContent.BmcOverviewDefaultTitle; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var allowNodeManagement = (bool) Profile["AllowNodeManagement"];
        var nodeId = GetInterfaceInstance<INodeProvider>().Node.NodeID;

        var retriever = new AppDomainImplemenationRetriever();
        var providers = retriever.RetrieveAndInstantiate<IBmcOverviewSectionProvider>();

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var sections = providers
                .Select(i => i.GetSections(nodeId))
                .Where(i => i.IsVisible)
                .OrderBy(i => i.Index)
                .Select(section => new DisplaySection
                {
                    Header = section.Header,
                    Rows = section.Rows.Select(i => MapRow(swis, i, allowNodeManagement)).ToList()
                })
                .ToList();

            if (sections.Any())
            {
                Sections.DataSource = sections;
                Sections.DataBind();
            }
            else
                Visible = false;
        }
    }

    private DisplayRow MapRow(IInformationServiceProxy swis, BmcOverviewSectionRow row, bool allowNodeManagement)
    {
        if (!row.IsManageable)
        {
            return new DisplayRow
            {
                Indent = row.Indent,
                Icon = row.Icon,
                Name = row.Name
            };
        }

        if (!row.NodeId.HasValue)
            return MapRegularRow(row, allowNodeManagement);

        var nodeInfo = swis.Query(SelectNodeInfo, new Dictionary<string, object>(1) {{"NodeID", row.NodeId.Value}});

        if (nodeInfo == null || nodeInfo.Rows.Count == 0)
            return MapRegularRow(row, allowNodeManagement);

        return MapRowOrionNodeRow(row, nodeInfo.Rows[0]);
    }

    private DisplayRow MapRegularRow(BmcOverviewSectionRow row, bool allowNodeManagement)
    {
        return new DisplayRow
        {
            Indent = row.Indent,
            Icon = EmptyIcon,
            Name = GetRegularRowName(row, allowNodeManagement),
            ResponseTime = Resources.HwHBMCWebContent.BmcOverviewNotAvailable,
            PercentLoss = Resources.HwHBMCWebContent.BmcOverviewNotAvailable,
            Status = Resources.HwHBMCWebContent.BmcOverviewNotAvailable
        };
    }

    private string GetRegularRowName(BmcOverviewSectionRow row, bool allowNodeManagement)
    {
        if ((allowNodeManagement || OrionConfiguration.IsDemoServer) && !string.IsNullOrEmpty(row.IpAddress))
        {
            if (row.Source == BmcOverviewSectionRowSource.Blade)
            {
                return string.Format(ManageLink, ManageServerAsNodeScript(row.Name, row.IpAddress, Resources.HwHBMCWebContent.ManageBladeAsNodeTitle), row.Name);
            }

            if (row.Source == BmcOverviewSectionRowSource.Rack)
            {
                return string.Format(ManageLink, ManageServerAsNodeScript(row.Name, row.IpAddress, Resources.HwHBMCWebContent.ManageRackAsNodeTitle), row.Name);
            }

            if (row.Source == BmcOverviewSectionRowSource.Other)
            {
                return string.Format(ManageLink, ManageAsNodeScript(row.Name, row.IpAddress, Resources.HwHBMCWebContent.ManageAsNodeTitle), row.Name);
            }         
        }          
        return row.Name;
    }

    private static DisplayRow MapRowOrionNodeRow(BmcOverviewSectionRow row, DataRow nodeInfo)
    {
        var statusName = StatusInfo.GetStatus(Convert.ToInt32(nodeInfo["Status"])).StatusName;

        return new DisplayRow
        {
            Indent = row.Indent,
            Icon = string.Format(NodeStatusIcon, statusName),
            Name = string.Format(DetailsLink, nodeInfo["DetailsUrl"], row.Name),
            ResponseTime = new ResponseTime(TimeSpan.FromMilliseconds(Convert.ToDouble(nodeInfo["ResponseTime"]))).ToString(),
            PercentLoss = new PacketLoss(Convert.ToSingle(nodeInfo["PercentLoss"])).ToString(),
            Status = statusName
        };
    }

    private string ManageServerAsNodeScript(string name, string ip, string dialogTitle)
    {
        if (OrionConfiguration.IsDemoServer)
            return "demoAction('HWHBMC_Overview_ManageEntity', this);";

        return string.Format(@"ManageServerAsNodeDialog('{0}', '{1}', '{2}');", ip, name, dialogTitle);
    }

    private string ManageAsNodeScript( string name, string ip, string dialogTitle)
    {
        if (OrionConfiguration.IsDemoServer)
            return "demoAction('HWHBMC_Overview_ManageEntity', this);";

        return string.Format(@"ManageAsNodeDialog('{0}', '{1}', '{2}');", ip, string.Format(Resources.HwHBMCWebContent.BmcOverviewManageWithOrionQuestion, name), dialogTitle);
    }

    protected string GetSectionSeparator(RepeaterItem obj)
    {
        var section = obj.DataItem as DisplaySection;
        if (section == null)
            return "";
        var sections = Sections.DataSource as List<DisplaySection>;
        if (sections == null)
            return "";
        if (sections.IndexOf(section) == 0)
            return "";
        return "<br />";
    }

    #region Indent

    protected int GetHeaderColspan(RepeaterItem obj)
    {
        var section = obj.DataItem as DisplaySection;
        if (section == null)
            return 0;
        return (section.Rows.Max(i => (int?) i.Indent) ?? 0) + 2;
    }

    protected int GetNameColspan(RepeaterItem obj)
    {
        var row = obj.DataItem as DisplayRow;
        if (row == null)
            return 0;
        var parentItem = obj.Parent.Parent as RepeaterItem;
        if (parentItem == null)
            return 0;
        var section = parentItem.DataItem as DisplaySection;
        if (section == null)
            return 0;
        return section.Rows.Max(i => i.Indent) - row.Indent + 1;
    }

    protected string GetIndentCols(RepeaterItem obj)
    {
        var row = obj.DataItem as DisplayRow;
        var sb = new StringBuilder();
        if (row != null)
            for (var i = 0; i < row.Indent; i++)
                sb.Append(IndentColumn);
        return sb.ToString();
    }

    #endregion

    #region Classes

    protected class DisplaySection
    {
        public string Header { get; set; }
        public List<DisplayRow> Rows { get; set; }
    }

    protected class DisplayRow
    {
        public int Indent { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string ResponseTime { get; set; }
        public string PercentLoss { get; set; }
        public string Status { get; set; }
    }

    #endregion
}
