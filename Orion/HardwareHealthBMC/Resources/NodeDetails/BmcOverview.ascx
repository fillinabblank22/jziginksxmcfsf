﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BmcOverview.ascx.cs" Inherits="Orion_HardwareHealthBMC_Resources_Details_BMCOverview" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>
<%@ Register TagPrefix="orion" TagName="resourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="HardwareHealthBMC/Resources/NodeDetails/styles.css" />
<orion:Include runat="server" File="HardwareHealthBMC/Resources/NodeDetails/Js/ManageServerAsNodeDialog.js" />
<orion:Include runat="server" File="HardwareHealthBMC/Resources/NodeDetails/Js/ManageAsNodeDialog.js" />

<orion:ResourceWrapper runat="server" ID="Wrapper" >
    <Content>        
        <asp:Repeater runat="server" ID="Sections">
            <ItemTemplate>
                <%# GetSectionSeparator(Container) %>
                <b><%# Eval("Header") %></b>
                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                    <thead style="text-align:left;">
                        <tr>
                            <th class="ReportHeader" colspan="<%# GetHeaderColspan(Container) %>"><%=HwHBMCWebContent.BmcOverviewName%></th>
                            <th class="ReportHeader" width="25%"><%=HwHBMCWebContent.BmcOverviewResponseTime%></th>
                            <th class="ReportHeader" width="25%"><%=HwHBMCWebContent.BmcOverviewPercentLoss%></th>
                            <th class="ReportHeader" width="15%"><%=HwHBMCWebContent.BmcOverviewStatus%></th>
                        </tr>
                    </thead>
                    <tbody>
                       <asp:Repeater DataSource='<%# Eval("Rows") %>' runat="server">
                            <ItemTemplate>
                                <tr>
                                    <%# GetIndentCols(Container) %>
                                    <td class="PropertyIcon">
                                        <%# !string.IsNullOrEmpty(Eval("Icon").ToString()) ? "<img src='" + Eval("Icon") + "'/>" : string.Empty %>
                                    </td>
                                    <td class="Property" colspan="<%# GetNameColspan(Container) %>"><%# Eval("Name") %></td>
                                    <td class="Property"><%# Eval("ResponseTime") %></td>
                                    <td class="Property"><%# Eval("PercentLoss") %></td>
                                    <td class="Property"><%# Eval("Status") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </ItemTemplate>
        </asp:Repeater>   
    </Content>
</orion:ResourceWrapper>
