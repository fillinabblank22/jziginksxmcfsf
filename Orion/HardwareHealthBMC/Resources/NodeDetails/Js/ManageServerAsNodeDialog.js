﻿ManageServerAsNodeDialog = function (ipAddress, name, dialogTitle) {

    var dialog;
    function onCancel() { if (dialog) { dialog.dialog("close"); ; dialog = null; } };

    function onManage() {  

        if (dialog) { closeTrigger = "btnManage"; dialog.dialog("close"); ; dialog = null; }
        var radioValue = $('input[name=interface]:checked').val();
        if(radioValue === 'os')
        {
            window.location = '/Orion/Nodes/Add/Default.aspx?&restart=false';
        }
        if(radioValue === 'mng')
        {
            window.location = String.format('/Orion/Nodes/Add/Default.aspx?IPAddress={0}&restart=false', ipAddress);
        }       
    };

    var textHtml =
                '<div id="id-manage-as-node-dialog" style="height:70px; background-color:white; display:block; padding:10px;">' +
                   '<table>' +
                        '<tr>' +
                            '<td style="width:40px;">' +
                                '<img src="/Orion/Images/Info-Notification.gif" alt="" />' +
                            '</td>' +
                            '<td>' +
                                '<b>@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_ServerQuestion;E=js}</b>' +
                            '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td colspan="2">' +
                            '<br/>' +
                            '<input type="radio" name="interface" value="mng" checked="checked">' + '@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Mngmt;E=js}' + '<br/>' +
                            '<input type="radio" name="interface" value="os">' + '@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Os;E=js}' + '<br/><br/>' +
                            '</td>' +
                        '</tr>' +  
                    '</table> ' +
                    '<table class="sw-suggestion-info" style="margin-top: 20px; width: 100%;">' +
                        '<tr>' +
                            '<td style="padding:5px" class="ManageAsNode-ICMPWarning-HelpLinkPlaceholder">' +
                            '<p><b>@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_ManagementInterface;E=js} </b>'
                                     + '@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Mngmt_Info;E=js}' +
                            '<p><b>@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_OperatingSystemInterface;E=js} </b>'
                                     + '@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Os_Info;E=js}' +
                            '</td>' +
                        '</tr>' +
                    '</table>' +
                    '{1}' +
                '</div>';
                
    var dialogButtons = '<br /><div style="float: right">';  
    dialogButtons += SW.Core.Widgets.Button('@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Button_Manage;E=js}', { type: 'primary', id: 'btnManage' });
    dialogButtons += '&nbsp;' + SW.Core.Widgets.Button('@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Button_Cancel;E=js}', { type: 'secondary', id: 'btnCancel' });  
    dialogButtons += '</div>';
    
    textHtml = String.format(textHtml,name, dialogButtons);

    var $dialog = $(textHtml);
    $dialog.find("#btnManage").click(function () { onManage(); });
    $dialog.find("#btnCancel").click(function () { onCancel(); });
    dialog = $dialog.dialog({ width: 600,
        title: dialogTitle,
        modal: true,
        resizable: false
    });
}