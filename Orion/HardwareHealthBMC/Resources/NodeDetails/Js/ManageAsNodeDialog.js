﻿ManageAsNodeDialog = function (ipAddress, manageWithOrionQuestion, dialogTitle) {

    var dialog;
    function onCancel() { if (dialog) { dialog.dialog("close"); ; dialog = null; } };

    function onManage() {  

        if (dialog) { closeTrigger = "btnManage"; dialog.dialog("close"); ; dialog = null; }
        window.location = String.format('/Orion/Nodes/Add/Default.aspx?IPAddress={0}&restart=false', ipAddress);
      
    };

    var textHtml =
                '<div id="id-manage-as-node-dialog" style="height:70px; background-color:white; display:block; padding:10px;">' +
                   '<table>' +
                        '<tr>' +
                            '<td style="width:40px;">' +
                                '<img src="/Orion/Images/Info-Notification.gif" alt="" />' +
                            '</td>' +
                            '<td>' +
                                '<b>{0}</b>' +
                            '</td>' +
                        '</tr>' +
                    '</table> ' +
                    '{1}' +
                '</div>';
                
    var dialogButtons = '<br /><div style="float: right">';  
    dialogButtons += SW.Core.Widgets.Button('@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Button_Manage;E=js}', { type: 'primary', id: 'btnManage' });
    dialogButtons += '&nbsp;' + SW.Core.Widgets.Button('@{R=Solarwinds.Orion.HardwareHealth.BMC.Strings;K=ManageAsNode_Button_Cancel;E=js}', { type: 'secondary', id: 'btnCancel' });  
    dialogButtons += '</div>';
    
    textHtml = String.format(textHtml, manageWithOrionQuestion, dialogButtons);

    var $dialog = $(textHtml);
    $dialog.find("#btnManage").click(function () { onManage(); });
    $dialog.find("#btnCancel").click(function () { onCancel(); });
    dialog = $dialog.dialog({ width: 600,
        title: dialogTitle,
        modal: true,
        resizable: false
    });
}