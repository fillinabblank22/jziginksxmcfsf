<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB1_4 %>"  Inherits="SolarWinds.Orion.Web.UI.OrionErrorPageBase" ValidateRequest="false" %>
<%@ Import Namespace="SolarWinds.Internationalization.Exceptions" %>
<%@ Register TagPrefix="orion" TagName="PageHeader" Src="~/Orion/Controls/PageHeader.ascx" %>
<%@ Register TagPrefix="orion" TagName="PageFooter" Src="~/Orion/Controls/PageFooter.ascx" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server" >
    <orion:Include runat="server" File="Error.css" />
    </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" Runat="Server">
    
    <div id="container">
        <div id="content">
            <orion:PageHeader Minimalistic="true" Visible="<%# !IsEmbeddable %>" runat="server" />
            <form runat="server" class="sw-app-region">
                <input type="hidden" name="errorsite" value="<%= DefaultSanitizer.SanitizeHtml(Details.ErrorSite) %>" />
                <input type="hidden" name="errortype" value="<%= DefaultSanitizer.SanitizeHtml(Details.ErrorType) %>" />

                <div id="errorcontent">
                    <div class="sw-error-page-image">

                        <h1 class="sw-title-main"><%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Details.Title) %></h1>

                        <p class="sw-error-page-main-message"><%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(ExceptionHelper.GetLocalizedOrDefaultMessage(Details.Error)).Replace("\n", "<br />") %></p>

                        <% if (string.IsNullOrEmpty(Details.HtmlHelpfulHints) == false)
                           { %>
                            <div class='sw-suggestion'><span class='sw-suggestion-icon'></span>
                                <div class="sw-suggestion-title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_5) %></div>
                                <%= DefaultSanitizer.SanitizeHtml(Details.HtmlHelpfulHints) %>
                            </div>
                        <% } %>

                        <div class="sw-btn-bar">
                            <% if (IncludeErrorDetail || Profile.AllowAdmin)
                               { %>
                               <orion:LocalizableButtonLink Visible="<%# !IsUncached %>" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml("/Orion/Error.aspx?save=true&id=" + Details.Id + (IsSqlFaulted ? "&nosql=true" : string.Empty)) %>' DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB1_6 %>" runat="server" />
                            <% } %>
                            <orion:LocalizableButtonLink DisplayType="<%# !IsUncached ? SolarWinds.Orion.Web.UI.ButtonType.Secondary : SolarWinds.Orion.Web.UI.ButtonType.Primary %>" NavigateUrl="/Orion/View.aspx" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB1_7 %>" runat="server" />
                        </div>

                        <p>
                            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_VB1_8, string.Format("<a target=\"_blank\" href={0}>", KbSearchUrl), "</a>", string.Format("<a target=\"_blank\" href={0}>", Resources.CoreWebContent.SolarWindsSupportUrl))) %>
                        </p>
                    </div>
                </div>
            </form>  
        </div>
        <div id="container-after">&nbsp;</div>
    </div>
    <orion:PageFooter Visible="<%# !IsEmbeddable %>" runat="server" />
</asp:Content>
