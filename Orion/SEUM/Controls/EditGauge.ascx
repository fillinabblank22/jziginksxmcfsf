﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditGauge.ascx.cs" Inherits="Orion_SEUM_Controls_EditGauge" %>

<p>
    <b><%= rangeLabel %>:</b><br />
    <asp:DropDownList ID="rangeDropDown" runat="server" onchange="UpdateRangeFields()"/><br />
    <span class="SEUM_HelpText">
        Gauge has range from 0 to X where X can be calculated in various ways.<br />
        <b>Dynamic:</b> Gauge gets list of values and starts with first defined range. If value exceeds this range, range is changed to next defined value until value fits into it.<br />
        <b>Static:</b> Gauge range is defined statically as one value and is not changed even if displayed value exceeds this range.<br />
        <b>Thresholds dependent:</b> Gauge range is defined as higher threshold multiplied by defined value. If item has no thresholds, dynamic method is used.
    </span>
</p>

<p class="hidden" id="staticRangeFields">
    <b><%= staticRangeLabel%>:</b><br />
    <asp:TextBox runat="server" ID="staticMaxValueBox" />
    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="staticMaxValueBox" Display="Dynamic"
        ErrorMessage="Value must be in interval from 1 to 10000." MinimumValue="1" MaximumValue="10000" Type="Integer" />
    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="staticMaxValueBox"  Display="Dynamic"
        ErrorMessage="Value must be a valid integer value." Operator="DataTypeCheck"
        Type="Integer" />
</p>

<p class="hidden" id="dynamicRangeFields">
    <b><%= dynamicRangeLabel%>:</b><br />
    <asp:TextBox runat="server" ID="dynamicValuesBox" Width="300" />
    <asp:RegularExpressionValidator runat="server" ControlToValidate="dynamicValuesBox"
    ErrorMessage="Value can be only numbers separated by comma."
    ValidationExpression="^\s*\d+\s*(,\s*\d+\s*)*" />
</p>

<p class="hidden" id="multiplierFields">
    <b><%= thresholdMultiplierLabel%>:</b><br />
    <asp:TextBox runat="server" ID="thresholdsMultiplierBox" />
    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="thresholdsMultiplierBox" Display="Dynamic"
        ErrorMessage="Value must be in interval from 1 to 100." MinimumValue="1" MaximumValue="100" Type="Double" />
    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="thresholdsMultiplierBox"  Display="Dynamic"
        ErrorMessage="Value must be a valid decimal value." Operator="DataTypeCheck"
        Type="Double" />
</p>

<p>
    <span id="GaugesList">
        <b><%= selectStyleLabel %>:</b><br />
        <asp:DropDownList ID="stylesList" runat="server" onchange="SelectCurrentGauge()"/>
    </span>
</p>

<p>
    <b><%= gaugeSizeLabel %>:</b><br />
    <asp:TextBox ID="scaleInput" runat="server" MaxLength="3"></asp:TextBox>
    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="scaleInput" Display="Dynamic"
        ErrorMessage="Size must be in interval from 30% to 250%" MinimumValue="30" MaximumValue="250"
        Type="Integer">*</asp:RangeValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
            runat="server" ControlToValidate="scaleInput" ErrorMessage="Gauge size field can not be left empty">*</asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="scaleInput"  Display="Dynamic"
        ErrorMessage="Gauge size must be correct integer value" Operator="DataTypeCheck"
        Type="Integer">*</asp:CompareValidator>
</p>

<a href="javascript:void(0);" class="ExpanderLink">
    <img src="/Orion/images/Button.Expand.gif" />
    <b>Available Styles</b>
</a>
<div id="GaugeStylesPanel" runat="server" style="background-color: White; width: 840px; display: none;">
</div>
