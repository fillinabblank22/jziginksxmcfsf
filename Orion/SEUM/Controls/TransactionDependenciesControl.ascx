﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionDependenciesControl.ascx.cs" Inherits="Orion_SEUM_Controls_TransactionDependenciesControl" ClassName="SolarWinds.SEUM.Controls.TransactionDependenciesControl" %>
<%@ Import Namespace="SolarWinds.SEUM.Licensing" %>
<%@ Register Src="~/Orion/SEUM/Controls/AssignObjectsControl.ascx" TagName="AssignObjectsControl" TagPrefix="orion" %>
<div class="SEUM_RelatedEntitiesSelection_Transaction">
    <div class="SEUM_RelatedEntitiesSelection_SelectorsContainer">
        <% if (LicenseInfo.AreNodesAvailable) { %>
            <div class="SEUM_RelatedEntitiesSelection_Selector">
                <h3>Nodes:</h3>
                <orion:AssignObjectsControl runat="server" ID="nodesSelector" EntityCaption="nodes"/> 
            </div>
        <% } %>
        <% if (LicenseInfo.AreApplicationsAvailable) { %>
            <div class="SEUM_RelatedEntitiesSelection_Selector">
                <h3>Applications:</h3>
                <orion:AssignObjectsControl runat="server" ID="applicationsSelector" EntityCaption="applications"/> 
            </div>
        <% } %>
    </div>
    <div>
        <asp:HyperLink NavigateUrl="javascript:void(0)" runat="server" ID="expandButton"><img src="/Orion/Images/Button.Expand.gif" alt="+" /><span>Set individual dependencies for steps</span></asp:HyperLink>
    </div>
    <asp:Panel runat="server" ID="stepsContainer" CssClass="SEUM_RelatedEntitiesSelection_StepsContainer hidden">
        <asp:Repeater runat="server" ID="stepsRepeater" OnItemDataBound="stepsRepeater_OnItemDataBound">
            <HeaderTemplate>
                <table class="SEUM_RelatedEntitiesSelection_StepsTable">
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="SEUM_RelatedEntitiesSelection_StepsTable_Row">
                    <td class="SEUM_RelatedEntitiesSelection_StepsTable_CaptionCell">
                        <asp:Label runat="server" ID="transactionStepLabel" CssClass="SEUM_TransactionStepName" />
                    </td>
                    <td class="SEUM_RelatedEntitiesSelection_StepsTable_SelectorsCell">
                        <% if (LicenseInfo.AreNodesAvailable) { %>
                            <div class="SEUM_RelatedEntitiesSelection_Selector">
                                <h3>Nodes:</h3>
                                <orion:AssignObjectsControl runat="server" ID="nodesSelector" EntityCaption="nodes"/> 
                            </div>
                        <% } %>
                        <% if (LicenseInfo.AreApplicationsAvailable) { %>
                            <div class="SEUM_RelatedEntitiesSelection_Selector">
                                <h3>Applications:</h3>
                                <orion:AssignObjectsControl runat="server" ID="applicationsSelector" EntityCaption="applications"/> 
                            </div>
                        <% } %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function() {
            var $expandButton = $("#<%=expandButton.ClientID%>");
            var $stepsContainer = $("#<%=stepsContainer.ClientID%>");

            $expandButton.toggle(
                function() {
                    $("img:first", this).attr('src', '/Orion/Images/Button.Collapse.gif');
                    $("span:first", this).html("Hide individual dependencies for steps");
                    $stepsContainer.slideDown('fast');
                }, function() {
                    $("img:first", this).attr('src', '/Orion/Images/Button.Expand.gif');
                    $("span:first", this).html("Set individual dependencies for steps");
                    $stepsContainer.slideUp('fast');
                });
        });
    </script>
</div>