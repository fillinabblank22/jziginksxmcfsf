﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.Shared;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_EditResourceControls_EditTransactionsHealthOverviewResource : SEUMBaseResourceEditControl
{
    private const string HideUnmanagedLabel = "Hide Unmanaged Transactions";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool hideUnmanaged;

        if (!Boolean.TryParse(Resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "true", out hideUnmanaged))
            hideUnmanaged = true;

        hideUnmanagedCheckbox.Text = HideUnmanagedLabel;
        hideUnmanagedCheckbox.Checked = hideUnmanaged;

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Filter]))
            filterEditor.Filter = Resource.Properties[ResourcePropertiesKeys.Filter];

        filterEditor.Entity = SwisEntities.Transactions;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties[ResourcePropertiesKeys.HideUnmanaged] = this.hideUnmanagedCheckbox.Checked;
            properties[ResourcePropertiesKeys.Filter] = this.filterEditor.Filter;

            return properties;
        }
    }
}
