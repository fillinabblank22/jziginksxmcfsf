﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_GeneralStatusIcon : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Color color;
        switch (Status)
        {
            case SEUMStatuses.Up:
                color = SEUMColors.UpStatusChartColor;
                break;
            case SEUMStatuses.Warning:
                color = SEUMColors.WarningStatusChartColor;
                break;
            case SEUMStatuses.Critical:
                color = SEUMColors.CriticalStatusChartColor;
                break;
            case SEUMStatuses.Failed:
                color = SEUMColors.FailedStatusChartColor;
                break;
            default:
                color = SEUMColors.UnknownStatusChartColor;
                break;
        }

        Literal lit = new Literal();
        lit.Text = string.Format("<span class=\"{0}\" style=\"background-color: {1}\">&nbsp;</span>",
                                 CssClasses.GeneralStatusIcon,
                                 ColorTranslator.ToHtml(color));
        icon.Controls.Add(lit);
    }

    public int Status
    {
        get; set;
    }
}