﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTransactionsHealthOverviewResource.ascx.cs"
    Inherits="Orion_SEUM_Controls_EditResourceControls_EditTransactionsHealthOverviewResource" %>
<%@ Register TagPrefix="SEUM" TagName="FilterEditor" Src="~/Orion/SEUM/Controls/FilterEditor.ascx" %>

<p> 
    <asp:CheckBox runat="server" ID="hideUnmanagedCheckbox" />
</p>

<SEUM:FilterEditor runat="server" ID="filterEditor" />
