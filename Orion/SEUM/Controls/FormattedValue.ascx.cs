﻿using System;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Common;

public partial class Orion_SEUM_Controls_FormattedValue : System.Web.UI.UserControl
{
    protected string FormattedValue
    {
        get
        {
            if (ValueFormatter != null)
                return ValueFormatter.Format(Value);

            if (!string.IsNullOrEmpty(FormatString))
                return string.Format(FormatString, Value);

            return Value.ToString();

        }
    }

    public IValueFormatter ValueFormatter { get; set; }

    public string FormatString
    {
        get; 
        set;
    }

    public int Status
    {
        get; 
        set;
    }

    public object Value { get; set; }

    private double? DoubleValue
    {
        get
        {
            try
            {
                if (Value is String)
                {
                    double val;
                    if (Double.TryParse((string) Value, out val))
                        return val;
                    else
                        return null;
                }
                else if (Value != DBNull.Value)
                    return Convert.ToDouble(Value);
                else
                    return null;
            }
            catch (InvalidCastException)
            {
                return null;
            }
            catch (FormatException)
            {
                return null;
            }
        }
    }

    public bool? IsWarning
    {
        get; 
        set;
    }

    public bool? IsCritical
    {
        get;
        set;
    }

    protected bool IsWarningInternal
    {
        get
        {
            if (IsWarning.HasValue)
                return IsWarning.Value;

            if (!DoubleValue.HasValue)
                return false;

            return Status == SEUMStatuses.Warning;
        }
    }

    protected bool IsCriticalInternal
    {
        get
        {
            if (IsCritical.HasValue)
                return IsCritical.Value;

            if (!DoubleValue.HasValue)
                return false;

            return Status == SEUMStatuses.Critical;
        }
    }
}