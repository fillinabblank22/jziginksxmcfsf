﻿using System;
using System.Web.UI;
using SolarWinds.SEUM.Web.Controls;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_DownloadRecorderMenu : UserControl
{
    private DropdownMenuItem[] _downloadMenuItems = new DropdownMenuItem[]
    {
        new DropdownMenuItem("For Windows", CommonLinks.ChromiumRecorderInstaller),
        new DropdownMenuItem("For macOS", CommonLinks.ChromiumRecorderMacInstaller),
    };


    protected DropdownMenuItem[] DownloadMenuItems
    {
        get { return _downloadMenuItems; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DownloadMenu.DataBind();
        }
    }
}