﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoadIndicator.ascx.cs" Inherits="Orion_SEUM_Controls_LoadIndicator" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>

<table class="sw-seum-load-progress-bar">
    <tr>
        <td><orion:PercentStatusBar runat="server" ID="statusBar" /></td>
        <td><asp:Label runat="server" ID="percentLabel"></asp:Label></td>
        <% if (ShowHelpLink && HelpLink != String.Empty) { %>
        <td><a href="<%= HelpLink %>" class="SEUM_HelpLink">&#0187; <%= LearnMoreText %></a></td>
        <% } %>
    </tr>
</table>
