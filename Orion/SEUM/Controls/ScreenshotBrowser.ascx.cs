﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using System.Web;
using SolarWinds.SEUM.Web.Helpers;

public partial class Orion_SEUM_Controls_ScreenshotBrowser : System.Web.UI.UserControl
{
    #region Strings for localization

    protected const string navFirstButtonText = "First";
    protected const string navPrevButtonText = "Previous";
    protected const string navNextButtonText = "Next";
    protected const string navLastButtonText = "Last";

    protected const string fullscreenLinkText = "View screenshot full screen";
    protected const string htmlLinkText = "view HTML";
    protected const string orText = "or";
    protected const string screenshotLoadingMessage = "Loading screenshot ...";

    #endregion

    #region Events

    public event EventHandler<EventArgs<string>> DropDownValueChanged;
    public event EventHandler<EventArgs> FirstClicked;
    public event EventHandler<EventArgs> LastClicked;
    public event EventHandler<EventArgs> PrevClicked;
    public event EventHandler<EventArgs> NextClicked;

    #endregion

    private bool supressEvents = false;

    public void Update(String entity, int entityId, DateTime date, String screenshotName, bool isFirst, bool isLast)
    {
        // setup screenshot and links
        string screenshotParameters = string.Format("entity={0}&entityId={1}&dateTime={2}&noImageText={3}", 
            entity, 
            entityId, 
            date.Ticks,
            HttpUtility.UrlEncode(InvalidScreenshotText));
        string thumbnailScreenshotParameters = string.Format("{0}&useThumbnail=true", screenshotParameters);

        string screenshotUrl = string.Format("/Orion/SEUM/Controls/ScreenshotImage.ashx?{0}&width={1}", thumbnailScreenshotParameters, this.Width);
        string screenshotTitle = string.Format("Screenshot - {0}", screenshotName);
        string fullscreenUrl = string.Format("/Orion/SEUM/FullscreenScreenshot.aspx?title={0}&{1}", HttpUtility.UrlEncode(screenshotTitle), screenshotParameters);
        string htmlUrl = string.Format("/Orion/SEUM/PageHTML.aspx?{0}", screenshotParameters);

        ScreenshotUrl = screenshotUrl;
        FullscreenUrl = fullscreenUrl;
        HtmlUrl = htmlUrl;
        IsFirst = isFirst;
        IsLast = isLast;

        UpdateInternal();

        // demo server requires copyright information for screenshots
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            WebControl copyrightControl = DemoHelper.GetCopyrightControlForStepScreenshot(Server.MapPath, entityId);
            if (copyrightControl != null)
            {
                copyrightPanel.Controls.Add(copyrightControl);
                copyrightPanel.Visible = true;
            }
        }
    }

    public void SetNoCurrentScreenshot()
    {
        ScreenshotUrl = null;
        UpdateInternal();
    }

    public void SetNoScreenshots()
    {
        ScreenshotUrl = null;

        screenshotImage.Visible = false;
        linksPanel.Visible = false;
        loadingPanel.Visible = false;
        messageLiteral.Text = NoScreenshotsText;
        noDataPanel.Visible = true;

        navigationTableRow.Visible = false;
    }

    public void SetDropDownValue(string value)
    {
        supressEvents = true;
        customDropdown.SelectedValue = value;
        supressEvents = false;
    }

    public void SetChangeButtonVisibility(bool show)
    {
        changeButton.Visible = show;
    }

    #region Properties

    private string ScreenshotUrl
    {
        get;
        set;
    }

    private string FullscreenUrl
    {
        get;
        set;
    }

    private string HtmlUrl
    {
        get;
        set;
    }

    private bool IsFirst
    {
        get;
        set;
    }

    private bool IsLast
    {
        get;
        set;
    }

    public string NoCurrentScreenshotText
    {
        get;
        set;
    }

    public string NoScreenshotsText
    {
        get;
        set;
    }


    /// <summary>
    /// Text used when provided image from DB is not valid image
    /// </summary>
    public string InvalidScreenshotText
    {
        get;
        set;
    }

    public int ParentResourceId
    {
        get;
        set;
    }

    public int Width
    {
        get;
        set;
    }

    public ListItemCollection DropDownItems
    {
        get { return customDropdown.Items; }
    }

    #endregion

    #region Private methods

    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterHandlers();

        var dropdownWidth = this.Width / 2 - 100; // -100 is for change button

        if(dropdownWidth > 0)
            customDropdown.Width = dropdownWidth;
    }

    private void UpdateInternal()
    {
        navigationTableRow.Visible = true;

        if (string.IsNullOrEmpty(ScreenshotUrl))
        {
            // show notice
            screenshotImage.Visible = false;
            linksPanel.Visible = false;
            loadingPanel.Visible = false;
            messageLiteral.Text = NoCurrentScreenshotText;
            noDataPanel.Visible = true;
        }
        else
        {
            loadingPanel.Visible = true;
            screenshotImage.Visible = true;
            linksPanel.Visible = true;
            noDataPanel.Visible = false;

            // setup screenshot and links
            screenshotImage.ImageUrl = ScreenshotUrl;
            fullscreenLink.NavigateUrl = FullscreenUrl;
            htmlLink.NavigateUrl = HtmlUrl;

            screenshotImage.Width = Unit.Percentage(100);
        }

        UpdateNavigationButtons();
    }

    private void RegisterHandlers()
    {
        navLinkFirst.Click += navLinkFirst_Click;
        navLinkPrev.Click += navLinkPrev_Click;
        navLinkNext.Click += navLinkNext_Click;
        navLinkLast.Click += navLinkLast_Click;

        customDropdown.SelectedIndexChanged += customDropdown_SelectedIndexChanged;

        changeButton.Click += changeButton_Click;
    }

    private void UpdateNavigationButtons()
    {
        navLinkLast.Visible = true;
        navLinkNext.Visible = true;
        navLinkFirst.Visible = true;
        navLinkPrev.Visible = true;
        navDivider.Visible = true;

        if (string.IsNullOrEmpty(ScreenshotUrl))
        {
            // show only first and last links
            navLinkLast.Visible = true;
            navLinkNext.Visible = false;
            navLinkFirst.Visible = true;
            navLinkPrev.Visible = false;
            navDivider.Visible = true;
        }
        else
        {
            if (IsLast)
            {
                navLinkLast.Visible = false;
                navLinkNext.Visible = false;
                navDivider.Visible = false;
            }
            if (IsFirst)
            {
                navLinkFirst.Visible = false;
                navLinkPrev.Visible = false;
                navDivider.Visible = false;
            }
        }
    }

    private void OnDropDownValueChanged()
    {
        if (DropDownValueChanged != null)
            DropDownValueChanged(this, new EventArgs<string>(customDropdown.SelectedValue));
    }
    #endregion

    #region Handlers

    void changeButton_Click(object sender, EventArgs e)
    {
        OnDropDownValueChanged();
    }

    void customDropdown_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (supressEvents)
            return;

        OnDropDownValueChanged();
    }

    void navLinkLast_Click(object sender, EventArgs e)
    {
        if (LastClicked != null)
            LastClicked(this, EventArgs.Empty);
    }

    void navLinkNext_Click(object sender, EventArgs e)
    {
        if (NextClicked != null)
            NextClicked(this, EventArgs.Empty);
    }

    void navLinkPrev_Click(object sender, EventArgs e)
    {
        if (PrevClicked != null)
            PrevClicked(this, EventArgs.Empty);
    }

    void navLinkFirst_Click(object sender, EventArgs e)
    {
        if (FirstClicked != null)
            FirstClicked(this, EventArgs.Empty);
    }

    #endregion
}