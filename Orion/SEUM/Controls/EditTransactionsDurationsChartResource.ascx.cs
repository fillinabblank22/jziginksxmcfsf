﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Charting;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_EditTransactionsDurationsChartResource : SEUMBaseResourceEditControl
{
    protected const string timePeriodLabel = "Time Period for Chart";
    protected const string customPeriodFromLabel = "From date and time";
    protected const string customPeriodToLabel = "To date and time";

    protected const string sampleIntervalLabel = "Sample Interval";
    
    protected const string widthLabel = "Chart Width";
    protected const string heightLabel = "Chart Height";
    protected const string dimensionHelpText = "You can set this dimension to zero to let chart use autosize for it.";

    protected const string dateNotValidError = "Entered date has wrong format.";

    protected const string sampleIntervalHelpText = "A single point will be plotted for each time period. Data within each sample is automatically summarized."
                                                  + "If you select period shorter than your transaction interval, chart can contain gaps.";

    protected const string numberOfItemsLabel = "Maximum Number of Items to Display";
    protected const string numberMustBeFilledError = "Number must be filled in";
    protected const string numberError = "Value must be a number between 1 and 1000.";
    protected const string numberError2 = "Value must be a number between 1 and 10000.";

    protected List<ListItem> intervals;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        integerValueValidate.ErrorMessage = numberError;
        integerValueValidate2.ErrorMessage = numberError2;
        integerValueValidate3.ErrorMessage = numberError2;

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.MaxCount]))
            maxCountBox.Text = Resource.Properties[ResourcePropertiesKeys.MaxCount];
        else
            maxCountBox.Text = "5";

        foreach (TimePeriod period in TimePeriod.Periods)
        {
            timePeriodCombo.Items.Add(new ListItem(period.Label, period.Value.ToString()));
        }

        intervals = new List<ListItem>();
        foreach (SampleInterval interval in SampleInterval.Intervals)
        {
            ListItem item = new ListItem(interval.Label, interval.Value.ToString());
            intervals.Add(item);
            sampleIntervalCombo.Items.Add(item);
        }

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.TimePeriod]))
            timePeriodCombo.SelectedValue = Resource.Properties[ResourcePropertiesKeys.TimePeriod];
        else
            timePeriodCombo.SelectedValue = TimePeriod.Today.Value.ToString();

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.SampleInterval]))
            sampleIntervalCombo.SelectedValue = Resource.Properties[ResourcePropertiesKeys.SampleInterval];
        else
            sampleIntervalCombo.SelectedValue = TimePeriod.Today.DefaultSampleInterval.Value.ToString();

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.TimePeriodCustomFrom]))
        {
            DateTime customPeriodFrom;
            if (DateTime.TryParse(Resource.Properties[ResourcePropertiesKeys.TimePeriodCustomFrom], CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out customPeriodFrom))
                customPeriodFromBox.Text = customPeriodFrom.ToLocalTime().ToString();
        }

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.TimePeriodCustomTo]))
        {
            DateTime customPeriodTo;
            if (DateTime.TryParse(Resource.Properties[ResourcePropertiesKeys.TimePeriodCustomTo], CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out customPeriodTo))
                customPeriodToBox.Text = customPeriodTo.ToLocalTime().ToString();
        }

        customPeriodFromValidator.ServerValidate += customPeriodValidator_ServerValidate;
        customPeriodFromValidator.ErrorMessage = dateNotValidError;
        customPeriodToValidator.ServerValidate += customPeriodValidator_ServerValidate;
        customPeriodToValidator.ErrorMessage = dateNotValidError;

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Width]))
            widthTextBox.Text = Resource.Properties[ResourcePropertiesKeys.Width];
        else
            widthTextBox.Text = "0";

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Height]))
            heightTextBox.Text = Resource.Properties[ResourcePropertiesKeys.Height];
        else
            heightTextBox.Text = "0";

        // Register JavaScript
        // --------------------------------------------------------------------
        StringBuilder stringBuilder = new StringBuilder("");
        foreach (var item in intervals)
        {
            stringBuilder.AppendFormat("['{0}','{1}'],", item.Text, item.Value);
        }

        string script =
            String.Format(
                @"
    var intervals = [{0}
        []
    ];

    function UpdateIntervals()
    {{
        var timePeriodCombo = document.getElementById('{1}');
        var sampleIntervalCombo = document.getElementById('{2}');
        var selectedValue = sampleIntervalCombo.value;

        var period = parseInt(timePeriodCombo.value);

        var intervalsFrom = 0;
        var intervalsTo = intervals.length - 2; // there is exptra empty element ad the end of intervals array

        if (period <= {3}) {{
                intervalsTo = 4;
        }} else if (period <= {4}) {{
                intervalsTo = 5;
        }} else if (period <= {5}) {{
                intervalsTo = 8;
        }} else if (period <= {6}) {{
                intervalsFrom = 1;
                intervalsTo = 9;
        }} else if (period <= {7}) {{
                intervalsFrom = 3;
        }} else if (period <= {8}) {{
                intervalsFrom = 7;
        }} 
        
        if (period == {9}) {{
            $('#customPeriodFrom').show();
            $('#customPeriodTo').show();
        }} else {{
            $('#customPeriodFrom').hide();
            $('#customPeriodTo').hide();
        }}



        while (sampleIntervalCombo.options.length>0)
        {{
            sampleIntervalCombo.remove(sampleIntervalCombo.options.length-1);
        }}

        for (var i = intervalsFrom, j=0; i <= intervalsTo; i++, j++)
        {{
            sampleIntervalCombo.options[j] = new Option(intervals[i][0], intervals[i][1], false, (intervals[i][1] == selectedValue));
        }}
    }}

    UpdateIntervals();",
                stringBuilder.ToString(), timePeriodCombo.ClientID, sampleIntervalCombo.ClientID,
                TimePeriod.PastHour.Value, TimePeriod.Last2Hours.Value, TimePeriod.Yesterday.Value,
                TimePeriod.Last30Days.Value, TimePeriod.Last3Months.Value, TimePeriod.Last12Months.Value,
                TimePeriod.Custom.Value);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "SEUM_EditTransactionsDurationsChartJS", script, true);
        // -------------------------------------------------------------------- // Register JavaScript

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            timePeriodCombo.Attributes.Add("onchange", "UpdateIntervals();SaveData('" + ResourcePropertiesKeys.TimePeriod + "', this.value);");
            sampleIntervalCombo.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.SampleInterval + "', this.value);");
            customPeriodFromBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.TimePeriodCustomFrom + "', this.value);");
            customPeriodToBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.TimePeriodCustomTo + "', this.value);");
            widthTextBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Width + "', this.value);");
            heightTextBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Height + "', this.value);");
        }
    }

    void customPeriodValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (timePeriodCombo.SelectedValue != TimePeriod.Custom.Value.ToString())
            return;

        DateTime customPeriodFrom;
        if (!DateTime.TryParse(customPeriodFromBox.Text, out customPeriodFrom))
        {
            ((CustomValidator) source).ErrorMessage = dateNotValidError;
            args.IsValid = false;
        }
    }

    protected string DateFormat
    {
        get
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
        }
    }

    protected string TimeFormat
    {
        get
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern;
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            properties.Add(ResourcePropertiesKeys.MaxCount, maxCountBox.Text);
            properties.Add(ResourcePropertiesKeys.TimePeriod, timePeriodCombo.SelectedValue);
            properties.Add(ResourcePropertiesKeys.SampleInterval, sampleIntervalCombo.SelectedValue);

            if (timePeriodCombo.SelectedValue == TimePeriod.Custom.Value.ToString())
            {
                DateTime customPeriodFrom;
                if (DateTime.TryParse(customPeriodFromBox.Text, out customPeriodFrom))
                    properties.Add(ResourcePropertiesKeys.TimePeriodCustomFrom, customPeriodFrom.ToUniversalTime().ToString(DateTimeFormatInfo.InvariantInfo));

                DateTime customPeriodTo;
                if (DateTime.TryParse(customPeriodToBox.Text, out customPeriodTo))
                    properties.Add(ResourcePropertiesKeys.TimePeriodCustomTo, customPeriodTo.ToUniversalTime().ToString(DateTimeFormatInfo.InvariantInfo));
            }
            else
            {
                properties.Add(ResourcePropertiesKeys.TimePeriodCustomFrom, string.Empty);
                properties.Add(ResourcePropertiesKeys.TimePeriodCustomTo, string.Empty);
            }

            int width = 0;
            Int32.TryParse(widthTextBox.Text, out width);
            if (width >= 0)
                properties.Add(ResourcePropertiesKeys.Width, width);

            int height = 0;
            Int32.TryParse(heightTextBox.Text, out height);
            if (height >= 0)
                properties.Add(ResourcePropertiesKeys.Height, height);

            return properties;
        }
    }
}
