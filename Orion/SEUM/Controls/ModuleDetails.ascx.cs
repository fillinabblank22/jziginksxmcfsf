﻿using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Controls_ModuleDetails : System.Web.UI.UserControl
{
    const string Unlimited = "Unlimited";

    private static Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo("WPM");
            }
            catch (Exception ex)
            {
                _log.Error("Error while displaying details for WPM module.", ex);
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            SEUMDetails.Name = module.ProductDisplayName;

            values.Add("Product Name", module.ProductName);
            values.Add("Version", module.Version);
            values.Add("Service Pack", String.IsNullOrEmpty(module.HotfixVersion) ? "None" : module.HotfixVersion);
            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add("License", module.LicenseInfo);

            AddSEUMInfo(values);

            SEUMDetails.DataSource = values;
            break; // we support only one "Primary" engine
        }
    }

    private void AddSEUMInfo(Dictionary<string, string> values)
    {
        try
        {
            DataTable licenseSummary;

            using (var proxy = BusinessLayerFactory.Create())
            {
                licenseSummary = proxy.GetLicenseSummary();
            }

            values.Add("Allowed Number of Transactions", (string)licenseSummary.Rows[0]["LicenseLimitDisplay"]);
            values.Add("Total Number of Transactions", licenseSummary.Rows[0]["MonitorCount"].ToString());
            values.Add("Licensed Transactions", licenseSummary.Rows[0]["LicensedMonitors"].ToString());
            values.Add("Unlicensed Transactions", licenseSummary.Rows[0]["UnlicensedMonitors"].ToString());
            values.Add("Available Transactions", (string)licenseSummary.Rows[0]["AvailableMonitorsDisplay"]);
        }
        catch (Exception ex)
        {
            _log.Error("Cannot get number of licensed elements", ex);
        }
        
    }
}