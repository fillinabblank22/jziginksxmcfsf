﻿using System;
using System.Collections.Generic;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_EditLocationDetailsResource : SEUMBaseResourceEditControl
{
    protected const string hideUnmanagedLabel = "Hide Unmanaged Transactions";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        hideUnmanagedCheckBox.Text = hideUnmanagedLabel;

        bool hideUnmanaged = true;
        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.HideUnmanaged]))
        {
            if (!Boolean.TryParse(Resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "true", out hideUnmanaged))
                hideUnmanaged = true;
        }

        hideUnmanagedCheckBox.Checked = hideUnmanaged;

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            hideUnmanagedCheckBox.Attributes.Add("onclick", "SaveData('" + ResourcePropertiesKeys.HideUnmanaged + "', this.checked ? 'true' : 'false');");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            properties[ResourcePropertiesKeys.HideUnmanaged] = hideUnmanagedCheckBox.Checked.ToString();

            return properties;
        }
    }
}