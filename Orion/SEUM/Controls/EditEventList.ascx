﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditEventList.ascx.cs"
    Inherits="Orion_SEUM_Controls_EditEventList" %>

<p>
    <b><%= maxEventsLabel %>:</b><br />
    <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />

    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="* Events number has to be positive integer less than 5000."
                Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
</p>

<p>
    <b><%= timePeriodLabel %>:</b><br />
     <asp:DropDownList runat="server" ID="Period" />
</p>
