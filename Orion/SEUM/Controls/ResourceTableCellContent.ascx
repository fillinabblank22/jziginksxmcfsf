﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceTableCellContent.ascx.cs" Inherits="Orion_SEUM_Controls_ResourceTableCellContent" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Controls.ResourceTable" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="FormattedValue" Src="~/Orion/SEUM/Controls/FormattedValue.ascx" %>

<% if (Cell.IsNetObject) { %>
    <orion:EntityStatusIcon ID="entityStatusIcon" runat="server" IconSize="Small" WrapInBox="false" />

    <% if (Cell.IsTransactionNetObject) { %>
        <!-- Transaction from Location -->
        <a href="<%= System.Web.HttpUtility.HtmlEncode(NetObject.DetailsUrl) %>"><%= System.Web.HttpUtility.HtmlEncode(TransactionNetObject.RecordingName) %></a>
        <span style="vertical-align:middle;"><%# ResourceTableTransactionEntity.TransactionLocationDelimiter %></span>
        <a href="<%= System.Web.HttpUtility.HtmlEncode(TransactionNetObject.AgentDetailsUrl) %>"><%= System.Web.HttpUtility.HtmlEncode(TransactionNetObject.AgentName) %></a>
    <% } else { %>
        <a href="<%= NetObject.DetailsUrl %>"><%= NetObject.Name %></a>
    <% } %> 
<% } else if(Cell.HasControl) { %>
    <asp:PlaceHolder runat="server" ID="controlPlaceholder" />
<% } else { %>
    <SEUM:FormattedValue runat="server" ID="formattedValue" />
<% } %> 