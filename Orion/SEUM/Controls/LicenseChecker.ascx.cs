﻿using SolarWinds.Logging;
using System;
using System.Web;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Controls_LicenseChecker : System.Web.UI.UserControl
{
    private static readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                proxy.ValidateLicense();
            }
        }
        catch (SEUMBusinessLayerException ex)
        {
            log.Error("Unable to connect to Business layer to validate license", ex);
            Server.Transfer(string.Format("/Orion/SEUM/LicenseError.aspx?msg={0}", HttpUtility.UrlEncode(ex.Message)));
        }
        catch (Exception ex)
        {
            log.Error("License is not valid", ex);
            Server.Transfer(string.Format("/Orion/SEUM/LicenseError.aspx?msg={0}", HttpUtility.UrlEncode(ex.Message)));
        }
    }
}