﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_SEUM_Controls_AgentsGrid : System.Web.UI.UserControl
{
    protected const string NameLabel = "Name for player at this location";
    protected const string IPLabel = "IP Address or Hostname where the player is installed";
    protected const string AdvancedLabel = "Advanced";
    protected const string PortLabel = "Player port";
    protected const string PasswordLabel = "Player password";
    protected const string WhatIsThisLabel = "What is this?";
    protected const string WhatIsProxyLabel = "What is a proxy?";
    protected const string TroubleshootingLabel = "Troubleshooting";
    protected const string LoadingText = "Loading";
    protected const string LogLevelLabel = "Log level";
    protected const string DiagnosticsLabel = "Diagnostics";
    protected const string GenerateNewDiagnosticsLabel = "Generate new diagnostics";
    protected const string LocationNotRespondingError = "Player is not responding. Remote troubleshooting is not available.";
    protected const string AssignToPollerLabel = "Assign to an Orion Poller";
    protected const string PlayerNotFoundError = "WPM Player not found. Install the WPM Player on the remote computer and then try again.";
    protected const string LearnMoreLabel = "Learn More";
    protected const string AgentGuidLabel = "Player GUID";
    protected const string UseProxyLabel = "Use proxy";
    protected const string UpdateRemoteSettingsLabel = "Update player port and password directly on player location";

    protected const string ChooseExistingOrCreateNewOneText = "Choose existing credential from the library or create new one:";
    protected const string ChooseCredentialText = "Choose Credential:";
    protected const string UserNameText = "User Name:";
    protected const string PasswordText = "Password:";

    private bool isEditGrid = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        PollingEngines = new AgentsDAL().GetAvailablePollingEngines();
    }

    public int PageSize
    {
        // TODO
        get { return 20; }
    }

    public bool IsEditGrid
    {
        get { return isEditGrid; }
        set { isEditGrid = value; }
    }

    protected String SelectedAgentsFieldClientID
    {
        get { return selectedAgents.ClientID; }
    }

    public IEnumerable<Int32> SelectedAgents
    {
        get
        {
            string[] ids = selectedAgents.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string idStr in ids)
            {
                int id = 0;
                if (Int32.TryParse(idStr, out id))
                    yield return id;
            }

            yield break;
        }
        set
        {
            selectedAgents.Value = string.Join(",", value.Select(id => id.ToString()).ToArray());
        }
    }

    protected string ExcludedAgentsString
    {
        get
        {
            // -1 is for "waiting agents" and it should always be disabled
            if (ExcludedAgentsIds == null)
                return "-1";

            return string.Join(",", ExcludedAgentsIds.Concat(new[] { -1 }).Select(id => id.ToString()).ToArray());
        }
    }

    public IEnumerable<Int32> ExcludedAgentsIds
    {
        get; set;
    }

    protected string UnsupportedAgentsString
    {
        get
        {
            if (UnsupportedAgentsIds == null)
                return string.Empty;

            return string.Join(",", UnsupportedAgentsIds.Select(id => id.ToString()).ToArray());
        }
    }

    public IEnumerable<Int32> UnsupportedAgentsIds
    {
        get;
        set;
    }

    public string ReturnUrl { get; set; }

    public IEnumerable<KeyValuePair<int, string>> PollingEngines { get; private set; }

    protected string LearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsPlaybackLocations");
        }
    }

    protected string InstallingPlayerHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGInstallingPlayer");
        }
    }
}