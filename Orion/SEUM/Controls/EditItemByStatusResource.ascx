﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditItemByStatusResource.ascx.cs" Inherits="Orion_SEUM_Controls_EditItemByStatusResource" %>
<%@ Register TagPrefix="SEUM" TagName="FilterEditor" Src="~/Orion/SEUM/Controls/FilterEditor.ascx" %>

<p>
    <b>Statuses to filter by:</b><br />
    <asp:CheckBoxList runat="server" ID="statusesList" />
</p>

<SEUM:FilterEditor runat="server" ID="filterEditor" />