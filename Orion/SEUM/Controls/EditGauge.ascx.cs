﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.SEUM.Web.Resources;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Web;
using System.Globalization;

public partial class Orion_SEUM_Controls_EditGauge : SEUMBaseResourceEditControl
{
    protected const string selectStyleLabel = "Select Style for Gauges";
    protected const string gaugeSizeLabel = "Gauge Size 30% to 250%";
    protected const string rangeLabel = "Gauge Range";
    protected const string staticRangeLabel = "Maximal Value in Seconds";
    protected const string dynamicRangeLabel = "Limits for Dynamic Range in Seconds";
    protected const string thresholdMultiplierLabel = "Upper Threshold Multiplier";

    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        rangeDropDown.Items.Add(new ListItem("Dynamic", "dynamic"));
        rangeDropDown.Items.Add(new ListItem("Static", "static"));
        rangeDropDown.Items.Add(new ListItem("Thresholds dependent", "thresholds"));

        _properties.Add(ResourcePropertiesKeys.Scale, string.Empty);
        _properties.Add(ResourcePropertiesKeys.Style, string.Empty);
        _properties.Add(ResourcePropertiesKeys.RangeMethod, string.Empty);
        _properties.Add(ResourcePropertiesKeys.StaticRange, string.Empty);
        _properties.Add(ResourcePropertiesKeys.DynamicRange, string.Empty);
        _properties.Add(ResourcePropertiesKeys.ThresholdsRange, string.Empty);

        //Set scale input
        string scale = Resource.Properties[ResourcePropertiesKeys.Scale];
        if (string.IsNullOrEmpty(scale))
            scale = "100";
        scaleInput.Text = scale;

        //Set styles combobox
        List<string> styles = GaugeHelper.GetAllGaugeStyles(GaugeType.Radial);
        stylesList.DataSource = styles;
        stylesList.DataBind();
        string style = Resource.Properties[ResourcePropertiesKeys.Style];
        if (string.IsNullOrEmpty(style))
            style = "Elegant Black";
        stylesList.SelectedValue = style;

        // Set Range method
        string rangeMethod = Resource.Properties[ResourcePropertiesKeys.RangeMethod];
        if (string.IsNullOrEmpty(rangeMethod))
            rangeMethod = "dynamic";
        rangeDropDown.SelectedValue = rangeMethod;

        // Set Static range
        string staticRange = Resource.Properties[ResourcePropertiesKeys.StaticRange];
        if (string.IsNullOrEmpty(staticRange))
            staticRange = "30";
        staticMaxValueBox.Text = staticRange;

        // Set Dynamic range
        string dynamicRange = Resource.Properties[ResourcePropertiesKeys.DynamicRange];
        if (string.IsNullOrEmpty(dynamicRange))
            dynamicRange = "10, 20, 40, 60, 100, 200, 400, 600, 1000";
        dynamicValuesBox.Text = dynamicRange;

        // Set Thresholds range (thresholds are saved as strings with invariant culture)
        double thresholdRange;
        string thresholdsRangeText = Resource.Properties[ResourcePropertiesKeys.ThresholdsRange];
        if (string.IsNullOrEmpty(thresholdsRangeText))
            thresholdRange = 1.3;
        else
            thresholdRange = Convert.ToDouble(thresholdsRangeText, CultureInfo.InvariantCulture);
        // Threshold is displayed with current culture
        thresholdsMultiplierBox.Text = thresholdRange.ToString();

        //Set sample gauges panel
        foreach (string gaugeStyle in styles)
        {
            Gauge gauge = new Gauge();
            gauge.GaugeStyle = gaugeStyle;
            gauge.IsSample = true;
            GaugeStylesPanel.Controls.Add(gauge);
        }

        // Register JavaScript
        // --------------------------------------------------------------------

        string script = String.Format(@"function UpdateRangeFields()
    {{
        var rangeCombo = $('#{0}');
        switch (rangeCombo.val()) {{
            case 'static':
                $('#staticRangeFields').show();
                $('#dynamicRangeFields').hide();
                $('#multiplierFields').hide();
                break;
            case 'dynamic':
                $('#staticRangeFields').hide();
                $('#dynamicRangeFields').show();
                $('#multiplierFields').hide();
                break;
            case 'thresholds':
                $('#staticRangeFields').hide();
                $('#dynamicRangeFields').hide();
                $('#multiplierFields').show();
                break;
        }}
    }}

    SelectCurrentGauge();
    UpdateRangeFields();

    $(function () {{
        $('.ExpanderLink').toggle(
                    function () {{
                        $('img:first', this).attr('src', '/Orion/images/Button.Collapse.gif');
                        $('#{1}').slideDown('fast');
                    }},
                    function () {{
                        $('img:first', this).attr('src', '/Orion/images/Button.Expand.gif');
                        $('#{1}').slideUp('fast');
                    }}
                )
    }});", rangeDropDown.ClientID, GaugeStylesPanel.ClientID);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "SEUM_EditGaugeJS", script, true);
        // -------------------------------------------------------------------- // Register JavaScript

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            rangeDropDown.Attributes.Add("onchange", "UpdateRangeFields();SaveData('" + ResourcePropertiesKeys.RangeMethod + "', this.value);");
            staticMaxValueBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.StaticRange + "', this.value);");
            dynamicValuesBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.DynamicRange + "', this.value);");
            thresholdsMultiplierBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.ThresholdsRange + "', this.value);");
            scaleInput.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Scale + "', this.value);");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties[ResourcePropertiesKeys.Scale] = scaleInput.Text;
            _properties[ResourcePropertiesKeys.Style] = stylesList.SelectedValue;
            _properties[ResourcePropertiesKeys.RangeMethod] = rangeDropDown.SelectedValue;

            int staticRange = 0;
            if (!Int32.TryParse(staticMaxValueBox.Text, out staticRange))
                staticRange = 30;
            _properties[ResourcePropertiesKeys.StaticRange] = staticRange;

            string[] dynamicRangeStr = dynamicValuesBox.Text.Split(',');
            List<int> dynamicRange = new List<int>(dynamicRangeStr.Length);
            foreach (string value in dynamicRangeStr)
            {
                int val;
                if (Int32.TryParse(value.Trim(), out val))
                {
                    dynamicRange.Add(val);
                }
            }
            _properties[ResourcePropertiesKeys.DynamicRange] = string.Join(",", dynamicRange.OrderBy(x => x).Select(x => x.ToString()).ToArray());

            double thresholdsMultiplier = 0;
            if (!Double.TryParse(thresholdsMultiplierBox.Text, out thresholdsMultiplier))
                thresholdsMultiplier = 1.3;
            // Value is saved as string, so we must convert double values to string using invariant culture
            _properties[ResourcePropertiesKeys.ThresholdsRange] = thresholdsMultiplier.ToString(CultureInfo.InvariantCulture);

            return _properties;
        }
    }
}