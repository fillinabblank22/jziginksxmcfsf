﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Web.Configuration;
using SolarWinds.SEUM.Web.Controls.ResourceTable;

public partial class Orion_SEUM_Controls_LoadIndicator : System.Web.UI.UserControl, IResourceTableControl
{
    protected const string PercentFormat = @"{0:0}%";
    protected const string LearnMoreText = "Learn more";

    private int _warningThreshold = SEUMWebConfiguration.Instance.AgentLoadWarningThreshold;
    private int _errorThreshold = SEUMWebConfiguration.Instance.AgentLoadErrorThreshold;

    private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

    [PersistenceMode(PersistenceMode.Attribute)]
    public double LoadPercentage { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int WarningThreshold
    {
        get { return _warningThreshold; }
        set { _warningThreshold = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int ErrorThreshold
    {
        get { return _errorThreshold; }
        set { _errorThreshold = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowHelpLink { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string HelpLinkFragment { get; set; }

    protected string HelpLink
    {
        get
        {
            if (string.IsNullOrEmpty(HelpLinkFragment))
                return String.Empty;

            return HelpHelper.GetHelpUrl(HelpLinkFragment);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_parameters.ContainsKey("Value"))
        {
            LoadPercentage = (double)_parameters["Value"];
        }

        percentLabel.Text = String.Format(PercentFormat, LoadPercentage);

        // setting status bar levels to 100 will disable them completely
        var statusBarWarningLevel = 100;
        var statusBarErrorLevel = 100;

        if(ErrorThreshold <= 100)
        {
            statusBarErrorLevel = ErrorThreshold - 1;   // minus one because status bar uses > instead of >=
        }
        else if(ErrorThreshold <= LoadPercentage)
        {
            statusBarErrorLevel = 99;   // because the percentage will be trimmed to 100 in the status bar, setting error level to 99 will trigger the proper color change
        }

        if(WarningThreshold <= 100)
        {
            statusBarWarningLevel = WarningThreshold - 1;
        }
        else if (WarningThreshold <= LoadPercentage)
        {
            statusBarWarningLevel = 99;
        }

        statusBar.WarningLevel = statusBarWarningLevel;
        statusBar.ErrorLevel = statusBarErrorLevel;
        statusBar.Status = (int)LoadPercentage;

        if(LoadPercentage >= ErrorThreshold)
            percentLabel.CssClass += " Error";
        else if (LoadPercentage >= WarningThreshold)
            percentLabel.CssClass += " Warning";
    }

    public Dictionary<string, object> Parameters
    {
        get { return _parameters; }
    }

    public Control GetControl()
    {
        return this;
    }

    public object Clone()
    {
        return LoadControl("~/Orion/SEUM/Controls/LoadIndicator.ascx");
    }
}