﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Controls_ActiveAlertsControl : System.Web.UI.UserControl
{
    public bool ShowAcknowledgedAlerts { get; set; }
    public bool ShowItemName { get; set; }
    public IEnumerable<int> ItemIds { get; set; }
    public string ItemType { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        AlertsDAL dal = new AlertsDAL();

        DataTable data;
        switch (ItemType)
        {
            case NetObjectPrefix.Transaction:
                data = dal.GetTransactionAlerts(ItemIds, this.ShowAcknowledgedAlerts);
                break;
            case NetObjectPrefix.TransactionStep:
                data = dal.GetStepAlerts(ItemIds, this.ShowAcknowledgedAlerts);
                break;
            case NetObjectPrefix.Location:
                data = dal.GetAgentAlerts(ItemIds, this.ShowAcknowledgedAlerts);
                break;
            default:
                data = dal.GetAllAlerts(this.ShowAcknowledgedAlerts);
                break;
        }

        alertsRepeater.DataSource = data;
        alertsRepeater.DataBind();
    }
}