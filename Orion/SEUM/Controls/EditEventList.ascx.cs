﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_EditEventList : BaseResourceEditControl
{
	private static int defaultMaxEventCount = 25;
	private const string defaultPeriod = "Last 12 Months";
    protected const string maxEventsLabel = "Maximum Number of Events";
    protected const string timePeriodLabel = "Selected Time Period";

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		// fill in period list
		List<string> plist = Periods.GenerateSelectionList();
		this.Period.DataSource = plist;
		this.Period.DataBind();

		// load values from resources

		this.MaxEventsText.Text = Resource.Properties[ResourcePropertiesKeys.MaxCount] ?? defaultMaxEventCount.ToString();
		this.Period.Text = Resource.Properties[ResourcePropertiesKeys.TimePeriod] ?? defaultPeriod;

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            Period.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.TimePeriod + "', this.value);");
            MaxEventsText.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.MaxCount + "', this.value);");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			try
			{
				Int32.Parse(this.MaxEventsText.Text);
				properties.Add(ResourcePropertiesKeys.MaxCount, this.MaxEventsText.Text);
			}
			catch
			{
				properties.Add(ResourcePropertiesKeys.MaxCount, defaultMaxEventCount.ToString());
			}

			properties.Add(ResourcePropertiesKeys.TimePeriod, this.Period.Text);
			return properties;
		}
	}

    public override bool ShowSubTitle
    {
        get { return false; }
    }
}
