﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Controls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Exceptions;
using SolarWinds.SEUM.Web.Helpers;

public partial class Orion_SEUM_Controls_EditLocationNodesControl : System.Web.UI.UserControl
{
    #region Constants

    protected const string TitlePattern = "Location status troubleshooting{0}";
    protected const string DefaultReturnUrl = "/Orion/SEUM/Admin/ManagePlayers.aspx";

    #endregion

    private IDictionary<int, IEnumerable<string>> agentNodes;

    private string returnUrl = null;

    private Agent agent;

    private IEnumerable<Agent> agents;

    protected bool IsSingle;

    public IEnumerable<int> AgentIds { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        AgentsDAL agentsDal = new AgentsDAL();
        agents = agentsDal.GetAgents(AgentIds);
        IsSingle = agents.Count() == 1;
        agent = IsSingle ? agents.First() : null;
        BindControls();
    }

    private void BindControls()
    {
        Page.Title = String.Format(TitlePattern, IsSingle ? ": " + agent.Name : String.Empty);
        if (!Page.IsPostBack)
        {
            agentNodes = new AgentsDAL().GetAgentRelatedEntities(agents.Select(a => a.AgentId));
            nodesRepeater.DataSource = agents;
            nodesRepeater.DataBind();
        }
    }


    protected void nodesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemIndex < 0)
            return;

        Agent agent = e.Item.DataItem as Agent;

        ImageButton expandButton = e.Item.FindControl("expandButton") as ImageButton;
        Label locationLabel = e.Item.FindControl("locationLabel") as Label;
        Panel detailsPanel = e.Item.FindControl("detailsPanel") as Panel;
        AssignObjectsControl nodesSelector = e.Item.FindControl("nodesSelector") as AssignObjectsControl;

        if (IsSingle)
        {
            expandButton.Parent.Controls.Remove(expandButton);
            locationLabel.Parent.Controls.Remove(locationLabel);
        }
        else
        {
            locationLabel.Text = agent.Name;
            expandButton.OnClientClick = string.Format("ToggleDetails('{0}','{1}'); return false;", expandButton.ClientID, detailsPanel.ClientID);
        }
        nodesSelector.EntityType = SwisEntities.Nodes;
        nodesSelector.SelectedObjectUris = agentNodes.ContainsKey(agent.AgentId) ?
            new DependenciesDAL().FilterSwisUrisByEntityType(agentNodes[agent.AgentId], SwisEntities.Nodes) : 
            Enumerable.Empty<String>();
    }

    public void Save()
    {
        if (ShowRelatedEntities.Checked)
        {
            int index = 0;
            foreach (Agent agent in agents)
            {
                RepeaterItem item = nodesRepeater.Items[index++];
                AssignObjectsControl nodesSelector = item.FindControl("nodesSelector") as AssignObjectsControl;

                RelatedEntitiesHelper.SaveAgentRelatedNodes(agent, nodesSelector.SelectedObjectUris);
            }
        }
    }
}