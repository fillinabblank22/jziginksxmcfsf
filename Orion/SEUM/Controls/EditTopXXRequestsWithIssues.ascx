﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTopXXRequestsWithIssues.ascx.cs" Inherits="Orion_SEUM_Controls_EditTopXXRequestsWithIssues" %>
<%@ Register TagPrefix="SEUM" TagName="FilterEditor" Src="~/Orion/SEUM/Controls/FilterEditor.ascx" %>

<p>
    <b><%= numberOfItemsLabel %>:</b><br />
    <asp:TextBox runat="server" ID="maxCountBox" Rows="1" Columns="30" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="maxCountBox" Display="Dynamic"
        SetFocusOnError="true">* <%= numberMustBeFilledError %></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="maxCountBox"
        Display="Dynamic" Type="Integer" MaximumValue="1000" MinimumValue="1"
        SetFocusOnError="true">* <%= numberError %></asp:RangeValidator>
</p>

<p>
    <b><%= statusCategoryLabel %>:</b><br />
    <asp:CheckBoxList runat="server" ID="statusesList" />
</p>

<SEUM:FilterEditor runat="server" ID="filterEditor" />
        
