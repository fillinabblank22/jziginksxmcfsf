﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryCentralPlugin.ascx.cs" Inherits="Orion_SEUM_Controls_DiscoveryCentralPlugin" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl= "~/Orion/SEUM/images/Icon.Recording.Discovery.Central.svg"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2><%= TitleText %></h2>
    <div>
        <ol type="1">
          <li style="font-weight:bold;">
             <b><%= ListItem1 %>.</b>
             <ol type="a" style="padding-top:8px;font-weight:normal;">
                <li style="padding-top:5px;">
                   <%= ListItem1SubAPart1 %><a href="<%= CommonLinks.ChromiumRecorderInstaller %>" class="SEUM_HelpLink" target="_blank"><%= CommonTexts.DownloadNewRecorderText %></a>.
                   <%= ListItem1SubARest %>
                   <orion:HelpLink ID="WpmNewRecorderHelpLink" runat="server" HelpUrlFragment="OrionWPM30Recorder"
                                    HelpDescription="&#0187;&nbsp;Learn more about the new WPM Recorder" CssClass="helpLink" />
                </li>
                <li style="padding-top:10px;">
                   <%= ListItem1SubB %><br /><%= Click %> <asp:Image ID="Image2" runat="server" style="vertical-align:middle;" ImageUrl= "~/Orion/SEUM/images/icon_record.png"/><b><%= Record %></b>.                  
                </li>
                <li style="padding-top:10px;padding-bottom:10px;">
                   <%= ListItem1SubCpre %> <asp:Image ID="Image3" runat="server" style="vertical-align:middle;" ImageUrl= "~/Orion/SEUM/images/icon_stop.png"/><b><%= Stop %></b> <%= ListItem1SubC %> <asp:Image ID="Image4" runat="server" style="vertical-align:middle;" ImageUrl= "~/Orion/SEUM/images/icon_save.png"/> <b><%= Save %></b>.
                </li>
             </ol>
          </li>
          <li style="font-weight:bold;" >
            <b><%= ListItem2 %></b> <a href="<%= LearnMoreHelpUrl %>" class="helpLink" target="_blank">&#0187;&nbsp;<%= LearnMoreText %></a> 
            <ol type="a" type="a" style="font-weight:normal;">
              <li style="padding-top:10px;">
                 <%= ListItem2SubA %>                 
              </li>
              <li style="padding-top:10px;">
                 <%= ListItem2SubB %>
                 <br />
              </li>
              <li style="padding-top:10px;">
                  <%= ListItem2SubC %>
                  <orion:HelpLink ID="WpmNewPlayerHelpLink" runat="server" HelpUrlFragment="OrionSEUMAGInstallingPlayer"
                                  HelpDescription="&#0187;&nbsp;Learn more about the WPM Player" CssClass="helpLink" />
                  <br />
              </li>
              <li style="padding-top:10px;">
                  <%= ListItem2SubD %>
                  <br />
              </li>
              <li style="padding-top:10px;">
                  <%= ListItem2SubE %>
                  <br />
              </li>
              <li style="padding-top:10px;">
                  <%= ListItem2SubF %>
                  <br />
              </li>
            </ol>
          </li>
        </ol>               
    </div>      
    <orion:ManagedNetObjectsInfo ID="monitorsInfo" runat="server" EntityName="Transactions" NumberOfElements="<%# EnabledMonitorCount %>" />    
    
    <orion:LocalizableButton ID="addPlaybackButton" LocalizedText="CustomText" Text="Add a Transaction Monitor" runat="server" OnClick="Button_Click" />
</div>
