﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsTable.ascx.cs" Inherits="Orion_SEUM_Controls_ApplicationsTable" %>
<%@ Import Namespace="SolarWinds.SEUM.Licensing" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>    
    <% if (LicenseInfo.AreApplicationsAvailable)
    { %>
        <div class="sw-seum-custom-query-table-wrapper">
            <orion:CustomQueryTable runat="server" ID="ApplicationsTable"/>
        </div>
        <script type="text/javascript">
            SW.Core.Resources.CustomQuery.initialize({
                uniqueId: "<%= ApplicationsTable.UniqueClientID %>",
                allowSearch: false,
            });

            SW.Core.Resources.CustomQuery.refresh("<%= ApplicationsTable.UniqueClientID %>");
        </script>
    <% } else { %>
    <% =ApplicationsAreNotAvailable %>
    <% } %>
