﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Configuration;
using System.Threading;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Controls_TransactionPropertiesEditor : System.Web.UI.UserControl
{
    public const string InvalidFrequencyTextFormat = "Playback interval must be between {0} seconds and {1} days.";
    public const string InvalidOptimalThresholdText = "Typical value must be less than or equal to warning threshold.";
    public const string InvalidWarningThresholdText = "Warning threshold must be less than or equal to critical threshold.";
    public const string InvalidCriticalThresholdTextFormat = "Critical threshold must be between {0} and {1} seconds.";
    public const string InvalidWarningThresholdTextFormat = "Warning threshold must be between {0} and {1} seconds.";
    public const string InvalidOptimalThresholdTextFormat = "Typical value must be between {0} and {1} seconds.";
    
    public const string LicenseSummaryText = "License Summary";

    public const string FrequencyWarningText = "You may experience performance issues if the playback interval time is decreased under five minutes.";

    public const string DescriptionLabel = "Description";
    public const string PlaybackIntervalLabel = "Playback interval";
    public const string EveryLabel = "Every";
    public const string ThresholdsLabel = "Thresholds";

    public const string StepNameLabel = "Step name";
    public const string OptimalLabel = "Typical value";
    public const string WarningLabel = "Warning";
    public const string CriticalLabel = "Critical";

    protected const string WhatIsProxyLabel = "How do I use a proxy?";
    protected const string ProxyUrlLabel = "Proxy URL (optional)";
    protected const string AdvancedLabel = "Advanced";

    public const string EnableDisableLabel = "Enable/Disable Thresholds";

    public const string WhatIsThisText = "Learn more";
    public const string OptimalThresholdHelpText = "Shown on charts for reference only";

    protected const string ReapplyDefaultThresholdsLabel = "Default thresholds are captured during playback in the recorder.";

    protected const string ScreenshotCapturesLabel = "Screenshot capturing:";
    protected const string ScreenshotCaptureDescriptionLabel = "Disabling screenshot capturing will improve performance by using less database space and bandwidth.";
    protected const string EnabledLabel = "Enabled";
    protected const string DisabledLabel = "Disabled";

    protected const string ScreenshotCaptureSetGloballyLabel = @"This setting is not allowed on a ""per transaction"" basis.<br />
You can change global settings <a href=""/Orion/SEUM/Admin/Settings.aspx"" class=""sw-link"">here</a>";

    public const double OptimalThresholdCoeficient = 0.5;
    public const double MinimumThresholdMiliseconds = 500;

    private int minFrequency;
    private int defaultFrequency;
    private int maxFrequencyDays;
    private int maxFrequency;
    private int minThreshold;
    private int maxThreshold;

    private readonly SettingsDAL settingsDal = new SettingsDAL();

    public event EventHandler ExpandItem;

    protected bool ScreenshotCapturePerTransaction { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Current decimal number delimiter - for possibility to enter thresholds in seconds (see FB46323)
        CurrentCultureNumberDecimalDelimiter = GetCurrentCultureNumberDecimalDelimiter();

        ScreenshotCapturePerTransaction = String.IsNullOrEmpty(settingsDal.GetValue("ScreenshotCaptures"));

        minFrequency = SEUMWebConfiguration.Instance.MinFrequencyS;
        defaultFrequency = SEUMWebConfiguration.Instance.DefaultFrequencyS;
        maxFrequencyDays = 365;
        maxFrequency = (int)TimeSpan.FromDays(maxFrequencyDays).TotalSeconds; // one year should be enough
        minThreshold = 0;
        maxThreshold = (int) TimeSpan.FromDays(1).TotalSeconds;

        if (!Page.IsPostBack)
        {
            RefreshTransaction();
        }
        else
        {
            LoadTransactionDataFromControls();
        }
    }

    public Transaction Transaction
    {
        get; set;
    }

    public string CssClass
    {
        get
        {
            return mainTable.Attributes["class"] ?? string.Empty;
        }
        set
        {
            mainTable.Attributes["class"] = value;
        }
    }

    /// <summary>
    /// Use this value if you have multiple editors on one page to prevent conflicts
    /// </summary>
    public int EditorIndex
    {
        get; set;
    }

    /// <summary>
    /// Client script code that will be called when editor validation fails.
    /// </summary>
    public string ValidationFailedClientHandler
    {
        get; set;
    }

    public string CurrentCultureNumberDecimalDelimiter
    {
        get;
        private set;
    }

    private string GetCurrentCultureNumberDecimalDelimiter()
    {
        string decimalSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

        return decimalSeparator.Replace(".", @"\.");
    }

    private void OnExpandItem()
    {
        if (ExpandItem != null)
        {
            ExpandItem(this, EventArgs.Empty);
        }
    }

    private void RefreshTransaction()
    {
        descriptionBox.Text = Transaction.Description;
        string intervalValueStr;
        string intervalUnitStr;
        if (Transaction.Frequency.TotalDays == (int)Transaction.Frequency.TotalDays)
        {
            intervalValueStr = Transaction.Frequency.TotalDays.ToString();
            intervalUnitStr = "d";
        }
        else if (Transaction.Frequency.TotalHours == (int)Transaction.Frequency.TotalHours)
        {
            intervalValueStr = Transaction.Frequency.TotalHours.ToString();
            intervalUnitStr = "h";
        }
        else if (Transaction.Frequency.TotalMinutes == (int)Transaction.Frequency.TotalMinutes)
        {
            intervalValueStr = Transaction.Frequency.TotalMinutes.ToString();
            intervalUnitStr = "m";
        }
        else
        {
            intervalValueStr = Transaction.Frequency.TotalSeconds.ToString();
            intervalUnitStr = "s";
        }
        intervalValueBox.Text = intervalValueStr;
        intervalUnitBox.Text = intervalUnitStr;

        // set values for client side validation
        frequencyValidator.Attributes.Add("unitBoxId", intervalUnitBox.ClientID);
        frequencyValidator.Attributes.Add("minSeconds", minFrequency.ToString());
        frequencyValidator.Attributes.Add("defaultSeconds", defaultFrequency.ToString());
        frequencyValidator.Attributes.Add("maxSeconds", maxFrequency.ToString());
        frequencyValidator.Attributes.Add("maxDays", maxFrequencyDays.ToString());
        frequencyValidator.ClientValidationFunction =
            string.Format("SW.SEUM.TransactionPropertiesEditor{0}.ValidateFrequency", EditorIndex);

        ConvertTransactionElementsToExtJs();

        // load transaction parameter for proxy
        var proxyParameter = Transaction.RunParameters.Get(TransactionRunParametersKeys.ProxyUrl);
        proxyUrlBox.Text = proxyParameter != null ? proxyParameter.Value : string.Empty;

        // setup screenshot captures radio list
        screenshotCapturesRadio.Items.Clear();
        screenshotCapturesRadio.Items.Add(new ListItem(EnabledLabel, true.ToString(), ScreenshotCapturePerTransaction));
        screenshotCapturesRadio.Items.Add(new ListItem(DisabledLabel, false.ToString(), ScreenshotCapturePerTransaction));

        if (ScreenshotCapturePerTransaction)
        {
            var screenshotCaptureParameter =
                Transaction.RunParameters.Get(TransactionRunParametersKeys.ScreenshotCapture);
            screenshotCapturesRadio.SelectedValue = screenshotCaptureParameter != null
                                                        ? screenshotCaptureParameter.Value
                                                        : true.ToString();
        }
        else
        {
            screenshotCapturesRadio.SelectedValue = settingsDal.GetValue("ScreenshotCaptures");
        }

        screenshotCapturesRadio.CssClass += ScreenshotCapturePerTransaction ? "" : " sw-btn-disabled";

        // process steps
        stepsRepeater.ItemDataBound += stepsRepeater_ItemDataBound;
        stepsRepeater.DataSource = Transaction.Steps;
        stepsRepeater.DataBind();
    }

    private void stepsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            // header binding
            Orion_Controls_EntityStatusIcon warningStatusIcon =
                e.Item.FindControl("warningStatusIcon") as Orion_Controls_EntityStatusIcon;
            warningStatusIcon.Entity = SwisEntities.TransactionSteps;
            Orion_Controls_EntityStatusIcon criticalStatusIcon =
                e.Item.FindControl("criticalStatusIcon") as Orion_Controls_EntityStatusIcon;
            criticalStatusIcon.Entity = SwisEntities.TransactionSteps;

            CheckBox enableAllCheckBox = e.Item.FindControl("enableAllThresholdsCheckBox") as CheckBox;
            enableAllCheckBox.Attributes.Add("onclick", "SW.SEUM.TransactionPropertiesEditor.EnableAllClicked(this, " + EditorIndex + ");");

            return;
        }
        else if ((e.Item.ItemType != ListItemType.Item) && (e.Item.ItemType != ListItemType.AlternatingItem))
            return;

        TransactionStep step = e.Item.DataItem as TransactionStep;

        TextBox optimalBox = e.Item.FindControl("optimalBox") as TextBox;
        TextBox warningBox = e.Item.FindControl("warningBox") as TextBox;
        TextBox criticalBox = e.Item.FindControl("criticalBox") as TextBox;
        CustomValidator validator = e.Item.FindControl("thresholdsValidator") as CustomValidator;
        CheckBox enableThresholdsCheckBox = e.Item.FindControl("enableThresholdsCheckbox") as CheckBox;

        // Set default value for optimal threshold if needed
        if (step.OptimalThreshold.TotalMilliseconds == 0)
            step.OptimalThreshold = TimeSpan.FromMilliseconds(step.WarningThreshold.TotalMilliseconds * OptimalThresholdCoeficient);

        warningBox.Text = step.WarningThreshold.TotalSeconds.ToString();
        criticalBox.Text = step.CriticalThreshold.TotalSeconds.ToString();
        optimalBox.Text = step.OptimalThreshold.TotalSeconds.ToString();

        // set values for client side validation
        validator.Attributes.Add("optimalBoxId", optimalBox.ClientID);
        validator.Attributes.Add("warningBoxId", warningBox.ClientID);
        validator.Attributes.Add("criticalBoxId", criticalBox.ClientID);
        validator.Attributes.Add("maxSeconds", maxThreshold.ToString());
        validator.Attributes.Add("minSeconds", minThreshold.ToString());

        validator.ClientValidationFunction =
            string.Format("SW.SEUM.TransactionPropertiesEditor{0}.ValidateThresholds", EditorIndex);

        ConvertStepsElementsToExtJs(e.Item);

        enableThresholdsCheckBox.Attributes.Add("onclick", "SW.SEUM.TransactionPropertiesEditor.EnableStepThresholds(this, '" + warningBox.ClientID + "', '" + criticalBox.ClientID + "', '" + optimalBox.ClientID + "', " + EditorIndex + ");");
        enableThresholdsCheckBox.Checked = warningBox.Text != "0" || criticalBox.Text != "0";
        if(!enableThresholdsCheckBox.Checked)
        {
            warningBox.Attributes.Add("disabled", "disabled");
            criticalBox.Attributes.Add("disabled", "disabled");
        }

        Orion_Controls_EntityStatusIcon statusIcon = e.Item.FindControl("entityStatusIcon") as Orion_Controls_EntityStatusIcon;
        statusIcon.Entity = SwisEntities.RecordingSteps;
    }

    private void ConvertTransactionElementsToExtJs()
    {
        jsConvertLiteral.Text = string.Format(
            @"SW.SEUM.TransactionPropertiesEditor{4}.ConvertIntervalUnitCombo('{0}', '{1}');
              SW.SEUM.TransactionPropertiesEditor{4}.ConvertToNumericTextBox('{2}');
              SW.SEUM.TransactionPropertiesEditor{4}.ConvertDescriptionBox('{3}');",
              intervalUnitBox.ClientID, (!string.IsNullOrEmpty(intervalUnitBox.Text)) ? intervalUnitBox.Text : "s",
              intervalValueBox.ClientID,
              descriptionBox.ClientID,
              EditorIndex);
    }

    private void ConvertStepsElementsToExtJs(RepeaterItem item)
    {
        if (item.ItemIndex < 0)
            return;

        TextBox optimalBox = item.FindControl("optimalBox") as TextBox;
        TextBox warningBox = item.FindControl("warningBox") as TextBox;
        TextBox criticalBox = item.FindControl("criticalBox") as TextBox;
        Literal jsConvertLiteral = item.FindControl("jsConvertLiteral") as Literal;

        jsConvertLiteral.Text = string.Format(
            @"SW.SEUM.TransactionPropertiesEditor{3}.ConvertToNumericWithFloatingPointSupportTextBox('{0}');
              SW.SEUM.TransactionPropertiesEditor{3}.ConvertToNumericWithFloatingPointSupportTextBox('{1}');
              SW.SEUM.TransactionPropertiesEditor{3}.ConvertToNumericWithFloatingPointSupportTextBox('{2}');",
              optimalBox.ClientID,
              warningBox.ClientID,
              criticalBox.ClientID,
              EditorIndex);
    }

    /// <summary>
    /// Validate entered frequency value for transaction. It must be greater or eqal than minimal allowed value.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void ValidateFrequency(object source, ServerValidateEventArgs args)
    {
        TimeSpan frequency = GetFrequency(intervalValueBox.Text, intervalUnitBox.Text);

        if (frequency.TotalSeconds < minFrequency || frequency.TotalSeconds > maxFrequency)
        {
            args.IsValid = false;
            frequencyValidator.Text = string.Format(InvalidFrequencyTextFormat, minFrequency, maxFrequencyDays);
            // expand items with errors
            OnExpandItem();
        }
    }

    /// <summary>
    /// Validate entered thresholds values for transaction. They must be in relation optimal<warning<critical
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void ValidateThresholds(object source, ServerValidateEventArgs args)
    {
        // find containing repeater item to get access to controls to validate
        CustomValidator validator = source as CustomValidator;
        RepeaterItem repeaterItem = FindParentRepeaterItem(validator);
        if (repeaterItem == null)
            return;


        // in correct repeater item
        TextBox optimalBox = repeaterItem.FindControl("optimalBox") as TextBox;
        TextBox warningBox = repeaterItem.FindControl("warningBox") as TextBox;
        TextBox criticalBox = repeaterItem.FindControl("criticalBox") as TextBox;

        double optimal = 0;
        double warning = 0;
        double critical = 0;
        double.TryParse(optimalBox.Text, out optimal);
        double.TryParse(warningBox.Text, out warning);
        double.TryParse(criticalBox.Text, out critical);

        // Zero-values test
        if (critical < 0 || critical > maxThreshold)
        {
            args.IsValid = false;
            validator.Text = string.Format(InvalidCriticalThresholdTextFormat, minThreshold, maxThreshold);
            // expand items with errors
            OnExpandItem();
        }
        else if (warning < 0 || warning > maxThreshold)
        {
            args.IsValid = false;
            validator.Text = string.Format(InvalidWarningThresholdTextFormat, minThreshold, maxThreshold); ;
            // expand items with errors
            OnExpandItem();
        }
        else if (optimal < 0 || optimal > maxThreshold)
        {
            args.IsValid = false;
            validator.Text = string.Format(InvalidOptimalThresholdTextFormat, minThreshold, maxThreshold); ;
            // expand items with errors
            OnExpandItem();
        }
        else if (critical > 0 && warning > critical)
        {
            args.IsValid = false;
            validator.Text = InvalidWarningThresholdText;
            // expand items with errors
            OnExpandItem();
        }
        else if (warning > 0 && optimal > warning)
        {
            args.IsValid = false;
            validator.Text = InvalidOptimalThresholdText;
            // expand items with errors
            OnExpandItem();
        }
    }

    private TimeSpan GetFrequency(string value, string unit)
    {
        int intervalValue = 0;
        int.TryParse(value, out intervalValue);

        TimeSpan result = new TimeSpan();
        switch (unit)
        {
            case "s":
                result = TimeSpan.FromSeconds(intervalValue);
                break;
            case "m":
                result = TimeSpan.FromMinutes(intervalValue);
                break;
            case "h":
                result = TimeSpan.FromHours(intervalValue);
                break;
            case "d":
                result = TimeSpan.FromDays(intervalValue);
                break;
        }

        return result;
    }

    private void LoadTransactionDataFromControls()
    {
        Transaction.Description = descriptionBox.Text;
        Transaction.Frequency = GetFrequency(intervalValueBox.Text, intervalUnitBox.Text);

        if(string.IsNullOrEmpty(proxyUrlBox.Text))
        {
            Transaction.RunParameters.Remove(TransactionRunParametersKeys.ProxyUrl);
        }
        else
        {
            Transaction.RunParameters.Update(TransactionRunParametersKeys.ProxyUrl, RunParameterTypes.Transaction, proxyUrlBox.Text);
        }

        // if screenshot captures setting is allowed on transaction basis save value
        if (ScreenshotCapturePerTransaction)
        {
            Transaction.RunParameters.Update(TransactionRunParametersKeys.ScreenshotCapture,
                                             RunParameterTypes.Transaction,
                                             screenshotCapturesRadio.SelectedValue);
        }

        ConvertTransactionElementsToExtJs();

        LoadStepsDataFromControls();
    }

    private void LoadStepsDataFromControls()
    {
        // Update status icons in repeater header. We can't access header directly so we have to use controls collection.
        if (stepsRepeater.Controls.Count > 0)
        {
            RepeaterItem headerItem = stepsRepeater.Controls[0] as RepeaterItem;
            Orion_Controls_EntityStatusIcon warningStatusIcon =
                headerItem.FindControl("warningStatusIcon") as Orion_Controls_EntityStatusIcon;
            warningStatusIcon.Entity = SwisEntities.TransactionSteps;
            Orion_Controls_EntityStatusIcon criticalStatusIcon =
                headerItem.FindControl("criticalStatusIcon") as Orion_Controls_EntityStatusIcon;
            criticalStatusIcon.Entity = SwisEntities.TransactionSteps;
        }

        int index = 0;
        foreach (TransactionStep step in Transaction.Steps)
        {
            RepeaterItem item = stepsRepeater.Items[index];

            Orion_Controls_EntityStatusIcon statusIcon = item.FindControl("entityStatusIcon") as Orion_Controls_EntityStatusIcon;
            statusIcon.Entity = SwisEntities.RecordingSteps;

            TextBox optimalBox = item.FindControl("optimalBox") as TextBox;
            TextBox warningBox = item.FindControl("warningBox") as TextBox;
            TextBox criticalBox = item.FindControl("criticalBox") as TextBox;

            double warningThreshold = 0;
            double.TryParse(warningBox.Text, out warningThreshold);
            step.WarningThreshold = TimeSpan.FromSeconds(warningThreshold);

            double criticalThreshold = 0;
            double.TryParse(criticalBox.Text, out criticalThreshold);
            step.CriticalThreshold = TimeSpan.FromSeconds(criticalThreshold);

            double optimalThreshold = 0;
            double.TryParse(optimalBox.Text, out optimalThreshold);
            step.OptimalThreshold = TimeSpan.FromSeconds(optimalThreshold);

            index++;
        }
    }

    private RepeaterItem FindParentRepeaterItem(Control control)
    {
        Control parent = control.Parent;
        while (parent != null && (!(parent is RepeaterItem)))
        {
            parent = parent.Parent;
        }
        RepeaterItem repeaterItem = parent as RepeaterItem;
        return repeaterItem;
    }

    protected string GetZebraStripeClass(int index)
    {
        return (index % 2 == 0) ? "ZebraStripe" : string.Empty;
    }

    protected string OptimalThresholdLearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsThresholds");
        }
    }

    protected void ReapplyDefaultThresholdsClick(object sender, EventArgs e)
    {
        foreach (var step in this.Transaction.Steps)
        {
            step.CriticalThreshold = step.RecordingStep.CriticalThreshold;
            step.WarningThreshold = step.RecordingStep.WarningThreshold;
        }
        RefreshTransaction();
    }

    protected string LearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsPlaybackLocations");
        }
    }

    protected string HowToUseProxyHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionSEUMPHProxy"); }
    }
}
