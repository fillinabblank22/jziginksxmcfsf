﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetObjectPickerDialog.ascx.cs" Inherits="Orion_SEUM_Controls_NetObjectPickerDialog" %>
<%@ Register TagPrefix="orion" TagName="NetObjectPicker" Src="~/Orion/Controls/NetObjectPicker.ascx" %>
<script type="text/javascript">
    $(document).ready(function() {
        $("#<%=DialogSaveButton.ClientID%>").live("click", function() {
            $("#selectordlg").trigger("selector-dialog-save");
            $("#selectordlg").dialog("close");
            return false;
        });
        $("#<%=DialogCancelButton.ClientID%>").live("click", function() {
            $("#selectordlg").dialog("close");
            return false;
        });
    });
</script>
<div id="selectordlg" style="display: none">
    <orion:NetObjectPicker runat="server" /> 
    <div style="text-align: right">
        <orion:LocalizableButton runat="server" ID="DialogSaveButton" LocalizedText="Save" DisplayType="Primary" CausesValidation="false" />
        <orion:LocalizableButton runat="server" ID="DialogCancelButton" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
    </div>
</div>
