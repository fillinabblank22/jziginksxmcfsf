﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecordingsGrid.ascx.cs" Inherits="Orion_SEUM_Controls_RecordingsGrid" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="~/Orion/js/OrionCore.js"/>
        <asp:ScriptReference Path="~/Orion/SEUM/js/SearchField.js" />
        <asp:ScriptReference Path="~/Orion/SEUM/js/FileUploadField.js" />
        <asp:ScriptReference Path="~/Orion/SEUM/js/CheckboxSelectionModel.js" />
        <asp:ScriptReference Path="~/Orion/SEUM/js/RadioSelectionModel.js" />
        <asp:ScriptReference Path="~/Orion/SEUM/js/RecordingsGrid.js" />
    </Scripts>
</asp:ScriptManagerProxy>

<div id="RecordingsGrid" class="SEUM_ExtJsGrid"></div>

<asp:HiddenField runat="server" ID="selectedRecordings"/>

<div id="editRecordingDescription" class="SEUM_EditRecordingDescription" style="display: none;">
    <div class="x-panel-body SEUM_First">
    <p class="SEUM_First">
    <%= descriptionPart1 %>
    </p>
    <p>
    <b><%= CommonTexts.RecorderStartLocation %></b> <%= descriptionPart3 %>
    </p>
        <p id="editRecordingDownloadNewRecorderLink" style="display: none"><a href="<%= CommonLinks.ChromiumRecorderInstaller %>" class="SEUM_HelpLink" target="_blank"><%= DownloadNewRecorderText %></a></p>
        <p id="editRecordingDownloadLegacyRecorderLink" style="display: none"><a href="<%= CommonLinks.RecorderInstaller %>" class="SEUM_HelpLink" target="_blank"><%= DownloadLegacyRecorderText %></a></p>
    <p>
    <%= descriptionPart4 %>
    </p>
    <a id="learnMoreForNewRecorder" style="display: none" href="<%= LearnMoreHelpUrlForNewRecorder %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= LearnMoreText%></a>
    <a id="learnMoreForLegacyRecorder" style="display: none" href="<%= LearnMoreHelpUrlForLegacyRecorder %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= LearnMoreText%></a>
    </div>
</div>

<div id="editVerifyAction" class="SEUM_EditRecordingDescription" style="display: none;">
    <div class="x-panel-body">
        <div class="SEUM_First SEUM_ExtJSSelectInlineBlock">
            <%= ValidateTextPart1 %>
            <select id="verifyActionType">
                <option value="0"><%= DoesNotContainText %></option>
                <option value="1"><%= ContainsText%></option>
            </select>
            <br />
            <%= ValidateTextPart2 %>
        </div>
        <p>
            <textarea cols="20" id="verifyActionText"></textarea>
        </p>
        <div class="sw-suggestion">
            <span class="sw-suggestion-icon"></span>
            <%= InformationText %>
        </div>
        <p>
            <%= RecommendedWaitTimeText %> <input type="checkbox" id="verifyActionDefaultWaitTime" /> <input type="text" id="verifyActionWaitTimeText" style="display: none;" />
        </p>
        <div class="sw-suggestion">
            <span class="sw-suggestion-icon"></span>
            <%= WaitTimeInfoText %>
        </div>
    </div>
</div>

<script type="text/javascript">
        // <![CDATA[
        SW.SEUM.RecordingsGrid.SetEditGrid(<%= this.IsEditGrid.ToString().ToLower() %>);
        SW.SEUM.RecordingsGrid.SetSelectedRecordingsFieldClientID('<%= this.SelectedRecordingsFieldClientID %>');
        SW.SEUM.RecordingsGrid.SetPageSize(<%= this.PageSize %>);
        SW.SEUM.RecordingsGrid.SetRecordingEntity('<%= SwisEntities.Recordings %>');
        SW.SEUM.RecordingsGrid.SetRecordingStepEntity('<%= SwisEntities.RecordingSteps %>');
        SW.SEUM.RecordingsGrid.SetUserName("<%=HttpUtility.JavaScriptStringEncode(Context.User.Identity.Name.Trim())%>");
        SW.SEUM.RecordingsGrid.SetImportLearnMoreLink("<%=ImportLearnMoreHelpUrl%>");
        SW.SEUM.RecordingsGrid.SetExcludedRecordings("<%=this.ExcludedRecordingsString%>");
        SW.SEUM.RecordingsGrid.SetUnsupportedRecordings("<%=this.UnsupportedRecordingsString%>");
        // ]]>

        $('#verifyActionDefaultWaitTime').change(function(){
            if(this.checked)
                $('#verifyActionWaitTimeText').hide();
            else
                $('#verifyActionWaitTimeText').show();
        });

</script>
