﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorsGrid.ascx.cs" Inherits="Orion_SEUM_Controls_MonitorsGrid" %>
<%@ Register TagPrefix="orion" TagName="DateTimePicker" Src="~/Orion/Controls/DateTimePicker.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="UnmanageWindow" Src="~/Orion/SEUM/Controls/UnManageTransactionDialog.ascx" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Licensing" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SEUM" File="SearchField.js" />
<orion:Include runat="server" Module="SEUM" File="CheckboxSelectionModel.js" />
<orion:Include runat="server" Module="SEUM" File="UnManageTransactionDialog.js" />
<orion:Include runat="server" Module="SEUM" File="MonitorsGrid.js" />

<style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9100!important;
    }
</style>

<%--<div id="MonitorsGrid" class="SEUM_ExtJsGrid"></div>--%>

<div id="SEUM_GroupingPanel" class="SEUM_GroupingPanel">
	<div class="GroupSection">
		<label for="groupBySelect">Group by:</label>
		<select id="groupBySelect">
			<option value="test"/>
		</select>
	</div>
    <div id="TestID"></div>
</div>

<div id="MonitorsGrid" class="SEUM_ExtJsGrid"></div>

<SEUM:UnmanageWindow runat="server" />

<script type="text/javascript">
        // <![CDATA[
        SW.SEUM.MonitorsGrid.SetGroupBy('<%= this.GroupBy %>');
        SW.SEUM.MonitorsGrid.SetGroupByItem('<%= this.GroupByItem %>');
        SW.SEUM.MonitorsGrid.SetMonitorEntity('<%= SwisEntities.Transactions %>');
        SW.SEUM.MonitorsGrid.SetRecordingEntity('<%= SwisEntities.Recordings %>');
        SW.SEUM.MonitorsGrid.SetPageSize(<%= this.PageSize %>);
        SW.SEUM.MonitorsGrid.SetUserName("<%=HttpUtility.JavaScriptStringEncode(Context.User.Identity.Name.Trim())%>");
        SW.SEUM.MonitorsGrid.SetRelatedNodesVisible(<%=LicenseInfo.AreNodesAvailable.ToString().ToLower()%>);
        SW.SEUM.MonitorsGrid.SetRelatedApplicationsVisible(<%=LicenseInfo.AreApplicationsAvailable.ToString().ToLower()%>);
        // ]]>
</script>
