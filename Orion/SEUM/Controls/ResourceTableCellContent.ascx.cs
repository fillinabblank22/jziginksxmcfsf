﻿using System;
using SolarWinds.SEUM.Web.Controls.ResourceTable;

public partial class Orion_SEUM_Controls_ResourceTableCellContent : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cell.IsNetObject)
        {
            entityStatusIcon.Entity = NetObject.SwisEntity;
            entityStatusIcon.Status = NetObject.Status;
        }
        else if(Cell.HasControl)
        {
            controlPlaceholder.Controls.Add(Cell.Control);
        }
        else
        {
            formattedValue.Value = Cell.Value;
            formattedValue.FormatString = Cell.Column.FormatString;
            formattedValue.ValueFormatter = Cell.Column.ValueFormatter;
            formattedValue.Status = Cell.Status;
        }
    }

    public ResourceTableCell Cell
    {
        get; set;
    }

    public ResourceTableEntity NetObject
    {
        get
        {
            if (Cell.IsNetObject)
                return (ResourceTableEntity)Cell.Value;

            return null;
        }
    }

    public ResourceTableTransactionEntity TransactionNetObject
    {
        get
        {
            if (Cell.IsTransactionNetObject)
                return (ResourceTableTransactionEntity) Cell.Value;

            return null;
        }
    }
}