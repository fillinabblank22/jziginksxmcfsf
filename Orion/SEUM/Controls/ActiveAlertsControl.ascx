﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveAlertsControl.ascx.cs"
    Inherits="Orion_SEUM_Controls_ActiveAlertsControl" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<asp:Repeater ID="alertsRepeater" runat="server">
    <HeaderTemplate>
        <table class="DataGrid" CellPadding="3">
            <thead>
                <tr>
                    <td  class="ReportHeader SEUM_ReportHeader">
                        &nbsp;
                    </td>
                    <td  class="ReportHeader SEUM_ReportHeader SEUM_ReportHeader_TimeOfAlert">
                        Time Of Alert
                    </td>
                    <%if (this.ShowItemName) { %>
                    <td  class="ReportHeader SEUM_ReportHeader" >
                        Object Name
                    </td>
                    <%} %>
                    <td  class="ReportHeader SEUM_ReportHeader" >
                        Message
                    </td>
                </tr>
            </thead>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td class="Property SEUM_Property">
                <asp:Image ID="imgAlert" runat="server" ImageUrl="/NetPerfMon/images/Event-19.gif" />
            </td>
            <td class="Property SEUM_Property">
                <%# Eval("AlertTime")%>
            </td>
            <%if (this.ShowItemName)  { %>
            <td class="Property SEUM_Property">
                <a href="<%# Eval("Url") %>">
                    <%# HttpUtility.HtmlEncode(Eval("EntityName")) %>
                </a>
            </td>
            <%} %>
            <td class="Property SEUM_Property">
                <%# Eval("EventMessage")%>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
