﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScreenshotBrowser.ascx.cs" Inherits="Orion_SEUM_Controls_ScreenshotBrowser" %>

<style type="text/css">
    html.RenderPdf .SEUM_ScreenshotTable img
    {
        page-break-inside: auto!important;
    }
</style>

<script type="text/javascript">
    function SEUMUpdateResourceSubtitle<%= ParentResourceId %>(value) {
        var parent =  $('#SEUMScreenshotTable-<%= ParentResourceId %>').parent();
        while (parent && (!parent.hasClass('ResourceWrapper') || parent.find('.HeaderBar').length == 0))
        {
            parent = parent.parent();
        }

        if (parent)
            parent.find('h2').text(value);
    }
</script>
<asp:UpdatePanel runat="server" ID="screenshotUpdatePanel" UpdateMode="Conditional">
    <ContentTemplate>
        <table class="SEUM_ScreenshotTable" cellspacing="0" id="SEUMScreenshotTable-<%= ParentResourceId %>">
            <tr runat="server" id="navigationTableRow">
                <th>
                    <asp:DropDownList runat="server" ID="customDropdown" AutoPostBack="true"></asp:DropDownList>
                    <asp:Button ID="changeButton" runat="server" Visible="false" Text="Change" />
                </th>
                <th class="SEUM_Right SEUM_ScreenshotNavButtons">
                    <asp:LinkButton runat="server" ID="navLinkFirst">
                        <asp:Image ID="Image1" ImageUrl="~/Orion/SEUM/Images/Button.Paging.First.gif" runat="server" />
                        <%= navFirstButtonText %>
                    </asp:LinkButton>

                    <asp:LinkButton runat="server" ID="navLinkPrev">
                        <asp:Image ID="Image2" ImageUrl="~/Orion/SEUM/Images/Button.Paging.Previous.gif" runat="server" />
                        <%= navPrevButtonText %>
                    </asp:LinkButton>

                    <asp:Literal runat="server" ID="navDivider">|</asp:Literal>

                    <asp:LinkButton runat="server" ID="navLinkNext">
                        <%= navNextButtonText %>
                        <asp:Image ID="Image3" ImageUrl="~/Orion/SEUM/Images/Button.Paging.Next.gif" runat="server" />
                    </asp:LinkButton>
                    
                    <asp:LinkButton runat="server" ID="navLinkLast">
                        <%= navLastButtonText %>
                        <asp:Image ID="Image4" ImageUrl="~/Orion/SEUM/Images/Button.Paging.Last.gif" runat="server" />
                    </asp:LinkButton>
                </th>
            </tr>
            <tr>
                <td colspan="2" class="SEUM_ScreenshotContainer">
                    <asp:Panel runat="server" ID="loadingPanel" CssClass="SEUM_ImageLoadingIndicatorContainer">
                        <div class="SEUM_ImageLoadingIndicator">
                            <asp:Image runat="server" ID="loadingImage" ImageUrl="~/Orion/images/AJAX-Loader.gif" /><br />
                            <%= screenshotLoadingMessage %>
                        </div>
                    </asp:Panel>
                    <asp:Image runat="server" ID="screenshotImage" CssClass="SEUM_ScreenshotImage" />
                    <asp:Panel CssClass="SEUM_NoData" runat="server" ID="noDataPanel" Visible="false">
                        <asp:Literal runat="server" ID="messageLiteral"></asp:Literal>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="SEUM_Right" colspan="2">
                    <asp:Panel runat="server" ID="copyrightPanel" CssClass="sw-seum-screenshot-copyright" Visible="False"></asp:Panel>
                    <asp:Panel runat="server" ID="linksPanel">
                        &#0187; <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="fullscreenLink"><%= fullscreenLinkText %></asp:Hyperlink>
                        <%= orText %> <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="htmlLink"><%= htmlLinkText %></asp:Hyperlink>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
