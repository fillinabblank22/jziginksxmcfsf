﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UnManageTransactionDialog.ascx.cs" Inherits="Orion_SEUM_Controls_UnManageTransactionDialog" %>
<%@ Register TagPrefix="orion" TagName="DateTimePicker" Src="~/Orion/Controls/DateTimePicker.ascx" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4"  />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SEUM" File="UnManageTransactionDialog.js" />

<style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9100!important;
    }
</style>
<asp:Panel ID="UnmanageWindow" ClassName="SEUM_UnmanageWindow" Style="display: none;" runat="server">
    <div class="x-panel-body">
    <p>
        <%= PollingSuspendedInfo %>
    </p>
    <p>
        <input type="radio" id="setUnmanageTime" name="resetScheduledWindows" value="0" /> <label for="setUnmanageTime"><%= UnmanageDuringText %></label>
    </p>
    <div class="SEUM_Indent">
	    <table width="100%" id="SEUM_UnmanageTimesTable" class="SEUM_UnmanageTable">
		    <tr>
			    <td class="SEUM_BottomBorder SEUM_LightBorder"><%= UnmanageBeginningText %></td>
			    <td class="SEUM_BottomBorder SEUM_LightBorder"><orion:DateTimePicker runat="server" ID="unmanageStart" /></td>
		    </tr>
		    <tr>
			    <td class="SEUM_NoBottom"><%= UnmanageUntilText %></td>
			    <td class="SEUM_NoBottom"><orion:DateTimePicker runat="server" ID="unmanageEnd" /></td>
		    </tr>
		    <tr>
			    <td colspan="2" class="sw-text-helpful"><%= PollingRestartedInfo %></td>
		    </tr>
	    </table>
    </div>
    <p class="SEUM_ResetUnmanage">
        <input type="radio" id="resetScheduledWindows" name="resetScheduledWindows" value="1" /> <label for="resetScheduledWindows"><%= ClearWindowsText %></label>
    </p>
    <div class="SEUM_Indent">
        <div class="SEUM_UnmanageClearWrapper">
            <div id="SEUM_UnmanageClearText" class="sw-text-helpful"><%= ClearWindowsTextHelpful %></div>
	        <table width="100%" id="SEUM_UnmanageClearTable" class="SEUM_UnmanageTable">
	            <tr>
			        <td><%= UnmanageTimesWillBeRemovedText %></td>
                </tr>
                <tr>
                    <td class="SEUM_TextGray"><%= FromToText %></td>
		        </tr>
	        </table>
        </div>
    </div>
    </div>
</asp:Panel>

<script type="text/javascript">
        // <![CDATA[
        SW.SEUM.UnManageTransactionDialog.SetUnmanageWindowClientId('<%= UnmanageWindow.ClientID %>');
        SW.SEUM.UnManageTransactionDialog.SetUnmanageFromClientId('<%= unmanageStart.ClientID %>');
        SW.SEUM.UnManageTransactionDialog.SetUnmanageUntilClientId('<%= unmanageEnd.ClientID %>');

        $('input[name=resetScheduledWindows]').click(function(){
            if($('input[name=resetScheduledWindows]:checked').val() == '1') {
                $('#<%= UnmanageWindow.ClientID %> #SEUM_UnmanageTimesTable').hide();
                $('#<%= UnmanageWindow.ClientID %> .SEUM_UnmanageClearWrapper').show();
            }
            else {
                $('#<%= UnmanageWindow.ClientID %> #SEUM_UnmanageTimesTable').show();
                $('#<%= UnmanageWindow.ClientID %> .SEUM_UnmanageClearWrapper').hide();
            }
        });
        // ]]>
</script>
