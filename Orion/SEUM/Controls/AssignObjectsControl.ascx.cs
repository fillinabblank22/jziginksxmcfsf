﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SEUM_Controls_AssignObjectsControl : System.Web.UI.UserControl
{
    public string EntityType { get; set; }
    public string EntityCaption { get; set; }

    private IEnumerable<string> selectedObjectsOverride = null;

    public IEnumerable<string> SelectedObjectUris 
    {
        get
        {
            return selectedObjectsOverride ?? (String.IsNullOrEmpty(SelectedObjectsField.Value.Trim()) ? 
                                                    Enumerable.Empty<String>() : SelectedObjectsField.Value.Trim().Split(','));
        }
        set
        {
            selectedObjectsOverride = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        AddObjectsButton.Text = String.Format("Add {0}", EntityCaption);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (selectedObjectsOverride != null)
        {
            SelectedObjectsField.Value = String.Join(",", selectedObjectsOverride);
        }
    }
}