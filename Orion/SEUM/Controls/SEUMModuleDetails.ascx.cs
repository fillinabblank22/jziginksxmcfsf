﻿using SolarWinds.Logging;
using SolarWinds.SEUM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Admin_SEUMModuleDetails : UserControl
{
    private const string _productNameLabel = "Product Name";
    private const string _productVersionLabel = "Version";
    private const string _licenseLabel = "License";
    private const string _licenseLimitLabel = "License limit";
    private const string _licenseRemainingLabel = "Licenses remaining";
    private const string _licenseRemainingTimeLabel = "Remaining evaluation days";

    private static Log _log;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo();
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("Error while displaying details for WPM. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo()
    {
        DataTable licenseSummary;
        using (ISEUMBusinessLayer bl = BusinessLayerFactory.Create())
        {
            licenseSummary = bl.GetLicenseSummary();
        }

        var values = new Dictionary<string, string>();

        SEUMDetails.Name = (string)licenseSummary.Rows[0]["ProductName"];

        values.Add(_productNameLabel, (string)licenseSummary.Rows[0]["DisplayName"]);
        values.Add(_productVersionLabel, (string)licenseSummary.Rows[0]["ProductVersion"]);
        values.Add(_licenseLabel, licenseSummary.Rows[0]["LicenseType"].ToString());
        values.Add(_licenseLimitLabel, ((int)licenseSummary.Rows[0]["LicenseLimit"]).ToString());
        values.Add(_licenseRemainingLabel, ((int)licenseSummary.Rows[0]["AvailableMonitors"]).ToString());

        if ((bool)licenseSummary.Rows[0]["DaysRemaining"])
        {
            values.Add(_licenseRemainingTimeLabel, ((int)licenseSummary.Rows[0]["DaysRemaining"]).ToString());
        }

        SEUMDetails.DataSource = values;


    }
}