﻿<%@ Control Language="C#" ClassName="IncludeCharts" %>

<script runat="server">
    private bool _debug = false;
    private string _version = string.Empty;

    public bool Debug
    {
        get { return _debug; }
        set { _debug = value; }
    }
    public string Version
    {
        get { return _version; }
        set { _version = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // TODO:  Should we convert this to a "Framework" so we can use OrionInclude.CoreFramework?

        OrionInclude.CoreFile("Charts/Charts.css");
        

        if (Debug)
        {
            OrionInclude.ModuleFile("SEUM", "Highstock-1.1.2/highstock.src.js");
            //OrionInclude.CoreFile("Charts/js/modules/exporting.src.js");
        }
        else
        {
            OrionInclude.ModuleFile("SEUM", "Highstock-1.1.2/highstock.src.js");
            //OrionInclude.CoreFile("Charts/js/modules/exporting.js");
        }

        //OrionInclude.CoreFile("Charts/js/themes/orion.js");
        //OrionInclude.CoreFile("Charts/js/json2.js");
        OrionInclude.ModuleFile("SEUM", "themes/orion.js");
        OrionInclude.ModuleFile("SEUM", "Charts.js");
        OrionInclude.ModuleFile("SEUM", "Charts.Colors.js");
        //OrionInclude.CoreFile("Charts/js/Charts.Legend.js");
        //OrionInclude.CoreFile("Charts/js/Charts.Colors.js");
        //OrionInclude.CoreFile("Charts/js/Charts.Export.js", OrionInclude.Section.Bottom);
    }

</script>
