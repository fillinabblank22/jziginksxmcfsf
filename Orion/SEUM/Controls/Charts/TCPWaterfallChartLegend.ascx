﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TCPWaterfallChartLegend.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_TCPWaterfallLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.seum_tcpWaterfallLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.SEUM.Charts.TCPWaterfallLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
        
        
</script>

<table runat="server" id="legend" class="chartLegend SEUM_TCPWaterfallChartLegend"></table>
<div style="float:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
