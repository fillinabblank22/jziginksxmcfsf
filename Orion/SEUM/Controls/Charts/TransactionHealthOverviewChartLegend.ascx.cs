﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Web.Configuration;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_Charts_TransactionHealthOverviewChartLegend : UserControl, IChartLegendControl
{
    private const string _showMoreSingularMessage = "There is {0} more item. Show all...";
    private const string _showMorePluralMessage = "There are {0} more items. Show all...";

    protected const string resourceFilterErrorClass = "SEUM_ResourceError";
    protected const string resourceFilterErrorText = "Data for this resource can't be loaded. If you have custom filter defined, check that it is correct.";

    protected string ShowMoreSingularMessage
    {
        get { return _showMoreSingularMessage; }
    }

    protected string ShowMorePluralMessage
    {
        get { return _showMorePluralMessage; }
    }

    protected int BatchSize
    {
        get { return SEUMWebConfiguration.Instance.AjaxTreeBatchSize; }
    }

    public string LegendInitializer { get { return "seum_transactionHealthOverviewLegendInitializer__" + tree.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.TransactionHealthOverviewLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("SEUM", "AjaxTree.js");
        OrionInclude.ModuleFile("SEUM", "SEUM.css");
    }
}