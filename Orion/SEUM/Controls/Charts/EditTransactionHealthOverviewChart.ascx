﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTransactionHealthOverviewChart.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_EditTransactionHealthOverviewChart" %>
<%@ Register TagPrefix="SEUM" TagName="FilterEditor" Src="~/Orion/SEUM/Controls/FilterEditor.ascx" %>

<orion:Include runat="server" Module="SEUM" File="SEUM.css" />

<div class="sw-res-editor-row">
    <div>
        <asp:CheckBox runat="server" ID="rememberExpanded" />
    </div>
    <div>
        <asp:CheckBox runat="server" ID="hideUnmanagedCheckbox" />
    </div>
</div>

<SEUM:FilterEditor runat="server" ID="filterEditor" />

