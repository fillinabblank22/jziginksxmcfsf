﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionHealthOverviewChartLegend.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_TransactionHealthOverviewChartLegend" %>

<asp:ScriptManagerProxy id="TransactionsTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/SEUM/Services/TransactionsHealthTree.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<script type="text/javascript">
    SW.Core.Charts.Legend.seum_transactionHealthOverviewLegendInitializer__<%= tree.ClientID %> = function (chart) {
        SW.SEUM.Charts.TransactionHealthOverviewLegend.createStandardLegend(chart, TransactionsHealthTree, '<%= tree.ClientID %>', '<%= ShowMoreSingularMessage %>', '<%= ShowMorePluralMessage %>', <%= BatchSize %>);
    };
        
        
</script>

<div id="tree" runat="server">
</div>

<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>