﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditDurationsByLocationChart.ascx.cs"
    Inherits="Orion_SEUM_Controls_Charts_EditDurationsByLocationChart" %>
<div class="sw-res-editor-row">
    <div>
        <b><%= DisplayItemsNumberLabel %>:</b><br />
        <asp:TextBox runat="server" ID="maxCountBox" Rows="1" Columns="30" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="maxCountBox" Display="Dynamic"
            SetFocusOnError="true">* <%= numberMustBeFilledError %></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="maxCountBox"
            Display="Dynamic" Type="Integer" MaximumValue="1000" MinimumValue="1"
            SetFocusOnError="true"></asp:RangeValidator>
    </div>
</div>
