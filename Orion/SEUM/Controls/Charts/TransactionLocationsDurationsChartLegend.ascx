﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionLocationsDurationsChartLegend.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_TransactionLocationsDurationsChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.seum_transactionLocationsDurationsLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.SEUM.Charts.TransactionLocationsDurationsLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
        
        
</script>

<table runat="server" ID="legend" class="DataGrid">
    <tr>
        <td class="ReportHeader SEUM_ReportHeader" colspan="2"><%= transactionNameLabel%></td>
        <td class="ReportHeader SEUM_ReportHeader"><%= currentDurationLabel %></td>
        <td class="ReportHeader SEUM_ReportHeader"><%= avgDurationLabel %></td>
    </tr>

</table>

<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
