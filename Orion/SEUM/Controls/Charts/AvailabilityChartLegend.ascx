﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvailabilityChartLegend.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_AvailabilityChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.seum_availabilityChartLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
        SW.SEUM.Charts.AvailabilityChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
        
        
</script>

<div runat="server" id="legend" class="chartLegend sw-seum-availability-legend"></div>
<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
