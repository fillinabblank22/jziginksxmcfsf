﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTCPWaterfallChart.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_EditTCPWaterfallChart" %>

<p>
    <b><%= numberOfItemsLabel %>:</b><br />
    <asp:TextBox runat="server" ID="maxCountBox" Rows="1" Columns="30" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="maxCountBox" Display="Dynamic"
        SetFocusOnError="true">* <%= numberMustBeFilledError %></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="maxCountBox"
        Display="Dynamic" Type="Integer" MaximumValue="1000" MinimumValue="0"
        SetFocusOnError="true"></asp:RangeValidator>
</p>
