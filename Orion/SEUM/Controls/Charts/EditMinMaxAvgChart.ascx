﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMinMaxAvgChart.ascx.cs"
    Inherits="Orion_SEUM_Controls_Charts_EditMinMaxAvgChart" %>
<div class="sw-res-editor-row">
    <div>
        <b><%= DisplayThresholdsLabel %></b>:
    </div>
    <div>
        <asp:Label ID="Label1" runat="server" AssociatedControlID="optimalThresholdCheckBox">
            <asp:CheckBox runat="server" ID="optimalThresholdCheckBox" />
            <%= OptimalThresholdLabel %>
        </asp:Label>
    </div>
    <div>
        <asp:Label ID="Label2" runat="server" AssociatedControlID="warningThresholdCheckBox">
            <asp:CheckBox runat="server" ID="warningThresholdCheckBox" />
            <%= WarningThresholdLabel %>
        </asp:Label>
    </div>
    <div>
        <asp:Label ID="Label3" runat="server" AssociatedControlID="criticalThresholdCheckBox">
            <asp:CheckBox runat="server" ID="criticalThresholdCheckBox" />
            <%= CriticalThresholdLabel %>
        </asp:Label>
    </div>
</div>
