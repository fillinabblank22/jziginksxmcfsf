﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionStepLocationsDurationsChartLegend.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_TransactionStepLocationsDurationsChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.seum_transactionStepLocationsDurationsLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.SEUM.Charts.TransactionStepLocationsDurationsLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
        
        
</script>

<table runat="server" ID="legend" class="DataGrid">
    <tr>
        <td class="ReportHeader SEUM_ReportHeader" colspan="2"><%= StepNameLabel %></td>
        <td class="ReportHeader SEUM_ReportHeader"><%= TransactionNameLabel %></td>
        <td class="ReportHeader SEUM_ReportHeader"><%= CurrentDurationLabel %></td>
        <td class="ReportHeader SEUM_ReportHeader"><%= AvgDurationLabel %></td>
    </tr>

</table>

<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>