﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_Charts_EditTransactionHealthOverviewChart : UserControl, IChartEditorSettings
{
    private const string RememberExpandedStateLabel = "Remember Expanded State";
    private const string HideUnmanagedLabel = "Hide Unmanaged Transactions";

    protected void Page_Load(object sender, EventArgs e)
    {
        hideUnmanagedCheckbox.Text = HideUnmanagedLabel;
        rememberExpanded.Text = RememberExpandedStateLabel;

        filterEditor.Entity = SwisEntities.Transactions;
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        bool hideUnmanaged;
        bool rememberExpandedState;

        if (!Boolean.TryParse(resourceInfo.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "true", out hideUnmanaged))
            hideUnmanaged = true;

        hideUnmanagedCheckbox.Checked = hideUnmanaged;

        if (!Boolean.TryParse(resourceInfo.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true", out rememberExpandedState))
            rememberExpandedState = true;

        rememberExpanded.Checked = rememberExpandedState;

        if (!string.IsNullOrEmpty(resourceInfo.Properties[ResourcePropertiesKeys.Filter]))
            filterEditor.Filter = resourceInfo.Properties[ResourcePropertiesKeys.Filter];
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties[ResourcePropertiesKeys.HideUnmanaged] = hideUnmanagedCheckbox.Checked.ToString();
        properties[ResourcePropertiesKeys.Filter] = filterEditor.Filter;
        properties[ResourcePropertiesKeys.RememberCollapseState] = rememberExpanded.Checked.ToString();
    }
}