﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_SEUM_Controls_Charts_TransactionStepLocationsDurationsChartLegend : UserControl, IChartLegendControl
{
    protected const string StepNameLabel = "Step name";
    protected const string TransactionNameLabel = "Transaction name";
    protected const string CurrentDurationLabel = "Current duration";
    protected const string AvgDurationLabel = "Avg over time";

    public string LegendInitializer { get { return "seum_transactionStepLocationsDurationsLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.TransactionStepLocationsDurationsLegend.js", OrionInclude.Section.Middle);
    }
}