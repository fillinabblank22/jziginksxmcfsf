﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_Charts_EditPageElementsChart : BaseResourceEditControl
{
    protected const string countInsteadOfSizeLabel = "Use count of elements instead of their size as the chart value";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        useCountCheckbox.Text = countInsteadOfSizeLabel;

        useCountCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.SummarizeByCount] ?? "false");

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            useCountCheckbox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.SummarizeByCount + "', this.value);");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            properties[ResourcePropertiesKeys.SummarizeByCount] = useCountCheckbox.Checked.ToString();

            return properties;
        }
    }
}