﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_SEUM_Controls_Charts_TransactionStepsDurationsChartLegend : UserControl, IChartLegendControl
{
    protected const string optimalLabel = "Typical";
    protected const string percentileLabelFormat = "{0}th percentile";
    protected const string stepNameLabel = "Step name";
    protected const string stepLabel = "Step";

    public string LegendInitializer { get { return "seum_transactionStepsDurationsLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.TransactionStepsDurationsLegend.js", OrionInclude.Section.Middle);

        Percentile = Convert.ToInt32(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("Web-ChartPercentile").SettingValue);
    }

    protected int Percentile
    {
        get;
        set;
    }

    protected string PercentileLabel
    {
        get { return string.Format(percentileLabelFormat, Percentile); }
    }
}