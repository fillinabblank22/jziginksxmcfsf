﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_SEUM_Controls_Charts_TCPWaterfallLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "seum_tcpWaterfallLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

}