﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionStepsDurationsChartLegend.ascx.cs" Inherits="Orion_SEUM_Controls_Charts_TransactionStepsDurationsChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.seum_transactionStepsDurationsLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.SEUM.Charts.TransactionStepsDurationsLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
        
        
</script>

<table runat="server" ID="legend" class="DataGrid">
    <tr>
        <td class="ReportHeader SEUM_ReportHeader" colspan="2"><%: stepNameLabel %></td>
        <td class="ReportHeader SEUM_ReportHeader"><%= optimalLabel %></td>
        <td class="ReportHeader SEUM_ReportHeader"><%= PercentileLabel %></td>
    </tr>
</table>

<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
