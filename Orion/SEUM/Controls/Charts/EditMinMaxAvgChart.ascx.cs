﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_Charts_EditMinMaxAvgChart : UserControl, IChartEditorSettings
{
    protected const string DisplayThresholdsLabel = "Display following thresholds on chart";
    protected const string OptimalThresholdLabel = "optimal value";
    protected const string WarningThresholdLabel = "warning threshold";
    protected const string CriticalThresholdLabel = "critical threshold";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        optimalThresholdCheckBox.Checked = Boolean.Parse(resourceInfo.Properties[ResourcePropertiesKeys.DisplayOptimalThreshold] ?? Boolean.FalseString);
        warningThresholdCheckBox.Checked = Boolean.Parse(resourceInfo.Properties[ResourcePropertiesKeys.DisplayWarningThreshold] ?? Boolean.FalseString);
        criticalThresholdCheckBox.Checked = Boolean.Parse(resourceInfo.Properties[ResourcePropertiesKeys.DisplayCriticalThreshold] ?? Boolean.FalseString);
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties[ResourcePropertiesKeys.DisplayOptimalThreshold] = optimalThresholdCheckBox.Checked.ToString();
        properties[ResourcePropertiesKeys.DisplayWarningThreshold] = warningThresholdCheckBox.Checked.ToString();
        properties[ResourcePropertiesKeys.DisplayCriticalThreshold] = criticalThresholdCheckBox.Checked.ToString();
    }
}