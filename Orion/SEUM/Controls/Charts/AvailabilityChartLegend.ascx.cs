﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_SEUM_Controls_Charts_AvailabilityChartLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "seum_availabilityChartLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.AvailabilityChartLegend.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("SEUM", "SEUM.css");
    }
}