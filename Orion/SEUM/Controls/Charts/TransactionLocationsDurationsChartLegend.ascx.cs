﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_SEUM_Controls_Charts_TransactionLocationsDurationsChartLegend : UserControl, IChartLegendControl
{
    protected const string transactionNameLabel = "Transaction name";
    protected const string currentDurationLabel = "Current duration";
    protected const string avgDurationLabel = "Avg over time";

    public string LegendInitializer { get { return "seum_transactionLocationsDurationsLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.TransactionLocationsDurationsLegend.js", OrionInclude.Section.Middle);

    }
}