﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_Charts_EditTCPWaterfallChart : BaseResourceEditControl
{
    protected const string numberOfItemsLabel = "Maximum Number of Items to Display in Chart (use 0 for unlimited)";
    protected const string numberMustBeFilledError = "Number must be filled in";
    protected const string numberError = "Value must be a number between 0 and 1000.";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        integerValueValidate.ErrorMessage = numberError;

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.MaxCount]))
            maxCountBox.Text = Resource.Properties[ResourcePropertiesKeys.MaxCount];
        else
            maxCountBox.Text = "20";

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            maxCountBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.MaxCount + "', this.value);");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            properties.Add(ResourcePropertiesKeys.MaxCount, maxCountBox.Text);

            return properties;
        }
    }
}