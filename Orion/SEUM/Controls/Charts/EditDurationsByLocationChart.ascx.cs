﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_Charts_EditDurationsByLocationChart : UserControl, IChartEditorSettings
{
    protected const string DisplayItemsNumberLabel = "Maximum Number of Items to Display";
    protected const string numberMustBeFilledError = "Number must be filled in";
    protected const string numberError = "Value must be a number between 1 and 1000.";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        if (!string.IsNullOrEmpty(resourceInfo.Properties[ResourcePropertiesKeys.MaxCount]))
            maxCountBox.Text = resourceInfo.Properties[ResourcePropertiesKeys.MaxCount];
        else
            maxCountBox.Text = "10";
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties.Add(ResourcePropertiesKeys.MaxCount, maxCountBox.Text);
    }
}