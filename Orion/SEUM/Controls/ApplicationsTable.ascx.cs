﻿using System;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Licensing;

public partial class Orion_SEUM_Controls_ApplicationsTable : System.Web.UI.UserControl
{
    private static readonly string SWQLFormat = String.Format(@"
        SELECT 
            Application.Name AS [Application],
            '/Orion/StatusIcon.ashx?entity={0}&size=Small&id=' + TOSTRING(Application.ApplicationID) + '&status=' + TOSTRING(Application.Status) AS [_IconFor_Application],
            Application.DetailsUrl AS [_LinkFor_Application],
            Application.Node.Caption AS [Node],
            '/Orion/StatusIcon.ashx?entity={1}&size=Small&id=' + TOSTRING(Application.Node.NodeId) + '&status=' + TOSTRING(Application.Node.Status) AS [_IconFor_Node],
            Application.Node.DetailsUrl AS [_LinkFor_Node]
        FROM {0} {{0}}", 
            SwisEntities.Applications,
            SwisEntities.Nodes);

    protected const string ApplicationsAreNotAvailable = "Applications are not available in your version of the product.";

    public string Filter { get; set; }

    public int UniqueClientID { get; set; }

    public override void DataBind()
    {
        if (LicenseInfo.AreApplicationsAvailable)
        {
            ApplicationsTable.UniqueClientID = UniqueClientID;
            ApplicationsTable.SWQL = String.Format(SWQLFormat,
                !String.IsNullOrEmpty(Filter) ? " WHERE " + Filter : String.Empty);
            ApplicationsTable.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}