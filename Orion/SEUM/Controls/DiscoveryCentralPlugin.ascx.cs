﻿using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Controls_DiscoveryCentralPlugin : System.Web.UI.UserControl
{
    protected const string TitleText = "Creating and Adding a Web Transaction";
    protected const string ListItem1 = "CREATE A RECORDING";
    protected const string ListItem1SubAPart1 = "Download and install the ";
    protected const string ListItem1SubARest = "To open it, go to Start, type WPM, and select WPM Recorder.";
    protected const string ListItem1SubB = "In the recorder, navigate to the web page where you want to record steps, then ";
    protected const string ListItem1SubCpre = "Click ";
    protected const string ListItem1SubC = "when you're finished recording steps, then click";
    protected const string Click = "click";
    protected const string Stop = "Stop";
    protected const string Save = "Save";
    protected const string Record = "Record";
    protected const string ListItem2 = "CREATE A TRANSACTION BY ASSIGNING YOUR RECORDING TO A PLAYBACK LOCATION";
    protected const string ListItem2SubA = "Click Settings > All Settings > WPM Settings > Add a Transaction Monitor.";
    protected const string ListItem2SubB = "On the Add Transaction page, select the recording and click Next.";
    protected const string ListItem2SubC = "On the Location tab, click Download player and install the WPM Player on the machine where you'll play the recording.";
    protected const string ListItem2SubD = "Select a playback location for the recording and click Next.";
    protected const string ListItem2SubE = "On the Transaction Monitor tab, add a description and select a playback interval.";
    protected const string ListItem2SubF = "Enter thresholds to warn if certain steps take too long to finish and save your settings.";
    protected const string LearnMoreText = "What makes a good playback location?";
    protected const string TextPart1 = "From your desktop, record a sequence you want to monitor, go to";
    protected const string TextPart2 = "on your Orion server";
    protected const string TextPart3 = "Add a transaction to Orion, select the recording you want to monitor and assign location to play the recording.";


    private static Log _log = new Log();

    protected int EnabledMonitorCount { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            TransactionDAL dal = new TransactionDAL();
            EnabledMonitorCount = dal.GetLicensedMonitorsCount();

            if (EnabledMonitorCount == 0)
            {
                addPlaybackButton.DisplayType = ButtonType.Primary;
            }
            else
            {
                addPlaybackButton.DisplayType = ButtonType.Secondary;
            }
        }
        catch (Exception ex) {
            _log.ErrorFormat("Cannot get number of enabled objects. Details: {0}",ex);
        }

        DataBind();
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();

        Response.Redirect(String.Format("~/Orion/SEUM/Admin/Add/Default.aspx?{0}={1}", CommonParameterNames.ReturnUrl, UrlHelper.ToSafeUrlParameter(Request.RawUrl)));
    }

    protected string LearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsDiscoveryCentral");
        }
    }
}
