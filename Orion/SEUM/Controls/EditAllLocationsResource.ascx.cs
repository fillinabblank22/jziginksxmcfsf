﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using SolarWinds.Shared;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_EditAllLocationsResource : SEUMBaseResourceEditControl
{
    private Log log = new Log();

    protected string FilteredStatusesMessage = "Transactions with problems are in the selected states:";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        StatusInfo.Init(new StatusInfoProvider(), log);

        IEnumerable<int> selectedStatuses;
        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Statuses]))
        {
            selectedStatuses = Resource.Properties[ResourcePropertiesKeys.Statuses].Split(',').Select(val =>
            {
                int status;
                Int32.TryParse(val, out status);
                return status;
            });
        }
        else
            selectedStatuses = Enumerable.Empty<int>();

        var validStatuses = SEUMStatuses.ValidStatuses.Where(s => s != SEUMStatuses.Up);

        foreach (int validStatus in validStatuses)
        {
            StatusInfo status = StatusInfo.GetStatus(validStatus);
            ListItem item = new ListItem(status.ShortDescription, status.StatusId.ToString());
            item.Selected = selectedStatuses.Any(s => s == status.StatusId);
            statusesList.Items.Add(item);
        }

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Filter]))
            filterEditor.Filter = Resource.Properties[ResourcePropertiesKeys.Filter];

        filterEditor.Entity = Resource.Properties[ResourcePropertiesKeys.SwisEntity];
    }

    private List<int> GetStatuses(CheckBoxList checkboxList)
    {
        List<int> statuses = new List<int>();
        foreach (ListItem item in checkboxList.Items)
        {
            if (item.Selected)
            {
                int id;
                if (Int32.TryParse(item.Value, out id))
                    statuses.Add(id);
            }
        }

        return statuses;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            List<int> statuses = GetStatuses(statusesList);

            properties.Add(ResourcePropertiesKeys.Filter, filterEditor.Filter);
            properties.Add(ResourcePropertiesKeys.Statuses, string.Join(",", statuses.Select(s => s.ToString()).ToArray()));

            return properties;
        }
    }
}