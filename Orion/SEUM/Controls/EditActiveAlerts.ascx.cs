﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_EditActiveAlerts : BaseResourceEditControl
{
    protected const string showAcknowledgedLabel = "Show Acknowledged Alerts";

    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        _properties.Add(ResourcePropertiesKeys.ShowAcknowledgedAlerts, ShowAckAlertsCheckBox.Checked);
        string showAck = Resource.Properties[ResourcePropertiesKeys.ShowAcknowledgedAlerts];
        bool ch = false;

        if (!String.IsNullOrEmpty(showAck))
            Boolean.TryParse(showAck, out ch);

        ShowAckAlertsCheckBox.Checked = ch;
        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            ShowAckAlertsCheckBox.Attributes.Add("onclick", "SaveData('ShowAcknowledgedAlerts', this.checked ? 'True' : 'False');");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties[ResourcePropertiesKeys.ShowAcknowledgedAlerts] = ShowAckAlertsCheckBox.Checked;
            return _properties;
        }
    }
}
