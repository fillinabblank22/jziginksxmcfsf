﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLocationNodesControl.ascx.cs" Inherits="Orion_SEUM_Controls_EditLocationNodesControl" %>
<%@ Register TagPrefix="orion" TagName="NetObjectPickerDialog" Src="~/Orion/SEUM/Controls/NetObjectPickerDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="AssignObjectsControl" Src="~/Orion/SEUM/Controls/AssignObjectsControl.ascx" %>
<script type="text/javascript">
    function SetSelectionVisibility() {
        $("#SEUM_RelatedEntitiesSelection")[$("#<%=HideRelatedEntities.ClientID%>").is(":checked") ? "addClass" : "removeClass"]("hidden");
    }

    function ToggleDetails(buttonId, panelId) {
        var $panel = $("#" + panelId);
        var $button = $("#" + buttonId);

        if (panel.hasClass("hidden")) {
            $panel.removeClass("hidden");
            $button.attr("src", "/Orion/images/Button.Collapse.gif");
        } else {
            $panel.addClass("hidden");
            $button.attr("src", "/Orion/images/Button.Expand.gif");
        }
    }

    $(document).ready(function() {
        $("#<%=HideRelatedEntities.ClientID%>").change(SetSelectionVisibility);
        $("#<%=ShowRelatedEntities.ClientID%>").change(SetSelectionVisibility);
    });

</script>
<h1><%= Page.Title %></h1>
    
<orion:NetObjectPickerDialog runat="server" /> 
<div id="SEUM_RelatedEntitiesSelection_Radios">
    <asp:RadioButton runat="server" ID="HideRelatedEntities" GroupName="DisplayRelatedEntities" Checked="true" />
    <label for="<%= HideRelatedEntities.ClientID%>">
        Don't use any further informaton for advanced troubleshooting (Recomended for evaluation)
    </label>
    <br />
    <asp:RadioButton runat="server" ID="ShowRelatedEntities" GroupName="DisplayRelatedEntities" Checked="false" />
    <label for="<%= HideRelatedEntities.ClientID%>">
        Use an ability to troubleshoot location by adding related nodes (Advanced)
    </label>
</div>
<div id="SEUM_RelatedEntitiesSelection" class="hidden <% if (IsSingle){ %>SEUM_SingleLocationRelatedEntitiesSelection<% } %>">
    <div id="SEUM_RelatedEntitiesSelection_Descirption">
        You can select nodes related to a location to help you troubleshoot possible problems. By selecting related nodes you will be able to see their status right in details of Transaction using this location.
    </div>
    <asp:Repeater runat="server" ID="nodesRepeater" OnItemDataBound="nodesRepeater_ItemDataBound">
        <ItemTemplate>
            <asp:Panel runat="server" ID="propertiesBlock" CssClass="SEUM_RelatedEntitiesSelecion_Block">
                <asp:ImageButton ID="expandButton" runat="server" ImageUrl="~/Orion/images/Button.Collapse.gif" CausesValidation="false" />
                <asp:Label runat="server" ID="locationLabel" CssClass="SEUM_RelatedEntitiesSelection_Label" />
                <asp:Panel runat="server" ID="detailsPanel">
                    <div class="SEUM_RelatedEntitiesSelection_SelectorContainer">
                        <orion:AssignObjectsControl runat="server" ID="nodesSelector" EntityCaption="nodes"/> 
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ItemTemplate>
    </asp:Repeater>
</div>