﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SEUM_Controls_UnManageTransactionDialog : System.Web.UI.UserControl
{
    #region Text constants

    protected const string PollingSuspendedInfo =
        "Polling and Statistics collection will be suspended while the transactions are Unmanaged.";

    protected const string PollingRestartedInfo =
        "Polling and Statistics collection will automatically restart after this Date/Time";

    protected const string UnmanageBeginningText = "<b>Start time</b> to unmanage this transaction:";
    protected const string UnmanageUntilText = "<b>End time</b> to unmanage this transaction:";
    protected const string ClearWindowsText = "Clear scheduled unmanage times for this transaction";

    protected const string ClearWindowsTextHelpful =
        "If this transaction is set to be unmanaged on a schedule that was created from the web console, not the Unmanage utility, then select this option to clear this schedule.";
    protected const string UnmanageDuringText = "I want to unmanage this transaction during the following time period";

    protected const string UnmanageTimesWillBeRemovedText = "Following unmanage times will be removed:";
    protected const string FromToText = @"From <span class=""SEUM_FromPlaceholder""></span> to <span class=""SEUM_ToPlaceholder""></span>";

    #endregion
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}