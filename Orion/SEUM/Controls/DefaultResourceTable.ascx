﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefaultResourceTable.ascx.cs"
    Inherits="Orion_SEUM_Controls_DefaultResourceTable" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Controls.ResourceTable" %>
<%@ Register TagPrefix="SEUM" TagName="CellContent" Src="~/Orion/SEUM/Controls/ResourceTableCellContent.ascx" %>

<table class="DataGrid">
    <tr>
        <asp:Repeater runat="server" ID="headerRepeater">
            <ItemTemplate>
                <td class="ReportHeader SEUM_ReportHeader <%# ((ResourceTableColumn)Container.DataItem).CssClass %>">
                    <%# ((ResourceTableColumn)Container.DataItem).Name %>
                </td>
            </ItemTemplate>
        </asp:Repeater>
    </tr>
<asp:Repeater runat="server" ID="resourceTable" OnItemDataBound="resourceTable_ItemDataBound">
    <ItemTemplate>
        <tr>
            <asp:Repeater runat="server" ID="rowRepeater">
                <ItemTemplate>
                    <td class="Property SEUM_Property <%# ((ResourceTableCell)Container.DataItem).Column.CssClass %>">
                        <SEUM:CellContent runat="server" Cell="<%# (ResourceTableCell)Container.DataItem %>" />
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr>
            <asp:Repeater runat="server" ID="rowRepeater">
                <ItemTemplate>
                    <td class="ZebraStripe Property SEUM_Property <%# ((ResourceTableCell)Container.DataItem).Column.CssClass %>">
                        <SEUM:CellContent runat="server" Cell="<%# (ResourceTableCell)Container.DataItem %>" />
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </AlternatingItemTemplate>
</asp:Repeater>
</table>
