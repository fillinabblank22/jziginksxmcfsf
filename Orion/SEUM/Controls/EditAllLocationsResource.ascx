﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllLocationsResource.ascx.cs" Inherits="Orion_SEUM_Controls_EditAllLocationsResource" %>
<%@ Register TagPrefix="SEUM" TagName="FilterEditor" Src="~/Orion/SEUM/Controls/FilterEditor.ascx" %>

<p>
    <b><%= FilteredStatusesMessage %></b><br />
    <asp:CheckBoxList runat="server" ID="statusesList" />
</p>

<SEUM:FilterEditor runat="server" ID="filterEditor" />