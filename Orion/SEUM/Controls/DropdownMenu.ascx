﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DropdownMenu.ascx.cs" Inherits="Orion_SEUM_Controls_DropdownMenu" %>
<div class="sw-seum-dropdown-menu" data-seum-dropdown="true">
    <div class="sw-seum-dropdown-menu__trigger">
        <img class="sw-seum-dropdown-menu__trigger__icon" src="<%= "/Orion/SEUM/Images/" + Icon %>" /><a><%= MenuTitle  %></a>
    </div>
    <div class="sw-seum-dropdown-menu__content" style="display: none">
        <% foreach(var item in Items) { %>
            <a href="<%= item.Url %>" class="sw-seum-dropdown-menu__content__item"><%= item.Label %></a>
        <% } %>
    </div>
</div>
