﻿using System.Web.UI;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Controls_FilterEditor : System.Web.UI.UserControl
{
    private string _entity;

    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);

        string script = @"//<![CDATA[
        $(function () {
            $('.ExpanderLink').toggle(
                        function () {
                            $('img:first', this).attr('src', '/Orion/images/Button.Collapse.gif');
                            $('#propertiesContainer').slideDown('fast');
                        },
                        function () {
                            $('img:first', this).attr('src', '/Orion/images/Button.Expand.gif');
                            $('#propertiesContainer').slideUp('fast');
                        }
                    )
        });
        //]]>";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "SEUM_FilterEditorJS", script, true);
    }

    public string Filter
    {
        get { return filterBox.Text; }
        set { filterBox.Text = value; }
    }

    public string Entity
    {
        get { return _entity; }
        set
        {
            _entity = value;
            SwisMetadataDAL dal = new SwisMetadataDAL();
            propertiesRepeater.DataSource = dal.GetEntityProperties(_entity, SwisMetadataDAL.PropertyType.FilterBy);
            propertiesRepeater.DataBind();
        }
    }
}