using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Common;

public partial class Orion_SEUM_Controls_EventList : System.Web.UI.UserControl
{
    private const int defaultMaxEventCount = 25;
    private const string defaultPeriod = "Last 12 Months";

    private string subTitle = "";

    private string periodName = "";
    public string PeriodName 
    { 
        get 
        { 
            return periodName; 
        } 
        set 
        { 
            periodName = value; 
        } 
    }
    public int MaxEventCount { get; set; }
    public IEnumerable<int> ItemIds { get; set; }
    public string ItemType { get; set; }
    public int ResourceID { get; set; }

    public void LoadData()
    {
        var startDate = new DateTime();
        var endDate = new DateTime();

        if (MaxEventCount <= 0)
        {
            MaxEventCount = defaultMaxEventCount;
        }

        if (String.IsNullOrEmpty(periodName))
        {
            periodName = defaultPeriod;
        }
        
        Periods.Parse(ref periodName, ref startDate, ref endDate);
        subTitle = periodName;

        EventsDAL dal = new EventsDAL();

        DataTable eventsTable;
        switch (ItemType)
        {
            case NetObjectPrefix.Transaction:
                eventsTable = dal.GetTransactionEvents(ItemIds, MaxEventCount, startDate, endDate);
                break;
            case NetObjectPrefix.Location:
                eventsTable = dal.GetTransactionEventsForAgent(ItemIds, MaxEventCount, startDate, endDate);
                break;
            default:
                eventsTable = dal.GetAllEvents(MaxEventCount, startDate, endDate);
                break;
        }

        EventsGrid.DataSource = eventsTable;
        EventsGrid.DataBind();
    }

    public string DefaultTitle
    {
        get { return "Last XX Events"; }
    }

    public string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesLastXXEvents"; }
    }

    public string SubTitle
    {
        get
        {
            return subTitle;
        }
    }

	public string EditControlLocation
	{
		get { return "/Orion/SEUM/Controls/EditEventList.ascx"; }
	} 
}
