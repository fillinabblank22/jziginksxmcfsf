﻿using System.Web.UI;
using SolarWinds.SEUM.Web.Controls;

public partial class Orion_SEUM_Controls_DropdownMenu : UserControl
{
    public string MenuTitle { get; set; }
    public string Icon { get; set; }
    public DropdownMenuItem[] Items { get; set; }
}
