﻿using System;
using System.Collections.Generic;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_EditStepDetailsResource : SEUMBaseResourceEditControl
{
    protected const string showActionsListLabel = "Show Actions List";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.ShowActions]))
            showActionsCheckBox.Checked = (Resource.Properties[ResourcePropertiesKeys.ShowActions] != "0");
        else
            showActionsCheckBox.Checked = true;

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            showActionsCheckBox.Attributes.Add("onclick", "SaveData('" + ResourcePropertiesKeys.ShowActions + "', this.checked ? '1' : '0');");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            properties.Add(ResourcePropertiesKeys.ShowActions, showActionsCheckBox.Checked ? "1" : "0");

            return properties;
        }
    }
}