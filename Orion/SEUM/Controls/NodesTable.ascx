﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodesTable.ascx.cs" Inherits="Orion_SEUM_NodesTable" %>
<%@ Import Namespace="SolarWinds.SEUM.Licensing" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
    <% if (LicenseInfo.AreNodesAvailable)
       { %>
    <div class="sw-seum-custom-query-table-wrapper">
        <orion:CustomQueryTable runat="server" ID="NodesTable" />
    </div>
    <script type="text/javascript">
        SW.Core.Resources.CustomQuery.initialize({
            uniqueId: "<%= NodesTable.UniqueClientID %>",
            allowSearch: false,
            customFormatters: {
                "Current Response Time": function (value) { return !isNaN(value) && value >= 0 ? value + " ms" : "No response."; },
                "Packet Loss": function (value) { return !isNaN(value) && value >= 0 ? value + " %" : ""; }
            }
        });

        SW.Core.Resources.CustomQuery.refresh("<%= NodesTable.UniqueClientID %>");
    </script>
    <% } else { %>
    <% =NodesAreNotAvailable %>
    <% } %>