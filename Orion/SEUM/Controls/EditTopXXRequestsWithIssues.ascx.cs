﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.Shared;

public partial class Orion_SEUM_Controls_EditTopXXRequestsWithIssues : SEUMBaseResourceEditControl
{
    protected const string numberOfItemsLabel = "Page size";
    protected const string statusCategoryLabel = "Display requests with status code";
    protected const string numberMustBeFilledError = "Number must be filled in";
    protected const string numberError = "Value must be a number between 1 and 1000.";
    
    protected const string statusInformation = "Information (100-199)";
    protected const string statusSuccess = "Success (200-299)";
    protected const string statusRedirect = "Redirect (300-399)";
    protected const string statusClientError = "Client error (400-499)";
    protected const string statusServerError = "Server error (500-599)";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        integerValueValidate.ErrorMessage = numberError;

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.MaxCount]))
            maxCountBox.Text = Resource.Properties[ResourcePropertiesKeys.MaxCount];
        else
            maxCountBox.Text = "10";

        // generate request statuses list
        BrowserRequest.StatusCategory selectedStatuses = BrowserRequest.StatusCategory.ClientError | BrowserRequest.StatusCategory.ServerError;
        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.RequestStatusCategory]))
        {
            int status = 0;
            if (int.TryParse(Resource.Properties[ResourcePropertiesKeys.RequestStatusCategory], out status))
            {
                selectedStatuses = (BrowserRequest.StatusCategory) status;
            }
        }

        const string statusUpdateFormat = @"if ($('#requests_statuses_{0}').is(':checked')) {{
  status |= $('#requests_statuses_{0}').val();
}}
";
        // generate JavaScript to handle settings update on Custom Object Resource
        StringBuilder script = new StringBuilder("");
        script.AppendLine(
                @"
// attach handlers to checkboxes for status categories
$(document).ready(function() {
    $('#requests_statuses').find('input:checkbox').click(function() { UpdateRequestStatusCategory(); });
});

function UpdateRequestStatusCategory() {
  var status = 0;");

        // Make ID static to find it easily. It's not a problem on edit page.
        statusesList.ClientIDMode = ClientIDMode.Static;
        statusesList.ID = "requests_statuses";

        int index = 0;
        ListItem item = new ListItem(statusInformation, ((int)BrowserRequest.StatusCategory.Information).ToString(CultureInfo.InvariantCulture));
        item.Selected = (selectedStatuses & BrowserRequest.StatusCategory.Information) != 0;
        statusesList.Items.Add(item);
        script.AppendFormat(statusUpdateFormat, index++);

        item = new ListItem(statusSuccess, ((int)BrowserRequest.StatusCategory.Success).ToString(CultureInfo.InvariantCulture));
        item.Selected = (selectedStatuses & BrowserRequest.StatusCategory.Success) != 0;
        statusesList.Items.Add(item);
        script.AppendFormat(statusUpdateFormat, index++);

        item = new ListItem(statusRedirect, ((int)BrowserRequest.StatusCategory.Redirect).ToString(CultureInfo.InvariantCulture));
        item.Selected = (selectedStatuses & BrowserRequest.StatusCategory.Redirect) != 0;
        statusesList.Items.Add(item);
        script.AppendFormat(statusUpdateFormat, index++);

        item = new ListItem(statusClientError, ((int)BrowserRequest.StatusCategory.ClientError).ToString(CultureInfo.InvariantCulture));
        item.Selected = (selectedStatuses & BrowserRequest.StatusCategory.ClientError) != 0;
        statusesList.Items.Add(item);
        script.AppendFormat(statusUpdateFormat, index++);

        item = new ListItem(statusServerError, ((int)BrowserRequest.StatusCategory.ServerError).ToString(CultureInfo.InvariantCulture));
        item.Selected = (selectedStatuses & BrowserRequest.StatusCategory.ServerError) != 0;
        statusesList.Items.Add(item);
        script.AppendFormat(statusUpdateFormat, index++);

        script.AppendLine("SaveData('" + ResourcePropertiesKeys.RequestStatusCategory + "', status);");
        script.AppendLine("}");

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Filter]))
            filterEditor.Filter = Resource.Properties[ResourcePropertiesKeys.Filter];

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            // during ajax call swis entity is not properly set so we must set it here (we know that this edit resource is used only for "Screenshots of Last XX .. Failures")
            if (!Resource.Properties.ContainsKey(ResourcePropertiesKeys.SwisEntity))
            {
                if (NetObjectID.StartsWith("T:") || NetObjectID.StartsWith("TS:"))
                    Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.StepResponseTimeDetail;
            }

            maxCountBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.MaxCount + "', this.value);");
            filterEditor.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Filter + "', this.value);");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "SEUM_EditTopXXRequestsWithIssuesJS", script.ToString(), true);
        }

        filterEditor.Entity = Resource.Properties[ResourcePropertiesKeys.SwisEntity];
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            properties.Add(ResourcePropertiesKeys.MaxCount, maxCountBox.Text);
            properties.Add(ResourcePropertiesKeys.Filter, filterEditor.Filter);

            int status = 0;
            foreach (ListItem item in statusesList.Items)
            {
                if (item.Selected)
                {
                    int id;
                    if (Int32.TryParse(item.Value, out id))
                    {
                        status |= id;
                    }
                }
            }
            properties.Add(ResourcePropertiesKeys.RequestStatusCategory, status);

            return properties;
        }
    }
}