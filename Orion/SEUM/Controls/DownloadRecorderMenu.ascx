﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownloadRecorderMenu.ascx.cs" Inherits="Orion_SEUM_Controls_DownloadRecorderMenu" %>
<%@ Register TagPrefix="seum" TagName="DropdownMenu" Src="~/Orion/SEUM/Controls/DropdownMenu.ascx" %>

<seum:DropdownMenu ID="DownloadMenu" Icon="Icon.Download.svg" MenuTitle="Download Recorder" Items="<%# DownloadMenuItems %>" runat="server"></seum:DropdownMenu>
