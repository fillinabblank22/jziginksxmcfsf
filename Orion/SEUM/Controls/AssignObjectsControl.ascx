﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignObjectsControl.ascx.cs" Inherits="Orion_SEUM_Controls_AssignObjectsControl" ClassName="SolarWinds.SEUM.Controls.AssignObjectsControl" EnableViewState="true"%>
<orion:Include runat="server" File="js/OrionCore.js"/>
<orion:Include runat="server" File="SEUM/js/AssignObjectsControl.js"/>
<asp:Panel ID="Container" runat="server">
    <div class="SEUM_AssignObjectsControl_ObjectsSelection">
        <img class="SEUM_AssignObjectsControl_ExpandButton" src="/Orion/images/Button.Collapse.gif"/>
        <span class="SEUM_AssignObjectsControl_Label"></span>
        <a href="javascript:void()" class="SEUM_AssignObjectsControl_EditButton">Edit</a>
        <table class="SEUM_AssignObjectsControl_Table">
        </table>
    </div>
    <asp:Button ID="AddObjectsButton" runat="server" CssClass="SEUM_AssignObjectsControl_AddButton" />
    <asp:HiddenField ID="SelectedObjectsField" runat="server"/>
    <script type="text/javascript">
        var control = new SW.SEUM.AssignObjectsControl("<%=Container.ClientID%>", "<%=SelectedObjectsField.ClientID%>", "<%=EntityType%>", "<%=EntityCaption%>");
    </script>
</asp:Panel>