﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Web.NetObjects;
using System.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_SEUM_Controls_ViewTitle : System.Web.UI.UserControl
{
    protected const string fromText = "from";
    protected const string netObjectDetailUrl = @"/Orion/View.aspx?NetObject=";

    private List<SEUMNetObjectBase> netObjects = new List<SEUMNetObjectBase>();

    public string ViewName
    {
        get;
        set;
    }

    protected override void OnLoad(System.EventArgs e)
    {
        base.OnLoad(e);

        netObjectsRepeater.DataSource = NetObjects;
        netObjectsRepeater.DataBind();
    }

    public void AddNetObject(SEUMNetObjectBase netObject)
    {
        netObjects.Add(netObject);
    }

    public IEnumerable<SEUMNetObjectBase> NetObjects
    {
        get { return netObjects; }
    }

    protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;

        var standardNetobjectLink = e.Item.FindControl("netobjectLink") as HyperLink;
        var locationObjectLink = e.Item.FindControl("locationLink") as HyperLink;
        var fromLiteral = e.Item.FindControl("fromLiteral") as Literal;
        var entityStatusIcon = e.Item.FindControl("entityStatusIcon") as Orion_Controls_EntityStatusIcon;

        var netobject = (SEUMNetObjectBase)e.Item.DataItem;
        if (e.Item.DataItem is TransactionObject)
        {
            var transaction = (TransactionObject)e.Item.DataItem;
            standardNetobjectLink.Text = UIHelper.Escape(transaction.Recording.Name);
            fromLiteral.Text = fromText;
            locationObjectLink.Text = transaction.AgentName;
            locationObjectLink.NavigateUrl = netObjectDetailUrl + new TransactionLocationObject(transaction.Agent).NetObjectID;
        }
        else
        {
            standardNetobjectLink.Text = netobject.Name;
            fromLiteral.Visible = false;
            locationObjectLink.Visible = false;
        }

        if(e.Item.DataItem is TransactionLocationObject)
        {
            var location = (TransactionLocationObject) e.Item.DataItem;
            entityStatusIcon.Status = location.Model.ConnectionStatus;
        }
        
        standardNetobjectLink.NavigateUrl = netObjectDetailUrl + netobject.NetObjectID;
    }
}