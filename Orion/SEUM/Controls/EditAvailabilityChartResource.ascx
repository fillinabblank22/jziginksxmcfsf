﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAvailabilityChartResource.ascx.cs" Inherits="Orion_SEUM_Controls_EditAvailabilityChartResource" %>

<p>
    <b><%= timePeriodLabel %>:</b><br />
    <asp:DropDownList runat="server" ID="timePeriodCombo" onchange="UpdateIntervals();" />
</p>

<p class="hidden" id="customPeriodFrom">
    <b><%= customPeriodFromLabel %>:</b><br />
    <asp:TextBox runat="server" ID="customPeriodFromBox" /> (<%= DateFormat %> [<%= TimeFormat %>])
    <asp:CustomValidator runat="server" ID="customPeriodFromValidator" ControlToValidate="customPeriodFromBox" Display="Dynamic" ValidateEmptyText="true" />
</p>

<p class="hidden" id="customPeriodTo">
    <b><%= customPeriodToLabel %>:</b><br />
    <asp:TextBox runat="server" ID="customPeriodToBox" /> (<%= DateFormat %> [<%= TimeFormat %>])
    <asp:CustomValidator runat="server" ID="customPeriodToValidator" ControlToValidate="customPeriodToBox" Display="Dynamic" ValidateEmptyText="true" />
</p>

<p>
    <b><%= sampleIntervalLabel %>:</b><br />
    <asp:DropDownList runat="server" ID="sampleIntervalCombo" /> <span class="SEUM_HelpText"><%= sampleIntervalHelpText %></span>
</p>

<p>
    <b><%= widthLabel %>:</b><br />
    <asp:TextBox runat="server" ID="widthTextBox" />
    <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="widthTextBox"
        Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="0" ErrorMessage="Value must be a number between 0 and 10000."
        SetFocusOnError="true"></asp:RangeValidator>
    <span class="SEUM_HelpText"><%= dimensionHelpText %></span>
</p>

<p>
    <b><%= heightLabel %>:</b><br />
    <asp:TextBox runat="server" ID="heightTextBox" />
    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="heightTextBox"
        Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="0" ErrorMessage="Value must be a number between 0 and 10000."
        SetFocusOnError="true"></asp:RangeValidator>
    <span class="SEUM_HelpText"><%= dimensionHelpText %></span>
</p>
