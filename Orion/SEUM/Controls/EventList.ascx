<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventList.ascx.cs" Inherits="Orion_SEUM_Controls_EventList" %>

<asp:Repeater ID="EventsGrid" runat="server">
	<HeaderTemplate>
		<table width="100%" cellspacing="0" cellpadding="3" class="Events">
	</HeaderTemplate>
	
	<ItemTemplate>
		<tr>
            <td style="width: 100px; background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>; text-align:center;" ><%#Eval("EventTime", "{0:g}") %></td>
            <td style="width: 25px; background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">
                <img src="/NetPerfMon/images/Event-<%#Eval("EventType") %>.gif" alt="Event Type" ></td>
            <td style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">
                <asp:HyperLink ID="ItemHyperLink" runat="server" NavigateUrl='<%#Eval("URL")%>'><%#Eval("Message") %></asp:HyperLink></td>
		</tr>
	</ItemTemplate>
	
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>
