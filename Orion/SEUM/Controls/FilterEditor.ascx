﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterEditor.ascx.cs" Inherits="Orion_SEUM_Controls_FilterEditor" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.DAL" %>

<p>
    <b>Filter (SWQL):</b><br />
    <asp:TextBox runat="server" ID="filterBox" Rows="1" Columns="60" />
</p>
<p class="SEUM_HelpText">
    Filters are optional and can be used to limit the list of items displayed.<br />
    This is an advanced feature. We recommend you have a basic understanding of SWQL Queries.<br />
    List of properties that can be used in filter expression is below.
</p>

<p>
    <a href="javascript:void(0);" class="ExpanderLink">
        <img src="/Orion/images/Button.Expand.gif" />
        <b>Properties available for filtering</b>
    </a>
    <div id="propertiesContainer" style="display: none;">
        <p>
            Following table contains all properties that can be used in "Filter" field.<br /> 
            "Property Name" contains descriptive name of property. If name consists of multiple parts separated by "-", property belongs to some related entity.<br />
            "Property Syntax" is the exact form of property that can be used in filter expression.<br />
            "Property Type" contains type of data stored in property.<br />
            To use a property in filter, take "Property Syntax" value for the property and use it in filter expression.
        </p>
        <asp:Repeater runat="server" ID="propertiesRepeater">
            <HeaderTemplate>
                <table>
                    <tr>
                        <th class="ReportHeader SEUM_ReportHeader">Property name</th>
                        <th class="ReportHeader SEUM_ReportHeader">Property syntax</th>
                        <th class="ReportHeader SEUM_ReportHeader">Property type</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property SEUM_Property"><%# ((SwisMetadataDAL.EntityProperty)Container.DataItem).DisplayName %></td>
                    <td class="Property SEUM_Property"><%# ((SwisMetadataDAL.EntityProperty)Container.DataItem).NameForFilter %></td>
                    <td class="Property SEUM_Property"><%# ((SwisMetadataDAL.EntityProperty)Container.DataItem).SimpleType %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</p>
