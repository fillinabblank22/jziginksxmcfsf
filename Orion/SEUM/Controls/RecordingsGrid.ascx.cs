﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Controls_RecordingsGrid : System.Web.UI.UserControl
{
    protected const string LearnMoreText = "Learn more";
    protected const string descriptionPart1 = "To edit the content of a recording, <a id=\"SEUMopenRecordingLink\">download the recording</a> or go to:";
    protected const string descriptionPart3 = "on your Orion server.";
    protected const string descriptionPart4 = "Open the recording in the WPM Recorder and edit your sequence.";
    public string DownloadNewRecorderText = string.Format("Download {0}", CommonTexts.DownloadNewRecorderText);
    public string DownloadLegacyRecorderText = string.Format("Download {0}", CommonTexts.DownloadLegacyRecorderText);

    #region Text Validation Dialog

    protected const string InformationText = @"The status of this step will be 'down' if Match Content does not validate the text on this page.";
    protected const string ValidateTextPart1 = @"Validate that the current page";
    protected const string ValidateTextPart2 = @"the following text:";
    protected const string ContainsText = @"contains";
    protected const string DoesNotContainText = @"does not contain";
    protected const string RecommendedWaitTimeText = @"Use recommended maximum wait time (30s):";
    protected const string WaitTimeInfoText = @"Wait time for text to appear. Some elements are generated dynamically and may not be available if playback is faster than script generating them.";
    
    #endregion // Text Validation Dialog

    private bool isEditGrid = true;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public int PageSize
    {
        // TODO
        get { return 20; }
    }

    public bool IsEditGrid
    {
        get { return isEditGrid; }
        set { isEditGrid = value; }
    }

    protected String SelectedRecordingsFieldClientID
    {
        get { return selectedRecordings.ClientID; }
    }

    public IEnumerable<Int32> SelectedRecordings
    {
        get
        {
            string[] ids = selectedRecordings.Value.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string idStr in ids)
            {
                int id = 0;
                if (Int32.TryParse(idStr, out id))
                    yield return id;
            }

            yield break;
        }
        set
        {
            selectedRecordings.Value = string.Join(",", value.Select(id => id.ToString()).ToArray());
        }
    }

    protected string ExcludedRecordingsString
    {
        get
        {
            if (ExcludedRecordingsIds == null)
                return String.Empty;

            return string.Join(",", ExcludedRecordingsIds.Select(id => id.ToString()).ToArray());
        }
    }

    public IEnumerable<Int32> ExcludedRecordingsIds
    {
        get;
        set;
    }

    protected string UnsupportedRecordingsString
    {
        get
        {
            if (UnsupportedRecordingsIds == null)
                return string.Empty;

            return string.Join(",", UnsupportedRecordingsIds.Select(id => id.ToString()).ToArray());
        }
    }

    public IEnumerable<Int32> UnsupportedRecordingsIds
    {
        get;
        set;
    }

    protected string ImportLearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGRecordingsManagingImporting");
        }
    }

    protected string LearnMoreHelpUrlForLegacyRecorder
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGRecordingsManaging");
        }
    }
    protected string LearnMoreHelpUrlForNewRecorder
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionWPMAdminGuide_EditRecordings");
        }
    }
}