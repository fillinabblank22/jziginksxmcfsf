﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormattedValue.ascx.cs" Inherits="Orion_SEUM_Controls_FormattedValue" %>

<% if (IsCriticalInternal) { %><span class="SEUM_CriticalValueText"><%= FormattedValue %></span>
<% } else if (IsWarningInternal) { %><span class="SEUM_WarningValueText"><%= FormattedValue %></span>
<% } else { %><%= FormattedValue %><% } %>