﻿using System;
using System.Collections.Generic;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_EditTopXXResource : SEUMBaseResourceEditControl
{
    protected const string numberOfItemsLabel = "Maximum Number of Items to Display";
    protected const string numberMustBeFilledError = "Number must be filled in";
    protected const string numberError = "Value must be a number between 1 and 1000.";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        integerValueValidate.ErrorMessage = numberError;

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.MaxCount]))
            maxCountBox.Text = Resource.Properties[ResourcePropertiesKeys.MaxCount];
        else
            maxCountBox.Text = "10";

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Filter]))
            filterEditor.Filter = Resource.Properties[ResourcePropertiesKeys.Filter];

        if (SolarWinds.SEUM.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            // during ajax call swis entity is not properly set so we must set it here (we know that this edit resource is used only for "Screenshots of Last XX .. Failures")
            if (!Resource.Properties.ContainsKey(ResourcePropertiesKeys.SwisEntity))
            {
                if (NetObjectID.StartsWith("T:") || NetObjectID.StartsWith("TS:"))
                    Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.StepResponseTimeDetail;
            }

            maxCountBox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.MaxCount + "', this.value);");
            filterEditor.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Filter + "', this.value);");
        }

        filterEditor.Entity = Resource.Properties[ResourcePropertiesKeys.SwisEntity];
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            properties.Add(ResourcePropertiesKeys.MaxCount, maxCountBox.Text);
            properties.Add(ResourcePropertiesKeys.Filter, filterEditor.Filter);

            return properties;
        }
    }
}