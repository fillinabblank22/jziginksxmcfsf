﻿using System;
using SolarWinds.SEUM.Licensing;

public partial class Orion_SEUM_NodesTable : System.Web.UI.UserControl
{
    private const string SWQL = @"
        SELECT 
            Nodes.Caption as [Name],
            '/Orion/StatusIcon.ashx?entity=Orion.Nodes&size=Small&id=' + TOSTRING(Nodes.NodeId) + '&status=' + TOSTRING(Nodes.Status) as [_IconFor_Name],
            Nodes.DetailsUrl as [_LinkFor_Name],
            Nodes.Stats.ResponseTime AS [Current Response Time],
            Nodes.Stats.PercentLoss AS [Packet Loss]
        FROM Orion.Nodes";

    protected const string NodesAreNotAvailable = "Nodes are not available in your version of the product.";

    public string Filter { get; set; }

    public int UniqueClientID { get; set; }

    public override void DataBind()
    {
        if (LicenseInfo.AreNodesAvailable)
        {
            NodesTable.UniqueClientID = UniqueClientID;
            NodesTable.SWQL = SWQL + (!String.IsNullOrEmpty(Filter) ? " WHERE " + Filter : String.Empty);
            NodesTable.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
}