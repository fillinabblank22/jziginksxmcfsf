﻿<%@ Control Language="C#" AutoEventWireup="true" ClassName="ViewTitle" CodeFile="ViewTitle.ascx.cs" Inherits="Orion_SEUM_Controls_ViewTitle" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<%= ViewName %>

<asp:Repeater runat="server" ID="netObjectsRepeater" OnItemDataBound="Repeater_ItemDataBound">
    <ItemTemplate>
        -
        <orion:EntityStatusIcon ID="entityStatusIcon" 
                                runat="server" 
                                IconSize="Small" 
                                Entity="<%# ((SEUMNetObjectBase)Container.DataItem).SwisEntity %>" 
                                Status="<%# ((SEUMNetObjectBase)Container.DataItem).Status %>" 
                                WrapInBox="false" />
            <asp:HyperLink ID="netobjectLink" runat="server"></asp:HyperLink>
            <asp:Literal ID="fromLiteral" runat="server" />
            <asp:HyperLink ID="locationLink" runat="server"></asp:HyperLink>
    </ItemTemplate>
</asp:Repeater>
