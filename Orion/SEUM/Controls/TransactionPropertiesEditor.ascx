﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionPropertiesEditor.ascx.cs" Inherits="Orion_SEUM_Controls_TransactionPropertiesEditor" %>
<%@ Import Namespace="SolarWinds.SEUM.Common.Models" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="~/Orion/SEUM/js/TransactionPropertiesEditor.js" />
    </Scripts>
</asp:ScriptManagerProxy>

<script type="text/javascript">
    //<![CDATA[
    Ext.namespace('SW');
    Ext.namespace('SW.SEUM');

    SW.SEUM.TransactionPropertiesEditor<%= EditorIndex %> = new function () {
        this.ConvertIntervalUnitCombo = function (elementId, value) {
            var combo = new Ext.form.ComboBox({
                store: new Ext.data.ArrayStore({
                    fields: ['itemValue', 'itemLabel'],
                    data: [['s', 'seconds'],
                            ['m', 'minutes'],
                            ['h', 'hours'],
                            ['d', 'days']]
                }),
                valueField: 'itemValue',
                displayField: 'itemLabel',
                triggerAction: 'all',
                mode: 'local',
                width: 80,
                editable: false,
                transform: elementId,
                hiddenId: elementId,
                cls: "SEUM_Left",
                listeners: {
                    select: function (record, index) {
                        ValidatorValidate(document.getElementById('<%= frequencyValidator.ClientID %>'));
                    }
                }
            });

            combo.setValue(value);
        };

        this.ConvertToNumericTextBox = function (elementId, width) {
            if (!width)
                width = 50;

            var box = new Ext.form.TextField({
                width: width,
                maskRe: /\d/,
                applyTo: elementId
            });
        };

        this.ConvertToNumericWithFloatingPointSupportTextBox = function (elementId, width) {
            if (!width)
                width = 50;

            var box = new Ext.form.TextField({
                width: width,
                // Support for "." and "," as floating point delimiter
                maskRe: /[0-9<%= CurrentCultureNumberDecimalDelimiter %>]/,
                regex: /^((\d+(<%= CurrentCultureNumberDecimalDelimiter %>\d*)?)|((\d*<%= CurrentCultureNumberDecimalDelimiter %>)?\d+))?$/,
                applyTo: elementId
            });
        };

        this.ConvertDescriptionBox = function (elementId) {
            var box = new Ext.form.TextField({
                autoScroll: true,
                applyTo: elementId
            });
        };

        this.ValidateFrequency = function (source, arguments) {
            var validator = $(source);
            var unitBox = $('#' + validator.attr('unitBoxId'));
            var minSeconds = parseInt(validator.attr('minSeconds'));
            var defaultSeconds = parseInt(validator.attr('defaultSeconds'));
            var maxSeconds = parseInt(validator.attr('maxSeconds'));
            var maxDays = parseInt(validator.attr('maxDays'));
            var seconds = 0;
            if (!isNaN(arguments.Value))
                seconds = parseInt(arguments.Value);
            if (isNaN(seconds))
                seconds = 0;

            switch (unitBox.val()) {
                case 's':
                    // nothing, already have seconds
                    break;
                case 'm':
                    seconds *= 60;
                    break;
                case 'h':
                    seconds *= 3600;
                    break;
                case 'd':
                    seconds *= 86400;
                    break;
            }


            if (seconds < minSeconds || seconds > maxSeconds) {
                arguments.IsValid = false;

                validator.text(String.format('<%= InvalidFrequencyTextFormat %>', minSeconds, maxDays));
                $('#SEUM_FrequencyWarning<%= EditorIndex %>').hide();

                <%= ValidationFailedClientHandler %>
            } else if(seconds < defaultSeconds) {
                arguments.IsValid = true;
                validator.text('');

                $('#SEUM_FrequencyWarning<%= EditorIndex %>').show();
            } else {
                arguments.IsValid = true;
                validator.text('');
                $('#SEUM_FrequencyWarning<%= EditorIndex %>').hide();
            }
        };

        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        this.ValidateThresholds = function (source, arguments) {
            var validator = $(source);
            var optimalBox = $('#' + validator.attr('optimalBoxId'));
            var warningBox = $('#' + validator.attr('warningBoxId'));
            var criticalBox = $('#' + validator.attr('criticalBoxId'));
            var minSeconds = parseInt(validator.attr('minSeconds'));
            var maxSeconds = parseInt(validator.attr('maxSeconds'));

            var optimal = 0;
            var optimalStr = optimalBox.val().replace(",", ".");
            if (isNumber(optimalStr))
                optimal = parseFloat(optimalStr);
            var warning = 0;
            var warningStr = warningBox.val().replace(",", ".");
            if (isNumber(warningStr))
                warning = parseFloat(warningStr);
            var critical = 0;
            var criticalStr = criticalBox.val().replace(",", ".");
            if (isNumber(criticalStr))
                critical = parseFloat(criticalStr);

            if (critical < minSeconds || critical > maxSeconds) {
                arguments.IsValid = false;
                validator.text(String.format('<%= InvalidCriticalThresholdTextFormat %>', minSeconds, maxSeconds));
                <%= ValidationFailedClientHandler %>
            } else if (warning < minSeconds || warning > maxSeconds) {
                arguments.IsValid = false;
                validator.text(String.format('<%= InvalidWarningThresholdTextFormat %>', minSeconds, maxSeconds));
                <%= ValidationFailedClientHandler %>
            } else if (optimal < minSeconds || optimal > maxSeconds) {
                arguments.IsValid = false;
                validator.text(String.format('<%= InvalidOptimalThresholdTextFormat %>', minSeconds, maxSeconds));
                <%= ValidationFailedClientHandler %>
            } else if (critical > 0 && warning > critical) {
                arguments.IsValid = false;
                validator.text('<%= InvalidWarningThresholdText %>');
                <%= ValidationFailedClientHandler %>
            } else if (warning > 0 && optimal > warning) {
                arguments.IsValid = false;
                validator.text('<%= InvalidOptimalThresholdText %>');
                <%= ValidationFailedClientHandler %>
            } else {
                arguments.IsValid = true;
                validator.text('');
            }
        };
    };

    SW.SEUM.TransactionPropertiesEditor.SetDelimiter('<%= CurrentCultureNumberDecimalDelimiter %>');
//]]>
</script>

<table class="SEUM_TransactionPropertiesTable" runat="server" id="mainTable">
    <tr>
        <th class="SEUM_FormLabel"><%= DescriptionLabel  %>:</th>
        <td><asp:TextBox runat="server" ID="descriptionBox" Rows="3" CssClass="SEUM_TransactionDescriptionBox" TextMode="MultiLine" /></td>
    </tr>
    <tr>
        <th class="SEUM_FormLabel"><%= PlaybackIntervalLabel  %>:</th>
        <td>
            <table>
                <tr>
                    <td style="padding-bottom: 0px;"><%= EveryLabel  %></td>
                    <td style="padding-bottom: 0px;"><asp:TextBox runat="server" ID="intervalValueBox" CssClass="SEUM_Right" /></td>
                    <td style="padding-bottom: 0px; width: 80px"><asp:TextBox runat="server" ID="intervalUnitBox" /></td>
                    <td style="padding-bottom: 0px;"><div class="sw-suggestion sw-suggestion-warn" style="display: none" id="SEUM_FrequencyWarning<%= EditorIndex %>"><span class="sw-suggestion-icon"></span><%= FrequencyWarningText %></div><asp:CustomValidator ID="frequencyValidator" ControlToValidate="intervalValueBox" ValidateEmptyText="true"  runat="server" OnServerValidate="ValidateFrequency" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <th class="SEUM_FormLabel"><%= ThresholdsLabel  %>:</th>
        <td>
            <asp:Repeater runat="server" ID="stepsRepeater">
                <HeaderTemplate>
                    <table class="SEUM_StepsThresholdsTable" id="SEUM_StepsThresholdsTable<%= EditorIndex %>" cellspacing="0">
                        <tr>
                            <th class="SEUM_RightBorder SEUM_LightBorder"><span class="SEUM_ColumnHeader"><%= StepNameLabel %></span></th>
                            <th class="SEUM_LeftBorder SEUM_TopBorder SEUM_DarkBorder">
                                <asp:CheckBox ID="enableAllThresholdsCheckBox" runat="server" />
                            </th>
                            <th class="SEUM_TopBorder SEUM_DarkBorder"><span class="sw-text-helpful">&raquo;</span></th>
                            <th class="SEUM_TopBorder SEUM_DarkBorder">
                                <span class="SEUM_NoWrap"><orion:EntityStatusIcon ID="warningStatusIcon" runat="server" IconSize="Small" Status="3" WrapInBox="false" /><%= WarningLabel %></span>
                            </th>
                            <th class="SEUM_TopBorder SEUM_DarkBorder"><span class="sw-text-helpful">&raquo;</span></th>
                            <th class="SEUM_RightBorder SEUM_TopBorder SEUM_DarkBorder">
                                <span class="SEUM_NoWrap"><orion:EntityStatusIcon ID="criticalStatusIcon" runat="server" IconSize="Small" Status="14" WrapInBox="false" /><%= CriticalLabel %></span>
                            </th>
                            <th class="SEUM_LeftBorder SEUM_LightBorder">
                                <span class="SEUM_StepTypical SEUM_NoWrap"><%= OptimalLabel %></span>
                            </th>
                            <th></th>
                        </tr>
                        <tr class="SEUM_HelpRow">
                            <td class="SEUM_RightBorder SEUM_LightBorder"></td>
                            <td colspan="5" class="SEUM_LeftBorder SEUM_RightBorder SEUM_DarkBorder"><span class="sw-text-helpful"><%= EnableDisableLabel %></span></td>
                            <td colspan="2" class="SEUM_LeftBorder SEUM_LightBorder"><span class="SEUM_NoWrap"><span class="sw-text-helpful"><%= OptimalThresholdHelpText %></span> <a href="<%= OptimalThresholdLearnMoreHelpUrl %>" class="SEUM_HelpLink" target="_blank">&#0187;&nbsp;<%= WhatIsThisText%></a></span></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %> SEUM_RightBorder SEUM_LightBorder">
                            <orion:EntityStatusIcon ID="entityStatusIcon" runat="server" IconSize="Small" Status="0" WrapInBox="false" />
                            <%# UIHelper.Escape(((TransactionStep)Container.DataItem).Name) %>
                        </td>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %> <%# Container.ItemIndex == Transaction.Steps.Count - 1 ? "SEUM_BottomBorder" : "" %> SEUM_LeftBorder SEUM_DarkBorder"><asp:CheckBox ID="enableThresholdsCheckBox" runat="server" /></td>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %> <%# Container.ItemIndex == Transaction.Steps.Count - 1 ? "SEUM_BottomBorder" : "" %> SEUM_DarkBorder"><span class="sw-text-helpful">&raquo;</span></td>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %> <%# Container.ItemIndex == Transaction.Steps.Count - 1 ? "SEUM_BottomBorder" : "" %> SEUM_DarkBorder SEUM_Right"><span class="SEUM_StepWarning SEUM_NoWrap"><asp:TextBox runat="server" ID="warningBox" CssClass="SEUM_Right" />&nbsp;s</span></td>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %> <%# Container.ItemIndex == Transaction.Steps.Count - 1 ? "SEUM_BottomBorder" : "" %> SEUM_DarkBorder"><span class="sw-text-helpful">&raquo;</span></td>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %> <%# Container.ItemIndex == Transaction.Steps.Count - 1 ? "SEUM_BottomBorder" : "" %> SEUM_Right SEUM_RightBorder SEUM_DarkBorder"><span class="SEUM_StepCritical SEUM_NoWrap"><asp:TextBox runat="server" ID="criticalBox" CssClass="SEUM_Right" />&nbsp;s</span></td>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %> SEUM_Left SEUM_LeftBorder SEUM_LightBorder"><span class="SEUM_StepTypical SEUM_NoWrap"><asp:TextBox runat="server" ID="optimalBox" CssClass="SEUM_Right" />&nbsp;s</span></td>
                        <td class="<%# GetZebraStripeClass(Container.ItemIndex) %>">
                            <asp:CustomValidator ID="thresholdsValidator" ValidateEmptyText="true"  runat="server" OnServerValidate="ValidateThresholds" />
                            <script type="text/javascript">
                            //<![CDATA[
                                <asp:Literal runat="server" ID="jsConvertLiteral" />
                            //]]>
                            </script>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <tr>
                        <td></td>
                        <td colspan="7">
                            <orion:LocalizableButton ID="reapplyDefaultThresholdsButton" LocalizedText="CustomText" Text="Re-apply default thresholds" DisplayType="Secondary" runat="server" CausesValidation="false"  OnClick="ReapplyDefaultThresholdsClick" />
                            <span class="SEUM_HelpText"><%= ReapplyDefaultThresholdsLabel %></span>
                        </td>
                    </tr>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </td>
    </tr>
    <tr>
        <th class="SEUM_FormLabel">
            <div>
                <a href="javascript:void(0);" id="SEUM_ExpanderLink_<%= EditorIndex %>"><img src="/Orion/Images/Button.Expand.gif" alt="+" /> <%= AdvancedLabel %></a>
            </div>
        </th>
        <td>
            <div id="SEUM_TransactionAdvancedSettings_<%= EditorIndex %>" class="PlayerSettings" style="display: none;">
                <!-- Proxy definition fields -->
                <div class="sw-seum-settngs-proxy">
                    <asp:Label ID="proxyUrlLabel" runat="server" AssociatedControlID="proxyUrlBox"><%= ProxyUrlLabel %>:
                        <span class="LinkArrow">&#0187;</span> <a href="<%= HowToUseProxyHelpUrl %>" class="SEUM_HelpLink" target="_blank"><%= WhatIsProxyLabel %></a>
                    </asp:Label>
                    <asp:TextBox runat="server" ID="proxyUrlBox" CssClass="SEUM_EditBox" />
                </div>

                <div class="sw-seum-settings-screenshots">
                    <b><%= ScreenshotCapturesLabel %></b>
                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="screenshotCapturesRadio" />
                    <% if (!ScreenshotCapturePerTransaction) { %><div class="sw-suggestion"><span class="sw-suggestion-icon"></span><%= ScreenshotCaptureSetGloballyLabel%></div><% } %>

                    <div class="sw-text-helpful"><%= ScreenshotCaptureDescriptionLabel %></div>
                </div>
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
//<![CDATA[
    <asp:Literal runat="server" ID="jsConvertLiteral" />

    $(function () {
        SW.SEUM.TransactionPropertiesEditor.EnableAllCheckBoxChangeState(<%= EditorIndex %>);
        ValidatorValidate(document.getElementById('<%= frequencyValidator.ClientID %>'));
                $('#SEUM_ExpanderLink_<%= EditorIndex %>').toggle(
                    function () {
                            $('img:first', this).attr('src', '/Orion/Images/Button.Collapse.gif');
                            $('#SEUM_TransactionAdvancedSettings_<%= EditorIndex %>').slideDown('fast');
                    },
                    function () {
                            $('img:first', this).attr('src', '/Orion/Images/Button.Expand.gif');
                            $('#SEUM_TransactionAdvancedSettings_<%= EditorIndex %>').slideUp('fast');
                    }
                );
    });
//]]>
</script>
