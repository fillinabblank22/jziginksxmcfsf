﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentsGrid.ascx.cs" Inherits="Orion_SEUM_Controls_AgentsGrid" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Configuration" %>
<%@ Import Namespace="SolarWinds.SEUM.Licensing" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="~/Orion/js/OrionCore.js"/>
        <asp:ScriptReference Path="~/Orion/SEUM/js/SearchField.js" />
        <asp:ScriptReference Path="~/Orion/SEUM/js/CheckboxSelectionModel.js" />
        <asp:ScriptReference Path="~/Orion/SEUM/js/AgentsGrid.js" />
    </Scripts>
</asp:ScriptManagerProxy>

<div id="AgentsGrid" class="SEUM_ExtJsGrid"></div>

<div id="ForceDeployWindow" style="display: none;">
    <div class="x-panel-body sw-seum-credential-window">
        <p><%= ChooseExistingOrCreateNewOneText %></p>
        <table>
            <tr class="sw-seum-first-row">
                <td><%= ChooseCredentialText %></td><td><select id="CredentialNamesCombo"><option value="test">test</option></select></td>
            </tr>
            <tr class="sw-seum-hideable">
                <td><%= UserNameText %></td><td><input type="text" id="CredentialUsername" value="" /></td>
            </tr>
            <tr class="sw-seum-hideable">
                <td><%= PasswordText %></td><td><input type="password" id="CredentialPassword" value="" /></td>
            </tr>
        </table>
    </div>
</div>


<asp:HiddenField runat="server" ID="selectedAgents"/>

<script type="text/javascript">
        // <![CDATA[
        SW.SEUM.AgentsGrid.SetEditGrid(<%= this.IsEditGrid.ToString().ToLower() %>);
        SW.SEUM.AgentsGrid.SetSelectedAgentsFieldClientID('<%= this.SelectedAgentsFieldClientID %>');
        SW.SEUM.AgentsGrid.SetPageSize(<%= this.PageSize %>);
        SW.SEUM.AgentsGrid.SetUserName("<%=HttpUtility.JavaScriptStringEncode(Context.User.Identity.Name.Trim())%>");
        SW.SEUM.AgentsGrid.SetExcludedAgents("<%=this.ExcludedAgentsString%>");
        SW.SEUM.AgentsGrid.SetUnsupportedAgents("<%=this.UnsupportedAgentsString%>");
        SW.SEUM.AgentsGrid.SetReturnUrl("<%= this.ReturnUrl %>");
        SW.SEUM.AgentsGrid.SetAgentLoadWarningThreshold(<%= SEUMWebConfiguration.Instance.AgentLoadWarningThreshold %>);
        SW.SEUM.AgentsGrid.SetAgentLoadErrorThreshold(<%= SEUMWebConfiguration.Instance.AgentLoadErrorThreshold %>);
        SW.SEUM.AgentsGrid.SetRelatedNodesVisible(<%=LicenseInfo.AreNodesAvailable.ToString().ToLower()%>);
        // ]]>
</script>

<!-- This is used to grab the results from a failed Web Service call -->
<div id="test" class="x-hidden"></div>
