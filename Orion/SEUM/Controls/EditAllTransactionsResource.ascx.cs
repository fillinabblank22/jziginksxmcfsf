﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.Shared;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Controls_EditResourceControls_EditAllTransactionsResource : SEUMBaseResourceEditControl
{
    private const string ShowDurationsLabel = "Show Durations";
    private const string RememberExpandedStateLabel = "Remember Expanded State";
    private const string HideUnmanagedLabel = "Hide Unmanaged Transactions";
    private const string ExpandRootLevelLabel = "Always Expand Root Level";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        rememberStateCheckbox.Text = RememberExpandedStateLabel;
        showDurationsCheckbox.Text = ShowDurationsLabel;
        hideUnmanagedCheckbox.Text = HideUnmanagedLabel;
        expandRootLevelCheckbox.Text = ExpandRootLevelLabel;

        foreach (var value in Enum.GetValues(typeof(EvaluationMethod)))
        {
            this.statusRollup.Items.Add(new ListItem(value.ToString(), value.ToString()));
        }

        SwisMetadataDAL dal = new SwisMetadataDAL();
        foreach (SwisMetadataDAL.EntityProperty property in dal.GetEntityProperties(SwisEntities.Transactions, SwisMetadataDAL.PropertyType.GroupBy))
        {
            // unlicensed components are not displayed at all on website
            if (property.Name == "Unmanaged")
                continue;

            this.groupByList.Items.Add(new ListItem(property.DisplayName, property.QualifiedName));
        }

        this.rememberStateCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true");
        this.expandRootLevelCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.ExpandRootLevel] ?? "false");
        this.showDurationsCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.ShowDurations] ?? "false");
        this.hideUnmanagedCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "true");
        this.groupByList.SelectedValue = Resource.Properties[ResourcePropertiesKeys.Grouping];
        this.orderByList.SelectedValue = Resource.Properties[ResourcePropertiesKeys.Ordering];
        this.statusRollup.SelectedValue = Resource.Properties[ResourcePropertiesKeys.StatusRollup] ?? EvaluationMethod.Mixed.ToString();

        if (!string.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Filter]))
            filterEditor.Filter = Resource.Properties[ResourcePropertiesKeys.Filter];

        filterEditor.Entity = SwisEntities.Transactions;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            // if grouping or ordering changed, clear remembered expansion state because it's not valid anymore
            if ((Resource.Properties.ContainsKey(ResourcePropertiesKeys.Grouping) &&
                Resource.Properties[ResourcePropertiesKeys.Grouping] != this.groupByList.SelectedValue)
             || (Resource.Properties.ContainsKey(ResourcePropertiesKeys.Ordering) &&
                Resource.Properties[ResourcePropertiesKeys.Ordering] != this.orderByList.SelectedValue))
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

            properties[ResourcePropertiesKeys.RememberCollapseState] = this.rememberStateCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.ExpandRootLevel] = this.expandRootLevelCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.ShowDurations] = this.showDurationsCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.HideUnmanaged] = this.hideUnmanagedCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.Grouping] = this.groupByList.SelectedValue;
            properties[ResourcePropertiesKeys.Ordering] = this.orderByList.SelectedValue;
            properties[ResourcePropertiesKeys.StatusRollup] = this.statusRollup.SelectedValue;
            properties[ResourcePropertiesKeys.Filter] = this.filterEditor.Filter;

            // We changed stuff so clear out the expanded tree node information
            if (!this.rememberStateCheckbox.Checked)
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

            return properties;
        }
    }
}
