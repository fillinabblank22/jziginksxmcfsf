﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllTransactionsResource.ascx.cs"
    Inherits="Orion_SEUM_Controls_EditResourceControls_EditAllTransactionsResource" %>
<%@ Register TagPrefix="SEUM" TagName="FilterEditor" Src="~/Orion/SEUM/Controls/FilterEditor.ascx" %>

<p>
    <b>Group by:</b><br />
    <asp:DropDownList ID="groupByList" runat="server" Width="183">
        <asp:ListItem Text="None" Value="" />
    </asp:DropDownList>
</p>

<p>
    <b>Group status rollup type:</b><br />
    <asp:DropDownList ID="statusRollup" runat="server" Width="183">
    </asp:DropDownList>
</p>

<p>
    <b>Order by:</b><br />
    <asp:DropDownList ID="orderByList" runat="server" Width="183">
        <asp:ListItem Text="Name" Value="Name" />
        <asp:ListItem Text="Status" Value="Status" />
    </asp:DropDownList>
</p>

<p> 
    <asp:CheckBox runat="server" ID="rememberStateCheckbox" />
</p>

<p> 
    <asp:CheckBox runat="server" ID="expandRootLevelCheckbox" />
</p>

<p> 
    <asp:CheckBox runat="server" ID="showDurationsCheckbox" />
</p>

<p> 
    <asp:CheckBox runat="server" ID="hideUnmanagedCheckbox" />
</p>

<SEUM:FilterEditor runat="server" ID="filterEditor" />
