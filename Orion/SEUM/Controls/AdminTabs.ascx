﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminTabs.ascx.cs" Inherits="Orion_SEUM_Controls_AdminTabs" %>
<%
    this.AddTab("Transaction Monitors", VirtualPathUtility.ToAbsolute("~/Orion/SEUM/Admin/ManageMonitors.aspx"), IsPageRegexMatch("(^|/)ManageMonitors.aspx$"));
    this.AddTab("Recordings", VirtualPathUtility.ToAbsolute("~/Orion/SEUM/Admin/ManageRecordings.aspx"), IsPageRegexMatch("(^|/)ManageRecordings.aspx$"));
    this.AddTab("Transaction Locations", VirtualPathUtility.ToAbsolute("~/Orion/SEUM/Admin/ManagePlayers.aspx"), IsPageRegexMatch("(^|/)ManagePlayers.aspx$"));
%>

<script type="text/javascript">
    $(document).ready(function () {
        $(".SEUM_AdminTabs li.sw-tab").bind('mouseover', function () { $(this).addClass('x-tab-strip-over'); });
        $(".SEUM_AdminTabs li.sw-tab").bind('mouseout', function () { $(this).removeClass('x-tab-strip-over'); });
    });
</script>

<div class="SEUM_AdminTabs">
    <div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">
        <div class="x-tab-strip-wrap">
            <ul class="x-tab-strip x-tab-strip-top">
                <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />
                <li class="x-tab-edge"></li>
                <div class="x-clear">
                    <!-- -->
                </div>
            </ul>
            <%--<div class="x-tab-strip-spacer" style="border-bottom: none;">
                <!-- -->
            </div>--%>
        </div>
    </div>
</div>
