﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Controls;
using SolarWinds.SEUM.Licensing;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Controls_TransactionDependenciesControl : System.Web.UI.UserControl
{
    public Transaction Transaction { get; set; }

    private bool _overrideTransactionDependencies;
    private IEnumerable<string> _transactionDependencies;
    public IEnumerable<string> TransactionDependencies
    {
        get
        {
            if (_transactionDependencies == null)
            {
                LoadDependencies();
            }
            return _transactionDependencies;
        }
        set
        {
            _overrideTransactionDependencies = true;
            _transactionDependencies = value;
        }
    } 

    private bool _overrideStepsDependencies;
    private IDictionary<TransactionStep, IEnumerable<string>> _stepsDependencies;
    public IDictionary<TransactionStep, IEnumerable<string>> StepsDependencies
    {
        get
        {
            if (_stepsDependencies == null)
            {
                LoadDependencies();
            }
            return _stepsDependencies;
        }
        set
        {
            _overrideStepsDependencies = true;
            _stepsDependencies = value;
        }
    } 

    private readonly Lazy<DependenciesDAL> _dependenciesDal = new Lazy<DependenciesDAL>(); 

    public override void DataBind()
    {
        if (LicenseInfo.AreNodesAvailable)
        {
            nodesSelector.EntityType = SwisEntities.Nodes;
            if (_transactionDependencies != null)
            {
                nodesSelector.SelectedObjectUris =
                    _dependenciesDal.Value.FilterSwisUrisByEntityType(_transactionDependencies, SwisEntities.Nodes);
            }
        }
        if (LicenseInfo.AreApplicationsAvailable)
        {
            applicationsSelector.EntityType = SwisEntities.Applications;
            if (_transactionDependencies != null)
            {
                applicationsSelector.SelectedObjectUris =
                    _dependenciesDal.Value.FilterSwisUrisByEntityType(_transactionDependencies,
                        SwisEntities.Applications);
            }
        }
        if (Transaction != null)
        {
            LoadDependencies();
            stepsRepeater.DataSource = Transaction.Steps;
            stepsRepeater.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDependencies();
    }

    protected void stepsRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemIndex < 0)
            return;

        TransactionStep step = e.Item.DataItem as TransactionStep;

        Label stepLabel = e.Item.FindControl("transactionStepLabel") as Label;
        
        if (LicenseInfo.AreNodesAvailable)
        {
            AssignObjectsControl nodesSelector = e.Item.FindControl("nodesSelector") as AssignObjectsControl;
            nodesSelector.EntityType = SwisEntities.Nodes;
            if (_stepsDependencies != null && _stepsDependencies.ContainsKey(step))
            {
                nodesSelector.SelectedObjectUris =
                    _dependenciesDal.Value.FilterSwisUrisByEntityType(_stepsDependencies[step], SwisEntities.Nodes);
            }
        }
        if (LicenseInfo.AreApplicationsAvailable)
        {
            AssignObjectsControl applicationsSelector =
                e.Item.FindControl("applicationsSelector") as AssignObjectsControl;
            applicationsSelector.EntityType = SwisEntities.Applications;
            if (_stepsDependencies != null && _stepsDependencies.ContainsKey(step))
            {
                applicationsSelector.SelectedObjectUris =
                    _dependenciesDal.Value.FilterSwisUrisByEntityType(_stepsDependencies[step],
                        SwisEntities.Applications);
            }
        }

        stepLabel.Text = step.RecordingStep.Name;
    }

    private void LoadDependencies()
    {
        if (!_overrideTransactionDependencies)
        {
            _transactionDependencies = nodesSelector != null
                ? nodesSelector.SelectedObjectUris
                : Enumerable.Empty<string>();
            _transactionDependencies = applicationsSelector != null
                ? TransactionDependencies.Concat(applicationsSelector.SelectedObjectUris)
                : TransactionDependencies;
        }
        if (Transaction == null)
        {
            return;
        }
        if (!_overrideStepsDependencies)
        {
            var index = 0;
            _stepsDependencies = new Dictionary<TransactionStep, IEnumerable<string>>(Transaction.Steps.Count);
            foreach (TransactionStep step in Transaction.Steps)
            {
                RepeaterItem item = stepsRepeater.Items[index++];

                var nodes = Enumerable.Empty<String>();
                var applications = Enumerable.Empty<String>();

                if (LicenseInfo.AreNodesAvailable)
                {
                    var stepNodesSelector = (AssignObjectsControl) item.FindControl("nodesSelector");
                    nodes = stepNodesSelector.SelectedObjectUris;
                }

                if (LicenseInfo.AreApplicationsAvailable)
                {
                    var stepApplicationsSelector = (AssignObjectsControl) item.FindControl("applicationsSelector");
                    applications = stepApplicationsSelector.SelectedObjectUris;
                }

                _stepsDependencies[step] = nodes.Concat(applications);
            }
        }
    }
}