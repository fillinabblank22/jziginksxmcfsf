﻿using SolarWinds.Logging;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Exceptions;
using System;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Admin_SEUMAdminPage : System.Web.UI.MasterPage
{
    private static readonly Log log = new Log();

    protected bool _isOrionDemoMode = false;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                _isOrionDemoMode = true;
            else
                throw new SEUMUnauthorizedAccessException();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "Global",
                                                           this.ResolveClientUrl("~/Orion/SEUM/js/SEUM.js"));

        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }

        SettingsDAL settingsDal = new SettingsDAL();

        if (settingsDal.IsFirstRun())
        {
            log.Info("First website run");

            AgentsDAL agentsDal = new AgentsDAL();

            using (var proxy = BusinessLayerFactory.Create())
            {
                if (!agentsDal.IsAnyAgent())
                {
                    log.Info("Adding default agent");
                    proxy.AgentAdd(agentsDal.GetDefaultAgent());
                }
                else
                {
                    log.Info("Default agent already present");
                }

                proxy.SetSettingValue(Settings.FirstRunSettingKey, "0");
            }
        }
    }

    public string HelpFragment { get; set; }
}
