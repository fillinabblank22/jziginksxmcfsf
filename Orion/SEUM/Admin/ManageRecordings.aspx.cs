﻿using System;
using SolarWinds.Orion.Web;

public partial class Orion_SEUM_Admin_ManageRecordings : System.Web.UI.Page, IBypassAccessLimitation
{
    protected const string PageTitle = "Manage Recordings";
    protected const string HowToMonitorTransactionText = "How to monitor transactions";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = PageTitle;

        HelpLink.HelpUrlFragment = "OrionSEUMAGTransactions";
        HelpLink.HelpDescription = HowToMonitorTransactionText;
    }
}