﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/SEUMAdminPage.master" AutoEventWireup="true" CodeFile="EditPlayer.aspx.cs" Inherits="Orion_SEUM_Admin_EditPlayer" %>
<%@ Register TagPrefix="orion" TagName="NetObjectPickerDialog" Src="~/Orion/SEUM/Controls/NetObjectPickerDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="AssignObjectsControl" Src="~/Orion/SEUM/Controls/AssignObjectsControl.ascx" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Import Namespace="SolarWinds.SEUM.Licensing" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Orion/js/OrionCore.js"/>
            <asp:ScriptReference Path="~/Orion/SEUM/js/EditPlayer.js" />
        </Scripts>
    </asp:ScriptManagerProxy>

    <h1><%= Page.Title %></h1>
    <p>
        <%= MainDescription %>
        <a href="<%= InstallingPlayerHelpUrl %>" class="SEUM_HelpLink" target="_blank"><span class="LinkArrow">&#0187;</span> <%= LearnMoreLabel %></a>
        <a href="<%= CommonLinks.PlayerInstaller %>" class="SEUM_HelpLink" target="_blank"><span class="LinkArrow">&#0187;</span> <%= DownloadPlayerText %></a>
    </p>

    <div class="GroupBox PlayerSettings">
        <asp:HiddenField runat="server" ID="originalAgentDnsNameBox" />
        <asp:HiddenField runat="server" ID="originalAgentIpBox" />
        <asp:HiddenField runat="server" ID="originalAgentPollingEngineId" />
        <asp:HiddenField runat="server" ID="originalAgentPollingEngineName" />
        <asp:HiddenField runat="server" ID="agentIdBox" />
        <asp:HiddenField runat="server" ID="agentDnsNameBox" />
        <asp:HiddenField runat="server" ID="agentIpBox" />
        <asp:HiddenField runat="server" ID="originalAgentGuidBox" />
        <asp:HiddenField runat="server" ID="originalAgentUrlBox" />
        <asp:HiddenField runat="server" ID="originalAgentModeBox" />
        <asp:HiddenField runat="server" ID="agentStatusBox" />
        <asp:HiddenField runat="server" ID="agentStatusMessageBox" />
        <asp:HiddenField runat="server" ID="pollingEngineActiveId" />
        <asp:HiddenField runat="server" ID="agentVersionFromTest" />

        <asp:Label runat="server" AssociatedControlID="nameBox"><b><%= NameLabel %>:</b></asp:Label>
        <asp:TextBox runat="server" ID="nameBox" CssClass="SEUM_EditBox" />
        <asp:CustomValidator runat="server" ID="nameValidator" ControlToValidate="nameBox" Display="Dynamic" />

        <span class="SEUM_PlayerSettingsSectionLabel"><b><%= CommunicationTypeLabel %>:</b></span>

        <asp:Label runat="server" AssociatedControlID="passiveRadio"><asp:RadioButton runat="server" ID="passiveRadio" GroupName="modeGroup" /> <%= PassiveRadioLabel %>  <a href="<%= LearnMoreActivePassiveUrl %>" class="SEUM_HelpLink" target="_blank"><span class="LinkArrow">&#0187;</span> <%= LearnMoreLabel %></a></asp:Label>
        <div class="SEUM_HelpText"><%= PassiveHelpText %></div>

        <!-- PASSIVE config -->
        <div class="SEUM_PlayerAdvancedSettings" id="SEUM_PassiveSettings" style="display: none;">
            <asp:Label runat="server" AssociatedControlID="urlBox"><%= IPLabel %>:</asp:Label>
            <asp:TextBox runat="server" ID="urlBox" CssClass="SEUM_EditBox" />
            <asp:CustomValidator runat="server" ID="urlValidator" ControlToValidate="urlBox" Display="Dynamic" />
        
            <!-- PASSIVE advanced config -->
            <div id="SEUM_ToggleAdvancedPassiveDiv">
                <img id="CollapsePassiveImg" src="/Orion/Images/Button.Expand.gif" /> <%= AdvancedLabel %>
                <asp:CheckBox ID="advancedPassiveCheckbox" runat="server" style="display: none;" />
            </div>  
            <div id="SEUM_PlayerAdvancedPassiveSettings" style="display: none;">
                <!-- Password field -->
                <asp:Label runat="server" AssociatedControlID="passwordPassiveBox"><%= PasswordLabel %>:
                    <span class="LinkArrow">&#0187;</span> <a href="<%= LearnMoreHelpUrl %>" class="SEUM_HelpLink" target="_blank"><%= WhatIsThisLabel %></a>
                </asp:Label>
                <asp:TextBox runat="server" TextMode="Password" ID="passwordPassiveBox" class="SEUM_EditBox" />

                <!-- Port field -->
                <asp:Label runat="server" AssociatedControlID="portBox"><%= PortLabel%>:</asp:Label>
                <asp:TextBox runat="server" ID="portBox" CssClass="SEUM_EditBox" />
                <asp:CustomValidator runat="server" ID="portValidator" ControlToValidate="portBox" Display="Dynamic" />

                <div style="display: <%= ShowPollerDropDown ? "block" : "none" %>;">
                <asp:Label runat="server" AssociatedControlID="pollingEnginePassive"><%= AssignToPollerLabel %>:</asp:Label>
                <asp:DropDownList ID="pollingEnginePassive" runat="server" CssClass="SEUM_EditBox">
                </asp:DropDownList>
                <asp:CustomValidator runat="server" ID="passivePollerValidator" ControlToValidate="pollingEnginePassive" Display="Dynamic" />
                </div>

                <!-- Proxy definition fields -->
                <asp:Label runat="server" AssociatedControlID="useProxyCheckbox"><asp:Checkbox runat="server" ID="useProxyCheckbox" /> <%= UseProxyLabel %></asp:Label> 
                <div id="SEUM_ProxySettings" style="display:none;">
                    <asp:Label runat="server" AssociatedControlID="proxyUrlBox"><%= ProxyUrlLabel %>:
                        <span class="LinkArrow">&#0187;</span> <a href="<%= WhatIsProxyHelpUrl %>" class="SEUM_HelpLink" target="_blank"><%= WhatIsProxyLabel %></a>
                    </asp:Label>
                    <asp:TextBox runat="server" ID="proxyUrlBox" CssClass="SEUM_EditBox" />   
                    <asp:CustomValidator runat="server" ID="proxyValidator" ControlToValidate="proxyUrlBox" Display="Dynamic" />

                
                    <!-- Proxy authentication definition fields -->
                    <asp:Label runat="server" AssociatedControlID="useProxyAuthenticationCheckbox"><asp:Checkbox runat="server" ID="useProxyAuthenticationCheckbox" /> <%= UseProxyAuthenticationLabel %></asp:Label> 
                    <div id="SEUM_ProxyAuthenticationSettings" style="display:none;">
                        <asp:Label runat="server" AssociatedControlID="proxyUserNameBox"><%= ProxyUserNameLabel %>: </asp:Label>
                        <asp:TextBox runat="server" ID="proxyUserNameBox" CssClass="SEUM_EditBox" /> 
                        <asp:Label runat="server" AssociatedControlID="proxyPasswordBox"><%= ProxyPasswordLabel %>: </asp:Label>
                        <asp:TextBox runat="server" TextMode="Password" ID="proxyPasswordBox" CssClass="SEUM_EditBox" />   
                        <asp:CustomValidator runat="server" ID="proxyAuthenticationValidator" ControlToValidate="proxyUserNameBox" Display="Dynamic" />
                    </div>
                </div>
            </div>
            <!-- / PASSIVE advanced config -->
        </div>
        <!-- / PASSIVE config -->

        <asp:Label runat="server" AssociatedControlID="activeRadio"><asp:RadioButton runat="server" ID="activeRadio" GroupName="modeGroup" /> <%= ActiveRadioLabel %>  <a href="<%= LearnMoreActivePassiveUrl %>" class="SEUM_HelpLink" target="_blank"><span class="LinkArrow">&#0187;</span> <%= LearnMoreLabel %></a></asp:Label>
        <div class="SEUM_HelpText"><%= ActiveHelpText %></div>

        <!-- ACTIVE config -->
        <div class="SEUM_PlayerAdvancedSettings" id="SEUM_ActiveSettings" style="display: none;">
            <asp:Label runat="server" AssociatedControlID="guidBox"><%= ActiveAgentLabel%>:</asp:Label>
            <select id="activeAgentsCombo" class="SEUM_EditBox">
            </select>
            <asp:TextBox runat="server" ID="guidBox" CssClass="SEUM_EditBox x-hidden" />
            <asp:CustomValidator runat="server" ID="guidValidator" ControlToValidate="guidBox" Display="Dynamic" />
            

            <!-- ACTIVE advanced config -->
            <div id="SEUM_ToggleAdvancedActiveDiv">
                <img id="CollapseActiveImg" src="/Orion/Images/Button.Expand.gif" /> <%= AdvancedLabel %>
                <asp:CheckBox ID="advancedActiveCheckbox" runat="server" style="display: none;" />
            </div>  
            <div id="SEUM_PlayerAdvancedActiveSettings" style="display: none;">

                <div style="display:block">
                <asp:Label runat="server" AssociatedControlID="pollingEngineActiveName"><%= AssignedToPollerLabel %>:</asp:Label>
                <asp:TextBox runat="server" ID="pollingEngineActiveName" CssClass="SEUM_EditBox" />
                </div>
            </div>
            <!-- / ACTIVE advanced config -->
        </div>
        <!-- / ACTIVE config -->

        <% if (IsEditPage)
           { %>
        <!-- Remote settings config -->
        <span class="SEUM_PlayerSettingsSectionLabel"><b><%= RemoteSettingsLabel%>:</b></span>

        <asp:Label runat="server" AssociatedControlID="updateRemoteSettingsCheckbox"><asp:CheckBox runat="server" ID="updateRemoteSettingsCheckbox" /> <%= RemoteSettingsCheckboxLabel%></asp:Label>
        <div class="SEUM_HelpText"><%= RemoteSettingsHelpText%></div>

        <!-- Troubleshooting config -->
        <div id="ToggleTroubleshootDiv">
            <img id="TroubleshootCollapseImg" src="/Orion/Images/Button.Expand.gif" /> <%= TroubleshootingLabel%>
        </div>
        <div id="TroubleshootDivLoading" style="display: none;">
            <img src="/Orion/images/loading_gen_small.gif" /> <%= LoadingText%> ...
        </div>
        <div id="TroubleshootDiv" style="display: none;">
            <asp:Label AssociatedControlID="logLevelCombo" runat="server"><%= LogLevelLabel %>:</asp:Label>
            <asp:DropDownList ID="logLevelCombo" runat="server" CssClass="SEUM_EditBox">
                <asp:ListItem value="">No change</asp:ListItem>
                <asp:ListItem value="INFO">INFO</asp:ListItem>
                <asp:ListItem value="DEBUG">DEBUG</asp:ListItem>
                <asp:ListItem value="VERBOSE">VERBOSE</asp:ListItem>
            </asp:DropDownList>
            <label><%= DiagnosticsLabel%>:</label> 
            <span id="SEUM_DiagnosticsStatus"></span>
            <br />
            <a href="javascript:void(0);" id="GenerateDiagnosticsLink" class="sw-link" target="_blank"><%= GenerateNewDiagnosticsLabel%></a>
        </div>
        <div id="TroubleshootDivError" class="sw-suggestion sw-suggestion-fail" style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <%= LocationNotRespondingError%>
        </div>
        <!-- / Troubleshooting config -->
        <%
           } %>

        <div id="SEUM_TestProgress" style="display: none;">
            <img src="/Orion/images/loading_gen_small.gif" /> <%= TestingText %> ...
        </div>
        <div id="SEUM_TestResults" class="sw-suggestion" style="display: none;">
            <span class="sw-suggestion-icon"></span>
            <div class="sw-suggestion-title"></div>
            <div id="SEUM_TestDetail">
                <br /><%= PlayerNotRespondingError %> <a href="<%= InstallingPlayerHelpUrl %>" class="SEUM_HelpLink" target="_blank"><span class="LinkArrow">&#0187;</span> <%= LearnMoreLabel %></a>
            </div>
        </div>
        
        <% if (LicenseInfo.AreNodesAvailable)
           { %>
        <!--
        <div id="SEUM_PlayerRelatedNodes">
            <orion:NetObjectPickerDialog runat="server" /> 
            <asp:Label runat="server" ID="RelatedNodesLabel" CssClass="SEUM_PlayerRelatedNodes_Label"><b>Select related Nodes</b></asp:Label>
            <div id="SEUM_PlayerRelatedNodes_Selector">
                <orion:AssignObjectsControl runat="server" ID="nodesSelector" EntityCaption="nodes"/>     
            </div>
        </div>
            -->
        <% } %>

        <orion:LocalizableButton ID="LocalizableButton1" LocalizedText="Test" DisplayType="Small" runat="server" OnClick="testButton_Click" OnClientClick="return SEUMDisableInDemo();" CssClass="SEUM_AgentTestButton" />

        <div class="SEUM_EditAgentButtons">
            <orion:LocalizableButton LocalizedText="Submit" DisplayType="Primary" runat="server" OnClientClick="return SEUMDisableInDemo();" OnClick="Save_Click" />
            <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" CausesValidation="false" OnClick="Cancel_Click" />
        </div>
    </div>

    <script type="text/javascript" defer="defer">
      //<![CDATA[
        $(document).ready(function () {
            SW.SEUM.EditPlayer.SetAgentIdBoxId('<%= agentIdBox.ClientID %>');
            SW.SEUM.EditPlayer.SetAgentDNSNameBoxId('<%= agentDnsNameBox.ClientID %>');
            SW.SEUM.EditPlayer.SetAgentIPBoxId('<%= agentIpBox.ClientID %>');
            SW.SEUM.EditPlayer.SetNameBoxId('<%= nameBox.ClientID %>');
            SW.SEUM.EditPlayer.SetPasswordPassiveBoxId('<%= passwordPassiveBox.ClientID %>');
            SW.SEUM.EditPlayer.SetPortBoxId('<%= portBox.ClientID %>');
            SW.SEUM.EditPlayer.SetUseProxyCheckboxId('<%= useProxyCheckbox.ClientID %>');
            SW.SEUM.EditPlayer.SetProxyUrlBoxId('<%= proxyUrlBox.ClientID %>');
            SW.SEUM.EditPlayer.SetUseProxyAuthenticationCheckboxId('<%= useProxyAuthenticationCheckbox.ClientID %>');
            SW.SEUM.EditPlayer.SetProxyUserNameBoxId('<%= proxyUserNameBox.ClientID %>');
            SW.SEUM.EditPlayer.SetProxyPasswordBoxId('<%= proxyPasswordBox.ClientID %>');
            SW.SEUM.EditPlayer.SetGuidBoxId('<%= guidBox.ClientID %>');
            SW.SEUM.EditPlayer.SetOriginalGuidBoxId('<%= originalAgentGuidBox.ClientID %>');
            SW.SEUM.EditPlayer.SetUrlBoxId('<%= urlBox.ClientID %>');
            SW.SEUM.EditPlayer.SetOriginalUrlBoxId('<%= originalAgentUrlBox.ClientID %>');
            SW.SEUM.EditPlayer.SetOriginalModeBoxId('<%= originalAgentModeBox.ClientID %>');
            SW.SEUM.EditPlayer.SetPassiveRadioId('<%= passiveRadio.ClientID %>');
            SW.SEUM.EditPlayer.SetActiveRadioId('<%= activeRadio.ClientID %>');
            SW.SEUM.EditPlayer.SetActiveRadioId('<%= activeRadio.ClientID %>');
            SW.SEUM.EditPlayer.SetPollingEnginePassiveComboId('<%= pollingEnginePassive.ClientID %>');
            SW.SEUM.EditPlayer.SetPollingEngineActiveId('<%= pollingEngineActiveId.ClientID %>');
            SW.SEUM.EditPlayer.SetPollingEngineActiveNameId('<%= pollingEngineActiveName.ClientID %>');
            SW.SEUM.EditPlayer.SetAdvancedPassiveCheckboxId('<%= advancedPassiveCheckbox.ClientID %>');
            SW.SEUM.EditPlayer.SetAdvancedActiveCheckboxId('<%= advancedActiveCheckbox.ClientID %>');
            SW.SEUM.EditPlayer.SetAgentStatusBoxId('<%= agentStatusBox.ClientID %>');
            SW.SEUM.EditPlayer.SetAgentStatusMessageBoxId('<%= agentStatusMessageBox.ClientID %>');
            SW.SEUM.EditPlayer.SetOriginalAgentIpBoxId('<%= originalAgentIpBox.ClientID %>');
            SW.SEUM.EditPlayer.SetOriginalAgentDnsNameBoxId('<%= originalAgentDnsNameBox.ClientID %>');
            SW.SEUM.EditPlayer.SetOriginalAgentPollingEngineIdId('<%= originalAgentPollingEngineId.ClientID %>');
            SW.SEUM.EditPlayer.SetOriginalAgentPollingEngineNameId('<%= originalAgentPollingEngineName.ClientID %>');
            SW.SEUM.EditPlayer.SetAgentVersionFromTestId('<%= agentVersionFromTest.ClientID %>');
            <% if (IsEditPage)
               {%>
            SW.SEUM.EditPlayer.SetLogLevelComboId('<%=logLevelCombo.ClientID%>');
            SW.SEUM.EditPlayer.SetUpdateRemoteSettingsCheckboxId('<%=updateRemoteSettingsCheckbox.ClientID%>');
            <%
               }%>
            SW.SEUM.EditPlayer.SetSavePostBack(function() { <%= SavePostBack %> });
            SW.SEUM.EditPlayer.Init();

            <% if (RunTest)
               {%>
               SW.SEUM.EditPlayer.TestAgentSettings();
            <%
               }%>
            <% if (RunTestAndSave)
               {%>
               SW.SEUM.EditPlayer.TestAgentSettingsAndSave();
            <%
               }%>
        });
      //]]>
    </script>

    <!-- This is used to grab the results from a failed Web Service call -->
    <div id="test" class="x-hidden"></div>
</asp:Content>
