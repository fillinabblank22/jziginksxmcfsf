﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Orion_SEUM_Admin_Settings" MasterPageFile="~/Orion/SEUM/Admin/SEUMAdminPage.master" Title="Web Performance Monitor Settings" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <h1><%= Page.Title %></h1>
    
    <table id="adminContentTable" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table id="SEUM_SettingsTable" cellspacing="0">
                    <tr class="header">
                        <td colspan="3">
                            <h2><%= DatabaseSettingsTitle%></h2>
                        </td>
                    </tr>

                    <tr class="ZebraStripe">
                        <td class="PropertyHeader"><%= DetailedRetentionLabel %></td>
                        <td class="Property">
                            <web:ValidatedTextBox ID="detailedRetentionBox" 
                                                CustomStringValue="RetainDetail" Columns="5" Type="Integer" runat="server"
                                                UseRequiredValidator="true" UseRangeValidator="true" MinValue="1" MaxValue="180"
                                                ValidatorText="<img src='/Orion/images/Small-Down.gif' title='Value must be a valid integer from 1 to 180'/>" />
                            &nbsp;<%= DetailedRetentionUnit %></td>
                        <td class="Property"><%= DetailedRetentionDescription %></td>
                    </tr>

                    <tr>
                        <td class="PropertyHeader"><%= HourlyRetentionLabel %></td>
                        <td class="Property">
                            <web:ValidatedTextBox CustomStringValue="RetainHourly"
                                                ID="hourlyRetentionBox" Columns="5" Type="Integer" runat="server"
                                                UseRequiredValidator="true" UseRangeValidator="true" MinValue="1" MaxValue="365"
                                                ValidatorText="<img src='/Orion/images/Small-Down.gif' title='Value must be a valid integer from 1 to 365'/>" />
                            &nbsp;<%= HourlyRetentionUnit %>
                        </td>
                        <td class="Property"><%= HourlyRetentionDescription %></td>
                    </tr>

                    <tr class="ZebraStripe">
                        <td class="PropertyHeader"><%= DailyRetentionLabel %></td>
                        <td class="Property">
                            <web:ValidatedTextBox CustomStringValue="RetainDaily"
                                                ID="dailyRetentionBox" Columns="5" Type="Integer" runat="server"
                                                UseRequiredValidator="true" UseRangeValidator="true" MinValue="1" MaxValue="5000"
                                                ValidatorText="<img src='/Orion/images/Small-Down.gif' title='Value must be a valid integer from 1 to 5000'/>" />
                            &nbsp;<%= DailyRetentionUnit %></td>
                        <td class="Property"><%= DailyRetentionDescription %></td>
                    </tr>

                    <tr>
                        <td class="PropertyHeader"><%= ScreenshotsRetentionLabel %></td>
                        <td class="Property">
                            <web:ValidatedTextBox CustomStringValue="RetainScreenshots"
                                                ID="screenshotsRetentionBox" Columns="5" Type="Integer" runat="server"
                                                UseRequiredValidator="true" UseRangeValidator="true" MinValue="1" MaxValue="180"
                                                ValidatorText="<img src='/Orion/images/Small-Down.gif' title='Value must be a valid integer from 1 to 180'/>" />
                            &nbsp;<%= ScreenshotsRetentionUnit %></td>
                        <td class="Property"><%= ScreenshotsRetentionDescription %></td>
                    </tr>

                    <tr class="ZebraStripe">
                        <td class="PropertyHeader"><%= ScreenshotCapturesLabel %></td>
                        <td class="Property">
                            <asp:RadioButtonList ID="screenshotCapturesRadio" RepeatDirection="Vertical" RepeatLayout="UnorderedList" runat="server" />
                        </td>
                        <td class="Property"><%= ScreenshotCapturesDescription %></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div class="SEUM_StandardButtons">
        <orion:LocalizableButton ID="bt_Submit" LocalizedText="Submit" DisplayType="Primary" runat="server" CausesValidation="true" OnClick="Submit_Click" />
        <span style="width: 20px">&nbsp;</span>
        <orion:LocalizableButton ID="bt_Cancel" LocalizedText="Cancel" DisplayType="Secondary" runat="server" CausesValidation="false" OnClick="Cancel_Click" />
    </div>

</asp:Content>