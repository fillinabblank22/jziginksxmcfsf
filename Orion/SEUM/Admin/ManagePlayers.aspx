﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/SEUMManagementPage.master" AutoEventWireup="true" CodeFile="ManagePlayers.aspx.cs" Inherits="Orion_SEUM_Admin_ManagePlayers" %>

<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<%@ Register TagPrefix="SEUM" TagName="AgentsGrid" Src="~/Orion/SEUM/Controls/AgentsGrid.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentHeaderPlaceholder" runat="Server">
    <h1><%= Page.Title %></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <SEUM:AgentsGrid runat="server" ID="agentsGrid" />
</asp:Content>

