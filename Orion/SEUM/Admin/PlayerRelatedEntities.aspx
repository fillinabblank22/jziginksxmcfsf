﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/SEUM/Admin/SEUMAdminPage.master" CodeFile="PlayerRelatedEntities.aspx.cs" Inherits="Orion_SEUM_Admin_PlayerRelatedEntities" %>
<%@ Register TagPrefix="orion" TagName="EditLocationNodesControl" Src="~/Orion/SEUM/Controls/EditLocationNodesControl.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <orion:EditLocationNodesControl runat="server" ID="EditControl"/>
    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton LocalizedText="Submit" DisplayType="Primary" runat="server" CausesValidation="false" OnClick="Save_Click" />
        <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" CausesValidation="false" OnClick="Cancel_Click" />
    </div>
</asp:Content>
