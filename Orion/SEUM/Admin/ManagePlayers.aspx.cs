﻿using System;
using SolarWinds.Orion.Web;

public partial class Orion_SEUM_Admin_ManagePlayers : System.Web.UI.Page, IBypassAccessLimitation
{
    protected const string PageTitle = "Manage Transaction Locations";
    protected const string HelpLinkFragment = "OrionWPMPHManageTransactionLocations";


    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = PageTitle;

        ((Orion_SEUM_Admin_SEUMManagementPage)this.Master).HelpFragment = HelpLinkFragment;
    }
}