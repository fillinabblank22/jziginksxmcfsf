﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Admin_Default : System.Web.UI.Page, IBypassAccessLimitation
{
    protected const string gettingStartedText = "Getting Started with Web Performance Monitor";
    protected const string gettingStartedDescText = "From your desktop record a sequence you want to monitor, open the recording in Orion and assign location to play the recording.";
    protected const string learnMoreText = "Learn more";
    protected const string addMonitorText = "Add a Transaction Monitor";
    protected const string thwackText = "thwack Community";
    protected const string thwackDescText = "thwack features helpful resources, tips, and downloads from over 20,000 network engineers.";
    protected const string thwackForumText = "WPM thwack Forum";
    protected const string thwackUrl = "https://thwack.solarwinds.com/community/systems-management/web-performance-monitor-wpm-seum";
    protected const string licText = "License Summary";
    protected const string licView = "View your license usage.";
    protected const string licSumText = "WPM License Summary";
    protected const string managementText = "Transaction Management";
    protected const string managementContrlText = "Add, edit, delete and manage the properties of the transactions you want to monitor.";
    protected const string managePlaybackText = "Manage Transaction Monitors";
    protected const string manageRecText = "Manage Recordings";
    protected const string manageLocText = "Manage Player Locations";
    protected const string uimLicensePage = "/Orion/UIM/Admin/Details/LicenseDetails.aspx";

    protected const string settingsTitle = "WPM Settings";
    protected const string settingsText = "Set WPM settings such as data retention intervals.";
    protected const string settingsLabel = "Settings";

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_SEUM_Admin_SEUMAdminPage)Master).HelpFragment = "OrionSEUMPHSettings";

        this.licensingBucket.Visible = !SolarWinds.Orion.Core.Common.RegistrySettings.IsUnifiedITManager();
    }

    protected string GettingStartedHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGIntroduction");
        }
    }

    protected string LicenseDetailsUrl
    {
        get
        {
            if (SolarWinds.Orion.Core.Common.RegistrySettings.IsUnifiedITManager())
            {
                return uimLicensePage;
            }

            if (Profile.AllowAdmin)
                return CommonLinks.LicenseDetails;
            else
                return CommonLinks.ModuleOnlyLicenseDetails;
        }
    }

}