﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;
using System;
using System.Linq;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Admin_EditMonitor : System.Web.UI.Page, IBypassAccessLimitation
{
    protected const string PageTitleFormat = "Edit Transaction Monitor - {0}";

    public const string LicenseExceptionTitle = "License limitation";
    public const string LicenseExceptionText = "You have no available monitors in your license";
    public const string JobEngineExceptionTitle = "Job Engine Error";
    public const string JobEngineExceptionText = @"There is a problem creating polling job for transaction. Please check that ""SolarWinds Job Engine"" service is running and try to save changes again.";
    
    private Transaction transaction;
    private string returnUrl = null;

    protected bool ShowValidationError
    {
        get;
        set;
    }

    protected bool ShowJobEngineError
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ModelsDAL dal = new ModelsDAL();
        int transactionId;
        Int32.TryParse(Request["id"], out transactionId);

        if (!string.IsNullOrEmpty(Request[CommonParameterNames.ReturnUrl]))
            returnUrl = UrlHelper.FromSafeUrlParameter(Request[CommonParameterNames.ReturnUrl]);

        if (transactionId == 0)
            throw new ArgumentException("Specified ID is invalid.");

        transaction = dal.GetTransactionModel(transactionId, true, true);

        if (transaction == null)
            throw new ArgumentException("Specified transaction was not found.");

        transactionEditor.Transaction = transaction;

        Page.Title = string.Format(PageTitleFormat,
            NameHelper.GenerateTransactionFullName(transaction.Recording.Name, transaction.Agent.Name, null, null));

        ShowValidationError = false;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        BindRelatedEntities();
    }

    protected void Save_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid || !SEUMProfile.AllowAdmin) 
            return;

        try
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                proxy.TransactionUpdate(transaction);
            }
        }
        catch(SEUMBusinessLayerException ex)
        {
            if (ex.ErrorType == SEUMBusinessLayerErrorType.LicenseError)
            {
                ShowValidationError = true;
                return;
            }
            if (ex.ErrorType == SEUMBusinessLayerErrorType.JobEngineError)
            {
                ShowJobEngineError = true;
                return;
            }
            throw;
        }

        dependenciesControl.Transaction = transaction;
        RelatedEntitiesHelper.SaveTransactionRelatedEntities(transaction, dependenciesControl.TransactionDependencies);
        foreach (var step in transaction.Steps)
        {
            RelatedEntitiesHelper.SaveTransactionStepRelatedEntities(step, dependenciesControl.StepsDependencies != null && dependenciesControl.StepsDependencies.ContainsKey(step) ? 
                dependenciesControl.StepsDependencies[step] : Enumerable.Empty<string>());
        }

        if (string.IsNullOrEmpty(returnUrl))
            GoBack();
        else
            Response.Redirect(returnUrl);
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(returnUrl))
            GoBack();
        else
            Response.Redirect(returnUrl);
    }

    private void GoBack()
    {
        Response.Redirect("/Orion/SEUM/Admin/ManageMonitors.aspx");
    }

    private void BindRelatedEntities()
    {
        dependenciesControl.Transaction = transaction;
        if (!IsPostBack)
        {
            var monitorsDal = new MonitorsDAL();
            dependenciesControl.TransactionDependencies = monitorsDal.GetRelatedEntities(transaction.TransactionId);
            var stepDependecies = monitorsDal.GetStepRelatedEntities(transaction.TransactionId);
            dependenciesControl.StepsDependencies = transaction.Steps.ToDictionary(s => s,
                s => stepDependecies.ContainsKey(s.TransactionStepId) ? stepDependecies[s.TransactionStepId] : Enumerable.Empty<string>());
        }
        dependenciesControl.DataBind();
    }
}