﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_SEUM_Admin_LicenseDetails : System.Web.UI.Page, IBypassAccessLimitation
{
    protected const string PageTitle = "License Details";

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_SEUM_Admin_SEUMAdminPage) this.Master).HelpFragment = "OrionPHAdminDetailsLicense";

        Page.Title = PageTitle;
    }
}