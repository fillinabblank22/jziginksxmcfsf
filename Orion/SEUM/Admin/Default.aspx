﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_SEUM_Admin_Default"  MasterPageFile="~/Orion/SEUM/Admin/SEUMAdminPage.master" Title="Web Performance Monitor Settings" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Import Namespace="SolarWinds.SEUM.BusinessLayer.Servicing" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <style type="text/css">
        #adminNav { display: none; }
        #adminContent p { border: none; width: auto; padding: 0; font-size: 12px;}
        #adminContent a { color: #336699; }
        #adminContent a:hover {color:orange;}
        .column1of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
        .column2of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
        .BucketLinkContainer { width: 100%; }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <h1 style="padding-bottom:2px;"><%= Page.Title %></h1>

    <div style="font-size: 11px;">
		<span><%= SolarWinds.SEUM.Web.Helpers.ModuleInfoHelper.GetModuleVersionString()%></span>
		<% /* We might want removing this before release. So far its for QAs */ %>
		<span> Build #<%= typeof(ISEUMBusinessLayer).Assembly.GetName().Version.ToString() %></span>
    </div>

	<table id="adminContent" style="padding-top: 0; padding-left: 0;" width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td class="column1of2">
				<!-- Begin Getting started Bucket -->

				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/SEUM/images/getting_started_36x36.gif" />
							</td>
							<td>
								<div class="BucketHeader"><%= gettingStartedText %></div>
								<p><%= gettingStartedDescText %>
                                 <a href="<%= GettingStartedHelpUrl %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= learnMoreText %></a></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="66%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/SEUM/Admin/Add/Default.aspx"><%= addMonitorText %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p>&nbsp;</p>
							</td>
						</tr>
					</table>
				</div>

				<!-- End Getting started Bucket -->

                  <!-- Begin Management Bucket -->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/SEUM/Images/Icon.Recording_36x36.gif" alt="" />
							</td>
							<td>
								<div class="BucketHeader"><%= managementText %></div>
								<p><%= managementContrlText %></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/SEUM/Admin/ManageMonitors.aspx"><%= managePlaybackText %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/SEUM/Admin/ManageRecordings.aspx"><%= manageRecText %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/SEUM/Admin/ManagePlayers.aspx"><%= manageLocText %></a></p>
							</td>
						</tr>
					</table>
				</div>
				<!-- End Management Bucket -->

                <!-- Begin Settings Bucket -->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/SEUM/Images/Icon.Settings_36x36.gif" alt="" />
							</td>
							<td>
								<div class="BucketHeader"><%= settingsTitle %></div>
								<p><%= settingsText %></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/SEUM/Admin/Settings.aspx"><%= settingsLabel%></a></p>
							</td>
						</tr>
					</table>
				</div>
				<!-- End Settings Bucket -->


			</td>

			<!-- Begin 2nd Column -->
			<td class="column2of2">

				<!-- Begin License Bucket -->

				<div class="ContentBucket" runat="server" id="licensingBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/SEUM/images/icon_license_36x36.gif" />
							</td>
							<td>
								<div class="BucketHeader"><%= licText %></div>
								<p><%= licView %></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="66%">
								<p><span class="LinkArrow">&#0187;</span> <a href="<%= LicenseDetailsUrl %>"><%= licSumText %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p>&nbsp;</p>
							</td>
						</tr>
					</table>
				</div>

				<!-- End License Bucket -->

                <!-- Begin Thwack Bucket -->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img alt="" class="BucketIcon" src="/Orion/SEUM/Images/Icon.Thwack_36x36.gif" />
							</td>
							<td>
								<div class="BucketHeader"><%= thwackText %></div>
								<p><%= thwackDescText %></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="<%= thwackUrl %>"><%= thwackForumText %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								&nbsp;
							</td>
							<td class="LinkColumn" width="33%">
								&nbsp;
							</td>
						</tr>
					</table>
				</div>
				<!-- End Thwack Bucket -->


			</td>
			<!-- End 2nd Column -->
		</tr>
	</table>
	<br/>

</asp:Content>
