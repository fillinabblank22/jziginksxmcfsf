﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Exceptions;

public partial class Orion_SEUM_Admin_PlayerRelatedEntities : System.Web.UI.Page
{
    #region Constants

    protected const string TitlePattern = "Location status troubleshooting{0}";
    protected const string DefaultReturnUrl = "/Orion/SEUM/Admin/ManagePlayers.aspx";

    #endregion

    private IDictionary<int, IEnumerable<string>> agentNodes;

    private string returnUrl = null;

    private Agent agent;

    private IEnumerable<Agent> agents;

    private bool single;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!SEUMProfile.AllowAdmin)
        {
            throw new SEUMUnauthorizedAccessException();
        }
        ResolveReturnUrl();

        IEnumerable<int> agentIds = TryParseAgentIds();
        if (agentIds == null || !agentIds.Any())
        {
            throw new ArgumentException("Parameter agentIds is required");
        }
        EditControl.AgentIds = agentIds;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void ResolveReturnUrl()
    {
        if (!string.IsNullOrEmpty(Request[CommonParameterNames.ReturnUrl]))
        {
            returnUrl = UrlHelper.FromSafeUrlParameter(Request[CommonParameterNames.ReturnUrl]);
        }
        else
        {
            returnUrl = DefaultReturnUrl;
        }
    }

    private int[] TryParseAgentIds()
    {
        if (Request["agentIds"] == null) return null;
        var strIds = Request["agentIds"].Split(',');
        var result = new int[strIds.Length];
        if (strIds.Where((s, i) => !Int32.TryParse(s, out result[i])).Any())
        {
            return null;
        }
        return result;
    }

    protected void Save_Click(object sender, EventArgs e)
    {
        EditControl.Save();
        LeavePage();
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        LeavePage();
    }

    private void LeavePage()
    {
        Response.Redirect(returnUrl);
    }
}
