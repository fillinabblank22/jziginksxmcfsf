﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Admin_Deployment_Default : DeploymentWizardPageBase, IBypassAccessLimitation
{
    #region Constants

    protected const string Heading = "Where do you want to install the player?";
    protected const string NewLocationText = "New Location:";
    protected const string EnterIPOrHostnameText = "Enter IP Address or Hostname:";
    protected const string SelectFromExistingNodesText = "or <b>select Location from existing Windows nodes:</b>";
    protected const string LocationsSelectedText = "Locations selected";

    protected const string IpAddressOrHostnameIsRequiredError = "A valid IP address or Hostname is required.";
    protected const string InvalidHostnameError = "The entered value is not a valid IP address or Hostname.";
    protected const string HostnameAlreadyInListError = "The entered Hostname or IP Address is already in list.";

    protected const string NoLocationsSelectedError = "You must select at least one location for automatic player deployment.";

    protected const string AddToListButtonLabel = "Add to list";

    protected const string LearnMoreText = "Learn more";
    protected const string DeploymentInfoText = "Here you can install a player on your network by entering the IP address or hostname of the node (e.g. 10.10.10.10)";

    #endregion // Constants

    private readonly JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();
    private List<DeploymentHost> _hosts;

    protected bool ShowError { get; set; }
    protected string ErrorMessage { get; set; }
    protected bool ShowGrid { get; set; }

    protected string LearnMoreUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionSEUMAGDeployPlayer"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_SEUM_Admin_Deployment_DeploymentWizard)this.Master).HelpFragment = "OrionSEUMAGDeployPlayer";

        if (Request["keepData"] == null)
            Workflow.Reset();

        var dal = new NodesDAL();
        ShowGrid = dal.GetNodesCount("") != 0;
        
        ShowError = false;

        buttonAddToList.Text = AddToListButtonLabel;

        hostnameRequiredValidator.ErrorMessage = IpAddressOrHostnameIsRequiredError;
        hostnameCustomValidator.ErrorMessage = InvalidHostnameError;

        if(!IsPostBack || string.IsNullOrEmpty(selectedNodes.Value))
        {
            _hosts = new List<DeploymentHost>(Workflow.Hosts);
        }
        else
        {
            _hosts = _jsSerializer.Deserialize<List<DeploymentHost>>(selectedNodes.Value);
        }
    }

    protected override bool Next()
    {
        // automatically add location if the next is hit and hostname textbox contains valid hostname
        if (!string.IsNullOrEmpty(addHostTextbox.Text))
        {
            Page.Validate("AddHost");

            if (!Page.IsValid)
                return false;

            _hosts.Add(new DeploymentHost()
            {
                NodeId = 0,
                Name = addHostTextbox.Text,
                Hostname = addHostTextbox.Text,
                StatusName = "",
                CredentialId = -1
            });

            addHostTextbox.Text = string.Empty;
        }

        if (_hosts == null || _hosts.Count == 0)
        {
            ShowError = true;
            ErrorMessage = NoLocationsSelectedError;
            return false;
        }

        // merge selected hosts with values in workflow
        Workflow.Merge(_hosts);

        return true;
    }

    protected void addHostTextbox_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        var hostnameType = Uri.CheckHostName(e.Value);
        if (hostnameType == UriHostNameType.Unknown || hostnameType == UriHostNameType.Basic)
        {
            e.IsValid = false;
        }
        else if(_hosts.FirstOrDefault(h => string.Compare(h.Hostname, e.Value, StringComparison.OrdinalIgnoreCase) == 0) == null)
        {
            e.IsValid = true;
        }
        else
        {
            hostnameCustomValidator.ErrorMessage = HostnameAlreadyInListError;
            e.IsValid = false;
        }
    }

    protected void buttonAddToList_OnClick(object sender, EventArgs e)
    {
        if (hostnameRequiredValidator.IsValid && hostnameCustomValidator.IsValid)
        {
            _hosts.Add(new DeploymentHost()
                           {
                               NodeId = 0,
                               Name = addHostTextbox.Text,
                               Hostname = addHostTextbox.Text,
                               StatusName = "",
                               CredentialId = -1
                           });

            addHostTextbox.Text = string.Empty;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if(_hosts != null)
        {
            selectedNodes.Value = _jsSerializer.Serialize(_hosts);
        }
    }
}