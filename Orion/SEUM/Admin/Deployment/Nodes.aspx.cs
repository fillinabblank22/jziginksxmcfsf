﻿using System;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Admin_Deployment_Nodes : DeploymentWizardPageBase, IBypassAccessLimitation
{
    protected void Page_Init(object sender, EventArgs e)
    {
        EditControl.AgentIds = Workflow.CreatedAgents.Select(a => a.AgentId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override bool Next()
    {
        EditControl.Save();
        return true;
    }
}