﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Deployment/DeploymentWizard.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_SEUM_Admin_Deployment_Default" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4"  />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" Module="SEUM" File="SearchField.js" />
    <orion:Include runat="server" Module="SEUM" File="CheckboxSelectionModel.js" />
    <orion:Include runat="server" Module="SEUM" File="NodesBrowser.js" />

    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }
    </script>

    <h2><%= Heading %></h2>

    <p><%= DeploymentInfoText%> <a href="<%= LearnMoreUrl %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= LearnMoreText %></a></p>

    <% if(ShowError) { %><div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= ErrorMessage %></div><p></p><% } %>
    
    <b><%= NewLocationText %></b>
    <table class="sw-seum-nodes-table">
        <tr>
            <td class="sw-seum-first-column">
                <div class="sw-seum-enter-host">
                    <%= EnterIPOrHostnameText%> <asp:TextBox ID="addHostTextbox" runat="server" CssClass="SEUM_EditBox" /> <orion:LocalizableButton ValidationGroup="AddHost" ID="buttonAddToList" LocalizedText="CustomText" DisplayType="Small" OnClick="buttonAddToList_OnClick" runat="server" />
                    <asp:RequiredFieldValidator ID="hostnameRequiredValidator" ControlToValidate="addHostTextbox" ValidationGroup="AddHost" Display="Dynamic" runat="server" />
                    <asp:CustomValidator ID="hostnameCustomValidator" ControlToValidate="addHostTextbox" OnServerValidate="addHostTextbox_ServerValidate" ValidationGroup="AddHost" Display="Dynamic" runat="server" />
                </div>
                
                <div style="display: <%= ShowGrid ? "block" : "none" %>">

                <p><%= SelectFromExistingNodesText%></p>
                
                <div id="SEUM_GroupingPanel" class="SEUM_GroupingPanel">
	                <div class="GroupSection">
		                <label for="groupBySelect">Group by:</label>
		                <select id="groupBySelect">
			                <option value="test"/>
		                </select>
	                </div>
                    <div id="TestID"></div>
                </div>

                <div id="NodesGrid" class="SEUM_ExtJsGrid"></div>

                </div>
            </td>
            <td class="SEUM_LeftBorder SEUM_DarkBorder sw-seum-second-column">
                <div class="sw-seum-locations-header">
                    <a href="javascript:void(0);" id="sw-seum-expander-link"><img src="/Orion/Images/Button.Collapse.gif" alt="+" /> <b><span id="sw-seum-locations-count">0</span> <%= LocationsSelectedText %></b></a>
                </div>
                <div id="sw-seum-hosts-list">
                    <ul></ul>
                </div>
            </td>
        </tr>
    </table>
    
    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" ValidationGroup="WholePage" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

    <asp:HiddenField ID="selectedNodes" runat="server" />
    <asp:HiddenField ID="SEUM_CurrentPage" ClientIDMode="Static" runat="server" />

    <script type="text/javascript">
        // <![CDATA[
        SW.SEUM.NodesBrowser.SetPageSize(20);
        SW.SEUM.NodesBrowser.SetUserName("<%=HttpUtility.JavaScriptStringEncode(Context.User.Identity.Name.Trim())%>");
        SW.SEUM.NodesBrowser.SelectedNodesFieldId("<%= selectedNodes.ClientID %>");

        $(function () {
            $('#sw-seum-expander-link').toggle(
                function () {
                    $('img:first', this).attr('src', '/Orion/Images/Button.Expand.gif');
                    $('#sw-seum-hosts-list').slideUp('fast');
                },
                function () {
                    $('img:first', this).attr('src', '/Orion/Images/Button.Collapse.gif');
                    $('#sw-seum-hosts-list').slideDown('fast');
                }
            );
        });
        // ]]>
    </script>

</asp:Content>