﻿using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Admin_Deployment_Credentials : DeploymentWizardPageBase, IBypassAccessLimitation
{
    #region Constants

    protected const string Heading = "Enter Credentials for each Location";
    protected const string NewCredentialText = "<New Credential>";

    protected const string DeployPlayerSingularText = "Deploy Player";
    protected const string DeployPlayerPluralText = "Deploy Players";

    protected const string NoLocationsSelectedError = "You must select at least one location for automatic player deployment.";
    protected const string UnassignedCredentialsError = "All locations must have assigned credentials.";
    protected const string UnableToSaveLocationFormat = "Unable to save location '{0}' - {1}";
    protected const string InvalidHostnameOrIPFormat = "Settings for location '{0}' does not contain a valid IP address and/or Hostname.";
    protected const string InvalidCredentialsFormat = "Location '{0}' has invalid credentials.";
    protected const string UnableToScheduleDeploymentFormat = "Unable to schedule deployment for location '{0}'. {1}";
    protected const string LocationRestartWarning = "To complete the installation, the computer at the remote location will install any needed prerequisites. If required, the remote computer may be restarted after the player is deployed.";


    protected const string ChooseExistingOrCreateNewOneText = "Choose existing credential from the library or create new one:";
    protected const string ChooseCredentialText = "Choose Credential:";
    protected const string UserNameText = "User Name:";
    protected const string PasswordText = "Password:";

    protected const string SuccessMessageFormat = "Player deployment was successfuly started at {0} location(s).";
    protected const string ErrorMessageFormat = "Player deployment failed at {0} location(s). {1}";

    protected const string DefaultAgentProtocol = "https://";

    #endregion // Constants
    
    private readonly CredentialManager _credentialManager = new CredentialManager();
    private readonly JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();
    private readonly IList<string> _errorMessages = new List<string>();

    private int _successfulCount = 0;
    /// <summary>
    /// Overall error message if this message is set, error messages for single deployments are not shown
    /// </summary>
    private string _overallErrorMessage = null;

    protected string CredentialsList
    {
        get
        {
            return
                _jsSerializer.Serialize(
                    (new[] {new KeyValuePair<int, string>(-1, NewCredentialText)}).Concat(
                        _credentialManager.GetCredentialNames<UsernamePasswordCredential>(
                            CoreConstants.CoreCredentialOwner)));
        }
    }

    protected bool ShowSuccess
    {
        get { return _successfulCount > 0; }
    }
    protected string SuccessMessage
    {
        get { return String.Format(SuccessMessageFormat, _successfulCount); }
    }
    protected bool ShowError
    {
        get { return _overallErrorMessage != null || _errorMessages.Any(); }
    }
    protected string ErrorMessage
    {
        get
        {
            // if there is an overall error message set don't show errors for single deployment hosts
            if (_overallErrorMessage != null)
                return _overallErrorMessage;

            var errorList = "<ul>" +
                            _errorMessages.Select(
                                x => String.Format("<li>{0}</li>", x.Replace(Environment.NewLine, "<br />"))).Aggregate(
                                    (a, b) => a + Environment.NewLine + b) + "</ul>";

            return String.Format(ErrorMessageFormat, _errorMessages.Count, errorList);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Workflow.SessionInstanceHasExpired)
            GotoFirstStep();

        ((Orion_SEUM_Admin_Deployment_DeploymentWizard)this.Master).HelpFragment = "OrionSEUMAGDeployPlayer";

        imgbNext.Text = Workflow.Hosts.Count() > 1 ? DeployPlayerPluralText : DeployPlayerSingularText;
    }

    protected override bool Next()
    {
        if (!Workflow.Hosts.Any())
        {
            _overallErrorMessage = NoLocationsSelectedError;
            return false;
        }

        if(Workflow.Hosts.Any(h => h.CredentialId == -1))
        {
            _overallErrorMessage = UnassignedCredentialsError;
            return false;
        }

        var successfullyDeployed = new List<string>();

        using (var proxy = BusinessLayerFactory.Create())
        {
            foreach (var host in Workflow.Hosts)
            {
                if (!ValidateAndCompleteHost(host))
                {
                    _log.ErrorFormat("Invalid host with name '{0}'", host.Name);
                    continue;
                }

                var agent = new Agent();

                agent.ConnectionStatus = (int)AgentConnectionStatus.DeploymentPending;
                agent.ConnectionStatusMessage = LocalizableStrings.AgentDeployment.Pending;
                agent.InCloud = false;

                agent.PollingEngineId = host.PollerId;
                agent.Name = host.PlayerName;
                agent.Port = host.PlayerPort;
                agent.Password = host.PlayerPassword ?? string.Empty;
                agent.UseProxy = false;
                agent.UseProxyAuthentication = false;
                agent.ProxyUserName = string.Empty;
                agent.ProxyPassword = string.Empty;

                agent.Url = DefaultAgentProtocol +
                            (string.IsNullOrEmpty(host.Hostname) ? host.IpAddress : host.Hostname);

                agent.Hostname = (new[] { host.Hostname, host.IpAddress }).First(i => !String.IsNullOrEmpty(i));
                agent.IP = host.IpAddress ?? string.Empty;

                int agentId;
                try
                {
                    agentId = proxy.AgentAdd(agent);
                    agent.AgentId = agentId;
                    Workflow.CreatedAgents.Add(agent);
                }
                catch (Exception ex)
                {
                    _log.Error("Unable to add agent.", ex);
                    _errorMessages.Add(String.Format(UnableToSaveLocationFormat, host.Name, ex.Message));
                    continue;
                }

                try
                {
                    var deploymentDescription = new AgentDeploymentDescription()
                    {
                        PlayerPort = host.PlayerPort,
                        PlayerPassword = host.PlayerPassword ?? string.Empty,
                        MachineUsername = host.Credential.Username,
                        MachinePassword = host.Credential.Password,
                        Hostname = host.Hostname,
                        IPAddress = host.IpAddress
                    };

                    proxy.ScheduleDeployment(agentId, InstallerDeploymentKind.Install, deploymentDescription);
                }
                catch (Exception ex)
                {
                    _log.Error("Unable to schedule player deployment for agent ID " + agentId + ".", ex);
                    _errorMessages.Add(String.Format(UnableToScheduleDeploymentFormat, host.Name, ex.Message));

                    // if we were unable to schedule deployment, we need to remove saved agent
                    try
                    {
                        proxy.AgentDelete(new[] { agentId });
                        Workflow.CreatedAgents.Remove(agent);
                    }
                    catch (Exception innerEx)
                    {
                        // just log
                        _log.Error("Unable to delete invalid agent with ID " + agentId, innerEx);
                    }
                    continue;
                }

                // add host to the list of successfully deployed
                successfullyDeployed.Add(host.Hostname);
            }
        }

        // remove successfully deployed hosts from the list
        foreach (var deployedHost in successfullyDeployed)
        {
            Workflow.Remove(deployedHost);
        }

        _successfulCount = successfullyDeployed.Count;

        // if there are any hosts remaining do not redirect to return url
        if (Workflow.Hosts.Any())
            return false;

        return true;
    }

    private bool ValidateAndCompleteHost(DeploymentHost host)
    {
        if (string.IsNullOrEmpty(host.Hostname) && string.IsNullOrEmpty(host.IpAddress))
        {
            _errorMessages.Add(String.Format(InvalidHostnameOrIPFormat, host.Name));
            return false;
        }
        if (host.CredentialId == -1)
        {
            _errorMessages.Add(String.Format(InvalidCredentialsFormat, host.Name));
            return false;
        }
        if (host.CredentialId == 0 && host.Credential == null)
        {
            _errorMessages.Add(String.Format(InvalidCredentialsFormat, host.Name));
            return false;
        }

        if (host.CredentialId == 0 && string.IsNullOrEmpty(host.Credential.Username) || string.IsNullOrEmpty(host.Credential.Password))
        {
            _errorMessages.Add(String.Format(InvalidCredentialsFormat, host.Name));
            return false;
        }

        if (host.CredentialId != 0 && host.Credential == null)
        {
            try
            {
                host.Credential = _credentialManager.GetCredential<UsernamePasswordCredential>(host.CredentialId);
            }
            catch (CredentialNotFoundException)
            {
                _errorMessages.Add(String.Format(InvalidCredentialsFormat, host.Name));
                return false;
            }
        }

        if (host.CredentialId != 0 && host.Credential.IsBroken)
        {
            _errorMessages.Add(String.Format(InvalidCredentialsFormat, host.Name));
            return false;
        }

        return true;
    }
}