﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Deployment/DeploymentWizard.master" AutoEventWireup="true" CodeFile="Nodes.aspx.cs" Inherits="Orion_SEUM_Admin_Deployment_Nodes" %>
<%@ Register TagPrefix="orion" TagName="EditLocationNodesControl" Src="~/Orion/SEUM/Controls/EditLocationNodesControl.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4"  />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
    <orion:EditLocationNodesControl runat="server" ID="EditControl"/>
    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" runat="server" />
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="Submit" DisplayType="Primary" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>
</asp:Content>