﻿using System;
using System.Linq;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Common;

public partial class Orion_SEUM_Admin_Deployment_Settings : DeploymentWizardPageBase, IBypassAccessLimitation
{
    #region Constants

    protected const string Heading = "Player Settings";

    protected const string UseDefaultText = "Use default settings";
    protected const string UseDefaultHelpfulText = "Default works for the most uses";
    protected const string UseAdvancedText = "Use advanced settings";

    protected const string InvalidHostnameOrIPFormat = "Settings for location '{0}' does not contain a valid IP address and/or Hostname.";

    #endregion // Constants

    private readonly AgentsDAL _agentsDal = new AgentsDAL();
    private readonly JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();

    protected bool ShowError
    {
        get { return !string.IsNullOrEmpty(ErrorMessage); }
    }

    protected string ErrorMessage { get; private set; }

    protected string PollerList
    {
        get
        {
            var dal = new AgentsDAL();
            return _jsSerializer.Serialize(dal.GetAvailablePollingEngines());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Workflow.SessionInstanceHasExpired)
            GotoFirstStep();

        ((Orion_SEUM_Admin_Deployment_DeploymentWizard)this.Master).HelpFragment = "OrionSEUMAGDeployPlayer";
    }

    protected override bool Next()
    {
        foreach (var host in Workflow.Hosts)
        {
            if (!ValidateAndCompleteHost(host))
            {
                _log.ErrorFormat("Invalid host with name '{0}'", host.Name);
            }
        }

        return !ShowError;
    }

    private bool ValidateAndCompleteHost(DeploymentHost host)
    {
        if (string.IsNullOrEmpty(host.Hostname) && string.IsNullOrEmpty(host.IpAddress))
        {
            ErrorMessage = String.Format(InvalidHostnameOrIPFormat, host.Name);
            return false;
        }

        if (host.PollerId == 0)
            host.PollerId = _agentsDal.GetAvailablePollingEngines().First().Key;

        if (host.PlayerPort == 0)
            host.PlayerPort = Constants.PassiveAgentPort;

        return true;
    }
}
