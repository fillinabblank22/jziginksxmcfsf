﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Deployment/DeploymentWizard.master" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Orion_SEUM_Admin_Deployment_Settings" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" Runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4"  />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" Module="SEUM" File="DeploymentSettingsGrid.js" />

    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }
    </script>

    <h2><%= Heading %></h2>

    <% if(ShowError) { %><div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= ErrorMessage %></div><p></p><% } %>

    <table class="sw-seum-radiolist">
        <tr class="sw-seum-row">
            <td><asp:RadioButton ID="useDefault" Checked="true" GroupName="LocationType" runat="server" /></td>
            <td>
                <asp:Label ID="Label1" AssociatedControlID="useDefault" runat="server">
                    <span class="sw-seum-description"><%= UseDefaultText %></span><br />
                    <span class="sw-text-helpful"><%= UseDefaultHelpfulText %></span>
                </asp:Label>
            </td>
        </tr>
        <tr class="sw-seum-row">
            <td><asp:RadioButton ID="useAdvanced" GroupName="LocationType" runat="server" /></td>
            <td>
                <asp:Label ID="Label2" AssociatedControlID="useAdvanced" runat="server">
                    <span class="sw-seum-description"><%= UseAdvancedText%></span>
                </asp:Label>
            </td>
        </tr>
    </table>

    <div id="DeploymentSettingsGrid" class="SEUM_ExtJsGrid"></div>

    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" runat="server" />
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

    <script type="text/javascript">
        // <![CDATA[
        SW.SEUM.DeploymentSettingsGrid.SetPollerList(<%= PollerList %>);

        $(function () {
            $(".sw-seum-radiolist input:radio").change(function () {
                if ($('#<%= useDefault.ClientID %>').is(':checked'))
                    SW.SEUM.DeploymentSettingsGrid.ToggleAdvancedMode(false);
                if ($('#<%= useAdvanced.ClientID %>').is(':checked'))
                    SW.SEUM.DeploymentSettingsGrid.ToggleAdvancedMode(true);
            });

            SW.SEUM.DeploymentSettingsGrid.ToggleAdvancedMode($('#<%= useAdvanced.ClientID %>').is(':checked'));
        });
        // ]]>
    </script>
</asp:Content>

