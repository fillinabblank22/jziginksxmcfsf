﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Deployment/DeploymentWizard.master" AutoEventWireup="true" CodeFile="Credentials.aspx.cs" Inherits="Orion_SEUM_Admin_Deployment_Credentials" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" Runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4"  />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" Module="SEUM" File="CheckboxSelectionModel.js" />
    <orion:Include runat="server" Module="SEUM" File="DeploymentCredentialsGrid.js" />

    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }
    </script>

    <h2><%= Heading %></h2>
    
    <div class="sw-seum-message-block">

    <div class="sw-suggestion sw-suggestion-warn"><span class="sw-suggestion-icon"></span><%= LocationRestartWarning %></div><br />

    <% if(ShowSuccess) { %><div class="sw-suggestion sw-suggestion-pass"><span class="sw-suggestion-icon"></span><%= SuccessMessage %></div><br /><% } %>

    <% if(ShowError) { %><div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= ErrorMessage %></div><br /><% } %>

    </div>

    <div id="CredentialsGrid" class="SEUM_ExtJsGrid"></div>

    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" runat="server" />
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="CustomText" DisplayType="Primary" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

    <div id="AssignCredentialWindow" style="display: none;">
        <div class="x-panel-body sw-seum-credential-window">
            <p><%= ChooseExistingOrCreateNewOneText %></p>
            <table>
                <tr class="sw-seum-first-row">
                    <td><%= ChooseCredentialText %></td>
                    <td><select id="CredentialNamesCombo"><option value="test">test</option></select></td>
                </tr>
                <tr class="sw-seum-hideable">
                    <td><%= UserNameText %></td>
                    <td><input type="text" id="CredentialUsername" value="" /></td>
                </tr>
                <tr class="sw-seum-hideable">
                    <td><%= PasswordText %></td>
                    <td><input type="password" id="CredentialPassword" value="" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <orion:LocalizableButton ID="imgbTest" LocalizedText="Test" DisplayType="Small" runat="server" OnClientClick="SW.SEUM.DeploymentCredentialsGrid.TestCredentialsInPopup();return false;" />
                        <ul id="testResults" style="display: none;"></ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        // <![CDATA[
        SW.SEUM.DeploymentCredentialsGrid.SetCredentialsList(<%= CredentialsList %>);
        // ]]>
    </script>
</asp:Content>

