﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SEUM_Admin_Deployment_DeploymentWizard : System.Web.UI.MasterPage
{
    public const string PageTitle = "Remote Player Deployment";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = PageTitle;
    }

    public string HelpFragment
    {
        get
        {
            return ((Orion_SEUM_Admin_SEUMAdminPage)this.Master).HelpFragment;
        }
        set
        {
            ((Orion_SEUM_Admin_SEUMAdminPage)this.Master).HelpFragment = value;
        }
    }
}
