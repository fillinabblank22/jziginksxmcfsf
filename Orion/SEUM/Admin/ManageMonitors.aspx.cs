﻿using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Web;
using System;
using SolarWinds.SEUM.BusinessLayer.Servicing;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_SEUM_Admin_ManageMonitors : System.Web.UI.Page, IBypassAccessLimitation
{
    protected const string PageTitle = "Manage Transaction Monitors";

    protected const string ViewDetailsText = "View Details";
    protected const string LicenseUsageText = "License Usage";
    protected const string LicenseUsageNumbersFormatText = "{0}/{1}";
    protected const string LicenseUsageNumbersFormatExceededText = @"<span class=""Error"">{0}/{1}</span>";
    protected const string UnlimitedText = "Unlimited";
    protected const string HelpLinkFragment = "OrionSEUMAGTransactions";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = PageTitle;

        monitorsGrid.GroupBy = Request["groupBy"];
        monitorsGrid.GroupByItem = Request["groupByItem"];

        ((Orion_SEUM_Admin_SEUMManagementPage)this.Master).HelpFragment = HelpLinkFragment;
    }
    
    protected string LicenseUsageNumbers
    {
        get
        {
            int used;
            int total;
            using(var proxy = BusinessLayerFactory.Create())
            {
                total = proxy.GetLicenseMaxElementCount();
                used = proxy.GetAllTransactionCount();
            }
            
            if(total > 99000)
            {
                return UnlimitedText;
            }
            return (used > total)
                       ? string.Format(LicenseUsageNumbersFormatExceededText, used, total)
                       : string.Format(LicenseUsageNumbersFormatText, used, total);
        }
    }
}