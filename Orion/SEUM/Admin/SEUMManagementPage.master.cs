﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SEUM_Admin_SEUMManagementPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string HelpFragment
    {
        get
        {
            return ((Orion_SEUM_Admin_SEUMAdminPage)this.Master).HelpFragment;
        }
        set
        {
            ((Orion_SEUM_Admin_SEUMAdminPage)this.Master).HelpFragment = value;
        }
    }
}
