﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/SEUMAdminPage.master" AutoEventWireup="true" CodeFile="AddPlayer.aspx.cs" Inherits="Orion_SEUM_Admin_AddPlayer" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <orion:Include runat="server" File="OrionCore.js" />

    <h1><%= Page.Title %></h1>
    <div class="GroupBox">
        <h2><%= SelectMethodText %></h2>
        <table class="sw-seum-radiolist">
            <tr class="sw-seum-row">
                <td><asp:RadioButton ID="installLocation" Checked="true" GroupName="LocationType" runat="server" /></td>
                <td><img src="/Orion/SEUM/Images/install_location_16x16.png" alt="<%= InstallLocationOnNetworkLabelTitle %>" width="16" height="16" /></td>
                <td>
                    <asp:Label AssociatedControlID="installLocation" runat="server">
                        <span class="sw-seum-description"><%= InstallLocationOnNetworkLabelTitle %></span><br />
                        <span class="sw-text-helpful"><%= InstallLocationOnNetworkLabelDetail %></span>
                    </asp:Label>
                </td>
            </tr>
            <tr class="sw-seum-row">
                <td><asp:RadioButton ID="existingLocation" GroupName="LocationType" runat="server" /></td>
                <td><img src="/Orion/images/add_16x16.gif" alt="<%= AddExistingLocationLabelTitle %>" width="16" height="16" /></td>
                <td>
                    <asp:Label AssociatedControlID="existingLocation" runat="server">
                        <span class="sw-seum-description"><%= AddExistingLocationLabelTitle %></span><br />
                        <span class="sw-text-helpful"><%= AddExistingLocationLabelDetail %></span><br />
                        <a href="<%= CommonLinks.PlayerInstaller %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= DownloadPlayerText %></a>
                    </asp:Label>
                </td>
            </tr>
        </table>
        <hr class="sw-seum-line" />

        <div class="SEUM_WorkflowButtons">
            <orion:LocalizableButton ID="nextButton" LocalizedText="Next" DisplayType="Primary" runat="server" OnClick="nextButton_OnClick" OnClientClick="return SEUMDisableInDemo();" />
            <orion:LocalizableButton ID="cancelButton" LocalizedText="Cancel" DisplayType="Secondary" runat="server" CausesValidation="false" OnClick="cancelButton_OnClick" />
        </div>
    </div>
</asp:Content>
