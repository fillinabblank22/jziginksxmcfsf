﻿using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.DAL;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Admin_Settings : System.Web.UI.Page, IBypassAccessLimitation
{
    private static readonly Log log = new Log();

    protected readonly string DetailedRetentionLabel = "Detailed Statistics Retention";
    protected readonly string DetailedRetentionDescription = "Detailed statistics will be summarized into hourly statistics after configured.";
    protected readonly string DetailedRetentionUnit = "days";

    protected readonly string HourlyRetentionLabel = "Hourly Statistics Retention";
    protected readonly string HourlyRetentionDescription = "Hourly statistics will be summarized into daily statistics after configured.";
    protected readonly string HourlyRetentionUnit = "days";

    protected readonly string DailyRetentionLabel = "Daily Statistics Retention";
    protected readonly string DailyRetentionDescription = "Daily statistics will be deleted from the database after configured.";
    protected readonly string DailyRetentionUnit = "days";

    protected readonly string ScreenshotsRetentionLabel = "Screenshot Retention";
    protected readonly string ScreenshotsRetentionDescription = "Screenshots in detailed statistics will be deleted from the database after configured.";
    protected readonly string ScreenshotsRetentionUnit = "days";

    protected const string ScreenshotCapturesLabel = "Screenshot Capturing";
    protected const string ScreenshotCapturesDescription = "Disabling screenshot capturing will improve performance by using less database space and less bandwidth.";

    protected const string EnabledLabel = "Enabled";
    protected const string DisabledLabel = "Disabled";
    protected const string PerTransactionLabel = "Per Transaction";

    protected readonly string DatabaseSettingsTitle = "Database Settings";

    private SettingsDAL dal = new SettingsDAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        // TODO:
        ((Orion_SEUM_Admin_SEUMAdminPage)Master).HelpFragment = "";

        // set up the captures radio list
        screenshotCapturesRadio.Items.Add(new ListItem(PerTransactionLabel, string.Empty));
        screenshotCapturesRadio.Items.Add(new ListItem(EnabledLabel, true.ToString()));
        screenshotCapturesRadio.Items.Add(new ListItem(DisabledLabel, false.ToString()));

        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }

    protected void Cancel_Click(object source, EventArgs e)
    {
       Response.Redirect("/Orion/SEUM/Admin");
    }

    protected void Submit_Click(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        using (ISEUMBusinessLayer proxy = BusinessLayerFactory.Create())
        {
            SaveValue(detailedRetentionBox, proxy);
            SaveValue(hourlyRetentionBox, proxy);
            SaveValue(dailyRetentionBox, proxy);
            SaveValue(screenshotsRetentionBox, proxy);

            if (screenshotCapturesRadio.SelectedValue != dal.GetValue(Settings.ScreenshotCaptures))
            {
                SaveValue(Settings.ScreenshotCaptures, screenshotCapturesRadio.SelectedValue, proxy);
                proxy.RefreshAllTransactionsModifyDates();
            }
        }

        Response.Redirect("/Orion/SEUM/Admin");
    }

    private void LoadData()
    {
        LoadValue(detailedRetentionBox);
        LoadValue(hourlyRetentionBox);
        LoadValue(dailyRetentionBox);
        LoadValue(screenshotsRetentionBox);

        LoadCaptureSettings();
    }

    private void SaveValue(ValidatedTextBox tb, ISEUMBusinessLayer proxy)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        try
        {
            double val = Convert.ToDouble(tb.Text);

            if (tb.UseRangeValidator)
            {
                double min = Convert.ToDouble(tb.MinValue);
                double max = Convert.ToDouble(tb.MaxValue);

                if (val < min || val > max)
                    return;
            }

            SaveValue(tb.CustomStringValue, val.ToString(), proxy);
        }
        catch (Exception e)
        {
            log.Error("Error saving WPM setting value.", e);
        }
    }

    private void SaveValue(string name, string value, ISEUMBusinessLayer proxy)
    {
        if (String.IsNullOrEmpty(name))
            throw new ArgumentException("Name must not be null or empty.", "name");

        try
        {
            proxy.SetSettingValue(name, value);
        }
        catch (Exception e)
        {
            log.Error("Error saving WPM setting value.", e);
        }
    }

    private void LoadValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        try
        {
            string value = dal.GetValue(tb.CustomStringValue);

            switch (tb.Type)
            {
                case ValidatedTextBox.TextBoxType.Integer:
                    {
                        int val = 0;
                        if (Int32.TryParse(value, out val))
                            tb.Text = value;
                    }
                    break;
                case ValidatedTextBox.TextBoxType.Double:
                    {
                        double val = 0;
                        if (Double.TryParse(value, out val))
                            tb.Text = value;
                    }
                    break;
                default:
                    tb.Text = value;
                    break;
            }
        }
        catch (Exception e)
        {
            log.Error("Error loading WPM setting value.", e);
        }
    }

    private void LoadCaptureSettings()
    {
        try
        {
            string value = dal.GetValue(Settings.ScreenshotCaptures);

            screenshotCapturesRadio.SelectedValue = value ?? string.Empty;
        }
        catch (Exception e)
        {
            log.Error("Error loading WPM setting value.", e);
        }
    }
}