﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/SEUMManagementPage.master" AutoEventWireup="true" CodeFile="ManageMonitors.aspx.cs" Inherits="Orion_SEUM_Admin_ManageMonitors" %>

<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<%@ Register TagPrefix="SEUM" TagName="MonitorsGrid" Src="~/Orion/SEUM/Controls/MonitorsGrid.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentHeaderPlaceholder" runat="Server">
    <h1><%= Page.Title %></h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <SEUM:MonitorsGrid runat="server" ID="monitorsGrid" />

    <div class="SEUM_LicenseUsage">
        <b><%= LicenseUsageText %>:</b> <span id="licenseUsageNumbers"><%= LicenseUsageNumbers %></span> <a href="<%= Profile.AllowAdmin ? CommonLinks.LicenseDetails : CommonLinks.ModuleOnlyLicenseDetails  %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= ViewDetailsText %></a>
    </div>
</asp:Content>

