﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/SEUMManagementPage.master" AutoEventWireup="true" 
CodeFile="ManageRecordings.aspx.cs" Inherits="Orion_SEUM_Admin_ManageRecordings" %>
<%@ Register TagPrefix="SEUM" TagName="RecordingsGrid" Src="~/Orion/SEUM/Controls/RecordingsGrid.ascx" %>
<%@ Register Src="~/Orion/SEUM/Controls/DownloadRecorderMenu.ascx" TagPrefix="seum" TagName="DownloadRecorderMenu" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <seum:DownloadRecorderMenu  runat="server"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentHeaderPlaceholder" Runat="Server">
    <h1>
        <%= Page.Title %>
        <orion:HelpLink ID="HelpLink" runat="server" CssClass="SEUM_ManageTransactionMonitorsHelpLink" />
    </h1>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <SEUM:RecordingsGrid runat="server" ID="recordingsGrid" />
</asp:Content>

