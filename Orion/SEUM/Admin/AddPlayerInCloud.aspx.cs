﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.SEUM.Common.Helpers;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Configuration;
using SolarWinds.SEUM.Web.Exceptions;

public partial class Orion_SEUM_Admin_AddPlayerInCloud : System.Web.UI.Page
{
    private static readonly Log _log = new Log();

    private const string ReturnUrlFragment = "/Orion/SEUM/Admin/FinalizeCloud.aspx";

    protected const string ReloadIFrameAddress = "/Orion/Admin";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
        {
            throw new SEUMUnauthorizedAccessException();
        }

        if(!string.IsNullOrEmpty(Request["returnUrl"]))
        {
            Session[CommonParameterNames.ReturnUrlSessionKey] = UrlHelper.FromSafeUrlParameter(Request[CommonParameterNames.ReturnUrl]);
        }
        else
        {
            Session[CommonParameterNames.ReturnUrlSessionKey] = null;
        }
    }

    /// <summary>
    /// Get unique secret key
    /// </summary>
    protected string SecretKey
    {
        get
        {
            if (Session[CommonParameterNames.SecretKeySessionKey] == null)
            {
                _log.Debug("Cloud secret key not found - generating new one.");
                Session[CommonParameterNames.SecretKeySessionKey] = Guid.NewGuid().ToString();
            }

            return (string)Session[CommonParameterNames.SecretKeySessionKey];
        }
    }

    /// <summary>
    /// Returns URL address to cloud management portal with both return URL and secret key
    /// </summary>
    protected string CloudManagementPortalAddress
    {
        get
        {
            var address = UrlHelper.AppendParameter(SEUMWebConfiguration.Instance.CloudManagementPortalAddress, CommonParameterNames.ReturnUrl,
                                             UrlHelper.ToSafeUrlParameter(HostUrl + ReturnUrlFragment));

            address = UrlHelper.AppendParameter(address, "secretKey", UrlHelper.ToSafeUrlParameter(SecretKey));

            if (Request.Cookies["SEUMCloudHideDisclamer"] != null)
                address = UrlHelper.AppendParameter(address, "hideDisclaimer", "1");

            return address;
        }
    }

    /// <summary>
    /// Get full host url
    /// </summary>
    private string HostUrl
    {
        get
        {
            return Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Host + ":" + Request.Url.Port;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
