﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Exceptions;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Admin_EditPlayer : System.Web.UI.Page, IBypassAccessLimitation
{
    protected const string AddPageTitle = "Add Transaction Location";
    protected const string EditPageTitle = "Edit Transaction Location";

    protected const string MainDescription =
        "Web Performance Monitor's Player must be installed on the local machine to play the recording.";
    protected const string NameLabel = "Name for player at this location";
    protected const string CommunicationTypeLabel = "Orion server communication with this player";
    protected const string RemoteSettingsLabel = "Remotely send changes to this player";
    protected const string RemoteSettingsCheckboxLabel = "Update player settings";
    protected const string RemoteSettingsHelpText = "Only password and/or port can be changed remotely. Conflicting network settings may cause the player to become unavailable."
        + " Changes will not be pushed to additional WPM installations that share access to this player.";

    protected const string PassiveRadioLabel = "Server initiated communication (recommended default)";
    protected const string ActiveRadioLabel = "Player initiated communication ";
    protected const string PassiveHelpText = "This player waits for requests from the Orion server on a specified port."
            + " This port must be opened on the player machine firewall so the Orion server can connect to it."
            + " No change on the Orion server firewall is required.";
    protected const string ActiveHelpText = @"This player initiates communication with Orion server. Player connects to the Orion server on port 17782."
            + " This port must be opened on the Orion server firewall so the player can connect to it."
            + " No change on the player firewall is required.";

    protected const string IPLabel = "IP Address or hostname where the player is located";
    protected const string ProxyUrlLabel = "Proxy URL";

    protected const string AdvancedLabel = "Advanced";
    protected const string PortLabel = "Player port";
    protected const string PasswordLabel = "Player password (optional)";
    protected const string WhatIsThisLabel = "What is this?";
    protected const string WhatIsProxyLabel = "What is a proxy?";
    protected const string TroubleshootingLabel = "Troubleshooting";
    protected const string LoadingText = "Loading";
    protected const string TestingText = "Testing";
    protected const string LogLevelLabel = "Log level";
    protected const string DiagnosticsLabel = "Diagnostics";
    protected const string GenerateNewDiagnosticsLabel = "Generate new diagnostics";
    protected const string LocationNotRespondingError = "Player is not responding. Remote troubleshooting is not available.";
    protected const string AssignToPollerLabel = "Assign to Poller";
    protected const string AssignedToPollerLabel = "Assigned to Poller";
    protected const string PlayerNotRespondingError = "Check the player configuration on both the remote player and the WPM Web Console and that the remote player service is up and running.";
    protected const string LearnMoreLabel = "Learn More";
    protected const string ActiveAgentLabel = "Player";
    protected const string AgentGuidLabel = "Player GUID";
    protected const string UseProxyLabel = "Use proxy";
    protected const string UpdateRemoteSettingsLabel = "Update player port and password directly on player location";


    protected const string AgentNameEmptyError = "Name can not be empty";
    protected const string AgentNameExistsError = "Location with the same name already exists. Please enter different name.";
    
    // GUID validator is for hidden guid field. User selects agent from combo box so validation message must refer to that box and not to GUID.
    protected const string AgentGuidEmptyError = "Some player must be selected";
    protected const string AgentGuidExistsError = "Selected player is already added. Please select different player.";
    protected const string AgentGuidFormatError = "Player GUID has incorrect format. It should be xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.";
    
    protected const string PollingEngineEmptyError = "Poller must be specified.";
    protected const string PollingEngineFormatError = "Invalid poller selected.";
    protected const string AgentUrlEmptyError = "IP Address/Hostname can not be empty";
    protected const string AgentUrlFormatError = "IP Address or hostname must be a valid url. Actual Url: ";
    protected const string AgentPortError = "Port must be a number between 1 and 65535";
    protected const string AgentProxyUrlEmptyError = "Proxy Url can not be empty";
    protected const string AgentProxyUserNameEmptyError = "Proxy Username can not be empty";
    protected const string AgentLocationExistsError = "Location with the same IP Address/Hostname and port already exists. Please enter different IP Address/Hostname or port.";
    protected const string AdditionalPollerAssignedToLocalhostError = "Location with relative address (localhost or 127.0.0.0/8 loopback block) cannot be assigned to an additional poller.";
    
    protected const string DownloadPlayerText = "Download player locally";

    protected const string UseProxyAuthenticationLabel = "Use proxy authentication";
    protected const string ProxyUserNameLabel = "Username";
    protected const string ProxyPasswordLabel = "Password";

    private const string savePostBackArgument = "saveAgent";

    private Agent agent;
    private string returnUrl = null;
    // Main poller ID (Engines table).
    private static int mainPollerEngineId = 0;

    private void CheckPermissions()
    {
        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
        {
            throw new SEUMUnauthorizedAccessException();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request[CommonParameterNames.ReturnUrl]))
            returnUrl = UrlHelper.FromSafeUrlParameter(Request[CommonParameterNames.ReturnUrl]);

        SavePostBack = Page.ClientScript.GetPostBackEventReference(this, savePostBackArgument);

        int agentId = 0;
        if (Int32.TryParse(Request["id"], out agentId))
        {
            CheckPermissions(); // in demo user shouldn't be able to edit player
            Page.Title = EditPageTitle;
            IsEditPage = true;
        }
        else
        {
            Page.Title = AddPageTitle;
            IsEditPage = false;
        }

        RunTest = false;
        RunTestAndSave = false;

        nodesSelector.EntityType = SwisEntities.Nodes;

        // setup validators
        nameValidator.ServerValidate += nameValidator_ServerValidate;
        nameValidator.ValidateEmptyText = true;
        nameValidator.CssClass = "SEUM_ValidationError";
        guidValidator.ServerValidate += guidValidator_ServerValidate;
        guidValidator.ValidateEmptyText = true;
        guidValidator.CssClass = "SEUM_ValidationError";
        passivePollerValidator.ServerValidate += pollerValidator_ServerValidate;
        passivePollerValidator.ValidateEmptyText = true;
        passivePollerValidator.CssClass = "SEUM_ValidationError";
        urlValidator.ServerValidate += urlValidator_ServerValidate;
        urlValidator.ValidateEmptyText = true;
        urlValidator.CssClass = "SEUM_ValidationError";
        portValidator.ServerValidate += portValidator_ServerValidate;
        portValidator.ValidateEmptyText = true;
        portValidator.CssClass = "SEUM_ValidationError";
        proxyValidator.ServerValidate += proxyValidator_ServerValidate;
        proxyValidator.ValidateEmptyText = true;
        proxyValidator.CssClass = "SEUM_ValidationError";
        proxyAuthenticationValidator.ServerValidate += proxyAuthenticationValidator_ServerValidate;
        proxyAuthenticationValidator.ValidateEmptyText = true;
        proxyAuthenticationValidator.CssClass = "SEUM_ValidationError";

        if (!Page.IsPostBack)
        {
            var agentsDal = new AgentsDAL(); 
            // Fill poller engines combo
            var pollingEngines = agentsDal.GetAvailablePollingEngines();
            if (pollingEngines.Count() < 1)
                throw new InvalidOperationException("There is no available polling engine.");

            // Main poller is always the first in the list
            mainPollerEngineId = pollingEngines.ElementAt(0).Key;
            foreach (var kvp in pollingEngines)
            {
                pollingEnginePassive.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
            }

            if (agentId != 0)
            {
                using (var proxy = BusinessLayerFactory.Create())
                {
                    agent = proxy.AgentGet(agentId);
                }

                if (agent == null)
                    throw new ArgumentException("Specified agent was not found.");
            }
            else
            {
                agent = new Agent();
            }

            LoadAgentIntoForm();

            originalAgentDnsNameBox.Value = agent.DNSName;
            originalAgentIpBox.Value = agent.IP;
            originalAgentPollingEngineId.Value = agent.PollingEngineId.ToString();
            originalAgentPollingEngineName.Value = agent.PollingEngineName;

            // override default passive selection)))
            if (Request["mode"] != null)
            {
                activeRadio.Checked = Request["mode"].Equals("active", StringComparison.OrdinalIgnoreCase);
            }
            /*
            // bind nodes selector
            if (IsEditPage && LicenseInfo.AreNodesAvailable)
            {
                var entities = agentsDal.GetAgentRelatedEntities(new[] {agent.AgentId});
                nodesSelector.SelectedObjectUris = entities.ContainsKey(agent.AgentId) ? 
                            new DependenciesDAL().FilterSwisUrisByEntityType(entities[agent.AgentId], SwisEntities.Nodes) :
                            Enumerable.Empty<String>();
                nodesSelector.DataBind();
            }*/
            passiveRadio.Checked = !activeRadio.Checked;
        }
        else
        {
            // set correct URL format if required
            if (!string.IsNullOrEmpty(urlBox.Text) && !urlBox.Text.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                urlBox.Text = "https://" + urlBox.Text;
            }

            if (updateRemoteSettingsCheckbox.Checked)
            {
                using (var proxy = BusinessLayerFactory.Create())
                {
                    agent = proxy.AgentGet(agentId);
                }

                if (agent == null)
                    throw new ArgumentException("Specified agent was not found.");

                // when updating remote settings, propagate just password and port
                agent.Password = passwordPassiveBox.Text;
                int port;
                Int32.TryParse(portBox.Text, out port);
                agent.Port = port;

                LoadAgentIntoForm();
            }
            else
            {
                agent = GetAgentFromForm();
            }

            passwordPassiveBox.Attributes["value"] = agent.Password;
            proxyPasswordBox.Attributes["value"] = agent.ProxyPassword;

            if (Request["__EVENTARGUMENT"] == savePostBackArgument)
            {
                Page.Validate();
                if (Page.IsValid && SEUMProfile.AllowAdmin)
                {
                    SaveAgent();
                }
            }
        }

        ShowPollerDropDown = pollingEnginePassive.Items.Count != 1;
        pollingEngineActiveName.Attributes.Add("readonly", "readonly");
        if (originalAgentModeBox.Value == "active")
            updateRemoteSettingsCheckbox.InputAttributes.Add("disabled", "disabled");
    }

    protected string SavePostBack { get; set; }

    protected bool IsEditPage { get; set; }

    protected bool ShowPollerDropDown { get; set; }
    
    void proxyValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        if (updateRemoteSettingsCheckbox.Checked)
            return;

        if (agent.IsActiveAgent)
            return;

        if (!agent.UseProxy)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            validator.ErrorMessage = AgentProxyUrlEmptyError;
            args.IsValid = false;
            return;
        }
    }

    void proxyAuthenticationValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        if (updateRemoteSettingsCheckbox.Checked)
            return;

        if (agent.IsActiveAgent)
            return;

        if (!agent.UseProxy)
            return;

        if (!agent.UseProxyAuthentication)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            validator.ErrorMessage = AgentProxyUserNameEmptyError;
            args.IsValid = false;
            return;
        }
    }

    void portValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        if (agent.IsActiveAgent)
            return;

        int port;
        if (!Int32.TryParse(args.Value, out port))
        {
            validator.ErrorMessage = AgentPortError;
            args.IsValid = false;
            return;
        }
    }

    void urlValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        if (updateRemoteSettingsCheckbox.Checked)
            return;

        if (agent.IsActiveAgent)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            validator.ErrorMessage = AgentUrlEmptyError;
            args.IsValid = false;
            return;
        }

        Uri tempUri;
        if (!Uri.TryCreate(args.Value, UriKind.Absolute, out tempUri))
        {
            validator.ErrorMessage = AgentUrlFormatError + args.Value;
            args.IsValid = false;
            return;
        }

        if (!IsUniqueAgentLocation(agent.AgentId, agent.Url, agent.Port))
        {
            validator.ErrorMessage = AgentLocationExistsError;
            args.IsValid = false;
            return;
        }
    }

    void pollerValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        if (updateRemoteSettingsCheckbox.Checked)
            return;

        if (string.IsNullOrEmpty(args.Value))
        {
            validator.ErrorMessage = PollingEngineEmptyError;
            args.IsValid = false;
            return;
        }

        int pollerId;
        if (!Int32.TryParse(args.Value, out pollerId))
        {
            validator.ErrorMessage = PollingEngineFormatError;
            args.IsValid = false;
            return;
        }

        // Additional poller can't be assigned to agent with relative address (localhost/127.0.0.1)
        if (pollerId != mainPollerEngineId)
        {
            string url = urlBox.Text.Trim();
            if (string.IsNullOrEmpty(url))
                return;

            if (Regex.Match(url, @"^(http(s)?\:\/\/)?(localhost|127\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})", RegexOptions.IgnoreCase).Success)
            {
                validator.ErrorMessage = AdditionalPollerAssignedToLocalhostError;
                args.IsValid = false;
                return;
            }
        }
    }

    void guidValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        if (updateRemoteSettingsCheckbox.Checked)
            return;
        
        if (!agent.IsActiveAgent)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            validator.ErrorMessage = AgentGuidEmptyError;
            args.IsValid = false;
            return;
        }

        try
        {
            Guid guid = new Guid(args.Value);
            if (guid == Guid.Empty)
            {
                validator.ErrorMessage = AgentGuidEmptyError;
                args.IsValid = false;
                return;
            }
        }
        catch (Exception)
        {
            validator.ErrorMessage = AgentGuidFormatError;
            args.IsValid = false;
        }

        if (!IsUniqueAgentName(agent.AgentId, args.Value))
        {
            validator.ErrorMessage = AgentGuidExistsError;
            args.IsValid = false;
            return;
        }
    }

    void nameValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator validator = source as CustomValidator;
        if (validator == null)
            return;

        args.IsValid = true;

        // don't validate when updating remote settings
        if (updateRemoteSettingsCheckbox.Checked)
            return;

        if (string.IsNullOrEmpty(args.Value) || string.IsNullOrEmpty(args.Value.Trim()))
        {
            validator.ErrorMessage = AgentNameEmptyError;
            args.IsValid = false;
            return;
        }

        if (!IsUniqueAgentName(agent.AgentId, args.Value))
        {
            validator.ErrorMessage = AgentNameExistsError;
            args.IsValid = false;
            return;
        }
    }

    private void LoadAgentIntoForm()
    {
        agentIdBox.Value = agent.AgentId.ToString();
        agentDnsNameBox.Value = agent.DNSName;
        agentIpBox.Value = agent.IP;
        nameBox.Text = agent.Name;
        agentVersionFromTest.Value = agent.AgentVersion.ToString();
        // passive
        urlBox.Text = agent.Url;
        originalAgentUrlBox.Value = agent.Url;
        portBox.Text = agent.Port != 0 ? agent.Port.ToString() : "17781";
        useProxyCheckbox.Checked = agent.UseProxy;
        proxyUrlBox.Text = agent.ProxyUrl;
        useProxyAuthenticationCheckbox.Checked = agent.UseProxyAuthentication;
        proxyUserNameBox.Text = agent.ProxyUserName;
        proxyPasswordBox.Attributes["value"] = agent.ProxyPassword;
        pollingEnginePassive.SelectedValue = agent.PollingEngineId.ToString();

        // active
        activeRadio.Checked = agent.IsActiveAgent;
        guidBox.Text = agent.AgentGuid.ToString();
        originalAgentGuidBox.Value = agent.AgentGuid.ToString();
        pollingEngineActiveId.Value = agent.PollingEngineId.ToString();
        pollingEngineActiveName.Text = agent.PollingEngineName;

        originalAgentModeBox.Value = agent.IsActiveAgent ? "active" : "passive";

        passwordPassiveBox.Attributes["value"] = agent.Password;
    }

    private Agent GetAgentFromForm()
    {
        Agent agent = new Agent();

        int agentId;
        Int32.TryParse(agentIdBox.Value, out agentId);
        agent.AgentId = agentId;

        agent.Name = nameBox.Text;
        agent.IsActiveAgent = activeRadio.Checked;
        agent.DNSName = agentDnsNameBox.Value;
        agent.IP = agentIpBox.Value;

        Version version;
        if (Version.TryParse(agentVersionFromTest.Value, out version))
            agent.AgentVersion = version;

        int pollingEngineId;
        if (agent.IsActiveAgent)
        {
            try
            {
                agent.AgentGuid = new Guid(guidBox.Text);
                if (int.TryParse(pollingEngineActiveId.Value, out pollingEngineId))
                    agent.PollingEngineId = pollingEngineId;
            }
            catch (Exception)
            { }
            agent.Password = string.Empty;
        }
        else
        {
            agent.Url = urlBox.Text;
            agent.Password = passwordPassiveBox.Text;
            int port;
            Int32.TryParse(portBox.Text, out port);
            agent.Port = port;
            agent.UseProxy = useProxyCheckbox.Checked;
            agent.ProxyUrl = proxyUrlBox.Text;
            agent.UseProxyAuthentication = useProxyAuthenticationCheckbox.Checked;
            agent.ProxyUserName = proxyUserNameBox.Text;
            agent.ProxyPassword = proxyPasswordBox.Text;
            if (Int32.TryParse(pollingEnginePassive.SelectedValue, out pollingEngineId))
                agent.PollingEngineId = pollingEngineId;
        }

        int status;
        Int32.TryParse(agentStatusBox.Value, out status);
        agent.ConnectionStatus = status;
        agent.ConnectionStatusMessage = agentStatusMessageBox.Value;

        agent.LogLevel = logLevelCombo.SelectedValue;

        return agent;
    }

    public void SaveAgent()
    {
        SaveAgentInternal(updateRemoteSettingsCheckbox.Checked);

        if (string.IsNullOrEmpty(returnUrl))
            GoBack();
        else
            Response.Redirect(returnUrl);
    }

    public void SaveAgentInternal(bool updateRemoteSettings)
    {
        Agent newAgent = GetAgentFromForm();
        Agent existingAgent = null;

        using (var proxy = BusinessLayerFactory.Create())
        {
            if (newAgent.AgentId == 0)
            {
                newAgent.AgentId = proxy.AgentAdd(newAgent);
            }
            else
            {
                // We don't want to update any of the fields we don't already fill out...
                existingAgent = proxy.AgentGet(newAgent.AgentId);

                if (!updateRemoteSettings)
                {
                    existingAgent.Url = newAgent.Url;
                    existingAgent.Name = newAgent.Name;
                    existingAgent.UseProxy = newAgent.UseProxy;
                    existingAgent.ProxyUrl = newAgent.ProxyUrl;
                    existingAgent.UseProxyAuthentication = newAgent.UseProxyAuthentication;
                    existingAgent.ProxyUserName = newAgent.ProxyUserName;
                    existingAgent.ProxyPassword = newAgent.ProxyPassword;
                    existingAgent.PollingEngineId = newAgent.PollingEngineId;
                    existingAgent.ConnectionStatus = newAgent.ConnectionStatus;
                    existingAgent.ConnectionStatusMessage = newAgent.ConnectionStatusMessage;
                    existingAgent.IsActiveAgent = newAgent.IsActiveAgent;
                    existingAgent.AgentGuid = newAgent.AgentGuid;
                }

                existingAgent.Port = newAgent.Port;
                existingAgent.Password = newAgent.Password;
                existingAgent.DNSName = newAgent.DNSName;
                existingAgent.IP = newAgent.IP;
                existingAgent.AgentVersion = newAgent.AgentVersion;

                proxy.AgentUpdate(existingAgent, updateRemoteSettings);
            }
        }

        if (existingAgent != null && !string.IsNullOrEmpty(newAgent.LogLevel))
        {
            using (var proxy = BusinessLayerFactory.Create(existingAgent.PollingEngineId))
            {
                proxy.SetAgentLogLevel(existingAgent.AgentId, newAgent.LogLevel);
            }
        }
        /*
        if (LicenseInfo.AreNodesAvailable)
        {
            RelatedEntitiesHelper.SaveAgentRelatedNodes(existingAgent ?? newAgent, nodesSelector.SelectedObjectUris);
        }*/
    }

    public bool IsUniqueAgentName(int agentId, string agentName)
    {
        AgentsDAL dal = new AgentsDAL();
        return dal.IsUniqueAgentName(agentName, agentId);
    }

    public bool IsUniqueAgentLocation(int agentId, string url, int port)
    {
        AgentsDAL dal = new AgentsDAL();
        return dal.IsUniqueAgentLocation(url, port, agentId);
    }

    public bool IsUniqueAgentGuid(int agentId, Guid guid)
    {
        AgentsDAL dal = new AgentsDAL();
        return dal.IsUniqueAgentGuid(guid, agentId);
    }

    private bool IsAgentActive()
    {
        return originalAgentModeBox.Value == "active";
    }

    protected string LearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsPlaybackLocations");
        }
    }

    protected string InstallingPlayerHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGInstallingPlayer");
        }
    }

    protected string LearnMoreActivePassiveUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMPHActivePassivePlayer");
        }
    }

    protected string WhatIsProxyHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionSEUMPHProxy"); }
    }

    /// <summary>
    /// todo: after there are more items than just poller dropdown in active agent advanced settings section remove this property and 'if' wrapping advanced section
    /// </summary>
    protected bool ShowActiveAdvanced
    {
        get { return ShowPollerDropDown; }
    }

    protected bool RunTest { get; set; }
    protected bool RunTestAndSave { get; set; }

    protected void Save_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && SEUMProfile.AllowAdmin)
        {
            RunTestAndSave = true;
        }
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(returnUrl))
            GoBack();
        else
            Response.Redirect(returnUrl);
    }

    protected void testButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && SEUMProfile.AllowAdmin)
        {
            RunTest = true;
        }
    }

    private void GoBack()
    {
        Response.Redirect("/Orion/SEUM/Admin/ManagePlayers.aspx");
    }
}
