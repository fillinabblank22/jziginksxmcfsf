﻿using SolarWinds.Logging;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Helpers;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using SolarWinds.SEUM.BusinessLayer.Servicing;

public partial class Orion_SEUM_Admin_FinalizeCloud : System.Web.UI.Page
{
    private static readonly Log _log = new Log();

    #region Constants

    protected const string DefaultCancelUrl = "/Orion/SEUM/Admin/ManagePlayers.aspx";
    protected const string DefaultRedirectUrlFormat = "/Orion/SEUM/Admin/PlayerRelatedEntities.aspx?agentIds={0}";

    protected const string ErrorTitle = "Couldn't create location";
    protected const string HideLabel = "hide";
    protected const string ShowLabel = "show";

    protected const string RetryButtonLabel = "Try again";

    protected const string AgentNameLabel = "Agent Name:";
    protected const string AgentUrlLabel = "Agent URL:";
    protected const string AgentPortLabel = "Agent Port:";
    protected const string AgentPasswordLabel = "Agent Password:";
    protected const string AgentProxyLabel = "Agent Proxy URL:";
    protected const string AgentProxyUserNameLabel = "Agent Proxy Username:";
    protected const string AgentProxyPasswordLabel = "Agent Proxy Password:";

    protected const string LoadingLabel = @"Loading...
            <div class=""sw-text-helpful"">Do not reload the page!</div>";

    protected const string IncorrectDataErrorMessage =
        "Cloud manager recieved incomplete or incorrect data and cannot continue. Please add Location manually.";

    protected const string TroubleshootingLabel =
        @"For more troubleshooting help, <a target=""_blank"" href=""#"">see the SolarWinds Knowledge Base</a>, or
        <a target=""_blank"" href=""http://www.solarwinds.com/support/"">contact support</a>.";

    private const string DefaultAgentProtocol = "https://";

    protected const Int32 LoadingScreenTimeOutMs = 2000;

    /// <summary>
    /// Average cloud creation duration (in minutes)
    /// </summary>
    private const int AverageCloudCreationDuration = 30;

    #endregion // Constants

    private int agentId;

    /// <summary>
    /// Cancel Url on Orion server -> where to redirect after cancelled
    /// </summary>
    protected string CancelUrl
    {
        get
        {
            if (Session[CommonParameterNames.ReturnUrlSessionKey] != null)
	 	        return (string)Session[CommonParameterNames.ReturnUrlSessionKey];
            else
                return DefaultCancelUrl;
        }
    }

    /// <summary>
    /// Return Url on Orion server -> where to redirect after add is finished
    /// </summary>
    protected string OrionRedirectUrl
    {
        get
        {
            var returnUrl = Session[CommonParameterNames.ReturnUrlSessionKey] as string;
            /*if (LicenseInfo.AreNodesAvailable)
            {
                String url = String.Format(DefaultRedirectUrlFormat, agentId);
                return string.IsNullOrEmpty(Session[CommonParameterNames.ReturnUrlSessionKey] as string)
                    ? url
                    : url + String.Format("&{0}={1}", CommonParameterNames.ReturnUrl,UrlHelper.ToSafeUrlParameter(returnUrl));
            }*/
            return returnUrl ?? CancelUrl;
        }
    }

    /// <summary>
    /// Information if page should automatically redirect
    /// </summary>
    protected bool DoRedirect { get; private set; }

    /// <summary>
    /// Url to redirect to
    /// </summary>
    protected string RedirectUrl { get; private set; }

    /// <summary>
    /// Error text
    /// </summary>
    protected string ErrorText { get; private set; }

    /// <summary>
    /// Agent instance
    /// </summary>
    protected Agent Agent { get; private set; }

    /// <summary>
    /// Secret key
    /// </summary>
    protected string SecretKey
    {
        get
        {
            if (Session[CommonParameterNames.SecretKeySessionKey] != null)
                return (string)Session[CommonParameterNames.SecretKeySessionKey];
            else
                throw new InvalidOperationException("Could not get secret key");
        }
    }

    /// <summary>
    /// Allow retry button
    /// </summary>
    protected bool AllowRetry { get; set; }

    /// <summary>
    /// Retry url
    /// </summary>
    protected string RetryUrl
    {
        get { return Request.Url.ToString(); }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
        {
            throw new SEUMUnauthorizedAccessException();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DoRedirect = false;
        AllowRetry = false;
        ErrorText = "";

        // hide disclaimer
        if (Request["hideDisclaimer"] != null)
            Response.SetCookie(new HttpCookie("SEUMCloudHideDisclamer", "true") { Expires = DateTime.UtcNow.AddYears(1) });

        // wizard was cancelled
        if(Request["cancel"] != null || Request["legal"] != null)
        {
            DoRedirect = true;
            RedirectUrl = CancelUrl;
            _log.Debug("Cloud wizard was cancelled.");
            return;
        }

        _log.Debug("Processing data from cloud manager [URL]=" + Request.Url.ToString());

        Agent = new Agent();
        AgentsDAL agentsDal = new AgentsDAL();

        try
        {
            Agent.Url = DefaultAgentProtocol + UrlHelper.FromSafeUrlParameter(Request["agentIp"]);
            var name = UrlHelper.FromSafeUrlParameter(Request["name"]);
            var index = 1;
            Agent.Name = name;

            // check that agent name is unique
            while (!agentsDal.IsUniqueAgentName(Agent.Name, 0))
            {
                Agent.Name = name + "(" + index + ")";
                index++;
            }

            Agent.Port = UInt16.Parse(UrlHelper.FromSafeUrlParameter(Request["agentPort"]));
            if (Request["agentProxy"] != null)
            {
                Agent.UseProxy = true;
                Agent.ProxyUrl = UrlHelper.FromSafeUrlParameter(Request["agentProxy"]);
            }
            else
            {
                Agent.UseProxy = false;
                Agent.ProxyUrl = string.Empty;
            }

            bool decryptProxyPassword = false;
            if (Request["proxyUserName"] != null && Request["proxyPassword"] != null)
            {
                Agent.UseProxyAuthentication = true;
                Agent.ProxyUserName = UrlHelper.FromSafeUrlParameter(Request["proxyUserName"]);
                decryptProxyPassword = true;
            }
            else
            {
                Agent.UseProxyAuthentication = false;
                Agent.ProxyUserName = string.Empty;
                Agent.ProxyPassword = string.Empty;
            }

            int avgDuration;
            TimeSpan timeSpan;

            if(Int32.TryParse(UrlHelper.FromSafeUrlParameter(Request["duration"]), out avgDuration))
                timeSpan = new TimeSpan(0, avgDuration, 0);
            else
                timeSpan = new TimeSpan(0, AverageCloudCreationDuration, 0);
            
            Agent.ConnectionStatusTimeStamp = DateTime.UtcNow + timeSpan;

            Agent.ConnectionStatus = 0; // set status to preparation
            Agent.ConnectionStatusMessage = timeSpan.ToString();
            Agent.InCloud = true;

            using (var aes = Aes.Create())
            {
                using (var decryptor = aes.CreateDecryptor(Encoding.UTF8.GetBytes(SecretKey).Take(16).ToArray(), Encoding.UTF8.GetBytes(SecretKey).Take(16).ToArray()))
                {
                    // Decrypt agent password
                    byte[] encryptedPassword = Convert.FromBase64String(Request["agentPassword"]);
                    var decryptedPassword = decryptor.TransformFinalBlock(encryptedPassword, 0, encryptedPassword.Length);
                    Agent.Password = Encoding.UTF8.GetString(decryptedPassword);
                }
            }

            if (decryptProxyPassword)
            {
                using (var aes = Aes.Create())
                {
                    using (var decryptor = aes.CreateDecryptor(Encoding.UTF8.GetBytes(SecretKey).Take(16).ToArray(),
                                                            Encoding.UTF8.GetBytes(SecretKey).Take(16).ToArray()))
                    {
                        byte[] encryptedPassword = Convert.FromBase64String(Request["proxyPassword"]);
                        var decryptedPassword = decryptor.TransformFinalBlock(encryptedPassword, 0, encryptedPassword.Length);
                        Agent.ProxyPassword = Encoding.UTF8.GetString(decryptedPassword);
                    }
                }
            }

            IEnumerable<KeyValuePair<int, string>> pollingEngines = agentsDal.GetAvailablePollingEngines();
            if(!pollingEngines.Any())
                throw new InvalidOperationException("Cannot add new location, there is no polling engine available.");

            Agent.PollingEngineId = pollingEngines.First().Key;
        }
        catch (FormatException ex)
        {
            ErrorText = IncorrectDataErrorMessage;
            AllowRetry = false;
            _log.Error("Incorrect or partial data recieved from cloud manager [URL]='" + Request.Url.ToString() + "'", ex);
            return;
        }
        catch (Exception ex)
        {
            ErrorText = IncorrectDataErrorMessage + "\n\n" + ex.Message;
            AllowRetry = false;
            _log.Error("Incorrect or partial data recieved from cloud manager [URL]='" + Request.Url.ToString() + "'", ex);
            return;
        }

        try
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                agentId = proxy.AgentAdd(Agent);
            }
        }
        catch (Exception ex)
        {
            ErrorText = ex.Message;
            AllowRetry = true;
            _log.Error("Could not add cloud agent to database. Allowing retry.", ex);
            return;
        }

        DoRedirect = true;
        RedirectUrl = OrionRedirectUrl;
    }
}
