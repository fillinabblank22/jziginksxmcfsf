﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LicenseDetails.aspx.cs" Inherits="Orion_SEUM_Admin_LicenseDetails" MasterPageFile="~/Orion/SEUM/Admin/SEUMAdminPage.master" %>

<%@ Register TagPrefix="SEUM" TagName="ModuleDetails" Src="~/Orion/SEUM/Controls/ModuleDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= Page.Title %></h1>
    <div class="GroupBox">
        <SEUM:ModuleDetails runat="server" ID="moduleDetails" />
    </div>
</asp:Content>
