﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/SEUMAdminPage.master" AutoEventWireup="true" CodeFile="EditMonitor.aspx.cs" Inherits="Orion_SEUM_Admin_EditMonitor" %>
<%@ Import Namespace="SolarWinds.SEUM.Licensing" %>
<%@ Register TagPrefix="SEUM" TagName="TransactionPropertiesEditor" Src="~/Orion/SEUM/Controls/TransactionPropertiesEditor.ascx" %>
<%@ Register TagPrefix="SEUM" Src="~/Orion/SEUM/Controls/TransactionDependenciesControl.ascx" TagName="TransactionDependenciesControl" %>
<%@ Register TagPrefix="SEUM" Src="~/Orion/SEUM/Controls/NetObjectPickerDialog.ascx" TagName="NetObjectPickerDialog"%>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%= Page.Title %></h1>

    <div class="GroupBox">
        <SEUM:TransactionPropertiesEditor runat="server" ID="transactionEditor" />
        <% if (LicenseInfo.AreNodesAvailable || LicenseInfo.AreApplicationsAvailable) { %>
            <div id="SEUM_EditMonitor_RelatedEntities">
                <SEUM:NetObjectPickerDialog runat="server" />
                <h3>Select related entities</h3>
                <div class="SEUM_RelatedEntitiesSelection_SelectorsContainer">
                    <SEUM:TransactionDependenciesControl runat="server" ID="dependenciesControl" />
                </div>
            </div>
        <% } %>
        <div class="SEUM_WorkflowButtons">
            <orion:LocalizableButton ID="imgbSave" OnClick="Save_Click" LocalizedText="Save" DisplayType="Primary" OnClientClick="return SEUMDisableInDemo();" runat="server" />
            &nbsp;
            <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" runat="server" />
        </div>
    </div>
     <% if (ShowValidationError) { %>
    <script type="text/javascript" defer="defer">
      //<![CDATA[
        $(document).ready(function () {
            Ext.Msg.show({
                title: '<%=LicenseExceptionTitle%>',
                msg: '<%=LicenseExceptionText%>',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        });
      //]]>
    </script>
    <% } %>

    <% if (ShowJobEngineError) { %>
    <script type="text/javascript" defer="defer">
      //<![CDATA[
        $(document).ready(function () {
            Ext.Msg.show({
                title: '<%=JobEngineExceptionTitle%>',
                msg: '<%=JobEngineExceptionText%>',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        });
      //]]>
    </script>
    <% } %>
</asp:Content>
