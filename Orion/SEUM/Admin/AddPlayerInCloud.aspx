﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="AddPlayerInCloud.aspx.cs" Inherits="Orion_SEUM_Admin_AddPlayerInCloud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <script type="text/javascript" src="../../SEUM/js/IFrameManager.js"></script>
	<script type="text/javascript">
	    //<![CDATA[
	    $(function () {
	        var resize = function () {
	            $("#CloudManagementIFrame").height($(window).height() - $("#CloudManagementIFrame").offset().top - parseInt($("#content").css("padding-bottom"), 10));
	        };
	        $(window).resize(resize);
	        resize(); // set up initial size
	    });

        $(document).ready(function () {
            IFrameManager.InitParent('CloudManagementIFrame');

            setInterval(function () {
                var iframe = document.getElementById('HiddenIFrame');
                iframe.contentWindow.location.reload(true);
            }, 5 * 60 * 1000);
        });
    //]]>
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <iframe id="CloudManagementIFrame" width="100%" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src="<%= CloudManagementPortalAddress %>"></iframe>
    <iframe id="HiddenIFrame" width="0" height="0" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" src="<%= ReloadIFrameAddress %>" style="visibility: hidden; position: absolute; top: -20000px; left: -20000px;"></iframe>
</asp:Content>
