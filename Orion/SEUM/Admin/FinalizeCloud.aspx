﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMinReqs.master" CodeFile="FinalizeCloud.aspx.cs" Inherits="Orion_SEUM_Admin_FinalizeCloud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
                if(<%= DoRedirect.ToString().ToLower() %>) {
                    window.top.location = '<%= RedirectUrl %>';
                    return;
                }

                window.setTimeout(function() {
                    $('#sw-seum-cloud-loading').hide('fast');
                    $('#sw-seum-cloud-loading-mask').hide('fast');
                }, <%= LoadingScreenTimeOutMs %>);
            });
    </script>
    <style type="text/css">
        #sw-seum-cloud-loading-mask {
          position: absolute;
          left:     0;
          top:      0;
          width:    100%;
          height:   100%;
          z-index:  20000;
          background-color: white;
        }

        #sw-seum-cloud-loading {
          position: absolute;
          left:     50%;
          top:      50%;
          padding:  2px;
          z-index:  20001;
          height:   auto;
          margin:   -35px 0 0 -30px;
        }

        #sw-seum-cloud-loading .sw-seum-cloud-loading-indicator {
          background: url('/Orion/images/animated_loading_sm3_whbg.gif') no-repeat;
          color:      #555;
          font:       bold 13px tahoma,arial,helvetica;
          padding:    8px 42px;
          margin:     0;
          z-index:    20002;
          text-align: center;
          height:     auto;
        }
        .sw-title-main { margin: 5px 0 5px 0; padding: 0; font-size: 18px; line-height: 22px; font-weight: normal; }
        .sw-title-aux { margin: 5px 0 5px 0; padding: 0; font-size: 16px; line-height: 20px; font-weight: normal; }
        .sw-app-region a { color: #336699; }
        .sw-app-region a:hover { color: #f99d1c; }
        #errorcontent { border: 1px solid #ccc; background: #fff; display: inline-block; margin: 15px 15px 0 15px; }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" Runat="Server">

<form id="Form1" runat="server" class="sw-app-region">

    <div id="sw-seum-cloud-loading-mask"></div>
    <div id="sw-seum-cloud-loading">
        <div class="sw-seum-cloud-loading-indicator">
            <%= LoadingLabel %>
        </div>
    </div>

    <div id="errorcontent">
        <div style="margin: 10px; padding-left: 42px; background: url(/Orion/images/stop_32x32.gif) top left no-repeat;">

            <h1 class="sw-title-main"><%= ErrorTitle %></h1>

            <p style="max-width: 790px; overflow: auto;"><%=ErrorText.Replace("\n", "<br />")%></p>

            <% if (Agent != null) { %>
            <script type="text/javascript">
                $(function () {
                    $('.sw-cloud-show-password').toggle(
                            function () {
                                $('#sw-seum-cloud-password-field').html('<%= Agent.Password %>');
                                $('.sw-seum-cloud-show-password').html('<%= HideLabel %>');
                            },
                            function () {
                                $('#sw-seum-cloud-password-field').html("<%= new string('\u25CF', Agent.Password.Length) %>");
                                $('.sw-seum-cloud-show-password').html('<%= ShowLabel %>');
                            }
                    );
                    $('.sw-cloud-show-proxy-password').toggle(
                            function () {
                                $('#sw-seum-cloud-proxy-password-field').html('<%= Agent.ProxyPassword %>');
                                $('.sw-seum-cloud-show-proxy-password').html('<%= HideLabel %>');
                            },
                            function () {
                                $('#sw-seum-cloud-proxy-password-field').html("<%= new string('\u25CF', Agent.ProxyPassword.Length) %>");
                                $('.sw-seum-cloud-show-proxy-password').html('<%= ShowLabel %>');
                            }
                    );
                });
            </script>
            <div>
                <h2 class="sw-title-aux">Agent configuration</h2>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><%= AgentNameLabel %></td><td><%= Agent.Name %></td>
                    </tr><tr>
                        <td><%= AgentUrlLabel %></td><td><%= Agent.Url %></td>
                    </tr><tr>
                        <td><%= AgentPortLabel %></td><td><%= Agent.Port %></td>
                    </tr><tr>
                        <td><%= AgentPasswordLabel %></td><td><span id="sw-seum-cloud-password-field"><%= new string('\u25CF', Agent.Password.Length) %></span> &nbsp; &nbsp;<a href="javascript:void();" class="sw-seum-cloud-show-password"><%= ShowLabel %></a></td>
                    </tr>
                    <% if(Agent.UseProxy) { %>
                    <tr>
                        <td><%= AgentProxyLabel %></td><td><%= Agent.ProxyUrl %></td>
                    </tr>
                    <% if(Agent.UseProxyAuthentication) { %>
                    <tr>
                        <td><%= AgentProxyUserNameLabel %></td><td><%= Agent.ProxyUserName %></td>
                    </tr><tr>
                        <td><%= AgentProxyPasswordLabel %></td><td><span id="sw-seum-cloud-proxy-password-field"><%= new string('\u25CF', Agent.ProxyPassword.Length)%></span> &nbsp; &nbsp;<a href="javascript:void();" class="sw-seum-cloud-show-proxy-password"><%= ShowLabel %></a></td>
                    </tr>
                    <% } %>
                    <% } %>
                </table>
            </div>
            <% } %>

            <% if (AllowRetry) { %>
            <div class="sw-btn-bar">
                <a class="sw-btn sw-btn-resource" href="<%=RetryUrl%>">
	                <span class="sw-btn-c"><span class="sw-btn-t"><%= RetryButtonLabel %></span></span>
                </a>
            </div>
            <% } %>

            <p>
                <%= TroubleshootingLabel %>
            </p>
        </div>
    </div>
</form>
</asp:Content>
