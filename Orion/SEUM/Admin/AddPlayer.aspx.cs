﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Common.Helpers;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_Admin_AddPlayer : System.Web.UI.Page, IBypassAccessLimitation
{
    #region Text constants

    protected const string PageTitle = "Add Transaction Location";
    protected const string SelectMethodText = "Select method of adding Location";
    protected const string DownloadPlayerText = "Download Player";

    protected const string AddExistingLocationLabelTitle = "Add an existing location";
    protected const string AddExistingLocationLabelDetail = "A player must already be installed on the local machine or at a remote location to play the recording.";
    
    protected const string InstallLocationOnNetworkLabelTitle = "Install location on my network";
    protected const string InstallLocationOnNetworkLabelDetail = "A player will be deployed to the desired network location.";

    #endregion // Text constants

    #region Private properties

    private string AddExistingLocationUrl
    {
        get
        {
            return string.IsNullOrEmpty(_returnUrl)
                       ? "/Orion/SEUM/Admin/EditPlayer.aspx"
                       : String.Format("/Orion/SEUM/Admin/EditPlayer.aspx?{0}={1}", CommonParameterNames.ReturnUrl, UrlHelper.ToSafeUrlParameter(_returnUrl));
        }
    }

    private string InstallLocationUrl
    {
        get
        {
            return string.IsNullOrEmpty(_returnUrl)
                       ? "/Orion/SEUM/Admin/Deployment/Default.aspx"
                       : String.Format("/Orion/SEUM/Admin/Deployment/Default.aspx?{0}={1}", CommonParameterNames.ReturnUrl, UrlHelper.ToSafeUrlParameter(_returnUrl));
        }
    }

    #endregion // Private properties

    private string _returnUrl = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = PageTitle;

        if (!string.IsNullOrEmpty(Request[CommonParameterNames.ReturnUrl]))
            _returnUrl = UrlHelper.FromSafeUrlParameter(Request[CommonParameterNames.ReturnUrl]);

        ((Orion_SEUM_Admin_SEUMAdminPage)this.Master).HelpFragment = "";    // todo: helplink

    }

    protected void nextButton_OnClick(object sender, EventArgs e)
    {
        if (!SEUMProfile.AllowAdmin)
            return;

        // redirect to proper wizard/page
        if(existingLocation.Checked)
        {
            Response.Redirect(AddExistingLocationUrl);
        }
        else if (installLocation.Checked)
        {
            Response.Redirect(InstallLocationUrl);
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(_returnUrl))
            GoBack();
        else
            Response.Redirect(_returnUrl);
    }

    private void GoBack()
    {
        Response.Redirect("/Orion/SEUM/Admin/ManagePlayers.aspx");
    }
}