﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.SEUM.Web.Helpers;


public partial class Orion_SEUM_Admin_Add_Controls_TransactionSaveErrorsControl : System.Web.UI.UserControl
{
    private const string UnableToCreateTransactionErrorFormat = "Error creating transaction monitor '{0}':<br />{1}.";
    private const string UnableToCreateTransactionJobErrorFormat = "Error creating polling job for transaction monitor '{0}':<br />{1}.";
    private const string UnableToCreateTransactionsError = "Unable to create some monitors.";
    private const string UnableToCreateTransactionJobsError = "Unable to create some polling jobs.";
    private const string UnableToDeleteTransactionsError = "Unable to delete already created monitors.";
    private const string UnableToAssingObjectsErrorFormat = "Error assigning objects to transaction {0}:<br/> {1}.";
    private const string UnableToAssingObjectsError = "Unable to assign related objects.";
    private const string NoTransactionsCreatedError = "No monitors were saved. Please try again.";
    private const string SomeTransactionsWereCreatedError = "Some monitors were successfuly created and were removed from list below.";
    private const string FailedToSaveTransactionObjectsError = "All monitors were created but some objects failed to assign. Please try again.";
    private const string FixJobsForTransactionMessage = @"There were problems creating polling jobs for some transaction. Please check that 
    ""SolarWinds Job Engine"" service is running. To correctly setup polling jobs please restart ""SolarWinds Orion Module Engine"" service.";

    public SolarWinds.SEUM.Web.TransactionWizardWorkflow.TransactionsSaveResult TransactionsSaveResult { get; set; } 
 
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void DataBind()
    {
        if (!TransactionsSaveResult.IsSuccessful)
        {
            List<String> errors = new List<string>();

            if (TransactionsSaveResult.Errors.Count > 0)
            {
                errors.Add(UnableToCreateTransactionsError);
            }
            errors.AddRange(TransactionsSaveResult.Errors.Select(e => string.Format(
                                UnableToCreateTransactionErrorFormat,
                                NameHelper.GenerateTransactionFullName(e.Key.Recording.Name, e.Key.Agent.Name, null, null),
                                e.Value.Message)));

            if (TransactionsSaveResult.JobErrors.Count > 0)
            {
                errors.Add(UnableToCreateTransactionJobsError);
                errors.AddRange(TransactionsSaveResult.JobErrors.Select(e => string.Format(
                                UnableToCreateTransactionErrorFormat,
                                NameHelper.GenerateTransactionFullName(e.Key.Recording.Name, e.Key.Agent.Name, null, null),
                                e.Value.Message)));
                errors.Add(FixJobsForTransactionMessage);
            }

            if (TransactionsSaveResult.RelatedEntitiesErrors.Count > 0)
            {
                errors.Add(UnableToAssingObjectsError);
                errors.AddRange(TransactionsSaveResult.RelatedEntitiesErrors.Select(e => string.Format(
                                UnableToAssingObjectsErrorFormat,
                                NameHelper.GenerateTransactionFullName(e.Key.Recording.Name, e.Key.Agent.Name, null, null),
                                e.Value.Message)));
            }

            if (TransactionsSaveResult.CreatedTransactions.Count() > 0)
            {
                    
                if (TransactionsSaveResult.AreAllTransactionsCreated)
                {
                    errors.Add(FailedToSaveTransactionObjectsError);
                }
                else
                {
                    errors.Add(SomeTransactionsWereCreatedError);
                }
            }
            else
            {
                errors.Add(NoTransactionsCreatedError);
            }

            errorsRepeater.DataSource = errors;
            errorsRepeater.DataBind();
        }
    }
}