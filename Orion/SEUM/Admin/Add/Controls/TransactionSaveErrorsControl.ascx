﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionSaveErrorsControl.ascx.cs" Inherits="Orion_SEUM_Admin_Add_Controls_TransactionSaveErrorsControl" ClassName="SolarWinds.SEUM.Web.Controls.TransactionSaveErrorsControl" %>

<asp:Repeater runat="server" ID="errorsRepeater">
    <HeaderTemplate>
        <ul class="SEUM_ErrorsList">
    </HeaderTemplate>
    <ItemTemplate>
        <li><%# (string)Container.DataItem %></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>