﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Controls;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;

public partial class Orion_SEUM_Admin_Add_Relations : TransactionWizardPageBase, IBypassAccessLimitation
{
    protected const string Heading = "Transaction status troubleshooting";

    private const string SaveMonitorButtonSingularText = "Save monitor";
    private const string SaveMonitorButtonPlurarText = "Save monitors";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Workflow.CurrentStepType != TransactionWizardWorkflow.StepType.Relations)
            GotoFirstStep();

        imbgSave.Text = Workflow.Transactions.Count() > 1 ? SaveMonitorButtonPlurarText : SaveMonitorButtonSingularText;

        if (Page.IsPostBack)
        {
            SaveNodesSelection();

        }
        BindRelatedEntities();

        ((Orion_SEUM_Admin_Add_AddMonitorWizard)this.Master).HelpFragment = "OrionWPMPHAddTransactionStatusTroubleshooting";
    }

    private void BindRelatedEntities()
    {
        var transactionsIds = Workflow.Transactions.Select(t => t.TransactionId);
        relatedEntitiesRepeater.DataSource = Workflow.Transactions;
        relatedEntitiesRepeater.DataBind();
        
        TransactionsCount.InnerHtml = transactionsIds.Count().ToString();
    }

    private void SaveNodesSelection()
    {
        int index = 0;
        Workflow.TransactionsEntities = new Dictionary<Transaction, IEnumerable<string>>();
        Workflow.TransactionStepEntities = new Dictionary<TransactionStep, IEnumerable<string>>();
        foreach (Transaction transaction in Workflow.Transactions)
        {
            RepeaterItem item = relatedEntitiesRepeater.Items[index++];

            var dependenciesControl = (TransactionDependenciesControl)item.FindControl("transactionDependencies");
            dependenciesControl.Transaction = transaction;

            Workflow.TransactionsEntities[transaction] = dependenciesControl.TransactionDependencies;
            foreach (var step in transaction.Steps)
            {
                var dependencies = dependenciesControl.StepsDependencies != null && dependenciesControl.StepsDependencies.ContainsKey(step)
                    ? dependenciesControl.StepsDependencies[step]
                    : Enumerable.Empty<string>();
                Workflow.TransactionStepEntities[step] = dependencies;
            }
        }
    }

    protected override bool Next()
    {
        if (!SEUMProfile.AllowAdmin)
            return false;

        var result = Workflow.SaveTransactions(ShowRelatedEntities.Checked);
        TransactionSaveErrors.TransactionsSaveResult = result;
        TransactionSaveErrors.DataBind();

        return result.IsSuccessful;
    }

    protected void relatedEntitiesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemIndex < 0)
            return;
        Transaction transaction = e.Item.DataItem as Transaction;

        ImageButton expandButton = e.Item.FindControl("expandButton") as ImageButton;
        Label transactionLabel = e.Item.FindControl("transactionLabel") as Label;
        Panel detailsPanel = e.Item.FindControl("detailsPanel") as Panel;
        TransactionDependenciesControl dependenciesControl =
            e.Item.FindControl("transactionDependencies") as TransactionDependenciesControl;

        dependenciesControl.Transaction = transaction;
        dependenciesControl.TransactionDependencies = Workflow.TransactionsEntities != null && Workflow.TransactionsEntities.ContainsKey(transaction) 
            ? Workflow.TransactionsEntities[transaction] : Enumerable.Empty<string>();

        var stepsDependencies = new Dictionary<TransactionStep, IEnumerable<string>>();
        foreach (var step in transaction.Steps)
        {
            if (Workflow.TransactionStepEntities != null && Workflow.TransactionStepEntities.ContainsKey(step))
            {
                stepsDependencies[step] = Workflow.TransactionStepEntities[step];
            }
        }
        dependenciesControl.StepsDependencies = stepsDependencies;
        dependenciesControl.DataBind();

        transactionLabel.Text = NameHelper.GenerateTransactionFullName(transaction.Recording.Name, transaction.Agent.Name, null, null);
        expandButton.OnClientClick = string.Format("ToggleDetails('{0}','{1}'); return false;", expandButton.ClientID, detailsPanel.ClientID);
    }
}