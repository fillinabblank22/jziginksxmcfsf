<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Add/AddMonitorWizard.master" AutoEventWireup="true" CodeFile="Properties.aspx.cs" Inherits="Orion_SEUM_Admin_Add_Properties" %>
<%@ Register TagName="TransactionPropertiesEditor" TagPrefix="SEUM" Src="~/Orion/SEUM/Controls/TransactionPropertiesEditor.ascx" %>
<%@ Register TagName="TransactionSaveErrorsControl" TagPrefix="orion" Src="~/Orion/SEUM/Admin/Add/Controls/TransactionSaveErrorsControl.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <h2><%= Heading %></h2>

    <script type="text/javascript">
    //<![CDATA[
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }

        function ToggleDetails(buttonId, panelId, expandedFlagId) {
            var panel = $("#"+panelId);

            if (panel.hasClass("hidden")) {
                ExpandDetails(buttonId, panelId, expandedFlagId);
            } else {
                CollapseDetails(buttonId, panelId, expandedFlagId);
            }
        }

        function ExpandDetails(buttonId, panelId, expandedFlagId) {
            var panel = $("#" + panelId);
            var button = $("#" + buttonId);
            var expandedFlag = $("#" + expandedFlagId);

            if (panel.hasClass("hidden")) {
                panel.removeClass("hidden");
                button.attr("src", "/Orion/images/Button.Collapse.gif");
                expandedFlag.val('1');
            }
        }

        function CollapseDetails(buttonId, panelId, expandedFlagId) {
            var panel = $("#" + panelId);
            var button = $("#" + buttonId);
            var expandedFlag = $("#" + expandedFlagId);

            if (!panel.hasClass("hidden")) {
                panel.addClass("hidden");
                button.attr("src", "/Orion/images/Button.Expand.gif");
                expandedFlag.val('0');
            }
        }
    //]]>
    </script>
    <orion:TransactionSaveErrorsControl ID="TransactionSaveErrors" runat="server" />
    <br />
    <asp:Repeater runat="server" ID="transactionsRepeater">
        <ItemTemplate>
            <asp:Panel runat="server" ID="propertiesBlock" class="SEUM_TransactionPropertiesBlock">
                <asp:ImageButton ID="expandButton" runat="server" ImageUrl="~/Orion/images/Button.Collapse.gif" CausesValidation="false" />
                <asp:Label runat="server" ID="transactionLabel" CssClass="SEUM_TransactionName" />

                <asp:Panel runat="server" ID="detailsPanel" CssClass="SEUM_TransactionPropertiesBlockContent">
                    <div runat="server" ID="messageLabel" class="sw-suggestion" Visible="False"><span class="sw-suggestion-icon"></span></div>
                    <SEUM:TransactionPropertiesEditor runat="server" ID="transactionEditor" />
                </asp:Panel>
                <asp:HiddenField runat="server" ID="expandedFlagBox" Value="1" />
                <script type="text/javascript">
                //<![CDATA[
                    <asp:Literal runat="server" ID="jsToggleLiteral" />
                //]]>
                </script>
            </asp:Panel>
        </ItemTemplate>
    </asp:Repeater>

    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" runat="server" />
        <orion:LocalizableButton ID="imbgNext" OnClick="Next_Click" DisplayType="Primary" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>
</asp:Content>
