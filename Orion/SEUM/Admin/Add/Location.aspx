<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Add/AddMonitorWizard.master" AutoEventWireup="true" CodeFile="Location.aspx.cs" Inherits="Orion_SEUM_Admin_Add_Location" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Register TagPrefix="SEUM" TagName="AgentsGrid" Src="~/Orion/SEUM/Controls/AgentsGrid.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }
    </script>
    
    <h2><%: Heading %></h2>

    <p>
        <b><%= WhatIsText %></b>
        <%= DescriptionText %>
        <a href="<%= LearnMoreHelpUrl %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= LearnMoreText %></a>       
    </p>
    <div class="sw-suggestion SEUM_NotificationPanel">
        <span class="sw-suggestion-icon"></span> 
        <%= BulbText%> <a href="<%= CommonLinks.PlayerInstaller %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= DownloadPlayerText%></a>
    </div>

    <SEUM:AgentsGrid runat="server" ID="agentsGrid" IsEditGrid="false" />

    <div class="SEUM_WorkflowButtons">
        <% if(!Workflow.IsFirstStep) { %>
        <orion:LocalizableButton ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" runat="server" />
        <% } %>
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

    <% if (ShowValidationError) { %>
    <script type="text/javascript" defer="defer">
      //<![CDATA[
        $(document).ready(function () {
            Ext.Msg.show({
                title: '<%=NoAgentSelectedErrorTitle%>',
                msg: '<%=NoAgentSelectedErrorText%>',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        });
      //]]>
    </script>
    <% } %>
</asp:Content>
