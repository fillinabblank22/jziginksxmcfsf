using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Helpers;
using System.Web.UI.HtmlControls;

public partial class Orion_SEUM_Admin_Add_Properties : TransactionWizardPageBase, IBypassAccessLimitation
{
    public const string Heading = "Define properties for your monitor(s)";
    public const string TransactionNameFormat = "{0} from {1}";

    public const string UnsupportedAgentErrorFormat = "Transaction <b>{0}</b> can't be added to player <b>{1}</b>."
        + " Transaction requires Windows Vista, Windows Server 2008 or Windows 7 for playback. Player operating system is Windows Server 2003 or older.";
    public const string RDPEnableWarningFormat =
        "Transaction <b>{0}</b> requires an interactive RDP session for playback due to the technology used in this transaction. "
        + "RDP connections will be automatically enabled on player <b>{1}</b>. If player uses NLA authentication "
        + "for RDP connections, NLA authentication will be disabled. Once this transaction is assigned, a WPM user account "
        + "will be added to Remote Desktop Users group.";

    public const string SaveMonitorButtonSingularText = "Save monitor";
    public const string SaveMonitorButtonPlurarText = "Save monitors";
    public const string NextButtonText = "Next";

    private const string CollapsSessionKey = "SEUM_PropertiesCollapseState";

    private static readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Workflow.CurrentStepType != TransactionWizardWorkflow.StepType.Properties)
            GotoFirstStep();

        if (Workflow.IsLastStep)
        {
            imbgNext.Text = Workflow.Transactions.Count() > 1
                ? SaveMonitorButtonPlurarText
                : SaveMonitorButtonSingularText;
        }
        else
        {
            imbgNext.Text = NextButtonText;
        }

        ((Orion_SEUM_Admin_Add_AddMonitorWizard)this.Master).HelpFragment = "OrionWPMPHAddTransactionTransactionMonitor";

        if (!Page.IsPostBack)
        {
            RefreshTransactions();
        }
        else
        {
            SetupTransactionsEditors();
        }
    }

    protected override bool Next()
    {
        if (Workflow.IsLastStep)
        {
            var result = Workflow.SaveTransactions(false);
            TransactionSaveErrors.TransactionsSaveResult = result;
            TransactionSaveErrors.DataBind();

            return result.IsSuccessful;
        }
        return base.Next();
    }

    protected override void Cancel()
    {
        CollapsedTransactions.Clear();
    }

    private void transactionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemIndex < 0)
            return;

        Transaction transaction = e.Item.DataItem as Transaction;

        ImageButton expandButton = e.Item.FindControl("expandButton") as ImageButton;
        Label transactionLabel = e.Item.FindControl("transactionLabel") as Label;
        HtmlGenericControl messageLabel = e.Item.FindControl("messageLabel") as HtmlGenericControl;
        Panel detailsPanel = e.Item.FindControl("detailsPanel") as Panel;
        HiddenField expandedFlagBox = e.Item.FindControl("expandedFlagBox") as HiddenField;

        Orion_SEUM_Controls_TransactionPropertiesEditor editor = e.Item.FindControl("transactionEditor") as Orion_SEUM_Controls_TransactionPropertiesEditor;
        editor.EditorIndex = e.Item.ItemIndex;
        editor.Transaction = transaction;
        editor.ValidationFailedClientHandler = GetExpandClientCode(e.Item);
        editor.ExpandItem += editor_ExpandItem;

        transactionLabel.Text = NameHelper.GenerateTransactionFullName(transaction.Recording.Name, transaction.Agent.Name, null, null);
        expandButton.OnClientClick = string.Format("ToggleDetails('{0}','{1}', '{2}'); return false;", 
            expandButton.ClientID, detailsPanel.ClientID, expandedFlagBox.ClientID);

        if (!IsTransactionSupportedOnAgent(transaction))
        {
            editor.CssClass = "SEUM_Hidden";
            messageLabel.InnerHtml = messageLabel.InnerHtml + string.Format(UnsupportedAgentErrorFormat, transaction.Recording.Name, transaction.Agent.Name);
            messageLabel.Visible = true;
            messageLabel.Attributes["class"] = messageLabel.Attributes["class"] + " sw-suggestion-fail";
            transactionLabel.CssClass = transactionLabel.CssClass + " SEUM_ErrorText";
        }
        else if (WillRDPBeEnabled(transaction))
        {
            messageLabel.InnerHtml = messageLabel.InnerHtml + string.Format(RDPEnableWarningFormat, transaction.Recording.Name, transaction.Agent.Name);
            messageLabel.Visible = true;
            messageLabel.Attributes["class"] = messageLabel.Attributes["class"] + " sw-suggestion-warn";
        }
        else
        {
            // collapse panel if required
            if (CollapsedTransactions.Contains(e.Item.ItemIndex))
                CollapseItem(e.Item);
        }
    }

    private void editor_ExpandItem(object sender, EventArgs e)
    {
        Orion_SEUM_Controls_TransactionPropertiesEditor editor = sender as Orion_SEUM_Controls_TransactionPropertiesEditor;
        ExpandItem(transactionsRepeater.Items[editor.EditorIndex]);
    }

    private HashSet<int> CollapsedTransactions
    {
        get
        {
            if (Session[CollapsSessionKey] == null)
                Session[CollapsSessionKey] = new HashSet<int>();

            return (HashSet<int>)Session[CollapsSessionKey];
        }
    }

    private void ExpandItem(RepeaterItem item)
    {
        CollapsedTransactions.Remove(item.ItemIndex);

        ImageButton expandButton = item.FindControl("expandButton") as ImageButton;
        Panel detailsPanel = item.FindControl("detailsPanel") as Panel;
        Literal jsToggleLiteral = item.FindControl("jsToggleLiteral") as Literal;
        HiddenField expandedFlagBox = item.FindControl("expandedFlagBox") as HiddenField;

        jsToggleLiteral.Text = string.Format("ExpandDetails('{0}','{1}','{2}');",
                expandButton.ClientID, detailsPanel.ClientID, expandedFlagBox.ClientID);
    }

    private void CollapseItem(RepeaterItem item)
    {
        CollapsedTransactions.Add(item.ItemIndex);

        ImageButton expandButton = item.FindControl("expandButton") as ImageButton;
        Panel detailsPanel = item.FindControl("detailsPanel") as Panel;
        Literal jsToggleLiteral = item.FindControl("jsToggleLiteral") as Literal;
        HiddenField expandedFlagBox = item.FindControl("expandedFlagBox") as HiddenField;

        jsToggleLiteral.Text = string.Format("CollapseDetails('{0}','{1}','{2}');",
                expandButton.ClientID, detailsPanel.ClientID, expandedFlagBox.ClientID);
    }

    private string GetExpandClientCode(RepeaterItem item)
    {
        ImageButton expandButton = item.FindControl("expandButton") as ImageButton;
        Panel detailsPanel = item.FindControl("detailsPanel") as Panel;
        HiddenField expandedFlagBox = item.FindControl("expandedFlagBox") as HiddenField;

        return string.Format("ExpandDetails('{0}','{1}','{2}');",
                expandButton.ClientID, detailsPanel.ClientID, expandedFlagBox.ClientID);
    }

    private void RefreshTransactions()
    {
        transactionsRepeater.ItemDataBound += transactionsRepeater_ItemDataBound;
        transactionsRepeater.DataSource = Workflow.Transactions;
        transactionsRepeater.DataBind();
    }

    private void SetupTransactionsEditors()
    {
        int index = 0;
        foreach (Transaction transaction in Workflow.Transactions)
        {
            RepeaterItem item = transactionsRepeater.Items[index];

            Orion_SEUM_Controls_TransactionPropertiesEditor editor = item.FindControl("transactionEditor") as Orion_SEUM_Controls_TransactionPropertiesEditor;
            editor.Transaction = transaction;
            editor.EditorIndex = index;
            editor.ValidationFailedClientHandler = GetExpandClientCode(item);

            HiddenField expandedFlagBox = item.FindControl("expandedFlagBox") as HiddenField;

            if (expandedFlagBox.Value == "1")
                ExpandItem(item);
            else
                CollapseItem(item);

            Panel propertiesBlock = item.FindControl("propertiesBlock") as Panel;
            propertiesBlock.Visible = !Workflow.SavedTransactions.Contains(transaction);

            index++;
        }
    }

    private bool IsTransactionSupportedOnAgent(Transaction transaction)
    {
        // check if this transaction is supported on target agent
        if (transaction.Recording.ContainsRawActions)
        {
            return transaction.Agent.SupportsRawActions;
        }

        return true;
    }

    private bool WillRDPBeEnabled(Transaction transaction)
    {
        // check if transaction reaquires RDP and agent has RDP disabled
        return transaction.Recording.RequiresInteractiveSession && !transaction.Agent.RDPEnabled;
    }
}