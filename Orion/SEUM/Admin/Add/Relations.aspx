﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Add/AddMonitorWizard.master" AutoEventWireup="true" CodeFile="Relations.aspx.cs" Inherits="Orion_SEUM_Admin_Add_Relations" %>
<%@ Register Src="~/Orion/SEUM/Controls/TransactionDependenciesControl.ascx" TagName="TransactionDependenciesControl" TagPrefix="orion" %>
<%@ Register Src="~/Orion/SEUM/Controls/NetObjectPickerDialog.ascx" TagName="NetObjectPickerDialog" TagPrefix="orion" %>
<%@ Register Src="~/Orion/SEUM/Admin/Add/Controls/TransactionSaveErrorsControl.ascx" TagName="TransactionSaveErrorsControl" TagPrefix="orion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="Include3" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include2" runat="server" File="Admin/Containers/Containers.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }

        function ToggleDetails(buttonId, panelId) {
            var panel = $("#" + panelId);
            var button = $("#" + buttonId);

            if (panel.hasClass("hidden")) {
                panel.slideDown("fast");
                button.attr("src", "/Orion/images/Button.Collapse.gif");
            } else {
                panel.slideUp("fast");
                button.attr("src", "/Orion/images/Button.Expand.gif");
            }
        }

        function SetSelectionVisibility() {
            $("#SEUM_RelatedEntitiesSelection")[$("#<%=HideRelatedEntities.ClientID%>").is(":checked") ? "addClass" : "removeClass"]("hidden");
        }

        $(document).ready(function() {
            $("#<%=HideRelatedEntities.ClientID%>").live("change", SetSelectionVisibility);
            $("#<%=ShowRelatedEntities.ClientID%>").live("change", SetSelectionVisibility);
        });
    </script>
    
    <h2><%= Heading %></h2>

    <orion:TransactionSaveErrorsControl ID="TransactionSaveErrors" runat="server" />
    <orion:NetObjectPickerDialog runat="server" /> 
    <div>
        <p>Link existing Nodes and Applications related to Transaction for better troubleshooting. For example, a warning status of Transaction could mean that database 
            application of your monitored Website has critical issues. By linking this database application you will see it's status right in details of transaction.
        </p>
    </div>
    <div id="SEUM_RelatedEntitiesSelection_Radios">
        <asp:RadioButton runat="server" ID="HideRelatedEntities" GroupName="DisplayRelatedEntities" Checked="true" />
        <label for="<%= HideRelatedEntities.ClientID%>">
            Don't use any linking of Nodes and Application for transaction status troubleshooting <b>(Recomended for evaluation)</b>
        </label>
        <br />
        <asp:RadioButton runat="server" ID="ShowRelatedEntities" GroupName="DisplayRelatedEntities" Checked="false" />
        <label for="<%= ShowRelatedEntities.ClientID%>">
            Improve transaction troubleshooting by associating nodes or applications to this web transaction <b>(Advanced)</b>
        </label>
    </div>
    <div id="SEUM_RelatedEntitiesSelection" class="hidden">
        <h3 id="SEUM_RelatedEntitiesSelection_TransactionsLabel">Created transactions: <span runat="server" id="TransactionsCount"></span>.</h3>
        <div id="SEUM_RelatedEntitiesSelection_Description">
            For each created transaction below you can select Nodes and Applications
        </div>
        <asp:Repeater runat="server" ID="relatedEntitiesRepeater" OnItemDataBound="relatedEntitiesRepeater_ItemDataBound">
            <ItemTemplate>
                <asp:Panel runat="server" ID="propertiesBlock" CssClass="SEUM_RelatedEntitiesSelecion_Block">
                    <asp:ImageButton ID="expandButton" runat="server" ImageUrl="~/Orion/images/Button.Collapse.gif" CausesValidation="false" />
                    <asp:Image runat="server" ImageUrl="~/Orion/SEUM/images/Icon.Recording.gif" /> 
                    <asp:Label runat="server" ID="transactionLabel" CssClass="SEUM_TransactionName" />
                    <asp:Panel runat="server" ID="detailsPanel">
                        <orion:TransactionDependenciesControl runat="server" ID="transactionDependencies" />
                    </asp:Panel>
                </asp:Panel>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton ID="LocalizableButton1" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" runat="server" />
        <orion:LocalizableButton ID="imbgSave" OnClick="Next_Click" LocalizedText="CustomText" OnClientClick="return SEUMDisableInDemo();" DisplayType="Primary" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="LocalizableButton2" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

</asp:Content>
