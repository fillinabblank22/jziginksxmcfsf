using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Admin_Add_Default : TransactionWizardPageBase, IBypassAccessLimitation
{
    public const string HeadingFormat = "Choose the recordings you would like to monitor from {0}";
    public const string HeadingShort = "Choose the recordings you would like to monitor";
    public const string DescriptionText = "If your recording is not listed below, you may need to import the file";
    public const string LearnMoreText = "Learn more";

    public const string RecorderDescriptionText = "<b>You can create recordings with the WPM Recorder.</b>";
    protected const string RecorderDescriptionTextInfo = "To open the recorder, go to the Start menu, type WPM, and select WPM Recorder.";
    protected const string DownloadNewRecorderText = "Download recorder";

    public const string NoRecordingSelectedErrorTitle = "No recording selected";
    public const string NoRecordingSelectedErrorText = "You have to select a recording for this monitor.";

    protected void Page_Load(object sender, EventArgs e)
    {
        ShowValidationError = false;

        ((Orion_SEUM_Admin_Add_AddMonitorWizard)this.Master).HelpFragment = "OrionWPMPHAddTransactionRecording";

        if(Request["recordings"] != null)
        {
            string recordingsString = Request["recordings"];
            string[] parts = recordingsString.Split(new[] {','});
            List<Int32> recordings = new List<int>(parts.Length);
            int recordingId = 0;

            foreach (string part in parts)
            {
                if(Int32.TryParse(part, out recordingId))
                {
                    recordings.Add(recordingId);
                }
            }

            Workflow.RecordingIds = recordings;

            if(Workflow.RecordingIds.Any()) // were given recordings valid? if yes then configure the wizard and redirect to first step
            {
                Workflow.DisableStep("Recording");
                GotoFirstStep();
            }
        }

        if (Request["agents"] != null)
        {
            string agentsString = Request["agents"];
            string[] parts = agentsString.Split(new[] { ',' });
            List<Int32> agents = new List<int>(parts.Length);
            int agentId = 0;

            foreach (string part in parts)
            {
                if (Int32.TryParse(part, out agentId))
                {
                    agents.Add(agentId);
                }
            }

            Workflow.AgentsIds = agents;

            if (Workflow.AgentsIds.Any()) // were given agents valid? if yes then configure the wizard and redirect to first step
            {
                Workflow.DisableStep("Location");
                GotoFirstStep();
            }
        }

        if(Workflow.AgentsIds.Any())    // if there are already some agents selected, than disable recordings which are already deployed on that agents
        {
            RecordingDAL recordingDal = new RecordingDAL();
            recordingsGrid.ExcludedRecordingsIds = recordingDal.GetRecordingsHavingAllAgents(Workflow.AgentsIds);

            // if all agents does not support raw actions, exclude recording with raw actions
            if (Workflow.Agents.Count() > 0 && Workflow.Agents.All(a => !a.SupportsRawActions))
            {
                ModelsDAL modelsDal = new ModelsDAL();
                IEnumerable<Recording> recordings = modelsDal.GetAllRecordingsModels();
                recordingsGrid.UnsupportedRecordingsIds = recordings.Where(r => r.ContainsRawActions).Select(r => r.RecordingId);
            }
        }

        if (Page.IsPostBack)
        {
            Workflow.RecordingIds = recordingsGrid.SelectedRecordings;
        }
        else
        {
            recordingsGrid.SelectedRecordings = Workflow.RecordingIds;
        }
    }

    protected override bool ValidateUserInput()
    {
        if (!recordingsGrid.SelectedRecordings.Any() || recordingsGrid.SelectedRecordings.First() == 0)
        {
            ShowValidationError = true;
            return false;
        }

        return true;
    }

    protected bool ShowValidationError
    {
        get;
        set;
    }

    protected string Heading
    {
        get
        {
            if (Workflow.AgentsIds.Count() == 1)
            {
                ModelsDAL dal = new ModelsDAL();
                Agent agent = dal.GetAgentModel(Workflow.AgentsIds.First());

                return (agent != null) ? string.Format(HeadingFormat, UIHelper.Escape(agent.Name)) : HeadingShort;
            }
            else
            {
                return HeadingShort;
            }
        }
    }

    protected string LearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsImportingRecordings");
        }
    }
}