using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;

public partial class Orion_SEUM_Admin_Add_Location : TransactionWizardPageBase, IBypassAccessLimitation
{
    public const string HeadingFormat = "Select the playback locations for {0}";
    public const string HeadingShort = "Select the playback locations";
    protected const string WhatIsText = "What is a playback location?";
    protected const string BulbText = "Turn any Microsoft Windows computer into a playback location by installing the WPM Player.";
    protected const string DescriptionText = "Playback locations allow you to specify different computer systems and networks from which WPM can monitor the performance of a recording.";
    public const string LearnMoreText = "Learn more";
    protected const string DownloadPlayerText = "Download player";
    public const string NoAgentSelectedErrorTitle = "No location selected";
    public const string NoAgentSelectedErrorText = "You have to select at least one location for this monitor.";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Workflow.CurrentStepType != TransactionWizardWorkflow.StepType.Location)
            GotoFirstStep();

        ShowValidationError = false;

        ((Orion_SEUM_Admin_Add_AddMonitorWizard)this.Master).HelpFragment = "OrionWPMPHAddTransactionLocation";

        // get already assigned agents and remove them from grid
        RecordingDAL dal = new RecordingDAL();
        agentsGrid.ExcludedAgentsIds = dal.GetAgentsHavingAllRecordings(Workflow.RecordingIds);
        if(Workflow.RecordingIds.Any())
        {
            agentsGrid.ReturnUrl = UrlHelper.ToSafeUrlParameter(Request.Url.ToString());
        }

        // if all recordings contain raw actions exclude agents that can't play raw actions
        if (Workflow.Recordings.Count() > 0 && Workflow.Recordings.All(r => r.ContainsRawActions))
        {
            ModelsDAL modelsDal = new ModelsDAL();
            IEnumerable<Agent> agents = modelsDal.GetAllAgentsModels();
            agentsGrid.UnsupportedAgentsIds = agents.Where(a => !a.SupportsRawActions)
                        .Select(a => a.AgentId);
        }

        if (Page.IsPostBack)
        {
            Workflow.AgentsIds = agentsGrid.SelectedAgents;
        }
        else
        {
            agentsGrid.SelectedAgents = Workflow.AgentsIds;
        }
    }

    protected override bool ValidateUserInput()
    {
        if (!agentsGrid.SelectedAgents.Any() || agentsGrid.SelectedAgents.First() == 0)
        {
            ShowValidationError = true;
            return false;
        }

        return true;
    }

    protected bool ShowValidationError
    {
        get;
        set;
    }

    protected string Heading
    {
        get
        {
            if (Workflow.RecordingIds.Count() == 1)
            {
                ModelsDAL dal = new ModelsDAL();
                Recording recording = dal.GetRecordingModel(Workflow.RecordingIds.First());

                return (recording != null) ? string.Format(HeadingFormat, UIHelper.Escape(recording.Name)) : HeadingShort;
            }
            else
            {
                return HeadingShort;
            }
        }
    }

    protected string LearnMoreHelpUrl
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsPlaybackLocations");
        }
    }
}