﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SEUM/Admin/Add/AddMonitorWizard.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_SEUM_Admin_Add_Default" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<%@ Register TagPrefix="SEUM" TagName="RecordingsGrid" Src="~/Orion/SEUM/Controls/RecordingsGrid.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <script type="text/javascript">
        function ConfirmCancel(button) {
            return SynchronousConfirm(button, '<%=ConfirmCancelTitle%>', '<%=ConfirmCancelText%>', Ext.MessageBox.WARNING);
        }
    </script>

    <h2><%= Heading %></h2>
    <p>
        <%= DescriptionText %>
        <a href="<%= LearnMoreHelpUrl %>" class="SEUM_HelpLink" target="_blank">&#0187; <%= LearnMoreText %></a>
    </p>
    <div class="sw-suggestion sw-suggestion-info SEUM_NotificationPanel ManageTransactionMonitorsNotificationPanel">
        <span class="sw-suggestion-icon"></span>
        <%= RecorderDescriptionText %>
        <br/>
        <span class="info"><%= RecorderDescriptionTextInfo %> </span>
        <br/>
        <a href="<%= CommonLinks.ChromiumRecorderInstaller %>" class="SEUM_HelpLinkButton" target="_blank"><%= DownloadNewRecorderText %></a>
    </div>

    <SEUM:RecordingsGrid runat="server" ID="recordingsGrid" IsEditGrid="false" UseRadioList="false" />

    <div class="SEUM_WorkflowButtons">
        <orion:LocalizableButton ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" runat="server" />
        &nbsp;
        <orion:LocalizableButton ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClientClick="return ConfirmCancel(this);" runat="server" />
    </div>

    <% if (ShowValidationError) { %>
    <script type="text/javascript">
      //<![CDATA[
        $(document).ready(function () {
            Ext.Msg.show({
                title: '<%=NoRecordingSelectedErrorTitle%>',
                msg: '<%=NoRecordingSelectedErrorText%>',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        });
      //]]>
    </script>
    <% } %>
</asp:Content>
