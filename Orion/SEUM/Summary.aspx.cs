using System;
using SolarWinds.Orion.Web.UI;

public partial class Orion_SEUM_Summary : OrionView
{
    public override string ViewType
    {
        get
        {
            return "TransactionsSummary";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_SEUM_SEUMView)this.Master).HelpFragment = "OrionWPMPHTransactionSummary";
    }
}
