﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.UnManageTransactionDialog = function () {
    var unmanageTitleSingle = "Unmanage Monitor";
    var unmanageTitleMultiple = "Unmanage Transaction Monitors";
    var unmanageText = "OK";
    var cancelText = "Cancel";
    var unmanageErrorTitle = "Unmanage Transaction Error";
    var unmanageWrongTimeErrorMessage = "The End Time must come after the Start Time. Check the Date and Time fields.";
    var unmanagingMonitorsText = "Unmanaging Monitors ...";

    var unmanageWindow;
    var initialized = false;

    var unmanageWindowClientId;
    var unmanageFromClientId;
    var unmanageUntilClientId;

    var unmanageBeforeCallback = function () { };
    var unmanageAfterCallback = function () { };

    var itemsToUnmanage = new Array();

    var unmanageFrom = null;
    var unmanageUntil = null;

    UnmanageMonitors = function (ids, from, until, onSuccess) {
        var waitMsg = Ext.Msg.wait(unmanagingMonitorsText);

        ORION.callWebService("/Orion/SEUM/Services/MonitorsGrid.asmx",
                        "DisableMonitors", { monitorsIds: ids, from: from, until: until },
                        function (result) {
                            waitMsg.hide();
                            onSuccess();
                        });
    };

    InitUnmanageWindow = function () {
        var $unmanageWindow = $('#' + unmanageWindowClientId);
        unmanageWindow = new Ext.Window({
            title: itemsToUnmanage.length > 1 ? unmanageTitleMultiple : unmanageTitleSingle,
            width: 550,
            height: 278,
            closable: false,
            modal: true,
            resizable: false,
            cls: 'SEUM_UnmanageWindow',

            items: [
                    new Ext.Panel({
                        applyTo: $unmanageWindow[0],
                        border: false
                    })
                ],

            buttons: [
                {
                    text: unmanageText,
                    disabled: false,
                    handler: function () {
                        unmanageBeforeCallback();

                        if ($('#resetScheduledWindows').is(':checked')) {
                            UnmanageMonitors(itemsToUnmanage, null, null, unmanageAfterCallback);
                            unmanageWindow.hide();
                            return;
                        }

                        var startDate = $('#' + unmanageFromClientId).orionGetDate();
                        var endDate = $('#' + unmanageUntilClientId).orionGetDate();

                        if (endDate <= startDate) {
                            Ext.Msg.show({
                                title: unmanageErrorTitle,
                                msg: unmanageWrongTimeErrorMessage,
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        UnmanageMonitors(itemsToUnmanage, startDate, endDate, unmanageAfterCallback);

                        unmanageWindow.hide();
                    }
                },
                {
                    text: cancelText,
                    handler: function () {
                        unmanageWindow.hide();
                    }
                }],
            listeners: {
                beforeshow: function () {
                    $unmanageWindow.show();
                    var now = new Date();
                    $unmanageWindow.find('#' + unmanageFromClientId).orionSetDate(now);
                    now = new Date(now.getTime() + 24 * 60 * 60 * 1000);
                    $unmanageWindow.find('#' + unmanageUntilClientId).orionSetDate(now);
                    $unmanageWindow.find('#setUnmanageTime').attr('checked', true);
                    $unmanageWindow.find('#SEUM_UnmanageTimesTable').show();
                    $unmanageWindow.find('.SEUM_UnmanageClearWrapper').hide();
                    $unmanageWindow.find('#SEUM_UnmanageClearTable').hide();

                    if (itemsToUnmanage.length == 1) {
                        if (unmanageFrom != null && unmanageUntil != null) {
                            $unmanageWindow.find('.SEUM_FromPlaceholder').text(unmanageFrom);
                            $unmanageWindow.find('.SEUM_ToPlaceholder').text(unmanageUntil);
                            $unmanageWindow.find('#SEUM_UnmanageClearTable').show();
                        }
                    }
                }
            }
        });
    };

    return {
        ShowUnmanageWindow: function (items, from, until) {
            if (items && items.constructor != Array) {
                itemsToUnmanage = new Array();
                itemsToUnmanage.push(items);
            } else {
                itemsToUnmanage = items;
            }

            unmanageFrom = null;
            unmanageUntil = null;

            if (from)
                unmanageFrom = from;
            if (until)
                unmanageUntil = until;

            unmanageWindow.show().center();
        },
        SetUnmanageBeforeCallback: function (callback) {
            unmanageBeforeCallback = callback;
        },
        SetUnmanageAfterCallback: function (callback) {
            unmanageAfterCallback = callback;
        },
        SetUnmanageWindowClientId: function (value) {
            unmanageWindowClientId = value;
        },
        SetUnmanageFromClientId: function (value) {
            unmanageFromClientId = value;
        },
        SetUnmanageUntilClientId: function (value) {
            unmanageUntilClientId = value;
        },
        init: function () {
            if (initialized)
                return;

            initialized = true;

            InitUnmanageWindow();
        }
    };
} ();

Ext.onReady(SW.SEUM.UnManageTransactionDialog.init, SW.SEUM.UnManageTransactionDialog);