﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.DeploymentSettingsGrid = function () {
    var errorTitle = "Error";

    var initialized;

    var grid;
    var dataStore;

    var playerNameEditor;
    var playerPortEditor;
    var playerPasswordEditor;
    var pollerComboBoxEditor;

    var pollerList;
    var pollerStore;

    var loadingMask = null;

    UpdateLocationSettings = function (hostname, playerName, pollerId, port, playerPassword, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/DeploymentWizard.asmx",
                         "UpdateLocationSettings", { hostName: hostname, playerName: playerName, pollerId: pollerId, port: port, playerPassword: playerPassword },
                         function (result) {
                             onSuccess();
                         });
    };

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: 'Loading Locations ...'
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {};
        grid.store.baseParams = {};
        grid.store.on('load', function () {
            loadingMask.hide();
        });
        grid.store.load();
    };

    function renderPoller(value, meta, record) {
        meta.css += ' sw-seum-combobox-cell';
      return Ext.util.Format.htmlEncode(pollerStore.getById(value).data.Value, meta, record);
    }

    function renderPassword(value, meta, record) {
        return renderEditableCell(String.format("<b>{0}</b>", value != null ? value.replace(/./gi, '*') : '') + '&nbsp;', meta, record);
    }

    function renderEditableCell(value, meta, record) {
        meta.css += ' sw-seum-editable-cell';
        return renderDefault(value, meta, record);
    }

    function renderDefault(value, meta, record) {
      return value;
    }

    InitLayout = function () {
        var panel = new Ext.Container({
            renderTo: 'DeploymentSettingsGrid',
            height: SEUM_GetManagementGridHeight(),
            layout: 'border',
            items: [grid]
        });
        Ext.EventManager.onWindowResize(function () {
            panel.setHeight(SEUM_GetManagementGridHeight());
            panel.doLayout();
        }, panel);
    };

    InitGrid = function () {
        dataStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/DeploymentWizard.asmx/GetLocationsForSettingsGrid",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'LocationName', mapping: 1 },
                                { name: 'PlayerName', mapping: 2 },
                                { name: 'PollerId', mapping: 3 },
                                { name: 'PlayerPort', mapping: 4 },
                                { name: 'PlayerPassword', mapping: 5 }
                            ], "LocationName");

        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        dataStore.addListener("update", function (store, record, operation) {
            if (operation == Ext.data.Record.EDIT) {
                UpdateLocationSettings(
                    record.data.ID,
                    record.data.PlayerName,
                    record.data.PollerId,
                    record.data.PlayerPort,
                    record.data.PlayerPassword,
                    function () {
                    });
            }
        });

        playerNameEditor = new Ext.form.TextField({
            allowBlank: false
        });

        playerPortEditor = new Ext.form.NumberField({
            allowBlank: false,
            allowNegative: false,
            minValue: 1,
            maxValue: 65535,
            allowDecimals: false,
            style: 'text-align: left'
        });

        playerPasswordEditor = new Ext.form.TextField({
            allowBlank: true,
            inputType: 'password'
        });

        pollerStore = new Ext.data.JsonStore({
            fields: ['Key', 'Value'],
            data: pollerList,
            idProperty: 'Key'
        });

        pollerComboBoxEditor = new Ext.form.ComboBox({
            store: pollerStore,
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            editable: false
        });

        grid = new Ext.grid.EditorGridPanel({

            store: dataStore,

            columns: [
                    { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                    { header: 'Location Name', width: 220, hideable: false, sortable: true, dataIndex: 'LocationName', renderer: renderDefault },
                    { header: 'Player Name', width: 220, hideable: false, sortable: false, dataIndex: 'PlayerName', renderer: renderEditableCell, editor: playerNameEditor },
                    { header: 'Poller', width: 150, hideable: false, hidden: true, sortable: false, dataIndex: 'PollerId', renderer: renderPoller, editor: pollerComboBoxEditor },
                    { header: 'Player Port', width: 80, hideable: false, hidden: true, sortable: false, dataIndex: 'PlayerPort', renderer: renderEditableCell, editor: playerPortEditor },
                    { header: 'Player Password', width: 150, hideable: false, hidden: true, sortable: false, dataIndex: 'PlayerPassword', renderer: renderPassword, editor: playerPasswordEditor },
		            { id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 }
                ],

            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            clicksToEdit: 1,
            autoExpandColumn: 'filler',
            viewConfig: {
                emptyText: 'There are no locations.'
            }
        });

        // hide poller column if only one poller is available
        grid.getColumnModel().setHidden(3, pollerStore.getCount() == 1);

    };

    ORION.prefix = "SEUM_DeploymentSettingsGrid_";

    return {
        SetPollerList: function (pollerlist) {
            pollerList = pollerlist;
        },
        ToggleAdvancedMode: function (advanced) {
            this.init();

            grid.getColumnModel().setHidden(3, !advanced || pollerStore.getCount() == 1);
            grid.getColumnModel().setHidden(4, !advanced);
            grid.getColumnModel().setHidden(5, !advanced);
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            InitGrid();
            InitLayout();

            RefreshObjects();
        }
    };
} ();

Ext.onReady(SW.SEUM.DeploymentSettingsGrid.init, SW.SEUM.DeploymentSettingsGrid);