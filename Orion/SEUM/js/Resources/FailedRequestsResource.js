﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');
Ext.namespace('SW.SEUM.Resources');

SW.SEUM.Resources.FailedRequestsResource = function () {

    var params = Array();
    var loadingRowTemplate = '<tr><td colspan="3" class="sw-seum-loading"><img src="/Orion/images/AJAX-loader.gif"> Loading &hellip;</td></tr>'
    var rowTemplateFormat = '<tr><td class="Property SEUM_Property {6}"><img src="{1}" alt="{0}"/> <a href="{2}" target="_blank" title="{2}">{3}</a></td>'
        + '<td class="Property SEUM_Property {6}">{0}</td><td class="Property SEUM_Property {6}"><a href="{7}" target="_blank"><img src="{5}" alt="{0}"/> {4}</a></td></tr>';

    LoadData = function (resourceId, page, onSuccess) {
        // remove all rows except the first one
        $('#' + params[resourceId].requestsTableId).find("tr:gt(0)").remove();
        $('#' + params[resourceId].requestsTableId).append(loadingRowTemplate);

        ORION.callWebService("/Orion/SEUM/Services/StepResources.asmx",
                         "GetStepRequestsByCategoryPageable", { transactionStepId: params[resourceId].transactionStepId, page: page, pageSize: params[resourceId].currentPageSize, categories: params[resourceId].categories, filter: params[resourceId].filter },
                         function (result) {
                             onSuccess(result);
                         });
    };

    GetLastPageNumber = function (resourceId) {
        return parseInt(params[resourceId].totalRecords / params[resourceId].currentPageSize) + (params[resourceId].totalRecords % params[resourceId].currentPageSize == 0 ? 0 : 1);
    };

    FirstClicked = function (resourceId) {
        // if we are on the first page do nothing
        if (params[resourceId].currentPage == 1)
            return;

        params[resourceId].currentPage = 1;
        UpdateTable(resourceId);
    };

    PrevClicked = function (resourceId) {
        // if we are on the first page do nothing
        if (params[resourceId].currentPage == 1)
            return;

        params[resourceId].currentPage--;
        UpdateTable(resourceId);
    };

    NextClicked = function (resourceId) {
        // if we are on the last page do nothing
        if (params[resourceId].currentPage >= GetLastPageNumber(resourceId))
            return;

        params[resourceId].currentPage++;
        UpdateTable(resourceId);
    };

    LastClicked = function (resourceId) {
        // if we are on the last page do nothing
        if (params[resourceId].currentPage == GetLastPageNumber(resourceId))
            return;

        params[resourceId].currentPage = GetLastPageNumber(resourceId);
        UpdateTable(resourceId);
    };

    ViewAllClicked = function (resourceId) {
        params[resourceId].currentPage = 1;
        params[resourceId].currentPageSize = params[resourceId].totalRecords;
        UpdateTable(resourceId);
    };

    UpdatePagingPanel = function (resourceId) {
        if (params[resourceId].totalRecords == 0) {
            $('#' + params[resourceId].pagingTableId).remove();
            return;
        }

        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-page-box').val(params[resourceId].currentPage);
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-page-size').val(params[resourceId].currentPageSize);
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-of').text(GetLastPageNumber(resourceId));

        var canGoBack = true;
        var canGoForward = true;

        if (params[resourceId].currentPage == 1) {
            canGoBack = false;
        }
        if (params[resourceId].currentPage >= GetLastPageNumber(resourceId)) {
            canGoForward = false;
        }

        // attach event handlers to paging buttons
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-first span').removeClass();
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-first span').addClass('sw-seum-paging-first-' + (canGoBack ? 'enabled' : 'disabled'));
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-prev span').removeClass();
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-prev span').addClass('sw-seum-paging-prev-' + (canGoBack ? 'enabled' : 'disabled'));
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-next span').removeClass();
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-next span').addClass('sw-seum-paging-next-' + (canGoForward ? 'enabled' : 'disabled'));
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-last span').removeClass();
        $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-last span').addClass('sw-seum-paging-last-' + (canGoForward ? 'enabled' : 'disabled'));
    };

    UpdateTable = function (resourceId) {
        LoadData(resourceId, params[resourceId].currentPage, function (result) {

            params[resourceId].totalRecords = result.TotalRows;

            var odd = true;
            var rows = '';
            Ext.each(result.DataTable.Rows, function (record) {
                var row = String.format(rowTemplateFormat, record[0], record[1], record[2], record[3], record[4], record[5], odd ? '' : 'ZebraStripe ', params[resourceId].statusCodeHelpUrl);
                rows += row;
                odd = !odd;
            });

            // remove all rows except the first one
            $('#' + params[resourceId].requestsTableId).find("tr:gt(0)").remove();
            $('#' + params[resourceId].requestsTableId).hide().append(rows).fadeIn();

            UpdatePagingPanel(resourceId);
        });
    };

    return {
        Init: function (resourceId, stepId, categories, filter, pageSize, statusCodeHelpUrl) {
            if(typeof params[resourceId] == 'undefined') {
                params[resourceId] = {
                    transactionStepId: stepId,
                    currentPage: 1,
                    currentPageSize: pageSize,
                    categories: categories,
                    filter: filter,
                    requestsTableId: 'RequestsTable' + resourceId,
                    pagingTableId: 'RequestsTablePaging' + resourceId,
                    totalRecords: 0,
                    statusCodeHelpUrl: statusCodeHelpUrl
                }
            } else {
                return;
            }

            UpdateTable(resourceId);

            // attach event handlers to paging buttons
            $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-first').click(function () { FirstClicked(resourceId); });
            $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-prev').click(function () { PrevClicked(resourceId); });
            $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-next').click(function () { NextClicked(resourceId); });
            $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-last').click(function () { LastClicked(resourceId); });
            $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-all').click(function () { ViewAllClicked(resourceId); });

            $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-page-box').change(function () {
                if ($(this).val() <= 0 || isNaN($(this).val())) {
                    $(this).val(params[resourceId].currentPage);
                    alert('Page number must be greater than zero.');
                    return;
                }
                var page = parseInt($(this).val());
                params[resourceId].currentPage = page > GetLastPageNumber(resourceId) ? GetLastPageNumber(resourceId) : page;

                UpdateTable(resourceId);
            });

            $('#' + params[resourceId].pagingTableId + ' .sw-seum-paging-page-size').change(function () {
                if ($(this).val() <= 0 || isNaN($(this).val())) {
                    $(this).val(params[resourceId].currentPageSize);
                    alert('Page size must be number greater than zero.');
                    return;
                }
                params[resourceId].currentPageSize = parseInt($(this).val());

                if(params[resourceId].currentPage > GetLastPageNumber(resourceId))
                    params[resourceId].currentPage = GetLastPageNumber(resourceId);

                UpdateTable(resourceId);
            });
        }
    };
} ();
