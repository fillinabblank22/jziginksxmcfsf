﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');
Ext.namespace('SW.SEUM.EditPlayer');

SW.SEUM.EditPlayer = function () {
    var diagnosticsRunningMessage = "Generating new diagnostics. Please wait...";
    var noDiagnosticsMessage = "No diagnostics available";
    var diagnosticsAvailableFormat = "From {0}";
    var downloadText = "Download";
    var saveWithFailedTestMessageTitle = "Save current values";
    var saveWithFailedTestMessageFormat = "Location test failed with error: \"{0}\".<br />Please verify if Player settings correspond to these changes.<br />Do you still want to save current settings?";
    var failedTestPollerDownMessageFormat = "Location test failed with error: \"{0}\".<br />Please verify if Additional Poller is up and ready. Player settings couldn't be updated.";
    var updateRemoteSettingsMessageTitle = "Update remote player settings";
    var updateRemoteSettingsWithFailedTestMessageFormat = "Location test failed with error: \"{0}\".<br />Remote player settings couldn't be updated.";
    var testingMessage = "Verifying connection details ...";
    var noAgentAvailableText = "No player available";
    var loadingAgentsText = "Loading players list ...";

    var useProxyCheckboxId;
    var passiveRadioId;
    var activeRadioId;
    var agentIdBoxId;
    var agentDnsNameBoxId;
    var agentIpBoxId;
    var nameBoxId;
    var passwordPassiveBoxId;
    var portBoxId;
    var proxyUrlBoxId;
    var useProxyAuthenticationCheckboxId;
    var proxyUserNameBoxId;
    var proxyPasswordBoxId;
    var guidBoxId;
    var originalGuidBoxId;
    var urlBoxId;
    var originalUrlBoxId;
    var originalModeBoxId;
    var logLevelComboId;
    var pollingEnginePassiveComboId;
    var pollingEngineActiveId;
    var pollingEngineActiveNameId;
    var advancedPassiveCheckboxId;
    var advancedActiveCheckboxId;
    var updateRemoteSettingsCheckboxId;
    var agentStatusMessageBoxId;
    var agentStatusBoxId;
    var originalAgentIpBoxId;
    var originalAgentDnsNameBoxId;
    var originalAgentPollingEngineIdId;
    var originalAgentPollingEngineNameId;
    var savePostBack;
    var testMask;
    var previousDiagnosticsRunStatus = null;
    var agentVersionFromTestId;

    var activeAgents = {};

    Init = function () {
        $('#' + advancedPassiveCheckboxId).click(function (e) {
            e.stopPropagation();
        });

        $('#' + advancedActiveCheckboxId).click(function (e) {
            e.stopPropagation();
        });

        $('#SEUM_ToggleAdvancedPassiveDiv').click(function () {
            $('#' + advancedPassiveCheckboxId).click();

            RefreshAdvancedSections();
        });

        $('#SEUM_ToggleAdvancedActiveDiv').click(function () {
            $('#' + advancedActiveCheckboxId).click();

            RefreshAdvancedSections();
        });

        $('#ToggleTroubleshootDiv').click(function () {
            if ($('#TroubleshootCollapseImg').attr('src') == '/Orion/Images/Button.Expand.gif') {
                $('#TroubleshootDivLoading').show();
                $('#TroubleshootDivError').hide();
                $('#TroubleshootDiv').hide();
                $('#TroubleshootCollapseImg').attr('src', '/Orion/Images/Button.Collapse.gif');

                RefreshDiagnosticsStatus();
            }
            else {
                $('#TroubleshootDiv').hide();
                $('#TroubleshootDivError').hide();
                $('#TroubleshootDivLoading').hide();
                $('#TroubleshootCollapseImg').attr('src', '/Orion/Images/Button.Expand.gif');
            }
        });

        $('#GenerateDiagnosticsLink').click(
            function () {
                $('#GenerateDiagnosticsLink').hide();
                var agent = GetAgentFromForm(false);
                CallGenerateDiagnostics(agent.AgentId,
                    function () {
                        setTimeout(RefreshDiagnosticsStatus, 1000);
                    });
                return false;
            });

        $('#' + useProxyCheckboxId).click(function () {
            RefreshProxySection();
        });

        $('#' + useProxyAuthenticationCheckboxId).click(function () {
            RefreshProxyAuthenticationSection();
        });

        $('#' + passiveRadioId).click(function () {
            RefreshSetingsSections();

            if ($('#' + passiveRadioId).is(':checked')) {
                var agent = GetAgentFromForm(false);
                if (agent.Url == '')
                    $('#' + urlBoxId).val('https://' + agent.DNSName);
            }
        });

        $('#' + activeRadioId).click(function () {
            RefreshSetingsSections();
        });

        $('#activeAgentsCombo').change(function () {
            UpdateSelectedActiveAgent();
        });

        if (updateRemoteSettingsCheckboxId) {
            if ($('#' + updateRemoteSettingsCheckboxId).is(':checked'))
                DisableControlsForRemoteSettings();

            $('#' + updateRemoteSettingsCheckboxId).click(function () {
                var isChecked = $(this).is(':checked');
                if (isChecked)
                    DisableControlsForRemoteSettings();
                else
                    EnableControlsForRemoteSettings();
            });
        }

        RefreshSetingsSections();
        RefreshAdvancedSections();
        RefreshProxySection();
        RefreshProxyAuthenticationSection();
        RefreshActiveAgentsList();
    };

    CallGetDiagnosticsStatus = function (agentId, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                             "GetDiagnosticsStatus", { agentId: agentId },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    CallGenerateDiagnostics = function (agentId, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                             "GenerateDiagnostics", { agentId: agentId },
                             function (result) {
                                 onSuccess();
                             });
    };

    CallCheckAgentNameExists = function (agentId, agentName, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                            "CheckAgentNameExists", { agentId: agentId, agentName: agentName },
                            function (result) {
                                onSuccess(result);
                            });
    };

    CallCheckAgentLocationExists = function (agentId, agentUrl, agentPort, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                            "CheckAgentLocationExists", { agentId: agentId, url: agentUrl, port: agentPort },
                            function (result) {
                                onSuccess(result);
                            });
    };

    CallTestAgent = function (agent, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                             "TestAgent", { agent: agent, updateRemoteSettings: updateRemoteSettingsCheckboxId ? $('#' + updateRemoteSettingsCheckboxId).is(':checked') : false },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    CallGetActiveAgentsWaitingForAdd = function (onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                             "GetActiveAgentsWaitingForAdd", {},
                             function (result) {
                                 onSuccess(result);
                             });
    };

    RefreshActiveAgentsList = function () {
        $('#activeAgentsCombo').html("");

        // Get original agent
        var originalAgent = GetAgentFromForm(true);
        // Get agent from form
        var agentFromForm = GetAgentFromForm(false);

        var currentAgentSelected = false;
        var originalAgentAdded = false;
        var agentFromFormAdded = false;

        // If there is an original agent and it is different that selected agent, display it
        if (originalAgent.IP != '' && originalAgent.IP != agentFromForm.IP) {
            $('#activeAgentsCombo').append('<option value="' + originalAgent.AgentGuid + '">' + originalAgent.DNSName + ' (' + originalAgent.IP + ')</option>');
            originalAgentAdded = true;
        }
        if (agentFromForm.IP != '') {
            $('#activeAgentsCombo').append('<option value="' + agentFromForm.AgentGuid + '" selected="selected">' + agentFromForm.DNSName + ' (' + agentFromForm.IP + ')</option>');
            agentFromFormAdded = true;
            currentAgentSelected = true;
        }

        $('#activeAgentsCombo').append('<option value="loadingAgents">' + loadingAgentsText + '</option>');

        CallGetActiveAgentsWaitingForAdd(function (agents) {
            $('#activeAgentsCombo option[value="loadingAgents"]').remove();

            if (agents.length == 0 && !originalAgentAdded && !agentFromFormAdded) {
                $('#activeAgentsCombo').append('<option value="" selected="selected">' + noAgentAvailableText + '</option>');
            }

            for (var i = 0; i < agents.length; i++) {
                // add agent to agent list for later use
                activeAgents[agents[i].AgentGuid] = agents[i];

                // skip current agent, it's already there
                if ((agents[i].AgentGuid == agentFromForm.AgentGuid && agentFromFormAdded)
                    || (agents[i].AgentGuid == originalAgent.AgentGuid && originalAgentAdded))
                    continue;

                if (i == 0 && !currentAgentSelected)
                    $('#activeAgentsCombo').append('<option value="' + agents[i].AgentGuid + '" selected="selected">' + agents[i].DNSName + ' (' + agents[i].IP + ')</option>');
                else
                    $('#activeAgentsCombo').append('<option value="' + agents[i].AgentGuid + '">' + agents[i].DNSName + ' (' + agents[i].IP + ')</option>');
            }

            UpdateSelectedActiveAgent();
        });
    };

    UpdateSelectedActiveAgent = function () {
        var guid = $('#activeAgentsCombo option:selected').val();
        var originalGuid = $('#' + originalGuidBoxId).val();

        // TODO: Move agentFromForm and originalAgent to agents collection and select always from that collection
        if (guid != '' && typeof (activeAgents[guid]) != 'undefined') {
            $('#' + guidBoxId).val(guid);
            $('#' + agentDnsNameBoxId).val(activeAgents[guid].DNSName);
            $('#' + agentIpBoxId).val(activeAgents[guid].IP);
            $('#' + pollingEngineActiveId).val(activeAgents[guid].PollingEngineId);
            $('#' + pollingEngineActiveNameId).val(activeAgents[guid].PollingEngineName);
        }
        else if (originalGuid != '' && originalGuid == guid) {
            $('#' + guidBoxId).val(originalGuid);
            $('#' + agentDnsNameBoxId).val($('#' + originalAgentDnsNameBoxId).val());
            $('#' + agentIpBoxId).val($('#' + originalAgentIpBoxId).val());
            $('#' + pollingEngineActiveId).val($('#' + originalAgentPollingEngineIdId).val());
            $('#' + pollingEngineActiveNameId).val($('#' + originalAgentPollingEngineNameId).val());
        }
    };

    RefreshAdvancedSections = function () {
        var isChecked = $('#' + advancedPassiveCheckboxId).is(':checked');

        if (isChecked) {
            $('#SEUM_PlayerAdvancedPassiveSettings').show();
            $('#CollapsePassiveImg').attr('src', '/Orion/Images/Button.Collapse.gif');
        }
        else {
            $('#SEUM_PlayerAdvancedPassiveSettings').hide();
            $('#CollapsePassiveImg').attr('src', '/Orion/Images/Button.Expand.gif');
        }

        isChecked = $('#' + advancedActiveCheckboxId).is(':checked');

        if (isChecked) {
            $('#SEUM_PlayerAdvancedActiveSettings').show();
            $('#CollapseActiveImg').attr('src', '/Orion/Images/Button.Collapse.gif');
        }
        else {
            $('#SEUM_PlayerAdvancedActiveSettings').hide();
            $('#CollapseActiveImg').attr('src', '/Orion/Images/Button.Expand.gif');
        }
    };

    RefreshProxySection = function () {
        var isChecked = $('#' + useProxyCheckboxId).is(':checked');

        if (isChecked) {
            $('#SEUM_ProxySettings').show();
        }
        else {
            $('#SEUM_ProxySettings').hide();
            $('#' + useProxyAuthenticationCheckboxId).attr('checked', false);
        }
        RefreshProxyAuthenticationSection();
    };

    RefreshProxyAuthenticationSection = function () {
        var isChecked = $('#' + useProxyAuthenticationCheckboxId).is(':checked');

        if (isChecked) {
            $('#SEUM_ProxyAuthenticationSettings').show();
        }
        else {
            $('#SEUM_ProxyAuthenticationSettings').hide();
        }
    };

    RefreshSetingsSections = function () {
        var isChecked = $('#' + passiveRadioId).is(':checked');

        if (isChecked) {
            $('#SEUM_ActiveSettings').hide();
            $('#SEUM_PassiveSettings').show();
        }
        else {
            $('#SEUM_PassiveSettings').hide();
            $('#SEUM_ActiveSettings').show();
        }
    };

    SetTestResults = function (message, detail, className, showHelp) {
        $('#SEUM_TestProgress').hide();

        var testResults = $('#SEUM_TestResults');
        testResults.removeAttr('class');
        $('#SEUM_TestResults .sw-suggestion-title').text(message);

        testResults.addClass('sw-suggestion');
        if (className != '')
            testResults.addClass(className);

        var testDetail = $('#SEUM_TestDetail');
        if (detail != null) {
            if (showHelp)
                testDetail.html(detail + testDetail.html());
            else
                testDetail.text(detail);
        } else {
            testDetail.text("");
        }

        $('#SEUM_TestResults').show();
    }

    TestAgentSettings = function (onSuccess) {
        var agent = GetAgentFromForm(false);
        if (agent == null)
            return;

        $('#SEUM_TestResults').hide();
        $('#SEUM_TestProgress').show();
        CallTestAgent(agent, function (results) {
            var className = results.Success == '1' ? 'sw-suggestion-pass' : 'sw-suggestion-fail';
            SetTestResults(results.Message, results.Detail, className, (results.Status == 0 || results.Status == 2));

            if (onSuccess)
                onSuccess(results);
        });
    }

    GetAgentFromForm = function (useOriginal) {
        var agent = {};
        agent.AgentId = $('#' + agentIdBoxId).val();
        if (useOriginal) {
            agent.DNSName = $('#' + originalAgentDnsNameBoxId).val();
            agent.IP = $('#' + originalAgentIpBoxId).val();
        } else {
            agent.DNSName = $('#' + agentDnsNameBoxId).val();
            agent.IP = $('#' + agentIpBoxId).val();
        }
        agent.Name = $('#' + nameBoxId).val();
        agent.IsActiveAgent = $('#' + activeRadioId).is(':checked');
        agent.Url = $('#' + urlBoxId).val();
        agent.Port = $('#' + portBoxId).val() || 0;

        if (agent.IsActiveAgent) {
            if (useOriginal) {
                agent.PollingEngineId = $('#' + originalAgentPollingEngineIdId).val();
                agent.PollingEngineName = $('#' + originalAgentPollingEngineNameId).val();
            } else {
                agent.PollingEngineId = $('#' + pollingEngineActiveId).val();
                agent.PollingEngineName = $('#' + pollingEngineActiveNameId).val();
            }
        } else {
            agent.Password = $('#' + passwordPassiveBoxId).val();
            agent.PollingEngineId = $('#' + pollingEnginePassiveComboId).val();
            agent.PollingEngineName = $('#' + pollingEnginePassiveComboId + ' option:selected').text();
        }

        agent.UseProxy = $('#' + useProxyCheckboxId).is(':checked');
        agent.ProxyUrl = $('#' + proxyUrlBoxId).val();
        agent.UseProxyAuthentication = $('#' + useProxyAuthenticationCheckboxId).is(':checked');
        agent.ProxyUserName = $('#' + proxyUserNameBoxId).val();
        agent.ProxyPassword = $('#' + proxyPasswordBoxId).val();
        if (useOriginal) {
            agent.AgentGuid = $('#' + originalGuidBoxId).val();
        } else {
            if ($('#' + guidBoxId).val() != '') {
                agent.AgentGuid = $('#' + guidBoxId).val();
            }
        }

        if (logLevelComboId)
            agent.LogLevel = $('#' + logLevelComboId + ' option:selected').text();

        return agent;
    }

    RefreshDiagnosticsStatus = function () {
        var agent = GetAgentFromForm(false);

        if (!agent.AgentId || agent.AgentId == 0) {
            $('#ToggleTroubleshootDiv').hide();
            return;
        }

        $('#ToggleTroubleshootDiv').show();

        CallGetDiagnosticsStatus(agent.AgentId,
            function (result) {
                if (!result) {
                    $('#TroubleshootDivLoading').hide();
                    $('#TroubleshootDiv').hide();
                    $('#TroubleshootDivError').show();
                    return;
                }

                // stop refresh if completed
                if (!result.Running) {
                    if (result.Exists) {
                        $('#SEUM_DiagnosticsStatus').html(String.format(diagnosticsAvailableFormat, result.DateTime) + ' <a  class="sw-link" href="/Orion/SEUM/Admin/AgentDiagnosticsDownloader.ashx?agentId=' + agent.AgentId + '" target="_blank">' + downloadText + '</a>');
                    } else {
                        $('#SEUM_DiagnosticsStatus').html(noDiagnosticsMessage);
                    }
                    $('#GenerateDiagnosticsLink').show();
                }
                // set loading icon only fo running status changed to prevent lags in animation
                else if (previousDiagnosticsRunStatus != result.Running) {
                    $('#SEUM_DiagnosticsStatus').html('<img src="/Orion/images/loading_gen_small.gif" /> ' + diagnosticsRunningMessage);
                    $('#GenerateDiagnosticsLink').hide();
                }

                $('#' + logLevelComboId).val(result.CurrentLogLevel);

                previousDiagnosticsRunStatus = result.Running;

                $('#TroubleshootDivLoading').hide();
                $('#TroubleshootDiv').show();

                if (result.Running)
                    setTimeout(RefreshDiagnosticsStatus, 1000);
            });
    };

    ShowError = function (msg, title) {
        Ext.Msg.show({
            title: title || 'Error',
            msg: msg,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR
        });
    };

    DisableControlsForRemoteSettings = function () {
        // reset changes except port and password
        $('#' + guidBoxId).val($('#' + originalGuidBoxId).val());
        $('#' + urlBoxId).val($('#' + originalUrlBoxId).val());

        if ($('#' + originalModeBoxId).val() == 'active') {
            $('#' + activeRadioId).click();
            $('#' + activeRadioId).trigger('click');
        } else {
            $('#' + passiveRadioId).click();
            $('#' + passiveRadioId).trigger('click');
        }

        // disable mode selection
        $('#' + nameBoxId).attr('disabled', true);
        $('#' + activeRadioId).attr('disabled', true);
        $('#' + passiveRadioId).attr('disabled', true);
        $('#' + guidBoxId).attr('disabled', true);
        $('#' + urlBoxId).attr('disabled', true);
        $('#' + useProxyCheckboxId).attr('disabled', true);
        $('#' + proxyUrlBoxId).attr('disabled', true);
        $('#' + useProxyAuthenticationCheckboxId).attr('disabled', true);
        $('#' + proxyUserNameBoxId).attr('disabled', true);
        $('#' + proxyPasswordBoxId).attr('disabled', true);
        $('#' + pollingEnginePassiveComboId).attr('disabled', true);
    }

    EnableControlsForRemoteSettings = function () {
        $('#' + nameBoxId).removeAttr('disabled');
        $('#' + activeRadioId).removeAttr('disabled');
        $('#' + passiveRadioId).removeAttr('disabled');
        $('#' + guidBoxId).removeAttr('disabled');
        $('#' + urlBoxId).removeAttr('disabled');
        $('#' + useProxyCheckboxId).removeAttr('disabled');
        $('#' + proxyUrlBoxId).removeAttr('disabled');
        $('#' + useProxyAuthenticationCheckboxId).removeAttr('disabled');
        $('#' + proxyUserNameBoxId).removeAttr('disabled');
        $('#' + proxyPasswordBoxId).removeAttr('disabled');
        $('#' + pollingEnginePassiveComboId).removeAttr('disabled');
    }

    var originalHandleError = ORION.handleError;
    ORION.handleError = function (xhr) {
        originalHandleError(xhr);

        var msg = $('#test').text();
        if (msg) {
            ShowError(msg);
        }
    };

    return {

        Init: function () {
            Init();
        },

        TestAgentSettings: function () {
            TestAgentSettings();
        },
        TestAgentSettingsAndSave: function () {
            if (testMask == null)
                testMask = new Ext.LoadMask(Ext.getBody(), { msg: testingMessage });

            testMask.show();

            TestAgentSettings(function (results) {
                $('#' + agentStatusBoxId).val(results.Status);
                $('#' + agentStatusMessageBoxId).val(results.Message);

                var version = results.AgentVersion;
                if (typeof version !== 'undefined' && version != null) {
                    $('#' + agentVersionFromTestId).val(version.Major + '.' + version.Minor + '.' + version.Build + '.' + version.Revision);
                }

                if (results.Status == 1) {
                    savePostBack();
                } else {
                    testMask.hide();

                    if ($('#' + updateRemoteSettingsCheckboxId).is(':checked')) {
                        Ext.Msg.show({
                            title: updateRemoteSettingsMessageTitle,
                            msg: String.format(updateRemoteSettingsWithFailedTestMessageFormat, results.Message),
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    } else {
                        if (results.IsPollerDown) {
                            Ext.Msg.show({
                                title: saveWithFailedTestMessageTitle,
                                msg: String.format(failedTestPollerDownMessageFormat, results.Message),
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        } else {
                            Ext.Msg.show({
                                title: saveWithFailedTestMessageTitle,
                                msg: String.format(saveWithFailedTestMessageFormat, results.Message),
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.MessageBox.WARNING,
                                fn: function (buttonId) {
                                    if (buttonId == 'yes') {
                                        testMask.show();
                                        savePostBack();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        },

        SetUseProxyCheckboxId: function (id) { useProxyCheckboxId = id; },
        SetPassiveRadioId: function (id) { passiveRadioId = id; },
        SetActiveRadioId: function (id) { activeRadioId = id; },
        SetAgentIdBoxId: function (id) { agentIdBoxId = id; },
        SetAgentDNSNameBoxId: function (id) { agentDnsNameBoxId = id; },
        SetAgentIPBoxId: function (id) { agentIpBoxId = id; },
        SetNameBoxId: function (id) { nameBoxId = id; },
        SetPasswordPassiveBoxId: function (id) { passwordPassiveBoxId = id; },
        SetPortBoxId: function (id) { portBoxId = id; },
        SetProxyUrlBoxId: function (id) { proxyUrlBoxId = id; },
        SetUseProxyAuthenticationCheckboxId: function (id) { useProxyAuthenticationCheckboxId = id; },
        SetProxyUserNameBoxId: function (id) { proxyUserNameBoxId = id; },
        SetProxyPasswordBoxId: function (id) { proxyPasswordBoxId = id; },
        SetGuidBoxId: function (id) { guidBoxId = id; },
        SetOriginalGuidBoxId: function (id) { originalGuidBoxId = id; },
        SetUrlBoxId: function (id) { urlBoxId = id; },
        SetOriginalUrlBoxId: function (id) { originalUrlBoxId = id; },
        SetOriginalModeBoxId: function (id) { originalModeBoxId = id; },
        SetLogLevelComboId: function (id) { logLevelComboId = id; },
        SetPollingEnginePassiveComboId: function (id) { pollingEnginePassiveComboId = id; },
        SetPollingEngineActiveId: function (id) { pollingEngineActiveId = id; },
        SetPollingEngineActiveNameId: function (id) { pollingEngineActiveNameId = id; },
        SetAdvancedPassiveCheckboxId: function (id) { advancedPassiveCheckboxId = id; },
        SetAdvancedActiveCheckboxId: function (id) { advancedActiveCheckboxId = id; },
        SetUpdateRemoteSettingsCheckboxId: function (id) { updateRemoteSettingsCheckboxId = id; },
        SetAgentStatusBoxId: function (id) { agentStatusBoxId = id; },
        SetAgentStatusMessageBoxId: function (id) { agentStatusMessageBoxId = id; },
        SetSavePostBack: function (postback) { savePostBack = postback; },
        SetOriginalAgentIpBoxId: function (id) { originalAgentIpBoxId = id; },
        SetOriginalAgentDnsNameBoxId: function (id) { originalAgentDnsNameBoxId = id; },
        SetOriginalAgentPollingEngineIdId: function (id) { originalAgentPollingEngineIdId = id; },
        SetOriginalAgentPollingEngineNameId: function (id) { originalAgentPollingEngineNameId = id; },
        SetAgentVersionFromTestId: function (id) { agentVersionFromTestId = id; }
    };
} ();
