﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.StepPlaybackSelector = function () {
    var selectorWindow = [];
    var groupByCombo = [];
    var groupByList = [];
    var selectPanel = [];
    var groupingPanel = [];
    var groupByStore = [];

    var gridPanel = [];
    var grid = [];

    var selectPostBack = [];

    var loadingMask = [];
    var resultFieldID = [];

    var pageSize = 15;

    ShowLoadingMask = function (resourceId) {
        loadingMask[resourceId] = new Ext.LoadMask(grid[resourceId].el, {
            msg: "Loading data ..."
        });
    }

    RefreshObjects = function (resourceId, stepId) {
        ShowLoadingMask(resourceId);

        var records = groupByList[resourceId].getSelectedRecords();
        var timePeriod = 0;
        if (records.length > 0)
            timePeriod = records[0].data.Value;

        grid[resourceId].store.on('beforeload', function () { loadingMask[resourceId].show(); });
        grid[resourceId].store.proxy.conn.jsonData = { stepId: stepId, timePeriod: timePeriod };
        grid[resourceId].store.baseParams = { start: 0, limit: pageSize };
        grid[resourceId].store.on('load', function () {
            loadingMask[resourceId].hide();
        });
        grid[resourceId].store.load();
    }

    RefreshGroupByList = function (resourceId, stepId, groupByCategory, handler) {
        groupByStore[resourceId].proxy.conn.jsonData = { stepId: stepId, groupByCategory: groupByCategory };
        if (handler) {
            var internalHandler = function () {
                handler();
                groupByStore[resourceId].un('load', internalHandler);
            };
            groupByStore[resourceId].on('load', internalHandler);
        }

        groupByStore[resourceId].load();
    }

    InitGroupingPanel = function (resourceId, stepId) {
        groupByCombo[resourceId] = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: [['time', 'Relative time period']]
            }),
            valueField: 'itemValue',
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            width: 150,
            fieldLabel: 'Group by',
            editable: false,
            listeners: {
                select: function (record, index) {
                    RefreshGroupByList(resourceId, stepId, record.value);
                }
            }
        });
        groupByCombo[resourceId].setValue('time');

        selectPanel[resourceId] = new Ext.Panel({
            height: 55,
            layout: 'form',
            cls: 'SEUM_GroupingPanel',
            items: groupByCombo[resourceId],
            border: false,
            labelWidth: 150
        });

        var tpl = new Ext.XTemplate(
        	'<tpl for=".">',
                '<div class="SEUM_GroupByItem" id="{Value}">{Label}</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        );

        groupByStore[resourceId] = new ORION.WebServiceStore(
                "/Orion/SEUM/Services/StepPlaybackSelector.asmx/GetGroupByItems",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Label', mapping: 1 }
                ],
                "Label");

        groupByList[resourceId] = new Ext.DataView({
            cls: 'SEUM_GroupByList',
            border: true,
            store: groupByStore[resourceId],
            tpl: tpl,
            autoHeight: true,
            singleSelect: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            itemSelector: 'div.SEUM_GroupByItem',
            listeners: {
                selectionchange: function (view, selections) {
                    if (selections.length > 0)
                        RefreshObjects(resourceId, stepId);
                }
            }
        });

        groupingPanel[resourceId] = new Ext.Panel({
            region: 'west',
            width: 165,
            split: false,
            items: [selectPanel[resourceId], groupByList[resourceId]]
        });
    }

    InitGridPanel = function (resourceId, stepId) {
        var selectorModel = new Ext.sw.SEUM.grid.RadioSelectionModel();

        var dataStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/StepPlaybackSelector.asmx/GetPlaybacksPaged",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Date', mapping: 1 },
                                { name: 'Time', mapping: 2 },
                                { name: 'Status', mapping: 3 },
                                { name: 'Duration', mapping: 4 }
                            ],
                            "Date DESC"); // this is passed all the way to our DAL and there we change sort direction

        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: "Error",
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });
        });

        var pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: pageSize,
            displayInfo: true,
            displayMsg: 'Displaying transactions {0} - {1} of {2}',
            emptyMsg: "No transaction to display",
            listeners: {
                change: function () {
                    if (this.displayItem) {
                        var count = 0;
                        this.store.each(function (record) {
                            if (record.data.Level == 0)
                                count++;
                        });

                        var msg = count == 0 ?
                            this.emptyMsg :
                            String.format(
                                this.displayMsg,
                                this.cursor + 1, this.cursor + count, this.store.getTotalCount()
                            );
                        this.displayItem.setText(msg);
                    }
                }
            }
        });

        grid[resourceId] = new Ext.grid.GridPanel({

            store: dataStore,

            columns: [
                    selectorModel,
                    { header: 'ID', width: 40, hidden: true, hideable: false, sortable: false, dataIndex: 'ID' },
                    { header: 'Date', width: 100, hideable: false, sortable: true, dataIndex: 'Date' },
                    { header: 'Time', width: 100, hideable: false, sortable: true, dataIndex: 'Time' },
                    { header: 'Status', width: 80, hideable: false, sortable: true, dataIndex: 'Status' },
                    { id: 'Duration', header: 'Duration', width: 80, hideable: false, sortable: true, dataIndex: 'Duration' }
                ],

            sm: selectorModel,

            autoscroll: true,
            stripeRows: true,
            autoExpandColumn: 'Duration',
            viewConfig: {
                emptyText: 'There are no transactions available or no transactions match specified search value'
            },

            tbar: [
					'->'//,
//                    'Filter by date:',
//                    new Ext.form.TextField({
//                        name: 'filterDate',
//                        width: 100,
//                        listeners: {
//                            change: function (field, newValue, oldValue) {
//                                alert(newValue);
//                            }
//                        }
//                    })
                    ],

            bbar: pagingToolbar
        });

        gridPanel[resourceId] = new Ext.Panel({
            region: 'center',
            layout: 'fit',
            items: grid[resourceId]
        });
    }

    CreateSelectorWindow = function (resourceId, stepId) {
        InitGroupingPanel(resourceId, stepId);
        InitGridPanel(resourceId, stepId);

        var mainPanel = new Ext.Panel({
            layout: 'border',
            border: false,
            items: [groupingPanel[resourceId], gridPanel[resourceId]]
        });

        selectorWindow[resourceId] = new Ext.Window({
            title: 'Select a specific transaction',
            width: 700,
            height: 550,
            closeAction: 'hide',
            closable: true,
            modal: true,
            cls: 'SEUM_StepHistoryRecordSelectorWindow',
            layout: 'fit',
            items: mainPanel,
            buttons: [{
                text: 'Select transaction',
                handler: function () {
                    var record = grid[resourceId].getSelectionModel().getSelected();
                    if (record)
                        $('#' + resultFieldID[resourceId]).val(record.data.ID);

                    selectPostBack[resourceId]("test");
                    selectorWindow[resourceId].hide();
                }
            }, {
                text: 'Cancel',
                handler: function () {
                    selectorWindow[resourceId].hide();
                }
            }],
            listeners: {
                beforeshow: function () {
                    grid[resourceId].store.removeAll();
                }
            }
        });
    };

    return {
        show: function (resourceId, stepId, resultFieldClientID, postBackFn) {
            selectPostBack[resourceId] = postBackFn;
            resultFieldID[resourceId] = resultFieldClientID;

            if (!selectorWindow[resourceId])
                CreateSelectorWindow(resourceId, stepId);

            selectorWindow[resourceId].show();

            RefreshGroupByList(resourceId, stepId, "time", function () {
                groupByList[resourceId].select(0);
            });
        }
    };
} ();
