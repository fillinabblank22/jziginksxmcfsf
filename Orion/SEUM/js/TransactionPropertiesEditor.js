﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.TransactionPropertiesEditor = new function () {
    this.currentCultureNumberDecimalDelimiter = '.';

    this.SetDelimiter = function (delimiter) {
        currentCultureNumberDecimalDelimiter = delimiter;
    };

    this.EnableStepThresholds = function (checkbox, warningBoxId, criticalBoxId, optimalBoxId, editorIndex) {
        var checked = $(checkbox).is(':checked');

        if (checked) {
            var optimal = 0;
            var optimalStr = $('#' + optimalBoxId).val().replace(",", ".");
            if (!isNaN(parseFloat(optimalStr)) && isFinite(optimalStr))
                optimal = parseFloat(optimalStr);

            var attr = $('#' + warningBoxId).attr('data-sw-default-value');
            if (typeof attr !== 'undefined' && attr !== false) { // get previously saved value
                $('#' + warningBoxId).val(attr);
            } else { // if not available -> calculate new threshold from typical value
                $('#' + warningBoxId).val((optimal * 2).toString().replace('.', currentCultureNumberDecimalDelimiter));
            }
            $('#' + warningBoxId).removeAttr('disabled');

            var attr = $('#' + criticalBoxId).attr('data-sw-default-value');
            if (typeof attr !== 'undefined' && attr !== false) { // get previously saved value
                $('#' + criticalBoxId).val(attr);
            } else { // if not available -> calculate new threshold from typical value
                $('#' + criticalBoxId).val((optimal * 4).toString().replace('.', currentCultureNumberDecimalDelimiter));
            }
            $('#' + criticalBoxId).removeAttr('disabled');
        }
        else {
            $('#' + warningBoxId).attr('disabled', 'disabled');
            var attr = $('#' + warningBoxId).attr('data-sw-default-value');
            if (typeof attr === 'undefined' || attr === false) {
                $('#' + warningBoxId).attr('data-sw-default-value', $('#' + warningBoxId).val());
            }
            $('#' + warningBoxId).val(0);

            $('#' + criticalBoxId).attr('disabled', 'disabled');
            var attr = $('#' + criticalBoxId).attr('data-sw-default-value');
            if (typeof attr === 'undefined' || attr === false) {
                $('#' + criticalBoxId).attr('data-sw-default-value', $('#' + criticalBoxId).val());
            }
            $('#' + criticalBoxId).val(0);
        }

        this.EnableAllCheckBoxChangeState(editorIndex);
    };

    this.EnableAllClicked = function (checkbox, editorIndex) {
        var checked = $(checkbox).is(':checked');

        if (checked) {
            $('#SEUM_StepsThresholdsTable' + editorIndex + ' input[type=checkbox]').each(function (i) {
                if (!$(this).is(':checked')) {
                    $(this).attr('checked', true);  //because the click hander is called before the checkbox state is set we need to set checkbox state and trigger handler directly
                    $(this).triggerHandler('click');
                }
            });
        }
        else {
            $('#SEUM_StepsThresholdsTable' + editorIndex + ' input[type=checkbox]').each(function (i) {
                if ($(this).is(':checked')) {
                    $(this).attr('checked', false);
                    $(this).triggerHandler('click');
                }
            });
        }
    };

    this.EnableAllCheckBoxChangeState = function (editorIndex) {
        var thresholdsDisabled = true;

        $('#SEUM_StepsThresholdsTable' + editorIndex + ' input[type="checkbox"][name*="enableThresholdsCheckBox"]').each(function (i) {
            thresholdsDisabled = thresholdsDisabled && $(this).is(':checked');
        });

        $('#SEUM_StepsThresholdsTable' + editorIndex + ' input[type="checkbox"][name*="enableAllThresholdsCheckBox"]').attr('checked', thresholdsDisabled);
    };
};