﻿/**
* Ext.sw.grid.RadioSelectionModel
*/

Ext.ns('Ext.sw.SEUM.grid');

/**
* Similar selection model to CheckboxSelectionModel, just uses radio buttons.
* @class Ext.sw.SEUM.grid.RadioSelectionModel
* @extends Ext.grid.RowSelectionModel
* @constructor
* @param {Object} config The configuration options
*/
Ext.sw.SEUM.grid.RadioSelectionModel = function (config) {
    var maxRadioLevel = 99;
    if (config != null && config.maxRadioLevel != null)
        maxRadioLevel = config.maxRadioLevel;

    // call parent
    Ext.sw.SEUM.grid.RadioSelectionModel.superclass.constructor.call(this, config);

    // force renderer to run in this scope
    this.renderer = function (v, p, record) {
        var level = 0;
        if (record.data != null && record.data.Level != null)
            level = record.data.Level;

        if (level > maxRadioLevel)
            return '';

        var checked = record === this.selections.get(0);
        var retval =
            '<div><input type="radio" name="' + this.id + '"'
            + (checked ? 'checked="checked"' : '')
            + '></div>';
        return retval;
    } .createDelegate(this);

    this.canBeSelected = function (record) {
        if (record == null  || record.data == null)
            return true;

        if (record.data.Level != null && record.data.Level > maxRadioLevel)
            return false;

        return true;
    }

};       // end of constructor

Ext.extend(Ext.sw.SEUM.grid.RadioSelectionModel, Ext.grid.RowSelectionModel, {
    header: '<div class="x-grid3-hd-radio">&#160;</div>',
    width: 23,
    sortable: false,
    // private
    menuDisabled: true,
    fixed: true,
    dataIndex: '',
    id: 'checker',
    singleSelect: true,

    selectRow: function (index) {
        // call parent
        Ext.sw.SEUM.grid.RadioSelectionModel.superclass.selectRow.apply(this, arguments);

        // check radio
        var row = Ext.fly(this.grid.view.getRow(index));
        if (row) {
            var radio = row.child('input[type=radio]');
            if (radio)
                radio.dom.checked = true;
        }
    },

    listeners: {
        beforerowselect: function (obj, rowIndex, keepExisting, record) {
            if (!this.canBeSelected(record))
                return false;
        }
    }
});