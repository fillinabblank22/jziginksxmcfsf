﻿function SynchronousConfirm(button, title, text, icon) {
    if ($(button).attr("confirmed"))
        return true;

    Ext.Msg.show({
        title: title,
        msg: text,
        buttons: Ext.Msg.YESNO,
        icon: icon,
        fn: function (buttonId) {
            if (buttonId == 'yes') {
                $(button).attr("confirmed", true);
                $(button).click();
                // button click doesn't work so we need to do it this way
                document.location = $(button).attr('href');
            }
        }
    });

    return false;
}

function SEUMIsDemoMode() {
    return $("#isOrionDemoMode").length > 0
}

function SEUMDemoModeMessage() {
    alert("This functionality is disabled in the demo.");
    return false;
}

function SEUMDisableInDemo() {
    if (SEUMIsDemoMode())
        return SEUMDemoModeMessage();
    return true;
}

function SEUM_GetViewportHeight() {
    var viewportheight;

    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight

    if (typeof window.innerWidth != 'undefined') {
        viewportheight = window.innerHeight
    }

    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)

    else if (typeof document.documentElement != 'undefined'
     && typeof document.documentElement.clientWidth !=
     'undefined' && document.documentElement.clientWidth != 0) {
        viewportheight = document.documentElement.clientHeight
    }

    // older versions of IE

    else {
        viewportheight = document.getElementsByTagName('body')[0].clientHeight
    }

    return viewportheight;
}

function SEUM_GetManagementGridHeight() {
    var height = SEUM_GetViewportHeight() - 410;
    if (height >= 200)
        return height;
    else
        return 200;
}

function SEUM_HoverHelpLinkHtml(options) {
    var defaults = {
        helpLinkIcon: '/orion/SEUM/images/Icon.HelpLink.png',
        helpLinkCls: 'SEUM_HoverHelpLink',
        helpLinkText: '',
        helpLinkTextCls: 'SEUM_HelpLink',
        html: '',
        extraClsLink: '',
        extraClsTooltip: '',
        title: '',
        hoverArrowCls: 'SEUM_HoverHelpArrow',
        hoverBodyCls: 'SEUM_HoverHelpBody',
        tooltipCls: 'SEUM_HoverHelpTooltip'
    };
    var settings = $.extend({}, defaults, options);

    var value = '<span class="' + settings.helpLinkCls + ' ' + settings.ExtraClsLink + '"'
    + ' onmousemove="SEUM_HoverHelpLink_ShowHelp(event, $(this).next(\'div\'), \'.' + settings.hoverArrowCls + '\');"'
    + ' onmouseout="SEUM_HoverHelpLink_HideHelp(event, $(this).next(\'div\'), this);">'
    + '<img src="' + settings.helpLinkIcon + '" />';

    if (settings.helpLinkText.length != 0) {
        value += '<span class="' + settings.helpLinkTextCls + '">' + settings.helpLinkText + '</span>';
    }
    value += '</span>';
    value += '<div class="' + settings.tooltipCls + '" style="display: none;">';
    value += '  <div class="' + settings.hoverArrowCls + '"></div>';
    value += '  <div class="' + settings.hoverBodyCls + ' sw-suggestion">';
    value += '    <span class="sw-suggestion-icon"></span>';

    if (settings.title.length != 0) {
        value += '<div class="sw-suggestion-title">' + settings.title + '</div>';
    }

    value += settings.html;
    value += '  </div>';
    value += '</div>';

    return value;
}

function SEUM_HoverHelpLink_ShowHelp(e, helpBodyDiv, helpArrowDiv) {
    if (!e)
        e = window.event;

    var tooltipHeight = $(helpBodyDiv).height();

    $(helpBodyDiv).css('left', e.clientX + 3);
    $(helpBodyDiv).css('top', e.clientY - tooltipHeight - 3);
    $(helpBodyDiv).children(helpArrowDiv).css('top', tooltipHeight - 17);

    $(helpBodyDiv).fadeIn("fast");
}

function SEUM_HoverHelpLink_HideHelp(e, helpBodyDiv, helpLinkSpan) {
    var el = e.toElement || e.relatedTarget; // we need to check that event is not from child element
    if (!SEUM_IsChild(helpLinkSpan, el) && el != helpLinkSpan)
        $(helpBodyDiv).fadeOut("fast");
}

function SEUM_IsChild(parent, child) {
    if (child != null) {
        while (child.parentNode) {
            if ((child = child.parentNode) == parent) {
                return true;
            }
        }
    }
    return false;
}

function SEUM_ConvertDateToUtc(date) {
    return new Date(date.getUTCFullYear(),
            date.getUTCMonth(),
            date.getUTCDate(),
            date.getUTCHours(),
            date.getUTCMinutes(),
            date.getUTCSeconds());
}

$(document).ready(function () {
    function activateDropdownMenus() {
        $("*[data-seum-dropdown='true']").each(function(index, container) {
            var triggerElement = $(".sw-seum-dropdown-menu__trigger", container);
            var contentElement = $(".sw-seum-dropdown-menu__content", container);

            triggerElement.click(function() {
                contentElement.toggle("fast");
            });

            $(document).mouseup(function(e) {
                if (!contentElement.is(e.target)) {
                    contentElement.hide();
                }
            });
        });
    }

    activateDropdownMenus();
})