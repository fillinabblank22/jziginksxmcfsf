﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.AssignObjectsControl = function (containerId, selectedIdsFieldId, entityType, entityCaption) {
    var $container;
    var $addButton;
    var $editButton;
    var $expandButton;
    var $label;
    var $objectsSelection;
    var $objectsTable;
    var $selectedIdsField;
    
    var updateObjectsTable = function () {
        var objectIds = $selectedIdsField.val();
        if (objectIds.length > 0) {
            $addButton.hide();
            $objectsSelection.show();
            ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "LoadEntitiesByUris", { entityType: entityType, uris: objectIds.split(",") }, function (result) {
                $objectsTable.find(".SEUM_AssignObjectsControl_Table_ObjectRow").remove();
                for (var i = 0; i < result.length; i++) {
                    var statusIconHtml = "";
                    if (result[i].Status > -1) {
                        var src = String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small{2}',
                            entityType, result[i].Status, (!result[i].StatusIconHint) ? "" : "&hint=" + result[i].statusIconHint);
                        statusIconHtml = "<img src='" + src + "'/>";
                    }
                    var $row = $("<tr class='SEUM_AssignObjectsControl_Table_ObjectRow '>" +
                                    "<td class='SEUM_AssignObjectsControl_Table_StatusIconCell'>" + statusIconHtml + "</td>" +
                                    "<td class='SEUM_AssignObjectsControl_Table_CaptionCell'>" + result[i].FullName + "</td>" +
                                    "<td class='SEUM_AssignObjectsControl_Table_DeleteCell'>" +
                                        "<input type='button' class='SEUM_AssignObjectsControl_DeleteItemButton' />" +
                                    "</td>" +
                                 "</tr>");
                    $objectsTable.append($row);
                    (function($row, uri) {
                        $row.find(".SEUM_AssignObjectsControl_DeleteItemButton").click(function() {
                            deleteRow($row, uri);
                        });
                    })($row, result[i].Uri);
                }
                updateLabel();
            });
        } else {
            $objectsSelection.hide();
            $addButton.show();
        }
    };

    var updateLabel = function() {
        $label.html(getSelectedUris().length + " related " + entityCaption);
    };

    var setSelectedUris = function (value) {
        $selectedIdsField.val(value.join(","));
    };

    var getSelectedUris = function () {
        return $selectedIdsField.val().split(",");
    };

    var deleteRow = function($row, uri) {
        var selectedUris = getSelectedUris();
        selectedUris.splice(selectedUris.indexOf(uri), 1);
        setSelectedUris(selectedUris);
        $row.remove();
        if (selectedUris.length == 0) {
            $objectsSelection.hide();
            $addButton.show();
        } else {
            updateLabel();
        }
    };

    var init = function () {
        $container = $("#" + containerId);
        $addButton = $container.find(".SEUM_AssignObjectsControl_AddButton");
        $editButton = $container.find(".SEUM_AssignObjectsControl_EditButton");
        $expandButton = $container.find(".SEUM_AssignObjectsControl_ExpandButton");
        $label = $container.find(".SEUM_AssignObjectsControl_Label");
        $objectsSelection = $container.find(".SEUM_AssignObjectsControl_ObjectsSelection");
        $objectsTable = $container.find(".SEUM_AssignObjectsControl_Table");
        $selectedIdsField = $("#" + selectedIdsFieldId);

        $($addButton).add($editButton).click(function () {
            SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: entityType }], entityType, getSelectedUris(), 'multiple');
            
            $('#selectordlg').dialog({
                position: 'center',
                width: 890,
                height: 'auto',
                modal: true,
                draggable: true,
                show: { duration: 0 },
                title: "Select " + entityCaption,
                close: function() {
                    $("#selectordlg").unbind("selector-dialog-save");
                }
            });
            $("#selectordlg").bind("selector-dialog-save", function () {
                setSelectedUris(SW.Orion.NetObjectPicker.GetSelectedEntities());
                updateObjectsTable();
                $("#selectordlg").unbind("selector-dialog-save");
            });
            return false;
        });
       
        $expandButton.click(function () {
            var setVisible = $objectsTable.hasClass("hidden");
            $objectsTable[setVisible ? "removeClass" : "addClass"]("hidden");
            $expandButton.attr("src", setVisible ? "/Orion/images/Button.Collapse.gif" : "/Orion/images/Button.Expand.gif");
        });

        updateObjectsTable();
    };

    init();
}