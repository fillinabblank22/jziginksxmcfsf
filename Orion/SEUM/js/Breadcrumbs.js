﻿
// --> Breadcrumbs navigation JS

$(function () {
    $('.SEUM_BreadcrumbsWrapper').livequery(function () {
        $('ul.bc_submenu').each(function () {
            var netObject = $(this).attr('childnodedataid');
            $('a.bc_submenulink[href$="NetObject=' + netObject + '"]', this).livequery(function () {
                $(this).parent().addClass('SEUM_BcCurrent');
            });
        });
    });
});

// <-- Breadcrumbs navigation JS