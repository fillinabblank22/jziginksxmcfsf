﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.RecordingsGrid = function () {
    var selectorModel;
    var dataStore;
    var initialPage = 1;
    var pagingToolbar;
    var grid;
    var searchField;
    var initialized;
    var expandedItems = new Array();

    var excludedRecordings = new Array();
    var unsupportedRecordings = new Array();

    var selectedRecordingsFieldClientID;

    var recordingEntity;
    var recordingStepEntity;

    var pageSizeBox;
    var pageSize = 1;
    var userName = "";

    var isEditGrid = true;

    var loadingMask = null;

    var importWindow = null;
    var exportWindow = null;
    var editWindow = null;
    var editStepWindow = null;
    var editWriteActionWindow = null;
    var editVerifyActionWindow = null;

    var currentEditRecording = null;
    var currentEditStep = null;
    var currentEditAction = null;

    var importLearnMoreLink = null;

    // --> Messages ================================================
    var loadingMsg = "Loading data ...";
    var deletingRecordingsMsg = "Deleting recordings ...";
    var deletingActionMsg = "Removing action ...";
    var errorTitle = "Error";
    var emptySearchText = 'Search all recordings ...';
    var pagingMsg = 'Displaying recordings {0} - {1} of {2}';
    var noRecordingsPagingMsg = "No recordings to display";
    var idHeader = 'ID';
    var recordingNameHeader = 'Recording Name';
    var locationsHeader = 'Assigned to Locations';
    var recorderTypeHeader = 'Recorder Type';
    var lastUpdatedHeader = 'Last Updated';
    var descriptionHeader = 'Description';
    var emptyGridText = 'There are no recordings available or no recordings match specified search value';
    var importButtonText = 'Import';
    var importButtonTooltip = 'Import a new recording';
    var exportButtonText = 'Export';
    var exportButtonTooltip = 'Export selected recordings';
    var editButtonText = 'Edit';
    var addMonitorButtonText = 'Start Monitoring';
    var addMonitorButtonTooltip = 'Start monitoring selected recordings';
    var deleteButtonText = 'Delete';
    var deleteButtonTooltip = 'Delete selected recordings';
    var deleteRecordingTitle = "Delete Recording{0}";
    var deleteRecordingQuestion = "Do you really want to delete selected recording{0}?";
    var deleteTransactionPromptHeader = "Deleting the selected recording{0} will permanently delete {1} associated transaction{2} including:";
    var deleteActionTitle = "Delete Action";
    var deleteActionQuestion = "Do you really want to delete selected action?";
    var importPasswordLabel = 'Password for import';
    var importingMsg = 'Importing recording ...';
    var importSuccessfulTitle = 'Import successful';
    var importErrorTitle = 'Import error';
    var recordingNameLabel = 'Recording name';
    var recordingNameEmptyText = 'Will be loaded from recording file';
    var recordingFileEmptyText = 'Select a file with recording';
    var recordingFileLabel = 'Recording';
    var importRecordingTitle = 'Import recording';
    var exportRecordingTitle = 'Export recording';
    var importRecordingText = '1. Use the recorder to create a recording<br />'
                            + '2. Save the recording to a file<br />'
                            + '3. Upload that file below';
    var importPasswordHelpText = '<span class="SEUM_HelpText">This recording is password protected.</span> <a href="{0}" class="SEUM_HelpLink"  target="_blank">&#0187; Learn more</a>';
    var exportRecordingText = "<b>Do you want to password protect {0}?</b><br />"
                            + "Password protected recordings cannot be imported without the correct password.";
    var cancelLabel = 'Cancel';
    var exportPasswordLabel = 'Password';
    var confirmPasswordLabel = 'Confirm password';
    var importLabel = 'Import';
    var exportLabel = 'Export';
    var mismatchPasswordsMsg = "Entered passwords don't match";
    var emptyPasswordsMsg = "Please enter password!";
    var exportErrorTitle = 'Export error';
    var encryptFieldLabel = 'Yes, password protect this recording';
    var recordingNameLabel = "Recording name";
    var editRecordingTitle = 'Edit recording';
    var editActionLabel = 'Edit action';
    var editWriteActionTitle = 'Edit text';
    var editWriteActionLabel = 'Text';
    var saveLabel = 'Save';
    var savingMsg = "Saving changes ...";
    var uniqueNameError = 'Recording name is not unique. Please enter a unique recording name.';
    var perPageLabel = "Per Page: ";
    var invalidPageSizeText = "Invalid page size";
    var editRecordingStepLabel = 'Edit recording step';
    var stepNameLabel = 'Step name';

    var unsupportedLocationTextShort = 'Unsupported OS';
    var unsupportedLocationText = 'Recording is not supported on selected location. Recordings containing X,Y Capture Mode are supported only on Windows Vista, Windows 7, Windows 2008 or newer.';
    var alreadyAssignedTextShort = 'Already assigned';
    var alreadyAssignedText = 'Recording is already assigned to selected location.';
    // <-- Messages ================================================

    var ChromiumProbe = 2;
    var LegacyProbe = 1;

    getDeleteRecordingPromptHeader = function (recordingsCount, linkedTransactionsCount) {
        return String.format(deleteTransactionPromptHeader, recordingsCount > 1 ? "s" : "", linkedTransactionsCount, linkedTransactionsCount > 1 ? "s" : "");
    }

    GetPageSize = function () {
        var pSize = parseInt(getCookie(userName + '_SEUM_RecordingsGrid_PageSize'), 10);
        return isNaN(pSize) ? pageSize : pSize;
    }

    RefreshObjects = function (initializeSelection) {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: loadingMsg
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {};
        grid.store.baseParams = { start: (initialPage - 1) * GetPageSize(), limit: GetPageSize() };
        grid.store.on('load', function () {
            loadingMask.hide();
            if (initializeSelection)
                UpdateSelection();
        });
        grid.store.load();
    }

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        var isRecording = false;
        if (count == 1) {
            isRecording = selItems.selections.items[0].data.IsRecording;
        }

        var isEditable = false;
        if (count == 1) {
            isEditable = selItems.selections.items[0].data.Entity == recordingStepEntity ||
                            (selItems.selections.items[0].data.Entity == '' && selItems.selections.items[0].data.IsEditable);
        }

        if (!isEditGrid) {
            map.ExportButton.hide();
            map.EditButton.hide();
            map.AddMonitorButton.hide();
            map.DeleteButton.hide();
        }

        map.ImportButton.setDisabled(false);
        map.ExportButton.setDisabled(true);
        map.EditButton.setDisabled(true);
        map.AddMonitorButton.setDisabled(true);
        map.DeleteButton.setDisabled(true);

        if (count === 1 && isRecording) {
            map.ExportButton.setDisabled(false);
            map.EditButton.setDisabled(false);
            map.AddMonitorButton.setDisabled(false);
            map.DeleteButton.setDisabled(false);
        } else if (count === 1 && isEditable) {
            map.EditButton.setDisabled(false);
            if (IsActionRemovable(selItems.selections.items[0].data.Action)) {
                map.DeleteButton.setDisabled(false);
            }
        } else {
            var items = grid.getSelectionModel().getSelections();
            var enableDelete = false;
            Ext.each(items, function (item) {
                if (item.data.IsRecording) {
                    enableDelete = true;
                }
                else {
                    enableDelete = false;
                    return false;   // this equals break on Ext.each
                }
            });
            if (enableDelete) {
                map.AddMonitorButton.setDisabled(false);
                map.DeleteButton.setDisabled(false);
            }
        }
    };

    SetSelectedRecordingsToField = function () {
        var selectedRecordings = '';
        grid.getSelectionModel().each(function (item) {
            if (item.data.IsRecording) {
                if (selectedRecordings != '')
                    selectedRecordings += ',';

                selectedRecordings += item.data.ID;
            }
        });

        var field = $('#' + selectedRecordingsFieldClientID);
        field.val(selectedRecordings);
    };

    UpdateSelection = function () {
        var field = $('#' + selectedRecordingsFieldClientID);
        var selectedRecordings = field.val();
        var ids = selectedRecordings.split(',');
        var selectionModel = grid.getSelectionModel();

        selectionModel.clearSelections();
        Ext.each(ids, function (recordingId) {
            var recordIndex = dataStore.findBy(function (record, id) {
                return record.data.IsRecording && record.data.ID == recordingId;
            });

            selectionModel.selectRow(recordIndex, true);
        });
    };

    IsActionRemovable = function (action) {
        if (action === undefined)
            return false;
        switch (action.ActionType) {
            case 13:    // action Verify content
                return true;
            default:
                return false;
        }
    };

    EditRow = function (item) {
        if (item.data.IsRecording) {    // edit recording
            switch (item.data.ProbeType) {
                case 1: // probeType.Classic
                    $('#editRecordingDownloadLegacyRecorderLink').show();  
                    $('#learnMoreForLegacyRecorder').show();  
                    break;
                case 2: // probeType.Chromium
                    $('#editRecordingDownloadNewRecorderLink').show();
                    $('#learnMoreForNewRecorder').show();
                    break;
            }
            currentEditRecording = item;
            editWindow.show();
        } else if (item.data.Entity == recordingStepEntity) {   // edit recording step
            if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

            currentEditStep = { id: item.data.ID, name: item.data.RawName, recordId: item.id };
            editStepWindow.show();
        } else if (item.data.Entity == '' && item.data.IsEditable) {    // edit action
            if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

            EditAction(item.id, item.data.StepId, item.data.Action);
        }
    };

    EditAction = function (recordId, stepId, action) {
        currentEditAction = { action: action, stepId: stepId, recordId: recordId };

        // swich action types
        switch (action.ActionType) {
            case 7:    // action Write text/password
                editWriteActionWindow.show();
                break;
            case 13:    // action Verify content
                editVerifyActionWindow.show();
                break;
            default:
                break;
        }
    };

    AddMonitor = function (items) {
        var selectedRecordings = '';
        grid.getSelectionModel().each(function (item) {
            if (item.data.IsRecording) {
                if (selectedRecordings != '')
                    selectedRecordings += ',';

                selectedRecordings += item.data.ID;
            }
        });

        window.location = '/Orion/SEUM/Admin/Add/Default.aspx?recordings=' + selectedRecordings;
    };

    ImportRecording = function () {
        importWindow.show();
    }

    ExportRecording = function (item) {
        exportWindow.recordingId = item.data.ID;
      exportWindow.recordingName = item.data.RawName;
      exportWindow.probeType = item.data.ProbeType;
      exportWindow.show();
    }

    UpdateAction = function (stepId, action, onSuccess) {
        var method = null;

        switch (action.ActionType) {
            case 13:    // action Verify content
                method = "UpdateActionVerifyContent";
                params = {
                    stepId: stepId, 
                    actionId: action.Guid, 
                    maxWaitTimeMs: action.MaxWaitTimeMs, 
                    shouldContain: action.ShouldContain, 
                    textToVerify: action.TextToVerify
                };
                break;
            default:
                return;
        }

        ORION.callWebService("/Orion/SEUM/Services/RecordingsGrid.asmx",
                         method, params,
                         function (result) {
                             onSuccess(result);
                         });
    };

    DeleteRecordings = function (ids, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/RecordingsGrid.asmx",
                         "DeleteRecordings", { recordingIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    DeleteAction = function (stepId, actionId, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/RecordingsGrid.asmx",
                         "DeleteAction", { stepId: stepId, actionId: actionId },
                         function (result) {
                             onSuccess();
                         });
    };

    DeleteSelectedItems = function (selectionModel) {
        var items = selectionModel.getSelections();

        if (selectionModel.getCount() == 1 && items[0].data.Entity == '') { // actions
            var waitMsg = Ext.Msg.wait(deletingActionMsg);

            DeleteAction(items[0].data.StepId, items[0].data.Action.Guid, function () {
                waitMsg.hide();
                grid.store.reload();
            });
        } else {    // recordings
            var toDelete = [];

            Ext.each(items, function (item) {
                toDelete.push(item.data.ID);
            });

            var waitMsg = Ext.Msg.wait(deletingRecordingsMsg);

            DeleteRecordings(toDelete, function () {
                waitMsg.hide();
                grid.store.reload();
            });
        }
    };

    GetLoadingRecord = function(level) {
        var blankRecord = Ext.data.Record.create(dataStore.fields);
        var loadingRecord = new blankRecord({
            ID: -1,
            Level: level + 1,
            Name: loadingMsg
        });
        return loadingRecord;
    };

    // HTTP Decode method. Unfortunately Javascript doesn't have built-in routine for this.
    htmlDecode = function(value) {
        return $('<div/>').html(value).text();
    };

    ToggleRow = function(expander, recordId) {
        var item = dataStore.getById(recordId);
        var itemID = item.data.Path;

        if (expandedItems[recordId]) {
            CollapseRow(expander, recordId);
        } else {
            loadingMask.show();
            var items = [];
            items.push(itemID);
            ExpandRow(expander, recordId);
        }
    };

    ExpandRow = function (expander, recordId, callback, items) {
        var rowRecord = dataStore.getById(recordId);
        var store;

        if (rowRecord.data.Entity == recordingEntity) {
            store = new ORION.WebServiceStore(
                    "/Orion/SEUM/Services/RecordingsGrid.asmx/GetSteps",
                    [
                        { name: 'ID', mapping: 0 },
                        { name: 'Name', mapping: 1 },
                        { name: 'Description', mapping: 2 },
                        { name: 'IsRecording', mapping: 3 },
                        { name: 'IsExpandable', mapping: 4 },
                        { name: 'Entity', mapping: 5 },
                        { name: 'Level', mapping: 6 },
                        { name: 'RawName', mapping: 7 }

                    ],
                    "StepOrder");

            store.proxy.conn.jsonData = { recordingId: rowRecord.data.ID };
        }
        else if (rowRecord.data.Entity == recordingStepEntity) {
            store = new ORION.WebServiceStore(
                    "/Orion/SEUM/Services/RecordingsGrid.asmx/GetActions",
                    [
                        { name: 'Name', mapping: 0 },
                        { name: 'Description', mapping: 1 },
                        { name: 'IsRecording', mapping: 2 },
                        { name: 'IsExpandable', mapping: 3 },
                        { name: 'Entity', mapping: 4 },
                        { name: 'Level', mapping: 5 },
                        { name: 'IsEditable', mapping: 6 },
                        { name: 'StepId', mapping: 7 },
                        { name: 'Action', mapping: 8 }
                    ],
                    "");

            store.proxy.conn.jsonData = { recordingStepId: rowRecord.data.ID };
        }


        store.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });


        var insertIndex = dataStore.indexOfId(recordId) + 1;
        dataStore.insert(insertIndex, GetLoadingRecord(parseInt(rowRecord.data.Level)));

        store.addListener("load", function (store) {
            expandedItems[recordId] = true;
            expander.className = 'tree-grid-expander-collapse';

            var toAdd = new Array();

            store.each(function (record) {
                toAdd.push(record);
            });

            dataStore.removeAt(insertIndex); // remove "loading" record
            if (toAdd.length > 0) {
                dataStore.insert(insertIndex, toAdd);
            }

            loadingMask.hide();
        });
        store.load();
    };

    CollapseRow = function (expander, recordId) {
        var rowRecord = dataStore.getById(recordId);
        var index = dataStore.indexOfId(recordId);
        var limit = dataStore.getCount();
        var toRemove = new Array();

        // remove all records in a branch under current record, use Level to check this
        for (var i = index + 1; i < limit; i++) {
            var record = dataStore.getAt(i);
            if (record.data.Level > rowRecord.data.Level) {
                toRemove.push(record);
            } else {
                break;
            }
        }

        dataStore.remove(toRemove);
        delete expandedItems[recordId];
        expander.className = 'tree-grid-expander-expand';
    };

    function renderName(value, meta, record) {

        var id = record.data.ID;
        var level = record.data.Level;
        var expandable = record.data.IsExpandable;
        var entity = record.data.Entity;
        var status = 0;

        var supported = $.inArray(record.data.ID, unsupportedRecordings) == -1;
        var enabled = $.inArray(record.data.ID, excludedRecordings) == -1;

        var result = '';
        for (var i = 0; i < level; i++) {
            result += '<span class="tree-grid-indent-block">';
        }

        if (expandable && enabled) {
            result += String.format('<span><img src="/Orion/images/Pixel.gif" class="tree-grid-expander-expand" onclick="SW.SEUM.RecordingsGrid.toggleRow(this, \'{0}\');">', record.id);
        } else {
            result += '<span class="tree-grid-expander-dummy">';
        }

        if (id === -1) { // "loading" record
            result = String.format('{0} <span class="entityIconBox"><img src="/Orion/images/AJAX-Loader.gif" /></span> {1}', result, value);
        } else if (entity == recordingEntity) {
            if (SEUMIsDemoMode()) {
                result = String.format('{0} <span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={1}&amp;id={2}&amp;status={3}&amp;size=small" />'
                + '</span> {4}', result, entity, id, status, value);
            } else {
                result = String.format('{0} <span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={1}&amp;id={2}&amp;status={3}&amp;size=small" />'
                  + '</span> <a href="javascript: void(0)">{4}</a>', result, entity, id, status, value);
            }
        } else {
            result = String.format('{0} <span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={1}&amp;id={2}&amp;status={3}&amp;size=small" />'
            + '</span> {4}', result, entity, id, status, value);
        }

        result += '</span>';
        for (var i = 0; i < level; i++) {
            result += '</span>';
        }

        if (!supported && (entity == recordingEntity)) {
            result = result + SEUM_HoverHelpLinkHtml({
                    helpLinkText: unsupportedLocationTextShort,
                    html: unsupportedLocationText
                });
        } else if (!enabled && (entity == recordingEntity)) {
            result = result + SEUM_HoverHelpLinkHtml({
                    helpLinkText: alreadyAssignedTextShort,
                    html: alreadyAssignedText
                });
        }

        return renderDefault(result, meta, record);

        var index = $.inArray(record.data.ID, excludedRecordings);
    }

    function renderLocations(value, meta, record) {
        var result = value != '' ? String.format('<a href="/Orion/SEUM/Admin/ManageMonitors.aspx?groupBy=recording&amp;groupByItem={0}">{1}</a>', record.data.ID, value) : value;

        return renderDefault(result, meta, record);
    }

    function renderRecorderType(value, meta, record) {
        var result = "";
        switch (record.data.ProbeType) {
            case 1:
                result = "Deprecated";
                break;
            case 2:
                result = "WPM 3.0+";
                break;
        }
        return renderDefault(result, meta, record);
    }

    function renderRecordingId(value, meta, record) {
        var result = value != '' && record.data.IsRecording ? value : '';

        return renderDefault(result, meta, record);
    }

    function renderDefault(value, meta, record) {
        var index = $.inArray(record.data.ID, excludedRecordings);
        if (index != -1)
            return String.format('<span class="SEUM_DisabledText">{0}</span>', value);

        return value;
    }

    ORION.prefix = "SEUM_RecordingsGrid_";

    return {
        toggleRow: ToggleRow,
        SetPageSize: function (size) {
            pageSize = size;
        },
        SetEditGrid: function (editGrid) {
            isEditGrid = editGrid;
        },
        SetRecordingEntity: function (entity) {
            recordingEntity = entity;
        },
        SetRecordingStepEntity: function (entity) {
            recordingStepEntity = entity;
        },
        SetUserName: function (user) {
            if (user)
                userName = user;
        },
        SetSelectedRecordingsFieldClientID: function (id) {
            selectedRecordingsFieldClientID = id;
        },
        SetImportLearnMoreLink: function (link) {
            importLearnMoreLink = link;
        },
        SetExcludedRecordings: function (idsString) {
            var ids = idsString.split(',');
            excludedRecordings.length = 0;
            if (ids.length > 0 && ids[0]) {
                Ext.each(ids, function (recordingId) {
                    excludedRecordings.push(parseInt(recordingId));
                });
            }
        },
        SetUnsupportedRecordings: function (idsString) {
            var ids = idsString.split(',');
            unsupportedRecordings.length = 0;
            if (ids.length > 0 && ids[0]) {
                Ext.each(ids, function (agentId) {
                    unsupportedRecordings.push(parseInt(agentId));
                    excludedRecordings.push(parseInt(agentId));
                });
            }
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            selectorModel = new Ext.sw.SEUM.grid.CheckboxSelectionModel(
            {
                maxCheckboxLevel: isEditGrid ? 2 : 0,
                disabledRows: excludedRecordings
            });

            selectorModel.on("selectionchange", function () {
                UpdateToolbarButtons();
                SetSelectedRecordingsToField();
            });

            dataStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/RecordingsGrid.asmx/GetRecordingsPaged",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'RawName', mapping: 1 },
                                { name: 'Name', mapping: 2 },
                                { name: 'Locations', mapping: 3 },
                                { name: 'LastUpdate', mapping: 4 },
                                { name: 'Description', mapping: 5 },
                                { name: 'IsRecording', mapping: 6 },
                                { name: 'IsExpandable', mapping: 7 },
                                { name: 'Entity', mapping: 8 },
                                { name: 'Level', mapping: 9 },
                                { name: 'ProbeType', mapping: 10},
                            ],
                            "Name");

            dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: errorTitle,
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });

                loadingMask.hide();
            });

            searchField = new Ext.ux.form.SearchField({
                store: dataStore,
                width: 200,
                emptyText: emptySearchText,
                paramName: 'search'
            });

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 1000,
                value: GetPageSize()
            });

            pageSizeBox.on('change', function (f, numbox, o) {
                var pSize = parseInt(numbox, 10);
                if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                    Ext.Msg.show({
                        title: errorTitle,
                        msg: invalidPageSizeText,
                        icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                    });

                    return;
                }

                if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                    // Cursor determines current page, pointing to the first element displayed on current page
                    // We determine new cursor so that the element it pointed to before is visisble on a new page
                    pagingToolbar.cursor = Math.floor(pagingToolbar.cursor / pSize) * pSize;
                    pagingToolbar.pageSize = pSize;
                    if (pSize * (pagingToolbar.cursor - 1) >= pagingToolbar.getTotalCount() )
                    setCookie(userName + '_SEUM_RecordingsGrid_PageSize', pSize, 'months', 1);
                    pagingToolbar.doLoad(pagingToolbar.cursor);
                }

            });

            pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: GetPageSize(),
                displayInfo: true,
                displayMsg: pagingMsg,
                emptyMsg: noRecordingsPagingMsg,
                listeners: {
                    change: function () {
                        if (this.displayItem) {
                            var count = 0;
                            this.store.each(function (record) {
                                if (record.data.Level == 0)
                                    count++;
                            });
                            
                            var msg = count == 0 ?
                            this.emptyMsg :
                            String.format(
                                this.displayMsg,
                                this.cursor + 1, this.cursor + count, this.store.getTotalCount()
                            );
                            this.displayItem.setText(msg);
                        }
                    }
                },
                items: [
                    '-',
                    perPageLabel,
                    pageSizeBox
                ]
            });

            var locationsColumnHidden = getCookie(userName + '_SEUM_RecordingsGrid_Locations');
            var recorderTypeColumnHidden = getCookie(userName + '_SEUM_RecordingsGrid_RecorderType');
            var descriptionColumnHidden = getCookie(userName + '_SEUM_RecordingsGrid_Description');
            var lastUpdatedColumnHidden = getCookie(userName + '_SEUM_RecordingsGrid_LastUpdate');
            var recordingIdColumnHidden = getCookie(userName + '_SEUM_RecordingsGrid_RecordingId');

            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                    selectorModel,
                    { header: recordingNameHeader, width: 350, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                    { header: idHeader, width: 50, hidden: recordingIdColumnHidden == 'H', sortable: true, align: "center", dataIndex: 'ID', renderer: renderRecordingId },
		            { header: locationsHeader, hidden: locationsColumnHidden == 'H', width: 200, sortable: false, dataIndex: 'Locations', renderer: renderLocations },
                    { header: recorderTypeHeader, hidden: recorderTypeColumnHidden == 'H', width: 200, sortable: true, dataIndex: 'ProbeType', renderer: renderRecorderType },
		            { header: lastUpdatedHeader, hidden: lastUpdatedColumnHidden == 'H', width: 150, sortable: true, dataIndex: 'LastUpdate', renderer: renderDefault },
		            { id: 'Description', header: descriptionHeader, hidden: descriptionColumnHidden == 'H', width: 120, sortable: true, dataIndex: 'Description', renderer: renderDefault }
                ],

                sm: selectorModel,

                layout: 'fit',
                region: 'center',
                autoscroll: true,
                stripeRows: true,
                autoExpandColumn: 'Description',
                viewConfig: {
                    emptyText: emptyGridText,
                    markDirty: false
                },

                tbar: [
					{
					    id: 'ImportButton',
					    text: importButtonText,
					    tooltip: importButtonTooltip,
					    iconCls: 'SEUM_ImportRecordingButton',
					    handler: function () {
					        ImportRecording();
					    }

					}, (isEditGrid) ? '-' : '', {
					    id: 'ExportButton',
					    text: exportButtonText,
					    tooltip: exportButtonTooltip,
					    iconCls: 'SEUM_ExportRecordingButton',
					    handler: function () {
					        ExportRecording(grid.getSelectionModel().getSelected());
					    }
					}, (isEditGrid) ? '-' : '', {
					    id: 'EditButton',
					    text: editButtonText,
					    iconCls: 'SEUM_EditRecordingButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();
					        EditRow(grid.getSelectionModel().getSelected());
					    }
					}, (isEditGrid) ? '-' : '', {
					    id: 'AddMonitorButton',
					    text: addMonitorButtonText,
					    tooltip: addMonitorButtonTooltip,
					    iconCls: 'SEUM_AddMonitorButton',
					    handler: function () {
					        AddMonitor(grid.getSelectionModel().getSelected());
					    }
					}, (isEditGrid) ? '-' : '', {
					    id: 'DeleteButton',
					    text: deleteButtonText,
					    tooltip: deleteButtonTooltip,
					    iconCls: 'SEUM_DeleteRecordingButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

					        var totalTransactionsCount = 0;
					        var ids = [];
					        Ext.each(grid.getSelectionModel().getSelections(), function (item) {
					            if (item.data.IsRecording) {
					                ids.push(item.data.ID);
					                totalTransactionsCount += item.data.Locations.length;
					            }
					        });

					        var selectedRecordingsCount = ids.length;
					        var multiSelect = selectedRecordingsCount > 1;

					        var msgTitle = String.format(deleteRecordingTitle, selectedRecordingsCount > 1? "s": "");
					        var msgQuestion = String.format(deleteRecordingQuestion, selectedRecordingsCount > 1 ? "s" : "");

					        if (totalTransactionsCount == 0) {
					            // choose right question text
					            if (grid.getSelectionModel().getCount() == 1 && !grid.getSelectionModel().getSelections()[0].data.IsRecording) {
					                msgTitle = deleteActionTitle;
					                msgQuestion = deleteActionQuestion;
					            }

					            Ext.Msg.confirm(msgTitle,
                                    msgQuestion,
                                    function (btn, text) {
                                        if (btn == 'yes') {
                                            DeleteSelectedItems(grid.getSelectionModel());
                                        }
                                    });
					        }
					        else {
                                //Get dialog header words depending on data shown
					            var message = getDeleteRecordingPromptHeader(selectedRecordingsCount, totalTransactionsCount);
					            message += '<br /><br /><ul class="SEUM_RecordingsDeleteList">';

					            ORION.callWebService("/Orion/SEUM/Services/RecordingsGrid.asmx",
                                 "GetRecordingsTransactions", { recordingIds: ids, maxCount: 5 },
                                 function (results) {
                                     for (var i = 0; i < results.length; i++) {
                                         message += '<li>' + results[i] + '</li>';
                                     }

                                     message += '</ul><br />Do you want to continue?';

                                     var yesText = Ext.Msg.buttonText.yes;
                                     Ext.Msg.buttonText.yes = 'Yes, delete all';
                                     var dlg = Ext.Msg.show({
                                         title: msgTitle,
                                         msg: message,
                                         buttons: Ext.Msg.YESNO,
                                         icon: Ext.Msg.WARNING,
                                         fn: function (btn, text) {
                                             if (btn == 'yes') {
                                                 DeleteSelectedItems(grid.getSelectionModel());
                                             }
                                         }
                                     }).getDialog();
                                     // this sets "NO" button as default
                                     dlg.defaultButton = 2;
                                     dlg.focus();
                                     Ext.Msg.buttonText.yes = yesText;
                                 });
					        }
					    }
					}, '->',
                        searchField
                    ],

                bbar: pagingToolbar
            });

            grid.getColumnModel().on('hiddenchange', function (cm, index, hidden) {
                setCookie(userName + '_SEUM_RecordingsGrid_' + cm.getDataIndex(index), hidden ? 'H' : 'V', 'days', 1);
            });

            var panel = new Ext.Container({
                renderTo: 'RecordingsGrid',
                height: SEUM_GetManagementGridHeight(),
                layout: 'border',
                items: [grid]
            });
            Ext.EventManager.onWindowResize(function () {
                panel.setHeight(SEUM_GetManagementGridHeight());
                panel.doLayout();
            }, panel);

            RefreshObjects(true);

            UpdateToolbarButtons();

            // --> Create Import recording window
            var importPasswordField = null; // is generated dynamically when needed
            var importPasswordPanel = null;

            function generateImportPasswordField() {
                return new Ext.form.TextField({
                    fieldLabel: importPasswordLabel,
                    name: 'password',
                    inputType: 'password',
                    width: 198,
                    allowBlank: false
                });
            }

            var importButton = new Ext.Button({
                text: importLabel,
                disabled: false,
                handler: function () {
                    if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

                    if (importForm.getForm().isValid()) {
                        importForm.getForm().submit({
                            url: '/Orion/SEUM/Admin/RecordingImporter.ashx',
                            waitMsg: importingMsg,
                            success: function (fp, o) {
                                Ext.Msg.show({
                                    title: importSuccessfulTitle,
                                    msg: o.result.msg,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO
                                });
                                RefreshObjects();
                                importWindow.hide();
                            },
                            failure: function (fp, o) {
                                Ext.Msg.show({
                                    title: importErrorTitle,
                                    msg: o.result.msg,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        });
                    }
                }
            });
            var nameField = new Ext.form.TextField({
                fieldLabel: recordingNameLabel,
                name: 'recordingName',
                emptyText: recordingNameEmptyText,
                width: 198,
                allowBlank: false
            });
            var uploadField = new Ext.ux.form.FileUploadField({
                emptyText: recordingFileEmptyText,
                fieldLabel: recordingFileLabel,
                name: 'recordingFile',
                width: 282,
                allowBlank: false,
                listeners: {
                    fileselected: function (field, v) {
                        if (SEUMIsDemoMode()) return;

                        importButton.disable();
                        nameField.removeClass('SEUM_InvalidRecordingFileField');
                        nameField.setValue(loadingMsg);

                        importForm.getForm().submit({
                            clientValidation: false,
                            url: '/Orion/SEUM/Admin/RecordingImporter.ashx',
                            params: {
                                mode: 'getName'
                            },
                            success: function (fp, o) {
                                nameField.setValue(htmlDecode(o.result.msg));
                                nameField.removeClass('SEUM_InvalidRecordingFileField');

                                if (importPasswordField != null) {
                                    importForm.remove(importPasswordPanel, true);
                                    importWindow.setHeight(203);
                                    importPasswordField = null;
                                    importPasswordPanel = null;
                                }

                                if (o.result.encrypted) {
                                    importPasswordField = generateImportPasswordField();
                                    importPasswordPanel = new Ext.Container({
                                        autoEl: 'div',  // This is the default
                                        layout: 'form',
                                        cls: 'SEUM_RecordingImportPasswordFields',
                                        items: [
                                            importPasswordField,
                                            new Ext.Container({
                                                html: String.format(importPasswordHelpText, importLearnMoreLink),
                                                cls: 'SEUM_ImportPasswordHelpText'
                                            })
                                        ]
                                    });
                                    importForm.add(importPasswordPanel);
                                    importWindow.setHeight(270);
                                }

                                importButton.enable();
                            },
                            failure: function (fp, o) {
                                // jQuery automatically convert some html entities to its string notation (&quot;) etc.
                                // which caused errors when these entities are in Recording name.
                                // I must call HttpEncode on sender side and replace & with &amp;
                                // and then decode it on client
                                nameField.setValue(htmlDecode(o.result.msg));
                                nameField.addClass('SEUM_InvalidRecordingFileField');
                                importButton.disable();

                                if (importPasswordField != null) {
                                    importForm.remove(importPasswordPanel, true);
                                    importWindow.setHeight(203);
                                    importPasswordField = null;
                                    importPasswordPanel = null;
                                }
                            }
                        });
                    }
                }
            });
            var importForm = new Ext.FormPanel({
                layout: 'form',
                labelWidth: 128,
                fileUpload: true,
                padding: '10px 5px 10px 10px',
                border: false,
                items: [
                    uploadField,
                    nameField
                ]
            });

            importWindow = new Ext.Window({
                title: importRecordingTitle,
                width: 450,
                height: 203,
                closeAction: 'hide',
                closable: false,
                modal: true,
                resizable: false,
                cls: 'SEUM_ImportRecordingWindow',

                items: [
                    new Ext.Panel({
                        html: importRecordingText,
                        padding: '10px 10px 5px 10px',
                        border: false
                    }),
                    importForm
                ],

                buttons: [
                importButton,
                {
                    text: cancelLabel,
                    handler: function () {
                        importWindow.hide();
                    }
                }],
                listeners: {
                    beforeshow: function (form) {
                        nameField.reset();
                        uploadField.reset();
                        nameField.removeClass('SEUM_InvalidRecordingFileField');
                        if (importPasswordField != null) {
                            importForm.remove(importPasswordPanel, true);
                            importWindow.setHeight(203);
                            importPasswordField = null;
                        }
                    }
                }
            });
            // <-- Create Import recording window

            // --> Create Export recording window
          var exportPasswordField = generateExportPasswordField();
          var exportPasswordConfirmField = generateExportPasswordConfirmField();
          var exportPasswordPanel = new Ext.Container({
            autoEl: 'div',  // This is the default
            layout: 'form',
            cls: 'SEUM_RecordingExportPasswordFields',
            items: [exportPasswordField, exportPasswordConfirmField]
          });

            function generateExportPasswordField() {
                return new Ext.form.TextField({
                    fieldLabel: exportPasswordLabel,
                    name: 'password',
                    inputType: 'password',
                  width: 200,
                  allowBlank: true
                });
            }
            function generateExportPasswordConfirmField() {
                return new Ext.form.TextField({
                    fieldLabel: confirmPasswordLabel,
                    name: 'confirm_password',
                    inputType: 'password',
                  width: 200,
                  allowBlank: true
                });
            }

			function arePasswordFieldsValid() {
                if (exportPasswordField.getValue() !== exportPasswordConfirmField.getValue()) {
                    Ext.Msg.show({
                        title: errorTitle,
                        msg: mismatchPasswordsMsg,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                } else if (exportPasswordField.getValue() === '') {
                    Ext.Msg.show({
                        title: errorTitle,
                        msg: emptyPasswordsMsg,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }
                return true;
            };

            var exportButton = new Ext.Button({
                text: exportLabel,
                disabled: false,
                handler: function () {
                    if (exportForm.getForm().isValid()) {
                        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();
                        
                        if ((exportWindow.probeType === LegacyProbe && encryptField.checked) ||
                            exportWindow.probeType === ChromiumProbe) {
                            if (arePasswordFieldsValid() === false) return;
                        }

                        exportForm.getForm().submit({
                            url: '/Orion/SEUM/Admin/RecordingExporter.ashx?action=export&recordingId=' + exportWindow.recordingId,
                            failure: function (fp, o) {
                                Ext.Msg.show({
                                    title: exportErrorTitle,
                                    msg: Ext.util.Format.htmlEncode(o.result.msg),
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        });
                        exportWindow.hide();
                    }
                }
            });

            var encryptField = new Ext.form.Checkbox({
                boxLabel: encryptFieldLabel,
              name: 'encrypt',
                id:'encrypt-field',
                inputValue: "1",
                width: 250,
              listeners: {
                check: function (form, checked) {
                  if (exportWindow.probeType !== LegacyProbe) return;
                  if (checked) {
                    exportWindow.setHeight(260);
                    exportPasswordPanel.show();
                  }
                  else {
                    exportWindow.setHeight(180);
                    exportPasswordPanel.hide();
                    exportPasswordField.reset();
                    exportPasswordConfirmField.reset();
                  }
                    }
                }
            });
            var exportForm = new Ext.FormPanel({
                layout: 'form',
                labelWidth: 130,
                fileUpload: true,
                border: false,
                cls: 'SEUM_ExportRecordingForm',
                items: [
                  encryptField, exportPasswordPanel
                ]
            });

            var textPanel = new Ext.Panel({
                html: exportRecordingText,
                border: false
            });

            exportWindow = new Ext.Window({
                title: exportRecordingTitle,
                width: 450,
                height: 260,
                closeAction: 'hide',
                closable: false,
                modal: true,
                resizable: false,
                cls: 'SEUM_ImportRecordingWindow',
                recordingId: 0,
                recordingName: '',
                probeType: null,
                items: [
                    textPanel,
                    exportForm
                ],

                buttons: [
                exportButton,
                {
                    text: cancelLabel,
                    handler: function () {
                        exportWindow.hide();
                    }
                }],
                listeners: {
                  beforeshow: function (form) {
                    if (exportWindow.probeType === LegacyProbe) {
                      exportWindow.setHeight(180);
                      exportPasswordPanel.hide();
                      encryptField.show();
                    } else if (exportWindow.probeType === ChromiumProbe) {
                      exportWindow.setHeight(260);
                      encryptField.hide();
                      exportPasswordPanel.show();
                    }

                    textPanel.update(String.format(exportRecordingText, this.recordingName));
                    encryptField.reset();
                    exportPasswordField.reset();
                    exportPasswordConfirmField.reset();
                  }
                }
            });
            // <-- Create Export recording window

            // --> Create Edit recording window
            var editNameField = new Ext.form.TextField({
                fieldLabel: recordingNameLabel,
                name: 'recordingName',
                width: 250,
                allowBlank: false
            });
            var editForm = new Ext.FormPanel({
                layout: 'form',
                labelWidth: 100,
                fileUpload: true,
                border: false,
                items: [editNameField]
            });

            editWindow = new Ext.Window({
                title: editRecordingTitle,
                width: 450,
                autoHeight: true,
                closeAction: 'hide',
                closable: false,
                modal: true,
                resizable: false,
                cls: 'SEUM_EditRecordingWindow',

                items: [
                    editForm,
                    new Ext.Panel({
                        applyTo: 'editRecordingDescription',
                        border: false
                    })
                ],

                buttons: [
                {
                    text: saveLabel,
                    disabled: false,
                    handler: function () {
                        if (editForm.getForm().isValid()) {
                            var waitMsg = Ext.Msg.wait(savingMsg);

                            ORION.callWebService("/Orion/SEUM/Services/RecordingsGrid.asmx",
                             "EditRecording", { recordingId: currentEditRecording.data.ID, recordingName: editNameField.getValue() },
                             function (result) {
                                 waitMsg.hide();

                                 if (result) {
                                     RefreshObjects();
                                     editWindow.hide();
                                     $('#editRecordingDownloadNewRecorderLink').hide();
                                     $('#editRecordingDownloadLegacyRecorderLink').hide(); 
                                     $('#learnMoreForNewRecorder').hide(); 
                                     $('#learnMoreForLegacyRecorder').hide(); 
                                     currentEditRecording = null;
                                 } else {
                                     Ext.Msg.show({
                                         title: errorTitle,
                                         msg: uniqueNameError,
                                         buttons: Ext.Msg.OK,
                                         icon: Ext.MessageBox.ERROR
                                     });
                                 }
                             });
                        }
                    }
                },
                {
                    text: cancelLabel,
                    handler: function () {
                        editWindow.hide();
                        $('#editRecordingDownloadNewRecorderLink').hide();
                        $('#editRecordingDownloadLegacyRecorderLink').hide(); 
                        $('#learnMoreForNewRecorder').hide(); 
                        $('#learnMoreForLegacyRecorder').hide(); 
                    }
                }],
              listeners: {
                beforeshow: function () {
                  $('#editRecordingDescription').show();
                  editNameField.setValue(currentEditRecording.data.RawName);
                  $('#SEUMopenRecordingLink').click(function () {
                    exportWindow.recordingId = currentEditRecording.data.ID;
                    exportWindow.recordingName = currentEditRecording.data.RawName;
                    exportWindow.probeType = currentEditRecording.data.ProbeType;
                    exportWindow.show();
                  });
                }
              }
            });
            // <-- Create Edit recording window

            // --> Create Edit step name window
            var editStepNameField = new Ext.form.TextField({
                fieldLabel: stepNameLabel,
                name: 'stepName',
                width: 250,
                allowBlank: false
            });

            var editStepForm = new Ext.FormPanel({
                layout: 'form',
                labelWidth: 100,
                fileUpload: true,
                border: false,
                items: [editStepNameField]
            });

            editStepWindow = new Ext.Window({
                title: editRecordingStepLabel,
                width: 450,
                height: 115,
                closable: false,
                modal: true,
                resizable: false,
                cls: 'SEUM_EditRecordingWindow',

                items: [
                    editStepForm
                ],

                buttons: [
                {
                    text: saveLabel,
                    disabled: false,
                    handler: function () {
                        if (editStepForm.getForm().isValid()) {
                            var waitMsg = Ext.Msg.wait(savingMsg);

                            ORION.callWebService("/Orion/SEUM/Services/RecordingsGrid.asmx",
                             "EditRecordingStep", { stepId: currentEditStep.id, stepName: editStepNameField.getValue() },
                             function (result) {
                                 waitMsg.hide();

                                 var record = dataStore.getById(currentEditStep.recordId);
                                 record.set("Name", editStepNameField.getValue());
                                 record.set("RawName", editStepNameField.getValue());
                                 editStepWindow.hide();
                                 currentEditStep = null;
                             });
                        }
                    }
                },
                {
                    text: cancelLabel,
                    handler: function () {
                        editStepWindow.hide();
                        currentEditStep = null;
                    }
                }],
                listeners: {
                    beforeshow: function () {
                        editStepNameField.setValue(currentEditStep.name);
                    }
                }
            });

            // <-- Create Edit step name window

            // --> Create Edit write action window
            var editWriteActionTextField = new Ext.form.TextField({
                fieldLabel: editWriteActionLabel,
                name: 'writeActionText',
                width: 250,
                allowBlank: false
            });

            var editWriteActionPasswordField = new Ext.form.TextField({
                fieldLabel: editWriteActionLabel,
                name: 'writeActionPassword',
                width: 250,
                inputType: 'password',
                allowBlank: false,
                hidden: true
            });

            var editWriteActionForm = new Ext.FormPanel({
                layout: 'form',
                labelWidth: 100,
                fileUpload: true,
                border: false,
                items: [editWriteActionTextField, editWriteActionPasswordField]
            });

            editWriteActionWindow = new Ext.Window({
                title: editWriteActionTitle,
                width: 450,
                height: 115,
                closable: false,
                modal: true,
                resizable: false,
                cls: 'SEUM_EditRecordingWindow',

                items: [
                    editWriteActionForm
                ],

                buttons: [
                {
                    text: saveLabel,
                    disabled: false,
                    handler: function () {
                        var field = (currentEditAction.action.ElementIdentification.ElementType == 21) ? editWriteActionPasswordField : editWriteActionTextField;
                        if (field.isValid()) {
                            var waitMsg = Ext.Msg.wait(savingMsg);

                            ORION.callWebService("/Orion/SEUM/Services/RecordingsGrid.asmx",
                             "UpdateActionTextWrite", { stepId: currentEditAction.stepId, actionId: currentEditAction.action.Guid, text: field.getValue() },
                             function (result) {
                                 waitMsg.hide();

                                 var record = dataStore.getById(currentEditAction.recordId);
                                 record.set("Action", result);
                                 record.set("Name", result.Description);
                                 editWriteActionWindow.hide();
                                 currentEditAction = null;
                             });
                        }
                    }
                },
                {
                    text: cancelLabel,
                    handler: function () {
                        editWriteActionWindow.hide();
                        currentEditAction = null;
                    }
                }],
                listeners: {
                    beforeshow: function () {
                        editWriteActionPasswordField.setValue("");
                        editWriteActionTextField.setValue("");

                        if (currentEditAction.action.ElementIdentification.ElementType == 21) // password field
                        {
                            editWriteActionTextField.hide();
                            editWriteActionPasswordField.show();
                            editWriteActionPasswordField.setValue(currentEditAction.action.Text);
                        }
                        else {
                            editWriteActionTextField.show();
                            editWriteActionTextField.setValue(currentEditAction.action.Text);
                            editWriteActionPasswordField.hide();
                        }
                    }
                }
            });

            // <-- Create Edit write action window

            // --> Create Edit verify action window
            var editVerifyActionWaitTimeField = new Ext.form.TextField({
                applyTo: 'verifyActionWaitTimeText',
                maskRe: /\d/,
                width: 50
            });

            var editVerifyActionTextField = new Ext.form.TextArea({
                applyTo: 'verifyActionText',
                width: 345,
                height: 55,
                allowBlank: false
            });

            var editVerifyActionComboBox = new Ext.form.ComboBox({
                triggerAction: 'all',
                transform: 'verifyActionType',
                forceSelection: true,
                width: 130
            });

            editVerifyActionWindow = new Ext.Window({
                title: editActionLabel,
                width: 385,
                height: 310,
                closable: false,
                modal: true,
                resizable: false,
                cls: 'SEUM_EditRecordingWindow',

                items: [
                    new Ext.Panel({
                        applyTo: 'editVerifyAction',
                        border: false
                    })
                ],

                buttons: [
                {
                    text: saveLabel,
                    disabled: false,
                    handler: function () {
                        if (editVerifyActionWaitTimeField.isValid() && editVerifyActionTextField.isValid() && editVerifyActionComboBox.isValid()) {
                            var waitMsg = Ext.Msg.wait(savingMsg);

                            var actionCopy = currentEditAction.action;
                            actionCopy.MaxWaitTimeMs = editVerifyActionWaitTimeField.getValue() * 1000;
                            actionCopy.TextToVerify = editVerifyActionTextField.getValue();
                            actionCopy.ShouldContain = editVerifyActionComboBox.getValue() == 1 ? true : false;

                            if ($('#verifyActionDefaultWaitTime').is(':checked'))
                                actionCopy.MaxWaitTimeMs = 0;

                            UpdateAction(currentEditAction.stepId, actionCopy, function (result) {
                                waitMsg.hide();

                                // update row
                                var record = dataStore.getById(currentEditAction.recordId);
                                record.set("Name", result.Description);
                                record.set("Action", result);
                                editVerifyActionWindow.hide();
                                currentEditAction = null;
                            });
                        }
                    }
                },
                {
                    text: cancelLabel,
                    handler: function () {
                        $('#verifyActionWaitTimeText').hide();
                        editVerifyActionWindow.hide();
                        currentEditAction = null;
                    }
                }],
                listeners: {
                    beforeshow: function () {
                        $('#editVerifyAction').show();
                        editVerifyActionWaitTimeField.setValue(currentEditAction.action.MaxWaitTimeMs / 1000);
                        editVerifyActionTextField.setValue(currentEditAction.action.TextToVerify);
                        editVerifyActionComboBox.setValue(currentEditAction.action.ShouldContain ? 1 : 0);
                        $('#verifyActionDefaultWaitTime').attr('checked', currentEditAction.action.MaxWaitTimeMs == 0);
                        if (currentEditAction.action.MaxWaitTimeMs == 0) {
                            $('#verifyActionWaitTimeText').hide();
                        } else {
                            $('#verifyActionWaitTimeText').show();
                        }
                    }
                }
            });

            // <-- Create Edit step name window
        }
    };
} ();

Ext.onReady(SW.SEUM.RecordingsGrid.init, SW.SEUM.RecordingsGrid);
