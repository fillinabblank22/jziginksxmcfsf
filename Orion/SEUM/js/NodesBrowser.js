﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.NodesBrowser = function () {
    var errorTitle = "Error";

    var initialized;

    var grid;
    var selectorModel;
    var searchField;
    var pageSizeBox;
    var pagingToolbar;
    var dataStore;

    var initialPage = 1;
    var pageSize = 1;
    var userName = "";
    var loadingMask = null;

    var groupByCombo;
    var groupByStore;
    var groupByCategoriesStore;
    var groupByList;
    var groupingPanel;
    var selectedNodesFieldId;
    var selectedNodes = new Ext.util.MixedCollection(false, function (o) { return o.NodeId != 0 ? o.NodeId : o.Hostname; });

    var selectedHostsTemplate = '<li title="{0}"><table style="width: 100%"><td> <div>&#8226; {0}</div></td><td style="width: 20px;"><img src="/Orion/Discovery/images/icon_delete.gif" style="cursor:pointer;" title="Remove host from list." onclick="SW.SEUM.NodesBrowser.RemoveHost({1});"></td></table></li>';

    GetPageSize = function () {
        var pSize = parseInt(getCookie(userName + '_SEUM_NodesBrowser_PageSize'), 10);
        return isNaN(pSize) ? pageSize : pSize;
    };

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: 'Loading Nodes ...'
            });
        }

        var records = groupByList.getSelectedRecords();
        var groupByItem = '';
        if (records.length > 0)
            groupByItem = records[0].data.Value;

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {
            groupByCategory: groupByCombo.getValue(),
            groupByItem: groupByItem
        };
        grid.store.baseParams = { start: (initialPage - 1) * GetPageSize(), limit: GetPageSize(), search: searchField.getValue() };
        grid.store.on('load', function () {
            loadingMask.hide();
            RefreshSelected();
        });
        grid.store.load();
    };

    RefreshGroupByList = function (groupByCategory, handler) {
        groupByStore.proxy.conn.jsonData = { groupByCategory: groupByCategory };
        if (handler) {
            var internalHandler = function () {
                handler();
                groupByStore.un('load', internalHandler);
            };
            groupByStore.on('load', internalHandler);
        }

        groupByStore.load();
    };

    InitGroupingPanel = function () {

        groupByCategoriesStore = new ORION.WebServiceStore(
                "/Orion/SEUM/Services/DeploymentWizard.asmx/GetGroupByCategories",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Label', mapping: 1 }
                ],
                "Label");

        groupByCategoriesStore.proxy.conn.jsonData = {};

        groupByCombo = new Ext.form.ComboBox({
            store: groupByCategoriesStore,
            valueField: 'Value',
            displayField: 'Label',
            triggerAction: 'all',
            lazyInit: false,
            width: 150,
            editable: false,
            transform: 'groupBySelect',
            hiddenId: 'groupBySelect',
            listeners: {
                select: function (record, index) {
                    RefreshGroupByList(record.value);

                    if (record.value == 'none')
                        RefreshObjects();
                }
            }
        });

        groupByCategoriesStore.load({
            callback: function (r, options, success) {
                groupByCombo.setValue('none');
            }
        });

        var selectPanel = new Ext.Container({
            applyTo: 'SEUM_GroupingPanel',
            region: 'north',
            height: 50,
            layout: 'form'
        });

        var tpl = new Ext.XTemplate(
		    '<tpl for=".">',
                '<div class="SEUM_GroupByItem" id="{Value}">',
                '<tpl if="Type == \'Orion.Nodes.Status\'"><img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;status={Value}&amp;size=small" /> </tpl>',
                '{Label}</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
	    );

        groupByStore = new ORION.WebServiceStore(
                "/Orion/SEUM/Services/DeploymentWizard.asmx/GetGroupByItems",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Label', mapping: 1 },
                    { name: 'Type', mapping: 2 },
                ],
                "Label");

        groupByList = new Ext.DataView({
            cls: 'SEUM_GroupByList',
            border: true,
            store: groupByStore,
            tpl: tpl,
            height: SEUM_GetManagementGridHeight() - selectPanel.getSize().height,
            autoHeight: false,
            autoScroll: true,
            singleSelect: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            itemSelector: 'div.SEUM_GroupByItem',
            listeners: {
                selectionchange: function (view, selections) {
                    if (selections.length > 0)
                        RefreshObjects();
                }
            }
        });

        groupingPanel = new Ext.Panel({
            region: 'west',
            width: 200,
            split: true,
            items: [
                selectPanel,
                groupByList
            ]
        });

        groupingPanel.on('bodyresize', function () {
            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        });
    };

    function renderName(value, meta, record) {
        return renderDefault(
                    String.format('<img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;status={0}&amp;size=small" /> {1}', record.data.Status, record.data.Name),
                meta, record);
    }

    function renderDefault(value, meta, record) {
        return value;
    }

    UpdateSelectedHostsList = function () {
        $('#sw-seum-locations-count').html(selectedNodes.getCount());
        var listhtml = '';

        selectedNodes.each(function (item) {
            listhtml += String.format(selectedHostsTemplate, item.Name, (item.NodeId != 0 ? item.NodeId : ("'" + item.Hostname + "'")));
        });

        $('#sw-seum-hosts-list ul').html(listhtml);
    }

    RefreshSelected = function () {
        var toSelect = [];
        dataStore.data.each(function (record) {
            if (selectedNodes.containsKey(record.data.ID)) {
                toSelect.push(record);
            }
        }, this);
        selectorModel.selectRecords(toSelect);
    };

    UpdateSelectedNodeIds = function () {
        var ids = Array();
        selectedNodes.each(function (item) {
            ids.push(item);
        });

        $('#' + selectedNodesFieldId).val(Ext.util.JSON.encode(ids));

        UpdateSelectedHostsList();
    };

    InitLayout = function () {
        var panel = new Ext.Container({
            renderTo: 'NodesGrid',
            height: SEUM_GetManagementGridHeight(),
            layout: 'border',
            items: [groupingPanel, grid]
        });
        Ext.EventManager.onWindowResize(function () {
            panel.setHeight(SEUM_GetManagementGridHeight());
            panel.doLayout();
        }, panel);
    };

    InitGrid = function () {
        selectorModel = new Ext.grid.CheckboxSelectionModel();

        selectorModel.on('rowselect', function (sm, rowIndex, r) {
            selectedNodes.add({ NodeId: r.data.ID, Name: r.data.Name, StatusName: r.data.StatusName, IpAddress: r.data.IPAddress, Hostname: r.data.DNS, CredentialId: r.data.CredentialId });
            UpdateSelectedNodeIds();
        });

        selectorModel.on('rowdeselect', function (sm, rowIndex) {
            var r = dataStore.data.itemAt(rowIndex);
            selectedNodes.removeKey(r.data.ID);
            UpdateSelectedNodeIds();
        });

        dataStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/DeploymentWizard.asmx/GetNodesPaged",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'Status', mapping: 2 },
                                { name: 'Vendor', mapping: 3 },
                                { name: 'IPAddress', mapping: 4 },
                                { name: 'DNS', mapping: 5 },
                                { name: 'MachineType', mapping: 6 },
                                { name: 'IOSVersion', mapping: 7 },
                                { name: 'StatusIcon', mapping: 8 },
                                { name: 'StatusName', mapping: 9 },
                                { name: 'CredentialId', mapping: 11 }
                            ],
                            "Name");

        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        dataStore.on('datachanged', RefreshSelected);

        searchField = new Ext.ux.form.SearchField({
            store: dataStore,
            width: 200,
            emptyText: 'Search nodes',
            paramName: 'search'
        });

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 1000,
            value: GetPageSize()
        });

        pageSizeBox.on('change', function (f, numbox, o) {
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                Ext.Msg.show({
                    title: errorTitle,
                    msg: invalidPageSizeText,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });

                return;
            }

            if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                // Cursor determines current page, pointing to the first element displayed on current page
                // We determine new cursor so that the element it pointed to before is visisble on a new page
                pagingToolbar.cursor = Math.floor(pagingToolbar.cursor / pSize) * pSize;
                pagingToolbar.pageSize = pSize;
                setCookie(userName + '_SEUM_NodesBrowser_PageSize', pSize, 'months', 1);
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }

        });

        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: GetPageSize(),
            displayInfo: true,
            displayMsg: 'Displaying nodes {0} - {1} of {2}',
            emptyMsg: 'No nodes to display',
            items: [
                    '-',
                    'Per Page: ',
                    pageSizeBox
                ],
            listeners: {
                change: function (paging, params) { $('#SEUM_CurrentPage').val(params.activePage); }
            }
        });

        grid = new Ext.grid.GridPanel({

            store: dataStore,

            columns: [
                    selectorModel,
                    { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                    { id: 'node-name', header: 'Node Name', width: 500, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                    { header: 'Status', width: 80, hideable: false, sortable: true, dataIndex: 'StatusName', renderer: renderDefault }
                ],

            sm: selectorModel,

            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            autoExpandColumn: 'node-name',
            viewConfig: {
                emptyText: 'Either no nodes are available, or no nodes match the specified criteria.'
            },

            tbar: [
					'->',
                        searchField
                    ],

            bbar: pagingToolbar
        });

    };

    ORION.prefix = "SEUM_NodesBrowser_";

    return {
        SetPageSize: function (size) {
            pageSize = size;
        },
        SetUserName: function (user) {
            if (user)
                userName = user;
        },
        SelectedNodesFieldId: function (id) {
            selectedNodesFieldId = id;
            var obj = Ext.util.JSON.decode($('#' + selectedNodesFieldId).val());

            selectedNodes.clear();
            $.each(obj, function (index, value) {
                selectedNodes.add(value);
            });
            UpdateSelectedNodeIds();
        },
        RemoveHost: function (id) {
            selectedNodes.removeKey(id);
            RefreshSelected();
            UpdateSelectedNodeIds();
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            var currentPage = parseInt($('#SEUM_CurrentPage').val());
            if (!isNaN(currentPage))
                initialPage = currentPage;

            InitGroupingPanel();
            InitGrid();
            InitLayout();

            RefreshObjects();

            if (Ext.isIE7 || Ext.isIE8) {   // ie7 & ie8 hack to reselect items in grid on page load
                setTimeout(RefreshObjects, 100);
            }
        }
    };
} ();

Ext.onReady(SW.SEUM.NodesBrowser.init, SW.SEUM.NodesBrowser);