﻿/**
* Ext.sw.grid.CheckboxSelectionModel
*/

Ext.ns('Ext.sw.SEUM.grid');

/**
* Extension to Checkbox selection model for expandable grids 
* to allow checkboxes only for specific levels of nesting.
* @class Ext.sw.SEUM.grid.CheckboxSelectionModel
* @extends Ext.grid.RowSelectionModel
* @constructor
* @param {Object} config The configuration options
*/
Ext.sw.SEUM.grid.CheckboxSelectionModel = function (config) {
    var maxCheckboxLevel = 99;
    var disabledRows = new Array();
    if (config != null) {
        if (config.maxCheckboxLevel != null)
            maxCheckboxLevel = config.maxCheckboxLevel;

        if (config.disabledRows != null)
            disabledRows = config.disabledRows;
    }

    // call parent
    Ext.sw.SEUM.grid.CheckboxSelectionModel.superclass.constructor.call(this, config);

    // force renderer to run in this scope
    this.renderer = function (v, p, record) {
        var level = 0;
        var id = 0;
        if (record.data != null) {
            if (record.data.Level != null)
                level = record.data.Level;
            if (record.data.ID != null)
                id = record.data.ID;
        }

        if (level > maxCheckboxLevel)
            return '';

        if (this.isDisabled(id))
            return '';

        return Ext.sw.SEUM.grid.CheckboxSelectionModel.superclass.renderer.apply(this, arguments);
    } .createDelegate(this);

    this.isDisabled = function (id) {
        var i = disabledRows.length;
        while (i--) {
            if (disabledRows[i] == id) {
                return true;
            }
        }
        return false;
    }

    this.canBeSelected = function (record) {
        if (record == null || record.data == null)
            return true;

        if (record.data.Level != null && record.data.Level > maxCheckboxLevel)
            return false;

        if (record.data.ID != null && this.isDisabled(record.data.ID))
            return false;

        return true;
    }

};                  // end of constructor

Ext.extend(Ext.sw.SEUM.grid.CheckboxSelectionModel, Ext.grid.CheckboxSelectionModel, {
    listeners: {
        beforerowselect: function (obj, rowIndex, keepExisting, record) {
            if (!this.canBeSelected(record))
                return false;
        }
    }
});