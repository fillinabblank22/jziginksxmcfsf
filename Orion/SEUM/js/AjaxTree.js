﻿SEUM = {};

SEUM.AjaxTree = function () {
    var nodeTemplateId = [];
    var noDataTemplateIds = [];
    var treeNodeIdPrefixes = [];
    var treeRoot = [];
    var rememberExpandedGroups = [];
    var maxBatchSize = [];
    var showDescription = [];
    var stripeRows = [];
    var filters = [];
    var nodeIdCounter = 0;
    var showMoreSingularMessages = [];
    var showMorePluralMessages = [];
    var treeServices = [];
    var expandRootLevelFlags = [];
    var isDemoFlags = [];

    Load = function (treeService, elementId, resId, treeNodeIdPrefix, treeNodeTemplateId, noDataTemplateId, rememberExpanded, batchSize, filter, showMoreSingularMessage, showMorePluralMessage, showDescriptions, xstripeRows, expandRootLevel, isDemo) {
        nodeTemplateId[resId] = treeNodeTemplateId;
        noDataTemplateIds[resId] = noDataTemplateId;
        treeNodeIdPrefixes[resId] = treeNodeIdPrefix;
        rememberExpandedGroups[resId] = rememberExpanded;
        maxBatchSize[resId] = batchSize;
        filters[resId] = filter;
        showMoreSingularMessages[resId] = showMoreSingularMessage;
        showMorePluralMessages[resId] = showMorePluralMessage;
        treeRoot[resId] = $('#' + elementId);
        treeServices[resId] = treeService;
        showDescription[resId] = showDescriptions;
        stripeRows[resId] = xstripeRows;
        expandRootLevelFlags[resId] = expandRootLevel;
        isDemoFlags[resId] = isDemo;

        SEUM_CreateRootLevelBatch(resId, 0);
    }

    SEUM_CreateTreeLevelBatch = function (resId, nodeId, fromIndex, expandAll) {
        var moreNode = $("#" + SEUM_GetUniqueNodeID(resId, nodeId + '_' + 'showMore'));

        if (moreNode) {
            moreNode.remove();
        }

        var node = new SEUM_Node(resId, nodeId);

        treeServices[resId].GetTreeNodes(resId, node.itemType(), node.itemId(), fromIndex, maxBatchSize[resId], filters[resId], function (items, e) {
            var loadingNode = $("#" + SEUM_GetUniqueNodeID(resId, 'loadingNode'));
            if (loadingNode) {
                loadingNode.remove();
            }

            if ((items.length > 0) && (node.expanded())) {
                SEUM_CreateTreeLevel(resId, nodeId, items, true);

                if (items[items.length - 1].Id == '') {
                    if (expandAll) {
                        node.addChild(SEUM_CreateLoadingNode(resId).getNode());
                        setTimeout(function () { SEUM_CreateTreeLevelBatch(resId, nodeId, fromIndex + items.length - 1, true); }, 100);
                    }
                }
            }
        });
    }

    SEUM_CreateRootLevelBatch = function (resId, fromIndex, expandAll) {
        treeServices[resId].GetTreeNodes(resId, '', 0, fromIndex, maxBatchSize[resId], filters[resId], function (items, e) {
            var loadingNode = $("#" + SEUM_GetUniqueNodeID(resId, 'loadingNode'));
            if (loadingNode) {
                loadingNode.remove();
            }

            if (fromIndex == 0) {
                treeRoot[resId].find("*").remove();
            }

            if (items.length > 0) {
                SEUM_CreateRootLevel(resId, items);
                if (items[items.length - 1].Id == '') {
                    treeRoot[resId].append(SEUM_CreateLoadingNode(resId).getNode());
                    setTimeout(function () { SEUM_CreateRootLevelBatch(resId, fromIndex + items.length - 1); }, 100);
                }
            }
            else if (noDataTemplateIds[resId]) {
                treeServices[resId].GetShowGettingStarted(resId, filters[resId], function (show, e) {
                    if (!show)
                        $('#' + SEUM_GetNoDataElementID(resId) + ' .SEUM_GettingStartedBox').hide();
                    $('#' + SEUM_GetNoDataElementID(resId)).show();
                });
            }
        }, function (error) {
            // handle error only if it's FaultException or SwisQueryException
            if (error.get_exceptionType().match("^System.ServiceModel.FaultException") || error.get_exceptionType().match("^SolarWinds.Orion.Web.InformationService.SwisQueryException")) {
                // show error node
                var resourceErrorNode = $("#" + SEUM_GetResourceErrorID(resId));
                if (resourceErrorNode) {
                    resourceErrorNode.show();
                }

                // remove loading
                var loadingNode = $("#" + SEUM_GetUniqueNodeID(resId, 'loadingNode'));
                if (loadingNode) {
                    loadingNode.remove();
                }
                treeRoot[resId].find("center[type='loading_container']").remove();
            } else { // rethrow
                throw error;
            }

        });
    }

    SEUM_CreateRootLevel = function (resId, items) {
        for (var i = 0; i < items.length; i++) {
            var node;
            if (items[i].Id != '') {
                node = SEUM_CreateTreeNode(
                    resId,
                    items[i].Id,
                    0,
                    items[i].Name,
                    items[i].Entity,
                    items[i].Status,
                    items[i].IsExpandable,
                    items[i].Description,
                    items[i].Id,
                    items[i].ItemType,
                    (i % 2 != 0));
            } else {
                continue;
            }

            treeRoot[resId].append(node.getNode());

            if (expandRootLevelFlags[resId]
                || (isDemoFlags[resId] && i == 0)) // demo should have first node expanded
            {
                SEUM_ExpandNode(resId, node.id(), true);
            } else if (rememberExpandedGroups[resId]) {
                treeServices[resId].IsExpanded(resId, node.id(), function (nodeId, eventArgs) {
                    if (nodeId != '') {
                        SEUM_ExpandNode(resId, nodeId, true);
                    }
                });
            }
        }

        SEUM_MakeZebraStripes(resId);
    }

    SEUM_CreateTreeLevel = function (resId, parentNodeId, items, checkForExpansion) {
        var node = new SEUM_Node(resId, parentNodeId);
        for (var i = 0; i < items.length; i++) {
            var newNodeID;
            var newNode;

            if (items[i].Id != '') {
                newNode = SEUM_CreateTreeNode(
                    resId,
                    newNodeID = parentNodeId + '-' + items[i].Id,
                    parseInt(node.level()),
                    items[i].Name,
                    items[i].Entity,
                    items[i].Status,
                    items[i].IsExpandable,
                    items[i].Description,
                    items[i].Id,
                    items[i].ItemType,
                    (i % 2 != 0));

                node.addChild(newNode.getNode());

            } else {
                node.addChild(SEUM_CreateShowMoreNode(resId, parentNodeId, parseInt(items[items.length - 1].Name),
                        function () {
                            SEUM_CreateTreeLevelBatch(resId, parentNodeId, items.length - 1, true);
                        }).getNode());
                continue;
            }

            if (checkForExpansion && rememberExpandedGroups[resId] && items[i].IsExpandable) {
                treeServices[resId].IsExpanded(resId, newNode.id(), function (nodeId, eventArgs) {
                    if (nodeId != '') {
                        SEUM_ExpandNode(resId, nodeId, checkForExpansion);
                    }
                });
            }
        }

        SEUM_MakeZebraStripes(resId);
    }

    SEUM_CreateTreeNode = function (resId, nodeId, parentLevel, name, entity, status, isExpandable, description, itemId, itemType, even) {
        var node = new SEUM_Node(resId, nodeTemplateId[resId]);
        node.id(nodeId); // this clones node
        node.level(parentLevel + 1);
        node.itemId(itemId);
        node.name(name);

        if (showDescription[resId])
            node.description(description);

        node.isExpandable(isExpandable);
        node.itemType(itemType);
        node.statusIcon('/Orion/StatusIcon.ashx?size=small&entity=' + entity + '&id=' + itemId + '&status=' + status);
        node.onExpand(function () { SEUM_Toggle(resId, nodeId); });

        if (itemId.startsWith("group-") && !name.startsWith("group-"))
            node.onClick(function () { SEUM_Toggle(resId, nodeId); });

        node.setEven(even);
        node.show();

        return node;
    }

    SEUM_CreateLoadingNode = function (resId) {
        var node = new SEUM_Node(resId, nodeTemplateId[resId]);
        node.id('loadingNode'); // this clones node
        node.level(0);
        node.name('Loading...');
        node.description('');
        node.isExpandable(false);
        node.statusIcon('/Orion/images/AJAX-Loader.gif');
        node.show();

        return node;
    }

    SEUM_CreateShowMoreNode = function (resId, parentId, numMore, callback) {
        var node = new SEUM_Node(resId, nodeTemplateId[resId]);
        node.id(parentId + '_' + 'showMore'); // this clones node
        node.level(0);
        if (numMore == 1)
            node.name(String.format(showMoreSingularMessages[resId], numMore));
        else
            node.name(String.format(showMorePluralMessages[resId], numMore));
        node.description('');
        node.isExpandable(false);
        node.statusIcon(null);
        node.onClick(callback);
        node.show();

        return node;
    }

    SEUM_Toggle = function (resId, nodeId) {
        var node = new SEUM_Node(resId, nodeId);
        if (node.expanded()) {
            node.collapse();
            if (rememberExpandedGroups) {
                treeServices[resId].ChangeTree(resId, node.id(), false);
            }
        } else {
            node.addChild(SEUM_CreateLoadingNode(resId).getNode());
            node.expand();

            SEUM_CreateTreeLevelBatch(resId, nodeId, 0);
            if (rememberExpandedGroups[resId]) {
                treeServices[resId].ChangeTree(resId, node.id(), true);
            }
        }

        SEUM_MakeZebraStripes(resId);
    }

    SEUM_MakeZebraStripes = function (resId) {
        if (!stripeRows[resId])
            return;

        $(".SEUM_TransactionsTreeNode-itemHeader:odd").addClass("ZebraStripe");
        $(".SEUM_TransactionsTreeNode-itemHeader:even").removeClass("ZebraStripe");
    }

    SEUM_GetResourceErrorID = function (resId) {
        return treeNodeIdPrefixes[resId] + resId + '-ResourceError';
    }

    SEUM_GetUniqueNodeID = function (resId, nodeId) {
        return treeNodeIdPrefixes[resId] + resId + '-' + SEUM_GetSafeID(nodeId);
    }

    SEUM_GetNoDataElementID = function (resId) {
        return treeNodeIdPrefixes[resId] + resId + '-' + noDataTemplateIds[resId];
    }

    SEUM_GetSafeID = function (id) {
        return id.replace(/[^\w-]/g, ''); // element id can't non-word characters
    }

    SEUM_ExpandNode = function (resId, nodeId, checkForExpansion) {
        var n = new SEUM_Node(resId, nodeId);

        n.addChild(SEUM_CreateLoadingNode(resId).getNode());
        n.expand();

        treeServices[resId].GetTreeNodes(resId, n.itemType(), n.itemId(), 0, maxBatchSize[resId], filters[resId], function (items, e) {
            n.childrenDiv.children().remove();
            SEUM_CreateTreeLevel(resId, n.id(), items, checkForExpansion);
        });

        n.expand();
    }

    function SEUM_Node(resId, nodeId) {
        this.resId = resId;
        this.elementId = SEUM_GetUniqueNodeID(resId, nodeId);

        this.node = $('#' + this.elementId);
        this.expandButton = this.node.find('.SEUM_TransactionsTreeNode-expandButton');
        this.childrenDiv = $('#' + this.elementId + '-children');
        this.expandable = true;

        this.id = function (id) {
            if (typeof id == 'undefined') {
                return this.node.attr('nodeId');
            } else {
                this.elementId = SEUM_GetUniqueNodeID(this.resId, id);

                this.node = this.node.clone();
                this.expandButton = this.node.find('.SEUM_TransactionsTreeNode-expandButton');
                this.node.attr('id', this.elementId);
                this.node.attr('nodeId', id);
                this.childrenDiv = this.node.children('div.SEUM_TransactionsTreeNode-children')
                this.childrenDiv.attr('id', this.elementId + '-children');
            }
        },
        this.elementId = function () {
            return this.elementId;
        },
        this.expanded = function () {
            return this.node.hasClass('expanded');
        },
        this.isExpandable = function (isExpandable) {
            if (typeof isExpandable == 'undefined') {
                return this.expandable;
            } else {
                this.expandable = isExpandable;
                if (!isExpandable) {
                    this.expandButton.css('display', 'none');
                } else {
                    this.expandButton.css('display', 'inline');
                }

            }
        },
        this.itemId = function (itemId) {
            if (typeof itemId == 'undefined') {
                return this.node.attr("itemId");
            } else {
                this.node.attr('itemId', itemId);
            }
        },
        this.level = function (level) {
            if (typeof level == 'undefined') {
                return this.node.attr("level");
            } else {
                this.node.attr('level', level);
                this.node.addClass('SEUM_Level' + level);
            }
        },
        this.itemType = function (type) {
            if (typeof type == 'undefined') {
                return this.node.attr("itemType");
            } else {
                this.node.attr('itemType', type);
            }
        },
        this.expand = function () {
            this.node.addClass('expanded');
            this.expandButton.attr('src', '/Orion/images/Button.Collapse.gif');
            this.childrenDiv.slideDown('fast');
        },
        this.collapse = function () {
            this.childrenDiv.slideUp('fast');
            this.node.removeClass('expanded');
            this.expandButton.attr('src', '/Orion/images/Button.Expand.gif');
            this.childrenDiv.children().remove();
        },
        this.name = function (name) {
            if (typeof name == 'undefined') {
                return this.node.find('.SEUM_TransactionsTreeNode-itemName').html();
            } else {
                this.node.find('.SEUM_TransactionsTreeNode-itemName').html(name);
            }
        },
        this.description = function (description) {
            if (typeof description == 'undefined') {
                return this.node.find('.SEUM_TransactionsTreeNode-itemDescription').html();
            } else {
                this.node.find('.SEUM_TransactionsTreeNode-itemDescription').html(description);
            }
        },
        this.statusIcon = function (src) {
            if (typeof src == 'undefined') {
                return this.node.find('img.SEUM_TransactionsTreeNode-statusIcon').attr('src');
            } else if (src === null) {
                this.node.find('img.SEUM_TransactionsTreeNode-statusIcon').hide();
            } else {
                this.node.find('img.SEUM_TransactionsTreeNode-statusIcon').attr('src', src);
            }
        },
        this.show = function () {
            this.node.show();
        },
        this.getNode = function () {
            return this.node;
        },
        this.onClick = function (handler) {
            var self = this;

            this.node.find('.SEUM_TransactionsTreeNode-itemName').wrap('<a href="javascript:void(0);" class="SEUM_TransactionsTreeNode-clickWrapper"></a>');

            this.node.find('a.SEUM_TransactionsTreeNode-clickWrapper').click(
            function () {
                handler();
            });
        },
        this.onExpand = function (handler) {
            var self = this;
            if (self.isExpandable()) {
                this.node.find('a.SEUM_TransactionsTreeNode-expandLink').click(
                function () {
                    if (self.isExpandable())
                        handler();
                });
            }
        },
        this.addChild = function (child) {
            this.childrenDiv.append(child);
        },
        this.setEven = function (even) {
            if (even)
                this.node.addClass('SEUM_Even');
            else
                this.node.addClass('SEUM_Odd');
        }
    }

    return {
        Load: Load
    };
} ();
