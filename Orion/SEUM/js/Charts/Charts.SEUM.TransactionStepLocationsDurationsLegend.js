﻿SW = SW || {};
SW.SEUM = SW.SEUM || {};
SW.SEUM.Charts = SW.SEUM.Charts || {};
SW.SEUM.Charts.TransactionStepLocationsDurationsLegend = SW.SEUM.Charts.TransactionStepLocationsDurationsLegend || {};
(function (legend) {
    legend.createStandardLegend = function (chart, legendContainerId) {
        var table = $('#' + legendContainerId);
        var numTransactions = chart.series.length;

        table.html("");
        for (var i = 0; i < numTransactions; i++) {
            if (chart.series[i].options.id == 'highcharts-navigator-series')
                continue;

            
            var stepName = '';
            if(chart.options.steps[i].link != null) {
                stepName = String.format('<a href="{0}">{1}</a>', chart.options.steps[i].link, Ext.util.Format.htmlEncode(chart.options.steps[i].name));
            } else {
                stepName = Ext.util.Format.htmlEncode(chart.options.steps[i].name);
            }

            var row = $('<tr />').appendTo(table);
            var td = $('<td class="Property SEUM_Property SEUM_LegendColorIconCell" />').appendTo(row);
            var legendIcon = $('<span class="SEUM_LegendColorIcon" style="background-color: ' + chart.series[i].color + '" />').appendTo(td);
            td = $('<td class="Property SEUM_Property SEUM_LegendItemName" />').appendTo(row);
            td.html(stepName);
            td = $('<td class="Property SEUM_Property" />').appendTo(row);
            td.text(chart.options.steps[i].transactionName);
            td = $('<td class="Property SEUM_Property" />').appendTo(row);
            td.html('<span class="' + chart.options.steps[i].customClass + '">' + chart.options.steps[i].currentDuration + '</span>');
            td = $('<td class="Property SEUM_Property" />').appendTo(row);
            td.html(chart.options.steps[i].averageDuration);
        }
    };

} (SW.SEUM.Charts.TransactionStepLocationsDurationsLegend));