﻿SW.Core.Charts = SW.Core.Charts || {}; 
SW.Core.Charts.dataFormatters = SW.Core.Charts.dataFormatters || {}; 
SW.Core.Charts.dataFormatters.duration = function (value, axis) {
	if (value >= 1000) {
		return (value/1000).toFixed(1) + 's';
	} else {
		return parseFloat(value).toFixed(1) + 'ms';
	}
}