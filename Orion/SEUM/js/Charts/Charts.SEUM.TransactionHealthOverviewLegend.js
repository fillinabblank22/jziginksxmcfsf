﻿SW = SW || {};
SW.SEUM = SW.SEUM || {};
SW.SEUM.Charts = SW.SEUM.Charts || {};
SW.SEUM.Charts.TransactionHealthOverviewLegend = SW.SEUM.Charts.TransactionHealthOverviewLegend || {};
(function (legend) {
    legend.createStandardLegend = function (chart, treeService, clientId, showMoreSingular, showMorePlural, batchSize) {
        var resourceId = chart.options.resourceId;
        var resourceFilterErrorClass = 'SEUM_ResourceError';
        var resourceFilterErrorText = 'Data for this resource can\'t be loaded. If you have custom filter defined, check that it is correct.';

        $('#' + clientId).html(
            '<div class="SEUM_TransactionsTree" ID="SEUMTree-Tree-' + resourceId + '" runat="server">' +
            '	<center type="loading_container">' +
            '		<img alt="Loading..." src="/Orion/images/AJAX-Loader.gif" /> Loading...' +
            '	</center>' +
            '</div>' +
            '<div id="SEUMTreeNode-' + resourceId + '-ResourceError" class="' + resourceFilterErrorClass + '" style="display: none;">' +
            resourceFilterErrorText +
            '</div>' +
            '<div class="SEUM_StatusTableRowContainer" id="SEUMTreeNode-' + resourceId + '-TransactionsTreeNode" style="display: none;">' +
            '	<div class="SEUM_StatusTableRow">' +
            '        <a href="javascript:void(0);" class="SEUM_TransactionsTreeNode-expandLink">' +
            '		    <span class="SEUM_TransactionsTreeNode-expandButtonWrapper"><img src="/Orion/images/Button.Expand.gif" class="SEUM_TransactionsTreeNode-expandButton" /></span>' +
            '		    <img class="SEUM_TransactionsTreeNode-statusIcon" />' +
            '		</a>' +
            '		<div class="SEUM_TransactionsTreeNode-itemName"></div>' +
            '        <div class="SEUM_Cleaner"></div>' +
            '    </div>' +
            '	<div class="SEUM_TransactionsTreeNode-children" style="display: none; padding-left: 30px;"></div>' +
            '</div>');

	    SEUM.AjaxTree.Load(treeService,
                                    'SEUMTree-Tree-' + resourceId, 
                                    resourceId, 
                                    'SEUMTreeNode-', 
                                    'TransactionsTreeNode', 
                                    null,
                                    chart.options.rememberExpanded,
                                    batchSize,
                                    chart.options.filter,
                                    showMoreSingular,
                                    showMorePlural,
                                    false);
    };

} (SW.SEUM.Charts.TransactionHealthOverviewLegend));