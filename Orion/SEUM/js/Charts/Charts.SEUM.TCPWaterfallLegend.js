﻿SW = SW || {}; 
SW.SEUM = SW.SEUM || {}; 
SW.SEUM.Charts = SW.SEUM.Charts || {}; 
SW.SEUM.Charts.TCPWaterfallLegend = SW.SEUM.Charts.TCPWaterfallLegend || {}; 
(function (legend) {
    legend.legendHeader = 'Legend:';
    legend.redirectRequestText = '3xx HTTP Status Codes';
    legend.errorRequestText = '4xx, 5xx HTTP Status Codes';

    legend.toggleSeries = function(series) {
        series.visible ? series.hide() : series.show();
    };
    
    legend.addTitle = function(nameAndColor, container) {
        var label = $('<span/>');
        label.text(nameAndColor.name);
        label.appendTo(container);
    };

    legend.addLegendSymbol = function(nameAndColor, container) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
        
        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
                .attr({
                    stroke: nameAndColor.color,
                    fill: nameAndColor.color
                })
                .add();

        label.appendTo(container);
    };

    legend.createStandardLegend = function(chart, legendContainerId) {
        var errorCodes = new Array({ name: legend.errorRequestText, color: 'rgba(255,0,0,0.2)'}, { name: legend.redirectRequestText, color: 'rgba(255,255,0,0.3)' });

        var legendItems = new Array();
        
        // generate array of items for legend
        for (var i = chart.series.length-1; i >= 0; i--) {
            var series = chart.series[i];
            
            if (!series.options.showInLegend)
                continue;
            
            legendItems.push({ name: series.name, color: series.color });
        }
        
        for (var i = errorCodes.length-1; i >= 0; i--) {
            var errorCode = errorCodes[i];
            legendItems.push({ name: errorCode.name, color: errorCode.color });
        }
        
        var table = $('#' + legendContainerId);
        table.html("");
        var header = $('<tr />').appendTo(table);;
        var th = $('<th colspan="4" />').appendTo(header);
        th.text(legend.legendHeader);

        var numItems = legendItems.length;
        var halfIndex = Math.ceil(numItems / 2);

        // loop for adding rows to the legends table
        for (var i = 0; i < halfIndex; i++) {
            var leftItem = legendItems[i];
            var rightItem = (halfIndex + i < numItems) ? legendItems[halfIndex + i] : null;
            
            var row = $('<tr/>');
            
            var td = $('<td style="width:20px;"/>').appendTo(row);
            legend.addLegendSymbol(leftItem, td);
            
            td = $('<td/>').appendTo(row);
            legend.addTitle(leftItem, td);

            if(rightItem != null) {
                td = $('<td style="width:20px;"/>').appendTo(row);
                legend.addLegendSymbol(rightItem, td);
            
                td = $('<td/>').appendTo(row);
                legend.addTitle(rightItem, td);
            } else {
                $('<td colspan="2" />').appendTo(row);
            }

            row.appendTo(table);
        }

    };
    

    
    
} (SW.SEUM.Charts.TCPWaterfallLegend));