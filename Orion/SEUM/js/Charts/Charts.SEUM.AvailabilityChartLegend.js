﻿SW = SW || {};
SW.SEUM = SW.SEUM || {};
SW.SEUM.Charts = SW.SEUM.Charts || {};
SW.SEUM.Charts.AvailabilityChartLegend = SW.SEUM.Charts.AvailabilityChartLegend || {};
(function (legend) {

    legend.toggleSeries = function(series) {
        series.visible ? series.hide() : series.show();
    };

    legend.addCheckbox = function(series, container) {
        
        var check = $('<input type="checkbox"/>')
                    .click(function() { legend.toggleSeries(series); });
        
        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };

    legend.addTitle = function(series, container) {
        var label = $('<span/>');
        label.text(series.name);
        label.appendTo(container);
    };

    legend.addLegendSymbol = function(series, container) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        label.html('&nbsp;');

        label.css({
            height: symbolHeight,
            width: symbolWidth,
            'background-color': series.color.stops[series.color.stops.length - 1][1],
            display: 'inline-block'
        });

        label.appendTo(container);
    };

    legend.createStandardLegend = function(chart, legendContainerId) {
        var div = $('#' + legendContainerId);
        div.html("");
        for (var i = chart.series.length - 1; i >= 0 ; i--) {
            var series = chart.series[i];

            if (!series.options.showInLegend)
                continue;
            
            var wrapper = $('<span/>')

            var elem = $('<span style="width:20px;"/>').appendTo(wrapper);
            legend.addLegendSymbol(series, elem);
            
            elem = $('<span class="sw-seum-title"/>').appendTo(wrapper);
            legend.addTitle(series, elem);

            wrapper.appendTo(div);
        }
    };
} (SW.SEUM.Charts.AvailabilityChartLegend));