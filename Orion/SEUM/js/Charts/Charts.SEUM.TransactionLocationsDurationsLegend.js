﻿SW = SW || {};
SW.SEUM = SW.SEUM || {};
SW.SEUM.Charts = SW.SEUM.Charts || {};
SW.SEUM.Charts.TransactionLocationsDurationsLegend = SW.SEUM.Charts.TransactionLocationsDurationsLegend || {};
(function (legend) {
    legend.createStandardLegend = function (chart, legendContainerId) {
        var table = $('#' + legendContainerId);
        var numTransactions = chart.series.length;

        table.html("");
        for (var i = 0; i < numTransactions; i++) {
            if (chart.series[i].options.id == 'highcharts-navigator-series')
                continue;

            var row = $('<tr />').appendTo(table);
            var td = $('<td class="Property SEUM_Property SEUM_LegendColorIconCell" />').appendTo(row);
            var legendIcon = $('<span class="SEUM_LegendColorIcon" style="background-color: ' + chart.series[i].color + '" />').appendTo(td);
            td = $('<td class="Property SEUM_Property SEUM_LegendItemName" />').appendTo(row);
            if (chart.options.transactions[i].link != null) {
                td.html(String.format('<a href="{0}"></a>', chart.options.transactions[i].link))
                td.find("a").text(chart.options.transactions[i].name);
            } else {
                td.text(chart.options.transactions[i].name);
            }
            td = $('<td class="Property SEUM_Property" />').appendTo(row);
            td.html('<span class="' + chart.options.transactions[i].customClass + '">' + chart.options.transactions[i].currentDuration + '</span>');
            td = $('<td class="Property SEUM_Property" />').appendTo(row);
            td.html(chart.options.transactions[i].averageDuration);
        }
    };

}(SW.SEUM.Charts.TransactionLocationsDurationsLegend));