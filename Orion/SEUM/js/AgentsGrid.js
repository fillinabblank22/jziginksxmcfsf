﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.AgentsGrid = function () {
    var deleteTitleSingle = "Delete Location";
    var deleteTitleMultiple = "Delete Locations";
    var deletePromptSingle = "Do you really want to delete selected location?";
    var deletePromptMultiple = "Do you really want to delete selected locations?";
    var deletePromptSingleHeader = "Deleting the selected location will permanently delete {0} associated transactions including:";
    var deletePromptMultipleHeader = "Deleting the selected locations will permanently delete {0} associated transactions including:";
    var addMonitorButtonText = 'Assign monitors';
    var addMonitorButtonTooltip = 'Assign monitors to selected locations';
    var modeActive = "Player initiated communication";
    var modePassive = "Server initiated communication";
    var perPageLabel = "Per Page: ";
    var errorTitle = "Error";
    var invalidPageSizeText = "Invalid page size";
    var waitingAgentsSingularMessageFormat =
        'There is {0} player waiting for add. You can add it by clicking <a class="sw-link SEUM_EnabledLink" href="/Orion/SEUM/Admin/EditPlayer.aspx?mode=active">here</a>.'
        + ' If you want to hide this message in the future, click <a class="sw-link SEUM_EnabledLink" href="javascript:void(0);" onclick="SW.SEUM.AgentsGrid.HideNotificationRow()">here</a>';
    var waitingAgentsPluralMessageFormat =
        'There are {0} players waiting for add. You can add them by clicking <a class="sw-link SEUM_EnabledLink" href="/Orion/SEUM/Admin/EditPlayer.aspx?mode=active">here</a>.'
        + ' If you want to hide this message in the future, click <a class="sw-link SEUM_EnabledLink" href="javascript:void(0);" onclick="SW.SEUM.AgentsGrid.HideNotificationRow()">here</a>';
    var waitingAgentsSingularNotificationFormat = '<a class="sw-link SEUM_EnabledLink" href="javascript:void(0);" onclick="SW.SEUM.AgentsGrid.ShowNotificationRow()">{0} player waiting for add</a>';
    var waitingAgentsPluralNotificationFormat = '<a class="sw-link SEUM_EnabledLink" href="javascript:void(0);" onclick="SW.SEUM.AgentsGrid.ShowNotificationRow()">{0} players waiting for add</a>';
    var unsupportedLocationTextShort = 'Unsupported OS';
    var unsupportedLocationText = 'Selected recording is not supported on this location. Recordings containing X,Y Capture Mode are supported only on Windows Vista, Windows 7, Windows 2008 or newer.';
    var alreadyAssignedTextShort = 'Already assigned';
    var alreadyAssignedText = 'Selected recording is already assigned to this location.';
    var forceUpdateWaitMsg = 'Forcing update of players ...';
    var forceUpdatePromptTitle = 'Force automatic update';
    var forceUpdateButtonText = 'Force Player Update';
    var forceUpdateButtonTooltip = 'Forces the player to update.';
    var forceUpdateMsgSingular = 'Do you really want to force an update of the selected player?';
    var forceUpdateMsgPlural = 'Do you really want to force an update of the selected players?';

    var forceRedeployWaitDialogText = 'Forcing deployment of players ...';
    var forceRedeployButtonTooltip = 'Forces the player to deploy.';
    var forceRedeployWindowTitle = 'Force Player Deployment - Assign Credential';
    var forceRedeployButtonText = 'Force Player Deployment';

    var monitorSingularStr = 'monitor';
    var monitorPluralStr = 'monitors';
    var nodesPluralStr = 'nodes';

    var selectorModel;
    var dataStore;
    var initialPage = 1;
    var pagingToolbar;
    var grid;
    var searchField;
    var initialized;
    var excludedAgents = new Array();
    var unsupportedAgents = new Array();
    var pageSizeBox;
    var forceUpdateAllowed = false;
    var forceRedeployAllowed = false;

    var selectedAgentsFieldClientID;

    // active agents service lock
    var activeAgentsCallInProgress = false;

    // force deploy stuff
    var forceDeployWindow;
    var credentialStore;
    var credentialsCombo;
    var usernameField;
    var passwordField;
    var selectedCredential = -1;

    var pageSize = 1;
    var userName = "";

    var isEditGrid = true;

    var loadingMask = null;

    var returnUrl = null;

    var loadWarningThreshold = 80;
    var loadErrorThreshold = 100;

    var relatedNodesVisible = false;

    var task;
    var runner;

    AddAgent = function () {
        window.location = '/Orion/SEUM/Admin/AddPlayer.aspx?returnUrl=' + returnUrl;
    }

    EditAgent = function (item) {
        window.location = '/Orion/SEUM/Admin/EditPlayer.aspx?id=' + item.data.ID;
    }

    GetPageSize = function () {
        var pSize = parseInt(getCookie(userName + '_SEUM_AgentsGrid_PageSize'), 10);
        return isNaN(pSize) ? pageSize : pSize;
    }

    RefreshObjects = function (initializeSelection) {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: "Loading data..."
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {};
        grid.store.baseParams = { start: (initialPage - 1) * GetPageSize(), limit: GetPageSize() };
        grid.store.on('load', function () {
            loadingMask.hide();
            if (initializeSelection)
                UpdateSelection();
        });
        grid.store.load();
    };

    UpdateAgentsWithPendingOperation = function () {
        // check if agent is in one of the states that signalize pending operation
        // 6 - Cloud installation
        // 9 - Update pending
        // 10 - Update uploading
        // 11 - Update in progress
        // 13 - Deployment pending
        // 14 - Deployment in progress
        // 15 - Waiting for service to start
        var nodeIndex = dataStore.find("ConnectionStatus", /^6|9|10|11|13|14$/);
        if (nodeIndex < 0)    // no agents with pending operation in grid -> skip
            return;

        dataStore.each(function (record) {
            var recordCopy = record;
            if (record.data.ConnectionStatus == 6 || record.data.ConnectionStatus == 9
                    || record.data.ConnectionStatus == 10 || record.data.ConnectionStatus == 11
                    || record.data.ConnectionStatus == 13 || record.data.ConnectionStatus == 14) { // only load new data for agents with pending operation
                GetAgent(record.data.ID, function (result, context) {
                    if (result.ConnectionStatus == 12) {   // if update failed show force button
                        forceUpdateAllowed = true;
                        context.record.set("ForceUpdateSupported", true);
                        UpdateToolbarButtons();
                    }
                    if (result.ConnectionStatus == 15) {   // if deployment failed show force button
                        forceRedeployAllowed = true;
                        context.record.set("RedeploySupported", true);
                        UpdateToolbarButtons();
                    }
                    context.record.set("ConnectionStatusMessage", result.ConnectionStatusMessage);
                    context.record.set("ConnectionStatus", result.ConnectionStatus);
                    context.record.set("AgentVersion", result.AgentVersion.Major + '.' + result.AgentVersion.Minor + '.' + result.AgentVersion.Build + '.' + result.AgentVersion.Revision);
                }, { 'record': recordCopy });
            }
        });
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        if (!isEditGrid) {
            map.EditButton.hide();
            map.DeleteButton.hide();
            map.AddMonitorButton.hide();
        }

        if (forceUpdateAllowed && isEditGrid) {
            map.UpdateAgentButton.setVisible(true);
            map.UpdateAgentSeparator.setVisible(true);

            var disable = false;
            // enable update button only if all selected agents are updateable
            Ext.each(selItems.getSelections(), function (item) {
                if (!item.data.ForceUpdateSupported) {
                    disable = true;
                    return false;   // break
                }
            });

            map.UpdateAgentButton.setDisabled(count > 0 ? disable : true);
        }

        if (forceRedeployAllowed && isEditGrid) {
            map.DeployAgentButton.setVisible(true);
            map.DeployAgentSeparator.setVisible(true);

            var disable = false;
            // enable redeploy button only if all selected agents are redeployable
            Ext.each(selItems.getSelections(), function (item) {
                if (!item.data.RedeploySupported) {
                    disable = true;
                    return false;   // break
                }
            });

            map.DeployAgentButton.setDisabled(count > 0 ? disable : true);
        }

        map.AddButton.setDisabled(false);
        map.EditButton.setDisabled(true);
        map.DeleteButton.setDisabled(true);
        map.AddMonitorButton.setDisabled(true);

        if (count === 1) {
            map.EditButton.setDisabled(false);
            map.DeleteButton.setDisabled(false);
            map.AddMonitorButton.setDisabled(false);
        } else if (count > 1) {
            map.DeleteButton.setDisabled(false);
            map.AddMonitorButton.setDisabled(false);
        }
    };

    SetSelectedAgentsToField = function () {
        var selectedAgents = '';
        grid.getSelectionModel().each(function (item) {
            if (selectedAgents != '')
                selectedAgents += ',';

            selectedAgents += item.data.ID;
        });

        var field = $('#' + selectedAgentsFieldClientID);
        field.val(selectedAgents);
    };

    UpdateSelection = function () {
        var field = $('#' + selectedAgentsFieldClientID);
        var selectedAgents = field.val();
        var ids = selectedAgents.split(',');
        var selectionModel = grid.getSelectionModel();

        // count only valid records (value > 0)
        var excludedAgentsLen = 0;
        $.each(excludedAgents, function (index, value) {
            if (value > 0)
                excludedAgentsLen++;
        });

        if (ids.length > 0 && ids[0]) {
            selectionModel.clearSelections();
            Ext.each(ids, function (agentId) {
                var recordIndex = dataStore.findBy(function (record, id) {
                    return record.data.ID == agentId;
                });

                selectionModel.selectRow(recordIndex, true);
            });
        } else if (!isEditGrid && (dataStore.getTotalCount() - excludedAgentsLen == 1)) {
            // preselect single enabled record in the grid
            dataStore.each(function (record) {
                if (record.data.ID > 0 && $.inArray(record.data.ID, excludedAgents) == -1) {
                    var index = dataStore.find("ID", record.data.ID);
                    if (index > -1) {
                        selectionModel.selectRow(index, true);
                        return false;
                    }
                }
            });
        }
    };

    DeployWithExistingCredential = function (items, credentialId) {
        var toAssign = [];

        Ext.each(items, function (item) {
            toAssign.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(forceRedeployWaitDialogText);

        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                         "DeployWithExistingCredential", { agentIds: toAssign, credentialId: credentialId },
                         function (result) {
                             waitMsg.hide();
                             grid.store.reload();
                         });
    };

    DeployWithNewCredential = function (items, username, password) {
        var toAssign = [];

        Ext.each(items, function (item) {
            toAssign.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(forceRedeployWaitDialogText);

        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                        "DeployWithNewCredential", { agentIds: toAssign, userName: username, password: password },
                        function (result) {
                            waitMsg.hide();
                            grid.store.reload();
                        });
    };

    AddMonitor = function (items) {
        var selectedAgents = '';
        grid.getSelectionModel().each(function (item) {
            if (selectedAgents != '')
                selectedAgents += ',';

            selectedAgents += item.data.ID;
        });

        window.location = '/Orion/SEUM/Admin/Add?agents=' + selectedAgents;
    };

    GetAgent = function (id, onSuccess, context) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                        "GetAgent", { agentId: id },
                        function (result) {
                            onSuccess(result, context);
                        });
    };

    CallDeleteAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                         "DeleteAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         },
                         function (msg) {
                             activeAgentsCallInProgress = false; // reset flag if call failed
                         });
    };

    CallGetActiveAgentsWaitingForAdd = function (onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                        "GetActiveAgentsWaitingForAdd", {},
                        function (result) {
                            onSuccess(result);
                        });
    };

    DeleteSelectedItems = function (items) {
        var toDelete = [];

        Ext.each(items, function (item) {
            toDelete.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait("Deleting locations ...");

        CallDeleteAgents(toDelete, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    ForceUpdateSelectedItems = function (items) {
        var toUpdate = [];

        Ext.each(items, function (item) {
            if (item.data.ForceUpdateSupported)
                toUpdate.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(forceUpdateWaitMsg);

        ForceUpdateOfAgents(toUpdate, function () {
            waitMsg.hide();
            RefreshObjects();
        });
    };

    ForceUpdateOfAgents = function (ids, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                         "UpdateAgents", { agentIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    FilterNotificationItems = function () {
        agentsNotificationPanel.update('');

        if (getCookie(userName + '_SEUM_AgentsGrid_HideActiveAgentsNotification') == 1) {
            grid.store.filterBy(function (record, id) {
                if (record.data.ID == -1) {
                    if (record.data.Name == 1)
                        agentsNotificationPanel.update(String.format(waitingAgentsSingularNotificationFormat, record.data.Name));
                    else
                        agentsNotificationPanel.update(String.format(waitingAgentsPluralNotificationFormat, record.data.Name));

                    return false;
                }

                return true;
            });
        } else {
            grid.store.clearFilter();
        }
    }

    function renderName(value, meta, record, rowIndex) {
        if (record.data.ID == -1) { // this record contains number of waiting agents
            if (value == 1)
                value = String.format(waitingAgentsSingularMessageFormat, value);
            else
                value = String.format(waitingAgentsPluralMessageFormat, value);

            // wrap into span and make it relative so that it can span over multiple columns
            value = '<span style="position: absolute; top: 3px; left: 25px;">' + value + '</span>';
        } else {
            if (isEditGrid) {
                value = String.format('<a href="/Orion/SEUM/TransactionLocationDetails.aspx?NetObject=L:{0}">{1}</a>', record.data.ID, record.data.Name);
            } else {
                value = record.data.Name;
            }

            var index1 = $.inArray(record.data.ID, unsupportedAgents);
            var index2 = $.inArray(record.data.ID, excludedAgents);
            if (index1 != -1) {
                value = value + SEUM_HoverHelpLinkHtml({
                    helpLinkText: unsupportedLocationTextShort,
                    html: unsupportedLocationText
                });
            } else if (index2 != -1) {
                value = value + SEUM_HoverHelpLinkHtml({
                    helpLinkText: alreadyAssignedTextShort,
                    html: alreadyAssignedText
                });
            }
        }

        return renderDefault(value, meta, record);
    }

    function renderStatus(value, meta, record) {
        if (record.data.ID < 0)
            return;

        var message = record.data.ConnectionStatusMessage;
        var icon;
        var cls = '';

        switch (value) {
            case 1: icon = '/Orion/images/ok_16x16.gif'; break;
            case 6:     // cloud
            case 9:     // update pending
            case 10:    // update uploading
            case 11:    // update in progress
            case 13:    // deployment pending
            case 14:    // deployment in progress
                icon = '/Orion/images/loading_gen_small.gif'; break;
            case 12:    // update failed
                cls += ' Error';
                icon = '/Orion/SEUM/Images/StatusIcons/Small-Agent-UpdateFailed.png';
                break;
            case 15:    // deployment failed
                cls += ' Error';
            default: icon = '/Orion/images/failed_16x16.gif';
        }

        return renderDefault(
        String.format('<span title="{2}" class="{3}"><img src="{0}" /> {1}</span>', icon, message, Ext.util.Format.stripTags(message), cls),
        meta,
        record);
    }

    function renderAssignedMonitors(value, meta, record) {
        if (record.data.ID < 0)
            return;

        var monitorStr = monitorSingularStr;
        if (value != 1)
            monitorStr = monitorPluralStr;

        var str = renderDefault(String.format("{0} {1}", value, monitorStr), meta, record);
        return String.format('<a href="/Orion/SEUM/Admin/ManageMonitors.aspx?groupBy=location&amp;groupByItem={0}">{1}</a>',
            record.data.ID, str);
    }

    function renderAssignedNodes(value, meta, record) {
        if (record.data.ID < 0)
            return;

        var result;
        if (value) {
            result = value.length == 0 ? "" : value.length == 1 ? value[0] : value.length + " " + nodesPluralStr;
        }
        return renderDefault(result, meta, record);
    }

    function renderLoadPercentage(value, meta, record) {
        if (value >= loadErrorThreshold)
            return renderDefault(String.format('<span class="Error">{0}%</span>', value), meta, record);

        if (value >= loadWarningThreshold)
            return renderDefault(String.format('<span class="Warning">{0}%</span>', value), meta, record);

        return renderDefault(String.format('{0}%', value), meta, record);
    }

    function renderMode(value, meta, record) {
        if (record.data.ID < 0)
            return;

        return renderDefault(value ? modeActive : modePassive, meta, record);
    }

    function renderPort(value, meta, record) {
        if (record.data.ID < 0)
            return;

        return renderDefault(record.data.IsActiveAgent ? "-" : value, meta, record);
    }

    function renderDefault(value, meta, record) {
        if (typeof (value) == 'undefined')
            return '';

        var index = $.inArray(record.data.ID, excludedAgents);
        if (index != -1)
            return String.format('<span class="SEUM_DisabledText">{0}</span>', value);

        return value;
    }

    function textFieldsValidator(value) {
        if (selectedCredential != -1)
            return true;

        if (value == null || value == '')
            return 'Field must not be empty.';

        return true;
    }

    InitForceDeployWindow = function () {
        credentialStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/DeploymentWizard.asmx/GetCredentials",
                            [
                                { name: 'Key', mapping: 0 },
                                { name: 'Value', mapping: 1 }
                            ],
                            "Key");

        credentialStore.proxy.conn.jsonData = {};

        credentialsCombo = new Ext.form.ComboBox({
            store: credentialStore,
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            lazyInit: false,
            width: 230,
            editable: false,
            transform: 'CredentialNamesCombo',
            hiddenId: 'CredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedCredential = index.data.Key;
                    usernameField.validate();
                    passwordField.validate();
                    if (selectedCredential == -1) {
                        $('#ForceDeployWindow tr.sw-seum-hideable').show();
                    } else {
                        $('#ForceDeployWindow tr.sw-seum-hideable').hide();
                    }

                    forceDeployWindow.syncShadow();
                }
            }
        });

        credentialStore.load({
            callback: function (r, options, success) {
                credentialsCombo.setValue(-1);
            }
        });

        usernameField = new Ext.form.TextField({
            applyTo: "CredentialUsername",
            width: 230,
            validator: textFieldsValidator
        });

        passwordField = new Ext.form.TextField({
            applyTo: "CredentialPassword",
            width: 230,
            validator: textFieldsValidator
        });

        forceDeployWindow = new Ext.Window({
            title: forceRedeployWindowTitle,
            width: 400,
            autoHeight: true,
            closable: false,
            modal: true,
            resizable: false,

            items: [
                    new Ext.Panel({
                        applyTo: 'ForceDeployWindow',
                        border: false
                    })
                ],

            buttons: [
                {
                    text: "Submit",
                    disabled: false,
                    handler: function () {
                        if (selectedCredential != -1) { // new credential
                            DeployWithExistingCredential(grid.getSelectionModel().getSelections(), selectedCredential);
                        } else {    // existing credential
                            if (!usernameField.isValid() || !passwordField.isValid())
                                return;

                            var username = usernameField.getValue();
                            var password = passwordField.getValue();

                            DeployWithNewCredential(grid.getSelectionModel().getSelections(), username, password);
                        }
                        forceDeployWindow.hide();
                    }
                },
                {
                    text: "Cancel",
                    handler: function () {
                        forceDeployWindow.hide();
                        usernameField.setValue('');
                        passwordField.setValue('');
                    }
                }],
            listeners: {
                beforeshow: function () {
                    $('#ForceDeployWindow').show();
                    $('#ForceDeployWindow tr.sw-seum-hideable').show();

                    if (grid.getSelectionModel().getCount() != 1) {
                        credentialsCombo.setValue(-1);
                    } else {
                        var record = grid.getSelectionModel().getSelected();
                        if (record.data.CredentialId > 0) {
                            credentialsCombo.setValue(record.data.CredentialId);
                            $('#ForceDeployWindow tr.sw-seum-hideable').hide();
                        } else {
                            credentialsCombo.setValue(-1);
                        }
                    }
                    usernameField.setValue('');
                    usernameField.clearInvalid();
                    passwordField.setValue('');
                    passwordField.clearInvalid();
                }
            }
        });
    };

    ORION.prefix = "SEUM_AgentsGrid_";

    return {
        SetPageSize: function (size) {
            pageSize = size;
        },
        SetEditGrid: function (editGrid) {
            isEditGrid = editGrid;
        },
        SetUserName: function (user) {
            if (user)
                userName = user;
        },
        SetSelectedAgentsFieldClientID: function (id) {
            selectedAgentsFieldClientID = id;
        },
        SetExcludedAgents: function (idsString) {
            var ids = idsString.split(',');
            excludedAgents.length = 0;
            if (ids.length > 0 && ids[0]) {
                Ext.each(ids, function (agentId) {
                    excludedAgents.push(parseInt(agentId));
                });
            }
        },
        SetUnsupportedAgents: function (idsString) {
            var ids = idsString.split(',');
            unsupportedAgents.length = 0;
            if (ids.length > 0 && ids[0]) {
                Ext.each(ids, function (agentId) {
                    unsupportedAgents.push(parseInt(agentId));
                    excludedAgents.push(parseInt(agentId));
                });
            }
        },
        SetRelatedNodesVisible: function(visible) {
            relatedNodesVisible = visible;
        },
        HideNotificationRow: function () {
            setCookie(userName + '_SEUM_AgentsGrid_HideActiveAgentsNotification', 1, 'months', 1);
            FilterNotificationItems();
        },
        ShowNotificationRow: function () {
            setCookie(userName + '_SEUM_AgentsGrid_HideActiveAgentsNotification', 0, 'months', 1);
            FilterNotificationItems();
        },
        SetReturnUrl: function (url) {
            returnUrl = url;
        },
        SetAgentLoadWarningThreshold: function (threshold) {
            loadWarningThreshold = threshold;
        },
        SetAgentLoadErrorThreshold: function (threshold) {
            loadErrorThreshold = threshold;
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            selectorModel = new Ext.sw.SEUM.grid.CheckboxSelectionModel(
            {
                disabledRows: excludedAgents
            });

            selectorModel.on("selectionchange", function () {
                UpdateToolbarButtons();
                SetSelectedAgentsToField();
            });

            dataStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/AgentsGrid.asmx/GetAgentsPaged",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'Url', mapping: 2 },
                                { name: 'ConnectionStatus', mapping: 3 },
                                { name: 'ConnectionStatusMessage', mapping: 4 },
                                { name: 'Port', mapping: 5 },
                                { name: 'PollingEngineId', mapping: 6 },
                                { name: 'PollingEngineName', mapping: 7 },
                                { name: 'AssignedMonitors', mapping: 8 },
                                { name: 'IsActiveAgent', mapping: 9 },
                                { name: 'AgentGuid', mapping: 10 },
                                { name: 'Hostname', mapping: 11 },
                                { name: 'DNSName', mapping: 12 },
                                { name: 'IP', mapping: 13 },
                                { name: 'AgentVersion', mapping: 14 },
                                { name: 'ForceUpdateSupported', mapping: 15 },
                                { name: 'RedeploySupported', mapping: 16 },
                                { name: 'LoadPercentage', mapping: 17 },
                                { name: 'AssignedNodes', mapping: 18 }
                            ],
                            "Name");

            dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: "Error",
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });

                loadingMask.hide();
            });

            dataStore.addListener("add", function (store, records, options) {
                FilterNotificationItems();
            });

            dataStore.addListener("load", function (store, records, options) {
                if (isEditGrid && !activeAgentsCallInProgress) {
                    // add notification record for active agents
                    activeAgentsCallInProgress = true; // allow only one service call at time
                    CallGetActiveAgentsWaitingForAdd(function (agents) {
                        if (agents.length > 0) {
                            NotificationRecord = Ext.data.Record.create([
                                { name: "ID", type: "string" },
                                { name: "Name", type: "string" }
                            ]);
                            var record = new NotificationRecord({
                                ID: -1,
                                Name: agents.length
                            });

                            store.insert(0, record);
                        }

                        activeAgentsCallInProgress = false;
                    });
                }
                if (isEditGrid) {
                    // check if there is at least one record with agent which can be force-updated or force-redeployed, if it is then show appropriate button(s)
                    forceUpdateAllowed = false;
                    forceRedeployAllowed = false;
                    Ext.each(records, function (record) {
                        if (record.data.ForceUpdateSupported) {
                            forceUpdateAllowed = true;
                        }
                        if (record.data.RedeploySupported) {
                            forceRedeployAllowed = true;
                        }
                        if (forceUpdateAllowed && forceRedeployAllowed) {
                            return false;   // break;
                        }
                    });

                    UpdateToolbarButtons();
                }
            });

            agentsNotificationPanel = new Ext.Panel({
                html: '',
                border: false,
                bodyCssClass: 'SEUM_ActiveAgentsNotificationLabel'
            });

            searchField = new Ext.ux.form.SearchField({
                store: dataStore,
                width: 200,
                emptyText: 'Search all locations ...',
                paramName: 'search'
            });

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 1000,
                value: GetPageSize()
            });

            pageSizeBox.on('change', function (f, numbox, o) {
                var pSize = parseInt(numbox, 10);
                if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                    Ext.Msg.show({
                        title: errorTitle,
                        msg: invalidPageSizeText,
                        icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                    });

                    return;
                }

                if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                    // Cursor determines current page, pointing to the first element displayed on current page
                    // We determine new cursor so that the element it pointed to before is visisble on a new page
                    pagingToolbar.cursor = Math.floor(pagingToolbar.cursor / pSize) * pSize;
                    pagingToolbar.pageSize = pSize;
                    setCookie(userName + '_SEUM_AgentsGrid_PageSize', pSize, 'months', 1);
                    pagingToolbar.doLoad(pagingToolbar.cursor);
                }

            });

            pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: GetPageSize(),
                displayInfo: true,
                displayMsg: 'Displaying locations {0} - {1} of {2}',
                emptyMsg: "No locations to display",
                items: [
                    '-',
                    perPageLabel,
                    pageSizeBox
                ]
            });
            
            var addressColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_Url');
            var statusColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_ConnectionStatus');
            var monitorsColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_AssignedMonitors');
            var portColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_Port');
            var pollerColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_PollingEngineName');
            var modeColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_IsActiveAgent');
            var guidColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_AgentGuid');
            var hostnameColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_Hostname');
            var dnsnameColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_DNSName');
            var ipColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_IP');
            var assignedNodesColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_AssignedNodes');
            var versionColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_AgentVersion');
            var loadColumnHidden = getCookie(userName + '_SEUM_AgentsGrid_LoadPercentage');
            var columns = [
                selectorModel,
                { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                { header: 'Location Name', width: 300, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                { header: 'IP Address or Hostname', hidden: addressColumnHidden == 'H', width: 200, sortable: true, dataIndex: 'Url', renderer: renderDefault },
                { header: 'Location Status', hidden: statusColumnHidden == 'H', width: 400, sortable: true, dataIndex: 'ConnectionStatus', renderer: renderStatus },
                { header: 'Assigned Monitors', hidden: monitorsColumnHidden == 'H', width: 130, sortable: true, dataIndex: 'AssignedMonitors', renderer: renderAssignedMonitors },
                { header: 'Port', hidden: (portColumnHidden == 'H') || !portColumnHidden, width: 80, sortable: true, dataIndex: 'Port', renderer: renderPort },
                { header: 'Orion Poller', hidden: (pollerColumnHidden == 'H') || !pollerColumnHidden, width: 150, sortable: true, dataIndex: 'PollingEngineName', renderer: renderDefault },
                { header: 'Player Load', hidden: loadColumnHidden == 'H', width: 80, sortable: true, dataIndex: 'LoadPercentage', renderer: renderLoadPercentage },
                { header: 'Mode', hidden: (modeColumnHidden == 'H') || !modeColumnHidden, width: 200, sortable: true, dataIndex: 'IsActiveAgent', renderer: renderMode },
                { header: 'Guid', hidden: (guidColumnHidden == 'H') || !guidColumnHidden, width: 250, sortable: true, dataIndex: 'AgentGuid', renderer: renderDefault },
                { header: 'Version', hidden: (versionColumnHidden == 'H'), width: 90, sortable: true, dataIndex: 'AgentVersion', renderer: renderDefault },
                { header: 'Hostname', hidden: (hostnameColumnHidden == 'H') || !hostnameColumnHidden, width: 200, sortable: true, dataIndex: 'Hostname', renderer: renderDefault },
                { header: 'DNS Name', hidden: (dnsnameColumnHidden == 'H') || !dnsnameColumnHidden, width: 200, sortable: true, dataIndex: 'DNSName', renderer: renderDefault },
                { header: 'IP Address', hidden: (ipColumnHidden == 'H') || !ipColumnHidden, width: 180, sortable: true, dataIndex: 'IP', renderer: renderDefault }
            ];
            /*if (relatedNodesVisible) {
                columns.push({ header: 'Related Nodes', hidden: (assignedNodesColumnHidden == 'H') || !assignedNodesColumnHidden, width: 120, sortable: true, dataIndex: 'AssignedNodes', renderer: renderAssignedNodes });
            }*/
            columns.push({ id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 });
            grid = new Ext.grid.GridPanel({

                store: dataStore,
                columns: columns,
                sm: selectorModel,
                layout: 'fit',
                region: 'center',
                autoscroll: true,
                stripeRows: true,
                autoExpandColumn: 'filler',
                viewConfig: {
                    emptyText: 'There are no locations available or no locations match specified search value',
                    getRowClass: function (record, index) {
                        if (record.data.ID == -1)
                            return "SEUM_NotificationGridRow";
                    },
                    markDirty: false
                },

                tbar: [
					{
					    id: 'AddButton',
					    text: 'Add Location',
					    iconCls: 'SEUM_AddAgentButton',
					    handler: function () {
					        AddAgent();
					    }

					}, (isEditGrid) ? '-' : '', {
					    id: 'EditButton',
					    text: 'Edit',
					    iconCls: 'SEUM_EditAgentButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();
					        EditAgent(grid.getSelectionModel().getSelected());
					    }
					}, (isEditGrid) ? '-' : '', {
					    id: 'AddMonitorButton',
					    text: addMonitorButtonText,
					    tooltip: addMonitorButtonTooltip,
					    iconCls: 'SEUM_AddMonitorButton',
					    handler: function () {
					        AddMonitor(grid.getSelectionModel().getSelected());
					    }
					}, (isEditGrid) ? '-' : '', {
					    id: 'DeleteButton',
					    text: 'Delete',
					    iconCls: 'SEUM_DeleteAgentButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

					        var multiSelect = grid.getSelectionModel().getCount() > 1;

					        var totalTransactionsCount = 0;
					        var ids = [];
					        Ext.each(grid.getSelectionModel().getSelections(), function (item) {
					            totalTransactionsCount += item.data.AssignedMonitors;
					            ids.push(item.data.ID);
					        });


					        if (totalTransactionsCount == 0) {
					            // No transactions will be deleted, show just normal question
					            var dlg = Ext.Msg.confirm(multiSelect ? deleteTitleMultiple : deleteTitleSingle,
								    multiSelect ? deletePromptMultiple : deletePromptSingle,
								    function (btn, text) {
								        if (btn == 'yes') {
								            DeleteSelectedItems(grid.getSelectionModel().getSelections());
								        }
								    }).getDialog();
					            // this sets "NO" button as default
					            dlg.defaultButton = 2;
					            dlg.focus();
					        }
					        else {
					            // some transactions will be deleted - show full warning
					            var message = String.format(multiSelect ? deletePromptMultipleHeader : deletePromptSingleHeader, totalTransactionsCount);
					            message += '<br /><br /><ul class="SEUM_LocationDeleteList">';

					            ORION.callWebService("/Orion/SEUM/Services/AgentsGrid.asmx",
                                 "GetAgentsTransactions", { agentIds: ids, maxCount: 5 },
                                 function (results) {
                                     for (var i = 0; i < results.length; i++) {
                                         message += '<li>' + results[i] + '</li>';
                                     }

                                     message += '</ul><br />Do you want to continue?';

                                     var yesText = Ext.Msg.buttonText.yes;
                                     Ext.Msg.buttonText.yes = 'Yes, delete all';
                                     var dlg = Ext.Msg.show({
                                         title: multiSelect ? deleteTitleMultiple : deleteTitleSingle,
                                         msg: message,
                                         buttons: Ext.Msg.YESNO,
                                         icon: Ext.Msg.WARNING,
                                         fn: function (btn, text) {
                                             if (btn == 'yes') {
                                                 DeleteSelectedItems(grid.getSelectionModel().getSelections());
                                             }
                                         }
                                     }).getDialog();
                                     // this sets "NO" button as default
                                     dlg.defaultButton = 2;
                                     dlg.focus();
                                     Ext.Msg.buttonText.yes = yesText;
                                 });
					        }
					    }
					}, {
					    // do not change this separator to '-' we need its reference
					    id: 'UpdateAgentSeparator',
					    xtype: 'tbseparator',
					    hidden: true
					}, {
					    id: 'UpdateAgentButton',
					    text: forceUpdateButtonText,
					    tooltip: forceUpdateButtonTooltip,
					    iconCls: 'SEUM_UpdateAgentButton',
					    hidden: true,
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

					        var multiSelect = grid.getSelectionModel().getCount() > 1;

					        var dlg = Ext.Msg.show({
					            title: forceUpdatePromptTitle,
					            msg: multiSelect ? forceUpdateMsgPlural : forceUpdateMsgSingular,
					            buttons: Ext.Msg.YESNO,
					            icon: Ext.Msg.WARNING,
					            fn: function (btn, text) {
					                if (btn == 'yes') {
					                    ForceUpdateSelectedItems(grid.getSelectionModel().getSelections());
					                }
					            }
					        }).getDialog();
					    }
					}, {
					    // do not change this separator to '-' we need its reference
					    id: 'DeployAgentSeparator',
					    xtype: 'tbseparator',
					    hidden: true
					}, {
					    id: 'DeployAgentButton',
					    text: forceRedeployButtonText,
					    tooltip: forceRedeployButtonTooltip,
					    iconCls: 'SEUM_DeployAgentButton',
					    hidden: true,
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

					        var multiSelect = grid.getSelectionModel().getCount() > 1;

					        forceDeployWindow.show();
					    }
					}, '->',
                        agentsNotificationPanel,
                        searchField
                    ],

                bbar: pagingToolbar
            });

            grid.getColumnModel().on('hiddenchange', function (cm, index, hidden) {
                setCookie(userName + '_SEUM_AgentsGrid_' + cm.getDataIndex(index), hidden ? 'H' : 'V', 'days', 1);
            });

            var panel = new Ext.Container({
                renderTo: 'AgentsGrid',
                height: SEUM_GetManagementGridHeight(),
                layout: 'border',
                items: [grid]
            });
            Ext.EventManager.onWindowResize(function () {
                panel.setHeight(SEUM_GetManagementGridHeight());
                panel.doLayout();
            }, panel);

            InitForceDeployWindow();

            task = {
                run: function () {
                    UpdateAgentsWithPendingOperation();
                },
                interval: 120000 // 120 seconds update interval
            }

            runner = new Ext.util.TaskRunner();
            runner.start(task);

            RefreshObjects(true);

            UpdateToolbarButtons();
        }
    };
} ();

Ext.onReady(SW.SEUM.AgentsGrid.init, SW.SEUM.AgentsGrid);
