﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.MonitorsGrid = function () {
    var deleteTitleSingle = "Delete Monitor";
    var deleteTitleMultiple = "Delete Monitors";
    var deletePromptSingle = "Do you really want to delete the selected monitor?";
    var deletePromptMultiple = "Do you really want to delete the selected monitors?";
    var enableTitleSingle = "Remanage Monitor";
    var enableTitleMultiple = "Remanage Monitors";
    var enablePromptSingle = "Do you really want to remanage the selected monitor?";
    var enablePromptMultiple = "Do you really want to remanage the selected monitors?";
    var loadingMonitorsText = "Loading Monitors ...";
    var deletingMonitorsText = "Deleting Monitors ...";
    var enablingMonitorsText = "Remanaging Monitors ...";
    var playNowWaitText = "Requesting playback ...";
    var playNowSuccessMessageText = "Playback was started. It may take few minutes until results are displayed.";
    var playNowErrorMessageText = "There was an error when starting transaction."
    var errorTitle = "Error";
    var searchMonitorsText = "Search all monitors ...";
    var pagingToolbarFormat = "Displaying monitors {0} - {1} of {2}";
    var noMonitorsPagingToolbarText = "No monitors to display";
    var noMonitorsGridText = "There are no monitors available or no monitors match specified criteria";
    var addMonitorButtonText = "Add Monitor";
    var editMonitorButtonText = "Edit";
    var deleteMonitorButtonText = "Delete";
    var enableMonitorButtonText = "Remanage";
    var disableMonitorButtonText = "Unmanage";
    var playNowButtonText = "Play now";
    var monitoredText = "Monitored";
    var notMonitoredText = "Not monitored";
    var disabledText = "Unmanaged";
    var outOfLicense = "Not enough free monitors in your license";
    var perPageLabel = "Per Page: ";
    var invalidPageSizeText = "Invalid page size";
    var unmanageText = "OK";
    var cancelText = "Cancel";
    var nodesHeader = 'Related nodes';
    var applicationsHeader = 'Related applications';
    var relatedNodesText = 'nodes';
    var relatedApplicationsText = 'applications';

    var monitorEntity;
    var recordingEntity;
    var groupBy;
    var groupByItem;

    var selectorModel;
    var dataStore;
    var initialPage = 1;
    var pagingToolbar;
    var grid;
    var gridPanel;
    var groupByCombo;
    var groupByStore;
    var groupByList;
    var groupingPanel;
    var searchField;
    var initialized;
    var pageSizeBox;

    var pageSize = 1;
    var userName = "";

    var loadingMask = null;
    var enabledMonitors = 0;
    var licLimit = 0;

    var relatedNodesVisible = false;
    var relatedApplicationsVisible = false;

    var playedNowMonitors = [];

    GetPageSize = function () {
        var pSize = parseInt(getCookie(userName + '_SEUM_MonitorsGrid_PageSize'), 10);
        return isNaN(pSize) ? pageSize : pSize;
    }

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: loadingMonitorsText
            });
        }

        var records = groupByList.getSelectedRecords();
        var groupByItem = '';
        if (records.length > 0)
            groupByItem = records[0].data.Value;

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {
            groupByCategory: groupByCombo.getValue(),
            groupByItem: groupByItem
        };
        grid.store.baseParams = { start: (initialPage - 1) * GetPageSize(), limit: GetPageSize(), search: searchField.getValue() };
        grid.store.on('load', function () {
            loadingMask.hide();
        });
        grid.store.load();
    }

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        map.AddButton.setDisabled(false);
        map.EditButton.setDisabled(true);
        map.DeleteButton.setDisabled(true);
        map.EnableMonitorButton.setDisabled(true);
        map.DisableMonitorButton.setDisabled(true);
        map.PlayNowButton.setDisabled(true);

        if (count === 1) {
            map.EditButton.setDisabled(false);
            map.DeleteButton.setDisabled(false);
        } else if (count > 1) {
            map.DeleteButton.setDisabled(false);
        }

        Ext.each(selItems.getSelections(), function (item) {
            if (item.data.Unmanaged != 1)
                map.DisableMonitorButton.setDisabled(false);
            else
                map.EnableMonitorButton.setDisabled(false);
        });

        Ext.each(selItems.getSelections(), function (item) {
            if (item.data.Unmanaged != 1 && $.inArray(item.data.ID, playedNowMonitors) == -1) {
                map.PlayNowButton.setDisabled(false);
                return false; // break
            }
        });
    };

    AddMonitor = function () {
        window.location = '/Orion/SEUM/Admin/Add/Default.aspx';
    }

    EditMonitor = function (item) {
        window.location = '/Orion/SEUM/Admin/EditMonitor.aspx?id=' + item.data.ID;
    }

    DeleteMonitors = function (ids, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/MonitorsGrid.asmx",
                         "DeleteMonitors", { monitorsIds: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    DeleteSelectedItems = function (items) {
        var toDelete = [];

        Ext.each(items, function (item) {
            toDelete.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(deletingMonitorsText);

        DeleteMonitors(toDelete, function () {
            waitMsg.hide();
            grid.store.reload();
            GetEnabledMonitors(function () {
                $('#licenseUsedCounter').text(enabledMonitors);
            });
        });
    };

    EnableMonitors = function (ids, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/MonitorsGrid.asmx",
                        "EnableMonitors", { monitorsIds: ids },
                        function (result) {
                            onSuccess();
                        });
    };


    GetEnabledMonitors = function (onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/MonitorsGrid.asmx",
                                                 "GetEnabledMonitors", {},
                                                 function (result) {
                                                     enabledMonitors = result;
                                                     onSuccess();

                                                 });
    };

    GetLicLimit = function (onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/MonitorsGrid.asmx",
                                                 "GetLicenseLimit", {},
                                                 function (result) {
                                                     licLimit = result;
                                                     onSuccess();
                                                 });
    };

    EnableSelectedItems = function (items) {
        var toEnable = [];

        Ext.each(items, function (item) {
            if (item.data.Unmanaged == 1)
                toEnable.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(enablingMonitorsText);

        GetLicLimit(function () { });

        GetEnabledMonitors(function () {
            if (enabledMonitors > licLimit) {
                waitMsg.hide();
                alert(outOfLicense);
                grid.store.reload();
                return false;
            };
        });

        EnableMonitors(toEnable, function () {
            waitMsg.hide();
            grid.store.reload();
            GetEnabledMonitors(function () {
                $('#licenseUsedCounter').text(enabledMonitors);
            });
        });
    };

    PlayNow = function (items) {
        var toPlayNow = [];

        Ext.each(items, function (item) {
            if (item.data.Unmanaged != 1 && $.inArray(item.data.ID, playedNowMonitors) == -1) {
                toPlayNow.push(item.data.ID);
                playedNowMonitors.push(item.data.ID);
            }
        });

        var waitMsg = Ext.Msg.wait(playNowWaitText);

        ORION.callWebService("/Orion/SEUM/Services/MonitorsGrid.asmx",
                        "PlayNow", { monitorsIds: toPlayNow },
                        function (result) {
                            waitMsg.hide();
                            grid.store.reload();
                            Ext.Msg.alert("Play Now", playNowSuccessMessageText).setIcon(Ext.Msg.INFO);
                        },
                        function (msg) {
                            waitMsg.hide();
                            Ext.Msg.alert("Play Now", playNowErrorMessageText).setIcon(Ext.Msg.ERROR);
                        });

    };

    RefreshGroupByList = function (groupByCategory, handler) {
        groupByStore.proxy.conn.jsonData = { groupByCategory: groupByCategory };
        if (handler) {
            var internalHandler = function () {
                handler();
                groupByStore.un('load', internalHandler);
            };
            groupByStore.on('load', internalHandler);
        }

        groupByStore.load();
    }

    InitGroupingPanel = function () {
        groupByCombo = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: [['none', '[No grouping]'],
                   ['status', 'Status'],
                    ['location', 'Location'],
                    ['recording', 'Recording'],
                    ['licensed', 'Monitoring Status']
                    ]
            }),
            valueField: 'itemValue',
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            width: 150,
            editable: false,
            transform: 'groupBySelect',
            hiddenId: 'groupBySelect',
            listeners: {
                select: function (record, index) {
                    RefreshGroupByList(record.value);

                    if (record.value == 'none')
                        RefreshObjects();
                }
            }
        });
        groupByCombo.setValue('none');

        var selectPanel = new Ext.Container({
            applyTo: 'SEUM_GroupingPanel',
            region: 'north',
            height: 50,
            layout: 'form'
        });

        var tpl = new Ext.XTemplate(
		'<tpl for=".">',
            '<div class="SEUM_GroupByItem" id="{Value}">',
            '<tpl if="Type == \'status\'"><img src="/Orion/StatusIcon.ashx?entity=' + monitorEntity + '&amp;status={Value}&amp;size=small" /> </tpl>',
            '<tpl if="Type == \'recording\'"><img src="/Orion/StatusIcon.ashx?entity=' + recordingEntity + '&amp;status={Value}&amp;size=small" /> </tpl>',
            '<tpl if="Type == \'licensed\'">',
                '<tpl if="Value == 1"><img src="/Orion/images/failed_16x16.gif" /> </tpl>',
                '<tpl if="Value != 1"><img src="/Orion/images/ok_16x16.gif" /> </tpl>',
            '</tpl>',
            '{Label}</div>',
        '</tpl>',
        '<div class="x-clear"></div>'
	);

        groupByStore = new ORION.WebServiceStore(
                "/Orion/SEUM/Services/MonitorsGrid.asmx/GetGroupByItems",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Label', mapping: 1 },
                    { name: 'Type', mapping: 2 },
                ],
                "Label");

        groupByList = new Ext.DataView({
            cls: 'SEUM_GroupByList',
            border: true,
            store: groupByStore,
            tpl: tpl,
            height: SEUM_GetManagementGridHeight() - selectPanel.getSize().height,
            autoHeight: false,
            autoScroll: true,
            singleSelect: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            itemSelector: 'div.SEUM_GroupByItem',
            listeners: {
                selectionchange: function (view, selections) {
                    if (selections.length > 0)
                        RefreshObjects();
                }
            }
        });

        groupingPanel = new Ext.Panel({
            region: 'west',
            width: 200,
            split: true,
            items: [
                selectPanel,
                groupByList
            ]
        });

        groupingPanel.on('bodyresize', function () {
            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        });
    }

    InitGrid = function () {
        selectorModel = new Ext.grid.CheckboxSelectionModel();

        selectorModel.on("selectionchange", function () {
            UpdateToolbarButtons();
        });

        dataStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/MonitorsGrid.asmx/GetMonitorsPaged",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'Unmanaged', mapping: 2 },
                                { name: 'Status', mapping: 3 },
                                { name: 'RecordingName', mapping: 4 },
                                { name: 'AgentName', mapping: 5 },
                                { name: 'LastPlayback', mapping: 6 },
                                { name: 'Entity', mapping: 7 },
                                { name: 'StatusName', mapping: 8 },
                                { name: 'LastDateTimeUtc', mapping: 9 },
                                { name: 'Frequency', mapping: 10 },
                                { name: 'Description', mapping: 11 },
                                { name: 'DetailsUrl', mapping: 12 },
                                { name: 'AgentDetailsUrl', mapping: 13 },
                                { name: 'UnManageFromUtc', mapping: 14 },
                                { name: 'UnManageUntilUtc', mapping: 15 },
                                { name: 'UnManageFrom', mapping: 16 },
                                { name: 'UnManageUntil', mapping: 17 },
                                { name: 'ProbeType', mapping: 18 },
                                { name: 'Nodes', mapping: 19 },
                                { name: 'Applications', mapping: 20 }
                            ],
                            "Name");

        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        searchField = new Ext.ux.form.SearchField({
            store: dataStore,
            width: 200,
            emptyText: searchMonitorsText,
            paramName: 'search'
        });

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 1000,
            value: GetPageSize()
        });

        pageSizeBox.on('change', function (f, numbox, o) {
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                Ext.Msg.show({
                    title: errorTitle,
                    msg: invalidPageSizeText,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });

                return;
            }

            if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                // Cursor determines current page, pointing to the first element displayed on current page
                // We determine new cursor so that the element it pointed to before is visisble on a new page
                pagingToolbar.cursor = Math.floor(pagingToolbar.cursor / pSize) * pSize;
                pagingToolbar.pageSize = pSize;
                setCookie(userName + '_SEUM_MonitorsGrid_PageSize', pSize, 'months', 1);
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }

        });

        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: GetPageSize(),
            displayInfo: true,
            displayMsg: pagingToolbarFormat,
            emptyMsg: noMonitorsPagingToolbarText,
            items: [
                    '-',
                    perPageLabel,
                    pageSizeBox
                ]
        });

        var licensedColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_IsLicensed');
        var statusColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_Status');
        var recordingColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_RecordingName');
        var locationColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_AgentName');
        var lastPlaybackColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_LastPlayback');
        var frequencyColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_Frequency');
        var descriptionColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_Description');
        var unmanageFromColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_UnManageFrom');
        var unmanageUntilColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_UnManageUntil');
        var recorderTypeColumnHidden = getCookie(userName + '_SEUM_LocationsGrid_RecorderType');

        var columns = [
            selectorModel,
            { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
            { header: 'Transaction Name', width: 300, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
            { header: 'Recorder Type', width: 300, hidden: recorderTypeColumnHidden, sortable: true, dataIndex: 'ProbeType', renderer: renderRecorderType },
            { header: 'Monitoring Status', hidden: licensedColumnHidden == 'H', width: 140, sortable: true, dataIndex: 'Unmanaged', renderer: renderMonitoredStatus },
            { header: 'Status', hidden: statusColumnHidden == 'H', width: 80, sortable: true, dataIndex: 'Status', renderer: renderStatus },
            { header: 'Recording Name', hidden: (recordingColumnHidden == 'H') || (!recordingColumnHidden), width: 180, sortable: true, dataIndex: 'RecordingName', renderer: renderDefault },
            { header: 'Location Name', hidden: (locationColumnHidden == 'H') || (!locationColumnHidden), width: 130, sortable: true, dataIndex: 'AgentName', renderer: renderDefault },
            { header: 'Last Playback', hidden: (lastPlaybackColumnHidden == 'H') || (!lastPlaybackColumnHidden), width: 200, sortable: true, dataIndex: 'LastPlayback', renderer: renderDefault },
            { header: 'Frequency', hidden: (frequencyColumnHidden == 'H') || (!frequencyColumnHidden), width: 100, sortable: true, dataIndex: 'Frequency', renderer: renderDefault },
            { header: 'Description', hidden: (descriptionColumnHidden == 'H') || (!descriptionColumnHidden), width: 250, sortable: true, dataIndex: 'Description', renderer: renderDefault },
            { header: 'Unmanage From', hidden: (unmanageFromColumnHidden == 'H') || (!unmanageFromColumnHidden), width: 100, sortable: true, dataIndex: 'UnManageFrom', renderer: renderDefault },
            { header: 'Unmanage Until', hidden: (unmanageUntilColumnHidden == 'H') || (!unmanageUntilColumnHidden), width: 100, sortable: true, dataIndex: 'UnManageUntil', renderer: renderDefault }
        ];
        if (relatedNodesVisible) {
            columns.push({ header: nodesHeader, hidden: descriptionColumnHidden == 'H', width: 120, sortable: true, dataIndex: 'Nodes', renderer: renderNodes });
        }
        if (relatedApplicationsVisible) {
            columns.push({ header: applicationsHeader, hidden: descriptionColumnHidden == 'H', width: 150, sortable: true, dataIndex: 'Applications', renderer: renderApplications });
        }
        columns.push({ id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 });
        grid = new Ext.grid.GridPanel({

            store: dataStore,
            columns: columns,
            sm: selectorModel,
            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            autoExpandColumn: 'filler',
            viewConfig: {
                emptyText: noMonitorsGridText
            },

            tbar: [
					{
					    id: 'AddButton',
					    text: addMonitorButtonText,
					    iconCls: 'SEUM_AddMonitorButton',
					    handler: function () {
					        AddMonitor();
					    }

					}, '-', {
					    id: 'EditButton',
					    text: editMonitorButtonText,
					    iconCls: 'SEUM_EditMonitorButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();
					        EditMonitor(grid.getSelectionModel().getSelected());
					    }
					}, '-', {
					    id: 'PlayNowButton',
					    text: playNowButtonText,
					    iconCls: 'SEUM_PlayNowButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();
					        PlayNow(grid.getSelectionModel().getSelections());
					    }
					}, '-', {
					    id: 'DisableMonitorButton',
					    text: disableMonitorButtonText,
					    iconCls: 'SEUM_DisableMonitorButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();

					        SW.SEUM.UnManageTransactionDialog.SetUnmanageAfterCallback(function () {
					            grid.store.reload();
					            GetEnabledMonitors(function () {
					                $('#licenseUsedCounter').text(enabledMonitors);
					            });
					        });

					        var toDisable = [];
					        var items = grid.getSelectionModel().getSelections();

					        Ext.each(items, function (item) {
					            if (item.data.Unmanaged != 1)
					                toDisable.push(item.data.ID);
					        });

					        var from = null;
					        var until = null;

					        if (grid.getSelectionModel().getCount() == 1) {
					            if (items[0].data.UnManageFromUtc != null && items[0].data.UnManageUntilUtc != null) {
					                from = items[0].data.UnManageFrom;
					                until = items[0].data.UnManageUntil;
					            }
					        }

					        SW.SEUM.UnManageTransactionDialog.ShowUnmanageWindow(toDisable, from, until);
					    }
					}, '-', {
					    id: 'EnableMonitorButton',
					    text: enableMonitorButtonText,
					    iconCls: 'SEUM_EnableMonitorButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();
					        var multiSelect = grid.getSelectionModel().getCount() > 1;

					        Ext.Msg.confirm(multiSelect ? enableTitleMultiple : enableTitleSingle,
								multiSelect ? enablePromptMultiple : enablePromptSingle,
								function (btn, text) {
								    if (btn == 'yes') {
								        EnableSelectedItems(grid.getSelectionModel().getSelections());
								    }
								});
					    }
					}, '-', {
					    id: 'DeleteButton',
					    text: deleteMonitorButtonText,
					    iconCls: 'SEUM_DeleteMonitorButton',
					    handler: function () {
					        if (SEUMIsDemoMode()) return SEUMDemoModeMessage();
					        var multiSelect = grid.getSelectionModel().getCount() > 1;

					        Ext.Msg.confirm(multiSelect ? deleteTitleMultiple : deleteTitleSingle,
								multiSelect ? deletePromptMultiple : deletePromptSingle,
								function (btn, text) {
								    if (btn == 'yes') {
								        DeleteSelectedItems(grid.getSelectionModel().getSelections());
								    }
								});
					    }
					}, '->',
                        searchField
                    ],

            bbar: pagingToolbar
        });

        grid.getColumnModel().on('hiddenchange', function (cm, index, hidden) {
            setCookie(userName + '_SEUM_LocationsGrid_' + cm.getDataIndex(index), hidden ? 'H' : 'V', 'days', 1);
        });
    }

    InitLayout = function () {
        var panel = new Ext.Container({
            renderTo: 'MonitorsGrid',
            height: SEUM_GetManagementGridHeight(),
            layout: 'border',
            items: [groupingPanel, grid]
        });
        Ext.EventManager.onWindowResize(function () {
            panel.setHeight(SEUM_GetManagementGridHeight());
            panel.doLayout();
        }, panel);
    }

    function renderName(value, meta, record) {
        var id = record.data.ID;
        var entity = record.data.Entity;
        var status = record.data.Status;
        var agentName = record.data.AgentName;
        var detailsUrl = record.data.DetailsUrl;
        var agentDetailsUrl = record.data.AgentDetailsUrl;
        var recordingName = record.data.RecordingName;

        return renderDefault(
        String.format('<a href="{5}"><span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={0}&amp;id={1}&amp;status={2}&amp;size=small" /></span> {3}</a> from <a href="{6}">{4}</a>',
            entity, id, status, recordingName, agentName, detailsUrl, agentDetailsUrl), meta, record);
    }

    function renderMonitoredStatus(value, meta, record) {
        var message = monitoredText;

        var icon = '/Orion/images/ok_16x16.gif';
        if (value == 1) { // is unmanaged?
            icon = '/Orion/images/failed_16x16.gif';
            message = notMonitoredText;
        }

        return renderDefault(String.format('<img src="{0}" /> {1}', icon, message), meta, record);
    }

    function renderStatus(value, meta, record) {
        var message = "";

        if (record.data.Unmanaged == 1)
            message = disabledText;
        else {
            message = record.data.StatusName;
        }

        return renderDefault(String.format('{0}', message), meta, record);
    }
    
    function renderRecorderType(value, meta, record) {
        var result = "";
        switch (record.data.ProbeType) {
        case 1:
            result = "Deprecated";
            break;
        case 2:
            result = "WPM 3.0+";
            break;
        }
        return renderDefault(result, meta, record);
    }

    function renderDefault(value, meta, record) {
        if (record.data.Unmanaged == 1)
            return String.format('<span class="SEUM_DisabledText">{0}</span>', value);

        return value;
    }

    function renderNodes(value, meta, record) {
        return renderRelatedEntities(value, meta, record, relatedNodesText);
    }

    function renderApplications(value, meta, record) {
        return renderRelatedEntities(value, meta, record, relatedApplicationsText);
    }

    function renderRelatedEntities(value, meta, record, caption) {
        var text = '', hint = '';
        if (value) {
            text = value.length == 0 ? '' : value.length == 1 ? value[0] : value.length + " " + caption;
            hint = value.join(', ');
        }
        return renderDefault(String.format('<span title="{0}">{1}</span>', hint, text), meta, record);
    }

    ORION.prefix = "SEUM_MonitorsGrid_";

    return {
        SetPageSize: function (size) {
            pageSize = size;
        },
        SetMonitorEntity: function (entity) {
            monitorEntity = entity;
        },
        SetRecordingEntity: function (entity) {
            recordingEntity = entity;
        },
        SetGroupBy: function (value) {
            groupBy = value;
        },
        SetGroupByItem: function (value) {
            groupByItem = value;
        },
        SetUserName: function (user) {
            if (user)
                userName = user;
        },
        SetRelatedNodesVisible: function (visible) {
            relatedNodesVisible = visible;
        },
        SetRelatedApplicationsVisible: function (visible) {
            relatedApplicationsVisible = visible;
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            InitGroupingPanel();
            InitGrid();
            InitLayout();

            if (groupBy && groupByItem) {
                groupByCombo.setValue(groupBy);
                RefreshGroupByList(groupBy, function () {
                    groupByList.select(groupByItem);
                });
            }

            RefreshObjects();

            UpdateToolbarButtons();
        }
    };
} ();

Ext.onReady(SW.SEUM.MonitorsGrid.init, SW.SEUM.MonitorsGrid);