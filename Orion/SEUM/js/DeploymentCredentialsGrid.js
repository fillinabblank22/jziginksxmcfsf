﻿Ext.namespace('SW');
Ext.namespace('SW.SEUM');

SW.SEUM.DeploymentCredentialsGrid = function () {
    var errorTitle = "Error";
    var notManagedByOrionText = "Not managed by Orion";
    var assignCredentialsButtonText = "Assign Credentials";
    var testCredentialsButtonText = "Test Credentials";
    var removeLocationButtonText = "Remove Location";
    var removingLocationsText = "Removing locations from the list";
    var removePromptSingle = "Do you really want to remove the selected location?";
    var removePromptMultiple = "Do you really want to remove the selected locations?";
    var assigningCredentialsText = "Assigning credentials to the selected locations...";
    var unassignedText = "unassigned";
    var testingCredentialsText = "testing ...";
    var notTestedText = "Not tested";
    var testSuccessfulText = "Test successful";
    var testFailedText = "Test failed";

    var initialized;

    var grid;
    var selectorModel;
    var dataStore;

    var loadingMask = null;

    var assignCredentialWindow;
    var credentialsCombo;
    var credentialsList;
    var selectedCredential = -1;

    var usernameField;
    var passwordField;

    RemoveLocations = function (ids, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/DeploymentWizard.asmx",
                         "RemoveLocation", { hostnames: ids },
                         function (result) {
                             onSuccess();
                         });
    };

    AssignExistingCredentialsCall = function (ids, credentialId, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/DeploymentWizard.asmx",
                         "AssignExistingCredentials", { hostnames: ids, credentialId: credentialId },
                         function (result) {
                             onSuccess(result);
                         });
    };

    AssignExistingCredentials = function (items, credentialId) {
        var toAssign = [];

        Ext.each(items, function (item) {
            toAssign.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(assigningCredentialsText);

        AssignExistingCredentialsCall(toAssign, credentialId, function (credentialName) {
            waitMsg.hide();
            Ext.each(items, function (item) {
                item.data.CredentialId = credentialId;
                item.data.CredentialsName = credentialName;
                item.commit();
            });

            TestCredentials(items);
        });
    };

    AssignNewCredentialsCall = function (ids, username, password, onSuccess) {
        ORION.callWebService("/Orion/SEUM/Services/DeploymentWizard.asmx",
                         "AssignNewCredentials", { hostnames: ids, username: username, password: password },
                         function (result) {
                             onSuccess(result);
                         });
    };

    AssignNewCredentials = function (items, username, password) {
        var toAssign = [];

        Ext.each(items, function (item) {
            toAssign.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(assigningCredentialsText);

        AssignNewCredentialsCall(toAssign, username, password, function () {
            waitMsg.hide();

            Ext.each(items, function (item) {
                item.data.CredentialId = 0;
                item.data.CredentialsName = username;
                item.commit();
            });

            TestCredentials(items);
        });
    };

    RemoveSelectedItems = function (items) {
        var toDelete = [];

        Ext.each(items, function (item) {
            toDelete.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait(removingLocationsText);

        RemoveLocations(toDelete, function () {
            waitMsg.hide();
            grid.store.reload();
        });
    };

    TestCredentials = function (items) {
        Ext.each(items, function (item) {
            item.data.CredentialsTestResult = -1; // test in progress
            item.commit();

            ORION.callWebService("/Orion/SEUM/Services/DeploymentWizard.asmx",
                "TestCredentials", { hostname: item.data.ID, credentialId: 0, username: null, password: null },
                function (result) {
                    item.data.CredentialsTestResult = result.Success;
                    item.data.CredentialsTestMessage = result.Message;
                    item.commit();
                });
        });
    };

    TestCredentialsInPopupInternal = function (items) {
        if (!usernameField.isValid() || !passwordField.isValid())
            return;

        var username = usernameField.getValue();
        var password = passwordField.getValue();

        var testResults = $('#testResults');
        testResults.html('');
        testResults.show();
        Ext.each(items, function (item) {
            var localItem = item;

            testResults.append(String.format('<li>{0}</li>', getCredentialTestResultMarkup('popup' + item.id, -1, '', localItem.data.Name)));

            ORION.callWebService("/Orion/SEUM/Services/DeploymentWizard.asmx",
                "TestCredentials", { hostname: localItem.data.ID, credentialId: selectedCredential, username: username, password: password },
                function (result) {
                    $('#' + getCredentialTestResultId('popup' + localItem.id)).remove();
                    testResults.append(getCredentialTestResultMarkup('popup' + localItem.id, result.Success, result.Message, localItem.data.Name));
                });
        });
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        map.AssignCredentials.setDisabled(true);
        map.RemoveLocation.setDisabled(true);
        map.TestCredentials.setDisabled(true);

        if (count >= 1) {
            map.AssignCredentials.setDisabled(false);
            map.RemoveLocation.setDisabled(false);
            map.TestCredentials.setDisabled(false);
        }
    };

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: 'Loading Locations ...'
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {};
        grid.store.baseParams = {};
        grid.store.on('load', function () {
            loadingMask.hide();
        });
        grid.store.load();
    };

    function renderDefault(value, meta, record) {
        return value;
    }

    function renderStatus(value, meta, record) {
        if (value == null || value == "")
            return renderDefault(String.format('<span class="sw-text-helpful">{0}</a>', notManagedByOrionText), meta, record);
        else
            return renderDefault(value, meta, record);
    }

    function renderCredentialsName(value, meta, record) {
        if (record.data.CredentialId == -1 && record.data.CredentialsName == '')
            return renderDefault(String.format('<span class="Warning">{0}</span>', unassignedText), meta, record);
        else
            return renderDefault(value, meta, record);
    }

    function renderCredentialsTestResult(value, meta, record) {
        return renderDefault(getCredentialTestResultMarkup(record.id, value, record.data.CredentialsTestMessage), meta, record);
    }

    function getCredentialTestResultId(itemId) {
        return "test-result-" + itemId;
    }

    function getCredentialTestResultMarkup(itemId, resultValue, resultMessage, itemName) {
        if (typeof (itemName) == 'undefined')
            itemName = '';
        else
            itemName = itemName + ' - ';

        if (typeof (resultValue) == 'undefined')
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-warn" automation="warn"><span class="sw-suggestion-icon"></span>{1}{2}</div>', getCredentialTestResultId(itemId), itemName, notTestedText);
        else if (resultValue == -1)
            return String.format('<div id="{0}"><img src="/Orion/images/loading_gen_small.gif" alt="loading..." />{1}{2}</div>', getCredentialTestResultId(itemId), itemName, testingCredentialsText);
        else if (resultValue == true)
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-pass" automation="pass"><span class="sw-suggestion-icon"></span>{1}{2}</div>', getCredentialTestResultId(itemId), itemName, testSuccessfulText);
        else
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-fail" automation="fail"><span class="sw-suggestion-icon"></span>{1}{2} - {3}</div>', getCredentialTestResultId(itemId), itemName, testFailedText, resultMessage);

    }

    function textFieldsValidator(value) {
        if (selectedCredential != -1)
            return true;

        if (value == null || value == '')
            return 'Field must not be empty.';

        return true;
    }

    InitLayout = function () {
        var panel = new Ext.Container({
            renderTo: 'CredentialsGrid',
            height: SEUM_GetManagementGridHeight(),
            layout: 'border',
            items: [grid]
        });
        Ext.EventManager.onWindowResize(function () {
            panel.setHeight(SEUM_GetManagementGridHeight());
            panel.doLayout();
        }, panel);
    };

    InitGrid = function () {
        selectorModel = new Ext.grid.CheckboxSelectionModel();

        selectorModel.on("selectionchange", function () {
            UpdateToolbarButtons();
        });

        dataStore = new ORION.WebServiceStore(
                            "/Orion/SEUM/Services/DeploymentWizard.asmx/GetLocationsForCredentialsGrid",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'CredentialId', mapping: 2 },
                                { name: 'CredentialsName', mapping: 3 },
                                { name: 'StatusName', mapping: 4 },
                                { name: 'CredentialsTestResult', mapping: 5, defaultValue: undefined },
                                { name: 'CredentialsTestMessage', mapping: 6, defaultValue: undefined }
                            ], "Name");

        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        grid = new Ext.grid.GridPanel({

            store: dataStore,

            columns: [
                    selectorModel,
                    { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                    { header: 'Location Name', width: 300, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderDefault },
                    { header: 'Status', width: 150, hideable: false, sortable: false, dataIndex: 'StatusName', renderer: renderStatus },
                    { header: 'Credential name', width: 200, hideable: false, sortable: false, dataIndex: 'CredentialsName', renderer: renderCredentialsName },
                    { id: 'credential-test', header: 'Credential test', width: 500, hideable: false, sortable: false, dataIndex: 'CredentialsTestResult', renderer: renderCredentialsTestResult }
                ],

            sm: selectorModel,

            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            autoExpandColumn: 'credential-test',
            viewConfig: {
                emptyText: 'There are no locations.'
            },

            tbar: [
					{
					    id: 'AssignCredentials',
					    text: assignCredentialsButtonText,
					    iconCls: 'SEUM_AssignCredentialsButton',
					    handler: function () {
					        assignCredentialWindow.show();
					    }

					}, '-', {
					    id: 'TestCredentials',
					    text: testCredentialsButtonText,
					    iconCls: 'SEUM_TestCredentialsButton',
					    handler: function () {
					        TestCredentials(grid.getSelectionModel().getSelections());
					    }

					}, '-', {
					    id: 'RemoveLocation',
					    text: removeLocationButtonText,
					    iconCls: 'SEUM_DeleteAgentButton',
					    handler: function () {
					        var multiSelect = grid.getSelectionModel().getCount() > 1;

					        Ext.Msg.confirm(multiSelect ? "Remove Locations" : "Remove Location",
								multiSelect ? removePromptMultiple : removePromptSingle,
								function (btn, text) {
								    if (btn == 'yes') {
								        RemoveSelectedItems(grid.getSelectionModel().getSelections());
								    }
								});
					    }
					}
                ]

        });

    };

    InitAssignCredentialWindow = function () {
        credentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: credentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'CredentialNamesCombo',
            hiddenId: 'CredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedCredential = index.data.Key;
                    usernameField.validate();
                    passwordField.validate();
                    if (selectedCredential == -1) {
                        $('#AssignCredentialWindow tr.sw-seum-hideable').show();
                    } else {
                        $('#AssignCredentialWindow tr.sw-seum-hideable').hide();
                    }

                    assignCredentialWindow.syncShadow();
                }
            }
        });
        credentialsCombo.setValue(selectedCredential);

        usernameField = new Ext.form.TextField({
            applyTo: "CredentialUsername",
            width: 230,
            validator: textFieldsValidator
        });

        passwordField = new Ext.form.TextField({
            applyTo: "CredentialPassword",
            width: 230,
            validator: textFieldsValidator
        });

        assignCredentialWindow = new Ext.Window({
            title: "Assign Credential",
            width: 400,
            autoHeight: true,
            closable: false,
            modal: true,
            resizable: false,

            items: [
                    new Ext.Panel({
                        applyTo: 'AssignCredentialWindow',
                        border: false
                    })
                ],

            buttons: [
                {
                    text: "Submit",
                    disabled: false,
                    handler: function () {
                        if (selectedCredential != -1) { // new credential
                            AssignExistingCredentials(grid.getSelectionModel().getSelections(), selectedCredential);
                        } else {    // existing credential
                            if (!usernameField.isValid() || !passwordField.isValid())
                                return;

                            var username = usernameField.getValue();
                            var password = passwordField.getValue();

                            AssignNewCredentials(grid.getSelectionModel().getSelections(), username, password);
                        }
                        assignCredentialWindow.hide();
                    }
                },
                {
                    text: "Cancel",
                    handler: function () {
                        assignCredentialWindow.hide();
                        usernameField.setValue('');
                        passwordField.setValue('');
                    }
                }],
            listeners: {
                beforeshow: function () {
                    $('#AssignCredentialWindow').show();
                    $('#AssignCredentialWindow tr.sw-seum-hideable').show();

                    if (grid.getSelectionModel().getCount() != 1) {
                        credentialsCombo.setValue(-1);
                    } else {
                        var record = grid.getSelectionModel().getSelected();
                        if (record.data.CredentialId > 0) {
                            credentialsCombo.setValue(record.data.CredentialId);
                            $('#AssignCredentialWindow tr.sw-seum-hideable').hide();
                        } else {
                            credentialsCombo.setValue(-1);
                        }
                    }
                    usernameField.setValue('');
                    usernameField.clearInvalid();
                    passwordField.setValue('');
                    passwordField.clearInvalid();

                    var testResults = $('#testResults');
                    testResults.html('');
                    testResults.hide();
                }
            }
        });
    };

    ORION.prefix = "SEUM_DeploymentCredentialsGrid_";

    return {
        SetCredentialsList: function (list) {
            credentialsList = list;
        },
        TestCredentialsInPopup: function () {
            TestCredentialsInPopupInternal(grid.getSelectionModel().getSelections());
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            InitGrid();
            InitLayout();
            InitAssignCredentialWindow();

            RefreshObjects();
            UpdateToolbarButtons();
        }
    };
} ();

Ext.onReady(SW.SEUM.DeploymentCredentialsGrid.init, SW.SEUM.DeploymentCredentialsGrid);