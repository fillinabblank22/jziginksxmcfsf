using System;
using System.Web.UI;
using System.Web;

public partial class Orion_SEUM_LicenseError : Page
{
    protected override void OnInit(EventArgs e)
    {
        SolarWinds.Orion.Web.ControlHelper.AddStylesheet(Page, "/Orion/SEUM/styles/SEUM.css");

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["msg"] != null)
        {
            Message = HttpUtility.UrlDecode(Request["msg"]);
        }
    }

    protected string Message { get; set; }
}
