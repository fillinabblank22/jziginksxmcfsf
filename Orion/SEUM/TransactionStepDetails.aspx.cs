﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using ASP;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Licensing;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_TransactionStepDetails : OrionView, ITransactionStepProvider 
{
    protected const string locationText = "on";
    protected const string fromText = "from";

    private const string TransactionNodesResource = "Orion/SEUM/Resources/TransactionStepDetails/TransactionStepNodes.ascx";
    private const string TransactionAppsResource = "Orion/SEUM/Resources/TransactionStepDetails/TransactionStepApplications.ascx";

    public override string ViewType
    {
        get
        {
            return "SEUMTransactionStepDetails";
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Step request net object does not have separate page, it always points to Step details.
        // Redirect to correct step page using StepRequest object if needed.
        if (NetObject is TransactionStepRequestObject)
        {
            TransactionStepObject step = ((TransactionStepRequestObject)NetObject).Step;
            Response.Redirect(string.Format("/Orion/View.aspx?NetObject={0}", step.NetObjectID));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = this.ViewInfo.ViewTitle;

        this.DataBind();
        HideIntegrationResourcesIfNeeded();

        ((Orion_SEUM_SEUMView)this.Master).HelpFragment = "OrionWPMPHStepDetails";
    }

    protected override void OnInit(EventArgs e)
    {
        this.ResourceContainer.DataSource = this.ViewInfo;
        this.ResourceContainer.DataBind();
        
        base.OnInit(e);
    }

    public TransactionStepObject TransactionStepObject
    {
        get { return (TransactionStepObject)NetObject; }
    }

    public TransactionObject TransactionObject
    {
        get { return TransactionStepObject.TransactionObject; }
    }

    private void HideIntegrationResourcesIfNeeded()
    {
        if (!LicenseInfo.AreNodesAvailable || !LicenseInfo.AreApplicationsAvailable)
        {
            var resourcesToMatch = new List<string>();
            if (!LicenseInfo.AreNodesAvailable)
            {
                resourcesToMatch.Add(TransactionNodesResource);
            }
            if (!LicenseInfo.AreApplicationsAvailable)
            {
                resourcesToMatch.Add(TransactionAppsResource);
            }

            foreach (var control in GetAllControls(ResourceContainer))
            {
                if (control is SEUMBaseResource &&
                    resourcesToMatch.Any(rm => (control as SEUMBaseResource).Resource.File.Contains(rm)))
                {
                    control.Visible = false;
                }
            }
        }
    }

    private static IEnumerable<Control> GetAllControls(Control parent)
    {
        foreach (Control control in parent.Controls)
        {
            yield return control;
            foreach (Control descendant in GetAllControls(control))
            {
                yield return descendant;
            }
        }
    }
}