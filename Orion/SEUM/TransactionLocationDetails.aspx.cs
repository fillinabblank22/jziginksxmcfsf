﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using System;

public partial class Orion_SEUM_TransactionLocationDetails : OrionView, ITransactionLocationProvider
{
    protected void Page_Load(object sender, EventArgs e)
    {
        viewTitle.ViewName = this.ViewInfo.ViewTitle;
        viewTitle.AddNetObject(TransactionLocationObject);

        ((Orion_SEUM_SEUMView)this.Master).HelpFragment = "OrionWPMPHLocationDetailsView";
    }

    protected override void OnInit(EventArgs e)
    {
        this.ResourceContainer2.DataSource = this.ViewInfo;
        this.ResourceContainer2.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SEUMTransactionLocationDetails"; }
    }

    public TransactionLocationObject TransactionLocationObject
    {
        get { return (TransactionLocationObject) NetObject; }
    }
}