﻿using System;

public partial class Orion_SEUM_SEUMView : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        SolarWinds.Orion.Web.ControlHelper.AddStylesheet(Page, "/Orion/SEUM/styles/SEUM.css");

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }
    }

    public string HelpFragment { get; set; }
}
