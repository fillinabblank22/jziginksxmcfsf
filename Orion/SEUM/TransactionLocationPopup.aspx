﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransactionLocationPopup.aspx.cs" Inherits="Orion_SEUM_TransactionLocationPopup" %>

<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Import Namespace="SolarWinds.SEUM.Common.Models" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="LoadIndicator" Src="~/Orion/SEUM/Controls/LoadIndicator.ascx" %>

<!-- Header -->
<h3 class="Status<%=TransactionLocation.StatusInfo.ToString()%>">
    <%= popupName %></h3>
<div class="NetObjectTipBody SEUM_NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <%= TransactionLocation.Name %>
            </td>
        </tr>
        <tr>
            <th>
                <%= locationStatus %>:
            </th>
            <td>
                <orion:EntityStatusIcon ID="locationStatusIcon" runat="server" IconSize="Small" Entity="<%# TransactionLocation.SwisEntity %>"
                    Status="<%# TransactionLocation.Model.ConnectionStatus %>" WrapInBox="false" />
                <%= TransactionLocation.StatusDescription %>
            </td>
        </tr>
        <tr>
            <th><%= playerLoadLabel %>:</th>
            <td><SEUM:LoadIndicator ID="playerLoadIndicator" runat="server" /></td>
        </tr>
        <% if (TopTransactions.Count != 0) { %>
        <tr>
            <td colspan="2">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th colspan="3">
                            <%= transactionsWithProblems%>:
                        </th>
                    </tr>
                    <%
               int count = 0;
               foreach (Transaction transaction in this.TopTransactions)
               {
                   count++;
                   string rowColor = (count % 2 == 0) ? "apm_ZebraStripe" : "";
                    %>
                    <tr class="<%=rowColor %>">
                        <td style="width: 20px;">
                            &nbsp;
                        </td>
                        <td>
                            <img src="<%= GetStatusImagePath(transaction) %>" alt="" />
                        </td>
                        <td>
                            <%= UIHelper.Escape(transaction.Name)%>
                        </td>
                    </tr>
                    <% } %>
                    <% if (this.RemainingTransactionCount > 0) { %>
                    <tr style="background-color: #E2E1D4;">
                        <td colspan="3" align="center">
                            <%= this.MoreTransactionsMessage%>
                        </td>
                    </tr>
                    <% } %>
                </table>
            </td>
        </tr>
        <% } %>
    </table>
</div>
