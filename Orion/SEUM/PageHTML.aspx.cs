﻿using System;
using System.Web;
using SolarWinds.Logging;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web;

public partial class Orion_SEUM_PageHTML : System.Web.UI.Page
{
    private static Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        string entity = Request["entity"];
        if (string.IsNullOrEmpty(entity))
        {
            log.Error("Empty or null entity for HTML source.");
            return;
        }

        int entityId;
        if (!Int32.TryParse(Request["entityId"], out entityId))
        {
            log.ErrorFormat("Entity ID for screenshot is invalid: {0}", Request["entityId"] ?? "null");
            return;
        }

        ResourcesDAL dal = new ResourcesDAL();
        switch (entity)
        {
            case SwisEntities.StepResponseTimeDetail:
                long ticks;
                if (!long.TryParse(Request["dateTime"], out ticks))
                {
                    log.ErrorFormat("DateTime ticks for screenshot is invalid: {0}", Request["dateTime"] ?? "null");
                    return;
                }
                DateTime date = new DateTime(ticks);

                TransactionStepHistoryRecord record = dal.GetStepHistoryRecord(entityId, date, false, true);
                if (record != null && record.RawHtml != null)
                {
                    htmlLiteral.Text = HttpUtility.HtmlEncode(record.RawHtml);
                }
                break;
            case SwisEntities.TransactionSteps:
                TransactionStep step = dal.GetTransactionStep(entityId, false, true);
                if (step != null && step.RawHtml != null)
                {
                    htmlLiteral.Text = HttpUtility.HtmlEncode(step.RawHtml);
                }
                break;
            default:
                log.ErrorFormat("Entity for HTML source is invalid: {0}", entity);
                break;
        }
    }
}
