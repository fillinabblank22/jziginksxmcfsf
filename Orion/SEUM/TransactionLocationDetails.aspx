﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/SEUM/SEUMView.master" CodeFile="TransactionLocationDetails.aspx.cs" Inherits="Orion_SEUM_TransactionLocationDetails" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="ViewTitle" Src="~/Orion/SEUM/Controls/ViewTitle.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="SEUMViewPageTitle">
     <%if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div class="SEUM_BreadcrumbsWrapper">
        <orion:PluggableDropDownMapPath ID="TransactionSiteMapPath" runat="server" SiteMapKey="TransactionObjectDetails" LoadSiblingsOnDemand="true"/>
    </div>
    <% } %>
	<h1><SEUM:ViewTitle runat="server" ID="viewTitle"/></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SEUMMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="ResourceContainer2" />
    </orion:ResourceHostControl>
</asp:Content>

