﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.Shared;
using System.Web;

public partial class Orion_SEUM_TransactionPopup : System.Web.UI.Page
{
    internal const string popupName = "Transaction&nbsp;Monitor";
    internal const string playbackStatus = "Transaction&nbsp;status";
    internal const string locationStatus = "Location";
    internal const string stepsWithProblems = "Steps with problems";
    internal const string allStepsAreUp = "All steps are up";
    internal const string errorMessageLabel = "Error&nbsp;message";
    protected const int TopStepsCount = 5;

    protected TransactionObject Transaction { get; set; }
    protected List<TransactionStep> TopSteps { get; set; }
    protected int RemainingStepCount { get; set; }
    protected string MoreStepsMessage
    {
        get
        {
            return String.Format("{0} more step{1} with problems",
                                 RemainingStepCount,
                                 RemainingStepCount == 1 ? "" : "s");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Disabled caching for popup window (see FB65993)
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        this.Transaction = NetObjectFactory.Create(Request["NetObject"], true) as TransactionObject;

        int[] badStatuses = new int[] { 1 };
        var steps = Transaction.Model.Steps.Where(step => step.LastStatus != StatusInfo.UpStatusId).ToList(); // TODO: Fix that!
        TopSteps = steps.Take(TopStepsCount).ToList();
        RemainingStepCount = steps.Count - TopStepsCount;

        this.DataBind();
    }

    protected string GetStatusImagePath(TransactionStep step)
    {
        return new StatusIconProvider().GetImagePath(SwisEntities.TransactionSteps, step.LastStatus, SolarWinds.Orion.Web.UI.StatusIcons.StatusIconSize.Small);
    }
}