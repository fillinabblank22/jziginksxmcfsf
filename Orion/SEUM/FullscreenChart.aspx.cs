﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;

public partial class Orion_SEUM_FullscreenChart : System.Web.UI.Page
{
    private static Log _log = new Log();

    protected HtmlGenericControl ScriptInitializer { get; private set; }
    protected HtmlGenericControl ChartPlaceHolder { get; set; }
    protected PlaceHolder LegendPlaceholder { get; set; }
    protected IncludeCharts IncludeChartsControl { get; set; }

    protected IChartLegendControl ChartLegendControl { get; private set; }
    protected ChartResourceSettings ChartResourceSettings { get; private set; }

    protected string DisplayDetails { get; set; }
    protected bool ForceDisplayOfNeedsEditPlaceholder { get; set; }
    private NetObject _netObject;
    private int _netObjectId;

    protected string ChartOptions
    {
        get { return ChartResourceSettings.IsValid ? ChartResourceSettings.ChartOptionsJson : "{}"; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (Page != null && ScriptManager.GetCurrent(Page) == null)
        {
            Page.Form.Controls.AddAt(0, new ScriptManager());
        }

        OrionInclude.ModuleFile("SEUM", "SEUM.css");
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.DataFormatters.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.TCPWaterfallLegend.js", OrionInclude.Section.Middle);

        int resourceId = 0;
        if (!Int32.TryParse(Request["ResourceId"], out resourceId))
        {
            _log.Error("ResourceId parameter for fullscreen chart is missing or is not a valid resource ID.");
            return;
        }

        if (Request["NetObject"] == null)
        {
            _log.Error("NetObject parameter for fullscreen chart is missing.");
            return;
        }

        ResourceInfo ri = ResourceManager.GetResourceByID(resourceId);
        if (ri == null)
        {
            _log.ErrorFormat("Can't load resource info for resource {0} because it doesn't exist.", resourceId);
            return;
        }

        _netObject = NetObjectFactory.Create(Request["NetObject"]);

        if (_netObject == null)
        {
            _log.ErrorFormat("NetObject {0} doesn't exist.", Request["NetObject"]);
            return;
        }

        string[] parts = _netObject.NetObjectID.Split(':');
        if (parts.Length < 2 || !Int32.TryParse(parts[1], out _netObjectId))
        {
            _log.ErrorFormat("NetObject ID {0} doesn't have expected format.", Request["NetObject"]);
            return;
        }

        ChartResourceSettings = ChartResourceSettings.FromResource(ri, new ChartSettingsDAL());

        ScriptInitializer = new HtmlGenericControl("script");
        ScriptInitializer.Attributes.Add("type", "text/javascript");

        ChartPlaceHolder = new HtmlGenericControl("div");
        ChartPlaceHolder.Attributes.Add("class", "SEUM_NoclipResource");
        LegendPlaceholder = new PlaceHolder();
        IncludeChartsControl = new IncludeCharts { Debug = true };

        Contents.Controls.Add(IncludeChartsControl);
        Contents.Controls.Add(ChartPlaceHolder);
        Contents.Controls.Add(LegendPlaceholder);
        Contents.Controls.Add(ScriptInitializer);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!Page.IsPostBack)
        {
            CreateStandardChartResource();
        }
    }

    protected void CreateStandardChartResource()
    {
        if (!ChartResourceSettings.IsValid || ForceDisplayOfNeedsEditPlaceholder)
        {
            DisplayDetails = "{}";
            return;
        }

        CreateLegendControl();

        var details = GenerateDisplayDetails();
        DisplayDetails = new JavaScriptSerializer().Serialize(details);

        SetChartInitializerScript();
    }

    protected virtual void SetChartInitializerScript()
    {
        var script = String.Format("SW.Core.Charts.initializeStandardChart({0}, {1});", DisplayDetails, ChartOptions);
        ScriptInitializer.InnerHtml = script;
    }

    protected virtual Dictionary<string, object> GenerateDisplayDetails()
    {
        var title = ChartResourceSettings.Title;
        var subtitle = ChartResourceSettings.SubTitle;

        if (String.IsNullOrEmpty(title))
            title = _netObject.Name;

        // Parse macros in the title/subtitle (if they are there)
        if (title.Contains("${") || subtitle.Contains("${"))
        {
            var context = _netObject.ObjectProperties;
            title = Macros.ParseDataMacros(title, context, true, false);
            subtitle = Macros.ParseDataMacros(subtitle, context, true, false);
        }

        var details = new Dictionary<string, object>();

        details["dataUrl"] = ChartResourceSettings.DataUrl;
        details["title"] = title;
        details["subtitle"] = subtitle;
        details["showTitle"] = true;
        details["netObjectIds"] = new [] {_netObjectId}.ToArray();
        details["renderTo"] = ChartPlaceHolder.ClientID;
        details["legendInitializer"] = ChartLegendControl.LegendInitializer;

        return details;
    }

    private void CreateLegendControl()
    {
        if (String.IsNullOrEmpty(ChartResourceSettings.ChartLegendControl))
            return;

        ChartLegendControl = (IChartLegendControl)LoadControl(ChartResourceSettings.ChartLegendControl);
        LegendPlaceholder.Controls.Add((Control)ChartLegendControl);
    }
}
