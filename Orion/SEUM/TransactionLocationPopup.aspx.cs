﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.Shared;

public partial class Orion_SEUM_TransactionLocationPopup : System.Web.UI.Page
{
    internal const string popupName = "Location";
    internal const string locationStatus = "Location&nbsp;status";
    internal const string transactionsWithProblems = "Transactions with problems";
    internal const string allTransactionsAreUp = "All transaction are up";
    internal const string errorMessageLabel = "Error&nbsp;message";
    internal const string playerLoadLabel = "Current&nbsp;player&nbsp;load";
    protected const int TopTransactionCount = 5;

    protected TransactionLocationObject TransactionLocation { get; set; }
    protected List<Transaction> TopTransactions { get; set; }
    protected int RemainingTransactionCount { get; set; }
    
    protected string MoreTransactionsMessage
    {
        get
        {
            return String.Format("{0} more transaction{1} with problems",
                                 RemainingTransactionCount,
                                 RemainingTransactionCount == 1 ? "" : "s");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Disabled caching for popup window (see FB65993)
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        TransactionLocation = NetObjectFactory.Create(Request["NetObject"], true) as TransactionLocationObject;

        var modelsDal = new ModelsDAL();
        var transactions = modelsDal.GetTransactionModelsByAgent(TransactionLocation.Id, false, false).Where(HasTransactionProblems);
        TopTransactions = transactions.Take(TopTransactionCount).ToList();
        RemainingTransactionCount = transactions.Count() - TopTransactionCount;

        playerLoadIndicator.LoadPercentage = TransactionLocation.Model.LoadPercentage;

        this.DataBind();
    }

    private bool HasTransactionProblems(Transaction transaction)
    {
        if (TransactionLocation.Status != StatusInfo.UpStatusId)
            return false;

        switch (transaction.LastStatus)
        {
            case StatusInfo.UpStatusId:
            case StatusInfo.UnmanagedStatusId:
                return false;
            default:
                return true;
        }
    }

    protected string GetStatusImagePath(Transaction transaction)
    {
        return new StatusIconProvider().GetImagePath(SwisEntities.Transactions, transaction.LastStatus, SolarWinds.Orion.Web.UI.StatusIcons.StatusIconSize.Small);
    }
}