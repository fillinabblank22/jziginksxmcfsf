﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.NetObjects;
using Infragistics.WebUI.UltraWebGauge;

public partial class Orion_SEUM_GaugePage : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private string style;
    private string gaugeType;
    private string unitsLabel;
    private string netObject;

    private int scale;
    private int units;
    private int min;
    private int max;
    private string rangeMethod;
    private string rangeValue;
    private float warningThreshold;
    private float criticalThreshold;
    private bool reverseThreshold;
    private double value;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadParameters();
        
        int netObjectID = 0;
        string netObjectPrefix = string.Empty;
        if (!string.IsNullOrEmpty(netObject))
        {
            netObjectID = Convert.ToInt32(netObject.Substring(netObject.IndexOf(":") + 1, netObject.Length - netObject.IndexOf(":") - 1));
            netObjectPrefix = netObject.Substring(0, netObject.IndexOf(":"));
            GetDataByName(netObjectPrefix, netObjectID, ref  value, ref warningThreshold, ref criticalThreshold);
        }

        // Make maximum of gauge dynamic depending on thresholds. If there are no thresholds,
        // make if dynamic depending only on current value.
        double resultValue = value*units;

        // fallback to dynamic method if current is thresholds and there are not thresholds
        if (rangeMethod.Equals("thresholds", StringComparison.OrdinalIgnoreCase))
        {
            if (criticalThreshold == Int32.MaxValue && warningThreshold == Int32.MaxValue)
            {
                rangeMethod = "dynamic";
                rangeValue = "10, 20, 40, 60, 100, 200, 400, 600, 1000";
            }
        }

        switch (rangeMethod)
        {
            case "static":
                if (!Int32.TryParse(rangeValue, out max))
                    max = 30;
                break;
            case "dynamic":
                string[] dynamicRangeStr = rangeValue.Split(',');
                List<int> dynamicRange = new List<int>(dynamicRangeStr.Length);
                foreach (string strVal in dynamicRangeStr)
                {
                    int val;
                    if (Int32.TryParse(strVal.Trim(), out val))
                    {
                        dynamicRange.Add(val);
                    }
                }

                int numScales = dynamicRange.Count;
                int i = 0;
                max = dynamicRange.FirstOrDefault();
                while (resultValue > max && i < numScales)
                {
                    max = dynamicRange[i++];
                }
                break;
            case "thresholds":
                Double thresholdsMultiplier;
                if (!Double.TryParse(rangeValue, out thresholdsMultiplier))
                    thresholdsMultiplier = 1.3;

                if (criticalThreshold != Int32.MaxValue)
                    max = (int)(criticalThreshold * thresholdsMultiplier);
                else
                    max = (int)(warningThreshold*thresholdsMultiplier);
                break;
            default:
                break;
        }
        
        // if max is for some reason smaller than zero, set it to one to make gauge work
        max = Math.Max(1, max);

        System.Drawing.Image image = null;
        BaseGaugeGenerator generator;
        if (gaugeType.Equals("Radial"))
        {
            generator = new GaugeGenerator();

            UltraGauge gauge = generator.GenerateGauge(scale, style, resultValue, min, max, string.Format("{0}{1}", resultValue, unitsLabel),
                                Request.QueryString["Property"], warningThreshold, criticalThreshold, reverseThreshold);

            image = gauge.GetImage(Infragistics.UltraGauge.Resources.GaugeImageType.Png, new Size((int)(gauge.Width.Value), (int)(gauge.Height.Value)));
        }

        using (image)
        using (MemoryStream stream = new MemoryStream())
        {
            image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            Response.ContentType = "image/png";
            stream.WriteTo(Response.OutputStream);
            Response.End();
        }
    }

    private void LoadParameters()
    {
        style = Request.QueryString["Style"];
        gaugeType = Request.QueryString["GaugeType"];
        unitsLabel = Request.QueryString["UnitsLabel"];
        netObject = Request.QueryString["NetObject"];

        if (string.IsNullOrEmpty(unitsLabel)) unitsLabel = " s";

        if (!Single.TryParse(Request.QueryString["WarningThreshold"], out warningThreshold)) { warningThreshold = 0; }
        if (!Single.TryParse(Request.QueryString["CriticalThreshold"], out criticalThreshold)) { criticalThreshold = 0; }
        if (!Boolean.TryParse(Request.QueryString["ReverseThreshold"], out reverseThreshold)) { reverseThreshold = false; }
        if (!Int32.TryParse(Request.QueryString["Min"], out min)) { min = 0; }
        if (!Int32.TryParse(Request.QueryString["Max"], out max)) { max = 10; }
        if (!Int32.TryParse(Request.QueryString["Scale"], out scale)) { scale = 1; }
        if (!Int32.TryParse(Request.QueryString["Units"], out units)) { units = 1; }
        if (!Double.TryParse(Request.QueryString["Value"], out value)) { value = 0; }

        rangeMethod = Request.QueryString["RangeMethod"];
        rangeValue = Request.QueryString["RangeValue"];

        if (string.IsNullOrEmpty(style))
        {
            style = "Elegant Black";
        }
    }

    private void GetDataByName(string netObjectPrefix, int netObjectID, ref double value, ref  float warningThreshold, ref float criticalThreshold)
    {   
        switch (netObjectPrefix.ToUpperInvariant())
        {
            case NetObjectPrefix.Transaction:
                GetDataForTransactionGauge(netObjectID, ref value, ref warningThreshold, ref criticalThreshold);
                break;

            case NetObjectPrefix.TransactionStep:
                GetDataForStepDetailGauge(netObjectID, ref value, ref warningThreshold, ref criticalThreshold);
                break;
        }
    }

    private void GetDataForTransactionGauge(int netObjectID, ref  double value, ref float warningThreshold, ref float criticalThreshold)
    {
        TransactionObject transaction = new TransactionObject(string.Format("{0}:{1}", TransactionObject.Prefix, netObjectID));

        value = transaction.Model.LastDuration.GetValueOrDefault(TimeSpan.Zero).TotalSeconds;
        warningThreshold = Int32.MaxValue;
        criticalThreshold = Int32.MaxValue;      
    }

    private void GetDataForStepDetailGauge(int netObjectID,ref double value,ref float warningTrheshold, ref float criticalThreshold)
    {
        TransactionStepObject step = new TransactionStepObject(string.Format("{0}:{1}",TransactionStepObject.Prefix,netObjectID));

        value = step.Model.LastDuration.GetValueOrDefault(TimeSpan.Zero).TotalSeconds;
        
        warningTrheshold = (float)step.Model.WarningThreshold.TotalSeconds;
        if (warningTrheshold == 0)
            warningTrheshold = Int32.MaxValue;

        criticalThreshold = (float)step.Model.CriticalThreshold.TotalSeconds;
        if (criticalThreshold == 0)
            criticalThreshold = Int32.MaxValue;
    }
}