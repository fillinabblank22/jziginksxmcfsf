﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransactionStepPopup.aspx.cs" Inherits="Orion_SEUM_TransactionStepPopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<!-- Header -->
<h3 class="Status<%=Step.StatusInfo.ToString()%>"><%= popupName %></h3>

<div class="NetObjectTipBody SEUM_NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
    	<tr>
			<td colspan="2"><%=  Step.Name %></td>
		</tr>
		<tr>
			<th><%= stepStatus %>:</th>
			<td>
                <orion:EntityStatusIcon ID="entityStatusIcon" runat="server" IconSize="Small" Entity="<%# Step.SwisEntity %>" Status="<%# Step.Status %>" WrapInBox="false" />
                <%# UIHelper.Escape(Step.StatusDescription) %>
            </td>
		</tr>

        <% if (Step.Status == SEUMStatuses.Failed && !string.IsNullOrEmpty(Step.Model.LastErrorMessage))
               {%>
            <tr>
                <th><%=errorMessageLabel%></th>
                <td><%# UIHelper.Escape(Step.Model.LastErrorMessage)%></td>
            </tr>
            <%
        }%>

        <tr>
			<th><%= stepDuration %>:</th>
			<td><%= (Step.Transaction.LastStatus == SEUMStatuses.Unknown) ? CommonTexts.NotAvailable : new DurationFormatter().Format(Step.LastDuration)%></td>
		</tr>

        <tr>
			<th><%= playbackName %>:</th>
			<td>
                <orion:EntityStatusIcon ID="playbackStatusIcon" runat="server" IconSize="Small" Entity="<%# Step.TransactionObject.SwisEntity %>" Status="<%# Step.TransactionObject.Status %>" WrapInBox="false" />
                <%=  Step.TransactionObject.Name %>
            </td>
		</tr>

        <tr>
			<th><%= locationStatus %>:</th>
            <td>
                <orion:EntityStatusIcon ID="locationStatusIcon" runat="server" IconSize="Small" Entity="<%# SwisEntities.Agents %>" Status="<%# Step.Transaction.Agent.ConnectionStatus %>" WrapInBox="false" />
                <%# UIHelper.Escape(Step.Transaction.Agent.Name) %> (<%= Step.Transaction.Agent.ConnectionStatusMessage %>)
            </td>
		</tr>
	</table>
</div>
	