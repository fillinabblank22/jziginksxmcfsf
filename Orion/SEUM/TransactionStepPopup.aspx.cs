﻿using System;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.Orion.Web;
using System.Web;

public partial class Orion_SEUM_TransactionStepPopup : System.Web.UI.Page
{
    internal const string popupName = "Step";
    internal const string stepStatus = "Step&nbsp;status";
    internal const string stepDuration = "Duration";
    internal const string playbackName = "Transaction";
    internal const string locationStatus = "Location";
    internal const string errorMessageLabel = "Error&nbsp;message";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Disabled caching for popup window (see FB65993)
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        this.Step = NetObjectFactory.Create(Request["NetObject"], true) as TransactionStepObject;

        this.DataBind();
    }

    protected TransactionStepObject Step { get; set; }
}