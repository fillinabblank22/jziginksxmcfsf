﻿<%@ WebService Language="C#" Class="TransactionsHealthTree" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TransactionsHealthTree : WebService, IAjaxTreeService
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    [WebMethod(EnableSession = true)]
    public void ChangeTree(int resourceId, string path, bool expand)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, resourceId);

        if (expand)
        {
            tm.Expand(path);
        }
        else
        {
            tm.Collapse(path);
        }
    }

    [WebMethod(EnableSession = true)]
    public string IsExpanded(int resourceId, string nodeId)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, resourceId);

        return tm.IsExpanded(nodeId) ? nodeId : string.Empty;
    }

    [WebMethod(EnableSession = true)]
    ///<param name="resourceId">ID of resource with tree. This is used to get resource settings.</param>
    public IEnumerable<AjaxTreeNode> GetTreeNodes(int resourceId, string itemType, string itemValue, int fromIndex, int limit, string filter)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        ResourcesDAL dal = new ResourcesDAL();

        IEnumerable<AjaxTreeNode> nodes = null;
        bool hideUnmanaged;

        if (!Boolean.TryParse(resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "true", out hideUnmanaged))
            hideUnmanaged = true;

        int totalCount = 0;

        if (itemType == AjaxTreeNodeType.Status.ToString())
        {
            int statusId;
            if (Int32.TryParse(itemValue, out statusId))
                nodes = dal.GetTransactionsByStatus(statusId, "Name", fromIndex, limit, filter, out totalCount);
        } 
        else
        {
            // get root level
            nodes = dal.GetTransactionStatusesWithCounts(filter, hideUnmanaged);
        }
        
        // If there are more nodes than "limit", we should add "Show more" item in tree. It is represented by empty node.
        if ((totalCount > 0) && (totalCount > fromIndex + limit))
        {
            AjaxTreeNode showMoreNode = new AjaxTreeNode()
                                            {
                                                Id = "",
                                                Name = (totalCount - limit).ToString()
                                            };

            return nodes.Concat(new[] { showMoreNode });
        }
        
        return nodes;
    }
}

