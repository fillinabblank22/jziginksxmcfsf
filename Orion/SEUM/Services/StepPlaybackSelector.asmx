﻿<%@ WebService Language="C#" Class="StepPlaybackSelector" %>
using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Web.Charting;
using SolarWinds.SEUM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class StepPlaybackSelector : WebService
{
    [WebMethod]
    public PageableDataTable GetPlaybacksPaged(int stepId, int timePeriod)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        DateTime fromTime = new DateTime(1900, 1, 1); // can't use MinValue because of SWIS
        DateTime toTime = DateTime.UtcNow;

        TimePeriod period = TimePeriod.FromValue(timePeriod);
        if (period != null)
        {
            fromTime = period.DateTimeFrom;
            toTime = period.DateTimeTo;
        }

        TransactionDAL dal = new TransactionDAL();

        DataTable data = dal.GetFailedStepPlaybacks(
            pageSize,
            startRowNumber,
            sortColumn,
            sortDirection,
            stepId,
            fromTime,
            toTime);

        long count = dal.GetFailedStepPlaybacksCount(stepId, fromTime, toTime);

        return new PageableDataTable(data, count);
    }
    
    [WebMethod]
    public PageableDataTable GetGroupByItems(int stepId, string groupByCategory)
    {
        DataTable table = new DataTable();
        table.Columns.Add("value", typeof(int));
        table.Columns.Add("label", typeof(string));

        switch (groupByCategory)
        {
            case "time":
                table.Rows.Add(0, "All");
                foreach (TimePeriod period in TimePeriod.Periods)
                {
                    if (period != TimePeriod.Custom)
                        table.Rows.Add(period.Value, period.Label);
                }
                break;
        }

        return new PageableDataTable(table, table.Rows.Count);
    }
}