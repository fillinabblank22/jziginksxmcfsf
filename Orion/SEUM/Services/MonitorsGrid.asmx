﻿<%@ WebService Language="C#" Class="MonitorsGrid" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.BusinessLayer.Servicing;
using SolarWinds.Shared;
using SolarWinds.SEUM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class MonitorsGrid  : WebService
{

    private void CheckMonitorsManagement()
    {
        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
            throw new UnauthorizedAccessException("You must have the Admin rights to perform this operation.");
    }

    [WebMethod]
    public PageableDataTable GetMonitorsPaged(string groupByCategory, string groupByItem)
    {
        int pageSize;
        int startRowNumber;

        string search = Context.Request.QueryString["search"];
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        int.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        int.TryParse(Context.Request.QueryString["limit"], out pageSize);

        int groupByItemId;
        int.TryParse(groupByItem, out groupByItemId);

        string filter = string.Empty;
        switch (groupByCategory.ToLower())
        {
            case "status":
                filter = string.Format("Transactions.Status = {0}", groupByItemId);
                break;
            case "recording":
                filter = string.Format("Transactions.RecordingId = {0}", groupByItemId);
                break;
            case "location":
                filter = string.Format("Transactions.AgentId = {0}", groupByItemId);
                break;
            case "licensed":
                filter = string.Format("Transactions.Unmanaged = {0}", groupByItemId);
                break;
        }

        MonitorsDAL dal = new MonitorsDAL();

        DataTable data = dal.GetMonitorsForGrid(
            pageSize,
            startRowNumber,
            sortColumn,
            sortDirection,
            search,
            filter);

        long count = dal.GetMonitorsCount(search);

        return new PageableDataTable(data, count);
    }

    [WebMethod]
    public void DeleteMonitors(IEnumerable<int> monitorsIds)
    {
        CheckMonitorsManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.TransactionDelete(monitorsIds);
        }
    }

    [WebMethod]
    public void EnableMonitors(IEnumerable<int> monitorsIds)
    {
        CheckMonitorsManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.TransactionManage(monitorsIds);
        }
    }

    [WebMethod]
    public int GetLicenseLimit()
    {
        int licLimit;
        using (var proxy = BusinessLayerFactory.Create())
        {
            licLimit = proxy.GetLicenseMaxElementCount();
        }

        return licLimit;
    }

    [WebMethod]
    public int GetEnabledMonitors()
    {
        int monitors;
        using (var proxy = BusinessLayerFactory.Create())
        {
            monitors = proxy.GetAllTransactionCount();
        }

        return monitors;
    }


    [WebMethod]
    public void DisableMonitors(IEnumerable<int> monitorsIds, DateTime? from, DateTime? until)
    {
        CheckMonitorsManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.TransactionUnmanage(monitorsIds,
                                      from.HasValue ? from.Value.ToUniversalTime() : DateTime.MaxValue,
                                      until.HasValue ? until.Value.ToUniversalTime() : DateTime.MaxValue);
        }
    }

    [WebMethod]
    public PageableDataTable GetGroupByItems(string groupByCategory)
    {
        DataTable table = new DataTable();
        table.Columns.Add("value", typeof (string));
        table.Columns.Add("label", typeof (string));
        table.Columns.Add("type", typeof (string));

        MonitorsDAL dal = new MonitorsDAL();

        switch (groupByCategory)
        {
            case "status":
                IEnumerable<int> statuses = dal.GetMonitorStatuses();
                foreach (int id in statuses)
                {
                    StatusInfo status = StatusInfo.GetStatus(id);
                    table.Rows.Add(status.StatusId, status.ShortDescription, groupByCategory);
                }
                break;
            case "recording":
                IEnumerable<MonitorsDAL.NameIdPair> recordings = dal.GetMonitorRecordings();
                foreach (var recording in recordings)
                {
                    table.Rows.Add(recording.Id, UIHelper.Escape(recording.Name), groupByCategory);
                }
                break;
            case "location":
                IEnumerable<MonitorsDAL.NameIdPair> locations = dal.GetMonitorLocations();
                foreach (var location in locations)
                {
                    table.Rows.Add(location.Id, UIHelper.Escape(location.Name), groupByCategory);
                }
                break;
            case "licensed":
                table.Rows.Add(0, "Monitored", groupByCategory);
                table.Rows.Add(1, "Not monitored", groupByCategory);
                break;
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod]
    public void PlayNow(IEnumerable<int> monitorsIds)
    {
        CheckMonitorsManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.TransactionPlayNow(monitorsIds);
        }
    }
}