﻿<%@ WebService Language="C#" Class="StepResources" %>

using System.Data;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class StepResources  : System.Web.Services.WebService {

    [WebMethod]
    public PageableDataTable GetStepRequestsByCategoryPageable(int transactionStepId, int page, int pageSize, BrowserRequest.StatusCategory categories, string filter)
    {
        var resourcesDal = new ResourcesDAL();

        int startRow = ((page - 1)*pageSize) + 1;

        var requests = resourcesDal.GetTopXXStepRequestsByCategory(transactionStepId, startRow, pageSize - 1, categories,
                                                                   filter);

        var count = resourcesDal.GetTopXXStepRequestsByCategoryCount(transactionStepId, categories, filter);
        
        var table = new DataTable();
        table.Columns.Add("MimeType", typeof (string));
        table.Columns.Add("MimeTypeIcon", typeof(string));
        table.Columns.Add("FullUrl", typeof(string));
        table.Columns.Add("ShortUrl", typeof (string));
        table.Columns.Add("StatusCode", typeof (int));
        table.Columns.Add("StatusCodeIcon", typeof (string));
        
        foreach (BrowserRequest browserRequest in requests)
        {
            var shortenedUrl = WrapHelper.ForceWrapUrl(
                SolarWinds.SEUM.Common.Helpers.UrlHelper.ShortenUrlForDisplay(
                    browserRequest.URL,
                    WrapHelper.DefaultMaxUrlLength,
                    WrapHelper.DefaultMaxUrlLength/2,
                    false));

            table.Rows.Add(browserRequest.MimeType, BrowserRequestHelper.GetTypeIconForRequest(browserRequest),
                           browserRequest.URL, shortenedUrl, browserRequest.StatusCode,
                           BrowserRequestHelper.GetIconForStatusCode(browserRequest.StatusCode));
        }
        
        return new PageableDataTable(table, count);
    }
}