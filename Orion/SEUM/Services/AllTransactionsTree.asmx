﻿<%@ WebService Language="C#" Class="TransactionsTree" %>

using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TransactionsTree : WebService, IAjaxTreeService
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    [WebMethod(EnableSession = true)]
    public void ChangeTree(int resourceId, string path, bool expand)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, resourceId);

        if (expand)
        {
            tm.Expand(path);
        }
        else
        {
            tm.Collapse(path);
        }
    }

    [WebMethod(EnableSession = true)]
    public string IsExpanded(int resourceId, string nodeId)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, resourceId);

        return tm.IsExpanded(nodeId) ? nodeId : string.Empty;
    }

    [WebMethod(EnableSession = true)]
    ///<param name="resourceId">ID of resource with tree. This is used to get resource settings.</param>
    public IEnumerable<AjaxTreeNode> GetTreeNodes(int resourceId, string itemType, string itemValue, int fromIndex, int limit, string filter)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

		string grouping = resource.Properties[ResourcePropertiesKeys.Grouping] ?? String.Empty;
		string orderBy = resource.Properties[ResourcePropertiesKeys.Ordering] ?? "Name";
        string statusRollupStr = resource.Properties[ResourcePropertiesKeys.StatusRollup] ?? EvaluationMethod.Mixed.ToString();
        bool hideUnmanaged;
        if (!Boolean.TryParse(resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "True", out hideUnmanaged))
        {
            hideUnmanaged = true;
        }

        EvaluationMethod statusRollup = EvaluationMethod.Mixed;
        if (Enum.IsDefined(typeof(EvaluationMethod), statusRollupStr))
            statusRollup = (EvaluationMethod)Enum.Parse(typeof (EvaluationMethod), statusRollupStr);

        AllTransactionsTreeDAL dal = new AllTransactionsTreeDAL();

        IEnumerable<AjaxTreeNode> nodes = null;

        int totalCount = 0;
        
        if (itemType == AjaxTreeNodeType.Transaction.ToString())
        {
            nodes = dal.GetTransactionSteps(Convert.ToInt32(itemValue), fromIndex, limit, out totalCount);
        } 
        else if (!string.IsNullOrEmpty(itemType))
        {
            if (!string.IsNullOrEmpty(grouping))
                itemValue = itemValue.Substring(6); // remove "group-" prefix
            
            nodes = dal.GetTransactionsByGroup(itemType, itemValue, orderBy, fromIndex, limit, filter, hideUnmanaged, out totalCount);
        }
        else
        {
            // get root level, grouping values are not of AjaxTreeNodeType
            if (string.IsNullOrEmpty(grouping) || grouping.Equals("None", StringComparison.OrdinalIgnoreCase))
            {
                nodes = dal.GetTransactions(orderBy, fromIndex, limit, filter, hideUnmanaged, out totalCount);
            } 
            else
            {
                nodes = dal.GetTransactionsGroups(grouping, fromIndex, limit, filter, hideUnmanaged, out totalCount);
                foreach (AjaxTreeNode node in nodes)
                {
                    node.Id = string.Format("group-{0}", node.Id);
                }
            }
        }
        
        foreach (AjaxTreeNode node in nodes)
        {
            node.StatusRollupType = statusRollup;
        }

        // If there are more nodes than "limit", we should add "Show more" item in tree. It is represented by empty node.
        if ((totalCount > 0) && (totalCount > fromIndex + limit))
        {
            AjaxTreeNode showMoreNode = new AjaxTreeNode()
                                            {
                                                Id = "",
                                                Name = (totalCount - limit).ToString()
                                            };

            return nodes.Concat(new[] { showMoreNode });
        }
        
        return nodes;
    }
    
    [WebMethod(EnableSession = true)]
    public bool GetShowGettingStarted(int resourceId, string filter)
    {
        AllTransactionsTreeDAL dal = new AllTransactionsTreeDAL();
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        bool hideUnmanaged;
        if (!Boolean.TryParse(resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "True", out hideUnmanaged))
        {
            hideUnmanaged = true;
        }

        // check if number of transactions without filter is zero))
        return dal.GetTransactionsCount("", hideUnmanaged) == 0 ;
    }
}

