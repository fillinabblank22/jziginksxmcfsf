﻿<%@ WebService Language="C#" Class="AgentsGrid" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.BusinessLayer.Servicing;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AgentsGrid : WebService
{
    private static readonly object _lock = new Object();
    private static readonly Log _log = new Log();
    private readonly Regex locationUrlRegex = new Regex(@"^(https?://)?(.*)", RegexOptions.IgnoreCase);
    
    private void CheckAgentsManagement()
    {
        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
            throw new UnauthorizedAccessException("You must have the Admin rights to perform this operation.");
    }

    [WebMethod]
	public PageableDataTable GetAgentsPaged()
    {
        return GetAgentsPaged(new int[] {});
    }
	public PageableDataTable GetAgentsPaged(IEnumerable<Int32> excludedIds)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string search = Context.Request.QueryString["search"];
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        AgentsDAL dal = new AgentsDAL();

        DataTable data = dal.GetAgentsForGrid(
            pageSize,
            startRowNumber,
            sortColumn,
            sortDirection,
            search,
            excludedIds);

	    long count = dal.GetAgentsCount(search);

        return new PageableDataTable(data, count);
    }

    [WebMethod]
    public void DeleteAgents(IEnumerable<Int32> agentIds)
    {
        CheckAgentsManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.AgentDelete(agentIds);
        }
    }
    
    [WebMethod]
    public void UpdateAgents(IEnumerable<Int32> agentIds)
    {
        CheckAgentsManagement();
        
        using (var proxy = BusinessLayerFactory.Create())
        {
            foreach (var agentId in agentIds)
            {
                proxy.ScheduleDeployment(agentId, InstallerDeploymentKind.Update);
            }
        }
    }
    
    [WebMethod]
    public Agent GetAgent(int agentId)
    {
        lock (_lock)
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                var agent = proxy.AgentGet(agentId);
                // generate status message for agents with status "preparing" and "updating"
                agent.ConnectionStatusMessage = StatusHelper.ProcessStatusMessage(agent.ConnectionStatus,
                                                                                  agent.ConnectionStatusMessage);
                return agent;
            }
        }
    }

    [WebMethod]
    public IEnumerable<string> GetAgentsTransactions(IEnumerable<int> agentIds, int maxCount)
    {
        AgentsDAL dal = new AgentsDAL();
        return dal.GetAgentsTransactions(agentIds, maxCount);
    }

    [WebMethod]
    public AgentTestResults TestAgent(Agent agent, bool updateRemoteSettings)
    {
        CheckAgentsManagement();

        var proxy = BusinessLayerFactory.Create(agent.PollingEngineId);
        
        if (proxy == null)
        {
            return new AgentTestResults
            {
                IsPollerDown = true,
                Success = false,
                Status = 0, // Unknown
                Message = String.Format("Unable to test connection using poller {0}.", agent.PollingEngineName)
            };
        }
        
        using (proxy)
        {
            // When updating remote settings, current agent has already invalid port andor password.
            // We must use original values to test connectivity.
            if (updateRemoteSettings)
                agent = proxy.AgentGet(agent.AgentId);
            
            var results = proxy.AgentTestSettings(agent);
            return results;
        }        
    }

    [WebMethod]
    public DiagnosticsStatus GetDiagnosticsStatus(int agentId)
    {
        CheckAgentsManagement();

        var agent = GetAgent(agentId);
        using (var proxy = BusinessLayerFactory.Create(agent.PollingEngineId))
        {
            AgentDiagnosticsStatus status = proxy.GetAgentDiagnosticsStatus(agentId);
            if (status == null)
                return null;
            
            DiagnosticsStatus result = new DiagnosticsStatus()
                                           {
                                               FileName = status.FileName,
                                               Exists = status.Exists,
                                               Running = status.Running,
                                               DateTime = status.DateTime.ToLocalTime().ToString(),
                                               CurrentLogLevel = status.CurrentLogLevel
                                           };

            return result;
        }
    }

    [WebMethod]
    public void GenerateDiagnostics(int agentId)
    {
        CheckAgentsManagement();
        
        var agent = GetAgent(agentId);
        using (var proxy = BusinessLayerFactory.Create(agent.PollingEngineId))
        {
            proxy.GenerateAgentDiagnostics(agentId);
        }
    }

    [WebMethod]
    public IEnumerable<Agent> GetActiveAgentsWaitingForAdd()
    {
        CheckAgentsManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            return proxy.GetActiveAgentsWaitingForAdd();
        }
    }
    
    [WebMethod]
    public void DeployWithExistingCredential(IEnumerable<int> agentIds, int credentialId)
    {
        CheckAgentsManagement();
        
        CredentialManager _credentialManager = new CredentialManager();

        UsernamePasswordCredential credential;
        try
        {
            credential = _credentialManager.GetCredential<UsernamePasswordCredential>(credentialId);
        }
        catch (CredentialNotFoundException ex)
        {
            _log.Error("Unable to load credentials.", ex);
            throw new ArgumentException("Invalid credentials.", "credentialId");
        }

        if (credential.IsBroken)
        {
            _log.Error("Loaded broken credential");
            throw new ArgumentException("Invalid credentials.", "credentialId");
        }
        
        // we have username and password now, so we can initate deployment using these
        DeployWithNewCredential(agentIds, credential.Username, credential.Password);
    }
    
    [WebMethod]
    public void DeployWithNewCredential(IEnumerable<int> agentIds, string userName, string password)
    {
        CheckAgentsManagement();
        
        if (agentIds == null || !agentIds.Any())
            throw new ArgumentException("Agent ids collection cannot be null or empty.", "agentIds");
        if (string.IsNullOrEmpty(userName))
            throw new ArgumentException("Username cannot be null or empty.", "userName");
        if (string.IsNullOrEmpty(password))
            throw new ArgumentException("Password cannot be null or empty.", "password");
        
        using(var proxy = BusinessLayerFactory.Create())
        {
            foreach (int agentId in agentIds)
            {
                Agent agent;
                try 
                {
                    agent = proxy.AgentGet(agentId);
                }
                catch (KeyNotFoundException ex)
                {
                    _log.Error("Unable to load agent.", ex);
                    continue;
                }

                string locationFromUrl = locationUrlRegex.Match(agent.Url).Groups[2].Value;

                if(string.IsNullOrEmpty(agent.DNSName) && string.IsNullOrEmpty(agent.IP) && string.IsNullOrEmpty(locationFromUrl))
                {
                    _log.ErrorFormat("Unable to get both IP and hostname of the player machine. Deployment to this machine is not possible. Player ID {0}", agent.AgentId);
                    continue;
                }
                
                try
                {
                    var deploymentDescription = new AgentDeploymentDescription()
                    {
                        PlayerPort = (ushort)agent.Port,
                        PlayerPassword = agent.Password ?? string.Empty,
                        MachineUsername = userName,
                        MachinePassword = password,
                        Hostname = (new [] { agent.DNSName, agent.IP, locationFromUrl }).First(i => !string.IsNullOrEmpty(i)),
                        IPAddress = (new [] { agent.IP, agent.DNSName, locationFromUrl }).First(i => !string.IsNullOrEmpty(i))
                    };

                    proxy.ScheduleDeployment(agentId, InstallerDeploymentKind.Install, deploymentDescription);
                }
                catch (Exception ex)
                {
                    _log.Error("Unable to schedule player deployment for agent ID " + agentId + ".", ex);
                    continue;
                }
            }
        }
    }
    
    public class DiagnosticsStatus
    {
        public string FileName { get; set; }
        public string DateTime { get; set; }
        public bool Running { get; set; }
        public bool Exists { get; set; }
        public string CurrentLogLevel { get; set; }
    }
}

