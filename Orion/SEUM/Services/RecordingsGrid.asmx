﻿<%@ WebService Language="C#" Class="RecordingsGrid" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.WPM.Recordings.Servicing;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Actions;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class RecordingsGrid : WebService
{

    private void CheckRecordingsManagement()
    {
        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
            throw new UnauthorizedAccessException("You must have the Admin rights to perform this operation.");
    }

    private static IRecordingsBusinessLayer CreateRecordingsBusinessLayer()
    {
        var factory = new RecordingsBusinessLayerFactory();

        return factory.Create();
    }

    [WebMethod]
    public PageableDataTable GetRecordingsPaged()
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string search = Context.Request.QueryString["search"];
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        RecordingDAL dal = new RecordingDAL();

        DataTable data = dal.GetRecordingsForGrid(
            pageSize,
            startRowNumber,
            sortColumn,
            sortDirection,
            search);

        long count = dal.GetRecordingsCount(search);

        return new PageableDataTable(data, count);
    }

    [WebMethod]
    public IEnumerable<string> GetRecordingsTransactions(IEnumerable<int> recordingIds, int maxCount)
    {
        RecordingDAL dal = new RecordingDAL();
        return dal.GetRecordingsTransactions(recordingIds, maxCount);
    }


    [WebMethod]
    public PageableDataTable GetSteps(int recordingId)
    {
        RecordingDAL dal = new RecordingDAL();

        DataTable pageData = dal.GetRecordingStepsForGrid(recordingId);

        return new PageableDataTable(pageData, pageData.Rows.Count);
    }

    [WebMethod]
    public PageableDataTable GetActions(int recordingStepId)
    {
        RecordingDAL dal = new RecordingDAL();

        DataTable pageData = dal.GetRecordingStepActionsForGrid(recordingStepId);

        return new PageableDataTable(pageData, pageData.Rows.Count);
    }

    [WebMethod]
    public bool EditRecording(int recordingId, string recordingName)
    {
        CheckRecordingsManagement();

        ModelsDAL dal = new ModelsDAL();
        Recording recording = dal.GetRecordingModel(recordingId);

        // allow only unique names
        RecordingDAL recDal = new RecordingDAL();
        if (!recDal.IsUniqueRecordingName(recordingName, recording.RecordingId))
        {
            return false;
        }
        else
        {
            using (var proxy = CreateRecordingsBusinessLayer())
            {
                proxy.RecordingRename(recording.RecordingId, recordingName);
            }
            return true;
        }
    }

    [WebMethod]
    public void EditRecordingStep(int stepId, string stepName)
    {
        CheckRecordingsManagement();

        using(var proxy = CreateRecordingsBusinessLayer())
        {
            proxy.RecordingStepRename(stepId, stepName);
        }
    }

    [WebMethod]
    public void DeleteRecordings(IEnumerable<Int32> recordingIds)
    {
        CheckRecordingsManagement();

        using (var proxy = CreateRecordingsBusinessLayer())
        {
            proxy.RecordingDelete(recordingIds);
        }
    }

    [WebMethod]
    public void DeleteAction(int stepId, Guid actionId)
    {
        CheckRecordingsManagement();

        ModelsDAL dal = new ModelsDAL();
        IRecordingStep recordingStep = dal.GetRecordingStepModel(stepId);
        ActionBase action = recordingStep.Actions.First(a => a.Guid == actionId);

        if(!recordingStep.RemoveAction(action))
        {
            return;
        }

        using(var proxy = CreateRecordingsBusinessLayer())
        {
            proxy.RecordingStepUpdate(recordingStep);
        }
    }

    [WebMethod]
    public ActionVerifyContent UpdateActionVerifyContent(int stepId, Guid actionId, int maxWaitTimeMs, bool shouldContain, string textToVerify)
    {
        CheckRecordingsManagement();

        ModelsDAL dal = new ModelsDAL();
        IRecordingStep recordingStep = dal.GetRecordingStepModel(stepId);
        ActionVerifyContent actionToUpdate = recordingStep.Actions.First(a => a.Guid == actionId) as ActionVerifyContent;

        // update values
        actionToUpdate.MaxWaitTimeMs = maxWaitTimeMs;
        actionToUpdate.ShouldContain = shouldContain;
        actionToUpdate.TextToVerify = textToVerify;

        using(var proxy = CreateRecordingsBusinessLayer())
        {
            proxy.RecordingStepUpdate(recordingStep);
        }

        return actionToUpdate;
    }

    [WebMethod]
    public ActionTextWrite UpdateActionTextWrite(int stepId, Guid actionId, string text)
    {
        CheckRecordingsManagement();

        ModelsDAL dal = new ModelsDAL();
        IRecordingStep recordingStep = dal.GetRecordingStepModel(stepId);
        ActionTextWrite actionToUpdate = recordingStep.Actions.First(a => a.Guid == actionId) as ActionTextWrite;

        // update values
        actionToUpdate.TextForInput = new TextInfo(text);

        using (var proxy = CreateRecordingsBusinessLayer())
        {
            proxy.RecordingStepUpdate(recordingStep);
        }

        return actionToUpdate;
    }
}

