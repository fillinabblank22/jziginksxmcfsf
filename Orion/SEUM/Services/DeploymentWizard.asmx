﻿<%@ WebService Language="C#" Class="DeploymentWizard" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.BusinessLayer.Servicing;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class DeploymentWizard : WebService
{
    private static readonly Log _log = new Log();
    private readonly CredentialManager _credentialManager = new CredentialManager();
    protected const string NewCredentialText = "<New Credential>";
    protected const string InvalidHostnameError = "Location was not found";
    protected const string InvalidPollingEngineError = "Polling engine for location was not found.";
    protected const string PollingEngineConnectionError = "Unable to connect to polling engine.";
    protected const string MissingCredentialsError = "Location does not have assigned credentials.";
    protected const string UnknownError = "Unknown error.";

    private void CheckAgentsManagement()
    {
        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
            throw new UnauthorizedAccessException("You must have the Admin rights to perform this operation.");
    }

    [WebMethod]
    public PageableDataTable GetNodesPaged(string groupByCategory, string groupByItem)
    {
        CheckAgentsManagement();

        int pageSize = 0;
        int startRowNumber = 0;

        string search = Context.Request.QueryString["search"];
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        string filter = string.Empty;

        if (!string.IsNullOrEmpty(groupByCategory) && groupByCategory != "none")
        {
            string columnName = groupByCategory.Split(new[] {'.'}, 2)[1];

            if (groupByItem == NodesDAL.UnknownValue)
            {
                filter = string.Format("({0} IS NULL OR {0} = '')", columnName);
            }
            else
            {
                filter = string.Format("{0} = '{1}'", columnName, groupByItem);
            }
        }

        var dal = new NodesDAL();

        DataTable data = dal.GetNodesForGrid(
            pageSize,
            startRowNumber,
            sortColumn,
            sortDirection,
            search,
            filter);

        long count = dal.GetNodesCount(search);

        return new PageableDataTable(data, count);
    }

    [WebMethod]
    public PageableDataTable GetGroupByCategories()
    {
        CheckAgentsManagement();

        DataTable table = new DataTable();
        table.Columns.Add("Value", typeof (string));
        table.Columns.Add("Label", typeof (string));

        ContainersMetadataDAL dal = new ContainersMetadataDAL();

        table.Rows.Add("none", Resources.CoreWebContent.WEBCODE_AK0_70);

        foreach (var property in dal.GetEntityProperties("Orion.Nodes", ContainersMetadataDAL.PropertyType.GroupBy))
        {
            table.Rows.Add(property.FullyQualifiedName, UIHelper.Escape(property.DisplayName));
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod]
    public PageableDataTable GetGroupByItems(string groupByCategory)
    {
        CheckAgentsManagement();

        DataTable table = new DataTable();
        table.Columns.Add("value", typeof(string));
        table.Columns.Add("label", typeof(string));
        table.Columns.Add("type", typeof(string));

        var dal = new NodesDAL();

        if(groupByCategory != "none")
        {
            foreach (KeyValuePair<string, int> pair in dal.GetEntityGroups(groupByCategory))
            {
                if (groupByCategory == "Orion.Nodes.Status")
                    table.Rows.Add(pair.Value, pair.Key, groupByCategory);
                else
                    table.Rows.Add(pair.Key, pair.Key, groupByCategory);
            }
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetLocationsForCredentialsGrid()
    {
        CheckAgentsManagement();

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        DataTable table = new DataTable();
        table.Columns.Add("ID", typeof(string));   // the ID is computer's hostname
        table.Columns.Add("Name", typeof(string));
        table.Columns.Add("CredentialId", typeof(int));
        table.Columns.Add("CredentialNames", typeof(string));
        table.Columns.Add("StatusName", typeof(string));

        IEnumerable<DeploymentHost> hosts;
        switch (sortColumn)
        {
            case "Name":
                hosts = sortDirection == "ASC"
                            ? DeploymentWizardWorkflow.Instance.Hosts.OrderBy(h => h.Name)
                            : DeploymentWizardWorkflow.Instance.Hosts.OrderByDescending(h => h.Name);
                break;
            default:
                hosts = DeploymentWizardWorkflow.Instance.Hosts;
                break;
        }

        foreach (DeploymentHost host in hosts)
        {
            string credentialName = string.Empty;
            switch (host.CredentialId)
            {
                case -1:    // no credentials assigned
                    break;
                case 0:     // credentials assigned by us
                    if (host.Credential != null)
                    {
                        credentialName = host.Credential.Name;
                    }
                    else
                    {
                        // if assigned credentials are empty remove assignment
                        host.CredentialId = -1;
                    }
                    break;
                default:
                    if (host.Credential != null && !host.Credential.IsBroken)
                    {
                        credentialName = host.Credential.Name;
                    }
                    else
                    {
                        // if assigned credentials are empty load credentials using the credentials manager
                        try
                        {
                            host.Credential = _credentialManager.GetCredential<UsernamePasswordCredential>(host.CredentialId);
                            if(host.Credential.IsBroken)
                            {
                                _log.Error("Loaded broken credential.");
                                host.CredentialId = -1;
                            }
                            else
                            {
                                credentialName = host.Credential.Name;
                            }
                        }
                        catch (CredentialNotFoundException ex)
                        {
                            _log.Error("Unable to load credentials.", ex);
                            host.CredentialId = -1; // remove assignement
                        }
                    }
                    break;
            }
            table.Rows.Add(host.Hostname, host.Name, host.CredentialId, credentialName, host.StatusName);
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public void RemoveLocation(string[] hostnames)
    {
        CheckAgentsManagement();

        foreach (string hostname in hostnames)
        {
            DeploymentWizardWorkflow.Instance.Remove(hostname);
        }
    }

    [WebMethod(EnableSession = true)]
    public string AssignExistingCredentials(string[] hostnames, int credentialId)
    {
        CheckAgentsManagement();

        UsernamePasswordCredential credential;
        try
        {
            credential = _credentialManager.GetCredential<UsernamePasswordCredential>(credentialId);
        }
        catch (CredentialNotFoundException ex)
        {
            _log.Error("Unable to load credentials.", ex);
            return string.Empty;
        }

        if(credential.IsBroken)
        {
            _log.Error("Loaded broken credential");
            return string.Empty;
        }

        foreach (var hostname in hostnames)
        {
            var host = DeploymentWizardWorkflow.Instance.GetHost(hostname);
            if(host == null)
            {
                _log.Warn("Could not find host with hostname: " + hostname);
                continue;
            }

            host.CredentialId = credential.ID.GetValueOrDefault(-1);
            host.Credential = credential;
        }

        return credential.Name;
    }

    [WebMethod(EnableSession = true)]
    public void AssignNewCredentials(string[] hostnames, string username, string password)
    {
        CheckAgentsManagement();

        var newCredential = GetTempCredential(username, password);

        foreach (var hostname in hostnames)
        {
            var host = DeploymentWizardWorkflow.Instance.GetHost(hostname);
            if (host == null)
            {
                _log.Warn("Could not find host with hostname: " + hostname);
                continue;
            }

            host.CredentialId = 0;
            host.Credential = newCredential;
        }
    }

    [WebMethod(EnableSession = true)]
    public AgentDeploymentTestResults TestCredentials(string hostname, int credentialId, string username, string password)
    {
        CheckAgentsManagement();

        var host = DeploymentWizardWorkflow.Instance.GetHost(hostname);
        if (host == null)
        {
            _log.Warn("Could not find host with hostname: " + hostname);
            return new AgentDeploymentTestResults(false, InvalidHostnameError);
        }

        if (credentialId > 0)
        {
            try
            {
                host.Credential = _credentialManager.GetCredential<UsernamePasswordCredential>(credentialId);
            }
            catch (CredentialNotFoundException ex)
            {
                _log.Debug("Unable to load credentials.", ex);
            }
        }
        else if (!string.IsNullOrWhiteSpace(username))
        {
            host.Credential = GetTempCredential(username, password);
        }
        
        if (host.Credential == null)
        {
            return new AgentDeploymentTestResults(false, MissingCredentialsError);
        }

        try
        {
            var proxy = BusinessLayerFactory.Create(host.PollerId);
            if (proxy == null)
            {
                return new AgentDeploymentTestResults(false, PollingEngineConnectionError);
            }
            using (proxy)
            {
                return proxy.AgentValidateDeploymentCredentials(hostname, host.Credential.Username,
                                                                host.Credential.Password);
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error testing player deployment credentials.", ex);
            return new AgentDeploymentTestResults(false, UnknownError);
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetLocationsForSettingsGrid()
    {
        CheckAgentsManagement();

        AgentsDAL agentsDal = new AgentsDAL();
        var primaryPollerId = agentsDal.GetAvailablePollingEngines().First().Key;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        DataTable table = new DataTable();
        table.Columns.Add("ID", typeof(string));   // the ID is computer's hostname
        table.Columns.Add("LocationName", typeof(string));
        table.Columns.Add("PlayerName", typeof(string));
        table.Columns.Add("PollerId", typeof(int));
        table.Columns.Add("PlayerPort", typeof(int));
        table.Columns.Add("PlayerPassword", typeof(string));

        IEnumerable<DeploymentHost> hosts;
        switch (sortColumn)
        {
            case "LocationName":
                hosts = sortDirection == "ASC"
                            ? DeploymentWizardWorkflow.Instance.Hosts.OrderBy(h => h.Name)
                            : DeploymentWizardWorkflow.Instance.Hosts.OrderByDescending(h => h.Name);
                break;
            default:
                hosts = DeploymentWizardWorkflow.Instance.Hosts;
                break;
        }

        foreach (DeploymentHost host in hosts)
        {
            if (host.PollerId == 0)
                host.PollerId = primaryPollerId;

            table.Rows.Add(host.Name, host.Name, host.PlayerName, host.PollerId, host.PlayerPort, host.PlayerPassword);
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public void UpdateLocationSettings(string hostName, string playerName, int pollerId, int port, string playerPassword)
    {
        CheckAgentsManagement();

        var host = DeploymentWizardWorkflow.Instance.GetHost(hostName);
        if(host == null)
        {
            _log.Error("Unable to load host. Hostname: " + hostName);
            return;
        }

        host.PlayerName = playerName;
        host.PollerId = pollerId;
        host.PlayerPort = (UInt16)port;
        host.PlayerPassword = playerPassword;
    }

    [WebMethod]
    public PageableDataTable GetCredentials()
    {
        CheckAgentsManagement();

        var table = new DataTable();
        table.Columns.Add("Key", typeof (int));
        table.Columns.Add("Value", typeof (string));

        table.Rows.Add(-1, NewCredentialText);

        var coreCredentials =
            _credentialManager.GetCredentialNames<UsernamePasswordCredential>(CoreConstants.CoreCredentialOwner);

        foreach (var coreCredential in coreCredentials)
        {
            table.Rows.Add(coreCredential.Key, coreCredential.Value);
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    private static UsernamePasswordCredential GetTempCredential(string username, string password)
    {
        return new UsernamePasswordCredential()
        {
            ID = null,
            Name = username,
            Description = "WPM Deployment Wizard temporary credential",
            Username = username,
            Password = password
        };
    }
}