﻿<%@ WebService Language="C#" Class="LocationChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.Shared;
using SEUMCharting = SolarWinds.SEUM.Web.Charting;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;
using SolarWinds.SEUM.Web.NetObjects;
using ChartDataResults = SolarWinds.Orion.Web.Charting.v2.ChartDataResults;
using DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;
using DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class LocationChartData : ChartDataWebService
{
    [WebMethod]
    public ChartDataResults AgentLoad(ChartDataRequest request)
    {
        var chartsDal = new ChartsDAL();
        var modelsDal = new ModelsDAL();

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        int agentId = request.NetObjectIds.Select(x => int.Parse(x)).First();
        DataTable loadPercentage = chartsDal.GetAgentLoadPercentage(agentId, dateRange.StartDate, dateRange.EndDate);
        
        if(loadPercentage.Rows.Count == 0)
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        
        DataSeries loadSeries = new DataSeries() {NetObjectId = agentId.ToString(), TagName = "AgentLoad"};
        
        Dictionary<DateTime, double> values = new Dictionary<DateTime, double>();
        
        foreach (DataRow row in loadPercentage.Rows)
        {
            values.Add(((DateTime) row["DateTimeUtc"]).ToLocalTime(), (double) row["LoadPercentage"]);
        }

        // compute missing values
        var computed = SEUMCharting.ChartDataProcessor.ComputeMissingPoints(values, TimeSpan.FromMinutes(request.SampleSizeInMinutes));
        
        foreach (KeyValuePair<DateTime, double> pair in computed)
        {
            loadSeries.AddPoint(DataPoint.CreatePoint(pair.Key, pair.Value));
        }
        
        // load agent info
        var agent = modelsDal.GetAgentModel(agentId);
        
        var seriesToReturn = new List<DataSeries>();

        // create sampled load series
        var sampled = loadSeries.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Average);
        sampled.Label = agent.Name;
        sampled.TagName = "AgentLoad";
        
        seriesToReturn.Add(sampled);

        if (!seriesToReturn.Any() || !seriesToReturn.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        
        // if enabled create also trend series
        if(request.CalculateTrendLine)
            seriesToReturn.Add(loadSeries.CreateTrendSeries(dateRange, request.SampleSizeInMinutes));

        return new ChartDataResults(seriesToReturn);
    }

    [WebMethod]
    public ChartDataResults Availability(ChartDataRequest request)
    {
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        int agentId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out agentId);
        }
        if (agentId == 0)
        {
            throw new ArgumentException("No Location ID is defined in request.");
        }

        ChartsDAL dal = new ChartsDAL();
        DataTable dt = dal.GetTransactionAvailabilityForAgent(agentId, dateRange.StartDate, dateRange.EndDate);

        var sampledSeries = ChartHelper.GetBasicAvailabilitySeries(dt, request, true);
        
        return new ChartDataResults(sampledSeries);
    }
    
    [WebMethod]
    public ChartDataResults TransactionsByDuration(ChartDataRequest request)
    {
        List<DataSeries> series = new List<DataSeries>();
        List<object> transactionsDetails = new List<object>();
        int agentId = 0;
        int maxCount = 10;
        object maxCountStr;
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        DurationFormatter durationFormatter = new DurationFormatter();
        ChartsDAL dal = new ChartsDAL();

        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out agentId);
        }
        if (agentId == 0)
        {
            throw new ArgumentException("No Location ID is defined in request.");
        }

        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.MaxCount, out maxCountStr))
        {
            if (!Int32.TryParse(maxCountStr.ToString(), out maxCount))
            {
                maxCount = 10;
            }
        }

        IEnumerable<TransactionDurations> transactions = dal.GetTransactionsDurationsForAgent(
            agentId,
            maxCount,
            dateRange.StartDate,
            dateRange.EndDate);
        
        foreach (TransactionDurations transactionDurations in transactions)
        {
            DataSeries serie = new DataSeries()
            {
                Label = WrapHelper.ForceWrap(transactionDurations.Transaction.Name, 50, 50, "..."),
                TagName = "TransactionDuration",
                NetObjectId = string.Format(string.Format("{0}:{1}", TransactionObject.Prefix, transactionDurations.Transaction.TransactionId))
            };

            // Put all non-zero value to data serie for chart. Zero value will be nulls in chart.
            foreach (KeyValuePair<DateTime, int?> sample in transactionDurations.Durations.Where(x => x.Value.HasValue))
            {
                serie.AddPoint(DataPoint.CreatePoint(sample.Key, sample.Value.Value));
            }

            series.Add(serie);

            // generate steps details for chart options
            string durationClass = string.Empty;
            switch (transactionDurations.Transaction.LastStatus)
            {
                case SEUMStatuses.Warning:
                    durationClass = "SEUM_WarningValueText";
                    break;
                case SEUMStatuses.Critical:
                    durationClass = "SEUM_CriticalValueText";
                    break;
            }

            transactionsDetails.Add(new
            {
                name = transactionDurations.Transaction.Name,
                link = String.Format(CommonLinks.NetObjectViewLinkFormat, NetObjectPrefix.Transaction, transactionDurations.Transaction.TransactionId),
                currentDuration = durationFormatter.Format(transactionDurations.Transaction.LastDuration.HasValue
                    ? transactionDurations.Transaction.LastDuration.Value.TotalMilliseconds
                    : 0),
                averageDuration = durationFormatter.Format(transactionDurations.AverageDuration),
                customClass = durationClass
            });
        }

        List<DataSeries> sampledSeries = new List<DataSeries>(
            series.Select(serie => serie.CreateSampledSeries(
                dateRange, request.SampleSizeInMinutes, SampleMethod.Average)));

        // add average serie and it's details
        int cumulativeAverage;
        DataSeries averageSerie = ChartHelper.GetAverageSerie(sampledSeries, out cumulativeAverage);
        averageSerie.TagName = "CumulativeAverage";
        averageSerie.Label = CommonTexts.CumulativeAverageLabel;
        sampledSeries.Add(averageSerie);

        transactionsDetails.Add(new
        {
            name = CommonTexts.CumulativeAverageLabel,
            currentDuration = durationFormatter.Format(averageSerie.Data.Any(x => !x.IsNullPoint) 
                ? averageSerie.Data.Last(x => !x.IsNullPoint).Value 
                : 0),
            averageDuration = durationFormatter.Format(cumulativeAverage),
            customClass = string.Empty
        });

        if(!sampledSeries.Any()  || !sampledSeries.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        
        ChartDataResults result = new ChartDataResults(sampledSeries);
        // put transactions details to chart options so that they can be accessed from legend
        result.ChartOptionsOverride = new
        {
            transactions = transactionsDetails.ToArray()
        };

        return result;
    }
}