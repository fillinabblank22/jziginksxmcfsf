﻿<%@ WebService Language="C#" Class="SummaryChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using ChartDataResults = SolarWinds.SEUM.Web.Charting.ChartDataResults;
using DataPoint = SolarWinds.SEUM.Web.Charting.DataPoint;
using DataSeries = SolarWinds.SEUM.Web.Charting.DataSeries;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class SummaryChartData  : System.Web.Services.WebService {
    private static readonly Log _log = new Log();

    [WebMethod]
    public ChartDataResults TransactionHealthOverview(ChartDataRequest request)
    {
        object settingValue;

        string filter = String.Empty;
        bool hideUnmanaged = true;
        bool rememberExpanded = true;
        int resourceId = 0;

        // set view info to httpcontext so the view limitations are properly applied
        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.ViewId, out settingValue))
        {
            Context.Items[typeof (ViewInfo).Name] = ViewManager.GetViewById((int)settingValue);
        }

        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.Filter, out settingValue))
        {
            filter = settingValue as String ?? String.Empty;
        }

        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.HideUnmanaged, out settingValue))
        {
            if (!Boolean.TryParse(settingValue as String, out hideUnmanaged))
                hideUnmanaged = true;
        }

        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.RememberCollapseState, out settingValue))
        {
            if (!Boolean.TryParse(settingValue as String, out rememberExpanded))
                rememberExpanded = true;
        }

        if (request.AllSettings.TryGetValue("SEUM-Summary-ResourceId", out settingValue) && settingValue != null)
        {
            resourceId = (int)settingValue;
        }

        var chartDal = new ChartsDAL();

        DataTable data = null;
        try
        {
            data = chartDal.GetTransactionsHealthOverviewData(filter, hideUnmanaged).Tables[0];
        }
        catch (Exception ex)
        {
            // just log this and return empty chart, we will show error when loading tree
            _log.Error("Unable to load transactions for Transaction Health Overview resource. Filter is set to: '" + filter + "'", ex);
        }

        var dataSeriesColection = new List<DataSeries>();
        dataSeriesColection.Add(new DataSeries()
        {
            TagName = "Default"
        });

        var result = new ChartDataResults(dataSeriesColection);

        result.ChartOptionsOverride = new
        {
            hideUnmanaged = hideUnmanaged,
            filter = filter,
            rememberExpanded = rememberExpanded,
            resourceId = resourceId
        };

        if (data == null || data.Rows.Count == 0)
        {
            return result;
        }

        var colors = new Dictionary<int, string>()
                         {
                             {SEUMStatuses.Up, "#77bd2d"},
                             {SEUMStatuses.Warning, "#fcd928"},
                             {SEUMStatuses.Critical, "#f99d1c"},
                             {SEUMStatuses.Failed, "#e61929"},
                             {SEUMStatuses.Unknown, "#A4A4A4"},
                             {SEUMStatuses.Unmanaged, "#211c7b"},
                             {SEUMStatuses.Unreachable, "#646464"}
                         };

        // DataRow row = data.Rows[0];
        foreach (DataRow row in data.Rows)
        {
            if ((int)row["value"] == 0)
                continue;

            dataSeriesColection[0].Data.Add(new DataPoint((string)row["label"], new Dictionary<string, object>()
                                                                                                 {
                                                                                                     {"y", row["value"]},
                                                                                                     {"count", row["value"]},
                                                                                                     {"color", colors[(int)row["status"]]}
                                                                                                 }));
        }

        return result;
    }
}