﻿<%@ WebService Language="C#" Class="StepChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Helpers;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;
using SolarWinds.SEUM.Web.NetObjects;
using ChartDataResults = SolarWinds.SEUM.Web.Charting.ChartDataResults;
using DataPoint = SolarWinds.SEUM.Web.Charting.DataPoint;
using DataSeries = SolarWinds.SEUM.Web.Charting.DataSeries;
using CoreCharting = SolarWinds.Orion.Web.Charting.v2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class StepChartData : ChartDataWebService
{
    private const string MoreRequestsNotificationTextFormat =
        "show {0} remaining request";
    private const string MoreRequestsNotificationTextFormatPlural =
        "show {0} remaining requests";

    private const string PageElementsLegendSingularFormat = "{0} ({1} request, {2})";
    private const string PageElementsLegendPluralFormat = "{0} ({1} requests, {2})";

    // Parameter @NodeID will contain list of NetObjectIds from ChartDataRequest. Unfortunately its name is hardcoded in Orion Core even it doesn't make sense for modules. 
    private readonly string _transactionStepNameLookupSwql = string.Format(
        "SELECT TS.TransactionStepId, CONCAT(TS.DisplayName, ' on ', TS.Transaction.DisplayName) AS DisplayName " +
        "FROM {0} AS TS " +
        "INNER JOIN {1} AS T ON (TS.TransactionId = T.TransactionId) " +
        "WHERE TransactionStepId IN @NodeID",
        SwisEntities.TransactionSteps, SwisEntities.Transactions);
    
    private enum DataSerieType
    {
        Requests = 0,
        ContentDownload = 1,
        TimeToFirstByte = 2,
        Send = 3,
        Connection = 4,
        DNSLookup = 5,
        InitialDelay = 6
    }

    [WebMethod]
    public CoreCharting.ChartDataResults MMADuration(ChartDataRequest request)
    {
        // make sure we ignore -1 or -2 values since these have special meaning.
        const string sql = @"SELECT DateTimeUtc,  
                                    NULLIF(NULLIF(AvgDuration, -1), -2) AvgDuration, 
                                    NULLIF(NULLIF(MinDuration, -1), -2) MinDuration, 
                                    NULLIF(NULLIF(MaxDuration, -1), -2) MaxDuration
                             FROM SEUM_StepResponseTime 
                             WHERE TransactionStepId = @NetObjectId
                             AND DateTimeUtc >= @StartTime AND DateTimeUtc <= @EndTime
                             ORDER BY [DateTimeUtc]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, _transactionStepNameLookupSwql, DateTimeKind.Utc, "AvgDuration", "MinMaxDuration");

        if (!result.DataSeries.Any() || result.DataSeries.All(s => s.Data.All(dp => dp.IsNullPoint)))
            return new SolarWinds.Orion.Web.Charting.v2.ChartDataResults(Enumerable.Empty<SolarWinds.Orion.Web.Charting.v2.DataSeries>());
        
        // generate plot lines thresholds override
        result.ChartOptionsOverride = ChartHelper.GetThresholdPlotLinesValuesOverride(
            request.AllSettings,
            ResourcePropertiesKeys.DisplayOptimalThreshold, 
            ResourcePropertiesKeys.OptimalThresholdMs, 
            ResourcePropertiesKeys.DisplayWarningThreshold,
            ResourcePropertiesKeys.WarningThresholdMs,
            ResourcePropertiesKeys.DisplayCriticalThreshold,
            ResourcePropertiesKeys.CriticalThresholdMs);

        return result;
    }

    [WebMethod]
    public CoreCharting.ChartDataResults Availability(ChartDataRequest request)
    {
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        int stepId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out stepId);
        }
        if (stepId == 0)
        {
            throw new ArgumentException("No Step ID is defined in request.");
        }

        ChartsDAL dal = new ChartsDAL();
        DataTable dt = dal.GetTransactionStepAvailability(stepId, dateRange.StartDate, dateRange.EndDate);

        var sampledSeries = ChartHelper.GetBasicAvailabilitySeries(dt, request, true);
        
        return new CoreCharting.ChartDataResults(sampledSeries);
    }

    [WebMethod]
    public CoreCharting.ChartDataResults LocationsDurations(ChartDataRequest request)
    {
        var series = new List<CoreCharting.DataSeries>();
        List<object> stepsDetails = new List<object>();
        int stepId = 0;
        int maxCount = 10;
        object settingValue;
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        DurationFormatter durationFormatter = new DurationFormatter();
        ChartsDAL dal = new ChartsDAL();

        // set view info to httpcontext so the view limitations are properly applied
        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.ViewId, out settingValue))
        {
            Context.Items[typeof(ViewInfo).Name] = ViewManager.GetViewById((int)settingValue);
        }

        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out stepId);
        }
        if (stepId == 0)
        {
            throw new ArgumentException("No Step ID is defined in request.");
        }
        TransactionStep step = new ModelsDAL().GetTransactionStepModel(stepId, true);
        if (step == null)
        {
            throw new ArgumentException("Specified transaction step was not found.");
        }

        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.MaxCount, out settingValue))
        {
            if (!Int32.TryParse(settingValue.ToString(), out maxCount))
            {
                maxCount = 10;
            }
        }

        IEnumerable<StepDurations> stepsDurations = dal.GetStepsDurations(
            step.StepId,
            maxCount,
            dateRange.StartDate,
            dateRange.EndDate);
        
        foreach (StepDurations stepDurations in stepsDurations)
        {
            var serie = new CoreCharting.DataSeries()
            {
                Label = WrapHelper.ForceWrap(stepDurations.Step.Name, 50, 50, "..."),
                TagName = "StepDuration",
                NetObjectId = string.Format(string.Format("{0}:{1}", TransactionStepObject.Prefix, stepDurations.Step.StepId))
            };

            // Put all non-zero value to data serie for chart. Zero value will be nulls in chart.
            foreach (KeyValuePair<DateTime, int?> sample in stepDurations.Durations.Where(x => x.Value.HasValue))
            {
                serie.AddPoint(CoreCharting.DataPoint.CreatePoint(sample.Key, sample.Value.Value));
            }

            series.Add(serie);

            // generate steps details for chart options
            string durationClass = string.Empty;
            switch (stepDurations.Step.LastStatus)
            {
                case SEUMStatuses.Warning:
                    durationClass = "SEUM_WarningValueText";
                    break;
                case SEUMStatuses.Critical:
                    durationClass = "SEUM_CriticalValueText";
                    break;
            }

            stepsDetails.Add(new
            {
                name = stepDurations.Step.Name,
                link = String.Format(CommonLinks.NetObjectViewLinkFormat, NetObjectPrefix.TransactionStep, stepDurations.Step.TransactionStepId),
                transactionName = stepDurations.Step.Transaction.Name,
                currentDuration = durationFormatter.Format(stepDurations.Step.LastDuration.HasValue
                    ? stepDurations.Step.LastDuration.Value.TotalMilliseconds
                    : 0),
                averageDuration = durationFormatter.Format(stepDurations.AverageDuration),
                customClass = durationClass
            });
        }

        var sampledSeries = new List<CoreCharting.DataSeries>(
            series.Select(serie => serie.CreateSampledSeries(
                dateRange, request.SampleSizeInMinutes, SampleMethod.Average)));

        // add average serie and it's details
        int cumulativeAverage;
        CoreCharting.DataSeries averageSerie = ChartHelper.GetAverageSerie(sampledSeries, out cumulativeAverage);
        averageSerie.TagName = "CumulativeAverage";
        averageSerie.Label = CommonTexts.CumulativeAverageLabel;
        sampledSeries.Add(averageSerie);

        stepsDetails.Add(new
        {
            name = CommonTexts.CumulativeAverageLabel,
            currentDuration = durationFormatter.Format(averageSerie.Data.Any(x => !x.IsNullPoint)
                ? averageSerie.Data.Last(x => !x.IsNullPoint).Value
                : 0),
            averageDuration = durationFormatter.Format(cumulativeAverage),
            customClass = string.Empty
        });

        if (!sampledSeries.Any() || !sampledSeries.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new CoreCharting.ChartDataResults(Enumerable.Empty<CoreCharting.DataSeries>());
        }

        var result = new CoreCharting.ChartDataResults(sampledSeries);

        // put steps details containing to chart options so that they can be accessed from legend
        result.ChartOptionsOverride = new
        {
            steps = stepsDetails.ToArray()
        };

        return result;
    }

    [WebMethod]
    public ChartDataResults PageElementsOverview(ChartDataRequest request)
    {
        if (request.NetObjectIds.Length < 1)
            return null;

        int stepId = 0;
        if (!Int32.TryParse(request.NetObjectIds[0], out stepId))
        {
            return null;
        }

        bool byCount = false;
        object byCountObj;
        if (request.AllSettings.TryGetValue("byCount", out byCountObj))
        {
            if (!Boolean.TryParse((string)byCountObj, out byCount))
                byCount = false;
        }

        ChartsDAL dal = new ChartsDAL();
        List<DataSeries> data = new List<DataSeries>();
        data.Add(new DataSeries()
        {
            TagName = "Default"
        });

        Dictionary<BrowserRequestHelper.ResourceType, DataPoint> dataPoints = new Dictionary<BrowserRequestHelper.ResourceType, DataPoint>();
        
        // prepare empty data
        dataPoints[BrowserRequestHelper.ResourceType.Html] = new DataPoint("HTML",
            new Dictionary<string, object>()
                    {
                        {"y", (long)0},
                        {"size", (long)0},
                        {"count", 0},
                        {"color", "#9673ea"}
                    });
        dataPoints[BrowserRequestHelper.ResourceType.Image] = new DataPoint("Images",
            new Dictionary<string, object>()
                    {
                        {"y", (long)0},
                        {"size", (long)0},
                        {"count", 0},
                        {"color", "#49d2f2"}
                    });
        dataPoints[BrowserRequestHelper.ResourceType.JavaScript] = new DataPoint("JavaScript",
            new Dictionary<string, object>()
                    {
                        {"y", (long)0},
                        {"size", (long)0},
                        {"count", 0},
                        {"color", "#f149e7"}
                    });
        dataPoints[BrowserRequestHelper.ResourceType.Css] = new DataPoint("CSS",
            new Dictionary<string, object>()
                    {
                        {"y", (long)0},
                        {"size", (long)0},
                        {"count", 0},
                        {"color", "#c5de00"}
                    });
        dataPoints[BrowserRequestHelper.ResourceType.Flash] = new DataPoint("Flash",
            new Dictionary<string, object>()
                    {
                        {"y", (long)0},
                        {"size", (long)0},
                        {"count", 0},
                        {"color", "#a87000"}
                    });
        dataPoints[BrowserRequestHelper.ResourceType.Unknown] = new DataPoint("Other",
            new Dictionary<string, object>()
                    {
                        {"y", (long)0},
                        {"size", (long)0},
                        {"count", 0},
                        {"color", "#515151"}
                    });

        // get data and sumarize individual types
        string valueKey = byCount ? "TotalCount" : "TotalSize";
        
        DataTable dt = dal.GetStepRequestsSummary(stepId);
        if (dt.Rows.Count == 0)
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataRow row = dt.Rows[i];

            BrowserRequestHelper.ResourceType type = BrowserRequestHelper.GetTypeFromMimeType(
                row["MimeType"] != DBNull.Value ? (string) row["MimeType"] : string.Empty);

            dataPoints[type]["y"] = (long)dataPoints[type]["y"] + Convert.ToInt64(row[valueKey]);
            dataPoints[type]["size"] = (long)dataPoints[type]["size"] + (long)row["TotalSize"];
            dataPoints[type]["count"] = (int)dataPoints[type]["count"] + (int)row["TotalCount"];
        }
        
        // generate legent labels here to make then localizable
        foreach (var value in dataPoints.Values)
        {
            // skip empty groups
            if ((long)value["y"] == 0)
                continue;
            
            value["legendLabel"] = string.Format(
                ((int)value["count"] == 1 ? PageElementsLegendSingularFormat : PageElementsLegendPluralFormat),
                value["name"], value["count"], WebHelper.FormatBytes((long)value["size"]));
            
            data[0].Data.Add(value);
        }
        
        return new ChartDataResults(data);
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults TCPWaterfallData(ChartDataRequest request)
    {
        if (request.NetObjectIds.Length < 1)
            return null;

        int stepId = 0;
        if (!Int32.TryParse(request.NetObjectIds[0], out stepId))
        {
            return null;
        }

        int limit = 0;
        object limitObj;
        if (request.AllSettings.TryGetValue("itemsLimit", out limitObj))
        {
            limit = (int) limitObj;
        }
        
        ChartsDAL dal = new ChartsDAL();
        List<DataSeries> data = new List<DataSeries>();
        
        // series used for request data
        data.Add(new DataSeries()
                    {
                        TagName = "Requests"
                    });
        
        
        data.Add(new DataSeries()
                     {
                         TagName = "Download"
                     });
        data.Add(new DataSeries()
                     {
                         TagName = "TimeToFirstByte"
                     });
        data.Add(new DataSeries()
                    {
                        TagName = "Send"
                    });
        data.Add(new DataSeries()
                     {
                         TagName = "Connection"
                     });
        data.Add(new DataSeries()
                     {
                         TagName = "DNSLookup"
                     });
        data.Add(new DataSeries()
                    {
                        TagName = "InitialDelay"
                    });

        bool moreDataAvailable = false;
        string partialDataCookie = request.PartialDataCookie ?? Guid.NewGuid().ToString();
        int fromIndex = 0;
        
        if (Session[partialDataCookie] != null)
        {
            fromIndex = (int) Session[partialDataCookie];
        }

        int numRequests = dal.GetStepRequestsCount(stepId);
        if (numRequests == 0)
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        
        // More requests link is hidden if there is 0 remaining requests to show. If there is exactly 1 request to show,
        // then we show this request instead of the link/message.
        // If there are two or more requests to show, then we show the link.
        if (limit == 0 || numRequests <= limit+1)
        {
            // get all requests at once
            int i = 0;
            foreach (BrowserRequest r in dal.GetStepRequests(stepId, fromIndex, limit))
            {
                AddRequest(data, r, i++);
            }
        }
        else
        {
            // There are more requests than we want to get. Let's get first limit/2 and last limit/2 
            // and add message about more resources in the middle.
            int i = 0;
            int half = limit/2;
            
            // first half of requests
            foreach (BrowserRequest r in dal.GetStepRequests(stepId, 0, half))
            {
                AddRequest(data, r, i++);
            }
            
            object fullScreenUrl;
            request.AllSettings.TryGetValue("fullScreenLink", out fullScreenUrl);
            
            AddMoreRequestsPlaceholder(data, numRequests - limit, (string)fullScreenUrl, i++);

            // last half of requests
            foreach (BrowserRequest r in dal.GetStepRequests(stepId, numRequests - (limit - half), limit - half))
            {
                AddRequest(data, r, i++);
            }
        }

        ChartDataResults results = new ChartDataResults(data);
        results.MoreDataAvailable = moreDataAvailable;
        results.PartialDataCookie = partialDataCookie;
        
        return results;
    }
    
    private void AddRequest(List<DataSeries> data, BrowserRequest request, int index)
    {
        double begin = (request.RequestBeginTime ?? 0) + request.BlockedDuration.TotalMilliseconds;

        data[(int)DataSerieType.Requests].AddPoint(
            new DataPoint(
                // normal dash is used for soft breaks so we have to replace it with different character looking exactly same
                UrlHelper.ShortenUrlForDisplay(request.URL, 23, 15, true).Replace('-', '‒'), 
                new Dictionary<string, object>
                        {
                            {"url", HttpUtility.HtmlEncode(request.URL)}, // some URL from request can contain illegal HTML characters such as < or >
                            {"statusCode", request.StatusCode},
                            {"size", request.Size},
                            {"mimeType", request.MimeType},
                            {"beginTime", string.Format("+{0:0.000}s", begin/1000)},
                            {"index", index}
                        }));

        data[(int)DataSerieType.InitialDelay].AddPoint(new DataPoint(begin));
        data[(int)DataSerieType.DNSLookup].AddPoint(new DataPoint((int)request.DNSResolutionDuration.TotalMilliseconds));
        data[(int)DataSerieType.Connection].AddPoint(new DataPoint((int)request.ConnectionDuration.TotalMilliseconds));
        data[(int)DataSerieType.Send].AddPoint(new DataPoint((int)request.SendDuration.TotalMilliseconds));
        data[(int)DataSerieType.TimeToFirstByte].AddPoint(new DataPoint((int)request.TimeToFirstByteDuration.TotalMilliseconds));
        data[(int)DataSerieType.ContentDownload].AddPoint(new DataPoint((int)request.DownloadDuration.TotalMilliseconds));
    }

    private void AddMoreRequestsPlaceholder(List<DataSeries> data, int numRequests, string fullScreenUrl, int index)
    {
        data[(int) DataSerieType.Requests].AddPoint(
                    new DataPoint(
                        // URL for full screen chart
                        string.Format("<a href=\"{0}\" target=\"_blank\">... {1} ...</a>",
                                      fullScreenUrl ?? string.Empty,
                                      numRequests == 1
                                      ? string.Format(MoreRequestsNotificationTextFormat, numRequests)
                                      : string.Format(MoreRequestsNotificationTextFormatPlural, numRequests)),
                        new Dictionary<string, object>
                            {
                                {"isMoreRequestsPlaceholder", true},
                                {"index", index}
                            }));

        data[(int)DataSerieType.InitialDelay].AddPoint(new DataPoint());
        data[(int)DataSerieType.DNSLookup].AddPoint(new DataPoint());
        data[(int)DataSerieType.Connection].AddPoint(new DataPoint());
        data[(int)DataSerieType.Send].AddPoint(new DataPoint());
        data[(int)DataSerieType.TimeToFirstByte].AddPoint(new DataPoint());
        data[(int)DataSerieType.ContentDownload].AddPoint(new DataPoint());
    }
}

