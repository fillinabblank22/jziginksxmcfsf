﻿<%@ WebService Language="C#" Class="TransactionChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Common.Models;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;
using SolarWinds.SEUM.Web.NetObjects;
using ChartDataResults = SolarWinds.Orion.Web.Charting.v2.ChartDataResults;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TransactionChartData : ChartDataWebService
{
    // Parameter @NodeID will contain list of NetObjectIds from ChartDataRequest. Unfortunately its name is hardcoded in Orion Core even it doesn't make sense for modules. 
    private readonly string _transactionNameLookupSwql = string.Format(
        "SELECT TransactionId, DisplayName FROM {0} WHERE TransactionId in @NodeID", SwisEntities.Transactions);
    
    [WebMethod]
    public ChartDataResults MMADuration(ChartDataRequest request)
    {
        // make sure we ignore -1 or -2 values since these have special meaning.
        const string sql = @"SELECT DateTimeUtc,  
                                    NULLIF(NULLIF(AvgDuration, -1), -2) AvgDuration, 
                                    NULLIF(NULLIF(MinDuration, -1), -2) MinDuration, 
                                    NULLIF(NULLIF(MaxDuration, -1), -2) MaxDuration
                             FROM SEUM_ResponseTime 
                             WHERE TransactionId = @NetObjectId
                             AND DateTimeUtc >= @StartTime AND DateTimeUtc <= @EndTime
                             ORDER BY [DateTimeUtc]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, _transactionNameLookupSwql, DateTimeKind.Utc, "AvgDuration", "MinMaxDuration");

        if (!result.DataSeries.Any() || result.DataSeries.All(s => s.Data.All(dp => dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }

        return result;
    }

    [WebMethod]
    public ChartDataResults Availability(ChartDataRequest request)
    {
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        int transactionId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out transactionId);
        }
        if (transactionId == 0)
        {
            throw new ArgumentException("No Transaction ID is defined in request.");
        }

        ChartsDAL dal = new ChartsDAL();
        DataTable dt = dal.GetTransactionAvailability(transactionId, dateRange.StartDate, dateRange.EndDate);

        var sampledSeries = ChartHelper.GetBasicAvailabilitySeries(dt, request, true);
        
        return new ChartDataResults(sampledSeries);
    }

    [WebMethod]
    public ChartDataResults StepsDurations(ChartDataRequest request)
    {
        List<DataSeries> series = new List<DataSeries>();
        List<object> stepsDetails = new List<object>();
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        DurationFormatter durationFormatter = new DurationFormatter();
        int percentile = Convert.ToInt32(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("Web-ChartPercentile").SettingValue);
        ChartsDAL dal = new ChartsDAL();

        int transactionId = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out transactionId);
        }
        if (transactionId == 0)
        {
            throw new ArgumentException("No Transaction ID is defined in request.");
        }
        
        IList<StepDurations> steps = dal.GetStepByStepDurations(transactionId, dateRange.StartDate, dateRange.EndDate);

        // process steps in reverse because we need first step at the bottom of chart instead the top
        foreach (StepDurations stepDurations in steps.Reverse())
        {
            DataSeries serie = new DataSeries()
                                   {
                                       Label = WrapHelper.ForceWrap(stepDurations.Step.Name, 50, 50, "..."), 
                                       TagName = "StepDuration", 
                                       NetObjectId = string.Format(string.Format("{0}:{1}", TransactionStepObject.Prefix, stepDurations.Step.StepId))
                                   };

            // Put all non-zero value to data serie for chart. Zero value will be nulls in chart.
            foreach (KeyValuePair<DateTime, int?> sample in stepDurations.Durations.Where(x => x.Value.HasValue))
            {
                serie.AddPoint(DataPoint.CreatePoint(sample.Key, sample.Value.Value));
            }
            series.Add(serie);
            
            // generate steps details for chart options that will be used in legend
            stepsDetails.Add(new
            {
                name = stepDurations.Step.Name,
                link = String.Format(CommonLinks.NetObjectViewLinkFormat, NetObjectPrefix.TransactionStep, stepDurations.Step.TransactionStepId),
                optimal = durationFormatter.Format(stepDurations.Step.OptimalThreshold.TotalMilliseconds),
                percentile = durationFormatter.Format(stepDurations.GetPercentile(percentile, TimeSpan.FromMinutes(request.SampleSizeInMinutes)))
            });
        }

        if (!series.Any() || !series.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        
        ChartDataResults result = new ChartDataResults(
            series.Select(serie => serie.CreateSampledSeries(dateRange, request.SampleSizeInMinutes, SampleMethod.Average)));

        // put steps details containing optimal threshold and percentile to chart options so that they can be accessed from legend
        result.ChartOptionsOverride = new
        {
            steps = stepsDetails.ToArray()
        };
        
        return result;
    }

    [WebMethod]
    public ChartDataResults LocationsDurations(ChartDataRequest request)
    {
        List<DataSeries> series = new List<DataSeries>();
        List<object> transactionsDetails = new List<object>();
        int transactionId = 0;
        int maxCount = 10;
        object settingValue;
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        DurationFormatter durationFormatter = new DurationFormatter();
        ChartsDAL dal = new ChartsDAL();

        // set view info to httpcontext so the view limitations are properly applied
        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.ViewId, out settingValue))
        {
            Context.Items[typeof(ViewInfo).Name] = ViewManager.GetViewById((int)settingValue);
        }

        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out transactionId);
        }
        if (transactionId == 0)
        {
            throw new ArgumentException("No Transaction ID is defined in request.");
        }
        Transaction transaction = new ModelsDAL().GetTransactionModel(transactionId, false, false);
        if (transaction == null)
        {
            throw new ArgumentException("Specified transaction was not found.");
        }

        if (request.AllSettings.TryGetValue(ResourcePropertiesKeys.MaxCount, out settingValue))
        {
            if (!Int32.TryParse(settingValue.ToString(), out maxCount))
            {
                maxCount = 10;
            }
        }

        IEnumerable<TransactionDurations> transactions = dal.GetTransactionsDurations(
            transaction.RecordingId,
            maxCount,
            dateRange.StartDate,
            dateRange.EndDate);
        
        foreach (TransactionDurations transactionDurations in transactions)
        {
            DataSeries serie = new DataSeries()
            {
                Label = WrapHelper.ForceWrap(transactionDurations.Transaction.Name, 50, 50, "..."),
                TagName = "TransactionDuration",
                NetObjectId = string.Format(string.Format("{0}:{1}", TransactionObject.Prefix, transactionDurations.Transaction.TransactionId))
            };

            // Put all non-zero value to data serie for chart. Zero value will be nulls in chart.
            foreach (KeyValuePair<DateTime, int?> sample in transactionDurations.Durations.Where(x => x.Value.HasValue))
            {
                serie.AddPoint(DataPoint.CreatePoint(sample.Key, sample.Value.Value));
            }

            series.Add(serie);

            // generate steps details for chart options
            string durationClass = string.Empty;
            switch (transactionDurations.Transaction.LastStatus)
            {
                case SEUMStatuses.Warning:
                    durationClass = "SEUM_WarningValueText";
                    break;
                case SEUMStatuses.Critical:
                    durationClass = "SEUM_CriticalValueText";
                    break;
            }
            
            transactionsDetails.Add(new
            {
                name = transactionDurations.Transaction.Name,
                link = String.Format(CommonLinks.NetObjectViewLinkFormat, NetObjectPrefix.Transaction, transactionDurations.Transaction.TransactionId),
                currentDuration = durationFormatter.Format(transactionDurations.Transaction.LastDuration.HasValue 
                    ? transactionDurations.Transaction.LastDuration.Value.TotalMilliseconds
                    : 0),
                averageDuration = durationFormatter.Format(transactionDurations.AverageDuration),
                customClass = durationClass
            });
        }

        List<DataSeries> sampledSeries = new List<DataSeries>(
            series.Select(serie => serie.CreateSampledSeries(
                dateRange, request.SampleSizeInMinutes, SampleMethod.Average)));

        // add average serie and it's details
        int cumulativeAverage;
        DataSeries averageSerie = ChartHelper.GetAverageSerie(sampledSeries, out cumulativeAverage);
        averageSerie.TagName = "CumulativeAverage";
        averageSerie.Label = CommonTexts.CumulativeAverageLabel;
        sampledSeries.Add(averageSerie);
        
        transactionsDetails.Add(new
        {
            name = CommonTexts.CumulativeAverageLabel,
            currentDuration = durationFormatter.Format(averageSerie.Data.Any(x => !x.IsNullPoint)
                ? averageSerie.Data.Last(x => !x.IsNullPoint).Value
                : 0),
            averageDuration = durationFormatter.Format(cumulativeAverage),
            customClass = string.Empty
        });

        if (!sampledSeries.Any() || !sampledSeries.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        
        ChartDataResults result = new ChartDataResults(sampledSeries);

        // put transactions details containing to chart options so that they can be accessed from legend
        result.ChartOptionsOverride = new
        {
            transactions = transactionsDetails.ToArray()
        };
        
        return result;
    }
}