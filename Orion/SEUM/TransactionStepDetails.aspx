﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/SEUM/SEUMView.master" CodeFile="TransactionStepDetails.aspx.cs" Inherits="Orion_SEUM_TransactionStepDetails" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="ViewTitle" Src="~/Orion/SEUM/Controls/ViewTitle.ascx" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.NetObjects" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="SEUMViewPageTitle">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div class="SEUM_BreadcrumbsWrapper">
        <orion:PluggableDropDownMapPath ID="TransactionSiteMapPath" runat="server" SiteMapKey="TransactionObjectDetails" LoadSiblingsOnDemand="true"/>
    </div>
    <% } %>
	<h1><%= Title %></h1>
    <h2>
        <orion:EntityStatusIcon ID="transactionStatusIcon" 
                                runat="server" 
                                IconSize="Small" 
                                Entity="<%# TransactionStepObject.SwisEntity %>" 
                                Status="<%# TransactionStepObject.Status %>" 
                                WrapInBox="false" />
        <a href="/Orion/View.aspx?NetObject=<%# TransactionStepObject.NetObjectID %>">
            <%# TransactionStepObject.Name %>
        </a>
        <%= locationText %>
        <orion:EntityStatusIcon ID="stepStatusIcon" 
                                runat="server" 
                                IconSize="Small" 
                                Entity="<%# TransactionObject.SwisEntity %>" 
                                Status="<%# TransactionObject.Status %>" 
                                WrapInBox="false" />
        <a href="/Orion/View.aspx?NetObject=<%# TransactionObject.NetObjectID %>">
            <%# TransactionObject.RecordingName %>
        </a>
        <%# fromText %>
        <a href="/Orion/View.aspx?NetObject=<%# new TransactionLocationObject(TransactionObject.Agent).NetObjectID %>">
            <%# TransactionObject.AgentName %>
        </a>
    </h2>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SEUMMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="ResourceContainer" />
    </orion:ResourceHostControl>
</asp:Content>



