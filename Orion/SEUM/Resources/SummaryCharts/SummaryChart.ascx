﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SummaryChart.ascx.cs" Inherits="Orion_SEUM_Resources_SummaryCharts_SummaryChart" %>
<orion:Include runat="server" Module="SEUM" File="Charts/Charts.SEUM.Common.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper> 