﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_SummaryCharts_SummaryChart : SEUMBaseStandardChartResource, IResourceIsInternal
{
    protected void Page_Init(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.DataFormatters.js", OrionInclude.Section.Middle);

        HandleInit(WrapperContents);

        // see FB169719, in Galaga charts have added Export button which is useless for this chart, so we will disable it this way
        Wrapper.ShowManageButton = false;
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        Dictionary<string, object> details = base.GenerateDisplayDetails();

        details[ResourcePropertiesKeys.RememberCollapseState] = Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true";
        details[ResourcePropertiesKeys.Filter] = CheckNetObjectAndModifyFilter(this.Resource.Properties[ResourcePropertiesKeys.Filter] ?? string.Empty);
        details[ResourcePropertiesKeys.HideUnmanaged] = Resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "true";
        details["SEUM-Summary-ResourceId"] = Resource.ID;
        details[ResourcePropertiesKeys.ViewId] = Resource.View.ViewID;

        return details;
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return Enumerable.Empty<string>();
    }

    protected override string DefaultTitle
    {
        get { return "Summary Chart"; }
    }

    protected override string NetObjectPrefix
    {
        get
        {
            // this is virtual net object prefix for summary charts
            return "SEUM-Summary";
        }
    }

    /// <summary>
    /// This method will check if the resource is shown for transaction location NetObject.
    /// If it is, it will modify user defined filter to filter based on location NetObject and SwisEntity property.
    /// </summary>
    /// <returns>Modified SWQL filter statement</returns>
    protected virtual string CheckNetObjectAndModifyFilter(string filter)
    {
        return SEUMBaseResource.CheckResourceNetObjectAndModifyFilter(this, filter);
    }

    public bool IsInternal
    {
        get { return true; }
    }
}