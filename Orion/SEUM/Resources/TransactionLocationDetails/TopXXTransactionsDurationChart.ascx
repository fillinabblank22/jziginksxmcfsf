﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXTransactionsDurationChart.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionLocationDetails_TopXXTransactionsDurationChart" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Charting" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Helpers" %>
<%@ Register TagPrefix="SEUM" TagName="FormattedValue" Src="~/Orion/SEUM/Controls/FormattedValue.ascx" %>
<%@ Register TagPrefix="SEUM" Namespace="SolarWinds.SEUM.Web.Charting.Custom" Assembly="SolarWinds.SEUM.Web"  %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="contentPanel" UpdateMode="Conditional" >
            <ContentTemplate>
                <SEUM:Chart runat="server" id="chart" />

                <table class="DataGrid">
                    <tr>
                        <td class="ReportHeader SEUM_ReportHeader" colspan="2"><%= transactionNameLabel%></td>
                        <td class="ReportHeader SEUM_ReportHeader"><%= currentDurationLabel %></td>
                        <td class="ReportHeader SEUM_ReportHeader"><%= avgDurationLabel %></td>
                    </tr>

                <asp:Repeater runat="server" ID="transactionsRepeater">
                    <ItemTemplate>
                        <tr>
                            <td class="Property SEUM_Property SEUM_LegendColorIconCell"><asp:PlaceHolder runat="server" ID="legendColorIconPlaceholder"></asp:PlaceHolder></td>
                            <td class="Property SEUM_Property SEUM_LegendItemName">
                                <%# NameHelper.GenerateTransactionFullName(
                                    ((TransactionDurations)Container.DataItem).Transaction.Name,
                                    ((TransactionDurations)Container.DataItem).Transaction.DetailsUrl) %>
                            </td>
                            <td class="Property SEUM_Property">
                                <SEUM:FormattedValue ID="currentDuration" runat="server" />
                            </td>
                            <td class="Property SEUM_Property">
                            <%# durationFormatter.Format(((TransactionDurations)Container.DataItem).AverageDuration)%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                        <tr>
                            <td class="Property SEUM_Property SEUM_LegendColorIconCell"><asp:PlaceHolder runat="server" ID="averageColorPlaceholder"></asp:PlaceHolder></td>
                            <td class="Property SEUM_Property SEUM_LegendItemName">
                                <%= averageLabel %>
                            </td>
                            <td class="Property SEUM_Property">
                                <SEUM:FormattedValue ID="averageDuration" runat="server" />
                            </td>
                            <td class="Property SEUM_Property">
                                <asp:Literal runat="server" ID="averageAverageDuration" />
                            </td>
                        </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel CssClass="SEUM_NoData" runat="server" ID="noDataPanel" Visible="false">
            <asp:Literal runat="server" ID="messageLiteral"></asp:Literal>
        </asp:Panel>
    </Content>
</orion:resourceWrapper>

<script type="text/javascript">
    function SetupCustomMenu<%= Resource.ID %>() {
        var contentPanel = $('#<%= contentPanel.ClientID %>');
        var resourceWrapper = contentPanel.parent().parent();

        var menuDiv = resourceWrapper.find('#customMenu');
        if (!menuDiv)
            return;

        menuDiv.html("<select onchange=\"<%= DropDownPostBack %>\" name=\"<%= HeaderMenuSelectName %>\">" +
        '<option value="">View Options</option>' +
        <% foreach (TimePeriod item in TimePeriodShortcuts){%>
           '<option value="<%= item.Value %>"><%=item.Label %></option>' +
        <%}%>
        '</select>');
        menuDiv.show();
    }
    SetupCustomMenu<%= Resource.ID %>();

    function SEUMUpdateResourceSubtitle<%= Resource.ID %>(value) {
        var parent = $('#<%= contentPanel.ClientID %>').parent();
        while (parent && (!parent.hasClass('ResourceWrapper') || parent.find('.HeaderBar').length == 0)) {
            parent = parent.parent();
        }

        if (parent)
            parent.find('h2').text(value);
    }
</script>