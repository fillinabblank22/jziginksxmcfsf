﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionLocationDetails.ascx.cs" Inherits="Orion_SEUM_Resources_Details_TransactionLocationDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="ResourceTable" Src="~/Orion/SEUM/Controls/DefaultResourceTable.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="LoadIndicator" Src="~/Orion/SEUM/Controls/LoadIndicator.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
            function showBusyImage() {
                $('#busyImage<%= Resource.ID %>').show();
            }
        </script>
        <table class="NeedsZebraStripes SEUM_DetailsTable">
            <tr>
                <td class="PropertyHeader"><%= managementLabel %></td>
                <td>
                    <asp:UpdatePanel runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:ImageButton ID="editButton" runat="server" ImageUrl="~/Orion/images/edit_16x16.gif" OnClick="EditClick" /> <asp:LinkButton runat="server" ID="editLink" Text="Edit" OnClick="EditClick" />
                            &nbsp;
                            <asp:ImageButton ID="updateButton" runat="server" ImageUrl="~/Orion/SEUM/Images/Icon.ForcePlayerUpdate.png" OnClick="UpdateClick" /> <asp:LinkButton runat="server" ID="updateLink" Text="Force player update" OnClick="UpdateClick" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= statusLabel %></td>
                <td>
                    <orion:EntityStatusIcon ID="entityStatusIcon" runat="server" IconSize="Small" Entity="<%# TransactionLocation.SwisEntity %>" Status="<%# TransactionLocation.Model.ConnectionStatus %>" WrapInBox="false" />
                    <%= TransactionLocation.StatusDescription %>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= locationName %></td>
                <td> <%# TransactionLocation.Name %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= communicationTypeLabel%></td>
                <td> <%# UIHelper.Escape(TransactionLocation.Model.IsActiveAgent ? activeDescription : passiveDescription) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= dnsLabel%></td>
                <td> <%# UIHelper.Escape(TransactionLocation.Model.DNSName) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= hostnameLabel%></td>
                <td> <%# UIHelper.Escape(TransactionLocation.Model.Hostname) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= ipLabel%></td>
                <td> <%# UIHelper.Escape(TransactionLocation.Model.IP.ToString()) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= assignedPollerLabel%></td>
                <td> <%# UIHelper.Escape(TransactionLocation.Model.PollingEngineName) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= versionLabel %></td>
                <td> <%# UIHelper.Escape(TransactionLocation.Model.AgentVersion.ToString()) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= playerLoadLabel %></td>
                <td><SEUM:LoadIndicator ID="playerLoadIndicator" runat="server" /></td>
            </tr>
        </table>
        <span class="SEUM_TableSpacer"></span>

        <SEUM:ResourceTable runat="server" ID="table" />
    </Content>
</orion:resourceWrapper>
