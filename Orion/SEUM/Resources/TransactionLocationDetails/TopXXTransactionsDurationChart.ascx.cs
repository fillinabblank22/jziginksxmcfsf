﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web.Charting.Custom;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web;
using System.Drawing;
using SolarWinds.SEUM.Web.Charting;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionLocationDetails_TopXXTransactionsDurationChart : SEUMTransactionLocationBaseResource
{
    private const string defaultTitle = "Legacy Top XX Transactions by Duration";

    protected const string transactionNameLabel = "Transaction name";
    protected const string currentDurationLabel = "Current duration";
    protected const string avgDurationLabel = "Avg over time";
    protected const string yAxisLabel = "Duration in seconds";

    protected const string chartSubtitle = "Transaction Duration";

    protected const string averageLabel = "CUMULATIVE AVERAGE";

    private const string noDataError = "There is no data for selected time period";
    private const string noStepsText = "There are no steps to display";

    private const string SECURE_IMAGE_PIPE_LOCATION = "/Orion/Controls/ChartImagePipe.aspx";

    private const string PostBackArgument = "SEUMUpdateChartInterval";

    private TimePeriod timePeriod;
    protected SampleInterval sampleInterval;
    private DateTime dateFrom;
    private DateTime dateTo;
    private int width;
    private int height;
    private Color averageColor = Color.Black;

    protected DurationFormatter durationFormatter = new DurationFormatter();

    protected override string DefaultTitle
    {
        get
        {
            return defaultTitle;
        }
    }

    public override string DisplayTitle
    {
        get
        {
            return Title.Replace("XX", MaxCount.ToString());
        }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(base.SubTitle))
                return string.Format("{0} - {1}", base.SubTitle, timePeriod.Label);
            else
                return timePeriod.Label;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHTopTransactionsDuration"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SEUM/Controls/EditTransactionsDurationsChartResource.ascx"; }
    }

    protected int MaxCount
    {
        get
        {
            return GetIntProperty(ResourcePropertiesKeys.MaxCount, 5);
        }
    }

    protected string DropDownPostBack
    {
        get;
        set;
    }

    protected string HeaderMenuSelectName
    {
        get
        {
            return string.Format("SEUM_transactions_durations_chart_header_menu_{0}", Resource.ID);
        }
    }

    protected IEnumerable<TimePeriod> TimePeriodShortcuts
    {
        get
        {
            return new[]
                       {
                           TimePeriod.Last2Hours,
                           TimePeriod.Today, 
                           TimePeriod.Last24Hours, 
                           TimePeriod.Last7Days, 
                           TimePeriod.Last30Days, 
                           TimePeriod.Last3Months
                       };
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        DropDownPostBack = Page.ClientScript.GetPostBackEventReference(contentPanel, PostBackArgument);

        LoadParameters();

        ChartsDAL dal = new ChartsDAL();

        IEnumerable<TransactionDurations> transactions = dal.GetTransactionsDurationsForAgent(
            TransactionLocation.Model.AgentId,
            MaxCount,
            dateFrom,
            dateTo);

        transactionsRepeater.ItemDataBound += new RepeaterItemEventHandler(transactionsRepeater_ItemDataBound);

        transactionsRepeater.DataSource = transactions;
        transactionsRepeater.DataBind();

        SetAverageValue(transactions);

        DataSource = GetDataForChart(transactions);

        if (DataSource != null)
        {
            InitChart();
        }
        else
        {
            contentPanel.Visible = false;
            noDataPanel.Visible = true;
            messageLiteral.Text = noStepsText;
        }

        ScriptManager.RegisterStartupScript(this.Page,
            this.GetType(),
            string.Format("SEUM_temp{0}", Resource.ID),
            string.Format("SEUMUpdateResourceSubtitle{0}('{1}');", Resource.ID, SubTitle),
            true);
    }

    private void SetAverageValue(IEnumerable<TransactionDurations> transactions)
    {
        averageColorPlaceholder.Controls.Add(new Literal()
        {
            Text = string.Format("<span class=\"SEUM_LegendColorIcon\" style=\"background-color: {0}\">&nbsp;</span>", ColorTranslator.ToHtml(averageColor))
        });

        IEnumerable<TransactionDurations> trans =
            transactions.Where(x => x.Transaction.LastDuration.HasValue);
        averageDuration.Value = (trans.Any()) ? trans.Average(x => x.Transaction.LastDuration.Value.TotalMilliseconds) : 0;
        averageDuration.ValueFormatter = durationFormatter;

        IEnumerable<TransactionDurations> trans2 =
            transactions.Where(x => x.AverageDuration > 0);
        averageAverageDuration.Text = durationFormatter.Format((trans2.Any()) ? trans2.Average(x => x.AverageDuration) : 0);
    }

    void transactionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        PlaceHolder iconPlaceholder = item.FindControl("legendColorIconPlaceholder") as PlaceHolder;
        if (iconPlaceholder != null)
        {
            Color color = chart.ChartColors[item.ItemIndex % chart.ChartColors.Length];

            iconPlaceholder.Controls.Add(new Literal()
            {
                Text = string.Format("<span class=\"SEUM_LegendColorIcon\" style=\"background-color: {0}\">&nbsp;</span>", ColorTranslator.ToHtml(color))
            });
        }

        Orion_SEUM_Controls_FormattedValue currentDuration = item.FindControl("currentDuration") as Orion_SEUM_Controls_FormattedValue;
        if (currentDuration != null)
        {
            TransactionDurations transaction = item.DataItem as TransactionDurations;
            if (transaction != null)
            {
                currentDuration.Value = transaction.Transaction.LastDuration.GetValueOrDefault(TimeSpan.Zero).TotalMilliseconds;
                currentDuration.Status = transaction.Transaction.LastStatus;
                currentDuration.ValueFormatter = durationFormatter;
            }
        }
    }

    public DataTable DataSource;

    private void LoadParameters()
    {
        if (Page.IsPostBack)
        {
            if (Request["__EVENTARGUMENT"] == PostBackArgument)
            {
                int periodShortcut = 0;
                if (Int32.TryParse(Request[HeaderMenuSelectName], out periodShortcut))
                {
                    timePeriod = TimePeriod.FromValue(periodShortcut);
                    sampleInterval = timePeriod.DefaultSampleInterval;
                }
            }
        }

        if (timePeriod == null)
            timePeriod = TimePeriod.FromValue(GetIntProperty(ResourcePropertiesKeys.TimePeriod, TimePeriod.Today.Value));

        if (sampleInterval == null)
            sampleInterval = SampleInterval.FromValue(GetIntProperty(ResourcePropertiesKeys.SampleInterval, TimePeriod.Today.DefaultSampleInterval.Value));

        if (timePeriod == TimePeriod.Custom)
        {
            DateTime.TryParse(GetStringValue(ResourcePropertiesKeys.TimePeriodCustomFrom, DateTime.UtcNow.Date.ToString()), CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateFrom);
            DateTime.TryParse(GetStringValue(ResourcePropertiesKeys.TimePeriodCustomTo, DateTime.UtcNow.AddDays(1).Date.AddSeconds(-1).ToString()), CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateTo);
        }
        else
        {
            dateFrom = timePeriod.DateTimeFrom.ToUniversalTime();
            dateTo = timePeriod.DateTimeTo.ToUniversalTime();
        }

        width = GetIntProperty(ResourcePropertiesKeys.Width, 0);
        height = GetIntProperty(ResourcePropertiesKeys.Height, 0);
    }

    private DataTable GetDataForChart(IEnumerable<TransactionDurations> transactions)
    {
        bool hasData = false;

        // generate data table containing transactions and their durations for chart
        DataTable dt = new DataTable();
        dt.Columns.Add("DateTime", typeof(DateTime));

        DataTable tmpDt = new DataTable();
        tmpDt.Columns.Add("Sum", typeof(int));
        tmpDt.Columns.Add("RecordCount", typeof(int));

        int numItems = transactions.Count();
        for (int i = 0; i < numItems; i++)
        {
            dt.Columns.Add("Location" + i, typeof(int));
        }

        dt.Columns.Add(Chart.AverageColumnName, typeof(int));

        if (numItems > 0)
        {
            for (int i = 0; i < numItems; i++)
            {
                int j = 0;
                foreach (KeyValuePair<DateTime, int?> sample in transactions.ElementAt(i).GetSamples(sampleInterval.Interval))
                {
                    if (i == 0)
                    {
                        dt.Rows.Add();
                        dt.Rows[j]["DateTime"] = sample.Key;
                        dt.Rows[j][Chart.AverageColumnName] = 0;

                        tmpDt.Rows.Add();
                        tmpDt.Rows[j]["Sum"] = 0;
                        tmpDt.Rows[j]["RecordCount"] = 0;
                    }

                    if (sample.Value.HasValue)
                    {
                        dt.Rows[j]["Location" + i] = sample.Value.Value;
                        hasData = true;
                    }
                    else
                    {
                        dt.Rows[j]["Location" + i] = DBNull.Value;
                    }

                    tmpDt.Rows[j]["Sum"] = (int)tmpDt.Rows[j]["Sum"] + (sample.Value ?? 0);
                    if (sample.Value != 0)
                        tmpDt.Rows[j]["RecordCount"] = (int)tmpDt.Rows[j]["RecordCount"] + 1;

                    j++;
                }
            }

            // get average
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                DataRow tmpRow = tmpDt.Rows[i];

                if ((int)tmpRow["RecordCount"] > 0)
                    row[Chart.AverageColumnName] = (int)tmpRow["Sum"] / (int)tmpRow["RecordCount"];

                if ((int)row[Chart.AverageColumnName] == 0)
                    row[Chart.AverageColumnName] = DBNull.Value;
            }

        }

        if (!hasData)
            dt.Rows.Clear();

        return dt;
    }

    private void InitChart()
    {
        chart.ImagePipePageName = SECURE_IMAGE_PIPE_LOCATION;
        chart.ShowLegend = false;
        chart.ChartType = Chart.ChartTypes.LineChart;
        chart.Width = (width > 0) ? width : Resource.Width;

        if (height > 0)
            chart.Height = height;
        else
            chart.Height = Math.Max(chart.Width / 2, 200);

        chart.ChartTitle = TransactionLocation.Name;
        chart.ChartSubTitle = chartSubtitle;
        if (timePeriod == TimePeriod.Custom)
            chart.ChartTimeSpan = string.Format("{0} - {1}", dateFrom.ToLocalTime(), dateTo.ToLocalTime());
        else
            chart.ChartTimeSpan = timePeriod.Label.ToUpper();

        chart.AverageColor = averageColor;

        chart.YAxisDescription = yAxisLabel;

        chart.NoDataError = noDataError;

        chart.DataSource = DataSource;
    }
}