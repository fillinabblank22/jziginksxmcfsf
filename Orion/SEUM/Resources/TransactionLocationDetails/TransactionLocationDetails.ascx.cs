﻿using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.BusinessLayer.Servicing;
using SolarWinds.SEUM.Common;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Details_TransactionLocationDetails : SEUMTransactionLocationBaseResource
{
    private static readonly Log log = new Log();

    internal const string defaultTitle = "Location Details";
    internal const string locationName = "Location&nbsp;Name";
    internal const string statusLabel = "Status";
    internal const string managementLabel = "Management";
    internal const string transactionsCol = "TRANSACTIONS";
    internal const string durationCol = "DURATION";
    internal const string statusCol = "STATUS";
    internal const string miliseconds = "ms";
    internal const string communicationTypeLabel = "Communication type";
    internal const string activeDescription = "Player initiated communication (active)";
    internal const string passiveDescription = "Server initiated communication (passive)";
    internal const string dnsLabel = "DNS name";
    internal const string hostnameLabel = "Hostname";
    internal const string ipLabel = "IP address";
    internal const string assignedPollerLabel = "Assigned to poller";
    internal const string versionLabel = "Version";
    internal const string playerLoadLabel = "Current player load";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();

        updateButton.Visible = false;
        updateLink.Visible = false;

        // handle permissions
        if (!SEUMProfile.AllowAdmin)
        {
            editButton.Visible = false;
            editLink.Visible = false;
        }
        else if (TransactionLocation.Model.ConnectionStatus == (int)AgentConnectionStatus.UpdateFailed && TransactionLocation.Model.AgentVersion >= AgentConnectionStatusHelper.MinVersionForUpdate)
        {
            updateButton.Visible = true;
            updateLink.Visible = true;
        }

        bool hideUnmanaged;
        if (!Boolean.TryParse(Resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "True", out hideUnmanaged))
        {
            hideUnmanaged = true;
        }

        ResourcesDAL dal = new ResourcesDAL();
        DataTable data = dal.GetTransactionsByLocation(TransactionLocation.Id, !hideUnmanaged);        

        List<ResourceTableColumn> tableColumns = new List<ResourceTableColumn>();
        tableColumns.Add(new ResourceTableColumn()
        {
            DataItemKey = "Transaction",
            Name = transactionsCol
        });
        tableColumns.Add(new ResourceTableColumn()
        {
            DataItemKey = "Duration",
            Name = durationCol,
            ValueFormatter = new DurationFormatter(),
            CssClass = CssClasses.ResourceDurationColumn,
            StatusDataItemKey = "StatusId",
            IsHighlighted = true
        });
        tableColumns.Add(new ResourceTableColumn()
        {
            DataItemKey = "Status",
            Name = statusCol
        });

        table.Columns = tableColumns;
        table.SetData(data);

        if (data.Rows.Count == 0)
            table.Visible = false;

        playerLoadIndicator.LoadPercentage = TransactionLocation.Model.LoadPercentage;
    }

    protected void EditClick(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("/Orion/SEUM/Admin/EditPlayer.aspx?id={0}&{1}={2}", 
            TransactionLocation.Id, 
            CommonParameterNames.ReturnUrl,
            UrlHelper.ToSafeUrlParameter(Request.RawUrl)));
    }

    protected void UpdateClick(object sender, EventArgs e)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.ScheduleDeployment(TransactionLocation.Model.AgentId, InstallerDeploymentKind.Update);
        }

        // redirect to self to update whole page
        Response.Redirect(Request.Url.ToString());
    }

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionSEUMPHResourcesTransactionLocationDetails";
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/EditLocationDetailsResource.ascx";
        }
    }

    #endregion

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Synchronous;
}
