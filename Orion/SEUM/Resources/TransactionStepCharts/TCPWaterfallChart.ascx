﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TCPWaterfallChart.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionStepCharts_TCPWaterfallChart" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <!-- This element is used to access ResourceWrapper element for this resource so that 
        we can add extra CSS class to it to fix chart tooltips clipping. (FB109023) -->
        <span runat="server" ID="SEUM_TCPWaterfall_Anchor"></span>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
        <asp:HyperLink runat="server" ID="FullscreenLink" CssClass="coloredLink SEUM_FullscreenChartLink"></asp:HyperLink>

        <script type="text/javascript">
            // Add class to ResourceWrapper to fix tooltips clipping issue for charts. (FB109023)
            $('#<%= SEUM_TCPWaterfall_Anchor.ClientID %>').parents('.ResourceWrapper').addClass('SEUM_NoclipResource');
        </script>
    </Content>
</orion:resourceWrapper> 