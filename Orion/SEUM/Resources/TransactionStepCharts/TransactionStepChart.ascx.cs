﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionStepCharts_TransactionStepChart : SEUMBaseStandardChartResource, IResourceIsInternal
{
    private TransactionStepObject _transactionStep;
    private int _maxCount = 10;

    protected void Page_Init(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "SEUM.css", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.DataFormatters.js", OrionInclude.Section.Middle);

        _transactionStep = GetInterfaceInstance<ITransactionStepProvider>().TransactionStepObject;
       _maxCount = GetIntProperty(ResourcePropertiesKeys.MaxCount, 10);

        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        Dictionary<string, object> details = base.GenerateDisplayDetails();
        details[ResourcePropertiesKeys.OptimalThresholdMs] = (int)_transactionStep.OptimalThreshold.TotalMilliseconds;
        details[ResourcePropertiesKeys.WarningThresholdMs] = (int)_transactionStep.WarningThreshold.TotalMilliseconds;
        details[ResourcePropertiesKeys.CriticalThresholdMs] = (int)_transactionStep.CriticalThreshold.TotalMilliseconds;
        details[ResourcePropertiesKeys.DisplayOptimalThreshold] =
            Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.DisplayOptimalThreshold] ?? Boolean.FalseString);
        details[ResourcePropertiesKeys.DisplayWarningThreshold] =
            Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.DisplayWarningThreshold] ?? Boolean.FalseString);
        details[ResourcePropertiesKeys.DisplayCriticalThreshold] =
            Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.DisplayCriticalThreshold] ?? Boolean.FalseString);
        details[ResourcePropertiesKeys.MaxCount] = _maxCount;
        details[ResourcePropertiesKeys.ViewId] = Resource.View.ViewID;

        return details;
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (_transactionStep != null)
        {
            return new[] { _transactionStep.Id.ToString() };
        }

        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return SolarWinds.SEUM.Common.NetObjectPrefix.TransactionStep; }
    }

    protected override string DefaultTitle
    {
        get { return "Transaction Step Chart"; }
    }

    public override string DisplayTitle
    {
        get
        {
            string title = Title;
            if (title.Contains("XX"))
            {
                title = title.Replace("XX", _maxCount.ToString());
            }

            return title;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ITransactionStepProvider) }; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}