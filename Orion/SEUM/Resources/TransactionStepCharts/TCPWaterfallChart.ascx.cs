﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionStepCharts_TCPWaterfallChart : SEUMBaseStandardChartResource
{
    private const string FullscreenLinkText = "Show this chart in fullscreen";

    private TransactionStepObject _step;
    private string _fullScreenLink;
    // this is approximate average width of one letter used in english text drawn with default chart title font
    private const double WidestLetterWidth = 10;

    protected void Page_Init(object sender, EventArgs e)
    {
        _step = GetInterfaceInstance<ITransactionStepProvider>().TransactionStepObject;
        _fullScreenLink = ResolveUrl(string.Format(
            "~/Orion/SEUM/FullscreenChart.aspx?ResourceId={0}&NetObject={1}",
            Resource.ID,
            _step.NetObjectID));

        // initialize properties for this chart resource if needed
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", "SEUMTCPWaterfall");
        }

        HandleInit(WrapperContents);

        // see FB169719, in Galaga charts have added Export button which is useless for this chart, so we will disable it this way
        Wrapper.ShowManageButton = false;

        OrionInclude.ModuleFile("SEUM", "SEUM.css");
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.DataFormatters.js", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.TCPWaterfallLegend.js", OrionInclude.Section.Middle);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        
        // TCP Waterfall chart has dynamic height
        ChartPlaceHolder.Style[HtmlTextWriterStyle.Height] = "auto";
        
        FullscreenLink.Target = "_blank";
        FullscreenLink.Text = FullscreenLinkText + " »";
        FullscreenLink.NavigateUrl = _fullScreenLink;
        FullscreenLink.Visible = !Resource.IsInReport;

        var dal = new ChartsDAL();
        // hide if there are no data
        if (dal.GetStepRequestsCount(_step.Id) == 0)
        {
            Wrapper.Visible = false;
        }
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var title = ChartResourceSettings.Title;
        var subtitle = ChartResourceSettings.SubTitle;

        if (String.IsNullOrEmpty(title))
            title = _step.Name;

        // Parse macros in the title/subtitle (if they are there)
        if (title.Contains("${") || subtitle.Contains("${"))
        {
            var context = _step.ObjectProperties;
            title = Macros.ParseDataMacros(title, context, true, false);
            subtitle = Macros.ParseDataMacros(subtitle, context, true, false);
        }

        // if the title is longer than width of resource we will trim it and add ellipsis at the end
        int maxTextSize = (int)Math.Ceiling((double)ChartResourceSettings.Width / WidestLetterWidth);
        if(title.Length > maxTextSize)
        {
            title = title.Substring(0, maxTextSize) + "...";
        }

        var details = new Dictionary<string, object>();

        details["dataUrl"] = ChartResourceSettings.DataUrl;
        details["title"] = title;
        details["subtitle"] = subtitle;
        details["showTitle"] = true;
        details["netObjectIds"] = GetElementIdsForChart().ToArray();
        details["renderTo"] = ChartPlaceHolder.ClientID;
        details["legendInitializer"] = ChartLegendControl.LegendInitializer;
        details["itemsLimit"] = !Resource.IsInReport ? GetIntProperty(ResourcePropertiesKeys.MaxCount, 20) : 0;
        details["fullScreenLink"] = _fullScreenLink;

        return details;
    }

    public override String EditURL
    {
        get
        {
            string url = string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}", this.Resource.ID);

            if (!string.IsNullOrEmpty(Request["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
            if (Page is OrionView)
                url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
            if (!String.IsNullOrEmpty(Request.QueryString["ThinAP"]))
                url = String.Format("{0}&ThinAP={1}", url, Request.QueryString["ThinAP"]);

            return (url);
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/Charts/EditTCPWaterfallChart.ascx";
        }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (_step != null)
        {
            return new[] {_step.Id.ToString()};
        }

        return new string[0];
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return SolarWinds.SEUM.Common.NetObjectPrefix.TransactionStep; }
    }

    protected override string DefaultTitle
    {
        get { return "TCP Waterfall Chart"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ITransactionStepProvider) }; }
    }
}
