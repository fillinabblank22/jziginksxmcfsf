﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StepDetails.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionStepDetails_StepDetails" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="ActionsList" Src="~/Orion/SEUM/Resources/TransactionStepDetails/ActionsListControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
         <script type="text/javascript">
             function showBusyImage() {
                 $('#busyImage<%= Resource.ID %>').show();
             }
        </script>
        <table class="NeedsZebraStripes SEUM_DetailsTable">
            <tr>
                <td class="PropertyHeader"><%= managementLabel %></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:ImageButton ID="editButton" runat="server" ImageUrl="~/Orion/images/edit_16x16.gif" OnClick="EditClick" /> <asp:LinkButton runat="server" ID="editLink" Text="Edit" OnClick="EditClick" />
                            &nbsp;
                            <asp:ImageButton CausesValidation="false" ID="playButton" runat="server" ImageUrl="~/Orion/images/pollnow_16x16.gif" OnClick="PlayClick" OnClientClick="showBusyImage();" /> <asp:LinkButton runat="server" ID="playLink" Text="Poll now" OnClick="PlayClick" OnClientClick="showBusyImage();"  />                           
                            <img id="busyImage<%= Resource.ID %>" src="/Orion/images/AJAX-Loader.gif" style="display: none;" />
                            <span class="SEUM_HelpText"><asp:Literal runat="server" ID="playNotificationText" /></span>                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= statusLabel %></td>
                <td>
                    <orion:EntityStatusIcon ID="entityStatusIcon" runat="server" IconSize="Small" Entity="<%# Step.SwisEntity %>" Status="<%# Step.Status %>" WrapInBox="false" />
                    <%# UIHelper.Escape(Step.StatusDescription)%>
                </td>
            </tr>
            <% if (Step.Status == SEUMStatuses.Failed && !string.IsNullOrEmpty(Step.Model.LastErrorMessage))
                {%>
            <tr>
                <td class="PropertyHeader"><%=errorMessageLabel%></td>
                <td><%# UIHelper.Escape(Step.Model.LastErrorMessage)%></td>
            </tr>
            <%}%>
            <tr>
                <td class="PropertyHeader"><%= pageNameLabel %></td>
                <td> <%# Step.Name %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= urlLabel %></td>
                <td><a class="coloredLink" href="<%# Step.Url %>" target="_blank"><%# WrapHelper.ForceWrapUrl(Step.Url) %></a></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= durationLabel %></td>
                <td><%= (Step.Transaction.LastStatus == SEUMStatuses.Unknown) ? CommonTexts.NotAvailable : new DurationFormatter().Format(Step.LastDuration)%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= recordingNameLabel %></td>
                <td><%# UIHelper.Escape(Step.Recording.Name) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= locationLabel %></td>
                <td>
                    <orion:EntityStatusIcon ID="locationStatusIcon" runat="server" IconSize="Small" Entity="<%# SwisEntities.Agents %>" Status="<%# Step.Transaction.Agent.ConnectionStatus %>" WrapInBox="false" />
                    <a href="<%= Step.Transaction.Agent.DetailsUrl %>"><%# UIHelper.Escape(Step.Transaction.Agent.Name) %> (<%= AgentStatusMessage%>)</a>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= playbackIntervalLabel %></td>
                <td><asp:Literal runat="server" ID="playbackIntervalLiteral" /></td>
            </tr> 
            <tr>
                <td class="PropertyHeader"><%= lastPlaybackLabel %></td>
                <td><asp:Literal runat="server" ID="lastPlaybackLiteral" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= nextPlaybackLabel %></td>
                <td><asp:Literal runat="server" ID="nextPlaybackLiteral" /></td>
            </tr> 
        </table>
        <span class="SEUM_TableSpacer"></span>

        <SEUM:ActionsList runat="server" ID="actionsListTable" />
    </Content>
</orion:resourceWrapper>