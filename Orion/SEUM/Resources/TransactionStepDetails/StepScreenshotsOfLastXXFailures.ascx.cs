﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web;
using System.Web.UI.WebControls;
using System.Web;
using SolarWinds.SEUM.Common.Helpers;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_StepScreenshotsOfLastXXFailures : BaseTopXXResource
{
    protected const string defaultTitle = "Screenshots of Last XX Failures";
    protected const string timeOfFailureTitle = "Time of failure";
    protected const string errorMessageTitle = "Error message";
    protected const string screenshotTitle = "Screenshot";
    
    protected const string fullscreenLinkText = "View screenshot full screen";
    protected const string htmlLinkText = "view HTML";
    protected const string orText = "or";
    protected const string invalidScreenshotText = "There is no screenshot for this step";

    private TransactionStepObject step;
    protected virtual TransactionStepObject Step
    {
        get
        {
            if (this.step == null)
            {
                this.step = this.GetInterfaceInstance<ITransactionStepProvider>().TransactionStepObject;
            }
            return this.step;
        }
    }
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ITransactionStepProvider) }; }
    }

    protected override void OnInit(EventArgs e)
    {
        // pass entity type to edit control
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.StepResponseTimeDetail;
        
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ResourcesDAL dal = new ResourcesDAL();

        DataTable data = dal.GetTopXXStepFailuresByTransactionStep(MaxCount, Filter, Step.Id);

        stepFailures.ItemDataBound += new RepeaterItemEventHandler(stepFailures_ItemDataBound);

        stepFailures.DataSource = data;
        stepFailures.DataBind();
    }

    void stepFailures_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Image screenshotImage = e.Item.FindControl("screenshotImage") as Image;
        HyperLink fullscreenImageLink = e.Item.FindControl("fullscreenImageLink") as HyperLink;
        HyperLink fullscreenLink = e.Item.FindControl("fullscreenLink") as HyperLink;
        HyperLink htmlLink = e.Item.FindControl("htmlLink") as HyperLink;
        if (screenshotImage == null || fullscreenImageLink == null || fullscreenLink == null || htmlLink == null)
        {
            return;
        }
        
        // Get data
        DataRow row = ((DataRowView)e.Item.DataItem).Row;
        int entityId = (int)row["TransactionStepId"];
        //string detailsUrl = (DateTime)row["DetailsUrl"];
        DateTime date = (DateTime)row["DateTimeUtc"];
        string screenshotName = (string)row["Name"];

        // Create URL params
        string screenshotParameters = string.Format("entity={0}&entityId={1}&dateTime={2}&noImageText={3}", 
            SwisEntities.StepResponseTimeDetail, 
            entityId,
            date.Ticks,
            HttpUtility.UrlEncode(invalidScreenshotText));
        string thumbnailScreenshotParameters = string.Format("{0}&useThumbnail=true", screenshotParameters);

        // Create URLs
        string screenshotUrl = string.Format("/Orion/SEUM/Controls/ScreenshotImage.ashx?{0}&width={1}&height={2}", thumbnailScreenshotParameters, ImageHelper.DefaultThumbnailWidth, ImageHelper.DefaultThumbnailHeight);
        string screenshotTitle = string.Format("Screenshot - {0}", screenshotName);
        string fullscreenUrl = string.Format("/Orion/SEUM/FullscreenScreenshot.aspx?title={0}&{1}", HttpUtility.UrlEncode(screenshotTitle), screenshotParameters);
        string htmlUrl = string.Format("/Orion/SEUM/PageHTML.aspx?{0}", screenshotParameters);

        // Set URLs
        screenshotImage.ImageUrl = screenshotUrl;
        screenshotImage.AlternateText = invalidScreenshotText;
        screenshotImage.ToolTip = screenshotTitle;
        fullscreenImageLink.NavigateUrl = fullscreenUrl;
        fullscreenLink.NavigateUrl = fullscreenUrl;
        htmlLink.NavigateUrl = htmlUrl;
    }

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesScreenshotsOfLastXXFailures"; }
    }

    #endregion
}