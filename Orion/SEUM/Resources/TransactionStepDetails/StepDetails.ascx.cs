﻿using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Helpers;
using SolarWinds.SEUM.Web.Resources;
using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.BusinessLayer.Servicing;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionStepDetails_StepDetails : SEUMTransactionStepBaseResource
{
    private static readonly Log log = new Log();

    private const string defaultTitle = "Step Details";

    protected const string statusLabel = "Status";
    protected const string pageNameLabel = "Page&nbsp;name";
    protected const string urlLabel = "URL";
    protected const string recordingNameLabel = "Recording&nbsp;name";
    protected const string locationLabel = "Location";
    protected const string lastPlaybackLabel = "Last&nbsp;played";
    protected const string neverPlayedText = "Never";
    protected const string managementLabel = "Management";
    protected const string durationLabel = "Duration";

    protected const string playbackIntervalLabel = "Playback&nbsp;interval";
    protected const string nextPlaybackLabel = "Next&nbsp;playback";

    protected const string playbackStartedMessage = "Playback was started. It may take few minutes until results are displayed here.";
    protected const string playbackErrorMessage = "There was an error when starting transaction.";

    protected const string playNowLabel = "Play entire transaction now";
    protected const string nowText = "Now";
    protected const string disabledText = "Transaction is unmanaged";
    protected const string inText = "in";
    protected const string playbackIntervalEvery = "Every";

    protected const string errorMessageLabel = "Error&nbsp;message";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();

        playLink.Text = playNowLabel;
        playbackIntervalLiteral.Text = TimeSpanHelper.ToHumanReadableString(Step.Transaction.Frequency, playbackIntervalEvery + " {0}");              

        if (Step.Transaction.LastDateTimeUtc < new DateTime(1900, 1, 1))
        {
            lastPlaybackLiteral.Text = neverPlayedText;
            nextPlaybackLiteral.Text = nowText;
        }
        else
        {
            lastPlaybackLiteral.Text = Step.Transaction.LastDateTimeUtc.ToLocalTime().ToString();

            TimeSpan nextPlayback = (Step.Transaction.LastDateTimeUtc + Step.Transaction.Frequency) - DateTime.UtcNow;
            if (nextPlayback.Ticks < 0)
            {
                nextPlaybackLiteral.Text = nowText;
            }
            else
            {
                nextPlaybackLiteral.Text = TimeSpanHelper.ToHumanReadableString(nextPlayback, inText + " {0}");
            }
        }

        // handle disabled playback
        if (Step.Transaction.Unmanaged)
        {
            playButton.Visible = false;
            playLink.Visible = false;
            nextPlaybackLiteral.Text = disabledText;
        }

        // handle permissions
        if (!SolarWinds.SEUM.Web.SEUMProfile.AllowAdmin)
        {
            editButton.Visible = false;
            editLink.Visible = false;
        }

        bool showActions = GetStringValue(ResourcePropertiesKeys.ShowActions, "1") != "0";

        if (showActions)
        {
            actionsListTable.SetData(Step.Actions);
        }
        else
        {
            actionsListTable.Visible = false;
        }
    }

    protected string AgentStatusMessage
    {
        get
        {
            return System.Text.RegularExpressions.Regex.Replace(Step.Transaction.Agent.ConnectionStatusMessage, @"<[^>]*>", String.Empty);
        }
    }

    protected void EditClick(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("/Orion/SEUM/Admin/EditMonitor.aspx?id={0}&{1}={2}",
            Step.Transaction.TransactionId,
            CommonParameterNames.ReturnUrl,
            UrlHelper.ToSafeUrlParameter(Request.RawUrl)));
    }

    protected void PlayClick(object sender, EventArgs e)
    {
        string message = playbackStartedMessage;
        
        try
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                proxy.TransactionPlayNow(new int[] { Step.Transaction.TransactionId });
            }
        }
        catch (Exception ex)
        {
            message = playbackErrorMessage;
            log.Error("Error starting monitor transaction.", ex);
        }

        playNotificationText.Text = string.Format("<br/>{0}", message);
        playButton.Enabled = false;
        playLink.PostBackUrl = string.Empty;
        playLink.OnClientClick = "return false;";
        playLink.CssClass = "SEUM_DisabledLink";

        playLink.Enabled = false;
        playLink.PostBackUrl = string.Empty;
        playLink.OnClientClick = "return false;";
        playLink.CssClass = "SEUM_DisabledLink";
        
    }

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/EditStepDetailsResource.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionSEUMPHResourcesStepDetails";
        }
    }

    #endregion
    
    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Synchronous;
}
