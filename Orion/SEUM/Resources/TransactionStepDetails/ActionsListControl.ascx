﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActionsListControl.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionStepDetails_ActionsListControl" %>

<div class="SEUM_ActionsList">
    <h1 class="ReportHeader SEUM_ReportHeader">
        <%: ActionsTitle %>
    </h1>
    <ul>
        <asp:Repeater runat="server" ID="actionsList" OnItemDataBound="actionsList_OnItemDataBound">
            <ItemTemplate>
                <li><asp:PlaceHolder runat="server" ID="actionNamePlaceHolder"></asp:PlaceHolder></li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>