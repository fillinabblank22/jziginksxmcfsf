﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common.Helpers;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Common;
using SolarWinds.Orion.Web.Helpers;
using UrlHelper = SolarWinds.SEUM.Common.Helpers.UrlHelper;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionStepDetails_TopXXFailedRequests : BaseTopXXResource
{
    protected const string defaultTitle = "Requests With Issues";
    protected const string URLTitle = "URL";
    protected const string TypeTitle = "Type";
    protected const string StatusCodeTitle = "Status Code";
    protected const string ViewAllText = "View all";
    protected const string PageSizeText = "Items per page";

    private TransactionStepObject step;
    protected virtual TransactionStepObject Step
    {
        get
        {
            if (this.step == null)
            {
                this.step = this.GetInterfaceInstance<ITransactionStepProvider>().TransactionStepObject;
            }
            return this.step;
        }
    }
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ITransactionStepProvider) }; }
    }

    protected override void OnInit(EventArgs e)
    {
        // pass entity type to edit control
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.TransactionStepRequests;
        
        base.OnInit(e);

        // prepare the export url
        string csvExportUrl = "/Orion/SEUM/Controls/Export.ashx";

        csvExportUrl = UrlHelper.AppendParameter(csvExportUrl, "format", "csv");
        csvExportUrl = UrlHelper.AppendParameter(csvExportUrl, "source", "RequestsWithIssues");
        csvExportUrl = UrlHelper.AppendParameter(csvExportUrl, "transactionStepId", Step.Id.ToString());
        csvExportUrl = UrlHelper.AppendParameter(csvExportUrl, "categories", StatusCategories.ToString());
        csvExportUrl = UrlHelper.AppendParameter(csvExportUrl, "filter", UrlHelper.ToSafeUrlParameter(Filter));

        Wrapper.HeaderButtons.Controls.Add(new LocalizableButtonLink()
                                               {
                                                   Text = "Export CSV",
                                                   DisplayType = ButtonType.Resource,
                                                   CssClass = "EditResourceButton",
                                                   NavigateUrl = csvExportUrl
                                               });
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected int StatusCategories
    {
        get
        {
            return GetIntProperty(ResourcePropertiesKeys.RequestStatusCategory,
                                  (int)
                                  (BrowserRequest.StatusCategory.ClientError | BrowserRequest.StatusCategory.ServerError));
        }
    }

    protected int PageSize
    {
        get { return GetIntProperty(ResourcePropertiesKeys.MaxCount, 10); }
    }

    protected string StatusCodesHelpLink
    {
        get { return HelpHelper.GetHelpUrl("OrionWPMPHCodeDefinitions"); }
    }

    #region Overrides of BaseResourceControl

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/EditTopXXRequestsWithIssues.ascx";
        }
    }

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionWPMRequestswithIssues"; }
    }

    #endregion
}