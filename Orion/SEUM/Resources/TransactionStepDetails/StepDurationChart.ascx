﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StepDurationChart.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionStepDetails_StepDurationChart" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper>