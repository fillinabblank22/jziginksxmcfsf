﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvailabilityChart.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionStepDetails_AvailabilityChart" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Charting" %>
<%@ Register TagPrefix="SEUM" Namespace="SolarWinds.SEUM.Web.Charting.Custom" Assembly="SolarWinds.SEUM.Web"  %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="contentPanel" UpdateMode="Conditional">
            <ContentTemplate>
                <SEUM:Chart runat="server" id="chart" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel CssClass="SEUM_NoData" runat="server" ID="noDataPanel" Visible="false">
            <asp:Literal runat="server" ID="messageLiteral"></asp:Literal>
        </asp:Panel>
    </Content>
</orion:resourceWrapper>

<script type="text/javascript">
    function SetupCustomMenu<%= Resource.ID %>() {
        var contentPanel = $('#<%= contentPanel.ClientID %>');
        var resourceWrapper = contentPanel.parent().parent();

        var menuDiv = resourceWrapper.find('#customMenu');
        if (!menuDiv)
            return;

        menuDiv.html("<select onchange=\"<%= DropDownPostBack %>\" name=\"<%= HeaderMenuSelectName %>\">" +
        '<option value="">View Options</option>' +
        <% foreach (TimePeriod item in TimePeriodShortcuts){%>
           '<option value="<%= item.Value %>"><%=item.Label %></option>' +
        <%}%>
        '</select>');
        menuDiv.show();
    }
    SetupCustomMenu<%= Resource.ID %>();

    function SEUMUpdateResourceSubtitle<%= Resource.ID %>(value) {
        var parent = $('#<%= contentPanel.ClientID %>').parent();
        while (parent && (!parent.hasClass('ResourceWrapper') || parent.find('.HeaderBar').length == 0)) {
            parent = parent.parent();
        }

        if (parent)
            parent.find('h2').text(value);
    }
</script>