﻿using System;
using System.Collections.Generic;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Resources_TransactionStepDetails_TransactionStepNodes : SEUMBaseResource
{
    private const string Title = "Step Node Dependencies";

    private static readonly string FilterFormat = String.Format(@"Nodes.Uri IN (
        SELECT ParentUri 
        FROM {0} 
            JOIN {1} ON Dependencies.ChildUri = TransactionSteps.Uri
        WHERE TransactionSteps.TransactionStepId = {{0}})", 
                             SwisEntities.Dependencies,
                             SwisEntities.TransactionSteps);

    protected void Page_Load(object sender, EventArgs e)
    {
        NodesTable.UniqueClientID = Resource.JavaScriptFriendlyID;
        NodesTable.Filter = String.Format(FilterFormat, GetTransactionStepObject().Id);
        NodesTable.DataBind();
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new [] { typeof(ITransactionStepProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionWPMPHStepNodeDependencies"; }
    }

    private TransactionStepObject GetTransactionStepObject()
    {
        var tsp = GetInterfaceInstance<ITransactionStepProvider>();
        return tsp.TransactionStepObject;
    }
}