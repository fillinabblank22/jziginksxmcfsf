﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.Helpers;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Common.Models;
using System.Globalization;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SEUM.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionStepDetails_StepScreenshots : SEUMTransactionStepBaseResource
{
    #region Strings for localization

    private const string defaultTitle = "Current Screenshot";

    private const string noDataText = "There is no screenshot for selected time";
    private const string noPlaybacksText = "There are no screenshots to display because there was no transaction yet";
    private const string invalidScreenshotText = "There is no screenshot for this transaction";

    protected const string fullscreenLinkText = "View screenshot full screen";
    protected const string htmlLinkText = "view HTML";
    protected const string orText = "or";

    #endregion

    private readonly ResourcesDAL dal = new ResourcesDAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        TransactionStep step = dal.GetTransactionStep(this.Step.Id, true);

        if (string.IsNullOrEmpty(Resource.SubTitle))
            this.ResourceSubtitle = step.LastDateTimeUtc.ToLocalTime().ToString(CultureInfo.CurrentCulture);
        else
            this.ResourceSubtitle = string.Format("{0} - {1}", Resource.SubTitle,
                                                  step.LastDateTimeUtc.ToLocalTime().ToString(CultureInfo.CurrentCulture));

        // Create URL params
        string screenshotParameters = string.Format("entity={0}&entityId={1}&noImageText={2}",
            SwisEntities.TransactionSteps,
            step.TransactionStepId,
            HttpUtility.UrlEncode(invalidScreenshotText));
        string thumbnailScreenshotParameters = string.Format("{0}&useThumbnail=true", screenshotParameters);

        var viewportWidth= Resource.Width - 20; //need to account for padding
        // Create URLs
        string screenshotUrl = string.Format("/Orion/SEUM/Controls/ScreenshotImage.ashx?{0}&width={1}", thumbnailScreenshotParameters, viewportWidth);
        string screenshotTitle = string.Format("Screenshot - {0}", step.Name);
        string fullscreenUrl = string.Format("/Orion/SEUM/FullscreenScreenshot.aspx?title={0}&{1}", HttpUtility.UrlEncode(screenshotTitle), screenshotParameters);
        string htmlUrl = string.Format("/Orion/SEUM/PageHTML.aspx?{0}", screenshotParameters);

        // Set URLs
        screenshotImage.ImageUrl = screenshotUrl;
        screenshotImage.AlternateText = invalidScreenshotText;
        screenshotImage.ToolTip = UIHelper.Escape(screenshotTitle);
        fullscreenLink.NavigateUrl = fullscreenUrl;
        htmlLink.NavigateUrl = htmlUrl;

        ScriptManager.RegisterStartupScript(this.Page,
            this.GetType(),
            string.Format("SEUM_temp{0}", Resource.ID),
            string.Format("SEUMUpdateResourceSubtitle{0}('{1}');", Resource.ID, ResourceSubtitle),
            true);

        // demo server requires copyright information for screenshots
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            WebControl copyrightControl = DemoHelper.GetCopyrightControlForStepScreenshot(Server.MapPath, Step.Id);
            if (copyrightControl != null)
            {
                copyrightPanel.Controls.Add(copyrightControl);
                copyrightPanel.Visible = true;
            }
        }
    }

    protected string ResourceSubtitle
    {
        get;
        private set;
    }

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesStepScreenshot"; }
    }

    #endregion
}