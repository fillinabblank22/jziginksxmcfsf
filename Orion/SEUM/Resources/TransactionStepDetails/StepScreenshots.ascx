﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StepScreenshots.ascx.cs"
    Inherits="Orion_SEUM_Resources_TransactionStepDetails_StepScreenshots" %>

<style type="text/css">
    html.RenderPdf .SEUM_ScreenshotTable img
    {
        page-break-inside: auto!important;
    }
</style>

<script type="text/javascript">
    function SEUMUpdateResourceSubtitle<%= Resource.ID %>(value) {
        var parent = $('#SEUMScreenshotTable-<%= Resource.ID %>').parent();
        while (parent && (!parent.hasClass('ResourceWrapper') || parent.find('.HeaderBar').length == 0)) {
            parent = parent.parent();
        }

        if (parent)
            parent.find('h2').text(value);
    }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table class="SEUM_ScreenshotTable" cellspacing="0" ID="SEUMScreenshotTable-<%= Resource.ID %>">
        <tr>
            <td class="SEUM_Right">
                <asp:Image runat="server" ID="screenshotImage" CssClass="SEUM_ScreenshotImage" />
                <asp:Panel runat="server" ID="copyrightPanel" CssClass="sw-seum-screenshot-copyright" Visible="False"></asp:Panel>
                <asp:Panel runat="server" ID="linksPanel">
                    &#0187; <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="fullscreenLink"><%= fullscreenLinkText %></asp:Hyperlink>
                    <%= orText %> <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="htmlLink"><%= htmlLinkText %></asp:Hyperlink>    
                </asp:Panel>
            </td>
         </tr>
         </table>
    </Content>
</orion:resourceWrapper>
