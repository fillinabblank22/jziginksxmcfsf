﻿using System;
using System.Collections.Generic;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Licensing;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Resources_TransactionStepDetails_TransactionStepApplications : SEUMBaseResource
{
    private const string Title = "Step Application Dependencies";

    private static readonly string FilterFormat = String.Format(@"Application.Uri IN (
        SELECT ParentUri 
        FROM {0} 
            JOIN {1} ON Dependencies.ChildUri = TransactionSteps.Uri
        WHERE TransactionSteps.TransactionStepId = {{0}})", 
                                                          SwisEntities.Dependencies,
                                                          SwisEntities.TransactionSteps);
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (LicenseInfo.AreApplicationsAvailable)
        {
            ApplicationsTable.UniqueClientID = Resource.JavaScriptFriendlyID;
            ApplicationsTable.Filter = String.Format(FilterFormat, GetTransactionStepObject().Id);
            ApplicationsTable.DataBind();
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ITransactionStepProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionWPMPHStepApplicationDependencies"; }
    }

    private TransactionStepObject GetTransactionStepObject()
    {
        var tsp = GetInterfaceInstance<ITransactionStepProvider>();
        return tsp.TransactionStepObject;
    }
}