﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StepScreenshotsOfLastXXFailures.ascx.cs" Inherits="Orion_SEUM_Resources_Summary_StepScreenshotsOfLastXXFailures" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Helpers" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table class="DataGrid">

            <asp:Repeater runat="server" ID="stepFailures">
            <HeaderTemplate>
                <tr>
                    <td class="ReportHeader SEUM_ReportHeader" width="80">
                        <%= timeOfFailureTitle %>
                    </td>
                    <td class="ReportHeader SEUM_ReportHeader">
                        <%= errorMessageTitle %>
                    </td>
                    <td class="ReportHeader SEUM_ReportHeader" width="120">
                        <%= screenshotTitle %>
                    </td>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property SEUM_Property">
                        <%# DataBinder.Eval(Container.DataItem, "LocalDateTime")%>
                    </td>
                    <td class="Property SEUM_Property"><%# WrapHelper.ForceWrap(UIHelper.Escape(DataBinder.Eval(Container.DataItem, "ErrorMessage").ToString())) %></td>
                    <td class="Property SEUM_Property">
                        <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="fullscreenImageLink"><asp:Image runat="server" ID="screenshotImage" CssClass="SEUM_ScreenshotImage" /></asp:Hyperlink>
                        <br />
                        &#0187; <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="fullscreenLink"><%= fullscreenLinkText %></asp:Hyperlink>
                                <%= orText %> <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="htmlLink"><%= htmlLinkText %></asp:Hyperlink>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr>
                    <td class="ZebraStripe Property SEUM_Property">
                        <%# DataBinder.Eval(Container.DataItem, "LocalDateTime")%>
                    </td>
                    <td class="ZebraStripe Property SEUM_Property"><%# WrapHelper.ForceWrap(UIHelper.Escape(DataBinder.Eval(Container.DataItem, "ErrorMessage").ToString()))%></td>
                    <td class="ZebraStripe Property SEUM_Property">
                        <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="fullscreenImageLink"><asp:Image runat="server" ID="screenshotImage" CssClass="SEUM_ScreenshotImage" /></asp:Hyperlink>
                        <br />
                        &#0187; <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="fullscreenLink"><%= fullscreenLinkText %></asp:Hyperlink>
                                <%= orText %> <asp:Hyperlink Target="_blank" CssClass="coloredLink" runat="server" ID="htmlLink"><%= htmlLinkText %></asp:Hyperlink>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            </asp:Repeater>
        </table>
    </Content>
</orion:resourceWrapper>