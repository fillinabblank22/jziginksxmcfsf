﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common.Actions;

public partial class Orion_SEUM_Resources_TransactionStepDetails_ActionsListControl : System.Web.UI.UserControl
{
    protected const string ActionsTitle = "Actions";

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void SetData(IEnumerable<ActionBase> actions)
    {
        var displayActions = actions.Select(CreateDisplayAction).ToList();

        actionsList.DataSource = displayActions;
        actionsList.DataBind();
    }

    private DisplayAction CreateDisplayAction(ActionBase action)
    {
        if (action is ActionImageMatchBase)
        {
            return DisplayAction.CreateForImageMatch(
                action.Description, action.Step.StepId, action.Guid, action.GetHashCode());
        }

        return DisplayAction.CreateDefault(action.Description);
    }

    protected void actionsList_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var dataItem = (DisplayAction) e.Item.DataItem;
        var placeHolder = e.Item.FindControl("actionNamePlaceHolder");

        placeHolder.Controls.Add(new Literal
        {
            Mode = LiteralMode.Encode,
            Text = dataItem.ActionName,
            EnableViewState = false
        });
        
        if (dataItem.IsImageMatch)
        {
            var htmlCode = string.Format(
                @"&nbsp;<span id=""sw-seum-placeholder-{2}""></span>
                        <script type=""text/javascript"">
                            $().ready(function() {{
                                $('#sw-seum-placeholder-{2}').html(SEUM_HoverHelpLinkHtml({{
                                    html: '<img src=""/Orion/SEUM/Controls/ActionImage.ashx?stepId={0}&actionId={1}&width=250"" />',
                                    title: 'Image preview:'
                                }}));
                            }});
                        </script>",
                dataItem.StepId, dataItem.ActionGuid, dataItem.UniqueId);

            placeHolder.Controls.Add(new Literal
            {
                Mode = LiteralMode.PassThrough,
                Text = htmlCode,
                EnableViewState = false
            });
        }
    }

    private class DisplayAction
    {
        public string ActionName { get; private set; }

        public bool IsImageMatch { get; private set; }

        public int StepId { get; private set; }

        public Guid ActionGuid { get; private set; }

        public int UniqueId { get; private set; }

        public static DisplayAction CreateDefault(string actionName)
        {
            return new DisplayAction
            {
                ActionName = actionName,
                IsImageMatch = false
            };
        }

        public static DisplayAction CreateForImageMatch(string actionName, int stepId, Guid actionGuid, int uniqueId)
        {
            return new DisplayAction
            {
                ActionName = actionName,
                IsImageMatch = true,
                ActionGuid = actionGuid,
                StepId = stepId,
                UniqueId = uniqueId
            };
        }
    }
}