﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXFailedRequests.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionStepDetails_TopXXFailedRequests" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4"  />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include Module="SEUM" File="Resources/FailedRequestsResource.js" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table class="DataGrid" id="RequestsTable<%= Resource.ID %>">
                <tr>
                    <td class="ReportHeader SEUM_ReportHeader">
                        <%= URLTitle %>
                    </td>
                    <td class="ReportHeader SEUM_ReportHeader">
                        <%= TypeTitle %>
                    </td>
                    <td class="ReportHeader SEUM_ReportHeader">
                        <%= StatusCodeTitle %>
                    </td>
                </tr>
        </table>
        <table class="sw-seum-paging" id="RequestsTablePaging<%= Resource.ID %>">
            <tr>
                <td class="sw-seum-paging-button"><a href="javascript:void(0)" class="sw-seum-paging-first"><span class="sw-seum-paging-first-disabled"></span></a></td>
                <td class="sw-seum-paging-button"><a href="javascript:void(0)" class="sw-seum-paging-prev"><span class="sw-seum-paging-prev-disabled"></span></a></td>
                <td class="sw-seum-paging-current-page SEUM_RightBorder">Page <asp:TextBox runat="server" ID="pageBox" CssClass="sw-seum-paging-page-box" Columns="3" /> of <span class="sw-seum-paging-of">1</span></td>
                <td class="sw-seum-paging-button"><a href="javascript:void(0)" class="sw-seum-paging-next"><span class="sw-seum-paging-next-disabled"></span></a></td>
                <td class="sw-seum-paging-button"><a href="javascript:void(0)" class="sw-seum-paging-last"><span class="sw-seum-paging-last-disabled"></span></a></td>
                <td class="sw-seum-paging-view-all"><a href="javascript:void(0)" class="sw-seum-paging-all coloredLink"><%= ViewAllText %></a></td>
                <td class="sw-seum-paging-page-size-col"><%= PageSizeText %>&nbsp;<asp:TextBox runat="server" ID="pageSizeBox" CssClass="sw-seum-paging-page-size" Columns="3" /></td>
            </tr>
        </table>

        <script type="text/javascript">
            // <![CDATA[
            $(function () {
                SW.SEUM.Resources.FailedRequestsResource.Init(<%= Resource.ID %>, <%= Step.Id %>, <%= StatusCategories %>, '<%=HttpUtility.JavaScriptStringEncode(Filter) %>', <%= PageSize %>, '<%= StatusCodesHelpLink %>');
            });
            // ]]>
        </script>
    </Content>
</orion:resourceWrapper>