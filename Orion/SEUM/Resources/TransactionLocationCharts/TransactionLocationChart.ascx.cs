﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionLocationCharts_TransactionLocationChart : SEUMBaseStandardChartResource, IResourceIsInternal
{
    private TransactionLocationObject _location;
    private int _maxCount = 10;

    protected void Page_Init(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "SEUM.css", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.DataFormatters.js", OrionInclude.Section.Middle);

        _location = GetInterfaceInstance<ITransactionLocationProvider>().TransactionLocationObject;
        _maxCount = GetIntProperty(ResourcePropertiesKeys.MaxCount, 10);

        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        Dictionary<string, object> details = base.GenerateDisplayDetails();
        details[ResourcePropertiesKeys.MaxCount] = _maxCount;

        return details;
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (_location != null)
        {
            return new[] { _location.Id.ToString() };
        }

        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return SolarWinds.SEUM.Common.NetObjectPrefix.Location; }
    }

    protected override string DefaultTitle
    {
        get { return "Player Location Chart"; }
    }

    public override string DisplayTitle
    {
        get
        {
            string title = Title;
            if (title.Contains("XX"))
            {
                title = title.Replace("XX", _maxCount.ToString());
            }

            return title;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ITransactionLocationProvider) }; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}