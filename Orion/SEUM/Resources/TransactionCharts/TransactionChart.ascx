﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionChart.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionCharts_TransactionChart" %>
<orion:Include ID="Include1" runat="server" Module="SEUM" File="Charts/Charts.SEUM.Common.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper> 