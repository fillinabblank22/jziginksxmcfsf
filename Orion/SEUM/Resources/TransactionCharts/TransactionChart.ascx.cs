﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionCharts_TransactionChart : SEUMBaseStandardChartResource, IResourceIsInternal
{
    private TransactionObject _transaction;
    private int maxCount = 10;

    protected void Page_Init(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("SEUM", "SEUM.css", OrionInclude.Section.Middle);
        OrionInclude.ModuleFile("SEUM", "Charts/Charts.SEUM.DataFormatters.js", OrionInclude.Section.Middle);

        _transaction = GetInterfaceInstance<ITransactionProvider>().TransactionObject;
        maxCount = GetIntProperty(ResourcePropertiesKeys.MaxCount, 10);

        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        Dictionary<string, object> details = base.GenerateDisplayDetails();
        details[ResourcePropertiesKeys.MaxCount] = maxCount;
        details[ResourcePropertiesKeys.ViewId] = Resource.View.ViewID;

        return details;
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (_transaction != null)
        {
            return new[] { _transaction.Id.ToString() };
        }

        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return SolarWinds.SEUM.Common.NetObjectPrefix.Transaction; }
    }

    protected override string DefaultTitle
    {
        get { return "Transaction Chart"; }
    }

    public override string DisplayTitle
    {
        get
        {
            string title = Title;
            if (title.Contains("XX"))
            {
                title = title.Replace("XX", maxCount.ToString());
            }

            return title;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ITransactionProvider) }; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}