﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Common.Models;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionDetails_TransactionScreenshots : SEUMTransactionBaseResource
{
    #region Strings for localization

    private const string defaultTitle = "Current Screenshots of Steps";

    private const string noDataText = "There is no screenshot for selected time";
    private const string noStepsText = "There are no steps in this monitor";
    private const string invalidScreenshotText = "There is no screenshot for this step";

    #endregion

    private ResourcesDAL dal = new ResourcesDAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        browser.DropDownValueChanged += browser_DropDownValueChanged;
        browser.NextClicked += browser_NextClicked;
        browser.PrevClicked += browser_PrevClicked;
        browser.FirstClicked += browser_FirstClicked;
        browser.LastClicked += browser_LastClicked;

        browser.ParentResourceId = Resource.ID;
        browser.Width = Resource.Width;

        browser.NoCurrentScreenshotText = noDataText;
        browser.NoScreenshotsText = noStepsText;
        browser.InvalidScreenshotText = invalidScreenshotText;

        if (!IsPostBack)
        {
            ModelsDAL modelsDal = new ModelsDAL();
            IEnumerable<TransactionStep> steps = modelsDal.GetTransactionStepModelsByTransaction(Transaction.Id, true);

            CurrentStepId = 0;
            if (steps.Any())
            {
                StepIds = steps.Select(s => s.TransactionStepId).ToList();

                foreach (TransactionStep step in steps)
                {
                    browser.DropDownItems.Add(new ListItem(step.Name, step.TransactionStepId.ToString()));
                }

                // show last screenshot by default
                CurrentStepId = StepIds.First();

                RefreshScreenshot();
            }
            else
            {
                browser.SetNoScreenshots();
            }
        }
    }

    private void RefreshScreenshot()
    {
        string resourceSubtitle;

        // get latest step screenshot
        TransactionStepHistoryRecord currentRecord = dal.GetStepHistoryRecord(CurrentStepId, DateTime.UtcNow, false); 

        if (currentRecord == null)
        {
            CurrentStepId = 0;
            browser.SetNoCurrentScreenshot();
            resourceSubtitle = noStepsText;
        }
        else
        {
            TransactionStepObject step = new TransactionStepObject(CurrentStepId);

            string screenshotName = string.Format("{0} - {1}", step.Name, currentRecord.DateTimeUtc.ToLocalTime());

            bool isFirst = CurrentStepId == StepIds.First();
            bool isLast = CurrentStepId == StepIds.Last();
            browser.SetDropDownValue(CurrentStepId.ToString());
            browser.Update(SwisEntities.TransactionSteps, step.Id, currentRecord.DateTimeUtc, screenshotName, isFirst, isLast);

            resourceSubtitle = currentRecord.DateTimeUtc.ToLocalTime().ToString(CultureInfo.CurrentCulture);
        }

        UpdateSubTitle(resourceSubtitle);
    }

    private void UpdateSubTitle(String subtitle)
    {
        string resultingSubTitle;

        if (string.IsNullOrEmpty(Resource.SubTitle))
            resultingSubTitle = subtitle;
        else
            resultingSubTitle = string.Format("{0} - {1}", Resource.SubTitle, subtitle);

        ScriptManager.RegisterStartupScript(this.Page,
            this.GetType(),
            string.Format("SEUM_temp{0}", Resource.ID),
            string.Format("SEUMUpdateResourceSubtitle{0}('{1}');", Resource.ID, resultingSubTitle),
            true); 
    }

    private int CurrentStepId
    {
        get
        {
            return (int)Session[StepIdSessionKey];
        }
        set
        {
            Session[StepIdSessionKey] = value;
        }
    }
    
    private string StepIdSessionKey
    {
        get
        {
            return string.Format("SEUM_Screenshot_{0}_{1}_record", Resource.ID, Transaction.Id);
        }
    }

    private IList<int> StepIds
    {
        get
        {
            return (List<int>)Session[StepIdsSessionKey];
        }
        set
        {
            Session[StepIdsSessionKey] = value;
        }
    }

    private string StepIdsSessionKey
    {
        get
        {
            return string.Format("SEUM_Screenshot_{0}_{1}_StepIds", Resource.ID, Transaction.Id);
        }
    }

    #region Handlers

    void browser_DropDownValueChanged(object sender, EventArgs<string> e)
    {
        int stepId;
        if (!Int32.TryParse(e.Object, out stepId))
            return;

        CurrentStepId = stepId;

        RefreshScreenshot();
    }

    void browser_LastClicked(object sender, EventArgs e)
    {
        CurrentStepId = StepIds.Last();
        RefreshScreenshot();
    }

    void browser_NextClicked(object sender, EventArgs e)
    {
        int index = StepIds.IndexOf(CurrentStepId);
        if (index >= StepIds.Count - 1)
            return;

        CurrentStepId = StepIds[index + 1];
        RefreshScreenshot();
    }

    void browser_PrevClicked(object sender, EventArgs e)
    {
        int index = StepIds.IndexOf(CurrentStepId);
        if (index <= 0)
            return;

        CurrentStepId = StepIds[index - 1];
        RefreshScreenshot();
    }

    void browser_FirstClicked(object sender, EventArgs e)
    {
        CurrentStepId = StepIds.First();
        RefreshScreenshot();
    }

    #endregion

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesCurrentScreenshotsOfSteps"; }
    }
    
    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Synchronous;

    #endregion
}