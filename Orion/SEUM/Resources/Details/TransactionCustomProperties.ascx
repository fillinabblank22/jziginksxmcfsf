﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionCustomProperties.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionCustomProperty_TransactionCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="SEUM_TransactionCustomProperties"/>
    </Content>
</orion:resourceWrapper>