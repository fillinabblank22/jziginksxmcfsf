﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionDetails.ascx.cs" Inherits="OrionSeumResourcesDetailsTransactionDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="ResourceTable" Src="~/Orion/SEUM/Controls/DefaultResourceTable.ascx" %>
<%@ Register TagPrefix="SEUM" TagName="UnmanageWindow" Src="~/Orion/SEUM/Controls/UnManageTransactionDialog.ascx" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4"  />
<orion:Include runat="server" Module="SEUM" File="UnManageTransactionDialog.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
            function showBusyImage() {
                $('#busyImage<%= Resource.ID %>').show();
            }
        </script>
        <table class="NeedsZebraStripes SEUM_DetailsTable">
            <tr>
                <td class="PropertyHeader"><%= ManagementLabel %></td>
                <td>
                    <asp:UpdatePanel runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:ImageButton ID="editButton" runat="server" ImageUrl="~/Orion/images/edit_16x16.gif" OnClick="EditClick" /> <asp:LinkButton runat="server" ID="editLink" Text="Edit" OnClick="EditClick" />
                            &nbsp;
                            <asp:ImageButton ID="unmanageButton" runat="server" ImageUrl="~/Orion/SEUM/images/icon_manage.gif" OnClientClick="UnmanageTransaction(); return false;" /> <asp:LinkButton runat="server" ID="unmanageLink" Text="Unmanage" OnClientClick="UnmanageTransaction(); return false;" />
                            <asp:ImageButton ID="remanageButton" runat="server" ImageUrl="~/Orion/SEUM/images/icon_remanage.gif" OnClick="RemanageClick" /> <asp:LinkButton runat="server" ID="remanageLink" Text="Remanage" OnClick="RemanageClick" />
                            &nbsp;
                            <asp:ImageButton ID="playButton" runat="server" ImageUrl="~/Orion/images/pollnow_16x16.gif" OnClick="PlayClick" OnClientClick="showBusyImage();" /> <asp:LinkButton runat="server" ID="playLink" Text="Play now" OnClick="PlayClick" OnClientClick="showBusyImage();"  />
                            <img id="busyImage<%= Resource.ID %>" src="/Orion/images/AJAX-Loader.gif" style="display: none;" />
                            <span class="SEUM_HelpText"><asp:Literal runat="server" ID="playNotificationText" /></span>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= StatusLabel %></td>
                <td>
                    <orion:EntityStatusIcon ID="entityStatusIcon" runat="server" IconSize="Small" Entity="<%# Transaction.SwisEntity %>" Status="<%# Transaction.Status %>" WrapInBox="false" />
                    <%# UIHelper.Escape(Transaction.StatusDescription) %>
                </td>
            </tr>
            <% if (Transaction.Status == SEUMStatuses.Failed && !string.IsNullOrEmpty(Transaction.Model.LastErrorMessage))
               {%>
            <tr>
                <td class="PropertyHeader"><%=ErrorMessageLabel%></td>
                <td><%# UIHelper.Escape(Transaction.Model.LastErrorMessage) %></td>
            </tr>
            <%
               }%>
            <tr>
                <td class="PropertyHeader"><%= RecordingName %></td>
                <td> <%# UIHelper.Escape(Transaction.Recording.Name) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= RecordingId %></td>
                <td> <%# UIHelper.Escape(Transaction.Recording.RecordingId.ToString()) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Location %></td>
                <td>
                    <orion:EntityStatusIcon ID="locationStatusIcon" runat="server" IconSize="Small" Entity="<%# SwisEntities.Agents %>" Status="<%# Transaction.Model.Agent.ConnectionStatus %>" WrapInBox="false" />
                    <a href="<%= Transaction.Model.Agent.DetailsUrl %>"><%# Transaction.AgentName %> (<%= AgentStatusMessage %>)</a>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= RecorderVersionLabel %></td>
                <td><asp:Literal runat="server" ID="recorderVersionLiteral" /></td>
            </tr>   
            <tr>
                <td class="PropertyHeader"><%= PlaybackIntervalLabel %></td>
                <td><asp:Literal runat="server" ID="playbackIntervalLiteral" /></td>
            </tr>    
            <tr>
                <td class="PropertyHeader"><%= LastPlayback %></td>
                <td><asp:Literal runat="server" ID="lastPlaybackLiteral" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= NextPlaybackLabel %></td>
                <td><asp:Literal runat="server" ID="nextPlaybackLiteral" /></td>
            </tr>
            <% if (!String.IsNullOrEmpty(Transaction.Model.Description))
               { %>
            <tr>
                <td class="PropertyHeader"><%= Description %></td>
                <td><%# UIHelper.Escape(Transaction.Model.Description) %></td>
            </tr>
            <% } %>
        </table>
        <span class="SEUM_TableSpacer"></span>

        <SEUM:ResourceTable runat="server" ID="table" />

        <SEUM:UnmanageWindow runat="server" />
        <script type="text/javascript">
        // <![CDATA[
            function UnmanageTransaction() {
                SW.SEUM.UnManageTransactionDialog.ShowUnmanageWindow(<%# Transaction.Id %>, <%# UnmanageFrom %>, <%# UnmanageUntil %>);
                SW.SEUM.UnManageTransactionDialog.SetUnmanageAfterCallback(function () {
                    window.location.reload();
                });
            }
        // ]]>
        </script>

    </Content>
</orion:resourceWrapper>
