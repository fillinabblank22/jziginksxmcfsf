﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web.Charting.Custom;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web;
using System.Drawing;
using SolarWinds.SEUM.Web.Charting;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Details_StepByStepDurationChart : SEUMTransactionBaseResource
{
    private const string defaultTitle = "Legacy Step Duration";

    protected const string optimalLabel = "Typical";
    protected const string percentileLabelFormat = "{0}th percentile";
    protected const string stepNameLabel = "Step name";
    protected const string stepLabel = "Step";
    protected const string yAxisLabel = "Duration in seconds";

    private const string noDataError = "There is no data for selected time period";
    private const string noStepsText = "There are no steps to display";

    private const string SECURE_IMAGE_PIPE_LOCATION = "/Orion/Controls/ChartImagePipe.aspx";

    private const string PostBackArgument = "SEUMUpdateChartInterval";

    private TimePeriod timePeriod;
    protected SampleInterval sampleInterval;
    private DateTime dateFrom;
    private DateTime dateTo;
    private int width;
    private int height;

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(base.SubTitle))
                return string.Format("{0} - {1}", base.SubTitle, timePeriod.Label);
            else
                return timePeriod.Label;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesStepByStepDurationChart"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ITransactionProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SEUM/Controls/EditStepByStepDurationResource.ascx"; }
    }

    protected string DropDownPostBack
    {
        get;
        set;
    }

    protected string HeaderMenuSelectName
    {
        get 
        { 
            return string.Format("SEUM_step_chart_header_menu_{0}", Resource.ID); 
        }
    }

    protected IEnumerable<TimePeriod> TimePeriodShortcuts
    {
        get
        {
            return new[]
                       {
                           TimePeriod.Last2Hours,
                           TimePeriod.Today, 
                           TimePeriod.Last24Hours, 
                           TimePeriod.Last7Days, 
                           TimePeriod.Last30Days, 
                           TimePeriod.Last3Months
                       };
        }
    }

    protected int Percentile
    {
        get;
        set;
    }

    protected string PercentileLabel
    {
        get { return string.Format(percentileLabelFormat, Percentile); }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        Percentile = Convert.ToInt32(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("Web-ChartPercentile").SettingValue);

        DropDownPostBack = Page.ClientScript.GetPostBackEventReference(contentPanel, PostBackArgument);

        LoadParameters();

        ChartsDAL dal = new ChartsDAL();

        IList<StepDurations> steps = dal.GetStepByStepDurations(Transaction.Id, dateFrom, dateTo);

        playbacksRepeater.ItemDataBound += new RepeaterItemEventHandler(playbacksRepeater_ItemDataBound);

        playbacksRepeater.DataSource = steps;
        playbacksRepeater.DataBind();

        DataSource = GetDataForChart(steps);

        if (DataSource != null)
        {
            InitChart();
        }
        else
        {
            contentPanel.Visible = false;
            noDataPanel.Visible = true;
            messageLiteral.Text = noStepsText;
        }

        ScriptManager.RegisterStartupScript(this.Page,
            this.GetType(),
            string.Format("SEUM_temp{0}", Resource.ID),
            string.Format("SEUMUpdateResourceSubtitle{0}('{1}');", Resource.ID, SubTitle),
            true); 
    }

    void playbacksRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        PlaceHolder iconPlaceholder = item.FindControl("legendColorIconPlaceholder") as PlaceHolder;
        if (iconPlaceholder != null)
        {
            Color color = chart.ChartColors[item.ItemIndex % chart.ChartColors.Length];

            iconPlaceholder.Controls.Add(new Literal()
                                             {
                                                 Text = string.Format("<span class=\"SEUM_LegendColorIcon\" style=\"background-color: {0}\">&nbsp;</span>", ColorTranslator.ToHtml(color))
                                             });
        }
    }

    public DataTable DataSource;

    private void LoadParameters()
    {
        if (Page.IsPostBack)
        {
            if (Request["__EVENTARGUMENT"] == PostBackArgument)
            {
                int periodShortcut = 0;
                if (Int32.TryParse(Request[HeaderMenuSelectName], out periodShortcut))
                {
                    timePeriod = TimePeriod.FromValue(periodShortcut);
                    sampleInterval = timePeriod.DefaultSampleInterval;
                }
            }
        }

        if (timePeriod == null)
            timePeriod = TimePeriod.FromValue(GetIntProperty(ResourcePropertiesKeys.TimePeriod, TimePeriod.Today.Value));
        
        if (sampleInterval == null)
            sampleInterval = SampleInterval.FromValue(GetIntProperty(ResourcePropertiesKeys.SampleInterval, TimePeriod.Today.DefaultSampleInterval.Value));

        if (timePeriod == TimePeriod.Custom)
        {
            DateTime.TryParse(GetStringValue(ResourcePropertiesKeys.TimePeriodCustomFrom, DateTime.UtcNow.Date.ToString()), CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateFrom);
            DateTime.TryParse(GetStringValue(ResourcePropertiesKeys.TimePeriodCustomTo, DateTime.UtcNow.AddDays(1).Date.AddSeconds(-1).ToString()), CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateTo);
        }
        else
        {
            dateFrom = timePeriod.DateTimeFrom.ToUniversalTime();
            dateTo = timePeriod.DateTimeTo.ToUniversalTime();
        }

        width = GetIntProperty(ResourcePropertiesKeys.Width, 0);
        height = GetIntProperty(ResourcePropertiesKeys.Height, 0);
    }

    private DataTable GetDataForChart(IList<StepDurations> steps)
    {
        bool hasData = false;

        // generate data table containing steps and their durations for chart
        DataTable dt = new DataTable();
        dt.Columns.Add("DateTime", typeof(DateTime));

        int numSteps = steps.Count;
        for (int i = 0; i < numSteps; i++)
        {
            dt.Columns.Add("Step" + i, typeof(int));
        }

        if (numSteps > 0)
        {
            for (int i = 0; i < numSteps; i++)
            {
                int j = 0;
                foreach (KeyValuePair<DateTime, int?> sample in steps[i].GetSamples(sampleInterval.Interval))
                {
                    if (i == 0)
                    {
                        dt.Rows.Add();
                        dt.Rows[j]["DateTime"] = sample.Key;
                    }

                    if (sample.Value.HasValue)
                    {
                        dt.Rows[j]["Step" + i] = sample.Value.Value;
                        hasData = true;
                    }
                    else
                    {
                        dt.Rows[j]["Step" + i] = DBNull.Value;
                    }
                    j++;
                }
            }
        }

        if (!hasData)
            dt.Rows.Clear();

        return dt;
    }

    private void InitChart()
    {
        chart.ImagePipePageName = SECURE_IMAGE_PIPE_LOCATION;
        chart.ShowLegend = false;
        chart.ChartType = Chart.ChartTypes.StackAreaChart;
        chart.Width = (width > 0) ? width : Resource.Width;

        if (height > 0)
            chart.Height = height;
        else
            chart.Height = Math.Max(chart.Width/2, 200);

        chart.ChartTitle = Transaction.Name;
        chart.ChartSubTitle = defaultTitle;
        if (timePeriod == TimePeriod.Custom)
            chart.ChartTimeSpan = string.Format("{0} - {1}", dateFrom.ToLocalTime(), dateTo.ToLocalTime());
        else
            chart.ChartTimeSpan = timePeriod.Label.ToUpper();

        chart.YAxisDescription = yAxisLabel;

        chart.NoDataError = noDataError;

        chart.DataSource = DataSource;
    }
}