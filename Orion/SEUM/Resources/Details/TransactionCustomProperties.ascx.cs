﻿using System;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_SEUM_Resources_TransactionCustomProperty_TransactionCustomProperties : SEUMTransactionBaseResource
{
    private const string Title = "Transaction Custom Properties";

    protected void Page_Load(object sender, EventArgs e)
    {
        customPropertyList.Resource = Resource;
        customPropertyList.NetObjectId = Transaction.Id;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return new TransactionDAL().GetTransactionCustomProperties(Transaction.Id, displayProperties);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/Admin/CPE/Default.aspx");
        };
    }

    protected override string DefaultTitle
    {
        get { return Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionWPMPHTransactionCustomProperties"; }
    }
}