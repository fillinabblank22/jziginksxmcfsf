using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Common;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Details_LastXXEvents : SEUMTransactionBaseResource
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        eventList.MaxEventCount = GetIntProperty(ResourcePropertiesKeys.MaxCount, -1);
        eventList.PeriodName = Resource.Properties[ResourcePropertiesKeys.TimePeriod];
        eventList.ResourceID = Resource.ID;
        eventList.ItemIds = new[] { Transaction.Id };
        eventList.ItemType = NetObjectPrefix.Transaction;
        eventList.LoadData();
    }

    public override string DisplayTitle
    {
        get
        {
            return Title.Replace("XX", eventList.MaxEventCount.ToString());
        }
    }

    protected override string DefaultTitle
    {
        get { return eventList.DefaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return eventList.HelpLinkFragment; }
    }

    public override string SubTitle
    {
        get { return eventList.SubTitle; }
    }

    public override string  EditControlLocation
    {
        get { return eventList.EditControlLocation; }
    }
 }
