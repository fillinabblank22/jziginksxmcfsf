﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Dials)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Details_TransactionGauge : SEUMTransactionBaseResource
{
    private const string title = "Duration - Radial Gauge";
    private const string transactionDuration = "Current duration";
    private string gaugeStyle;
    private int gaugeScale;
    private string gaugeRangeMethod;
    private string gaugeRangeValue;

    protected void Page_Load(object sender, EventArgs e)
    {
        SolarWinds.Orion.Web.ControlHelper.AddStylesheet(Page, "/Orion/styles/GaugeStyle.css");

        gaugeStyle = GetStringValue(ResourcePropertiesKeys.Style, "Elegant Black");
        gaugeScale = GetIntProperty(ResourcePropertiesKeys.Scale, 100);
        gaugeRangeMethod = GetStringValue(ResourcePropertiesKeys.RangeMethod, "dynamic");

        switch (gaugeRangeMethod)
        {
            case "static":
                gaugeRangeValue = GetIntProperty(ResourcePropertiesKeys.StaticRange, 30).ToString();
                break;
            case "dynamic":
                gaugeRangeValue = GetStringValue(ResourcePropertiesKeys.DynamicRange, "10, 20, 40, 60, 100, 200, 400, 600, 1000");
                break;
            case "thresholds":
                gaugeRangeValue = GetStringValue(ResourcePropertiesKeys.ThresholdsRange, "1.3");
                break;
        }

        GaugeHelper.CreateGauge(
            GaugeResponseTime,
            gaugeScale,
            gaugeStyle,
            transactionDuration,
            "",
            Transaction.NetObjectID,
            "Radial",
            gaugeRangeMethod,
            0,
            gaugeRangeValue,
            Int64.Parse(GetStringValue("GaugeUnitsValue", "1")),
            GetStringValue("GaugeUnitsLabel", ""));
    }

    protected override string DefaultTitle
    {
        get { return title ; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/EditGauge.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesDurationGauge"; }
    }
}