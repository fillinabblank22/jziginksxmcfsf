﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecordingCustomProperties.ascx.cs" Inherits="Orion_SEUM_Resources_Details_RecordingCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="SEUM_RecordingCustomProperties" />
    </Content>
</orion:resourceWrapper>