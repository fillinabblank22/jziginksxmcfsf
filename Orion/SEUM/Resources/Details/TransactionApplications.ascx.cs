﻿using System;
using System.Collections.Generic;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Licensing;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

public partial class Orion_SEUM_Resources_Details_TransactionApplications : SEUMBaseResource
{
    private const string Title = "Transaction Application Dependencies";

    private static readonly string FilterFormat = String.Format(@"Application.Uri IN (
        SELECT ParentUri 
        FROM {0} 
            JOIN {1} ON Dependencies.ChildUri = Transactions.Uri
        WHERE Transactions.TransactionId = {{0}})",
                                                          SwisEntities.Dependencies,
                                                          SwisEntities.Transactions);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (LicenseInfo.AreApplicationsAvailable)
        {
            ApplicationsTable.UniqueClientID = Resource.JavaScriptFriendlyID;
            ApplicationsTable.Filter = String.Format(FilterFormat, GetTransactionObject().Id);
            ApplicationsTable.DataBind();
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ITransactionProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionWPMPHTransactionApplicationDependencies"; }
    }

    private TransactionObject GetTransactionObject()
    {
        var tp = GetInterfaceInstance<ITransactionProvider>();
        return tp.TransactionObject;
    }
}