﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionNodes.ascx.cs" Inherits="Orion_SEUM_Resources_Details_TransactionNodes" %>
<%@ Register TagPrefix="orion" TagName="TransactionNodesTable" Src="~/Orion/SEUM/Controls/NodesTable.ascx" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:TransactionNodesTable runat="server" ID="NodesTable" />
    </Content>
</orion:resourceWrapper>
