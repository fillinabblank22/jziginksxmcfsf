﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_TransactionDetails_TransactionDurationChart : SEUMBaseGraphResource
{
    private const string defaultTitle = "Legacy Min/Max/Average Duration";

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesDurationChart"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ITransactionProvider) }; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CreateChart(null, GetInterfaceInstance<ITransactionProvider>().TransactionObject.NetObjectID, "SEUMTransactionDuration", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("SEUMTransactionDuration", Resource, TransactionObject.Prefix);
    }

    public override string EditControlLocation
    {
        // TODO
        get { return string.Empty; }
    }
}