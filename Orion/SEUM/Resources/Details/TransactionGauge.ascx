﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionGauge.ascx.cs" Inherits="Orion_SEUM_Resources_Details_TransactionGauge" %>

<link rel="stylesheet" type="text/css" href="/Orion/styles/GaugeStyle.css" />

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <table class="GaugeTable">
            <tr>
                <td>
                <a href="/Orion/NetPerfMon/CustomChart.aspx?chartName=AvgRt&NetObject=<%# Transaction.NetObjectID %>&Period=Today" target="_blank"></a>                       
                <a href="/Orion/NetPerfMon/CustomChart.aspx?chartName=AvgRt&NetObject=<%# Transaction.NetObjectID %>&Period=Today" target="_blank">
                    <asp:PlaceHolder runat="server" ID="GaugeResponseTime" />
                </a>   
                </td>                                     
            </tr>
        </table>        
    </Content>
</orion:resourceWrapper>