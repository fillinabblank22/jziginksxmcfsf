﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StepByStepDurationChart.ascx.cs" Inherits="Orion_SEUM_Resources_Details_StepByStepDurationChart" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Import Namespace="SolarWinds.SEUM.Web.Charting" %>
<%@ Register TagPrefix="SEUM" Namespace="SolarWinds.SEUM.Web.Charting.Custom" Assembly="SolarWinds.SEUM.Web"  %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="contentPanel" UpdateMode="Conditional" >
            <ContentTemplate>
                <SEUM:Chart runat="server" id="chart" />

                <table class="DataGrid">
                    <tr>
                        <td class="ReportHeader SEUM_ReportHeader" colspan="2"><%= stepNameLabel%></td>
                        <td class="ReportHeader SEUM_ReportHeader"><%= optimalLabel %></td>
                        <td class="ReportHeader SEUM_ReportHeader"><%= PercentileLabel %></td>
                    </tr>

                <asp:Repeater runat="server" ID="playbacksRepeater">
                    <ItemTemplate>
                        <tr>
                            <td class="Property SEUM_Property SEUM_LegendColorIconCell"><asp:PlaceHolder runat="server" ID="legendColorIconPlaceholder"></asp:PlaceHolder></td>
                            <td class="Property SEUM_Property SEUM_LegendItemName">
                                <b><%= stepLabel %> <%# Container.ItemIndex+1 %>:</b> <a href="<%# ((StepDurations)Container.DataItem).Step.DetailsUrl%>"><%# ((StepDurations)Container.DataItem).Step.Name%></a>
                            </td>
                            <td class="Property SEUM_Property"><%# new DurationFormatter().Format(((StepDurations)Container.DataItem).Step.OptimalThreshold.TotalMilliseconds) %></td>
                            <td class="Property SEUM_Property"><%# new DurationFormatter().Format(((StepDurations)Container.DataItem).GetPercentile(Percentile, sampleInterval.Interval)) %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel CssClass="SEUM_NoData" runat="server" ID="noDataPanel" Visible="false">
            <asp:Literal runat="server" ID="messageLiteral"></asp:Literal>
        </asp:Panel>
    </Content>
</orion:resourceWrapper>

<script type="text/javascript">
    function SetupCustomMenu<%= Resource.ID %>() {
        var contentPanel = $('#<%= contentPanel.ClientID %>');
        var resourceWrapper = contentPanel.parent().parent();

        var menuDiv = resourceWrapper.find('#customMenu');
        if (!menuDiv)
            return;

        menuDiv.html("<select onchange=\"<%= DropDownPostBack %>\" name=\"<%= HeaderMenuSelectName %>\">" +
        '<option value="">View Options</option>' +
        <% foreach (TimePeriod item in TimePeriodShortcuts){%>
           '<option value="<%= item.Value %>"><%=item.Label %></option>' +
        <%}%>
        '</select>');
        menuDiv.show();
    }
    SetupCustomMenu<%= Resource.ID %>();

    function SEUMUpdateResourceSubtitle<%= Resource.ID %>(value) {
        var parent = $('#<%= contentPanel.ClientID %>').parent();
        while (parent && (!parent.hasClass('ResourceWrapper') || parent.find('.HeaderBar').length == 0)) {
            parent = parent.parent();
        }

        if (parent)
            parent.find('h2').text(value);
    }
</script>