﻿using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Helpers;
using SolarWinds.SEUM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.BusinessLayer.Servicing;
using SolarWinds.SEUM.Common.Models;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class OrionSeumResourcesDetailsTransactionDetails : SEUMTransactionBaseResource
{
    private static readonly Log Log = new Log();

    internal const string TransactionDetailsTitle = "Transaction Details";
    internal const string PlaybackIntervalEvery = "Every";
    internal const string LastPlayback = "Last&nbsp;played";
    internal const string Location = "Location";
    internal const string RecordingName = "Recording&nbsp;Name";
    internal const string RecordingId = "Recording&nbsp;ID";
    internal const string StatusLabel = "Status";
    internal const string PlaybackIntervalLabel = "Playback&nbsp;interval";
    internal const string RecorderVersionLabel = "Recorder version";
    internal const string NextPlaybackLabel = "Next&nbsp;playback";
    internal const string ManagementLabel = "Management";
    internal const string StepCol = "STEPS";
    internal const string DurationCol = "DURATION";
    internal const string StatusCol = "STATUS";
    internal const string Miliseconds = "ms";
    internal const string PlaybackStartedMessage = "Playback was started. It may take few minutes until results are displayed here.";
    internal const string PlaybackErrorMessage = "There was an error when starting transaction.";
    internal const string NeverPlayedText = "Never";
    internal const string NowText = "Now";
    internal const string DisabledText = "Transaction is unmanaged";
    internal const string InText = "in";
    internal const string PlayNowLabel = "Play&nbsp;now";
    internal const string ErrorMessageLabel = "Error&nbsp;message";
    internal const string Description = "Description";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        DataBind();

        playLink.Text = PlayNowLabel;
        playbackIntervalLiteral.Text = TimeSpanHelper.ToHumanReadableString(Transaction.Model.Frequency, PlaybackIntervalEvery + " {0}");
        recorderVersionLiteral.Text = GetRecordingTypeLiteral(Transaction.Model.Recording.ProbeType);

        if (Transaction.Model.LastDateTimeUtc < new DateTime(1900, 1, 1))
        {
            lastPlaybackLiteral.Text = NeverPlayedText;
            nextPlaybackLiteral.Text = NowText;
        }
        else
        {
            lastPlaybackLiteral.Text = Transaction.Model.LastDateTimeUtc.ToLocalTime().ToString(CultureInfo.InvariantCulture);

            TimeSpan nextPlayback = (Transaction.Model.LastDateTimeUtc + Transaction.Model.Frequency) - DateTime.UtcNow;
            nextPlaybackLiteral.Text = nextPlayback.Ticks < 0 
                ? NowText 
                : TimeSpanHelper.ToHumanReadableString(nextPlayback, InText + " {0}");
        }

        // handle disabled playback
        if (Transaction.Model.Unmanaged)
        {
            playButton.Visible = false;
            playLink.Visible = false;
            nextPlaybackLiteral.Text = DisabledText;

            unmanageButton.Visible = false;
            unmanageLink.Visible = false;
        }
        else
        {
            remanageButton.Visible = false;
            remanageLink.Visible = false;
        }

        // handle permissions
        if (!SEUMProfile.AllowAdmin)
        {
            editButton.Visible = false;
            editLink.Visible = false;

            unmanageButton.Visible = false;
            unmanageLink.Visible = false;
            remanageButton.Visible = false;
            remanageLink.Visible = false;
        }

        ResourcesDAL dal = new ResourcesDAL();
        DataTable data = dal.GetStepsByTransaction(Transaction.Id);

        List<ResourceTableColumn> tableColumns = new List<ResourceTableColumn>
        {
            new ResourceTableColumn() {DataItemKey = "Step", Name = StepCol},
            new ResourceTableColumn()
            {
                DataItemKey = "Duration",
                Name = DurationCol,
                ValueFormatter = new DurationFormatter(),
                CssClass = CssClasses.ResourceDurationColumn,
                StatusDataItemKey = "StatusId",
                IsHighlighted = true
            },
            new ResourceTableColumn() {DataItemKey = "Status", Name = StatusCol}
        };

        table.Columns = tableColumns;
        table.SetData(data);
    }

    protected void EditClick(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("/Orion/SEUM/Admin/EditMonitor.aspx?id={0}&{1}={2}", 
            Transaction.Id, 
            CommonParameterNames.ReturnUrl,
            UrlHelper.ToSafeUrlParameter(Request.RawUrl)));
    }

    protected string AgentStatusMessage
    {
        get
        {
            return System.Text.RegularExpressions.Regex.Replace(Transaction.Agent.ConnectionStatusMessage, @"<[^>]*>", string.Empty);
        }
    }

    protected string UnmanageFrom
    {
        get { return Transaction.Model.UnmanageFrom == DateTime.MinValue ? "null" : string.Format("'{0}'", Transaction.Model.UnmanageFrom.ToLocalTime()) ; }
    }

    protected string UnmanageUntil
    {
        get { return Transaction.Model.UnmanageUntil == DateTime.MaxValue ? "null" : string.Format("'{0}'", Transaction.Model.UnmanageUntil.ToLocalTime()); }
    }

    protected void PlayClick(object sender, EventArgs e)
    {
        string message = PlaybackStartedMessage;

        try
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                proxy.TransactionPlayNow(new[] { Transaction.Id });
            }
        }
        catch (Exception ex)
        {
            message = PlaybackErrorMessage;
            Log.Error("Error starting monitor transaction.", ex);
        }

        playNotificationText.Text = string.Format("<br/>{0}", message);
        playButton.Enabled = false;
        playLink.PostBackUrl = string.Empty;
        playLink.OnClientClick = "return false;";
        playLink.CssClass = "SEUM_DisabledLink";

        playLink.Enabled = false;
        playLink.PostBackUrl = string.Empty;
        playLink.OnClientClick = "return false;";
        playLink.CssClass = "SEUM_DisabledLink";
    }

    protected void RemanageClick(object sender, EventArgs e)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.TransactionManage(new[] {Transaction.Id});
        }

        // redirect to self to update whole page
        Response.Redirect(Request.Url.ToString());
    }

    protected string GetRecordingTypeLiteral(ProbeType type)
    {
        switch (type)
        {
            case ProbeType.Classic:
                return "Deprecated";
            case ProbeType.Chromium:
                return "WPM 3.0+";
            default:
                return "";
        }
    }

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return TransactionDetailsTitle; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionSEUMPHResourcesTransactionDetails";
        }
    }

    #endregion

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Synchronous;
}
