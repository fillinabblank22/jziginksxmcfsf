using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Common;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Details_ActiveAlerts : SEUMTransactionBaseResource
{
    protected override void OnInit(EventArgs e)
    {
        string showAckString = this.Resource.Properties[ResourcePropertiesKeys.ShowAcknowledgedAlerts];
        bool showAck = false;

        if (!String.IsNullOrEmpty(showAckString) && showAckString.Equals("true", StringComparison.InvariantCultureIgnoreCase))
        {
            showAck = true;
        }

        this.AlertsControl.ItemType = NetObjectPrefix.Transaction;
        this.AlertsControl.ItemIds = new[] {Transaction.Id};
        this.AlertsControl.ShowAcknowledgedAlerts = showAck;
        this.AlertsControl.ShowItemName = true;

        base.OnInit(e);
    }

    protected override string DefaultTitle
    {
        get { return "Active Transaction Alerts"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesActiveAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SEUM/Controls/EditActiveAlerts.ascx"; }
    }
}

