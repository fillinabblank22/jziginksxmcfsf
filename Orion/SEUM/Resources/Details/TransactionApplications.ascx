﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionApplications.ascx.cs" Inherits="Orion_SEUM_Resources_Details_TransactionApplications" %>
<%@ Register TagPrefix="orion" TagName="TransactionApplicationsTable" Src="~/Orion/SEUM/Controls/ApplicationsTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:TransactionApplicationsTable runat="server" ID="ApplicationsTable"/>
    </Content>
</orion:resourceWrapper>