﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionDurationChart.ascx.cs" Inherits="Orion_SEUM_Resources_TransactionDetails_TransactionDurationChart" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper>