<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveAlerts.ascx.cs"
    Inherits="Orion_SEUM_Resources_Details_ActiveAlerts" %>
<%@ Register TagPrefix="SEUM" TagName="ActiveAlertsControl" Src="~/Orion/SEUM/Controls/ActiveAlertsControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <SEUM:ActiveAlertsControl ID="AlertsControl" runat="server" ></SEUM:ActiveAlertsControl>
    </Content>
</orion:resourceWrapper>
