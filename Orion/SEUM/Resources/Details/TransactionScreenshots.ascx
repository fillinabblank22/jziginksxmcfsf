﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionScreenshots.ascx.cs"
    Inherits="Orion_SEUM_Resources_TransactionDetails_TransactionScreenshots" %>

<%@ Register TagPrefix="SEUM" TagName="ScreenshotBrowser" Src="~/Orion/SEUM/Controls/ScreenshotBrowser.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <SEUM:ScreenshotBrowser runat="server" ID="browser" />
    </Content>
</orion:resourceWrapper>
