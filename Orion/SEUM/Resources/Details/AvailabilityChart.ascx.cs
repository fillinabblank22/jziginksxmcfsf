﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Web.Charting.Custom;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web;
using System.Drawing;
using SolarWinds.SEUM.Web.Charting;
using SolarWinds.SEUM.Common;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Details_AvailabilityChart : SEUMTransactionBaseResource
{
    private const string defaultTitle = "Legacy Transaction Availability";

    private const string noDataError = "There is no data for selected time period";
    private const string noStepsText = "There are no steps to display";

    private const string SECURE_IMAGE_PIPE_LOCATION = "/Orion/Controls/ChartImagePipe.aspx";

    private const string PostBackArgument = "SEUMUpdateChartInterval";

    private TimePeriod timePeriod;
    protected SampleInterval sampleInterval;
    private DateTime dateFrom;
    private DateTime dateTo;
    private int width;
    private int height;

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(base.SubTitle))
                return string.Format("{0} - {1}", base.SubTitle, timePeriod.Label);
            else
                return timePeriod.Label;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesAvailabilityChart"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ITransactionProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SEUM/Controls/EditAvailabilityChartResource.ascx"; }
    }

    protected string DropDownPostBack
    {
        get;
        set;
    }

    protected string HeaderMenuSelectName
    {
        get 
        { 
            return string.Format("SEUM_step_chart_header_menu_{0}", Resource.ID); 
        }
    }

    protected IEnumerable<TimePeriod> TimePeriodShortcuts
    {
        get
        {
            return new[]
                       {
                           TimePeriod.Last2Hours,
                           TimePeriod.Today, 
                           TimePeriod.Last24Hours, 
                           TimePeriod.Last7Days, 
                           TimePeriod.Last30Days, 
                           TimePeriod.Last3Months
                       };
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        DropDownPostBack = Page.ClientScript.GetPostBackEventReference(contentPanel, PostBackArgument);

        LoadParameters();

        ChartsDAL dal = new ChartsDAL();

        DataTable availabilityData = dal.GetTransactionAvailability(Transaction.Id, dateFrom, dateTo);

        DataSource = GetDataForChart(availabilityData);

        if (DataSource != null)
        {
            InitChart();
        }
        else
        {
            contentPanel.Visible = false;
            noDataPanel.Visible = true;
            messageLiteral.Text = noStepsText;
        }

        ScriptManager.RegisterStartupScript(this.Page,
            this.GetType(),
            string.Format("SEUM_temp{0}", Resource.ID),
            string.Format("SEUMUpdateResourceSubtitle{0}('{1}');", Resource.ID, SubTitle),
            true); 
    }

    public DataTable DataSource;

    private void LoadParameters()
    {
        if (Page.IsPostBack)
        {
            if (Request["__EVENTARGUMENT"] == PostBackArgument)
            {
                int periodShortcut = 0;
                if (Int32.TryParse(Request[HeaderMenuSelectName], out periodShortcut))
                {
                    timePeriod = TimePeriod.FromValue(periodShortcut);
                    sampleInterval = timePeriod.DefaultSampleIntervalForAvailability;
                }
            }
        }

        if (timePeriod == null)
            timePeriod = TimePeriod.FromValue(GetIntProperty(ResourcePropertiesKeys.TimePeriod, TimePeriod.Today.Value));
        
        if (sampleInterval == null)
            sampleInterval = SampleInterval.FromValue(GetIntProperty(ResourcePropertiesKeys.SampleInterval, TimePeriod.Today.DefaultSampleIntervalForAvailability.Value));

        if (timePeriod == TimePeriod.Custom)
        {
            DateTime.TryParse(GetStringValue(ResourcePropertiesKeys.TimePeriodCustomFrom, DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture)), CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateFrom);
            DateTime.TryParse(GetStringValue(ResourcePropertiesKeys.TimePeriodCustomTo, DateTime.UtcNow.AddDays(1).Date.AddSeconds(-1).ToString(CultureInfo.InvariantCulture)), CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateTo);
        }
        else
        {
            dateFrom = timePeriod.DateTimeFrom.ToUniversalTime();
            dateTo = timePeriod.DateTimeTo.ToUniversalTime();
        }

        width = GetIntProperty(ResourcePropertiesKeys.Width, 0);
        height = GetIntProperty(ResourcePropertiesKeys.Height, 0);
    }

    private DataTable GetDataForChart(DataTable availabilityData)
    {
        List<DateTime> timePoints = GetTimePoints(dateFrom, dateTo, sampleInterval.Interval);

        DataTable dt = new DataTable();
        dt.Columns.Add("DateTime", typeof(DateTime));
        dt.Columns.Add("Up", typeof(int));
        dt.Columns.Add("Warning", typeof(int));
        dt.Columns.Add("Critical", typeof(int));
        dt.Columns.Add("Down", typeof(int));
        dt.Columns.Add("Unknown", typeof(int));

        int intervalIndex = -1;
        DateTime maxDateTimeForCurrentRow = timePoints[++intervalIndex];

        DataRow currentRow = null;
        bool emptyRow = true;

        foreach (DataRow row in availabilityData.Rows)
        {
            while ((DateTime)row["DateTime"] >= maxDateTimeForCurrentRow)
            {
                if (currentRow != null)
                {
                    // no data means unknown status
                    if (emptyRow)
                        currentRow["Unknown"] = 1;
                    dt.Rows.Add(currentRow);
                }

                // check for last interval
                if (intervalIndex == timePoints.Count - 1)
                {
                    maxDateTimeForCurrentRow = DateTime.Now;
                    // set after the end of list because we use intervalIndex-1 later
                    ++intervalIndex;
                }
                else
                    maxDateTimeForCurrentRow = timePoints[++intervalIndex];

                currentRow = dt.NewRow();
                // mark beginning of this interval so use previous time point
                currentRow["DateTime"] = timePoints[intervalIndex - 1];
                currentRow["Up"] = 0;
                currentRow["Warning"] = 0;
                currentRow["Critical"] = 0;
                currentRow["Down"] = 0;
                currentRow["Unknown"] = 0;

                emptyRow = true;
            }

            emptyRow = false;

            if (currentRow == null)
                continue;

            switch ((int)row["Status"])
            {
                case SEUMStatuses.Up:
                    currentRow["Up"] = (int)currentRow["Up"] + (Int64)row["RecordCount"];
                    break;
                case SEUMStatuses.Warning:
                    currentRow["Warning"] = (int)currentRow["Warning"] + (Int64)row["RecordCount"];
                    break;
                case SEUMStatuses.Critical:
                    currentRow["Critical"] = (int)currentRow["Critical"] + (Int64)row["RecordCount"];
                    break;
                case SEUMStatuses.Failed:
                    currentRow["Down"] = (int)currentRow["Down"] + (Int64)row["RecordCount"];
                    break;
                case SEUMStatuses.Unknown:
                    currentRow["Unknown"] = (int)currentRow["Unknown"] + (Int64)row["RecordCount"];
                    break;
            }
        }

        if (currentRow != null)
            dt.Rows.Add(currentRow);

        return dt;
    }

    /// <summary>
    /// Get time points to divide displayed period into sample of "interval" length.
    /// </summary>
    /// <param name="from">Start time in UTC</param>
    /// <param name="to">End time in UTC</param>
    /// <param name="interval">Interval to divide time period.</param>
    /// <returns>List of times in LocalTime to split original time period</returns>
    public List<DateTime> GetTimePoints(DateTime from, DateTime to, TimeSpan interval)
    {
        List<DateTime> timePoints = new List<DateTime>();
        // generate time points
        for (DateTime i = from; i <= to; i = i.Add(interval))
        {
            timePoints.Add(i.ToLocalTime());
        }

        return timePoints;
    }

    private void InitChart()
    {
        chart.ImagePipePageName = SECURE_IMAGE_PIPE_LOCATION;
        chart.ShowLegend = false;
        chart.ChartType = Chart.ChartTypes.StackColumnChart;
        chart.Width = (width > 0) ? width : Resource.Width;

        if (height > 0)
            chart.Height = height;
        else
            chart.Height = Math.Max(chart.Width/2, 200);

        chart.ChartTitle = Transaction.Name;
        chart.ChartSubTitle = defaultTitle;
        if (timePeriod == TimePeriod.Custom)
            chart.ChartTimeSpan = string.Format("{0} - {1}", dateFrom.ToLocalTime(), dateTo.ToLocalTime());
        else
            chart.ChartTimeSpan = timePeriod.Label.ToUpper();

        chart.YDataType = Chart.DataTypes.Percent;

        chart.NoDataError = noDataError;

        chart.DataSource = DataSource;

        chart.ShowLegend = true;

        chart.ChartColors = new[] 
        { 
            SEUMColors.UpStatusChartColor, 
            SEUMColors.WarningStatusChartColor,
            SEUMColors.CriticalStatusChartColor,
            SEUMColors.FailedStatusChartColor,
            SEUMColors.UnknownStatusChartColor
        };
    }
}