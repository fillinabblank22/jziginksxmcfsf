﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransactionsHealthOverview.ascx.cs" Inherits="Orion_SEUM_Resources_Summary_TransactionsHealthOverview" %>
<%@ Register TagPrefix="SEUM" TagName="ChartImage" Src="~/Orion/SEUM/Controls/ChartImage.ascx" %>

<asp:ScriptManagerProxy id="TransactionsTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/SEUM/Services/TransactionsHealthTree.asmx" />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="/Orion/SEUM/js/AjaxTree.js" />
	</Scripts>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="SEUM_PieChart">
            <SEUM:ChartImage runat="server" ID="chartImage" />
        </div>

        <script type="text/javascript">
	        $(function() {
	            SEUM.AjaxTree.Load(TransactionsHealthTree,
                                          '<%=this.tree.ClientID%>', 
                                          '<%=this.Resource.ID %>', 
                                          'SEUMTreeNode-', 
                                          'TransactionsTreeNode', 
                                          null,
                                          <%=this.RememberExpandedGroups.ToLower() %>, 
                                          <%=this.BatchSize %>,
                                          '<%=HttpUtility.JavaScriptStringEncode(this.Filter) %>',
                                          '<%=this.ShowMoreSingularMessage %>',
                                          '<%=this.ShowMorePluralMessage %>',
                                          false);
	        });	    
	    </script>
	
		<div class="SEUM_TransactionsTree" ID="tree" runat="server">
			<center type="loading_container">
				<img alt="Loading..." src="/Orion/images/AJAX-Loader.gif" /> Loading...
			</center>
		</div>

        <div id="SEUMTreeNode-<%=this.Resource.ID %>-ResourceError" class="<%=resourceFilterErrorClass %>" style="display: none;">
            <%=resourceFilterErrorText %>
        </div>
		
		<div class="SEUM_StatusTableRowContainer" id="SEUMTreeNode-<%=this.Resource.ID %>-TransactionsTreeNode" style="display: none;">
		    <div class="SEUM_StatusTableRow">
                <a href="javascript:void(0);" class="SEUM_TransactionsTreeNode-expandLink">
		            <span class="SEUM_TransactionsTreeNode-expandButtonWrapper"><img src="/Orion/images/Button.Expand.gif" class="SEUM_TransactionsTreeNode-expandButton" /></span>
		            <img class="SEUM_TransactionsTreeNode-statusIcon" />
		        </a>
		        <div class="SEUM_TransactionsTreeNode-itemName"></div>
                <div class="SEUM_Cleaner"></div>
            </div>
		    <div class="SEUM_TransactionsTreeNode-children" style="display: none; padding-left: 30px;"></div>
		</div>
    </Content>
</orion:resourceWrapper>