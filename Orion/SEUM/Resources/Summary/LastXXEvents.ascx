<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXXEvents.ascx.cs" Inherits="Orion_SEUM_Resources_Summary_LastXXEvents" %>
<%@ Register TagPrefix="SEUM" TagName="EventList" Src="~/Orion/SEUM/Controls/EventList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <SEUM:EventList runat="server" ID="eventList">
	    </SEUM:EventList>
	</Content>
</orion:resourceWrapper>
