﻿using System;
using System.Collections;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.Shared;
using SolarWinds.SEUM.Web.Charting;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Configuration;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Graphs)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_TransactionsHealthOverview : SEUMBaseResource
{
    private const string defaultTitle = "Legacy Transaction Health Overview";
    private const string _showMoreSingularMessage = "There is {0} more item. Show all...";
    private const string _showMorePluralMessage = "There are {0} more items. Show all...";

    protected string RememberExpandedGroups
    {
        get { return this.Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true"; }
    }

    protected string ShowMoreSingularMessage
    {
        get { return _showMoreSingularMessage; }
    }

    protected string ShowMorePluralMessage
    {
        get { return _showMorePluralMessage; }
    }

    protected int BatchSize
    {
        get { return SEUMWebConfiguration.Instance.AjaxTreeBatchSize; }
    }

    protected string Filter
    {
        get { return CheckNetObjectAndModifyFilter(this.Resource.Properties[ResourcePropertiesKeys.Filter] ?? string.Empty); }
    }

    protected string HideUnmanaged
    {
        get { return Resource.Properties[ResourcePropertiesKeys.HideUnmanaged] ?? "true"; }
    }


    protected override void OnInit(EventArgs e)
    {
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.Transactions;
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // create chart
        chartImage.URL = SEUMTransactionsHealthChartInfo.GenerateChartURL(this.Resource.ID, 300, 150, Filter, HideUnmanaged);
    }

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/EditTransactionsHealthOverviewResource.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionSEUMPHResourcesTransactionHealthOverview";
        }
    }

    #endregion

    protected class StatusRecord : IEnumerable<ResourceTableCell>
    {
        private List<ResourceTableCell> _items = new List<ResourceTableCell>();
        private StatusInfo _status;

        public StatusRecord(int statusId)
        {
            _status = StatusInfo.GetStatus(statusId);
        }

        public int Id 
        {
            get
            {
                return (_status != null) ? _status.StatusId : -1;
            }
        }

        public string Name
        {
            get
            {
                return (_status != null) ? _status.ShortDescription : string.Empty;
            }
        }
        
        public int Count
        {
            get { return _items.Count; }
        }

        public IEnumerable<ResourceTableCell> Items
        {
            get { return _items; }
        }

        public void AddItem(ResourceTableCell item)
        {
            _items.Add(item);
        }

        #region Implementation of IEnumerable

        public IEnumerator<ResourceTableCell> GetEnumerator()
        {
            return (Items != null) ? Items.GetEnumerator() : null;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}