﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_TransactionsByStatus : BaseItemByStatusResource
{
    private const string defaultTitle = "Transaction by Status";
    private const string transactionNameCol = "Transaction Name";
    private const string durationNameCol = "Duration";

    protected override void OnLoad(EventArgs e)
    {
        // pass entity type to edit control
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.Transactions;

        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ResourcesDAL dal = new ResourcesDAL();
            DataTable data = dal.GetTransactionsByStatus(Statuses, Filter);

            List<ResourceTableColumn> tableColumns = new List<ResourceTableColumn>();
            tableColumns.Add(new ResourceTableColumn()
                                 {
                                     DataItemKey = "Transaction",
                                     Name = transactionNameCol
                                 });
            tableColumns.Add(new ResourceTableColumn()
                                 {
                                     DataItemKey = "Duration",
                                     Name = durationNameCol,
                                     ValueFormatter = new DurationFormatter(),
                                     CssClass = CssClasses.ResourceDurationColumn,
                                     IsHighlighted = true
                                 });

            table.Columns = tableColumns;
            table.SetData(data);
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            SetResourceFilterError(Wrapper.Content);
        }
    }

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesTransactionsbyStatus"; }
    }
}