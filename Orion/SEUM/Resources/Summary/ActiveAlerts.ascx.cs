using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_ActiveAlerts : SEUMBaseResource
{
    protected override void OnLoad(EventArgs e)
    {
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.Transactions;
        string showAckString = this.Resource.Properties[ResourcePropertiesKeys.ShowAcknowledgedAlerts];
        bool showAck = false;

        if (!String.IsNullOrEmpty(showAckString) && showAckString.Equals("true", StringComparison.InvariantCultureIgnoreCase))
        {
            showAck = true;
        }

        AlertsControl.ShowAcknowledgedAlerts = showAck;
        AlertsControl.ShowItemName = true;
        AlertsControl.ItemIds = GetFilteredObjectIds();
        AlertsControl.ItemType = AlertsControl.ItemIds != null ? NetObjectPrefix.Transaction : null;

        base.OnLoad(e);
    }

    protected override string DefaultTitle
    {
        get { return "Active Transaction Alerts"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesActiveAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SEUM/Controls/EditActiveAlerts.ascx"; }
    }
}

