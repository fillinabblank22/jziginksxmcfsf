﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_TopXXStepsByDuration : BaseTopXXResource
{
    private const string defaultTitle = "Top XX Steps by Duration";
    private const string stepNameCol = "Step Name";
    private const string transactionNameCol = "Transaction Name";
    private const string durationNameCol = "Duration";
    private const string optimalNameCol = "% of Optimal";

    protected override void OnInit(EventArgs e)
    {
        // pass entity type to edit control
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.TransactionSteps;

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ResourcesDAL dal = new ResourcesDAL();
            DataTable data = dal.GetTopXXStepsByDuration(MaxCount, Filter);

            List<ResourceTableColumn> tableColumns = new List<ResourceTableColumn>();
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "Step",
                Name = stepNameCol
            });
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "Transaction",
                Name = transactionNameCol
            });
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "Duration",
                Name = durationNameCol,
                ValueFormatter = new DurationFormatter(),
                StatusDataItemKey = "StatusId",
                CssClass = CssClasses.ResourceDurationColumn,
                IsHighlighted = true
            });
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "PercentOfOptimal",
                ValueFormatter = new PercentOfOptimalFormatter(),
                CssClass = CssClasses.ResourcePercentOfOptimalColumn,
                Name = optimalNameCol,
            });

            table.Columns = tableColumns;
            table.SetData(data);
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            SetResourceFilterError(Wrapper.Content);
        }
    }

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesTopXXStepsbyDuration"; }
    }
    #endregion
}