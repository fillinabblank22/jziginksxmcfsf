using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Configuration;
using SolarWinds.SEUM.Web.DAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_AllTransactions : SEUMBaseResource
{
    private static Log log = new Log();

    private const string defaultTitle = "All Transactions";
    private const string manageText = "Manage Transactions";
    private const string noGroupingLabel = "No Grouping Applied";
    private const string groupingFormatString = "Grouped by {0}";
    
    protected const string loadingLabel = "Loading...";
    protected const string noTransactionLabel = "No transactions exist";
    protected const string downloadRecorderLabel = "Download recorder locally";
    protected const string learnMoreLabel = "Learn more";
    
    protected const string recordTransactionText = "<b>Record a new sequence</b> with Orion's desktop recorder, go to the Start menu, type WPM, and select WPM recorder on your Orion server.";
    protected const string addTransactionText = "<b>Add a monitor</b> to Orion, select the recording you want to monitor and assign location to play the recording.";
    protected const string addTransactionTextNotAdmin = "You do not have sufficient rights to add a transaction monitor. Contact your administrator.";
    protected const string gettingStartedText = "Getting started:";
    protected const string howtoMonitorText = "How do I monitor a transaction?";

    private const string showMoreSingularMessage = "There is {0} more item. Show all...";
    private const string showMorePluralMessage = "There are {0} more items. Show all...";

    protected const string addNewPlaybackMessage = "Add a new transaction";
    protected const string NameLabel = "Name";
    protected const string DurationLabel = "Duration";

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #region Resource Properties

    protected bool RememberExpandedGroups
    {
        get { return Boolean.Parse(this.Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true"); }
    }

    protected bool ExpandRootLevel
    {
        get { return Boolean.Parse(this.Resource.Properties[ResourcePropertiesKeys.ExpandRootLevel] ?? "false"); }
    }

    protected bool ShowDurations
    {
        get { return Boolean.Parse(this.Resource.Properties[ResourcePropertiesKeys.ShowDurations] ?? "false"); }
    }

    protected string ShowMoreSingularMessage
    {
        get { return showMoreSingularMessage; }
    }

    protected string ShowMorePluralMessage
    {
        get { return showMorePluralMessage; }
    }

    protected int BatchSize
    {
        get { return SEUMWebConfiguration.Instance.AjaxTreeBatchSize; }
    }

    protected string Filter
    {
        get { return CheckNetObjectAndModifyFilter(Resource.Properties[ResourcePropertiesKeys.Filter] ?? string.Empty); }
    }

    protected bool IsDemo
    {
        get { return SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer; }
    }
    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMPHResourcesAllTransactions"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/EditAllTransactionsResource.ascx";
        }
    }

    public override string SubTitle
    {
        get
        {
            if (!String.IsNullOrEmpty(base.SubTitle))
            {
                return base.SubTitle;
            }

            if (String.IsNullOrEmpty(Resource.Properties[ResourcePropertiesKeys.Grouping]))
            {
                return noGroupingLabel;
            }
            else
            {
                SwisMetadataDAL dal = new SwisMetadataDAL();
                SwisMetadataDAL.EntityProperty property = dal.GetEntityProperty(SwisEntities.Transactions, Resource.Properties[ResourcePropertiesKeys.Grouping]);
                if (property == null)
                {
                    log.ErrorFormat("Unable to find grouping property {0} for entity {1}", Resource.Properties[ResourcePropertiesKeys.Grouping], SwisEntities.Transactions);
                    return string.Empty;
                }

                return String.Format(groupingFormatString, property.DisplayName);
            }
        }
    }

    #endregion

    protected string AddMonitorLearnMoreLink
    {
        get { return HelpHelper.GetHelpUrl("OrionSEUMAGTransactionsCreating"); }
    }

    protected bool AllowAdmin
    {
        get { return SEUMProfile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.Transactions;
        Wrapper.ManageButtonImage = "~/Orion/SEUM/Images/Button.ManageTransactions.gif";
        Wrapper.ManageButtonTarget = "~/Orion/SEUM/Admin/ManageMonitors.aspx";
        // TODO: do we need own permissions?
        Wrapper.ShowManageButton = AllowAdmin;
        Wrapper.ManageButtonText = manageText;
    }

    protected void AddNewTransactionClick(object sender, EventArgs e)
    {
        Response.Redirect("/Orion/SEUM/Admin/Add/Default.aspx");
    }
}
