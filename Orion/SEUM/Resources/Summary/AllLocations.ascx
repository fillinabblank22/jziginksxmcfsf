<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllLocations.ascx.cs" Inherits="Orion_SEUM_Resources_Summary_AllLocations" %>
<%@ Register TagPrefix="SEUM" TagName="ResourceTable" Src="~/Orion/SEUM/Controls/DefaultResourceTable.ascx" %>
<%@ Reference Control="~/Orion/SEUM/Controls/LoadIndicator.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <SEUM:ResourceTable runat="server" ID="table" />
    </Content>
</orion:resourceWrapper>
