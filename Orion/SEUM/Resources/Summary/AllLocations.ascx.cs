using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web.Controls.ResourceTable;
using SolarWinds.SEUM.Web.Resources;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Configuration;
using SolarWinds.SEUM.Web.DAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_AllLocations : BaseItemByStatusResource
{
    private static Log log = new Log();

    private const string defaultTitle = "All Locations";
    private const string manageText = "Manage Locations";
    private const string locationNameCol = "Location Name";
    private const string assignedTransactionsNameCol = "Managed Transactions";
    private const string agentVersionNameCol = "Version";
    private const string agentLoadNameCol = "Current Player Load";

    protected override void OnLoad(EventArgs e)
    {
        // pass entity type to edit control
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.Agents;

        base.OnLoad(e);

        Wrapper.ManageButtonTarget = "~/Orion/SEUM/Admin/ManagePlayers.aspx";
        Wrapper.ShowManageButton = AllowAdmin;
        Wrapper.ManageButtonText = manageText;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ResourcesDAL dal = new ResourcesDAL();

            DataTable data = dal.GetAllLocations(Statuses, Filter);

            List<ResourceTableColumn> tableColumns = new List<ResourceTableColumn>();
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "Location",
                Name = locationNameCol
            });
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "AssignedTransactions",
                Name = assignedTransactionsNameCol,
                ValueFormatter = new WithProblemsValueFormatter()
            });
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "AgentVersion",
                Name = agentVersionNameCol
            });
            tableColumns.Add(new ResourceTableColumn()
            {
                DataItemKey = "LoadPercentage",
                Name = agentLoadNameCol,
                CellControl = (IResourceTableControl) LoadControl("~/Orion/SEUM/Controls/LoadIndicator.ascx")
           });

            table.Columns = tableColumns;
            table.SetData(data);
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            SetResourceFilterError(Wrapper.Content);
        }
    }

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSEUMAllLocationsResource"; }
    }

    protected bool AllowAdmin
    {
        get { return SEUMProfile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SEUM/Controls/EditAllLocationsResource.ascx";
        }
    }
}
