﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXStepsByDuration.ascx.cs" Inherits="Orion_SEUM_Resources_Summary_TopXXStepsByDuration" %>
<%@ Register TagPrefix="SEUM" TagName="ResourceTable" Src="~/Orion/SEUM/Controls/DefaultResourceTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <SEUM:ResourceTable runat="server" ID="table" />
    </Content>
</orion:resourceWrapper>