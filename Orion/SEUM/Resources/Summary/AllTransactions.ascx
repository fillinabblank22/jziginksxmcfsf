<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllTransactions.ascx.cs" Inherits="Orion_SEUM_Resources_Summary_AllTransactions" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>

<asp:ScriptManagerProxy id="TransactionsTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/SEUM/Services/AllTransactionsTree.asmx" />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="/Orion/SEUM/js/AjaxTree.js" />
	</Scripts>
</asp:ScriptManagerProxy>


<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <script type="text/javascript">
	        $(function() {
	            SEUM.AjaxTree.Load(TransactionsTree,
                                            '<%=this.tree.ClientID%>', 
                                            '<%=this.Resource.ID %>', 
                                            'SEUMTreeNode-', 
                                            'TransactionsTreeNode', 
                                            'NoDataContent', 
                                            <%=this.RememberExpandedGroups.ToString(CultureInfo.InvariantCulture).ToLower() %>, 
                                            <%=this.BatchSize %>,
                                            '<%=HttpUtility.JavaScriptStringEncode(this.Filter) %>',
                                            '<%=this.ShowMoreSingularMessage %>',
                                            '<%=this.ShowMorePluralMessage %>',
                                            <%=this.ShowDurations.ToString(CultureInfo.InvariantCulture).ToLower() %>,
                                            true,
	                                        <%=this.ExpandRootLevel.ToString(CultureInfo.InvariantCulture).ToLower() %> ,
	                                        <%=this.IsDemo.ToString(CultureInfo.InvariantCulture).ToLower() %>);
	        });	    
	    </script>
	
        <% if (ShowDurations)
           {%>
        <div class="SEUM_TransactionsTreeHeader ReportHeader SEUM_ReportHeader">
            <span class="SEUM_FloatRight"><%= DurationLabel %></span>
            <%= NameLabel %>
        </div>
        <%
           }%>
		<div class="SEUM_TransactionsTree" ID="tree" runat="server">
			<center type="loading_container">
				<img alt="Loading..." src="/Orion/images/AJAX-Loader.gif" /> <%= loadingLabel %>
			</center>
		</div>
		
		<div id="SEUMTreeNode-<%=this.Resource.ID %>-TransactionsTreeNode" style="display: none;">
            <div class="SEUM_TransactionsTreeNode-itemHeader">
                <% if (ShowDurations) {%><span class="SEUM_TransactionsTreeNode-itemDescription"></span><% } %>
		        <a href="javascript:void(0);" class="SEUM_TransactionsTreeNode-expandLink">
		            <span class="SEUM_TransactionsTreeNode-expandButtonWrapper"><img src="/Orion/images/Button.Expand.gif" class="SEUM_TransactionsTreeNode-expandButton" /></span>
		            <img class="SEUM_TransactionsTreeNode-statusIcon" />
		        </a>
		        <div class="SEUM_TransactionsTreeNode-itemName<% if (ShowDurations) {%> SEUM_TransactionsTreeNode-descriptionDisplayed<% } %>"></div>
                <div class="SEUM_Cleaner"></div>
            </div>
		    <div class="SEUM_TransactionsTreeNode-children" style="display: none;"></div>
		</div>

        <div id="SEUMTreeNode-<%=this.Resource.ID %>-ResourceError" class="<%=resourceFilterErrorClass %>" style="display: none;">
            <%=resourceFilterErrorText %>
        </div>

        <div id="SEUMTreeNode-<%=this.Resource.ID %>-NoDataContent" style="display: none;">
            <%= noTransactionLabel %>

            <div class="SEUM_GettingStartedBox">
                <div class="SEUM_First">
                    <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                    <h3><b><%= gettingStartedText %></b> <%= howtoMonitorText %></h3>
                </div>

                <div>
                    <img src="/Orion/SEUM/images/Icon.Recording.gif" />
                    <p>
                        <%= recordTransactionText %> &#0187; <a href="<%= CommonLinks.ChromiumRecorderInstaller %>" class="SEUM_HelpLink" target="_blank"><%= downloadRecorderLabel %></a>
                    </p>
                </div>

                <div>
                    <img src="/Orion/SEUM/images/Icon.AddMonitor.gif" />
                    <p>
                        <%= AllowAdmin ? addTransactionText : addTransactionTextNotAdmin %>
                        <span style="display: <%= AllowAdmin ? "inline" : "none" %>;">
                        &#0187; <a href="<%= AddMonitorLearnMoreLink %>" class="SEUM_HelpLink" target="_blank"><%= learnMoreLabel %></a>
                        </span>
                    </p>
                </div>

                <div class="SEUM_Buttons" style="display: <%= AllowAdmin ? "block" : "none" %>;">
                    <orion:LocalizableButton ID="addMonitorButton" LocalizedText="CustomText" Text="Add a Transaction Monitor" DisplayType="Primary" runat="server" CausesValidation="false"  OnClick="AddNewTransactionClick" />
                </div>
            </div>
        </div>
	</Content>	
</orion:resourceWrapper>
