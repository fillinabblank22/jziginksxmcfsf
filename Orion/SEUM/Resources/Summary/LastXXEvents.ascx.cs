using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SEUM_Resources_Summary_LastXXEvents : SEUMBaseResource
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        Resource.Properties[ResourcePropertiesKeys.SwisEntity] = SwisEntities.Transactions;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        eventList.MaxEventCount = GetIntProperty(ResourcePropertiesKeys.MaxCount, -1);
        eventList.ItemIds = GetFilteredObjectIds();
        eventList.ItemType = eventList.ItemIds != null ? NetObjectPrefix.Transaction : null;
        eventList.PeriodName = Resource.Properties[ResourcePropertiesKeys.TimePeriod];
        eventList.ResourceID = Resource.ID;
        eventList.LoadData();
    }

    public override string DisplayTitle
    {
        get
        {
            return Title.Replace("XX", eventList.MaxEventCount.ToString());
        }
    }

    protected override string DefaultTitle
    {
        get { return eventList.DefaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return eventList.HelpLinkFragment; }
    }

    public override string SubTitle
    {
        get { return eventList.SubTitle; }
    }

    public override string  EditControlLocation
    {
        get { return eventList.EditControlLocation; }
    }
}
