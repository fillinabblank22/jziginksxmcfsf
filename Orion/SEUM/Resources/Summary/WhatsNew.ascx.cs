﻿using System;
using System.ServiceModel;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web.DAL;
using SolarWinds.SEUM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_SEUM_Resources_Summary_WhatsNew : SEUMBaseResource
{
    private string _moduleVersion = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if(string.IsNullOrEmpty(this._moduleVersion))
            {
                ResourcesDAL dal = new ResourcesDAL();
                _moduleVersion = dal.GetSEUMModuleVersion();
            } 
			imgNewFeature.Text = string.Format("Learn more about new features in {0} »", this._moduleVersion);
            imgNewFeature.NavigateUrl = HelpHelper.GetHelpUrl("wpm","OrionWPMWhatsNew202022");
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            SetResourceFilterError(Wrapper.Content);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Static;

    public sealed override string DisplayTitle
    {
        get { return string.Format("{0} {1}?", this.Title, this._moduleVersion); }
    }

    protected override string DefaultTitle
    {
        get { return "What's New in WPM"; }
    }
    
    public override string EditURL
    {
        get { return String.Empty; }
    }

    protected void OnRemoveResourceButtonClicked(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        var whereToRedirect = (this.Request.Url.ToString().Contains("DetachResource.aspx")) ? this.Request.UrlReferrer : this.Request.Url;
        this.Response.Redirect(whereToRedirect.ToString());
    }
}