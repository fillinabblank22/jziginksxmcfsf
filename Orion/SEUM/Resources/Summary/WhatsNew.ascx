﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNew.ascx.cs" Inherits="Orion_SEUM_Resources_Summary_WhatsNew" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<orion:resourcewrapper runat="server" id="Wrapper" CssClass="sw-seum-whatsnew-bg">
    <Content>
        <div class="sw-seum-whatsnew">
            <table>
                <tr>
                    <td style="border-bottom:0; padding-right: 10px; vertical-align: top; width: 55px;">
                        <img src="/Orion/SEUM/Images/WhatsNewResource/pic_newfeature.gif" alt="new feature" style="border: 0px;" />
                    </td>
                    <td style="border-bottom:0; padding-right: 10px; vertical-align: top;">
                        <div>						
                            This release includes the following improvements:
                            <ul style="list-style-type:disc;">
                                <li><strong>macOS support</strong> - Use the new Web Transaction Recorder to create recordings on Mac systems.</li>
                                <li><strong>Increased FIPS support</strong> - Create recordings and transactions that include Basic, Digest, and NTLM authentication when FIPS mode is enabled.</li>
                                <li><strong>SolarWinds Pingdom<sup>&#174;</sup> integration</strong> - Create recordings that can be used for Transaction Monitoring in Pingdom.
                                    <orion:HelpLink ID="WpmMapsHelpLink" runat="server" HelpUrlFragment="OrionWPMPingdom"
                                                    HelpDescription="Learn more" CssClass="helpLink" />
                                </li>
                                <li><strong>Improved authentication support</strong> - Record scenarios for websites where access is controlled with Basic Authentication, NTLM, or certificates.</li>
                                <li><strong>Proxy server support</strong> - Recorded steps can now traverse proxy servers in your environment.</li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom:0; padding-top: 10px; text-align:right; vertical-align: top; padding-bottom: 0px;">
                        <orion:LocalizableButtonLink runat="server" ID="imgNewFeature" Target="_blank" DisplayType="Primary">
                        </orion:LocalizableButtonLink>&nbsp;
                        <%if (!Request.Url.ToString().Contains(DetachURL) && Profile.AllowAdmin)
                          { %>
                            <orion:LocalizableButton runat="server" ID="btnRemoveResource" Text="Remove this widget" DisplayType="Secondary" OnClick="OnRemoveResourceButtonClicked"/>
                        <% } %>		
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</orion:resourcewrapper>
