<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="LicenseError.aspx.cs" Inherits="Orion_SEUM_LicenseError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>SEUM License Verification Failed</h1>
    <p class="SEUM_LicenseErrorText">
        License verification of WPM module failed. This may be caused by expired license or business layer not running.
    </p>
    <h2>Additional information</h2>
    <% if (Message != null)
       {%>
    <p class="SEUM_LicenseErrorText">
        <%= Message %>
    </p>
    <%
       }%>
    <h2>Troubleshooting steps</h2>
    <ol class="SEUM_LicenseErrorTroubleshooting">
        <li>Confirm that the SolarWinds Orion Module Engine service is running, as shown in the following steps: 
            <ol>
                <li>Click <b>Start &gt; Administrative Tools &gt; Services</b>.</li>
                <li>Right-click <b>SolarWinds Orion Module Engine</b>, and then click <b>Start</b>.</li>
            </ol>
        </li>
        <li>If the SolarWinds Orion Module Engine service is already running, your license is probably invalid. Contact SolarWinds Support.</li>
        <li>Refresh the Orion Web Console browser.</li>
        <li>If the Orion Web Console does not display, contact SolarWinds Support.</li>
    </ol>
</asp:Content>