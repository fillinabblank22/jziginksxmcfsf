<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/SEUM/SEUMView.master" CodeFile="Summary.aspx.cs" Inherits="Orion_SEUM_Summary" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Src="~/Orion/SEUM/Controls/DownloadRecorderMenu.ascx" TagPrefix="seum" TagName="DownloadRecorderMenu" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AdditionalTopRightPageLinks" runat="server">
    <div class="sw-seum-additional-menu-link">
        <seum:DownloadRecorderMenu runat="server"/>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SEUMMainContentPlaceHolder" runat="server">
    <orion:ResourceContainer runat="server" ID="resContainer" />
</asp:Content>
