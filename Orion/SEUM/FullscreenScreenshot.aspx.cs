﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.SEUM.Common;
using SolarWinds.SEUM.Web;
using SolarWinds.SEUM.Web.Helpers;

public partial class Orion_SEUM_FullscreenScreenshot : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = HttpUtility.HtmlEncode(HttpUtility.UrlDecode(Request["title"]));
        screenshotImage.ImageUrl = string.Format("/Orion/SEUM/Controls/ScreenshotImage.ashx?{0}", this.Request.QueryString);

        // demo server requires copyright information for screenshots
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            if (Request["entity"] != SwisEntities.TransactionSteps && Request["entity"] != SwisEntities.StepResponseTimeDetail)
                return;
            int stepId;
            if (!Int32.TryParse(Request["entityId"], out stepId))
                return;

            WebControl copyrightControl = DemoHelper.GetCopyrightControlForStepScreenshot(Server.MapPath, stepId);
            if (copyrightControl != null)
            {
                copyrightPanel.Controls.Add(copyrightControl);
                copyrightPanel.Visible = true;
            }
        }
    }
}