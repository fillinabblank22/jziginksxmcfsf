﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FullscreenScreenshot.aspx.cs" Inherits="Orion_SEUM_FullscreenScreenshot" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <orion:IncludedFiles Kind="Css" Section="Top" runat="server" />
    <orion:Include File="MainLayout.css" Section="Top" runat="server" />
    <orion:Include Module="SEUM" File="SEUM.css" Section="Top" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Image runat="server" ID="screenshotImage" />
        <asp:Panel runat="server" ID="copyrightPanel" CssClass="sw-seum-screenshot-copyright" Visible="False"></asp:Panel>
    </div>
    </form>
</body>
</html>
