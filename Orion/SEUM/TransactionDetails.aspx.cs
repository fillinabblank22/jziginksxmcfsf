﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.SEUM.Web.NetObjects;
using SolarWinds.SEUM.Web.Providers;
using SolarWinds.SEUM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.SEUM.Licensing;

public partial class Orion_SEUM_TransactionDetails : OrionView,ITransactionProvider
{
    private TransactionObject _transactionObject;

    private const string TransactionNodesResource = "Orion/SEUM/Resources/Details/TransactionNodes.ascx";
    private const string TransactionAppsResource = "Orion/SEUM/Resources/Details/TransactionApplications.ascx";
    
    public override string ViewType
    {
        get
        {
            return "SEUMTransactionDetails";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        viewTitle.ViewName = this.ViewInfo.ViewTitle;
        viewTitle.AddNetObject(TransactionObject);

        ((Orion_SEUM_SEUMView)this.Master).HelpFragment = "OrionWPMPHTransactionDetails";
    }

    protected override void OnInit(EventArgs e)
    {
        this.ResourceContainer2.DataSource = this.ViewInfo;
        this.ResourceContainer2.DataBind();
        HideIntegrationResourcesIfNeeded();
        base.OnInit(e);
    }

    private void HideIntegrationResourcesIfNeeded()
    {
        if (!LicenseInfo.AreNodesAvailable || !LicenseInfo.AreApplicationsAvailable)
        {
            var resourcesToMatch = new List<string>();
            if (!LicenseInfo.AreNodesAvailable)
            {
                resourcesToMatch.Add(TransactionNodesResource);
            }
            if (!LicenseInfo.AreApplicationsAvailable)
            {
                resourcesToMatch.Add(TransactionAppsResource);
            }

            foreach (var control in GetAllControls(ResourceContainer2))
            {
                if (control is SEUMBaseResource &&
                    resourcesToMatch.Any(rm => (control as SEUMBaseResource).Resource.File.Contains(rm)))
                {
                    control.Visible = false;
                }
            }
        }
    }

    private static IEnumerable<Control> GetAllControls(Control parent)
    {
        foreach (Control control in parent.Controls)
        {
            yield return control;
            foreach (Control descendant in GetAllControls(control))
            {
                yield return descendant;
            }
        }
    }

    public TransactionObject TransactionObject
    {
        get
        {
            // we need to reinstantiate transaction object to apply possible view limitations
            return _transactionObject ?? (_transactionObject = new TransactionObject(this.NetObject.NetObjectID));
        }
    }
}