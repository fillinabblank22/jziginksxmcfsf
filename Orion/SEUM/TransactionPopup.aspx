﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransactionPopup.aspx.cs" Inherits="Orion_SEUM_TransactionPopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SEUM.Common" %>
<%@ Import Namespace="SolarWinds.SEUM.Web" %>
<%@ Import Namespace="SolarWinds.SEUM.Common.Models" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<!-- Header -->
<h3 class="Status<%=Transaction.StatusInfo.ToString()%>"><%= popupName %></h3>

<div class="NetObjectTipBody SEUM_NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
    	<tr>
			<td colspan="2"><%= Transaction.Name%></td>
		</tr>
		<tr>
			<th><%= playbackStatus %>:</th>
			<td>
                <orion:EntityStatusIcon ID="playbackStatusIcon" runat="server" IconSize="Small" Entity="<%# Transaction.SwisEntity %>" Status="<%# Transaction.Status %>" WrapInBox="false" />
                <%# UIHelper.Escape(Transaction.StatusDescription) %>
            </td>
		</tr>

        <% if (Transaction.Status == SEUMStatuses.Failed && !string.IsNullOrEmpty(Transaction.Model.LastErrorMessage))
               {%>
            <tr>
                <th><%=errorMessageLabel%></th>
                <td><%# UIHelper.Escape(Transaction.Model.LastErrorMessage) %></td>
            </tr>
            <%
               }%>

        <tr>
			<th><%= locationStatus %>:</th>
			<td>
                <orion:EntityStatusIcon ID="locationStatusIcon" runat="server" IconSize="Small" Entity="<%# SwisEntities.Agents %>" Status="<%# Transaction.Model.Agent.ConnectionStatus %>" WrapInBox="false" />
                <%# UIHelper.Escape(Transaction.Model.Agent.Name) %> (<%= Transaction.Model.Agent.ConnectionStatusMessage %>)
            </td>
		</tr>

        <tr>
            <td colspan="2">
                <table cellpadding="0" cellspacing="0">
	            <tr>
	                <th colspan="3"><%= stepsWithProblems %>:</th>
	            </tr>
	        <%
	            int count = 0;
	            foreach (TransactionStep step in this.TopSteps) 
                {
                    count++;
					string rowColor = (count % 2 == 0) ? "apm_ZebraStripe" : "";
           %>
	            <tr class="<%=rowColor %>">
	                <td style="width:20px;">&nbsp;</td>
	                <td><img src="<%= GetStatusImagePath(step) %>" alt="" /></td>
	                <td><%= step.Name %></td>
	            </tr>
            <% } %>
        
            <% if (this.RemainingStepCount > 0) { %>
                <tr style="background-color:#E2E1D4;">
                    <td colspan="3" align="center"><%= this.MoreStepsMessage %></td>
                </tr>
            <% } %>
        
            <% if (this.TopSteps.Count == 0) { %>
                <tr style="background-color:#E2E1D4;">
                    <td colspan="3" align="center"><%= allStepsAreUp %></td>
                </tr>
            <% } %>
	    </table>
        </td>
		</tr>
	</table>
</div>