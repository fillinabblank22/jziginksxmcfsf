﻿<%@ Page Title="Download Web Performance Monitor Recorder" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" %>


<asp:Content ID="Content6" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1>Download Web Transaction Recorder</h1>

    <div style="padding-left:20px;">
        <p>The recorder download should automatically begin in a few seconds. If it does not, <a href="TransactionRecorder.exe"><u>click here</u></a> to start the download.</p>
    </div>
    <script type="text/javascript">
        $().ready(function () {
            window.setTimeout(function () {
                window.location = "TransactionRecorder.exe";
            }, 1500);

        });    
    </script>


</asp:Content>

