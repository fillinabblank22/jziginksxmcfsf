﻿using System;

public partial class Orion_Recommendations_Admin_EditViews_RecommendationsEditAccount : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var propValue = false;
            if (!String.IsNullOrEmpty(PropertyValue))
            {
                if (!Boolean.TryParse(PropertyValue, out propValue))
                {
                    propValue = false;
                }
            }
            ctrAllowProperty.ClearSelection();
            ctrAllowProperty.SelectedValue = propValue.ToString();
        }

        if (Boolean.TrueString.Equals(ctrAllowProperty.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
        {
            PropertyValue = Boolean.TrueString;
        }
        else
        {
            PropertyValue = Boolean.FalseString;
        }
    }

    public override string PropertyValue { get; set; }
}