﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecommendationsEditAccount.ascx.cs" Inherits="Orion_Recommendations_Admin_EditViews_RecommendationsEditAccount" %>

<asp:RadioButtonList ID="ctrAllowProperty" runat="server">
    <asp:ListItem Text="<%$ Resources: RecommendationsWebContent, Admin_Accounts_EditAccount_Allow %>" Value="True" />
    <asp:ListItem Text="<%$ Resources: RecommendationsWebContent, Admin_Accounts_EditAccount_Disallow %>" Value="False" />
</asp:RadioButtonList>