﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllowRecordingsManagement.ascx.cs" Inherits="Orion.WPMRecordings.Admin.EditViews.AllowRecordingsManagementControl" %>
<asp:ListBox ID="ctrAllowRecordingsManagement" SelectionMode="Single" Rows="1" runat="server">
    <asp:ListItem Text="Yes" Value="True"/>
    <asp:ListItem Text="No" Selected="True"  Value="False"/>
</asp:ListBox>