﻿using System;

namespace Orion.WPMRecordings.Admin.EditViews
{
    public partial class AllowRecordingsManagementControl : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
    {
        public override string PropertyValue { get; set; }

        protected void Page_Load(Object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var allowRecordingsManagementPostBack = false;
                Boolean.TryParse(PropertyValue, out allowRecordingsManagementPostBack);
                ctrAllowRecordingsManagement.ClearSelection();
                ctrAllowRecordingsManagement.SelectedValue = allowRecordingsManagementPostBack.ToString();
            } else {
                var allowRecordingsManagement = false;
                Boolean.TryParse(ctrAllowRecordingsManagement.SelectedValue, out allowRecordingsManagement);
                PropertyValue = allowRecordingsManagement.ToString();
            }
        }
    }
}