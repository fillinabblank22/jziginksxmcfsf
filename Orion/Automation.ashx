﻿<%@ WebHandler Language="C#" Class="Automation" %>

using System;
using System.Web;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Helpers;
using System.Xml.Serialization;

/// <summary>
/// Purpose is to provide information for automation which is easy to process programatically. 
/// </summary>
public class Automation : IHttpHandler 
{    
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "text/xml";
        context.Response.ContentEncoding = Encoding.UTF8;
        
        // no caching, get always fresh data
        context.Response.Expires = -1;
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetAllowResponseInBrowserHistory(true); // required for IE to work with HTTPS

        AutomationHelper.GetXMLDataForAutomation().Save(context.Response.Output);
    }
 
    public bool IsReusable 
    {
        get { return false; }
    }

}