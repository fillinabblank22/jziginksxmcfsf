﻿using System;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.SSH.Common;
using SolarWinds.SSH.Common.Model;

namespace Orion.SSH
{
    public partial class Orion_SSH_Terminal : Page
    {
        private readonly Log log = new Log();
        protected string HostName = "";
        protected SSHHostSettings Settings;

        protected void Page_Load(object sender, EventArgs e)
        {
            int nodeId;
            var strNodeId = Request.QueryString["NodeId"];
            if(SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                Response.Clear();
                string demomessage = "alert('" + Resources.SSHWebContent.WEBDATA_SK_06 + "'); window.close();";
                Response.Write("<script type='text/javascript'>" + demomessage + "</script>");
                Response.End();
            }
            if (!int.TryParse(strNodeId, out nodeId))
            {
                log.ErrorFormat("Unable to parse nodeId:{0}", strNodeId);
                throw new ArgumentException(string.Format("Unable to Parse NodeId {0}", strNodeId));
            }
            var node = NodeDAL.GetNode(nodeId);
            if (node != null)
            {
                HostName = node.IpAddress;
                Settings = new SSHHostSettings
                {
                    NodeId = node.ID,
                    TerminalRows = 24,
                    TerminalColumns = 80,
                    SSHPort = GetSSHPort(node)
                };
            }
        }

        private int GetSSHPort(Node node)
        {
            string strPort;
            int port;

            log.DebugFormat("NodeSettings: Getting SSH port settings for Node:{0}", node.ID);
            if (!(node.NodeSettings.TryGetValue(Strings.NODE_SETTINGS_SSH_PORT, out strPort) && int.TryParse(strPort, out port)))
            {
                log.DebugFormat("NodeSettings: Unable to get ssh port for Node:{0}, Using default port: {1}", node.ID,Strings.SSH_DEFAULT_PORT);
                port = Convert.ToInt32(Strings.SSH_DEFAULT_PORT);
            }
            return port;
        }
    }
}