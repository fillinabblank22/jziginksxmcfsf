﻿<%@ Page Title="<%$ Resources: SSHWebContent, WEBDATA_YK0_01 %>" Language="C#" AutoEventWireup="true" CodeFile="Terminal.aspx.cs" Inherits="Orion.SSH.Orion_SSH_Terminal" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Import Namespace="SolarWinds.SSH.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" type="text/css" href="/WebEngine/Resources/common/globalstyles.css"/>

    <orion:Include File="extjs/4.2.2/ext-all-sandbox-42.js" runat="server"/>
    <link rel="stylesheet" type="text/css" href="../js/extjs/4.2.2/resources/ext-theme-gray-sandbox-42-all.css"/>
    <link rel="stylesheet" type="text/css" href="styles/Terminal.css"/>
    <script type="text/javascript" src="js/ToolsetLogger.js"></script>
    <orion:Include File="signalR/jquery.signalR-1.2.2.js" runat="server"/>
    <script type="text/javascript" src="<%= Startup.SignalrPath %>/hubs"></script>
    <script type="text/javascript" src="js/term.js"></script>
    <script type="text/javascript" src="js/SSHUI.js"></script>
    <script type="text/javascript">
        var hostSettings = {
            NodeId: <%= Settings.NodeId %>,
            hostName: "<%= HostName %>",
            cols: <%= Settings.TerminalColumns %>,
            rows: <%= Settings.TerminalRows %>,
            port: <%= Settings.SSHPort %>,
        };
        $(document).ready(function() {
            var termWindow = new TermWindow(hostSettings, document.getElementById('term-container'));
            termWindow.init();
        });

        function isDemoMode() {
            return <%= OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <div id="term-container">
    </div>
</asp:Content>