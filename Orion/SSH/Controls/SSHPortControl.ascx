﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SSHPortControl.ascx.cs" Inherits="Orion_SSH_Controls_SSHPortControl" %>
<div class="contentBlock">
    <div class ="contentBlockHeader" runat="server" id="OrionSSHSettingsHeader">
        <table>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:Literal ID="OrionSSHPort" runat="server" Text="<%$ Resources:SSHWebContent,WEBDATA_SJ_01%>"/>
                </td>
                <td class="rightInputColumn">
                    <asp:TextBox ID="txtSSHPort" runat="server" Width="200"/>
                    <asp:RangeValidator ID="rangePortValidator" runat="server" MinimumValue ="1" MaximumValue ="65535" ControlToValidate ="txtSSHPort" ErrorMessage ="(1-65535)" Display="dynamic" Type="Integer" SetFocusOnError ="true"/>
                    <br/>
                    <span class="helpfulText" style="padding: 0;"><%= Resources.SSHWebContent.WEBDATA_SJ_02 %></span>
                </td>
            </tr>
        </table>
    </div>
</div>