﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.SSH.Common;

public partial class Orion_SSH_Controls_SSHPortControl : UserControl, INodePropertyPlugin
{
    
    private IList<Node> editedNodes;

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode,
        Dictionary<string, object> pluginState)
    {
        editedNodes = nodes;
        if (!IsPostBack)
        {
            var node = editedNodes.First();
            string port;

            if (!node.NodeSettings.TryGetValue(Strings.NODE_SETTINGS_SSH_PORT, out port))
            {
                port = Strings.SSH_DEFAULT_PORT;
            }
            txtSSHPort.Text = port;
        }
    }

    public bool Validate()
    {
        return true;
    }

    public bool Update()
    {
        var port = string.IsNullOrEmpty(txtSSHPort.Text) ? Strings.SSH_DEFAULT_PORT : txtSSHPort.Text;
        
        var indications = new List<IIndication>();
        foreach (var node in editedNodes)
        {
            string currentPort;
            if (!node.NodeSettings.TryGetValue(Strings.NODE_SETTINGS_SSH_PORT, out currentPort))
            {
                currentPort = Strings.SSH_DEFAULT_PORT;
            }

            NodeSettingsDAL.SafeInsertNodeSetting(node.ID, Strings.NODE_SETTINGS_SSH_PORT, port);
            if (!port.Equals(currentPort))
            {
                indications.Add(new NodeSettingsIndication(IndicationType.System_InstanceModified, node.ID, node.Caption,
                    Strings.NODE_SETTINGS_SSH_PORT, port, currentPort));
            }
        }

        if (indications.Count > 0)
        {
            var publisher = IndicationPublisher.CreateV3();
            publisher.ReportIndications(indications);
        }
        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}