﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;
using SolarWinds.SSH.Web;

public partial class Orion_SSH_Controls_SSHManagementPlugin : UserControl, IManagementTasksPlugin
{
    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private const string LogMessageTemplate = "Creating management task '{0}' in section '{1}' for NetObject {2}";
    public IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        if (!Profile.AllowNodeManagement)
            yield break;
        
        var node = netObject as Node;
        if (node == null)
            yield break;
        if (!SSHRoleAccessor.SSHEnabled)
        {
            log.DebugFormat("SSH Client plugin is disabled...");
            yield break;
        }

        log.DebugFormat(LogMessageTemplate, SSHWebContent.WEBDATA_SJ_04, CoreWebContent.WEBDATA_AK0_171, node.NodeID);

        var nodeSectionName = CoreWebContent.WEBDATA_AK0_171;
        const string JsOnClickFormat = "return SW.Orion.SSH.NodeManagement.openTerminalWindow({0});";
        yield return new ManagementTaskItem
        {
            Section = nodeSectionName,
            LinkInnerHtml = "<img width='16px' height='16px' src='/Orion/images/ToolsetIntegration/Small.SSH.mo.gif' alt='' />&nbsp;" + SSHWebContent.WEBDATA_SJ_04,
            OnClick = string.Format(JsOnClickFormat, node.NodeID)
        };
    }
    
}