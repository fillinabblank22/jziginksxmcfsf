﻿$(function () {
    var nodeManagement = SW.Core.namespace('SW.Orion.SSH.NodeManagement');

    function openTerminalWindow(nodeid) {
        var params = 'width=600,height=500,scrollbars=yes,resizable=yes,location=no,toolbar=no,status=no',
       url = '/Orion/SSH/Terminal.aspx?NodeId=' + nodeid;
        window.open(url, 'Terminal' + nodeid, params);
        return false;
    }
    //publish methods
    nodeManagement.openTerminalWindow = openTerminalWindow;
});
