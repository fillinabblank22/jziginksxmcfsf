﻿/*
Dependencies:
* Signalr
* ExtJS/jQuery
*/
(function ($) {
    //ref codes :http://www.real-world-systems.com/docs/ANSIcode.html
    Terminal.prototype.prompt = function (prompt, echo) {
        var deferred = $.Deferred();
        var term = this;
        try {

            var response = [],
                caret = 0;
            function writeToTerm(key) {
                if (echo) {
                    term.write(key);
                }
            }
            function onTerminalData(key, ev) {
                var printable = (!ev.altKey && !ev.altGraphKey && !ev.ctrlKey && !ev.metaKey);
               
                if (!ev.charCode || ev.charCode === 0) {
                    switch (ev.keyCode) {
                        case 13: //ENTER
                            term.off("key", onTerminalData);
                            term.write("\r\n");
                            deferred.resolve(response.join(""));
                            break;
                        case 8: //BACKSPACE
                            if (caret > 0) {
                                response.splice(--caret, 1);
                                //remove the current line 
                                writeToTerm("\x1b[D"); //Move cursor back one position
                                writeToTerm("\x1b[K"); //delete the line from current position to the end of the line
                                var slicedResponse = response.slice(caret, response.length).join("");
                                writeToTerm(slicedResponse);
                                //restore the cursor
                                var i;
                                for (i = 0; i < slicedResponse.length; i++) {
                                    writeToTerm("\x1b[D"); //Move cursor back one position
                                }
                            }
                            break;
                        case 46: //DELETE
                            response.splice(caret, 1);
                            writeToTerm("\x1b[1P");
                            break;
                        case 37: //LEFT ARROW
                            if (caret > 0) {
                                caret--;
                                writeToTerm(key);
                            }
                            break;
                        case 39: //RIGHT ARROW
                            if (response.length > caret) {
                                caret++;
                                writeToTerm(key);
                            }
                            break;
                        default:
                            if (printable && key.length === 1) { //ignore unhandled escape codes
                                response.splice(caret++, 1, key); //replace element at the current position
                                writeToTerm(key);
                            }
                            break;

                    }
                }
                else {
                    if (printable && key.length === 1) { //ignore unhandled escape codes
                        response.splice(caret++, 1, key); //replace element at the current position
                        writeToTerm(key);
                    }
                }

            }
            term.write(prompt);
            term.on("key", onTerminalData);
        } catch (e) {
            deferred.reject(e);
        }
        return deferred.promise();
    }
})(jQuery);
function Retry(options) {
    this.states = {
        stopped: 0,
        running: 1,
        scheduled: 2
    };
    this._init(options);
}

Retry.prototype._init = function (options) {
    this._options = options || {
        timeout: 2 * 1000,
        retries: 10
    };
    this.deferred = null;
    this.attempts = 0;
    this._fn = undefined;
    this.timeoutFn = undefined;
    this.cancelRequested = false;
    this.status = this.states.stopped;
};
Retry.prototype.attempt = function (fn, scope) {
    var self = this;
    var deferred = $.Deferred();
    scope = scope || self;
    self._fn = fn.bind(scope);
    self.deferred = deferred;
    self.isRunning = true;
    self.timeoutFn = setTimeout(self._run.bind(self));
    return deferred.promise();
};
Retry.prototype._run = function () {
    var self = this;
    self.attempts = self.attempts + 1;
    var options = self._options;
    self.status = self.states.running;
    if (self.cancelRequested) {
        self.status = self.states.stopped;
        self.deferred.reject();
        self._init();
        return;
    }
    this._fn()
        .done(function () {
            self.status = self.states.stopped;
            self.deferred.resolve.apply(this, Array.prototype.slice.call(arguments));
            self._init();
        })
        .fail(function () {
            console.log(self.attempts + " >= " + options.retries);
            if (self.attempts >= options.retries) {
                self.status = self.states.stopped;
                self.deferred.reject.apply(this, Array.prototype.slice.call(arguments));
                self._init();
            } else {
                self.status = self.states.scheduled;
                self.timeoutFn = setTimeout(self._run.bind(self), options.timeout);
            }
        });
};
Retry.prototype.cancel = function () {
    if (this.status !== this.states.stopped) {
        this.cancelRequested = true;
    } else {
        console.log("retry method is not running");
    }
};

function TermWindow(hostSettings, parentElem) {
    this.hostSettings = hostSettings;
    this.parentElem = parentElem;
}

TermWindow.prototype.init = function () {
    //Terminal inits
    var self = this;
    //event emitter uses jquery events on object
    self.emit = $({});
    self.termMask = new Ext42.LoadMask(Ext42.getBody(), { msg: "Resuming session" });
    self.termMask.hide();

    self.termContainer = $(".term-container");
    self.sshHub = $.connection.SSHHub;
    self.hub = $.connection.hub;
    self.hub.url = "@{R=Toolset.Startup;K=SignalrPath;E=js}";

    self.isNetworkProblem = false;

    self.Log = SW.Toolset.Log;
    self.Log.LogLevel = SW.Toolset.LogLevelEnum.all;

    self.connectionStates = {
        Disconnected: 0,
        Connecting: 1,
        Connected: 2,
        Disconnecting: 3
    };
    self.terminalFaultEvents = {
        SSH: 0,
        Unknown: 1
    };
    self.connectionStatesText = ["Disconnected", "Connecting", "Connected", "Disconnecting"];
    self.connectionStatus = self.connectionStates.Disconnected;

    self.terminalEventType = {
        Response: 0,
        ConnectionStatus: 1,
        Fault: 2
    };

    self.sessionTimeout = 600; //seconds


    self.hub.reconnecting(function () {
        self.emit.trigger("hub:reconnecting");
    });
    self.hub.disconnected(function () {
        self.emit.trigger("hub:disconnect");
    });
    self.hub.reconnected(function () {
        self.emit.trigger("hub:reconnected");
    });

    $.extend(self.sshHub.client, {
        receive: function (terminalEvents) {
            if (!$.isArray(terminalEvents)) {
                self.Log.debug("Recieved empty response");
                return;
            }
            $.each(terminalEvents, function (index, terminalEvent) {
                if (self.responseId >= terminalEvent.ID)
                    return;
                self.responseId = terminalEvent.ID;
                switch (terminalEvent.MessageType) {
                    case self.terminalEventType.Response:
                        self.term.write(terminalEvent.TextResponse);
                        self.term.focus();
                        break;
                    case self.terminalEventType.ConnectionStatus:
                        self.statusChanged.call(self, terminalEvent.OldState, terminalEvent.NewState);
                        break;
                    case self.terminalEventType.Fault:
                        self.onHubError.call(self, terminalEvent.Message);
                        break;
                    default:
                        break;
                }
            });
        },
        sendError: function (message, details) {
            self.onHubError.call(self, message);
            if (self.connectionStatus === self.connectionStates.Disconnected) {
                //Error happened before session is created, Reset the terminal
                self.initSesssion();
            }

        }
    });

    self.startHub()
        .done(function () {
            self.Log.debug("Signalr connected");
            self.onHubEvents();
        }).fail(function () {
            alert("Unable to connect to signalr,please reload the page.");
        });

    self.initSesssion();


};
TermWindow.prototype.initSesssion = function () {

    var self = this;
    self.clientId = null;
    self.requestId = 0;
    self.responseId = 0;
    self.resizePending = false;

    if (self.term) {
        self.term.destroy();
    }
    self.term = new Terminal({
        cols: self.hostSettings.cols,
        rows: self.hostSettings.rows,
        useStyle: true,
        cursorBlink: true,
        convertEol: true,
        screenKeys: true
    });
    self.term.open(self.parentElem);

    //resize events
    self.maximize();
    self.term.resize(self.hostSettings.cols, self.hostSettings.rows);
    function doneResizing() {
        //resize the terminal only after the windows resize is completed
        self.term.resize(self.hostSettings.cols, self.hostSettings.rows);
        if (self.connectionStatus === self.connectionStates.Connected) {
            self.resizePending = false;
            console.log("resize");
            self.sshHub.server.resize(self.clientId, self.term.rows, self.term.cols)
                .fail(function (error) {
                    self.Log.error("Unable to send resize request  :" + error);
                });
        } else if (self.connectionStatus === self.connectionStates.Connecting) {
            self.resizePending = true;
        }
    }

    var resizeTimeout;
    $(window).off("resize")
            .on("resize", (function () {
                if (self.minimize) {
                    self.minimize();
                    self.maximize();
                }
                clearTimeout(resizeTimeout);
                resizeTimeout = setTimeout(doneResizing, 500);
            }).bind(self));
    
    //close the session, incase of browser close
    window.onbeforeunload = function () {
        if (self.connectionStatus === self.connectionStates.Connected || self.connectionStatus === self.connectionStates.Connecting) {
                self.disconnect();
            }
        };
    self.connect();
}
TermWindow.prototype.onHubError = function (message) {
    Ext42.Msg.show({
        title: "Error",
        msg: message,
        buttons: Ext42.MessageBox.OK,
        icon: Ext42.Msg.ERROR
    });
}; //region states

TermWindow.prototype.startHub = function (options) {
    var self = this;
    var ops = options || {
        timeout: 30 * 1000,
        retries: 1
    };
    var retry = new Retry(ops);
    return retry.attempt(function () {
        return self.hub.start();
    }, self);

};

TermWindow.prototype.getCredentials = function () {
    var self = this,
        deferred = $.Deferred();

    var userNamePromise = self.term.prompt("login as:", true);
    userNamePromise.done(function (username) {
        var passwordPromise = self.term.prompt(username + "@" + self.hostSettings.hostName + " password:", false);
        passwordPromise.done(function (password) {

            deferred.resolve({
                'userName': username,
                'password': password
            });
        })
        .fail(function (e) {
            deferred.reject(e);
        });
    }).fail(function (e) {
        deferred.reject(e);
    });

    return deferred.promise();
}
TermWindow.prototype.connect = function () {
    var self = this;
    self.getCredentials().then(function (credentials) {
        var connectionDetails = {
            NodeId: self.hostSettings.NodeId,
            Username: credentials.userName,
            Password: credentials.password,
            SSHPort: self.hostSettings.port,
            terminalColumns: self.hostSettings.cols,
            terminalRows: self.hostSettings.rows
        };
        return self.sshHub.server.createSession(connectionDetails);
    }).done(function () {
        //assign signalrId as connection id for the first time
        self.clientId = self.hub.id;
        //self.requestId = 0;
        self.responseId = 0;

    }).fail(function (error) {
        self.Log.error("SSH Connection request could not be sent", error);
        alert("Connection request could not be sent");
    });
};
TermWindow.prototype.disconnect = function (evt) {
    var self = this;
    self.sshHub.server.endSession(self.clientId)
        .done(function () {
            self.Log.debug("SSH Disconnect request sent");
        })
        .fail(function (error) {
            self.Log.error("SSH Disconnect request failed", error);            
        });
};

TermWindow.prototype.statusChanged = function (oldStatus, newStatus) {
    var self = this;
    self.connectionStatus = newStatus;
    if (newStatus === self.connectionStates.Connecting) {
        self.onHubEventsForActiveSession();
    } else if (newStatus === self.connectionStates.Disconnected) {
        self.emit.trigger("session:disconnected");
        if (oldStatus === self.connectionStates.Connecting) {
            //issue while connecting to ssh server like  authentication failure donot clean up the terminal
            self.onHubEvents();
            self.onSessionDisconnected();
        } else {
            //remove session and close the popup
            self.sshHub.server.removeSession(self.clientId)
            .always(function () {
                window.close();
            });
        }

    } else if (newStatus === self.connectionStates.Connected) {
        self.emit.trigger("session:connected");
        self.onSessionConnected();
    }
};
TermWindow.prototype.newTermRequest = function (command) {
    this.requestId += 1;
    return {
        requestId: this.requestId + 1,
        command: command
    };
};

TermWindow.prototype.maximize = function () {
    var self = this,
        el = this.parentElem,
        root = document.documentElement,
        term = this.term;

    var x, y;
    var m = {
        cols: term.cols,
        rows: term.rows,
        left: el.offsetLeft,
        top: el.offsetTop,
        root: root.className
    };

    this.minimize = function () {
        delete this.minimize;

        el.style.left = m.left + "px";
        el.style.top = m.top + "px";
        el.style.width = "";
        el.style.height = "";
        term.element.style.width = "";
        term.element.style.height = "";
        el.style.boxSizing = "";
        root.className = m.root;
        self.hostSettings.cols = m.cols;
        self.hostSettings.rows = m.rows;
    };
    window.scrollTo(0, 0);

    x = root.clientWidth / term.element.offsetWidth;
    y = root.clientHeight / term.element.offsetHeight;
    x = (x * term.cols) | 0;
    y = (y * term.rows) | 0;

    self.hostSettings.cols = x;
    self.hostSettings.rows = y;

    el.style.left = "0px";
    el.style.top = "0px";
    el.style.width = "100%";
    el.style.height = "100%";
    term.element.style.width = "100%";
    term.element.style.height = "100%";
    el.style.boxSizing = "border-box";

    root.className = "maximized";

};
TermWindow.prototype.onSessionConnected = function () {
    var self = this;
    //clear the terminal screen 
    self.term.write("\x1b[1J");
    self.term.write("\x1b[2F");
    if (self.resizePending) {
        self.sshHub.server.resize(self.clientId, self.term.rows, self.term.cols)
               .fail(function (error) {
                   self.Log.error("Unable to send resize request  :" + error);
               });
        self.resizePending = false;
    }
    self.term.on("data", function (command) {
        //TODO:
        var request = {
            requestId: ++self.requestId,
            command: command
        };
        self.sshHub.server.receiveCommand(self.clientId, request)
            .done(function () {

            }).fail(function () {
                //TODO:To support resending messages incase of failures
                //self.termMask.show();
                //var retry = new Retry({
                //    timeout: 10 * 1000,
                //    retries: 10
                //});
                //retry.attempt(function () {
                //    //return self.sshHub.server.receiveCommand(self.clientId, request);
                //}).always(function () {
                //    self.termMask.hide();
                //}).done(function () {
                //    this.requestId += 1;
                //}).fail(function () {
                //    alert('Unable to send data after repeated trails');
                //});
            });
    });

    self.term.on("title", function (title) {
        document.title = title;
    });

};


TermWindow.prototype.onSessionDisconnected = function () {
    this.initSesssion();
};



TermWindow.prototype.onHubEvents = function () {
    var self = this;
    this.offHubEvents();
    self.emit.on("hub:disconnect", self.hubDisconnect.bind(self));
    self.emit.on("hub:reconnected", self.hubReconnected.bind(self));
    self.emit.on("hub:reconnecting", self.hubReconnecting.bind(self));
};

TermWindow.prototype.offHubEvents = function () {
    var self = this;
    self.emit.off("hub:disconnect");
    self.emit.off("hub:reconnect");
    self.emit.off("hub:reconnecting");
};
TermWindow.prototype.onHubEventsForActiveSession = function () {
    var self = this;
    this.offHubEvents();
    self.emit.on("hub:disconnect", self.hubDisconnectActiveSession.bind(self));
    self.emit.on("hub:reconnected", self.hubReconnectedActiveSession.bind(self));
    self.emit.on("hub:reconnecting", self.hubReconnectingActiveSession.bind(self));
}; //region signalr connection events before SSH connection
TermWindow.prototype.hubReconnecting = function () {
    this.isNetworkProblem = true;
};
TermWindow.prototype.hubReconnected = function () {
    this.isNetworkProblem = false;
};
TermWindow.prototype.hubDisconnect = function () {
    if (this.isNetworkProblem) {
        alert("@{R=SSH.Strings;K=SSHGUICODE_PS_01;E=js}");
    }
}; //endregion before SSH connection
//endregion states
TermWindow.prototype.hubReconnectingActiveSession = function () {
    this.isNetworkProblem = true;
    this.termMask.show();
    //TODO:tell any pending messages to be moved to queue;
};
TermWindow.prototype.hubReconnectedActiveSession = function () {
    this.isNetworkProblem = false;
    this.termMask.hide();
    //start processing any pending messages;
};
TermWindow.prototype.hubDisconnectActiveSession = function () {
    var self = this;
    if (!self.isNetworkProblem)
        return; //tell any pending messages to stop
    self.offHubEvents();
    self.startHub({
        timeout: 30 * 1000,
        retries: 15
    }).always(function () {
        self.termMask.hide();
    }).done(function () {

        self.onHubEventsForActiveSession();
        self.sshHub.server.resumeSession(self.clientId, self.responseId)
                    .fail(function (error) {
                        self.Log.error("Unable to resume session :" + error);
                    });
        //TODO:start processing any pending messages;
    }).fail(function () {
        var message = "Unable to resume the SSH session due to network issues, please reload the tool";
        self.onHubEvents();
        self.Log.error("Unable to resume the SSH session due to network issues");
        Ext42.Msg.show({
            title: "Error",
            msg: message,
            buttons: Ext42.MessageBox.OK,
            icon: Ext42.Msg.ERROR
        });
    });
};