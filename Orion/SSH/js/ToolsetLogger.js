﻿SW.Core.namespace('SW');
var ts = SW.Core.namespace('SW.Toolset');

ts.LogLevelEnum = 
{
    all: 0, 
    trace: 1,
    debug: 2,
    info: 3,
    warn: 4,
    error: 5,
    off: 100 
};

ts.debugging = 0; //you can change behavior of some features for dubugging purposes 
ts.LogLevel = ts.LogLevelEnum.info; //you can rule detail level of logging by this

SW.Toolset.Log = new (function () {
    var self = this;
    var pageLoadTime = new Date().getTime();
    var fmt = SW.Core.String.Format;
    var logLevelStrings = {
        "1": "TRACE",
        "2": "DEBUG",
        "3": "INFO",
        "4": "WARN",
        "5": "ERROR"
    };

    /** to support a fallback to 'console.log' old browsers */
    var console = window.console || {};
    console.log = console.log || function() {};
    console.debug = console.debug || console.log;
    console.info = console.info || console.debug;
    console.warn = console.warn || console.info;
    console.error = console.error || console.warn;

    var doLog = function(msg, logLevel) {
        if (logLevel === ts.LogLevelEnum.trace) return console.debug(msg);
        if (logLevel === ts.LogLevelEnum.debug) return console.debug(msg);
        if (logLevel === ts.LogLevelEnum.info) return console.info(msg);
        if (logLevel === ts.LogLevelEnum.warn) return console.warn(msg);
        if (logLevel === ts.LogLevelEnum.error) return console.error(msg);
    };
    self.pad = function (num, size) {
        var s = "000000000" + num;
        return s.substr(s.length - size);
    }
    self.diffDates = function(d1, d2) {
        var diff = d2 - d1, sign = diff < 0 ? -1 : 1, milliseconds, seconds, minutes, hours;
        diff /= sign; // or diff=Math.abs(diff);
        diff = (diff - (milliseconds = diff % 1000)) / 1000;
        diff = (diff - (seconds = diff % 60)) / 60;
        hours = (diff - (minutes = diff % 60)) / 60;
        return self.pad(hours, 2) + ':' + self.pad(minutes, 2)
             + ':' + self.pad(seconds, 2) + '.' + self.pad(milliseconds, 3);
    }
    self.log = function(msg, logLevel) {
        if (!logLevel) {
            logLevel = ts.LogLevelEnum.info;
        }
        var overrideLogLevel = msg[0] == '!';
        if (logLevel < ts.LogLevel && !overrideLogLevel)
            return this;
        var strTime = self.diffDates(pageLoadTime, new Date().getTime());
        if (overrideLogLevel) msg = msg.substring(1);
        doLog("[" + logLevelStrings[logLevel] + "][" + new Date().toLocaleTimeString() + ' | ' + strTime + "] " + msg, logLevel);
        return this;
    };

    self.trace = function (msg) {
        if (arguments.length > 1) {
            msg = fmt.apply(null, arguments);
        }
       return self.log(msg, ts.LogLevelEnum.trace);
    };
    self.debug = function (msg) {
        if (arguments.length > 1) {
            msg = fmt.apply(null, arguments);
        }
        return self.log(msg, ts.LogLevelEnum.debug);
    };
    self.info = function (msg) {
        if (arguments.length > 1) {
            msg = fmt.apply(null, arguments);
        }
        return self.log(msg, ts.LogLevelEnum.info);
    };
    self.warn = function (msg) {
        if (arguments.length > 1) {
            msg = fmt.apply(null, arguments);
        }
        return self.log(msg, ts.LogLevelEnum.warn);
        
    };
    self.error = function (msg) {
        if (arguments.length > 1) {
            msg = fmt.apply(null, arguments);
        }
        return self.log(msg, ts.LogLevelEnum.error);
    };
    return self;
})();

if (Object.freeze) {
    Object.freeze(ts.LogLevelEnum);
}
