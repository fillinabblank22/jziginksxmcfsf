﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SSH_Admin_EditViews_SSHClientEnabled : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    public override string PropertyValue { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        { 
            if (!String.IsNullOrEmpty(PropertyValue))
            {
                ctrSSHClientEnabled.SelectedValue = PropertyValue;
            }
        }
        PropertyValue = ctrSSHClientEnabled.SelectedValue;
    }
}