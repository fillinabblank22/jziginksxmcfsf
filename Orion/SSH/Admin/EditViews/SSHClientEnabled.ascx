﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SSHClientEnabled.ascx.cs" Inherits="Orion_SSH_Admin_EditViews_SSHClientEnabled" %>
<asp:RadioButtonList ID="ctrSSHClientEnabled" SelectionMode="Single" Rows="1" runat="server">
	<asp:ListItem Text="<%$ Resources: SSHWebContent, WEBDATA_SK_04 %>" Selected="True" Value="True"/>
	<asp:ListItem Text="<%$ Resources: SSHWebContent, WEBDATA_SK_05 %>" Value="False"/>
</asp:RadioButtonList>