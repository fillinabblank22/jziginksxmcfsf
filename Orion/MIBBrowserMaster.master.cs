﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_MIBBrowserMaster : System.Web.UI.MasterPage
{    
    internal bool _isOrionDemoMode = false;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

		bool isDemo = _isOrionDemoMode = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
		bool isAdmin = Profile.AllowAdmin;
		bool bypassAccessLimitation = this.Page is IBypassAccessLimitation;
		
        if (!isAdmin && !bypassAccessLimitation)
        {
            if(isDemo)
			{
				return;
			}			
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });
        }

        if ((CommonWebHelper.IsProductBlogDisabled && Request.Path.Equals("/Orion/Admin/ProductsBlog.aspx", StringComparison.InvariantCultureIgnoreCase))
            || (CommonWebHelper.IsMaintenanceRenewalsDisabled && Request.Path.Equals("/Orion/Admin/MaintenanceRenewals.aspx", StringComparison.InvariantCultureIgnoreCase)))
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_324) });
    }

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            string viewID = (Request["ViewID"] != null) ? Request["ViewID"].ToString() : null;
            string returnTo = (Request["ReturnTo"] != null) ? Request["ReturnTo"].ToString() : null;
            string accountID = (Request["AccountID"] != null) ? Request["AccountID"].ToString() : null;
            var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
            OrionSiteMapPath.SetUpRenderer(renderer);
        }
    }
}
