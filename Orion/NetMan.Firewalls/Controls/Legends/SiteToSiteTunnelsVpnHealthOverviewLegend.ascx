<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteToSiteTunnelsVpnHealthOverviewLegend.ascx.cs" Inherits="Orion_ASA_Controls_SiteToSiteTunnelsVpnHealthLegend" %>

<orion:Include ID="includeJsImplementation" File="NetMan.Firewalls/js/HealthChartLegend.js" runat="server" />

<script type="text/C#" runat="server">
    protected override void OnInit(EventArgs e)
    {
        var initString = string.Format(@"
            SW.Core.Charts.Legend.vpn_TunnelHealthLegendInitializer__{0} = function (chart) {{
                SW.ASA.Charts.HealthChartLegend.createStandardLegend(chart, '{0}', {{
                    objectLabel: '{1}',
                    baseCssClass: 'asa-vpn-tunnel-health',
                    iconUrl: '/Orion/NetMan.Firewalls/Images/StatusIcons/SiteToSiteTunnel_{{0}}.png'
                }});
            }};", legend.ClientID, Resources.NetManFirewallsWebContent.Vpn_HealthLegend_ObjectName);

        OrionInclude.CoreFile("/Charts/js/allcharts.js").AddJsInit(initString);
    }  
</script>

<div id="SiteToSiteTunnelHealthLegend" class="asa-vpn-tunnel-health-legend" runat="server">
    <div class="asa-vpn-tunnel-health-legend-inner">
        <table runat="server" id="legend" class="chartLegend asa-vpn-tunnel-health-status" ></table>
    </div>
</div>