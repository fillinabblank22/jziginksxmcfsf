using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_ASA_Controls_SiteToSiteTunnelsVpnHealthLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer => $"vpn_TunnelHealthLegendInitializer__{legend.ClientID}";

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}