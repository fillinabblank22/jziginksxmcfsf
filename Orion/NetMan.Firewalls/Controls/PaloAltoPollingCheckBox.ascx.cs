﻿using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.NetMan.Firewalls.Common.Dal;
using SolarWinds.Orion.NetMan.Firewalls.Web;
using SolarWinds.Orion.NetMan.Firewalls.Web.Credentials;
using SolarWinds.Orion.NetMan.Firewalls.Web.Models;
using SolarWinds.Orion.NetMan.Firewalls.Common.Credentials;
using SolarWinds.Orion.Web.Model.SharedCredentials;
using SolarWinds.Orion.NetMan.Firewalls.Web.Controllers;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using Resources;
using SolarWinds.Newtonsoft.Json;
using SolarWinds.Orion.Web.DAL;

/// <summary>
/// Control for Palo Alto Node management plugin. Handles behavior for both Add and Edit node plugins.
/// </summary>
public partial class OrionFirewallsControlsPaloAltoPollingCheckBox : UserControl, INodePropertyPlugin
{
    /// <summary>
    /// Name of Palo Alto plugin defined in SolarWinds.NetMan.Firewalls.plugin Config file
    /// </summary>
    private const string NodePropertyPluginName = "PaloAltoPollingCheckBox";
    private const string PollingEnabledSessionId = "PaloAltoCheckBox_PollingEnabled";
    private const string MultieditEnabledSessionId = "PaloAltoCheckBox_MultieditEnabled";
    private const string WasPollingEnabledSessionId = "PaloAltoCheckBox_WasPollingEnabled";

    private const string UsernameSessionid = "PaloAltoCredentials_Username";
    private const string PasswordSessionid = "PaloAltoCredentials_Password";
    private const string ApiKeySessionid = "PaloAltoCredentials_ApiKey";
    private const string InvalidCertificateKey = "PaloAltoCredentials_InvalidCertificatesAccepted";

    private readonly string[] unsupportedEngineTypes = { "RemoteCollector" };

    private IList<Node> relevantNodes;
    private NodePropertyPluginExecutionMode pluginExecutionMode;
    private Dictionary<string, object> propertyBag;
    private static readonly Log Log = new Log();

    private static readonly IEngineDAL EngineDal = new EngineDAL();
    private static readonly ICredentialsManagerFactory credentialsFactory = new CredentialsManagerFactory();
    private readonly CortexPollingController cortexPollingController;
    private static readonly OrionEntitySharedCredentialManager<UsernamePasswordCredentialWithContent> usernamePasswordCredentialManager = new OrionEntitySharedCredentialManager<UsernamePasswordCredentialWithContent>();

    private readonly Dictionary<int,int> orionEngineCache = new Dictionary<int, int>();

    public int NodePollingEngineId
    {
        get
        {
            return relevantNodes.First().EngineID;
        }
    }

    public int NodeOrionEngineId
    {
        get
        {
            return GetOrionEngineId(relevantNodes.First());
        }
    }

    private string EnteredUsername
    {
        get
        {
            if (!propertyBag.ContainsKey(UsernameSessionid))
            {
                propertyBag[UsernameSessionid] = string.Empty;
            }

            return (string)propertyBag[UsernameSessionid];
        }
        set { propertyBag[UsernameSessionid] = value; }
    }

    private string EnteredPassword
    {
        get
        {
            if (!propertyBag.ContainsKey(PasswordSessionid))
            {
                propertyBag[PasswordSessionid] = string.Empty;
            }

            return (string)propertyBag[PasswordSessionid];
        }
        set { propertyBag[PasswordSessionid] = value; }
    }

    private string ApiKey
    {
        get
        {
            if (!propertyBag.ContainsKey(ApiKeySessionid))
            {
                propertyBag[ApiKeySessionid] = string.Empty;
            }

            return (string)propertyBag[ApiKeySessionid];
        }
        set { propertyBag[ApiKeySessionid] = value; }
    }

    public bool PaloAltoPollingEnabled
    {
        get
        {
            return propertyBag.ContainsKey(PollingEnabledSessionId) && (bool)propertyBag[PollingEnabledSessionId];
        }

        set { propertyBag[PollingEnabledSessionId] = value; }
    }

    public bool PaloAltoMultieditEnabled
    {
        get
        {
            return propertyBag.ContainsKey(MultieditEnabledSessionId) && (bool)propertyBag[MultieditEnabledSessionId];
        }

        set { propertyBag[MultieditEnabledSessionId] = value; }
    }

    /// <summary>
    /// Determines whether Palo Alto polling was enabled (true) or disabled (false) on node when this control was loaded
    /// </summary>
    public bool InitialPaloAltoPollingEnabled
    {
        get
        {
            return propertyBag.ContainsKey(WasPollingEnabledSessionId) && (bool)propertyBag[WasPollingEnabledSessionId];
        }

        set { propertyBag[WasPollingEnabledSessionId] = value; }
    }

    private Dictionary<string, string> GetApiKeys()
    {
        Dictionary<string, string> apiKeyList;
        apiKeyList = String.IsNullOrEmpty(ApiKey) ? new Dictionary<string, string>() : JsonConvert.DeserializeObject<Dictionary<string, string>>(ApiKey);

        return apiKeyList;
    }

    private void UpdatePollingEnabled(bool pollingEnabled)
    {
        PollForPaloAltoChecked = pollingEnabled;
        PaloAltoPollingEnabled = pollingEnabled;
    }

    private void UpdateMultieditEnabled(bool pollingEnabled)
    {
        MultipleSelectionChecked = pollingEnabled;
        PaloAltoMultieditEnabled = pollingEnabled;
    }

    public bool PollForPaloAltoVisible
    {
        get
        {
            return cbIsPaloAltoPollingEnabled.Visible;
        }
        set
        {
            cbIsPaloAltoPollingEnabled.Visible = value;
            lPaloAltoPollingDescription.Visible = value;
        }
    }

    public bool PollForPaloAltoEnabled
    {
        get { return cbIsPaloAltoPollingEnabled.Enabled; }
        set { cbIsPaloAltoPollingEnabled.Enabled = value; }
    }

    public bool PollForPaloAltoChecked
    {
        get { return cbIsPaloAltoPollingEnabled.Checked; }
        set { cbIsPaloAltoPollingEnabled.Checked = value; }
    }

    public bool PaloAltoCredentialsSectionVisible
    {
        get { return PaloAltoCredentialsSection.Visible; }
        set { PaloAltoCredentialsSection.Visible = value; }
    }
    /// <summary>
    /// Defines if plugin "Enable multiple selection" checkbox is visible.
    /// </summary>
    public bool MultipleSelectionVisible
    {
        get { return cbMultipleSelection.Visible; }
        set { cbMultipleSelection.Visible = value; }
    }

    public bool MultipleSelectionChecked
    {
        get { return cbMultipleSelection.Checked; }
        set { cbMultipleSelection.Checked = value; }
    }

    public bool MultipleSelectionEnabled
    {
        get { return cbMultipleSelection.Enabled; }
        set { cbMultipleSelection.Enabled = value; }
    }

    public OrionFirewallsControlsPaloAltoPollingCheckBox()
    {
        cortexPollingController = new CortexPollingController(EngineDal, new SwisNodeSettingsDal(new SwisConnectionProxyFactory()));
    }

    public Dictionary<string, string> ListOfTrustedCertificateIdentities
    {
        get
        {
            var listOfTrustedCertificates = new Dictionary<string, string>();
            foreach (var node in relevantNodes)
            {
                if (node.NodeSettings.ContainsKey(InvalidCertificateKey))
                {
                    if (!listOfTrustedCertificates.ContainsKey(node.IpAddress))
                    {
                        listOfTrustedCertificates.Add(node.IpAddress, node.NodeSettings[InvalidCertificateKey]);
                    }
                    if (!string.IsNullOrEmpty(node.SysName) && !listOfTrustedCertificates.ContainsKey(node.SysName))
                    {
                        listOfTrustedCertificates.Add(node.SysName, node.NodeSettings[InvalidCertificateKey]);
                    }
                }
            }
            return listOfTrustedCertificates;
        }
    }

    public string ListOfTrustedCertificateIdentitiesForWeb
    {
        get
        {
            string result = JsonConvert.SerializeObject(ListOfTrustedCertificateIdentities);
            result = HttpUtility.HtmlEncode(result);
            return result;
        }
    }

    public bool AreAllNodesOnTheSameEngine
    {
        get
        {
            return relevantNodes.Select(x => x.EngineID).Distinct().Count() == 1;
        }
    }

    private Action SslCertificateApproval()
    {
        Action result = () =>
                        {
                            Dictionary<string, string> customData = JsonConvert.DeserializeObject<Dictionary<string, string>>(PaloAltoUsernamePasswordCredentialsControl.CustomData);
                            relevantNodes.Where(x => !string.IsNullOrEmpty(x.IpAddress) && customData.ContainsKey(x.IpAddress))
                                         .ToList()
                                         .ForEach(node => node.NodeSettings[InvalidCertificateKey] = customData[node.IpAddress]);
                            relevantNodes.Where(x => !string.IsNullOrEmpty(x.SysName) && customData.ContainsKey(x.SysName))
                                         .ToList()
                                         .ForEach(node => node.NodeSettings[InvalidCertificateKey] = customData[node.SysName]);
                        };
        return result;
    }

    public void Page_Load(object sender, EventArgs e)
    {
        PaloAltoUsernamePasswordCredentialsControl.RegisterAction(FirewallCredentialsValidationController.SSL_INVALID_CERTIFICATE_ACTION, SslCertificateApproval());
    }

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        Log.Debug("Initializing Palo Alto 'Node management' plugin.");

        relevantNodes = nodes;
        pluginExecutionMode = mode;
        propertyBag = pluginState;

        if (!IsPostBack)
        {
            // This is for add node wizard, so it will 'remember' credentials between add node wizard pages.
            UpdatePollingEnabled(PaloAltoPollingEnabled);
            UpdateMultieditEnabled(PaloAltoMultieditEnabled);
            PollForPaloAltoEnabled = MultipleSelectionChecked;
            PaloAltoUsernamePasswordCredentialsControl.UserName = EnteredUsername;
            PaloAltoUsernamePasswordCredentialsControl.Password = EnteredPassword;
            cbMultipleSelection.Visible = false;
            PaloAltoDifferentEnginesWarning.Visible = !AreAllNodesOnTheSameEngine;

            switch (mode)
            {
                case NodePropertyPluginExecutionMode.WizDefineNode:
                    InitializeAddNodePlugin();
                    break;
                case NodePropertyPluginExecutionMode.EditProperies:
                    InitializeEditNodePlugin();
                    break;
                default:
                    Log.Debug($"Initialization of Palo Alto 'Node management' plugin for '{mode}' execution mode is not defined.");
                    break;
            }
        }
        else
        {
            EnteredUsername = PaloAltoUsernamePasswordCredentialsControl.UserName;
            EnteredPassword = PaloAltoUsernamePasswordCredentialsControl.Password;
        }

        Log.Debug("Palo Alto 'Node management' plugin initialized.");
    }

    private void InitializeAddNodePlugin()
    {
        Log.Debug("Initializing Palo Alto 'Add node' plugin.");

        UpdatePollingEnabled(true);
        UpdateMultieditEnabled(true);
        PollForPaloAltoVisible = false;
        MultipleSelectionVisible = false;
        PaloAltoDifferentEnginesWarning.Visible = false;

        Log.Debug("Palo Alto 'Add node' plugin initialized.");
    }

    private void InitializeEditNodePlugin()
    {
        if (relevantNodes != null)
        {
            Log.Debug("Initializing Palo Alto 'Edit node' plugin.");

            var editNodePlugin = NodePropertyPluginManager.Plugins
                                                          .FirstOrDefault(p => p.Name == NodePropertyPluginName);

            if (editNodePlugin != null)
            {
                SetUpEditNodePage(editNodePlugin);
            }
            else
            {
                throw new InvalidOperationException($"No 'Edit node' plugin named {NodePropertyPluginName} found.");
            }

            Log.Debug("Palo Alto 'Edit node' plugin initialized.");
        }
    }

    private void SetUpEditNodePage(NodePropertyPluginWrapper editNodePlugin)
    {
        if (unsupportedEngineTypes.Contains(GetEngineType()))
        {
            editNodePlugin.Visible = false;
        }
        else if (AreAllNodesOnTheSameEngine)
        {
            PaloAltoDifferentEnginesWarning.Visible = false;
            bool multipleNodes = relevantNodes.Count > 1;

            bool isPollingEnabled = false;
            string userNameToFill = string.Empty;
            string passwordToFill = string.Empty;

            editNodePlugin.Visible = true;
            PaloAltoPollingEnabled = true;

            MultipleSelectionVisible = multipleNodes;
            MultipleSelectionEnabled = multipleNodes;
            MultipleSelectionChecked = !multipleNodes;
            PollForPaloAltoEnabled = !multipleNodes;

            GetInitialData(ref userNameToFill, ref passwordToFill, ref isPollingEnabled);

            if (!multipleNodes || CheckThatAllCredentialsAreSame(ref userNameToFill, ref passwordToFill, ref isPollingEnabled))
            {
                cbIsPaloAltoPollingEnabled.Checked = isPollingEnabled;
                cbIsPaloAltoPollingEnabled_CheckedChanged(this, EventArgs.Empty);
                InitialPaloAltoPollingEnabled = isPollingEnabled;
                SetCredentials(userNameToFill, passwordToFill);
            }
            else if (multipleNodes)
            {
                cbIsPaloAltoPollingEnabled.Checked = false;
                cbIsPaloAltoPollingEnabled_CheckedChanged(this, EventArgs.Empty);
                PaloAltoCredentialsSectionMultipleNodesWarning.Visible = true;
                InitialPaloAltoPollingEnabled = false;
            }
        }
        else
        {
            editNodePlugin.Visible = true;
            MultipleSelectionVisible = true;
            MultipleSelectionChecked = false;
            MultipleSelectionEnabled = false;
            PollForPaloAltoVisible = false;
            PaloAltoDifferentEnginesWarning.Visible = true;
            PaloAltoCredentialsSectionVisible = false;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    public bool Update()
    {
        if (MultipleSelectionChecked)
        {
            var apiKeyList = GetApiKeys();
            switch (pluginExecutionMode)
            {
                case NodePropertyPluginExecutionMode.WizChangeProperties: // Add node last step
                    if (PaloAltoPollingEnabled)
                    {
                        var currentNode = NodeWorkflowHelper.Node;
                        UpdatePolling(currentNode);

                        var engineId = GetOrionEngineId(currentNode);
                        PaloAltoCredentialsManager.CreateForEngine(engineId, EngineDal)
                                                  .AssignCredentialsToNode(EnteredUsername,
                                                                           EnteredPassword,
                                                                           apiKeyList[currentNode.IpAddress],
                                                                           currentNode.Id);
                    }
                    return true;

                case NodePropertyPluginExecutionMode.EditProperies:
                    if (relevantNodes != null)
                    {
                        foreach (var node in relevantNodes)
                        {
                            if (PaloAltoPollingStateChanged())
                            {
                                UpdatePolling(node);
                            }

                            var apiKey = apiKeyList.ContainsKey(node.IpAddress) ? apiKeyList[node.IpAddress] : string.Empty;
                            HandleCredentials(node, apiKey);
                        }
                    }
                    return true;

                default:
                    return true;
            }
        }
        return true;
    }

    private void HandleCredentials(Node node, string apikey)
    {
        var paloAltoCredentialsManager = PaloAltoCredentialsManager.CreateForEngine(NodeOrionEngineId, EngineDal);

        if (PaloAltoPollingStateChanged())
        {
            HandleCredentialsWhenPollingStateChanged(node, apikey, paloAltoCredentialsManager);
        }
        else
        {
            HandleCredentialsWhenPollingStateNotChanged(node, apikey, paloAltoCredentialsManager);
        }
    }

    private bool PaloAltoPollingStateChanged() => PaloAltoPollingEnabled != InitialPaloAltoPollingEnabled;

    private void HandleCredentialsWhenPollingStateChanged(Node node, string apikey, ICredentialsManager paloAltoCredentialsManager)
    {
        if (PaloAltoPollingEnabled)
        {
            AssignCredentials(node, apikey, paloAltoCredentialsManager);
        }
        else
        {
            RemoveCredentials(node, paloAltoCredentialsManager);
        }
    }

    private void AssignCredentials(Node node, string apikey, ICredentialsManager paloAltoCredentialsManager)
    {
        if (!string.IsNullOrEmpty(apikey))
        {
            Log.Info($"PaloAlto polling has been enabled on node [{node.Id}]");
            paloAltoCredentialsManager.AssignCredentialsToNode(EnteredUsername, EnteredPassword, apikey, node.Id);
            var trustedCertificateIdentity = GetTrustedCertificateIdentity(node);
            paloAltoCredentialsManager.SetNodeSettings(node.Id, InvalidCertificateKey, trustedCertificateIdentity);
            Log.Debug($"PaloAlto polling has been enabled on node [{node.Id}], credentials assigned.");
        }
        else
        {
            Log.Error($"PaloAlto polling has been enabled on node [{node.Id}], but there is no API key. Credentials cannot be saved.");
        }
    }

    private void RemoveCredentials(Node node, ICredentialsManager paloAltoCredentialsManager)
    {
        Log.Info($"PaloAlto polling has been disabled on node [{node.Id}]");
        paloAltoCredentialsManager.DeleteCredentialsOnNode(node.Id);
        paloAltoCredentialsManager.SetNodeSettings(node.Id, InvalidCertificateKey, null);
        Log.Debug($"PaloAlto polling has been disabled on node [{node.Id}], credentials and accepted certificate removed.");
    }

    private void HandleCredentialsWhenPollingStateNotChanged(Node node, string apikey, ICredentialsManager paloAltoCredentialsManager)
    {
        if (PaloAltoPollingEnabled)
        {
            UpdateCredentials(node, apikey, paloAltoCredentialsManager);
        }
        //otherwise PaloAlto Polling remains disabled => no need to do any actions
    }

    private void UpdateCredentials(Node node, string apikey, ICredentialsManager paloAltoCredentialsManager)
    {
        if (!string.IsNullOrEmpty(apikey))
        {
            Log.Info($"PaloAlto polling is still enabled on node [{node.Id}]");
            paloAltoCredentialsManager.UpdateCredentialsOnNode(EnteredUsername, EnteredPassword, apikey, node.Id);
            var trustedCertificateIdentity = GetTrustedCertificateIdentity(node);
            paloAltoCredentialsManager.SetNodeSettings(node.Id, InvalidCertificateKey, trustedCertificateIdentity);
            Log.Debug($"PaloAlto polling is still enabled on node [{node.Id}], credentials updated.");
        }
        else
        {
            Log.Error($"PaloAlto polling is still enabled on node [{node.Id}], but there is no API key. Credentials cannot be saved.");
        }
    }

    /// <summary>
    /// Returns trusted certificate identity for a node. If there is no such identity, null is returned.
    /// </summary>
    private string GetTrustedCertificateIdentity(Node node)
    {
        return ListOfTrustedCertificateIdentities.ContainsKey(node.IpAddress)
            ? ListOfTrustedCertificateIdentities[node.IpAddress]
            : null;
    }

    public bool Validate()
    {
        if (PaloAltoPollingEnabled)
        {
            var sharedCredentials = new SharedCredential
            {
                Username = PaloAltoUsernamePasswordCredentialsControl.UserName,
                Password = PaloAltoUsernamePasswordCredentialsControl.Password
            };

            List<string> listOfHostOrIps = new List<string>();
            foreach (var node in relevantNodes)
            {
                listOfHostOrIps.Add(Regex.Replace(node.IpAddress, @"\s+", ""));
            }

            var sharedCredentialsWithIp = new CredentialsValidationData
            {
                HostOrIpAddressList = listOfHostOrIps,
                Credentials = sharedCredentials,
                EngineId = NodeOrionEngineId,
                FirewallType = CortexFirewallType.PaloAlto,
                TrustedCertificateIdentity = ListOfTrustedCertificateIdentities
            };

            var credentilValidationControler = new FirewallCredentialsValidationController(credentialsFactory);
            Dictionary<string,string> apiKeys;

            var result = credentilValidationControler.ValidateCredentials(sharedCredentialsWithIp, out apiKeys);

            if (result.ValidationResultState == ValidationResultState.Error)
            {
                throw new ArgumentException($"{result.Message.Replace(System.Environment.NewLine, " ")}");
            }
            if (result.ValidationResultState == ValidationResultState.Warning)
            {
                throw new ArgumentException($"{result.Message.Replace(System.Environment.NewLine, " ")}");
            }

            ApiKey = JsonConvert.SerializeObject(apiKeys);
            return true;
        }
        return true;
    }

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        PollForPaloAltoEnabled = MultipleSelectionChecked;
        PaloAltoPollingEnabled = PollForPaloAltoChecked && MultipleSelectionChecked;
        PaloAltoCredentialsSectionVisible = PaloAltoPollingEnabled;
    }

    protected void cbIsPaloAltoPollingEnabled_CheckedChanged(object sender, EventArgs e)
    {
        PaloAltoPollingEnabled = PollForPaloAltoChecked && MultipleSelectionChecked;
        PaloAltoCredentialsSectionVisible = PaloAltoPollingEnabled;
    }

    private bool CheckThatAllCredentialsAreSame(ref string userNameToFill, ref string passwordToFill, ref bool isPollingEnabled)
    {
        bool areAllCredentialsSame = true;

        foreach (var node in relevantNodes.Skip(1))
        {
            var credentials = GetPaloAltoCredentialsForNode(CredentialUsage.PaloAltoFirewall, node.Id, usernamePasswordCredentialManager);
            if (credentials?.Credential != null)
            {
                CheckIfCredentialsAreSame(credentials.Credential.Username, userNameToFill, ref areAllCredentialsSame);
                CheckIfCredentialsAreSame(credentials.Credential.Password, passwordToFill, ref areAllCredentialsSame);
            }

            if (IsPollingEnabledOnNode(node) != isPollingEnabled)
            {
                areAllCredentialsSame = false;
            }

            if (!areAllCredentialsSame)
            {
                return areAllCredentialsSame;
            }
        }
        return areAllCredentialsSame;
    }

    public void GetInitialData(ref string userNameToFill, ref string passwordToFill, ref bool isPollingEnabled)
    {
        var initialCredentials = GetPaloAltoCredentialsForNode(CredentialUsage.PaloAltoFirewall, relevantNodes.First().Id, usernamePasswordCredentialManager);
        if (initialCredentials?.Credential != null)
        {
            userNameToFill = initialCredentials.Credential.Username;
            passwordToFill = initialCredentials.Credential.Password;
        }

        isPollingEnabled = IsPollingEnabledOnNode(relevantNodes.First());
    }

    private void CheckIfCredentialsAreSame(string value, string referenceValue, ref bool match)
    {
        if (referenceValue != value)
        {
            match = false;
        }
    }

    private bool IsPollingEnabledOnNode(Node node)
    {
        return cortexPollingController.IsPollingEnabled(node, CortexFirewallType.PaloAlto);
    }

    private void UpdatePolling(Node node)
    {
        var apiKeyList = GetApiKeys();
        var apikey = apiKeyList.ContainsKey(node.IpAddress) ? apiKeyList[node.IpAddress] : string.Empty;

        cortexPollingController.SetPollingState(node, CortexFirewallType.PaloAlto, PaloAltoPollingEnabled && !string.IsNullOrEmpty(apikey));
    }


    private OrionEntityCredential<UsernamePasswordCredentialWithContent> GetPaloAltoCredentialsForNode(string use, int nodeId, OrionEntitySharedCredentialManager<UsernamePasswordCredentialWithContent> sharedCredentialManager)
    {
        try
        {
            var credentialsManager = PaloAltoCredentialsManager.CreateForEngine(NodeOrionEngineId, EngineDal);
            var nodeUri = credentialsManager.GetNodeSwisUri(nodeId);
            var credentials = sharedCredentialManager.GetCredentialsForEntity(use, nodeUri);
            return credentials;
        }
        catch (Exception e)
        {
            Log.Error($"Failed to load credentials for node with ID='{nodeId}'", e);
        }

        return null;
    }

    private void SetCredentials(string username, string password)
    {
        PaloAltoUsernamePasswordCredentialsControl.UserName = username;
        PaloAltoUsernamePasswordCredentialsControl.Password = password;
    }

    private string GetEngineType()
    {
        return EngineDal.GetEngine(NodePollingEngineId).ServerType;
    }

    private int GetOrionEngineId(Node node)
    {
        var nodeEngineId = node.EngineID;

        int effectingEngineId;
        if (orionEngineCache.TryGetValue(nodeEngineId, out effectingEngineId))
        {
            return effectingEngineId;
        }

        effectingEngineId = EngineDal.GetOrionEngine(nodeEngineId).EngineID;
        orionEngineCache[nodeEngineId] = effectingEngineId;
        return effectingEngineId;
    }
}