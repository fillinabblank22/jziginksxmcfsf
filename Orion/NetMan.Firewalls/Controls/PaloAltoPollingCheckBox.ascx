﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PaloAltoPollingCheckBox.ascx.cs" Inherits="OrionFirewallsControlsPaloAltoPollingCheckBox" %>
<%@ Register Src="~/Orion/Controls/UsernamePasswordCredentialsControl.ascx" TagPrefix="orion" TagName="UsernamePasswordCredentialsControl" %>
<%@ Import Namespace="SolarWinds.Orion.NetMan.Firewalls.Common.Credentials" %>

<div id="PaloAltoCredentialsNodeProperties" data-engine-id="<%= NodeOrionEngineId %>" data-trusted-certificate-identity="<%= ListOfTrustedCertificateIdentitiesForWeb %>" style="display: none;"></div>

<div class="contentBlock">
    <div class="contentBlockHeader" runat="server" id="PaloAltoPollingCheckBoxHeader">
        <table>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
                    <asp:Literal ID="lPaloAltoPolling" runat="server" Text="<%$ Resources:NetManFirewallsWebContent,EditNode_PaloAltoPolling_Header%>" />
                </td>
                <td class="rightInputColumn">
                    <asp:CheckBox ID="cbIsPaloAltoPollingEnabled" runat="server" AutoPostBack="True" OnCheckedChanged="cbIsPaloAltoPollingEnabled_CheckedChanged" Enabled="false" />
                    <asp:Literal ID ="lPaloAltoPollingDescription" runat="server" Text="<%$ Resources:NetManFirewallsWebContent,EditNode_PaloAltoPolling_Description%>" />
                </td>
            </tr>
        </table>
        <div runat="server" id="PaloAltoDifferentEnginesWarning" class="sw-suggestion sw-suggestion-warning">
            <span id="PaloAltoMultipleCredentialsWarningIcon" class="sw-suggestion-icon"></span>
            <span id="PaloAltoMultipleCredentialsWarningText" style="white-space: pre-line"><%= Resources.NetManFirewallsWebContent.WarningMessage_EditNodede_DifferentEngines %></span>
        </div>
    </div>
    
    <div id="PaloAltoCredentialsSection" runat="server" class="blueBox">
        <div>
            <orion:UsernamePasswordCredentialsControl
                runat="server"
                CredentialRelationUse="<%# CredentialUsage.PaloAltoFirewall %>"
                ID="PaloAltoUsernamePasswordCredentialsControl"
                HideFailedTestDetailsLink="True"
                JavaScriptTestCredentialsHandler="function(credentials, callback) 
                {
                    var hostOrIpInputBox = document.querySelector('[id*=txtHostNameIP]') ||
                                           document.querySelector('[id*=pollingIP]') ||
                                           document.querySelector('[id*=NodeNameIP_tbIP]') ||
                                           document.querySelector('[id*=blNodes]');
                    
                    if (hostOrIpInputBox === null) {
                        throw 'Host or IP address input box not found.';
                    }
                    
                    var hostOrIpAddressList = [];
                    var hostOrIpInputBoxList = hostOrIpInputBox.querySelectorAll('li');
                    
                    if(hostOrIpInputBoxList.length &gt; 0) {
                        for(var i = 0; i &lt; hostOrIpInputBoxList.length; i++ ) {
                            var hostOrIp = hostOrIpInputBoxList[i].textContent;
                            hostOrIpAddressList.push(hostOrIp.replace(/\s/g, ''));
                        }
                    }
                    
                    if(hostOrIpAddressList.length &lt; 1) {
                        var hostOrIp2 = hostOrIpInputBox.value ? hostOrIpInputBox.value : hostOrIpInputBox.innerHTML;
                        hostOrIpAddressList.push(hostOrIp2.replace(/\s/g, ''));
                    }
                
                    var engineIdSelectBox = document.querySelector('[id*=ddlPollingEngine]');

                    var engineId;
                    
                    var nodeProperties = $('#PaloAltoCredentialsNodeProperties');

                    // Engine select box is displayed only in first phase of Add Node Wizard if user has some APs
                    if (engineIdSelectBox === null)
                    {
                        engineId = nodeProperties.data('engineId'); 
                    } else {
                        var selectedOptionIndex = engineIdSelectBox.selectedIndex;
                        engineId = engineIdSelectBox.options[selectedOptionIndex].value;
                    }
                
                    var trustedCertificateIdentity = nodeProperties.data('trustedCertificateIdentity');
                    
                    SW.Core.Services.callControllerAction('/sapi/FirewallCredentialsValidation/',
                                                            'ValidateCredentialsForEngine',
                                                            { 
                                                                Credentials: credentials, 
                                                                HostOrIpAddressList: hostOrIpAddressList,
                                                                EngineId: engineId,
                                                                FirewallType: 1, 
                                                                TrustedCertificateIdentity: trustedCertificateIdentity
                                                            },
                                                            callback,
                                                            console.error);
                }"
            />
            <table runat="server" id="PaloAltoCredentialsSectionMultipleNodesWarning" class="usernamePasswordCredentials"  visible="false">
                <tr>
                    <td class="leftLabelColumn">
                    </td>
                    <td class="rightInputColumn">
                        <div runat="server" id="PaloAltoMultipleCredentialsWarning" class="sw-suggestion sw-suggestion-warning">
                            <span id="PaloAltoMultipleCredentialsWarningIcon" class="sw-suggestion-icon"></span>
                            <span id="PaloAltoMultipleCredentialsWarningText" style="white-space: pre-line"><%= Resources.NetManFirewallsWebContent.EditNode_PaloAltoPolling_MultieditWarning %></span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>