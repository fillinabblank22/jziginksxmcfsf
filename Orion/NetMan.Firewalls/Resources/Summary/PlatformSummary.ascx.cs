﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.NetMan.Firewalls.Common.Enums;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.NetMan.Firewalls.Common.Helpers.Inventory;

public partial class Orion_ASA_Resources_PlatformSummary : BaseResourceControl, INodeProvider
{
    protected string HighAvailabilityOverallStatusName;
    protected OBJECT_STATUS HighAvailabilityOverallStatus;

    private readonly AsaInventoryHelper asaInventoryHelper = new AsaInventoryHelper();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        Visible = ShouldDisplayResource();
        LoadData();
    }

    private bool ShouldDisplayResource()
    {
        return asaInventoryHelper.IsItAsa(Node.Description);
    }

    private void LoadData()
    {
        LoadHardwareHealthData();
        LoadHighAvailabilityData();
    }

    private void LoadHardwareHealthData()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string query = @"
SELECT Status,count(Status) AS cnt,StatusName
    FROM Orion.HardwareHealth.HardwareItem
    LEFT JOIN Orion.StatusInfo si ON Status=si.StatusId
    WHERE NodeID=@nodeId AND IsDisabled=FALSE AND IsDeleted=FALSE
    GROUP BY Status,StatusName,UiOrder
    ORDER BY UiOrder";

            HwhOverviewRepeater.DataSource = swis.Query(query, new Dictionary<string, object> { { "nodeId", Node.NodeID } });
            HwhOverviewRepeater.DataBind();
        }

        if (HwhOverviewRepeater.Items.Count == 0)
        {
            HwhOverviewRow.Visible = false;
        }
    }

    private void LoadHighAvailabilityData()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string query = @"
    SELECT HAOverallState, ShortDescription, HAMode FROM Orion.ASA.Node 
    LEFT JOIN Orion.StatusInfo si ON HAOverallState=si.StatusId
    WHERE NodeID=@nodeId";

            var data = swis.Query(query, new Dictionary<string, object> { { "nodeId", Node.NodeID } });
            var row = data.Rows[0];
            var highAvailabilityMode = row.Field<HighAvailabilityMode>("HAMode");
            
            if (highAvailabilityMode == HighAvailabilityMode.NotConfigured)
            {
                InitHighAvailabilityControls(row, OBJECT_STATUS.Unknown, Resources.NetManFirewallsWebContent.HighAvailability_Unknown + ",", 
                    HelpHelper.GetHelpUrl(HelpLinkFragment), Resources.NetManFirewallsWebContent.HighAvailability_LearnMore, true);
            }
            else
            {
                InitHighAvailabilityControls(row, (OBJECT_STATUS) row.Field<byte>("HAOverallState"), row.Field<string>("ShortDescription") + ";", 
                    $"/Orion/View.aspx?NetObject={Node.NetObjectID}&SubViewKey=ASA Platform Subview", Resources.NetManFirewallsWebContent.HighAvailability_SeeDetails, false);
            }
        }   
    }

    protected void InitHighAvailabilityControls(DataRow row, OBJECT_STATUS status, string statusName, string linkUrl, string linkText, bool informationMessageVisible)
    {
        HighAvailabilityOverallStatus = status;
        HighAvailabilityOverallStatusName = statusName;
        HaOverallStatusLink.NavigateUrl = linkUrl;
        HaOverallStatusLink.Text = linkText;
        InformationMessage.Visible = informationMessageVisible;
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            yield return typeof(INodeProvider);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public Node Node => GetInterfaceInstance<INodeProvider>()?.Node;

    protected override string DefaultTitle => Resources.NetManFirewallsWebContent.ResourceTitle_PlatformSummary;

    public override string HelpLinkFragment => "orionphasa_platformsummary";
}