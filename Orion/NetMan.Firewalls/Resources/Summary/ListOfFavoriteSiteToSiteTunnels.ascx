﻿<%@ control language="C#" autoeventwireup="true" codefile="ListOfFavoriteSiteToSiteTunnels.ascx.cs" inherits="Orion_ASA_Resources_ListOfFavoriteSiteToSiteTunnels" %>
<%@ register tagprefix="orion" tagname="CustomQueryTable" src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:Include ID="Include1" runat="server" File="NetMan.Firewalls/Styles/VpnTunnels.css" /> 
<orion:Include ID="Include2" runat="server" File="NetMan.Firewalls/js/Formatters.js" />
<orion:Include ID="Include3" runat="server" File="NetMan.Firewalls/js/DurationHelper.js" />

<orion:resourcewrapper runat="server" id="Wrapper">
    <Content>       
        <orion:CustomQueryTable runat="server" ID="CustomTable" />       

        <script type="text/javascript">
            $(function () {              
                var query = SW.Core.Resources.CustomQuery;
                var resourceId = <%= ScriptFriendlyResourceID %>;

                var formatBps = function(value) {                    
                    return (typeof(value) == "number") ? SW.ASA.Formatters.formatWithUnit(value, 2, "bps") : "-";
                };

                var formatFailPhase = function(value) {
                    return String.format("<%= Resources.NetManFirewallsWebContent.FavoriteVpn_FailPhaseSentence %>", value);
                };

                var tableSettings = {
                    uniqueId: resourceId,
                    initialPage: 0,
                    allowSort: false,
                    allowPaging: false,
                    columnSettings: {
                        "TargetHostname": {
                            header: "<%= Resources.NetManFirewallsWebContent.FavoriteVpn_TunnelHeader %>",
                            headerCssClass: "asa-vpn-tunnel-first-column",
                            formatter: function (value, row, cellInfo) {
								var orionStatus = row[4];
								var orionStatusName = row[5];
								var statusIcon = String.format("<img src='/Orion/StatusIcon.ashx?entity=Orion.VPN.L2LTunnel&status={0}&size=small' title='{1}' />", orionStatus, orionStatusName);
								var caption = "→ " + (value ? value : row[6]);
								return "<span class=\"asa-vpn-tunnel-status-img\">" + statusIcon + "</span> <span class=\"asa-vpn-tunnel-text\">" + caption + "</span>";
							},
                            isHtml: true
                        },
                        "InBitsPerSecond": {
                            header: "<%= Resources.NetManFirewallsWebContent.FavoriteVpn_BandwidthInHeader %>",
							headerCssClass: "asa-vpn-tunnel-text",
                            formatter: function (value) {
                                return formatBps(value);
                            },
                            cellCssClassProvider: function() {
                                return 'asa-vpn-tunnel-text';
                            },
                            isHtml: true
                        },
                        "OutBitsPerSecond": {
                            header: "<%= Resources.NetManFirewallsWebContent.FavoriteVpn_BandwidthOutHeader %>",
							headerCssClass: "asa-vpn-tunnel-text",
                            formatter: function (value) {
                                return formatBps(value);
                            },
                            cellCssClassProvider: function() {
                                return 'asa-vpn-tunnel-text';
                            },
                            isHtml: true
                        },
                        "StartTime": {
                            header: "<%= Resources.NetManFirewallsWebContent.FavoriteVpn_DurationHeader %>",
							headerCssClass: "asa-vpn-tunnel-text",
                            cellCssClassProvider: function() {
                                return 'asa-vpn-tunnel-text';
                            },
                            formatter: function (value, row, cellInfo) {
								if(!row[3])
                                    return "-";
                                return SW.ASA.DurationHelper.utcToDuration(row[3], row[8]);
							}
                        },
                        "FailPhase": {
                            header: "<%= Resources.NetManFirewallsWebContent.FavoriteVpn_OtherHeader %>",
                            formatter: function (value) {
                                return value ? formatFailPhase(value) : String.empty;
                            },
                            cellCssClassProvider: function() {
                                return 'asa-vpn-tunnel-text-fail-phase';
                            }
                        }
                    }
                };

                query.initialize(tableSettings);

                var refresh = function () { query.refresh(resourceId); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID%>');
                refresh();
            });
          </script>
    </Content>
</orion:resourcewrapper>
