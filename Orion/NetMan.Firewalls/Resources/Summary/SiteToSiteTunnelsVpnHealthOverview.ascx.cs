﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.NetMan.Firewalls.Web.Vpn.SiteToSite;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_ASA_Resources_Summary_SiteToSiteTunnelsVpnHealthOverview : StandardChartResource, INodeProvider
{
    private ITunnelsDal tunnelsDal;

    private ITunnelsDal TunnelsDal => tunnelsDal ?? (tunnelsDal = new TunnelsDal());

    public Node Node => GetInterfaceInstance<INodeProvider>().Node;

    protected Boolean IsValidData
    {
        get
        {
            if (Node == null)
                return false;

            return TunnelsDal.GetTunnelsCountForNode(Node.NodeID) > 0;
        }
    }

    public override string EditURL => ExtendCustomEditUrl("/Orion/NetPerfMon/Resources/EditResource.aspx");
    protected override bool AllowCustomization => Profile.AllowCustomize;
    protected override string NetObjectPrefix => TunnelHealthResourceProperties.NetObjectType;
    protected override string DefaultTitle => Resources.NetManFirewallsWebContent.ResourceTitle_SiteToSiteVpnHealthOverview;
    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }
    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(INodeProvider) };

    protected override void OnInit(EventArgs e)
    {
        if (!IsValidData)
        {
            Visible = false;
        }

        base.OnInit(e);

        ChartWidth = Math.Max((int)(Resource.Width * 0.45), 230);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey(TunnelHealthResourceProperties.PropertyChartName))
        {
            Resource.Properties.Add(TunnelHealthResourceProperties.PropertyChartName, TunnelHealthResourceProperties.DefaultChartName);
        }

        HandleInit(WrapperContents);
    }

    protected override void SetAdditionalStyles(HtmlGenericControl chartPlaceHolder)
    {
        var chartWrapper = chartPlaceHolder.Parent as HtmlGenericControl;
        chartWrapper = chartWrapper ?? chartPlaceHolder;

        chartWrapper.Style["display"] = "inline-block";
        chartWrapper.Style["height"] = ChartResourceSettings.Height + "px";
        chartWrapper.Style["margin"] = "0 10px";
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();
        details["ResourceID"] = Resource.ID;
        details["NodeID"] = Node?.NodeID;

        return details;
    }

    protected override void AddChartExportControl(Control resourceWrapperContents)
    {
        // There is no need for export button
    }
}
