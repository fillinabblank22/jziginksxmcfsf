﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlatformSummary.ascx.cs" Inherits="Orion_ASA_Resources_PlatformSummary" %>
<%@ Import Namespace="Resources" %>

<orion:Include ID="Include" runat="server" File="NetMan.Firewalls/Styles/PlatformSummary.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>    
        <table class="NeedsZebraStripes AsaPlatformSummary">
            <tr runat="server" ID="HwhOverviewRow">
                <td class="PropertyHeader"><%= Resources.NetManFirewallsWebContent.HardwareHealth %></td>
                <td class="Property AsaHwhOverview">
                    <asp:Repeater ID="HwhOverviewRepeater" runat="server">
                        <ItemTemplate><span><img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&status=<%#Eval("Status")%>&size=small' alt='<%#Eval("StatusName")%>' title='<%#Eval("StatusName")%>'><%#Eval("cnt")%></span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr id="HaOverallStatusRow">
                <td class="PropertyHeader"><%= Resources.NetManFirewallsWebContent.HighAvailability %></td>
                <td class="Property AsaHaOverallStatus">
                    <img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&status=<%= (int)HighAvailabilityOverallStatus %>&size=small' alt='<%= HighAvailabilityOverallStatusName %>' title='<%=HighAvailabilityOverallStatusName%>'><%= HighAvailabilityOverallStatusName %><span runat="server" ID ="InformationMessage"><%= Resources.NetManFirewallsWebContent.HighAvailability_StatusNotAvailable %>;</span>
                    <asp:HyperLink runat="server" ID="HaOverallStatusLink"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:ResourceWrapper>