﻿using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

public partial class Orion_ASA_Resources_ListOfFavoriteSiteToSiteTunnels : ResourceControl, INodeProvider
{
    private const string nodeIdParameterName = "NodeID";
    private const string nodeIdSwqlParameterName = "@" + nodeIdParameterName;
    private const string dataQuery = @"
SELECT   
  l.TargetHostname,
  l.InBitsPerSecond,
  l.OutBitsPerSecond,
  l.StartTime,
  l.Status AS _Status,
  s.StatusName AS _StatusName,
  l.TargetIPAddress AS _TargetIPAddress,
  l.FailPhase,
  GETUTCDATE() as _ServerUtcTime
FROM Orion.VPN.L2LTunnel l
JOIN Orion.ASA.Favorite(nolock=true) af ON l.Uri = af.EntityUri
INNER JOIN Orion.StatusInfo s ON l.Status = s.StatusId
WHERE l.NodeID = " + nodeIdSwqlParameterName + @"
ORDER BY s.Ranking ASC, l.TargetHostname ASC";

    private const string dataExistsQuery = @"
SELECT TOP 1
  TunnelID
FROM Orion.VPN.L2LTunnel l
JOIN Orion.ASA.Favorite(nolock=true) af ON l.Uri = af.EntityUri
WHERE l.Node.NodeID = " + nodeIdSwqlParameterName;

    private const string viewIdQuery = "SELECT TOP 1 ViewID FROM Orion.Views WHERE ViewKey = 'VPN SiteToSite Subview'";

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NetManFirewallsWebContent.ResourceTitle_FavoriteSiteToSiteVpn; }
    }

    protected int ScriptFriendlyResourceID
    {   // to handle reporting which uses negative IDs
        get { return Math.Abs(Resource.ID); }
    }

    public String SWQL
    {
        get
        {
            return dataQuery;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = IsVisible();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            AddShowAllTunnelsButton(Node);
            CustomTable.SWQL = ApplyNodeIdToSwqlTemplate(SWQL, Node.NodeID);
            CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        }
    }

    private string ApplyNodeIdToSwqlTemplate(string swqlQuery, int nodeId)
    {
        return swqlQuery.Replace(nodeIdSwqlParameterName, nodeId.ToString(CultureInfo.InvariantCulture));
    }

    private void AddShowAllTunnelsButton(Node node)
    {
        int? viewID = GetSiteToSiteTunnelSubViewID();

        if (!viewID.HasValue)
        {
            // don't add button if target view does not exist.
            return;
        }

        Wrapper.ManageButtonText = Resources.NetManFirewallsWebContent.ManageLink_FavoriteSiteToSiteVpn;
        Wrapper.ManageButtonTarget = $"~/Orion/View.aspx?NetObject={node.NetObjectID}&ViewID={viewID}";
        Wrapper.ShowManageButton = true;
    }

    private int? GetSiteToSiteTunnelSubViewID()
    {
        int? viewId = null;
        var proxyCreator = SwisConnectionProxyPool.GetSystemCreator();
        using (var proxy = proxyCreator.Create())
        {
            var result = proxy.Query(viewIdQuery);
            viewId = result?.Rows?.Cast<DataRow>().FirstOrDefault()?.Field<int>(0);
        }

        return viewId;
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(INodeProvider); }
    }

    protected Boolean IsVisible()
    {
        return ExistsAnyFavoriteTunnel(Node.NodeID);
    }

    private bool ExistsAnyFavoriteTunnel(int nodeId)
    {
        var userName = Profile.UserName;
        var proxyCreator = SwisConnectionProxyPool.GetCreatorForUser(userName);       
        using (var proxy = proxyCreator.Create())
        {
            var queryParameters = new Dictionary<string, object>
            {
                [nodeIdParameterName] = Node.NodeID
            };
            DataTable results = proxy.Query(dataExistsQuery, queryParameters);
            return results?.Rows?.Count > 0;
        }

    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionphasa_favorites2svpn";
        }
    }

    public Node Node
    {
        get
        {
            var provider = GetInterfaceInstance<INodeProvider>();
            return provider.Node;
        }
    }
}