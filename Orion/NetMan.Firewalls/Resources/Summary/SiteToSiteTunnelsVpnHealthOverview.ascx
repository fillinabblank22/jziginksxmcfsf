﻿<%@ control Language="C#" AutoEventWireup="true" CodeFile="SiteToSiteTunnelsVpnHealthOverview.ascx.cs" Inherits="Orion_ASA_Resources_Summary_SiteToSiteTunnelsVpnHealthOverview" %>

<orion:Include ID="includeCss" File="NetMan.Firewalls/styles/VpnTunnels.css" runat="server" />

<orion:resourcewrapper runat="server" ID="Wrapper" CssClass="asa-vpn-tunnel-health">
	<Content>
	    <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
	</Content>
</orion:resourcewrapper>
