﻿SW.Core.namespace("SW.ASA.Charts").HealthChartLegend = (function () {
    var defaultSettings = {
        // prefixed before all css classes
        baseCssClass: "asa-vpn-health",
        // {0} will be replaced with "icon" parameter from data point
        iconUrl: null,
        // shows in the title above statuses
        objectLabel: "",
        // how many items in one row
        columns: 1,
        // optional - callback to create url to make statuses clickable
        urlConstructor: null,
        strings: {
            "count": "@{R=Orion.NetMan.Firewalls.Strings;K=HealthLegend_Count;E=js}"
        }
    };

    var createStatusIconString = function (point, settings, targetUrl) {
        var statusIconString = "<td class=\"" + settings["baseCssClass"] + "-status-icon\">";
        if (targetUrl) {
            statusIconString += "<a href=\"" + targetUrl + "\" target=\"\"_blank\"\">";
        }
        statusIconString += "<img src=\"" + String.format(settings["iconUrl"], point.icon) + "\"/>";
        if (targetUrl) {
            statusIconString += "</a>";
        }
        statusIconString += "</td>";

        return statusIconString;
    };

    var createStatusNameString = function(point, settings, targetUrl) {
        var statusNameString = "<td class=\"" + settings["baseCssClass"] + "-status-name\" style=\"white-space: nowrap;\">";
        if (targetUrl) {
            statusNameString += "<a href=\"" + targetUrl + "\" target=\"\"_blank\"\">";
        }
        statusNameString += point.legendLabel;
        if (targetUrl) {
            statusNameString += "</a>";
        }
        statusNameString += "</td>";

        return statusNameString;
    };

    var appendSummaryTitle = function(serie, table, settings) {
        var parentTypeLocalized = (settings["objectLabel"] ? settings["objectLabel"] + " " : "") + settings["strings"]["count"] + " ";

        var totalCount = serie.data.reduce(function(sum, point) {
            return sum + point.y;
        }, 0);

        var row = $("<tr><td colspan = 6 ><b> " + parentTypeLocalized + totalCount + "</b></td></tr>");
        row.appendTo(table);
        row = $("<tr><td colspan = 6 > </td></tr>");
        row.appendTo(table);
    };

    /**
     * Settings have to specify 
     * @param {any} chart
     * @param {any} legendContainerId
     * @param {any} settings
     */
    var createStandardLegend = function (chart, legendContainerId, settings) {
        settings = $.extend(defaultSettings, settings);

        var serie = chart.series[0];
        var table = $("#" + legendContainerId);
        var row = $("<tr />");

        // we need to clean the table, because of async refresh
        table.empty();
        if (serie.data.length === 0) {
            return;
        }

        appendSummaryTitle(serie, table, settings);
        
        for (var index = 1; index <= serie.data.length; index++) {
            var point = serie.data[index - 1];
            var targetUrl = null;

            if (typeof settings["urlConstructor"] === "function") {
                targetUrl = settings["urlConstructor"](point, settings, chart, legendContainerId);
            }

            var countString = "<td class=\"" + settings["baseCssClass"] + "-status-count-" + point.legendLabel + "\"><b>" + point.y + "</b></td>";
            $(countString).appendTo(row);

            if (settings["iconUrl"]) {
                var statusIconString = createStatusIconString(point, settings, targetUrl);
                $(statusIconString).appendTo(row);
            }

            var statusNameString = createStatusNameString(point, settings, targetUrl);
            $(statusNameString).appendTo(row);

            if (index % settings["columns"] === 0) {
                row.appendTo(table);
                row = $("<tr />");
            }
        }

        if (row.children().length) {
            row.appendTo(table);
        }
    };

    return {
        createStandardLegend: createStandardLegend
    };
}());
