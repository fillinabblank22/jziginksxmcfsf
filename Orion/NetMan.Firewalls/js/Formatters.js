﻿(function () {
    var unitPrefixes = {
        0: "",
        3: "k",
        6: "M",
        9: "G",
        12: "T",
        15: "P"
    };

    var unitBreakpoints = Object.keys(unitPrefixes);

    var formatWithUnit = function (value, decimals, unit) {
        var magnitude = Math.floor(Math.log(value) / Math.LN10);

        var appliedMagnitude = unitBreakpoints[0];
        for (var i = unitBreakpoints.length; i > 0; i--) {
            if (magnitude >= unitBreakpoints[i]) {
                appliedMagnitude = unitBreakpoints[i];
                break;
            }
        }

        value *= Math.pow(10, -appliedMagnitude);
        return String.format("<span>{0}</span>" +
            " <span class=\"asa-vpn-tunnel-text-bandwidth-unit\">{1}{2}</span>",
            value.toFixed(decimals), unitPrefixes[appliedMagnitude], unit);
    };

    SW.Core.namespace("SW.ASA").Formatters = {
        formatWithUnit: formatWithUnit
    };
}())
