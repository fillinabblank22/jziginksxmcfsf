﻿(function () {
	//override for default strings in localToTimeAgo function
	//these have removed 'ago' word to refer to duration rather than time passed since certain event
	var durationStrings = {
		justNow: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_JustNow;E=js}", // just now
		second: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Second;E=js}", // about a second
		seconds: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Seconds;E=js}", // {0} seconds
		minute: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Minute;E=js}", // about a minute
		minutes: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Minutes;E=js}", // {0} minutes
		hour: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Hour;E=js}", // about an hour
		hours: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Hours;E=js}", // {0} hours
		day: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Day;E=js}", // about a day
		days: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Days;E=js}", // {0} days
		month: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Month;E=js}", // about a month
		months: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Months;E=js}", // {0} months
		year: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Year;E=js}", // about a year
		years: "@{R=Orion.NetMan.Firewalls.Strings;K=FavoriteVpn_Duration_Years;E=js}" // {0} years
	};

    var utcToDuration = function (startUtcTime, serverUtcTime) {
        if (!startUtcTime)
            return null;
        if (!serverUtcTime)
            return null;
        return SW.Core.DateHelper.localToTimeAgo(startUtcTime, serverUtcTime, durationStrings);
    };

    SW.Core.namespace("SW.ASA").DurationHelper = {
        utcToDuration: utcToDuration
    };
}())
