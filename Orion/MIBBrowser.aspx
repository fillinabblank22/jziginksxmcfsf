<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/MIBBrowserMaster.master" CodeFile="MIBBrowser.aspx.cs" Inherits="Orion_MIBBrowser"  %>
<%@ Register Src="~/Orion/Controls/DynamicControlsContainer.ascx" TagPrefix="orion" TagName="notificationsContainer" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="4.2" />

    <orion:Include runat="server" File="OidPicker.js" />
    <orion:Include runat="server" File="OidPickerDefinitions.js" />
    <orion:Include runat="server" File="OidPicker.css" />
	<script type="text/javascript">
        SW.Core.namespace("SW.MIBBrowser").IsDemo = ("<%= OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") ? true : false;
    </script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="bodyContentPlaceholder" runat="Server">
    <style>
        #adminContent {
            min-height: 680px !important; /* compensate for fixed height content, stretch down at least as far as grid panels */
        }

        .mibBrowser-notifications > div {
            margin-bottom: 10px;
        }
    </style>
    <div id="gridPanel" style="max-width: 870px;max-height:600px">
        <div id="pickerPlaceHolder" />
        <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_79) %></h1>
        <orion:notificationsContainer runat="server" WebSettingsKey="MibBrowserNotificationControls" CssClasses="mibBrowser-notifications" />
		<div>
				<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OC0_01) %>
		</div>
		<br/>
    </div>
    <div style="clear: both;"></div>

    <div id="pickerResult"></div>
    <script type="text/javascript">
            var picker;
            var params = SW.Core.UriHelper.decodeURIParams();
            var nodeID;
            if (typeof params["NetObject"] === 'undefined') {
                nodeID = 1;

            }
            else{
                nodeID = params["NetObject"];
            }
            $(function() {
                SW.OidPicker.init({
                    selectionMode: 'single',
                    serviceUrl: 'Services/OidPicker.asmx',
                    wizardUrl: '/Orion/DeviceStudio/Services/Wizard.asmx',
                    nodeId: nodeID,
                    renderTo: 'pickerPlaceHolder'
                });
            });
    </script>
</asp:Content>