﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_ESI_Resources_AlertIntegration : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UniqueClientID = Resource.ID;
        SortablePageableTable.UniqueClientID = Resource.ID;

        ShowResource = IsIntegrationEnabled();

        NetObjectID = Request["NetObject"] ?? String.Empty;
    }

    private int ParseAlertObjectID(string id)
    {
        return Convert.ToInt32(id.Split(':').LastOrDefault());
    }

    private bool IsIntegrationEnabled()
    {
        const string query = "SELECT COUNT(InstanceID) AS [InstanceCount] FROM Orion.ESI.IncidentService";

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var table = swis.Query(query);
            return Convert.ToInt32(table.Rows[0]["InstanceCount"]) > 0;
        }
    }

    public int UniqueClientID { get; set; }

    public string NetObjectID { get; set; }

    public bool ShowResource { get; set; }

    protected override string DefaultTitle
    {
        get { return SolarWinds.SNI.Strings.Resources.AlertIntegration; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePHResourceServiceNowIncidents";
        }
    }
}