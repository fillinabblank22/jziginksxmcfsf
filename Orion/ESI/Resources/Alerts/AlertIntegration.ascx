﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertIntegration.ascx.cs" Inherits="Orion_ESI_Resources_AlertIntegration" %>
<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <span id="emptyTableMessage" style="display:none" class="ResourceContent" runat="server" automation="emptyTableMessage">
            <%= Resources.ESIWebContent.No_Incidents_For_Alert %>
        </span>
        <orion:SortablePageableTable ID="SortablePageableTable" runat="server" />
        

        <asp:Panel id="LoaderPanel" runat="server" HorizontalAlign="Center">
            <img alt="loader" src="/Orion/images/loading_gen_small.gif" />
        </asp:Panel>

        <input type="hidden" id="NetObjectID-<%= UniqueClientID %>" value="<%= NetObjectID %>" />
        <input type="hidden" id="OrderBy-<%= UniqueClientID %>" />
        <input type="hidden" id="ShowResource-<%= UniqueClientID %>" value="<%= ShowResource %>" />

        <script type="text/javascript">
            $(function () {

                var uniqueId = '<%= Resource.ID %>';
                var wrapper = $('[resourceid="<%= Resource.ID %>"]');
                var showResource = $('#ShowResource-' + uniqueId).val() === "True";
                if (!showResource) {
                    wrapper.hide();
                    return;
                }

                SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
                {
                    uniqueId: '<%= Resource.ID %>',
                    pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {

                        var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                        var netObjectId = $('#NetObjectID-' + uniqueId).val();

                        SW.Core.Services.callController("/api/AlertIntegration/GetIncidents", {
                            PageIndex: pageIndex,
                            PageSize: pageSize,
                            OrderByClause: currentOrderBy,
                            NetObject: netObjectId,
                            LimitationIds: [<%= string.Join(",", Limitation.GetCurrentListOfLimitationIDs().ToArray()) %>]
                        }, function (result) {
                            if (result.DataTable.Rows.length == 0) {
                                $('#<%= emptyTableMessage.ClientID %>').show();
                                $('#<%= SortablePageableTable.ClientID %>').hide();
                            }

                            $('#<%= LoaderPanel.ClientID %>').hide();

                            onSuccess(result);
                        }, function (errorResult) {

                            $('#<%= LoaderPanel.ClientID %>').hide();
                            onError(errorResult);
                        });

                    },
                    initialPage: 0,
                    rowsPerPage: '<%= Resource.Properties["RowsPerPage"] != null ? Convert.ToString(Resource.Properties["RowsPerPage"]) : "5" %>',
                    allowSort: true,
                        columnSettings: {
                        "IncidentType": {
                            caption: "<%= Resources.ESIWebContent.Incident_Type_Caption %>",
                            isHtml: false,
                            allowSort:true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                return cellValue;
                            }
                        },
                        "IncidentNumber": {
                            caption: "<%= Resources.ESIWebContent.Incident_Caption %>",
                            isHtml: true,
                            allowSort:true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                var a = '<a rel="noopener noreferrer nofollow" href="' + rowArray[2] + '" target="_blank" >' + cellValue + '</a>';
                                return a;
                            }
                        },
                        "IncidentState": {
                            caption: "<%= Resources.ESIWebContent.Status_Column_Caption %>",
                            isHtml: false,
                            allowSort: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                return cellValue;
                            }
                        },
                        "IncidentAssignee": {
                            caption: "<%= Resources.ESIWebContent.IncidentsResource_AssignedTo_Column_Caption %>",
                            isHtml: false,
                            allowSort: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                return cellValue;
                            }
                        },
                        "IncidentDescripton": {
                            caption: "<%= Resources.ESIWebContent.Description_Column_Caption %>",
                            isHtml: false,
                            allowSort: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                return cellValue;
                            }
                        }
                    },
                    pageSizeChanged: function(resourceId, newPageSize) {
                        SW.Core.Services.callController("/api/Resources/AddOrUpdateResourceProperty", {
                            resourceId: resourceId,
                            propertyName: "RowsPerPage",
                            propertyValue: newPageSize
                        });
                    }
                });

                var refresh = function () { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>