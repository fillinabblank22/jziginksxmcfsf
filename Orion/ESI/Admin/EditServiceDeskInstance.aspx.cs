using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Logging;
using System.Text;
using System.Net;
using System.Net.Sockets;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_ESI_Admin_EditServiceDeskInstance : System.Web.UI.Page
{
    public string InstanceId
    {
        get
        {
			if (!string.IsNullOrEmpty(Request.Params["instanceId"]))
                return WebSecurityHelper.SanitizeAngular(HttpUtility.HtmlEncode(Request.Params["instanceId"]));
			
            return Request.Params["instanceId"];
        }
    }
}