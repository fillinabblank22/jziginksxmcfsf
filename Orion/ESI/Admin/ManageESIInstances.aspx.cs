﻿using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Admin_ESI_ManageESIInstances : System.Web.UI.Page
{
    private static Log log = new Log();

    private const string DefaultColumnSelection = "Status,InstanceType,InstanceName,InstanceURL,OperationalState";
    private const string DefaulSortOrder = "InstanceType,Asc";

    protected void Page_Load(object sender, EventArgs e)
    {
        InstanceSuccessfullyCreated.Visible = IsNewInstanceCreated();

        ClientScript.RegisterStartupScript(GetType(), "storedValuesSetup", String.Format(
@"<script type=""text/javascript""> 
    $('#ManageESIInstances_SortOrder').val('{0}');
    $('#ManageESIInstances_SelectedColumns').val('{1}'); 
</script>",
                 WebUserSettingsDAL.Get("ManageESIInstances_SortOrder") ?? DefaulSortOrder,
                 WebUserSettingsDAL.Get("ManageESIInstances_SelectedColumns") ?? DefaultColumnSelection));

        Title = ESIWebContent.ManageInstances_PageTitle;
    }

    public bool IsNewInstanceCreated()
    {
        var newInstance = Session["NewIntegrationInstance"];
        Session["NewIntegrationInstance"] = null;
        return newInstance != null;
    }

    string GetIncidentCreationThreshold()
    {
        var thresholdValue = Resources.ESIWebContent.Value_Not_Available;
        try
        {
            using (var service = InformationServiceProxy.CreateV3())
            {
                var result = service.Query("SELECT S.Value FROM Orion.Setting as S where S.Name = 'ESI.IncidentsCreatedThreshold'");
                if (result.Rows.Count > 0)
                {
                    thresholdValue = Convert.ToString(result.Rows[0]["Value"]);
                }

            }
        }
        catch (Exception ex)
        {
            log.WarnFormat("Couldn't fetch settings value: {0}", ex);
        }
        return string.Format(thresholdValue);
    }

    public string GetRestrictedBySystemHint()
    {
        return string.Format(Resources.ESIWebContent.Restricted_by_system_Hint, GetIncidentCreationThreshold());
    }
}