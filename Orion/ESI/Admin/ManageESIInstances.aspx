﻿<%@ Page Language="C#"
	AutoEventWireup="true"
	MasterPageFile="~/Orion/Admin/OrionAdminPage.master"
	CodeFile="ManageESIInstances.aspx.cs"
	Inherits="Orion_Admin_ESI_ManageESIInstances" %>

<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
	<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
	<orion:Include runat="server" File="OrionCore.js" />
	<orion:Include runat="server" File="RenderControl.js" />
	<orion:Include runat="server" File="ESI/Admin/ManageESIInstances.js" />
	<orion:include runat="server" file="ESI_ManageInstances.css" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks">
	<orion:IconLinkExportToPDF runat="server" />
	<orion:IconHelpButton runat="server" HelpUrlFragment="orioncoremanagealertintegrations" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
	<div id="mainContent">
		<h1 class="page-title mb15"><%=this.Title %></h1>
		<input type="hidden" name="ManageESIInstances_PageSize" id="ManageESIInstances_PageSize" value='20' />
		<input type="hidden" name="ManageESIInstances_SortOrder" id="ManageESIInstances_SortOrder" />
		<input type="hidden" name="ManageESIInstances_SelectedColumns" id="ManageESIInstances_SelectedColumns" />

		<div runat="server" id="InstanceSuccessfullyCreated" class="sw-suggestion sw-suggestion-pass sw-suggestion-border mt15" clientidmode="Static">
			<span class="sw-suggestion-icon"></span>
			<span><%= Resources.ESIWebContent.Instance_Added %></span>
			<span style="font-weight: normal"><%= Resources.ESIWebContent.Instance_Added_Hint_1 %><a href="<%= string.Format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", Guid.NewGuid()) %>"><%= Resources.ESIWebContent.Instance_Added_Create_New_Alerts %></a><%= Resources.ESIWebContent.Instance_Added_Hint_2 %><a href="/Orion/Alerts/Default.aspx"><%= Resources.ESIWebContent.Instance_Added_Manage_Alerts %></a><%= Resources.ESIWebContent.Instance_Added_Hint_3 %></span>
			<span><a class="sw-suggestion-btn-close" href="#">&nbsp;</a></span>
		</div>

		<div id="ManageESIInstancesGrid" class="mt15"></div>
	</div>

	<%--TODO: need localize--%>
	<div id="addItemDialog" style="display: none;" />
	<% if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{ %>
	<div id="isOrionDemoMode" style="display: none;" />
	<% } %>
</asp:Content>
