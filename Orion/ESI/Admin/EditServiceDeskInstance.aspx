<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditServiceDeskInstance.aspx.cs"
    Inherits="Orion_ESI_Admin_EditServiceDeskInstance" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ Resources:ESIWebContent, Edit_Service_Desk_Instance %>" %>

<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="ESI_EditInstance.css" />
    <orion:Include runat="server" File="ESI/Libs.js" />
    <orion:Include runat="server" File="ESI/Admin/EditServiceDeskInstance.js" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder" automation="EditServiceDeskInstance">
    <div class="esi-panel" esi-scope="EditServiceDeskInstance">

        <input type="hidden" esi-bind="instanceId" automation="EditServiceDeskInstance_ServiceDeskInstance_InstanceId" value="<%= this.InstanceId %>" />
        <h1 automation="EditServiceDeskInstance_Title"><%= Resources.ESIWebContent.Edit_Service_Desk_Instance %></h1>

        <div class="esi-control-section esi-modal" esi-modal="showLoadingModal" esi-modal-append-to="#container">

            <!-- loading indicator -->
            <div class="esi-loading-indicator" esi-show="showLoading" automation="EditServiceDeskInstance_LoadingIndicator">
                <asp:Image runat="server" ID="loader" ImageUrl="~/Orion/images/loading_gen_small.gif" />
                <span><%=Resources.ESIWebContent.Service_Desk_Loading_Please_Wait %></span>
            </div>
            <div class="esi-control-group esi-cloak" esi-show="showErrorMessage" automation="EditServiceDeskInstance_Error">
                <p class="esi-error"><strong><%=Resources.ESIWebContent.Service_Desk_Error %></strong> <span esi-bind="errorMessage"></span></p>
                <ul class="esi-horizontal-stack" style="margin-left: auto; margin-right: auto; width: 58px">
                    <li><a esi-click="closeModal" href="#" class="esi-btn esi-btn-tertiary">Close</a></li>
                </ul>
            </div>

        </div>

        <div class="esi-control-section">
            <label automation="EditServiceDeskInstance_ServiceDeskInstance_Section_Label"><%=Resources.ESIWebContent.Service_Desk_Instance %></label>

            <div id="InstanceName" class="esi-control-group">
                <label automation="EditServiceDeskInstance_ServiceDeskInstance_Name_Label"><%=Resources.ESIWebContent.Service_Desk_Instance_Name %></label>
                <div>
                    <input type="text" esi-bind="name" esi-validate="validate" automation="EditServiceDeskInstance_ServiceDeskInstance_Name" />
                    <p automation="EditServiceDeskInstance_ServiceDeskInstance_Name_Description"><%= Resources.ESIWebContent.Service_Desk_Name_Description%></p>
                    <p class="esi-validation-message esi-cloak" esi-show="nameIsEmpty" automation="EditServiceDeskInstance_ServiceDeskInstance_Name_EmptyMessage"><%= Resources.ESIWebContent.Service_Desk_Name_Should_Not_Be_Empty %></p>
                </div>
            </div>

            <div id="InstanceToken" class="esi-control-group">
                <label class="text-area-label" automation="EditServiceDeskInstance_Authentication_Token_Label"><%=Resources.ESIWebContent.Service_Desk_Token %></label>
                <div>
                    <textarea rows="4" esi-bind="token" esi-validate="validate" esi-change="tokenChanged" automation="EditServiceDeskInstance_Authentication_Token"></textarea>
                    <p automation="EditServiceDeskInstance_Authentication_Token_Description">
                        <%= Resources.ESIWebContent.Service_Desk_Token_Description%>
                        <a class="sw-link" target="_blank" rel="noopener noreferrer" href="<%=SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer %>/documentation/helploader.aspx?topic=orioncoresdtoken.htm">&#0187;&nbsp;<%= Resources.ESIWebContent.Service_Desk_Token_Help_Link%></a>
                    </p>
                    <p class="esi-validation-message esi-cloak" esi-show="tokenIsEmpty" automation="EditServiceDeskInstance_Authentication_Token_EmptyMessage"><%= Resources.ESIWebContent.Service_Desk_Token_Should_Not_Be_Empty %></p>
                </div>
            </div>

            <div id="InstanceTest" class="esi-control-group">
                <label>&nbsp;</label>
                <div class="ssd-test-connection-container">
                    <ul class="esi-horizontal-stack">
                        <li esi-show="showTestCredentialsButton" class="esi-cloak"><a esi-click="test" href="#" class="esi-btn esi-btn-secondary" automation="EditServiceDeskInstance_Test"><%= Resources.ESIWebContent.Service_Desk_Test_Connection %></a></li>
                        <li esi-show="showTestInProgress" class="esi-cloak">

                            <ul class="esi-horizontal-stack" esi-show="showTestInProgress" class="esi-cloak">
                                <li>
                                    <span class="esi-loading-indicator esi-cloak" automation="EditServiceDeskInstance_TestConnection_LoadingIndicator">
                                        <asp:Image runat="server" ID="Image1" ImageUrl="~/Orion/images/loading_gen_small.gif" />
                                        <span><%=Resources.ESIWebContent.Service_Desk_Connection_Test_Loading %></span>
                                    </span>
                                </li>
                                <li><a esi-click="cancelTest" href="#" class="esi-btn esi-btn-tertiary"><%=Resources.ESIWebContent.Service_Desk_Cancel %></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="esi-vertical-stack">
                        <li esi-show="showCredentialsOkMessage" class="esi-cloak ssd-test-msg-container" automation="EditServiceDeskInstance_TestConnection_TestSuccess"><span class="esi-success esi-cloak"><%= Resources.ESIWebContent.Service_Desk_Connection_Test_Success %></span></li>
                        <li esi-show="showTestFailedMesssage" class="esi-cloak ssd-test-msg-container" automation="EditServiceDeskInstance_TestConnection_TestFailure">
                            <div class="esi-error esi-cloak">
                                <span esi-bind="testFailedMessage"></span>
                                <br />
                                <span class="fake-link" automation="EditServiceDeskInstance_TestFailureDetail" esi-show="showTestFailedDetail" esi-click="showDetails"><%= Resources.ESIWebContent.Service_Desk_Connection_Test_See_Details %></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div id="InstanceUrl" esi-show="showInstanceUrlSection" class="esi-control-group">
                <label id="InstanceUrlLabel" automation="EditServiceDeskInstance_ServiceDeskInstance_Url_Label"><%=Resources.ESIWebContent.Service_Desk_Instance_Url %></label>
                <div>
                    <a id="InstanceUrlLink" esi-bind="url" class="sw-link" target="_blank" automation="EditServiceDeskInstance_ServiceDeskInstance_UrlLink"></a>
                    <input esi-bind="url" type="hidden" automation="EditServiceDeskInstance_ServiceDeskInstance_UrlField" />
                </div>
            </div>
        </div>

        <div class="esi-control-section" id="proxySection">
            <label automation="EditServiceDeskInstance_Proxy_Section_Label"><%=Resources.ESIWebContent.Service_Desk_InstanceProxySetting_SectionName %></label>

            <div id="ProxyEnabled" class="esi-control-group">
                <label>&nbsp;</label>
                <div>
                    <p>
                        <%=Resources.ESIWebContent.Service_Desk_Proxy_Info %>
                        <a href="/Orion/Admin/HttpProxySettings/Default.aspx"
                            target="_blank"
                            automation="EditServiceDeskInstance_Proxy_SettingsLink"
                            class="sw-link">
                            <%=Resources.ESIWebContent.Service_Desk_Proxy_Info_Link_Text %>
                        </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="esi-bottom-section">
            <ul class="esi-horizontal-stack">
                <li><a esi-click="save" href="#" class="esi-btn esi-btn-primary" automation="EditServiceDeskInstance_Save"><%= Resources.ESIWebContent.Service_Desk_Instance_Detail_Save %></a></li>
                <li><a esi-click="discard" href="#" class="esi-btn esi-btn-tertiary" automation="EditServiceDeskInstance_Cancel"><%= Resources.ESIWebContent.Service_Desk_Instance_Detail_Cancel %></a></li>
            </ul>
        </div>

    </div>

    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {%>
    <div id="isOrionDemoMode" style="display: none;"></div>
    <%}%>
</asp:Content>
