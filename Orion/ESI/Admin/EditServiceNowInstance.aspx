<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditServiceNowInstance.aspx.cs"
	Inherits="Orion_ESI_Admin_EditServiceNowInstance" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ Resources:ESIWebContent, Edit_Service_Now_Instance %>" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
	<orion:include runat="server" framework="Ext" frameworkversion="3.4" />
	<orion:include runat="server" file="ESI_EditInstance.css" />
	<orion:include runat="server" file="ESI/Libs.js" />
	<orion:include runat="server" file="ESI/Admin/EditServiceNowInstance.js" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder" automation="EditServiceNowInstance">

	<script type="text/javascript">
		var ExpandCollapseOnHelpFromHumansWidget = function (colapseBlock) {
			var element = $('img:first', colapseBlock.parentElement)[0];
			var expanded = element.src.indexOf("Collapse") >= 0;

			element.src = expanded ? "/Orion/images/Button.Expand.gif" : "/Orion/images/Button.Collapse.gif";

			$("div:first", colapseBlock.parentElement).css("display", expanded ? "none" : "block");

			return false;
		};

	</script>

	<div class="esi-panel" esi-scope="EditServiceNowInstance">

		<input type="hidden" esi-bind="instanceId" automation="EditServiceNowInstance_ServiceNowInstance_InstanceId" value="<%= this.InstanceId %>" />
		<h1 automation="EditServiceNowInstance_Title"><%= Resources.ESIWebContent.Edit_Service_Now_Instance %></h1>

		<div class="esi-control-section esi-modal" esi-modal="showLoadingModal" esi-modal-append-to="#container">

			<!-- loading indicator -->
			<div class="esi-loading-indicator" esi-show="showLoading" automation="EditServiceNowInstance_LoadingIndicator">
				<asp:Image runat="server" ID="loader" ImageUrl="~/Orion/images/loading_gen_small.gif" />
				<span><%=Resources.ESIWebContent.Loading_Please_Wait %></span>
			</div>
			<div class="esi-control-group esi-cloak" esi-show="showErrorMessage" automation="EditServiceNowInstance_Error">

				<p class="esi-error"><strong><%=Resources.ESIWebContent.Error %></strong> <span esi-bind="errorMessage"></span></p>

				<ul class="esi-horizontal-stack" style="margin-left: auto; margin-right: auto; width: 58px">
					<li><a esi-click="closeModal" href="#" class="esi-btn esi-btn-tertiary">Close</a></li>
					</li>
				</ul>


			</div>


		</div>

		<div class="esi-control-section">
			<label automation="EditServiceNowInstance_ServiceNowInstance_Section_Label"><%=Resources.ESIWebContent.Service_Now_Instance %></label>

			<div id="InstanceName" class="esi-control-group">
				<label automation="EditServiceNowInstance_ServiceNowInstance_Name_Label"><%=Resources.ESIWebContent.Instance_Name %></label>
				<div>
					<input type="text" esi-bind="name" esi-validate="validate" automation="EditServiceNowInstance_ServiceNowInstance_Name" />
					<p automation="EditServiceNowInstance_ServiceNowInstance_Name_Description"><%= Resources.ESIWebContent.Instance_Name_Description%></p>
					<p class="esi-validation-message esi-cloak" esi-show="nameIsEmpty" automation="EditServiceNowInstance_ServiceNowInstance_Name_EmptyMessage"><%= Resources.ESIWebContent.Name_Should_Not_Be_Empty %></p>
				</div>
			</div>

			<div id="InstanceUrl" class="esi-control-group">
				<label automation="EditServiceNowInstance_ServiceNowInstance_Url_Label"><%=Resources.ESIWebContent.Instance_Url %></label>
				<div>
					<input type="text" esi-bind="url" esi-validate="validate" esi-change="connectionChanged" automation="EditServiceNowInstance_ServiceNowInstance_Url" />
					<p automation="EditServiceNowInstance_ServiceNowInstance_Url_Description"><%= Resources.ESIWebContent.Instance_Url_Description%></p>
					<p class="esi-validation-message esi-cloak" esi-show="urlIsEmpty" automation="EditServiceNowInstance_ServiceNowInstance_Url_EmptyMessage"><%= Resources.ESIWebContent.Url_Should_Not_Be_Empty %></p>
					<p class="esi-validation-message esi-cloak" esi-show="urlIsInvalid" automation="EditServiceNowInstance_ServiceNowInstance_Url_InvalidMessage"><%= Resources.ESIWebContent.Url_Is_Invalid %></p>
				</div>
			</div>
		</div>

		<div class="esi-control-section">
			<label automation="EditServiceNowInstance_Credentials_Section_Label"><%=Resources.ESIWebContent.Instance_Credentials %></label>
			<div id="InstanceLogin" class="esi-control-group">
				<label automation="EditServiceNowInstance_Credentials_Login_Label"><%=Resources.ESIWebContent.Instance_Login %></label>
				<div>
					<input type="text" esi-bind="login" esi-validate="validate" esi-change="connectionChanged" automation="EditServiceNowInstance_Credentials_Login" />
					<p automation="EditServiceNowInstance_Credentials_Login_Description"><%= Resources.ESIWebContent.Instance_Login_Description%></p>
					<p class="esi-validation-message esi-cloak" esi-show="loginIsEmpty" automation="EditServiceNowInstance_Credentials_Login_EmptyMessage"><%= Resources.ESIWebContent.Login_Should_Not_Be_Empty %></p>
				</div>
			</div>

			<div id="InstancePassword" class="esi-control-group">
				<label automation="EditServiceNowInstance_Credentials_Password_Label"><%=Resources.ESIWebContent.Instance_Password %></label>
				<div>
					<!-- readonly and onfocus attributes prevent browsers from autofill of stored username and password -->
					<input type="password" esi-bind="password" esi-validate="validate" esi-change="passwordChanged" readonly onfocus="this.removeAttribute('readonly');" automation="EditServiceNowInstance_Credentials_Password" />
					<p automation="EditServiceNowInstance_Credentials_Password_Description"><%= Resources.ESIWebContent.Instance_Password_Description%></p>
					<p class="esi-validation-message esi-cloak" esi-show="passwordIsEmpty" automation="EditServiceNowInstance_Credentials_Password_EmptyMessage"><%= Resources.ESIWebContent.Password_Should_Not_Be_Empty %></p>
				</div>
			</div>
			<div id="InstanceTest" class="esi-control-group">
				<label>&nbsp;</label>
				<div>
					<ul class="esi-horizontal-stack">
						<li esi-show="showTestCredentialsButton" class="esi-cloak"><a esi-click="test" href="#" class="esi-btn esi-btn-secondary" automation="EditServiceNowInstance_Test"><%= Resources.ESIWebContent.Test_Credentials %></a></li>
						<li esi-show="showTestInProgress" class="esi-cloak">

							<ul class="esi-horizontal-stack" esi-show="showTestInProgress" class="esi-cloak">
								<li>
									<span class="esi-loading-indicator esi-cloak" automation="EditServiceNowInstance_TestConnection_LoadingIndicator">
										<asp:Image runat="server" ID="Image1" ImageUrl="~/Orion/images/loading_gen_small.gif" />
										<span><%=Resources.ESIWebContent.Connection_Test_Loading %></span>
									</span>
								</li>
								<li><a esi-click="cancelTest" href="#" class="esi-btn esi-btn-tertiary"><%=Resources.ESIWebContent.Cancel %></a></li>
							</ul>
						</li>
					</ul>
					&nbsp;
                    <ul class="esi-vertical-stack">
						<li esi-show="showCredentialsOkMessage" class="esi-cloak" automation="EditServiceNowInstance_TestConnection_TestSuccess"><span class="esi-success esi-cloak"><%= Resources.ESIWebContent.Connection_Test_Success %></span></li>
						<li esi-show="showTestFailedMesssage" class="esi-cloak" automation="EditServiceNowInstance_TestConnection_TestFailure">
							<div class="esi-error esi-cloak"><span esi-bind="testFailedMessage"></span>&nbsp;&nbsp;&nbsp;<span class="fake-link" automation="EditServiceNowInstance_TestFailureDetail" esi-show="showTestFailedDetail" esi-click="showDetails"><%= Resources.ESIWebContent.Connection_Test_See_Details %></span></div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="esi-control-section" id="proxySection">
			<label automation="EditServiceNowInstance_Proxy_Section_Label"><%=Resources.ESIWebContent.InstanceProxySetting_SectionName %></label>

			<div id="ProxyEnabled" class="esi-control-group">
				<label>&nbsp;</label>
				<div>
					<p>
						<%=Resources.ESIWebContent.Service_Now_Proxy_Info %>
						<a href="/Orion/Admin/HttpProxySettings/Default.aspx"
						   target="_blank"
						   automation="EditServiceNowInstance_Proxy_SettingsLink"
						   class="sw-link">
							<%=Resources.ESIWebContent.Service_Now_Proxy_Info_Link_Text %>
						</a>
					</p>
				</div>
			</div>
		</div>

		<div class="esi-bottom-section">
			<ul class="esi-horizontal-stack">
				<li><a esi-click="save" href="#" class="esi-btn esi-btn-primary" automation="EditServiceNowInstance_Save"><%= Resources.ESIWebContent.Service_Now_Instance_Detail_Save %></a></li>
				<li><a esi-click="discard" href="#" class="esi-btn esi-btn-tertiary" automation="EditServiceNowInstance_Cancel"><%= Resources.ESIWebContent.Service_Now_Instance_Detail_Cancel %></a></li>
			</ul>
		</div>

	</div>

	<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{%>
	<div id="isOrionDemoMode" style="display: none;"></div>
	<%}%>
</asp:Content>
