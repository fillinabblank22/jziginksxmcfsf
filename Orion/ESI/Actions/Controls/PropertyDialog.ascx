﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PropertyDialog.ascx.cs" Inherits="Orion_ESI_Actions_Controls_PropertyDialog" %>

<orion:Include runat="server" File="esi_propertydialog.css" />

<script type="text/javascript">

    if (typeof (SW) == "undefined" || typeof (SW.ESI) == "undefined" || typeof (SW.ESI.PropertyDialog) == "undefined") {
        SW.Core.namespace('SW.ESI').PropertyDialog = function () {

            var callback_ref = null;
            var page_size = 10;
            var page_count = 1;
            var actualPage = 1;
            var props = null;


            var saveActualPageState = function () {
                var index = (actualPage - 1) * page_size;

                $(".property-dialog-checkbox").each(function () {
                    if ($(this).prop("checked")) {
                        props[index].checked = true;
                    }
                    else {
                        props[index].checked = false;
                    }

                    index++;
                });
            }

            var renderPage = function () {
                var offset = (actualPage - 1) * page_size;
                var end = Math.min(page_size, props.length - offset) + offset;

                $('#choosePropertyDialog_propertyTable').empty();

                for (i = offset; i < end; i++) {
                    var checked = props[i].checked ? " checked='checked'" : "";

                    if (i % 2 == 0) {
                        $('#choosePropertyDialog_propertyTable').append("<tr class='esi-property-dialog-odd-row'><td><label><input type='checkbox' id='" + props[i].id + "' "
                                                                        + checked + " class='property-dialog-checkbox' />&nbsp;" + props[i].caption + "</label></td></tr>");
                    }
                    else {
                        $('#choosePropertyDialog_propertyTable').append("<tr><td><label><input type='checkbox' id='" + props[i].id + "' "
                                                                        + checked + " class='property-dialog-checkbox' />&nbsp;" + props[i].caption + "</label></td></tr>");
                    }
                }

                $('#choosePropertyDialog_pageCount').html(page_count);
                $('#choosePropertyDialog_pageNumber').val(actualPage);
            }

            var changePage = function (pageNo) {
                saveActualPageState();

                actualPage = pageNo;

                if (actualPage < 1) {
                    actualPage = 1;
                }

                if (actualPage > page_count) {
                    actualPage = page_count;
                }

                renderPage();
            }

            var onSubmit = function () {
                saveActualPageState();

                if (callback_ref != null) {
                    callback_ref(true, props);
                    callback_ref = null;
                }

                $('#choosePropertyDialog').remove();
            }

            var onCancel = function () {
                saveActualPageState();

                if (callback_ref != null) {
                    callback_ref(false, props);
                    callback_ref = null;
                }

                $('#choosePropertyDialog').remove();
            }

            var setup = function () {
                // Button events
                $('#choosePropertyDialog_okButton').click(function () {
                    onSubmit();
                    return false;
                });

                $('#choosePropertyDialog_cancelButton').click(function () {
                    onCancel();
                    return false;
                });

                // Pager events
                $('#choosePropertyDialog_firstPage').click(function () { changePage(1); return false; });
                $('#choosePropertyDialog_lastPage').click(function () { changePage(page_count); return false; });
                $('#choosePropertyDialog_previousPage').click(function () { changePage(actualPage - 1); return false; });
                $('#choosePropertyDialog_nextPage').click(function () { changePage(actualPage + 1); return false; });

                var updatePager = function () {
                    var value = $('#choosePropertyDialog_pageNumber').val();
                    var number = parseInt(value);

                    if (value == number) {
                        changePage(number);
                    }
                    else {
                        changePage(1);
                    }
                }

                // Pager input
                $('#choosePropertyDialog_pageNumber').keyup(function (e) {
                    if (e.keyCode == 13) {
                        updatePager();
                    }
                });

                $('#choosePropertyDialog_pageNumber').focusout(function (e) {
                    updatePager();
                });

                renderPage();
            };

            return {

                // shows property dialog
                // @param {object[]} properties - initial setup { id: "unique_identifier_string", caption: "checkbox_text_string", checked: true/false }
                // @param {function(bool submit, object[] result)} callback - function called on dialog button press
                //           submit - true of "OK" pressed, false if "CANCEL" pressed
                //           result - actual state of checkboxes in the same format is input properties
                show: function (properties, callback) {
                    if (callback_ref != null) {
                        return; // only one dialog is allowed
                    }

                    props = properties;
                    callback_ref = callback;

                    page_count = Math.ceil(properties.length / page_size);
                    actualPage = 1;

                    var html =
"<div id='choosePropertyDialog' class='esi-hidden'>\
    <div class='esi-property-dialog-content'>\
        <table id='choosePropertyDialog_propertyTable' automation='property_table' />\
    </div>\
    <div id='paging' class='esi-property-dialog-paging'>\
        <table>\
            <tr>\
                <td><button type='button' id='choosePropertyDialog_firstPage' automation='pager_first' class='esi-property-dialog-pager-first' /></td>\
                <td><button type='button' id='choosePropertyDialog_previousPage' automation='pager_prev' class='esi-property-dialog-pager-previous' /></td>\
                <td><%= Resources.CoreWebContent.WEBDATA_VB0_9 %></td>\
                <td><input type='text' id='choosePropertyDialog_pageNumber' size='3' automation='pager_actual_page' class='sw-tbar-page-number' /></td>\
                <td><%= Resources.CoreWebContent.WEBDATA_TM0_264 %>&nbsp;<span id='choosePropertyDialog_pageCount' automation='pager_page_count'>X</span></td>\
                <td><button type='button' id='choosePropertyDialog_nextPage' automation='pager_next' class='esi-property-dialog-pager-next' /></td>\
                <td><button type='button' id='choosePropertyDialog_lastPage' automation='pager_last' class='esi-property-dialog-pager-last' /></td>\
            </tr>\
        </table>\
    </div>\
    <div class='bottom'>\
        <div class=''>\
            <button id='choosePropertyDialog_okButton' automation='property_ok' class='sw-btn-primary sw-btn' ><%= Resources.ESIWebContent.PropertyDialog_OK %></button>\
            <button id='choosePropertyDialog_cancelButton' automation='property_cancel' class='sw-btn-secondary sw-btn sw-btn-cancel'><%= Resources.ESIWebContent.PropertyDialog_Cancel %></button>\
        </div>\
    </div>\
</div>";
                    $(document.body).append(html);

                    // Dialog
                    // .first() is used because rendering the ascx control again adds the same html layout 
                    //  which would cause multiple dialogs to show
                    $("#choosePropertyDialog").css('overflow', 'hidden'); // disable scrollbars

                    $("#choosePropertyDialog").attr("title", $('#<%= choosePropertyDialogTitle.ClientID %>').val())
                    $("#choosePropertyDialog").dialog({
                        closeOnEscape: true,
                        resizable: false,
                        height: 450,
                        width: 400,
                        modal: true,
                        close: function (event, ui) { onCancel(); }
                    });

                    setup();
                }
            };
        }();
    }

</script>

<input runat="server" id="choosePropertyDialogTitle" style="display:none" ></input>

