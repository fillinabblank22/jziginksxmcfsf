﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_ESI_Actions_Controls_PropertyDialog : System.Web.UI.UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.choosePropertyDialogTitle.Value = this.Title;
    }
}