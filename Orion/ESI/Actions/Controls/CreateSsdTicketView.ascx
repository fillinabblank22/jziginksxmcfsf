﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CreateSsdTicketView.ascx.cs" Inherits="Orion_ESI_Actions_Controls_CreateSsdTicketView" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<orion:include runat="server" file="ESI/Libs.js" />
<orion:include runat="server" file="ESI/Actions/CreateSsdTicketAction.js" />
<orion:include runat="server" file="ESI.css" />

<div id="container" runat="server" esi-scope="CreateSsdTicketAction" automation="CreateSsdTicketAction_Container">

    <style type="text/css">
        .esi-control-group
        .entity-attributes,
        .esi-control-group
        .custom-attributes {
            border-collapse: collapse;
            width: 600px;
            table-layout: fixed;
        }

        .esi-control-group
        .alignRight {
            float: right;
        }

        .esi-control-group
        .esi-suggestion-first {
            margin-top: 0;
        }

        .esi-control-group
        .title-column:nth-child(1) {
            width: 30%;
        }

        .esi-control-group
        .title-column:nth-child(2) {
            width: 62%;
        }
		.add-attr-button {
			cursor: pointer;
		}
    </style>

    <div runat="server" id="Settings" class="esi-cloak">
        <h3 automation="CreateSsdTicketAction_InstanceSelect_Title"><%=Resources.ESIWebContent.Service_Desk_Select_Instance %></h3>
        <div id="selectSsdInstanceSection" class="section required esi-panel">

            <div class="esi-control-group esi-condensed">
                <table>
                    <tr>
                        <td>
                            <select esi-items="instances"
                                esi-items-spec='{"value":"id","display":"name"}'
                                esi-bind="instanceId"
                                esi-disable="editing"
                                automation="CreateSsdTicketAction_InstanceSelect">
                            </select>
                        </td>
                    </tr>
                </table>

                <p class="esi-suggestion esi-cloak" esi-show="showEditingInfo" automation="CreateSsdTicketAction_Editing_Instance_Info_Box">
                    <%=Resources.ESIWebContent.Service_Desk_EditingActionInfo %>
                </p>

                <p class="esi-suggestion esi-cloak" esi-show="hasIntegrationAction" automation="CreateSsdTicketAction_Has_Integration_Action_Warning">
                    <%=Resources.ESIWebContent.Service_Desk_Another_Action_Defined_Warning %>
                </p>

                <p class="esi-suggestion esi-cloak" esi-show="resetTab" automation="CreateSsdTicketAction_Reset_Tab_Warning">
                    <%=Resources.ESIWebContent.Service_Desk_Action_On_Reset_Warning %>
                </p>
            </div>
        </div>

        <h3 automation="CreateSsdTicketAction_EntityAttributes_Title"><%=Resources.ESIWebContent.Service_Desk_Action_Entity_Attributes_Section_Name %></h3>
        <div id="ssdEntityAttributesSection" class="section esi-panel table-section">
            <div class="esi-control-group esi-condensed">
                <a id="add_entity_attr_button" class="esi-btn-small esi-btn-secondary add-attr-button" automation="add_custom_attr_button" data-macro="entityAttributes"><%=Resources.ESIWebContent.Service_Desk_Action_Select_Entity_Attributes_Button_Label %></a>
                <span class="alignRight">
                <orion:IconHelpButton runat="server" HelpUrlFragment="orioncoresdaction" />
                </span>
            </div>
            <div class="esi-control-group esi-condensed">
                <table id="entity-attributes-table" automation="entity-attributes-table" class="entity-attributes" data-form="entityAttributes" esi-grid="entityAttributes"
                    esi-grid-column-names="{'DisplayName':'<%=Resources.ESIWebContent.Service_Desk_Action_Entity_Attributes_Name_Column_Name %>', 'Name':'<%=Resources.ESIWebContent.Service_Desk_Action_Entity_Attributes_Target_Attribute_Column_Name %>'}">
                </table>
            </div>
        </div>

        <h3 automation="CreateSsdTicketAction_CustomAttributes_Title" class="editable-table-section-header"><%=Resources.ESIWebContent.Service_Desk_Action_Custom_Attributes_Section_Name %></h3>
        <div id="ssdCustomAttributesSection" class="section esi-panel table-section">
            <div class="esi-control-group esi-condensed">
                <p class="esi-suggestion esi-cloak esi-suggestion-first" automation="CreateSsdTicketAction_Custom_Attributes_Hint">
                    <%=String.Format(Resources.ESIWebContent.Service_Desk_Action_Custom_Attrs_Hint, SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer + "/documentation/helploader.aspx?topic=orioncoresdaction.htm") %>
                </p>
            </div>
            <div class="esi-control-group esi-condensed">
                <a id="add_custom_attr_button" class="esi-btn-small esi-btn-secondary add-attr-button" automation="add_entity_attr_button" data-macro="customAttributes"><%=Resources.ESIWebContent.Service_Desk_Action_Select_Custom_Attributes_Button_Label %></a>
            </div>
            <div class="esi-control-group esi-condensed">
                <table id="custom-attributes-table" automation="custom-attributes-table" class="custom-attributes" data-form="customAttributes" esi-grid="customAttributes" esi-grid-editable
                    esi-grid-column-names="{'Name':'<%=Resources.ESIWebContent.Service_Desk_Action_Custom_Attributes_Name_Column_Name %>', 'Value':'<%=Resources.ESIWebContent.Service_Desk_Action_Custom_Attributes_Value_Column_Name %>'}">
                </table>
                <p class="esi-error esi-cloak" esi-show="customAttrRequiredError" automation="CreateSsdTicketAction_Custom_Attributes_Requied_Error_Box">
                    <%=Resources.ESIWebContent.Service_Desk_Action_Custom_Attrs_Required_Error_Message %>
                </p>
                <p class="esi-error esi-cloak" esi-show="customAttrUniqueError" automation="CreateSsdTicketAction_Custom_Attributes_Unique_Error_Box">
                    <%=Resources.ESIWebContent.Service_Desk_Action_Custom_Attrs_Unique_Error_Message %>
                </p>
                <p class="esi-error esi-cloak" esi-show="customAttrLengthError" automation="CreateSsdTicketAction_Custom_Attributes_Length_Error_Box">
                    <%=Resources.ESIWebContent.Service_Desk_Action_Custom_Attrs_Length_Error_Message %>
                </p>
            </div>
        </div>
    </div>

    <div runat="server" id="NoInstances" class="section" automation="CreateSsdTicketAction_NoInstances_Panel">
        <h1 automation="CreateSsdTicketAction_NoInstances_Panel_Title"><%=Resources.ESIWebContent.Service_Desk_No_SSD_Instances_Configured %></h1>
        <a target="_blank" automation="CreateSsdTicketAction_NoInstances_Panel_Settings_Page_Link" class="sw-link" href="/Orion/ESI/Admin/EditServiceDeskInstance.aspx">»&nbsp;<%=Resources.ESIWebContent.Service_Desk_Configure_Instance %></a>
    </div>

</div>


<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function () {
        var viewContext = <%= ToJson(ViewContext) %>;
        var actionContext =  <%= ToJson(ActionDefinition) %>;
        if (!actionContext) {
            actionContext = <%= CreateSsdActionDefinition() %>;
        };

        if (!actionContext.TransitiveID) {
            actionContext.TransitiveID = parseInt(Date.now() / 1000);
        }

        var config = {
            actionDefinition: actionContext,
            viewContext: viewContext,
            environmentType: <%= ToJson(EnviromentType) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            isActionSupported: <%= IsActionSupported.ToString().ToLower() %>,
            severity: "<%= AlertSeverity %>",
            insertVariableLabel: "<%= Resources.ESIWebContent.Insert_Variable_Label %>",
            alertSwisObjectType: "<%= AlertSwisObjectType %>"
        };

        config.hasIntegrationAction = function () {
            if (!actionContext) {
                return false;
            };
            return !actionContext.ID &&
                _.find(SW.Core.Actions.ActionsEditor.getActions(), function (item) { return item.ActionTypeID === actionContext.ActionTypeID && item.TransitiveID !== actionContext.TransitiveID; }) !== undefined;
        };

        config.onReadyCallback = function (plugin) {

            <%= OnReadyJsCallback %>(plugin);

            if (!config.isActionSupported) {
                $("#createActionDefinitionBtn").hide();
                $("#nextSectionInActionDefinitionBtn").hide();
                $("#addActionDefinitionBtn").hide();
                $('#_basicActionPlugins_').remove();
                $('#_topActionPlugins_').remove();
                $('#_timeOfDayActionPlugins_').remove();
            }
        };

        var controller = new SW.Core.Actions.CreateSsdTicketController(config);

        SW.Core.MacroVariablePickerController.BindPicker();
    })();
</script>
