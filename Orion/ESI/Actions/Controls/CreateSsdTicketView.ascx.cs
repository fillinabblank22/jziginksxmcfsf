﻿using Resources;
using SolarWinds.Newtonsoft.Json;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.SSDI.Actions.Actions.CreateSsdTicket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_ESI_Actions_Controls_CreateSsdTicketView : ActionPluginBaseView
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        var alert = AddAlertWorkflow.Instance.Alert;
        if (alert != null)
        {
            AlertSeverity = alert.Severity;
        }
    }

    public string CreateSsdActionDefinition()
    {
        var action = new CreateSsdTicketPlugin();

        return ToJson(new ActionDefinition
        {
            ActionTypeID = action.ActionTypeID,
            Description = action.Description,
            IconPath = action.IconPath,
            Title = action.Title,
            Enabled = true
        });
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ActionDefinitionJsonString))
        {
            ActionDefinition = JsonConvert.DeserializeObject<ActionDefinition>(ActionDefinitionJsonString);
        }

        //Check if we have any instances 
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var result = proxy.Query("SELECT COUNT(InstanceID) AS InstanceCount FROM Orion.ESI.IncidentService WHERE Type = 'ServiceDesk'");
            var count = Convert.ToInt32(result.Rows[0]["InstanceCount"]);
            IsActionSupported = count > 0;
            Settings.Visible = IsActionSupported;
            NoInstances.Visible = !IsActionSupported;
        }
    }

    public bool HasIntegrationActionPresent()
    {
        var alert = AddAlertWorkflow.Instance.Alert;
        if (alert == null || alert.TriggerActions == null)
        {
            return false;
        }

        return alert.TriggerActions.Exists(p => p.ActionTypeID == ActionTypeID);
    }

    public AlertSeverity AlertSeverity
    {
        get;
        private set;
    }

    public string AlertSwisObjectType
    {
        get 
		{
			var alert = AddAlertWorkflow.Instance.Alert;
			return alert?.Trigger.Conditions[0].Type.EntityProvider.GetEntityByObjectType(alert.ObjectType).FullName;
		}
    }

    public override string ActionTypeID
    {
        get { return CreateSsdTicketConstants.ActionTypeID; }
    }
}