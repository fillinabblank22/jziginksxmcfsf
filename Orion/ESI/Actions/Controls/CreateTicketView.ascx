﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CreateTicketView.ascx.cs" Inherits="Orion_ESI_Actions_Controls_CreateTicketView" %>
<orion:Include runat="server" File="ESI/Libs.js" />
<orion:Include runat="server" File="ESI/Actions/CreateTicketAction.js" />
<orion:Include runat="server" File="ESI.css" />

<%@ Register TagPrefix="orion" TagName="PropertyDialog" Src="~/Orion/ESI/Actions/Controls/PropertyDialog.ascx" %>

<div id="container" runat="server" esi-scope="CreateTicketAction" automation="CreateTicketAction_Container">

    <orion:propertydialog runat="server" ID="selectPropertiesDialog" />

    <div runat="server" id="Settings" class="esi-cloak">
        <h3 automation="CreateTicketAction_InstanceSelect_Title"><%=Resources.ESIWebContent.Select_Instance %></h3>
        <div class="section required esi-panel">
            
            <div class="esi-control-group esi-condensed">
                <table>
                    <tr>
                        <td>
                            <select esi-items="instances" 
                                    esi-items-spec='{"value":"id","display":"name"}' 
                                    esi-bind="instanceId" 
                                    esi-change="instanceChanged" 
                                    esi-disable="editing"
                                    automation="CreateTicketAction_InstanceSelect">
                            </select>
                        </td>
                        <td>
                            <img alt="Loading..." src="/Orion/images/loading_gen_16x16.gif" esi-show="loading" />
                        </td>
                    </tr>
                </table>

                <p class="esi-error esi-cloak" esi-show="getDataError" automation="CreateTicketAction_IncidentProperties_GetDataError" >
                    <strong><%=Resources.ESIWebContent.Error_Quering_SN_Instance %></strong>
                    <%=Resources.ESIWebContent.ServiceNow_Instance_Offline_Or_Not_Configured %>
                </p>

                <p esi-show="showNotification" class="esi-warning esi-cloak" automation="CreateTicketAction_IncidentProperties_NoLongerValid">
                    <strong><%=Resources.ESIWebContent.Some_Values_Are_No_Longer_Valid %></strong>
                    <%=Resources.ESIWebContent.Please_Configure_Incident_Properties %>
                </p>

                <p class="esi-suggestion esi-cloak" esi-show="showEditingInfo" automation="Editing_Instance_Info_Box">
                    <%=Resources.ESIWebContent.EditingActionInfo %>
                </p>

                <p class="esi-suggestion esi-cloak" esi-show="hasIntegrationAction" automation="Has_Integration_Action_Warning">
                    <%=Resources.ESIWebContent.Another_Action_Defined_Warning %>
                </p>

                <p class="esi-suggestion esi-cloak" esi-show="resetTab" automation="Reset_Tab_Warning">
                    <%=Resources.ESIWebContent.Action_On_Reset_Warning %>
                </p>
            </div>
        </div>
        <h3 id="incidentPropertiesSection" automation="CreateTicketAction_IncidentProperties_Title"><%=Resources.ESIWebContent.Configure_Incident_Properties %></h3>
        <div id="CreateTicketActionFields" class="section required esi-panel">
        </div>

        <h3 automation="CreateTicketAction_ResetProperties_Title"><%=Resources.ESIWebContent.Configure_Reset_Properties %></h3>
        <div id="CreateTicketActionResetFields" class="section required esi-panel">
            <div id="IncidentStatus" class="esi-control-group esi-condensed">
                <label automation="CreateTicketAction_Integration_Details_ResetState_Label"><%= Resources.ESIWebContent.Integration_Details_Status_Label %></label>

                <select automation="CreateTicketAction_Integration_Details_ResetState_Select" esi-bind="_resetState" esi-items="statuses" esi-items-spec='{"value":"value", "display":"name"}'  ></select>
                <p><%= Resources.ESIWebContent.Integration_Details_Status_Hint %></p>
            </div>
            <div id="IncidentReopenStatus" class="esi-control-group esi-condensed">
                <label automation="CreateTicketAction_Integration_Details_ReopenState_Label"><%= Resources.ESIWebContent.Integration_Details_Reopen_Status_Label %></label>
                
                <select automation="CreateTicketAction_Integration_Details_ReopenState_Select" esi-bind="_reopenState" esi-items="statuses" esi-items-spec='{"value":"value", "display":"name"}'  ></select>
                <p><%= Resources.ESIWebContent.Integration_Details_Reopen_Status_Hint %></p>
            </div>
            <div id="IncidentAcknowledgeStatus" class="esi-control-group esi-condensed">
                <label automation="CreateTicketAction_Integration_Details_AcknowledgeState_Label"><%= Resources.ESIWebContent.Integration_Details_Acknowledge_Status_Label %></label>
                
                <select automation="CreateTicketAction_Integration_Details_AcknowledgeState_Select" esi-bind="_acknowledgeState" esi-items="statuses" esi-items-spec='{"value":"value", "display":"name"}'  ></select>
                <p><%= Resources.ESIWebContent.Integration_Details_Acknowledge_Status_Hint %></p>
            </div>
            <div id="IncidentCloseCode" class="esi-control-group esi-condensed">
                <label automation="CreateTicketAction_Integration_Details_CloseCode_Label"><%= Resources.ESIWebContent.Integration_Details_CloseCode_Label %></label>

                    <select automation="CreateTicketAction_Integration_Details_CloseCode_Select" esi-bind="_closeCode" esi-items="closeCodes" esi-items-spec='{"value":"value", "display":"name"}' ></select>
                    <p><%= Resources.ESIWebContent.Integration_Details_CloseCode_Hint %></p>
            </div>
            <div id="IncidentCloseNotes" class="esi-control-group esi-condensed">
                <label automation="EditServiceNowInstance_Integration_Details_CloseNotes_Label"><%= Resources.ESIWebContent.Integration_Details_CloseNotes_Label %></label>
                    <textarea automation="EditServiceNowInstance_Integration_Details_CloseNotes_Text" cols="55" rows="5" esi-bind="_closeNote"></textarea>
                    <p automation="EditServiceNowInstance_Integration_Details_CloseNotes_Hint"><%= Resources.ESIWebContent.Integration_Details_CloseNotes_Hint %></p>
            </div>
            <div id="IncidentAdditionalComments" class="esi-control-group esi-condensed">
                <label automation="EditServiceNowInstance_Integration_Details_AdditionalComments_Label"><%= Resources.ESIWebContent.Integration_Details_AdditionalComments_Label%><span> <%= Resources.ESIWebContent.Integration_Details_AdditionalComments_Label_Subtext %></span></label>
                    <textarea automation="EditServiceNowInstance_Integration_Details_AdditionalComments_Text" cols="55" rows="5" esi-bind="_resetComment"></textarea>
                    <p automation="EditServiceNowInstance_Integration_Details_CloseNotes_Hint"><%= Resources.ESIWebContent.Integration_Details_AdditionalComments_Hint %></p>
            </div>
        </div>
    </div>

    <div runat="server" id="NoInstances" class="section" automation="CreateTicketAction_NoInstances_Panel">
        <h1 automation="CreateTicketAction_NoInstances_Panel_Title"><%=Resources.ESIWebContent.No_SN_Instances_Configured %></h1>
        <a target="_blank" automation="CreateTicketAction_NoInstances_Panel_Settings_Page_Link" class="link" href="/Orion/ESI/Admin/EditServiceNowInstance.aspx"><%=Resources.ESIWebContent.Configure_Instance %></a>
    </div>

</div>


<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {

        var viewContext = <%= ToJson(ViewContext) %>;
        var actionContext =  <%= ToJson(ActionDefinition) %>;
        if (!actionContext) {
            actionContext = <%= CreateActionDefinition() %>;
        };

        if ( !actionContext.TransitiveID ) {
                actionContext.TransitiveID = parseInt( Date.now() / 1000 );
        }
        
        var config = {
            actionDefinition : actionContext,
            viewContext :  viewContext ,
            environmentType : <%= ToJson(EnviromentType) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            isActionSupported: <%= IsActionSupported.ToString().ToLower() %>,
            severity: "<%= AlertSeverity %>",
            insertVariableLabel: "<%= Resources.ESIWebContent.Insert_Variable_Label %>"
        };

        config.hasIntegrationAction = function() {
            if (!actionContext) {
                return false;
            };
            return !actionContext.ID &&
                _.find(SW.Core.Actions.ActionsEditor.getActions(), function (item) { return item.ActionTypeID === actionContext.ActionTypeID && item.TransitiveID !== actionContext.TransitiveID; }) !== undefined;
        };

        config.onReadyCallback = function(plugin) {

            <%= OnReadyJsCallback %>(plugin);
        }; 

        var controller = new SW.Core.Actions.CreateTicketController(config);
        SW.Core.MacroVariablePickerController.BindPicker();
    })();

</script>
