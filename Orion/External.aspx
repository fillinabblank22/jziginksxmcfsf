<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="External.aspx.cs" Inherits="Orion_External" Title="Untitled Page" %>
<%@ Import Namespace="System.Web" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" runat="server">
	<script type="text/javascript">
	    //<![CDATA[
		$(function() {
			var resize = function() {
				$("#external").height($(window).height() - $("#external").offset().top - parseInt($("#content").css("padding-bottom"), 10));
			};
			$(window).resize(resize);
			resize(); // set up initial size
        });
    //]]>
	</script>
	<style type="text/css">
		html { overflow: hidden; /* hide disabled vertical scrollbar in IE */ }
	</style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<h1 style="display:<%= site.FullTitle.Length==0?"none":"block" %>"><%=DefaultSanitizer.SanitizeHtml(this.site.FullTitle)%></h1>
	<iframe id="external" src="<%= SanitizedURL(this.site.URL) %>" width="100%" frameborder="0"></iframe> <%--fix for chrome--%>
</asp:Content>
