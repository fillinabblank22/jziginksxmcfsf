﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Cirrus.Controls;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.Orion.Web.DAL;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NCM_UploadConfig : Page
{
    private readonly AdvNodes nodesTree = new AdvNodes();
    private Dictionary<string, string> allGroups;
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_UploadConfig()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    private List<Guid> SelectedNodeIds
    {
        get
        {
            if (ViewState["SelectedNodeIds"] == null)
            {
                return null;
            }

            return (List<Guid>)ViewState["SelectedNodeIds"];
        }
        set
        {
            ViewState.Add("SelectedNodeIds", value);

        }
    }

    protected string CloseWindowHandlerUrl
    {
        get
        {
            return UrlHelper.ToSafeUrlParameter(@"/Orion/NCM/CloseWindowHandler.aspx");
        }
    }

    protected string NcmNodeIds { get; set; }

    /// <summary>
    /// Gets enable multiple config type upload.
    /// </summary>
    protected bool EnableMultipleConfigTypeUpload
    {
        get
        {
            if (ViewState["NCM_EnableMultipleConfigTypeUpload"] == null)
            {
                var isMultipleConfigTypeUpload = SelectedNodeIds.Count == 1 && commonHelper.IsMultipleConfigTypeUpload(SelectedNodeIds[0]);
                ViewState["NCM_EnableMultipleConfigTypeUpload"] = isMultipleConfigTypeUpload; 
                return isMultipleConfigTypeUpload;
            }

            return Convert.ToBoolean(ViewState["NCM_EnableMultipleConfigTypeUpload"]);
        }
    }

    protected bool EnableConfigTypeDropdownForIndirectTransferProtocol
    {
        get
        {
            return commonHelper.IsIndirectTransferProtocol(SelectedNodeIds, HttpContext.Current.Profile.UserName);
        }
    }

    #region Page life cycle

    protected override void OnInit(EventArgs e)
    {
        if (!SecurityHelper.IsPermissionExist(SecurityHelper._canUpload) && !CommonHelper.IsDemoMode())
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_IC_173}");
        }

        base.OnInit(e);

        panelConfigTree.Controls.Add(nodesTree);
        InitializeNodesTree();
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"UltraWebTree_PreserveSelection", @"UltraWebTree_PreserveSelection();", true);
        
        if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
        {
            btnUpload.Text = Resources.NCMWebContent.WEBDATA_IC_233;
        }
        else
        {
            btnUpload.Text = Resources.NCMWebContent.WEBDATA_IA_01;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (hfIsBinary.Value == @"true")
        {
            txtConfig.Attributes[@"disabled"] = @"true";
            chkSaveToFile.Attributes[@"hidden"] = @"true";
        }

        string[] groupByArray;

        Page.Title = Resources.NCMWebContent.WEBDATA_IA_06;
        
        if (!Page.IsPostBack)
        {
            groupByArray = WebUserSettingsDAL.Get(@"NCM_UploadConf_SelectedGroups") == null
                ? new[] {@"Vendor"}
                : WebUserSettingsDAL.Get(@"NCM_UploadConf_SelectedGroups").Split(',');
            InitDropDownItems(groupByArray);
        }
        else
        {
            groupByArray = GetGroupByArray();
            WebUserSettingsDAL.Set(@"NCM_UploadConf_SelectedGroups", string.Join(@",", groupByArray));
        }

        InitializeSelectNodesControl();
        VerifyEnableMultipleConfigTypeUpload();
        InitDropDownItems(groupByArray);
        InitNodes(false);
    }

    #endregion

    #region Controls event handlers

    protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitNodes(true);
    }

    private void VerifyEnableMultipleConfigTypeUpload()
    {
        if (EnableConfigTypeDropdownForIndirectTransferProtocol || EnableMultipleConfigTypeUpload)
        {
            var ddlConfigTypes = new SolarWinds.NCMModule.Web.Resources.Controls.ConfigTypes(SelectedNodeIds) {ID = @"ddlConfigTypes"};
            pnlAdvancedConfig.Controls.Add(ddlConfigTypes);
            chkWritetoNVRAM.Visible = false;
            pnlAdvancedConfig.FindControl(@"lblConfigType").Visible = true;
        }
    }

    #endregion

    #region Additional methods

    private void BuildGroupByList()
    {
        allGroups = new Dictionary<string, string>
        {
            {@"none", Resources.NCMWebContent.WEBDATA_IA_17},
            {@"NodeGroup", Resources.NCMWebContent.WEBDATA_VK_273},
            {@"Status", Resources.NCMWebContent.WEBDATA_VK_274},
            {@"Contact", Resources.NCMWebContent.WEBDATA_VK_275},
            {@"Location", Resources.NCMWebContent.WEBDATA_VK_276},
            {@"SysObjectID", Resources.NCMWebContent.WEBDATA_VK_277},
            {@"Vendor", Resources.NCMWebContent.WEBDATA_VK_278},
            {@"MachineType", Resources.NCMWebContent.WEBDATA_VK_279},
            {@"IOSImage", Resources.NCMWebContent.WEBDATA_VK_280},
            {@"IOSVersion", Resources.NCMWebContent.WEBDATA_VK_281},
            {@"ConfigTypes", Resources.NCMWebContent.WEBDATA_VK_282},
            {@"EnableLevel", Resources.NCMWebContent.WEBDATA_VK_283},
            {@"ExecProtocol", Resources.NCMWebContent.WEBDATA_VK_284},
            {@"CommandProtocol", Resources.NCMWebContent.WEBDATA_VK_285},
            {@"LoginStatus", Resources.NCMWebContent.WEBDATA_VK_287}
        };

        var customPropList = GetCustomProperties();

        foreach (var customProperty in customPropList)
        {
            allGroups.Add(customProperty, customProperty);
        }
    }

    private void InitDropDownItems(string[] groupByValues)
    {
        BuildGroupByList();
        ddlGroupSecond.Items.Clear();
        ddlGroupThird.Items.Clear();
        ddlGroupFirst.Items.Clear();

        switch (groupByValues.Length)
        {
            case 0:
                BuildSingleDropDownItemsList(ddlGroupFirst, @"none", Resources.NCMWebContent.WEBDATA_IA_09, null, null);
                ddlGroupSecond.Visible = false;
                ddlGroupThird.Visible = false;
                break;
            case 1:
                BuildSingleDropDownItemsList(ddlGroupFirst, groupByValues[0], null, null, null);
                if (!string.IsNullOrEmpty(groupByValues[0]))
                {
                    BuildSingleDropDownItemsList(ddlGroupSecond, @"none", Resources.NCMWebContent.WEBDATA_IA_10, groupByValues[0], null);
                }
                else
                {
                    ddlGroupSecond.Visible = false;
                }

                ddlGroupThird.Visible = false;
                break;
            case 2:
                BuildSingleDropDownItemsList(ddlGroupFirst, groupByValues[0], null, groupByValues[1], null);
                BuildSingleDropDownItemsList(ddlGroupSecond, groupByValues[1], null, groupByValues[0], null);
                BuildSingleDropDownItemsList(ddlGroupThird, @"none", Resources.NCMWebContent.WEBDATA_IA_11, groupByValues[0], groupByValues[1]);
                break;
            case 3:
                BuildSingleDropDownItemsList(ddlGroupFirst, groupByValues[0], null, groupByValues[1], groupByValues[2]);
                BuildSingleDropDownItemsList(ddlGroupSecond, groupByValues[1], null, groupByValues[0], groupByValues[2]);
                BuildSingleDropDownItemsList(ddlGroupThird, groupByValues[2], null, groupByValues[0], groupByValues[1]);
                break;
        }
    }

    private void BuildSingleDropDownItemsList(DropDownList dropDownList, string selectedValue, string selectedText, string exclusion1, string exclusion2)
    {
        dropDownList.Visible = true;
        dropDownList.Items.Clear();

        foreach (var item in allGroups)
        {
            if (!item.Key.Equals(exclusion1, StringComparison.InvariantCultureIgnoreCase) && !item.Key.Equals(exclusion2, StringComparison.InvariantCultureIgnoreCase))
            {
                dropDownList.Items.Add(new ListItem(item.Value, item.Key));
            }
        }

        dropDownList.SelectedValue = selectedValue;
        if (selectedText != null)
        {
            dropDownList.SelectedItem.Text = HttpUtility.HtmlEncode(selectedText);
        }
    }

    private string[] GetGroupByArray()
    {
        List<string> items = new List<string>();
        if (ddlGroupFirst.SelectedValue.Equals(@"none", StringComparison.InvariantCultureIgnoreCase) ||
            ddlGroupFirst.SelectedValue.Equals("", StringComparison.InvariantCultureIgnoreCase))
        {
            return items.ToArray();
        }
        items.Add(ddlGroupFirst.SelectedValue);

        if (ddlGroupSecond.SelectedValue.Equals(@"none", StringComparison.InvariantCultureIgnoreCase) ||
            ddlGroupSecond.SelectedValue.Equals("", StringComparison.InvariantCultureIgnoreCase))
        {
            return items.ToArray();
        }
        items.Add(ddlGroupSecond.SelectedValue);

        if (ddlGroupThird.SelectedValue.Equals(@"none", StringComparison.InvariantCultureIgnoreCase) ||
            ddlGroupThird.SelectedValue.Equals("", StringComparison.InvariantCultureIgnoreCase))
        {
            return items.ToArray();
        }
        items.Add(ddlGroupThird.SelectedValue);
        return items.ToArray();
    }

    private void InitNodes(bool forceRefresh)
    {
        var groupByArray = GetGroupByArray();
        nodesTree.Grouping.Clear();

        for (var i = 0; i < groupByArray.Length; i++)
        {
            nodesTree.Grouping.Add(i + 1, groupByArray[i]);
        }

        if (forceRefresh)
        {
            nodesTree.Refresh();
        }
        else if (!Page.IsPostBack)
        {
            nodesTree.Refresh();
        }
    }

    private void InitializeNodesTree()
    {
        nodesTree.ID = @"nodesTree";
        nodesTree.ShowConfigs = true;
        nodesTree.RenderAnchors = false;
        nodesTree.ShowGroupCheckBoxes = false;
        nodesTree.ShowNodeCheckBoxes = false;
        nodesTree.TreeControl.RenderAnchors = false;
        nodesTree.ShowNodesCount = true;
        nodesTree.TreeControl.ClientSideEvents.BeforeNodeSelectionChange = @"UltraWebTree_NodeSelectionChange";        
    }

    private List<string> GetCustomProperties()
    {
        var customPropList = CustomPropertyHelper.GetCustomProperties();
        return customPropList;
    }

    private void InitializeSelectNodesControl()
    {
        if (SelectedNodeIds == null)
        {
            var postData = Server.UrlDecode(Request.Form[@"ncmNodeIds"]);
            if (!string.IsNullOrEmpty(postData))
            {
                NcmNodeIds = postData;
                JavaScriptSerializer js = new JavaScriptSerializer();
                SelectedNodeIds = new List<Guid>(js.Deserialize<Guid[]>(Server.UrlDecode(postData)));
            }
            else
            {
                NcmNodeIds = @"[]";
                SelectedNodeIds = new List<Guid> { Guid.Empty };
            }
        }

        selectNodesCntrl.AssignedNodes = SelectedNodeIds;
    }

    #endregion
}