﻿<%@ WebHandler Language="C#" Class="ConfigurationManagementTreeProvider" %>

using System;
using System.Globalization;
using System.Linq;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;

public class ConfigurationManagementTreeProvider : IHttpHandler, IRequiresSessionState
{
    private readonly Log _log = new Log();
    
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = @"text/plain";

        var level = Convert.ToInt32(context.Request.Params[@"level"]);
        var grouping = context.Request.Params[@"grouping"];
        var groupingQueryString = context.Request.Params[@"groupingQueryString"];

        var response = new StringBuilder();
        response.Append(@"[");
        var nodes = GetAsyncTreeNodes(level, grouping, groupingQueryString);
        if (nodes != null && nodes.Count > 0)
        {
            var first = true;
            foreach (var node in nodes)
            {
                if (first)
                    first = false;
                else
                    response.Append(@",");
                response.Append(node);
            }
        }
        response.Append(@"]");
        
        context.Response.Write(response);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private List<AsyncTreeNode> GetAsyncTreeNodes(int level, string grouping, string groupingQueryString)
    {
        try
        {
            var nodes = new List<AsyncTreeNode>();
            
            var groupBy = GetGroupBy(grouping, level);
            if (string.IsNullOrEmpty(groupBy)) return nodes;
            
            var isLeaf = IsLeaf(grouping, level + 1);

            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                var propType = typeof(String);
                CustomProperty cp;
                if (CustomPropertyHelper.IsCustomProperty(groupBy, out cp))
                {
                    propType = cp.PropertyType;
                }

                var dt = proxy.Query($@"SELECT {GetSelectClause(groupBy, propType)} FROM Orion.Nodes AS Nodes 
                INNER JOIN Cirrus.NodeProperties AS NCMNodeProperties ON Nodes.NodeID=NCMNodeProperties.CoreNodeID 
                INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON OrionCustomProperties.NodeID=Nodes.NodeID
                WHERE {GetWhereClause(groupingQueryString)}
                GROUP BY {GetGroupByClause(groupBy, propType)}
                ORDER BY {GetOrderByClause(groupBy, propType)}");

                foreach (DataRow row in dt.Rows)
                {
                    var node = new AsyncTreeNode()
                    {
                        Level = level + 1,
                        Text = groupBy.Equals(@"Status", StringComparison.InvariantCultureIgnoreCase) ? CommonHelper.LookupNodeStatusText(Convert.ToString(row["theValue"])) : string.IsNullOrEmpty(Convert.ToString(row["theValue"])) ? Resources.NCMWebContent.NodeGroupText_Unknown : Convert.ToString(row["theValue"]),
                        GroupCount = Convert.ToInt32(row["theCount"]),
                        GroupBy = groupBy,
                        GroupByValue = string.IsNullOrEmpty(Convert.ToString(row["theValue"])) ? "" : Convert.ToString(row["theValue"]),
                        Leaf = isLeaf,
                        Icon = groupBy.Equals(@"Vendor", StringComparison.InvariantCultureIgnoreCase) ? Convert.ToString(row["VendorIcon"]) : string.Empty
                    };
                    nodes.Add(node);
                }
                return nodes;
            }
        }
        catch (Exception ex)
        {
            _log.Error(ex);
            return null;
        }
    }

    [Localizable(false)]
    private string GetWhereClause(string groupingQueryString)
    {
        if (string.IsNullOrEmpty(groupingQueryString)) return "(1=1)";

        var whereClause = new StringBuilder();

        var first = true;
        foreach (var queryParam in groupingQueryString.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries))
        {
            if (first)
                first = false;
            else
                whereClause.Append(" AND ");

            var groupBy = queryParam.Split(new char[] { '=' })[0];
            var groupByValue = HttpUtility.UrlDecode(queryParam.Split(new char[] { '=' })[1]);

            var propType = typeof(String);
            CustomProperty cp;
            if (CustomPropertyHelper.IsCustomProperty(groupBy, out cp))
            {
                propType = cp.PropertyType;
            }

            if (string.IsNullOrEmpty(groupByValue))
            {
                if (propType == typeof(String))
                {
                    whereClause.AppendFormat(" ({0}='' OR {0} IS NULL) ", groupBy);
                }
                else
                {
                    whereClause.AppendFormat(" {0} IS NULL ", groupBy);
                }
            }
            else
            {
                if (propType == typeof(DateTime))
                {
                    whereClause.AppendFormat(" {0}=DateTime('{1:o}') ", groupBy, GetDateTimeSafe(groupByValue));
                }
                else
                {
                    whereClause.AppendFormat(" {0}='{1}' ", groupBy, SearchHelper.EncodeToSafeSqlType(groupByValue));
                }
            }
        }
        return whereClause.ToString();
    }

    [Localizable(false)]
    private string GetSelectClause(string groupBy, Type propType)
    {
        return groupBy.Equals("Vendor", StringComparison.InvariantCultureIgnoreCase) ? $"{(propType == typeof(String) ? $"ISNULL({groupBy},'')" : groupBy)} AS theValue, COUNT(ISNULL({groupBy},'')) AS theCount, MAX(VendorIcon) AS VendorIcon"
            : $"{(propType == typeof(String) ? $"ISNULL({groupBy},'')" : groupBy)} AS theValue, COUNT(ISNULL({groupBy},'')) AS theCount";
    }

    [Localizable(false)]
    private string GetGroupByClause(string groupBy, Type propType)
    {
        return $"{(propType == typeof(String) ? $"ISNULL({groupBy},'')" : groupBy)}";
    }

    [Localizable(false)]
    private string GetOrderByClause(string groupBy, Type propType)
    {
        return $"{(propType == typeof(String) ? $"ISNULL({groupBy},'')" : groupBy)}";
    }

    private string GetGroupBy(string grouping, int level)
    {
        var groupBy = string.Empty;
        var groupingList = grouping.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        if (groupingList.Count > level)
        {
            groupBy = groupingList[level];
        }
        return groupBy;
    }

    private bool IsLeaf(string grouping, int nextLevel)
    {
        var groupingList = grouping.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        return (groupingList.Count == nextLevel);
    }

    private DateTime GetDateTimeSafe(string date)
    {
        DateTime result;
        if (DateTime.TryParse(date, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
        {
            return result;
        }
        else { throw new FormatException(date); }
    }

    private class AsyncTreeNode
    {
        private const string iconPathTemplate = "/NetPerfMon/images/Vendors/{0}";

        public int Level { get; set; }
        public string Text { get; set; }
        public int GroupCount { get; set; }
        public string GroupBy { get; set; }
        public string GroupByValue { get; set; }
        public bool Leaf { get; set; }
        public string Icon { get; set; }
        public string IconCls { get; set; }

        public override string ToString()
        {
            var node = new StringBuilder();
            node.Append(@"{");
            node.AppendFormat(@"level: {0},", Level);
            node.AppendFormat(@"text: '{0} ({1})',", HttpUtility.JavaScriptStringEncode(HttpUtility.HtmlEncode(Text)), GroupCount);
            node.AppendFormat(@"group: '{0}',", HttpUtility.JavaScriptStringEncode(HttpUtility.HtmlEncode(Text)));
            node.AppendFormat(@"groupBy: '{0}',", GroupBy);
            node.AppendFormat(@"groupByValue: '{0}',", HttpUtility.UrlEncode(GroupByValue));
            node.AppendFormat(@"leaf: {0},", Leaf.ToString().ToLowerInvariant());
            node.AppendFormat(@"icon: '{0}',", string.IsNullOrEmpty(Icon) ? string.Empty : string.Format(iconPathTemplate, Icon));
            node.AppendFormat(@"iconCls: '{0}'", string.IsNullOrEmpty(Icon) ? @"tree-node-no-icon" : string.Empty);
            node.Append(@"}");
            return node.ToString();
        }
    }
}