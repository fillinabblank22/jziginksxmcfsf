﻿<%@ WebService Language="C#" Class="ChartData" %>

using System;
using System.Collections.Generic;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.Orion.Web.Charting.v2;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.NCM.Contracts.Baseline;
using SolarWinds.NCMModule.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ChartData : ChartDataWebService
{
    private static readonly string drillDownUrl = @"/Orion/NCM/ChartDrilldown.aspx?ChartID={0}&ResourceID={1}";

    private readonly ILogger log;

    public ChartData()
    {
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults EnabledEWNodesChartData(ChartDataRequest request)
    {
        try
        {
            var dataSeriesList = new List<DataSeries>();
            var isNpmExist = (bool) request.AllSettings[@"isNPMExist"];

            var resourceId = int.Parse(request.AllSettings[@"resourceID"].ToString());

            var urlRedirect =
                $@"/Orion/NCM/Resources/EnergyWise/EWChartDrilldown.aspx?ResourceID={resourceId}&Drilldown=True";

            if (!isNpmExist)
            {
                return new ChartDataResults(dataSeriesList);
            }

            var chartSwql = @"
            SELECT COUNT(Nodes.NodeID) AS Value 
            FROM Orion.NPM.EW.Readiness AS NpmEwReadiness INNER JOIN Orion.Nodes AS Nodes
            ON NpmEwReadiness.NodeID = Nodes.NodeID
            WHERE NpmEwReadiness.HasEnergyWise = 1
            UNION ALL
            (
            SELECT COUNT(Nodes.NodeID) AS Value 
            FROM Orion.NPM.EW.Readiness AS NpmEwReadiness INNER JOIN Orion.Nodes AS Nodes
            ON NpmEwReadiness.NodeID = Nodes.NodeID
            WHERE NpmEwReadiness.HasEnergyWise = 0 
            AND NpmEwReadiness.HasRightIOS = 1 
            AND NpmEwReadiness.IsEnergyWiseCapable = 1
            )";

            var dt = ExecuteSWQL(chartSwql, null);
            var dataSeries = new DataSeries {TagName = @"Default"};
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["Value"]) > 0 || Convert.ToInt32(dt.Rows[1]["Value"]) > 0)
                {
                    var notEnabledNodesDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {
                                @"name",
                                string.Format(Resources.NCMWebContent.WEBDATA_VK_261,
                                    Convert.ToInt32(dt.Rows[0]["Value"]))
                            },
                            {@"y", Convert.ToInt32(dt.Rows[0]["Value"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["Value"])},
                            {@"urlredirect", urlRedirect},
                            {@"color", @"#77BD2D"}
                        });

                    dataSeries.Data.Add(notEnabledNodesDataPoint);

                    var notEnabledEnergyWiseCapableDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {
                                @"name",
                                string.Format(Resources.NCMWebContent.WEBDATA_VK_262,
                                    Convert.ToInt32(dt.Rows[1]["Value"]))
                            },
                            {@"y", Convert.ToInt32(dt.Rows[1]["Value"])},
                            {@"count", Convert.ToInt32(dt.Rows[1]["Value"])},
                            {@"urlredirect", urlRedirect},
                            {@"color", @"#FCD928"}
                        });

                    dataSeries.Data.Add(notEnabledEnergyWiseCapableDataPoint);
                    dataSeriesList.Add(dataSeries);
                }
            }

            return new ChartDataResults(dataSeriesList);
        }
        catch (Exception ex)
        {
            log.Error("Error getting EnabledEWNodesChartData.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults ConfigurationChangesSnapshotChartData(ChartDataRequest request)
    {
        try
        {
            var dataSeriesList = new List<DataSeries>();
            ChartDataResults chartDataResults = null;
            var chartSWQL = string.Empty;

            var parameters = new Dictionary<string, object>();
            parameters =
                (Dictionary<string, object>) request.AllSettings[
                    @"NCM_ConfigurationChangesSnapshotChartDataParameters"];

            var isLastUpdate = (bool) parameters[@"isLastUpdate"];
            var limitationID = parameters[@"limitationID"].ToString();
            var criteria = parameters[@"criteria"].ToString();
            int comparisonType = Convert.ToInt16(parameters[@"comparisonType"]);

            var chartID = int.Parse(parameters[@"chartID"].ToString());
            var resourceID = int.Parse(parameters[@"resourceID"].ToString());

            var urlredirect = string.Format(drillDownUrl, chartID, resourceID);

            var startDate = ParseDateTime(parameters[@"@startDate"]);
            var endDate = ParseDateTime(parameters[@"@endDate"]);
            var filterClause =
                (parameters.ContainsKey(@"FilterClause") &&
                 !string.IsNullOrEmpty(parameters[@"FilterClause"].ToString()))
                    ? parameters[@"FilterClause"].ToString()
                    : @"1=1";

            var lastUpdateChartSWQLQueries =
                @"SELECT COUNT(C.NodeID) AS NodesCount FROM Cirrus.LatestComparisonResults AS C 
                JOIN Cirrus.NodeProperties AS N ON C.NodeID = N.NodeID join Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE C.ComparisonType={0} AND C.DiffFlag=1 {1} AND ({3})
                UNION ALL
                (

                SELECT COUNT(C.NodeID) AS NodesCount FROM Cirrus.LatestComparisonResults AS C 
                JOIN Cirrus.NodeProperties AS N ON C.NodeID = N.NodeID join Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE C.ComparisonType={0} AND C.DiffFlag=0 {1} AND ({3})
                )
                UNION ALL
                (
                SELECT COUNT(N.NodeID) AS NodesCount FROM Cirrus.NodeProperties AS N 
                JOIN Orion.Nodes as O on N.CoreNodeID=O.NodeID
                WHERE N.NodeID NOT IN (SELECT C.NodeID FROM Cirrus.LatestComparisonResults AS C 
                JOIN Cirrus.NodeProperties AS N1 ON C.NodeID = N1.NodeID WHERE C.ComparisonType={0} {1}) AND {3}
                )                   
                with limitation {2}";

            var timeRangeChartSWQLQueries =
                @"SELECT COUNT(DISTINCT C.NodeID) AS NodesCount FROM Cirrus.CacheDiffResults AS C
                JOIN Cirrus.ComparisonCache AS CC ON C.CacheID = CC.CacheID 
                JOIN Cirrus.NodeProperties AS N ON N.NodeID = C.NodeID 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE ToUtc(CC.TimeStamp) > @startDate AND ToUtc(CC.TimeStamp) < @endDate 
                AND C.ComparisonType={0} AND C.DiffFlag=1 {1} AND ({3})                
                UNION ALL
                (
                SELECT COUNT(DISTINCT C.NodeID) AS NodesCount FROM Cirrus.CacheDiffResults AS C
                JOIN Cirrus.ComparisonCache AS CC ON C.CacheID = CC.CacheID 
                JOIN Cirrus.NodeProperties AS N ON N.NodeID = C.NodeID 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE ToUtc(CC.TimeStamp) > @startDate AND ToUtc(CC.TimeStamp) < @endDate 
                AND C.ComparisonType={0} AND C.DiffFlag=0 {1}
                AND N.NodeID NOT IN
                (SELECT DISTINCT C.NodeID FROM Cirrus.CacheDiffResults AS C
                JOIN Cirrus.ComparisonCache AS CC ON C.CacheID = CC.CacheID 
                WHERE ToUtc(CC.TimeStamp) > @startDate AND ToUtc(CC.TimeStamp) < @endDate 
                AND C.ComparisonType={0} AND C.DiffFlag=1 {1}) AND ({3})
                )

                 UNION ALL
                (
                SELECT COUNT(N.NodeID) AS NodesCount FROM Cirrus.NodeProperties AS N 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE N.NodeID NOT IN 
                (SELECT DISTINCT C.NodeID FROM Cirrus.CacheDiffResults AS C 
                JOIN Cirrus.ComparisonCache AS CC ON C.CacheID = CC.CacheID 
                JOIN Cirrus.NodeProperties AS N1 ON N1.NodeID = C.NodeID 
                WHERE ToUtc(CC.TimeStamp) > @startDate AND ToUtc(CC.TimeStamp) < @endDate 
                AND C.ComparisonType={0} {1}) AND ({3})
                )
                with limitation {2}";


            chartSWQL = isLastUpdate
                ? string.Format(lastUpdateChartSWQLQueries, comparisonType, criteria, limitationID, filterClause)
                : string.Format(timeRangeChartSWQLQueries, comparisonType, criteria, limitationID, filterClause);

            var propertyBag = new PropertyBag();
            propertyBag.Add(@"startDate", startDate);
            propertyBag.Add(@"endDate", endDate);

            var dt = ExecuteSWQL(chartSWQL, propertyBag);
            var dataSeries = new DataSeries();
            dataSeries.TagName = @"Default";
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["NodesCount"]) > 0 || Convert.ToInt32(dt.Rows[1]["NodesCount"]) > 0 ||
                    Convert.ToInt32(dt.Rows[2]["NodesCount"]) > 0)
                {
                    DataPoint changedDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_195},
                            {@"y", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=conflict", urlredirect)},
                            {@"color", @"#FCD928"}
                        });

                    dataSeries.Data.Add(changedDataPoint);

                    DataPoint unchangedDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_196},
                            {@"y", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=noconflict", urlredirect)},
                            {@"color", @"#77BD2D"}
                        });

                    dataSeries.Data.Add(unchangedDataPoint);

                    DataPoint unknownDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_197},
                            {@"y", Convert.ToInt32(dt.Rows[2]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[2]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=unknown", urlredirect)},
                            {@"color", @"#7D7D7D"}
                        });

                    dataSeries.Data.Add(unknownDataPoint);
                    dataSeriesList.Add(dataSeries);
                }
            }

            chartDataResults = new ChartDataResults(dataSeriesList);

            return chartDataResults;
        }
        catch (Exception ex)
        {
            log.Error("Error getting ConfigurationChangesSnapshotChartData.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults RunningVsStartupChartData(ChartDataRequest request)
    {
        try
        {
            var dataSeriesList = new List<DataSeries>();
            ChartDataResults chartDataResults = null;
            var chartSWQL = string.Empty;

            var parameters = new Dictionary<string, object>();
            parameters = (Dictionary<string, object>) request.AllSettings[@"NCM_RunningVsStartupChartDataParameters"];

            var isLastUpdate = (bool) parameters[@"isLastUpdate"];
            var limitationID = parameters[@"limitationID"].ToString();

            var startDate = ParseDateTime(parameters[@"@startDate"]);
            var endDate = ParseDateTime(parameters[@"@endDate"]);
            var filterClause =
                (parameters.ContainsKey(@"FilterClause") &&
                 !string.IsNullOrEmpty(parameters[@"FilterClause"].ToString()))
                    ? parameters[@"FilterClause"].ToString()
                    : @"1=1";

            var chartID = int.Parse(parameters[@"chartID"].ToString());
            var resourceID = int.Parse(parameters[@"resourceID"].ToString());
            var urlredirect = string.Format(drillDownUrl, chartID, resourceID);

            var lastUpdateChartSWQLQueries =
                @"SELECT COUNT(C.NodeID) AS NodesCount FROM Cirrus.LatestComparisonResults AS C 
                JOIN Cirrus.NodeProperties AS N ON C.NodeID = N.NodeID 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE C.ComparisonType=1 AND C.DiffFlag=1 AND ({0})

                UNION ALL
                (
                SELECT COUNT(C.NodeID) AS NodesCount FROM Cirrus.LatestComparisonResults AS C 
                JOIN Cirrus.NodeProperties AS N ON C.NodeID = N.NodeID 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE C.ComparisonType=1 AND C.DiffFlag=0 AND ({0})
                )

                UNION ALL
                (
                SELECT COUNT(N.NodeID) AS NodesCount FROM Cirrus.NodeProperties AS N 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE N.NodeID NOT IN (SELECT C.NodeID FROM Cirrus.LatestComparisonResults AS C 
                JOIN Cirrus.NodeProperties AS N1 ON C.NodeID = N1.NodeID WHERE C.ComparisonType=1)  AND ({0})
                )
                with limitation {VL}";

            var timeRangeChartSWQLQueries =
                @"SELECT COUNT(DISTINCT CacheDiffResults.NodeID) AS NodesCount, '#E61929' AS Color  FROM Cirrus.CacheDiffResults AS CacheDiffResults
                JOIN Cirrus.ComparisonCache AS ComparisonCache ON CacheDiffResults.CacheID = ComparisonCache.CacheID 
                JOIN Cirrus.NodeProperties AS N ON N.NodeID = CacheDiffResults.NodeID 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE ToUtc(ComparisonCache.TimeStamp) > @startDate AND ToUtc(ComparisonCache.TimeStamp) < @endDate 
                AND CacheDiffResults.ComparisonType=1 AND CacheDiffResults.DiffFlag=1 AND ({0})

                UNION ALL
                (
                SELECT COUNT(DISTINCT CacheDiffResults.NodeID) AS NodesCount, '#BD2919' AS Color  FROM Cirrus.CacheDiffResults AS CacheDiffResults
                JOIN Cirrus.ComparisonCache AS ComparisonCache ON CacheDiffResults.CacheID = ComparisonCache.CacheID 
                JOIN Cirrus.NodeProperties AS N ON N.NodeID = CacheDiffResults.NodeID 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE ToUtc(ComparisonCache.TimeStamp) > @startDate AND ToUtc(ComparisonCache.TimeStamp) < @endDate 
                AND CacheDiffResults.ComparisonType=1 AND CacheDiffResults.DiffFlag=0 
                AND CacheDiffResults.NodeID NOT IN
                (SELECT DISTINCT CacheDiffResults.NodeID FROM Cirrus.CacheDiffResults AS CacheDiffResults
                JOIN Cirrus.ComparisonCache AS ComparisonCache ON CacheDiffResults.CacheID = ComparisonCache.CacheID 
                WHERE ToUtc(ComparisonCache.TimeStamp) > @startDate AND ToUtc(ComparisonCache.TimeStamp) < @endDate 
                AND CacheDiffResults.ComparisonType=1 AND CacheDiffResults.DiffFlag=1 ) AND ({0})
                )
                UNION ALL
                (
                SELECT COUNT(N.NodeID) AS NodesCount, '#7D7D7D' AS Color FROM Cirrus.NodeProperties AS N 
                JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                WHERE N.NodeID NOT IN 
                (SELECT DISTINCT CacheDiffResults.NodeID FROM Cirrus.CacheDiffResults AS CacheDiffResults 
                JOIN Cirrus.ComparisonCache AS ComparisonCache ON CacheDiffResults.CacheID = ComparisonCache.CacheID 
                JOIN Cirrus.NodeProperties AS N1 ON N1.NodeID = CacheDiffResults.NodeID 
                WHERE ToUtc(ComparisonCache.TimeStamp) > @startDate AND ToUtc(ComparisonCache.TimeStamp) < @endDate 
                AND CacheDiffResults.ComparisonType=1 ) AND ({0})
                )
                with limitation {VL}";


            chartSWQL = isLastUpdate
                ? string.Format(lastUpdateChartSWQLQueries.Replace(@"{VL}", limitationID), filterClause)
                : string.Format(timeRangeChartSWQLQueries.Replace(@"{VL}", limitationID), filterClause);

            var propertyBag = new PropertyBag();
            propertyBag.Add(@"startDate", startDate);
            propertyBag.Add(@"endDate", endDate);

            var dt = ExecuteSWQL(chartSWQL, propertyBag);
            var dataSeries = new DataSeries();
            dataSeries.TagName = @"Default";
            if (dt.Rows.Count > 0)
            {

                if (Convert.ToInt32(dt.Rows[0]["NodesCount"]) > 0 || Convert.ToInt32(dt.Rows[1]["NodesCount"]) > 0 ||
                    Convert.ToInt32(dt.Rows[2]["NodesCount"]) > 0)
                {
                    DataPoint conflictsDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_206},
                            {@"y", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=conflict", urlredirect)},
                            {@"color", @"#E61929"}
                        });

                    dataSeries.Data.Add(conflictsDataPoint);

                    DataPoint noConflictsDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_207},
                            {@"y", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=noconflict", urlredirect)},
                            {@"color", @"#73BD29"}
                        });

                    dataSeries.Data.Add(noConflictsDataPoint);

                    DataPoint unknownDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_197},
                            {@"y", Convert.ToInt32(dt.Rows[2]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[2]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=unknown", urlredirect)},
                            {@"color", @"#7D7D7D"}
                        });

                    dataSeries.Data.Add(unknownDataPoint);
                    dataSeriesList.Add(dataSeries);
                }
            }

            chartDataResults = new ChartDataResults(dataSeriesList);

            return chartDataResults;
        }
        catch (Exception ex)
        {
            log.Error("Error getting RunningVsStartupChartData.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults BaselineVsRunningChartData(ChartDataRequest request)
    {
        try
        {
            var dataSeriesList = new List<DataSeries>();

            var parameters =
                (Dictionary<string, object>) request.AllSettings[@"NCM_BaselineVsRunningChartDataParameters"];

            var limitationId = parameters[@"limitationID"].ToString();

            var filterClause =
                (parameters.ContainsKey(@"FilterClause") &&
                 !string.IsNullOrEmpty(parameters[@"FilterClause"].ToString()))
                    ? parameters[@"FilterClause"].ToString()
                    : @"1=1";

            var chartId = int.Parse(parameters[@"chartID"].ToString());
            var resourceId = int.Parse(parameters[@"resourceID"].ToString());
            var urlredirect = string.Format(drillDownUrl, chartId, resourceId);


            var chartSwql = $@"SELECT 
                                  IsNull (SUM (case WHEN Violations.MismatchStatus=3 THEN 1 else 0 end),0) as Changed,
                                  IsNull (SUM (case WHEN Violations.MismatchStatus=1 THEN 1 else 0 end),0) as Unchanged,  
                                  IsNull (SUM (case WHEN Violations.MismatchStatus=0 OR Violations.MismatchStatus=2  THEN 1 else 0 end),0) as Unknown   
                           FROM
                                (
                                 SELECT BaselineNodeMap.NodeId,
                                        MAX (CASE 
                                                  WHEN BaselineNodeMap.CacheState={(int) BaselineCacheState.Error} THEN 2
                                                  WHEN BaselineNodeMap.CacheState={(int) BaselineCacheState.Cached} AND V.IsViolation=1 THEN 3
                                                  WHEN BaselineNodeMap.CacheState={(int) BaselineCacheState.Cached} AND V.IsViolation=0 THEN 1
                                             ELSE 0 END) as MismatchStatus
                                 FROM NCM.BaselineNodeMap as BaselineNodeMap
                                 JOIN NCM.Baselines as Baselines on BaselineNodeMap.BaselineId = Baselines.Id
                                 LEFT JOIN NCM.BaselineViolations as V on 
                                           BaselineNodeMap.NodeId=V.NodeId 
                                           AND BaselineNodeMap.BaselineId=V.BaselineId 
                                           AND BaselineNodeMap.ConfigType=V.ConfigType
                                 WHERE Baselines.Disabled = 0
                                 GROUP BY BaselineNodeMap.NodeId
                                ) as Violations
                          JOIN NCM.NodeProperties as N on N.NodeID= Violations.NodeId
                          JOIN Orion.Nodes as O on O.NodeID=N.CoreNodeID
                          WHERE {filterClause}
                          with limitation {limitationId}";


            var dt = ExecuteSWQL(chartSwql, null);
            var dataSeries = new DataSeries {TagName = @"Default"};
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["Changed"]) > 0 || Convert.ToInt32(dt.Rows[0]["Unchanged"]) > 0 ||
                    Convert.ToInt32(dt.Rows[0]["Unknown"]) > 0)
                {
                    DataPoint conflictsDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_211},
                            {@"y", Convert.ToInt32(dt.Rows[0]["Changed"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["Changed"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=conflict", urlredirect)},
                            {@"color", @"#E61929"}
                        });

                    dataSeries.Data.Add(conflictsDataPoint);

                    DataPoint noConflictsDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_212},
                            {@"y", Convert.ToInt32(dt.Rows[0]["Unchanged"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["Unchanged"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=noconflict", urlredirect)},
                            {@"color", @"#77BD2D"}
                        });

                    dataSeries.Data.Add(noConflictsDataPoint);

                    DataPoint unknownDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_197},
                            {@"y", Convert.ToInt32(dt.Rows[0]["Unknown"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["Unknown"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=unknown", urlredirect)},
                            {@"color", @"#7D7D7D"}
                        });

                    dataSeries.Data.Add(unknownDataPoint);
                    dataSeriesList.Add(dataSeries);
                }
            }

            var chartDataResults = new ChartDataResults(dataSeriesList);

            return chartDataResults;
        }
        catch (Exception ex)
        {
            log.Error("Error getting BaselineVsRunningChartData.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults DeviceInventoriedVsNotChartData(ChartDataRequest request)
    {
        try
        {
            var dataSeriesList = new List<DataSeries>();
            ChartDataResults chartDataResults = null;
            var chartSWQL = string.Empty;

            var parameters = new Dictionary<string, object>();
            parameters =
                (Dictionary<string, object>) request.AllSettings[@"NCM_DeviceInventoriedVsNoChartDataParameters"];

            var isLastUpdate = (bool) parameters[@"isLastUpdate"];
            var limitationID = parameters[@"limitationID"].ToString();

            var startDate = ParseDateTime(parameters[@"@startDate"]);
            var endDate = ParseDateTime(parameters[@"@endDate"]);
            var filterClause =
                (parameters.ContainsKey(@"FilterClause") &&
                 !string.IsNullOrEmpty(parameters[@"FilterClause"].ToString()))
                    ? parameters[@"FilterClause"].ToString()
                    : @"1=1";

            var chartID = int.Parse(parameters[@"chartID"].ToString());
            var resourceID = int.Parse(parameters[@"resourceID"].ToString());
            var urlredirect = string.Format(drillDownUrl, chartID, resourceID);

            var lastUpdateChartSWQLQueries =
                @"SELECT COUNT(N.NodeID) AS NodesCount, '#77BD2D' AS Color FROM Cirrus.NodeProperties AS N 
                                                        JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                                                            WHERE N.LastInventory <>-2 AND ({0})
                                                            UNION ALL
                                                                    (
                                                            SELECT COUNT(N.NodeID) AS NodesCount, '#FCD928' AS Color  FROM Cirrus.NodeProperties AS N 
                                                            JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                                                            WHERE N.NodeID NOT IN (SELECT N1.NodeID FROM Cirrus.NodeProperties AS N1
                                                            WHERE N1.LastInventory <>-2 ) AND ({0})) with limitation {VL}";

            var timeRangeChartSWQLQueries =
                @"SELECT COUNT(DISTINCT N.NodeID) AS NodesCount, '#77BD2D' AS Color FROM Cirrus.NodeProperties AS N 
                                            JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                                            WHERE ToUtc(N.LastInventory) >= @startDate AND ToUtc(N.LastInventory) <= @endDate  AND ({0})
                                                UNION ALL
                                                    (
                                            SELECT COUNT(N.NodeID) AS NodesCount, '#FCD928' AS Color  FROM Cirrus.NodeProperties AS N 
                                            JOIN Orion.Nodes as O on O.NodeId=N.CoreNodeId
                                            WHERE N.NodeID NOT IN (SELECT DISTINCT N1.NodeID FROM Cirrus.NodeProperties AS N1 
                                            WHERE ToUtc(N1.LastInventory) >= @startDate AND ToUtc(N1.LastInventory) <= @endDate)  AND ({0}))
                                            with limitation {VL}";



            chartSWQL = isLastUpdate
                ? string.Format(lastUpdateChartSWQLQueries.Replace(@"{VL}", limitationID), filterClause)
                : string.Format(timeRangeChartSWQLQueries.Replace(@"{VL}", limitationID), filterClause);

            var propertyBag = new PropertyBag();
            propertyBag.Add(@"startDate", startDate);
            propertyBag.Add(@"endDate", endDate);

            var dt = ExecuteSWQL(chartSWQL, propertyBag);
            var dataSeries = new DataSeries();
            dataSeries.TagName = @"Default";

            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["NodesCount"]) > 0 || Convert.ToInt32(dt.Rows[1]["NodesCount"]) > 0)
                {
                    DataPoint inventoriedDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_187},
                            {@"y", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=noconflict", urlredirect)},
                            {@"color", Convert.ToString(dt.Rows[0]["Color"])}
                        });

                    dataSeries.Data.Add(inventoriedDataPoint);

                    DataPoint notInventoriedDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_188},
                            {@"y", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=conflict", urlredirect)},
                            {@"color", Convert.ToString(dt.Rows[1]["Color"])}
                        });

                    dataSeries.Data.Add(notInventoriedDataPoint);
                    dataSeriesList.Add(dataSeries);
                }
            }

            chartDataResults = new ChartDataResults(dataSeriesList);

            return chartDataResults;
        }
        catch (Exception ex)
        {
            log.Error("Error getting DeviceInventoriedVsNotChartData.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults BackupVsNotBackedUpChartData(ChartDataRequest request)
    {
        try
        {
            var dataSeriesList = new List<DataSeries>();
            ChartDataResults chartDataResults = null;
            var chartSWQL = string.Empty;

            var parameters = new Dictionary<string, object>();
            parameters =
                (Dictionary<string, object>) request.AllSettings[@"NCM_DevicesBackedUpVsNotChartDataParameters"];

            var isLastUpdate = (bool) parameters[@"isLastUpdate"];
            var includeDiscarded = (bool) parameters[@"includeDiscarded"];
            var limitationID = parameters[@"limitationID"].ToString();

            var startDate = ParseDateTime(parameters[@"@startDate"]);
            var endDate = ParseDateTime(parameters[@"@endDate"]);
            var filterClause =
                (parameters.ContainsKey(@"FilterClause") &&
                 !string.IsNullOrEmpty(parameters[@"FilterClause"].ToString()))
                    ? parameters[@"FilterClause"].ToString()
                    : @"1=1";

            var chartID = int.Parse(parameters[@"chartID"].ToString());
            var resourceID = int.Parse(parameters[@"resourceID"].ToString());
            var urlredirect = string.Format(drillDownUrl, chartID, resourceID);

            const string lastUpdateChartSWQLQueries =
                @"SELECT COUNT(DISTINCT N.NodeID) AS NodesCount, '#77BD2D' AS Color FROM Cirrus.NodeProperties AS N 
            JOIN Orion.Nodes as O on O.NodeID=N.CoreNodeId
            INNER JOIN Cirrus.ConfigArchive AS C ON N.NodeID=C.NodeID AND ({0})   
            UNION ALL
	        (
            SELECT COUNT(N.NodeID) AS NodesCount, '#FCD928' AS Color FROM Cirrus.NodeProperties AS N 
            JOIN Orion.Nodes as O on O.NodeID=N.CoreNodeId
            WHERE N.NodeID NOT IN (SELECT DISTINCT C.NodeID FROM Cirrus.ConfigArchive AS C)  AND ({0})   
	        )
            WITH LIMITATION {VL}";

            const string timeRangeChartSWQLQueries =
                @"SELECT COUNT(DISTINCT N.NodeID) AS NodesCount, '#77BD2D' AS Color FROM Cirrus.NodeProperties AS N 
            JOIN Orion.Nodes as O on O.NodeID=N.CoreNodeId
            INNER JOIN Cirrus.ConfigArchive AS C ON N.NodeID=C.NodeID 
            WHERE ToUtc(C.DownloadTime) >= @startDate AND ToUtc(C.DownloadTime) <= @endDate  AND ({0})   
    
	        UNION ALL
	        (
            SELECT COUNT(N.NodeID) AS NodesCount, '#FCD928' AS Color FROM Cirrus.NodeProperties AS N 
            JOIN Orion.Nodes as O on O.NodeID=N.CoreNodeId
            WHERE N.NodeID NOT IN (SELECT DISTINCT C.NodeID FROM Cirrus.ConfigArchive AS C 
            WHERE ToUtc(C.DownloadTime) >= @startDate AND ToUtc(C.DownloadTime) <= @endDate)  AND ({0})   
	        )
            WITH LIMITATION {VL}";

            if (isLastUpdate)
            {
                chartSWQL = string.Format(lastUpdateChartSWQLQueries.Replace(@"{VL}", limitationID), filterClause);
            }
            else
            {
                if (includeDiscarded)
                {
                    chartSWQL = string.Format(
                        timeRangeChartSWQLQueries.Replace(@"DownloadTime", @"AttemptedDownloadTime")
                            .Replace(@"{VL}", limitationID), filterClause);
                }
                else
                {
                    chartSWQL = string.Format(timeRangeChartSWQLQueries.Replace(@"{VL}", limitationID), filterClause);
                }
            }


            var propertyBag = new PropertyBag();
            propertyBag.Add(@"startDate", startDate);
            propertyBag.Add(@"endDate", endDate);

            var dt = ExecuteSWQL(chartSWQL, propertyBag);
            var dataSeries = new DataSeries();
            dataSeries.TagName = @"Default";

            if (dt.Rows.Count > 0)
            {

                if (Convert.ToInt32(dt.Rows[0]["NodesCount"]) > 0 || Convert.ToInt32(dt.Rows[1]["NodesCount"]) > 0)
                {
                    DataPoint backedDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_216},
                            {@"y", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[0]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=noconflict", urlredirect)},
                            {@"color", Convert.ToString(dt.Rows[0]["Color"])}
                        });

                    dataSeries.Data.Add(backedDataPoint);

                    DataPoint notBackedDataPoint = DataPointWithOptions.CreatePointWithOptions(
                        new Dictionary<string, object>
                        {
                            {@"name", Resources.NCMWebContent.WEBDATA_VK_217},
                            {@"y", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"count", Convert.ToInt32(dt.Rows[1]["NodesCount"])},
                            {@"urlredirect", String.Format(@"{0}&groupByValue=conflict", urlredirect)},
                            {@"color", Convert.ToString(dt.Rows[1]["Color"])}
                        });

                    dataSeries.Data.Add(notBackedDataPoint);
                    dataSeriesList.Add(dataSeries);
                }
            }

            chartDataResults = new ChartDataResults(dataSeriesList);

            return chartDataResults;
        }
        catch (Exception ex)
        {
            log.Error("Error getting BackupVsNotBackedUpChartData.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults BackupVsAllDevicesChartData(ChartDataRequest request)
    {
        try
        {
            var getChartDataQuery =
                @"SELECT B.TimeStamp, B.BackupNodes, B.AllNodes, B.Granularity FROM Cirrus.Backup_vs_AllNodes AS B WHERE ToUtc(B.TimeStamp)>=@startDate AND ToUtc(B.TimeStamp)<=@endDate AND B.Granularity=@Granularity ORDER BY B.TimeStamp";

            var queryParameters =
                (Dictionary<string, object>) request.AllSettings[@"NCM_BackupVsAllDevicesNewChartDataParameters"];

            var startDate = ParseDateTime(queryParameters[@"@startDate"]);
            var endDate = ParseDateTime(queryParameters[@"@endDate"]);

            var properties = new PropertyBag();
            properties.Add(@"startDate", startDate);
            properties.Add(@"endDate", endDate);
            properties.Add(@"Granularity", queryParameters[@"@granularity"].ToString());

            var dt = ExecuteSWQL(getChartDataQuery, properties);

            var backedUpNodes = new DataSeries();
            backedUpNodes.Label = Resources.NCMWebContent.WEBDATA_VK_222;

            var allNodes = new DataSeries();
            allNodes.Label = Resources.NCMWebContent.WEBDATA_VK_223;

            foreach (DataRow row in dt.Rows)
            {
                var dateTimeEpoch = ConvertToUTCEpoch(row["TimeStamp"].ToString());
                DataPoint pointBackedUpNodes = DataPointWithOptions.CreatePointWithOptions(
                    new Dictionary<string, object>
                    {
                        {@"y", row["BackupNodes"]},
                        {@"x", dateTimeEpoch},
                        {@"count", row["BackupNodes"]}
                    });
                backedUpNodes.AddPoint(pointBackedUpNodes);

                DataPoint pointAllNodes = DataPointWithOptions.CreatePointWithOptions(new Dictionary<string, object>
                {
                    {@"y", row["AllNodes"]},
                    {@"x", dateTimeEpoch},
                    {@"count", row["AllNodes"]}
                });
                allNodes.AddPoint(pointAllNodes);

                backedUpNodes.TagName = @"DataSeriesBackedUpNodes";
                allNodes.TagName = @"DataSeriesAllNodes";
            }

            var dataSeriesList = new List<DataSeries>() {allNodes, backedUpNodes};
            var chartDataResults = new ChartDataResults(dataSeriesList);

            return chartDataResults;
        }
        catch (Exception ex)
        {
            log.Error("Error getting BackupVsAllDevicesChartData.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults PolicyReportViolationsChartData(ChartDataRequest request)
    {
        try
        {
            var dataSeriesList = new List<DataSeries>();

            ChartDataResults chartDataResults = null;
            var queryParameters =
                (Dictionary<string, object>) request.AllSettings[@"NCM_PolicyReportViolationsChartDataParameters"];
            var properties = new PropertyBag();

            var startDate = ParseDateTime(queryParameters[@"@startDate"]);
            var endDate = ParseDateTime(queryParameters[@"@endDate"]);

            properties.Add(@"startDate", startDate);
            properties.Add(@"endDate", endDate);
            properties.Add(@"Granularity", queryParameters[@"@Granularity"]);
            properties.Add(@"PolicyReportID", queryParameters[@"@PolicyReportID"]);

            var getDataQuery =
                @"SELECT P.TimeStamp, P.Info, P.Warning, P.Error, P.Granularity FROM Cirrus.PolicyReportViolations AS P WHERE ToUtc(P.TimeStamp)>=@startDate AND ToUtc(P.TimeStamp)<=@endDate AND P.Granularity=@Granularity AND P.ReportID=@PolicyReportID ORDER BY P.TimeStamp";

            var dt = ExecuteSWQL(getDataQuery, properties);

            var dataSeriesInfo = new DataSeries();
            dataSeriesInfo.Label = Resources.NCMWebContent.WEBDATA_VK_227;

            var dataSeriesWarning = new DataSeries();
            dataSeriesWarning.Label = Resources.NCMWebContent.WEBDATA_VK_228;
            var dataSeriesError = new DataSeries();
            dataSeriesError.Label = Resources.NCMWebContent.WEBDATA_VK_229;

            foreach (DataRow row in dt.Rows)
            {
                var dateTimeEpoch = ConvertToUTCEpoch(row["TimeStamp"].ToString());
                DataPoint pointInfo = DataPointWithOptions.CreatePointWithOptions(new Dictionary<string, object>
                {
                    {@"y", row["Info"]},
                    {@"x", dateTimeEpoch},
                    {@"count", row["Info"]},
                    {@"color", @"#BF0B23"}
                });

                DataPoint pointWarning = DataPointWithOptions.CreatePointWithOptions(new Dictionary<string, object>
                {
                    {@"y", row["Warning"]},
                    {@"x", dateTimeEpoch},
                    {@"count", row["Warning"]},
                    {@"color", @"#BF0B23"}
                });

                DataPoint pointError = DataPointWithOptions.CreatePointWithOptions(new Dictionary<string, object>
                {
                    {@"y", row["Error"]},
                    {@"x", dateTimeEpoch},
                    {@"count", row["Error"]},
                    {@"color", @"#BF0B23"},
                });
                dataSeriesInfo.AddPoint(pointInfo);
                dataSeriesInfo.TagName = @"DataSeriesInfo";

                dataSeriesWarning.AddPoint(pointWarning);
                dataSeriesWarning.TagName = @"DataSeriesWarning";

                dataSeriesError.AddPoint(pointError);
                dataSeriesError.TagName = @"DataSeriesError";
            }

            dataSeriesList.Add(dataSeriesInfo);
            dataSeriesList.Add(dataSeriesWarning);
            dataSeriesList.Add(dataSeriesError);

            chartDataResults = new ChartDataResults(dataSeriesList);

            return chartDataResults;
        }
        catch (Exception ex)
        {
            log.Error("Error getting PolicyReportViolationsChartData.", ex);
            throw;
        }
    }

    private DataTable ExecuteSWQL(string swqlStatement, PropertyBag properties)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swqlStatement, properties);
            return dt;
        }
    }

    private long ConvertToUTCEpoch(string date)
    {
        var dateTruncated = Convert.ToDateTime(date);
        dateTruncated = new DateTime(dateTruncated.Year, dateTruncated.Month, dateTruncated.Day, 0, 0, 0, DateTimeKind.Utc);

        return long.Parse((dateTruncated.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString());
    }

    private DateTime ParseDateTime(object dateTime)
    {
        if (dateTime != null && !(string.IsNullOrEmpty(dateTime.ToString())))
        {
            return DateTime.ParseExact(dateTime.ToString(), @"MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToUniversalTime();
        }

        return DateTime.Now.GetAsLocal();
    }
}