<%@ WebService Language="C#" Class="ThwackPolicyReports" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Net;
using System.Data;
using SolarWinds.Orion.Web;
using System.Collections.Generic;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using SolarWinds.NCMModule.Web.Resources.Thwack;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ThwackPolicyReports  : ThwackBaseWebService {

    private static readonly string THWACK_SESSION_KEY = "thwack_policyreports_list";

    private readonly ILogger logger;
    private readonly IThwackPolicyDoc thwackPolicy;

    public ThwackPolicyReports()
    {
        logger = ServiceLocator.Container.Resolve<ILoggerFactory>().Create(GetType());
        thwackPolicy = ServiceLocator.Container.Resolve<IThwackPolicyDoc>();
    }

    #region import&export
    
    [WebMethod(EnableSession = true)]
    public string ImportPolicyReports(IList<int> Ids)
    {
        if (Ids == null)
            return CreateResponse(false, "The list of policy reports must not be emtpy");

        NetworkCredential cred = Session[THWACK_CRED_SESSION_KEY] as NetworkCredential;
        if (cred == null)
            return CreateResponse(false, "thwack Authentication Error. Please enter thwack credential.");

        try
        {
            List<ThwackPostItem> allReports = GetNCMPolicyPostItems();
            List<ThwackPostItem> reportsToBeImported = allReports.FindAll(delegate(ThwackPostItem item) { return Ids.Contains(item.Id); });
            ThwackPostItemList list = new ThwackPostItemList();
            list.ItemList = reportsToBeImported;
            thwackPolicy.ImportPolicyReportsFromThwack(list, cred.UserName, cred.Password);
            return CreateResponse(true, "Policy Reports were successfully imported.");
        }
        catch (Exception ex)
        {
            logger.Error("Error importing policy reports.", ex);
            return CreateResponse(false, ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    public string UploadPolicyReports(Guid[] ids)
    {
        if (ids == null)
            return CreateResponse(false, "The list of policy reports must not be emtpy");

        NetworkCredential cred = Session[THWACK_CRED_SESSION_KEY] as NetworkCredential;
        if (cred == null)
            return CreateResponse(false, "thwack Authentication Error. Please enter thwack credential.");

        try
        {
            foreach (var id in ids)
            {
                thwackPolicy.UploadPolicyReportToThwack(id, cred.UserName, cred.Password);
            }
            return CreateResponse(true, "Policy Reports were successfully uploaded.");
        }
        catch (Exception ex)
        {
            logger.Error("Error uploading policy reports.", ex);
            return CreateResponse(false, ex.Message);
        }
    }
    
    #endregion

    #region datasource
    
    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        DataTable table = new DataTable();
        table.Columns.Add("theValue");
        table.Columns.Add("theCount");

        List<ThwackPostItem> list = GetNCMPolicyPostItems();

        // return "All" as the blank/unknown value
        table.Rows.Add("", list.Count);

        Dictionary<string, int> postsForTag = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        foreach (ThwackPostItem item in list)
        {
            foreach (string tag in item.Tags)
            {
                if (!postsForTag.ContainsKey(tag))
                    postsForTag[tag] = 1;
                else
                    postsForTag[tag] += 1;
            }
        }

        List<string> allTags = new List<string>(postsForTag.Keys);
        allTags.Sort(StringComparer.OrdinalIgnoreCase);

        foreach (string tag in allTags)
        {
            table.Rows.Add(tag, postsForTag[tag]);
        }

        return table;
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetPolicyReportDescription(int reportId)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();

        List<ThwackPostItem> list = GetNCMPolicyPostItems();
        ThwackPostItem postItem = list.Find(delegate(ThwackPostItem item) { return item.Id == reportId; });

        if (postItem != null)
            result["description"] = postItem.Description.Trim();

        return result;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetPolicyReportsPaged(string property, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        List<ThwackPostItem> list = GetNCMPolicyPostItems();

        bool isInitialListEmpty = (list.Count == 0);

        list = FilterItemList(list, value, search);
        bool isFilteredListEmprt = (list.Count == 0);

        Comparison<ThwackPostItem> compareMethod = SortByTitle;

        if (String.Equals(sortColumn, "PubDate", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByDateTime;
        else if (String.Equals(sortColumn, "DownloadCount", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByDownloads;
        else if (String.Equals(sortColumn, "ViewCount", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByViews;
        else if (String.Equals(sortColumn, "Rating", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByRating;
        else if (String.Equals(sortColumn, "Owner", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByOwner;

        list.Sort(compareMethod);

        pageSize = Math.Max(pageSize, 25);

        if (String.Equals(sortDirection, "desc", StringComparison.OrdinalIgnoreCase))
        {
            list.Reverse();
        }

        string emptyText;
        if (isInitialListEmpty)
            emptyText = "Unable to reach thwack.com. Server may have limited Internet connectivity.";
        else if (isFilteredListEmprt)
            emptyText = "No policy reports match the search criteria.";
        else
            emptyText = "No policy reports to display.";

        DataTable table = PostItemToDataTable(list, startRowNumber, startRowNumber + pageSize);

        return new PageableDataTable(table, list.Count, emptyText);
    }
    #endregion

    #region private methods

    private List<ThwackPostItem> GetNCMPolicyPostItems()
    {
        ThwackPostItemList list = Session[THWACK_SESSION_KEY] as ThwackPostItemList;

        if (list != null)
            return list.ItemList;

        try
        {
            list = thwackPolicy.LoadPolicyReportsFromThwack();
            Session[THWACK_SESSION_KEY] = list;
        }
        catch
        {
            list = null;
        }

        return list == null ? new List<ThwackPostItem>() : list.ItemList;
    }

    #endregion
}

