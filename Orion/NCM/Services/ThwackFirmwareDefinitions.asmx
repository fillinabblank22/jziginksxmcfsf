﻿<%@ WebService Language="C#" Class="ThwackFirmwareDefinitions" %>

using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Net;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web;
using System.Data;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCMModule.Web.Resources.Thwack;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ThwackFirmwareDefinitions : SolarWinds.NCMModule.Web.Resources.NCMCore.ThwackBaseWebService
{
    private static readonly string THWACK_SESSION_KEY = "thwack_firmwaredefinition_list";
    private readonly ILogger log;
    private readonly IThwackFirmwareDefinitionDoc thwackFirmwareDefinition;
        
    public ThwackFirmwareDefinitions()
    {
        log = ServiceLocator.Container.Resolve<ILoggerFactory>().Create(GetType());
        thwackFirmwareDefinition = ServiceLocator.Container.Resolve<IThwackFirmwareDefinitionDoc>();
    }

    #region Import and Export

    [WebMethod(EnableSession = true)]
    public string ImportFirmwareDefinitions(IList<int> ids)
    {
        if (ids == null)
            return CreateResponse(false, "The list of firmware upgrade templates must not be emtpy");

        NetworkCredential cred = Session[THWACK_CRED_SESSION_KEY] as NetworkCredential;
        if (cred == null)
            return CreateResponse(false, Resources.NCMWebContent.WEBDATA_IA_61);

        try
        {
            List<ThwackPostItem> allFirmwareDefinitions = GetNCMFirmwareDefinitionItems();
            List<ThwackPostItem> reportsToBeImported = allFirmwareDefinitions.FindAll(item => ids.Contains(item.Id));
            ThwackPostItemList list = new ThwackPostItemList {ItemList = reportsToBeImported};

            thwackFirmwareDefinition.ImportFirmwareDefinitionsFromThwack(list, cred.UserName, cred.Password);
            return CreateResponse(true, Resources.NCMWebContent.WEBDATA_AP_06);
        }
        catch (Exception ex)
        {
            log.Error("Error importing firmware definition.", ex);
            return CreateResponse(false, ex.Message);
        }
    }
    
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public string UploadFirmwareDefinitionsToTwack(List<int> ids)
    {
        if (CommonHelper.IsDemoMode())
        {
            return null;
        }

        if (ids == null)
            return CreateResponse(false, Resources.NCMWebContent.WEBDATA_AP_07);

        NetworkCredential cred = Session[THWACK_CRED_SESSION_KEY] as NetworkCredential;
        if (cred == null)
            return CreateResponse(false, Resources.NCMWebContent.WEBDATA_IA_61);

        try
        {
            foreach (int id in ids)
            {
                thwackFirmwareDefinition.UploadFirmwareDefinitionToThwack(id, cred.UserName, cred.Password);                
            }
            return CreateResponse(true, Resources.NCMWebContent.WEBDATA_AP_08);
        }
        catch (Exception ex)
        {
            log.Error("Error uploading firmware definitions to Thwack.", ex);
            return CreateResponse(false, ex.Message);
        }
    }

    #endregion

    #region DataSource

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFirmwareDefinitionsPaged(string property, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        List<ThwackPostItem> list = GetNCMFirmwareDefinitionItems();

        var isInitialListEmpty = list.Count == 0;

        list = FilterItemList(list, value, search);
        var isFilteredListEmpty = list.Count == 0;

        Comparison<ThwackPostItem> compareMethod = SortByTitle;

        if (String.Equals(sortColumn, "PubDate", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByDateTime;
        else if (String.Equals(sortColumn, "DownloadCount", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByDownloads;
        else if (String.Equals(sortColumn, "ViewCount", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByViews;
        else if (String.Equals(sortColumn, "Rating", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByRating;
        else if (String.Equals(sortColumn, "Owner", StringComparison.OrdinalIgnoreCase))
            compareMethod = SortByOwner;

        list.Sort(compareMethod);

        pageSize = Math.Max(pageSize, 25);

        if (String.Equals(sortDirection, "desc", StringComparison.OrdinalIgnoreCase))
        {
            list.Reverse();
        }

        string emptyText;
        if (isInitialListEmpty)
            emptyText = Resources.NCMWebContent.WEBDATA_IA_66;
        else if (isFilteredListEmpty)
            emptyText = Resources.NCMWebContent.WEBDATA_AP_10;
        else
            emptyText = Resources.NCMWebContent.WEBDATA_AP_09;

        DataTable table = PostItemToDataTable(list, startRowNumber, startRowNumber + pageSize);
            
        return new PageableDataTable(table, list.Count, emptyText);
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        DataTable table = new DataTable();
        table.Columns.Add("theValue");
        table.Columns.Add("theCount");

        List<ThwackPostItem> list = GetNCMFirmwareDefinitionItems();

        // return "All" as the blank/unknown value
        table.Rows.Add("", list.Count);

        Dictionary<string, int> postsForTag = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        foreach (ThwackPostItem item in list)
        {
            foreach (string tag in item.Tags)
            {
                if (!postsForTag.ContainsKey(tag))
                    postsForTag[tag] = 1;
                else
                    postsForTag[tag] += 1;
            }
        }

        List<string> allTags = new List<string>(postsForTag.Keys);
        allTags.Sort(StringComparer.OrdinalIgnoreCase);

        foreach (string tag in allTags)
        {
            table.Rows.Add(tag, postsForTag[tag]);
        }

        return table;
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetFirmwareDefinitionDescription(int reportId)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();

        List<ThwackPostItem> list = GetNCMFirmwareDefinitionItems();
        ThwackPostItem postItem = list.Find(item => item.Id == reportId);

        if (postItem != null)
            result["description"] = postItem.Description.Trim();

        return result;
    }
    
    #endregion

    #region private methods

    private List<ThwackPostItem> GetNCMFirmwareDefinitionItems()
    {
        ThwackPostItemList list = Session[THWACK_SESSION_KEY] as ThwackPostItemList;

        if (list != null)
            return list.ItemList;

        try
        {
            list = thwackFirmwareDefinition.LoadFirmwareDefinitionsFromThwack();
            Session[THWACK_SESSION_KEY] = list;
        }
        catch (Exception ex)
        {
            list = null;
            log.Error("Error getting items from thwack.", ex);            
        }

        return list == null 
                ? new List<ThwackPostItem>() 
                : list.ItemList;
    }

    #endregion

}