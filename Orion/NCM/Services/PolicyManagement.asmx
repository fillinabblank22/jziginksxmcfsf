<%@ WebService Language="C#" Class="PolicyManagement" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using Resources;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PolicyManagement : WebService
{
    private readonly Log log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetPoliciesPaged(string property, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString[@"sort"];
        string sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        value = value.Replace(@"'", @"''");
        search = search.Replace(@"'", @"''");

        DataTable pageData = GetPagablePoliciesTable(pageSize, startRowNumber, sortColumn, sortDirection, property, value, search);

        return new PageableDataTable(pageData, GetRowCount(GetGroupByColumn(property), GetGroupByValue(value), search));
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeletePolicies(Guid[] policyIds, bool deleteChilds)
    {
        try
        {
            ISWrapper isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.PolicyReports.DeletePolicies(policyIds, deleteChilds);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in delete policy function", ex);
            return new ErrorEntry(Resources.NCMWebContent.PolicyManagement_DeleteErrorTitle, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public Guid CopyPolicy(Guid policyId)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                Policy policy = proxy.Cirrus.PolicyReports.GetPolicy(policyId, true);

                // Prepare new rules
                var assignedRulesList = new List<Guid>();
                foreach (PolicyRule policyRule in policy.AssignedPolicyRules)
                {
                    policyRule.RuleId = Guid.NewGuid();
                    policyRule.RuleName = $@"{policyRule.RuleName}_{NCMWebContent.WEBDATA_IC_202}";

                    assignedRulesList.Add(policyRule.RuleId);

                    // Prepare multiLineRulePattern
                    foreach (MultiLineRulePattern multiLineRulePattern in policyRule.MultiLineRulePatterns)
                    {
                        multiLineRulePattern.PatternId = Guid.NewGuid();
                        multiLineRulePattern.RuleId = policyRule.RuleId;
                    }
                }

                // Add policy
                policy.PolicyId = Guid.NewGuid();
                policy.PolicyName = $@"{policy.PolicyName}_{NCMWebContent.WEBDATA_IC_202}";
                policy.AssignedRulesList = assignedRulesList;
                return proxy.Cirrus.PolicyReports.AddPolicy(policy, true);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in copy policy function", ex);
            throw;
        }
    }
    
    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        if (string.IsNullOrEmpty(property))
            return new DataTable();

        string swql = GetGroupBySWQL(property);

        DataTable dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
        {
            int count = dt.Rows.Count;
            DataRow lastRow = dt.Rows[count - 1];
            int value = int.Parse(lastRow["theCount"].ToString());
            if (value == 0)
            {
                dt.Rows.Remove(lastRow);
            }
        }

        return dt;
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetPolicyDescription(string policyId)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();
        DataTable dtDesc = ExecuteSWQL(@"SELECT P.Comment FROM Cirrus.Policies AS P WHERE P.PolicyID=@policyId",
            new PropertyBag() {{@"policyId", policyId}});

        if (dtDesc.Rows.Count > 0)
        {
            result[@"description"] = dtDesc.Rows[0]["Comment"].ToString();
        }

        return result;
    }

    [WebMethod(EnableSession = true)]
    public string FindPolicyIndexInGrid(string sort, string dir, string policyId, int limit)
    {
        int pageSize = 0;
        int start = 0;
        bool success = false;

        string sortColumn = string.IsNullOrEmpty(sort) ? @"Name" : sort;
        string sortDirection = string.IsNullOrEmpty(dir) ? @"ASC" : dir;
        pageSize = limit;

        ISWrapper isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            int rowNumber = proxy.Cirrus.PolicyReports.GetRowNumber(@"NCM_Policies", sortColumn, sortDirection,
                $@"WHERE PolicyID='{policyId}'");

            success = true;
            int pos = rowNumber - 1;
            start = ((int)pos / pageSize) * pageSize;

            string jsonRes = $@"{{success:{success.ToString().ToLowerInvariant()},data:{{start:{start}}}}}";

            return jsonRes;
        }
    }

    #region Private Methods

    private DataTable GetPagablePoliciesTable(int pageSize, int startRowNumber, string sortColumn, string sortDirection, string groupByColumn, string groupByValue, string searchValue)
    {
        try
        {
            pageSize = Math.Max(pageSize, 25);

            ISWrapper isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                return proxy.Cirrus.PolicyReports.GetPagablePoliciesList(pageSize, startRowNumber,
                    $@"NCM_Policies.{sortColumn}", sortDirection, GetGroupByColumn(groupByColumn), GetGroupByValue(groupByValue), searchValue);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in GetPagablePoliciesTable", ex);
            return null;
        }
    }

    private string GetGroupByColumn(string groupByColumn)
    {
        switch (groupByColumn.ToUpperInvariant())
        {
            case "FOLDER":
                return @"NCM_Policies.Grouping";
            case "APPEARSINREPORT":
                return @"NCM_PolicyReports.PolicyReportID";
            case "CONTAINSRULE":
                return @"NCM_PolicyRules.PolicyRuleID";
        }

        return groupByColumn;
    }

    private string GetGroupByValue(string groupByValue)
    {
        switch (groupByValue.ToUpperInvariant())
        {
            case "NO FOLDER":
                return @"${IS_EMPTY}";
            case "NULL":
            case "NO REPORT":
            case "NO RULE":
                return @"${IS_NULL}";
        }

        return groupByValue;
    }

    private string GetGroupBySWQL(string groupBy)
    {
        switch (groupBy.ToUpperInvariant())
        {
            case "FOLDER":
                return @"
                SELECT DISTINCT P.Grouping AS theValue, COUNT(P.Grouping) AS theCount, P.Grouping AS theID, 0 AS theOrdinalNumber
                FROM Cirrus.Policies P WHERE P.Grouping <> ''
                GROUP BY P.Grouping
                UNION ALL
                (
                SELECT 'No Folder' AS theValue, COUNT(P.Grouping) AS theCount, P.Grouping AS theID, 1 AS theOrdinalNumber
                FROM Cirrus.Policies P WHERE P.Grouping = ''
                GROUP BY P.Grouping
                ) ORDER BY theOrdinalNumber, theValue";
            case "APPEARSINREPORT":
                return @"
                SELECT DISTINCT R.Name AS theValue, COUNT(P.Name) AS theCount, R.PolicyReportID AS theID, 0 AS theOrdinalNumber 
                FROM Cirrus.PolicyReports AS R
                JOIN Cirrus.PolicyAssignment AS A ON R.PolicyReportID= A.PolicyReportID
                RIGHT JOIN Cirrus.Policies AS P ON A.PolicyID = P.PolicyID
                WHERE R.Name IS NOT NULL OR R.Name<>''
                GROUP BY R.PolicyReportID, R.Name
                UNION ALL
                (
                SELECT 'No Report' AS theValue, COUNT(P.Name) AS theCount,  R.PolicyReportID AS theID, 1 AS theOrdinalNumber 
                FROM Cirrus.PolicyReports AS R
                JOIN Cirrus.PolicyAssignment AS A ON R.PolicyReportID= A.PolicyReportID
                RIGHT JOIN Cirrus.Policies AS P ON A.PolicyID = P.PolicyID
                WHERE R.Name IS NULL OR R.Name=''
                GROUP BY R.PolicyReportID, R.Name
                ) ORDER BY theOrdinalNumber, theValue";
            case "CONTAINSRULE":
                return @"                
                SELECT DISTINCT R.Name AS theValue, COUNT(P.Name) AS theCount, R.PolicyRuleID AS theID, 0 AS theOrdinalNumber 
                FROM Cirrus.PolicyRules AS R
                JOIN Cirrus.PolicyRuleAssignment AS A ON R.PolicyRuleID= A.PolicyRuleID
                RIGHT JOIN Cirrus.Policies AS P ON A.PolicyID = P.PolicyID
                WHERE R.Name IS NOT NULL OR R.Name<>''
                GROUP BY R.PolicyRuleID, R.Name
                UNION ALL
                (
                SELECT 'No Rule' AS theValue, COUNT(P.Name) AS theCount, R.PolicyRuleID AS theID, 1 AS theOrdinalNumber 
                FROM Cirrus.PolicyRules AS R
                JOIN Cirrus.PolicyRuleAssignment AS A ON R.PolicyRuleID= A.PolicyRuleID
                RIGHT JOIN Cirrus.Policies AS P ON A.PolicyID = P.PolicyID
                WHERE R.Name IS NULL OR R.Name=''
                GROUP BY R.PolicyRuleID
                ) ORDER BY theOrdinalNumber, theValue";
        }

        return string.Empty;
    }

    private int GetRowCount(string groupByColumn, string groupByValue, string searchValue)
    {
        ISWrapper isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.PolicyReports.GetPoliciesRowCount(groupByColumn, groupByValue, searchValue);
        }
    }

    private DataTable ExecuteSWQL(string swql, PropertyBag propertyBag = null)
    {
        ISWrapper isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            DataTable dt = proxy.Query(swql, propertyBag);
            return dt;
        }
    }

    #endregion
}
