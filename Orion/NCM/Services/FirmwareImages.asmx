﻿<%@ WebService Language="C#" Class="FirmwareImages" %>

using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Linq;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;
using SolarWinds.NCM.Common.Settings;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class FirmwareImages : WebService
{
    private static readonly Log _log = new Log();
    private readonly INcmBusinessLayerProxyHelper proxyHelper;
    private readonly INCMBLSettings settings;

    public FirmwareImages()
    {
        proxyHelper = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
        settings = ServiceLocator.Container.Resolve<INCMBLSettings>();
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFirmwareImages(string search, string machineType, double clientOffset)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString[@"sort"];
        string sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        search = !string.IsNullOrEmpty(search) ? search.Replace(@"'", @"''") : null;
        machineType = !string.IsNullOrEmpty(machineType) ? machineType.Replace(@"'", @"''") : null;

        string swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, search, machineType);
        DataTable dt = ExecuteSWQL(swql);

        DateTimeOffset localTime = DateTimeOffset.Now;
        double offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;
        dt.AsEnumerable().Where(row => !row.IsNull("DateTimeUtc")).ToList().ForEach(row => row["DateTimeUtc"] = Convert.ToDateTime(row["DateTimeUtc"]).ToLocalTime().AddMinutes(offsetMinutes));

        return new PageableDataTable(dt, GetRowCount(search, machineType));
    }

    [WebMethod(EnableSession = true)]
    public string GetFirmareImage(int imageId)
    {
        var dt = ExecuteSWQL(@"SELECT FileName, RelativePath, Description FROM NCM.FirmwareUpgradeImages WHERE ID=@id", new PropertyBag() { { @"id", imageId } });
        return JsonConvert.SerializeObject(dt.AsEnumerable().Select(x => new
        {
            FileName = x.Field<string>("FileName"),
            RelativePath = x.Field<string>("RelativePath"),
            Description = x.Field<string>("Description")
        }));
    }

    [WebMethod(EnableSession = true)]
    public string GetMachineTypes(int imageId)
    {
        var dt = ExecuteSWQL(@"SELECT MachineType FROM NCM.FirmwareUpgradeMachineTypes WHERE ImageID=@imageId", new PropertyBag() { { @"imageId", imageId } });
        if (dt != null || dt.Rows.Count > 0)
        {
            var list = dt.AsEnumerable().Select(r => r.Field<string>("MachineType")).ToList();
            return JsonConvert.SerializeObject(list);
        }
        return null;
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetAllMachineTypesList(int imageId)
    {
        string swql = $@"
            SELECT DISTINCT MachineType 
            FROM Orion.Nodes
            WHERE {(imageId > 0 ? @"MachineType NOT IN (SELECT MachineType FROM NCM.FirmwareUpgradeMachineTypes WHERE ImageID=@imageId)" : @"(1=1)")}";

        var dt = ExecuteSWQL(swql, imageId > 0 ? new PropertyBag() { { @"imageId", imageId } } : null);
        return dt;
    }

    [WebMethod]
    public void StartScanFirmwareRepository()
    {
        try
        {
            proxyHelper.CallMain(p => p.StartScanFirmwareRepository(settings.FirmwareRepositoryFileExtensions.Split(';'), TimeSpan.FromMinutes(settings.FirmwareRepositoryScanTimeout), settings.FirmwareRepositoryFileWatcherEnabled));
        }
        catch (Exception ex)
        {
            _log.Error("Error in FirmwareImages.StartScanFirmwareRepository", ex);
            throw;
        }
    }

    [WebMethod]
    public string IsScanFirmwareRepositoryRunning()
    {
        try
        {
            string lastError = proxyHelper.CallMain(p => p.GetScanFirmwareRepositoryLastError());
            return JsonConvert.SerializeObject(new
            {
                LastError = lastError,
                IsRunning = string.IsNullOrEmpty(lastError) && proxyHelper.CallMain(p => p.IsScanFirmwareRepositoryRunning())
            });
        }
        catch (Exception ex)
        {
            _log.Error("Error in FirmwareImages.IsScanFirmwareRepositoryRunning", ex);
            return JsonConvert.SerializeObject(new
            {
                LastError = ex.Message,
                IsRunning = false
            });
        }
    }

    private DataTable ExecuteSWQL(string swql, PropertyBag properties = null)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            return proxy.Query(swql, properties);
        }
    }

    private string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderbyDirection, string search, string machineType = null)
    {
        pageSize = Math.Max(pageSize, 1);

        string swql = $@"
            SELECT DISTINCT FirmwareImages.ID
	            ,FirmwareImages.FileName
	            ,FirmwareImages.Description
	            ,NodeProperties.NodeID
	            ,OrionNodes.Caption
	            ,OrionNodes.IP_Address
	            ,FirmwareImages.DateTimeUtc
                ,FirmwareImages.MD5Hash
                ,FirmwareImages.Size
            FROM NCM.FirmwareUpgradeImages AS FirmwareImages
            LEFT JOIN NCM.NodeProperties AS NodeProperties ON NodeProperties.CoreNodeID=FirmwareImages.CoreNodeID
            LEFT JOIN Orion.Nodes AS OrionNodes ON NodeProperties.CoreNodeID=OrionNodes.NodeID 
            LEFT JOIN NCM.FirmwareUpgradeMachineTypes AS FirmwareMachineTypes ON FirmwareMachineTypes.ImageID=FirmwareImages.ID
            WHERE {GetWhereClause(machineType)} AND {GetSearchClause(search)}
            ORDER BY {orderByColumn} {orderbyDirection}
            WITH ROWS {startRowNumber + 1} TO {startRowNumber + pageSize}";
        return swql;
    }

    private long GetRowCount(string search, string machineType = null)
    {
        string swql = $@"
            SELECT 
                COUNT(DISTINCT ID) AS theCount
            FROM NCM.FirmwareUpgradeImages AS FirmwareImages
            LEFT JOIN NCM.NodeProperties AS NodeProperties ON NodeProperties.CoreNodeID=FirmwareImages.CoreNodeID
            LEFT JOIN Orion.Nodes AS OrionNodes ON NodeProperties.CoreNodeID=OrionNodes.NodeID 
            LEFT JOIN NCM.FirmwareUpgradeMachineTypes AS FirmwareMachineTypes ON FirmwareMachineTypes.ImageID=FirmwareImages.ID
            WHERE {GetWhereClause(machineType)} AND {GetSearchClause(search)}";

        DataTable rowCountTable = ExecuteSWQL(swql);
        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        return 0;
    }

    private string GetWhereClause(string machineType)
    {
        if (string.IsNullOrEmpty(machineType))
            return @"(1=1)";

        string whereClause =
            $@"(FirmwareMachineTypes.MachineType='{machineType}' OR FirmwareMachineTypes.MachineType IS NULL)";
        return whereClause;
    }

    private string GetSearchClause(string search)
    {
        if (string.IsNullOrEmpty(search))
            return @"(1=1)";

        string searchValue = SearchHelper.WildcardToLike(search);
        string searchClause = string.Format(@"(FirmwareImages.FileName LIKE '%{0}%' OR FirmwareImages.Description LIKE '%{0}%' OR OrionNodes.Caption LIKE '%{0}%')", searchValue);
        return searchClause;
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeleteFirmwareImages(int[] ids)
    {
        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                proxy.Cirrus.FirmwareStorage.DeleteFirmwareImages(ids);
            }
            return null;
        }
        catch (Exception ex)
        {
            _log.Error("Error in FirmwareImages.DeleteFirmwareImages", ex);
            return new ErrorEntry(Resources.NCMWebContent.FirmwareRepository_DeleteFirmwareImagesError, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry SaveImage(int imageId, string description, List<string> machineTypes)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            try
            {
                if (imageId > 0)
                    proxy.Cirrus.FirmwareStorage.UpdateFirmwareImage(imageId, description, machineTypes);
                return null;
            }
            catch (Exception ex)
            {
                _log.Error("Error in FirmwareImages.SaveImage", ex);
                return new ErrorEntry(Resources.NCMWebContent.FirmwareRepository_SaveImageError, ex);
            }
        }
    }
}