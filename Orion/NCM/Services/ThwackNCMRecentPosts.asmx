<%@ WebService Language="C#" Class="ThwackNCMRecentPosts" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Thwack;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ThwackNCMRecentPosts : WebService
{
    private readonly ILogger logger;
    private readonly IThwackRecentPosts thwackRecentPosts;

    public ThwackNCMRecentPosts()
    {
        logger = ServiceLocator.Container.Resolve<ILoggerFactory>().Create(GetType());
        thwackRecentPosts = ServiceLocator.Container.Resolve<IThwackRecentPosts>();
    }

    [WebMethod]
    public string GetRecentPosts(int postsCount)
    {
        try
        {
            return thwackRecentPosts.GetRecentPosts(postsCount);
        }
        catch (Exception ex)
        {
            logger.Error("Error getting thwack items.", ex);
            throw;
        }
    }
}