<%@ WebService Language="C#" Class="JobManagement" %>

using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;
using SolarWinds.Orion.Web;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCMModule.Web.Resources.JobsManagement;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class JobManagement : WebService
{
    private readonly ILogger log;
    private readonly INcmBusinessLayerProxyHelper proxyHelper;
    private readonly IJobManagementService jobManagementService;
    private readonly IIsWrapper isLayer;

    public JobManagement()
    {
        proxyHelper = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
        jobManagementService = ServiceLocator.Container.Resolve<IJobManagementService>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetJobsPaged(string search)
    {
        var pageSize = 0;
        var startRowNumber = 0;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        search = search.Replace(@"'", @"''");

        var swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, search);
        var pageData = ExecuteSWQL(swql);

        Lookup(pageData);

        return new PageableDataTable(pageData, GetRowCount(search));
    }

    [WebMethod(EnableSession = true)]
    public string GetJobLog(int jobID)
    {
        using (var proxy = isLayer.GetProxy())
        {
            return proxy.Cirrus.Jobs.GetJobLog(jobID, true);
        }
    }

    [WebMethod(EnableSession = true)]
    public bool ClearJobLog(int jobID)
    {
        using (var proxy = isLayer.GetProxy())
        {
            var retVal = proxy.Cirrus.Jobs.ClearJobLog(jobID);
            return retVal;
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry StartJob(int jobID)
    {
        return jobManagementService.StartJob(jobID);
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry StopJob(int jobID)
    {
        try
        {
            proxyHelper.CallAllEngines(bl => bl.CancelJob(jobID), true);
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error stopping job.", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_802, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetJobStatusPaged(IEnumerable<int> jobIDs)
    {
        if (jobIDs != null && jobIDs.Any())
        {
            var swql = @"
            SELECT 
                NCMJobID AS ID, 
                Status,
                LastDateRun AS DR, 
                NextDateRunUtc AS NDR,
                Log,
                CompletedSubJobs,
                AllSubJobs
            FROM Cirrus.NCM_NCMJobsView
            WHERE IsHidden=0 AND ID IN ({0})";

            var dt = ExecuteSWQL(string.Format(swql, string.Join(@",", jobIDs)));
            if (dt != null)
            {
                Lookup(dt);
                return new PageableDataTable(dt, 0);
            }
        }
        return null;
    }

    private string ValidateOnceJob(NCMJob job)
    {
        if (job.JobSchedule.TriggerType == NCMJobSchedule.eTriggerType.Once)
        {
            if ((job.JobSchedule.StartDate - DateTime.Now).TotalMinutes < 15)
            {
                return job.JobName;
            }
        }
        return null;
    }

    private void ValidateJobsEnabling(List<int> jobIDs, ICirrusInfoProxy proxy)
    {
        var unvalidOnceJobsNames = new List<string>();

        foreach (var jobID in jobIDs)
        {
            var job = proxy.Cirrus.Jobs.GetJob(jobID);
            if (job != null)
            {
                var result = ValidateOnceJob(job);
                if (result != null)
                    unvalidOnceJobsNames.Add(result);
            }
        }

        if (unvalidOnceJobsNames.Count != 0)
        {
            var utcOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours;
            var ncmServerTime = string.Format(Resources.NCMWebContent.WEBDATA_VK_878, utcOffset >= 0 ? @"+" : @"-", Math.Abs(utcOffset), DateTime.Now.ToShortTimeString());
            var errorTimeMsg = $@"{Resources.NCMWebContent.WEBDATA_VM_216} {ncmServerTime}";

            var jobsNames = String.Join(@",<BR/> ", unvalidOnceJobsNames.ToArray());
            throw new Exception($@"{Resources.NCMWebContent.JobManagement_NotAllowedToEnableJobs}<BR/>{jobsNames}<BR/>{errorTimeMsg}");
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry EnableOrDisableJobs(List<int> jobIDs, bool enableOrDisable)
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                if (enableOrDisable)
                    ValidateJobsEnabling(jobIDs, proxy);

                proxy.Cirrus.Jobs.EnableOrDisableJobs(jobIDs.ToArray(), enableOrDisable);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in EnableOrDisableJobs", ex);
            if (enableOrDisable)
                return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_803, ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_804, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeleteJobs(List<int> jobIDs)
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.Jobs.DeleteJobs(jobIDs.ToArray());
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in DeleteJobs", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_805, ex);
        }
    }

    private void Lookup(DataTable dt)
    {
        Action<DataRow> instrument = (
            row =>
            {
                ConvertTimeToLocal(row, @"DR");
                ConvertTimeToLocal(row, @"NDR");
            });

        InstrumentValues(dt, instrument);
    }

    private void ConvertTimeToLocal(DataRow row, string colName)
    {
        if (row[colName] == DBNull.Value)
            return;

        var utcTime = Convert.ToDateTime(row[colName]);
        row[colName] = utcTime.ToLocalTime();
    }

    private void InstrumentValues(DataTable dt, Action<DataRow> instrument)
    {
        foreach (DataRow row in dt.Rows)
        {
            instrument(row);
        }
    }

    private DataTable ExecuteSWQL(string swql)
    {
        using (var proxy = isLayer.GetProxy())
        {
            return proxy.Query(swql);
        }
    }

    private string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderbyDirection, string search)
    {
        pageSize = Math.Max(pageSize, 25);

        var swql = $@"
        SELECT 
                NCMJobID AS ID, 
                NCMJobName AS Name, 
                NCMJobType AS Type, 
                NCMJobCreator AS Author, 
                LastDateRun AS DR, 
                NextDateRunUtc AS NDR,
                NCMJobDateCreated AS DC, 
                NCMJobDateModified AS DM, 
                Enabled, 
                Status, 
                Log,
                CompletedSubJobs,
                AllSubJobs
        FROM Cirrus.NCM_NCMJobsView 
        WHERE IsHidden=0 AND {GetSearchClause(search)} AND {GetAccountLimitClause()}
        ORDER BY {orderByColumn} {orderbyDirection}
        WITH ROWS {startRowNumber + 1} to {startRowNumber + pageSize}";

        return swql;
    }

    private long GetRowCount(string search)
    {
        var swqlFormat = @"
            SELECT COUNT(NCMJobID) AS theCount
            FROM Cirrus.NCM_NCMJobsView 
            WHERE IsHidden=0 AND {0} AND {1}";

        var rowCountTable = ExecuteSWQL(string.Format(swqlFormat,
                                                GetSearchClause(search),
                                                GetAccountLimitClause()
                                                ));
        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private string GetSearchClause(string search)
    {
        if (string.IsNullOrEmpty(search))
            return @"(1=1)";

        var searchValue = SearchHelper.WildcardToLike(search);

        var swqlFormat = @"(NCMJobName LIKE '%{0}%' OR NCMJobCreator LIKE '%{0}%')";
        var clause = string.Format(swqlFormat, searchValue);
        return clause;
    }

    private string GetAccountLimitClause()
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_WEBUPLOADER))
        {
            return $@"NCMJobCreator='{HttpContext.Current.Profile.UserName}'";
        }
        return @"(1=1)";
    }
}
