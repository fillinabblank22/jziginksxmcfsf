﻿<%@ WebService Language="C#" Class="NodeManagement" %>

using System;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NodeManagement : WebService
{
    private readonly Log _log = new Log();

    private readonly IIsWrapper isWrapper;
    private readonly ILicenseWrapper licenseWrapper;
    private readonly ICommonHelper commonHelper;

    public NodeManagement()
    {
        isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        licenseWrapper = ServiceLocator.Container.Resolve<ILicenseWrapper>();
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    [WebMethod(EnableSession = true)]
    public bool AddNodesEnabled(List<int> coreNodeIds, string where, bool allSelected)
    {
        if (coreNodeIds.Count == 0) return false;

        try
        {
            if (licenseWrapper.GetEvaluationDaysLeft() < 0) return false; //license expired
            if (!SecurityHelper.IsValidNcmAccountRole) return false; //NCM Account Role is None

            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query(string.Format(
                    CultureInfo.InvariantCulture,
                    @"SELECT COUNT(N.NodeID) AS NodesCount FROM Orion.Nodes AS N
                    LEFT JOIN Cirrus.NodeProperties AS NcmNodeProperties ON N.NodeID=NcmNodeProperties.CoreNodeID
                    INNER JOIN Orion.Engines AS E ON E.EngineID=N.EngineID
                    {0} 
                    AND NcmNodeProperties.CoreNodeID IS NULL",
                    GetWhereClause(coreNodeIds, where, allSelected)));

                if (dt != null && dt.Rows.Count > 0)
                {
                    var nodesCount = Convert.ToInt32(dt.Rows[0]["NodesCount"], CultureInfo.InvariantCulture);
                    return (nodesCount > 0);
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            _log.Error("AddNodesEnabled failed.", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool RemoveNodesEnabled(List<int> coreNodeIds, string where, bool allSelected)
    {
        if (coreNodeIds.Count == 0) return false;

        try
        {
            if (licenseWrapper.GetEvaluationDaysLeft() < 0) return false; //license expired
            if (!SecurityHelper.IsValidNcmAccountRole) return false; //NCM Account Role is None

            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query(string.Format(
                    CultureInfo.InvariantCulture,
                    @"SELECT COUNT(NcmNodeProperties.NodeID) AS NodesCount FROM Orion.Nodes AS N
                    LEFT JOIN Cirrus.NodeProperties AS NcmNodeProperties ON N.NodeID=NcmNodeProperties.CoreNodeID
                    INNER JOIN Orion.Engines AS E ON E.EngineID=N.EngineID
                    {0} 
                    AND NcmNodeProperties.CoreNodeID IS NOT NULL",
                    GetWhereClause(coreNodeIds, where, allSelected)));

                if (dt != null && dt.Rows.Count > 0)
                {
                    var nodesCount = Convert.ToInt32(dt.Rows[0]["NodesCount"], CultureInfo.InvariantCulture);
                    return (nodesCount > 0);
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            _log.Error("RemoveNodesEnabled failed.", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry AddNodes(List<int> coreNodeIds, string where, bool allSelected)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query(string.Format(
                    CultureInfo.InvariantCulture,
                    @"SELECT N.NodeID FROM Orion.Nodes AS N
                    LEFT JOIN Cirrus.NodeProperties AS NcmNodeProperties ON N.NodeID=NcmNodeProperties.CoreNodeID
                    INNER JOIN Orion.Engines AS E ON E.EngineID=N.EngineID
                    {0} 
                    AND NcmNodeProperties.CoreNodeID IS NULL",
                    GetWhereClause(coreNodeIds, where, allSelected)));

                if (dt != null && dt.Rows.Count > 0)
                {
                    var coreNodeIdsForAdd = dt.AsEnumerable().Select(row => row.Field<int>("NodeID")).ToArray();
                    ErrorEntry errorEntry;
                    if (TryGetLicenseOverLimit(coreNodeIdsForAdd.Length, out errorEntry))
                    {
                        return errorEntry; //license over limit
                    }

                    proxy.Cirrus.Nodes.AddNodes(coreNodeIdsForAdd);
                }

                return null;
            }
        }
        catch (Exception ex)
        {
            _log.Error("AddNodes failed.", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_81, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry RemoveNodes(List<int> coreNodeIds, string where, bool allSelected)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query(string.Format(
                    CultureInfo.InvariantCulture,
                    @"SELECT NcmNodeProperties.NodeID FROM Orion.Nodes AS N
                    LEFT JOIN Cirrus.NodeProperties AS NcmNodeProperties ON N.NodeID=NcmNodeProperties.CoreNodeID
                    INNER JOIN Orion.Engines AS E ON E.EngineID=N.EngineID
                    {0} 
                    AND NcmNodeProperties.CoreNodeID IS NOT NULL",
                    GetWhereClause(coreNodeIds, where, allSelected)));

                if (dt != null && dt.Rows.Count > 0)
                {
                    var ncmNodeIdsForRemove = dt.AsEnumerable().Select(row => row.Field<Guid>("NodeID")).ToArray();
                    proxy.Cirrus.Nodes.RemoveNodes(ncmNodeIdsForRemove);
                }
                return null;
            }
        }
        catch (Exception ex)
        {
            _log.Error("RemoveNodes failed.", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_82, ex);
        }
    }

    private string GetWhereClause(List<int> coreNodeIds, string where, bool allSelected)
    {
        if (allSelected)
        {
            return $@"WHERE {@where}";
        }

        return string.Format(CultureInfo.InvariantCulture, @"WHERE N.NodeID IN ({0})", string.Join(@",", coreNodeIds));
    }

    private bool TryGetLicenseOverLimit(int nodesCountToAdd, out ErrorEntry errorEntry)
    {
        var actualNodesCount = GetActualNodesCount();
        var licenseNodesCount = commonHelper.GetLicenseNodesLimit();

        if ((actualNodesCount + nodesCountToAdd) > licenseNodesCount)
        {
            var licenseOverLimit = string.Format(
                @"<table>
                <tr><td>" + Resources.NCMWebContent.WEBDATA_VK_84 + @"</td></tr>
                <tr><td>" + Resources.NCMWebContent.WEBDATA_VK_85 + @"</td></tr>
                <tr><td>" + Resources.NCMWebContent.WEBDATA_VK_86 + @"</td></tr>
                {2}                
                </table>",
                licenseNodesCount,
                actualNodesCount,
                licenseNodesCount - actualNodesCount > 0 ? string.Format(@"<tr><td>" + Resources.NCMWebContent.WEBDATA_VK_87 + @"</td></tr>", licenseNodesCount - actualNodesCount) : string.Empty
            );

            errorEntry = new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_83, new Exception(licenseOverLimit));
            return true;
        }

        errorEntry = null;
        return false;
    }

    private long GetActualNodesCount()
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query(@"SELECT COUNT(NodeID) AS NodesCount FROM Cirrus.NodeProperties");
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToInt64(dt.Rows[0][0]);
                }
            }
            return 0;
        }
        catch (Exception ex)
        {
            _log.Error("GetActualNodesCount failed.", ex);
            return 0;
        }
    }
}