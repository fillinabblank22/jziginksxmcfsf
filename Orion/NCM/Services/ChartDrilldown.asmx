`<%@ WebService Language="C#" Class="ChartDrilldown" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Globalization;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ChartDrilldown : WebService
{
    private readonly ILogger log;

    public ChartDrilldown()
    {
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetGridPaged(string property, string value, string search)
    {
        try
        {
            int pageSize = 0;
            int startRowNumber = 0;

            string sortColumn = Context.Request.QueryString[@"sort"];
            string sortDirection = Context.Request.QueryString[@"dir"];

            Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
            Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

            pageSize = Math.Max(pageSize, 25);

            string resourceId = GetValueFromQueryString(@"ResourceID");
            ResourceInfo resource = new ResourceInfo();
            resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));

            DateTime startDate;
            DateTime endDate;
            CalculateStartAndEndDate(resource, out startDate, out endDate);
            string filterClause = resource.Properties[@"FilterClause"];
            filterClause = string.IsNullOrEmpty(filterClause) ? @"1=1" : filterClause;

            bool includeDiscarded = false;
            if (!string.IsNullOrEmpty(resource.Properties[@"IncludeDiscardedConfigurations"]))
            {
                includeDiscarded = Convert.ToBoolean(resource.Properties[@"IncludeDiscardedConfigurations"]);
            }

            bool trackChanges = false;
            if (!string.IsNullOrEmpty(resource.Properties[@"TrackChanges"]))
            {
                trackChanges = Convert.ToBoolean(resource.Properties[@"TrackChanges"]);
            }

            string configType = resource.Properties[@"ConfigType"];

            string chartId = GetValueFromQueryString(@"ChartID");
            object chartType = Enum.Parse(typeof(eNCMChartType), chartId);

            int totalRows = NCMChartDetailsDAL.GetNodesCount(
                (eNCMChartType) chartType,
                startDate,
                endDate,
                includeDiscarded,
                trackChanges,
                configType,
                property,
                value,
                search,
                resource.View.LimitationID.ToString(),
                filterClause);

            DataTable pageData = NCMChartDetailsDAL.GetPagableGrid(
                (eNCMChartType) chartType,
                startDate,
                endDate,
                includeDiscarded,
                trackChanges,
                configType,
                property,
                value,
                search,
                sortColumn,
                sortDirection,
                startRowNumber + 1,
                pageSize - 1,
                resource.View.LimitationID.ToString(),
                filterClause
            );
            return new PageableDataTable(pageData, totalRows);
        }
        catch (Exception ex)
        {
            log.Error("Error getting GridPaged", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        try
        {
            string resourceId = GetValueFromQueryString(@"ResourceID");
            ResourceInfo resource = new ResourceInfo();
            resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));

            DateTime startDate;
            DateTime endDate;
            string filterClause = resource.Properties[@"FilterClause"];
            filterClause = string.IsNullOrEmpty(filterClause) ? @"1=1" : filterClause;
        
            CalculateStartAndEndDate(resource, out startDate, out endDate);

            bool includeDiscarded = false;
            if (!string.IsNullOrEmpty(resource.Properties[@"IncludeDiscardedConfigurations"]))
            {
                includeDiscarded = Convert.ToBoolean(resource.Properties[@"IncludeDiscardedConfigurations"]);
            }

            bool trackChanges = false;
            if (!string.IsNullOrEmpty(resource.Properties[@"TrackChanges"]))
            {
                trackChanges = Convert.ToBoolean(resource.Properties[@"TrackChanges"]);
            }
            string configType = resource.Properties[@"ConfigType"];

            string chartId = GetValueFromQueryString(@"ChartID");
            object chartType = Enum.Parse(typeof(eNCMChartType), chartId);

            DataTable dt = NCMChartDetailsDAL.GetGroupByValues((eNCMChartType)chartType, property, startDate, endDate, includeDiscarded, trackChanges, configType, resource.View.LimitationID.ToString(), filterClause);
            return dt;
        }
        catch (Exception ex)
        {
            log.Error("Error getting ValuesAndCountForProperty", ex);
            throw;
        }
    }

    private void CalculateStartAndEndDate(ResourceInfo resource, out DateTime startDate, out DateTime endDate)
    {
        string timeRange = string.IsNullOrEmpty(resource.Properties[@"TimeRange"])
            ? @"As of Last Update"
            : resource.Properties[@"TimeRange"];
        string dateFrom = resource.Properties[@"DateFrom"];
        string dateTo = resource.Properties[@"DateTo"];

        if (timeRange.Equals(@"As of Last Update", StringComparison.InvariantCultureIgnoreCase))
        {
            startDate = DateTime.MinValue;
            endDate = DateTime.MinValue;
        }
        else if (timeRange.Equals(@"Custom", StringComparison.InvariantCultureIgnoreCase))
        {
            startDate = Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(dateTo))
            {
                endDate = Convert.ToDateTime(dateTo, CultureInfo.InvariantCulture);
            }
            else
            {
                endDate = DateTime.Now; 
            }
        }
        else
        {
            endDate = DateTime.Now;
            if (timeRange.Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                endDate = new DateTime(endDate.Year, endDate.Month, endDate.AddDays(-endDate.Day + 1).Day).AddMilliseconds(-1);
            }
            startDate = TimeParser.GetTime(timeRange);
        }
    }    

    private string GetValueFromQueryString(string name)
    {
        Uri uri = HttpContext.Current.Request.UrlReferrer;
        return HttpUtility.ParseQueryString(uri.Query).Get(name);
    }
}