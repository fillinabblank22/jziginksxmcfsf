﻿<%@ WebService Language="C#" Class="EoLEoS" %>

using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Linq;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources.Eos;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class EoLEoS : WebService
{
    private readonly Log _log = new Log();
    private readonly IEosService eosService;

    public EoLEoS()
    {
        eosService = ServiceLocator.Container.Resolve<IEosService>();
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetEoLEoSData(string property, string value, string search, bool displayIgnoredDevices, double clientOffset)
    {
        var pageSize = 0;
        var startRowNumber = 0;

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        value = value.Replace(@"'", @"''");
        search = search.Replace(@"'", @"''");

        var dataProvider = new EosDataProvider()
        {
            GroupByColumn = CustomPropertyHelper.CovertGroupingPropertyName(property),
            GroupByValue = value,
            SearchTerm = search,
            SortDirection = Context.Request.QueryString[@"dir"],
            SortColumn = Context.Request.QueryString[@"sort"],
            DisplayIgnoredDevices = displayIgnoredDevices,
            FiltersCollection = Context.Request.QueryString
        };

        var dt = dataProvider.GetPageableEosData(startRowNumber + 1, startRowNumber + pageSize);
        var totalCount = dataProvider.GetEosDataRowCount();

        var localTime = DateTimeOffset.Now;
        var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;

        var dateTimeCustomProperties = new List<string>();
        foreach (DataColumn col in dt.Columns)
        {
            var colName = col.ColumnName;
            CustomProperty cp;
            if (CustomPropertyHelper.IsCustomProperty(colName, out cp) && cp.PropertyType == typeof(DateTime))
            {
                dateTimeCustomProperties.Add(colName);
            }
        }

        if (dateTimeCustomProperties.Any())
        {
            foreach (DataRow row in dt.Rows)
            {
                dateTimeCustomProperties.ForEach(colName =>
                {
                    ConvertTimeToServerTime(row, colName, offsetMinutes);
                });
            }
        }

        return new PageableDataTable(dt, totalCount);
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property, bool displayIgnoredDevices, double clientOffset)
    {
        if (string.IsNullOrEmpty(property))
            return null;

        property = CustomPropertyHelper.CovertGroupingPropertyName(property);

        var propertyType = typeof(String);
        CustomProperty cp;
        if (CustomPropertyHelper.IsCustomProperty(property, out cp))
        {
            propertyType = cp.PropertyType;
        }

        var query = new StringBuilder();
        query.AppendFormat(@" 
SELECT {0} AS theValue, 
	   COUNT(ISNULL({1},'')) AS theCount 
FROM Cirrus.NodeProperties AS NCMNodeProperties
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON NCMNodeProperties.CoreNodeID=OrionCustomProperties.NodeID
LEFT JOIN (SELECT NodeID, MIN(Model) AS Model FROM Cirrus.EntityPhysical WHERE (EntityClass='3') GROUP BY NodeID) AS EntityPhysical
    ON EntityPhysical.NodeID=NCMNodeProperties.NodeID
WHERE ({2})
GROUP BY {0}
ORDER BY {0} ASC",
        propertyType == typeof(String) ? $@"ISNULL({CustomPropertyHelper.DecodePropertyName(property)},'')" : CustomPropertyHelper.DecodePropertyName(property),
        CustomPropertyHelper.DecodePropertyName(property),
        BuildIgnoredDevicesClause(displayIgnoredDevices));

        var dt = ExecuteSWQL(query.ToString());

        var localTime = DateTimeOffset.Now;
        var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;

        if (cp != null && cp.PropertyType == typeof(DateTime))
        {
            foreach (DataRow row in dt.Rows)
            {
                ConvertTimeToServerTime(row, @"theValue", offsetMinutes);
            }
        }
        return dt;
    }

    [WebMethod(EnableSession = true)]
    public void RenewMatches()
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.Eos.BeginRefreshAll();
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error renewing matches.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool IsRenewMatchesInProgress()
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                return proxy.Cirrus.Eos.IsRefreshingAll();
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error checking renew matches progress.", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public void DeleteEosData(Guid[] nodeIds, Dictionary<string, string> dataProvider)
    {
        try
        {
            Guid[] eos_NodeIds = null;
            if (nodeIds != null)
            {
                eos_NodeIds = nodeIds;
            }
            else if (dataProvider != null)
            {
                eos_NodeIds = GetAllSelectedNodeIds(dataProvider).ToArray();
            }

            if (eos_NodeIds != null && eos_NodeIds.Length > 0)
            {
                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    proxy.Cirrus.Nodes.DeleteEOSData(eos_NodeIds);
                }
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error deleting EOS data.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> BulkEosAssign(List<string> nodeIds, Dictionary<string, string> dataProvider)
    {
        try
        {
            Dictionary<string, object> result = null;

            Session.Remove(NodeItem.cSessionKey);

            DataTable dt = null;
            if (nodeIds != null)
            {
                dt = GetSelectedNodesTable(nodeIds);
            }
            else if (dataProvider != null)
            {
                dt = GetAllSelectedNodesTable(dataProvider);
            }

            if (dt != null)
            {
                var nodeItems = dt.AsEnumerable()
                    .Select(row => new NodeItem
                    {
                        NodeId = row.Field<Guid>("NodeID"),
                        AgentIP = row.Field<string>("AgentIP"),
                        NodeCaption = row.Field<string>("NodeCaption"),
                        MachineType = row.Field<string>("MachineType"),
                        OSVersion = row.Field<string>("OSVersion"),
                        VendorIcon = row.Field<string>("VendorIcon"),
                        SystemOID = row.Field<string>("SystemOID"),
                        Model = row.Field<string>("EntityPhysical_Model")
                    }).ToList();

                Session.Add(NodeItem.cSessionKey, nodeItems);

                result = new Dictionary<string, object>();

                result[@"success"] = true;
                result[@"hasNodesWithDatesAssigned"] = dt.AsEnumerable().Any(row =>
                    row.Field<int>("EosType").Equals((int)eEoSType.User) ||
                    row.Field<int>("EosType").Equals((int)eEoSType.Manual) ||
                    row.Field<int>("EosType").Equals((int)eEoSType.Auto)
                    );
            }

            return result;
        }
        catch (Exception ex)
        {
            _log.Error("Error EOS assigning.", ex);
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public void IgnoreDevices(Guid[] nodeIds, Dictionary<string, string> dataProvider)
    {
        try
        {
            Guid[] eos_NodeIds = null;
            if (nodeIds != null)
            {
                eos_NodeIds = nodeIds;
            }
            else if (dataProvider != null)
            {
                eos_NodeIds = GetAllSelectedNodeIds(dataProvider).ToArray();
            }

            if (eos_NodeIds != null && eos_NodeIds.Length > 0)
            {
                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    proxy.Cirrus.Nodes.ChangeEOSType(eos_NodeIds, eEoSType.Ignored);
                }
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error ignoring devices.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void RemoveFromIgnoredDevices(Guid[] nodeIds, Dictionary<string, string> dataProvider)
    {
        try
        {
            Guid[] eos_NodeIds = null;
            if (nodeIds != null)
            {
                eos_NodeIds = nodeIds;
            }
            else if (dataProvider != null)
            {
                eos_NodeIds = GetAllSelectedNodeIds(dataProvider).ToArray();
            }

            if (eos_NodeIds != null && eos_NodeIds.Length > 0)
            {
                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    proxy.Cirrus.Nodes.ChangeEOSType(eos_NodeIds, eEoSType.NotAssigned);
                }
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error removing ignored devices.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> ShowEosNotification()
    {
        try
        {
            return eosService.ShowEosNotification().ToDictionary(p => p.Key, p => p.Value);
        }
        catch (Exception ex)
        {
            _log.Error("Error showing EOS notification.", ex);
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public List<string> SplitGridColumnModel(string columnModel)
    {
        const int maxLength = 255;

        var list = new List<string>();
        for (var i = 0; i < columnModel.Length; i += maxLength)
        {
            var length = Math.Min(maxLength, columnModel.Length - i);
            list.Add(columnModel.Substring(i, length));
        }
        return list;
    }

    #region private

    private IEnumerable<Guid> GetAllSelectedNodeIds(Dictionary<string, string> dataProvider)
    {
        var eosDataProvider = new EosDataProvider
        {
            GroupByColumn = dataProvider[@"groupBy"],
            GroupByValue = dataProvider[@"groupByValue"],
            SearchTerm = dataProvider[@"searchTerm"],
            SortDirection = dataProvider[@"sortDirection"],
            SortColumn = dataProvider[@"sortColumn"],
            DisplayIgnoredDevices = Convert.ToBoolean(dataProvider[@"displayIgnoredDevices"]),
            FiltersCollection = HttpUtility.ParseQueryString(dataProvider[@"filters"])
        };

        var dt = eosDataProvider.GetEosData();
        return dt.AsEnumerable().Select(row => row.Field<Guid>("NodeID"));
    }

    private DataTable GetAllSelectedNodesTable(Dictionary<string, string> dataProvider)
    {
        var eosDataProvider = new EosDataProvider()
        {
            GroupByColumn = dataProvider[@"groupBy"],
            GroupByValue = dataProvider[@"groupByValue"],
            SearchTerm = dataProvider[@"searchTerm"],
            SortDirection = dataProvider[@"sortDirection"],
            SortColumn = dataProvider[@"sortColumn"],
            DisplayIgnoredDevices = Convert.ToBoolean(dataProvider[@"displayIgnoredDevices"]),
            FiltersCollection = HttpUtility.ParseQueryString(dataProvider[@"filters"])
        };

        var dt = eosDataProvider.GetEosData();
        return dt;
    }

    private DataTable GetSelectedNodesTable(List<string> nodeIds)
    {
        var swql = $@"
SELECT NCMNodeProperties.NodeID, 
	   Nodes.Caption AS NodeCaption, 
       Nodes.IPAddress AS AgentIP,
       Nodes.MachineType,
       Nodes.IOSVersion AS OSVersion,
       Nodes.VendorIcon,
       Nodes.SysObjectID AS SystemOID,
       ISNULL(NCMNodeProperties.EosType, 0) AS EosType,
       EntityPhysical.Model AS EntityPhysical_Model
FROM Cirrus.NodeProperties AS NCMNodeProperties
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
LEFT JOIN (SELECT NodeID, MIN(Model) AS Model FROM Cirrus.EntityPhysical WHERE (EntityClass='3') GROUP BY NodeID) AS EntityPhysical 
    ON EntityPhysical.NodeID=NCMNodeProperties.NodeID
WHERE NCMNodeProperties.NodeID IN ({string.Join(@",", nodeIds.Select(item => $@"'{item}'"))})";

        var dt = ExecuteSWQL(swql);
        return dt;
    }

    private DataTable ExecuteSWQL(string swql)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swql);
            return dt;
        }
    }

    private string BuildIgnoredDevicesClause(bool displayIgnoredDevices)
    {
        if (displayIgnoredDevices)
        {
            return @" (1=1) ";
        }

        return $@" (NCMNodeProperties.EosType IS NULL OR NCMNodeProperties.EosType <> {(int) eEoSType.Ignored}) ";
    }

    private void ConvertTimeToServerTime(DataRow row, string colName, double offsetMinutes)
    {
        if (row[colName] == DBNull.Value)
            return;

        if (row[colName].GetType() == typeof(DateTime))
            row[colName] = Convert.ToDateTime(row[colName]).AddMinutes(offsetMinutes);
    }

    #endregion
}