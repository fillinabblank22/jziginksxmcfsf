﻿<%@ WebService Language="C#" Class="TransferStatus" %>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Resources;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TransferStatus : WebService
{
    private readonly ILogger logger;

    public TransferStatus()
    {
        logger = ServiceLocator.Container.Resolve<ILoggerFactory>().Create(GetType());
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetTransfersPaged(string search, double clientOffset)
    {
        var pageSize = 0;
        var startRowNumber = 0;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        var swql = BuildPageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, search);

        var pageData = ExecuteSWQL(swql);
        Lookup(pageData, clientOffset);
        return new PageableDataTable(pageData, GetRowCount(search));
    }

    [WebMethod(EnableSession = true)]
    public void ClearTransfers(IEnumerable<Guid> ids)
    {
        var processAll = ids == null || !ids.Any();

        var idsToRemove = processAll ? SelectAllTransferIds() : ids;
        var idsToCancel = (processAll ? SelectAllTransferIds(
            $@"(TR.Status = {(int) eTransferState.Queued} OR TR.Status = {(int) eTransferState.Transfering})") : ids);
        InvokeDeleteTransVerb(idsToRemove);
        InvokeCancelTransVerb(idsToCancel);
    }

    [WebMethod(EnableSession = true)]
    public void CancelTransfers(IEnumerable<Guid> ids)
    {
        var processAll = ids == null || !ids.Any();

        var idsToCancel = (processAll ? SelectAllTransferIds(
            $@"(TR.Status = {(int) eTransferState.Queued} OR TR.Status = {(int) eTransferState.Transfering})") : ids);

        InvokeDeleteTransVerb(idsToCancel);
        InvokeCancelTransVerb(idsToCancel);
    }

    [WebMethod(EnableSession = true)]
    public void ClearCompletedOrFailedTransfers(bool failedOnly)
    {
        var idsToRemove = SelectAllTransferIds(failedOnly
                                     ? $@"TR.Status = {(int) eTransferState.Error}"
                                     : $@"TR.Status = {(int) eTransferState.Complete}");
        InvokeDeleteTransVerb(idsToRemove);
    }

    [WebMethod(EnableSession = true)]
    public string GetScriptResults(string[] transferIds)
    {
        var swql = string.Format(@"
SELECT 
    CASE 
	    WHEN TR.Status={3} THEN TR.DeviceOutput 
	    WHEN TR.Status={4} THEN TR.ErrorMessage 
    END AS DeviceOutput,
    TR.DateTime, 
    TR.UserName,
    TR.NodeProperties.Nodes.Caption,
    TR.NodeProperties.Nodes.IPAddress
FROM NCM.TransferResults AS TR 
WHERE (TR.Action = {1} OR TR.Action = {2}) AND TR.TransferID in ({0})",
            string.Join(@",", transferIds.Select(x => $@"'{x}'")),
            (int)eTransferType.ExecuteScript,
            (int)eTransferType.Upload,
            (int)eTransferState.Complete,
            (int)eTransferState.Error);

        var dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
        {
            return BuildScriptResult(dt);
        }
        return string.Empty;
    }

    private string BuildScriptResult(DataTable dt)
    {
        var sb = new StringBuilder();
        foreach (DataRow row in dt.Rows)
        {
            var subtitle =
                $@"{Convert.ToDateTime(row["DateTime"])} on {Convert.ToString(row["Caption"])} ({Convert.ToString(row["IPAddress"])}) by {Convert.ToString(row["UserName"])}";

            sb.AppendFormat(@"{0}{1}{1}{2}{1}{1}{3}{1}{1}", subtitle, Environment.NewLine, row["DeviceOutput"], new String('_', 75));
        }
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public string GetTransStatusDetails(IEnumerable<Guid> ids)
    {
        if (ids != null && ids.Any())
        {
            var swql = @"
SELECT 
    TR.TransferId, 
    TR.NodeID, 
    TR.Status,
    TR.Action, 
    TR.ErrorMessage,
    TR.ConfigID, 
    '' as StatusDetails, 
    CASE WHEN TR.DeviceOutput IS NOT NULL THEN '' ELSE NULL END AS DeviceOutput
FROM NCM.TransferResults AS TR
WHERE TR.TransferId in ({0})";

            var dt = ExecuteSWQL(string.Format(swql, string.Join(@",", ids.Select(x => $@"'{x}'"))));

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                    LookupTransferDetails(row);

                return JsonConvert.SerializeObject(
                        dt.AsEnumerable().Select(x => new
                        {
                            id = x.Field<Guid>("TransferId"),
                            status = x.Field<int>("Status"),
                            statusDetails = x.Field<string>("StatusDetails"),
                            action = x.Field<int>("Action"),
                            nodeId = x.Field<Guid>("NodeID")
                        }));
            }

        }
        return JsonConvert.SerializeObject(null);
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry RetryScript(string[] ids)
    {
        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                var transferIdGuids = ids.Select(Guid.Parse).ToArray();
                proxy.Cirrus.ConfigArchive.ReExecute(transferIdGuids);
            }
            return null;
        }
        catch (Exception ex)
        {
            logger.Error("Error in retrying a script", ex);
            return new ErrorEntry(ex);
        }
    }

    [Localizable(false)] private static readonly Dictionary<string, string> columnsNameWithAlias =
        new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
            {
                {"DateTime","TR.DateTime"},
                {"Action","TR.Action"},
                {"NodeName","ONS.Caption"},
                {"IpAddress","ONS.IPAddress"},
                {"UserName","TR.UserName, TR.DateTime"},
                {"StatusDetails","TR.Status"}
            };

    private string BuildPageableQuery(int pageSize, int startRow, string sortColumn, string sortDirection, string search)
    {
        return string.Format(@"
SELECT TR.TransferId, 
    TR.DateTime, 
    TR.Action,
    ONS.Caption,
    ONS.IPAddress,
    TR.UserName,
    '' as StatusDetails,
    '' as ActionIcon, 
    NP.CoreNodeID,
    TR.Status, 
    TR.ErrorMessage, 
    TR.ConfigID, 
    TR.NodeID as NodeID, 
    CASE WHEN TR.DeviceOutput IS NOT NULL THEN '' ELSE NULL END AS DeviceOutput
FROM NCM.TransferResults AS TR
INNER JOIN NCM.NodeProperties AS NP ON NP.NodeID = TR.NodeID
INNER JOIN Orion.Nodes AS ONS  ON ONS.NODEID = NP.CoreNodeID
WHERE ({3}) AND ({4})
ORDER BY {0} 
WITH ROWS {1} to {2}",
                            GetSpecialOrderByClause(sortColumn, sortDirection),
                             startRow + 1,
                             startRow + pageSize,
                             GetAccountLimitClause(),
                             GetSearchClause(search));
    }


    private string GetSpecialOrderByClause(string sortColumn, string sortDirection)
    {
        if (sortColumn.Equals(@"UserName", StringComparison.CurrentCultureIgnoreCase))
            return $@"TR.UserName {sortDirection}, TR.DateTime desc";
        return $@"{columnsNameWithAlias[sortColumn]} {sortDirection}";

    }

    private DataTable ExecuteSWQL(string swql)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Query(swql);
        }
    }

    private void InvokeDeleteTransVerb(IEnumerable<Guid> ids)
    {
        if (ids == null)
            return;

        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            proxy.Cirrus.ConfigArchive.ClearTransfers(ids.ToArray());
        }
    }

    private void InvokeCancelTransVerb(IEnumerable<Guid> ids)
    {
        if (ids == null)
            return;

        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            proxy.Cirrus.ConfigArchive.CancelTransfers(ids.ToArray());
        }
    }

    private void Lookup(DataTable dt, double clientOffset)
    {
        foreach (DataRow row in dt.Rows)
        {
            OffsetDateTime(row, clientOffset);
            LookupTransferDetails(row);
        }
    }

    private long GetRowCount(string search)
    {
        var swqlFormat = @"
SELECT COUNT(TR.TransferId) as cnt FROM NCM.TransferResults AS TR
INNER JOIN NCM.NodeProperties AS NP ON NP.NodeID = TR.NodeID
INNER JOIN Orion.Nodes AS ONS  ON ONS.NODEID = NP.CoreNodeID
WHERE ({0}) AND ({1})";

        var rowCountTable = ExecuteSWQL(string.Format(swqlFormat, GetAccountLimitClause(), GetSearchClause(search)));
        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private readonly string ExecuteScript_Complete_Template = $@"<img src=""/Orion/NCM/Resources/images/Status.Complete.gif""><span>{NCMWebContent.TransferStatus_StatusLabel_Complete}</span>&nbsp;&nbsp;<a href=""#"" class=""scriptResults"" value=""{{0}}"">{NCMWebContent.TransferStatus_Link_ShowScriptResults}</a>";
    private readonly string ExecuteScript_Error_Template = $@"<img src=""/Orion/NCM/Resources/images/Status.Error.gif"" style=""vertical-align:middle;""><span>{0}</span>&nbsp;&nbsp;<a href=""#"" class=""scriptResults"" value=""{{1}}"">{NCMWebContent.TransferStatus_Link_ShowScriptResults}</a>";
    private readonly string Download_Complete_Template = $@"<img src=""/Orion/NCM/Resources/images/Status.Complete.gif""><span>{NCMWebContent.TransferStatus_StatusLabel_Complete}</span>&nbsp;&nbsp;<a target=""_blank"" href=""ConfigDetails.aspx?ConfigID={{0}}"">{NCMWebContent.TransferStatus_Link_DownloadedConfig}</a>";
    private readonly string Download_Complete_Rejected_Template = $@"<img src=""/Orion/NCM/Resources/images/Status.Complete.gif""><span>{NCMWebContent.TransferStatus_StausLabel_ConfigRejected}</span>";
    private readonly string Upload_Complete_Template = $@"<img src=""/Orion/NCM/Resources/images/Status.Complete.gif"" style=""vertical-align:middle;""><span>{NCMWebContent.TransferStatus_StatusLabel_Complete}</span>&nbsp;&nbsp;<a href=""#"" class=""scriptResults"" value=""{{0}}"">{NCMWebContent.TransferStatus_Link_ShowUploadResults}</a>";
    private readonly string Upload_Error_Template = $@"<img src=""/Orion/NCM/Resources/images/Status.Error.gif"" style=""vertical-align:middle;""><span>{0}</span>&nbsp;&nbsp;<a href=""#"" class=""scriptResults"" value=""{{1}}"">{NCMWebContent.TransferStatus_Link_ShowUploadResults}</a>";
    private readonly string Transfer_Error_Template = @"<img src=""/Orion/NCM/Resources/images/Status.Error.gif"" style=""vertical-align:middle;""><span>{0}</span>";

    private void OffsetDateTime(DataRow row, double clientOffset)
    {
        if (row["DateTime"] != DBNull.Value)
        {
            var localTime = DateTimeOffset.Now;
            var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;

            var time = Convert.ToDateTime(row["DateTime"]).AddMinutes(offsetMinutes);
            row["DateTime"] = time;
        }
    }

    private void LookupTransferDetails(DataRow row)
    {
        var actionType = (eTransferType)Convert.ToInt32(row["Action"]);
        var isConfigRejected = false;
        if (row["ConfigID"] == DBNull.Value) isConfigRejected = true;
        else if (Guid.Parse(row["ConfigID"].ToString()) == Guid.Empty) isConfigRejected = true;

        var status = (eTransferState)Convert.ToInt32(row["Status"]);
        var errorMessage = row["ErrorMessage"] as string;

        var statusMsg = string.Empty;

        if (status != eTransferState.Error)
        {
            if (status == eTransferState.Complete)
            {
                if (actionType == eTransferType.Downlaod)
                    if (isConfigRejected) statusMsg = Download_Complete_Rejected_Template;
                    else statusMsg = string.Format(Download_Complete_Template, row["ConfigID"]);

                if (actionType == eTransferType.ExecuteScript)
                    statusMsg = string.Format(ExecuteScript_Complete_Template, row["TransferId"]);

                if (actionType == eTransferType.Upload)
                    statusMsg = string.Format(Upload_Complete_Template, row["TransferId"]);
            }
            else
            {
                statusMsg = LookupTransferProgressMsg(actionType, status);
            }
        }
        else
        {
            if (actionType == eTransferType.ExecuteScript && !row.IsNull("DeviceOutput"))
                statusMsg = string.Format(ExecuteScript_Error_Template, errorMessage, row["TransferId"]);
            else if (actionType == eTransferType.Upload && !row.IsNull("DeviceOutput"))
                statusMsg = string.Format(Upload_Error_Template, errorMessage, row["TransferId"]);
            else
                statusMsg = string.Format(Transfer_Error_Template, errorMessage);
        }

        row["StatusDetails"] = statusMsg;
    }

    private const string Transfer_Progress_Template = "<img src='/Orion/NCM/Resources/images/spinner.gif'/><span>{0}...</span>";
    private string LookupTransferProgressMsg(eTransferType type, eTransferState state)
    {
        if (state == eTransferState.Queued)
            return string.Format(Transfer_Progress_Template, NCMWebContent.TransferStatus_StatusLable_Waiting);

        if (state == eTransferState.Transfering)
        {
            if (type == eTransferType.Downlaod)
                return string.Format(Transfer_Progress_Template, NCMWebContent.TransferStatus_StatusLable_Downloading);

            if (type == eTransferType.ExecuteScript)
                return string.Format(Transfer_Progress_Template, NCMWebContent.TransferStatus_StatusLable_Executing);

            if (type == eTransferType.Upload)
                return string.Format(Transfer_Progress_Template, NCMWebContent.TransferStatus_StatusLable_Uploading);
        }

        return string.Empty;
    }

    private string GetAccountLimitClause()
    {
        var isAdmin = SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR);
        bool showOnlyForCurrentUser;
        if (isAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) showOnlyForCurrentUser = false;
        else showOnlyForCurrentUser = true;


        if (showOnlyForCurrentUser)
        {
            return $@"TR.UserName='{HttpContext.Current.Profile.UserName}'";
        }
        return @"(1=1)";
    }

    private string GetSearchClause(string search)
    {
        if (string.IsNullOrEmpty(search))
            return @"(1=1)";

        var searchValue = SearchHelper.WildcardToLike(search);
        var swqlFormat = @"(ONS.Caption LIKE '%{0}%' OR ONS.IPAddress LIKE '%{0}%')";
        return string.Format(swqlFormat, searchValue);
    }

    private IEnumerable<Guid> SelectAllTransferIds(string whereClause = null)
    {
        var swql = $@"SELECT TR.TransferId FROM NCM.TransferResults AS TR
INNER JOIN NCM.NodeProperties AS NP ON NP.NodeID = TR.NodeID
INNER JOIN Orion.Nodes AS ONS  ON ONS.NODEID = NP.CoreNodeID
WHERE {GetAccountLimitClause()} AND {whereClause ?? @"1=1"}";

        var dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
        {
            var res = dt.AsEnumerable().Select(row => Guid.Parse(row[0].ToString()));
            return res.ToList();
        }
        return null;
    }
}
