<%@ WebService Language="C#" Class="PolicyReportManagement" %>

using System;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PolicyReportManagement : WebService
{
    private readonly Log log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetPolicyReportsPaged(string property, string value, string search)
    {

        var pageSize = 0;
        var startRowNumber = 0;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        value = value.Replace(@"'", @"''");
        search = search.Replace(@"'", @"''");

        var swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, property, value, search);

        var pageData = ExecuteSWQL(swql);
        return new PageableDataTable(pageData, GetRowCount(property, value, search));
    }


    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        // return "All" as the blank/unknown value
        var swql = @"
    SELECT '' AS theValue, COUNT(PR.PolicyReportID) AS theCount, 0 AS theOrdinalNumber
    FROM Cirrus.PolicyReports PR
    UNION ALL
    (
     SELECT DISTINCT PR.Grouping AS theValue, COUNT(PR.Grouping) AS theCount, 1 AS theOrdinalNumber
     FROM Cirrus.PolicyReports PR WHERE PR.Grouping <> ''
     GROUP BY PR.Grouping
    )
    UNION ALL
    (
     SELECT 'No Folder' AS theValue, COUNT(PR.Grouping) AS theCount, 2 AS theOrdinalNumber
     FROM Cirrus.PolicyReports PR WHERE PR.Grouping = ''
    ) ORDER BY theOrdinalNumber, theValue";

        var dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
        {
            var count = dt.Rows.Count;
            var lastRow = dt.Rows[count - 1];
            var value = int.Parse(lastRow["theCount"].ToString());
            if (value == 0)
            {
                dt.Rows.Remove(lastRow);
            }
        }
        return dt;
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry UpdateReportStatus(Guid[] selectedReportsIDs, string reportStatus, Dictionary<string, string> gridParams)
    {
        try
        {
            var status = (ReportStatus)Enum.Parse(typeof(ReportStatus), reportStatus);
            
            Guid[] reportIds = null;
            if (selectedReportsIDs != null)
            {
                reportIds = selectedReportsIDs;
            }
            else if (gridParams != null)
            {
                reportIds = GetAllSelectedPolicyReportIds(gridParams).ToArray();
            }

            if (reportIds != null && reportIds.Length > 0)
            {
                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    proxy.Cirrus.PolicyReports.UpdateReportStatus(status, reportIds);
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in UpdateReprotStatus", ex);
            return new ErrorEntry(Resources.NCMWebContent.PolicyReportManagement_UpdateReportStatusTitle, ex);
        }        
    }
    
    [WebMethod(EnableSession = true)]
    public ErrorEntry RunUpdateAll()
    {
        var cacheRuuning = IsCacheRunning();

        if (cacheRuuning)
            return new ErrorEntry(Resources.NCMWebContent.PolicyReportManagement_PolicyCacheErrorTitle, Resources.NCMWebContent.PolicyReportManagement_CacheIsAlreadyRunningErrorMessage, false);

        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.PolicyReports.StartCaching();
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in RunUpdateAll", ex);
            return new ErrorEntry(Resources.NCMWebContent.PolicyReportManagement_UpdateAllErrorTitle, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry RunUpdateSelected(Guid[] selectedReportsIDs, Dictionary<string, string> gridParams)
    {
        var cacheRuuning = IsCacheRunning();

        if (cacheRuuning)
            return new ErrorEntry(Resources.NCMWebContent.PolicyReportManagement_PolicyCacheErrorTitle, Resources.NCMWebContent.PolicyReportManagement_CacheIsAlreadyRunningErrorMessage, false);

        try
        {
            Guid[] reportIds = null;
            if (selectedReportsIDs != null)
            {
                reportIds = selectedReportsIDs;
            }
            else if (gridParams != null)
            {
                reportIds = GetAllSelectedPolicyReportIds(gridParams).ToArray();
            }

            if (reportIds != null && reportIds.Length > 0)
            {
                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    proxy.Cirrus.PolicyReports.StartCaching(reportIds);
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in RunUpdateSelected", ex);
            return new ErrorEntry(Resources.NCMWebContent.PolicyReportManagement_UpdateSeletectedErrorTitle, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetCacheUpdatingStatus()
    {
        var res = IsCacheRunning();
        if (res)
            return @"{success:true,running:true}";
        return @"{success:true,running:false}";
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeletePolicyReports(Guid[] selectedReportsIDs, bool deleteChilds, Dictionary<string, string> gridParams)
    {
        try
        {
            Guid[] reportIds = null;
            if (selectedReportsIDs != null)
            {
                reportIds = selectedReportsIDs;
            }
            else if (gridParams != null)
            {
                reportIds = GetAllSelectedPolicyReportIds(gridParams).ToArray();
            }

            if (reportIds != null && reportIds.Length > 0)
            {
                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    proxy.Cirrus.PolicyReports.DeletePolicyReports(reportIds, deleteChilds);
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in DeletePolicyReports", ex);
            return new ErrorEntry(Resources.NCMWebContent.PolicyReportManagement_DeleteErrorTitle, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public string FindReportIndexInGrid(string sort, string dir, string reportId, int limit)
    {
        var pageSize = 0;
        var start = 0;
        var success = false;

        var sortColumn = string.IsNullOrEmpty(sort) ? @"Name" : sort;
        var sortDirection = string.IsNullOrEmpty(dir) ? @"ASC" : dir;
        pageSize = limit;

        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var rowNumber = proxy.Cirrus.PolicyReports.GetRowNumber(@"NCM_PolicyReports", sortColumn, sortDirection,
                $@"WHERE PolicyReportID='{reportId}'");

            success = true;
            var pos = rowNumber - 1;
            start = ((int)pos / pageSize) * pageSize;

            var jsonRes = $@"{{success:{success.ToString().ToLowerInvariant()},data:{{start:{start}}}}}";

            return jsonRes;
        }
    }

    #region Private Methods

    private IEnumerable<Guid> GetAllSelectedPolicyReportIds(Dictionary<string, string> gridParams)
    {
        var dt = ExecuteSWQL($@"
                SELECT 
                    PolicyReportID
                FROM Cirrus.PolicyReports AS P
                WHERE {GetGroupWhereClause(@"P", gridParams[@"groupByValue"])}
                AND {GetSearchClause(@"P", gridParams[@"searchTerm"])}");
        return dt.AsEnumerable().Select(row => row.Field<Guid>("PolicyReportID"));
    }
    
    private bool IsCacheRunning()
    {
        var swql = @"select top 1 PolicyReportID from Cirrus.PolicyReports where (CacheStatus = 1 or CacheStatus = 2 or CacheStatus =5)";
        var dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
            return true;
        return false;
    }

    private long GetRowCount(string property, string value, string search)
    {
        const string swqlFormat = @"
            SELECT Count(A.PolicyReportID) as theCount
            FROM Cirrus.PolicyReports A
            WHERE {0}
            AND {1}";

        var swql = String.Format(swqlFormat, GetGroupWhereClause(@"A", value), GetSearchClause(@"A", search));

        var rowCountTable = ExecuteSWQL(swql);

        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private DataTable ExecuteSWQL(string swql)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swql);
            return dt;
        }
    }

    private string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderByDirection, string property, string value, string search)
    {
        pageSize = Math.Max(pageSize, 25);
        var pageNumber = (startRowNumber / pageSize);

        const string swqlFormat = @"
        SELECT TOP {0} T1.PolicyReportID as ReportID, 
			          T1.Name, 
			          T1.Comment,
                      T1.Grouping,
                      T1.LastModified,
                      T1.LastUpdated,
                      T1.CacheStatus,
                      T1.ReportStatus,
                      T1.LastError                                                                                        		           
        FROM Cirrus.PolicyReports T1
        WHERE T1.PolicyReportID NOT IN(
	        SELECT TOP {1} T.PolicyReportID
	        FROM Cirrus.PolicyReports T
	        WHERE {4}
	        ORDER BY T.{2} {3}
	        )
        AND {5}
        AND {6}	        
        ORDER BY T1.{2} {3}";

        var swql = String.Format(swqlFormat,
                                    pageSize,
                                    pageNumber * pageSize,
                                    orderByColumn,
                                    orderByDirection,
                                    GetGroupWhereClause(@"T", value),
                                    GetGroupWhereClause(@"T1", value),
                                    GetSearchClause(@"T1", search)
                                    );

        return swql;
    }

    private string GetGroupWhereClause(string tableAlias, string value)
    {
        var whereClause = @"1=1";

        if (String.IsNullOrEmpty(value) || value.Equals(@"null", StringComparison.OrdinalIgnoreCase))
            return whereClause;

        value = value.Replace(@"No Folder", string.Empty);
        whereClause = $@"{tableAlias}.PolicyReportID IN (
	                                SELECT T.PolicyReportID
	                                FROM Cirrus.PolicyReports T
	                                WHERE T.[Grouping] = '{value}')";
        return whereClause;

    }

    private string GetSearchClause(string tableAlias, string search)
    {
        if (String.IsNullOrEmpty(search))
            return @"(1=1)";

        var searchValue = SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers.SearchHelper.WildcardToLike(search);

        var swqlFormat = @"
            ({0}.Name LIKE '%{1}%' 
             OR {0}.PolicyReportID IN (
                    SELECT T.PolicyReportID
                    FROM Cirrus.PolicyReports T
                    WHERE T.[Grouping] LIKE '%{1}%'
                )             
            )";

        var clause = String.Format(swqlFormat, tableAlias, searchValue);
        return clause;
    }

    #endregion
}