<%@ WebService Language="C#" Class="ConfigSnippets" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ConfigSnippets  : WebService
{
    private readonly ILogger logger;

    public ConfigSnippets()
    {
        logger = ServiceLocator.Container.Resolve<ILoggerFactory>().Create(GetType());
    }

    #region public methods

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetSnippetsPaged(string property, string value, string search)
    {
        var pageSize = 0;
        var startRowNumber = 0;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        value = value.Replace(@"'", @"''");
        search = search.Replace(@"'", @"''");

        var swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, property, value, search);

        var pageData = ExecuteSWQL(swql);
        AddTagDataToTemplateTable(pageData);
        return new PageableDataTable(pageData, GetRowCount(property, value, search));
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        // return "All" as the blank/unknown value
        var swql = @"
      SELECT '' as theValue, count(CS.ID) as theCount
      FROM Cirrus.ConfigSnippets CS
    UNION ALL (        
      SELECT T.TagName as theValue, count(T.TagName) as theCount
      FROM Cirrus.Tags T
      GROUP BY T.TagName
    )";

        return ExecuteSWQL(swql);
    }

    [WebMethod]
    public ErrorEntry DeleteSnippets(List<int> snippetIds)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.Snippets.DeleteSnippets(snippetIds.ToArray());
            }

            return null;
        }
        catch (Exception ex)
        {
            logger.Error("Error Delete Config Change Templates.", ex);
            return new ErrorEntry(Resources.NCMWebContent.ConfigChangeTemplates_DeleteErrorTitle, ex);
        }
    }

    [WebMethod]
    public ErrorEntry CopySnippets(List<int> snippetIds)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.Snippets.CopySnippets(snippetIds.ToArray());
            }

            return null;
        }
        catch (Exception ex)
        {
            logger.Error("Error Copy Config Change Templates.", ex);
            return new ErrorEntry(Resources.NCMWebContent.ConfigChangeTemplates_CopyErrorTitle, ex);
        }
    }

    [WebMethod (EnableSession=true)]
    [ScriptMethod]
    public string[] GetTagsList(string prefixText, int count)
    {
        var dtTags = ExecuteSWQL(
            $@"SELECT DISTINCT TOP {count} T.TagName FROM Cirrus.Tags AS T WHERE T.TagName LIKE '{SearchHelper.EncodeToSafeSqlType(prefixText)}%'");

        var tags = new List<string>();
        foreach (DataRow row in dtTags.Rows)
        {
            tags.Add(row["TagName"].ToString());
        }

        return tags.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public string ValidateConfigSnippetScript(int snippetId)
    {
        try
        {
            var dtAdvScript = ExecuteSWQL(
                @"SELECT C.AdvancedScript FROM Cirrus.ConfigSnippets AS C WHERE C.ID=@snippetId",
                new PropertyBag() {{@"snippetId", snippetId}});

            if (dtAdvScript.Rows.Count > 0)
            {
                var script = dtAdvScript.Rows[0]["AdvancedScript"].ToString();

                if (string.IsNullOrEmpty(script))
                {
                    return SendResponce(false, Resources.NCMWebContent.ConfigChangeTemplates_ScriptIsEmptyError);
                }

                var exHelper = ServiceLocator.Container.Resolve<IExHelper>();
                var res = exHelper.ValidateConfigSnippetScript(script);
                if (!string.IsNullOrEmpty(res))
                {
                    return SendResponce(false, res);
                }

            }
            else
            {
                return SendResponce(false, Resources.NCMWebContent.ConfigChangeTemplates_ScriptIsNotLongerExistsError);
            }

            return SendResponce(true,string.Empty);
        }
        catch (Exception ex)
        {
            logger.Error("Error validating config snippet script.", ex);
            return SendResponce(false, ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetSnippetDescription(int snippetId)
    {
        var result = new Dictionary<string, object>();

        var dtDesc = ExecuteSWQL(@"SELECT C.Description FROM Cirrus.ConfigSnippets AS C WHERE C.ID=@snippetId",
            new PropertyBag() {{@"snippetId", snippetId}});

        if (dtDesc.Rows.Count > 0)
        {
            result[@"description"] = dtDesc.Rows[0]["Description"].ToString();
        }

        return result;
    }
    #endregion

    #region private methods

    private  string SendResponce(bool success, string msg)
    {
        var sb = new StringBuilder();
        sb.AppendFormat(@"{{success:{0},message:", success.ToString().ToLower());
        JsonHelper.Encode(sb, msg);
        sb.Append(@"}");
        return sb.ToString();
    }

    private DataTable ExecuteSWQL(string swql, PropertyBag propertyBag = null)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swql, propertyBag);
            return dt;
        }
    }

    private string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderbyDirection, string property, string value, string search)
    {
        pageSize = Math.Max(pageSize, 25);
        var pageNumber = (startRowNumber / pageSize);

        var orderByColumnName = @"Name";

        const string swqlFormat = @"
        SELECT TOP {0} T1.ID as SnippetID, 
			          T1.Name, 
			          T1.Description,
                      T1.Created,
                      T1.LastModified                      		           
        FROM Cirrus.ConfigSnippets T1
        WHERE T1.ID NOT IN(
	        SELECT TOP {1} T.ID
	        FROM Cirrus.ConfigSnippets T
	        WHERE {4}
	        ORDER BY T.{2} {3}
	        )
        AND {5}
        AND {6}	        
        ORDER BY T1.{2} {3}";

        var swql = String.Format(swqlFormat,
                                    pageSize,
                                    pageNumber * pageSize,
                                    orderByColumnName,
                                    orderbyDirection,
                                    GetTagWhereClause(@"T", value),
                                    GetTagWhereClause(@"T1", value),
                                    GetSearchClause(@"T1", search)
                                    );

        return swql;
    }

    private  string GetSearchClause(string tableAlias, string search)
    {
        if (String.IsNullOrEmpty(search))
            return @"(1=1)";

        var searchValue = SearchHelper.WildcardToLike(search);

        var swqlFormat = @"
            ({0}.Name LIKE '%{1}%' 
             OR {0}.ID IN (
                    SELECT T.SnippetID
                    FROM Cirrus.Tags T
                    WHERE T.TagName LIKE '%{1}%'
                )             
            )";

        var clause = String.Format(swqlFormat, tableAlias, searchValue);
        return clause;
    }

    private long GetRowCount(string property, string value, string search)
    {
        const string swqlFormat = @"
            SELECT Count(A.ID) as theCount
            FROM Cirrus.ConfigSnippets A
            WHERE {0}
            AND {1}";

        var swql = String.Format(swqlFormat, GetTagWhereClause(@"A", value), GetSearchClause(@"A", search));

        var rowCountTable = ExecuteSWQL(swql);

        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private string GetTagWhereClause(string tableAlias, string value)
    {
        var whereClause = @"1=1";

        if (String.IsNullOrEmpty(value) || value.Equals(@"null", StringComparison.OrdinalIgnoreCase))
            return whereClause;

        whereClause = $@"{tableAlias}.ID IN (
	                                SELECT T.SnippetID
	                                FROM Cirrus.Tags T
	                                WHERE T.TagName = '{value}')";
        return whereClause;

    }

    private void AddTagDataToTemplateTable(DataTable table)
    {
        var tagsColumn = table.Columns.Add("Tags", typeof(string));

        if (table.Rows.Count == 0)
            return;

        //
        // Load the tags
        //
        var ids = new List<string>(table.Rows.Count);
        var idColumn = table.Columns["SnippetID"].Ordinal;

        foreach (DataRow row in table.Rows)
        {
            ids.Add(row[idColumn].ToString());
        }

        var swql = $@"SELECT T.SnippetId, T.TagName
                                  FROM Cirrus.Tags T
                                  WHERE T.SnippetID in ({String.Join(@",", ids.ToArray())})";

        var tags = ExecuteSWQL(swql);

        //
        // Parse the tags into a lookup by id
        //
        var tagLookup = new Dictionary<int, List<string>>(table.Rows.Count);

        foreach (DataRow row in tags.Rows)
        {
            var id = Convert.ToInt32(row[0]);
            var tag = row[1].ToString();

            if (!tagLookup.ContainsKey(id))
                tagLookup[id] = new List<string>();

            tagLookup[id].Add(tag);
        }

        //
        // Add the tags as JSON to the Tags column
        //
        var serializer = new JavaScriptSerializer();

        foreach (DataRow row in table.Rows)
        {
            var id = Convert.ToInt32(row[idColumn]);

            if (tagLookup.ContainsKey(id))
            {
                row[tagsColumn] = serializer.Serialize(tagLookup[id]);
            }
        }
    }

    #endregion
}

