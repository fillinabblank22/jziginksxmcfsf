<%@ WebService Language="C#" Class="Settings" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using System.Text;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Settings  : WebService {

    [Localizable(false)] private static readonly string[] globalSettings = {
        "GlobalConfigRequestProtocol",
        "GlobalConfigTransferProtocol",
        "GlobalEnableLevel",
        "GlobalEnablePassword",
        "GlobalExecProtocol",
        "GlobalPassword",
        "GlobalSSHPort",
        "GlobalTelnetPort",
        "GlobalUsername",
        "GlobalSNMPPort"
    };

    private static readonly List<string> globalList = new List<string>(globalSettings);

    private readonly ILogger logger;
    private readonly IIsWrapper isLayer;

    public Settings()
    {
        logger = ServiceLocator.Container.Resolve<ILoggerFactory>().Create(GetType());
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetCustomMacrosPaged(string search)
    {
        var pageSize = 0;
        var startRowNumber = 0;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        search = search.Replace(@"'", @"''");

        var swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, search);

        var data = ExecuteSWQL(swql);
        return new PageableDataTable(data, GetRowCount(search));
    }

    [WebMethod(EnableSession = true)]
    public bool ValidateCustomMacroName( string name)
    {
        var swql = @"select C.SettingName from Cirrus.GlobalSettings C where SettingName = @settingName";
        var searchTerm = name.Replace(@"'",@"''");

        var dt = ExecuteSWQL(swql, new PropertyBag() {{@"settingName", searchTerm}});
        if (dt != null && dt.Rows.Count == 0)
            return true;
        return false;
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry CreateEditCustomMacros(string name, string value)
    {
        if (string.IsNullOrEmpty(name))
            return new ErrorEntry(Resources.NCMWebContent.CustomMacros_ErrorDialogTitle, Resources.NCMWebContent.CustomMacros_CreateEditMacroDialogMessage, true);

        try
        {
            var correctedName = name.Trim();

            if (correctedName.StartsWith(@"$"))
                correctedName = correctedName.Remove(0, 1);

            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.Settings.SetCustomMacros(correctedName, value);
            }

            return new ErrorEntry(@"CreateEditCustomMacros", @"success", false);
        }
        catch (Exception ex)
        {
            logger.Error("Error editing macros.", ex);
            return new ErrorEntry(Resources.NCMWebContent.CustomMacros_ErrorDialogTitle, ex);
        }
    }

    [WebMethod (EnableSession = true)]
    public ErrorEntry DeleteCustomMacros(string[] names)
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                foreach (var name in names)
                {
                    if (!globalList.Contains(name))
                        proxy.Cirrus.Settings.DeleteCustomMacros(name);
                }
            }
            return new ErrorEntry(Resources.NCMWebContent.CustomMacros_RemoveMacrosDialogTitle, Resources.NCMWebContent.CustomMacros_RemoveMacrosDialogMessage, false);
        }
        catch (Exception ex)
        {
            logger.Error("Error in deleting custom macros.", ex);
            return new ErrorEntry(Resources.NCMWebContent.CustomMacros_ErrorDialogTitle, ex);
        }
    }

    [WebMethod]
    public string GetWebSetting(string settingName, string defaultValue)
    {
        try
        {
            return SolarWinds.Orion.Core.Common.WebSettingsDAL.Get(settingName);
        }
        catch
        {
            return defaultValue;
        }
    }

    #region private

    private long GetRowCount(string search)
    {
        const string swqlFormat = @"
            SELECT Count(A.SettingName) as theCount
            FROM Cirrus.GlobalSettings A
            WHERE ({0}) AND ({1})";

        var swql = String.Format(swqlFormat, GetSearchClause(@"A", search), ExcludeGlobalMacros(@"A"));

        var rowCountTable = ExecuteSWQL(swql);

        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private string PageableQuery(int pageSize, int startRowNumber, string sortColumn, string sortDirection, string search)
    {
        pageSize = Math.Max(pageSize, 25);
        var pageNumber = (startRowNumber / pageSize);

        const string swqlFormat = @"
        SELECT TOP {0} T1.SettingName, 
			          T1.SettingValue
        FROM Cirrus.GlobalSettings T1
        WHERE T1.SettingName NOT IN(
	        SELECT TOP {1} T.SettingName
	        FROM Cirrus.GlobalSettings T
            WHERE ({6})
	        ORDER BY T.{2} {3}
	        )
        AND {4}
        AND ({5})
        ORDER BY T1.{2} {3}";

        var swql = String.Format(swqlFormat,
                                    pageSize,//0
                                    pageNumber * pageSize,//1
                                    sortColumn,//2
                                    sortDirection,//3
                                    GetSearchClause(@"T1", search),//4
                                    ExcludeGlobalMacros(@"T1"),
                                    ExcludeGlobalMacros(@"T")
                                    );

        return swql;
    }

    private string GetSearchClause(string tableAlias, string search)
    {
        if (String.IsNullOrEmpty(search))
            return @"(1=1)";

        var searchValue = SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers.SearchHelper.WildcardToLike(search);

        var swqlFormat = @"
            ({0}.SettingName LIKE '%{1}%'
             OR {0}.SettingValue LIKE '%{1}%')";

        var clause = String.Format(swqlFormat, tableAlias, searchValue);
        return clause;
    }

    private string ExcludeGlobalMacros(string tableAlias)
    {
        var sb = new StringBuilder();
        var first = true;

        foreach (var item in globalSettings)
        {
            if (first)
                first = false;
            else
                sb.Append(@" AND ");

            sb.AppendFormat(@"{0}.SettingName <> '{1}'", tableAlias, item);
        }

        return sb.ToString();
    }

    private DataTable ExecuteSWQL(string swql, PropertyBag propertyBag = null)
    {
        using (var proxy = isLayer.GetProxy())
        {
            var dt = proxy.Query(swql, propertyBag);
            return dt;
        }
    }

    #endregion
}
