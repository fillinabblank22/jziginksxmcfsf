<%@ WebService Language="C#" Class="ApprovalRequests" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using SolarWinds.Orion.Web;
using CirrusEntities = SolarWinds.InformationService.Linq.Plugins.NCM.Cirrus;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ApprovalRequests  : WebService
{
    private readonly ILogger log;
    private readonly ISwisRepositoryFactory swisRepositoryFactory;

    public ApprovalRequests()
    {
        swisRepositoryFactory = ServiceLocator.Container.Resolve<ISwisRepositoryFactory>();
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetApprovalRequestsPaged(string search, int rqsStatus)
    {
        try
        {
            int pageSize;
            int startRowNumber;

            var sortColumn = LookUpColumnName(Context.Request.QueryString[@"sort"]);
            var sortDirection = Context.Request.QueryString[@"dir"];

            int.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
            int.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

            search = search.Replace(@"'", @"''");

            var swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, search, rqsStatus);

            using (var repo = swisRepositoryFactory.CreateHttpContextRepository())
            {
                var pageData = repo.Query(swql);
                PopulateTargetNodesCount(pageData, repo);
                LookupRequestType(pageData);
                return new PageableDataTable(pageData, GetRowCount(search,rqsStatus));
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting approval requests paged", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry ApproveRequest(string requestId)
    {
        try
        {
            ValidateRequest(requestId);

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var ticket = proxy.Cirrus.ConfigChangeApproval.GetRequest(new Guid(requestId));
                if (ticket.NetworkNodes.Count == 0) throw new Exception(Resources.NCMWebContent.WEBDATA_VK_848);
                if (ticket.ExecutionType == SolarWinds.NCM.Contracts.InformationService.eExecutionType.SpecifiedTime)
                {
                    if (ticket.RunAt < DateTime.Now) throw new Exception(Resources.NCMWebContent.WEBDATA_VK_853);
                }
                proxy.Cirrus.ConfigChangeApproval.ApproveRequest(ticket, HttpContext.Current.User.Identity.Name);
            }

            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in approve request function.", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_30, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeleteRequests(List<string> requestIds)
    {
        try
        {
            ValidateRequests(requestIds);

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                foreach (var requestID in requestIds)
                {
                    proxy.Cirrus.ConfigChangeApproval.DeleteRequest (new Guid (requestID));
                }
            }

            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in delete requests function", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_32, ex);
        }
    }

    [WebMethod]
    public bool SetEnableApprovalSystem(bool enableApprovalSystem)
    {
        try
        {
            SolarWinds.Orion.Core.Common.WebSettingsDAL.Set(ConfigChangeApprovalHelper.NCM_ENABLE_APPROVAL_SYSTEM,
                enableApprovalSystem.ToString().ToLowerInvariant());
            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error enabling the approval system.", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public void ValidateRequest(string requestId)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(@"SELECT A.ID FROM Cirrus.ApproveQueue AS A WHERE A.ID=@requestId",
                    new PropertyBag() {{@"requestId", requestId}});
                if (dt.Rows.Count == 0) throw new Exception(Resources.NCMWebContent.WEBDATA_VK_33);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error validating request.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void ValidateRequests(List<string> requestIds)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(
                    $@"SELECT A.ID FROM Cirrus.ApproveQueue AS A WHERE A.ID IN('{string.Join(@"','", requestIds.ToArray())}')");
                if (dt.Rows.Count == 0) throw new Exception(Resources.NCMWebContent.WEBDATA_VK_34);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error validating request.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetComments(string requestId)
    {
        try
        {
            var result = new Dictionary<string, object>();

            var dt = ExecuteSWQL(@"SELECT A.RequestType, A.Comments FROM Cirrus.ApproveQueue AS A WHERE A.ID=@requestId",
                new SwqlParameter("requestId", requestId));

            if (dt.Rows.Count > 0)
            {
                var requestType = dt.Rows[0]["RequestType"].ToString();
                result[@"title"] = ConfigChangeApprovalHelper.LookupLocalizedRequestType(requestType);
                result[@"comments"] = dt.Rows[0]["Comments"].ToString();
            }

            return result;
        }
        catch (Exception ex)
        {
            log.Error("Error getting comments.", ex);
            throw;
        }
    }

    private DataTable ExecuteSWQL(string swql, params SwqlParameter[] parameters)
    {
        using (var repo = swisRepositoryFactory.CreateHttpContextRepository())
        {
            var dt = repo.Query(swql, parameters);

            return dt;
        }
    }

    private string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderbyDirection, string search, int requestStatus)
    {
        var swql2 = $@"SELECT  
                            A1.ID,
                            A1.RequestStatus,
                            A1.RequestType,
                            0 AS TargetNodes,
                            A1.UserName,
                            A1.StatusChangeTime,
                            A1.DateTime,
                            A1.ApprovedBy,
                            A1.Comments,
                            A1.NCMJobID
        
                        FROM Cirrus.NCM_ApproveQueueView AS A1
                        WHERE {GetWhereClause(@"A1", requestStatus)} AND {GetSearchClause(@"A1", search)}
                        ORDER BY A1.{orderByColumn} {orderbyDirection}
                        WITH ROWS {startRowNumber + 1} TO {startRowNumber + pageSize}";

        return swql2;
    }

    private string GetWhereClause(string tableAlias, int requestStatus)
    {
        var statusWhereClause = requestStatus < 0 ? @" (1=1) " : $@" {tableAlias}.RequestStatus={requestStatus} ";
        if (ConfigChangeApprovalHelper.IsApproverUser || CommonHelper.IsDemoMode())
        {
            return statusWhereClause;
        }

        return $@" {tableAlias}.UserName='{HttpContext.Current.Profile.UserName}' AND {statusWhereClause} ";
    }

    private string GetSearchClause(string tableAlias, string search)
    {
        if (string.IsNullOrEmpty(search))
        {
            return @"(1=1)";
        }

        var searchValue = SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers.SearchHelper.WildcardToLike(search);

        var swqlFormat = @"
            ({0}.UserName LIKE '%{1}%' 
            OR {0}.RequestType LIKE '%{1}%'
            OR {0}.Comments LIKE '%{1}%'
            OR {0}.ApprovedBy LIKE '%{1}%')";

        var clause = string.Format(swqlFormat, tableAlias, searchValue);
        return clause;
    }

    private long GetRowCount(string search,int requestType)
    {
        const string swqlFormat = @"
            SELECT Count(A.ID) as theCount
            FROM Cirrus.NCM_ApproveQueueView A
            WHERE {0} AND {1}";

        var swql = string.Format(swqlFormat,
            GetWhereClause(@"A",requestType),
            GetSearchClause(@"A", search));

        var rowCountTable = ExecuteSWQL(swql);

        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
        {
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        }

        return 0;
    }

    private string LookUpColumnName(string value)
    {
        switch (value)
        {
            case "a": return @"ID";
            case "b": return @"RequestStatus";
            case "c": return @"RequestType";
            case "d": return @"TargetNodes";
            case "e": return @"UserName";
            case "f": return @"StatusChangeTime";
            case "g": return @"DateTime";
            case "h": return @"ApprovedBy";
            case "i": return @"Comments";
            case "j": return @"NCMJobID";
            default: throw new ArgumentOutOfRangeException($"{value} is unsuported");
        }
    }

    private int GetTargetNodesCount(Guid requestId, ISwisRepository repo)
    {
        return repo.Get<CirrusEntities.ApproveQueueNodes>()
            .Where(a => a.ApproveQueueID == requestId)
            .Select(a => a.NodeID)
            .Count();
    }

    private void PopulateTargetNodesCount(DataTable dt, ISwisRepository repo)
    {
        foreach (DataRow row in dt.Rows)
        {
            var id = new Guid(row["ID"].ToString());
            row["TargetNodes"] = GetTargetNodesCount(id, repo);
        }
    }

    private void LookupRequestType(DataTable dt)
    {
        foreach (DataRow row in dt.Rows)
        {
            var requestType = row["RequestType"].ToString();
            row["RequestType"] = ConfigChangeApprovalHelper.LookupLocalizedRequestType(requestType);
        }
    }
}
