<%@ WebService Language="C#" Class="PolicyRuleManagement" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using Resources;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PolicyRuleManagement : WebService
{
    private readonly Log log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetPolicyRulesPaged(string property, string value, string search)
    {
        var pageSize = 0;
        var startRowNumber = 0;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        value = value.Replace(@"'", @"''");
        search = search.Replace(@"'", @"''");

        var pageData = GetPagablePolicyRulesTable(pageSize, startRowNumber, sortColumn, sortDirection, property, value, search);

        return new PageableDataTable(pageData, GetRowCount(GetGroupByColumn(property), GetGroupByValue(value), search));
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeletePolicyRules(Guid[] policyRuleIds)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.PolicyReports.DeletePolicyRules(policyRuleIds);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in DeletePolicyRules", ex);
            return new ErrorEntry(Resources.NCMWebContent.PolicyRuleManagement_DeleteErrorTitle, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public Guid CopyPolicyRule(Guid policyRuleId)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                PolicyRule policyRule;
                policyRule = proxy.Cirrus.PolicyReports.GetPolicyRule(policyRuleId);
                policyRule.RuleId = Guid.NewGuid();
                policyRule.RuleName = $@"{policyRule.RuleName}_{NCMWebContent.WEBDATA_IC_202}";

                // Prepare multiLineRulePattern
                foreach (var multiLineRulePattern in policyRule.MultiLineRulePatterns)
                {
                    multiLineRulePattern.PatternId = Guid.NewGuid();
                    multiLineRulePattern.RuleId = policyRule.RuleId;
                }
                return proxy.Cirrus.PolicyReports.AddPolicyRule(policyRule);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in CopyPolicyRule", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        if (string.IsNullOrEmpty(property))
            return new DataTable();

        var swql = GetGroupBySWQL(property);

        var dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
        {
            var count = dt.Rows.Count;
            var lastRow = dt.Rows[count - 1];
            var value = int.Parse(lastRow["theCount"].ToString());
            if (value == 0)
            {
                dt.Rows.Remove(lastRow);
            }
        }

        return dt;
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetPolicyRuleDescription(string policyRuleId)
    {
        var result = new Dictionary<string, object>();

        var dtDesc =
            ExecuteSWQL(@"SELECT P.Comment FROM Cirrus.PolicyRules AS P WHERE P.PolicyRuleID=@policyRuleId",
                new PropertyBag() {{@"policyRuleId", policyRuleId}});

        if (dtDesc.Rows.Count > 0)
        {
            result[@"description"] = dtDesc.Rows[0]["Comment"].ToString();
        }

        return result;
    }

    [WebMethod(EnableSession = true)]
    public string FindPolicyRuleInGrid(string sort, string dir, string policyId, int limit)
    {
        var pageSize = 0;
        var start = 0;
        var success = false;

        var sortColumn = string.IsNullOrEmpty(sort) ? @"Name" : sort;
        var sortDirection = string.IsNullOrEmpty(dir) ? @"ASC" : dir;
        pageSize = limit;

        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {

            var rowNumber = proxy.Cirrus.PolicyReports.GetRowNumber(@"NCM_PolicyRules", sortColumn, sortDirection,
                $@"WHERE PolicyRuleID='{policyId}'");

            success = true;
            var pos = rowNumber - 1;
            start = ((int)pos / pageSize) * pageSize;

            var jsonRes = $@"{{success:{success.ToString().ToLowerInvariant()},data:{{start:{start}}}}}";

            return jsonRes;
        }
    }

    #region Private Methods

    private DataTable GetPagablePolicyRulesTable(int pageSize, int startRowNumber, string sortColumn, string sortDirection, string groupByColumn, string groupByValue, string searchValue)
    {
        try
        {
            pageSize = Math.Max(pageSize, 25);

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                return proxy.Cirrus.PolicyReports.GetPagablePolicyRulesList(pageSize, startRowNumber, GetSortColumn(sortColumn), sortDirection, GetGroupByColumn(groupByColumn), GetGroupByValue(groupByValue), searchValue);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in GetPagablePolicyRulesTable", ex);
            return null;
        }
    }

    private string GetSortColumn(string sortColumn)
    {
        switch (sortColumn.ToUpperInvariant())
        {
            case "REMEDIATION":
                return $@"RemediationView.{sortColumn}";
            default:
                return $@"NCM_PolicyRules.{sortColumn}";
        }
    }
    
    private string GetGroupByColumn(string groupByColumn)
    {
        switch (groupByColumn.ToUpperInvariant())
        {
            case "FOLDER":
                return @"NCM_PolicyRules.Grouping";
            case "ERRORLEVEL":
                return @"NCM_PolicyRules.ErrorLevel";
            case "APPEARSINREPORT":
                return @"NCM_PolicyReports.PolicyReportID";
            case "APPEARSINPOLICY":
                return @"NCM_Policies.PolicyID";
        }

        return groupByColumn;
    }

    private string GetGroupByValue(string groupByValue)
    {
        switch (groupByValue.ToUpperInvariant())
        {
            case "NO FOLDER":
                return @"${IS_EMPTY}";
            case "NULL":
            case "NO REPORT":
            case "NO POLICY":
                return @"${IS_NULL}";
            case "INFORMATIONAL":
                return @"0";
            case "WARNING":
                return @"1";
            case "CRITICAL":
                return @"2";
        }

        return groupByValue;
    }

    private string GetGroupBySWQL(string groupBy)
    {
        switch (groupBy.ToUpperInvariant())
        {
            case "FOLDER":
                return @"
                SELECT DISTINCT PolicyRules.Grouping AS theValue, COUNT(PolicyRules.Grouping) AS theCount, PolicyRules.Grouping AS theID, 0 AS theOrdinalNumber
                FROM Cirrus.PolicyRules
                WHERE PolicyRules.Grouping<>''
                GROUP BY PolicyRules.Grouping
                UNION ALL 
                (
                SELECT 'No Folder' AS theValue, COUNT(PolicyRules.Grouping) AS theCount, PolicyRules.Grouping AS theID, 1 AS theOrdinalNumber
                FROM Cirrus.PolicyRules
                WHERE PolicyRules.Grouping=''
                GROUP BY PolicyRules.Grouping 
                ) ORDER BY theOrdinalNumber, theValue";
            case "ERRORLEVEL":
                return @"
                SELECT 'Informational' AS theValue, COUNT(PolicyRules.ErrorLevel) AS theCount, 'Informational' AS theID
                FROM Cirrus.PolicyRules
                WHERE PolicyRules.ErrorLevel=0
                GROUP BY PolicyRules.ErrorLevel
                UNION ALL
                (
                SELECT 'Warning' AS theValue, COUNT(PolicyRules.ErrorLevel) AS theCount, 'Warning' AS theID
                FROM Cirrus.PolicyRules
                WHERE PolicyRules.ErrorLevel=1
                GROUP BY PolicyRules.ErrorLevel
                )
                UNION ALL
                (
                SELECT 'Critical' AS theValue, COUNT(PolicyRules.ErrorLevel) AS theCount, 'Critical' AS theID
                FROM Cirrus.PolicyRules
                WHERE PolicyRules.ErrorLevel=2
                GROUP BY PolicyRules.ErrorLevel
                )";
            case "APPEARSINREPORT":
                return @"
                SELECT PolicyReports.Name AS theValue, COUNT(DISTINCT PolicyRules.PolicyRuleID) AS theCount, PolicyReports.PolicyReportID AS theID, 0 AS theOrdinalNumber
                FROM Cirrus.PolicyReports 
                JOIN Cirrus.PolicyAssignment ON PolicyAssignment.PolicyReportID=PolicyReports.PolicyReportID
                JOIN Cirrus.Policies ON PolicyAssignment.PolicyID=Policies.PolicyID
                JOIN Cirrus.PolicyRuleAssignment ON PolicyRuleAssignment.PolicyID=Policies.PolicyID
                RIGHT JOIN Cirrus.PolicyRules ON PolicyRules.PolicyRuleID=PolicyRuleAssignment.PolicyRuleID
                WHERE PolicyReports.PolicyReportID IS NOT NULL
                GROUP BY PolicyReports.PolicyReportID,PolicyReports.Name
                UNION ALL
                (
                SELECT 'No Report' AS theValue, COUNT(DISTINCT PolicyRules.PolicyRuleID) AS theCount, PolicyReports.PolicyReportID AS theID, 1 AS theOrdinalNumber
                FROM Cirrus.PolicyReports 
                JOIN Cirrus.PolicyAssignment ON PolicyAssignment.PolicyReportID=PolicyReports.PolicyReportID
                JOIN Cirrus.Policies ON PolicyAssignment.PolicyID=Policies.PolicyID
                JOIN Cirrus.PolicyRuleAssignment ON PolicyRuleAssignment.PolicyID=Policies.PolicyID
                RIGHT JOIN Cirrus.PolicyRules ON PolicyRules.PolicyRuleID=PolicyRuleAssignment.PolicyRuleID
                WHERE PolicyReports.PolicyReportID IS NULL
                GROUP BY PolicyReports.PolicyReportID
                ) ORDER BY theOrdinalNumber, theValue";
            case "APPEARSINPOLICY":
                return @"                
                SELECT Policies.Name AS theValue, COUNT(PolicyRules.PolicyRuleID) AS theCount, Policies.PolicyID AS theID, 0 AS theOrdinalNumber
                FROM Cirrus.Policies 
                JOIN Cirrus.PolicyRuleAssignment ON PolicyRuleAssignment.PolicyID= Policies.PolicyID
                RIGHT JOIN Cirrus.PolicyRules ON PolicyRules.PolicyRuleID= PolicyRuleAssignment.PolicyRuleID
                WHERE Policies.PolicyID IS NOT NULL
                GROUP BY Policies.PolicyID,Policies.Name
                UNION ALL
                (
                SELECT 'No Policy' AS theValue, COUNT(PolicyRules.PolicyRuleID) AS theCount, Policies.PolicyID AS theID, 1 AS theOrdinalNumber
                FROM Cirrus.Policies 
                JOIN Cirrus.PolicyRuleAssignment ON PolicyRuleAssignment.PolicyID=Policies.PolicyID
                RIGHT JOIN Cirrus.PolicyRules ON PolicyRules.PolicyRuleID=PolicyRuleAssignment.PolicyRuleID
                WHERE Policies.PolicyID IS NULL
                GROUP BY Policies.PolicyID,Policies.Name
                ) ORDER BY theOrdinalNumber, theValue";
        }

        return string.Empty;
    }

    private int GetRowCount(string groupByColumn, string groupByValue, string searchValue)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.PolicyReports.GetPolicyRulesRowCount(groupByColumn, groupByValue, searchValue);
        }
    }

    private DataTable ExecuteSWQL(string swql, PropertyBag propertyBag = null)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swql, propertyBag);
            return dt;
        }
    }

    #endregion
}
