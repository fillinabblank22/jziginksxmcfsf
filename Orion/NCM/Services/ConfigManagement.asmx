<%@ WebService Language="C#" Class="ConfigManagement" %>

using System;
using System.Globalization;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Data;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Contracts.Baseline;
using SolarWinds.NCM.Contracts.ConfigTypes;
using SolarWinds.NCM.Web.Contracts;
using SolarWinds.NCMModule.Web.Resources.NodeManagement;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ConfigManagement : WebService
{
    private static readonly string gridSelectionStateId = @"85D46102-3FF7-44B6-BDA0-6953C41B881B";

    private static readonly string[] CommonPropsToEncode =
    {
        @"NodeGroup",
        @"Caption",
        @"IPAddress",
        @"MachineType",
        @"Vendor",
        @"IOSVersion",
        @"ConfigTitle",
        @"OriginConfigType",
        @"ParentConfigType",
        @"LastTransferMessage"
    };

    private readonly IConfigTypesProvider configTypesProvider;
    private readonly ILogger log;
    private readonly IIsWrapper isWrapper;
    private readonly IDataDecryptor decryptor;
    private readonly IMacroParser macroParser;
    private readonly IConfigManagementQueryHelper configManagementQueryHelper;

    public ConfigManagement()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
        isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        decryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
        configManagementQueryHelper = ServiceLocator.Container.Resolve<IConfigManagementQueryHelper>();
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }


    [WebMethod(EnableSession = true)]
    public PageableDataTable GetNodesPaged(string groupingQueryString, bool showSelectedOnly, string colToSearch,
        string searchTerm, double clientOffset)
    {
        try
        {
            var pageSize = 0;
            var startRowNumber = 0;

            var sortColumn = Context.Request.QueryString[@"sort"];
            var sortDirection = Context.Request.QueryString[@"dir"];

            Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
            Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

            searchTerm = searchTerm.Replace(@"'", @"''");

            pageSize = Math.Max(pageSize, 1);

            using (var proxy = isWrapper.GetProxy())
            {
                var customProperties = CustomPropertyHelper.GetCustomProperties();

                var swql = $@"
                SELECT {configManagementQueryHelper.BuildGetNodesSelect(customProperties)}
                FROM Cirrus.NodeProperties AS NCMNodeProperties                
		        INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON OrionCustomProperties.NodeID=Nodes.NodeID
LEFT JOIN
(
select BaselineNodeMap.NodeId,
MAX (case when BaselineNodeMap.CacheState={(int)BaselineCacheState.NeedRecalculation} OR BaselineNodeMap.CacheState={(int)BaselineCacheState.Calculating} Then {(int)ViolationState.Updating}
          when BaselineNodeMap.CacheState={(int)BaselineCacheState.Error} Then {(int)ViolationState.Error}
          when BaselineNodeMap.CacheState={(int)BaselineCacheState.Cached} AND V.IsViolation=1 Then {(int)ViolationState.Mismatches}
          when BaselineNodeMap.CacheState={(int)BaselineCacheState.Cached} AND V.IsViolation=0 Then {(int)ViolationState.NoIssues}
          else {(int)ViolationState.Unknown} end) as MismatchStatus
from NCM.BaselineNodeMap as BaselineNodeMap
join NCM.Baselines as Baselines on BaselineNodeMap.BaselineId = Baselines.Id
left join NCM.BaselineViolations as V on BaselineNodeMap.NodeId=V.NodeId and BaselineNodeMap.BaselineId=V.BaselineId and BaselineNodeMap.ConfigType=V.ConfigType
where Baselines.Disabled = 0
group by BaselineNodeMap.NodeId
) as BaselineStatus on NCMNodeProperties.NodeID=BaselineStatus.NodeId
                WHERE {GetGroupingClause(groupingQueryString)} AND {GetSelectedOnlyClause(showSelectedOnly, @"Nodes", @"NodeID")} AND {GetSearchClause(colToSearch, searchTerm)}
                ORDER BY {sortColumn} {sortDirection} 
                WITH ROWS {startRowNumber + 1} TO {startRowNumber + pageSize}";

                var dt = proxy.Query(swql);

                var localTime = DateTimeOffset.Now;
                var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;

                foreach (DataRow row in dt.Rows)
                {
                    EncodeHtmlCommonProperties(row);
                    EncodeHtmlCustomProperites(customProperties, row);

                    if (!row.IsNull("LastTransferDate"))
                    {
                        row["LastTransferDate"] = Convert.ToDateTime(row["LastTransferDate"]).AddMinutes(offsetMinutes);
                    }

                    var id = Convert.ToInt32(row["OrionNodeID"]);
                    if (IsSelected(id))
                    {
                        row["Checked"] = 1;
                    }
                }

                var totalRows = GetTotalRows(groupingQueryString, showSelectedOnly, colToSearch, searchTerm);
                return new PageableDataTable(dt, totalRows);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting nodes paged", ex);
            throw;
        }
    }

    private void EncodeHtmlColumn(DataRow row, string columnName)
    {
        var column = row.Table.Columns[columnName];
        if (column != null && column.DataType == typeof(string))
        {
            row[columnName] = HttpUtility.HtmlEncode(row[columnName]);
        }
    }

    private void EncodeHtmlCommonProperties(DataRow row)
    {
        foreach (var commonPropName in CommonPropsToEncode)
        {
            EncodeHtmlColumn(row, commonPropName);
        }
    }

    private void EncodeHtmlCustomProperites(IReadOnlyList<string> customProperties, DataRow row)
    {
        foreach (var customProperty in customProperties)
        {
            EncodeHtmlColumn(row, customProperty);
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetConfigsPaged(string nodeId, int start, bool showAllConfigs, double clientOffset)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var customProperties = CustomPropertyHelper.GetCustomProperties();

                var end = 3;
                if (showAllConfigs)
                {
                    end = GetConfigsTotalRows(nodeId);
                }

                var swql = $@"
                    SELECT {configManagementQueryHelper.BuildGetConfigsForBaseConfigSelect(customProperties)}
                    FROM Cirrus.ConfigArchive AS C
                    INNER JOIN Cirrus.ConfigArchive AS C1 ON C.BaseConfigID=C1.ConfigID 
                    WHERE C.NodeID='{nodeId}' AND C.BaseConfigID IS NOT NULL
                    UNION ALL
                    (
                        SELECT {configManagementQueryHelper.BuildGetConfigsSelect(customProperties)}
                        FROM Cirrus.ConfigArchive AS C
                        WHERE C.NodeID='{nodeId}' AND C.BaseConfigID IS NULL
                    )
                    ORDER BY Caption DESC
                    WITH ROWS {start} TO {end}";

                var dt = proxy.Query(swql);

                var localTime = DateTimeOffset.Now;
                var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;

                foreach (DataRow row in dt.Rows)
                {
                    EncodeHtmlCommonProperties(row);
                    EncodeHtmlCustomProperites(customProperties, row);

                    if (!row.IsNull("Caption"))
                    {
                        row["Caption"] = Convert.ToDateTime(row["Caption"]).AddMinutes(offsetMinutes);
                    }
                }

                return new PageableDataTable(dt, 0);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting config paged", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetTransferStatusPaged(List<int> orionIds, double clientOffset)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query($@"
                SELECT NCMNodeProperties.NodeID AS NCMNodeID,                    
                    NCMNodeProperties.LastTransferActionType,
                    NCMNodeProperties.LastTransferDate,
                    NCMNodeProperties.LastTransferMessage,
                    NCMNodeProperties.IsTransferError,
                    NCMNodeProperties.IsActiveTransfer
                FROM Cirrus.NodeProperties AS NCMNodeProperties                
                WHERE ({(orionIds.Count == 0 ? @"1=1" : SecurityHelper.RunLenghtEncode(orionIds.ToArray(), @"NCMNodeProperties", @"CoreNodeID"))})");

                var localTime = DateTimeOffset.Now;
                var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;

                foreach (DataRow row in dt.Rows)
                {
                    if (!row.IsNull("LastTransferDate"))
                    {
                        row["LastTransferDate"] = Convert.ToDateTime(row["LastTransferDate"]).AddMinutes(offsetMinutes);
                    }
                }

                return new PageableDataTable(dt, 0);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting transfer status paged", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetBaselineStatusPaged(List<int> orionNodeIds)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query($@"
                SELECT
                    Nodes.NodeID AS OrionNodeID,                   
                    ISNULL (BaselineStatus.MismatchStatus, -1) as BaselineStatus                    
                FROM Cirrus.NodeProperties AS NCMNodeProperties                
		        INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON OrionCustomProperties.NodeID=Nodes.NodeID
                LEFT JOIN
                (
                select BaselineNodeMap.NodeId,
                MAX (case when BaselineNodeMap.CacheState={(int)BaselineCacheState.NeedRecalculation} OR BaselineNodeMap.CacheState={(int)BaselineCacheState.Calculating} Then {(int)ViolationState.Updating}
                          when BaselineNodeMap.CacheState={(int)BaselineCacheState.Error} Then {(int)ViolationState.Error}
                          when BaselineNodeMap.CacheState={(int)BaselineCacheState.Cached} AND V.IsViolation=1 Then {(int)ViolationState.Mismatches}
                          when BaselineNodeMap.CacheState={(int)BaselineCacheState.Cached} AND V.IsViolation=0 Then {(int)ViolationState.NoIssues}
                          else {(int)ViolationState.Unknown} end) as MismatchStatus
                from NCM.BaselineNodeMap as BaselineNodeMap
                join NCM.Baselines as Baselines on BaselineNodeMap.BaselineId = Baselines.Id
                left join NCM.BaselineViolations as V on BaselineNodeMap.NodeId=V.NodeId and BaselineNodeMap.BaselineId=V.BaselineId and BaselineNodeMap.ConfigType=V.ConfigType
                where Baselines.Disabled = 0
                group by BaselineNodeMap.NodeId
                ) as BaselineStatus on NCMNodeProperties.NodeID=BaselineStatus.NodeId
                WHERE ({(orionNodeIds.Count == 0 ? @"1=1" : SecurityHelper.RunLenghtEncode(orionNodeIds.ToArray(), @"NCMNodeProperties", @"CoreNodeID"))})");

                return new PageableDataTable(dt, 0);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting baseline status paged", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int GetConfigsTotalRows(string nodeId)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query(@"
                    SELECT COUNT(ConfigArchive.ConfigID) AS totalRows
                    FROM Cirrus.ConfigArchive                
                    WHERE ConfigArchive.NodeID=@nodeId", new PropertyBag() { { @"nodeId", nodeId } });

                return Convert.ToInt32(dt.Rows[0]["totalRows"]);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting configs total rows.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeleteConfig(string configId)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var configIdGuid = Guid.Parse(configId);
                proxy.Cirrus.ConfigArchive.DeleteConfigs(new List<Guid> { configIdGuid }, HttpContext.Current.Profile.UserName);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error deleting config.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry SetRemoveFavorite(string configId)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var configIdGuid = Guid.Parse(configId);
                proxy.Cirrus.ConfigArchive.SetClearBaseline(new List<Guid> { configIdGuid });
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error removing a favorite config.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DownloadConfigs(List<string> nodeIds, string configType)
    {
        try
        {
            if (nodeIds.Count == 0)
                return null;

            using (var proxy = isWrapper.GetProxy())
            {
                var nodeIdGuids = nodeIds.Select(item => Guid.Parse(item)).ToArray();
                proxy.Cirrus.ConfigArchive.DownloadConfig(nodeIdGuids, configType);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error downloading configs.", ex);
            return new ErrorEntry(ex);
        }
    }

    private bool TryGetBinaryConfig(string configId, out string config)
    {
        if (string.IsNullOrEmpty(configId))
        {
            config = null;
            return false;
        }

        using (var proxy = isWrapper.GetProxy())
        {
            var dt = proxy.Query(@"SELECT Config, IsBinary FROM Cirrus.ConfigArchive WHERE ConfigID=@configId",
                new PropertyBag() { { @"configId", configId } });
            if (dt != null && dt.Rows.Count > 0)
            {
                config = Convert.ToString(dt.Rows[0]["Config"]);
                return Convert.ToBoolean(dt.Rows[0]["IsBinary"]);
            }

            config = null;
            return false;
        }
    }


    [WebMethod(EnableSession = true)]
    public ErrorEntry UploadConfig(List<string> nodeIds, string configType, string config, bool reboot, string configId)
    {
        try
        {
            if (nodeIds.Count == 0)
                return null;

            if (string.IsNullOrEmpty(config))
            {
                string confFileName;
                if (TryGetBinaryConfig(configId, out confFileName))
                    config = confFileName;
            }

            using (var proxy = isWrapper.GetProxy())
            {
                var nodeIdGuids = nodeIds.Select(item => Guid.Parse(item)).ToArray();
                proxy.Cirrus.ConfigArchive.UploadConfig(nodeIdGuids, configType, config, reboot);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error uploading config.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry ExecuteScript(List<string> nodeIds, string script)
    {
        try
        {
            if (nodeIds.Count == 0)
                return null;

            using (var proxy = isWrapper.GetProxy())
            {
                var nodeIdGuids = nodeIds.Select(item => Guid.Parse(item)).ToArray();
                proxy.Cirrus.ConfigArchive.ExecuteScript(nodeIdGuids, script);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error executing script.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> RunConfigChangeReport()
    {
        try
        {
            var resourceSettings = new ResourceSettings();
            resourceSettings.Prefix = "ViewConfChanges";

            var result = new Dictionary<string, object>();
            result[@"starttime"] = !string.IsNullOrEmpty(resourceSettings.GetCustomProperty("DateTimeFrom")) ? resourceSettings.GetCustomProperty("DateTimeFrom") : DateTime.Now.AddDays(-1).ToString(CultureInfo.InvariantCulture);
            result[@"endtime"] = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            result[@"viewchangeson"] = !string.IsNullOrEmpty(resourceSettings.GetCustomProperty("ViewChangesOn")) && Convert.ToBoolean(resourceSettings.GetCustomProperty("ViewChangesOn"));
            result[@"configtype"] = !string.IsNullOrEmpty(resourceSettings.GetCustomProperty("ConfigType")) ? resourceSettings.GetCustomProperty("ConfigType") : @"Running";
            result[@"onlyshowdevicesthathadchanges"] = !string.IsNullOrEmpty(resourceSettings.GetCustomProperty("OnlyShowDevicesThatHadChanges")) && Convert.ToBoolean(resourceSettings.GetCustomProperty("OnlyShowDevicesThatHadChanges"));
            result[@"prefix"] = @"ViewConfChanges";
            result[@"clientid"] = @"Config_Change_Report_Control";
            return result;
        }
        catch (Exception ex)
        {
            log.Error("Error running config change report.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry SendRequestForApproval(List<string> nodeIds, string configType, bool reboot, string requestType, string script, string guidId, string configId)
    {
        try
        {
            if (nodeIds.Count == 0)
                return null;

            if (!string.IsNullOrEmpty(configId))
            {
                string confFileName;
                if (TryGetBinaryConfig(configId, out confFileName))
                    script = confFileName;
            }

            var ticket = new NCMApprovalTicket();
            ticket.ConfigType = configType;
            ticket.Reboot = reboot;
            ticket.RequestType = requestType;
            ticket.Script = script;
            ticket.Comments = string.Empty;
            ticket.SubmittedBy = HttpContext.Current.User.Identity.Name;
            ticket.ExecutionType = eExecutionType.Immediate;
            ticket.RequestStatus = eApprovalStatus.PendingApproval;
            ticket.RequestTime = DateTime.Now;
            foreach (var nodeId in nodeIds)
            {
                ticket.AddNode(new Guid(nodeId));
            }
            HttpContext.Current.Session.Add(guidId, ticket);
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error sending approval request.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry UpdateInventory(List<string> nodeIds)
    {
        try
        {
            if (nodeIds.Count == 0)
                return null;

            using (var proxy = isWrapper.GetProxy())
            {
                var nodeIdGuids = nodeIds.Select(item => Guid.Parse(item)).ToArray();
                proxy.Cirrus.Inventory.StartInventory(nodeIdGuids);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error updating inventory.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetNCMNodeIdsForTransfer()
    {
        try
        {
            var result = new Dictionary<string, object>();

            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query($@"SELECT Nodes.Caption, NCMNodeProperties.NodeID
                FROM Cirrus.NodeProperties AS NCMNodeProperties
                INNER JOIN Orion.Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                WHERE NCMNodeProperties.IsActiveTransfer=1 AND ({SecurityHelper.RunLenghtEncode(GetSelectedOrionNodeIds(), @"Nodes", @"NodeID")})");

                if (dt != null && dt.Rows.Count > 0)
                {
                    var captionList = dt.AsEnumerable().Select(row => Convert.ToString(row["Caption"])).ToArray();
                    var ncmNodeIdsInActiveTransfer = dt.AsEnumerable().Select(row => Convert.ToString(row["NodeID"])).ToArray();

                    var captionListCount = captionList.Count();

                    var msg =
                        $@"{Resources.NCMWebContent.WEBDATA_IA_35} [{string.Join(@", ", captionList, 0, captionListCount > 10 ? 10 : captionListCount)}{(captionListCount > 10 ? @" ..." : string.Empty)}]";

                    result[@"ncmNodeIdsForTransfer"] = GetSelectedNCMNodeIds().Except(ncmNodeIdsInActiveTransfer);
                    result[@"error"] = new ErrorEntry(new Exception(msg));
                }
                else
                {
                    result[@"ncmNodeIdsForTransfer"] = GetSelectedNCMNodeIds();
                    result[@"error"] = null;
                }
            }
            return result;
        }
        catch (Exception ex)
        {
            log.Error("Error getting NCM node ID's for transfer.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetNCMNodeIdsForTransferFromList(List<string> ncmNodeIds)
    {
        try
        {
            var result = new Dictionary<string, object>();

            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query($@"SELECT Nodes.Caption, NCMNodeProperties.NodeID
                FROM Cirrus.NodeProperties AS NCMNodeProperties
                INNER JOIN Orion.Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                WHERE NCMNodeProperties.IsActiveTransfer=1 AND {GetWhereClause(ncmNodeIds, @"NCMNodeProperties")}");

                if (dt != null && dt.Rows.Count > 0)
                {
                    var captionList = dt.AsEnumerable().Select(row => Convert.ToString(row["Caption"])).ToArray();
                    var ncmNodeIdsInActiveTransfer = dt.AsEnumerable().Select(row => Convert.ToString(row["NodeID"])).ToArray();

                    var captionListCount = captionList.Count();

                    var msg =
                        $@"{Resources.NCMWebContent.WEBDATA_IA_35} [{string.Join(@", ", captionList, 0, captionListCount > 10 ? 10 : captionListCount)}{(captionListCount > 10 ? @" ..." : string.Empty)}]";

                    result[@"ncmNodeIdsForTransfer"] = ncmNodeIds.Except(ncmNodeIdsInActiveTransfer);
                    result[@"error"] = new ErrorEntry(new Exception(msg));
                }
                else
                {
                    result[@"ncmNodeIdsForTransfer"] = ncmNodeIds;
                    result[@"error"] = null;
                }
            }
            return result;
        }
        catch (Exception ex)
        {
            log.Error("Error getting NCM node ID's for transfer from list.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool IsDifferentVendors(string parentNodeId, List<string> ncmNodeIds)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                ncmNodeIds.Add(parentNodeId);

                var dt = proxy.Query($@"SELECT Nodes.Vendor FROM Cirrus.NodeProperties AS NodeProperties 
            INNER JOIN Orion.Nodes AS Nodes ON NodeProperties.CoreNodeID=Nodes.NodeID
            WHERE {GetWhereClause(ncmNodeIds, @"NodeProperties")}");

                return (dt.AsEnumerable().Select(row => row.Field<string>("Vendor")).Distinct(StringComparer.InvariantCultureIgnoreCase).Count() != 1);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error getting IsDifferentVendors.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool IsDifferentTransferTypes(List<string> ncmNodeIds)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query($@"SELECT DISTINCT NodeID, TransferProtocol, CommandProtocol 
            FROM Cirrus.NodeProperties AS NodeProperties WHERE {GetWhereClause(ncmNodeIds, @"NodeProperties")}");

                var joinedProtocols = string.Empty;
                foreach (DataRow row in dt.Rows)
                {
                    var transferProtocol = macroParser.ParseMacro(Guid.Parse(row["NodeID"].ToString()),
                        Convert.ToString(row["TransferProtocol"]));
                    var commandProtocol = macroParser.ParseMacro(Guid.Parse(row["NodeID"].ToString()),
                        Convert.ToString(row["CommandProtocol"]));
                    if (string.IsNullOrEmpty(joinedProtocols))
                        joinedProtocols = $@"{transferProtocol},{commandProtocol}";
                    if (!joinedProtocols.Equals($@"{transferProtocol},{commandProtocol}")) return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            log.Error("Error getting IsDifferentTransferTypes.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int ChangeGridSingleSelectionState(int orionNodeId, string ncmNodeId, bool check)
    {
        try
        {
            var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
            if (check)
            {
                gridStateMng.Add(orionNodeId, ncmNodeId);
            }
            else
            {
                gridStateMng.Remove(orionNodeId);
            }
            return gridStateMng.Count();
        }
        catch (Exception ex)
        {
            log.Error("Error changing grid single selection state.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int ChangeGridMultipleSelectionState(Dictionary<string, string> nodeIds, bool check)
    {
        try
        {
            var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
            foreach (var nodeId in nodeIds)
            {
                if (check)
                {
                    gridStateMng.Add(Convert.ToInt32(nodeId.Key), nodeId.Value);
                }
                else
                {
                    gridStateMng.Remove(Convert.ToInt32(nodeId.Key));
                }
            }
            return gridStateMng.Count();
        }
        catch (Exception ex)
        {
            log.Error("Error changing grid multiple selection state.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int GetGridSelectionState()
    {
        try
        {
            var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
            return gridStateMng.Count();
        }
        catch (Exception ex)
        {
            log.Error("Error getting grid selection state.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int ChangeGridAllSelectionState(string groupingQueryString, string colToSearch, string searchTerm)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query($@"
            SELECT 
                NCMNodeProperties.CoreNodeID, 
                NCMNodeProperties.NodeID 
            FROM Cirrus.NodeProperties AS NCMNodeProperties
            INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
            INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON OrionCustomProperties.NodeID=Nodes.NodeID
            WHERE {GetGroupingClause(groupingQueryString)} AND {GetSearchClause(colToSearch, searchTerm)}");
                var nodeIds = dt.AsEnumerable().ToDictionary(row => Convert.ToString(row["CoreNodeID"]),
                    row => Convert.ToString(row["NodeID"]));
                return ChangeGridMultipleSelectionState(nodeIds, true);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error changing grid all selection state.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    private bool IsSelected(int id)
    {
        try
        {
            var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
            return gridStateMng.Contains(id);
        }
        catch (Exception ex)
        {
            log.Error("Error getting IsSelected.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void ClearGridSelectionState()
    {
        try
        {
            var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
            gridStateMng.Clear();
        }
        catch (Exception ex)
        {
            log.Error("Error clearing grid selection state.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public int[] GetSelectedOrionNodeIds()
    {
        try
        {
            var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
            return gridStateMng.GetKeys();
        }
        catch (Exception ex)
        {
            log.Error("Error getting selected orion node ID's.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public IEnumerable<string> GetSelectedNCMNodeIds()
    {
        var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
        return gridStateMng.GetValues();
    }

    [WebMethod(EnableSession = true)]
    public void PersonalizeGridState(string prefix, Dictionary<string, object> gridState)
    {
        var gridPersonalizationHelper = new GridPersonalizationHelper(prefix);
        gridPersonalizationHelper.SaveGridPersonalizationState(gridState);
    }

    [WebMethod(EnableSession = true)]
    public ConfigType[] GetConfigTypes(string[] nodeIds)
    {
        var nodesGuids = nodeIds.Select(x => new Guid(x));
        return configTypesProvider.GetCommon(nodesGuids).ToArray();
    }

    [WebMethod(EnableSession = true)]
    public void CloneConfig(string configId, string title, string config)
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                proxy.Cirrus.ConfigArchive.CloneConfig(Guid.Parse(configId), title, string.Empty, config,
                    HttpContext.Current.Profile.UserName);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error cloning config.", ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public object GetConfig(string configID)
    {
        var tuple = GetConfigInternal(configID);
        return new { Config = tuple.Item1, IsBinary = tuple.Item2 };
    }

    private Tuple<string, bool> GetConfigInternal(string configID)
    {
        var config = string.Empty;
        var isBinary = false;

        using (var proxy = isWrapper.GetProxy())
        {
            var dt = proxy.Query(@"SELECT Config, IsBinary FROM Cirrus.ConfigArchive WHERE ConfigID=@configID",
                new PropertyBag() { { @"configID", configID } });
            if (dt != null && dt.Rows.Count > 0)
            {
                config = Convert.ToString(dt.Rows[0]["Config"]);
                isBinary = Convert.ToBoolean(dt.Rows[0]["IsBinary"]);
            }
        }

        return new Tuple<string, bool>(config, isBinary);
    }

    /// <summary>
    /// This web method used on the Node Properties user control (\Orion\NCM\Admin\Nodes\NodeProperties.ascx).
    /// Returns Node Groups that start with the text you have typed.
    /// </summary>
    /// <param name="prefixText">The text you have typed.</param>
    /// <param name="count">The count matches.</param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public string[] GetNodeGroupValueList(string prefixText, int count)
    {
        using (var proxy = isWrapper.GetProxy())
        {
            var dt = proxy.Query(
                $@"SELECT DISTINCT TOP {count} NCMNodeProperties.NodeGroup FROM Cirrus.NodeProperties AS NCMNodeProperties 
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
WHERE NCMNodeProperties.NodeGroup LIKE '{SearchHelper.EncodeToSafeSqlType(prefixText)}%'");

            return dt.AsEnumerable().Select(row => row.Field<String>("NodeGroup")).ToArray();
        }
    }

    /// <summary>
    /// This web method used for Dynamic Selection control (SolarWinds.NCMModule.Web.Resources.dll\Resources\PolicyReportsManagement\Controls\DynamicSelection.cs).
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <param name="contextKey"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public string[] GetColumnValueList(string prefixText, int count, string contextKey)
    {
        using (var proxy = isWrapper.GetProxy())
        {
            var distinct = @"DISTINCT";
            if (contextKey.Equals(@"SysDescr", StringComparison.InvariantCultureIgnoreCase) ||
                contextKey.Equals(@"NodeComments", StringComparison.InvariantCultureIgnoreCase))
                distinct = string.Empty;

            var dt = proxy.Query(string.Format(@"SELECT {0} TOP {1} N.{2} FROM Cirrus.Nodes AS N INNER JOIN (SELECT NodeID AS OrionNodeID FROM Orion.Nodes) AS OrionNodes ON OrionNodes.OrionNodeID=N.CoreNodeID WHERE N.{2} LIKE '{3}%'", distinct, count, contextKey, SearchHelper.EncodeToSafeSqlType(prefixText)));

            var values = dt.AsEnumerable().Select(row => decryptor.Decrypt(Convert.ToString(row[contextKey]))).ToArray();
            return values;
        }
    }

    private int GetTotalRows(string groupingQueryString, bool showSelectedOnly, string colToSearch, string searchTerm)
    {
        var totalRows = 0;

        using (var proxy = isWrapper.GetProxy())
        {
            var dt = proxy.Query($@"
                SELECT COUNT(NCMNodeProperties.NodeID) AS totalRows 
                FROM Cirrus.NodeProperties NCMNodeProperties 
                INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON OrionCustomProperties.NodeID=Nodes.NodeID
                WHERE {GetGroupingClause(groupingQueryString)} AND {GetSelectedOnlyClause(showSelectedOnly, @"Nodes", @"NodeID")} AND {GetSearchClause(colToSearch, searchTerm)}");

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRows = (int)dt.Rows[0][0];
            }
        }
        return totalRows;
    }

    private string GetWhereClause(List<string> ncmNodeIds, string tableAlias)
    {
        if (ncmNodeIds.Count > 0)
            return
                $@"{tableAlias}.NodeID IN ({string.Join(@",", ncmNodeIds.Select(item => $@"'{item}'"))})";
        return @"(1=2)";
    }

    private string GetSelectedOnlyClause(bool showSelectedOnly, string tableName, string columnName)
    {
        if (showSelectedOnly)
        {
            var gridStateMng = new GridStateManager(Session, new Guid(gridSelectionStateId));
            var ids = gridStateMng.GetKeys();
            if (ids.Length == 0) return @"(1=1)";

            return $@"({SecurityHelper.RunLenghtEncode(ids, tableName, columnName)})";
        }

        return @"(1=1)";
    }

    private string GetGroupingClause(string groupingQueryString)
    {
        if (string.IsNullOrEmpty(groupingQueryString)) return @"(1=1)";

        var groupingClause = new StringBuilder();

        var first = true;
        foreach (var queryParam in groupingQueryString.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries))
        {
            if (first)
                first = false;
            else
                groupingClause.Append(@" AND ");

            var groupBy = queryParam.Split(new[] { '=' })[0];
            var groupByValue = HttpUtility.UrlDecode(queryParam.Split(new[] { '=' })[1]);

            var propType = typeof(String);
            CustomProperty cp;
            if (CustomPropertyHelper.IsCustomProperty(groupBy, out cp))
            {
                propType = cp.PropertyType;
            }

            if (string.IsNullOrEmpty(groupByValue))
            {
                if (propType == typeof(String))
                {
                    groupingClause.AppendFormat(@" ({0}='' OR {0} IS NULL) ", groupBy);
                }
                else
                {
                    groupingClause.AppendFormat(@" {0} IS NULL ", groupBy);
                }
            }
            else
            {
                if (propType == typeof(DateTime))
                {
                    groupingClause.AppendFormat(@" {0}=DateTime('{1:o}') ", groupBy, GetDateTimeSafe(groupByValue));
                }
                else
                {
                    groupingClause.AppendFormat(@" {0}='{1}' ", groupBy, SearchHelper.EncodeToSafeSqlType(groupByValue));
                }
            }
        }
        return groupingClause.ToString();
    }

    private string GetSearchClause(string colToSearch, string searchTerm)
    {
        if (string.IsNullOrEmpty(searchTerm))
            return @"(1=1)";

        var searchColumns = colToSearch.Split(';');
        var conditions = searchColumns.Select(t => $@"({t} LIKE '%{searchTerm}%')").ToArray();
        var stringBuilder = new StringBuilder(@"(");
        for (var i = 0; i < conditions.Length; i++)
        {
            stringBuilder.Append(conditions[i]);

            if ((i + 1) < conditions.Length)
                stringBuilder.Append(@" OR ");
        }
        stringBuilder.Append(@")");

        return stringBuilder.ToString();
    }

    private DateTime GetDateTimeSafe(string date)
    {
        DateTime result;
        if (DateTime.TryParse(date, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
        {
            return result;
        }

        throw new FormatException(date);
    }
}