<%@ WebService Language="C#" Class="ComparisonCriteria" %>

using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.ServiceModel;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Contracts.ConfigComparison;
using SolarWinds.Orion.Web;
using SolarWinds.Coding.Utils.Extensions;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;
using SolarWinds.NCMModule.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ComparisonCriteria : WebService
{
    private readonly ILogger log;
    private readonly IIsWrapper isLayer;

    public ComparisonCriteria()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetRegExPatternsPaged(string search)
    {
        try
        {
            int pageSize;
            int startRowNumber;

            var sortColumn = Context.Request.QueryString[@"sort"];
            var sortDirection = Context.Request.QueryString[@"dir"];

            int.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
            int.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

            return GetRegExPatternsPaged(search, sortColumn, sortDirection, Math.Max(pageSize, 25), startRowNumber);
        }
        catch (Exception ex)
        {
            log.Error("Error getting config compare regexes.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> GetRegExPattern(string regExID)
    {
        try
        {
            var result = new Dictionary<string, object>
            {
                {@"regExID", string.Empty},
                {@"title", string.Empty},
                {@"enabled", false},
                {@"regExPattern", string.Empty},
                {@"comment", string.Empty},
                {@"isBlock", false},
                {@"blockEndRegEx", string.Empty}
            };

            if (string.IsNullOrEmpty(regExID))
            {
                return result;
            }

            using (var proxy = isLayer.GetProxy())
            {
                var regEx = proxy.Cirrus.ComparisonCriteria.GetRegExById(Guid.Parse(regExID));

                if (regEx != null)
                {
                    result[@"regExID"] = regEx.RegExId.ToString();
                    result[@"title"] = regEx.Title;
                    result[@"enabled"] = regEx.Enabled;
                    result[@"regExPattern"] = regEx.RegExText;
                    result[@"comment"] = regEx.Comment;
                    result[@"isBlock"] = regEx.IsBlock;
                    result[@"blockEndRegEx"] = regEx.BlockEndRegEx;
                }

                return result;
            }
        }
        catch (Exception ex)
        {
            log.Error($"Error getting regex contract {regExID}.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> SaveRegExPattern(string regExID, bool enabled, string title, string regExPattern, string comment, bool isBlock, string blockEndRegEx)
    {
        try
        {
            Guid? id = null;
            if (!string.IsNullOrWhiteSpace(regExID))
            {
                id = Guid.Parse(regExID);
            }

            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.ComparisonCriteria.SaveRegExPattern(id, title, enabled, regExPattern, comment, isBlock, blockEndRegEx);
            }

            return null;
        }
        catch (Exception ex)
        {
            log.Error("ComparisonCriteria.Error in Save RegEx pattern", ex);
            return InitException(Resources.NCMWebContent.WEBDATA_VK_562, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> EnableOrDisableRegExPatterns(Guid[] regExIDs, bool enableOrDisable)
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.ComparisonCriteria.EnableOrDisableRegExPatterns(regExIDs, enableOrDisable);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error($"ComparisonCriteria.Error in {(enableOrDisable ? @"Enable" : @"Disable")} RegEx patterns", ex);
            if (enableOrDisable)
            {
                return InitException(Resources.NCMWebContent.WEBDATA_VK_702, ex);
            }

            return InitException(Resources.NCMWebContent.WEBDATA_VK_703, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> DeleteRegExPatterns(Guid[] regExIDs)
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.ComparisonCriteria.DeleteRegExPatterns(regExIDs);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("ComparisonCriteria.Error in Delete RegEx patterns", ex);
            return InitException(Resources.NCMWebContent.WEBDATA_VK_563, ex);
        }
    }

    private PageableDataTable GetRegExPatternsPaged(string search, string orderByColumn, string orderbyDirection, int pageSize, int startRowNumber)
    {
        using (var proxy = isLayer.GetProxy())
        {
            IEnumerable<RegEx> regexes = proxy.Cirrus.ComparisonCriteria.GetRegExes();
            regexes = PerformSearch(regexes, search).ToArray();

            var count = regexes.LongCount();

            regexes = SortRegExes(regexes, orderByColumn, orderbyDirection);
            regexes = regexes.GetPage(pageSize, startRowNumber);

            return new PageableDataTable(ToDataTable(regexes), count);
        }
    }

    private DataTable ToDataTable(IEnumerable<RegEx> regexes)
    {
        var table = new DataTable("Cirrus.CompareRegExs");
        table.Columns.Add("RegExID", typeof(Guid));
        table.Columns.Add("Title", typeof(string));
        table.Columns.Add("Enabled", typeof(bool));
        table.Columns.Add("RegEx", typeof(string));
        table.Columns.Add("IsBlock", typeof(bool));
        table.Columns.Add("BlockEndRegEx", typeof(string));
        table.Columns.Add("Comment", typeof(string));

        foreach (var entity in regexes)
        {
            var row = table.NewRow();
            row.ItemArray = new object[]
            {
                entity.RegExId,
                entity.Title,
                entity.Enabled,
                entity.RegExText,
                entity.IsBlock,
                entity.BlockEndRegEx,
                entity.Comment
            };

            table.Rows.Add(row);
        }

        return table;
    }

    private IEnumerable<RegEx> SortRegExesAsc(IEnumerable<RegEx> regExes, string orderByColumn)
    {
        IEnumerable<RegEx> sortedRegExes;
        switch (orderByColumn.ToUpperInvariant())
        {
            case "REGEXID":
                sortedRegExes = regExes.OrderBy(x => x.RegExId);
                break;
            case "TITLE":
                sortedRegExes = regExes.OrderBy(x => x.Title);
                break;
            case "ENABLED":
                sortedRegExes = regExes.OrderBy(x => x.Enabled);
                break;
            case "REGEX":
                sortedRegExes = regExes.OrderBy(x => x.RegExText);
                break;
            case "ISBLOCK":
                sortedRegExes = regExes.OrderBy(x => x.IsBlock);
                break;
            case "BLOCKENDREGEX":
                sortedRegExes = regExes.OrderBy(x => x.BlockEndRegEx);
                break;
            case "COMMENT":
                sortedRegExes = regExes.OrderBy(x => x.Comment);
                break;
            default: throw new ArgumentException(string.Format(Resources.NCMWebContent.ComparisonCriteria_UnknownColumnError, orderByColumn), nameof(orderByColumn));
        }

        return sortedRegExes;
    }

    private IEnumerable<RegEx> SortRegExesDesc(IEnumerable<RegEx> regExes, string orderByColumn)
    {
        return SortRegExesAsc(regExes, orderByColumn)
            .Reverse();
    }

    private IEnumerable<RegEx> SortRegExes(IEnumerable<RegEx> regExes, string orderByColumn, string orderbyDirection)
    {
        if (!string.IsNullOrEmpty(orderbyDirection))
        {
            switch (orderbyDirection.ToUpperInvariant())
            {
                case "ASC":
                    break;
                case "DESC":
                    return SortRegExesDesc(regExes, orderByColumn);
                default: throw new ArgumentException(string.Format(Resources.NCMWebContent.ComparisonCriteria_UnknownOrderByDirectionError, orderbyDirection), nameof(orderbyDirection));
            }
        }

        return SortRegExesAsc(regExes, orderByColumn);
    }

    private IEnumerable<RegEx> PerformSearch(IEnumerable<RegEx> foundRegExes, string search)
    {
        if (string.IsNullOrEmpty(search))
        {
            return foundRegExes;
        }

        return foundRegExes.Where(x => AnyStringContainsSearchPhrase(search, x.RegExText, x.BlockEndRegEx, x.Title, x.Comment));
    }

    private static bool AnyStringContainsSearchPhrase(string searchPhrase, params string[] texts)
    {
        foreach (var text in texts)
        {
            if (!string.IsNullOrEmpty(text) && text.Contains(searchPhrase, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
        }
        return false;
    }

    private Dictionary<string, object> InitException(string source, Exception ex)
    {
        var errorEntry = new Dictionary<string, object>();
        errorEntry[@"error"] = true;
        errorEntry[@"source"] = source;
        string message;
        if (ex is FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>)
        {
            message = (ex as FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message;
        }
        else
        {
            message = ex.Message;
        }
        errorEntry[@"message"] = message;
        return errorEntry;
    }
}
