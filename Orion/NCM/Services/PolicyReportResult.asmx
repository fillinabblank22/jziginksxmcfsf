<%@ WebService Language="C#" Class="PolicyReportResult" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PolicyReportResult  : WebService {

    private static readonly string REPORTS_RESULT_SESSION_KEY = @"complicance_report_result";
    
    private static readonly Log _log = new Log ();

    [WebMethod(EnableSession = true)]
    public string GenerateColumnModel(Guid reportId)
    {
        string json = string.Empty;
        try
        {
            ISWrapper isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                json = proxy.Cirrus.PolicyReports.GetComplianceColumnsInJSON(reportId, HttpContext.Current.User.Identity.Name);
            }
        }
        catch (Exception ex)
        {
            _log.Error("Generate column model error", ex);
        }

        return json;
    }
    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GenerateReportResults(Guid reportId)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        if (pageSize == 0)
        {
            pageSize = Convert.ToInt32(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(@"NCM_ComplianceReportResultViewer_PageSize"));
            pageSize = pageSize == 0 ? 250 : pageSize;
        }
        
        string sortColumn = Context.Request.QueryString[@"sort"];
        string sortDirection = Context.Request.QueryString[@"dir"];

        DataTable dt = SelectReportData(reportId, sortColumn, sortDirection);
        DataTable res = GetReportResultsPaget(dt, startRowNumber, pageSize);
        return new PageableDataTable(res, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public string FindNodeInGrid(string sort, string dir, string nodeId, int limit, Guid reportId)
    {
        bool success = false;
        int rowIndex = 0;
        int start = 0;

        try
        {
            if (!string.IsNullOrEmpty(nodeId))
            {
                DataTable dt = SelectReportData(reportId, sort, dir);
                DataRow[] dr = dt.Select($"NodeID='{nodeId}'");
                if (dr.Length > 0)
                {
                    rowIndex = dt.Rows.IndexOf(dr[0]);
                    int pos = rowIndex;
                    start = ((int)pos / limit) * limit;
                    success = true;
                }
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error finding node in grid.", ex);
            success = false;
            start = 0;
        }

        string jsonRes = $@"{{success:{success.ToString().ToLowerInvariant()},data:{{start:{start}}}}}";
        return jsonRes;
    }
    
    #region

    private DataTable SelectReportData(Guid reportid, string sortColumn, string sortDirection)
    {
        DataTable dt = Session[REPORTS_RESULT_SESSION_KEY] as DataTable;
        if (dt == null)
        {

            try
            {
                ISWrapper isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    dt = proxy.Cirrus.PolicyReports.GetComplianceDataTable(reportid, false, HttpContext.Current.User.Identity.Name);
                    Session[REPORTS_RESULT_SESSION_KEY] = dt;
                }
            }
            catch (Exception ex)
            {
                _log.Error("Report generation error", ex);
            }
        }

        string sortStr = $@"{sortColumn} {sortDirection}";
        if (!string.IsNullOrEmpty(sortStr) && dt != null)
        {
            dt.DefaultView.Sort = sortStr;
            dt = dt.DefaultView.ToTable();
            Session[REPORTS_RESULT_SESSION_KEY] = dt;
        }

        return dt ?? new DataTable();
    }
    
    private DataTable GetReportResultsPaget(DataTable dt, int start, int limit)
    {
        
        if (dt == null)
            return new DataTable();

        int posFrom = start;
        int posTo = start + limit;
        if (posTo >= dt.Rows.Count)
            posTo = dt.Rows.Count;
        
        DataTable res = dt.Clone();

        for (int i = posFrom; i < posTo; i++)
        {
            res.ImportRow(dt.Rows[i]);
        }

        return res;
    }
    
    #endregion
}

