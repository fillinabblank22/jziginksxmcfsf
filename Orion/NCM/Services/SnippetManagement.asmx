﻿<%@ WebService Language="C#" Class="SnippetManagement" %>

using System;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;
using System.Data;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SnippetManagement : WebService
{
    private readonly ILogger logger;

    public SnippetManagement()
    {
        logger = ServiceLocator.Container.Resolve<ILoggerFactory>().Create(GetType());
    }

    #region Services

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetSnippetPaged(string searchTerm)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            int startRowNumber;
            int pageSize;

            Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
            Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

            searchTerm = searchTerm.Replace(@"'", @"''");

            if (pageSize == 0)
            {
                pageSize = Convert.ToInt32(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(@"NCM_SnippetManagement_PageSize"));
                pageSize = pageSize == 0 ? 25 : pageSize;
            }

            var sortColumn = string.IsNullOrEmpty(Context.Request.QueryString[@"sort"]) ? @"ConfigTitle" : Context.Request.QueryString[@"sort"];
            var sortDirection = string.IsNullOrEmpty(Context.Request.QueryString[@"dir"]) ? @"asc" : Context.Request.QueryString[@"dir"];

            var queryGetData = string.Format(@"SELECT ConfigID, ConfigTitle, DownloadTime, ModifiedTime, Config, Comments
                                                            FROM Cirrus.SnippetArchive WHERE {4}
                                                                ORDER BY {0}  {1}
                                                                    WITH ROWS {2} TO {3} ", sortColumn, sortDirection, startRowNumber + 1, pageSize + startRowNumber, GetSearchCondition(searchTerm));

            var totalRowsCount = GetTotalRowsCount(searchTerm);
            var dt = proxy.Query(queryGetData);

            return new PageableDataTable(dt, totalRowsCount);
        }
    }


    [WebMethod(EnableSession = true)]
    public ErrorEntry AddNewSnippet(string configTitle, string config, string comments)
    {
        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                proxy.Cirrus.SnippetArchive.AddSnippet(configTitle, config, comments);
                return null;
            }
        }
        catch (Exception ex)
        {
            logger.Error("Error adding a new snippet.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry EditSnippet(string configId, string configTitle, string config, string comments)
    {
        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                proxy.Cirrus.SnippetArchive.UpdateSnippet(Guid.Parse(configId), configTitle, config, comments);
                return null;
            }
        }
        catch (Exception ex)
        {
            logger.Error("Error editing snippet.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry DeleteSnippet(List<string> configId)
    {
        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                foreach (var snippetId in configId)
                {
                    proxy.Cirrus.SnippetArchive.DeleteSnippet(Guid.Parse(snippetId));
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            logger.Error("Error deleting snippet.", ex);
            return new ErrorEntry(ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetSnippetsList()
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var queryGetData = @"SELECT ConfigID, ConfigTitle
                                                            FROM Cirrus.SnippetArchive 
                                                                ORDER BY ConfigTitle ASC";
            var dt = proxy.Query(queryGetData);

            return dt;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetSnippetConfig(string snippetId)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var queryGetData = @"SELECT Config FROM Cirrus.SnippetArchive where ConfigID=@snippetId";
            var dt = proxy.Query(queryGetData, new PropertyBag() {{@"snippetId", snippetId}});

            return dt.Rows[0][0].ToString();
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetConfigTypes()
    {
        var isLayer = new ISWrapper();
        var configTypesList = new List<string>();
        using (var proxy = isLayer.Proxy)
        {
            configTypesList = proxy.Cirrus.ConfigArchive.GetConfigTypes().Split(',').ToList();
        }

        var json = new JavaScriptSerializer().Serialize(configTypesList);
        return json;
    }

    #endregion

    #region Additional methods

    private int GetTotalRowsCount(string searchTerm)
    {
        int totalRowsCount;

        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var queryGetCount =
                $@"SELECT COUNT(ConfigTitle) as totalCount FROM Cirrus.SnippetArchive WHERE {GetSearchCondition(searchTerm)}";
            var dt = proxy.Query(queryGetCount);

            totalRowsCount = Convert.ToInt32(dt.Rows[0]["totalCount"]);
        }

        return totalRowsCount;
    }

    private string GetSearchCondition(string search)
    {
        if (string.IsNullOrEmpty(search))
            return @"(1=1)";

        return
            $@"ConfigTitle LIKE '%{SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers.SearchHelper.WildcardToLike(search)}%'";
    }

    #endregion
}
