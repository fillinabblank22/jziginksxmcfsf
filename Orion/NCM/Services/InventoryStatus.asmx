<%@ WebService Language="C#" Class="InventoryStatus" %>

using System;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class InventoryStatus : WebService
{
    private readonly Log log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetInventoryStatusPaged(double clientOffset)
    {
        var pageSize = 0;
        var startRowNumber = 0;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        var swql = BuildPageableSwqlQuery(pageSize, startRowNumber, sortColumn, sortDirection);
        var pageData = ExecuteSWQL(swql);
        Lookup(pageData, clientOffset);

        return new PageableDataTable(pageData, GetRowCount());
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable RefreshInventoryStatus(string[] inventoryIds, double clientOffset)
    {
        var swql = BuildSwqlQuery(inventoryIds);
        var pageData = ExecuteSWQL(swql);
        Lookup(pageData, clientOffset);

        return new PageableDataTable(pageData, 0);
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry ReExecuteInventory(Guid[] ids)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.Inventory.StartInventory(ids);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in InventoryStatus.ReExecute", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_102, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry CancelInventory(string[] ids)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var inventoryIds = ids != null ? ids : GetInventoryIds(@"NCMInventoryQueue.Error=0 AND NCMInventoryQueue.Status!='Complete'");
                if (inventoryIds != null)
                {
                    proxy.Cirrus.Inventory.ClearInventory(inventoryIds);
                    proxy.Cirrus.Inventory.CancelInventory(inventoryIds);
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in InventoryStatus.CancelInventory", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_99, ex);
        }

    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry ClearAll(string[] ids)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var inventoryIds = ids != null ? ids : GetInventoryIds(@"NCMInventoryQueue.Status='Complete' OR NCMInventoryQueue.Status='Inventory Cancelled by User' OR NCMInventoryQueue.Error=1");
                if (inventoryIds != null)
                    proxy.Cirrus.Inventory.ClearInventory(inventoryIds);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in InventoryStatus.ClearAll", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_101, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry ClearComplete()
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var inventoryIds = GetInventoryIds(@"NCMInventoryQueue.Status='Complete'");
                if (inventoryIds != null)
                    proxy.Cirrus.Inventory.ClearInventory(inventoryIds);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in InventoryStatus.ClearComplete", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_101, ex);
        }
    }

    [WebMethod(EnableSession = true)]
    public ErrorEntry ClearFailed()
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var inventoryIds = GetInventoryIds(@"NCMInventoryQueue.Error=1");
                if (inventoryIds != null)
                    proxy.Cirrus.Inventory.ClearInventory(inventoryIds);
            }
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in InventoryStatus.ClearFailed", ex);
            return new ErrorEntry(Resources.NCMWebContent.WEBDATA_VK_101, ex);
        }
    }

    private void Lookup(DataTable dt, double clientOffset)
    {
        foreach (DataRow row in dt.Rows)
        {
            OffsetDateTime(row, clientOffset);
        }
    }

    private void OffsetDateTime(DataRow row, double clientOffset)
    {
        if (row["DateTime"] != DBNull.Value)
        {
            var localTime = DateTimeOffset.Now;
            var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;

            var time = Convert.ToDateTime(row["DateTime"]).AddMinutes(offsetMinutes);
            row["DateTime"] = time;
        }
    }

    private string[] GetInventoryIds(string whereClause)
    {
        var swql = $@"
        SELECT 
            NCMInventoryQueue.InventoryID
        FROM Cirrus.InventoryQueue AS NCMInventoryQueue
        INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON NCMNodeProperties.NodeID=NCMInventoryQueue.NodeID
        INNER JOIN Orion.Nodes AS Nodes ON Nodes.NodeID=NCMNodeProperties.CoreNodeID
        WHERE {(!string.IsNullOrEmpty(whereClause) ? whereClause : @"(1=2)")}";

        var dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
            return dt.AsEnumerable().Select(row => row[0].ToString()).ToArray();
        return null;
    }

    private long GetRowCount()
    {
        var swql = @"
        SELECT 
            COUNT(NCMInventoryQueue.InventoryID) AS RowCount 
        FROM Cirrus.InventoryQueue AS NCMInventoryQueue
        INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON NCMNodeProperties.NodeID=NCMInventoryQueue.NodeID
        INNER JOIN Orion.Nodes AS Nodes ON Nodes.NodeID=NCMNodeProperties.CoreNodeID";

        var rowCountTable = ExecuteSWQL(swql);
        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        return 0;
    }

    private DataTable ExecuteSWQL(string swql)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Query(swql);
        }
    }

    private string BuildSwqlQuery(string[] inventoryIds)
    {
        return $@"
            SELECT 
                NCMInventoryQueue.InventoryID,
                NCMInventoryQueue.Status,
                NCMInventoryQueue.Error,
                NCMInventoryQueue.UserName,
                NCMInventoryQueue.DateTime,
                NCMInventoryQueue.Cancelling,
                NCMInventoryQueue.Rejected,
                CASE WHEN NCMInventoryQueue.Status='Complete' OR NCMInventoryQueue.Status='Inventory Cancelled by User' OR NCMInventoryQueue.Error=1 THEN 0 ELSE 1 END AS NumStatus
            FROM Cirrus.InventoryQueue AS NCMInventoryQueue
            INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON NCMInventoryQueue.NodeID=NCMNodeProperties.NodeID
            INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
            WHERE NCMInventoryQueue.InventoryID IN ({string.Join(@",", inventoryIds.Select(item => $@"'{item}'"))})";
    }

    private string BuildPageableSwqlQuery(int pageSize, int startRow, string sortColumn, string sortDirection)
    {
        return $@"
            SELECT 
                NCMInventoryQueue.InventoryID,
                Nodes.Caption AS SysName,
                Nodes.IP_Address AS IPAddress,
                NCMInventoryQueue.Status,
                NCMInventoryQueue.Error,
                NCMInventoryQueue.UserName,
                NCMInventoryQueue.DateTime,
                NCMInventoryQueue.NodeID,
                NCMInventoryQueue.Cancelling,
                NCMInventoryQueue.Rejected,
                CASE WHEN NCMInventoryQueue.Status='Complete' OR NCMInventoryQueue.Status='Inventory Cancelled by User' OR NCMInventoryQueue.Error=1 THEN 0 ELSE 1 END AS NumStatus
            FROM Cirrus.InventoryQueue AS NCMInventoryQueue
            INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON NCMInventoryQueue.NodeID=NCMNodeProperties.NodeID
            INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
            ORDER BY {GetOrderBy(sortColumn, sortDirection)}
            WITH ROWS {startRow + 1} TO {startRow + pageSize}";
    }

    private string GetOrderBy(string sortColumn, string sortDirection)
    {
        if (sortColumn.Equals(@"Status", StringComparison.InvariantCultureIgnoreCase))
            return string.Format(@"Error {0}, Status {0}", sortDirection);
        return $@"{sortColumn} {sortDirection}";
    }
}