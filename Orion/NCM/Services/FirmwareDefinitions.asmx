﻿<%@ WebService Language="C#" Class="FirmwareDefinitions" %>

using System;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class FirmwareDefinitions  : WebService {

    private readonly Log _log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFirmwareDefinitions(string search, double clientOffset)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString[@"sort"];
        string sortDirection = Context.Request.QueryString[@"dir"];
        
        Int32.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        search = search.Replace(@"'", @"''");
        
        string swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, search);
        DataTable dt = ExecuteSWQL(swql);

        DateTimeOffset localTime = DateTimeOffset.Now;
        double offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;
        dt.AsEnumerable().Where(row => !row.IsNull("LastModified")).ToList().ForEach(row => row["LastModified"] = Convert.ToDateTime(row["LastModified"]).ToLocalTime().AddMinutes(offsetMinutes));
        
        return new PageableDataTable(dt, GetRowCount(search));
    }

    [WebMethod(EnableSession = true)]
    public int DuplicateFirmwareDefinition(int id, string newName)
    {
        try
        {
            if (CommonHelper.IsDemoMode())
            {
                return 0;
            }

            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                var firmwareDefinition = proxy.Cirrus.FirmwareDefinitions.GetFirmwareDefinition(id);
                firmwareDefinition.Name = newName;
                firmwareDefinition.Canned = false;
                
                return proxy.Cirrus.FirmwareDefinitions.AddFirmwareDefinition(firmwareDefinition);
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error in DuplicateFirmwareDefinition", ex);
            return 0;
        }
    }
    
    [WebMethod(EnableSession = true)]
    public ErrorEntry DeleteFirmwareDefinitions(int[] ids)
    {
        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                proxy.Cirrus.FirmwareDefinitions.DeleteFirmwareDefinitions(ids);
            }
            return null;
        }
        catch (Exception ex)
        {
            _log.Error("Error deleting definitions.", ex);
            return new ErrorEntry(Resources.NCMWebContent.FirmwareDefinitions_DeleteErrorTitle, ex);
        }
    }

    private DataTable ExecuteSWQL(string swql)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            return proxy.Query(swql);
        }
    }

    private string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderbyDirection, string search)
    {
        pageSize = Math.Max(pageSize, 1);

        string swql = $@"
            SELECT 
                ID, 
                Name, 
                LastModified, 
                Description, 
                Author,
                Canned
            FROM NCM.FirmwareDefinitions 
            WHERE {GetSearchClause(search)}
            ORDER BY {orderByColumn} {orderbyDirection}
            WITH ROWS {startRowNumber + 1} TO {startRowNumber + pageSize}";
        return swql;
    }

    private long GetRowCount(string search)
    {
        string swql = $@"
            SELECT 
                COUNT(ID) AS theCount
            FROM NCM.FirmwareDefinitions
            WHERE {GetSearchClause(search)}";

        DataTable rowCountTable = ExecuteSWQL(swql);
        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
            return Convert.ToInt64(rowCountTable.Rows[0][0]);
        return 0;
    }

    private string GetSearchClause(string search)
    {
        if (string.IsNullOrEmpty(search))
            return @"(1=1)";

        string searchValue = SearchHelper.WildcardToLike(search);
        string searchClause = $@"Name LIKE '%{searchValue}%'";
        return searchClause;
    }
}