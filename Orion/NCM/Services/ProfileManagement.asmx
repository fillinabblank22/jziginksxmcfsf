﻿<%@ WebService Language="C#" Class="ProfileManagement" %>

using System;
using System.Security;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceModel;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ProfileManagement  : WebService
{
    private readonly ILogger log;
    private readonly IIsWrapper isLayer;

    public ProfileManagement()
    {
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetProfiles()
    {
        try
        {
            var sortColumn = Context.Request.QueryString[@"sort"];
            var sortDirection = Context.Request.QueryString[@"dir"];

            var dt = GetProfilesDataTable(sortColumn, sortDirection);

            return new PageableDataTable(dt, dt.Rows.Count);
        }
        catch (Exception ex)
        {
            log.Error("Error getting profiles.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> DeleteProfile(int profileID)
    {
        try
        {
            var denyAccess = !((ProfileCommon) HttpContext.Current.Profile).AllowNodeManagement;

            if (denyAccess)
            {
                throw new SecurityException(Resources.NCMWebContent.ProfileManagement_InsufficientPermissionsMessage);
            }

            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.Nodes.DeleteConnectionProfile(profileID);
            }

            return null;
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            log.Error("Error in DeleteProfile", ex);

            return BuildDeleteProfileErrorData(ex.Detail.Message);
        }
        catch (TargetInvocationException ex)
        {
            log.Error("Error in DeleteProfile", ex);

            var wcfEx = ex.InnerException as FaultException<InfoServiceFaultContract>;
            if (wcfEx != null)
            {
                return BuildDeleteProfileErrorData(wcfEx.Detail.Message);
            }

            var msg = string.Join(Environment.NewLine, ex.EnumerateExceptions().Select(e => e.Message));
            return BuildDeleteProfileErrorData(msg);
        }
        catch (Exception ex)
        {
            log.Error("Error in DeleteProfile", ex);

            var msg = string.Join(Environment.NewLine, ex.EnumerateExceptions().Select(e => e.Message));
            return BuildDeleteProfileErrorData(msg);
        }
    }

    private Dictionary<string, object> BuildDeleteProfileErrorData(string message)
    {
        return new Dictionary<string, object>
        {
            [@"Error"] = true,
            [@"Source"] = Resources.NCMWebContent.WEBDATA_VK_858,
            [@"Msg"] = message
        };
    }

    private DataTable GetProfilesDataTable(string sortColumn, string sortDirection)
    {
        var dt = new DataTable();
        dt.Columns.Add("ProfileID", typeof(int));
        dt.Columns.Add("ProfileName", typeof(string));
        dt.Columns.Add("UseForAutoDetect", typeof(bool));

        using (var proxy = isLayer.GetProxy())
        {
            var profileList = proxy.Cirrus.Nodes.GetAllConnectionProfiles();

            foreach (var profile in profileList)
            {
                dt.Rows.Add(profile.ID, profile.Name, profile.UseForAutoDetect);
            }
        }

        dt.DefaultView.Sort = $"{sortColumn} {sortDirection}";
        dt = dt.DefaultView.ToTable();

        return dt;
    }
}