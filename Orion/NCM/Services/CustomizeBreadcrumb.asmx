<%@ WebService Language="C#" Class="CustomizeBreadcrumb" %>

using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class CustomizeBreadcrumb  : WebService
{
    #region const
    private const string WEB_USER_PREFIX = "ncm_BC_Customization_";
    private const string CUSTOM_DLG_CONFIG = "CONFIG";
    private const string CUSTOM_DLG_POLICY_REPORT = "POLICYREPORT";

    private static readonly string[] OPTIONS_CONFIG = {
        @"<option value=''>" + Resources.NCMWebContent.WEBDATA_VM_172 + @"</option>" ,
        @"<option value='10'>" + Resources.NCMWebContent.WEBDATA_VM_173 + @"</option>" ,
        @"<option value='25'>"+ Resources.NCMWebContent.WEBDATA_VM_174 + @"</option>" ,
        @"<option value='50'>" + Resources.NCMWebContent.WEBDATA_VM_175 + @"</option>" ,
        @"<option value='100'>"+ Resources.NCMWebContent.WEBDATA_VM_176 + @"</option>"
    }; 
    
    private static readonly string[] OPTIONS_POLICY_REPORT = { 
        @"<option value=''>"+Resources.NCMWebContent.WEBDATA_VM_180+@"</option>" ,
        @"<option value='SameFolder'>"+Resources.NCMWebContent.WEBDATA_VM_181+@"</option>" ,
    };
    
    #endregion
    
    [WebMethod]
    public void SaveBcCustomization(string customizationValue, string objectName)
    {
        switch (objectName.ToUpper())
        {
            case CUSTOM_DLG_CONFIG:
                SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Set(WEB_USER_PREFIX + CUSTOM_DLG_CONFIG, customizationValue);
                break;
            case CUSTOM_DLG_POLICY_REPORT:
                SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Set(WEB_USER_PREFIX + CUSTOM_DLG_POLICY_REPORT, customizationValue); 
                break;
            default:
                return;
        }
    }

    [WebMethod]
    public string GetListOfOptions(string objectName)
    {
        string options = string.Empty;
        string custOption = string.Empty;

        switch (objectName.ToUpper())
        {
            case CUSTOM_DLG_CONFIG:
                custOption = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(WEB_USER_PREFIX + CUSTOM_DLG_CONFIG);
                options = OptionsToString(OPTIONS_CONFIG);
                break;
            case  CUSTOM_DLG_POLICY_REPORT:
                custOption = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(WEB_USER_PREFIX + CUSTOM_DLG_POLICY_REPORT);
                options = OptionsToString(OPTIONS_POLICY_REPORT);
                break;
            default:
                options = $@"<option value=''>[All {objectName}]</option>";
                break;
        }

        if (!string.IsNullOrEmpty(custOption))
        {
            options = options.Replace($@"value='{custOption}'", $@"value='{custOption}' selected='true'");
        }
        return options;
    }


    #region private methods
    private string OptionsToString(string[] options)
    {
        StringBuilder sb = new StringBuilder();
        foreach (string option in options)
        {
            sb.AppendLine(option);
        }

        return sb.ToString();
    }
    #endregion

}

