﻿<%@ WebService Language="C#" Class="FirmwareOperations" %>

using System;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Firmware;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class FirmwareOperations : WebService
{
    private static readonly Log log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFirmwareOperationsPaged(string value, string search, double clientOffset)
    {
        int pageSize;
        int startRowNumber;

        var sortColumn = Context.Request.QueryString[@"sort"];
        var sortDirection = Context.Request.QueryString[@"dir"];

        int.TryParse(Context.Request.QueryString[@"start"], out startRowNumber);
        int.TryParse(Context.Request.QueryString[@"limit"], out pageSize);

        var swql = PageableQuery(pageSize, startRowNumber, sortColumn, sortDirection, value, search);

        var dt = ExecuteSWQL(swql);

        var localTime = DateTimeOffset.Now;
        var offsetMinutes = localTime.Offset.TotalMinutes + clientOffset;
        dt.AsEnumerable().Where(row => !row.IsNull("CreationDate")).ToList().ForEach(row =>
            row["CreationDate"] = Convert.ToDateTime(row["CreationDate"]).ToLocalTime().AddMinutes(offsetMinutes));
        dt.AsEnumerable().Where(row => !row.IsNull("RunAt")).ToList().ForEach(row =>
            row["RunAt"] = Convert.ToDateTime(row["RunAt"]).ToLocalTime().AddMinutes(offsetMinutes));

        return new PageableDataTable(dt, GetRowCount(value, search));
    }

    [WebMethod(EnableSession = true)]
    public string GenerateScriptPreview(int coreNodeId)
    {
        try
        {
            if (CommonHelper.IsDemoMode())
            {
                return null;
            }

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                if (FirmwareUpgradeWizardController.FirmwareOperationNodes != null)
                {
                    var nodeOpts =
                        FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(node =>
                            node.CoreNodeId == coreNodeId);
                    if (nodeOpts != null)
                    {
                        return proxy.Cirrus.FirmwareOperations.GenerateScriptPreview(nodeOpts);
                    }
                }
            }

            return string.Empty;
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.GenerateScriptPreview", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetLog(int operationId, int lastLinesCount)
    {
        const string swql = "SELECT Log FROM NCM.FirmwareOperationsView WHERE ID=@operationId";

        var isLargeLogFile = false;
        var logToDisplay = string.Empty;

        var dt = ExecuteSWQL(swql, new PropertyBag() { { @"operationId", operationId } });
        if (dt != null && dt.Rows.Count > 0)
        {
            var fullLog = Convert.ToString(dt.Rows[0][0], CultureInfo.InvariantCulture);
            var fullLogLines = fullLog.Split('\n');

            isLargeLogFile = fullLogLines.Length > lastLinesCount;
            if (isLargeLogFile)
            {
                const string separator = "\n";
                var lastLines = fullLogLines.Reverse().Take(lastLinesCount);
                logToDisplay = string.Join(separator, lastLines.Reverse());
            }
            else
            {
                logToDisplay = fullLog;
            }
        }

        return JsonConvert.SerializeObject(new
        {
            IsLargeLogFile = isLargeLogFile,
            LogToDisplay = logToDisplay
        });
    }

    [WebMethod(EnableSession = true)]
    public string GetFirmwareOperationsStatus(IEnumerable<int> ids)
    {
        if (ids == null || !ids.Any())
        {
            return JsonConvert.SerializeObject(null);
        }

        var swql = $@"
            SELECT 
                ID,
                Status,
                ErrorMessage,
                CompletedOperations,
                AllOperations,
                CASE WHEN Log IS NOT NULL THEN '' ELSE NULL END AS Log
            FROM NCM.FirmwareOperationsView
            WHERE ID IN ({string.Join(@",", ids.Select(x => $@"'{x}'"))})";

        var dt = ExecuteSWQL(swql);
        if (dt != null)
        {
            return JsonConvert.SerializeObject(dt.AsEnumerable().Select(x => new
            {
                ID = x.Field<int>("ID"),
                Status = x.Field<int>("Status"),
                ErrorMessage = x.IsNull("ErrorMessage") ? string.Empty : x.Field<string>("ErrorMessage"),
                CompletedOperations = x.IsNull("CompletedOperations") ? 0 : x.Field<int>("CompletedOperations"),
                AllOperations = x.IsNull("AllOperations") ? 0 : x.Field<int>("AllOperations"),
                Log = x.IsNull("Log") ? null : x.Field<string>("Log"),
            }));
        }

        return JsonConvert.SerializeObject(null);
    }

    [WebMethod(EnableSession = true)]
    public bool CancelAll(int[] selectedIds)
    {
        try
        {
            if (CommonHelper.IsDemoMode())
            {
                return true;
            }

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var operationIds = selectedIds ?? GetOperationIds(new[] { eFirmwareUpgradeStatus.CollectingData, eFirmwareUpgradeStatus.Upgrading });
                if (operationIds != null && operationIds.Length > 0)
                {
                    proxy.Cirrus.FirmwareOperations.CancelUpgrade(operationIds);
                    proxy.Cirrus.FirmwareOperations.DeleteFirmwareOperations(operationIds);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.CancelAll", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool ClearAll(int[] selectedIds)
    {
        try
        {
            if (CommonHelper.IsDemoMode())
            {
                return true;
            }

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                if (selectedIds != null)
                {
                    proxy.Cirrus.FirmwareOperations.CancelUpgrade(selectedIds);
                    proxy.Cirrus.FirmwareOperations.DeleteFirmwareOperations(selectedIds);
                }
                else
                {
                    var activeOperationIds = GetOperationIds(new[] { eFirmwareUpgradeStatus.CollectingData, eFirmwareUpgradeStatus.Upgrading });
                    if (activeOperationIds != null && activeOperationIds.Length > 0)
                    {
                        proxy.Cirrus.FirmwareOperations.CancelUpgrade(activeOperationIds);
                    }
                    var allOperationIds = GetOperationIds();
                    proxy.Cirrus.FirmwareOperations.DeleteFirmwareOperations(allOperationIds);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.ClearAll", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool ClearComplete()
    {
        try
        {
            if (CommonHelper.IsDemoMode())
            {
                return true;
            }

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var operationIds = GetOperationIds(new[] { eFirmwareUpgradeStatus.Complete });
                proxy.Cirrus.FirmwareOperations.DeleteFirmwareOperations(operationIds);
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.ClearComplete", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool ClearFailed()
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var operationIds = GetOperationIds(new[] { eFirmwareUpgradeStatus.Error });
                proxy.Cirrus.FirmwareOperations.DeleteFirmwareOperations(operationIds);
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.ClearFailed", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public void ClearSessionData()
    {
        FirmwareUpgradeWizardController.ClearSessionData();
    }

    [WebMethod(EnableSession = true)]
    public bool SetSessionData(int operationId)
    {
        try
        {
            FirmwareUpgradeWizardController.ClearSessionData();
            var swql = @"
                   SELECT 
                        Caption,
                        MachineType,
                        EngineID,
                        ServerName,
                        NodeOptionsXml
                    FROM Orion.Nodes AS OrionNodes
                    INNER JOIN Orion.Engines as Engines ON 
                    OrionNodes.EngineID=Engines.EngineID 
                    INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON NCMNodeProperties.CoreNodeID=OrionNodes.NodeID
                    INNER JOIN NCM.FirmwareOperationNodes AS NCMFirmwareOperationNodes ON NCMFirmwareOperationNodes.CoreNodeID=NCMNodeProperties.CoreNodeID
                    WHERE OperationID=@operationId
                    ORDER BY Caption";

            var dt = ExecuteSWQL(swql, new PropertyBag() { { @"operationId", operationId } });

            var firmwareOperationNodes = dt.AsEnumerable().Select(row =>
            {
                var n = UpgradeNodeOptions.Deserialize(row.Field<string>("NodeOptionsXml"));

                n.Caption = row.Field<string>("Caption");
                n.MachineType = row.Field<string>("MachineType");
                n.EngineInfo = new PollingEngineInfo
                {
                    PollingEngineName = row.Field<string>("ServerName"),
                    PollingEngineId = row.Field<int>("EngineID")
                };
               
                n.ExpandUpgradeOptions =
                    (n.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.BackupFirmwareImageCommand) !=
                     null) &&
                    (n.BackupExistingImagePaths == null || n.BackupExistingImagePaths.Length == 0);
                return n;
            }
            ).ToList();

            FirmwareUpgradeWizardController.FirmwareOperationNodes = firmwareOperationNodes;
            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error in SetSessionData", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public void CheckDeleteExisingFirmwareImageOnNodes(int[] coreNodeIds)
    {
        foreach(var coreNodeId in coreNodeIds)
        {
            var upgradeNodeOptions = FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(n => n.CoreNodeId == coreNodeId);
            if (upgradeNodeOptions != null)
            {
                upgradeNodeOptions.ExpandUpgradeOptions = true;
                upgradeNodeOptions.BackupExisingFirmwareImage = true;
                upgradeNodeOptions.DeleteExisingFirmwareImage = true;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFirmwareOperationNodesWithFreeSpaceIssues()
    {
        var dt = new DataTable("NCM_FirmwareOperationNodesFreeSpaceIssues") { Locale = CultureInfo.InvariantCulture };
        dt.Columns.Add("CoreNodeId", typeof(int));
        dt.Columns.Add("Caption", typeof(string));
        dt.Columns.Add("MinimumSpaceNeeded", typeof(double));
        dt.Columns.Add("AvailableFreeSpace", typeof(long));

        FirmwareUpgradeWizardController.FirmwareOperationNodes
                .Where(n => n.UnableToDetectFreeSpace).ToList()
                .ForEach(n => dt.Rows.Add(n.CoreNodeId, n.Caption, GetFirmwareImagesSumSizeToUpload(n), GetAvailableFreeSpace(n)));

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public string GetFirmwareOperationNodes()
    {
        if (FirmwareUpgradeWizardController.FirmwareOperationNodes != null)
        {
            return JsonConvert.SerializeObject(FirmwareUpgradeWizardController.FirmwareOperationNodes.Select(n =>
                new
                {
                    CoreNodeID = n.CoreNodeId,
                    Caption = n.Caption,
                    MachineType = n.MachineType,
                    Confirmed = n.Confirmed
                }
            ));
        }

        return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public string GetFirmwareImageDetails(int? imageId)
    {
        if (imageId.HasValue)
        {
            var dt = ExecuteSWQL(@"SELECT FileName, Size FROM NCM.FirmwareUpgradeImages WHERE ID=@id",
                new PropertyBag() { { @"id", imageId } });
            if (dt != null && dt.Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(
                    new
                    {
                        IsImageExist = true,
                        FileName = (string)dt.Rows[0]["FileName"],
                        Size = (double)dt.Rows[0]["Size"]
                    }
                );
            }
        }

        return JsonConvert.SerializeObject(
            new
            {
                IsImageExist = false,
                FileName = string.Empty,
                Size = 0
            }
        );
    }

    [WebMethod(EnableSession = true)]
    public string DeleteFirmwareOperationNode(int coreNodeId)
    {
        if (FirmwareUpgradeWizardController.FirmwareOperationNodes == null)
        {
            return JsonConvert.SerializeObject(
                new
                {
                    Success = true,
                    ConfirmedNodes = 0,
                    AllNodes = 0,
                    NodesWithFreeSpaceIssues = 0
                }
            );
        }

        var upgradeNodeOptions = FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(n =>
            n.CoreNodeId == coreNodeId);
        if (upgradeNodeOptions != null)
        {
            FirmwareUpgradeWizardController.FirmwareOperationNodes.Remove(upgradeNodeOptions);
        }

        return JsonConvert.SerializeObject(
            new
            {
                Success = true,
                ConfirmedNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.Confirmed),
                AllNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count,
                NodesWithFreeSpaceIssues = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.UnableToDetectFreeSpace)
            }
        );
    }

    [WebMethod(EnableSession = true)]
    public string GetFirmwareOperationNodeOptions(int coreNodeId)
    {
        if (FirmwareUpgradeWizardController.FirmwareOperationNodes == null)
        {
            return string.Empty;
        }

        var upgradeNodeOptions = FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(n =>
            n.CoreNodeId == coreNodeId);
        if (upgradeNodeOptions == null)
        {
            return string.Empty;
        }

        var dt = ExecuteSWQL(
            @"SELECT Log FROM NCM.FirmwareOperationNodes WHERE OperationID=@operationId AND CoreNodeID=@coreNodeId",
            new PropertyBag() { { @"operationId", upgradeNodeOptions.FirmwareOperationID }, { @"coreNodeId", coreNodeId } });
        return JsonConvert.SerializeObject(
            new
            {
                Log = (dt != null && dt.Rows.Count > 0) ? dt.Rows[0][0].ToString() : string.Empty,

                FirmwareImages = upgradeNodeOptions.Definition.FirmwareImages,

                UploadImagePath = upgradeNodeOptions.UploadImagePath,

                ExpandUpgradeOptions = upgradeNodeOptions.ExpandUpgradeOptions,

                BackupFirmwareImageCommand =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.BackupFirmwareImageCommand) !=
                    null,
                BackupExisingFirmwareImage =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.BackupFirmwareImageCommand) !=
                    null && upgradeNodeOptions.BackupExisingFirmwareImage,
                BackupExistingImagePath = EncodeStringArray(upgradeNodeOptions.BackupExistingImagePaths),

                DeleteFirmwareImageCommand =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.DeleteFirmwareImageCommand) !=
                    null,
                DeleteExisingFirmwareImage =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.DeleteFirmwareImageCommand) !=
                    null && upgradeNodeOptions.DeleteExisingFirmwareImage,
                DeleteExistingImagePath = EncodeStringArray(upgradeNodeOptions.DeleteExistingImagePaths),

                SaveConfigToNVRAM = upgradeNodeOptions.Definition.SaveConfigToNVRAM,
                BackupRunningAndStartupConfigsBeforeUpgrade =
                    upgradeNodeOptions.Definition.BackupRunningAndStartupConfigsBeforeUpgrade,
                BackupRunningAndStartupConfigsAfterUpgrade = upgradeNodeOptions.Definition.BackupRunningAndStartupConfigsAfterUpgrade,

                UpdateConfigRegisterCommand =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateConfigRegisterCommand) !=
                    null,
                UpdateConfigRegister =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateConfigRegisterCommand) !=
                    null && upgradeNodeOptions.UpdateConfigRegister,

                UpdateBootVariableCommand =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateBootVariableCommand) !=
                    null,
                UpdateBootVariable =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateBootVariableCommand) !=
                    null && upgradeNodeOptions.Definition.UpdateBootVariable,

                VerifyUploadCommand =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUploadCommand) != null,
                VerifyUploadImage =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUploadCommand) != null &&
                    upgradeNodeOptions.Definition.VerifyUploadedImageIntegrity,

                VerifyBackupCommand =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyBackupCommand) != null,
                VerifyBackupImage =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyBackupCommand) != null &&
                    upgradeNodeOptions.Definition.VerifyBackedUpImageIntegrity,

                RebootCommand = upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.RebootCommand) !=
                                null,
                Reboot = upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.RebootCommand) != null &&
                         upgradeNodeOptions.Definition.Reboot,
                Unmanage = upgradeNodeOptions.Definition.Unmanage,
                RebootRequired = upgradeNodeOptions.Definition.RebootRequired,

                VerifyUpgradeCommand =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUpgradeCommand) != null,
                VerifyUpgrade =
                    upgradeNodeOptions.Definition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUpgradeCommand) != null &&
                    upgradeNodeOptions.Definition.VerifyUpgrade,

                Script = upgradeNodeOptions.Script,

                Confirmed = upgradeNodeOptions.Confirmed,

                IsRollBack = upgradeNodeOptions.IsRollBack,

                RestoreConfigs = upgradeNodeOptions.RestoreConfigs,

                RunningConfigIDBeforeUpgrade = upgradeNodeOptions.RunningConfigIDBeforeUpgrade,

                StartupConfigIDBeforeUpgrade = upgradeNodeOptions.StartupConfigIDBeforeUpgrade,

                IsFIPSModeEnabled = IsFIPSModeEnabled(coreNodeId)
            }
        );
    }

    [WebMethod(EnableSession = true)]
    public string UpdateFirmwareOperationNodeOptions(int coreNodeId, Dictionary<string, object> nodeUpgradeOptions)
    {
        var upgradeNodeOptions = FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(n => n.CoreNodeId == coreNodeId);
        if (upgradeNodeOptions == null)
        {
            return JsonConvert.SerializeObject(
                new
                {
                    Success = false
                }
            );
        }

        upgradeNodeOptions.Definition.FirmwareImages =
            ExtractFirmwareImages(GetNodeOption<object[]>(nodeUpgradeOptions, @"FirmwareImages"));
        upgradeNodeOptions.UploadImagePath = GetNodeOption<string>(nodeUpgradeOptions, @"UploadImagePath");
        upgradeNodeOptions.ExpandUpgradeOptions = GetNodeOption<bool>(nodeUpgradeOptions, @"ExpandUpgradeOptions");
        upgradeNodeOptions.BackupExisingFirmwareImage =
            GetNodeOption<bool>(nodeUpgradeOptions, @"BackupExisingFirmwareImage");
        if (upgradeNodeOptions.BackupExisingFirmwareImage && nodeUpgradeOptions.ContainsKey(@"BackupExistingImagePath"))
        {
            upgradeNodeOptions.BackupExistingImagePaths =
                SplitEncodedStringArray(
                    GetNodeOption<string>(nodeUpgradeOptions, @"BackupExistingImagePath"));
        }

        upgradeNodeOptions.DeleteExisingFirmwareImage =
            GetNodeOption<bool>(nodeUpgradeOptions, @"DeleteExisingFirmwareImage");
        if (upgradeNodeOptions.DeleteExisingFirmwareImage && nodeUpgradeOptions.ContainsKey(@"DeleteExistingImagePath"))

        {
            upgradeNodeOptions.DeleteExistingImagePaths =
                SplitEncodedStringArray(
                    GetNodeOption<string>(nodeUpgradeOptions, @"DeleteExistingImagePath"));
        }

        upgradeNodeOptions.UpdateConfigRegister = GetNodeOption<bool>(nodeUpgradeOptions, @"UpdateConfigRegister");
        upgradeNodeOptions.Definition.SaveConfigToNVRAM = GetNodeOption<bool>(nodeUpgradeOptions, @"SaveConfigToNVRAM");
        upgradeNodeOptions.Definition.BackupRunningAndStartupConfigsBeforeUpgrade =
            GetNodeOption<bool>(nodeUpgradeOptions, @"BackupRunningAndStartupConfigsBeforeUpgrade");
        upgradeNodeOptions.Definition.BackupRunningAndStartupConfigsAfterUpgrade = GetNodeOption<bool>(nodeUpgradeOptions,
            @"BackupRunningAndStartupConfigsAfterUpgrade");
        upgradeNodeOptions.Definition.UpdateBootVariable = GetNodeOption<bool>(nodeUpgradeOptions, @"UpdateBootVariable");
        upgradeNodeOptions.Definition.VerifyUploadedImageIntegrity =
            GetNodeOption<bool>(nodeUpgradeOptions, @"VerifyUploadImage");
        upgradeNodeOptions.Definition.VerifyBackedUpImageIntegrity =
            upgradeNodeOptions.BackupExisingFirmwareImage && GetNodeOption<bool>(nodeUpgradeOptions, @"VerifyBackupImage");
        upgradeNodeOptions.Definition.Reboot = GetNodeOption<bool>(nodeUpgradeOptions, @"Reboot");
        upgradeNodeOptions.Definition.Unmanage = GetNodeOption<bool>(nodeUpgradeOptions, @"Unmanage");
        upgradeNodeOptions.Script = GetNodeOption<string>(nodeUpgradeOptions, @"Script");
        upgradeNodeOptions.RestoreConfigs = GetNodeOption<bool>(nodeUpgradeOptions, @"RestoreConfigs");
        upgradeNodeOptions.Definition.VerifyUpgrade = GetNodeOption<bool>(nodeUpgradeOptions, @"VerifyUpgrade");

        return JsonConvert.SerializeObject(
            new
            {
                Success = true
            }
        );
    }

    [WebMethod(EnableSession = true)]
    public string ConfirmFirmwareOperationNode(int coreNodeId, bool confirmed)
    {
        var upgradeNodeOptions =
            FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(n => n.CoreNodeId == coreNodeId);
        if (upgradeNodeOptions != null)
        {
            upgradeNodeOptions.Confirmed = confirmed;
        }

        return JsonConvert.SerializeObject(
            new
            {
                Success = true,
                ConfirmedNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.Confirmed),
                AllNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count,
                NodesWithFreeSpaceIssues = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.UnableToDetectFreeSpace)
            }
        );
    }

    [WebMethod(EnableSession = true)]
    public string ConfirmFirmwareOperationNodes(List<int> coreNodeIds)
    {
        var confirmedNodeOptionsList = new List<UpgradeNodeOptions>();
        foreach (var coreNodeId in coreNodeIds)
        {
            var upgradeNodeOptions =
                FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(n => n.CoreNodeId == coreNodeId);
            if (upgradeNodeOptions != null)
            {
                upgradeNodeOptions.Confirmed = ValidateNodeOptions(upgradeNodeOptions);
                confirmedNodeOptionsList.Add(upgradeNodeOptions);
            }
        }

        return JsonConvert.SerializeObject(
            new
            {
                Success = true,
                ConfirmedNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.Confirmed),
                AllNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count,
                NodesWithFreeSpaceIssues = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.UnableToDetectFreeSpace),
                Nodes = confirmedNodeOptionsList.Select(n =>
                    new
                    {
                        CoreNodeID = n.CoreNodeId,
                        Confirmed = n.Confirmed
                    })
            });
    }

    [WebMethod(EnableSession = true)]
    public void SaveFirmwareOperationNodesOrder(Dictionary<string, string> orderedList)
    {
        if (orderedList != null)
        {
            FirmwareUpgradeWizardController.FirmwareOperationNodes.ForEach(n =>
                n.Order = Convert.ToInt32(orderedList[n.CoreNodeId.ToString(CultureInfo.InvariantCulture)],
                    CultureInfo.InvariantCulture));
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable LoadFirmwareOperationNodesOrder()
    {
        var orderedList = FirmwareUpgradeWizardController.FirmwareOperationNodes.OrderBy(x => x.Order);

        var dt = new DataTable("NCM_FirmwareOperationNodes") { Locale = CultureInfo.InvariantCulture };
        dt.Columns.Add("CoreNodeID", typeof(int));
        dt.Columns.Add("Caption", typeof(string));
        dt.Columns.Add("ImageFileName", typeof(string));
        dt.Columns.Add("Reboot", typeof(bool));
        dt.Columns.Add("EngineName", typeof(string));

        foreach (var node in orderedList)
        {
            var imageFileNames = GetFirmwareImageFileNames(node.Definition.FirmwareImages);

            dt.Rows.Add(
                node.CoreNodeId,
                node.Caption,
                imageFileNames != null && imageFileNames.Any() ? string.Join(@", ", imageFileNames) : $@"<i>{Resources.NCMWebContent.WEBDATA_VK_1041}</i>",
                node.Definition.Reboot, node.EngineInfo.PollingEngineName);
        }

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable LoadPollersFreeSpaceData()
    {
        var poolingEngines = FirmwareUpgradeWizardController.FirmwareOperationNodes
            .GroupBy(p => p.EngineInfo.PollingEngineId)
            .Select(g => g.First())
            .ToList();

        var commandProtocol =
            FirmwareUpgradeWizardController.FirmwareOperationNodes.First().Definition.TransferProtocol;

        var dt = new DataTable("NCM_PoolingEngines") { Locale = CultureInfo.InvariantCulture };
        dt.Columns.Add("EngineName", typeof(string));
        dt.Columns.Add("FreeSpace", typeof(long));
       
        foreach (var engine in poolingEngines)
        {
            var freeSpace = GetFreeSpaceFromEngine(engine.EngineInfo.PollingEngineId, commandProtocol);
            dt.Rows.Add(engine.EngineInfo.PollingEngineName, freeSpace);
        }
        return new PageableDataTable(dt, dt.Rows.Count);
    }

    private long GetFreeSpaceFromEngine(int engineId, eCommandProtocol commandProtocol)
    {
        try
        {
            var proxyHelper = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
            return proxyHelper.CallEngine(p => p.GetFreeSpaceInServerRoot(commandProtocol), engineId);
        }
        catch (Exception e)
        {
            log.Error($"NCMFirmwareUpgrade: Unable to retreive engine free space from polling engine {engineId}",e);
            return -1;
        }

    }


    [WebMethod(EnableSession = true)]
    public bool HasActiveOperationByIds(int[] selectedIds)
    {
        var swql = $@"
                SELECT
                    ID
                FROM NCM.FirmwareOperations
                WHERE (Status={(int)eFirmwareUpgradeStatus.CollectingData} OR Status={
                (int)eFirmwareUpgradeStatus.Upgrading
            })";

        if (selectedIds != null)
        {
            var inStatement = $@" AND ID IN ({string.Join(@",", selectedIds.Select(x => $@"'{x}'"))})";
            swql += inStatement;
        }

        var dt = ExecuteSWQL(swql);
        return dt != null && dt.Rows.Count > 0;
    }

    [WebMethod(EnableSession = true)]
    public bool RollbackFirmwareOperation(int operationId)
    {
        try
        {
            if (CommonHelper.IsDemoMode())
            {
                return true;
            }

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                return proxy.Cirrus.FirmwareOperations.PrepareRollBack(operationId) != -1;
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.RollbackFirmwareOperation", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool ReExecuteFirmwareOperation(int operationId)
    {
        try
        {
            if (CommonHelper.IsDemoMode())
            {
                return true;
            }

            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                return proxy.Cirrus.FirmwareOperations.PrepareReExecuteFailed(operationId) != -1;
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.ReExecuteFirmwareOperation", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public string ValidateFreeSpace(int coreNodeId, string uploadImagePath, object[] firmwareImages, bool deleteExisingFirmwareImage,
        string deleteExistingImagePath)
    {
        try
        {
            var nodeOpts =
                FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault(node =>
                    node.CoreNodeId == coreNodeId);

            if (nodeOpts != null)
            {
                nodeOpts.Definition.FirmwareImages = ExtractFirmwareImages(firmwareImages);
                nodeOpts.UploadImagePath = uploadImagePath;
                nodeOpts.DeleteExisingFirmwareImage = deleteExisingFirmwareImage;
                nodeOpts.DeleteExistingImagePaths = SplitEncodedStringArray(deleteExistingImagePath);
                var firmwareImagesSumSizeToUpload = firmwareImages.Select(image =>
                    GetNodeOption<double>((Dictionary<string, object>)image, @"ImageSize")).ToList().Sum();
                var status = FirmwareFreeSpaceDetector.ValidateFreeSpace(nodeOpts, firmwareImagesSumSizeToUpload);
                nodeOpts.UnableToDetectFreeSpace = status == FreeSpaceValidationStatus.Unknown || status == FreeSpaceValidationStatus.Warning;
                if (status == FreeSpaceValidationStatus.Fail && nodeOpts.Confirmed)
                {
                    nodeOpts.Confirmed = false;
                }
                return CreateValidateFreeSpaceResults(status);
            }
            return CreateValidateFreeSpaceResults(FreeSpaceValidationStatus.Unknown);
        }
        catch (Exception ex)
        {
            log.Error("Error in FirmwareOperations.ValidateFreeSpace", ex);
            return CreateValidateFreeSpaceResults(FreeSpaceValidationStatus.Unknown);
        }
    }

    private string CreateValidateFreeSpaceResults(FreeSpaceValidationStatus status)
    {
        return JsonConvert.SerializeObject(
            new
            {
                Status = status,
                ConfirmedNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.Confirmed),
                AllNodes = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count,
                NodesWithFreeSpaceIssues = FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.UnableToDetectFreeSpace)
            });
    }

    private bool ValidateNodeOptions(UpgradeNodeOptions upgradeNodeOptions)
    {
        if (upgradeNodeOptions.Definition.HasUploadImagePath() &&
            string.IsNullOrWhiteSpace(upgradeNodeOptions.UploadImagePath))
        {
            return false;
        }

        if (upgradeNodeOptions.BackupExisingFirmwareImage &&
            string.IsNullOrEmpty(EncodeStringArray(upgradeNodeOptions.BackupExistingImagePaths)))
        {
            return false;
        }

        if (upgradeNodeOptions.DeleteExisingFirmwareImage &&
            string.IsNullOrEmpty(EncodeStringArray(upgradeNodeOptions.DeleteExistingImagePaths)))
        {
            return false;
        }

        object[] firmwareImages;
        if (!TryGetFirmwareImages(upgradeNodeOptions, out firmwareImages))
        {
            return false;
        }

        if (upgradeNodeOptions.Definition.HasUploadImagePath())
        {
            var firmwareImagesSumSizeToUpload = firmwareImages.Select(image =>
                GetNodeOption<double>((Dictionary<string, object>) image, @"ImageSize")).ToList().Sum();
            var status = FirmwareFreeSpaceDetector.ValidateFreeSpace(upgradeNodeOptions, firmwareImagesSumSizeToUpload);
            upgradeNodeOptions.UnableToDetectFreeSpace = status == FreeSpaceValidationStatus.Unknown || status == FreeSpaceValidationStatus.Warning;
            return status != FreeSpaceValidationStatus.Fail;
        }

        return true;
    }

    private long GetAvailableFreeSpace(UpgradeNodeOptions nodeOpts)
    {
        FlashDevice flashDevice;
        if (FirmwareFreeSpaceDetector.TryGetFlashDevice(nodeOpts, out flashDevice))
        {
            return flashDevice.FreeSpaceInBytes;
        }
        return 0;
    }

    private double GetFirmwareImagesSumSizeToUpload(UpgradeNodeOptions upgradeNodeOptions)
    {
        object[] firmwareImages;
        if (!TryGetFirmwareImages(upgradeNodeOptions, out firmwareImages))
        {
            return 0;
        }

        return upgradeNodeOptions.Definition.HasUploadImagePath() ?
             firmwareImages.Select(image =>
                GetNodeOption<double>((Dictionary<string, object>)image, @"ImageSize")).ToList().Sum() : 0;
    }

    private bool TryGetFirmwareImages(UpgradeNodeOptions upgradeNodeOptions, out object[] firmwareImages)
    {
        var imageIds = upgradeNodeOptions.Definition.FirmwareImages.Where(x => x.ImageId.HasValue).Select(x => x.ImageId.Value);
        var inClause = imageIds.Any() ? string.Join(@",", imageIds) : @"''";
        var dt = ExecuteSWQL(
            $@"SELECT ID, Size FROM NCM.FirmwareUpgradeImages WHERE ID IN ({ inClause })");
        if (dt != null && dt.Rows.Count == upgradeNodeOptions.Definition.FirmwareImages.Count)
        {
            firmwareImages = dt.AsEnumerable().Select(r =>
            {
                return new Dictionary<string, object>
                        {
                        {@"ID", (int) r["ID"]},
                        {@"ImageSize", (double) r["Size"]}
                        };
            }).ToArray();
            return true;
        }

        firmwareImages = null;
        return false;
    }

    private string[] SplitEncodedStringArray(string encodedArray)
    {
        return encodedArray.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
    }

    private string EncodeStringArray(string[] paths)
    {
        if (paths == null || paths.Length == 0)
        {
            return string.Empty;
        }
        return string.Join(@";", paths);
    }

    private List<string> GetFirmwareImageFileNames(List<FirmwareImage> firmwareImages)
    {
        var ids = firmwareImages.Where(image => image.ImageId.HasValue).Select(i => i.ImageId.Value);

        var dt = ExecuteSWQL(
            $@"SELECT ID, FileName FROM NCM.FirmwareUpgradeImages WHERE ID IN ({string.Join(@",", ids)})");
        if (dt == null || dt.Rows.Count <= 0)
        {
            return null;
        }

        var d = dt.AsEnumerable().ToDictionary(row => row.Field<int>("ID"), row => row.Field<string>("FileName"));

        var imageFileNames = new List<string>();
        foreach (var firmwareImage in firmwareImages)
        {
            string imageFileName;
            if (firmwareImage.ImageId.HasValue && d.TryGetValue(firmwareImage.ImageId.Value, out imageFileName))
            {
                imageFileNames.Add($@"{firmwareImage.ImageType}: {imageFileName}");
            }
            else
            {
                imageFileNames.Add(Resources.NCMWebContent.WEBDATA_VK_1041);
            }
        }
        return imageFileNames;
    }

    private T GetNodeOption<T>(Dictionary<string, object> d, string key)
    {
        object o;
        if (d.TryGetValue(key, out o))
            return (T)Convert.ChangeType(o, typeof(T));
        return default(T);
    }

    private List<FirmwareImage> ExtractFirmwareImages(object[] images)
    {
        return images.Select(image =>
        {
            var imageType = GetNodeOption<string>((Dictionary<string, object>)image, @"ImageType");
            var imageId = GetNodeOption<string>((Dictionary<string, object>)image, @"ImageId");
            return new FirmwareImage()
            {
                ImageType = imageType,
                ImageId = !string.IsNullOrEmpty(imageId) ? int.Parse(imageId) : (int?)null
            };
        }).ToList();
    }

    private int[] GetOperationIds(eFirmwareUpgradeStatus[] statusList = null)
    {
        var swql = $@"
        SELECT
            ID
        FROM NCM.FirmwareOperations {(statusList == null || statusList.Length == 0 ? string.Empty : $@"WHERE Status IN ({string.Join(@",", Array.ConvertAll(statusList, x => (int)x))})")}";

        var dt = ExecuteSWQL(swql);
        if (dt != null && dt.Rows.Count > 0)
            return dt.AsEnumerable().Select(r => r.Field<int>("ID")).ToArray();
        return null;
    }

    private DataTable ExecuteSWQL(string swql, PropertyBag properties = null)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swql, properties);
            return dt;
        }
    }

    private long GetRowCount(string value, string search)
    {
        var swqlFormat = $@"
        SELECT 
            COUNT(Name) AS theCount 
        FROM NCM.FirmwareOperationsView
        WHERE {GetWhereClause(value)} AND {GetSearchClause(search)}";

        var rowCountTable = ExecuteSWQL(swqlFormat);

        if (rowCountTable != null && rowCountTable.Rows.Count > 0)
            return Convert.ToInt64(rowCountTable.Rows[0][0]);

        return 0;
    }

    private string PageableQuery(int pageSize, int startRowNumber, string orderByColumn, string orderbyDirection, string value, string search)
    {
        const string swqlFormat = @"
        SELECT 
            ID, 
            Name, 
            CreationDate, 
            RunAt, 
            Status,
            ErrorMessage,
            CompletedOperations,
            AllOperations,
            CASE WHEN Log IS NOT NULL THEN '' ELSE NULL END AS Log,
            UserName
        FROM NCM.FirmwareOperationsView
        WHERE {0} AND {1}
	    ORDER BY {2} {3}
        WITH ROWS {4} TO {5}";

        return string.Format(swqlFormat, GetWhereClause(value), GetSearchClause(search), orderByColumn, orderbyDirection, startRowNumber + 1, startRowNumber + pageSize);
    }

    private string GetWhereClause(string value)
    {
        if (string.IsNullOrEmpty(value))
            return @"(1=1)";

        return value.Equals(@"-1") ? @"(1=1)" : $@"Status={value}";
    }

    private string GetSearchClause(string search)
    {
        if (string.IsNullOrEmpty(search))
            return @"(1=1)";

        var searchValue = SearchHelper.WildcardToLike(search);
        var searchClause = $@"Name LIKE '%{searchValue}%'";
        return searchClause;
    }

    private bool IsFIPSModeEnabled(int coreNodeId)
    {
        var swql = $@"
        SELECT 
            FIPSModeEnabled
        FROM NCM.NodeProperties AS NcmNodeProperties
        INNER JOIN Orion.Nodes AS OrionNodes ON NcmNodeProperties.CoreNodeID=OrionNodes.NodeID
        INNER JOIN Orion.Engines AS OrionEngines ON OrionNodes.EngineID=OrionEngines.EngineID
        WHERE NcmNodeProperties.CoreNodeID=@coreNodeId";

        var dt = ExecuteSWQL(swql, new PropertyBag() { { @"coreNodeId", coreNodeId } });

        if (dt != null && dt.Rows.Count > 0)
        {
            return Convert.ToBoolean(dt.Rows[0][0], CultureInfo.InvariantCulture);
        }

        return false;
    }
}
