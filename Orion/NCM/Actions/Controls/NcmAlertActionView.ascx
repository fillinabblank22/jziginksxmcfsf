﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NcmAlertActionView.ascx.cs" Inherits="Orion_NCM_Actions_Controls_NcmAlertActionView" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>
<%@ Import Namespace="SolarWinds.NCM.Contracts.Actions" %>

<orion:Include runat="server" File="NCM/Actions/js/NcmAlertActionController.js" />

<style type="text/css">
    .ncmAlertAction 
    {
        width: 555px;
    }
    
    .ncmAlertAction .ActionType td 
    {
        line-height: 20px;
        height: 20px;
    }
    
    .ncmAlertAction .ComparisonType td 
    {
        line-height: 20px;
        height: 20px;
    }
    
    .ncmAlertAction .header 
    {
        padding: 10px 0px 2px 0px;
    }
    
    .ncmAlertAction .hint 
    {
        color: #808080;
        font-style: italic;
    }
</style>

<div runat="server" id="container" class="ncmAlertAction">
    <h3><%=Resources.NCMWebContent.WEBDATA_VK_982 %></h3>
    <div id="ncmAlertActionSection" class="section required">
        <div class="header">
            <asp:CheckBox ID="ActionTypeEditEnabled" runat="server" data-edited="<%$ Code: NcmAlertActionConstants.NcmActionType %>"/>
            <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_953 %>"/>
        </div>
        <asp:RadioButtonList ID="ActionType" runat="server" CssClass="ActionType">
            <asp:ListItem Value="Download" Selected="True" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_954 %>" onclick="ActionTypeOnClick(this);"></asp:ListItem>
            <asp:ListItem Value="ExecuteScript" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_955 %>" onclick="ActionTypeOnClick(this);"></asp:ListItem>
            <asp:ListItem Value="ConfigChangeReport" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_956 %>" onclick="ActionTypeOnClick(this);"></asp:ListItem>
        </asp:RadioButtonList>

        <div class="header">
            <asp:CheckBox ID="UsernameEditEnabled" runat="server" data-edited="<%$ Code: NcmAlertActionConstants.NcmUsername %>"/>
            <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_957 %>" AssociatedControlID="Username"/>
        </div>
        <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_958 %>" AssociatedControlID="Username"/>&nbsp;<asp:TextBox ID="Username" runat="server" CssClass="Username" Width="200px" data-form="<%$ Code: NcmAlertActionConstants.NcmUsername %>" />

        <div id="ScriptBox" runat="server" style="padding-top:50px;">
            <div class="header">
                <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_959 %>" AssociatedControlID="Script"/>
            </div>
            <asp:TextBox ID="Script" runat="server" CssClass="Script" TextMode="MultiLine" Height="100px" Width="500px" data-form="<%$ Code: NcmAlertActionConstants.NcmScript %>"/>
        </div>

        <div id="ComparisonTypeBox" runat="server" style="padding-top:50px;">
            <div class="header">
                <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_960 %>"/>
            </div>
            <asp:RadioButtonList ID="ComparisonType" runat="server" CssClass="ComparisonType">
                <asp:ListItem Value="3" Selected="True" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_961 %>"></asp:ListItem>
                <asp:ListItem Value="2" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_962 %>"></asp:ListItem>
            </asp:RadioButtonList>
        </div>

        <div class="header">
            <asp:CheckBox ID="ExecuteOnDifferentNodeEditEnabled" runat="server" data-edited="<%$ Code: NcmAlertActionConstants.NcmExecuteOnDifferentNode %>"/>
            <asp:CheckBox ID="ExecuteOnDifferentNode" runat="server" Text="<%$ Resources: NCMWebContent, NCMAlertAction_EnterIPAddressCheckboxText %>" onclick="ExecuteOnDifferentNodeCheck(this);" data-form="<%$ Code: NcmAlertActionConstants.NcmExecuteOnDifferentNode %>" />
        </div>
        <div id="IPAddressBox" runat="server">
            <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, NCMAlertAction_IPAddressLabel %>" AssociatedControlID="IPAddress"/>&nbsp;<asp:TextBox ID="IPAddress" runat="server" CssClass="Username" Width="200px" data-form="<%$ Code: NcmAlertActionConstants.NcmIPAddress %>" />
            <orion:LocalizableButton ID="btnInsertVariable" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Small" OnClientClick="return false" data-macro="<%$ Code: NcmAlertActionConstants.NcmIPAddress %>" runat="server"/>
            <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" DisplayType="Small"  Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" CausesValidation="false" OnClientClick="return false" validate-ip-address="<%$ Code: NcmAlertActionConstants.NcmIPAddress %>" />
        </div>

        <div class="hint" style="padding-top:20px;">
            <%=Resources.NCMWebContent.WEBDATA_VK_963 %>
        </div>
    </div>
</div>

<script type="text/javascript">
    // NCM Alert Action Types
    var ncmAlertActionTypeBackup = 'Download';
    var ncmAlertActionTypeExecute = 'ExecuteScript';
    var ncmAlertActionTypeShowChanges = 'ConfigChangeReport';

    // Self executing function creates local scope to avoid globals
    (function() {
        var controller = new SW.NCM.Actions.NcmAlertActionController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition) %>,
            viewContext :  <%= ToJson(ViewContext) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping : {
                actiontype : "<%= NcmAlertActionConstants.NcmActionType %>",
                username : "<%= NcmAlertActionConstants.NcmUsername %>",
                script : "<%= NcmAlertActionConstants.NcmScript %>",
                comparisontype : "<%= NcmAlertActionConstants.NcmConfigChangeComparisonType %>",
                executeondifferentnode : "<%= NcmAlertActionConstants.NcmExecuteOnDifferentNode %>"
            }
        });
        controller.init();
        controller.bind();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();

    function ExecuteOnDifferentNodeCheck(that) {
        if(that.checked) {
            $('#<%= IPAddressBox.ClientID %>').show();
        }
        else {
            $('#<%= IPAddressBox.ClientID %>').hide();
        }
    }

    function ActionTypeOnClick(that) {
        switch (that.value) {
            case ncmAlertActionTypeBackup:
                $('#<%= ScriptBox.ClientID %>').hide();
                $('#<%= ComparisonTypeBox.ClientID %>').hide();
                break;
            case ncmAlertActionTypeExecute:
                $('#<%= ScriptBox.ClientID %>').show();
                $('#<%= ComparisonTypeBox.ClientID %>').hide();
                break;
            case ncmAlertActionTypeShowChanges:
                $('#<%= ScriptBox.ClientID %>').hide();
                $('#<%= ComparisonTypeBox.ClientID %>').show();
                break;
            default:
                $('#<%= ScriptBox.ClientID %>').hide();
                $('#<%= ComparisonTypeBox.ClientID %>').hide();
                break;
        }
    }
</script>