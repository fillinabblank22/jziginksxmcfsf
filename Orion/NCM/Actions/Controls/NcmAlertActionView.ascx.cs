﻿using System;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.NCM.Contracts.Actions;

public partial class Orion_NCM_Actions_Controls_NcmAlertActionView : ActionPluginBaseView
{
    public override string ActionTypeID
    {
        get { return NcmAlertActionConstants.ActionTypeId; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ApplyMultiEditMode();

        if (ActionDefinition != null)
        {
            BindConfiguration();
        }
        else
        {
            SetDefaultValues();
        }

        SetVisibility(ActionType.SelectedValue);
    }

    private void ApplyMultiEditMode()
    {
        ActionTypeEditEnabled.Visible = MultiEditEnabled;
        UsernameEditEnabled.Visible = MultiEditEnabled;
        ExecuteOnDifferentNodeEditEnabled.Visible = MultiEditEnabled;
    }

    private void BindConfiguration()
    {
        string actionType = ActionDefinition.Properties.GetPropertyValue(NcmAlertActionConstants.NcmActionType) ?? @"Download";
        ActionType.SelectedValue = actionType;
        Username.Text = ActionDefinition.Properties.GetPropertyValue(NcmAlertActionConstants.NcmUsername);

        Script.Text = ActionDefinition.Properties.GetPropertyValue(NcmAlertActionConstants.NcmScript);

        string comparisonType = ActionDefinition.Properties.GetPropertyValue(NcmAlertActionConstants.NcmConfigChangeComparisonType) ?? @"3";
        ComparisonType.SelectedValue = comparisonType;

        bool executeOnDifferentNode = bool.Parse(ActionDefinition.Properties.GetPropertyValue(NcmAlertActionConstants.NcmExecuteOnDifferentNode) ?? @"false");
        ExecuteOnDifferentNode.Checked = executeOnDifferentNode;
        
        IPAddress.Text = ActionDefinition.Properties.GetPropertyValue(NcmAlertActionConstants.NcmIPAddress);
        IPAddressBox.Style.Add(@"display", executeOnDifferentNode ? @"block" : @"none");
    }

    private void SetDefaultValues()
    {
        string actionType = @"Download";
        ActionType.SelectedValue = actionType;

        ExecuteOnDifferentNode.Checked = false;
        IPAddressBox.Style.Add(@"display", @"none");
    }

    private void SetVisibility(string actionType)
    {
        switch (actionType)
        {
            case "Download":
                ScriptBox.Style.Add(@"display", @"none");
                ComparisonTypeBox.Style.Add(@"display", @"none");
                break;
            case "ExecuteScript":
                ScriptBox.Style.Add(@"display", @"block");
                ComparisonTypeBox.Style.Add(@"display", @"none");
                break;
            case "ConfigChangeReport":
                ScriptBox.Style.Add(@"display", @"none");
                ComparisonTypeBox.Style.Add(@"display", @"block");
                break;
            default:
                ScriptBox.Style.Add(@"display", @"none");
                ComparisonTypeBox.Style.Add(@"display", @"none");
                break;
        }
    }
}