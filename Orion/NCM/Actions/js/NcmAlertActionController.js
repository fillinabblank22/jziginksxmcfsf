﻿SW.Core.namespace("SW.NCM.Actions").NcmAlertActionController = function (config) {
    "use strict";

    var getActionType = function () {
        return $container.find(".ActionType :checked").val();
    };

    var getUsername = function () {
        return $container.find('[data-form="' + config.dataFormMapping.username + '"]').val();
    };

    var getUsernameElement = function () {
        return $container.find('[data-form="' + config.dataFormMapping.username + '"]');
    };

    var getScript = function () {
        return $container.find('[data-form="' + config.dataFormMapping.script + '"]').val();
    };

    var getScriptElement = function () {
        return $container.find('[data-form="' + config.dataFormMapping.script + '"]');
    };

    var getComparisonType = function () {
        return $container.find(".ComparisonType :checked").val();
    };

    var getExecuteOnDifferentNode = function () {
        return $container.find('[data-form="' + config.dataFormMapping.executeondifferentnode + '"] input:checked').length == 1;
    }

    var getIPAddress = function () {
        return $container.find('[data-form="NCM_IPAddress"]').val();
    };

    var getIPAddressElement = function () {
        return $container.find('[data-form="NCM_IPAddress"]');
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var createConfiguration = function () {
        var configuration = {
            ActionType: getActionType(),
            Username: getUsername(),
            Script: getScript(),
            ConfigChangeComparisonType: getComparisonType(),
            ExecuteOnDifferentNode: getExecuteOnDifferentNode(),
            IPAddress: getIPAddress()
        };

        return configuration;
    };

    var validateNcmAlertActionSection = function () {
        var isValid = true;

        if (isEditable(config.dataFormMapping.username)) {
            var username = getUsername();
            var usernameElement = getUsernameElement();

            if (username.trim().length == 0) {
                usernameElement.addClass('invalidValue');
                isValid = isValid && false;
            } else {
                usernameElement.removeClass('invalidValue');
                isValid = isValid && true;
            }
        }

        if (getActionType() == ncmAlertActionTypeExecute) {
            var script = getScript();
            var scriptElement = getScriptElement();
            if (script.trim().length == 0) {
                scriptElement.addClass('invalidValue');
                isValid = isValid && false;
            } else {
                scriptElement.removeClass('invalidValue');
                isValid = isValid && true;
            }
        }

        if (isEditable(config.dataFormMapping.executeondifferentnode)) {
            var isExecuteOnDifferentNode = getExecuteOnDifferentNode();
            if (isExecuteOnDifferentNode) {
                if (validateIPAddress()) {
                    isValid = isValid && true;
                }
                else {
                    isValid = isValid && false;
                }
            }
        }

        return isValid;
    };

    var validateIPAddress = function () {
        var isValid = false;

        var ipAddress = getIPAddress();
        var ipAddressElement = getIPAddressElement();
        if (ipAddress.trim().length == 0) {
            ipAddressElement.addClass('invalidValue');
            isValid = false;
        }
        else {
            SW.Core.Services.callControllerSync("/api/NcmAlertAction/ValidateIPAddress", { IPAddress: ipAddress }, function (result) {
                if (result) {
                    ipAddressElement.removeClass('invalidValue');
                    isValid = true;
                } else {
                    ipAddressElement.addClass('invalidValue');
                    isValid = false;
                }
            }, function () {
                ipAddressElement.addClass('invalidValue');
                isValid = false;
            });
        }
        return isValid;
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/NcmAlertAction/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    this.bind = function () {
        $('[validate-ip-address]').on('click', validateIPAddress);
    };

    this.init = function () {
        $container = $('#' + config.containerID);

        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                if (dataform == config.dataFormMapping.actiontype) {
                    $container.find(".ActionType input").attr("disabled", true);
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        $container.find(".ActionType input").attr("disabled", !$(this).find("input").is(':checked'));
                    });
                }
                if (dataform == config.dataFormMapping.username) {
                    $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                    });
                }
                if (dataform == config.dataFormMapping.executeondifferentnode) {
                    $container.find('[data-form="' + dataform + '"] input').attr("disabled", true);
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        $container.find('[data-form="' + dataedited + '"] input').attr("disabled", !$(this).find("input").is(':checked'));
                        $container.find('[data-form="NCM_IPAddress"]').attr("disabled", !$(this).find("input").is(':checked'));
                    });
                }
            }
        }

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Validate action configuration
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = true;

        if (sectionID === 'ncmAlertActionSection') {
            valid = validateNcmAlertActionSection();
        }

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();

        if ($container.find('[data-edited="' + config.dataFormMapping.actiontype + '"] input:checked').length == 1) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.actiontype, PropertyValue: getActionType() });
            jsonProperties.push({ PropertyName: config.dataFormMapping.script, PropertyValue: getScript() });
            jsonProperties.push({ PropertyName: config.dataFormMapping.comparisontype, PropertyValue: getComparisonType() });
        }

        if ($container.find('[data-edited="' + config.dataFormMapping.username + '"] input:checked').length == 1) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.username, PropertyValue: getUsername() });
        }

        if ($container.find('[data-edited="' + config.dataFormMapping.executeondifferentnode + '"] input:checked').length == 1) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.executeondifferentnode, PropertyValue: getExecuteOnDifferentNode() });
            jsonProperties.push({ PropertyName: "NCM_IPAddress", PropertyValue: getIPAddress() });
        }

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();
        if (config.actionDefinition) {
            // Edit mode
            // We have action definition we just want to updated existing one
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {
            // New action definition mode
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    // Constructor
    var self = this;
    var $container = null;
}