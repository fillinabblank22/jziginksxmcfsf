﻿using System;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NCM_TransferStatus : System.Web.UI.Page
{
    private readonly ICirrusEntityHelper cirrusEntityHelper = new CirrusEntityHelper();

    protected string CloseWindowHandlerUrl
    {
        get
        {
            return UrlHelper.ToSafeUrlParameter(@"/Orion/NCM/CloseWindowHandler.aspx");
        }
    }

    protected string NCMAccountRole
    {
        get { return SecurityHelper.GetNCMAccountRole(); }
    }

    protected bool AllowNodeManagement
    {
        get { return Profile.AllowNodeManagement; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_IA_15;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);
    }

    protected override void OnInit(EventArgs e)
    {
        if (!(SecurityHelper.IsPermissionExist(SecurityHelper._canDownload) || cirrusEntityHelper.IsDemoMode()))
            Server.Transfer($@"~/Orion/Error.aspx?Message={Server.HtmlEncode(Resources.NCMWebContent.WEBDATA_IC_173)}");
    }
}