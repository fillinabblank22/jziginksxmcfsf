﻿using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Common;
using System;

public partial class Orion_NCM_Firmware_FirmwareOperations : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && !SecurityHelper.IsPermissionExist(SecurityHelper._canUpload))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.FirmwareOperations_InsufficientPermissionsMessage}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.FirmwareUpgradeOperations_PageTitle;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);
    }
}