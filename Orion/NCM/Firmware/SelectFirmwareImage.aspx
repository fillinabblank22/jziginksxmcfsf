﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="SelectFirmwareImage.aspx.cs" Inherits="Orion_NCM_Firmware_SelectFirmwareImage" %>

<%@ Register Src="~/Orion/NCM/Firmware/Controls/FirmwareUpgradeWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Firmware/Controls/SelectFirmwareImages.ascx" TagPrefix="ncm" TagName="SelectFirmwareImages" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div>
                <table cellpadding="0" cellspacing="0" width="80%">
                    <tr>
                        <td style="white-space:nowrap;width:15%;">
                            <%= Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImagesLabel %>
                        </td>
                        <td style="padding-left:10px;white-space:nowrap;width:85%;">
                            <ncm:SelectFirmwareImages runat="server" ID="ncmSelectFirmwareImages" EnableValidateFirmwareImage="True" ReadOnly="True" />
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>