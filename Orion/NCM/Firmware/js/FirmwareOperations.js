﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.FirmwareOperations = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var pagingToolbar;

    ORION.prefix = "NCM_FirmwareOperations_";

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight();
            grid.setHeight(h);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 75;
    }

    var getDateTimeOffset = function () {
        var date = new Date();
        var offset = date.getTimezoneOffset();
        return offset;
    };
    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var items = grid.getSelectionModel().getSelections();
        var needsReviewOperationIds = getNeedsReviewOperationIds(items);
        var needsReviewOperationSelected = (selCount != 1) || (needsReviewOperationIds.length != 1);

        map.ReviewButton.setDisabled(needsReviewOperationSelected);
        var activeOperationIds = getActiveOperationIds(items);
        map.Rollback.setDisabled(selCount != 1 || activeOperationIds.length != 0);

        var needReExecuteOperations = getNeedsReExecuteOperationIds(items);
        map.ReExecute.setDisabled(selCount != 1 || needReExecuteOperations.length == 0);

        if (selCount > 0) {
            map.CancelAll.setDisabled(activeOperationIds.length == 0);
        }
        map.ClearComplete.setDisabled(selCount > 0);
        map.ClearFailed.setDisabled(selCount > 0);

        map.CancelAll.setText(selCount > 0 ? '@{R=NCM.Strings;K=WEBJS_VK_87;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_86;E=js}');
        map.CancelAll.setTooltip(selCount > 0 ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelSelectedButtonTooltip;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelAllButtonTooltip;E=js}');
        map.ClearAll.setText(selCount > 0 ? '@{R=NCM.Strings;K=WEBJS_VK_326;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_94;E=js}');
        map.ClearAll.setTooltip(selCount > 0 ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearSelectedButtonTooltip;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearAllButtonTooltip;E=js}');
    };
    var reloadGridStore = function () {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    };
    var getSelectedOperationIds = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            ids.push(item.data.ID);
        });
        return ids;
    };
    var getNeedsReviewOperationIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            if (item.data.Status == 3 || item.data.Status == 7)
                ids.push(item.data.ID);
        });

        return ids;
    };

    var getNeedsReExecuteOperationIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            if (item.data.Status == 1)
                ids.push(item.data.ID);
        });

        return ids;
    };
    var getActiveOperationIds = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            if (item.data.Status == 2 || item.data.Status == 5) {
                ids.push(item.data.ID);
            }
        });
        return ids;
    };
    var checkActiveIds = function (ids, func) {

        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "HasActiveOperationByIds", { selectedIds: ids },
            function (result) {
                func(result);
            });
    };
    var cancelAll = function (items) {
        var ids = items != null && items.length > 0 ? getActiveOperationIds(items) : null;
        var cancelSelected = items != null && items.length > 0;

        checkActiveIds(ids, function (hasActive) {
            if (hasActive) {
                Ext.Msg.confirm('@{R=NCM.Strings;K=FirmwareUpgradeOperations_WarningDialogTitle;E=js}',
                    String.format("{0}<br><br> {1}", '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelConfirmDialogMessage;E=js}', cancelSelected ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelSelectedConfirmDialogMessage;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelAllConfirmDialogMessage;E=js}'),
                    function (btn, text) {
                        if (btn == 'yes') {
                            cancelAllInner(ids);
                        }
                });
            } else {
                Ext.Msg.confirm(cancelSelected ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelSelectedDialogTitle;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelAllDialogTitle;E=js}',
                                cancelSelected ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelSelectedDialogMessage;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelAllDialogMessage;E=js}',
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        cancelAllInner(ids);
                                    }
                                });
            }
        });
    };
    var cancelAllInner = function (ids) {
        var waitMsg = Ext.Msg.wait('Cancel...');
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
                            "CancelAll", { selectedIds: ids },
                            function (result) {
                                waitMsg.hide();
                                reloadGridStore();
                            });
    };
    var clearAll = function (items) {
        var ids = items != null && items.length > 0 ? getSelectedOperationIds(items) : null;
        var clearSelected = items != null && items.length > 0;

        checkActiveIds(ids, function (hasActive) {
            if (hasActive) {
                Ext.Msg.confirm('@{R=NCM.Strings;K=FirmwareUpgradeOperations_WarningDialogTitle;E=js}',
                    String.format("{0}<br><br> {1}", '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearConfirmDialogMessage;E=js}', clearSelected ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearSelectedConfirmDialogMessage;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearAllConfirmDialogMessage;E=js}'),
                    function (btn, text) {
                        if (btn == 'yes') {
                            clearAllInner(ids);
                        }
                });
            } else {
                Ext.Msg.confirm(clearSelected ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearSelectedDialogTitle;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearAllDialogTitle;E=js}',
                                clearSelected ? '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearSelectedDialogMessage;E=js}' : '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearAllDialogMessage;E=js}',
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        clearAllInner(ids);
                                    }
                                });
            }
        });
        
        
    };
    var clearAllInner = function(ids) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearWaitDialogMessage;E=js}');
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
                            "ClearAll", { selectedIds: ids },
                            function (result) {
                                waitMsg.hide();
                                reloadGridStore();
                            });
    };
    var clearComplete = function () {
        Ext.Msg.confirm("@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearCompleteDialogTitle;E=js}",
            "@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearCompleteDialogMessage;E=js}",
            function(btn, text) {
                if (btn == "yes") {
                    var waitMsg = Ext.Msg.wait("@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearWaitDialogMessage;E=js}");
                    ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
                        "ClearComplete", {},
                        function(result) {
                            waitMsg.hide();
                            reloadGridStore();
                        });
                }
            });
    };
    var clearFailed = function () {
        Ext.Msg.confirm("@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearFailedDialogTitle;E=js}",
            "@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearFailedDialogMessage;E=js}",
            function(btn, text) {
                if (btn == "yes") {
                    var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearWaitDialogMessage;E=js}');
                    ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
                                        "ClearFailed", {},
                                        function (result) {
                                            waitMsg.hide();
                                            reloadGridStore();
                                        });
                }
            });
    };
    var reviewFirmwareOperation = function (operationId) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
			"SetSessionData", { operationId: operationId },
			function (result) {
			    if (result)
			        window.location = String.format("/Orion/NCM/Firmware/UpgradeOptions.aspx?StepIndex=UpgradeOptions&OperationID={0}", operationId);
			}
        );
    };
    var addNewOperation = function () {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
			"ClearSessionData", {},
			function () {
			        window.location = "/Orion/NCM/Firmware/AddNew.aspx";
			}
        );
    };

    var rollbackFirmwareOperation = function (operationId) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
			"RollbackFirmwareOperation", { operationId: operationId },
			function (result) {
			    if (result != true) {
			        Ext.Msg.show({
                        title: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_RollbackDialogTitle;E=js}',
                        msg: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_RollbackDialogMessage;E=js}',
			            minWidth: 500,
			            buttons: Ext.Msg.OK,
			            icon: Ext.MessageBox.ERROR
			        });
			    } 
			    refreshObjects("-1", "");
            }
        );
    };


    var reExecuteFirmwareOperation = function (operationId) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
			"ReExecuteFirmwareOperation", { operationId: operationId },
			function (result) {
			    if (result != true) {
			        Ext.Msg.show({
                        title: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ReExecuteDialogTitle;E=js}',
                        msg: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ReExecuteDialogMessage;E=js}',
			            minWidth: 500,
			            buttons: Ext.Msg.OK,
			            icon: Ext.MessageBox.ERROR
			        });
			    }
			    refreshObjects("-1", "");
			}
        );
    };

    var renderName = function (value, meta, record) {
        return Ext.util.Format.htmlEncode(value);
    }; //get status name
    var getStatusName = function (value) {
        switch (value) {
            case -1:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusAll;E=js}";
            case 0:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusUnknown;E=js}";
            case 1:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusError;E=js}";
            case 2:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusCollectingData;E=js}";
            case 3:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusReviewToContinue;E=js}";
            case 4:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusQueued;E=js}";
            case 5:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusUpgrading;E=js}";
            case 6:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusComplete;E=js}";
            case 7:
                return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusScheduled;E=js}";
            default:
                return "";
        }
    }; //render status bar for "Collecting Data" and  enum status values
    var renderStatus = function (value, meta, record) {
        switch (value) {
            case 1: //Error
                return String.format('<img src="/Orion/NCM/Resources/images/Status.Error.gif"> <span style="color:red;">{0}</span>', record.data.ErrorMessage);
            case 3: //NeedsReview
                return String.format('<a onclick="SW.NCM.FirmwareOperations.reviewFirmwareOperation({0});" href="#"><img src="/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_pending.png"> <span>{1}</span></a>', record.data.ID, getStatusName(value)); //TODO  render link here
            case 4: //Queued
                return String.format('<img src="/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_waiting_for_execution.png"> <span>{0}</span>', getStatusName(value));
            case 2: //"Collecting Data"
            case 5: //"Upgrading"
                var completed = record.data.CompletedOperations;
                var all = record.data.AllOperations;
                var percentage = (completed || all) ? Math.round(completed / all * 100) : 0;
                return String.format('<div class="sw-progress"><div class="sw-progress-text">{0} {1} %</div><div class="sw-progress-bar" style="width:{1}%;"></div></div>', getStatusName(value), percentage);
            case 6: //Complete
                return String.format('<img src="/Orion/NCM/Resources/images/Status.Complete.gif"> <span>{0}</span>', getStatusName(value)); 
            case 7: //Scheduled
                return String.format('<a onclick="SW.NCM.FirmwareOperations.reviewFirmwareOperation({0});" href="#"><img src="/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_scheduled.png"> <span>{1}</span></a>', record.data.ID, getStatusName(value)); //TODO  render link here
            default:
                return "";
        }
    };
    var renderDateTime = function (value, meta, record) {
        if (!value)
            return "";
        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    };

    function renderLog(value, meta, record) {
        if (value == null) //no log data yet
            return '';
        else
            return'<img class="firmwareUpgradeLog" style="cursor: pointer;" src="/Orion/NCM/Resources/images/InventoryIcons/icon_inventory_report.gif" />';
    }

    var currentSearchTerm = null;
    //refresh grid store
    var refreshObjects = function (value, search) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { value: value, search: search, clientOffset: getDateTimeOffset() };
        currentSearchTerm = search;
        grid.store.on('load', function () {
            searchHighlight();
        });
        pagingToolbar.doLoad(pagingToolbar.cursor);
    }; //auto refresh status column
    var timerRunner;
    var autoRefresh = function () {
        var task = {
            run: function () {
                var ids = getOperationIdsInProgress();
                if (ids.length > 0)
                    requestStatus(ids);
            },
            interval: 5000
        };
        timerRunner = new Ext.util.TaskRunner();
        timerRunner.start(task);
    };
    var getOperationIdsInProgress = function () {
        var ids = [];
        grid.store.data.each(function (record, index, length) {
            if (record.data.Status == 2 || record.data.Status == 4 || record.data.Status == 5 || record.data.Status == 7)
                ids.push(record.data.ID);
        });
        return ids;
    };
    var requestStatus = function (ids) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
           "GetFirmwareOperationsStatus", { ids: ids },
            function (result) {
                var obj = JSON.parse(result);
                if (obj != null) {
                    updateStatusColumn(obj);
                }
            });
    };
    var updateStatusColumn = function (operations) {
        operations.forEach(function (operation) {
            var recordIndex = grid.store.findExact("ID", operation.ID);
            if (recordIndex >= 0) {
                var recordToUpdate = grid.store.getAt(recordIndex);

                var prevStatus = recordToUpdate.data.Status;

                recordToUpdate.set('Status', operation.Status);
                recordToUpdate.set('ErrorMessage', operation.ErrorMessage);
                recordToUpdate.set('CompletedOperations', operation.CompletedOperations);
                recordToUpdate.set('AllOperations', operation.AllOperations);
                recordToUpdate.set('Log', operation.Log);
                recordToUpdate.commit();

                searchHighlight();

                if (prevStatus != operation.Status) {
                    var map = grid.getTopToolbar().items.map;
                    refreshObjects(map.StatusFilter.getValue(), "");
                }
            }
        });
    };
    var pagingToolbarChangeHandler = function (that) {
        if (that.displayItem) {
            var count = 0;
            that.store.each(function (record) {
                count++;
            });
            var msg = count == 0 ? that.emptyMsg : String.format(that.displayMsg, that.cursor + 1, that.cursor + count, that.store.getTotalCount());
            that.displayItem.setText(msg);
        }
    };
    var searchHighlight = function () {
        var search = currentSearchTerm;
        if ($.trim(search).length == 0) return;

        var columnsToHighlight = SW.NCM.ColumnIndexByDataIndex(grid.getColumnModel(), ['Name']);
        Ext.each(columnsToHighlight, function (columnNumber) {
            SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
        });
    };
    var cleanSearch = function () {
        currentSearchTerm = '';
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
    };
    var doClean = function () {
        cleanSearch();
        var map = grid.getTopToolbar().items.map;
        refreshObjects(map.StatusFilter.getValue(), "");
    };
    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        var statusValue = map.StatusFilter.getValue();
        if ($.trim(search).length == 0) return;

        refreshObjects(statusValue, search);
        map.Clean.setVisible(true);
    };
    var showFirmwareUpgradeLog = function () {
        var firmwareUpgradeLogDiv = $("#firmwareUpgradeLogTextArea");
        firmwareUpgradeLogDiv.empty();

        var loadMask = new Ext.LoadMask(Ext.get('Grid'), {
            msg: '@{R=NCM.Strings;K=WEBJS_VK_04;E=js}',
            removeMask: true
        });
        loadMask.show();

        var item = grid.getSelectionModel().getSelected();
        var lastLinesCount = 3000;

        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
                             "GetLog", { operationId: item.data.ID, lastLinesCount: lastLinesCount },
                             function (result) {
                                 loadMask.hide();

                                 var obj = JSON.parse(result);
                                 
                                 var firmwareUpgradeLogTextArea = new Ext.form.TextArea({
                                     id: 'firmwareUpgradeLogID',
                                     renderTo: 'firmwareUpgradeLogTextArea',
                                     style: 'border: none;',
                                     width: 680,
                                     height: obj.IsLargeLogFile ? 390 : 425,
                                     readOnly: true,
                                     emptyText: '@{R=NCM.Strings;K=WEBJS_VK_187;E=js}',
                                     value: obj.LogToDisplay
                                 });
                                 
                                 if (obj.IsLargeLogFile) {
                                     $("#firmwareUpgradeLogNotification").css("display", "block");
                                     $("#firmwareUpgradeLogNotification").empty().text(String.format("@{R=NCM.Strings;K=FirmwareUpgradeOperations_UpgradeReportNotification;E=js}", lastLinesCount));
                                 }
                                 else {
                                     $("#firmwareUpgradeLogNotification").css('display', 'none');
                                 }

                                 return ShowFirmwareUpgradeLogDialog(String.format('@{R=NCM.Strings;K=FirmwareUpgradeOperations_UpgradeReportDialogTitle;E=js}', item.data.Name));
                             });
    };
    var showFirmwareUpgradeLogDialog;
    ShowFirmwareUpgradeLogDialog = function (title) {
        if (!showFirmwareUpgradeLogDialog) {
            showFirmwareUpgradeLogDialog = new Ext.Window({
                applyTo: 'firmwareUpgradeLogDialog',
                layout: 'fit',
                width: 700,
                height: 500,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'firmwareUpgradeLogBody',
                buttonAlign: 'left',
                buttons: [
                '->',
                getButton({ primary: true, text: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_OpenAsTextFileButtonText;E=js}' }, function () {
                    var item = grid.getSelectionModel().getSelected();
                    $.download('/Orion/NCM/Firmware/DownloadFirmwareUpgradeLog.ashx', 'ID=' + item.data.ID + "&Name=" + item.data.Name);
                })
                ,
                getButton({ primary: false, text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}' }, function () {
                    showFirmwareUpgradeLogDialog.hide();
                })
                ]
            });
        }

        showFirmwareUpgradeLogDialog.setTitle(Ext.util.Format.htmlEncode(title));
        showFirmwareUpgradeLogDialog.alignTo(document.body, "c-c");
        showFirmwareUpgradeLogDialog.show();

        return false;
    };
    var getButton = function(btn, handler) {
        return new Ext.BoxComponent({
            autoEl: {
                tag: 'a',
                href: '#'
            },
            html: '<a class="sw-btn' + (btn.primary ? ' sw-btn-primary' : '') + '" href="#"><span class="sw-btn-c"><span class="sw-btn-t">' + btn.text + '</span></span></a>',
            listeners: {
                'afterrender': function (c) {
                    c.el.on('click', function (e) {
                        e.preventDefault();
                        handler();
                    });
                }
            }
        });
    };
    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('firmwareUpgradeLog')) {
                    showFirmwareUpgradeLog();
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                            "/Orion/NCM/Services/FirmwareOperations.asmx/GetFirmwareOperationsPaged",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'CreationDate', mapping: 2 },
                                { name: 'RunAt', mapping: 3 },
                                { name: 'Status', mapping: 4 },
                                { name: 'ErrorMessage', mapping: 5 },
                                { name: 'CompletedOperations', mapping: 6 },
                                { name: 'AllOperations', mapping: 7 },
                                { name: 'Log', mapping: 8 },
                                { name: 'UserName', mapping: 9 }
                            ],
                            "Status");

            var pageSize = parseInt(ORION.Prefs.load('PageSize', '25'));

            var pageSizeBox = new Ext.form.NumberField({
                id: 'pageSizeBox',
                enableKeyEvents: true,
                allowNegative: false,
                width: 30,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value > 0 && value <= 100) {
                                pageSize = value;
                                var pagingToolbar = grid.getBottomToolbar();
                                if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                    ORION.Prefs.save('PageSize', pageSize);
                                    pagingToolbar.pageSize = pageSize;
                                    pagingToolbar.doLoad(0);
                                }
                            }
                            else {
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value > 0 && value <= 100) {
                            pageSize = value;
                            var pagingToolbar = grid.getBottomToolbar();
                            if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                pagingToolbar.pageSize = pageSize;
                                pagingToolbar.doLoad(0);
                            }
                        }
                        else {
                            return;
                        }
                    }
                }
            });

            pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_PagingMessage;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_PagingEmptyMessage;E=js}',
                items: ['-', new Ext.form.Label({ text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }), pageSizeBox],
                listeners: {
                    change: function () {
                        pagingToolbarChangeHandler(this);
                    }
                }
            });
           
            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,
                columns: [
                    selectorModel,
                    { header: 'ID', width: 80, hidden: true, hideable: false, dataIndex: 'ID' },
                    { header: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_NameColumnHeader;E=js}', width: 400, sortable: true, dataIndex: 'Name', renderer: renderName },
                    { header: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CreationDateColumnHeader;E=js}', width: 200, sortable: true, dataIndex: 'CreationDate', renderer: renderDateTime },
                    { header: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusDetailsColumnHeader;E=js}', width: 450, sortable: true, dataIndex: 'Status', renderer: renderStatus },
                    { header: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_HistoryColumnHeader;E=js}', width: 100, sortable: true, dataIndex: 'Log', align: 'center', renderer: renderLog },
                    { header: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_RunAtColumnHeader;E=js}', width: 200, sortable: true, dataIndex: 'RunAt', renderer: renderDateTime },
                    { header: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_UserNameColumnHeader;E=js}', width: 150, sortable: true, dataIndex: 'UserName' }
                ],
                sm: selectorModel,
                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                height: 500,
                maxWidth: 1000,
                stripeRows: true,
                tbar: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VM_76;E=js}',
                    xtype: 'label'
                }, {
                    id: 'StatusFilter',
                    xtype: 'combo',
                    editable: false,
                    triggerAction: 'all',
                        store: [['-1', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_AllStatus;E=js}'],
                        ['1', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusError;E=js}'],
                        ['2', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusCollectingData;E=js}'],
                        ['3', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusNeedsReview;E=js}'],
                        ['7', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusScheduled;E=js}'],
                        ['4', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusQueued;E=js}'],
                        ['5', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusUpgrading;E=js}'],
                        ['6', '@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusComplete;E=js}']],
                    value: '-1',
                    selectOnFocus: true,
                    listeners: {
                        select: function (that, record) {
                            cleanSearch();
                            refreshObjects(that.getValue(), "");
                        }
                    }
                }, '-', {
                    id: 'AddButton',
                    text: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_AddButtonText;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_AddButtonTooltip;E=js}',
                    cls: 'x-btn-text-icon',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                    handler: function () {addNewOperation();  }
                }, '-', {
                    id: 'ReviewButton',
                    text: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ReviewButtonText;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ReviewButtonTooltip;E=js}',
                    cls: 'x-btn-text-icon',
                    icon: '/Orion/NCM/Resources/images/PolicyIcons/view_report.gif',
                    handler: function() {
                        var item = grid.getSelectionModel().getSelected();
                        reviewFirmwareOperation(item.data.ID);
                    }
                }, '-', {
                    id: 'CancelAll',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_86;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_CancelAllButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_cancel.gif',
                    cls: 'x-btn-text-icon',
                    handler: function() {
                        if (SW.NCM.checkDemoMode("NCM_FirmwareOperations_CancelAll")) {
                            return;
                        }
                        cancelAll(grid.getSelectionModel().getSelections());
                    }
                }, '-', {
                    id: 'ClearAll',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_94;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearAllButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_clear_all.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.checkDemoMode("NCM_FirmwareOperations_ClearAll")) {
                            return;
                        }
                        clearAll(grid.getSelectionModel().getSelections());
                    }
                }, '-', {
                    id: 'ClearComplete',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_95;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearCompleteButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_clear_complete.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.checkDemoMode("NCM_FirmwareOperations_ClearComplete")) {
                            return;
                        }
                        clearComplete();
                    }
                }, '-', {
                    id: 'ClearFailed',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_96;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ClearFailedButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_clear_failed.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.checkDemoMode("NCM_FirmwareOperations_ClearFailed")) {
                            return;
                        }
                        clearFailed();
                    }
                }, '-', {
                    id: 'Rollback',
                    text: '@{R=NCM.Strings;K=WEBJS_AP_1;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_RollbackButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_refresh.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.checkDemoMode("NCM_FirmwareOperations_Rollback")) {
                            return;
                        }

                        var item = grid.getSelectionModel().getSelected();
                        rollbackFirmwareOperation(item.data.ID);
                    }
                }, '-', {
                        id: 'ReExecute',
                        text: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ReExecuteButtonText;E=js}',
                        tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_ReExecuteButtonTooltip;E=js}',
                        icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_refresh.gif',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            if (SW.NCM.checkDemoMode("NCM_FirmwareOperations_ReExecute")) {
                                return;
                            }

                            var item = grid.getSelectionModel().getSelected();
                            reExecuteFirmwareOperation(item.data.ID);
                        }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=FirmwareUpgradeOperations_SearchEmptyText;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],
                bbar: pagingToolbar
            });

            grid.render('Grid');
            updateToolbarButtons();

            gridResize();
            $(window).resize(gridResize);

            refreshObjects("-1", "");
            
            autoRefresh();
        },
        reviewFirmwareOperation: function (id) {
            reviewFirmwareOperation(id);
        }
    };
}();

Ext.onReady(SW.NCM.FirmwareOperations.init, SW.NCM.FirmwareOperations);

jQuery.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
		.appendTo('body').submit().remove();
    };
};
