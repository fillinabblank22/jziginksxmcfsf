﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.UpgradeConfirmDialog = function () {

    var grid;
    var window;

    var refreshGrid = function (callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = {};
        grid.store.load({ callback: callback });
    };

    var gridResize = function (w, h) {
        if (grid != null) {
            grid.setWidth(w);
            grid.setHeight(h);
        }
    };

    var getSize = function (size) {
        if (size >= 1073741824)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInGBLabel;E=js}", (size / 1073741824).toFixed(2), size.toLocaleString());
        else if (size >= 1048576)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInMBLabel;E=js}", (size / 1048576).toFixed(2), size.toLocaleString());
        else if (size >= 1024)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInKBLabel;E=js}", (size / 1024).toFixed(2), size.toLocaleString());
        else
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInBytesLabel;E=js}", size.toLocaleString());
    };

    var renderMinimumSpaceNeeded = function (size) {
        return getSize(size);
    };

    var renderAvailableFreeSpace = function (size) {
        if (size === 0) {
            return '@{R=NCM.Strings;K=WEBJS_VK_150;E=js}';
        }

        return String.format('<img style="vertical-align:middle;" src="/Orion/NCM/Resources/images/severity_critical.png" /> {0}', getSize(size));
    };

    var getNodesWithFreeSpaceIssues = function () {
        var coreNodeIds = [];
        grid.store.data.each(function (item) {
            if (item.data.AvailableFreeSpace !== 0) {
                coreNodeIds.push(item.data.CoreNodeId);
            }
        });
        return coreNodeIds;
    };

    return {
        show: function (onConfirmed) {
            window = new Ext.Window({
                id: 'window',
                layout: 'anchor',
                width: Ext.getBody().getViewSize().width * 0.5,
                minWidth: 800,
                height: Ext.getBody().getViewSize().height * 0.5,
                minHeight: 500,
                closeAction: 'close',
                title: '@{R=NCM.Strings;K=FirmwareUpgradeConfirmDialog_Title;E=js}',
                modal: true,
                resizable: false,
                stateful: false,
                plain: true,
                html: '<div id="Grid"/>',
                buttons: [{
                        text: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_ConfirmButtonText;E=js}',
                        cls: 'sw-btn-primary',
                        handler: function () {
                            var coreNodeIds = getNodesWithFreeSpaceIssues();
                            window.close();
                            onConfirmed(coreNodeIds);
                        }
                    }, {
                        text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                        handler: function () {
                            window.close();
                        }
                }],
                listeners: {
                    resize: function (me, w, h) {
                        gridResize(me.getInnerWidth(), me.getInnerHeight() - $('#bottomInfoHolder').height() - 15);
                    },
                    show: function (me) {
                        me.alignTo(Ext.getBody(), "c-c");

                        var dataStore = new ORION.WebServiceStore(
                            "/Orion/NCM/Services/FirmwareOperations.asmx/GetFirmwareOperationNodesWithFreeSpaceIssues",
                            [
                                { name: 'CoreNodeId', mapping: 0 },
                                { name: 'Caption', mapping: 1 },
                                { name: 'MinimumSpaceNeeded', mapping: 2 },
                                { name: 'AvailableFreeSpace', mapping: 3 }
                            ],
                            "Caption");

                        var colModel = new Ext.grid.ColumnModel({
                            columns: [
                                {
                                    header: 'NodeID',
                                    width: 20,
                                    hidden: true,
                                    hideable: false,
                                    dataIndex: 'CoreNodeId'
                                }, {
                                    header: '<b>@{R=NCM.Strings;K=FirmwareRepository_NodeColumnHeader;E=js}</b>',
                                    width: 300,
                                    dataIndex: 'Caption'
                                }, {
                                    header: '<b>@{R=NCM.Strings;K=FirmwareUpgradeConfirmDialog_MinSpaceNeededColumnHeader;E=js}</b>',
                                    width: 200,
                                    dataIndex: 'MinimumSpaceNeeded',
                                    renderer: renderMinimumSpaceNeeded
                                }, {
                                    header: '<b>@{R=NCM.Strings;K=FirmwareUpgradeConfirmDialog_SpaceAvailableHeader;E=js}</b>',
                                    width: 200,
                                    dataIndex: 'AvailableFreeSpace',
                                    renderer: renderAvailableFreeSpace
                                }
                            ],
                            defaults: {
                                sortable: false,
                                resizable: false,
                                menuDisabled: true
                            }
                        });

                        grid = new Ext.grid.GridPanel({
                            id: 'gridControl',
                            store: dataStore,
                            colModel: colModel,
                            layout: 'fit',
                            autoScroll: 'true',
                            loadMask: true,
                            width: 800,
                            height: 500,
                            stripeRows: false,
                            trackMouseOver: false,
                            disableSelection: true,
                            enableColumnMove: false,
                            padding: 10
                        });
                        grid.render('Grid');

                        $('#Grid').prepend(' \
                            <div id="bottomInfoHolder" style = "padding:10px 10px 0px 10px;" > \
                                <img style="vertical-align:middle;" src="/Orion/NCM/Resources/images/Icon.Warning.Mock.png" /> <b>@{R=NCM.Strings;K=FirmwareUpgradeConfirmDialog_NodesNotHaveEnoughSpaceLabel;E=js}</b> \
                                </br> \
                                </br> \
                                @{R=NCM.Strings;K=FirmwareUpgradeConfirmDialog_ManuallyVerifySpaceLabel;E=js} \
                                </br> \
                                </br> \
                                @{R=NCM.Strings;K=FirmwareUpgradeConfirmDialog_NodesWithLessSpaceLabel;E=js} \
                            </div> \
                        ');
                        $('.x-grid3-scroller').css('border', 'solid 1px #eee');

                        gridResize(me.getInnerWidth(), me.getInnerHeight() - $('#bottomInfoHolder').height() - 15);
                        refreshGrid(function () {
                            $('.x-grid3-hd-row td.sort-asc').removeClass('sort-asc');
                        });
                    },
                    close: function (me) {
                        grid = null;
                        window = null;
                    }
                }

            }).show();

            return false;
        },
        resize: function () {
            if (window != null) {
                window.setSize(Ext.getBody().getViewSize().width * 0.5, Ext.getBody().getViewSize().height * 0.5);
                window.alignTo(Ext.getBody(), "c-c");
            }
        }
    };
}();