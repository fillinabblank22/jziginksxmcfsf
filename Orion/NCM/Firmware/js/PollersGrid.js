﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.namespace('SW.NCM.Firmware');
Ext.QuickTips.init();

SW.NCM.Firmware.PollersGrid = function () {

    var grid;

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#pollersGridCell').width());
        }
    }
    

    var refreshGrid = function (search, callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = {};
        grid.store.load();
    }
    

    function renderSize(value, meta, record) {
        if (value === -1)
            return "@{R=NCM.Strings;K=FirmwareUpgradeOperations_StatusUnknown;E=js}";
        else if (value >= 1073741824)
            return String.format("{0} {1}", (value / 1073741824).toFixed(2), "@{R=NCM.Strings;K=SizeInGb;E=js}");
        else if (value >= 1048576)
            return String.format("{0} {1}", (value / 1048576).toFixed(2), "@{R=NCM.Strings;K=SizeInMb;E=js}");
        else if (value >= 1024)
            return String.format("{0} {1}", (value / 1024).toFixed(2), "@{R=NCM.Strings;K=SizeInKB;E=js}");
        else
            return String.format("{0} {1}", value.toLocaleString(), "@{R=NCM.Strings;K=SizeInBytes;E=js}");
    }


    return {
        init: function () {
            var dataStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/FirmwareOperations.asmx/LoadPollersFreeSpaceData",
                [
                    { name: 'EngineName', mapping: 0 },
                    { name: 'FreeSpace', mapping: 1 },
                ],
                "EngineName");
            dataStore.sortInfo = null;
           

            grid = new Ext.grid.GridPanel({
                id: 'pollersGrid',
                store: dataStore,
                disableSelection: true,
                columns: [
                    {
                    header: '@{R=NCM.Strings;K=FirmwareUpgrade_GridColumn_PollingEngine;E=js}',
                    width: 200,
                    sortable: false,
                    menuDisabled: true,
                    dataIndex: 'EngineName',
                    css: 'height:23px;'
                    },
                    {
                    header: '@{R=NCM.Strings;K=FirmwareUpgrade_GridColumn_AvailableSpace;E=js}',
                    width: 120,
                    sortable: false,
                    menuDisabled: true,
                    dataIndex: 'FreeSpace',
                    renderer: renderSize,
                    css: 'height:23px;'
                }
                ],

                layout: 'fit',
                stateful: false,
                autoScroll: 'true',
                loadMask: true,
                width: 300,
                height: 300,
                stripeRows: false,
                trackMouseOver: false
            });
            grid.render('PollersGrid');
            refreshGrid();

            gridResize();
            $(window).resize(gridResize);
        }
    }

}();

Ext.onReady(SW.NCM.Firmware.PollersGrid.init, SW.NCM.Firmware.PollersGrid);
