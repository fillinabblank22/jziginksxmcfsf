﻿
Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.namespace('SW.NCM.Firmware');
Ext.QuickTips.init();

SW.NCM.Firmware.UpgradeOptions = function () {

    var resize = function () {
        var bottom = $('#txtScriptPreview').offset().top + $('#txtScriptPreview').height() + 10;
        var top = $('#selNodes').offset().top;
        $('#selNodes').height(bottom - top);

        SW.NCM.FirmwareImagesRadioGrid.resize();
        SW.NCM.UpgradeConfirmDialog.resize();
    };

    var parseSize = function (size) {
        if (size >= 1073741824)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_SizeLabel;E=js} @{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInGBLabel;E=js}", (size / 1073741824).toFixed(2), size.toLocaleString());
        else if (size >= 1048576)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_SizeLabel;E=js} @{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInMBLabel;E=js}", (size / 1048576).toFixed(2), size.toLocaleString());
        else if (size >= 1024)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_SizeLabel;E=js} @{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInKBLabel;E=js}", (size / 1024).toFixed(2), size.toLocaleString());
        else
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_SizeLabel;E=js} @{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInBytesLabel;E=js}", size.toLocaleString());
    };

    var updateNotification = function (confirmedNodes, allNodes) {
        $("#notification").empty();
        $("#notification").append(String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_ConfirmedNodesCountMessage;E=js}", "<b>", "</b>", confirmedNodes, allNodes));

        enableDisableNextButton(confirmedNodes === allNodes);
    };

    var enableDisableNextButton = function (enable) {
        if (enable) {
            $("#" + SW.NCM.Firmware.ButtonNextClientID).removeClass("aspNetDisabled sw-btn-disabled");
        }
        else {
            $("#" + SW.NCM.Firmware.ButtonNextClientID).addClass("aspNetDisabled sw-btn-disabled");
        }
    };

    var updateNodeIcon = function (coreNodeId, confirmed) {
        var tr = $("input[value='" + coreNodeId + "']").parent().parent();
        var img = tr.find('td:nth-child(2)').children();

        tr.attr('title', confirmed ? "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeConfirmedTooltip;E=js}" : "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeNotConfirmedTooltip;E=js}");
        img.attr("confirmed", confirmed)
           .attr("src", confirmed ? "/Orion/NCM/Resources/images/icon_ok.png" : "/Orion/NCM/Resources/images/Icon.Warning.Mock.png")
           .attr("title", confirmed ? "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeConfirmedTooltip;E=js}" : "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeNotConfirmedTooltip;E=js}");
    };

    var getFirmwareOperationNodes = function (onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "GetFirmwareOperationNodes", {},
            function (result) {
                if (result !== null)
                    onSuccess(result);
            });
    };

    var initFirmwareOperationNodes = function () {
        var listItems = $(".GroupItems").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");

        getFirmwareOperationNodes(function (result) {
            if (result !== null) {
                listItems.empty();
                var nodes = JSON.parse(result);
                nodes.forEach(function (node) {
                    var coreNodeId = node.CoreNodeID;
                    var caption = Ext.util.Format.htmlEncode(node.Caption);
                    var machineType = Ext.util.Format.htmlEncode(node.MachineType);
                    var confirmed = node.Confirmed;

                    $(".GroupItems").append($("<tr style='height:25px;'>")
                        .attr('title', confirmed ? "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeConfirmedTooltip;E=js}" : "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeNotConfirmedTooltip;E=js}")
                        .append($("<td class='td-node' style='width:20px;padding-left:4px;'>")
                            .append($("<input>")
                                .attr("type", "checkbox")
                                .attr("name", "firmware-operation-node")
                                .attr("onchange", "selectionChange()")
                                .attr("value", coreNodeId))
                        )
                        .append($("<td class='td-node' style='width:20px;padding-left:3px;'>")
                            .append($("<img>")
                                .attr("confirmed", confirmed)
                                .attr("src", confirmed ? "/Orion/NCM/Resources/images/icon_ok.png" : "/Orion/NCM/Resources/images/NodeStatusIcons/Small-Testing.gif")
                                .attr("title", confirmed ? "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeConfirmedTooltip;E=js}" : "@{R=NCM.Strings;K=FirmwareUpgradeWizard_NodeNotConfirmedTooltip;E=js}"))
                        )
                        .append($("<td class='td-node'>")
                            .append($("<a href='#'></a>")
                                .attr('coreNodeId', coreNodeId)
                                .attr('machineType', machineType).text(caption).click(selectNode))
                        )
                        .append($("<td class='td-node'>")
                            .append($("<img>")
                                .attr("src", "/Orion/NCM/Resources/images/icon_delete.png")
                                .attr("align", "right")
                                .attr("style", "cursor:pointer;")
                                .attr('coreNodeId', coreNodeId).click(deleteNode))
                        )
                    );
                });

                toSelect = $(".td-node a:first");
                toSelect.click();
            }
        });
    };

    var selectNode = function () {
        var prevCoreNodeId = $(".SelectedGroupItem a").length > 0 ? $(".SelectedGroupItem a").attr('coreNodeId') : 0;
        var confirmed = $(".SelectedGroupItem img").length > 0 ? Boolean.parse($(".SelectedGroupItem img").attr('confirmed')) : false;

        $(this).parent().parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");
        var coreNodeId = $(".SelectedGroupItem a").attr('coreNodeId');

        if (prevCoreNodeId !== coreNodeId) {
            updateNodeOptions(prevCoreNodeId, function () {
                if (prevCoreNodeId > 0) {
                    if (confirmed && !validateNodeOptions()) {
                        confirmFirmwareOperationNodeInternal(prevCoreNodeId, false);
                    }
                }
                prevCoreNodeId = coreNodeId;
                loadNodeOptions(coreNodeId);
            });
        }

        return false;
    };

    var deleteNode = function () {
        var rows = $(".GroupItems > tbody > tr").length;
        if (rows === 1) {
            Ext.Msg.show({
                title: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_DeleteNodeDialogTitle;E=js}',
                msg: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_DeleteNodeDialogMessage;E=js}',
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            });
        }
        else {
            var img = $(this);
            var coreNodeId = img.attr('coreNodeId');
            var caption = $(String.format('a[coreNodeId={0}]', coreNodeId)).text();
            Ext.Msg.confirm('@{R=NCM.Strings;K=FirmwareUpgradeWizard_DeleteNodeDialogTitle;E=js}',
                String.format('@{R=NCM.Strings;K=FirmwareUpgradeWizard_DeleteNodeConfirmDialogMessage;E=js}', caption),
                function (btn, text) {
                    if (btn === 'yes') {
                        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareUpgradeWizard_DeletingWaitDialogMessage;E=js}');
                        internalDeleteFirmwareOperationNode(coreNodeId, function (result) {
                            waitMsg.hide();
                            if (result) {
                                var d = JSON.parse(result);
                                if (d.Success) {
                                    img.parent().parent().remove();
                                    var toSelect = $(".SelectedGroupItem a");
                                    if (toSelect.length === 0) {
                                        prevCoreNodeId = 0;
                                        toSelect = $(".td-node a:first");
                                        toSelect.click();
                                    }
                                    SW.NCM.Firmware.ConfirmedNodes = d.ConfirmedNodes;
                                    SW.NCM.Firmware.AllNodes = d.AllNodes;
                                    SW.NCM.Firmware.NodesWithFreeSpaceIssues = d.NodesWithFreeSpaceIssues;
                                    updateNotification(d.ConfirmedNodes, d.AllNodes);
                                }
                            }
                        });
                    }
                });
        }
        return false;
    };

    var internalDeleteFirmwareOperationNode = function (coreNodeId, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "DeleteFirmwareOperationNode", { coreNodeId: coreNodeId },
            function (result) {
                onSuccess(result);
            });
    };

    var validateNodeOptions = function () {
        var isValid = true;
        if (!validateFirmwareImages()) {
            isValid = false;
        }

        if (SW.NCM.Firmware.HidePathToUploadImage) {
            ValidatorEnable($("#rfvUploadImagePath")[0], false);
        } else {
            if ($("#txtUploadImagePath").val().length === 0) {
                ValidatorEnable($("#rfvUploadImagePath")[0], true);
                isValid = false;
            } else {
                ValidatorEnable($("#rfvUploadImagePath")[0], false);
            }
        }

        var chkBackupExisingFirmwareImage = $("#chkBackupExisingFirmwareImage").prop('checked');
        if (chkBackupExisingFirmwareImage && $("#txtBackupExistingImagePath").val().length === 0) {
            ValidatorEnable($("#rfvBackupExistingImagePath")[0], true);
            isValid = false;
        } else {
            ValidatorEnable($("#rfvBackupExistingImagePath")[0], false);
        }

        var chkDeleteExisingFirmwareImage = $("#chkDeleteExisingFirmwareImage").prop('checked');
        if (chkDeleteExisingFirmwareImage && $("#txtDeleteExistingImagePath").val().length === 0) {
            ValidatorEnable($("#rfvDeleteExistingImagePath")[0], true);
            isValid = false;
        } else {
            ValidatorEnable($("#rfvDeleteExistingImagePath")[0], false);
        }

        return isValid;
    };

    expandUpgradeOptions = function (expand) {
        $("#expandImg").attr("src", expand ? "/Orion/NCM/Resources/images/arrowdown.gif" : "/Orion/NCM/Resources/images/arrowright.gif");
        $("#expandImg").attr("expand", expand ? "true" : "false");

        if (expand)
            $("#upgradeOptionsTable").show();
        else
            $("#upgradeOptionsTable").hide();

        resize();
    };

    var redirectToNotificationsStep = function () {
        var operationId = Ext.getUrlParam('OperationID');
        window.location = String.format("/Orion/NCM/Firmware/Notifications.aspx?StepIndex=Notifications&OperationID={0}", operationId);
    };

    validateFunc = function () {
        if (SW.NCM.Firmware.ConfirmedNodes > 0 && SW.NCM.Firmware.ConfirmedNodes === SW.NCM.Firmware.AllNodes) {
            if (validateNodeOptions()) {
                //save node options when user made some changes for selected node and click Next button
                var coreNodeId = $(".SelectedGroupItem a").attr('coreNodeId');
                updateNodeOptions(coreNodeId);

                if (SW.NCM.Firmware.NodesWithFreeSpaceIssues > 0) {
                    return SW.NCM.UpgradeConfirmDialog.show(function (coreNodeIds) {
                        if (coreNodeIds.length > 0) {
                            checkDeleteExisingFirmwareImageOnNodes(coreNodeIds, function () {
                                redirectToNotificationsStep();
                            });
                        }
                        else {
                            redirectToNotificationsStep();
                        }
                    });
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };

    generateScriptPreview = function () {
        if (!validateNodeOptions()) {
            return false;
        }

        var coreNodeId = $(".SelectedGroupItem a").attr('coreNodeId');
        if (coreNodeId && coreNodeId > 0) {
            updateNodeOptions(coreNodeId, function () {
                var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareUpgradeWizard_GenerateScriptWaitDialogMessage;E=js}');
                ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
                    "GenerateScriptPreview", { coreNodeId: coreNodeId },
                    function (result) {
                        waitMsg.hide();
                        $("#txtScriptPreview").val(result);
                        SW.NCM.checkDemoMode("NCM_FirmwareOperations_GenerateScriptPreview");
                    }
                );
            });
        }
        return false;
    };

    var updateNodeOptions = function (coreNodeId, onSuccess) {
        if (coreNodeId <= 0) {
            if (onSuccess !== undefined) {
                onSuccess();
            }
            return;
        }

        var nodeUpgradeOptions = {};

        nodeUpgradeOptions.FirmwareImages = getFirmwareImages();
        nodeUpgradeOptions.UploadImagePath = $("#txtUploadImagePath").val();

        nodeUpgradeOptions.ExpandUpgradeOptions = JSON.parse($('#expandImg').attr('expand'));

        var chkBackupExisingFirmwareImage = $("#chkBackupExisingFirmwareImage").prop('checked');
        nodeUpgradeOptions.BackupExisingFirmwareImage = chkBackupExisingFirmwareImage;
        if (chkBackupExisingFirmwareImage)
            nodeUpgradeOptions.BackupExistingImagePath = $("#txtBackupExistingImagePath").val();
        var chkDeleteExisingFirmwareImage = $("#chkDeleteExisingFirmwareImage").prop('checked');
        nodeUpgradeOptions.DeleteExisingFirmwareImage = chkDeleteExisingFirmwareImage;
        if (chkDeleteExisingFirmwareImage)
            nodeUpgradeOptions.DeleteExistingImagePath = $("#txtDeleteExistingImagePath").val();

        nodeUpgradeOptions.UpdateConfigRegister = $("#chkUpdateConfigRegister").prop('checked');
        nodeUpgradeOptions.SaveConfigToNVRAM = $("#chkSaveConfigToNVRAM").prop('checked');
        nodeUpgradeOptions.BackupRunningAndStartupConfigsBeforeUpgrade = $("#chkBackupRunningAndStartupConfigsBeforeUpgrade").prop('checked');
        nodeUpgradeOptions.BackupRunningAndStartupConfigsAfterUpgrade = $("#chkBackupRunningAndStartupConfigsAfterUpgrade").prop('checked');
        nodeUpgradeOptions.UpdateBootVariable = $("#chkUpdateBootVariable").prop('checked');
        nodeUpgradeOptions.VerifyUploadImage = $("#chkVerifyUploadImage").prop('checked');
        nodeUpgradeOptions.VerifyBackupImage = $("#chkVerifyBackupImage").prop('checked');
        nodeUpgradeOptions.Reboot = $("#chkReboot").prop('checked');
        nodeUpgradeOptions.Unmanage = nodeUpgradeOptions.Reboot ? $("#chkUnmanage").prop('checked') : false;
        nodeUpgradeOptions.Script = $("#txtScriptPreview").val();
        nodeUpgradeOptions.RestoreConfigs = $("#chkRestoreConfig").prop('checked');
        nodeUpgradeOptions.VerifyUpgrade = $("#chkVerifyUpgrade").prop('checked');

        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareUpgradeWizard_UpdatingWaitDialogMessage;E=js}');
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "UpdateFirmwareOperationNodeOptions", { coreNodeId: coreNodeId, nodeUpgradeOptions: nodeUpgradeOptions },
            function (result) {
                waitMsg.hide();

                var d = JSON.parse(result);
                if (d.Success) {
                    if (onSuccess !== undefined) {
                        onSuccess();
                    }
                }
            }
        );
    };

    var renderFirmwareImages = function (firmwareImages, onSuccess) {
        $("#firmwareImages").empty();

        firmwareImages.forEach(function (image) {

            SW.NCM.callWebMethod('/Orion/NCM/Services/FirmwareOperations.asmx/GetFirmwareImageDetails', { imageId: image.ImageId },
                function (result) {
                    var imageDetails = JSON.parse(result.d);

                    $("#firmwareImages").append(
                        $(String.format("<tr imageType='{0}' imageId='{1}' imageSize={2}>",
                            image.ImageType,
                            imageDetails.IsImageExist ? image.ImageId : null,
                            imageDetails.Size)).append($("<td>").html(image.ImageType))
                            .append($("<td class='firmware-image-details'>").html(imageDetails.IsImageExist
                                ? String.format("<b>{0}</b> {1}",
                                    imageDetails.FileName,
                                    parseSize(imageDetails.Size))
                                : "<i>@{R=NCM.Strings;K=FirmwareUpgradeWizard_NotDefinedLabel;E=js}</i>"))
                            .append($("<td>").html(!imageDetails.IsImageExist ? "<span class='firmware-image-validator sw-validation-error' style='display:none'>@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageRequiredMessage;E=js}</span" : ""))
                            .append($("<td>")
                                .append($('<a class=" sw-btn-small sw-btn" automation="Select New Image From Repository"><span class="sw-btn-c"><span class="sw-btn-t">@{R=NCM.Strings;K=FirmwareUpgradeWizard_SelectNewImageFromRepositoryButtonText;E=js}</span></span></a>')
                                    .click(showSelectFirmwareImageWindow)))
                    );

                    if (onSuccess !== undefined) {
                        onSuccess();
                    }
                });
        });
    };

    var getFirmwareImages = function () {
        var firmwareImages = [];

        $("#firmwareImages tr").each(function () {
            var row = $(this);

            var firmwareImage = {};
            firmwareImage.ImageType = row.attr("imageType");
            firmwareImage.ImageId = row.attr("imageId");
            firmwareImage.ImageSize = row.attr("imageSize");
            firmwareImages.push(firmwareImage);
        });
        return firmwareImages;
    };

    var validateFirmwareImages = function() {
        var isValidFirmwareImages = true;
        $("#firmwareImages tr").each(function () {
            var row = $(this);
            var validator = row.find(".firmware-image-validator");
            var imageId = row.attr("imageId");
            if (imageId === "") {
                if (validator) {
                    validator.css("display", "inline");
                }
                isValidFirmwareImages = false;
            } else {
                if (validator) {
                    validator.css("display", "none");
                }
            }
        });
        return isValidFirmwareImages;
    }

    var loadNodeOptions = function (coreNodeId) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareUpgradeWizard_LoadingWaitDialogMessage;E=js}');
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "GetFirmwareOperationNodeOptions", { coreNodeId: coreNodeId },
            function (result) {
                waitMsg.hide();

                var nodeOptions = JSON.parse(result);

                $("#freeSpaceValidatorHolder").empty();
                $("#freeSpaceValidatorHolder").hide();

                $("#txtLog").val(nodeOptions.Log);

                $("#txtUploadImagePath").val(nodeOptions.UploadImagePath);

                expandUpgradeOptions(nodeOptions.ExpandUpgradeOptions);

                if (nodeOptions.BackupFirmwareImageCommand)
                    $("#trBackupExistingImagePath").show();
                else
                    $("#trBackupExistingImagePath").hide();
                $("#chkBackupExisingFirmwareImage").prop('checked', nodeOptions.BackupFirmwareImageCommand ? nodeOptions.BackupExisingFirmwareImage : false);
                $("#txtBackupExistingImagePath").val(nodeOptions.BackupExistingImagePath);
                toggleOption(nodeOptions.BackupExisingFirmwareImage, 'tableBackupExistingImagePath', 'rfvBackupExistingImagePath');

                if (nodeOptions.DeleteFirmwareImageCommand)
                    $("#trDeleteExisingFirmwareImage").show();
                else
                    $("#trDeleteExisingFirmwareImage").hide();
                $("#chkDeleteExisingFirmwareImage").prop('checked', nodeOptions.DeleteFirmwareImageCommand ? nodeOptions.DeleteExisingFirmwareImage : false);
                $("#txtDeleteExistingImagePath").val(nodeOptions.DeleteExistingImagePath);
                toggleOption(nodeOptions.DeleteExisingFirmwareImage, 'tableDeleteExistingImagePath', 'rfvDeleteExistingImagePath');

                if (nodeOptions.UpdateConfigRegisterCommand)
                    $("#trUpdateConfigRegister").show();
                else
                    $("#trUpdateConfigRegister").hide();
                $("#chkUpdateConfigRegister").prop('checked', nodeOptions.UpdateConfigRegisterCommand ? nodeOptions.UpdateConfigRegister : false);

                $("#chkSaveConfigToNVRAM").prop('checked', nodeOptions.SaveConfigToNVRAM);
                $("#chkBackupRunningAndStartupConfigsBeforeUpgrade").prop('checked', nodeOptions.BackupRunningAndStartupConfigsBeforeUpgrade);
                $("#chkBackupRunningAndStartupConfigsAfterUpgrade").prop('checked', nodeOptions.BackupRunningAndStartupConfigsAfterUpgrade);

                if (nodeOptions.UpdateBootVariableCommand)
                    $("#trUpdateBootVariable").show();
                else
                    $("#trUpdateBootVariable").hide();
                $("#chkUpdateBootVariable").prop('checked', nodeOptions.UpdateBootVariableCommand ? nodeOptions.UpdateBootVariable : false);

                if (nodeOptions.IsFIPSModeEnabled) {
                    $("#trVerifyUploadImage").hide();
                    $("#chkVerifyUploadImage").prop('checked', false);
                } else {
                    if (nodeOptions.VerifyUploadCommand) {
                        $("#trVerifyUploadImage").show();
                    } else {
                        $("#trVerifyUploadImage").hide();
                    }
                    $("#chkVerifyUploadImage").prop('checked', nodeOptions.VerifyUploadCommand ? nodeOptions.VerifyUploadImage : false);
                    toggleWarning(true, 'verifyUploadImageWarning');
                }

                if (nodeOptions.IsFIPSModeEnabled) {
                    $("#trVerifyBackupImage").hide();
                    $("#chkVerifyBackupImage").prop('checked', false);
                    $("#chkVerifyBackupImage").attr('VerifyBackupCommand', false);
                } else {
                    if (nodeOptions.BackupExisingFirmwareImage && nodeOptions.VerifyBackupCommand) {
                        $("#trVerifyBackupImage").show();
                    } else {
                        $("#trVerifyBackupImage").hide();
                    }
                    $("#chkVerifyBackupImage").prop('checked', nodeOptions.BackupExisingFirmwareImage && nodeOptions.VerifyBackupCommand ? nodeOptions.VerifyBackupImage : false);
                    $("#chkVerifyBackupImage").attr('VerifyBackupCommand', nodeOptions.VerifyBackupCommand);
                    toggleWarning(true, 'verifyBackupImageWarning');
                    toggleBackup(nodeOptions.BackupExisingFirmwareImage && nodeOptions.VerifyBackupCommand, 'trVerifyBackupImage');
                }

                if (nodeOptions.RebootCommand)
                    $("#trReboot").show();
                else
                    $("#trReboot").hide();
                $("#chkReboot").prop('checked', nodeOptions.RebootCommand ? nodeOptions.Reboot : false);
                $("#chkReboot").attr('RebootRequired', nodeOptions.RebootRequired);
                toggleReboot(nodeOptions.RebootCommand ? nodeOptions.Reboot : false, 'UnmanageHolder');
                $("#chkUnmanage").prop('checked', nodeOptions.Unmanage);

                if (nodeOptions.VerifyUpgradeCommand)
                    $("#trVerifyUpgrade").show();
                else
                    $("#trVerifyUpgrade").hide();
                $("#chkVerifyUpgrade").prop('checked', nodeOptions.VerifyUpgradeCommand ? nodeOptions.VerifyUpgrade : false);

                $("#txtScriptPreview").val(nodeOptions.Script);

                $("#chkRestoreConfig").prop('checked', nodeOptions.RestoreConfigs);

                if (nodeOptions.IsRollBack) {
                    $("#chkBackupExisingFirmwareImage").prop('checked', false);
                    $("#trBackupExistingImagePath").hide();
                    $("#trSaveConfigToNVRAM").hide();
                    $("#trBackupLatestConfig").hide();
                    $("#trBackupRunningAndStartupConfigs").hide();
                    $("#trVerifyBackupImage").hide();

                    if (!isGuidEmpty(nodeOptions.RunningConfigIDBeforeUpgrade) && !isGuidEmpty(nodeOptions.StartupConfigIDBeforeUpgrade)) {
                        $("#trRestoreConfig").show();
                    } else {
                        $("#trRestoreConfig").hide();
                    }
                }
                else {
                    $("#trSaveConfigToNVRAM").show();
                    $("#trBackupLatestConfig").show();
                    $("#trBackupRunningAndStartupConfigs").show();
                    $("#trRestoreConfig").hide();
                }

                renderFirmwareImages(nodeOptions.FirmwareImages, function () {
                    validateFreeSpace();
                });
            });
    };

    isGuidEmpty = function (guid) {
        return guid === "00000000-0000-0000-0000-000000000000";
    };

    toggleOption = function (checked, controlHolderId, validatorId) {
        var confirmed = $("#rbConfirmed").prop('checked');
        if (checked) {
            $('#' + controlHolderId).show();
            if (confirmed)
                ValidatorEnable($('#' + validatorId)[0], true);
        }
        else {
            $('#' + controlHolderId).hide();
            ValidatorEnable($('#' + validatorId)[0], false);
        }
        resize();
    };

    toggleWarning = function (checked, controlHolderId) {
        if (!checked) {
            $('#' + controlHolderId).css('display', 'table-cell');
        }
        else {
            $('#' + controlHolderId).css('display', 'none');
        }
    };

    toggleBackup = function (checked, controlHolderId) {
        var VerifyBackupCommand = JSON.parse($('#chkVerifyBackupImage').attr('VerifyBackupCommand'));
        if (checked && VerifyBackupCommand) {
            $('#' + controlHolderId).show();
        }
        else {
            $('#' + controlHolderId).hide();
        }
        resize();
    };

    toggleReboot = function (checked, controlHolderId) {
        if (checked) {
            $('#' + controlHolderId).show();
        }
        else {
            $('#' + controlHolderId).hide();
        }
        resize();
    };

    showRebootRequiredWarning = function (rebootCheckboxEl) {
        var checked = rebootCheckboxEl.checked;
        var rebootRequired = JSON.parse(rebootCheckboxEl.getAttribute('RebootRequired'));
        if (rebootRequired) {
            toggleWarning(checked, 'rebootRequiredWarning');
        }
    };

    showSelectFirmwareImageWindow = function () {
        var machineType = $(".SelectedGroupItem a").attr('machineType');
        var td = $(this).parent();

        SW.NCM.FirmwareImagesRadioGrid.show(machineType, function (imageId, fileName, fileSize) {
            var tr = td.parent();
            tr.attr("imageId", imageId);
            tr.attr("imageSize", fileSize);
            tr.find(".firmware-image-details").html(String.format("<b>{0}</b> {1}", fileName, parseSize(fileSize)));

            if (validateFirmwareImages()) {
                validateFreeSpaceInternal();
            }
        });
    };

    validateFreeSpace = function () {
        if (!validateNodeOptions())
            return;

        validateFreeSpaceInternal();
    };

    var validateFreeSpaceInternal = function () {
        $("#freeSpaceValidatorHolder").empty();

        var firmwareImages = getFirmwareImages();
        var coreNodeId = $(".SelectedGroupItem a").attr('coreNodeId');
        var uploadImagePath = $("#txtUploadImagePath").val();
        var deleteExisingFirmwareImage = $("#chkDeleteExisingFirmwareImage").prop('checked');
        var deleteExistingImagePath = $("#txtDeleteExistingImagePath").val();

        if (!SW.NCM.Firmware.HidePathToUploadImage) {
            SW.NCM.callWebMethod('/Orion/NCM/Services/FirmwareOperations.asmx/ValidateFreeSpace', { coreNodeId: coreNodeId, uploadImagePath: uploadImagePath, firmwareImages: firmwareImages, deleteExisingFirmwareImage: deleteExisingFirmwareImage, deleteExistingImagePath: deleteExistingImagePath },
                function (result) {
                    var d = JSON.parse(result.d);

                    SW.NCM.Firmware.ConfirmedNodes = d.ConfirmedNodes;
                    SW.NCM.Firmware.AllNodes = d.AllNodes;
                    SW.NCM.Firmware.NodesWithFreeSpaceIssues = d.NodesWithFreeSpaceIssues;
                    updateNotification(d.ConfirmedNodes, d.AllNodes);

                    var freeSpaceValidatorMsgTemplate = ' \
                            <div class="sw-suggestion {0}" > \
                                <div class="sw-suggestion-icon"></div> \
                                {1} \
                            </div>';

                    $("#freeSpaceValidatorHolder").empty();
                    if (d.Status === 2) { //Success
                        $("#freeSpaceValidatorHolder").html(String.format(freeSpaceValidatorMsgTemplate, 'sw-suggestion-pass', '@{R=NCM.Strings;K=FirmwareUpgradeWizard_SuccessFreeSpaceMessage;E=js}'));
                    }
                    else if (d.Status === 1) { //Warning
                        $("#freeSpaceValidatorHolder").html(String.format(freeSpaceValidatorMsgTemplate, 'sw-suggestion-warn', '@{R=NCM.Strings;K=FirmwareUpgradeWizard_WarningFreeSpaceRemoveMessage;E=js}'));
                    }
                    else if (d.Status === 3) { //Fail
                        $("#freeSpaceValidatorHolder").html(String.format(freeSpaceValidatorMsgTemplate, 'sw-suggestion-fail', '@{R=NCM.Strings;K=FirmwareUpgradeWizard_FailFreeSpaceMessage;E=js}'));

                        var confirmed = Boolean.parse($(".SelectedGroupItem img").attr('confirmed'));
                        if (confirmed) {
                            updateNodeIcon(coreNodeId, !confirmed);
                        }
                    }
                    else { //Unknown
                        $("#freeSpaceValidatorHolder").html(String.format(freeSpaceValidatorMsgTemplate, 'sw-suggestion-warn', '@{R=NCM.Strings;K=FirmwareUpgradeWizard_UnknownFreeSpaceMessage;E=js}'));
                    }
                    $("#freeSpaceValidatorHolder").show();
                });
        }
    };

    var getSelectedCoreNodeIds = function () {
        var coreNodeIds = [];
        $.each($("input[name='firmware-operation-node']:checked"), function () {
            coreNodeIds.push($(this).val());
        });
        return coreNodeIds;
    };

    var checkDeleteExisingFirmwareImageOnNodes = function (coreNodeIds, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "CheckDeleteExisingFirmwareImageOnNodes", { coreNodeIds: coreNodeIds },
            function () {
                onSuccess();
            }
        );
    };

    var confirmFirmwareOperationNodeInternal = function (coreNodeId, confirmed) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "ConfirmFirmwareOperationNode", { coreNodeId: coreNodeId, confirmed: confirmed },
            function (result) {
                if (result) {
                    var d = JSON.parse(result);
                    if (d.Success) {
                        SW.NCM.Firmware.ConfirmedNodes = d.ConfirmedNodes;
                        SW.NCM.Firmware.AllNodes = d.AllNodes;
                        SW.NCM.Firmware.NodesWithFreeSpaceIssues = d.NodesWithFreeSpaceIssues;
                        updateNotification(d.ConfirmedNodes, d.AllNodes);
                        updateNodeIcon(coreNodeId, confirmed);
                    }
                }
            });
    };

    var confirmFirmwareOperationNodesInternal = function (coreNodeIds) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareOperations.asmx",
            "ConfirmFirmwareOperationNodes", { coreNodeIds: coreNodeIds },
            function (result) {
                var d = JSON.parse(result);
                if (d.Success) {
                    SW.NCM.Firmware.ConfirmedNodes = d.ConfirmedNodes;
                    SW.NCM.Firmware.AllNodes = d.AllNodes;
                    SW.NCM.Firmware.NodesWithFreeSpaceIssues = d.NodesWithFreeSpaceIssues;
                    updateNotification(d.ConfirmedNodes, d.AllNodes);

                    d.Nodes.forEach(function (node) {
                        updateNodeIcon(node.CoreNodeID, node.Confirmed);
                    });
                }
            });
    };

    var confirmClick = function () {
        var selectedCoreNodeId = $(".SelectedGroupItem a").attr('coreNodeId');
        var checkedCoreNodeIds = getSelectedCoreNodeIds();

        var index = checkedCoreNodeIds.indexOf(selectedCoreNodeId);
        if (index >= 0) {
            updateNodeOptions(selectedCoreNodeId, function () {
                confirmFirmwareOperationNodesInternal(checkedCoreNodeIds);
            });
        } else {
            confirmFirmwareOperationNodesInternal(checkedCoreNodeIds);
        }
    };

    var btnConfirm;
    var renderConfirmButton = function () {
        btnConfirm = new Ext.Button({
            renderTo: 'confirmBtn',
            id: 'confirmButton',
            text: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_ConfirmButtonText;E=js}',
            tooltip: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_ConfirmButtonTooltip;E=js}',
            icon: '/Orion/NCM/Resources/images/icon_ok.png',
            cls: 'x-btn-text-icon',
            handler: function () { confirmClick(); }
        });
    };

    selectionChange = function () {
        var coreNodeIds = getSelectedCoreNodeIds();
        var needsAtLeastOneSelected = coreNodeIds.length === 0;
        btnConfirm.setDisabled(needsAtLeastOneSelected);

        if ($("input[name='firmware-operation-node']:not:checked").length > 0) {
            $("input[name='select-all']").prop('checked', false);
        }
    };

    selectAllSelectionChange = function () {
        if ($("input[name='select-all']").is(':checked')) {
            $("input[name='firmware-operation-node']").prop('checked', true);
            btnConfirm.setDisabled(false);
        } else {
            $("input[name='firmware-operation-node']").prop('checked', false);
            btnConfirm.setDisabled(true);
        }       
    };

    return {
        init: function () {
            initFirmwareOperationNodes();
            updateNotification(SW.NCM.Firmware.ConfirmedNodes, SW.NCM.Firmware.AllNodes);
            
            renderConfirmButton();
            selectionChange();

            resize();
            $(window).resize(resize);
            $("#txtLog").mouseup(function () { resize(); });
        }
    };

}();

Ext.onReady(SW.NCM.Firmware.UpgradeOptions.init, SW.NCM.Firmware.UpgradeOptions);

SW.NCM.callWebMethod = function (url, data, success, failure) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (result) {
            if (success)
                success(result);
        },
        error: function (response) {
            if (failure)
                failure(response);
            else {
                alert(response);
            }
        }
    });
}