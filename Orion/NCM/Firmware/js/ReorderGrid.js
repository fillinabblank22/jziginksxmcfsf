﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.namespace('SW.NCM.Firmware');
Ext.QuickTips.init();

SW.NCM.Firmware.ReorderGrid = function () {

    var grid;

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());
        }
    }

    function renderReboot(value, meta, record) {
        return parseReboot(value);
    }

    var parseReboot= function(value) {
        if(value)
            return '@{R=NCM.Strings;K=WEBJS_VK_57;E=js}';
        else
            return '@{R=NCM.Strings;K=WEBJS_VK_58;E=js}';
    }

    var refreshGrid = function (search, callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { };
        grid.store.load();
    }

    return {
        init: function () {
            var dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/FirmwareOperations.asmx/LoadFirmwareOperationNodesOrder",
                                [
                                    { name: 'CoreNodeID', mapping: 0 },
                                    { name: 'Caption', mapping: 1 },
                                    { name: 'ImageFileName', mapping: 2 },
                                    { name: 'Reboot', mapping: 3 },
                                    { name: 'EngineName', mapping: 4 },
                                ],
                                "Caption");
            dataStore.sortInfo = null;

            var selModel = new Ext.grid.RowSelectionModel({
                singleSelect: true
            });

            grid = new Ext.grid.GridPanel({
                id: 'reorderGrid',
                store: dataStore,
                selModel: selModel,
                ddGroup: 'ddGroup',
                enableDragDrop: true,               
                columns: [{
                    header: 'CoreNodeID',
                    width: 80, 
                    hidden: true,
                    hideable: false, 
                    dataIndex: 'CoreNodeID'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_88;E=js}',
                    width: 200,
                    sortable: false,
                    menuDisabled: true,
                    dataIndex: 'Caption',
                    css: 'height:23px;'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_338;E=js}',
                    width: 400,
                    sortable: false,
                    menuDisabled: true,
                    dataIndex: 'ImageFileName',
                    css: 'height:23px;'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_337;E=js}',
                    width: 80,
                    sortable: false,
                    menuDisabled: true,
                    dataIndex: 'Reboot',
                    css: 'height:23px;',
                    renderer: renderReboot
                }, {
                     header: '@{R=NCM.Strings;K=FirmwareUpgrade_GridColumn_PollingEngine;E=js}',
                     width: 200,
                     sortable: false,
                     menuDisabled: true,
                     dataIndex: 'EngineName',
                     css: 'height:23px;'
                 }
                ],

                layout: 'fit',
                stateful: false,
                autoScroll: 'true',
                loadMask: true,
                width: 300,
                height: 300,
                stripeRows: false
            });
            grid.render('Grid');

            var dropTarget = new Ext.dd.DropTarget(grid.container, {
                ddGroup: 'ddGroup',
                copy: false,
                notifyDrop: function (dd, e, data) {
                    var selModel = grid.getSelectionModel();
                    var row = selModel.getSelected();
                    if (selModel.hasSelection() && dd.getDragData(e)) {
                        var rowIndex = dd.getDragData(e).rowIndex;
                        if (rowIndex != undefined) {
                            grid.getStore().remove(dataStore.getById(row.id));
                            grid.getStore().insert(rowIndex, row);
                            selModel.selectRecords([row]);
                        }
                    }
                }
            });

            refreshGrid();

            gridResize();
            $(window).resize(gridResize);
        },

        moveRow: function (direction) {
            var selModel = grid.getSelectionModel();
            var dataStore = grid.getStore();
            if (selModel.hasSelection()) {
                var row = selModel.getSelected();
                var rowIndex = dataStore.indexOfId(row.id);
                switch(direction) {
                    case 'top':
                        if (rowIndex > 0) {
                            dataStore.remove(dataStore.getById(row.id));
                            dataStore.insert(0, row);
                        }
                        break;
                    case 'up':
                        if (rowIndex > 0) {
                            dataStore.remove(dataStore.getById(row.id));
                            dataStore.insert(rowIndex - 1, row);
                        }
                        break;
                    case 'down':
                        if (rowIndex < dataStore.getCount() - 1) {
                            dataStore.remove(dataStore.getById(row.id));
                            dataStore.insert(rowIndex + 1, row);
                        }
                        break;
                    case 'bottom':
                        if (rowIndex < dataStore.getCount() - 1) {
                            dataStore.remove(dataStore.getById(row.id));
                            dataStore.insert(dataStore.getCount(), row);
                        }
                        break;
                }
                selModel.selectRecords([row]);
            }
        },

        save: function () {
            var orderedList = new Object();
            var dataStore = grid.getStore();
            dataStore.data.each(function (record, index, length) {
                orderedList[record.data.CoreNodeID] = index;
            });
            
            SW.NCM.callWebMethod('/Orion/NCM/Services/FirmwareOperations.asmx/SaveFirmwareOperationNodesOrder', { orderedList: orderedList }, function () { });
        }
    }

}();

Ext.onReady(SW.NCM.Firmware.ReorderGrid.init, SW.NCM.Firmware.ReorderGrid);

SW.NCM.callWebMethod = function (url, data, success, failure) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (result) {
            if (success)
                success(result);
        },
        error: function (response) {
            if (failure)
                failure(response);
            else {
                alert(response);
            }
        }
    });
}