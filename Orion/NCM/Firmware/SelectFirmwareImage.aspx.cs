﻿using System;
using System.Collections.Generic;
using SolarWinds.NCMModule.Web.Resources.Firmware;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Firmware_SelectFirmwareImage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = FirmwareUpgradeWizardController.PageTitle;
        if (!Page.IsPostBack)
        {
            if (FirmwareUpgradeWizardController.FirmwareImages == null)
            {
                var firmwareDefinition = FirmwareUpgradeWizardController.GetCurrentFwDefinition();

                ncmSelectFirmwareImages.LoadFirmwareImages(firmwareDefinition != null
                    ? firmwareDefinition.FirmwareImages
                    : new List<FirmwareImage>());
            }
            else
            {
                ncmSelectFirmwareImages.LoadFirmwareImages(FirmwareUpgradeWizardController.FirmwareImages);
            }
        }
    }

    protected void Next_Click(object sender, FirmwareUpgradeWizardEventArgs e)
    {
        if (Page.IsValid)
        {
            FirmwareUpgradeWizardController.FirmwareImages = ncmSelectFirmwareImages.GetSelectedFirmwareImages();
        }
    }
}
