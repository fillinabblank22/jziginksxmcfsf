﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectFirmwareImages.ascx.cs" Inherits="Orion_NCM_Firmware_Controls_SelectFirmwareImages" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

<input ID="IsStorageLocationSetField" type="hidden" value="<%=IsStorageLocationSet%>"/>
 
<link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
<script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
<script type="text/javascript" src="/Orion/NCM/Admin/Firmware/js/FirmwareImagesRadioGrid.js"></script>
<script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
<script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>

<style type="text/css">
    .firmware-images tbody tr td {
        padding:5px 5px 5px 0px;
    }

    .firmware-images tbody tr:not(:last-child) td {
        border-bottom: solid 1px #999999;
    }

    .add-new-image {
        margin-bottom: 0px;
    }
</style>

<ncm:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/> 

<script type="text/javascript">
    $(window).resize(function() { SW.NCM.FirmwareImagesRadioGrid.resize(); });

    var showSelectFirmwareImageWindow = function(obj) {
        var table = $("#firmwareImages")[0];

        var isStorageLocationSetField = document.getElementById("IsStorageLocationSetField").value.toLowerCase() == "true";

        if (isStorageLocationSetField) {
            SW.NCM.FirmwareImagesRadioGrid.show(null,
                function (imageId, fileName, fileSize) {
                    table.rows[obj.attributes.DataItemIndex.value].cells[1].children[0].value = imageId;
                    table.rows[obj.attributes.DataItemIndex.value].cells[1].children[1].innerHTML =
                        String.format("<b>{0}</b> {1}", fileName, parseSize(fileSize));
                });
        } else {
            var errorMessage = "<%= Resources.NCMWebContent.WEBDATA_AP_1 %> <br/><a class='sw-suggestion-link' href='/Orion/NCM/Admin/Firmware/FirmwareStorageLocation.aspx'>" +
                "<%= Resources.NCMWebContent.WEBDATA_AP_2 %></a>";
            Ext.Msg.show({
                title: 'Error',
                msg: errorMessage,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        }
    }

    var validateFirmwareImagesCount = function(sender, args) {
        var table = $("#firmwareImages")[0];
        if (table) {
            args.IsValid = table.rows.length > 1;
        } else {
            args.IsValid = false;
        }
    }

    var validateFirmwareImage = function(sender, args) {
        var table = $("#firmwareImages")[0];
        if (table) {
            var firmwareImageId = table.rows[sender.attributes.DataItemIndex.value].cells[1].children[0].value;
            args.IsValid = firmwareImageId.length > 0 && parseInt(firmwareImageId) > 0;
        } else {
            args.IsValid = true;
        }
    }

    var parseSize = function (size) {
        if (size >= 1073741824)
            return String.format('<%=Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInGBLabel%>', (size / 1073741824).toFixed(2), size.toLocaleString());
        else if (size >= 1048576)
            return String.format('<%=Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInMBLabel%>', (size / 1048576).toFixed(2), size.toLocaleString());
        else if (size >= 1024)
            return String.format('<%=Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInKBLabel%>', (size / 1024).toFixed(2), size.toLocaleString());
        else
            return String.format('<%=Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInBytesLabel%>', size.toLocaleString());
    }
</script>

<div>
    <asp:UpdatePanel ID="updatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:Repeater runat="server" ID="repeater" OnItemCommand="repeater_ItemCommand">
                <HeaderTemplate>                            
                    <table id="firmwareImages" class="firmware-images" cellpadding="0" cellspacing="0">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblFirmwareImageType" runat="server" Text='<%# HttpUtility.HtmlEncode(Eval(@"ImageType")) %>'></asp:Label>             
                            </td>
                            <td>
                                <asp:HiddenField ID="hfFirmwareImageId" runat="server" Value='<%# IsFirmwareImageExist(Eval(@"ImageId")) ? Eval(@"ImageId") : null %>'></asp:HiddenField>
                                <asp:Label ID="lblFirmwareImageFileNameAndSize" runat="server" Text='<%# RenderFirmwareImageFileNameAndSize(Eval(@"ImageId")) %>'></asp:Label>
                            </td>
                            <td>
                                <asp:CustomValidator ID="cvValidateFirmwareImage" runat="server" DataItemIndex="<%#Container.ItemIndex %>" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_FirmwareImageRequiredMessage %>" ClientValidationFunction="validateFirmwareImage" Enabled="<%#EnableValidateFirmwareImage %>"></asp:CustomValidator>
                            </td>
                            <td>
                                <orion:LocalizableButton runat="server" ID="btnSelectNewImageFromRepository" DataItemIndex="<%#Container.ItemIndex %>" LocalizedText="CustomText" DisplayType="Small" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_SelectNewImageFromRepositoryButtonText %>" CausesValidation="false" OnClientClick="showSelectFirmwareImageWindow(this); return false;" />
                            </td>
                            <td runat="server" id="tdRemove" visible="<%#!ReadOnly %>">
                                <asp:LinkButton ID="btnRemove" runat="server" CommandName="Delete" CausesValidation="false">
                                    <img src="/Orion/NCM/Resources/images/icon_delete.png" />
                                </asp:LinkButton>
                            </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                        <tr runat="server" id="trFooter" visible="<%#!ReadOnly %>">
                            <td colspan="2">
                                <asp:TextBox ID="txtNewFirmwareImageType" runat="server" BorderColor="#999999" BorderWidth="1px" Height="18px" style="width:100%;min-width:300px;" placeholder="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_FirmwareImageTypeEmptyText %>" ValidationGroup="AddNewFirmwareImageType"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvNewFirmwareImageType" runat="server" Display="Dynamic" ControlToValidate="txtNewFirmwareImageType" ValidationGroup="AddNewFirmwareImageType" ErrorMessage="*" />
                            </td>
                            <td>
                                <orion:LocalizableButton runat="server" ID="btnAddNewFirmwareImageType" CssClass="add-new-image" CommandName="Add" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_568 %>" DisplayType="Small" CausesValidation="true" ValidationGroup="AddNewFirmwareImageType" />
                            </td>
                            <td>
                                <asp:CustomValidator ID="cvValidateFirmwareImagesCount" runat="server" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_AtLeastOneFirmwareImageRequiredMessage %>" ClientValidationFunction="validateFirmwareImagesCount"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>