﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FirmwareUpgradeWizardContainer.ascx.cs" Inherits="Orion_NCM_Firmware_Controls_FirmwareUpgradeWizardContainer" %>
<%@ Import Namespace="SolarWinds.NCMModule.Web.Resources.Firmware" %>

<orion:Include runat="server" File="styles/ProgressIndicator.css" />

<div style="width:80%;padding:10px;">
    <div>
        <asp:Literal ID="wizardHeader" runat="server"></asp:Literal>
    </div>

    <div style="padding-top:20px;font-size:11pt;font-weight:bold;border-bottom:#e6e6dd 1px solid;padding-bottom:5px;">
        <asp:Literal ID="stepTitle" runat="server"></asp:Literal>
    </div>

    <div style="padding:10px 0px 10px 0px;">
        <asp:PlaceHolder runat="server" ID="Container" />
    </div>

    <div style="padding-top:10px;">
        <script>
            var isDemo = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
            var currentStepIndex = "<%=FirmwareUpgradeWizardController.CurrentStepIndex.ToString() %>";

            function checkDemo() {
                if (currentStepIndex === "<%= eFirmwareUpgradeWizardStep.SelectNodes %>" && isDemo) {
                    demoAction("NCM_FirmwareUpgradeWizardContainer_SelectNodes", this);
                    return false;
                }
                return true;
            }
        </script>

        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="text-align:right;">
                    <span style="padding-right:5px;">
                        <orion:LocalizableButton runat="server" ID="btnBack" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" OnClick="btnBack_Click" />
                    </span>
                    <span style="padding-right:5px;">
                        <orion:LocalizableButton runat="server" ID="btnNext" LocalizedText="Next" DisplayType="Primary" CausesValidation="true" OnClick="btnNext_Click" OnClientClick="return checkDemo();" />
                    </span>
                    <span style="padding-right:5px;padding-left:20px;">
                        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClick="btnCancel_Click" />
                    </span>
                </td>
            </tr>
        </table>
    </div>
</div>
