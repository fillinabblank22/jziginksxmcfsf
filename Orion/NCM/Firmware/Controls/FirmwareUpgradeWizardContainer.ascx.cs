﻿using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Firmware;
using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NCM_Firmware_Controls_FirmwareUpgradeWizardContainer : System.Web.UI.UserControl
{
    public delegate void dlgWizardClick(object sender, FirmwareUpgradeWizardEventArgs e);

    public event dlgWizardClick BackClick;
    public event dlgWizardClick NextClick;
    public event dlgWizardClick CancelClick;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return Container; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnNextClientClick
    {
        get { return btnNext.OnClientClick; }
        set { btnNext.OnClientClick = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnBackClientClick
    {
        get { return btnBack.OnClientClick; }
        set { btnBack.OnClientClick = value; }
    }

    public string ButtonNextClientID
    {
        get { return btnNext.ClientID; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidateSessionData();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetupWizard();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (BackClick != null)
        {
            var args = new FirmwareUpgradeWizardEventArgs();
            BackClick.Invoke(sender, args);
            if (!args.IsValid) return;
        }

        Page.Response.Redirect(FirmwareUpgradeWizardController.GetBackStepUrl());
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (FirmwareUpgradeWizardController.CurrentStepIndex == eFirmwareUpgradeWizardStep.SelectNodes &&
            CommonHelper.IsDemoMode())
        {
            return;
        }

        var steps = FirmwareUpgradeWizardController.GetWizardSteps();

        if (NextClick != null)
        {
            var args = new FirmwareUpgradeWizardEventArgs();
            NextClick.Invoke(sender, args);
            if (!args.IsValid) return;
        }

        if ((int)FirmwareUpgradeWizardController.CurrentStepIndex + 1 == steps.Count)
            Page.Response.Redirect(FirmwareUpgradeWizardController.GetFinishStepUrl());
        else
            Page.Response.Redirect(FirmwareUpgradeWizardController.GetNextStepUrl());
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (CancelClick != null)
        {
            var args = new FirmwareUpgradeWizardEventArgs();
            CancelClick.Invoke(sender, args);
            if (!args.IsValid) return;
        }

        Page.Response.Redirect(FirmwareUpgradeWizardController.GetCancelStepUrl());
    }

    private void SetupWizard()
    {
        var steps = FirmwareUpgradeWizardController.GetWizardSteps();

        if (FirmwareUpgradeWizardController.CurrentStepIndex == eFirmwareUpgradeWizardStep.CreateNew)
        {
            btnBack.Visible = false;
            btnNext.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.Next;
        }
        else if (FirmwareUpgradeWizardController.CurrentStepIndex == eFirmwareUpgradeWizardStep.SelectNodes)
        {
            btnNext.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.CustomText;
            btnNext.Text = Resources.NCMWebContent.FirmwareUpgradeWizard_StartCollectingDataButtonText;
        }
        else if ((int)FirmwareUpgradeWizardController.CurrentStepIndex + 1 == steps.Count)
        {
            btnBack.Visible = true;
            btnNext.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.Finish;
        }
        else
        {
            btnBack.Visible = true;
            btnNext.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.Next;
        }

        StringBuilder header = new StringBuilder();
        header.Append(@"<div class='ProgressIndicator'>");
        for (int i = 0; i < steps.Count; i++)
        {
            bool isActive;
            if ((int)FirmwareUpgradeWizardController.CurrentStepIndex == i)
            {
                header.AppendFormat(@"<div class='PI_on'>&nbsp;{0}</div>", steps[(eFirmwareUpgradeWizardStep)i].Title);
                header.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_on_off_sm.gif' />");
            }
            else
            {
                header.AppendFormat(@"<div class='PI_off'>&nbsp;{0}</div>", steps[(eFirmwareUpgradeWizardStep)i].Title);
                isActive = ((i < steps.Count - 1) && steps[FirmwareUpgradeWizardController.CurrentStepIndex].Equals(steps[(eFirmwareUpgradeWizardStep)(i + 1)]) ? true : false);
                if (isActive)
                    header.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_off_on_sm.gif' />");
                else
                    header.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif' />");
            }
        }
        header.Append(@"</div>");
        wizardHeader.Text = header.ToString();

        stepTitle.Text = steps[FirmwareUpgradeWizardController.CurrentStepIndex].Title;
    }

    private void ValidateSessionData()
    {
        if (FirmwareUpgradeWizardController.CurrentStepIndex > eFirmwareUpgradeWizardStep.SelectNodes && FirmwareUpgradeWizardController.FirmwareOperationNodes == null)
            Page.Response.Redirect(FirmwareUpgradeWizardController.GetReturnUrl());
    }
}
