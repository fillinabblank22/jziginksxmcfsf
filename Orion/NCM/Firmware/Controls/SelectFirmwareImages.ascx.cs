﻿using SolarWinds.Logging;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NCM_Firmware_Controls_SelectFirmwareImages : UserControl
{
    private readonly ISWrapper isWrapper = new ISWrapper();
    private static readonly Log log = new Log();

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ReadOnly { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool EnableValidateFirmwareImage { get; set; }
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool IsStorageLocationSet { get; private set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CheckFirmwareStorageLocation();
    }

    private void CheckFirmwareStorageLocation()
    {
        try
        {
            using (var proxy = isWrapper.Proxy)
            {
                var isSet = proxy.Cirrus.Settings.GetSetting(Settings.FirmwareStorageLocation, null, null);
                IsStorageLocationSet = !string.IsNullOrEmpty(isSet);
            }
        }
        catch
        {
            IsStorageLocationSet = false;
        }
    }

    public void LoadFirmwareImages(List<FirmwareImage> firmwareImages)
    {
        if (firmwareImages == null)
        {
            firmwareImages = new List<FirmwareImage>();
        }
        
        repeater.DataSource = firmwareImages;
        repeater.DataBind();
    }

    public List<FirmwareImage> GetSelectedFirmwareImages()
    {
        return repeater.Items.OfType<RepeaterItem>().Select(item =>
        {
            var hfFirmwareImageId = item.FindControl(@"hfFirmwareImageId") as HiddenField;
            var lblFirmwareImageType = item.FindControl(@"lblFirmwareImageType") as Label;
            return new FirmwareImage()
            {
                ImageId = !string.IsNullOrEmpty(hfFirmwareImageId.Value) ? int.Parse(hfFirmwareImageId.Value) : (int?)null,
                ImageType = HttpUtility.HtmlDecode((item.FindControl(@"lblFirmwareImageType") as Label).Text)
            };
        }).ToList();
    }

    protected void repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        var item = e.Item;
        if (item == null)
        {
            return;
        }

        var selectedFirmwareImages = GetSelectedFirmwareImages();

        if (e.CommandName.Equals(@"Delete"))
        {
            selectedFirmwareImages = GetSelectedFirmwareImages();
            selectedFirmwareImages.RemoveAt(item.ItemIndex);
        }

        if (e.CommandName.Equals(@"Add"))
        {
            var txtNewFirmwareImageType = item.FindControl(@"txtNewFirmwareImageType") as TextBox;
            var firmwareImageType = txtNewFirmwareImageType?.Text;
            if (!string.IsNullOrWhiteSpace(firmwareImageType))
            {
                selectedFirmwareImages.Add(new FirmwareImage() { ImageType = firmwareImageType });
                txtNewFirmwareImageType.Text = string.Empty;
            }
        }

        repeater.DataSource = selectedFirmwareImages;
        repeater.DataBind();
    }

    protected bool IsFirmwareImageExist(object objFirmwareImageId)
    {
        try
        {
            var intFirmwareImageId = objFirmwareImageId as int?;
            if (intFirmwareImageId.HasValue)
            {
                using (var proxy = isWrapper.Proxy)
                {
                    var dt = proxy.Query(@"SELECT ID FROM NCM.FirmwareUpgradeImages WHERE ID=@id",
                        new SolarWinds.InformationService.Contract2.PropertyBag() { { @"id", intFirmwareImageId.Value } });
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        return true;
                    }

                }
            }
            return false;
        }
        catch (Exception ex)
        {
            log.Error("Unable to get firmware image id.", ex);
            return false;
        }
    }

    protected string RenderFirmwareImageFileNameAndSize(object objFirmwareImageId)
    {
        try
        {
            if (!IsFirmwareImageExist(objFirmwareImageId))
            {
                return $@"<i>{Resources.NCMWebContent.WEBDATA_VK_1041}</i>";
            }

            var intFirmwareImageId = objFirmwareImageId as int?;
            if (intFirmwareImageId.HasValue)
            {
                using (var proxy = isWrapper.Proxy)
                {
                    var dt = proxy.Query(@"SELECT FileName, Size FROM NCM.FirmwareUpgradeImages WHERE ID=@id",
                        new SolarWinds.InformationService.Contract2.PropertyBag() {{@"id", intFirmwareImageId.Value}});
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var fileName = (string) dt.Rows[0]["FileName"];
                        var fileSize = (double) dt.Rows[0]["Size"];
                        return $@"<b>{fileName}</b> {ParseFileSize(fileSize)}";
                    }
                }
            }
            return $@"<i>{Resources.NCMWebContent.WEBDATA_VK_1041}</i>";
        }
        catch (Exception ex)
        {
            log.Error("Unable to get firmware image filename and size.", ex);
            return $@"<i>{Resources.NCMWebContent.WEBDATA_VK_1041}</i>"; ;
        }
    }

    private string ParseFileSize(double size)
    {
        if (size >= 1073741824)
        {
            return string.Format(Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInGBLabel, (size / 1024).ToString(@"0.00", CultureInfo.InvariantCulture), size.ToString(@"##,#", CultureInfo.InvariantCulture));
        }
        else if (size >= 1048576)
        {
            return string.Format(Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInMBLabel, (size / 1024).ToString(@"0.00", CultureInfo.InvariantCulture), size.ToString(@"##,#", CultureInfo.InvariantCulture));
        }
        else if (size >= 1024)
        {
            return string.Format(Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInKBLabel, (size / 1024).ToString(@"0.00", CultureInfo.InvariantCulture), size.ToString(@"##,#", CultureInfo.InvariantCulture));
        }
        else
        {
            return string.Format(Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImageSizeInBytesLabel, size.ToString(@"##,#", CultureInfo.InvariantCulture));
        }
    }
}