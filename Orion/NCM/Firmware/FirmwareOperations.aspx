﻿<%@  Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="FirmwareOperations.aspx.cs" Inherits="Orion_NCM_Firmware_FirmwareOperations" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagName="IconNCMSettings" %>
<%@ Register Src="~/Orion/NCM/NavigationTabBar.ascx" TagPrefix="ncm" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Firmware/js//FirmwareOperations.js"></script>
    
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server" /> 

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/Progress.css" />

    <style type="text/css">
        #Grid td
        {
            padding: 0px 0px 0px 0px;
            border: 0px;
        }
    </style>
    <script type="text/javascript">
        SW.NCM.IsDemoServer = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="Server">
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server" />
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMFirmwareUpgrade" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
    <%foreach (string setting in new string[] { @"PageSize" })
      { %>
    <input type="hidden" name="NCM_FirmwareOperations_<%=setting%>" id="NCM_FirmwareOperations_<%=setting%>"
        value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_FirmwareOperations_{setting}")%>' />
    <%}%>
    <asp:Panel ID="ContentContainer" runat="server">
        <div style="padding:10px;">
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td id="gridCell">
                        <div id="Grid" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="firmwareUpgradeLogDialog" class="x-hidden">
            <div id="firmwareUpgradeLogBody" class="x-panel-body">
                <div style="padding: 10px 0px 10px 0px;" id="firmwareUpgradeLogNotification"></div>
                <div id="firmwareUpgradeLogTextArea"></div>
            </div>
        </div>
    </asp:Panel>
    <div id="divError" style="padding:10px;" runat="server"/>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" runat="Server">
</asp:Content>