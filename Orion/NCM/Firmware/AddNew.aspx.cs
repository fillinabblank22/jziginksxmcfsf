﻿using SolarWinds.NCMModule.Web.Resources.Firmware;
using System;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Firmware_AddNew : System.Web.UI.Page
{
    private static readonly Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = FirmwareUpgradeWizardController.PageTitle;

        if (!Page.IsPostBack)
        {
            LoadDefinitionsDropDownList();
            PreloadData();
        }
    }

    protected void Next_Click(object sender, FirmwareUpgradeWizardEventArgs e)
    {
        if (Page.IsValid)
        {
            FirmwareUpgradeWizardController.FirmwareOperationName = txtOperationName.Text;
            FirmwareUpgradeWizardController.FirmwareDefinitionId = Convert.ToInt32(ddlFirmwareDefinitions.SelectedValue);
        }
    }

    private void LoadDefinitionsDropDownList()
    {
        ddlFirmwareDefinitions.DataSource = GetFirmwareDefinitionsList();
        ddlFirmwareDefinitions.DataValueField = @"ID";
        ddlFirmwareDefinitions.DataTextField = @"Name";
        ddlFirmwareDefinitions.DataBind();
    }

    private void PreloadData()
    {
        if (FirmwareUpgradeWizardController.FirmwareOperationName != null || FirmwareUpgradeWizardController.FirmwareDefinitionId != null)
        {
            txtOperationName.Text = FirmwareUpgradeWizardController.FirmwareOperationName;
            ddlFirmwareDefinitions.ClearSelection();
            ddlFirmwareDefinitions.SelectedValue = FirmwareUpgradeWizardController.FirmwareDefinitionId.ToString();
        }
    }

    private DataTable GetFirmwareDefinitionsList()
    {
        var swql = @"SELECT ID, Name FROM NCM.FirmwareDefinitions ";

        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                return proxy.Query(swql);
            }
        }

        catch (Exception ex)
        {
            _log.Error("Error in FirmwareOperations.ExecuteSWQL", ex);
        }

        return null;
    }

}