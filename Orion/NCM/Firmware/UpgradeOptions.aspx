﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="UpgradeOptions.aspx.cs" Inherits="Orion_NCM_Firmware_UpgradeOptions" %>

<%@ Register Src="~/Orion/NCM/Firmware/Controls/FirmwareUpgradeWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />

    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Firmware/js/UpgradeOptions.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Admin/Firmware/js/FirmwareImagesRadioGrid.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Firmware/js/UpgradeConfirmDialog.js"></script>
    
    <ncm:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>

    <style type="text/css">
        .expand-upgrade-options:hover {
            cursor: pointer;
        }

        .firmware-images tbody tr td {
            padding: 5px 5px 5px 0px;
        }

        .firmware-images tbody tr:not(:last-child) td {
            border-bottom: solid 1px #999999;
        }

        .x-toolbar {
            border-color: #fff;
            border-width: 1px;
        }

        table.ZebraStripes > tbody > tr:nth-child(odd) { background-color: #ffffff; }
        table.ZebraStripes > tbody > tr:nth-child(even) { background-color: #f6f6f6; }
    </style>

    <script type="text/javascript">
        SW.NCM.Firmware.ConfirmedNodes = <%=ConfirmedNodes%>;
        SW.NCM.Firmware.AllNodes = <%=AllNodes%>;
        SW.NCM.Firmware.NodesWithFreeSpaceIssues = <%=NodesWithFreeSpaceIssues%>;
        SW.NCM.Firmware.ButtonNextClientID = '<%=ButtonNextClientID%>';
        SW.NCM.Firmware.HidePathToUploadImage = <%=HidePathToUploadImage.ToString().ToLowerInvariant() %>;
        SW.NCM.IsDemoServer = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClientClick="return validateFunc();">
        <Content>
            <div style="padding-bottom:10px;">
                <%=Resources.NCMWebContent.FirmwareUpgradeWizard_UpgradeOptionsDescription%>
            </div>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="border:solid 1px #dcdbd7;width:20%;min-width:250px;" valign="top">
                        <div class="ncm_GroupSection x-toolbar x-small-editor">
                            <div style="text-transform:uppercase;"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_SelectedNodesLabel%></div>
                            <div id="notification"></div>
                        </div>
                        <div class="ncm_GroupSection x-toolbar x-small-editor">
                            <div id="confirmBtn"></div>
                        </div>
                        <div class="ncm_GroupSection x-toolbar x-small-editor">
                            <input type="checkbox" name="select-all" onchange="selectAllSelectionChange()"></input><span style="padding-left:10px;">Name</span>
                        </div>
                        <div id="selNodes" style="padding-left:2px;padding-right:2px;overflow:auto;">
                            <table class="GroupItems ZebraStripes" style="width:100%;" cellpadding="0" cellspacing="0">
                            </table>
                        </div>
                    </td>
                    <td style="padding-left:10px;"></td>
                    <td style="border:solid 1px #dcdbd7;padding:10px;width:80%;min-width:500px;" valign="top">
                        <table cellpadding="0" cellspacing="0" width="90%">
                            <tr>
                                <td colspan="2">
                                    <div><%=Resources.NCMWebContent.FirmwareUpgradeWizard_DeviceInfoScriptResultsLabel%></div>
                                    <asp:TextBox ID="txtLog" runat="server" ClientIDMode="Static" ReadOnly="True" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="100px" style="resize:vertical;min-height:100px;padding:5px;"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space:nowrap;padding-top:20px;width:20%;">
                                    <%=Resources.NCMWebContent.FirmwareUpgradeWizard_FirmwareImagesLabel%>
                                </td>
                                <td style="padding-left:10px;padding-top:20px;width:80%;white-space:nowrap;">
                                    <table id="firmwareImages" class="firmware-images" cellpadding="0" cellspacing="0">
                                    </table>
                                </td>
                            </tr>
                            <tr <%=HidePathToUploadImage ? @"style=""display:none;""" : "" %>>
                                <td style="white-space:nowrap;padding-top:10px;width:20%;">
                                    <%=Resources.NCMWebContent.FirmwareUpgradeWizard_PathToUploadImageLabel%>
                                </td>
                                <td style="padding-left:10px;padding-top:10px;width:80%;white-space:nowrap;">
                                    <asp:TextBox ID="txtUploadImagePath" runat="server" ClientIDMode="Static" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px" onblur="validateFreeSpace();"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUploadImagePath" runat="server" ClientIDMode="Static" Display="Dynamic" ControlToValidate="txtUploadImagePath" ErrorMessage="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_RequiredFieldMessage %>" style="padding-left:5px;" />
                                </td>
                            </tr>
                            <tr <%=HidePathToUploadImage ? @"style=""display:none;""" : "" %>>
                                <td></td>
                                <td style="padding-left:10px;width:80%;white-space:nowrap;">
                                    <div style="font-size:8pt;">
                                        &#0187;&nbsp;<a class="ncm-helplink" href="<%= LearnMore(@"OrionNCMFirmwareOperationUploadImagePath") %>" target="_blank"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_HowToDetermineUploadImagePathLink%></a>
                                      </div>
                                    <div id="freeSpaceValidatorHolder" style="display:none;padding-top:10px;">
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <div style="padding:50px 0px 5px 0px;">
                            <b class="expand-upgrade-options" style="text-transform:uppercase;" onclick="expandUpgradeOptions(!JSON.parse($('#expandImg').attr('expand')));"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_UpgradeOptionsLabel%></b>
                            <span>
                                <img class="expand-upgrade-options" id="expandImg" style="vertical-align:middle;" src="/Orion/NCM/Resources/images/arrowright.gif" expand="false" onclick="expandUpgradeOptions(!JSON.parse(this.attributes.expand.value));" />
                            </span>
                        </div>
                        <div style="border-bottom:solid 1px #dcdbd7;"></div>
                        
                        <table id="upgradeOptionsTable" cellpadding="0" cellspacing="0" width="90%">
                            <tr id="trSaveConfigToNVRAM">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkSaveConfigToNVRAM" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_SaveConfigToNVRAMCheckboxText %>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr id="trBackupLatestConfig">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkBackupRunningAndStartupConfigsBeforeUpgrade" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_BackupConfigsBeforeUpgradeCheckboxText %>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr id="trBackupExistingImagePath">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkBackupExisingFirmwareImage" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_BackupImageCheckboxText %>" onclick="toggleOption(this.checked, 'tableBackupExistingImagePath', 'rfvBackupExistingImagePath'); toggleBackup(this.checked, 'trVerifyBackupImage');" />
                                    <table id="tableBackupExistingImagePath" cellspacing="0" cellpadding="0" width="100%" style="padding-left:15px;padding-top:5px;">
                                        <tr>
                                            <td style="width:5%;white-space:nowrap;"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_ImagePathLabel%></td>
                                            <td style="width:95%;padding-left:10px;white-space:nowrap;">
                                                <asp:TextBox ID="txtBackupExistingImagePath" runat="server" ClientIDMode="Static" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvBackupExistingImagePath" runat="server" ClientIDMode="Static" Display="Dynamic" ControlToValidate="txtBackupExistingImagePath" ErrorMessage="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_RequiredFieldMessage %>" style="padding-left:5px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:5%;"></td>
                                            <td style="width:95%;padding-left:10px;">
                                                <div style="font-size:8pt;">
                                                    &#0187;&nbsp;<a class="ncm-helplink" href="<%= LearnMore(@"OrionNCMFirmwareOperationExistingImagePath") %>" target="_blank"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_HowToDetermineExistingImagePathLink%></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="trVerifyBackupImage">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkVerifyBackupImage" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_VerifyBackedupImageCheckboxText %>"
                                        onclick="toggleWarning(this.checked, 'verifyBackupImageWarning');"></asp:CheckBox>
                                    <div id="verifyBackupImageWarning" class="sw-suggestion sw-suggestion-warn" style="display:none; margin-top: 5px;">
                                        <div class="sw-suggestion-icon"></div>
                                        <span style="color:black;"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_BackupUploadWarningMessage%></span>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trDeleteExisingFirmwareImage">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkDeleteExisingFirmwareImage" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_DeleteImageCheckboxText %>" onclick="toggleOption(this.checked, 'tableDeleteExistingImagePath', 'rfvDeleteExistingImagePath'); validateFreeSpace();" />
                                    <table id="tableDeleteExistingImagePath" cellspacing="0" cellpadding="0" width="100%" style="padding-left:15px;padding-top:5px;">
                                        <tr>
                                            <td style="width:5%;white-space:nowrap;"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_ImagePathLabel%></td>
                                            <td style="width:95%;padding-left:10px;white-space:nowrap;">
                                                <asp:TextBox ID="txtDeleteExistingImagePath" runat="server" ClientIDMode="Static" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px" onblur="validateFreeSpace();"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDeleteExistingImagePath" runat="server" ClientIDMode="Static" Display="Dynamic" ControlToValidate="txtDeleteExistingImagePath" ErrorMessage="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_RequiredFieldMessage %>" style="padding-left:5px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:5%;"></td>
                                            <td style="width:95%;padding-left:10px;">
                                                <div style="font-size:8pt;">
                                                    &#0187;&nbsp;<a href="<%=LearnMore(@"OrionNCMFirmwareOperationExistingImagePath") %>" target="_blank"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_HowToDetermineExistingImagePathLink%></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="trVerifyUploadImage">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkVerifyUploadImage" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_VerifyUploadedImageCheckboxText %>"
                                         onclick="toggleWarning(this.checked, 'verifyUploadImageWarning');"></asp:CheckBox>
                                    <div id="verifyUploadImageWarning" class="sw-suggestion sw-suggestion-warn" style="display:none; margin-top: 5px;">
                                        <div class="sw-suggestion-icon"></div>
                                        <span style="color:black;"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_BackupUploadWarningMessage%></span>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trUpdateConfigRegister">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkUpdateConfigRegister" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_UpdateConfigRegisterCheckboxText %>" />
                                </td>
                            </tr>
                            <tr id="trUpdateBootVariable">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkUpdateBootVariable" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_UpdateBootVariableCheckboxText %>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr id="trReboot">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkReboot" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_RebootDeviceCheckboxText %>" onclick="toggleReboot(this.checked, 'UnmanageHolder'); showRebootRequiredWarning(this);"></asp:CheckBox>
                                    <div id="UnmanageHolder" style="padding-left:20px;padding-top:10px;">
                                        <asp:CheckBox ID="chkUnmanage" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_UnmanageDeviceCheckboxText %>"></asp:CheckBox>
                                    </div>
                                    <div id="rebootRequiredWarning" class="sw-suggestion sw-suggestion-warn" style="display:none; margin-top: 5px;">
                                        <div class="sw-suggestion-icon"></div>
                                        <span style="color:black;"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_RebootWarningMessage%></span>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trVerifyUpgrade">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkVerifyUpgrade" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_VerifyUpgradeCheckboxText %>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr id="trBackupRunningAndStartupConfigs">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkBackupRunningAndStartupConfigsAfterUpgrade" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_BackupConfigsAfterUpgradeCheckboxText %>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr id="trRestoreConfig">
                                <td style="white-space:nowrap;width:100%;padding-top:10px;" colspan="2">
                                    <asp:CheckBox ID="chkRestoreConfig" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_RestoreConfigsCheckboxText %>"></asp:CheckBox>
                                </td>
                            </tr>
                        </table>

                        <table cellpadding="0" cellspacing="0" width="90%">
                            <tr>
                                <td style="white-space:nowrap;width:100%;padding-top:30px;" colspan="2">
                                    <%=Resources.NCMWebContent.FirmwareUpgradeWizard_UpgradeScriptPreviewLabel%><span style="padding-left:5px;"><orion:LocalizableButton runat="server" ID="btnGenerateScript" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_GenerateScriptButtonText %>" DisplayType="Small" OnClientClick="return generateScriptPreview();" /></span>
                                    <div>
                                        <asp:TextBox ID="txtScriptPreview" runat="server" ClientIDMode="Static" ReadOnly="True" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="100px" placeholder="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_NoScriptEmptyText %>" style="resize:vertical;min-height:100px;padding:5px;"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
            </table>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" Runat="Server">
</asp:Content>