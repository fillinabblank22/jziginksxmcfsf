﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="AddNew.aspx.cs" Inherits="Orion_NCM_Firmware_AddNew" %>

<%@ Register Src="~/Orion/NCM/Firmware/Controls/FirmwareUpgradeWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div>
                <div>
                    <div><span><%= Resources.NCMWebContent.FirmwareUpgradeWizard_CreateUpgradeDescription %></span></div>
                    <img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath(@"NCM", @"FirmwareUpgrades/FirmwareWizardUpgradeFlow.PNG")%>" style="padding-left: 50px; width: 450px"/>
                </div>
                <div style="margin: 10px 0">
                    <span style="font-weight: bold;"><%= Resources.NCMWebContent.FirmwareUpgradeWizard_NameLabel %></span>
                    <div><asp:TextBox ID="txtOperationName" runat="server" Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_RequiredFieldMessage %>" style="padding-left:5px;" ControlToValidate="txtOperationName"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div style="margin: 10px 0">
                    <span style="font-weight: bold;"><%= Resources.NCMWebContent.FirmwareUpgradeWizard_SelectFirmwareUpgradeTemplateLabel %></span>
                    <div>
                        <asp:DropDownList ID="ddlFirmwareDefinitions" runat="server" Width="300px"></asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="ddlFirmwareDefinitions"></asp:RequiredFieldValidator>
                        <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" style="vertical-align: text-bottom;"/>
                        <a href="../Admin/Firmware/FirmwareDefinitions.aspx" class="RenewalsLink" target="_blank"><%= Resources.NCMWebContent.FirmwareUpgradeWizard_ManageFirmwareUpgradeTemplatesLinkText %></a>
                    </div>
                    
                </div>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" Runat="Server">
</asp:Content>

