﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="SelectNodes.aspx.cs" Inherits="Orion_NCM_Firmware_SelectNodes" %>

<%@ Register Src="~/Orion/NCM/Firmware/Controls/FirmwareUpgradeWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="padding-top: 10px;"><%=Resources.NCMWebContent.FirmwareUpgradeWizard_SelectNodesLabel%></div>
                    <div style="height: 410px; max-height: 410px; overflow: auto;">
                        <table id="nodetreecontrol_nodes_control" width="100%">
                            <tr>
                                <td id="nodes_container" runat="server">
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" Runat="Server">
</asp:Content>

